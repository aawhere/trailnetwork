/**
 * 
 */
package com.aawhere.trailhead;

import org.junit.Before;

import com.aawhere.persist.BaseObjectifyFilterRepositoryTest;

/**
 * @author aroller
 * 
 */
public class TrailheadObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<TrailheadId, Trailhead, TrailheadObjectifyRepository, TrailheadRepository, Trailheads, Trailheads.Builder> {

	private TrailheadServiceTestUtil testUtil;
	private TrailheadRepository repository;

	@Before
	public void setUp() {

		testUtil = TrailheadServiceTestUtil.create().build();
		repository = testUtil.datastoreRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected Trailhead createEntity() {
		return testUtil.persisted();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected TrailheadRepository getRepository() {
		return this.repository;
	}

}
