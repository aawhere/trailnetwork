package com.aawhere.trailhead;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.category.PersistenceTest;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 *
 */

/**
 * Tests the {@link RouteObjectifyRepository}.
 * 
 * @author Brian Chapman
 * 
 */
@Category(PersistenceTest.class)
public class TrailheadObjectifyRepositoryTest
		extends TrailheadUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();
	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	@Test
	public void testParent() throws com.google.appengine.api.datastore.EntityNotFoundException, EntityNotFoundException {
		Trailhead sample = getEntitySample();
		getRepository().store(sample);
		// Need a DatastoreTestUtil.listAllEntitites(kind) method.
		// DatastoreTestUtil.inspectDataStoreObject(sample.getId());
		// DatastoreTestUtil.inspectDatastoreObjects(Trailhead.class);
	}

	/*
	 * @see com.aawhere.trailhead.TrailheadUnitTest#createRepository()
	 */
	@Override
	protected TrailheadRepository createRepository() {
		return TrailheadServiceTestUtil.create().build().datastoreRepository();
	}

	@Override
	public void assertStored(Trailhead persisted) {
		super.assertStored(persisted);
	}

	public static TrailheadObjectifyRepositoryTest getInstance() {
		TrailheadObjectifyRepositoryTest instance = new TrailheadObjectifyRepositoryTest();
		try {
			instance.setUpObjectify();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		instance.baseSetUp();
		instance.setUp();
		return instance;
	}

}
