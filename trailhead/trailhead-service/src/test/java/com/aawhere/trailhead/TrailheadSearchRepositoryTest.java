package com.aawhere.trailhead;

import org.junit.experimental.categories.Category;

import com.aawhere.test.category.PersistenceTest;

@Category(PersistenceTest.class)
public class TrailheadSearchRepositoryTest
		extends TrailheadObjectifyRepositoryTest {

	@Override
	protected TrailheadRepository createRepository() {
		TrailheadServiceTestUtil util = TrailheadServiceTestUtil.create().build();
		return util.repository();
	}

}
