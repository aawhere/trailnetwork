/**
 *
 */
package com.aawhere.trailhead;

import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.persist.Filter;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;
import com.aawhere.queue.GaeQueueTestUtil;
import com.aawhere.queue.QueueFactory;
import com.aawhere.search.GaeSearchTestUtils;
import com.aawhere.search.SearchService;

/**
 * @author Brian Chapman
 * @author aroller
 * 
 */
public class TrailheadServiceTestUtil {

	/**
	 * Used to construct all instances of TrailheadServiceTestUtils.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadServiceTestUtil> {

		private FieldAnnotationDictionaryFactory dictionaryFactory;

		public Builder() {
			super(new TrailheadServiceTestUtil());
		}

		public Builder dictionaryFactory(FieldAnnotationDictionaryFactory dictionaryFactory) {
			this.dictionaryFactory = dictionaryFactory;
			return this;
		}

		@Override
		public TrailheadServiceTestUtil build() {
			TrailheadServiceTestUtil built = super.build();
			if (this.dictionaryFactory == null) {
				this.dictionaryFactory = FieldAnnotationTestUtil.getAnnoationFactory();
			}
			built.searchService = GaeSearchTestUtils.createGaeSearchService(this.dictionaryFactory);
			dictionaryFactory.getDictionary(Trailhead.class);
			built.datastoreRepository = new TrailheadObjectifyRepository(this.dictionaryFactory.getFactory(),
					Synchronization.SYNCHRONOUS);
			built.respository = new TrailheadSearchRepository(built.datastoreRepository, built.searchService);
			built.queueFactory = GaeQueueTestUtil.createFactory();
			;
			built.service = new TrailheadService(built.respository,
					GaeSearchTestUtils.createGaeSearchService(dictionaryFactory), built.queueFactory, dictionaryFactory);
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private TrailheadService service;
	private TrailheadRepository respository;
	private SearchService searchService;
	private QueueFactory queueFactory;
	private TrailheadObjectifyRepository datastoreRepository;

	public TrailheadService service() {
		return service;
	}

	TrailheadRepository repository() {
		return respository;
	}

	TrailheadRepository datastoreRepository() {
		return this.datastoreRepository;
	}

	public SearchService searchService() {
		return this.searchService;
	}

	/** Use {@link Builder} to construct TrailheadServiceTestUtils */
	private TrailheadServiceTestUtil() {
	}

	public Trailhead persisted(GeoCoordinate location) {
		Trailhead trailhead = TrailheadTestUtil.createTrailhead(location);
		return service.store(trailhead);
	}

	public Trailhead persisted() {
		return persisted(null);
	}

	/**
	 * Delete all entities up to {@link Filter#getMaxLimit()}
	 * 
	 * @param class1
	 */
	public void deleteAll() {
		repository().deleteAll(repository().filter(Filter.create().setLimitToMax().build()));
	}

	public Trailhead.Builder trailheadBuilder() {
		return TrailheadTestUtil.create(respository);
	}
}
