/**
 *
 */
package com.aawhere.trailhead;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import javax.measure.quantity.Length;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.ActivityTypeDistanceCategoryStatistic;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.activity.RelativeStatsActivityStatsAggregator;
import com.aawhere.activity.RelativeStatsTestUtil;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.search.FieldNotSearchableException;
import com.aawhere.search.MaxIndexAttemptsExceeded;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.UnknownIndexException;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ServiceTest;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Range;

/**
 * @see TrailheadService
 * 
 * @author Brian Chapman
 * 
 */
@Category(ServiceTest.class)
public class TrailheadServiceTest {

	private TrailheadService trailheadService;
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private TrailheadServiceTestUtil util;

	@Before
	public void setUpServices() {
		helper.setUp();
		this.util = TrailheadServiceTestUtil.create().build();
		trailheadService = this.util.service();
	}

	@After
	public void tearDownServices() {
		helper.tearDown();
	}

	@Test
	public void testStoreTrailhead() throws CannotCreateDuplicateTrailheadException {
		Trailhead th = this.util.persisted();
		assertNotNull(th.getId());
	}

	/**
	 * The area of a trailhead is bigger than the trailhead, however, it's symbol is placed at the
	 * coordinates of the trailhead. Limit the results to those where the trailhead location is
	 * within the bounds.
	 * 
	 * Should we make this an option? Option to disable?
	 */
	@Test
	public void testTrailheadNotCenteredWithinBounds() {
		Trailhead persisted = this.util.persisted();
		GeoCoordinate location = persisted.location();
		// min is 1 meter north of the centerpoint
		GeoCoordinate min = GeoMath.coordinateFrom(	location,
													MeasurementUtil.NORTH,
													MeasurementUtil.createLengthInMeters(1));
		// max is 100 meters northeast making the bounding box just miss the trailhead location, but
		// within the area
		GeoCoordinate max = GeoMath.coordinateFrom(	location,
													MeasurementUtil.NORTH_EAST,
													MeasurementUtil.createLengthInMeters(100));

		BoundingBox box = BoundingBox.create().setSouthWest(min).setNorthEast(max).build();
		Trailheads trailheads = this.trailheadService.trailheads(box);
		TestUtil.assertEmpty(trailheads);
	}

	@Test
	public void testGetTrailhead() throws EntityNotFoundException, CannotCreateDuplicateTrailheadException {
		Trailhead th = this.util.persisted();
		Trailhead retreived = trailheadService.trailhead(new TrailheadId(th.getId().getValue()));
		assertNotNull(retreived);
		assertEquals(th, retreived);
	}

	@Test
	public void testFindTrailhead_Empty() {
		Trailheads trailheads = trailheadService.near(MeasureTestUtil.createRandomGeoCoordinate());
		TestUtil.assertEmpty(trailheads.getCollection());
	}

	@Test
	public void testFilterNoConditions() {
		Trailhead persisted = this.util.persisted();
		Trailheads trailheads = this.trailheadService.trailheads(this.trailheadService.filterBuilder().build());
		TestUtil.assertSize(1, trailheads.all());
		TestUtil.assertContains(persisted, trailheads.all());
	}

	@Test
	public void testNear() {
		Trailhead gc1 = this.util.persisted(TrailheadTestUtil.GC1);
		Trailhead gc2 = this.util.persisted(TrailheadTestUtil.GC2);
		Trailheads trailheads = trailheadService.near(gc1.location());
		assertEquals(2, trailheads.size());
		TestUtil.assertContains(gc1, trailheads.all());
		TestUtil.assertContains(gc2, trailheads.all());
		Iterator<Trailhead> iterator = trailheads.iterator();
		assertEquals(gc1, iterator.next());
		assertEquals(gc2, iterator.next());
	}

	/**
	 * Ask for a trailhead by id and you can get it, but nearest will also be forgiving and provide
	 * an alias.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testNearestIdMatch() throws EntityNotFoundException {
		Trailhead loc = this.util.persisted(TrailheadTestUtil.LOC);
		Trailhead near = this.util.persisted(TrailheadTestUtil.LOC_SHOULD_MERGE_WITH_LOC);
		assertEquals("id match should have found expected", loc, trailheadService.nearest(loc.id()));
		assertEquals("id match should have found expected", near, trailheadService.nearest(near.id()));
	}

	/**
	 * Nothing persisted should not find anything and tell us so.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test(expected = EntityNotFoundException.class)
	public void testNoneNearest() throws EntityNotFoundException {
		trailheadService.nearest(TrailheadTestUtil.TARGET.id());
	}

	/**
	 * The only persisted in the area of the target is too far outside the size radius of a
	 * trailhead.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test(expected = EntityNotFoundException.class)
	public void testNearestOutOfRange() throws EntityNotFoundException {
		this.util.persisted(TrailheadTestUtil.LOC_SHOULD_NOT_MERGE_WITH_LOC);
		trailheadService.nearest(TrailheadTestUtil.TARGET.id());
	}

	/**
	 * A trailhead nearby should be found if that which we seek is not available by id.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testNearest() throws EntityNotFoundException {
		Trailhead near = this.util.persisted(TrailheadTestUtil.LOC_SHOULD_MERGE_WITH_LOC);
		// purposely don't persist loc so it won't be found by id and have to accept the nearby
		assertEquals(	"nearby trailhead should be found if loc could not",
						near,
						trailheadService.nearest(TrailheadTestUtil.TARGET.id()));
	}

	/**
	 * Creates three trailheads, two nearby and one away from the others and ensures searching finds
	 * the right ones.
	 * 
	 */
	@Test
	@Ignore("FIXME: remove this ignore.  it's not supposed to remain")
	public void testBoundingBox() {
		Trailhead trailhead = this.util.persisted();
		final Length withinRange = QuantityMath.create(TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS).dividedBy(2)
				.getQuantity();
		GeoCoordinate nearbyLocation = GeoMath.coordinateFrom(trailhead.location(), MeasurementUtil.NORTH, withinRange);
		Trailhead nearby = this.util.persisted(nearbyLocation);
		this.util.persisted(MeasureTestUtil.opposite(trailhead.location()));
		GeoCells area = GeoCells.create().add(TrailheadUtil.geoCellFrom(trailhead))
				.add(TrailheadUtil.geoCellFrom(nearby)).build();

		final GeoCellBoundingBox box = GeoCellBoundingBox.createGeoCell().setGeoCells(area).build();
		Trailheads trailheads = this.trailheadService.trailheads(box);
		TestUtil.assertContains("trailhead missing from " + box.getGeoCells() + " for " + box,
								trailhead,
								trailheads.all());
		TestUtil.assertContains("nearby missing " + nearby.area() + " -> " + trailhead.area(), nearby, trailheads.all());
		TestUtil.assertSize(2, trailheads.all());

		// try the same call, but using the bounds string in filter instead
		// purposely not using filter builder to simulate api client. service must finish
		// NOTICE: this uses the search service automatically switching
		Filter filter = Filter.create().setBounds(box.getBounds()).build();
		Trailheads trailheads2 = this.trailheadService.trailheads(filter);
		TestUtil.assertIterablesEquals("same bounds, same trailheads", trailheads, trailheads2);
	}

	@Test
	public void testFindTrailheadSingle() throws CannotCreateDuplicateTrailheadException {
		Trailhead trailhead = this.util.persisted();
		Trailheads trailheads = trailheadService.near(trailhead.location());
		assertEquals(1, trailheads.size());
		assertEquals(trailhead, trailheads.iterator().next());
	}

	/**
	 * This point (72.31684203035456,168.33510008684027) was found from flaky tests using randomly
	 * generated locations and was causing test failures.
	 */
	@Test
	public void testFindTrailheadSingle2() {
		Trailhead trailhead = this.util.persisted(GeoCoordinate.valueOf("72.31684203035456,168.33510008684027"));
		Trailheads trailheads = trailheadService.near(trailhead.location());
		assertEquals(1, trailheads.size());
		assertEquals(trailhead, trailheads.iterator().next());
	}

	@Test
	public void testFindTrailheadSingleFromDouble() throws CannotCreateDuplicateTrailheadException {
		Trailhead trailhead = this.util.persisted();
		final GeoCoordinate location = trailhead.location();
		GeoCoordinate opposite = MeasureTestUtil.opposite(location);
		Trailhead oppositeTrailhead = this.util.persisted(opposite);
		{
			Trailheads trailheads = trailheadService.near(location);
			assertEquals(1, trailheads.size());
			assertEquals(trailhead, trailheads.iterator().next());
		}
		{
			Trailheads trailheads2 = trailheadService.near(opposite);
			assertEquals(1, trailheads2.size());
			assertEquals(oppositeTrailhead, trailheads2.iterator().next());
		}
	}

	@Test
	public void testFindTrailheadSameLocation() throws CannotCreateDuplicateTrailheadException {
		GeoCoordinate coord = MeasureTestUtil.createRandomGeoCoordinate();
		Trailhead trailhead = this.util.persisted(coord);
		Trailhead trailhead2 = this.util.persisted(coord);
		MeasureTestUtil.assertEquals(trailhead.location(), trailhead2.location());
	}

	@Test
	public void testFindNearbyTrailheadAcrossCellBoundary() {
		GeoCoordinate trailheadLocation = MeasurementUtil.createGeoCoordinate(38.857149, -94.801345);
		GeoCoordinate nearbyTrailheadLocation = MeasurementUtil.createGeoCoordinate(38.856980, -94.800868);
		Trailhead trailhead = util.persisted(trailheadLocation);
		Trailhead nearbyTrailhead = util.persisted(nearbyTrailheadLocation);
		Trailheads near = trailheadService.near(trailheadLocation);
		TestUtil.assertContains(trailhead, near.all());
		TestUtil.assertContains(nearbyTrailhead, near.all());
	}

	@Test
	public void testAssignScore() throws EntityNotFoundException {
		final Integer expectedScore = TestUtil.generateRandomInt();
		final Trailhead th = TrailheadTestUtil.createTrailhead(TrailheadTestUtil.LOC);
		trailheadService.store(th);
		TestUtil.assertNotEquals("Looks like two random numbers are the same, try again.", expectedScore, th.score());

		Trailhead loadedFirst = trailheadService.trailhead(th.getId());
		assertEquals(th.score(), loadedFirst.score());

		TrailheadScoreCalculator calculator = new TrailheadScoreCalculator() {

			@Override
			public Integer score() {
				return expectedScore;
			}

			@Override
			public TrailheadId trailheadId() {
				return th.id();
			}

		};
		trailheadService.assignScore(th.id(), calculator);
		Trailhead loadedSecond = trailheadService.trailhead(th.getId());
		assertEquals(expectedScore, loadedSecond.score());
	}

	@Test
	public void testUpdateName() throws EntityNotFoundException {
		Message expectedName = new StringMessage(TestUtil.generateRandomAlphaNumeric());
		Trailhead th = TrailheadTestUtil.createTrailhead(TrailheadTestUtil.LOC);
		trailheadService.store(th);
		TestUtil.assertNotEquals("Looks like two random strings are the same, try again.", expectedName, th.name());

		Trailhead loadedFirst = trailheadService.trailhead(th.getId());
		assertEquals(th.name(), loadedFirst.name());

		trailheadService.updateName(th.id(), expectedName);
		Trailhead loadedSecond = trailheadService.trailhead(th.getId());
		assertEquals(expectedName, loadedSecond.name());
	}

	@Test
	public void testSearchTrailheadByName() throws MaxIndexAttemptsExceeded, EntityNotFoundException,
			FieldNotSearchableException, UnknownIndexException {

		Trailhead th = this.util.trailheadBuilder().name(new StringMessage("one")).build();
		// create a decoy with a different name
		Trailhead th2 = this.util.trailheadBuilder().name(new StringMessage("two")).build();
		trailheadService.store(th);
		trailheadService.searchIndexed(th.getId());
		trailheadService.searchIndexed(th2.getId());
		SearchDocuments doc = trailheadService.search(new Filter.Builder().setQuery(th.name().getValue()).build());
		assertNotNull(doc);
		TestUtil.assertSize(1, doc);

		TestUtil.assertSize(2, trailheadService.search(Filter.create().build()));

		// not a very disciplined test, but looks for any activity type in the first and ensures
		// that at least one is found and no more than two
		SortedSet<ActivityTypeStatistic> activityTypeStats = th.routeCompletionsStats().activityTypeStats();
		if (!activityTypeStats.isEmpty()) {
			SearchDocuments activityTypeDocuments = trailheadService.search(TrailheadSearchFilterBuilder.create()
					.activityType(activityTypeStats.first().category()).build());
			TestUtil.assertSize(Range.closed(1, 2), activityTypeDocuments);
			TestUtil.assertContains(activityTypeDocuments.iterator().next().index(), Layer.ACTIVITY_TYPE.toString());
		}

	}

	@Test
	public void testRelativeStarts() {
		ActivityType bike = ActivityType.BIKE;
		ActivityStats bikeStats = ActivityStats.create()
				.activityTypeCounter(ActivityTypeStatistic.counter(ImmutableList.of(bike)).build()).build();
		Trailhead bikeTrailhead = this.util.trailheadBuilder().routeCompletionStats(bikeStats).build();
		Filter bikeFilter = TrailheadSearchFilterBuilder.create().activityType(bike).relativeStartsExpanded().build();
		Trailheads trailheads = this.trailheadService.trailheads(bikeFilter);
		TestUtil.assertContainsOnly(bikeTrailhead, trailheads);
		List<RelativeStatistic> relativeStarts = trailheads.iterator().next().relativeStarts();
		TestUtil.assertNotEmpty(relativeStarts);
		RelativeStatistic relativeStart = relativeStarts.iterator().next();
		assertEquals("activityType", bike, relativeStart.activityType());
	}

	@Test
	public void testSearchTrailheadByDistanceCategory() {
		ActivityTypeDistanceCategory expectedDistanceCategory = ActivityTypeDistanceCategory.LONG_WALK;
		ActivityTypeDistanceCategoryStatistic expectedDistanceCategoryStatistic = ActivityTypeDistanceCategoryStatistic
				.create().category(expectedDistanceCategory).count(5).total(5).build();
		ImmutableSortedSet<ActivityTypeDistanceCategoryStatistic> distanceStats = ImmutableSortedSet
				.of(expectedDistanceCategoryStatistic);
		ActivityStats expectedStats = ActivityStats.create().distanceStats(distanceStats).build();
		Trailhead trailhead = this.util.trailheadBuilder().routeCompletionStats(expectedStats).build();
		TestUtil.assertContainsOnly(expectedDistanceCategoryStatistic, trailhead.routeCompletionsStats()
				.distanceStats());

		// find by distance category in the distance index
		{
			Filter filter = TrailheadSearchFilterBuilder.create().distanceCategory(expectedDistanceCategory).build();
			Trailheads trailheadsFoundForDistanceCategory = trailheadService.trailheads(filter);
			TestUtil.assertSize(1, trailheadsFoundForDistanceCategory);
		}
		// now confirm we can find the entry by activity type in the distance index
		{
			Filter activityTypeFilter = TrailheadSearchFilterBuilder.create()
					.activityType(expectedDistanceCategory.activityType).layer(Layer.ACTIVITY_TYPE_DISTANCE).build();
			Trailheads trailheadsForActivityType = trailheadService.trailheads(activityTypeFilter);
			TestUtil.assertSize(1, trailheadsForActivityType);
		}
	}

	@Test
	public void testActivityStatsBulkUpdate() throws EntityNotFoundException {
		Trailhead first = this.util.persisted();
		Trailhead second = this.util.persisted();
		TrailheadId firstTrailheadId = first.id();

		RelativeStatistic allStats = RelativeStatsTestUtil.allBuilder().featureId(firstTrailheadId).build();
		RelativeStatistic activityTypeStats = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.BIKE)
				.featureId(firstTrailheadId).build();
		RelativeStatistic personStats = RelativeStatsTestUtil.personBuilder().featureId(firstTrailheadId).build();
		RelativeStatistic personActivityTypesStats = RelativeStatsTestUtil.personActivityTypeBuilder()
				.activityType(ActivityType.BIKE).featureId(firstTrailheadId).build();
		RelativeStatsActivityStatsAggregator<TrailheadId> firstAggregator = RelativeStatsActivityStatsAggregator
				.create(firstTrailheadId)
				.statsIterator(ImmutableList.of(allStats, activityTypeStats, personStats, personActivityTypesStats)
						.iterator()).build();
		RelativeStatistic allStatsSecond = RelativeStatsTestUtil.allBuilder().featureId(second.id()).build();
		RelativeStatsActivityStatsAggregator<TrailheadId> secondAggregator = RelativeStatsActivityStatsAggregator
				.create(second.id()).statsIterator(ImmutableList.of(allStatsSecond).iterator()).build();
		Map<TrailheadId, Trailhead> trailheadActivityStatsUpdated = trailheadService
				.trailheadActivityStartStatsUpdated(ImmutableList.of(firstAggregator, secondAggregator));
		TestUtil.assertSize(2, trailheadActivityStatsUpdated.entrySet());

		Trailhead updatedFirst = this.trailheadService.trailhead(firstTrailheadId);
		assertEquals("first stats wrong", firstAggregator.activityStats().total(), updatedFirst.activityStartStats()
				.total());
		assertEquals("first person stats wrong", firstAggregator.personStats().total(), updatedFirst
				.activityStartPersonStats().total());

	}
}
