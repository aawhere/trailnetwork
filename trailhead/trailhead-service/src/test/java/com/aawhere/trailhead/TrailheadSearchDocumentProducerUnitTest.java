/**
 *
 */
package com.aawhere.trailhead;

import static org.junit.Assert.*;

import java.util.SortedSet;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.ActivityTypeDistanceCategoryStatistic;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.field.FieldKey;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.search.GaeSearchUtil;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchUtils;
import com.aawhere.test.TestUtil;

/**
 * @author Brian Chapman
 * 
 */
public class TrailheadSearchDocumentProducerUnitTest {

	private Trailhead th;
	private TrailheadSearchDocumentProducer.Builder producerBuilder;

	@Before
	public void setup() {
		th = TrailheadTestUtil.trailhead();
		producerBuilder = TrailheadSearchDocumentProducer.create();
		producerBuilder.trailhead(th);
		producerBuilder.relativeStats(RelativeStatistic.create().layer(Layer.ALL).count(TestUtil.generateRandomInt())
				.featureId(th.id()).build());
		producerBuilder.score(TestUtil.generateRandomInt());
	}

	@Test
	public void testBuilder() {
		SearchDocument doc = producerBuilder.build().searchDocument();
		assertNotNull(doc);
		FieldKey nameKey = new FieldKey(Trailhead.FIELD.DOMAIN, Trailhead.FIELD.NAME);
		assertNotNull(SearchUtils.findField(doc, SearchUtils.key(nameKey)));
		assertNotNull(SearchUtils.findField(doc, GaeSearchUtil.substringKey(nameKey)));
	}

	@Test
	public void testSearchDocsForPublic() {
		ActivityStats routeCompletionsStats = th.routeCompletionsStats();
		SortedSet<ActivityTypeStatistic> activityTypeStats = routeCompletionsStats.activityTypeStats();
		assertNotNull(activityTypeStats);
		SortedSet<ActivityTypeDistanceCategoryStatistic> distanceStats = routeCompletionsStats.distanceStats();
		assertNotNull(distanceStats);
		Iterable<SearchDocument> searchDocuments = TrailheadSearchDocumentProducer.searchDocuments(th);
		int oneForAll = 1;
		int expectedNumberOfDocs = oneForAll + activityTypeStats.size() + distanceStats.size();
		TestUtil.assertSize(expectedNumberOfDocs, searchDocuments);
	}

	@Test
	public void testPersonalWithDistance() {
		RelativeStatistic relativeStatistic = RelativeStatistic.create()
				.distance(ActivityTypeDistanceCategory.LONG_BIKE).personId(PersonTestUtil.personId())
				.featureId(TrailheadTestUtil.trailheadId()).count(5).build();
		SearchDocument searchDocument = TrailheadSearchDocumentProducer
				.searchDocumentForPersonal(th, relativeStatistic);
		TestUtil.assertContains(searchDocument.index(), Layer.PERSON_ACTIVITY_TYPE_DISTANCE.name());
	}
}
