package com.aawhere.di;

import com.aawhere.trailhead.TrailheadProvider;
import com.aawhere.trailhead.TrailheadRepository;
import com.aawhere.trailhead.TrailheadSearchRepository;
import com.aawhere.trailhead.TrailheadService;
import com.google.inject.AbstractModule;

/**
 * @author brian
 * 
 */
public class TrailheadModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(TrailheadRepository.class).to(TrailheadSearchRepository.class);
		bind(TrailheadProvider.class).to(TrailheadService.class);
	}

}
