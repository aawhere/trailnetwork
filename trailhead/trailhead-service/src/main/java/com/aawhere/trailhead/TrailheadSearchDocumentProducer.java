/**
 *
 */
package com.aawhere.trailhead;

import java.util.Map;
import java.util.SortedSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategoryStatistic;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.activity.RelativeStatsUtil;
import com.aawhere.collections.IterablesExtended;
import com.aawhere.field.FieldKey;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.person.PersonId;
import com.aawhere.search.GaeSearchQueryProducer;
import com.aawhere.search.GaeSearchUtil;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocument.Field;
import com.aawhere.search.SearchDocuments;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

/**
 * A {@link SearchDocument} for the Trailhead domain. These documents are separated by
 * 
 * @author Brian Chapman
 * 
 */
public class TrailheadSearchDocumentProducer {

	/**
	 * The resulting search document to be indexed.
	 * 
	 */
	private SearchDocument searchDocument;

	/**
	 * Used to construct all instances of TrailheadSearchDocument.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadSearchDocumentProducer> {
		private Integer score;
		private Trailhead trailhead;
		private RelativeStatistic relativeStats;

		public Builder() {
			super(new TrailheadSearchDocumentProducer());
		}

		/** @see #score */
		public Builder score(Integer score) {
			this.score = score;
			return this;
		}

		private void location(Trailhead trailhead, SearchDocument.Builder builder) {
			builder.addField(SearchDocument.Field.create().key(GaeSearchQueryProducer.FIELD.KEY.LOCATION)
					.value(trailhead.location()).build());
		}

		private Field trailheadNameField(Trailhead th, FieldKey nameKey) {
			return Field.create().dataType(FieldDataType.TEXT).key(nameKey).value(th.name().getValue()).build();
		}

		private void trailheadNameSubstringsField(SearchDocument.Builder builder, Trailhead th, FieldKey nameKey) {
			builder.addFields(GaeSearchUtil.substringFields(th.name().getValue(), nameKey));
		}

		public Builder relativeStats(RelativeStatistic relativeStats) {
			this.relativeStats = relativeStats;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.search.SearchDocument.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("score", this.score);
			Assertion.exceptions().notNull("trailhead", this.trailhead);
			Assertion.exceptions().notNull("relativeStats", this.relativeStats);

		}

		@Override
		public TrailheadSearchDocumentProducer build() {
			TrailheadSearchDocumentProducer built = super.build();
			FieldKey nameKey = TrailheadField.Key.NAME;

			SearchDocument.Builder builder = new SearchDocument.Builder();
			// domain and index assigned in mapLayers
			String indexName = TrailheadSearchFilterBuilder.indexName(relativeStats.layer());
			builder.index(indexName);
			// we must use the complex id to ensure unqiueness since trailhead id may show up twice
			// when including activity types or persons
			builder.id(relativeStats.id());
			builder.rank(score);
			builder.addField(SearchDocument.Field.create().key(TrailheadField.Key.SCORE).value(score).build());
			trailheadId(builder);
			relativeStarts(builder);
			if (!TrailheadUtil.isNameEmpty(trailhead.name())) {
				builder.addField(trailheadNameField(trailhead, nameKey));
				trailheadNameSubstringsField(builder, trailhead, nameKey);
			}
			location(trailhead, builder);
			mapLayers(builder);

			built.searchDocument = builder.build();
			return built;
		}

		private void trailheadId(SearchDocument.Builder builder) {
			builder.addField(SearchDocument.Field.create().key(TrailheadSearchFilterBuilder.TRAILHEAD_ID)
					.value(trailhead.id()).build());
		}

		private void relativeStarts(SearchDocument.Builder builder) {
			builder.addField(SearchDocument.Field.create().key(TrailheadSearchFilterBuilder.COUNT)
					.value(this.relativeStats.count()).build());
		}

		private void mapLayers(SearchDocument.Builder builder) {

			if (relativeStats != null) {
				ActivityType activityType = relativeStats.activityType();
				if (activityType != null) {
					builder.addField(SearchDocument.Field.create().key(TrailheadSearchFilterBuilder.ACTIVITY_TYPE)
							.value(activityType).build());
				}
				if (relativeStats.hasDistance()) {
					builder.addField(SearchDocument.Field.create().key(TrailheadSearchFilterBuilder.DISTANCE_CATEGORY)
							.value(relativeStats.distance()).build());
				}
				PersonId personId = relativeStats.personId();
				if (personId != null) {
					builder.addField(SearchDocument.Field.create().key(TrailheadSearchFilterBuilder.PERSON_ID)
							.value(personId).build());
				}
			}
		}

		public Builder trailhead(Trailhead th) {
			this.trailhead = th;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct TrailheadSearchDocument */
	private TrailheadSearchDocumentProducer() {
	}

	public static Builder create() {
		return new Builder();
	}

	public SearchDocument searchDocument() {
		return searchDocument;
	}

	public static Function<Iterable<Trailhead>, Iterable<SearchDocument>> documentsFromTrailheadsFunction() {
		return IterablesExtended.composeSingleToPlurals(documentsFromTrailhedFunction());
	}

	public static Function<Trailhead, Iterable<SearchDocument>> documentsFromTrailhedFunction() {
		return new Function<Trailhead, Iterable<SearchDocument>>() {

			@Override
			public Iterable<SearchDocument> apply(Trailhead input) {
				return searchDocuments(input);
			}
		};
	}

	public static Iterable<SearchDocument> searchDocuments(Iterable<Trailhead> trailheads) {
		return Iterables.concat(Iterables.transform(trailheads, documentsFromTrailhedFunction()));
	}

	/**
	 * The main method for creating search documents from a Trailhead. This applies only to public
	 * activities, not personal since that information is created for {@link RelativeStatistic} from
	 * the original trailhead.
	 * 
	 * Multiple documents are returned since the {@link Layer#ALL} and {@link Layer#ACTIVITY_TYPE}
	 * are created. There may be many activity types.
	 * 
	 * 
	 * @param trailhead
	 * @return
	 */
	@Nonnull
	public static Iterable<SearchDocument> searchDocuments(@Nonnull Trailhead trailhead) {
		SearchDocuments.Builder documents = SearchDocuments.create();
		if (TrailheadUtil.scoreIsSignificantForPublic(trailhead)) {
			TrailheadId trailheadId = trailhead.id();
			ActivityStats routeCompletionsStats = trailhead.routeCompletionsStats();
			// routeCompletionStats is required...so incomplete trailheads are not searchable
			if (routeCompletionsStats != null) {
				// one document for the all category
				{
					TrailheadSearchDocumentProducer.Builder searchDocumentBuilder = TrailheadSearchDocumentProducer
							.create();
					Layer layer = Layer.ALL;
					Integer routeCompletionCount = routeCompletionsStats.total();
					RelativeStatistic allStats = RelativeStatistic.create().layer(layer).count(routeCompletionCount)
							.featureId(trailhead.id()).build();
					searchDocumentBuilder.relativeStats(allStats);
					searchDocumentBuilder.trailhead(trailhead).score(trailhead.score());
					SearchDocument searchDocument = searchDocumentBuilder.build().searchDocument();
					documents.add(searchDocument);
				}
				// multiple documents...one for each activity type

				if (routeCompletionsStats.hasActivityTypeStats()) {
					ImmutableList.Builder<SearchDocument> activityTypeDocsBuilder = ImmutableList.builder();
					SortedSet<ActivityTypeStatistic> allActivityTypeStats = routeCompletionsStats.activityTypeStats();
					for (ActivityTypeStatistic activityTypeStats : allActivityTypeStats) {
						RelativeStatistic relativeStats = RelativeStatistic.create().layer(Layer.ACTIVITY_TYPE)
								.activityType(activityTypeStats.category()).count(activityTypeStats.count())
								.featureId(trailheadId).build();
						Integer relativeScore = RelativeStatsUtil.score(trailhead.score(),
																		activityTypeStats.getPercent());
						TrailheadSearchDocumentProducer producer = TrailheadSearchDocumentProducer.create()
								.trailhead(trailhead).relativeStats(relativeStats).score(relativeScore).build();
						activityTypeDocsBuilder.add(producer.searchDocument());
					}
					ImmutableList<SearchDocument> activityTypeDocs = activityTypeDocsBuilder.build();
					documents.addAll(activityTypeDocs);
				}
				// distance stats
				// FIXME:This block of code was copied from activity type block above
				// we can delegate common method
				if (routeCompletionsStats.hasDistanceStats()) {
					ImmutableList.Builder<SearchDocument> docsBuilder = ImmutableList.builder();
					SortedSet<ActivityTypeDistanceCategoryStatistic> allActivityTypeStats = routeCompletionsStats
							.distanceStats();
					for (ActivityTypeDistanceCategoryStatistic stat : allActivityTypeStats) {
						RelativeStatistic relativeStats = RelativeStatistic.create()
								.layer(Layer.ACTIVITY_TYPE_DISTANCE).distance(stat.category()).count(stat.count())
								.featureId(trailheadId).build();
						Integer relativeScore = RelativeStatsUtil.score(trailhead.score(), stat.getPercent());
						TrailheadSearchDocumentProducer producer = TrailheadSearchDocumentProducer.create()
								.trailhead(trailhead).relativeStats(relativeStats).score(relativeScore).build();
						docsBuilder.add(producer.searchDocument());
					}
					ImmutableList<SearchDocument> docs = docsBuilder.build();
					documents.addAll(docs);
				}

			}
		}

		SearchDocuments docs = documents.build();
		return docs;
	}

	/**
	 * Creates a single {@link SearchDocument} from the trailhead given choosing the proper index
	 * and adjusting the score to be relative to the personal contribution.
	 * 
	 * @param trailhead
	 * @param personalStats
	 * @return
	 */
	@Nullable
	public static SearchDocument searchDocumentForPersonal(@Nonnull Trailhead trailhead,
			@Nonnull RelativeStatistic personalStats) {
		// if the stars don't align then the result is null
		SearchDocument result = null;

		if (trailhead.routeCompletionsStats() != null && trailhead.routeCompletionsStats().total() > 0) {
			if (personalStats.layer().personal) {
				ActivityType activityType = personalStats.activityType();
				Ratio percentPersonCompletedOfTotal = null;
				if (activityType != null) {
					SortedSet<ActivityTypeStatistic> activityTypeStats = trailhead.routeCompletionsStats()
							.activityTypeStats();
					if (activityTypeStats != null) {
						ActivityTypeStatistic statsForCategory = ActivityTypeStatistic
								.statsForCategory(activityTypeStats, activityType);
						if (statsForCategory != null) {
							Ratio percentForActivityType = statsForCategory.getPercent();
							Ratio percentForPersonOfActivityType = MeasurementUtil.ratio(	personalStats.count(),
																							statsForCategory.count());
							// multiplying gives the portion the person contributed to the portion
							// the
							// activity type contributed
							percentPersonCompletedOfTotal = QuantityMath.create(percentForActivityType)
									.times(percentForPersonOfActivityType).getQuantity();
						}
					}
				}
				// just use the total...which gives the person the biggests disadvantage since the
				// total
				// is the biggest number (compared to activity type)
				if (percentPersonCompletedOfTotal == null) {
					percentPersonCompletedOfTotal = MeasurementUtil.ratio(personalStats.count(), trailhead
							.routeCompletionsStats().total());
				}
				// TN-862 zero scoring trailheads cause equality
				// adding 100 makes the percentage distribution significant
				// adding the count ensures this person's popularity affects compared to self
				// addign instead of multiplying ensures it does not overweight
				int personallyInflatedScore = trailhead.score() + 100 + personalStats.count();
				Integer score = RelativeStatsUtil.score(personallyInflatedScore, percentPersonCompletedOfTotal);

				result = TrailheadSearchDocumentProducer.create().trailhead(trailhead).relativeStats(personalStats)
						.score(score).build().searchDocument();
			} else {
				throw new InvalidChoiceException(personalStats.layer(), Layer.PERSON, Layer.PERSON_ACTIVITY_TYPE,
						Layer.PERSON_ACTIVITY_TYPE_DISTANCE);
			}
		} else {
			LoggerFactory.getLogger(TrailheadSearchUtil.class).warning(trailhead.id()
					+ " being skipped because it doesn't have completion stats");
		}

		return result;
	}

	private enum PersonalSearchDocumentFunction
			implements
			Function<Map.Entry<Trailhead, RelativeStatistic>, SearchDocument> {
		INSTANCE;
		@Override
		@Nullable
		public SearchDocument apply(@Nullable Map.Entry<Trailhead, RelativeStatistic> input) {
			return searchDocumentForPersonal(input.getKey(), input.getValue());
		}

		@Override
		public String toString() {
			return "PersonalSearchDocumentsFunction";
		}
	}

	/**
	 * calls {@link #searchDocumentForPersonal(Trailhead, RelativeStatistic)}
	 * 
	 * @return
	 */
	public static Function<Map.Entry<Trailhead, RelativeStatistic>, SearchDocument> personalSearchDocumentFunction() {
		return PersonalSearchDocumentFunction.INSTANCE;
	}
}
