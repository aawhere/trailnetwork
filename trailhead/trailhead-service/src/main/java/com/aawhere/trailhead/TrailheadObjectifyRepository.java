/**
 *
 */
package com.aawhere.trailhead;

import com.aawhere.activity.ActivityStats;
import com.aawhere.app.IdentityTranslators;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.measure.geocell.MeasureTranslators;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.persist.ObjectifyFilterRepositoryDelegate;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.aawhere.trailhead.Trailhead.Builder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author brian
 * @author aroller
 * 
 */
@Singleton
public class TrailheadObjectifyRepository
		extends ObjectifyFilterRepository<Trailhead, TrailheadId, Trailheads, Trailheads.Builder>
		implements TrailheadRepository {

	private TrailheadRepositoryDelegate delegate;

	static {
		IdentityTranslators.add(ObjectifyService.factory());
		MeasureTranslators.add(ObjectifyService.factory());
		ObjectifyService.register(Trailhead.class);
	}

	@Inject
	public TrailheadObjectifyRepository(FieldDictionaryFactory factory) {
		this(factory, SYNCHRONIZATION_DEFAULT);
	}

	/**
	 * @See {@link ObjectifyRepository#ObjectifyRepository(boolean)}
	 * @param syncronize
	 */
	public TrailheadObjectifyRepository(FieldDictionaryFactory factory, Synchronization syncronize) {
		super(new Delegate(factory), syncronize);
		this.delegate = new TrailheadRepositoryDelegate(this);
	}

	static class Delegate
			extends ObjectifyFilterRepositoryDelegate<Trailheads, Trailheads.Builder, Trailhead, TrailheadId> {
		/**
		 * @param factory
		 * 
		 */
		public Delegate(FieldDictionaryFactory factory) {
			super(factory);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.trailhead.TrailheadRepository#setScore(com.aawhere.trailhead.TrailheadId,
	 * java.lang.Integer)
	 */
	@Override
	public Trailhead setScore(TrailheadId id, Integer score) throws EntityNotFoundException {
		return this.delegate.setScore(id, score);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.trailhead.TrailheadRepository#setRouteCompletionStats(com.aawhere.trailhead.
	 * TrailheadId, com.aawhere.activity.ActivityStats)
	 */
	@Override
	public Trailhead setRouteCompletionStats(TrailheadId trailheadId, ActivityStats routeCompletionStats)
			throws EntityNotFoundException {
		return this.delegate.setRouteCompletionStats(trailheadId, routeCompletionStats);
	}

	@Override
	public Builder update(TrailheadId id) throws EntityNotFoundException {
		return delegate.update(id);
	}

	@Override
	public Trailhead update(Trailhead entity) {
		return super.update(entity);
	}

}
