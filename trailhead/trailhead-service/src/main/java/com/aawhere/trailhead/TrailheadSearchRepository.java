package com.aawhere.trailhead;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.search.GaeSearchRepository;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.trailhead.Trailheads.Builder;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class TrailheadSearchRepository
		extends GaeSearchRepository<Trailheads, Trailhead, TrailheadId>
		implements TrailheadRepository {

	@Inject
	public TrailheadSearchRepository(TrailheadObjectifyRepository datastoreRepository, SearchService searchService) {
		super(datastoreRepository, searchService);
	}

	TrailheadObjectifyRepository datastoreRepository() {
		return (TrailheadObjectifyRepository) datastoreRepository;
	}

	@Override
	public Trailhead setScore(TrailheadId id, Integer score) throws EntityNotFoundException {

		return index(datastoreRepository().setScore(id, score));
	}

	@Override
	public Trailhead setRouteCompletionStats(TrailheadId trailheadId, ActivityStats routeCompletionStats)
			throws EntityNotFoundException {
		Trailhead trailhead = datastoreRepository().setRouteCompletionStats(trailheadId, routeCompletionStats);
		index(trailhead);
		return trailhead;
	}

	@Override
	public Trailheads entities(Iterable<Trailhead> entities, SearchDocuments searchDocuments) {
		Builder builder = Trailheads.create();
		Filter filter = searchDocuments.getFilter();
		if (filter.containsOption(SearchWebApiUri.SEARCH)) {
			builder.addSupport(searchDocuments);
		}
		if (TrailheadSearchFilterBuilder.relativeStatsExpandRequested(filter)) {
			TrailheadSearchUtil.relativeStatsAdded(entities, searchDocuments);
		}
		return builder.addAll(entities).setFilter(filter).build();
	}

	@Override
	protected Iterable<TrailheadId> idsFromSearchDocumentIds(Iterable<String> searchDocumentIds) {
		return TrailheadSearchUtil.trailheadIdsFromDocumentIds(searchDocumentIds);
	}

	@Override
	protected Function<TrailheadId, Iterable<Filter>> deleteAllFromIdsFilterFunction() {
		return new Function<TrailheadId, Iterable<Filter>>() {

			@Override
			public Iterable<Filter> apply(TrailheadId trailheadId) {
				return ImmutableList.of(TrailheadSearchFilterBuilder.create().allIndexes().trailheadId(trailheadId)
						.build());
			}
		};
	}

	@Override
	protected Iterable<SearchDocument> searchDocuments(Iterable<Trailhead> entities) {

		return TrailheadSearchDocumentProducer.searchDocuments(entities);
	}

	@Override
	protected Filter prepareForSearch(Filter filter) {
		return TrailheadSearchFilterBuilder.clone(filter).build();
	}

	@Override
	protected Filter prepareForDatastore(Filter filter) {
		return TrailheadDatastoreFilterBuilder.clone(filter).build();
	}

	@Override
	public Trailhead.Builder update(TrailheadId id) throws EntityNotFoundException {
		return new Trailhead.Builder(load(id)) {
			@Override
			public Trailhead build() {
				return update(super.build());
			}
		};
	}

	@Override
	protected ImmutableSet<String> indexNames() {
		return ImmutableSet.of(	TrailheadSearchUtil.indexName(Layer.ALL),
								TrailheadSearchUtil.indexName(Layer.ACTIVITY_TYPE),
								TrailheadSearchUtil.indexName(Layer.ACTIVITY_TYPE_DISTANCE));
	}
}
