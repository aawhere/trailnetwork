package com.aawhere.trailhead;

import java.util.Map;

import javax.annotation.Nullable;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivitySearchUtil;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.activity.RelativeStatsUtil;
import com.aawhere.field.DomainWrongException;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.person.PersonId;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchUtils;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

/**
 * Specializes in translating {@link Trailhead} into {@link SearchDocument}.
 * 
 * @author aroller
 * 
 */
public class TrailheadSearchUtil {

	/**
	 * provides the corresponding {@link TrailheadId}
	 * 
	 * @param document
	 * @return
	 */
	@Nullable
	public static TrailheadId trailheadId(SearchDocument document) throws InvalidArgumentException {
		return (document != null) ? trailheadId(document.id()) : null;
	}

	/**
	 * Deciphers the {@link SearchDocument#id()} into it's corresponding {@link TrailheadId}.
	 * 
	 * @see SearchUtils#ids(Iterable)
	 * @see SearchUtils#idFunction()
	 * @return
	 */
	public static TrailheadId trailheadId(String searchDocumentId) throws DomainWrongException {
		String idValue = RelativeStatsUtil.featureIdForDomain(searchDocumentId, TrailheadField.DOMAIN);
		if (idValue != null) {
			return new TrailheadId(idValue);
		} else {
			return null;
		}
	}

	public static Function<SearchDocument, TrailheadId> trailheadIdFunction() {
		return new Function<SearchDocument, TrailheadId>() {

			@Override
			public TrailheadId apply(SearchDocument input) {
				return trailheadId(input);

			}
		};
	}

	public static Function<String, TrailheadId> trailheadIdFromDocumentIdFunction() {
		return new Function<String, TrailheadId>() {

			@Override
			public TrailheadId apply(String input) {
				return trailheadId(input);
			}
		};
	}

	public static Iterable<TrailheadId> trailheadIdsFromDocumentIds(Iterable<String> documentIds) {
		return Iterables.transform(documentIds, trailheadIdFromDocumentIdFunction());
	}

	public static Iterable<TrailheadId> trailheadIds(Iterable<SearchDocument> searchDocuments) {
		return Iterables.transform(searchDocuments, trailheadIdFunction());
	}

	public static TrailheadId trailheadId(RelativeStatistic relativeStats) {
		return trailheadId(relativeStats.id());

	}

	/**
	 * Provides all layers available with information from the Activity.
	 * 
	 * @param trailhead
	 * @param activity
	 * @param personForAccount
	 * @return
	 */
	public static ImmutableList<RelativeStatistic> relativeStats(Trailhead trailhead, Activity activity,
			PersonId personForAccount) {
		Layer[] values = RelativeStatistic.Layer.values();
		Builder<RelativeStatistic> listBuilder = ImmutableList.builder();
		for (Layer layer : values) {
			com.aawhere.activity.RelativeStatistic.Builder statsBuilder = RelativeStatistic.create();
			statsBuilder.featureId(trailhead.getId());
			statsBuilder.layer(layer);
			if (layer.activityTyped) {
				statsBuilder.activityType(activity.getActivityType());
			}
			if (layer.personal) {
				statsBuilder.personId(personForAccount);
			}
			listBuilder.add(statsBuilder.build());
		}
		return listBuilder.build();
	}

	/**
	 * Mutates the given entities adding the relativeStatstics derived from the search documents.
	 * 
	 * @param entities
	 * @param searchDocuments
	 * @return
	 */
	public static Map<TrailheadId, Trailhead> relativeStatsAdded(Iterable<Trailhead> entities,
			SearchDocuments searchDocuments) {
		// /FIXME: This was all copied from route...very similar
		Map<TrailheadId, Trailhead> trailheadsMap = IdentifierUtils.map(ImmutableSet.<Trailhead> builder()
				.addAll(entities).build());
		// we need to force mutation so iterating now.
		for (SearchDocument searchDocument : searchDocuments) {
			TrailheadId trailheadId = trailheadId(searchDocument);
			Trailhead trailhead = trailheadsMap.get(trailheadId);
			if (trailhead != null) {
				RelativeStatistic relativeStats = relativeStats(searchDocument);
				Trailhead.mutate(trailhead).relativeStartsAdded(relativeStats).build();
			} else {
				LoggerFactory.getLogger(TrailheadSearchUtil.class).warning(searchDocument.id()
						+ " provided missing route " + trailheadId);
			}
		}
		return trailheadsMap;

	}

	public static RelativeStatistic relativeStats(SearchDocument searchDocument) {
		return ActivitySearchUtil.relativeStats(searchDocument,
												TrailheadSearchFilterBuilder.PERSON_ID,
												TrailheadSearchFilterBuilder.ACTIVITY_TYPE,
												TrailheadSearchFilterBuilder.DISTANCE_CATEGORY,
												TrailheadSearchFilterBuilder.COUNT);
	}

	public static String indexName(Layer layer) {
		return SearchUtils.indexName(TrailheadField.DOMAIN, layer.name());
	}
}
