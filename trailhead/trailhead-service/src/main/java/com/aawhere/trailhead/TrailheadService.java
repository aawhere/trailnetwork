package com.aawhere.trailhead;

import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.RelativeStatsActivityStatsAggregator;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.persist.RepositoryUtil;
import com.aawhere.queue.QueueFactory;
import com.aawhere.search.FieldNotSearchableException;
import com.aawhere.search.MaxIndexAttemptsExceeded;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.search.UnknownIndexException;
import com.aawhere.track.TrackSummary;
import com.aawhere.trailhead.Trailhead.Builder;
import com.aawhere.util.rb.Message;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * ServiceStandard for {@link Trailhead}
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class TrailheadService
		extends EntityService<Trailheads, Trailhead, TrailheadId>
		implements TrailheadProvider {

	/**
	 * See {@link Resolution} for approximate size of rectangle created by GeoCells.
	 */
	static final Resolution FIND_RESOLUTION = TrailheadDatastoreFilterBuilder.FIND_RESOLUTION;
	private final Length NEAR_RADIUS = TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS;
	private TrailheadRepository repository;
	private SearchService searchService;
	private QueueFactory queueFactory;

	@Inject
	public TrailheadService(TrailheadRepository trailheadRepository, SearchService searchService,
			QueueFactory queueFactory, FieldAnnotationDictionaryFactory dictionaryFactory) {
		super(trailheadRepository);
		this.repository = trailheadRepository;
		this.searchService = searchService;
		this.queueFactory = queueFactory;
	}

	/**
	 * Retrieve an {@link Trailhead} from the {@link TrailheadRepository}.
	 * 
	 * Previously this found a fine representation, but TN-561 explains the complications so it is
	 * back to simple access for those that wish to be notified when a trailhead does not exist by
	 * id.
	 * 
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 *             when the exact trailhead isn't found
	 */
	@Override
	public Trailhead trailhead(TrailheadId trailheadId) throws EntityNotFoundException {
		return repository.load(trailheadId);
	}

	@Override
	public Trailhead entity(TrailheadId id) throws EntityNotFoundException {
		return nearest(id);
	}

	/**
	 * First attempts to find by the id given. If not found, then finds the nearest
	 * {@link Trailhead} to the given {@link TrailheadId}. This allows for clients with a stale
	 * reference to a non-existent trailhead that has been replaced by a nearby neighbor or "alias".
	 * 
	 * @see #NEAR_RADIUS
	 * @see #trailhead(TrailheadId)
	 * @see #near(TrailheadId)
	 * @see #best(GeoCoordinate)
	 * 
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 *             if none are in the nearby area.
	 */
	@Override
	public Trailhead nearest(TrailheadId trailheadId) throws EntityNotFoundException {
		Trailhead nearest;
		try {
			// looking and finding a match by id is the most common scenario so use it
			nearest = trailhead(trailheadId);
		} catch (EntityNotFoundException e) {
			// aliases will causes near misses...search for the nearest
			Trailheads near = near(trailheadId);
			nearest = RepositoryUtil.finderResult(trailheadId, near);
		}
		return nearest;

	}

	/**
	 * Finds the nearest existing {@link Trailhead} matching the id using
	 * {@link #nearest(TrailheadId)}, but if none are found then it creates a Trailhead using
	 * {@link #created(GeoCoordinate)}.
	 * 
	 * @param trailheadId
	 * @return
	 */
	public Trailhead nearestCreated(TrailheadId trailheadId) {
		try {
			return nearest(trailheadId);
		} catch (EntityNotFoundException e) {
			final GeoCoordinate coordinate = TrailheadUtil.coordinateFrom(trailheadId);
			return created(coordinate);
		}
	}

	/**
	 * @param coordinate
	 * @return
	 */
	private Trailhead created(final GeoCoordinate coordinate) {
		return store(Trailhead.create().location(coordinate).build());
	}

	/**
	 * Searches for the nearest using {@link #near(GeoCoordinate)}, but creates a {@link Trailhead}
	 * using {@link #created(GeoCoordinate)} if none are found.
	 * 
	 * @param location
	 * @return
	 */
	public Trailhead nearestCreated(GeoCoordinate location) {
		Trailheads near = near(location);
		if (near.isEmpty()) {
			return created(location);
		} else {
			return near.iterator().next();
		}
	}

	/**
	 * Given some collection of ids this will provide the alias that has replaced such id or will
	 * return the original if no replacement has been made or if no suitable alias can be found.
	 * 
	 * @see #nearest(TrailheadId)
	 * 
	 * @param ids
	 * @return
	 */
	public Iterable<TrailheadId> aliases(Iterable<TrailheadId> ids) {
		return Iterables.transform(ids, nearestFunction());
	}

	/**
	 * Provides all trailheads in the nearby area of the coordinates derived from the
	 * {@link TrailheadId}.
	 * 
	 * * @see #NEAR_RADIUS
	 * 
	 * @param trailheadId
	 * @return
	 */
	public Trailheads near(TrailheadId trailheadId) {
		return near(TrailheadUtil.coordinateFrom(trailheadId));
	}

	/**
	 * searches near using the {@link #NEAR_RADIUS}
	 * 
	 * @see #near(GeoCoordinate, Length)
	 * 
	 * 
	 * @param location
	 * @return
	 */
	public Trailheads near(GeoCoordinate location) {
		return near(location, NEAR_RADIUS);
	}

	/**
	 * Provides those trailheads near the given location within the range provided by the given
	 * radius. The results are sorted in order of distance from the location and are guaranteed to
	 * be no farther away than the given radius.
	 * 
	 * @param location
	 * @param resolution
	 * @return
	 */
	public Trailheads near(GeoCoordinate location, Length radius) {
		// TODO:use the SearchService to fulfill this better

		// be sloppy on the datastore search since the distance limit will be applied
		Length wideArea = QuantityMath.create(radius).times(2).getQuantity();

		TrailheadDatastoreFilterBuilder filterBuilder = filterBuilder();
		filterBuilder.area().intersection(location, wideArea);
		Filter filter = filterBuilder.build();
		Trailheads areaTrailheads = trailheads(filter);
		// since geocells aren't accurate we filter in code for radius limits
		Iterable<Trailhead> filtered = TrailheadUtil.trailheadsWithinProximity(areaTrailheads, location, radius);
		return TrailheadUtil.sortNearest(filtered, location);
	}

	/**
	 * Returns trailheads that match the filter. If no order is provided, returns the trailheads by
	 * score in decreasing order.
	 * 
	 * @param filter
	 * @return
	 */
	public Trailheads trailheads(Filter filter) {
		return repository.filter(filter);
	}

	/**
	 * Given a trailhead this will persist it returning the trailhead with the given id.
	 * 
	 * @param trailhead
	 * @return
	 */
	public Trailhead store(Trailhead trailhead) {
		return repository.store(trailhead);

	}

	public void delete(TrailheadId id) {
		repository.delete(id);
	}

	/**
	 * Provides the ability for extermal management of deleting ids. This should be used with
	 * caution.
	 * 
	 * @param all
	 */
	public void delete(Iterable<TrailheadId> all) {
		this.repository.deleteAllFromIds(all);
	}

	/**
	 * Provides all trailheads within the bounds up to the maximum limit.
	 * 
	 * @param bounds
	 * @return
	 */
	public Trailheads trailheads(BoundingBox bounds) {
		return trailheads(filterBuilder().build(), bounds);
	}

	/**
	 * Return within the given bounds limited to the count given.
	 * 
	 * FIXME:BoundingBox needs to be integrated into Filter.
	 * 
	 * @param bounds
	 * @param limit
	 * @return
	 */
	public Trailheads trailheads(BoundingBox bounds, Integer limit) {
		return trailheads(Filter.create().setLimit(limit).build(), bounds);
	}

	/**
	 * Filters out any trialheads based on the given filter and optionally within the given bounding
	 * box.
	 * 
	 * @param filter
	 * @param bounds
	 *            if provides limits to the area indicated.
	 * @return
	 */
	public Trailheads trailheads(Filter filter, @Nullable BoundingBox bounds) {

		TrailheadDatastoreFilterBuilder filterBuilder = filterBuilder(filter);
		if (bounds != null) {
			filterBuilder.area().intersection(bounds);
			filterBuilder.withinBounds(bounds);
		}
		final Trailheads trailheads = trailheads(filterBuilder.build());
		return trailheads;
	}

	/**
	 * Applies common properties to the filter for trailheads.
	 * 
	 * @return
	 */
	public TrailheadDatastoreFilterBuilder filterBuilder() {
		return TrailheadDatastoreFilterBuilder.create();
	}

	public TrailheadDatastoreFilterBuilder filterBuilder(Filter filter) {
		return TrailheadDatastoreFilterBuilder.clone(filter);
	}

	/**
	 * Updates only the score to the trailhead provided. The score will be assigned to the version
	 * stored in the database unless the trailhead can't be found...in such a case an
	 * {@link EntityNotFoundException} will be thrown.
	 * 
	 * {@link #trailhead(TrailheadId)} is used to retrieve the latest version so there is a chance
	 * you will get back a different trailhead than you gave.
	 * 
	 * @param th
	 * @param build
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Trailhead assignScore(TrailheadId thId, TrailheadScoreCalculator calculator) throws EntityNotFoundException {
		Trailhead th = trailhead(thId);
		th = Trailhead.mutate(th).score(calculator.score()).build();
		store(th);
		return th;
	}

	/**
	 * TN-563 declares this service should be in charge of naming making this private
	 * 
	 * @param th
	 * @param bestName
	 * @throws EntityNotFoundException
	 */
	public Trailhead updateName(TrailheadId thId, Message bestName) throws EntityNotFoundException {
		// let's not store everything you gave...it may be out of date
		Trailhead th = trailhead(thId);
		th = Trailhead.mutate(th).name(bestName).build();
		store(th);
		return th;
	}

	/** used to translate ids into their representative alias if needed . */
	public Function<TrailheadId, TrailheadId> nearestFunction() {
		return new Function<TrailheadId, TrailheadId>() {

			@Override
			public TrailheadId apply(TrailheadId input) {
				if (input == null) {
					return null;
				}
				try {
					return nearest(input).id();
				} catch (EntityNotFoundException e) {
					// oh well, just give them the original and let them find nothing
					return input;
				}
			}
		};
	}

	/**
	 * Useful for large batch processes that wish to only have the id as a result. Limit and
	 * pagination will be ignored and all results will be iterated so other indexes may be used to
	 * limit the results.
	 * 
	 * @param filter
	 * @return
	 */
	public Iterable<TrailheadId> trailheadIdsPaginator(Filter filter) {
		// the filter builder applies the bounds handling, but we don't want standard sorting
		return this.repository.idPaginator(filter);
	}

	/**
	 * Updates the stats to what is given.
	 * 
	 * @param completionCumulationStats
	 * @throws EntityNotFoundException
	 */
	public void setRouteCompletionStats(TrailheadId trailheadId, ActivityStats completionCumulationStats)
			throws EntityNotFoundException {
		this.repository.setRouteCompletionStats(trailheadId, completionCumulationStats);
	}

	/**
	 * A bulk select of the trailheads by id return in {@link Trailheads}.
	 * 
	 * @param trailheadIds
	 * @return
	 */
	public Trailheads trailheads(Iterable<TrailheadId> trailheadIds) {
		return repository.entities(trailheadIds);
	}

	/**
	 * Add all the "public" trailheads to the search index matching the filter. Updates trailheads
	 * if they already exist in the index. Does not delete trailheads that only exist in the index
	 * but exists in the datastore, so this is not a sync method.
	 * 
	 * For personal trailheads the search job must be invoked.
	 * 
	 * @throws MaxIndexAttemptsExceeded
	 * 
	 * @return the number of trailheads updated.
	 */
	public Integer searchIndexed(Filter filter) throws MaxIndexAttemptsExceeded {
		filter = TrailheadDatastoreFilterBuilder.clone(filter).orderByScore().build();
		Iterable<TrailheadId> trailheadIds = repository.idPaginator(filter);
		return SearchWebApiUri.indexTasks(	trailheadIds,
											TrailheadWebApiUri.TRAILHEADS,
											queueFactory.getQueue(SearchWebApiUri.SEARCH_QUEUE_NAME));
	}

	public SearchDocuments searchIndexed(TrailheadId trailheadId) throws MaxIndexAttemptsExceeded,
			EntityNotFoundException {
		Trailhead trailhead = repository.load(trailheadId);
		return searchIndexed(trailhead);
	}

	private SearchDocuments searchIndexed(Trailhead trailhead) throws MaxIndexAttemptsExceeded {
		Iterable<SearchDocument> searchDocuments = TrailheadSearchDocumentProducer.searchDocuments(trailhead);
		searchService.index(searchDocuments);
		return SearchDocuments.create().addAll(searchDocuments).build();
	}

	/**
	 * Provides a report of search results matching the supplied filter for the route's index.
	 * 
	 * @param filter
	 * @return
	 * @throws FieldNotSearchableException
	 * @throws UnknownIndexException
	 */
	public SearchDocuments search(Filter filter) throws FieldNotSearchableException, UnknownIndexException {
		TrailheadSearchFilterBuilder filterBuilder = TrailheadSearchFilterBuilder.clone(filter);
		Filter preparedFilter = filterBuilder.build();
		return searchService.searchDocuments(preparedFilter);
	}

	/**
	 * Simply retrieves or creates a {@link Trailhead} at the {@link Activity}
	 * {@link TrackSummary#getStart()}.
	 * 
	 * @see #nearestCreated(GeoCoordinate)
	 * @param activity
	 * @return
	 */
	public Trailhead nearestCreated(Activity activity) {
		return nearestCreated(activity.getTrackSummary().getStart().getLocation());

	}

	/**
	 * Handy method to find a trailhead near the given.
	 * 
	 * @param geoCell
	 * @return
	 */
	public Trailhead nearestCreated(GeoCell geoCell) {
		return nearestCreated(GeoCellUtil.boundingBoxFromGeoCell(geoCell).getCenter());
	}

	/**
	 * Bulk updates the {@link Trailheads} identified in the stream of
	 * {@link RelativeStatsActivityStatsAggregator} assigning to
	 * {@link Trailhead#activityStartStats()} and {@link Trailhead#activityStartPersonStats()}.
	 * 
	 * @see #trailheadRouteCompletionStatsUpdated(Iterable)
	 * @param trailheadIdActivityStats
	 * @return
	 */
	public Map<TrailheadId, Trailhead> trailheadActivityStartStatsUpdated(
			Iterable<RelativeStatsActivityStatsAggregator<TrailheadId>> trailheadIdActivityStats) {
		Boolean activityStarts = true;

		return trailheadStatsUpdated(trailheadIdActivityStats, activityStarts);
	}

	/**
	 * Bulk updates the {@link Trailheads} identified in the
	 * {@link RelativeStatsActivityStatsAggregator}s assinging to
	 * {@link Trailhead#routeCompletionsStats()} and {@link Trailhead#routeCompletionPersonStats()}.
	 * 
	 * @see #trailheadActivityStartStatsUpdated(Iterable)
	 * @param trailheadIdActivityStats
	 * @return
	 */
	public Map<TrailheadId, Trailhead> trailheadRouteCompletionStatsUpdated(
			Iterable<RelativeStatsActivityStatsAggregator<TrailheadId>> trailheadIdActivityStats) {
		Boolean activityStarts = false;

		return trailheadStatsUpdated(trailheadIdActivityStats, activityStarts);
	}

	/**
	 * bulk updates {@link ActivityStats} from {@link RelativeStatsActivityStatsAggregator} for the
	 * trailheads identified.
	 * 
	 * @param trailheadIdActivityStats
	 * @param activityStarts
	 *            if true then {@link Trailhead#activityStartStats()} and
	 *            {@link Trailhead#activityStartPersonStats()} is updated otherwise
	 *            {@link Trailhead#routeCompletionsStats()} and
	 *            {@link Trailhead#routeCompletionPersonStats()} are updated.
	 * @return
	 */
	Map<TrailheadId, Trailhead> trailheadStatsUpdated(
			Iterable<RelativeStatsActivityStatsAggregator<TrailheadId>> trailheadIdActivityStats,
			@Nullable Boolean activityStarts) {
		ImmutableMap<TrailheadId, RelativeStatsActivityStatsAggregator<TrailheadId>> trailheadIdAggregatorMap = Maps
				.uniqueIndex(trailheadIdActivityStats, RelativeStatsActivityStatsAggregator.<TrailheadId> idFunction());

		Set<TrailheadId> trailheadIds = trailheadIdAggregatorMap.keySet();
		Map<TrailheadId, Trailhead> trailheadMap = repository.loadAll(trailheadIds);
		// with freshly loaded trailheads, mutate them and store all
		for (Trailhead trailhead : trailheadMap.values()) {
			RelativeStatsActivityStatsAggregator<TrailheadId> aggregator = trailheadIdAggregatorMap.get(trailhead.id());
			if (aggregator != null) {
				ActivityStats activityStats = aggregator.activityStats();
				ActivityStats personStats = aggregator.personStats();
				Builder builder = Trailhead.mutate(trailhead);
				if (BooleanUtils.isTrue(activityStarts)) {
					// mutated version remains in the collection
					if (activityStats != null) {
						builder.activityStartStats(activityStats);
					}
					if (personStats != null) {
						builder.activityStartPersonStats(personStats);
					}
					builder.build();
				} else {
					if (activityStats != null) {
						builder.routeCompletionStats(activityStats);
					}
					if (activityStats != null) {

						builder.routeCompletionPersonStats(personStats);
					}
					builder.build();
				}
			} else {
				// this really shouldn't happen, unless the bulk retrieval finds aliases..which we
				// don't want.
				// in anycase no need to break operations...the trailhead just won't get it's new
				// stats.
				LoggerFactory.getLogger(getClass()).warning(trailhead.id() + " didn't find match from ids "
						+ aggregator);
			}
		}

		return repository.storeAll(trailheadMap.values());
	}

	/**
	 * Provides a connected mutator that will persist upon building. This is non-transactional so
	 * use with caution and without long delay.
	 * 
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Trailhead.Builder trailheadMutator(TrailheadId trailheadId) throws EntityNotFoundException {
		return repository.update(trailheadId);

	}
}
