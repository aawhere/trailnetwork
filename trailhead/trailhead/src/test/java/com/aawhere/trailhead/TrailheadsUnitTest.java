/**
 *
 */
package com.aawhere.trailhead;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Test;

import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.persist.BaseEntitiesBaseUnitTest;
import com.aawhere.persist.BaseEntity;
import com.aawhere.personalize.xml.XmlAdapterTestUtils;

/**
 * @author Brian Chapman
 * 
 */
public class TrailheadsUnitTest
		extends BaseEntitiesBaseUnitTest<Trailhead, Trailheads, Trailheads.Builder> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#createEntitySampleWithId()
	 */
	@Override
	public Trailhead createEntitySampleWithId() {
		return TrailheadUnitTest.getInstance().getEntitySampleWithId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return XmlAdapterTestUtils.getAdapters();
	}

	@Override
	protected Class<?>[] getClassesToBeBound() {
		Class<?>[] classes = { Trailhead.class, Trailheads.class, BaseEntity.class };
		return classes;
	}

	@Test
	public void testEmptyBounds() {
		assertNull("no trailheads, no bounds", Trailheads.create().build().boundingBox());
	}

	@Test
	public void testOneTrailheadBounds() {
		Trailhead solo = createEntitySampleWithId();
		Trailheads trailheads = Trailheads.create().add(solo).build();
		BoundingBoxTestUtils.assertEquals(TrailheadUtil.boundingBoxFromArea(solo), trailheads.boundingBox());
	}
}
