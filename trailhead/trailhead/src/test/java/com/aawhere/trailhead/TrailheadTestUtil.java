/**
 *
 */
package com.aawhere.trailhead;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.measure.quantity.Length;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.activity.ActivityStats;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.persist.ServiceTestUtilBase;
import com.aawhere.personalize.xml.XmlAdapterTestUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.trailhead.Trailheads.Builder;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessagePersonalizerUnitTest;
import com.aawhere.xml.bind.OptionalXmlAdapter.Show;
import com.google.common.collect.Lists;

/**
 * @author Brian Chapman
 * 
 */
public class TrailheadTestUtil {

	public static final GeoCoordinate LOC = MeasurementUtil.createGeoCoordinate(0d, 0d);
	public static final GeoCoordinate LOC_SHOULD_MERGE_WITH_LOC = MeasurementUtil.createGeoCoordinate(0.0001d, 0d);
	public static final GeoCoordinate LOC_SHOULD_NOT_MERGE_WITH_LOC = MeasurementUtil.createGeoCoordinate(1d, 0d);
	// GC1 and GC2 cross cell boundaries at resolution 7.
	static final GeoCoordinate GC1 = MeasurementUtil.createGeoCoordinate(37.806816, -122.406331);
	static final GeoCoordinate GC2 = MeasurementUtil.createGeoCoordinate(37.806398, -122.405931);
	static final Trailhead TARGET = createTrailhead(LOC);
	static final Trailhead SHOULD_MERGE = createTrailhead(LOC_SHOULD_MERGE_WITH_LOC);
	static final Trailhead SHOULD_NOT_MERGE = createTrailhead(LOC_SHOULD_NOT_MERGE_WITH_LOC);

	public static Trailhead trailhead() {
		return TrailheadUnitTest.getInstance().getEntitySample();
	}

	/** Creates a trailhead within the area, but different than that given. */
	public static Trailhead createNearbyTrailhead(TrailheadId trailheadId) {
		GeoCoordinate location = TrailheadUtil.coordinateFrom(trailheadId);
		return createTrailhead(nearby(location));
	}

	public static Trailhead createTrailhead(GeoCoordinate location) {
		return TrailheadUnitTest.getInstance(location).getEntitySample();
	}

	public static Trailhead createTrailhead(TrailheadId id) {
		// id value is geocell, which gives a coordinate.
		GeoCell cell = TrailheadUtil.geoCellFrom(id);
		BoundingBox boxFromGeoCell = GeoCellUtil.boundingBoxFromGeoCell(cell);
		return createTrailhead(boxFromGeoCell.getCenter());
	}

	public static Trailhead createTrailhead(TrailheadId id, Message name) {
		return Trailhead.mutate(createTrailhead(id)).name(name).build();
	}

	/**
	 * Provides the tres Amigos in shuffled order.
	 * 
	 * @return
	 */
	public static List<Trailhead> tresAmigos() {

		ArrayList<Trailhead> list = Lists.newArrayList(SHOULD_MERGE, SHOULD_NOT_MERGE, TARGET);
		Collections.shuffle(list);
		return list;
	}

	/**
	 * Generates some random trailheads with a random number of trailheads between 1-10 count.
	 * 
	 * @return
	 */
	public static Trailheads trailheads() {
		Builder builder = Trailheads.create();
		Integer count = TestUtil.generateRandomInt(1, 10);
		for (int i = 0; i < count; i++) {
			builder.add(trailhead());
		}

		return builder.build();
	}

	public static class TrailheadMockRepository
			extends MockFilterRepository<Trailheads, Trailhead, TrailheadId>
			implements TrailheadRepository {

		private TrailheadRepositoryDelegate delegate;

		TrailheadMockRepository() {
			this.delegate = new TrailheadRepositoryDelegate(this);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.trailhead.TrailheadRepository#setScore(com.aawhere.trailhead.TrailheadId,
		 * java.lang.Integer)
		 */
		@Override
		public Trailhead setScore(TrailheadId id, Integer score) throws EntityNotFoundException {
			return delegate.setScore(id, score);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.trailhead.TrailheadRepository#setRouteCompletionStats(com.aawhere.trailhead
		 * .TrailheadId, com.aawhere.activity.ActivityStats)
		 */
		@Override
		public Trailhead setRouteCompletionStats(TrailheadId trailheadId, ActivityStats routeCompletionStats)
				throws EntityNotFoundException {
			return this.delegate.setRouteCompletionStats(trailheadId, routeCompletionStats);
		}

		@Override
		public com.aawhere.trailhead.Trailhead.Builder update(TrailheadId id) throws EntityNotFoundException {
			return delegate.update(id);
		}

	}

	/**
	 * @return
	 */
	public static Set<XmlAdapter<?, ?>> adapters() {
		Set<XmlAdapter<?, ?>> adapters = new HashSet<XmlAdapter<?, ?>>();
		adapters.addAll(XmlAdapterTestUtils.getAdapters());
		adapters.add(MessagePersonalizerUnitTest.getInstance().getAdapter());
		adapters.add(new TrailheadIdXmlAdapter(trailheadProvider()));
		adapters.add(new Trailhead.ActivityStartStatsXmlAdapter(new Show()));
		adapters.add(new Trailhead.ActivityStartPersonStatsXmlAdapter(new Show()));
		adapters.add(new Trailhead.RouteCompletionStatsXmlAdapter(new Show()));
		adapters.add(new Trailhead.RouteCompletionPersonStatsXmlAdapter(new Show()));
		return adapters;
	}

	public static TrailheadProvider trailheadProvider() {
		return new TrailheadTestProvider();
	}

	public static class TrailheadTestProvider
			extends ServiceTestUtilBase<Trailheads, Trailhead, TrailheadId>
			implements TrailheadProvider {

		@Override
		public Trailhead trailhead(TrailheadId trailheadId) throws EntityNotFoundException {
			return entity(trailheadId);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.trailhead.TrailheadProvider#nearest(com.aawhere.trailhead.TrailheadId)
		 */
		@Override
		public Trailhead nearest(TrailheadId trailheadId) throws EntityNotFoundException {
			// nothing fancy in unit testing
			return trailhead(trailheadId);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.ServiceTestUtilBase#entity(com.aawhere.id.Identifier)
		 */
		@Override
		public Trailhead entity(TrailheadId id) {
			return createTrailhead(id);
		}

	}

	/**
	 * Given an existing trailhead this will create a different one at a location near the one
	 * provided, but with a different id.
	 * 
	 * @param trailhead
	 * @return
	 */
	public static Trailhead createNearbyTrailhead(Trailhead trailhead) {
		GeoCoordinate location = trailhead.location();
		GeoCoordinate point = nearby(location);
		return createTrailhead(point);
	}

	/** Allows others to mutate the filter during testing. */
	public static Filter addLocationFilter(Filter filter, GeoCells cells) {
		return TrailheadDatastoreFilterBuilder.clone(filter).area().contains(cells).build();
	}

	/**
	 * @param location
	 * @return
	 */
	private static GeoCoordinate nearby(GeoCoordinate location) {
		Length distance = QuantityMath.create(TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS).dividedBy(2).getQuantity();
		GeoCoordinate point = GeoMath.coordinateFrom(location, MeasureTestUtil.createRandomAngle(), distance);
		return point;
	}

	/**
	 * @return
	 */
	public static TrailheadId trailheadId() {
		return trailhead().id();
	}

	public static Trailhead.Builder create() {
		return Trailhead.mutate(trailhead());
	}

	public static Trailhead.Builder create(final TrailheadRepository repository) {
		return new Trailhead.Builder(trailhead()) {
			@Override
			public Trailhead build() {
				return repository.store(super.build());
			}
		};
	}
}
