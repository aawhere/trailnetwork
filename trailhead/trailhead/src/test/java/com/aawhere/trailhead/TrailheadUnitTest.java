/**
 *
 */
package com.aawhere.trailhead;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Before;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.test.TestUtil;
import com.aawhere.trailhead.Trailhead.Builder;
import com.aawhere.trailhead.TrailheadTestUtil.TrailheadMockRepository;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;

/**
 * @author Brian Chapman
 * 
 */
public class TrailheadUnitTest
		extends BaseEntityJaxbBaseUnitTest<Trailhead, Trailhead.Builder, TrailheadId, TrailheadRepository> {

	private GeoCoordinate location;
	private Integer score;
	private Integer numberOfRoutes;
	private Message name;
	private Integer mutatedScore;
	private Integer numberOfMains;
	private ActivityStats routeCompletionStats;
	private ActivityStats routeCompletionPersonStats;
	private ActivityStats activityStartStats;
	private ActivityStats activityStartPersonStats;

	/**
		 *
		 */
	public TrailheadUnitTest() {
		super(MUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Before
	@Override
	public void setUp() {

		location = MeasureTestUtil.createRandomGeoCoordinate();
		score = TestUtil.generateRandomInt();
		this.name = new StringMessage(TestUtil.generateRandomAlphaNumeric());
		this.mutatedScore = TestUtil.generateRandomInt();
		this.numberOfRoutes = TestUtil.generateRandomInt();
		this.numberOfMains = TestUtil.generateRandomInt();
		this.routeCompletionStats = ActivityTestUtil.activityStats();
		this.routeCompletionPersonStats = ActivityTestUtil.activityStats();
		this.activityStartStats = ActivityTestUtil.activityStats();
		this.activityStartPersonStats = ActivityTestUtil.activityStats();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere
	 * .persist.BaseEntity.Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.location(location);
		builder.score(score);
		builder.name(name);
		builder.numberOfRoutes(numberOfRoutes);
		builder.numberOfMains(numberOfMains);
		builder.routeCompletionStats(routeCompletionStats);
		builder.routeCompletionPersonStats(routeCompletionPersonStats);
		builder.activityStartStats(activityStartStats);
		builder.activityStartPersonStats(activityStartPersonStats);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	public void assertCreation(Trailhead sample) {
		assertEquals(location, sample.location());
		assertEquals(score, sample.score());
		assertEquals(name, sample.name());
		assertEquals(numberOfRoutes, sample.numberOfRoutes());
		assertEquals(numberOfMains, sample.numberOfMains());
		MeasureTestUtil.assertGeoCoordinateEqualsAfterJaxB(location, sample.location(), 0.00001d);
		super.assertCreation(sample);
		assertEquals(this.routeCompletionStats.total(), sample.routeCompletionsStats().total());
	}

	@Override
	public void assertStored(Trailhead persisted) {
		super.assertStored(persisted);
		TrailheadId id = persisted.getId();
		assertNotNull(id);
		assertNotNull(id.getValue());
		assertEquals(Trailhead.class, id.getKind());
		assertEquals(this.routeCompletionStats.total(), persisted.routeCompletionsStats().total());
		assertNotNull("total not assigned to stats.  call postLoad()?", persisted.routeCompletionsStats()
				.activityTypeStats().first().total());
	}

	@Override
	public void assertEntityEquals(Trailhead expected, Trailhead actual) {
		super.assertEntityEquals(expected, actual);
		MeasureTestUtil.assertGeoCoordinateEqualsAfterJaxB(expected.location(), actual.location(), 0.00001d);
		assertEquals(expected.score(), actual.score());
		assertEquals(expected.routeCompletionsStats().total(), actual.routeCompletionsStats().total());
		assertEquals(expected.routeCompletionPersonStats().total(), actual.routeCompletionPersonStats().total());
		assertEquals(expected.activityStartStats().total(), actual.activityStartStats().total());
		assertEquals(expected.activityStartPersonStats().total(), actual.activityStartPersonStats().total());

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(Trailhead mutated) {
		assertEquals(this.mutatedScore, mutated.score());
		super.assertMutation(mutated);
	}

	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return TrailheadTestUtil.adapters();
	}

	public static TrailheadUnitTest getInstance() {
		return getInstance(null);

	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected TrailheadRepository createRepository() {
		return new TrailheadMockRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.Identifier)
	 */
	@Override
	protected Trailhead mutate(TrailheadId id) throws EntityNotFoundException {
		getRepository().setScore(id, mutatedScore);
		return super.mutate(id);

	}

	public static TrailheadUnitTest getInstance(GeoCoordinate location) {
		TrailheadUnitTest instance = new TrailheadUnitTest();
		instance.baseSetUp();
		instance.setUp();
		if (location != null) {
			instance.location = location;
		}
		return instance;
	}

}
