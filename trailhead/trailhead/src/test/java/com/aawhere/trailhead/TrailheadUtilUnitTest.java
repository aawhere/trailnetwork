/**
 * 
 */
package com.aawhere.trailhead;

import static com.aawhere.trailhead.TrailheadTestUtil.*;
import static com.aawhere.trailhead.TrailheadUtil.*;
import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCellComparison;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.StringMessage;
import com.google.common.collect.Lists;

/**
 * @see TrailheadUtil
 * @author aroller
 * 
 */
public class TrailheadUtilUnitTest {

	@Test
	public void testOrderingLengthAway() {
		// this order is specifically wrong
		List<Trailhead> trailheads = tresAmigos();
		Trailheads sorted = sortNearest(trailheads, TARGET.location());
		Iterator<Trailhead> iterator = sorted.iterator();
		assertEquals(TARGET, iterator.next());
		assertEquals(SHOULD_MERGE, iterator.next());
		assertEquals(SHOULD_NOT_MERGE, iterator.next());
	}

	@Test
	public void testLimitLengthAway() {
		Iterable<Trailhead> lengthAwayFiltered = trailheadsWithinProximity(	tresAmigos(),
																			TARGET.location(),
																			MAXIMUM_TRAILHEAD_RADIUS);
		Trailheads sorted = sortNearest(lengthAwayFiltered, TARGET.location());
		Iterator<Trailhead> iterator = sorted.iterator();
		assertEquals(TARGET, iterator.next());
		assertEquals(SHOULD_MERGE, iterator.next());
		assertFalse("the limit should have been reached and the third amigo voted off the island.", iterator.hasNext());
	}

	@Test
	public void testBoundsArea() {
		Trailhead trailhead = TrailheadTestUtil.trailhead();
		BoundingBox boundingBox = TrailheadUtil.boundingBoxFromArea(trailhead);
		assertNotNull(boundingBox);
		assertTrue(	"bounding box is too big: " + boundingBox + " for " + trailhead.area(),
					QuantityMath.create(TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS).times(5)
							.greaterThan(boundingBox.getDiagonal()));
		assertTrue(GeoCellComparison.left(GeoCellUtil.geoCells(boundingBox))
				.right(GeoCellUtil.filterToMaxResolution(trailhead.area())).contains());
	}

	/**
	 * This test is derived from an actual problem debugging a scenario where a route is being
	 * assigned to an incorrect trailhead that is nearby, but outside the allowed proxity.
	 * 
	 */
	@Test
	public void testProximityFilter() {
		Trailhead existingTrailhead = Trailhead.create()
				.location(GeoCoordinate.create().setAsString("37.8784623,-122.5188578").build()).build();

		GeoCoordinate location = GeoCoordinate.create().setAsString("37.8797889,-122.515065").build();
		Iterable<Trailhead> withinProximity = TrailheadUtil.trailheadsWithinProximity(Lists
				.newArrayList(existingTrailhead), location, MAXIMUM_TRAILHEAD_RADIUS);
		TestUtil.assertEmpty(withinProximity);
	}

	@Test
	public void testNameEmpty() {
		assertTrue("enumeration", TrailheadUtil.isNameEmpty(TrailheadMessage.UNKNOWN));
		assertTrue(	"enumeratio value",
					TrailheadUtil.isNameEmpty(new StringMessage(TrailheadMessage.UNKNOWN.toString())));
		assertTrue("untitled", TrailheadUtil.isNameEmpty(new StringMessage("untitled")));
		assertTrue("null", TrailheadUtil.isNameEmpty(null));
		// assertTrue("null value", TrailheadUtil.isNameEmpty(new StringMessage(null)));
	}
}
