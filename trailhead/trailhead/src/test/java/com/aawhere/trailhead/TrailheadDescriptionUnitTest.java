/**
 *
 */
package com.aawhere.trailhead;

import static org.junit.Assert.*;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.person.pref.PrefTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.trailhead.TrailheadDescriptionPersonalizer.TrailheadDescription;
import com.aawhere.util.rb.VerbTense;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author brian
 * 
 */
public class TrailheadDescriptionUnitTest {

	private static final String ACTIVITIES_DESCRIPTOR = "activities";
	private static final String PEOPLE_DESCRIPTOR = "people";
	private static final String PERSON_DESCRIPTOR = "person";

	@Test
	public void testTrailheadDescriptionPersonalizer() {
		Trailhead th = TrailheadTestUtil.trailhead();

		TrailheadDescriptionPersonalizer TrailheadDescriptionPersonalizer = createTrailheadDescriptionPersonalizer();
		TrailheadDescriptionPersonalizer.personalize(th);

		TrailheadDescription trailheadDescription = TrailheadDescriptionPersonalizer.getResult();
		assertNotNull(trailheadDescription);
		assertNotNull(trailheadDescription.routeStats);

		assertJaxB(trailheadDescription);
	}

	@Test
	public void testTrailheadDescriptionPersonalizerNullNumberOfMains() {
		Trailhead th = TrailheadTestUtil.trailhead();
		th = Trailhead.mutate(th).numberOfMains(null).build();

		TrailheadDescriptionPersonalizer TrailheadDescriptionPersonalizer = createTrailheadDescriptionPersonalizer();
		TrailheadDescriptionPersonalizer.personalize(th);

		// We're happy it didn't throw an NPE.s
	}

	@Test
	public void testActivityStatsOnePersonOneActivity() throws Exception {
		String expectedPersonDescriptor = PERSON_DESCRIPTOR;
		String expectedActivityDescriptor = "activity";
		int numberOfActivities = 1;
		int numberOfPersons = 1;
		assertActivityStats(expectedPersonDescriptor, expectedActivityDescriptor, numberOfActivities, numberOfPersons);
	}

	@Test
	public void testActivityStatsMultiplePeopleAndActivities() throws Exception {
		assertActivityStats(PEOPLE_DESCRIPTOR, ACTIVITIES_DESCRIPTOR, 32, 55);
	}

	@Test
	public void testActivityStatsOnePersonAndMultipleActivities() throws Exception {
		assertActivityStats(PERSON_DESCRIPTOR, ACTIVITIES_DESCRIPTOR, 4, 1);
	}

	private TrailheadDescription assertActivityStats(String expectedPersonDescriptor,
			String expectedActivityDescriptor, int numberOfActivities, int numberOfPersons) throws Exception {
		ActivityStats personStats = ActivityStats.create().total(numberOfPersons).build();
		ActivityStats activityStats = ActivityStats.create().total(numberOfActivities).build();
		Trailhead trailhead = TrailheadTestUtil.create().activityStartPersonStats(activityStats)
				.activityStartPersonStats(personStats).build();
		TrailheadDescription trailheadDescription = marshal(trailhead);
		String expectedPersonCount;
		if (numberOfPersons == 1) {
			expectedPersonCount = "one";
		} else if (numberOfPersons == 0) {
			expectedPersonCount = "nobody";
		} else {
			expectedPersonCount = String.valueOf(numberOfPersons);
		}
		TestUtil.assertContains(trailheadDescription.activityStats, expectedPersonCount);
		TestUtil.assertContains(trailheadDescription.activityStats, expectedPersonDescriptor);
		TestUtil.assertContains(trailheadDescription.activityStats, expectedActivityDescriptor);
		return trailheadDescription;
	}

	@Test
	public void testRouteCompletionStatsNone() throws Exception {
		String expectedWord = "None";
		int numberOfCompletionsComparedTo100Activities = 0;
		assertRouteCompletion(expectedWord, numberOfCompletionsComparedTo100Activities);
	}

	@Test
	public void testRouteCompletionStatsSome() throws Exception {
		assertRouteCompletion("Some", 15);
	}

	@Test
	public void testRouteCompletionStatsMany() throws Exception {
		assertRouteCompletion("Many", 75);
	}

	private void assertRouteCompletion(String expectedWord, int numberOfCompletionsComparedTo100Activities)
			throws Exception {
		ActivityStats activityStarts = ActivityStats.create().total(100).build();
		ActivityStats routeCompletions = ActivityStats.create().total(numberOfCompletionsComparedTo100Activities)
				.build();

		Trailhead trailhead = TrailheadTestUtil.create().routeCompletionStats(routeCompletions)
				.activityStartStats(activityStarts).build();
		TrailheadDescription trailheadDescription = marshal(trailhead);

		TestUtil.assertContains(trailheadDescription.routeCompletionStats, expectedWord);
	}

	@Test
	public void testTrailheadDescriptionXmlAdapter() throws Exception {
		Trailhead th = TrailheadTestUtil.trailhead();
		TrailheadDescription td = marshal(th);
		assertNotNull(td);
	}

	private TrailheadDescription marshal(Trailhead th) throws Exception {
		TrailheadDescriptionXmlAdapter adapter = new TrailheadDescriptionXmlAdapter(
				createTrailheadDescriptionPersonalizer());
		TrailheadDescription td = adapter.marshal(th);
		return td;
	}

	@Test
	public void testDefaultTrailheadDescriptionXmlAdapter() throws Exception {
		TrailheadDescriptionXmlAdapter adapter = new TrailheadDescriptionXmlAdapter();
		Trailhead th = TrailheadTestUtil.trailhead();
		TrailheadDescription rd = adapter.marshal(th);
		assertNull(rd);
	}

	@Test
	public void testRelativeStatsActivityTypeSingle() throws Exception {
		ActivityType activityType = ActivityType.MTB;
		int count = 1;
		assertRelativeStats(activityType, null, null, count, "One", "mountain bike ride");

	}

	@Test
	public void testRelativeStatsActivityTypePerson() throws Exception {
		ActivityType activityType = ActivityType.MTB;
		int count = 1;
		PersonId personId = PersonTestUtil.personId();
		// past tense verb only used with person as the object of the sentence
		assertRelativeStats(activityType,
							null,
							personId,
							count,
							ActivityTestUtil.activityTypeVerb(activityType, VerbTense.PAST));

	}

	@Test
	public void testRelativeStatsDistance() throws Exception {
		int count = 1;
		// past tense verb only used with person as the object of the sentence
		ActivityTypeDistanceCategory epic = ActivityTypeDistanceCategory.EPIC;
		assertRelativeStats(null,
							epic,
							null,
							count,
							ActivityTestUtil.activityTypeDistanceCategoryAdjective(epic, count));

	}

	@Test
	public void testRelativeStatsPersonOnly() throws Exception {
		ActivityType activityType = ActivityType.U;
		// notice the activity type is null forcing a person-only message...so the default activity
		// type must be used.
		assertRelativeStats(null,
							null,
							PersonTestUtil.personId(),
							1,
							ActivityTestUtil.activityTypeVerb(activityType, VerbTense.PAST));

	}

	@Test
	public void testRelativeStatsPersonOnlyMultipleActivities() throws Exception {
		ActivityType activityType = ActivityType.U;
		// notice the activity type is null forcing a person-only message...so the default activity
		// type must be used.
		assertRelativeStats(null,
							null,
							PersonTestUtil.personId(),
							3,
							ActivityTestUtil.activityTypeVerb(activityType, VerbTense.PAST),
							"routes");

	}

	private void assertRelativeStats(ActivityType activityType, ActivityTypeDistanceCategory distanceCategory,
			PersonId personId, int count, String... expectedsInSentence) throws Exception {
		RelativeStatistic relativeStat = RelativeStatistic.create().featureId(TrailheadTestUtil.trailheadId())
				.personId(personId).activityType(activityType).distance(distanceCategory).count(count).build();
		Trailhead trailhead = TrailheadTestUtil.create().relativeStartsAdded(relativeStat).build();
		TrailheadDescription trailheadDescription = marshal(trailhead);
		for (int i = 0; i < expectedsInSentence.length; i++) {
			String expectedInSentence = expectedsInSentence[i];
			TestUtil.assertContains(trailheadDescription.relativeStats.toString(), expectedInSentence);
		}
	}

	private void assertJaxB(TrailheadDescription sample) {
		JAXBTestUtil<TrailheadDescription> util = JAXBTestUtil.create(sample).build();
		String xml = util.getXml();
		assertNotNull(xml);
		assertEquals(util.getExpected().routeStats, util.getActual().routeStats);
	}

	private TrailheadDescriptionPersonalizer createTrailheadDescriptionPersonalizer() {
		Configuration prefs = PrefTestUtil.createSiPreferences();

		return new TrailheadDescriptionPersonalizer.Builder().setPreferences(prefs)
				.personProvider(PersonTestUtil.instance()).build();
	}
}
