/**
 * 
 */
package com.aawhere.trailhead;

import com.aawhere.activity.ActivityStats;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.RepositoryDelegate;
import com.aawhere.trailhead.Trailhead.Builder;

/**
 * @author aroller
 * 
 */
public class TrailheadRepositoryDelegate
		extends RepositoryDelegate<TrailheadRepository, Trailheads, Trailhead, TrailheadId>
		implements TrailheadRepository {

	/**
	 * @param repository
	 */
	protected TrailheadRepositoryDelegate(TrailheadRepository repository) {
		super(repository);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.trailhead.TrailheadRepository#setScore(com.aawhere.trailhead.TrailheadId,
	 * java.lang.Integer)
	 */
	@Override
	public Trailhead setScore(TrailheadId id, Integer score) throws EntityNotFoundException {
		return update(mutate(id).score(score).build());
	}

	/**
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	private Builder mutate(TrailheadId id) throws EntityNotFoundException {
		return Trailhead.mutate(load(id));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.trailhead.TrailheadRepository#setRouteCompletionStats(com.aawhere.trailhead.
	 * TrailheadId, com.aawhere.activity.ActivityStats)
	 */
	@Override
	public Trailhead setRouteCompletionStats(TrailheadId trailheadId, ActivityStats routeCompletionStats)
			throws EntityNotFoundException {
		return update(mutate(trailheadId).routeCompletionStats(routeCompletionStats).build());
	}

	@Override
	public Builder update(TrailheadId id) throws EntityNotFoundException {
		return new Builder(load(id)) {
			@Override
			public Trailhead build() {
				return update(dis);
			}
		};
	}
}
