/**
 * 
 */
package com.aawhere.trailhead;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceStandard;

/**
 * @author aroller
 * 
 */
public interface TrailheadProvider
		extends ServiceStandard<Trailheads, Trailhead, TrailheadId> {

	/**
	 * Provides only the trailhead identified by the id.
	 * 
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 */
	Trailhead trailhead(TrailheadId trailheadId) throws EntityNotFoundException;

	/**
	 * Provides the actual trailhead requested by id or one that is within a normal proximity for a
	 * trailhead since trailhead merging can cause ids to change.
	 * 
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 *             when none existing are found in the standard area of a trailhead.
	 */
	Trailhead nearest(TrailheadId trailheadId) throws EntityNotFoundException;
}
