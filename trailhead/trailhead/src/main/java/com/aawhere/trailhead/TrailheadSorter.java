/**
 * 
 */
package com.aawhere.trailhead;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.Multiset;

/**
 * Given a {@link GeoCoordinate} of focus and a set of {@link Trailheads} this will determine which
 * trailhead, if any, relates best to the focus. If multiple exist then all are suggested as
 * candidates provided in the order of relevance. The scope of this class is specifically simple
 * leaving more complex organization to another.
 * 
 * TODO:This is simply a sorter now, but it leaves room to grow if other preferences may affect the
 * choice.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class TrailheadSorter {

	/**
	 * Used to construct all instances of TrailheadChooser.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<TrailheadSorter> {

		private Trailheads trailheads;

		public Builder() {
			super(new TrailheadSorter());
		}

		public Builder trailheads(Trailheads trailheads) {
			this.trailheads = trailheads;
			return this;
		}

		public Builder maxLengthAway(Length length) {
			building.maxLengthAway = length;
			return this;
		}

		public Builder focus(GeoCoordinate target) {
			building.target = target;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull("maxLengthAway", building.maxLengthAway);
		}

		@Override
		public TrailheadSorter build() {
			TrailheadSorter built = super.build();
			built.withinRange = new HashSet<>();
			TreeSet<Trailhead> sorted = new TreeSet<>(TrailheadScoreComparator.build());
			sorted.addAll(trailheads.getCollection());
			for (Trailhead trailhead : sorted) {
				Length length = GeoMath.length(built.target, trailhead.location());
				if (QuantityMath.create(length).lessThanEqualTo(built.maxLengthAway)) {
					built.withinRange.add(Pair.of(trailhead, length));
				}// else could be more efficient by exiting loop
			}
			return built;
		}

	}// end Builder

	@XmlElement
	private SortedSet<Trailhead> sorted;
	// @XmlElement - Length has problems when included in the entry set
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Set<Entry<Trailhead, Length>> withinRange;
	@XmlElement
	private GeoCoordinate target;
	@XmlElement
	private Length maxLengthAway;

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrailheadChooser */
	private TrailheadSorter() {
	}

	/**
	 * All of the given trailheads ordered in preference of the given variables. Since there may be
	 * equally valued trailheads this uses a {@link Multiset} which allows duplicates and would be
	 * an indication that Trailheads have enough in common to be merged (by something else).
	 * 
	 * @return
	 */
	public SortedSet<Trailhead> sorted() {
		return sorted;
	}

	/**
	 * Provides those trailheads that are within the range desired and the length away from the
	 * TARGET.
	 * 
	 * @return the withinRange
	 */
	public Set<Entry<Trailhead, Length>> withinRange() {
		return this.withinRange;
	}

	/**
	 * The point of interest that is looking for a related trailhead.
	 * 
	 * @return
	 */
	public GeoCoordinate target() {
		return this.target;
	}

	public Length maxDistanceAway() {
		return this.maxLengthAway;
	}
}
