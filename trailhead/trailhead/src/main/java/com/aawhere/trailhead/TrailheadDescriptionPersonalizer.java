/**
 *
 */
package com.aawhere.trailhead;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections4.CollectionUtils;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.lang.If;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.Person;
import com.aawhere.person.PersonDescriptionMessage;
import com.aawhere.person.PersonProvider;
import com.aawhere.personalize.BasePersonalizer;
import com.aawhere.personalize.NotHandledException;
import com.aawhere.personalize.PersonalizedBase;
import com.aawhere.personalize.xml.LocaleXmlAdapter;
import com.aawhere.personalize.xml.XmlLang;
import com.aawhere.trailhead.TrailheadDescriptionMessage.Param;
import com.aawhere.trailhead.TrailheadDescriptionPersonalizer.TrailheadDescription;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessageFormatter;
import com.aawhere.util.rb.ParamWrappingCompleteMessage;
import com.aawhere.util.rb.VerbTense;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.Lists;

/**
 * @author Brian Chapman
 * 
 */
public class TrailheadDescriptionPersonalizer
		extends BasePersonalizer<TrailheadDescription> {

	private PersonProvider personProvider;

	/**
	 * Used to construct all instances of BaseDurationPersonalizer.
	 */
	/* @formatter:off */
	public static class Builder extends BasePersonalizer.Builder<TrailheadDescriptionPersonalizer,
	Builder> {
	/* @formatter:on */
		public Builder() {
			super(new TrailheadDescriptionPersonalizer());
		}

		/** @see #personProvider */
		public Builder personProvider(PersonProvider personProvider) {
			building.personProvider = personProvider;
			return this;
		}

		@Override
		public TrailheadDescriptionPersonalizer build() {

			return super.build();
		}

	}

	/** Use {@link Builder} to construct BaseDurationPersonalizer */
	protected TrailheadDescriptionPersonalizer() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#personalize(java.lang.Object)
	 */
	@Override
	public void personalize(Object context) {
		if (context instanceof Trailhead) {
			result = new TrailheadDescription();
			personalizeRoute(((Trailhead) context));
		} else {
			throw new NotHandledException(handlesType(), context);
		}

	}

	private void personalizeRoute(Trailhead th) {
		formatRouteStats(th);
		formatActivityStartStats(th);
		formatRouteCompletionStartStats(th);
		formatRelativeStarts(th);
		StringBuilder completeBuilder = new StringBuilder();
		if (result.routeStats != null) {
			completeBuilder.append(result.routeStats);
		}
		if (result.activityStats != null) {
			completeBuilder.append(" ");
			completeBuilder.append(result.activityStats);
		}

		// add each of the relative stats since the user asked for this information it is important
		if (result.relativeStats != null) {
			for (String relativeStat : result.relativeStats) {
				completeBuilder.append(" ");
				completeBuilder.append(relativeStat);
			}
		}
		// if there are no routes...we already explained there were none so explaining no route
		// completions is redundant
		// if there are routes, but no route completions that is valuable information
		else if (result.routeCompletionStats != null && th.numberOfRoutes() != null && th.numberOfRoutes() > 0) {
			completeBuilder.append(" ");
			completeBuilder.append(result.routeCompletionStats);
		}

		result.complete = completeBuilder.toString();
		result.lang = getLocale();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#handlesType()
	 */
	@Override
	public Class<?> handlesType() {
		return Trailhead.class;
	}

	private void formatRouteStats(Trailhead th) {
		if (th.numberOfRoutes() == null) {
			return;
		}
		TrailheadDescriptionMessage routeStatsMessage = TrailheadDescriptionMessage.ROUTE_STATS;

		CompleteMessage routeStatsCompleteMessage = ParamWrappingCompleteMessage.create(routeStatsMessage)
				.paramWrapped(Param.ROUTE_COUNT, th.numberOfRoutes()).build();
		String formatted = formatMessage(routeStatsCompleteMessage);
		result.routeStats = formatted;
	}

	private void formatActivityStartStats(Trailhead th) {
		if (th.activityStartStats() != null && th.activityStartPersonStats() != null) {

			TrailheadDescriptionMessage routeStatsMessage = TrailheadDescriptionMessage.ACTIVITY_START_STATS;

			CompleteMessage routeStatsCompleteMessage = ParamWrappingCompleteMessage.create(routeStatsMessage)
					.paramWrapped(Param.ACTIVITY_COUNT, th.activityStartStats().total())
					.paramWrapped(PersonDescriptionMessage.Param.PERSON_COUNT, th.activityStartPersonStats().total())
					.build();
			String formatted = formatMessage(routeStatsCompleteMessage);
			result.activityStats = formatted;
		}
	}

	private void formatRouteCompletionStartStats(Trailhead th) {
		if (th.routeCompletionsStats() != null && th.activityStartStats() != null) {
			Integer numberOfActivities = th.activityStartStats().total();
			Integer numberOfCompletions = th.routeCompletionsStats().total();
			Ratio ratio = MeasurementUtil.ratio(numberOfCompletions, numberOfActivities);

			TrailheadDescriptionMessage routeStatsMessage = TrailheadDescriptionMessage.ROUTE_COMPLETION_STARTS_STATS;

			CompleteMessage routeStatsCompleteMessage = ParamWrappingCompleteMessage.create(routeStatsMessage)
					.paramWrapped(Param.ROUTE_COMPLETION_PERCENT, ratio).build();
			String formatted = formatMessage(routeStatsCompleteMessage);
			result.routeCompletionStats = formatted;
		}
	}

	private void formatRelativeStarts(Trailhead th) {
		List<RelativeStatistic> relativeStarts = th.relativeStarts();
		if (CollectionUtils.isNotEmpty(relativeStarts)) {

			for (RelativeStatistic relativeStatistic : relativeStarts) {
				Layer layer = relativeStatistic.layer();
				// nothing relative to talk about for the ALL filter.
				// FIXME: This code has moved to RelativeStatisticMessage...use it if possible
				if (!Layer.ALL.equals(layer)) {
					// the messsages are custom, but share some parameters so we work from top to
					// bottom
					CompleteMessage.Builder relativeStatisticMessage = new CompleteMessage.Builder();
					Message messageKey = null;
					Integer count = relativeStatistic.count();
					if (layer.personal) {
						messageKey = TrailheadDescriptionMessage.RELATIVE_STARTS_PERSON_ACTIVITY_TYPE;

						Person person;
						try {
							person = personProvider.person(relativeStatistic.personId());

						} catch (EntityNotFoundException e) {
							// this is unexpected. just treat the person as identity not shared
							person = null;
						}
						CompleteMessage personMessge = PersonDescriptionMessage.personStartingASentence(person);
						relativeStatisticMessage.addParameter(PersonDescriptionMessage.Param.PERSON, personMessge);
						ActivityType activityType = If.nil(relativeStatistic.activityType()).use(ActivityType.U);
						CompleteMessage activityMessage = CompleteMessage.create(activityType.verb)
								.param(VerbTense.Param.TENSE, VerbTense.PAST).build();
						relativeStatisticMessage.param(ActivityType.Param.ACTIVITY, activityMessage);
					} else if (layer.activityTyped) {
						messageKey = TrailheadDescriptionMessage.RELATIVE_STARTS_ACTIVITY_TYPE;

						Message activityMessageKey = relativeStatistic.activityType().nounAdjectivePhrase;
						// over-write and use the activity type with distance...they use the same
						// param
						if (layer.distanceMeasured) {
							activityMessageKey = relativeStatistic.distance().asAdjective;
						}
						CompleteMessage activityMessage = CompleteMessage.create(activityMessageKey)
								.param(ActivityType.Param.COUNT, count).build();

						relativeStatisticMessage.param(ActivityType.Param.ACTIVITY, activityMessage);
					}
					if (messageKey != null) {
						relativeStatisticMessage.param(Param.ACTIVITY_COUNT, count);
						result.addRelativeStat(formatMessage(relativeStatisticMessage.setMessage(messageKey).build()));
					}
				}
			}
		}
	}

	private String formatMessage(CompleteMessage message) {
		return MessageFormatter.create().message(message).locale(getLocale()).build().getFormattedMessage();
	}

	/**
	 * Provides a {@link PersonalizedBase} description of a trailhead, in perspective of end user.
	 * Provides a customized paragraph explaining all about the trailhead, but also provides
	 * individual sentences that may be targeted by a client application in snippets.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static class TrailheadDescription
			extends PersonalizedBase
			implements Serializable {

		/** the complete description */
		String complete;
		/** Describes number of routes available. Not popularity */
		String routeStats;
		/** Describes the number of activities starting from the trailhead. */
		String activityStats;
		/** Indicates how many activities have completed routes */
		String routeCompletionStats;
		/** If provided this shows information relating to the filter. */
		List<String> relativeStats;

		@XmlAttribute(name = XmlLang.ATTRIBUTE_NAME, required = false)
		@XmlJavaTypeAdapter(LocaleXmlAdapter.class)
		Locale lang;

		private void addRelativeStat(String formatted) {
			if (this.relativeStats == null) {
				relativeStats = Lists.newArrayList(formatted);
			} else {
				relativeStats.add(formatted);
			}
		}

		private static final long serialVersionUID = -56070682236512819L;
	}

}
