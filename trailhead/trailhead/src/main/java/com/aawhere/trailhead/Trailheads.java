/**
 *
 */
package com.aawhere.trailhead;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxProvider;
import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.xml.XmlNamespace;

/**
 * Represents an ordered collection of Trailheads
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class Trailheads
		extends BaseFilterEntities<Trailhead>
		implements BoundingBoxProvider {

	@XmlElement(name = BoundingBox.FIELD.NAME)
	private BoundingBox bounds;

	/** Use {@link Builder} to construct Trailheads */
	private Trailheads() {
	}

	@XmlElement(name = Trailhead.FIELD.DOMAIN)
	@Override
	public List<Trailhead> getCollection() {
		return super.getCollection();
	}

	/**
	 * Used to construct all instances of Trailheads.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, Trailhead, Trailheads> {
		public Builder() {
			this(new Trailheads());
		}

		public Builder(Trailheads trailhead) {
			super(trailhead, new ArrayList<Trailhead>());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public Trailheads build() {
			Trailheads built = super.build();
			built.bounds = TrailheadUtil.boundingBoxFromArea(built);
			return built;
		}
	}

	public static Builder create() {
		return new Builder();
	}

	@XmlTransient
	public static final class FIELD {
		public static final String BOUNDS = GeoWebApiUri.BOUNDS;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.BoundingBoxProvider#bounds()
	 */
	@Override
	public BoundingBox boundingBox() {
		return this.bounds;
	}
}
