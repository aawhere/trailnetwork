/**
 *
 */
package com.aawhere.trailhead;

import com.aawhere.activity.ActivityStats;
import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Repository;
import com.aawhere.trailhead.Trailhead.Builder;

/**
 * Storage {@link Repository} for {@link Trailhead}
 * 
 * @author Brian Chapman
 * 
 */
public interface TrailheadRepository
		extends CompleteRepository<Trailheads, Trailhead, TrailheadId> {

	/**
	 * modifies the score for the given Trailhead.
	 * 
	 * @deprecated use {@link #update(TrailheadId)}
	 * 
	 * @throws EntityNotFoundException
	 */
	@Deprecated
	public Trailhead setScore(TrailheadId id, Integer score) throws EntityNotFoundException;

	/**
	 * @see Trailhead#routeCompletionsStats()
	 * 
	 * @param trailheadId
	 * @param routeCompletionStats
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Trailhead setRouteCompletionStats(TrailheadId trailheadId, ActivityStats routeCompletionStats)
			throws EntityNotFoundException;

	/**
	 * Provides a connected builder that will allow mutation and update the repository when built.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	Builder update(TrailheadId id) throws EntityNotFoundException;
}
