/**
 *
 */
package com.aawhere.trailhead;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.id.Identifier;
import com.aawhere.id.StringIdentifier;
import com.aawhere.persist.ServicedBy;
import com.aawhere.xml.XmlNamespace;

/**
 * The {@link Identifier} for {@link Trailhead}
 * 
 * @see Trailhead#getId()
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@ServicedBy(TrailheadProvider.class)
public class TrailheadId
		extends StringIdentifier<Trailhead> {

	private static final long serialVersionUID = -5085746573498337373L;

	/**
	 * @param kind
	 */
	TrailheadId() {
		super(Trailhead.class);
	}

	/**
	 * Provided to any client that may have a Long formatted as a String.
	 * 
	 * @param value
	 * @param kind
	 */
	public TrailheadId(String value) {
		super(value, Trailhead.class);
	}

}
