/**
 *
 */
package com.aawhere.trailhead;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * @author Brian Chapman
 * 
 */
public class CannotCreateDuplicateTrailheadException
		extends BaseException {

	private static final long serialVersionUID = -5134713015620594332L;
	private Trailheads trailheads;

	public CannotCreateDuplicateTrailheadException(Trailheads trailheads, Exception cause) {
		super(messageFromTrailheads(trailheads), cause);
		this.trailheads = trailheads;
	}

	public CannotCreateDuplicateTrailheadException(Trailheads trailheads) {
		super(messageFromTrailheads(trailheads));
		this.trailheads = trailheads;
	}

	public Trailheads getTrailheads() {
		return trailheads;
	}

	private static CompleteMessage messageFromTrailheads(Trailheads trailheads) {
		return new CompleteMessage.Builder().setMessage(TrailheadMessage.TRAILHEADS_EXIST_CANNOT_CREATE)
				.addParameter(TrailheadMessage.Param.TRAILHEADS, trailheads.toString()).build();
	}
}
