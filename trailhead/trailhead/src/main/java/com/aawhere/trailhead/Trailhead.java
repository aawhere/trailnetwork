/**
 *
 */
package com.aawhere.trailhead;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatisticField;
import com.aawhere.app.account.Account;
import com.aawhere.field.DictionaryIndex;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.EmbeddedDictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsOptionalXmlAdapter;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.person.Person;
import com.aawhere.search.Searchable;
import com.aawhere.trailhead.Trailhead.AllIndex;
import com.aawhere.trailhead.Trailhead.AreaScoreIndex;
import com.aawhere.util.rb.Message;
import com.aawhere.xml.XmlNamespace;
import com.aawhere.xml.bind.OptionalCollectionXmlAdapter;
import com.aawhere.xml.bind.OptionalPassThroughXmlAdapter;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Represents the starting point for one or more Routes.
 * 
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Searchable(fieldKeys = { Trailhead.FIELD.NAME })
@Entity
@Cache
@Dictionary(domain = Trailhead.FIELD.DOMAIN, messages = TrailheadMessage.class, indexes = { AreaScoreIndex.class,
		AllIndex.class })
public class Trailhead
		extends StringBaseEntity<Trailhead, TrailheadId>
		implements SelfIdentifyingEntity {

	private static final long serialVersionUID = 3361547952417474673L;
	public static final Integer DEFAULT_SCORE = 0;

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(Trailhead trailhead) {
		return new Builder(trailhead);
	}

	public static final Resolution MAX_RESOLUTION = Resolution.ELEVEN;
	/**
	 * The resolution for which keys are created allowing trailhead to be well organized and self
	 * identifying.
	 */
	protected static final Resolution ID_RESOLUTION = MAX_RESOLUTION;

	@XmlElement
	private GeoCoordinate location;

	@Index
	@Field(key = FIELD.AREA, indexedFor = "TN-449")
	@XmlJavaTypeAdapter(GeoCellsOptionalXmlAdapter.class)
	private GeoCells area;

	/**
	 * A score indicating how popular a trailhead is. The greater the score, the greater the
	 * popularity.
	 */
	@Index
	@XmlAttribute
	@com.aawhere.field.annotation.Field(key = FIELD.SCORE, dataType = FieldDataType.NUMBER, indexedFor = "TN-449")
	private Integer score = DEFAULT_SCORE;

	/**
	 * Summarizes the activities that have completed routes that start from this trailhead. These
	 * are activities that could have started anywhere and passed through this trailhead. This
	 * should include repeat visits by the same {@link Person} or {@link Account}.
	 * 
	 */
	@XmlElement(name = FIELD.ROUTE_COMPLETION_STATS)
	@Field(key = FIELD.ROUTE_COMPLETION_STATS, xmlAdapter = RouteCompletionStatsXmlAdapter.class)
	@EmbeddedDictionary
	@XmlJavaTypeAdapter(RouteCompletionStatsXmlAdapter.class)
	@ApiModelProperty(value = "Number of Routes completed starting from this trailhead.", required = false)
	private ActivityStats routeCompletionsStats;

	@XmlElement
	@Field
	@XmlJavaTypeAdapter(RouteCompletionPersonStatsXmlAdapter.class)
	@ApiModelProperty(value = "Number of Persons that completed a Route starting from this Trailhead.",
			required = false)
	private ActivityStats routeCompletionPersonStats;

	@XmlElement
	@Field(xmlAdapter = ActivityStartStatsXmlAdapter.class)
	@XmlJavaTypeAdapter(value = ActivityStartStatsXmlAdapter.class)
	@ApiModelProperty(value = "The counts of Activities that have started at this trailhead.")
	private ActivityStats activityStartStats;

	@XmlElement
	@XmlJavaTypeAdapter(value = ActivityStartPersonStatsXmlAdapter.class)
	@Field(xmlAdapter = ActivityStartPersonStatsXmlAdapter.class)
	@ApiModelProperty(value = "The counts of Persons that have started an Activity at this trailhead.")
	private ActivityStats activityStartPersonStats;

	@XmlAttribute
	private Integer numberOfRoutes = 0;

	@XmlAttribute
	private Integer numberOfMains;

	@Field
	@XmlElement
	@ApiModelProperty(value = "The number of Activities that started from this Trailhead.")
	private Integer activityStartsCount;

	@ApiModelProperty(value = "Statistics about the starts limited by the filters of person, activity type, etc.")
	@Field(xmlAdapter = RelativeStartsStatsXmlAdapter.class, parameterType = RelativeStatistic.class)
	@XmlElement
	@Ignore
	@XmlJavaTypeAdapter(RelativeStartsStatsXmlAdapter.class)
	private List<RelativeStatistic> relativeStarts;

	/** @see #relativeStarts */
	public List<RelativeStatistic> relativeStarts() {
		return this.relativeStarts;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@com.aawhere.field.annotation.Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM,
			indexedFor = "TN-362")
	@Override
	public TrailheadId getId() {
		return super.getId();
	}

	@Nonnull
	public GeoCoordinate location() {
		return location;
	}

	@Nonnull
	public GeoCells area() {
		return area;
	}

	public Integer score() {
		return score;
	}

	public Message name() {
		return name;
	}

	@XmlElement(name = BaseEntity.BASE_FIELD.DESCRIPTION)
	@XmlJavaTypeAdapter(value = TrailheadDescriptionXmlAdapter.class)
	private Trailhead getRouteDescriptionForXml() {
		return this;
	}

	/**
	 * @return the routeCompletionsStats
	 */
	public ActivityStats routeCompletionsStats() {
		return this.routeCompletionsStats;
	}

	/** @see #routeCompletionPersonStats */
	public ActivityStats routeCompletionPersonStats() {
		return this.routeCompletionPersonStats;
	}

	/** @see #activityStartStats */
	public ActivityStats activityStartStats() {
		return this.activityStartStats;
	}

	/** @see #activityStartPersonStats */
	public ActivityStats activityStartPersonStats() {
		return this.activityStartPersonStats;
	}

	/**
	 * @return the numberOfRoutes
	 */
	public Integer numberOfRoutes() {
		return this.numberOfRoutes;
	}

	/**
	 * @return the numberOfMains
	 */
	public Integer numberOfMains() {
		return this.numberOfMains;
	}

	public Integer numberOfAlternates() {
		if (this.numberOfRoutes == null || this.numberOfMains == null) {
			return null;
		} else {
			return this.numberOfRoutes - numberOfMains;
		}
	}

	@XmlAttribute
	public Integer getNumberOfAlternates() {
		return numberOfAlternates();
	}

	/**
	 * The displayable common name of the Trailhead. If the name is unknown, it is
	 * ActivityMessage.UNKNOWN. This is a message, rather than a string to support a default,
	 * localized, name.
	 */
	@XmlElement
	@Field(key = FIELD.NAME)
	private Message name;

	/**
	 * Use {@link Builder} to construct {@link Trailhead}
	 */
	private Trailhead() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#postLoad()
	 */
	@Override
	protected void postLoad() {
		super.postLoad();
		if (this.routeCompletionsStats != null) {
			this.routeCompletionsStats.postLoad();
		}
		if (this.routeCompletionPersonStats != null) {
			this.routeCompletionPersonStats.postLoad();
		}
		if (this.activityStartStats != null) {
			this.activityStartStats.postLoad();
		}
		if (this.activityStartPersonStats != null) {
			this.activityStartPersonStats.postLoad();
		}
	}

	/**
	 * EIGHT Used to construct all instances of {@link Trailhead}.
	 */
	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<Trailhead, Builder, TrailheadId> {

		private Resolution resolution = MAX_RESOLUTION;
		private boolean mutating = false;

		public Builder() {
			super(new Trailhead());
		}

		Builder(Trailhead mutatee) {
			super(mutatee);
			this.mutating = true;
		}

		public Builder score(Integer score) {
			building.score = score;
			return this;
		}

		public Builder name(Message name) {
			building.name = name;
			return this;
		}

		public Builder numberOfRoutes(Integer numberOfRoutes) {
			building.numberOfRoutes = numberOfRoutes;
			return this;
		}

		public Builder numberOfMains(Integer numberOfMains) {
			building.numberOfMains = numberOfMains;
			return this;
		}

		public Builder routeCompletionStats(ActivityStats routeCompletionStats) {
			building.routeCompletionsStats = routeCompletionStats;
			return this;
		}

		/** @see #activityStartStats */
		public Builder activityStartStats(ActivityStats activityStartStats) {
			building.activityStartStats = activityStartStats;
			return this;
		}

		/** @see #activityStartPersonStats */
		public Builder activityStartPersonStats(ActivityStats personStartStats) {
			building.activityStartPersonStats = personStartStats;
			return this;
		}

		public Builder location(GeoCoordinate location) {
			building.location = location;
			// keeping the old id allows for a more stable datastore
			if (!this.mutating) {
				setId(new TrailheadId(GeoCellUtil.geoCell(building.location, ID_RESOLUTION).getCellAsString()));
			}
			return this;
		}

		public Builder area(BoundingBox area) {
			GeoCells areaCells = GeoCellBoundingBox.createGeoCell().maxGeoCellResolution(resolution).setOther(area)
					.build().getGeoCells();
			// make the cells searchable
			building.area = GeoCellUtil.withAncestors(areaCells);
			// set the default location if not already set
			if (building.location == null) {
				location(area.getCenter());
			}
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			if (building.area == null) {
				Assertion.exceptions().notNull("location", building.location);
				area(BoundingBoxUtil.boundedBy(building.location, TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS));
			}
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public Trailhead build() {

			// validates
			Trailhead built = super.build();
			if (built.name == null) {
				built.name = TrailheadMessage.UNKNOWN;
			}
			return built;
		}

		/** @see #routeCompletionPersonStats */
		public Builder routeCompletionPersonStats(ActivityStats routeCompletionPersonStats) {
			building.routeCompletionPersonStats = routeCompletionPersonStats;
			return this;
		}

		public Builder relativeStartsAdded(RelativeStatistic relativeStatistic) {
			if (building.relativeStarts == null) {
				building.relativeStarts = new ArrayList<RelativeStatistic>();
			}
			building.relativeStarts.add(relativeStatistic);
			return this;
		}

	}// end Builder

	@XmlTransient
	public static final class FIELD {
		public static final String DOMAIN = "trailhead";
		public static final String AREA = "area";
		public static final String SCORE = "score";
		public static final String NAME = "name";
		public static final String ROUTE_COMPLETION_STATS = "routeCompletionStats";

		public static final class KEY {
			public static final FieldKey AREA = new FieldKey(DOMAIN, Trailhead.FIELD.AREA);
			public static final FieldKey AREA_CELLS = new FieldKey(AREA, GeoCells.FIELD.CELLS);
			public static final FieldKey AREA__MAX_RESOLUTION = new FieldKey(AREA, GeoCells.FIELD.MAX_RESOLUTION);
			public static final FieldKey SCORE = new FieldKey(DOMAIN, Trailhead.FIELD.SCORE);
			public static final FieldKey NAME = new FieldKey(DOMAIN, FIELD.NAME);
			public static final FieldKey ROUTE_COMPLETION_ACTIVITY_TYPES = new FieldKey(new FieldKey(DOMAIN,
					FIELD.ROUTE_COMPLETION_STATS), ActivityStats.FIELD.KEY.ACTIVITY_TYPES);
			public static final FieldKey RELATIVE_STARTS_COUNT = new FieldKey(TrailheadField.Key.RELATIVE_STARTS,
					RelativeStatisticField.COUNT);
			public static final FieldKey RELATIVE_STARTS_ACTIVITY_TYPE = new FieldKey(
					TrailheadField.Key.RELATIVE_STARTS, RelativeStatisticField.ACTIVITY_TYPE);
			public static final FieldKey RELATIVE_STARTS_DISTANCE = new FieldKey(TrailheadField.Key.RELATIVE_STARTS,
					RelativeStatisticField.DISTANCE);
			public static final FieldKey RELATIVE_STARTS_PERSON_ID = new FieldKey(TrailheadField.Key.RELATIVE_STARTS,
					RelativeStatisticField.PERSON_ID);
			public static final FieldKey RELATIVE_STARTS_LAYER = new FieldKey(TrailheadField.Key.RELATIVE_STARTS,
					RelativeStatisticField.LAYER);
		}

		public static final class OPTION {
			/** Include this to show geocells for the trailhead area. */
			public static final String AREA = KEY.AREA.getAbsoluteKey();
		}
	}

	/**
	 * @see AAWHERE-TN-449
	 * 
	 */
	public static class AreaScoreIndex
			extends DictionaryIndex {
		public AreaScoreIndex() {
			super(FIELD.KEY.AREA_CELLS, FIELD.KEY.SCORE);
		}
	}

	/**
	 * @see TN-576 WW-39
	 * @author aroller
	 * 
	 */
	public static class AllIndex
			extends DictionaryIndex {
		public AllIndex() {
			super(FIELD.KEY.AREA_CELLS, FIELD.KEY.SCORE);
		}
	}

	@Singleton
	public static class ActivityStartStatsXmlAdapter
			extends OptionalPassThroughXmlAdapter<ActivityStats> {
		public ActivityStartStatsXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public ActivityStartStatsXmlAdapter(Show show) {
			super(show);
		}
	}

	@Singleton
	public static class ActivityStartPersonStatsXmlAdapter
			extends OptionalPassThroughXmlAdapter<ActivityStats> {
		public ActivityStartPersonStatsXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public ActivityStartPersonStatsXmlAdapter(Show show) {
			super(show);
		}
	}

	@Singleton
	public static class RouteCompletionStatsXmlAdapter
			extends OptionalPassThroughXmlAdapter<ActivityStats> {
		public RouteCompletionStatsXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public RouteCompletionStatsXmlAdapter(Show show) {
			super(show);
		}
	}

	@Singleton
	public static class RouteCompletionPersonStatsXmlAdapter
			extends OptionalPassThroughXmlAdapter<ActivityStats> {
		public RouteCompletionPersonStatsXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public RouteCompletionPersonStatsXmlAdapter(Show show) {
			super(show);
		}
	}

	@Singleton
	public static class RelativeStartsStatsXmlAdapter
			extends OptionalCollectionXmlAdapter<RelativeStatistic> {
		public RelativeStartsStatsXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public RelativeStartsStatsXmlAdapter(Show show) {
			super(show);
		}
	}

}
