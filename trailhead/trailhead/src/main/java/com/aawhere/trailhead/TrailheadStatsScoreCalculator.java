package com.aawhere.trailhead;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.activity.ActivityStats;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

/**
 * Calculates the score based on the information contained in the Trailhead statistics.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class TrailheadStatsScoreCalculator
		implements TrailheadScoreCalculator {

	@XmlAttribute
	private Integer score = Trailhead.DEFAULT_SCORE;
	@XmlElement
	private Trailhead trailhead;

	@XmlAttribute
	private Integer numberOfActivities;
	@XmlAttribute
	private Integer numberOfPersonsWithActivities;

	/**
	 * Used to construct all instances of TrailheadActivityCountScoreCalculator.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadStatsScoreCalculator> {

		public Builder() {
			super(new TrailheadStatsScoreCalculator());
		}

		/** @see #trailhead */
		public Builder trailhead(Trailhead trailhead) {
			building.trailhead = trailhead;
			return this;
		}

		@Override
		public TrailheadStatsScoreCalculator build() {
			TrailheadStatsScoreCalculator built = super.build();
			if (built.trailhead != null) {
				ActivityStats activityStartStats = built.trailhead.activityStartStats();
				if (activityStartStats != null) {
					// the number of unique people that started from this trailhead multiplied by
					// the total number of activities
					ActivityStats activityStartPersonStats = built.trailhead.activityStartPersonStats();
					if (activityStartPersonStats != null) {
						built.numberOfPersonsWithActivities = activityStartPersonStats.total();
					} else {
						// the data is out of whack...let's zero out the score
						built.numberOfPersonsWithActivities = 0;
					}
					built.numberOfActivities = activityStartStats.total();
					built.score += built.numberOfPersonsWithActivities * built.numberOfActivities;
				}
			}
			if (built.score == null) {
				built.score = 0;
			}
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrailheadActivityCountScoreCalculator */
	private TrailheadStatsScoreCalculator() {
	}

	@Override
	public Integer score() {
		return score;
	}

	/** @see #trailhead */
	public Trailhead trailhead() {
		return this.trailhead;
	}

	@Override
	public TrailheadId trailheadId() {
		return trailhead.id();
	}

}
