/**
 * 
 */
package com.aawhere.trailhead;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import com.aawhere.activity.ActivityNameSelector;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellComparison;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.util.rb.Message;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;

/**
 * Utility methods for {@link Trailhead}
 * 
 * @author Brian Chapman
 * 
 */
public class TrailheadUtil {

	/**
	 * A Trailhead may grow to be big because of it's organic combining of nearby trailheads. To
	 * avoid a trailhead that isn't practical to find we must have a limit.
	 * 
	 */
	public static final Length MAXIMUM_TRAILHEAD_RADIUS = MeasurementUtil.createLengthInMeters(200);

	/**
	 * 
	 * @param trailhead
	 * @return
	 */
	public static GeoCell geoCellFrom(Trailhead trailhead) {
		return geoCellFrom(trailhead.id());
	}

	/**
	 * Generate a GeoCell that represents the trailhead.
	 * 
	 * @param trailheadId
	 * @return
	 */
	public static GeoCell geoCellFrom(TrailheadId trailheadId) {
		return new GeoCell(trailheadId.getValue());
	}

	public static GeoCoordinate coordinateFrom(TrailheadId id) {
		return GeoCellUtil.boundingBoxFromGeoCell(TrailheadUtil.geoCellFrom(id)).getCenter();
	}

	public static Function<Trailhead, BoundingBox> boundingBoxFromAreaFunction() {
		return new Function<Trailhead, BoundingBox>() {

			@Override
			public BoundingBox apply(Trailhead input) {
				return TrailheadUtil.boundingBoxFromArea(input);
			}
		};
	}

	/**
	 * Provides the combined bounding box from the area represented by each trailhead.
	 * 
	 * @param trailheads
	 * @return
	 */
	public static BoundingBox boundingBoxFromArea(Iterable<Trailhead> trailheads) {
		Iterable<BoundingBox> boxes = Iterables.transform(trailheads, boundingBoxFromAreaFunction());
		return BoundingBoxUtil.combined(boxes);
	}

	/**
	 * @param input
	 * @return
	 */
	public static BoundingBox boundingBoxFromArea(Trailhead input) {
		return (input == null) ? null : GeoCellUtil.boundingBoxFromGeoCells(input.area());
	}

	/**
	 * Simply finds the highest score of those trailheads given.
	 * 
	 * @param trailheads
	 * @return the Trailhead with the highest score or null if no trailheads are provided.
	 */
	public static Trailhead highestScore(Trailheads trailheads) {
		Trailhead highest;
		if (trailheads.size() > 1) {
			ArrayList<Trailhead> sorted = new ArrayList<>(trailheads.all());
			Collections.sort(sorted, TrailheadScoreComparator.build());
			highest = sorted.get(0);
		} else {
			highest = Iterables.getFirst(trailheads.all(), null);
		}
		return highest;
	}

	/**
	 * Helper for {@link TrailheadLengthFromPointComparator}.
	 * 
	 * @param trailheads
	 * @param TARGET
	 * @return
	 */
	public static Trailheads sortNearest(Iterable<Trailhead> trailheads, GeoCoordinate target) {
		return Trailheads.create().addAll(lengthAwayOrdered(trailheads, target)).build();

	}

	/**
	 * Provides the closest trailhead to the given target location or null if no trailheads are
	 * provided.
	 */
	public static Trailhead nearest(Iterable<Trailhead> trailheads, GeoCoordinate target) {
		return Iterables.getFirst(lengthAwayOrdered(trailheads, target), null);
	}

	/**
	 * Returns true if the {@link Trailhead#area()} {@link GeoCellComparison#contains()} the given
	 * {@link GeoCoordinate} location.
	 * 
	 * @param trailhead
	 * @param location
	 * @return
	 */
	public static Boolean contains(Trailhead trailhead, GeoCoordinate location) {
		Set<GeoCell> finishPoint = GeoCellUtil.createGeoCells(location);
		Boolean contains = GeoCellComparison.left(trailhead.area()).right(finishPoint).contains();
		return contains;
	}

	/**
	 * Calculates the distance between the {@link Trailhead#location()}s of the two given using
	 * {@link GeoMath#length(GeoCoordinate, GeoCoordinate)}.
	 * 
	 * @param candidate
	 * @param existing
	 * @return
	 */
	public static Length length(Trailhead from, Trailhead to) {
		return GeoMath.length(from.location(), to.location());
	}

	public static Iterable<TrailheadId> trailheadIds(Iterable<Trailhead> trailheads) {
		return Iterables.transform(trailheads, trailheadIdFunction());
	}

	public static Iterable<TrailheadId> trailheadIdsFromIdValue(Iterable<String> trailheadIdValues) {
		return Iterables.transform(trailheadIdValues, trailheadIdFromValueFunction());
	}

	private enum TrailheadIdFromValueFunction implements Function<String, TrailheadId> {
		INSTANCE;
		@Override
		@Nullable
		public TrailheadId apply(@Nullable String idValue) {
			return (idValue != null) ? new TrailheadId(idValue) : null;
		}

		@Override
		public String toString() {
			return "TrailheadIdFromValueFunction";
		}
	}

	public static Function<String, TrailheadId> trailheadIdFromValueFunction() {
		return TrailheadIdFromValueFunction.INSTANCE;
	}

	/**
	 * Provides a function that converts a Trailhead to it's respective {@link GeoCoordinate}
	 * location.
	 * 
	 * @return
	 */
	public static Function<Trailhead, GeoCoordinate> locationFunction() {
		return new Function<Trailhead, GeoCoordinate>() {

			@Override
			public GeoCoordinate apply(Trailhead input) {
				if (input == null) {
					return null;
				}
				return input.location();
			}

		};
	}

	/**
	 * Provides a function that provides the length from the input {@link Trailhead#location()} to
	 * the given TARGET {@link GeoCoordinate}.
	 * 
	 * @param TARGET
	 * @return
	 */
	public static Function<Trailhead, Length> lengthAwayFunction(GeoCoordinate target) {
		return Functions.compose(MeasurementUtil.lengthAwayFunction(target), locationFunction());
	}

	/**
	 * Provides an {@link Ordering} which is also a {@link Comparator} to be used to sort
	 * 
	 * @see MeasurementUtil#quantityOrdering()
	 * @see #lengthAwayFunction(GeoCoordinate)
	 * @param TARGET
	 * @return
	 */
	public static Ordering<Trailhead> lengthAwayOrdering(GeoCoordinate target) {

		Ordering<Length> quantityComparator = MeasurementUtil.quantityOrdering();
		return quantityComparator.onResultOf(lengthAwayFunction(target));
	}

	/**
	 * Given the TARGET this will return an iterable that will provide the given trailheads in order
	 * of ascending length away from the TARGET.
	 */
	public static List<Trailhead> lengthAwayOrdered(Iterable<Trailhead> trailheads, GeoCoordinate target) {
		ArrayList<Trailhead> list = Lists.newArrayList(trailheads);
		Collections.sort(list, lengthAwayOrdering(target));
		return list;
	}

	/**
	 * Provide the TARGET and max length away this will filter out any Trailheads that exceed that
	 * distance.
	 */
	public static Iterable<Trailhead> trailheadsWithinProximity(Iterable<Trailhead> trailheads, GeoCoordinate target,
			Length maxLengthAway) {

		Range<Comparable<Length>> range = Range.atMost(MeasurementUtil.comparable(maxLengthAway));
		// makes length comparable for range check
		Function<Length, Comparable<Length>> comparableFunction = MeasurementUtil.comparableFunction();
		// range predicates wants other comparables...so this makes it so
		Function<Trailhead, Comparable<Length>> lengthAway = Functions.compose(	comparableFunction,
																				lengthAwayFunction(target));
		// combines the length providing with the range limiter
		Predicate<Trailhead> limitPredicate = Predicates.compose(range, lengthAway);
		return Iterables.filter(trailheads, limitPredicate);
	}

	/**
	 * @return
	 */
	public static Function<Trailhead, TrailheadId> trailheadIdFunction() {
		return IdentifierUtils.idFunction();
	}

	public static Predicate<Trailhead> withinBoundsPredicate(final BoundingBox bounds) {
		return new Predicate<Trailhead>() {

			@Override
			public boolean apply(Trailhead input) {
				return (input == null) ? false : BoundingBoxUtil.contains(bounds, input.location());
			}

			/*
			 * (non-Javadoc)
			 * @see java.lang.Object#toString()
			 */
			@Override
			public String toString() {
				return bounds.getBounds();
			}
		};
	}

	/**
	 * produces the corresponding trailhead id when given a geocell.
	 * 
	 * @param startGeoCell
	 * @return
	 */
	public static TrailheadId trailheadId(GeoCell geoCell) {
		Assertion.equals(Trailhead.MAX_RESOLUTION, geoCell.getResolution());
		return new TrailheadId(geoCell.getCellAsString());
	}

	/**
	 * If the name given is in any way a default name (null, emptystring,
	 * {@link TrailheadMessage#UNKNOWN}) then return true.
	 * 
	 * @param name
	 * @return
	 */
	public static boolean isNameEmpty(Message name) {
		return name == null || name.equals(TrailheadMessage.UNKNOWN)
				|| TrailheadMessage.UNKNOWN.toString().equalsIgnoreCase(name.getValue())
				|| "untitled".equals(name.getValue());
		// || StringUtils.isEmpty(name.getValue()) is not possible since value is nonnull
	}

	/**
	 * Pulblic map browes routes so this looks at the score and ensures their is at least one route
	 * leading from the trailhead.
	 * 
	 * @param trailhead
	 * @return
	 */
	public static boolean scoreIsSignificantForPublic(Trailhead trailhead) {
		return trailhead != null && trailhead.score() != null && scoreIsSignificantForPublic(trailhead.score())
				&& trailhead.numberOfMains() != null && trailhead.numberOfMains() > 0;
	}

	/**
	 * Currently a simple check to see fi the score is greather than the default, but provides a
	 * place for all to focus this question.
	 * 
	 * @param score
	 * @return
	 */
	public static boolean scoreIsSignificantForPublic(Integer score) {
		return score > Trailhead.DEFAULT_SCORE;
	}

	/**
	 * Picks the best name for a trailhead from the given selector. Returns null if no valuable name
	 * is provided.
	 * 
	 * @param selector
	 * @return
	 */
	@Nullable
	public static Message bestName(ActivityNameSelector selector) {
		Message name = null;

		if (selector.hasStartName()) {
			name = selector.bestStartName();
			// low scores aren't worth keeping
			// FIXME: figure out a better way of categorizing scores
			if (selector.startRanker().score(name.getValue()).score() < 2) {
				name = null;
			}
		}
		// apparently no start was found...use the activity name if available
		if (name == null && selector.hasActivityName()) {
			name = selector.bestName();
		}
		return name;
	}
}
