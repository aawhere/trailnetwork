/**
 *
 */
package com.aawhere.trailhead;

import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.ActivityType;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.person.PersonDescriptionMessage;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.text.format.custom.RangeKeywordFormat;
import com.aawhere.text.format.custom.RangeKeywordFormat.PluralKeyword;
import com.aawhere.trailhead.TrailheadDescriptionPersonalizer.TrailheadDescription;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.util.rb.SelectParamKey;
import com.google.common.base.Function;
import com.google.common.collect.Range;

/**
 * Support specifically for {@link TrailheadDescription}.
 * 
 * @author Brian Chapman
 * 
 */
public enum TrailheadDescriptionMessage implements Message {

	/** Sentence describing the route's completion stats. */
	ROUTE_STATS("{ROUTE_COUNT_VALUE, plural, " //
			+ "=0{There are no routes from this trailhead.} "//
			+ "one{There is {ROUTE_COUNT_XX}one{XX} route from this trailhead.} "//
			+ "other{There are {ROUTE_COUNT_XX}# routes{XX} from this trailhead.}}"//
	, Param.ROUTE_COUNT),
	ACTIVITY_START_STATS(
			"{PERSON_COUNT_VALUE, plural, "
					+ "=0{Nobody has started an Activity from this trailhead.} "
					+ "one{{PERSON_COUNT_XX}one person{XX} has started {ACTIVITY_COUNT_XX}{ACTIVITY_COUNT_VALUE, plural, one{one activity} other{# activities}}{XX} from here.} "
					+ "other{{PERSON_COUNT_XX}# people{XX} have started {ACTIVITY_COUNT_XX}{ACTIVITY_COUNT_VALUE, plural, other{# activities}}{XX} from here.}"//
					+ "}",//
			PersonDescriptionMessage.Param.PERSON_COUNT,
			Param.ACTIVITY_COUNT),
	ROUTE_COMPLETION_STARTS_STATS("{ROUTE_COMPLETION_PERCENT_KEY, select, " //
			+ "zero{None}"//
			+ "few{Some}"//
			+ "other{Many}"//
			+ "}"//
			+ " of the activities followed a route. "//
	, Param.ROUTE_COMPLETION_PERCENT),
	/** Relative starts for activity type layer only */
	RELATIVE_STARTS_ACTIVITY_TYPE("{ACTIVITY_COUNT_VALUE, plural, " //
			+ "=0{}"//
			+ "one{One {ACTIVITY} completed a route starting from here.}"//
			+ "other{# {ACTIVITY} completed routes starting from here.}"//
			+ "}"//
	, ActivityType.Param.ACTIVITY, Param.ACTIVITY_COUNT),
	/**
	 * when the person and activity type are known or the activity type can be generalized if only
	 * the person is known.
	 */
	RELATIVE_STARTS_PERSON_ACTIVITY_TYPE("{ACTIVITY_COUNT_VALUE, plural, " //
			+ "=0{}"//
			+ "one{{PERSON} {ACTIVITY} a route starting from here.}"//
			+ "other{{PERSON} {ACTIVITY} routes # times starting from here.}"//
			+ "}"//
	, ActivityType.Param.ACTIVITY, Param.ACTIVITY_COUNT, PersonDescriptionMessage.Param.PERSON),

	;

	private ParamKey[] parameterKeys;
	private String message;

	private TrailheadDescriptionMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements SelectParamKey {
		/** the number of main routes available */
		ROUTE_COUNT,

		/** The number of activities */
		ACTIVITY_COUNT,
		/**
		 * The percentage of activities that completed a route. The number of RouteCompletions is
		 * not an exact representation of this value since some activities may complete the same
		 * route twice or some activities may complete multiple routes. This percentage is used to
		 * generalize the wording.
		 */
		ROUTE_COMPLETION_PERCENT(TrailheadDescriptionMessage.routeCompletionPercentSelectFunction());

		CustomFormat<?> format;
		private Function<Object, String> selectKeyFunction;

		private Param() {
		}

		private Param(Function<?, String> selectKeyFunction) {
			this.selectKeyFunction = (Function<Object, String>) selectKeyFunction;
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}

		@Override
		public String selectKey(Object value) {
			return (this.selectKeyFunction != null) ? this.selectKeyFunction.apply(value) : null;
		}
	}

	static Function<Comparable<Ratio>, String> routeCompletionPercentSelectFunction() {
		Comparable<Ratio> low = MeasurementUtil.comparable(MeasurementUtil.ratio(.01));
		Comparable<Ratio> some = MeasurementUtil.comparable(MeasurementUtil.ratio(.25));
		Range<Comparable<Ratio>> none = Range.lessThan(low);
		Range<Comparable<Ratio>> few = Range.closed(low, some);
		Range<Comparable<Ratio>> many = Range.atLeast(some);
		Pair<Range<Comparable<Ratio>>, String> nonePair = Pair.of(none, PluralKeyword.ZERO.toKey());
		Pair<Range<Comparable<Ratio>>, String> fewPair = Pair.of(few, PluralKeyword.FEW.toKey());
		Pair<Range<Comparable<Ratio>>, String> manyPair = Pair.of(many, PluralKeyword.MANY.toKey());
		return RangeKeywordFormat.rangeFunction(nonePair, fewPair, manyPair);

	}
}