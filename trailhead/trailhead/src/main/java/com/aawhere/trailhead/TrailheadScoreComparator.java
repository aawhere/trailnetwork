/**
 * 
 */
package com.aawhere.trailhead;

import java.util.Comparator;

import com.aawhere.lang.ComparatorResult;

/**
 * Orders score descending putting the highest score first since it is likely a search for a higher
 * score is the preference.
 * 
 * @author aroller
 * 
 */
public class TrailheadScoreComparator
		implements Comparator<Trailhead> {

	public static TrailheadScoreComparator build() {
		return new TrailheadScoreComparator();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Trailhead o1, Trailhead o2) {
		if (o1 == o2) {
			return ComparatorResult.EQUAL.value;
		}
		if (o1 == null) {
			return ComparatorResult.LESS_THAN.value;
		}
		if (o2 == null) {
			return ComparatorResult.GREATER_THAN.value;
		}
		return o1.score().compareTo(o2.score());

	}

}
