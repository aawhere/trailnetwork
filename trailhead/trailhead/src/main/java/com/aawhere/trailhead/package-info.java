@XmlJavaTypeAdapters({
		@XmlJavaTypeAdapter(value = com.aawhere.util.rb.MessagePersonalizedXmlAdapter.class,
				type = com.aawhere.util.rb.Message.class),
		@XmlJavaTypeAdapter(value = DateTimeXmlAdapter.class, type = DateTime.class),
		@XmlJavaTypeAdapter(value = QuantityXmlAdapter.class, type = Quantity.class),
		@XmlJavaTypeAdapter(value = DurationXmlAdapter.class, type = Duration.class),
		@XmlJavaTypeAdapter(value = MapEntryXmlAdapter.class, type = Entry.class) })
@XmlSchema(xmlns = @XmlNs(prefix = com.aawhere.xml.XmlNamespace.API_PREFIX,
		namespaceURI = com.aawhere.xml.XmlNamespace.API_VALUE))
package com.aawhere.trailhead;

import java.util.Map.Entry;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.aawhere.collections.MapEntryXmlAdapter;
import com.aawhere.joda.time.xml.DateTimeXmlAdapter;
import com.aawhere.joda.time.xml.DurationXmlAdapter;
import com.aawhere.measure.xml.QuantityXmlAdapter;

