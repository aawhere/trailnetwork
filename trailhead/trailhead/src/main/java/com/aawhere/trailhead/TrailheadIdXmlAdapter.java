/**
 * 
 */
package com.aawhere.trailhead;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Provides the trailheads by id. This works with an injected provider. If no provider is given to
 * the XmlAdapters then this will just return a null.
 * 
 * @author aroller
 * 
 */
@Singleton
public class TrailheadIdXmlAdapter
		extends XmlAdapter<Trailhead, TrailheadId> {

	private TrailheadProvider provider;

	/**
	 * 
	 */
	@Inject
	public TrailheadIdXmlAdapter(TrailheadProvider provider) {
		this.provider = provider;
	}

	/**
	 * Default adapter providing no result.
	 * 
	 */
	public TrailheadIdXmlAdapter() {

	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public TrailheadId unmarshal(Trailhead trailhead) throws Exception {
		if (trailhead == null || provider == null) {
			return null;
		}
		return trailhead.id();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Trailhead marshal(TrailheadId id) throws Exception {
		if (id == null || provider == null) {
			return null;
		}
		return provider.nearest(id);
	}

}
