/**
 *
 */
package com.aawhere.trailhead;

import com.aawhere.identity.IdentityManager;
import com.aawhere.person.PersonProvider;
import com.aawhere.personalize.DisabledPersonalizer;
import com.aawhere.personalize.xml.PersonalizedXmlAdapter;
import com.aawhere.trailhead.TrailheadDescriptionPersonalizer.TrailheadDescription;

import com.google.inject.Inject;

/**
 * Provides the localized description of a route through an XmlAdapter.
 * 
 * @author Brian Chapman
 * 
 */
public class TrailheadDescriptionXmlAdapter
		extends PersonalizedXmlAdapter<TrailheadDescription, Trailhead> {

	public TrailheadDescriptionXmlAdapter() {
		super(new DisabledPersonalizer.Builder());
	}

	public TrailheadDescriptionXmlAdapter(TrailheadDescriptionPersonalizer plizer) {
		super(plizer);
	}

	@Inject
	public TrailheadDescriptionXmlAdapter(IdentityManager identityManager, PersonProvider personProvider) {
		super(new TrailheadDescriptionPersonalizer.Builder().setPreferences(identityManager.preferences())
				.personProvider(personProvider).build());
	}

}