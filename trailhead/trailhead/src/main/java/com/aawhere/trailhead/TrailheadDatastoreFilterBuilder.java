/**
 * 
 */
package com.aawhere.trailhead;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.geocell.GeoCellFilterBuilder;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseFilterBuilder;
import com.aawhere.persist.FilterSort;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchUtils;

/**
 * {@link Filter} builder for {@link Trailhead} for the {@link TrailheadRepository}. For use with
 * the {@link SearchService} use the {@link TrailheadSearchFilterBuilder} and the TrailheadService
 * will automatically use the {@link SearchService}.
 * 
 * 
 * @author aroller
 * 
 */
public class TrailheadDatastoreFilterBuilder
		extends BaseFilterBuilder<TrailheadDatastoreFilterBuilder> {

	private GeoCellFilterBuilder areaFilterBuilder;
	/**
	 * See {@link Resolution} for approximate size of rectangle created by GeoCells.
	 */
	static final Resolution FIND_RESOLUTION = Resolution.EIGHT;

	public static TrailheadDatastoreFilterBuilder create() {
		return new TrailheadDatastoreFilterBuilder();
	}

	static TrailheadDatastoreFilterBuilder clone(Filter filter) {
		return new TrailheadDatastoreFilterBuilder(Filter.cloneForBuilder(filter));
	}

	/**
	 * Provides standard sorting by the highest score first.
	 * 
	 * @return
	 */
	public TrailheadDatastoreFilterBuilder orderByScore() {
		return orderBy(Trailhead.FIELD.KEY.SCORE, FilterSort.Direction.DESCENDING);
	}

	/**
	 * @param filter
	 */
	public TrailheadDatastoreFilterBuilder() {
		this(new Filter());

	}

	private TrailheadDatastoreFilterBuilder(Filter filter) {
		super(filter);

	}

	/**
	 * provides the geo cell filter builder to query spatially against trailheads.
	 * 
	 * @return
	 */
	public GeoCellFilterBuilder area() {
		if (areaFilterBuilder == null) {
			areaFilterBuilder = GeoCellFilterBuilder.mutate(building).key(Trailhead.FIELD.KEY.AREA)
					.maxResolution(FIND_RESOLUTION).build();
		}
		orderByNone();
		return this.areaFilterBuilder;
	}

	/**
	 * Adds a programatic filter to ensure the location of the trailhead falls within the given
	 * bounding box.
	 */
	public TrailheadDatastoreFilterBuilder withinBounds(BoundingBox bounds) {
		addPredicate(TrailheadUtil.withinBoundsPredicate(bounds));
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.ObjectBuilder#build()
	 */
	@Override
	public Filter build() {

		if (!building.hasSortOrder()) {
			orderByScore();
		}
		// whoever is using this builder is expected to use the datastore
		// if search is desired use TrailheadSearchFilterBuilder
		SearchUtils.isDatastoreFilter(this);
		// before building we may make some calls
		// only assign bounds if no geo assigned yet.
		if (building.hasBounds() && this.areaFilterBuilder == null) {
			String bounds = building.getBounds();
			BoundingBox boundingBox = GeoCellUtil.boundingBox(bounds);
			// interesection of anything in the bounds is good enough
			area().intersection(boundingBox);
			withinBounds(boundingBox);
		}
		final Filter built = super.build();

		if (areaFilterBuilder != null) {
			areaFilterBuilder.build();
		}
		return built;

	}

}
