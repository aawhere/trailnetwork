/**
 * 
 */
package com.aawhere.trailhead;

/**
 * Computes the score for a {@link Trailhead}. The score is an integer that represents the
 * trailhead's popularity
 * 
 * @author Brian Chapman
 * 
 */
public interface TrailheadScoreCalculator {

	public TrailheadId trailheadId();

	public Integer score();

}
