/**
 *
 */
package com.aawhere.trailhead;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.EntityMessage;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * @author brian
 * 
 */
@XmlTransient
public enum TrailheadMessage implements Message {

	/** Used when the name is not provided. */
	UNKNOWN("Unknown"),
	TRAILHEADS_EXIST_CANNOT_CREATE(
			"Cannot create new Trailhead. Found duplicate trailheads at the same location as the one supplied. Duplicate Trailheads: {TRAILHEADS}",
			Param.TRAILHEADS),
	/** Used when the name is not provided. */
	ID_NAME(EntityMessage.ID_NAME.getValue()),
	ID_DESCRIPTION(EntityMessage.ID_DESCRIPTION.getValue()),
	DATE_CREATED_NAME(EntityMessage.DATE_CREATED_NAME.getValue()), /* */
	DATE_CREATED_DESCRIPTION(EntityMessage.DATE_CREATED_DESCRIPTION.getValue()), /* */
	DATE_UPDATED_NAME(EntityMessage.DATE_UPDATED_NAME.getValue()), /* */
	DATE_UPDATED_DESCRIPTION(EntityMessage.DATE_UPDATED_DESCRIPTION.getValue()),
	TRAILHEAD_NAME(""),
	TRAILHEAD_DESCRIPTION(""),
	LOCATION_NAME(""),
	LOCATION_DESCRIPTION(""),
	DESCRIPTION_NAME(""),
	DESCRIPTION_DESCRIPTION(""),
	NAME_NAME(""),
	NAME_DESCRIPTION("");

	private ParamKey[] parameterKeys;
	private String message;

	private TrailheadMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		/** A list of {@link Trailheads} */
		TRAILHEADS;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}