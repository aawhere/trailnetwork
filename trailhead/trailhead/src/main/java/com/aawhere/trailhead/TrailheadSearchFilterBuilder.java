package com.aawhere.trailhead;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.activity.RelativeStatsUtil;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldUtils;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.person.PersonId;
import com.aawhere.search.SearchUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

public class TrailheadSearchFilterBuilder
		extends Filter.BaseBuilder<Filter, TrailheadSearchFilterBuilder> {

	static final FieldKey COUNT = Trailhead.FIELD.KEY.RELATIVE_STARTS_COUNT;
	static final FieldKey ACTIVITY_TYPE = Trailhead.FIELD.KEY.RELATIVE_STARTS_ACTIVITY_TYPE;
	static final FieldKey DISTANCE_CATEGORY = Trailhead.FIELD.KEY.RELATIVE_STARTS_DISTANCE;
	static final FieldKey PERSON_ID = Trailhead.FIELD.KEY.RELATIVE_STARTS_PERSON_ID;
	static final FieldKey LAYER = Trailhead.FIELD.KEY.RELATIVE_STARTS_LAYER;
	static final FieldKey TRAILHEAD_ID = TrailheadField.Key.ID;
	static final FieldKey RELATIVE_STATS_EXPAND_KEY = TrailheadField.Key.RELATIVE_STARTS;
	private Layer layer;

	private TrailheadSearchFilterBuilder(Filter filter) {
		super(filter);
	}

	public static TrailheadSearchFilterBuilder create() {
		return new TrailheadSearchFilterBuilder(new Filter());
	}

	public static TrailheadSearchFilterBuilder clone(Filter filter) {
		return new TrailheadSearchFilterBuilder(Filter.cloneForBuilder(filter));

	}

	public TrailheadSearchFilterBuilder layer(Layer layer) {
		this.layer = layer;
		return this;
	}

	public Layer layer() {
		return layer;
	}

	public TrailheadSearchFilterBuilder activityType(ActivityType activityType) {
		addCondition(FilterCondition.create().field(ACTIVITY_TYPE).value(activityType).build());
		return this;
	}

	public TrailheadSearchFilterBuilder distanceCategory(ActivityTypeDistanceCategory distanceCategory) {
		addCondition(FilterCondition.create().field(DISTANCE_CATEGORY).value(distanceCategory).build());
		return this;
	}

	public TrailheadSearchFilterBuilder personId(PersonId personId) {
		return addCondition(FilterCondition.create().field(PERSON_ID).value(personId).build());
	}

	public TrailheadSearchFilterBuilder relativeStartsExpanded() {
		return addExpand(RELATIVE_STATS_EXPAND_KEY);
	}

	/**
	 * adds the fields required to populate relative starts.
	 * 
	 * @return
	 */
	private TrailheadSearchFilterBuilder relativeStartsFields(Layer layer) {
		if (layer != null) {
			if (layer.personal) {
				addField(PERSON_ID);
			}
			if (layer.activityTyped) {
				addField(ACTIVITY_TYPE);
			}
			if (layer.distanceMeasured) {
				addField(DISTANCE_CATEGORY);
			}
		} else {
			LoggerFactory.getLogger(getClass()).warning("requesting field inclusion, but no layer provided");
		}
		return addField(COUNT);
	}

	public static Boolean relativeStatsExpandRequested(Filter filter) {
		return filter.containsExpand(RELATIVE_STATS_EXPAND_KEY);
	}

	public String indexName() {
		return indexName(layer);
	}

	public TrailheadSearchFilterBuilder allIndexes() {
		List<String> indexNames = indexNames();
		addSearchIndex(indexNames.toArray(new String[] {}));
		return this;
	}

	public TrailheadSearchFilterBuilder trailheadId(TrailheadId trailheadId) {
		return addCondition(FilterCondition.create().field(TRAILHEAD_ID).value(trailheadId).build());
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Filter build() {
		Collection<FilterCondition> removed = removed(LAYER);
		if (CollectionUtils.isNotEmpty(removed)) {
			this.layer = (Layer) removed.iterator().next().getValue();
		}

		// FIXME: Remove this temporary fix from the old to the new
		Collection<FilterCondition> activityTypeCondition = removed(FieldUtils
				.key("trailhead-routeCompletionStats-activityTypeStats-activityType"));
		if (CollectionUtils.isNotEmpty(activityTypeCondition)) {
			activityType((ActivityType) ((Iterable<?>) activityTypeCondition.iterator().next().getValue()).iterator()
					.next());
		}

		// FIXME:Remove this temporary fix remove any call to score.
		removed(TrailheadField.Key.SCORE);

		// automatically determine the layer if not specifically declared
		if (!building.hasSearchIndexes()) {
			if (this.layer == null) {
				boolean person = building.containsCondition(PERSON_ID);
				boolean activityType = building.containsCondition(ACTIVITY_TYPE);
				boolean distance = building.containsCondition(DISTANCE_CATEGORY);
				this.layer = RelativeStatsUtil.layer(person, activityType, distance);
			}
			this.addSearchIndex(indexName());
		}
		// now that layer is built, we can request the appropriate fields
		if (relativeStatsExpandRequested(building)) {
			// assuming this request came from the api we must populate the required fields so they
			// don't have to
			relativeStartsFields(this.layer);
		}
		Filter built = super.build();

		return built;
	}

	/**
	 * Provides the qualified name of the index based on the layer
	 * 
	 * @param layer
	 * @return
	 */
	public static String indexName(Layer layer) {
		return SearchUtils.indexName(TrailheadField.DOMAIN, layer.name());
	}

	public static List<String> indexNames() {
		Builder<String> builder = ImmutableList.builder();
		for (Layer layer : Layer.values()) {
			builder.add(indexName(layer));
		}
		return builder.build();
	}

}
