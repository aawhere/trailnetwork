/**
 * 
 */
package com.aawhere.trailhead;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.route.RouteTestUtils;
import com.aawhere.route.Routes;

/**
 * Unit tests for {@link TrailheadScoreCalculator}
 * 
 * @author Brian Chapman
 * 
 */
public class TrailheadScoreCalculatorUnitTest {

	@Test
	public void testScoreCalculator() {
		Routes routes = RouteTestUtils.createRoutes();
		TrailheadScoreCalculator calc = RouteAwareTrailheadScoreCalculator.create().routes(routes).build();
		// Just make sure it calculated something. The actual calculation may change over time,
		// resulting in a brittle test. Plus we don't want to re-create the logic here.
		assertTrue(calc.score() > Trailhead.DEFAULT_SCORE);
	}
}
