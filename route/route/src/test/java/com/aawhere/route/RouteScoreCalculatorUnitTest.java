/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.activity.ActivityTestUtil.*;

import java.util.ArrayList;
import java.util.List;

import javax.measure.quantity.Length;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityType;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventStatistic;
import com.aawhere.route.event.RouteEventTestUtil;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Iterables;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class RouteScoreCalculatorUnitTest {

	/**
	 * 
	 */
	private static final Range<Integer> DEFAULT = Range.singleton(Route.DEFAULT_SCORE);
	private static final Range<Integer> ONE = Range.singleton(1);
	private static final Range<Integer> MINIMUM = Range.atLeast(2);
	private static final Range<Integer> SIGNIFICANT = Range.atLeast(20);
	private static final Range<Integer> LOW = Range.open(1, SIGNIFICANT.lowerEndpoint());
	private Range<Integer> expectedScore;
	private List<ActivityType> completionActivityTypes;
	private List<ActivityType> courseActivityTypes;
	private List<ActivityType> socialActivityTypes;
	private Ratio minimumCourseSocialCompletionRatio = MeasurementUtil.ratio(1, 100);
	// low for testing
	private Integer courseCountMin = 2;
	private List<RouteEventId> routeEventIds;
	private Boolean finishesAtStart;
	private Length boundsDiagonalLength = QuantityMath.create(RouteScoreCalculator.BOUNDS_LENGTH_MIN_DEFAULT).times(2)
			.getQuantity();

	@Before
	public void setUp() {
		expectedScore = Range.singleton(RouteScoreCalculator.LOWEST_SCORE);
		completionActivityTypes = new ArrayList<ActivityType>();
		courseActivityTypes = new ArrayList<ActivityType>();
		socialActivityTypes = new ArrayList<ActivityType>();
		this.routeEventIds = new ArrayList<RouteEventId>();
		this.finishesAtStart = true;
	}

	@Test
	public void testNoCourses() {
		Iterables.addAll(this.completionActivityTypes, activityTypes(SIGNIFICANT));
		// score remains at default because without course there is no validation

	}

	/**
	 * A single course is not validation since any route can be created to represent an activity
	 * that is the one course. It's the second course that should be significant.
	 * 
	 */
	@Test
	public void testOneCourse() {
		this.courseActivityTypes.add(ActivityType.BIKE);
		Iterables.addAll(this.completionActivityTypes, activityTypes(SIGNIFICANT));
		// score remains at zero
	}

	/** The minimum number of courses should indicate a score. */
	@Test
	public void testEnoughCourses() {
		populate(MINIMUM);
		this.expectedScore = Range.atLeast(1);
	}

	@Test
	public void testRouteBoundsTooSmall() {
		populate(SIGNIFICANT);
		setUpBoundsTooShort();
		// expecting no score
	}

	/**
	 * @param range
	 */
	private void populate(Range<Integer> range) {

		final Iterable<ActivityType> activityTypes = activityTypes(range);
		Iterables.addAll(this.courseActivityTypes, activityTypes);
		Iterables.addAll(this.completionActivityTypes, activityTypes);
		Iterables.addAll(this.socialActivityTypes, activityTypes);
	}

	/**
	 * A bunch of personal completions, but no social should keep the score small.
	 * 
	 */
	@Test
	public void testManyPersonalNoSocial() {
		populate(ONE);
		// adding more to general without adding to social means many personal.
		completions(SIGNIFICANT);
		Iterables.addAll(this.courseActivityTypes, activityTypes(SIGNIFICANT));
		// personal only routes remain at zero
	}

	private void completions(Range<Integer> range) {
		Iterables.addAll(this.completionActivityTypes, activityTypes(range));
	}

	private void socialCompletions(Range<Integer> range) {
		Iterables.addAll(this.socialActivityTypes, activityTypes(range));
	}

	@Test
	public void testEventsDominate() {
		populate(SIGNIFICANT);
		this.routeEventIds.addAll(RouteEventTestUtil.eventIds(RouteTestUtils.routeId(), SIGNIFICANT.lowerEndpoint()));
		// significant events should still have more than the default score so they participate in
		// the indexes.
		this.expectedScore = ONE;
	}

	@Test
	public void testEventsAreFew() {
		populate(SIGNIFICANT);
		this.routeEventIds.addAll(RouteEventTestUtil.eventIds(RouteTestUtils.routeId(), ONE.lowerEndpoint()));
		this.expectedScore = SIGNIFICANT;
	}

	@Test
	public void testManyPersonalEnoughSocial() {
		populate(MINIMUM);
		Iterables.addAll(this.courseActivityTypes, activityTypes(SIGNIFICANT));
		Iterables.addAll(this.completionActivityTypes, activityTypes(SIGNIFICANT));
		this.expectedScore = SIGNIFICANT;
	}

	/** TN-790 low percentage of courses should be low scoring */
	@Test
	public void testManyCompletionsNotEnoughCourses() {
		populate(MINIMUM);
		socialCompletions(SIGNIFICANT);
		this.expectedScore = DEFAULT;
		// bump up the requirement since the number of activity types can be variable
		this.minimumCourseSocialCompletionRatio = MeasurementUtil.ratio(50, 100);
	}

	@Test
	public void testManySocial() {
		populate(SIGNIFICANT);
		this.expectedScore = SIGNIFICANT;
	}

	@Test
	public void testManySocialOneWay() {
		// only the minimum number of courses
		populate(MINIMUM);
		// now inflate the number of completions
		Iterables.addAll(this.completionActivityTypes, activityTypes(SIGNIFICANT));
		this.expectedScore = LOW;
		this.finishesAtStart = false;
	}

	void setUpBoundsTooShort() {
		this.boundsDiagonalLength = QuantityMath.create(RouteScoreCalculator.BOUNDS_LENGTH_MIN_DEFAULT).dividedBy(2)
				.getQuantity();
	}

	/**
	 * 
	 */
	@After
	public void test() {
		CategoricalStatsCounter<RouteEventStatistic, RouteEventId> eventCounter = RouteEventStatistic
				.counter(routeEventIds).build();

		RouteStats completionStats = RouteStats.routeStatsCreator()
				.copy(ActivityStats.activityTypesOnly(completionActivityTypes)).routeEventStats(eventCounter.stats())
				.build();
		ActivityStats coursesStats = ActivityStats.activityTypesOnly(courseActivityTypes);
		ActivityStats socialStats = ActivityStats.activityTypesOnly(socialActivityTypes);
		RouteScoreCalculator scoreCalculator = RouteScoreCalculator.create().completionStats(completionStats)
				.boundsDiagonalLength(this.boundsDiagonalLength).courseStats(coursesStats).socialStats(socialStats)
				.finishesAtStart(finishesAtStart)
				.minimumCourseSocialCompletionRatio(minimumCourseSocialCompletionRatio).courseCountMin(courseCountMin)
				.build();

		TestUtil.assertRange(expectedScore, scoreCalculator.score());
	}
}
