/**
 * 
 */
package com.aawhere.route.event;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.Interval;
import org.junit.Before;

import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ActivityType;
import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.lang.If;
import com.aawhere.measure.Count;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.route.Route;
import com.aawhere.route.RouteTestUtils;
import com.aawhere.route.event.RouteEvent.Builder;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.Message;

import com.google.common.collect.Maps;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class RouteEventUnitTest
		extends BaseEntityJaxbBaseUnitTest<RouteEvent, Builder, RouteEventId, RouteEventRepository> {

	private Interval interval;
	private Route route;
	private RouteEventId id;
	private ActivityType activityType;
	private GeoCells startArea;
	private Integer score;
	private Count completionsCount;
	private Message name;
	private Interval intervalMutated;
	private Message nameMutated;

	/**
	 * 
	 */
	public RouteEventUnitTest() {
		super(MUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		if (this.interval == null) {
			this.interval = JodaTestUtil.interval();
		}
		if (this.route == null) {
			this.route = RouteTestUtils.createRouteWithId();
		}
		if (this.id == null) {
			this.id = RouteEventUtil.routeEventId(this.route.id(), interval);
		}
		if (this.activityType == null) {
			this.activityType = ActivityTestUtil.activityType();
		}
		if (this.startArea == null) {
			this.startArea = this.route.startArea();
		}
		if (this.score == null) {
			this.score = TestUtil.i(Range.closed(1, 1000));
		}
		if (this.completionsCount == null) {
			this.completionsCount = MeasurementUtil.count(TestUtil.i());
		}
		if (this.name == null) {
			this.name = TestUtil.message();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#getId()
	 */
	@Override
	protected RouteEventId getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.interval(interval);
		builder.route(route);
		builder.activityType(activityType);
		builder.score(score);
		builder.completionsCount(completionsCount);
		builder.name(name);
	}

	static RouteEventUnitTest instance(RouteEventId id, Route route, Interval interval, ActivityType activityType) {
		RouteEventUnitTest test = new RouteEventUnitTest();
		if (id != null) {
			test.id = id;
		}

		if (route != null) {
			test.route = route;
		}

		if (interval != null) {
			test.interval = interval;
		}
		if (activityType != null) {
			test.activityType = activityType;
		}
		test.baseSetUp();
		test.setUp();
		return test;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertSample(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertSample(RouteEvent sample) {
		super.assertSample(sample);
		assertEquals(route.id(), sample.routeId());
		assertEquals(activityType, sample.activityType());
		assertEquals(startArea, sample.startArea());
		assertEquals(this.id, sample.id());
		assertEquals(this.score, sample.score());
		assertEquals(this.completionsCount, sample.completionsCount());
		assertEquals("name", If.nil(this.nameMutated).use(this.name), sample.name());
		assertEquals("interval", If.nil(this.intervalMutated).use(this.interval), sample.interval());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.Identifier)
	 */
	@Override
	protected RouteEvent mutate(RouteEventId id) throws EntityNotFoundException {
		// interval
		this.intervalMutated = JodaTestUtil.interval();
		getRepository().interval(id, intervalMutated);

		// name
		this.nameMutated = TestUtil.message();
		HashMap<RouteEventId, Message> names = Maps.newHashMap();
		names.put(id, this.nameMutated);
		getRepository().names(names);
		return super.mutate(id);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(RouteEvent mutated) {
		assertEquals("mutated name", this.nameMutated, mutated.name());
		assertEquals("mutated interval", this.intervalMutated, mutated.interval());
		super.assertMutation(mutated);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return RouteEventTestUtil.adapters();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertEntityEquals(com.aawhere.persist.BaseEntity,
	 * com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertEntityEquals(RouteEvent expected, RouteEvent actual) {
		super.assertEntityEquals(expected, actual);
		Interval actualInterval = actual.interval();
		assertNotNull(actualInterval);
		TestUtil.assertDateTimeEquals(expected.interval().getStart(), actualInterval.getStart(), 0l);
		TestUtil.assertDateTimeEquals(expected.interval().getEnd(), actualInterval.getEnd(), 0l);
		assertEquals(expected.routeId(), actual.routeId());
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected RouteEventRepository createRepository() {
		return new RouteEventMockRepository();
	}

	public static class RouteEventMockRepository
			extends MockFilterRepository<RouteEvents, RouteEvent, RouteEventId>
			implements RouteEventRepository {
		private final RouteEventRepositoryDelegate delegate = new RouteEventRepositoryDelegate(this);

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.route.event.RouteEventRepository#update(com.aawhere.route.event.RouteEventId,
		 * org.joda.time.Interval)
		 */
		@Override
		public RouteEvent interval(RouteEventId id, Interval interval) throws EntityNotFoundException {
			return this.delegate.interval(id, interval);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.event.RouteEventRepository#names(java.util.Map)
		 */
		@Override
		public Map<RouteEventId, RouteEvent> names(Map<RouteEventId, Message> names) {
			return this.delegate.names(names);
		}

	}
}
