/**
 *
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.NotImplementedException;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.ActivityUnitTest;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatsTestUtil;
import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.persist.MutableRepository;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.person.group.SocialGroupTestUtil;
import com.aawhere.route.Route.Builder;
import com.aawhere.test.TestUtil;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadTestUtil;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author Brian Chapman
 * 
 */
public class RouteUnitTest
		extends BaseEntityJaxbBaseUnitTest<Route, Route.Builder, RouteId, RouteRepository> {

	private Trailhead start;
	private Trailhead finish;
	private Activity course;
	private Integer score;
	private RouteId mainId;
	private RouteId mainIdMutated;
	private Message name;
	private RouteId routeId;
	private Trailhead mutatedStart;
	private Trailhead mutatedFinish;
	private ActivityStats coursesSummary;
	private Integer numberOfAlternates;
	private RouteStats completionStats;
	private Set<SocialGroupId> socialGroups;
	private List<RouteRelation> routeRelations;
	private ArrayList<RelativeStatistic> relativeStats;

	public RouteUnitTest() {
		super(MUTABLE);
	}

	@Override
	@Before
	public void setUp() {
		setUpCourse(null, null);

	}

	/**
	 *
	 */
	private void setUpCourse(Activity course, Integer score) {

		if (course == null) {
			course = ActivityTestUtil.createActivity();
		}
		this.course = course;
		this.start = createTrailhead(this.course.getTrackSummary().getStart().getLocation());
		this.finish = createTrailhead(this.course.getTrackSummary().getFinish().getLocation());
		if (score == null) {
			score = TestUtil.generateRandomInt(0, 1000);
		}
		this.score = score;
		this.mainId = RouteTestUtils.routeId();
		this.name = new StringMessage(TestUtil.generateRandomAlphaNumeric());
		this.mutatedStart = TrailheadTestUtil.createNearbyTrailhead(this.start);
		this.mutatedFinish = TrailheadTestUtil.createNearbyTrailhead(finish);
		this.numberOfAlternates = TestUtil.generateRandomInt();
		this.coursesSummary = RouteTestUtils.routeCompletionsSummary();
		// routeId is not yet assigned...so just use the main id which may create confusing results.
		this.completionStats = RouteTestUtils.routeStats(this.mainId);
		this.socialGroups = Sets.newHashSet(SocialGroupTestUtil.socialGroupId());
		this.routeRelations = Lists.newArrayList(RouteRelation.create().activityType(ActivityType.BIKE)
				.relatedRouteId(RouteTestUtils.routeId()).build());
		this.relativeStats = Lists.newArrayList(RelativeStatsTestUtil.allBuilder().build());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#testDictionary()
	 */
	@Test
	@Override
	public void testDictionary() {
		super.testDictionary();
	}

	/**
	 * @return
	 * 
	 */
	protected Trailhead createTrailhead(GeoCoordinate location) {
		return TrailheadTestUtil.createTrailhead(location);
	}

	protected Activity createExemplarActivity() {
		return ActivityUnitTest.getInstance().getEntitySampleWithId();
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected RouteRepository createRepository() {
		return new RouteMockRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere
	 * .persist.BaseEntity.Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.start(start);
		builder.finish(finish);
		builder.course(course);
		builder.score(score);
		builder.mainId(mainId);
		builder.name(name);
		builder.courseCompletionStats(this.coursesSummary);
		builder.socialCompletionStats(this.coursesSummary);
		builder.completionStats(completionStats);
		builder.socialGroups(socialGroups);
		builder.routeRelations(routeRelations);
		builder.addRelativeStat(this.relativeStats.get(0));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulationWithId(com .aawhere
	 * .persist.BaseEntity.Builder)
	 */
	@Override
	protected void creationPopulationWithId(Builder builder) {
		// Calls #createionPopulation()
		super.creationPopulationWithId(builder);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	public void assertCreation(Route sample) {
		super.assertCreation(sample);
		assertEquals(this.start.id(), sample.start());
		assertEquals(this.finish.id(), sample.finish());
		assertEquals(this.mainId, sample.mainId());
		assertFalse("main id was not set to match the created id", sample.isMain());
		assertFalse(this.start + "shouldn't finish at " + this.finish, sample.isFinishAtStart());
		assertSample(sample);
	}

	@Test
	public void testFinishAtStart() {
		Route returnToStart = Route.mutate(getEntitySample()).finish(start).build();
		assertTrue(	returnToStart.start() + " should finish at " + returnToStart.finish(),
					returnToStart.isFinishAtStart());
	}

	/**
	 * @param sample
	 */
	@Override
	protected void assertSample(Route sample) {
		assertEquals(this.course.getId(), sample.courseId());
		assertEquals(this.score, sample.getScore());
		if (sample.isMain()) {
			assertEquals(this.numberOfAlternates, sample.alternateCount());
		} else {
			assertNull("alternates don't have alternates", sample.alternateCount());
		}
		assertEquals(this.name, sample.name());
		SortedSet<ActivityTypeStatistic> actualActivityTypeCounts = sample.completionStats().activityTypeStats();
		assertEquals("activityTypeCounts", this.completionStats.activityTypeStats(), actualActivityTypeCounts);
		assertEquals(this.completionStats.applicationStats(), sample.completionStats().applicationStats());
		// application and activity types both assign total, but app stats is second so it wins
		assertEquals(	"completionCount",
						CategoricalStatsCounter.sum(sample.completionStats().applicationStats()),
						sample.completionStats().total());
		MeasureTestUtil.assertEquals(	"distance should be set by activity",
										this.course.getTrackSummary().getDistance(),
										sample.distance());
		// BoundingBoxTestUtils.assertEquals(this.course.boundingBox(), sample.boundingBox());
		// bounding box is not JAXB marshaling/unmarshaling properly
		assertNotNull(sample.boundingBox());
		assertEquals(this.coursesSummary.applicationStats(), sample.courseCompletionStats().applicationStats());
		assertEquals(this.coursesSummary.activityTypeStats(), sample.courseCompletionStats().activityTypeStats());
		assertNotNull("total not assigned to stats", sample.courseCompletionStats().activityTypeStats().first().total());
		assertNull("no completions have been created so it shouldn't report anything", sample.isStale());

		final Integer routeEventsCompletionsCount = completionStats.routeEventsCompletionsCount();
		assertNotNull(routeEventsCompletionsCount);
		assertNotNull(completionStats.total());
		assertEquals(	"percent is wrong",
						MeasurementUtil.ratio(routeEventsCompletionsCount, completionStats.total()),
						sample.completionStats().routeEventCompletionsRatio());

		assertEquals(this.socialGroups, sample.socialGroupIds());
		assertEquals(this.routeRelations, sample.routeRelations());
		// completion counts is non-persistent so this is passive
		if (sample.relativeCompletionStats() != null) {
			// JAXB doesn't unmarshal so it's difficult to know when to test this
			// assertEquals(this.completionCounts, sample.relativeCompletionStats());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.Identifier)
	 */
	@Override
	protected Route mutate(RouteId id) throws EntityNotFoundException {
		// set the main id to itself...to test isMain=true
		mainIdMutated = id;
		getRepository().setMainId(id, mainIdMutated);
		getRepository().setAlternateCount(id, this.numberOfAlternates);
		getRepository().setStart(id, mutatedStart.id());
		getRepository().setFinish(id, mutatedFinish.id());

		return super.mutate(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertStored(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertStored(Route sample) {
		super.assertStored(sample);
		// i'm not sure why I have to do this here, but the parent doesn't call.
		assertSample(sample);

	}

	@Override
	public void assertEntityEquals(Route expected, Route actual) {
		super.assertEntityEquals(expected, actual);
		assertEquals(expected.start(), actual.start());
		assertEquals(expected.finish(), actual.finish());
	}

	@Test
	public void testSort() {
		Route route = getEntitySampleWithId();
		Route better = Route.mutate(getEntitySampleWithId()).score(route.getScore() + 1).build();
		Routes unsorted = Routes.create().add(route, better).build();
		assertEquals(route, Iterables.getFirst(unsorted, null));
		assertEquals(better, Iterables.getLast(unsorted, null));

		Routes sorted = Routes.create().add(route, better).sort().build();
		assertEquals(route, Iterables.getLast(sorted, null));
		assertEquals(better, Iterables.getFirst(sorted, null));

	}

	@Test
	public void testAddAliases() throws EntityNotFoundException {
		Route route = getEntitySample();
		Route stored = getRepository().store(route);
		TestUtil.assertEmpty(stored.aliases());
		Set<RouteId> expectedAliases = IdentifierTestUtil.generateRandomLongIds(Route.class, RouteId.class);
		// the route must always be included in it's own aliases
		expectedAliases.add(route.id());
		{
			Route updated = getRepository().aliasesAdded(stored.id(), expectedAliases);
			TestUtil.assertCollectionEquals(expectedAliases, updated.aliases());
		}
		// do it again to ensure additive.
		Set<RouteId> moreExpectedAliases = IdentifierTestUtil.generateRandomLongIds(Route.class, RouteId.class);
		Route updated = getRepository().aliasesAdded(stored.id(), moreExpectedAliases);
		HashSet<RouteId> combinedExpectedAliases = Sets.newHashSet(expectedAliases);
		combinedExpectedAliases.addAll(moreExpectedAliases);
		TestUtil.assertCollectionEquals(combinedExpectedAliases, updated.aliases());
		// add the same thing one more time and confirm no modification happened.
		Route routeAlreadyHasAliases = getRepository().aliasesAdded(stored.id(), combinedExpectedAliases);
		assertEquals(updated.getEntityVersion(), routeAlreadyHasAliases.getEntityVersion());
	}

	@Test
	public void testSetAliasesWhenOnlyRouteAndIsAlreadyNull() throws EntityNotFoundException {
		Route route = getEntitySamplePersisted();
		TestUtil.assertEmpty("aliases are empty until an update", route.aliases());
		HashSet<RouteId> aliases = Sets.newHashSet(route.id());
		Route mutated = getRepository().aliasesSet(route.id(), aliases);
		assertEquals(aliases, mutated.aliases());
	}

	@Test
	public void testSetAliasesWithoutMe() throws EntityNotFoundException {
		Route route = getEntitySamplePersisted();
		Route updated = getRepository().aliasesSet(route.id(), Sets.newHashSet(RouteTestUtils.routeId()));
		TestUtil.assertContains(route.id(), updated.aliases());
	}

	@Test
	public void testAddAliasWithouMe() throws EntityNotFoundException {
		Route route = getEntitySamplePersisted();
		getRepository().aliasesAdded(route.id(), Sets.newHashSet(RouteTestUtils.routeId()));
	}

	/**
	 * During mutation if the aliases is cleaned up then the route could end up being it's own
	 * alias..in that case aliases should be null.
	 */
	@Test
	public void testSetAliasesWhenOnlyRouteAndIsNotAlreadyNull() throws EntityNotFoundException {
		Route route = getEntitySamplePersisted();
		route = getRepository().aliasesAdded(	route.id(),
												Sets.newHashSet(RouteTestUtils.routeId(), RouteTestUtils.routeId()));

		TestUtil.assertSize("two aliases plus self", 3, route.aliases());
		HashSet<RouteId> aliasesOnlyMe = Sets.newHashSet(route.id());
		Route mutated = getRepository().aliasesSet(route.id(), aliasesOnlyMe);
		assertEquals("when only route then null is fine", aliasesOnlyMe, mutated.aliases());
	}

	@Override
	protected RouteId getId() {
		if (this.routeId == null) {
			return super.getId();
		} else {
			return routeId;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(Route mutated) {
		super.assertMutation(mutated);
		assertEquals(mainIdMutated, mutated.mainId());
		assertEquals("mainId was set as the mutation", mutated.id(), mutated.mainId());
		assertEquals(	"now it's a main so it can have alternate count",
						this.numberOfAlternates,
						mutated.alternateCount());
		assertTrue("mainId==id==isMain()", mutated.isMain());
		assertEquals("start", mutatedStart.id(), mutated.start());
		assertEquals("finish", mutatedFinish.id(), mutated.finish());
	}

	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return RouteTestUtils.adapters();

	}

	/*
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getClassesToBeBound()
	 */
	@Override
	protected Class<?>[] getClassesToBeBound() {
		// necessary for Route#socialGroups map
		return new Class[] { ActivityType.class };

	}

	public static RouteUnitTest getInstance() {
		return getInstance(null, null);
	}

	public static RouteUnitTest getInstance(RouteId routeId) {
		RouteUnitTest test = getInstance();
		test.routeId = routeId;
		return test;
	}

	/*
	 * @see
	 * com.aawhere.persist.BaseEntityJaxbBaseUnitTest#assertJaxb(com.aawhere.xml.bind.JAXBTestUtil)
	 */
	@Override
	protected void assertJaxb(JAXBTestUtil<Route> util) {
		super.assertJaxb(util);
		util.assertContains(RouteField.DESCRIPTION);
	}

	public static RouteUnitTest getInstance(Activity course) {
		return getInstance(course, null);
	}

	public static RouteUnitTest getInstance(Integer score) {
		return getInstance(null, score);
	}

	public static RouteUnitTest getInstance(Activity course, Integer score) {
		RouteUnitTest instance = new RouteUnitTest();
		instance.baseSetUp();
		instance.setUpCourse(course, score);
		return instance;
	}

	public static class RouteMockRepository
			extends MockFilterRepository<Routes, Route, RouteId>
			implements RouteRepository, MutableRepository<Route, RouteId> {

		private RouteRepositoryDelegate delegate;

		public RouteMockRepository() {
			super();
			this.delegate = RouteRepositoryDelegate.build(this);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.Route.RouteRepository#filter(com.aawhere.Route.RoutesFilter )
		 */
		@Override
		public Routes filter(Filter filter) {
			throw new NotImplementedException("not worth implementing completely");
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.FilterRepository#filterAsIterable(com.aawhere
		 * .persist.BaseFilter)
		 */
		@Override
		public Iterable<Route> filterAsIterable(Filter filter) {
			throw new NotImplementedException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteRepository#setScore(com.aawhere.route.Route,
		 * java.lang.Integer)
		 */
		@Override
		public Route setScore(RouteId route, Integer score) throws EntityNotFoundException {
			return delegate.setScore(route, score);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteRepository#addAliases(com.aawhere.route.RouteId,
		 * java.util.Set)
		 */
		@Override
		public Route aliasesAdded(RouteId id, Set<RouteId> aliases) throws EntityNotFoundException {
			return delegate.aliasesAdded(id, aliases);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteRepository#setMainId(com.aawhere.route.RouteId,
		 * com.aawhere.route.RouteId)
		 */
		@Override
		public Route setMainId(RouteId id, RouteId mainId) throws EntityNotFoundException {
			return delegate.setMainId(id, mainId);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.MockRepository#update(com.aawhere.persist.BaseEntity)
		 */
		@Override
		public Route update(Route entity) {
			return super.update(entity);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteRepository#setName(com.aawhere.route.RouteId,
		 * com.aawhere.util.rb.Message)
		 */
		@Override
		public Route setName(RouteId id, Message name) throws EntityNotFoundException {
			return delegate.setName(id, name);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteRepository#setStart(com.aawhere.route.RouteId,
		 * com.aawhere.trailhead.TrailheadId)
		 */
		@Override
		public Route setStart(RouteId id, TrailheadId start) throws EntityNotFoundException {
			return delegate.setStart(id, start);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteRepository#setFinish(com.aawhere.route.RouteId,
		 * com.aawhere.trailhead.TrailheadId)
		 */
		@Override
		public Route setFinish(RouteId id, TrailheadId bestId) throws EntityNotFoundException {
			return delegate.setFinish(id, bestId);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteRepository#setAlternateCount(com.aawhere.route.RouteId,
		 * java.lang.Integer)
		 */
		@Override
		public Route setAlternateCount(RouteId id, Integer alternateCount) throws EntityNotFoundException {
			return delegate.setAlternateCount(id, alternateCount);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteRepository#setCompletionsPending(com.aawhere.route.RouteId,
		 * java.lang.Boolean)
		 */
		@Override
		public Route setStale(RouteId routeId, Boolean completionsArePending) throws EntityNotFoundException {
			return delegate.setStale(routeId, completionsArePending);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteRepository#setAliases(com.aawhere.route.RouteId,
		 * java.util.Set)
		 */
		@Override
		public Route aliasesSet(RouteId id, Set<RouteId> aliases) throws EntityNotFoundException {
			return delegate.aliasesSet(id, aliases);
		}

		/*
		 * @see com.aawhere.route.RouteRepository#update(com.aawhere.route.RouteId)
		 */
		@Override
		public Builder update(RouteId routeId) throws EntityNotFoundException {
			return delegate.update(routeId);
		}

	}

}
