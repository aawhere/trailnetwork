/**
 *
 */
package com.aawhere.route;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.ServiceTestUtilBase;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.person.pref.PrefUtil;
import com.aawhere.route.Routes.Builder;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventStatistic;
import com.aawhere.route.event.RouteEventTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.trailhead.TrailheadTestUtil;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.xml.bind.OptionalXmlAdapter.Visibility;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;

/**
 * @author brian
 * 
 */
public class RouteTestUtils
		extends ServiceTestUtilBase<Routes, Route, RouteId>
		implements RouteProvider {

	public final static StringMessage NAME = new StringMessage("James");

	public static Route createRoute() {
		return RouteUnitTest.getInstance().getEntitySample();
	}

	public static Route createRoute(Iterable<Trackpoint> track) {
		Activity course = ActivityTestUtil.createActivity(track);
		return RouteUnitTest.getInstance(course).getEntitySample();
	}

	public static Route createRouteWithId() {
		return RouteUnitTest.getInstance().getEntitySampleWithId();
	}

	public static Route createRouteWithId(Iterable<Trackpoint> track) {
		return createRouteWithId(ActivityTestUtil.createActivity(track));
	}

	public static RouteTrailheadProvider trailheadProvider() {
		return new RouteTrailheadTestProvider();
	}

	public static Route createRouteWithMainId() {
		Route route = RouteUnitTest.getInstance().getEntitySampleWithId();
		return Route.mutate(route).mainId(route.getId()).alternateCount(TestUtil.generateRandomInt()).build();
	}

	/**
	 * Give me your course and I will give you back a decent representation of that course as a
	 * route.
	 * 
	 * @param course
	 * @return
	 */
	public static Route createRouteWithId(Activity course) {
		return RouteUnitTest.getInstance(course).getEntitySampleWithId();
	}

	public static Routes createRoutes() {
		Routes.Builder builder = new Routes.Builder();
		Integer numberToCreate = TestUtil.generateRandomInt(1, 5);
		for (int i = 0; i < numberToCreate; i++) {
			builder.add(createRoute());

		}
		return builder.build();
	}

	/**
	 * Generates a random completion summary providing both application and activty types, but the
	 * same size as required.
	 * 
	 * @return
	 */
	public static ActivityStats routeCompletionsSummary() {
		return ActivityTestUtil.activityStats();
	}

	/**
	 * Creates a random {@link RouteId}.
	 * 
	 * @return
	 */
	public static RouteId routeId() {
		return IdentifierTestUtil.generateRandomLongId(Route.class, RouteId.class);
	}

	public static Set<XmlAdapter<?, ?>> adapters() {
		Set<XmlAdapter<?, ?>> adapters = ActivityTestUtil.adapters();

		adapters.addAll(ActivityTestUtil.adapters());
		adapters.addAll(TrailheadTestUtil.adapters());
		adapters.add(new RouteXmlAdapter(new RouteIdXmlAdapter(new RouteTestUtils())));
		adapters.add(new RouteXmlAdapter.ForCompletion(new RouteIdXmlAdapter(new RouteTestUtils())));
		adapters.add(new RouteXmlAdapter.ForEvent(new RouteIdXmlAdapter(new RouteTestUtils())));
		com.aawhere.xml.bind.OptionalXmlAdapter.Show show = new com.aawhere.xml.bind.OptionalXmlAdapter.Show();
		adapters.add(new Route.CourseStatsXmlAdapter(show));
		adapters.add(new Route.SocialCompletionStatsXmlAdapter(show));
		adapters.add(new Route.CompletionStatsXmlAdapter(show));
		adapters.add(new Route.BoundingBoxXmlAdapter(show));
		adapters.add(new RouteDescriptionXmlAdapter(new RouteDescriptionPersonalizer.Builder()
				.trailheadProvider(RouteTestUtils.trailheadProvider()).personProvider(PersonTestUtil.instance())
				.setPreferences(PrefUtil.getPreferences(Locale.US)).build(), Visibility.SHOW));
		adapters.add(new RouteCompletion.ActivitiesXmlAdapter(ActivityTestUtil.createActivityProvider()));
		return adapters;
	}

	/**
	 * @param course
	 * @return
	 */
	public static Route createRoute(Activity course) {
		return RouteUnitTest.getInstance(course).getEntitySample();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteProvider#route(com.aawhere.route.RouteId)
	 */
	@Override
	public Route route(RouteId id) {
		return createRouteWithId(id);
	}

	public static Route createRouteWithId(RouteId routeId) {
		return RouteUnitTest.getInstance(routeId).getEntitySampleWithId();
	}

	public static RouteTestUtils instance() {
		return new RouteTestUtils();
	}

	public static RouteProvider provider(final Iterable<Route> routes) {
		return new RouteProvider.Default(routes);

	}

	/**
	 * Produces a route with a score higher than that given.
	 * 
	 * @param minimumScore
	 * @return
	 */
	public static Route higherScoringRoute(Integer minimumScore) {
		Route bigger;
		while ((bigger = RouteTestUtils.createRoute()).score() <= minimumScore) {

		}
		return bigger;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Route apply(RouteId input) {
		return (input != null) ? route(input) : null;
	}

	/**
	 * @return
	 */
	public static RouteStats routeStats(RouteId routeId) {
		final ActivityStats activityStats = ActivityTestUtil.activityStats();
		List<RouteEventId> eventIds = RouteEventTestUtil.eventIds(routeId, TestUtil.i(Range.closed(1, 100)));
		SortedSet<RouteEventStatistic> eventStats = RouteEventStatistic.counter(eventIds).build().stats();
		// FIXME: there will be a total discrepency between the activity stats and the event stats.
		return RouteStats.routeStatsCreator().copy(activityStats).routeEventStats(eventStats).build();
	}

	/**
	 * @return
	 */
	public static Integer scoreWorthy() {
		return TestUtil.generateRandomInt(Route.DEFAULT_SCORE + 1, 1000);
	}

	public Function<RouteId, Route> routeFunction() {
		return new Function<RouteId, Route>() {

			@Override
			public Route apply(RouteId input) {
				return (input != null) ? route(input) : null;
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceStandard#all(java.lang.Iterable)
	 */
	@Override
	public Map<RouteId, Route> all(Iterable<RouteId> ids) {
		return IdentifierUtils.map(Iterables.transform(ids, routeFunction()));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceTestUtilBase#entity(com.aawhere.id.Identifier)
	 */
	@Override
	public Route entity(RouteId id) {
		return route(id);
	}

	public static class RouteTrailheadTestProvider
			extends TrailheadTestUtil.TrailheadTestProvider
			implements RouteTrailheadProvider {

	}

	/**
	 * @return
	 */
	public static Function<Iterable<RouteId>, Iterable<Route>> idsFunction() {
		return new Function<Iterable<RouteId>, Iterable<Route>>() {

			@Override
			public Iterable<Route> apply(Iterable<RouteId> input) {
				return (input != null) ? routes(input) : null;
			}
		};
	}

	/**
	 * @param input
	 * @return
	 */
	public static Iterable<Route> routes(Iterable<RouteId> input) {
		ArrayList<Route> routes = Lists.newArrayList();
		for (RouteId routeId : input) {
			routes.add(createRouteWithId(routeId));
		}
		return routes;
	}

	/**
	 * @return
	 */
	public static Routes createRoutesWithIds() {
		int size = TestUtil.generateRandomInt(1, 10);
		Builder builder = Routes.create();
		for (int i = 0; i < size; i++) {
			builder.add(createRouteWithId());
		}
		return builder.build();
	}

	public static Route.Builder builder() {
		return builder(new RouteUnitTest.RouteMockRepository());
	}

	public static Route.Builder builder(final RouteRepository repository) {
		RouteUnitTest routeUnitTest = new RouteUnitTest();
		routeUnitTest.baseSetUp();
		routeUnitTest.setUp();
		Route.Builder builder = new Route.Builder() {
			@Override
			public Route build() {
				return repository.store(super.build());
			}
		};

		routeUnitTest.creationPopulation(builder);
		return builder;
	}
}
