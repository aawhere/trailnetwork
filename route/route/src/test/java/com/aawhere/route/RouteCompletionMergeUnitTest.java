/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.route.RouteCompletionUtil.*;
import static org.junit.Assert.*;

import org.joda.time.Duration;
import org.joda.time.ReadableDuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.joda.time.DurationTestUtil;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Range;

/**
 * Dedicated to the {@link RouteCompletionUtil#merged(RouteCompletion, RouteCompletion)} since there
 * are many combinations of what can happen and a lot of repeat this deserves its own test.
 * 
 * TODO:This suite is incomplete. Add more tests. There are so many combinations and the tests are
 * simple so add them as needed perhaps when concerns arise.
 * 
 * @author aroller
 * 
 */
public class RouteCompletionMergeUnitTest {

	private RouteCompletion original;
	private RouteCompletion replacement;
	private RouteCompletion.Builder originalBuilder;
	private RouteCompletion.Builder replacmentBuilder;
	private Boolean expectedIsCourse = false;
	private Integer expectedCoursesCompletedCount;
	private Integer expectedActivityCount;
	private Duration expectedDuration;

	@Before
	public void setUp() {
		RouteCompletion base = RouteCompletionTestUtil.routeCompletion();
		this.expectedDuration = base.duration();
		this.replacmentBuilder = RouteCompletion.clone(base);
		originalBuilder = RouteCompletionTestUtil.mutate(base);
	}

	@After
	public void test() {
		this.original = originalBuilder.build();
		this.replacement = this.replacmentBuilder.build();
		RouteCompletion merged = merged(original, replacement);
		assertSame("original  is mutated, not cloned", merged, original);
		assertEquals(expectedActivityCount, merged.courseActivityCount());
		assertEquals(expectedCoursesCompletedCount, merged.coursesCompletedCount());
		assertEquals(expectedIsCourse, merged.isCourse());
		assertEquals(expectedDuration, merged.duration());
	}

	@Test
	public void testMinimum() {
		// tests the defaults of a route completion being merged with another minimum with no
		// changes.
		// a nice reality check
	}

	/** Provides a value for every item that is to be merged, no nulls in this test. */
	@Test
	public void isCourseFromReplacement() {
		this.expectedIsCourse = true;
		this.replacmentBuilder.isCourse(expectedIsCourse);
	}

	@Test
	public void isCourseFromOriginal() {
		this.expectedIsCourse = true;
		this.replacmentBuilder.isCourse(!expectedIsCourse);
		originalBuilder.isCourse(expectedIsCourse);
	}

	// ================== Course Activity Count ==================
	@Test
	public void courseActivityCountFromOriginalNullReplacement() {
		this.expectedActivityCount = TestUtil.generateRandomInt();
		this.originalBuilder.courseActivityCount(expectedActivityCount);
	}

	@Test
	public void courseActivityCountFromOriginalSmallerReplacement() {
		this.expectedActivityCount = TestUtil.generateRandomInt();
		this.originalBuilder.courseActivityCount(expectedActivityCount);
		this.replacmentBuilder.courseActivityCount(expectedActivityCount - 1);
	}

	@Test
	public void courseActivityCountFromReplacementSmallerOriginal() {
		this.expectedActivityCount = TestUtil.generateRandomInt();
		this.originalBuilder.courseActivityCount(expectedActivityCount - 1);
		this.replacmentBuilder.courseActivityCount(expectedActivityCount);
	}

	@Test
	public void courseActivityCountFromReplacementNullOriginal() {
		this.expectedActivityCount = TestUtil.generateRandomInt();
		this.replacmentBuilder.courseActivityCount(expectedActivityCount);
	}

	// ===================== Courses COmpleted Count =========================
	@Test
	public void courseCoursesCompletedCountFromReplacementSmallerOriginal() {
		this.expectedCoursesCompletedCount = TestUtil.generateRandomInt();
		this.originalBuilder.coursesCompletedCount(expectedCoursesCompletedCount - 1);
		this.replacmentBuilder.coursesCompletedCount(expectedCoursesCompletedCount);
	}

	// ===================== Duration =========================
	@Test
	public void courseDurationFromReplacementBiggerOriginal() {
		this.expectedDuration = DurationTestUtil.duration();
		this.replacmentBuilder.duration(expectedDuration);
		// merge keeps the smaller of the two
		this.originalBuilder.duration(DurationTestUtil.duration(Range
				.greaterThan((ReadableDuration) this.expectedDuration)));
	}

}
