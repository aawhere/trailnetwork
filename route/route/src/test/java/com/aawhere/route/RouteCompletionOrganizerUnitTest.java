/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.lang.number.NumberUtilsExtended.*;
import static com.aawhere.route.RouteCompletionTestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.ActivityUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.joda.time.IntervalUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.person.Person;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventScoreCalculator;
import com.aawhere.route.event.RouteEventStatistic;
import com.aawhere.route.event.RouteEventTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.track.TrackSummaryUtil;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

/**
 * @author aroller
 * 
 */
public class RouteCompletionOrganizerUnitTest {

	/**
	 * 
	 */
	private List<RouteCompletion> completions = new ArrayList<RouteCompletion>();
	private Integer expectedCompletionStatsTotal;
	// allows a test to be invoked manually and not run twice
	private boolean tested = false;
	private RouteCompletionOrganizer organizer;
	private Function<ActivityId, Person> personProvider = ActivityTestUtil.personProvider();
	private Ratio significantActivityTypePercent = ActivityUtil.MIN_ACTIVITY_TYPE_PERCENT;
	private List<RouteEvent> existingEvents = new ArrayList<RouteEvent>();
	private Route route = RouteTestUtils.createRouteWithId();
	private Integer expectedNumberOfParticipatingEvents;

	@Before
	public void setUp() {

	}

	@After
	public void test() {
		if (!this.tested) {
			this.tested = true;
			if (this.expectedCompletionStatsTotal == null) {
				this.expectedCompletionStatsTotal = this.completions.size();
			}
			// order of input shouldn't affect the organizer
			Collections.shuffle(completions);
			final Function<ActivityId, Integer> randomIntegerFunction = TestUtil.randomIntegerFunction();
			this.organizer = RouteCompletionOrganizer.create().route(route).completions(completions)
					.existingEvents(existingEvents).personProvider(this.personProvider)
					.attemptTimingsCounter(randomIntegerFunction).courseTimingsCounter(randomIntegerFunction)
					.attemptTimingsCounter(randomIntegerFunction)
					.significantActivityTypePercent(significantActivityTypePercent).minNumberOfCoursesToScore(2)
					.build();
			RouteStats completionStats = organizer.completionStats();
			TestUtil.assertSize(expectedCompletionStatsTotal, this.organizer.completions());
			List<RouteCompletion> mutatedCompletions = organizer.completions();
			assertNotNull(organizer.routeScoreCalculator());
			int i = 0;
			for (RouteCompletion routeCompletion : mutatedCompletions) {
				assertNotNull("enforces that all completions have been built" + routeCompletion, routeCompletion.id());
				String message = routeCompletion.toString() + " at " + i++;
				assertNotNull(message, routeCompletion.socialOrder());
				assertNotNull(message, routeCompletion.socialRaceRank());
				assertNotNull(message, routeCompletion.socialCountRank());
				assertNotNull(message, routeCompletion.personalOrder());
				assertNotNull(message, routeCompletion.personalTotal());
				assertNotNull(message, routeCompletion.personalRaceRank());

			}
			assertEquals("completion stats total", this.expectedCompletionStatsTotal, completionStats.total());
			if (this.expectedNumberOfParticipatingEvents != null) {
				assertEquals("count must be part of events", I(1), this.organizer.completionStats().routeEventsCount());
				Multimap<RouteEvent, RouteCompletionId> events = this.organizer.events();
				TestUtil.assertSize(expectedNumberOfParticipatingEvents, events.entries());
				SortedSet<RouteEventStatistic> eventStats = completionStats.routeEventStats();
				final int expectedNumberOfEvents = 1;
				TestUtil.assertSize(expectedNumberOfEvents, eventStats);
				RouteEventStatistic eventStatistic = Iterables.getLast(eventStats);
				assertEquals(	"statistic didn't count the two",
								expectedNumberOfParticipatingEvents,
								eventStatistic.count());
				assertEquals(	"the total should represent all the completions, not just the events",
								this.completions.size(),
								eventStatistic.total().intValue());
				TestUtil.assertSize(expectedNumberOfEvents, this.organizer.routeEventScoreCalculators());
			}
		}
	}

	protected RouteCompletion[] add(RouteCompletion... completions) {
		this.completions.addAll(Lists.newArrayList(completions));
		return completions;
	}

	private void samePersons(Iterable<RouteCompletion> samePersonCompletions) {
		HashMap<ActivityId, Person> commonPerson = new HashMap<ActivityId, Person>();
		Person person = null;
		for (RouteCompletion routeCompletion : samePersonCompletions) {
			if (person == null) {
				person = PersonTestUtil.personWithId();
			}
			commonPerson.put(routeCompletion.activityId(), person);
		}
		this.personProvider = ActivityTestUtil.personProvider(commonPerson);
	}

	@Test
	public void testNoCompletions() {
		// the default is setup for nothing expected
	}

	@Test
	public void testOneCompletion() {
		completion();
	}

	@Test
	public void testTwo() {
		RouteCompletion slowerAndLater = RouteCompletionTestUtil.mutate(completion()).duration(slowerDuration)
				.startTime(laterDateTime).build();
		RouteCompletion fasterAndEarlier = RouteCompletionTestUtil.mutate(completion()).duration(fasterDuration)
				.startTime(earlierDateTime).build();
		test();
		assertEquals(1, fasterAndEarlier.socialRaceRank().intValue());
		assertEquals(2, slowerAndLater.socialRaceRank().intValue());
		assertEquals(1, fasterAndEarlier.socialOrder().intValue());
		assertEquals(2, slowerAndLater.socialOrder().intValue());
		assertEquals(2, fasterAndEarlier.socialCountRank().intValue());
		assertEquals("tie breaker goes to most recent", 1, slowerAndLater.socialCountRank().intValue());

		assertEquals(1, fasterAndEarlier.personalOrder().intValue());
		assertEquals(1, fasterAndEarlier.personalTotal().intValue());
		assertEquals(1, fasterAndEarlier.personalRaceRank().intValue());
		assertEquals(1, slowerAndLater.personalOrder().intValue());
		assertEquals(1, slowerAndLater.personalTotal().intValue());
		assertEquals(1, slowerAndLater.personalRaceRank().intValue());
		assertNotNull(fasterAndEarlier.routeScore());
		assertNotNull(slowerAndLater.routeScore());
	}

	@Test
	public void testThreeFromSamePerson() {
		Person person = PersonTestUtil.personWithId();
		RouteCompletion slowerAndLater = RouteCompletionTestUtil.mutate(completion()).duration(slowerDuration)
				.person(person.id()).startTime(laterDateTime).build();
		RouteCompletion fasterAndEarlier = RouteCompletionTestUtil.mutate(completion()).duration(fasterDuration)
				.startTime(earlierDateTime).person(person.id()).build();
		RouteCompletion middle = RouteCompletionTestUtil.mutate(completion()).duration(middleDuration)
				.startTime(middleDateTime).person(person.id()).build();
		HashMap<ActivityId, Person> personMap = Maps.newHashMap();
		personMap.put(slowerAndLater.activityId(), person);
		personMap.put(fasterAndEarlier.activityId(), person);
		personMap.put(middle.activityId(), person);
		this.personProvider = Functions.forMap(personMap);
		test();
		final int expectedTotal = 3;
		assertEquals("same person, same total", expectedTotal, fasterAndEarlier.personalTotal().intValue());
		assertEquals("same person, same total", expectedTotal, slowerAndLater.personalTotal().intValue());
		assertEquals("same person, same total", expectedTotal, middle.personalTotal().intValue());
		assertEquals(1, fasterAndEarlier.socialRaceRank().intValue());
		assertEquals("slower than personal best gets a rank, but doesn't increment social rank", 2, middle
				.socialRaceRank().intValue());
		assertEquals("slower than personal best gets a rank, but doesn't increment social rank", 2, slowerAndLater
				.socialRaceRank().intValue());

		assertEquals(1, fasterAndEarlier.personalRaceRank().intValue());
		assertEquals(2, middle.personalRaceRank().intValue());
		assertEquals(3, slowerAndLater.personalRaceRank().intValue());

		assertEquals(1, fasterAndEarlier.personalOrder().intValue());
		assertEquals(2, middle.personalOrder().intValue());
		assertEquals(3, slowerAndLater.personalOrder().intValue());

		assertEquals(1, fasterAndEarlier.socialOrder().intValue());
		assertEquals(2, middle.socialOrder().intValue());
		assertEquals(3, slowerAndLater.socialOrder().intValue());

		assertEquals(1, fasterAndEarlier.socialCountRank().intValue());
		assertEquals("same person, same count rank", 1, middle.socialCountRank().intValue());
		assertEquals("same person, same count rank", 1, slowerAndLater.socialCountRank().intValue());

	}

	@Test
	public void testDuplicatesCourseChosen() {
		List<RouteCompletion> completions = RouteCompletionTestUtil.completionsDifferentPersonSameTimeSameDuration();
		RouteCompletion course = completions.get(0);
		RouteCompletion not = RouteCompletionTestUtil.mutate(completions.get(1)).isCourse(false)
		// assigning a rank to ensure it is removed
				.socialCountRank(4).build();
		assertTrue("supposed to be duplicates", RouteCompletionUtil.areDuplicates(course, not));
		this.completions.add(not);
		this.completions.add(course);
		this.expectedCompletionStatsTotal = 1;
		test();
		TestUtil.assertContainsOnly(not, this.organizer.duplicateCompletions());
		TestUtil.assertContainsOnly(course, this.organizer.completions());
		RouteCompletion duplicate = Iterables.getOnlyElement(this.organizer.duplicateCompletions());
		RouteCompletionTestUtil.assertDuplicate(duplicate);
	}

	@Test
	public void testCourseActivityCount() {
		RouteCompletion course = RouteCompletionTestUtil.create().isCourse(true).build();
		RouteCompletion not = RouteCompletionTestUtil.create().isCourse(false).build();
		this.completions.add(course);
		this.completions.add(not);
		test();
		assertNotNull(course.courseActivityCount());
		assertNotNull("courses complete other courses", course.coursesCompletedCount());
		assertNotNull("everycompletion completes course", not.coursesCompletedCount());
		assertNull("not a course so no activity count for course", not.courseActivityCount());
		TestUtil.assertContainsOnly(course, this.organizer.courses());
	}

	@Test
	public void testRepeatCount() {
		Person person = PersonTestUtil.personWithId();
		Integer repeatTotal = 2;
		RouteCompletion first = RouteCompletionTestUtil.withRepeatCount(1, repeatTotal).duration(slowerDuration)
				.startTime(earlierDateTime).person(person).build();
		RouteCompletion second = RouteCompletionTestUtil.withRepeatCount(2, repeatTotal).duration(fasterDuration)
				.startTime(laterDateTime).person(person).build();
		this.completions.add(first);
		this.completions.add(second);
		{
			final Map<ActivityId, Person> personMap = Maps.newHashMap();
			personMap.put(first.activityId(), person);
			personMap.put(second.activityId(), person);
			this.personProvider = Functions.forMap(personMap);
		}
		test();
		assertEquals(1, first.personalOrder().intValue());
		assertEquals(2, second.personalOrder().intValue());
		assertEquals(repeatTotal, first.personalTotal());
		assertEquals(repeatTotal, second.personalTotal());

		assertEquals(repeatTotal, first.repeatTotal());
		assertEquals(repeatTotal, second.repeatTotal());

		assertEquals(2, first.personalRaceRank().intValue());
		assertEquals(1, second.personalRaceRank().intValue());

	}

	/**
	 * Duplicate uploads must be associated to a single completion. This tests that a completion
	 * that
	 * {@link TrackSummaryUtil#isLikelyFromTheSameTrack(com.aawhere.track.TrackSummary, com.aawhere.track.TrackSummary)}
	 * will be associated to {@link RouteCompletion#activityIds()}. It will also ensure the best
	 * activity is chosen and placed first in the list.
	 */
	@Test
	public void testDuplicateActivityCombined() {

		DateTime commonStartTime = middleDateTime;
		Duration commonDuration = fasterDuration;
		Duration uncommonDuration = slowerDuration;
		RouteCompletion chosenCompletion = RouteCompletionTestUtil.create().startTime(commonStartTime)
				.duration(commonDuration).isCourse(true).build();

		RouteCompletion differentPersonSameTimeShouldBeDuplicate = RouteCompletionTestUtil.create()
				.startTime(commonStartTime).duration(commonDuration).build();
		RouteCompletion differentAccountCompletionDifferentDurationPasses = RouteCompletionTestUtil.create()
				.startTime(commonStartTime).duration(uncommonDuration).build();
		RouteCompletion unrelatedCompletionPasses = RouteCompletionTestUtil.create().startTime(laterDateTime).build();
		this.completions = Lists.newArrayList(	unrelatedCompletionPasses,
												differentPersonSameTimeShouldBeDuplicate,
												differentAccountCompletionDifferentDurationPasses,
												chosenCompletion);
		this.expectedCompletionStatsTotal = 3;
	}

	@Test
	public void testDuplicateActivityOverlappingTimesSamePerson() {
		this.completions.addAll(RouteCompletionTestUtil.completionsSamePersonOverlappingTimes());
		this.expectedCompletionStatsTotal = 1;
		this.samePersons(completions);
		test();
		RouteCompletion routeCompletion = this.organizer.completions().get(0);
		TestUtil.assertSize(2, routeCompletion.activityIds());
		RouteCompletion duplicate = this.organizer.duplicateCompletions().get(0);
		assertEquals(routeCompletion.id(), duplicate.duplicateId());
	}

	/**
	 * To allow for repeats this has built in tolerance for start/finish times not to trigger a
	 * duplicate.
	 */
	@Test
	public void testRepeat() {
		Pair<RouteCompletion, RouteCompletion> completionsConsecutiveRepeats = RouteCompletionTestUtil
				.completionsConsecutiveRepeats();
		this.completions.add(completionsConsecutiveRepeats.getLeft());
		this.completions.add(completionsConsecutiveRepeats.getRight());
		this.samePersons(completions);
		this.expectedCompletionStatsTotal = 2;
	}

	@Test
	public void testMtbSelected() {
		assertActivityTypeSolo(ActivityType.MTB, ActivityType.MTB);
	}

	@Test
	public void testWalkSelected() {
		assertActivityTypeSolo(ActivityType.HIKE, ActivityType.WALK);

	}

	@Test
	public void testBikeOverRoadSelected() {
		assertActivityTypeSolo(ActivityType.ROAD, ActivityType.BIKE);
	}

	/**
	 * @param reportedActivityType
	 * @param expectedActivityType
	 */
	private void
			assertActivityTypeSolo(final ActivityType reportedActivityType, final ActivityType expectedActivityType) {
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(reportedActivityType).build());
		test();
		RouteCompletion completion = this.organizer.completions().iterator().next();
		assertEquals("activity type not assigned properly", expectedActivityType, completion.activityType());
		assertEquals("activity type not reported properly", reportedActivityType, completion.activityTypeReported());
		final ActivityTypeStatistic stats = this.organizer.completionStats().activityTypeStats().iterator().next();
		assertEquals(expectedActivityType, stats.category());
		assertEquals(I(1), stats.count());
	}

	@Test
	public void testMixedBikingMtbMeetsPercentageThreshold() {
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.BIKE).build());
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.MTB).build());
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.ROAD).build());
		test();
		final ActivityTypeStatistic stats = this.organizer.completionStats().activityTypeStats().iterator().next();
		assertEquals(ActivityType.MTB, stats.category());
		assertEquals(I(3), stats.count());
	}

	@Test
	public void testMixedBikingNotEnoughMtb() {
		// forces MTB to exceed by count over BIKE, not in general
		this.significantActivityTypePercent = MeasurementUtil.RATIO_ONE;
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.BIKE).build());
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.MTB).build());
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.ROAD).build());
		test();
		final ActivityTypeStatistic stats = this.organizer.completionStats().activityTypeStats().iterator().next();
		assertEquals(ActivityType.BIKE, stats.category());
		assertEquals(I(3), stats.count());
	}

	@Test
	public void testNotEnoughWalkingToReport() {
		// 3/4 biking, 1/4 walk doesn't pass required 1/2
		this.significantActivityTypePercent = MeasurementUtil.ratio(1, 2);
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.BIKE).build());
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.BIKE).build());
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.BIKE).build());
		this.completions.add(RouteCompletionTestUtil.withActivityTypeReported(ActivityType.WALK).build());
		test();
		Integer expectedTotal = 4;
		assertEquals(expectedTotal, this.organizer.completionStats().applicationStats().iterator().next().total());
		final SortedSet<ActivityTypeStatistic> activityTypeStats = this.organizer.completionStats().activityTypeStats();
		TestUtil.assertSize("walking shouldn't have been included", 1, activityTypeStats);
		final ActivityTypeStatistic stats = activityTypeStats.iterator().next();
		assertEquals(ActivityType.BIKE, stats.category());
		assertEquals("even though walk didn't make the cut, it still affects the total", expectedTotal, stats.total());
	}

	/**
	 * A simple test to verify two completions that overlap in time will be part of the same event.
	 * A third is created to prove it will not.
	 * 
	 */
	@Test
	public void testEventCount() {
		earlyCompletion();
		middleCompletion();
		lateCompletion();
		this.expectedNumberOfParticipatingEvents = 2;

	}

	@Test
	@Ignore("not working, suspicion is the completion data is finding duplicae something ")
	public void testEventHighScore() {
		final DateTime now = DateTime.now().minusDays(1);
		int adder = 10000;
		Integer expectedNumberOfParticipants = 10;
		for (int i = 0; i < expectedNumberOfParticipants; i++) {
			this.completions.add(RouteCompletionTestUtil.create().startTime(now.plus(adder += adder)).build());

		}

		test();
		Integer expectedNumberOfEvents = 1;
		TestUtil.assertSize(expectedNumberOfEvents, this.organizer.routeEventScoreCalculators());
		// the minimum score...not science so just chose a fine number
		final int expectedMinimum = 10000;
		final Set<RouteEvent> events = this.organizer.events().keySet();
		TestUtil.assertSize(expectedNumberOfEvents, events);
		RouteEvent event = events.iterator().next();
		TestUtil.assertSize(expectedNumberOfEvents, this.organizer.routeEventScoreCalculators());
		assertEquals("count must be part of events", expectedNumberOfEvents, this.organizer.completionStats()
				.routeEventsCount());
		RouteEventScoreCalculator scoreCalculator = organizer.routeEventScoreCalculators().iterator().next();
		assertEquals(expectedNumberOfParticipants, scoreCalculator.getNumberOfParticipants());
		assertNotNull(event.score());
		TestUtil.assertGreaterThan(expectedMinimum, event.score());
	}

	@Test
	public void testEventAllParticipate() {
		earlyCompletion();
		middleCompletion();
		test();
		final int expectedNumberOfCompletions = 2;
		TestUtil.assertSize("expecting both completions to be in the event",
							expectedNumberOfCompletions,
							this.organizer.events().entries());
		TestUtil.assertSize("expecting only one event for both completions", 1, this.organizer.events().keySet());
		RouteEvent event = this.organizer.events().keySet().iterator().next();
		assertNotNull("score isn't assigned", event.score());
		GeoCells startArea = event.startArea();
		assertEquals("start area should be same as route's", this.route.startArea(), startArea);
		assertEquals("wrong number of completions reported", expectedNumberOfCompletions, event.completionsCount()
				.getValue());
	}

	/**
	 * @return
	 */
	private RouteCompletion lateCompletion() {
		return add(late())[0];
	}

	/**
	 * @return
	 */
	private RouteCompletion middleCompletion() {
		return add(middle())[0];
	}

	/**
	 * @return
	 */
	private RouteCompletion earlyCompletion() {
		return add(early())[0];
	}

	@Test
	public void testEventExistingEarly() {
		middleCompletion();
		lateCompletion();
		assertExistingEvent(earlyCompletion());

	}

	@Test
	public void testEventExistingEarlyEarlierEvent() {
		final RouteCompletion earlyCompletion = earlyCompletion();
		// makes an event that starts before the earliest, but overlaps it
		this.existingEvents.add(RouteEventTestUtil.routeEventBuilder()
				.activityType(earlyCompletion.activityTypeReported())
				.interval(IntervalUtil.minus(earlyCompletion.interval(), slowerDuration)).build());
		middleCompletion();
		lateCompletion();
		assertExistingEvent(earlyCompletion);

	}

	@Test
	public void testEventExistingMiddle() {
		earlyCompletion();
		lateCompletion();
		assertExistingEvent(middleCompletion());

	}

	/** Tests that when an earlier event exists the correct existing event will be found. */
	@Test
	public void testEventExistingMiddleEarlierEvent() {
		this.existingEvents.add(RouteEventTestUtil.event(IntervalUtil.minus(earlyCompletion().interval(),
																			slowerDuration)));
		lateCompletion();
		assertExistingEvent(middleCompletion());

	}

	/**
	 * Simple assertion allowing tests for a single targeted event.
	 * 
	 * @param completionWithInterval
	 */
	private void assertExistingEvent(final RouteCompletion completionWithInterval) {
		final Interval existingInterval = completionWithInterval.interval();
		RouteEvent existingEvent = RouteEventTestUtil.routeEventBuilder().interval(existingInterval)
				.activityType(completionWithInterval.activityType()).build();
		this.existingEvents.add(existingEvent);
		final RouteEventId expectedEventId = existingEvent.id();
		test();
		Multimap<RouteEvent, RouteCompletionId> events = this.organizer.events();
		TestUtil.assertContains("existing event id did not get assigned",
								expectedEventId,
								IdentifierUtils.ids(events.keySet()));
	}

	/**
	 * adds a single random completion without any control
	 * 
	 * @return
	 */
	private RouteCompletion completion() {
		RouteCompletion completion = RouteCompletionTestUtil.routeCompletion();
		this.completions.add(completion);
		return completion;
	}

}
