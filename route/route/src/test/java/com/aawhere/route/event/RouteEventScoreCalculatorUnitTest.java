/**
 *
 */
package com.aawhere.route.event;

import static com.aawhere.route.event.RouteEventScoreCalculator.*;
import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.test.TestUtil;

import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class RouteEventScoreCalculatorUnitTest {

	private Integer numberOfParticipants;
	private Integer routeScore;
	private DateTime startTime;
	private Range<Integer> expectedScore;
	private boolean tested;
	private Integer expectedNumberOfWeeks;
	private final Integer adjustForDaylightSavings = 1;

	@Before
	public void setUp() {
		numberOfParticipants = PARTICIPANTS_MIN_TO_INFLUENCE;
		routeScore = ROUTE_SCORE_DEFAULT;
		startTime = DateTime.now().minusDays(1);
		this.expectedScore = Range.atLeast(WEEKS_TO_NON_SIGNIFICANT);
		this.expectedNumberOfWeeks = 0;
	}

	@After
	public void test() {
		if (!this.tested) {
			this.tested = true;
			RouteEventScoreCalculator calculator = RouteEventScoreCalculator.create()
					.numberOfParticipants(numberOfParticipants).routeScore(routeScore).startTime(startTime).build();
			TestUtil.assertRange(expectedScore, calculator.routeEventScore());
			assertEquals("incorrect number of weeks", this.expectedNumberOfWeeks, calculator.getWeeksSinceEvent());
		}
	}

	/** Default setup is minimum everything with maximum date. */
	@Test
	public void testDefaultSetup() {
	}

	@Test
	public void testParticipantsNotEnough() {
		this.numberOfParticipants = PARTICIPANTS_MIN_TO_INFLUENCE - 1;
		this.expectedScore = Range.singleton(0);
		// skips any calculations
		this.expectedNumberOfWeeks = null;
	}

	@Test
	public void testStartTime1MonthAgo() {
		this.startTime = DateTime.now().minusWeeks(this.expectedNumberOfWeeks = 4).minusHours(adjustForDaylightSavings);
	}

}
