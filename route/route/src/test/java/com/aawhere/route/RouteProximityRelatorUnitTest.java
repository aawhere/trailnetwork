/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.measure.quantity.Length;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityProvider;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ActivityUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Trackpoint;
import com.aawhere.trailhead.TrailheadUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

/**
 * Tests {@link RouteProximityRelator} using simple lines to provide starts at a known distance away
 * from a TARGET.
 * 
 * @author aroller
 * 
 */
public class RouteProximityRelatorUnitTest {

	static final Integer HIGH_SCORE = 5;
	static final Integer MIDDLE_SCORE = 3;
	static final Integer LOW_SCORE = 1;
	private Length maxRadiusOfArea = TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS;
	private ArrayList<Activity> activities = new ArrayList<>();
	private HashSet<Route> routes = new HashSet<>();
	private HashSet<Route> expectedRelated = new HashSet<>();
	private HashSet<Route> expectedUnrelated = new HashSet<>();
	private HashSet<Route> routesTargetedTransitively = new HashSet<>();
	private RouteProximityRelator relator;

	public static RouteProximityRelatorUnitTest instance() {
		return new RouteProximityRelatorUnitTest();
	}

	@Test
	public void testOnlyrelated() {

		related(ExampleTracks.LINE, HIGH_SCORE);
		assertChooser();
	}

	@Test
	public void testTwoRoutesSameStartTargetWins() {
		related(LINE, HIGH_SCORE);
		related(LINE_LONGER, LOW_SCORE);
		assertChooser();
	}

	@Test
	public void testTwoRoutesSameStartTargetLoses() {
		related(LINE, LOW_SCORE);
		Route related = related(LINE_LONGER, HIGH_SCORE);
		assertChooser();
		assertEquals("related should win over targetbecause of score", related, this.relator.related().iterator()
				.next());
	}

	@Test
	public void testOnlyOneIsUnrelated() {
		unrelated(LINE, LOW_SCORE);
		related(LINE_BEFORE, HIGH_SCORE);
		assertChooser();
	}

	@Test
	@Ignore("transitive growth is too complex for it's value")
	public void testThirdRelatedTransitively() {

		related(LINE_BEFORE, HIGH_SCORE);
		Route related = related(LINE, HIGH_SCORE);
		transitivelyRelated(RETURN_POINTS, HIGH_SCORE, related);
		// this length is long enough to reach to the end of the line
		this.maxRadiusOfArea = LINE_LENGTH;
		assertChooser();
	}

	/** @see #testThirdRelatedTransitively() */
	@Test
	public void testThirdUnrelated() {

		// this is the winner
		related(LINE, HIGH_SCORE);
		// line before is within range
		related(LINE_BEFORE, MIDDLE_SCORE);
		// the start is two segments away so shouldn't be related.
		unrelated(RETURN_POINTS, LOW_SCORE);
		// this length is too short to reach the end of the line
		this.maxRadiusOfArea = S1_LENGTH;
		assertChooser();
	}

	@Test
	public void testProperScoreSorting() {
		Route lowest = related(LINE, LOW_SCORE);
		Route middleScore = related(LINE_LONGER, MIDDLE_SCORE);
		Route highestScore = related(LINE_RETURN_PAST_START, HIGH_SCORE);
		assertChooser();
		Iterator<Route> iterator = this.relator.related().iterator();
		assertEquals(highestScore, iterator.next());
		assertEquals(middleScore, iterator.next());
		assertEquals(lowest, iterator.next());
	}

	@Test
	public void testXml() {
		testThirdUnrelated();
		JAXBTestUtil<RouteProximityRelator> util = JAXBTestUtil.create(this.relator).adapter(RouteTestUtils.adapters())
				.build();
		util.assertContains(maxRadiusOfArea.toString());
		util.assertContains(HIGH_SCORE.toString());
	}

	/**
	 * 
	 */
	private void assertChooser() {
		relator();
		assertEquals("related", this.expectedRelated, Sets.newHashSet(this.relator.related()));
		assertEquals("unrelated", this.expectedUnrelated, this.relator.unrelated());
		assertNotNull(relator.bestLocation());
		// if there is more than just the TARGET the relations should be recording
		if (this.routes.size() > 1) {
			Multimap<Route, Entry<RouteId, Length>> relations = relator.relations();
			// at minimum the TARGET should have been tested
			Set<Route> routesTargeted = relations.keySet();
			TestUtil.assertContainsAll(this.routesTargetedTransitively, routesTargeted);
		}
	}

	public Route related(Iterable<Trackpoint> track, Integer score) {
		Route route = route(track, score);
		this.expectedRelated.add(route);
		return route;
	}

	/**
	 * Some routes are only included because they are transitively related to another route that is
	 * not the main TARGET, but becomes a secondary TARGET. Provide that secondary TARGET and we'll
	 * verify it happened.
	 */
	public Route
			transitivelyRelated(Iterable<Trackpoint> track, Integer score, Route targetProvidingTransitiveRelation) {
		Route route = related(track, score);
		this.routesTargetedTransitively.add(targetProvidingTransitiveRelation);
		return route;
	}

	public Route unrelated(Iterable<Trackpoint> track, Integer score) {
		Route route = route(track, score);
		this.expectedUnrelated.add(route);
		return route;
	}

	public Route route(Iterable<Trackpoint> track, Integer score) {
		Activity course = ActivityTestUtil.createActivity(track);
		Route route = RouteTestUtils.createRouteWithId(course);
		this.activities.add(course);

		route = Route.mutate(route).score(score).build();
		this.routes.add(route);
		return route;
	}

	protected RouteProximityRelator relator() {
		ActivityProvider activityProvider = activityProvider();

		this.relator = RouteProximityRelator.create().routes(routes).activityProvider(activityProvider)
				.maxRadiusOfArea(this.maxRadiusOfArea).build();

		return relator;
	}

	/**
	 * @return
	 */
	ActivityProvider activityProvider() {
		return ActivityUtil.activityProvider(this.activities);
	}
}
