/**
 *
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.person.pref.PrefTestUtil;
import com.aawhere.route.RouteDescriptionPersonalizer.RouteDescription;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.aawhere.xml.bind.OptionalXmlAdapter.Visibility;

/**
 * @author Brian Chapman
 * 
 */
public class RouteDescriptionUnitTest {

	@Test
	public void testRouteDescriptionPersonalizer() {
		Route route = RouteTestUtils.createRouteWithMainId();

		RouteDescriptionPersonalizer routeDescriptionPersonalizer = createRouteDescriptionPersonalizer();
		routeDescriptionPersonalizer.personalize(route);

		RouteDescription routeDescription = routeDescriptionPersonalizer.getResult();
		assertNotNull(routeDescription);
		assertNotNull(routeDescription.completionStats);
		assertNotNull(routeDescription.trailheadName);
		assertNotNull(routeDescription.activityTypes);

		assertJaxB(routeDescription);
	}

	@Test
	public void testRouteDescriptionXmlAdapter() throws Exception {
		RouteDescriptionXmlAdapter adapter = new RouteDescriptionXmlAdapter(createRouteDescriptionPersonalizer(),
				Visibility.SHOW);
		Route route = RouteTestUtils.createRouteWithId();
		RouteDescription rd = adapter.marshal(route);
		assertNotNull(rd);
		// FIXME: toLowerCase should be coming from ActivityType
		TestUtil.assertContains(rd.activityTypes, route.completionStats().activityTypeStats().first().category()
				.getValue().toLowerCase());
	}

	@Test
	public void testRouteDesriptionXmlAdapterNotShown() throws Exception {
		RouteDescriptionXmlAdapter adapter = new RouteDescriptionXmlAdapter(createRouteDescriptionPersonalizer(),
				Visibility.HIDE);
		assertNull("description should be hidden", adapter.marshal(RouteTestUtils.createRouteWithId()));
	}

	@Test
	public void testDefaultRouteDescriptionXmlAdapter() throws Exception {
		RouteDescriptionXmlAdapter adapter = new RouteDescriptionXmlAdapter();
		Route route = RouteTestUtils.createRouteWithId();
		RouteDescription rd = adapter.marshal(route);
		assertNull(rd);
	}

	@Test
	public void testRelativeStats() {
		RouteDescriptionPersonalizer personalizer = createRouteDescriptionPersonalizer();
		// multiple count only since completions are always multiple
		Integer count = 2;
		PersonId personId = null;
		RelativeStatistic relativeStat = RelativeStatistic.create().personId(personId).activityType(ActivityType.BIKE)
				.count(count).featureId(RouteTestUtils.routeId()).build();
		Route route = RouteTestUtils.builder().addRelativeStat(relativeStat).build();
		personalizer.personalize(route);
		RouteDescription result = personalizer.getResult();
		TestUtil.assertSize(1, result.relativeStats);
		String formattedMessage = result.relativeStats.iterator().next();
		TestUtil.assertContains(formattedMessage, count.toString());

	}

	private void assertJaxB(RouteDescription sample) {
		JAXBTestUtil<RouteDescription> util = JAXBTestUtil.create(sample).build();
		String xml = util.getXml();
		assertNotNull(xml);
		assertEquals(util.getExpected().completionStats, util.getActual().completionStats);
		assertEquals(util.getExpected().trailheadName, util.getActual().trailheadName);
	}

	private RouteDescriptionPersonalizer createRouteDescriptionPersonalizer() {
		Configuration prefs = PrefTestUtil.createSiPreferences();

		return new RouteDescriptionPersonalizer.Builder().setPreferences(prefs)
				.trailheadProvider(new RouteTestUtils.RouteTrailheadTestProvider())
				.personProvider(PersonTestUtil.instance()).build();
	}
}
