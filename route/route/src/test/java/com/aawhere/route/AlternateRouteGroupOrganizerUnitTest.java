/**
 * 
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.activity.spatial.ActivitySpatialRelation;
import com.aawhere.activity.spatial.ActivitySpatialRelationId;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.google.appengine.repackaged.com.google.common.collect.Iterables;
import com.google.common.base.Functions;
import com.google.common.collect.Sets;

/**
 * Tests the logic for {@link AlternateRouteGroupOrganizer} to distribute routes based on
 * similarity.
 * 
 * @author aroller
 * 
 */
public class AlternateRouteGroupOrganizerUnitTest {

	private Ratio qualifyingOverlap = MeasurementUtil.ratio(0.85);
	private Ratio minOverlap = MeasurementUtil.ratio(0.75);
	private Ratio disqualifyingOverlap = MeasurementUtil.ratio(0.65);
	private Route m1 = RouteUnitTest.getInstance(100).getEntitySampleWithId();
	private Route a1m1 = RouteUnitTest.getInstance(50).getEntitySampleWithId();
	private Route a1m2 = RouteUnitTest.getInstance(45).getEntitySampleWithId();
	private Route m2 = RouteUnitTest.getInstance(50).getEntitySampleWithId();
	private Route m3 = RouteUnitTest.getInstance(40).getEntitySampleWithId();

	private Map<ActivitySpatialRelationId, ActivitySpatialRelation> relations;
	private Routes.Builder routes = Routes.create();
	private ArrayList<Routes> expectedGroups = new ArrayList<>();

	@Before
	public void setUp() {
		relations = new HashMap<ActivitySpatialRelationId, ActivitySpatialRelation>();

		relation("1st alternate expected", m1, a1m1, qualifyingOverlap, qualifyingOverlap);
		relation("2nd alternate expected", m1, a1m2, qualifyingOverlap, qualifyingOverlap);
		relation("both alternates", a1m1, a1m2, qualifyingOverlap, qualifyingOverlap);
		relation("both mains", m1, m2, disqualifyingOverlap, disqualifyingOverlap);
		relation("qualifying, but m1 beats m2", m2, a1m1, qualifyingOverlap, qualifyingOverlap);
		relation("m3 qualifies for m1 in 1 direction only", m1, m3, qualifyingOverlap, disqualifyingOverlap);
		relation("m3 qualifies for m2 in inverse only", m2, m3, disqualifyingOverlap, qualifyingOverlap);

	}

	/**
	 * @param courseIds
	 * @param attemptIds
	 * @param overlap
	 * @return
	 */
	private void relation(String description, Route course, Route attempt, Ratio overlap, Ratio inverseOverlap) {
		ActivityReferenceIds courseIds = ids(course.courseId());
		ActivityReferenceIds attemptIds = ids(attempt.courseId());
		put(courseIds, attemptIds, overlap);
		put(attemptIds, courseIds, inverseOverlap);
	}

	/**
	 * @param courseIds
	 * @param attemptIds
	 * @param overlap
	 * @return
	 */
	private ActivitySpatialRelation put(ActivityReferenceIds courseIds, ActivityReferenceIds attemptIds, Ratio overlap) {
		final ActivitySpatialRelation relation = ActivitySpatialRelation.create().course(courseIds).attempt(attemptIds)
				.overlap(overlap).build();
		this.relations.put(relation.id(), relation);
		return relation;
	}

	/**
	 * @return
	 */
	private ActivityReferenceIds ids(ActivityId activityId) {
		return ActivityReferenceIds.create().setActivityId(activityId).setAccountId(null).build();
	}

	@Test
	public void testOneMain() {
		expected(m1);
	}

	@Test
	public void testOneMainWithOneAlternate() {
		expected(m1, a1m1);
	}

	@Test
	public void testOneMainWithTwoAlternates() {
		expected(m1, a1m1, a1m2);
	}

	@Test
	public void testTwoMains() {
		expected(m1);
		expected(m2);
	}

	@Test
	public void testTwoMainsWithOneAlternateForFirst() {
		expected(m1, a1m1);
		expected(m2);
	}

	@Test
	public void testThreeMains() {
		expected(m1);
		expected(m2);
		expected(m3);
	}

	public void expected(Route main, Route... alternates) {
		routes.add(main);
		routes.add(alternates);
		expectedGroups.add(Routes.create().add(main).add(alternates).build());
	}

	/**
	 * Runs the route organizer and asserts the expected routes are assigned.
	 * 
	 * @param routes
	 * @param expected
	 */
	@After
	public void assertRoutes() {
		// TODO:test scores are being calculated properly
		AlternateRouteGroupOrganizer organizer = new AlternateRouteGroupOrganizer.Builder()
				.minNumberOfCoursesToScore(2).routes(routes.sort().build()).minOverlapForAlternate(minOverlap)
				.spatialProvider(Functions.forMap(relations)).build();
		assertEquals(expectedGroups, organizer.groups());
		List<Routes> groups = organizer.groups();
		for (Routes group : groups) {
			// ensure the highest scorer is first for testing main
			Iterator<Route> iterator = Routes.create().addAll(group).sort().build().iterator();

			Route main = iterator.next();
			RouteId mainId = main.id();
			String message = "mainId = " + mainId + " with group " + group.toString();
			assertTrue(message, main.isMain());
			assertEquals(message, mainId, main.mainId());
			while (iterator.hasNext()) {
				Route alternate = iterator.next();
				assertEquals(message, mainId, alternate.mainId());
				assertFalse(message + " alternate = " + alternate.mainId(), alternate.isMain());
				assertTrue(message, alternate.score() < main.score());
			}
		}
		Set<Route> allRoutes = Sets.newHashSet(Iterables.concat(expectedGroups));
		assertEquals(allRoutes, organizer.mutated());
	}
}
