/**
 *
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Test;

import com.aawhere.collections.ListUtilsExtended;
import com.aawhere.persist.BaseEntitiesBaseUnitTest;
import com.aawhere.personalize.xml.XmlAdapterTestUtils;

/**
 * @author brian
 * 
 */
public class RoutesUnitTest
		extends BaseEntitiesBaseUnitTest<Route, Routes, Routes.Builder> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#createEntitySampleWithId()
	 */
	@Override
	public Route createEntitySampleWithId() {
		return RouteUnitTest.getInstance().getEntitySampleWithId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return RouteTestUtils.adapters();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getClassesToBeBound()
	 */
	@Override
	protected Class<?>[] getClassesToBeBound() {
		Class<?>[] classes = { Routes.class, Route.class, MapKey.class };
		return classes;
	}

	@Test
	public void testSort() {
		Route higher = RouteTestUtils.createRoute();
		Route lower = Route.clone(higher).score(higher.getScore() - 1).build();
		Routes routes = Routes.create().add(lower).add(higher).sort().build();
		assertEquals(higher.getScore(), ListUtilsExtended.firstOrNull(routes.getCollection()).getScore());
	}

}
