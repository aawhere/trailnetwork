/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.route.RouteProximityRelatorUnitTest.*;
import static com.aawhere.track.ExampleTracks.*;
import static junit.framework.Assert.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.measure.quantity.Length;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.test.TestUtil;
import com.aawhere.track.Track;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadUtil;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * @see RouteTrailheadChooser
 * 
 * @author aroller
 * 
 */
public class RouteTrailheadChooserUnitTest {

	/**
	 * 
	 */
	public static final GeoCoordinate P1_NEAR_COORD = GeoMath.coordinateFrom(	P1.getLocation(),
																				MeasurementUtil.EAST,
																				TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS);
	public static final GeoCoordinate P1_NOT_NEAR_COORD = GeoMath
			.coordinateFrom(P1.getLocation(),
							MeasurementUtil.EAST,
							QuantityMath.create(TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS)
									.plus(MeasurementUtil.createLengthInMeters(1)).getQuantity());
	/** Line that starts on the edge of p1's radius */
	public static final Trackpoint P1_NEAR = TrackUtil.createPoint(T1, P1_NEAR_COORD.getLatitude()
			.getInDecimalDegrees(), P1_NEAR_COORD.getLongitude().getInDecimalDegrees(), E1);
	/** Line that start just outside p1's radius */
	public static final Trackpoint P1_NOT_NEAR = TrackUtil.createPoint(T1, P1_NOT_NEAR_COORD.getLatitude()
			.getInDecimalDegrees(), P1_NOT_NEAR_COORD.getLongitude().getInDecimalDegrees(), E1);

	private List<Trackpoint> NEAR_LINE = Lists.newArrayList(P1_NEAR, P2, P3);
	private List<Trackpoint> NOT_NEAR_LINE = Lists.newArrayList(P1_NOT_NEAR, P2, P3);
	private RouteProximityRelatorUnitTest relatorUnitTest;
	private Length maxTrailheadRadius;
	private BoundingBox searchArea;
	private LinkedList<Trailhead> trailheads;
	private RouteTrailheadChooser chooser;
	private LinkedList<Route> routes;

	@Before
	public void setUp() {
		this.relatorUnitTest = RouteProximityRelatorUnitTest.instance();
		this.maxTrailheadRadius = TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS;
		// this search area just happens to be centered at P1. go figure.
		this.searchArea = BoundingBox.create().setDefault().build();
		this.trailheads = new LinkedList<>();
		this.routes = new LinkedList<>();
	}

	@Test
	public void testNoRoutesNoTrailheads() {
		assertChooser();
		TestUtil.assertEmpty(this.chooser.mergeCandidates().keys());
		TestUtil.assertEmpty(this.chooser.trailheadRelations().entrySet());
		TestUtil.assertEmpty(this.chooser.outOfBounds().entrySet());
	}

	@Test
	public void testOneRouteNoTrailheads() {
		Track track = LINE;
		Integer score = HIGH_SCORE;
		Route solo = route(track, score);
		assertChooser();
		TestUtil.assertSize(1, this.chooser.trailheadRelations().entrySet());
		Trailhead trailhead = this.chooser.trailheadRelations().keySet().iterator().next();
		MeasureTestUtil.assertEquals(P1.getLocation(), trailhead.location());
		Routes relations = this.chooser.trailheadRelations().get(trailhead);

		assertEquals("only route not chosen", solo, Iterables.getFirst(relations, null));
		TestUtil.assertEmpty(this.chooser.mergeCandidates().entries());
		TestUtil.assertEmpty(this.chooser.outOfBounds().entrySet());
	}

	@Test
	public void testOneRouteOneTrailhead() {
		GeoCoordinate trailheadLocation = P1.getLocation();
		Trailhead trailhead = trailhead(trailheadLocation);
		Route related = route(LINE, HIGH_SCORE);
		assertChooser();
		Map<Trailhead, Routes> trailheadRelations = this.chooser.trailheadRelations();
		assertSame("the given trailhead should be chosen, not created", trailhead, trailheadRelations.keySet()
				.iterator().next());
		Routes relatedRoutes = trailheadRelations.get(trailhead);
		assertNotNull(relatedRoutes);
		assertEquals(related, Iterables.getFirst(relatedRoutes, null));
	}

	/**
	 * When the route starts on the edge of the search area that route should be placed in out of
	 * bounds.
	 */
	@Test
	public void testOneRouteOutOfBoundsSmallSearchArea() {
		Track track = LINE;
		route(track, HIGH_SCORE);
		this.searchArea = TrackUtil.boundingBox(Lists.newArrayList(track));
		assertChooser();
		TestUtil.assertSize(1, this.chooser.outOfBounds().entrySet());
		TestUtil.assertEmpty(this.chooser.trailheadRelations().entrySet());
	}

	@Test
	public void testTwoRoutesTwoTrailheadsOneMerge() {
		Route best = route(LINE, HIGH_SCORE);
		Route good = route(NEAR_LINE, MIDDLE_SCORE);
		Trailhead trailhead = trailhead(P1.getLocation());
		Trailhead trailheadToRemove = trailhead(P1_NEAR.getLocation());

		assertChooser();
		Routes related = this.chooser.trailheadRelations().get(trailhead);
		assertNotNull("relator not found for " + trailhead + " in " + this.chooser.trailheadRelations(), related);
		assertEquals(best, Iterables.getFirst(related, null));
		Iterator<Route> iterator = related.iterator();
		assertEquals(best, iterator.next());
		assertEquals(good, iterator.next());
		assertFalse(iterator.hasNext());

		TestUtil.assertContains(trailheadToRemove.id(), chooser.mergeCandidates().get(trailhead.id()));
	}

	/**
	 * Given the best line and associated trailhead, then another line just outside the first
	 * trailhead's zone, this will show that both trailheads remain and will each be associated to
	 * it's route. A third trailhead is added to prove that a route that is within range of two
	 * trailheads will always go to the trailhead with the route that has the higher score.
	 */
	@Test
	public void testTwoRoutesTwoTrailheadsNoMerge() {
		Route best = route(LINE, HIGH_SCORE);
		Route between = route(NEAR_LINE, LOW_SCORE);
		Route good = route(NOT_NEAR_LINE, MIDDLE_SCORE);
		Trailhead bestTrailhead = trailhead(P1.getLocation());
		Trailhead goodTrailhead = trailhead(P1_NOT_NEAR_COORD);
		assertChooser();
		TestUtil.assertSize(2, this.chooser.trailheadRelations().keySet());
		Routes bestRelator = this.chooser.trailheadRelations().get(bestTrailhead);
		TestUtil.assertSize(2, bestRelator);
		assertEquals(best, Iterables.getFirst(bestRelator, null));
		TestUtil.assertContains(between, bestRelator);
		// now confirm the second group was created
		Routes goodRelator = this.chooser.trailheadRelations().get(goodTrailhead);
		TestUtil.assertSize(1, goodRelator);
		assertEquals(good, Iterables.getFirst(goodRelator, null));
	}

	/**
	 * Exposed by an actual problem where a trailhead that already has an id of another will not
	 * receive it's new assignment to an appropriate trailhead.
	 */
	@Test
	public void testPreviouslyAssignedToTrailheadTooFarAway() {
		Route expected = route(LINE, HIGH_SCORE);
		Trailhead farAway = trailhead(P1_NOT_NEAR_COORD);
		expected = Route.mutate(expected).start(farAway.id()).build();
		assertChooser();
		Routes farAwayRoutes = this.chooser.trailheadRelations().get(farAway);
		assertNull("far away trailhead shouldn't have any routes associated", farAwayRoutes);
		TestUtil.assertContains(farAway.id(), chooser.getOrphanTrailheads());

		// now, make sure the route got it's new trailhead
		Set<Entry<Trailhead, Routes>> entrySet = chooser.trailheadRelations().entrySet();
		TestUtil.assertSize(1, entrySet);
		Entry<Trailhead, Routes> associatedRoutes = entrySet.iterator().next();
		Trailhead createdTrailhead = associatedRoutes.getKey();
		assertNotNull(createdTrailhead);
		Routes routesAssociated = associatedRoutes.getValue();
		TestUtil.assertSize(1, routesAssociated);
		Route actual = routesAssociated.all().iterator().next();
		assertEquals(expected, actual);
		// route start reference not yet updated.
	}

	/**
	 * @param trailheadLocation
	 * @return
	 */
	private Trailhead trailhead(GeoCoordinate trailheadLocation) {
		Trailhead trailhead = Trailhead.create().location(trailheadLocation).build();
		this.trailheads.add(trailhead);
		return trailhead;
	}

	/**
	 * @param track
	 * @param score
	 * @return
	 */
	private Route route(Iterable<Trackpoint> track, Integer score) {
		Route related = this.relatorUnitTest.route(track, score);
		this.routes.add(related);
		return related;
	}

	public RouteTrailheadChooser assertChooser() {

		this.chooser = RouteTrailheadChooser.create().activityProvider(this.relatorUnitTest.activityProvider())
				.maxTrailheadRadius(this.maxTrailheadRadius).searchArea(this.searchArea).trailheads(this.trailheads)
				.routes(routes).build();
		assertNotNull(chooser.trailheadRelations());
		assertNotNull(chooser.mergeCandidates());
		assertNotNull(chooser.outOfBounds());
		return this.chooser;
	}
}
