/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.route.RouteTestUtils.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.test.TestUtil;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadTestUtil;
import com.aawhere.util.rb.StringMessage;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;

/**
 * @author aroller
 * 
 */
public class RouteUtilUnitTest {

	@Test
	public void testHighScoreComparator() {
		Route smaller = RouteTestUtils.createRouteWithId();
		Integer minimumScore = smaller.score();
		Route bigger = higherScoringRoute(minimumScore);

		ImmutableList<Route> bigToSmall = ImmutableList.of(bigger, smaller);
		assertSorting(bigger, bigToSmall);
		ImmutableList<Route> smallToBig = ImmutableList.of(smaller, bigger);
		assertSorting(bigger, smallToBig);

		// same score, ties go to course completion counts, then completion counts
		// nulls must be ignored
		RouteStats more = RouteStats.routeStatsCreator().total(2).build();
		RouteStats less = RouteStats.routeStatsCreator().total(1).build();
		// course completion stats decide
		{
			// not completion stats are opposite, but ignored because course decides
			Route winner = RouteTestUtils.builder().score(minimumScore).courseCompletionStats(more)
					.completionStats(less).build();
			Route loser = RouteTestUtils.builder().score(minimumScore).courseCompletionStats(less)
					.completionStats(more).build();
			assertSorting(winner, ImmutableList.of(loser, winner));
			assertSorting(winner, ImmutableList.of(winner, loser));
		}

		// completion stats decide because course completion does not
		{
			Route winner = RouteTestUtils.builder().score(minimumScore).courseCompletionStats(less)
					.completionStats(more).build();
			Route loser = RouteTestUtils.builder().score(minimumScore).courseCompletionStats(less)
					.completionStats(less).build();
			assertSorting(winner, ImmutableList.of(loser, winner));
			assertSorting(winner, ImmutableList.of(winner, loser));
		}

		// ensure nulls don't cause a problem
		// course completion is null so completion stats should decide
		{
			Route winner = RouteTestUtils.builder().score(minimumScore).courseCompletionStats(null)
					.completionStats(more).build();
			Route loser = RouteTestUtils.builder().score(minimumScore).courseCompletionStats(null)
					.completionStats(less).build();
			assertSorting(winner, ImmutableList.of(loser, winner));
			assertSorting(winner, ImmutableList.of(winner, loser));
		}
	}

	/**
	 * @param bigger
	 * @param routes
	 */
	private void assertSorting(Route bigger, Iterable<Route> routes) {
		Comparator<? super Route> highScoreComparator = RouteUtil.highScoreComparator();
		List<Route> sortedCopy = Ordering.from(highScoreComparator).sortedCopy(routes);
		assertSame("Biggest not found first " + sortedCopy, bigger, Iterables.getFirst(sortedCopy, null));
	}

	@Test
	public void testFilterRouteIdConditions() {
		RouteId expected = RouteTestUtils.routeId();
		RouteId expectedFromString = RouteTestUtils.routeId();
		RouteId expectedFromLong = RouteTestUtils.routeId();
		// test using the id, string and long values
		Filter filter = RouteDatastoreFilterBuilder
				.create()
				.idEqualTo(expected)
				.addCondition(FilterCondition.create().field(RouteField.Key.ID).operator(FilterOperator.EQUALS)
						.value(expectedFromString.toString()).build())
				.addCondition(FilterCondition.create().field(RouteField.Key.ID).operator(FilterOperator.EQUALS)
						.value(expectedFromLong.getValue()).build())

				.build();
		Set<RouteId> routeIds = RouteUtil.routeIds(filter);
		TestUtil.assertContains(expected, routeIds);
		TestUtil.assertContains(expectedFromString, routeIds);
		TestUtil.assertContains(expectedFromLong, routeIds);

	}

	@Test
	public void testRouteShell() {
		Activity course = ActivityTestUtil.createActivity();
		Trailhead start = TrailheadTestUtil.createTrailhead(course.getTrackSummary().getStart().getLocation());
		Trailhead finish = TrailheadTestUtil.createTrailhead(course.getTrackSummary().getStart().getLocation());
		Route route = Route.create().course(course).start(start).finish(finish).build();
		assertNull("not persisted soo....", route.id());
		assertEquals(start.id(), route.start());
		assertEquals(finish.id(), route.finish());
		assertEquals(course.boundingBox(), route.boundingBox());
		// assertEquals("the course should have made the only entry in the summary", 1,
		// route.coursesSummary().total()
		// .intValue());
	}

	@Test
	public void testRouteCourseIdMap() {
		Routes routes = RouteTestUtils.createRoutesWithIds();
		BiMap<RouteId, ActivityId> routeIdMap = RouteUtil.courseIdsRouteIdMap(routes);
		TestUtil.assertContainsAll(IdentifierUtils.ids(routes), routeIdMap.keySet());
		TestUtil.assertContainsAll(RouteUtil.courseIds(routes), routeIdMap.values());
	}

	@Test
	public void testRouteNameIsEmpty() {
		assertTrue(RouteUtil.isNameEmpty(null));
		assertFalse(RouteUtil.isNameEmpty(NAME));
		assertTrue(RouteUtil.isNameEmpty(RouteMessage.UNKNOWN));
		assertTrue(RouteUtil.isNameEmpty(new StringMessage(StringUtils.EMPTY)));
	}

	/**
	 * TN-712 Routes may have duplicate courses until they are updated.
	 */
	@Test
	public void testRouteCourseIdMapDuplicateCourseId() {
		Activity activity = ActivityTestUtil.createActivity();
		Route route = RouteTestUtils.createRouteWithId(activity);
		Route routeWithDuplicate = RouteTestUtils.createRouteWithId(activity);
		ArrayList<Route> list = Lists.newArrayList(route, routeWithDuplicate);
		// previously this threw an IllegalArgumentException from BiMap enforcing no duplicates.
		BiMap<RouteId, ActivityId> courseIdsRouteIdMap = RouteUtil.courseIdsRouteIdMap(list);
		TestUtil.assertSize(1, courseIdsRouteIdMap.values());

	}
}
