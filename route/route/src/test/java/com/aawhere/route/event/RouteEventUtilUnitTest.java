/**
 * 
 */
package com.aawhere.route.event;

import static com.aawhere.route.event.RouteEventUtil.*;
import static org.junit.Assert.*;

import org.joda.time.Interval;
import org.junit.Test;

import com.aawhere.joda.time.IntervalUtil;
import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.route.RouteId;
import com.aawhere.route.RouteTestUtils;

/**
 * @author aroller
 * 
 */
public class RouteEventUtilUnitTest {

	@Test
	public void testRouteEventIdFromInterval() {
		RouteId routeId = RouteTestUtils.routeId();
		// first interval is too specific..it will be generalized later
		Interval interval = JodaTestUtil.interval();
		RouteEventId routeEventId = routeEventId(routeId, interval);
		// this is a more general interval that can be reproduced
		RouteEventId routeEventId2 = routeEventId(routeId, interval);
		assertEquals("interval produce from id creates a different id", routeEventId, routeEventId2);
		assertEquals(	"unexpected number of digits for " + routeEventId,
						RouteEventUtil.ID_TIME_PATTERN.length(),
						routeEventId.getValue().length());
	}

	@Test
	public void testExpandIntervalSameInterval() {
		RouteEvent event = RouteEventTestUtil.event();
		RouteEvent expanded = expanded(event, event.interval());
		assertSame("given the same interval the original event should be returned unchanged", event, expanded);
	}

	@Test
	public void testExpandedUpperBounds() {
		RouteEvent event = RouteEventTestUtil.event();
		Interval interval = IntervalUtil.plus(event.interval(), event.interval().toDuration());
		RouteEvent expanded = expanded(event, interval);
		assertNotSame(event, expanded);
		JodaTestUtil.assertInterval(expanded.interval()).contains(event.interval()).contains(interval);
	}

	@Test
	public void testExpandedLowerBounds() {
		RouteEvent event = RouteEventTestUtil.event();
		Interval interval = IntervalUtil.minus(event.interval(), event.interval().toDuration());
		RouteEvent expanded = expanded(event, interval);
		assertNotSame(event, expanded);
		JodaTestUtil.assertInterval(expanded.interval()).contains(event.interval()).contains(interval);
	}

}
