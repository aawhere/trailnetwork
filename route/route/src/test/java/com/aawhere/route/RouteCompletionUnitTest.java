/**
 *
 */
package com.aawhere.route;

import static com.aawhere.test.TestUtil.*;
import static org.junit.Assert.*;

import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.activity.timing.ActivityTimingTestUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.If;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.person.Person;
import com.aawhere.person.PersonField;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.route.RouteCompletion.Builder;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.google.common.base.Functions;

/**
 * @author aroller
 * 
 */
public class RouteCompletionUnitTest
		extends BaseEntityJaxbBaseUnitTest<RouteCompletion, Builder, RouteCompletionId, RouteCompletionRepository> {

	protected Route route;
	private Integer repeatCount;
	private Integer repeatTotal;
	private Activity activity;
	private Person person;
	private PersonId personId;
	private Completion completion;
	private Integer coursesCompletedCount;
	private Integer courseActivityCount;
	private boolean isCourse;
	private Integer routeScore;

	/**
	 *
	 */
	public RouteCompletionUnitTest() {
		super(NON_MUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		this.route = RouteTestUtils.createRouteWithId();
		this.completion = ActivityTimingTestUtil.createWithOneCompletion().getCompletions().get(0);
		this.activity = ActivityTestUtil.createActivity(this.completion.getTiming().attempt().getActivityId());
		this.repeatCount = 1;
		this.repeatTotal = 1;
		this.person = PersonTestUtil.personWithId();
		this.personId = this.person.id();
		this.courseActivityCount = TestUtil.generateRandomInt();
		this.courseActivityCount = TestUtil.generateRandomInt();
		this.isCourse = false;
		this.routeScore = TestUtil.generateRandomInt();

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#getId()
	 */
	@Override
	protected RouteCompletionId getId() {
		return new RouteCompletionId(RouteTestUtils.routeId(), ActivityTestUtil.createApplicationActivityId());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.completion(completion);
		builder.activity(activity);
		builder.person(person);
		builder.repeatCount(repeatCount);
		builder.repeatTotal(repeatTotal);
		builder.route(route);
		builder.person(this.personId);
		builder.isCourse(this.isCourse);
		builder.routeScore(routeScore);
		// only courses get courseActivityCount
		if (this.isCourse) {
			builder.courseActivityCount(courseActivityCount);
		} else {
			this.courseActivityCount = null;
		}
		builder.coursesCompletedCount(coursesCompletedCount);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertCreation(RouteCompletion sample) {
		super.assertCreation(sample);

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertSample(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertSample(RouteCompletion sample) {
		super.assertSample(sample);
		assertEquals(this.route.id(), sample.getId().parent());

		// FIXME:The unit tests need to test better once building is stablized.

		assertEquals(this.activity.getActivityType(), sample.activityTypeReported());
		TestUtil.assertNullExplanation("activity type is provided by organizer", sample.activityType());

		assertNotNull(sample.beyondDistance());
		assertNotNull(sample.beyondDuration());
		// assertNotNull(sample.socialRank());
		// assertNotNull(sample.personalAttemptOrder());
		// assertNotNull(sample.personalRank());
		assertNotNull(sample.personId());
		assertNotNull(sample.repeatCount());
		assertNotNull(sample.repeatTotal());
		assertEquals(this.coursesCompletedCount, sample.coursesCompletedCount());
		assertEquals(this.isCourse, sample.isCourse());
		assertEquals(this.courseActivityCount, sample.courseActivityCount());
		assertEquals("route score", this.routeScore, sample.routeScore());
		assertEquals("start trailhead", route.start(), sample.startTrailheadId());
	}

	@Test
	public void testIdSecondLap() {
		Integer iteration = 2;
		assertId(iteration);
	}

	@Test
	public void testIdFirstLap() {
		Integer iteration = 1;
		assertId(iteration);
	}

	@Test
	public void testIdDifferentParents() {
		ActivityId activityId = ActivityTestUtil.createApplicationActivityId();
		RouteId parent1 = RouteTestUtils.routeId();
		RouteId parent2 = RouteTestUtils.routeId();
		RouteCompletionId id1 = new RouteCompletionId(parent1, activityId);
		RouteCompletionId id2 = new RouteCompletionId(parent2, activityId);

		assertNotEquals(id1, id2);
	}

	/**
	 * @param iteration
	 */
	private void assertId(Integer iteration) {
		RouteId routeId = RouteTestUtils.routeId();
		ActivityId activityId = ActivityTestUtil.createApplicationActivityId();
		RouteCompletionId fromObjects = new RouteCompletionId(routeId, activityId, iteration);
		String string = fromObjects.toString();
		RouteCompletionId fromString = new RouteCompletionId(string);
		assertEquals(fromObjects, fromString);
		assertEquals("routeId parent", routeId, fromString.parent());
		assertEquals("activityId", activityId, fromString.activityId());
		assertEquals("iteration", If.nil(iteration).use(RouteCompletion.DEFAULT_REPEAT_COUNT), fromString.iteration());
	}

	@Test
	public void testIdNullIteration() {
		assertId(null);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return RouteTestUtils.adapters();
	}

	public static RouteCompletionUnitTest create() {
		final RouteCompletionUnitTest instance = new RouteCompletionUnitTest();
		instance.baseSetUp();
		instance.setUp();
		return instance;
	}

	@Test
	public void testAdoption() {
		// when a new Route is to be adotopted
		RouteCompletion routeCompletion = getEntitySampleWithId();
		Route newParent = RouteTestUtils.createRouteWithId();
		RouteCompletion adopted = RouteCompletion.adopt(newParent, routeCompletion).build();
		assertEquals(newParent.id(), adopted.routeId());
		TestUtil.assertNotEquals(routeCompletion.id(), adopted.id());
		assertEquals(routeCompletion.dateCreated(), adopted.dateCreated());
		assertEquals(routeCompletion.activityId(), adopted.activityId());
		assertEquals(new RouteCompletionId(newParent.id(), routeCompletion.activityId()), adopted.id());
	}

	@Test
	public void testMergingStore() {

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityJaxbBaseUnitTest#assertJaxb(com.aawhere.xml.bind.JAXBTestUtil)
	 */
	@Override
	protected void assertJaxb(JAXBTestUtil<RouteCompletion> util) {
		super.assertJaxb(util);
		// proves that person xml has been included.
		util.assertContains(PersonField.NAME_WITH_ALIASES);
		util.assertContains(RouteCompletion.FIELD.ACTIVITIES);
		util.assertContains(RouteCompletion.FIELD.APPLICATION_REFERENCE);
		util.assertContains(this.route.id().toString());
		util.assertContains(util.getExpected().getId().getCompositeValue());
	}

	/**
	 * @param repeatCount
	 * @param repeatTotal
	 */
	void repeatCount(Integer repeatCount, Integer repeatTotal) {
		this.repeatCount = repeatCount;
		this.repeatTotal = repeatTotal;
	}

	/**
	 * @param activity2
	 * @return
	 */
	RouteCompletionUnitTest activity(Activity activity2) {
		this.activity = activity2;
		return this;
	}

	@Test
	public void testMostRecentFavoriteForRoute() {
		RouteCompletion completion = getEntitySample();
		final Integer min = RouteCompletion.MIN_TOTAL_FOR_FAVORITE;
		RouteCompletion bareMinimum = RouteCompletion.mutate(completion).personalTotal(min).personalOrder(min).build();
		assertTrue("bare minimum", bareMinimum.isPopularAndMostRecent());
		final Integer belowMin = RouteCompletion.MIN_TOTAL_FOR_FAVORITE - 1;
		assertFalse("below minimum", RouteCompletion.mutate(completion).personalTotal(belowMin).personalOrder(belowMin)
				.build().isPopularAndMostRecent());
		assertFalse("not most recent", RouteCompletion.mutate(completion).personalTotal(min).personalOrder(belowMin)
				.build().isPopularAndMostRecent());
		assertFalse("null", RouteCompletion.mutate(completion).personalTotal(null).personalOrder(null).build()
				.isPopularAndMostRecent());
		assertFalse("route score is default", RouteCompletion.clone(bareMinimum).routeScore(Route.DEFAULT_SCORE)
				.build().isPopularAndMostRecent());
	}

	/**
	 * Merge logic is handled by {@link RouteCompletionMergeUnitTest}. This does basic checks to
	 * make sure persistence is using that merge utility properly.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testMergeSingle() throws EntityNotFoundException {
		RouteCompletion original = getEntitySamplePersisted();
		getRepository().setIsCourse(original.id(), true);
		// get a count guaranteed to be bigger than the persisted.
		Integer courseActivityCount = If.nil(original.courseActivityCount()).use(TestUtil.generateRandomInt());
		courseActivityCount += 1;
		RouteCompletion incoming = RouteCompletion.clone(original).courseActivityCount(courseActivityCount)
				.isCourse(false).build();
		RouteCompletion merged = getRepository().merge(incoming);
		assertEquals(original, merged);
		assertEquals(	"the incoming activity count is bigger so that should have been kept",
						courseActivityCount,
						merged.courseActivityCount());
		assertTrue("original was a course so it should have been kept", merged.isCourse());
	}

	static class RouteCompletionMockRepository
			extends MockFilterRepository<RouteCompletions, RouteCompletion, RouteCompletionId>
			implements RouteCompletionRepository {
		private RouteCompletionRepositoryDelegate delegate = new RouteCompletionRepositoryDelegate(this);

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.route.RouteCompletionRepository#setIsCourse(com.aawhere.route.RouteCompletionId
		 * , java.lang.Boolean)
		 */
		@Override
		public RouteCompletion setIsCourse(RouteCompletionId completionId, Boolean isCourse)
				throws EntityNotFoundException {
			return delegate.setIsCourse(completionId, isCourse);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteCompletionRepository#merge(com.aawhere.route.RouteCompletion)
		 */
		@Override
		public RouteCompletion merge(RouteCompletion replacement) {
			return delegate.merge(replacement);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteCompletionRepository#mergeAll(java.lang.Iterable)
		 */
		@Override
		public Map<RouteCompletionId, RouteCompletion> mergeAll(Iterable<RouteCompletion> all) {
			return delegate.mergeAll(all);
		}

	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected RouteCompletionRepository createRepository() {
		return new RouteCompletionMockRepository();
	}
}
