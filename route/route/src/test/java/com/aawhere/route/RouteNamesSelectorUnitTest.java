/**
 * 
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Map;

import org.junit.After;
import org.junit.Test;

import com.aawhere.activity.ActivityMessage;
import com.aawhere.activity.ActivityNameSelector;
import com.aawhere.activity.ActivityNameSelectorUnitTest;
import com.aawhere.lang.If;
import com.aawhere.route.RouteCompletion.Builder;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventTestUtil;
import com.aawhere.route.event.RouteNameSelector;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.Maps;

/**
 * Tests the function of {@link RouteNameSelector} and how it puts {@link ActivityNameSelector} to
 * work. The logic of proper name selection remains in {@link ActivityNameSelectorUnitTest} while
 * this ensures the proper selection how it relates to {@link RouteCompletion} so the names will be
 * generally easy to find a pattern.
 * 
 * @author aroller
 * 
 */
public class RouteNamesSelectorUnitTest {

	private static final Boolean IS_COURSE = true;
	private static final String UNKNOWN = ActivityMessage.UNKNOWN.toString();
	private static final String COURSE_NAME = "Miwok Loop";
	private static final String EVENT1_NAME = "Headlands Marathon 2011";
	private static final String EVENT2_NAME = "Headlands Marathon 2010";
	private static final String GENERAL_NAME = "Marin Headlands";
	private static final String DEFAULT_NAME = "Sumptin";
	private static final Integer NUMBER_TO_BE_CONFIDENT = 2;
	private static final Integer NUMBER_NOT_CONFIDENT = NUMBER_TO_BE_CONFIDENT - 1;

	private ArrayList<RouteCompletion> completions = new ArrayList<RouteCompletion>();
	private Supplier<? extends Message> defaultNameSupplier = Suppliers.ofInstance(new StringMessage(DEFAULT_NAME));
	private String expectedRouteName;
	private boolean tested = false;
	private RouteNameSelector routeNameSelector;
	private Map<RouteEventId, Message> expectedEventNames;

	/** Ensures the test is run for each test if not already manually run. */
	@After
	public void after() {
		test();
	}

	public RouteNameSelector test() {
		if (!tested) {
			tested = true;
			this.routeNameSelector = RouteNameSelector.create().completions(completions).minScoreForConfident(1)
					.defaultRouteNameSupplier(defaultNameSupplier).build();
			assertEquals("routeName", this.expectedRouteName, routeNameSelector.routeName().toString());

			if (expectedEventNames != null) {
				TestUtil.assertIterablesEquals(	"event names",
												this.expectedEventNames.entrySet(),
												this.routeNameSelector.eventNames().entrySet());
			}
		}
		return routeNameSelector;
	}

	protected Message expectedEventMessage(RouteEventId eventId, String message) {
		if (this.expectedEventNames == null) {
			this.expectedEventNames = Maps.newHashMap();

		}
		final StringMessage result = new StringMessage(message);
		this.expectedEventNames.put(eventId, result);
		return result;
	}

	/**
	 * Essentially the bare minimum route having only a single course. The default name is provided
	 * since there is not a sufficient sample.
	 */
	@Test
	public void testSingleCourse() {
		courseCompletion();
		this.expectedRouteName = DEFAULT_NAME;
	}

	@Test
	public void testEnoughCourses() {
		completion(COURSE_NAME, true, null, NUMBER_TO_BE_CONFIDENT);
		this.expectedRouteName = COURSE_NAME;
	}

	/**
	 * Enough courses should use the course name, unless those courses have an unknown activity
	 * name.
	 * 
	 */
	@Test
	public void testEnoughUnknownCourses() {
		completion(UNKNOWN, true, null, NUMBER_TO_BE_CONFIDENT);
		this.expectedRouteName = DEFAULT_NAME;
	}

	@Test
	public void testNotEnoughCoursesEnoughCompletions() {
		courseCompletion();
		completion(GENERAL_NAME, !IS_COURSE, null, NUMBER_TO_BE_CONFIDENT);
		this.expectedRouteName = GENERAL_NAME;
	}

	@Test
	public void testEnoughCoursesAfterEventRemoval() {
		// course name has enough
		completion(COURSE_NAME, IS_COURSE, null, NUMBER_TO_BE_CONFIDENT);
		// event should be removed
		completion(EVENT1_NAME, IS_COURSE, RouteEventTestUtil.eventId(), NUMBER_TO_BE_CONFIDENT);

		this.expectedRouteName = COURSE_NAME;
	}

	/** Not enough courses by itself so include the event. */
	@Test
	public void testNotEnoughCoursesSoIncludeEvent() {

		completion(COURSE_NAME, IS_COURSE, null, NUMBER_NOT_CONFIDENT);
		final RouteEventId eventId = RouteEventTestUtil.eventId();
		completion(EVENT1_NAME, IS_COURSE, eventId, NUMBER_TO_BE_CONFIDENT);

		this.expectedRouteName = EVENT1_NAME;
		this.expectedEventMessage(eventId, EVENT1_NAME);
	}

	/** Shows the preference towards the more dominant event when including events. */
	@Test
	public void testSecondEventDominatesWhenCourseIsInfufficientByItself() {
		completion(COURSE_NAME, IS_COURSE, null, NUMBER_NOT_CONFIDENT);
		completion(EVENT1_NAME, IS_COURSE, RouteEventTestUtil.eventId(), NUMBER_TO_BE_CONFIDENT);
		completion(EVENT2_NAME, IS_COURSE, RouteEventTestUtil.eventId(), NUMBER_TO_BE_CONFIDENT * 10);
		this.expectedRouteName = EVENT2_NAME;
	}

	/**
	 * 
	 */
	private void courseCompletion() {
		completion(COURSE_NAME, true, null);
	}

	void completion(String name, Boolean isCourse, RouteEventId eventId, Integer count) {
		count = If.nil(count).use(1);
		for (int i = 0; i < count; i++) {
			completion(name, isCourse, eventId);
		}
	}

	RouteCompletion completion(String name, Boolean isCourse, RouteEventId eventId) {

		Builder builder = RouteCompletionTestUtil.create();
		RouteCompletion routeCompletion = builder.isCourse(isCourse).routeEventId(eventId).activityName(name).build();
		this.completions.add(routeCompletion);
		return routeCompletion;
	}
}
