/**
 * 
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupTestUtil;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class RouteRelatorUnitTest {
	/**
	 * 
	 */
	private static final Track ROUTE_TRACK = ExampleTracks.LINE;
	/** acts as the datastore providing routes on demand so any that will be retrieved must be here. */
	private HashSet<Route> routesForSocialGroups;
	private HashSet<Route> alternatesOfTarget;
	private Route targetRoute;
	private List<SocialGroup> socialGroups;

	private List<Route> expectedSameTrailheads;
	private List<Route> expectedAlternates;
	private List<Route> expectedDiverse;
	private List<Route> expectedNearby;

	@Before
	public void setUp() {
		this.routesForSocialGroups = Sets.newHashSet();
		this.socialGroups = Lists.newArrayList();
		this.alternatesOfTarget = Sets.newHashSet();
		this.targetRoute = RouteTestUtils.createRouteWithId(ROUTE_TRACK);
		this.expectedAlternates = Lists.newArrayList();
		this.expectedSameTrailheads = Lists.newArrayList();
		this.expectedNearby = Lists.newArrayList();
		this.expectedDiverse = Lists.newArrayList();
	}

	@After
	public void test() {
		Function<Iterable<RouteId>, Map<RouteId, Route>> idsFunction = IdentifierUtils
				.mapFunction(this.routesForSocialGroups);
		RouteRelator relator = RouteRelator.create().alternatesOfTarget(IdentifierUtils.ids(this.alternatesOfTarget))
				.targetRoute(targetRoute).idsFunction(idsFunction).socialGroups(this.socialGroups).build();
		assertEquals("alternates", this.expectedAlternates, relator.alternates());
		assertEquals("same trailheads", this.expectedSameTrailheads, relator.sameTrailhead());
		assertEquals("diverse", this.expectedDiverse, relator.diverse());
		assertEquals("nearby", this.expectedNearby, relator.nearby());
	}

	@Test
	public void testNothing() {
		// nothing in, nothing out, no null pointers
	}

	@Test
	public void testDiverse() {
		diverseRoute();
	}

	@Test
	public void testSameTrailhead() {
		sameTrailhead();

	}

	@Test
	public void testAlternate() {
		Route alternate = differentTrailhead();
		this.alternatesOfTarget.add(alternate);
		this.expectedAlternates.add(alternate);
	}

	@Test
	public void testThySelfIgnored() {
		add(targetRoute);
		// everything else is empty
	}

	/**
	 * @return
	 */
	private Route sameTrailhead() {
		Route route = RouteTestUtils.createRouteWithId(ROUTE_TRACK);
		this.expectedSameTrailheads.add(route);
		this.expectedNearby.add(route);
		add(route);
		return route;
	}

	/**
	 * @return
	 * 
	 */
	private Route diverseRoute() {
		Route route = differentTrailhead();
		this.expectedDiverse.add(route);
		return route;
	}

	/**
	 * @return
	 */
	private Route differentTrailhead() {
		// go distant to avoid the nearby filter
		Route route = RouteTestUtils.createRouteWithId(ExampleTracks.DISTANT);
		add(route);
		return route;
	}

	/**
	 * @param route
	 * @return
	 */
	private Route add(Route route) {
		this.socialGroups.add(SocialGroupTestUtil.withId().context(route.id()).build());
		this.routesForSocialGroups.add(route);
		return route;
	}
}
