/**
 *
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.person.pref.PrefTestUtil;
import com.aawhere.route.RouteNamePersonalizer.DefaultNameFormat;
import com.aawhere.route.RouteNamePersonalizer.RouteNameXml;
import com.aawhere.test.TestUtil;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadMessage;
import com.aawhere.trailhead.TrailheadTestUtil;
import com.aawhere.util.rb.MessageDisplay;
import com.aawhere.util.rb.StringMessage;

/**
 * @author Brian Chapman
 * 
 */
public class RouteNamePersonalizerUnitTest {

	private RouteNameXml routeNameXml = null;

	@Before
	public void setup() {
		Route route = RouteTestUtils.createRouteWithId();
		routeNameXml = RouteNameXml.create().completionStats(route.completionStats()).distance(route.distance())
				.finishTrailheadId(route.finish()).startTrailheadId(route.start()).name(RouteMessage.UNKNOWN).build();
	}

	@Test
	public void testPersonalizerDifferentStartFinish() {
		RouteNamePersonalizer plizer = routeNamePersonalizerBuilder().build();
		plizer.personalize(routeNameXml);
		MessageDisplay result = plizer.getResult();
		assertDefaultName(result, DefaultNameFormat.START_AND_FINISH_TH);

	}

	@Test
	public void testPersonalizerSameStartFinish() {
		TrailheadId startTrailheadId = new TrailheadId("a");
		TrailheadId finishTrailheadId = new TrailheadId("a");
		RouteNameXml routeSameTh = RouteNameXml.mutate(routeNameXml).startTrailheadId(startTrailheadId)
				.finishTrailheadId(finishTrailheadId).build();
		RouteNamePersonalizer plizer = routeNamePersonalizerBuilder().build();
		plizer.personalize(routeSameTh);
		MessageDisplay result = plizer.getResult();
		assertDefaultName(result, DefaultNameFormat.START_TH);
	}

	@Test
	public void testPersonalizerUnknownStartFinish() {
		RouteNamePersonalizer plizer = routeNamePersonalizerBuilder()
				.routeTrailheadProvider(new UnknownTrailheadTestProvider()).build();
		plizer.personalize(routeNameXml);
		MessageDisplay result = plizer.getResult();
		assertDefaultName(result, DefaultNameFormat.NO_TH);
		// TODO: Make sure "skate" is converted to title case.
		// ActivityType is randomly selected, making this hard.
		// assertTrue(result.value().contains("Skate"));
	}

	@Test
	public void testPersonalizerUseExisting() {
		String routeName = "Mott Gym";
		RouteNameXml routeWithName = RouteNameXml.mutate(routeNameXml).name(new StringMessage(routeName)).build();
		RouteNamePersonalizer plizer = routeNamePersonalizerBuilder().build();
		plizer.personalize(routeWithName);
		MessageDisplay result = plizer.getResult();
		assertNotNull(result);
		// System.out.println(result.value());
		TestUtil.assertContains(result.value(), routeName);
	}

	private void assertDefaultName(MessageDisplay result, DefaultNameFormat format) {
		assertNotNull(result);
		String value = result.value();
		String startId = routeNameXml.startThId().getValue();
		String finishId = routeNameXml.finishThId().getValue();

		if (format.equals(DefaultNameFormat.NO_TH)) {
			TestUtil.assertDoesntContain(value, startId);
			TestUtil.assertDoesntContain(value, finishId);
		} else if (format.equals(DefaultNameFormat.START_AND_FINISH_TH)) {
			TestUtil.assertContains(value, startId);
			TestUtil.assertContains(value, finishId);
		} else if (format.equals(DefaultNameFormat.START_TH)) {
			TestUtil.assertContains(value, startId);
			TestUtil.assertContains(value, finishId);
			TestUtil.assertDoesntContain(value, startId + " to " + finishId);
		}
		TestUtil.assertDoesntContain(value, "{");
	}

	private RouteNamePersonalizer.Builder routeNamePersonalizerBuilder() {
		return RouteNamePersonalizer.create()//
				.setPreferences(PrefTestUtil.createSiPreferences())//
				.routeTrailheadProvider(new IdAsNameRouteTrailheadTestProvider());
	}

	public static class IdAsNameRouteTrailheadTestProvider
			extends TrailheadTestUtil.TrailheadTestProvider
			implements RouteTrailheadProvider {

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.ServiceTestUtilBase#entity(com.aawhere.id.Identifier)
		 */
		@Override
		public Trailhead entity(TrailheadId id) {
			return TrailheadTestUtil.createTrailhead(id, new StringMessage(id.getValue()));
		}
	}

	public static class UnknownTrailheadTestProvider
			extends TrailheadTestUtil.TrailheadTestProvider
			implements RouteTrailheadProvider {

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.ServiceTestUtilBase#entity(com.aawhere.id.Identifier)
		 */
		@Override
		public Trailhead entity(TrailheadId id) {
			return TrailheadTestUtil.createTrailhead(id, TrailheadMessage.UNKNOWN);
		}
	}

}
