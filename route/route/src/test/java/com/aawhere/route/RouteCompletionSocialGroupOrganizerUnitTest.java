/**
 * 
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityType;
import com.aawhere.person.PersonId;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupTestUtil;
import com.aawhere.person.group.SocialGroups;
import com.aawhere.test.TestUtil;

import com.google.common.base.Function;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;

/**
 * Tests the many paths that can happen organizer routes into social groups using
 * {@link RouteCompletionSocialGroupOrganizer}. This is a stateful test that setsup the common
 * scenario, the test allows modification to create a unique scenario, then after is invoked for
 * every tests which executes the test automatically.
 * 
 * @author aroller
 * 
 */
public class RouteCompletionSocialGroupOrganizerUnitTest {

	private RouteCompletionSocialGroupOrganizer organizer;
	private Route routeTargeted;
	private List<RouteCompletion> completions;
	private Set<SocialGroup> existingSocialGroups;
	private Function<Entry<SocialGroup, ? extends Iterable<Entry<PersonId, Integer>>>, SocialGroup> membershipUpdateFunction;
	private Multiset<ActivityType> expectedGroupSizes;

	@Before
	public void setUp() {
		this.routeTargeted = RouteTestUtils.createRouteWithId();
		this.completions = Lists.newArrayList();
		this.existingSocialGroups = Sets.newHashSet();
		this.membershipUpdateFunction = SocialGroupTestUtil.membershipUpdateFunction();
		this.expectedGroupSizes = HashMultiset.create();
	}

	@After
	public void test() {

		this.organizer = RouteCompletionSocialGroupOrganizer.create().routeTargeted(routeTargeted)
				.completions(completions)
				.socialGroupsExisting(SocialGroups.create().addAll(existingSocialGroups).build())
				.membershipUpdateFunctions(membershipUpdateFunction).build();
		TestUtil.assertIterablesEquals(	"existing social groups",
										this.existingSocialGroups,
										organizer.socialGroupsExisting());
		assertEquals(this.routeTargeted, organizer.routeTargeted());
		SocialGroups socialGroupsUpdated = this.organizer.socialGroupsUpdated();
		TestUtil.assertSize(this.expectedGroupSizes.toString(),
							this.expectedGroupSizes.elementSet().size(),
							socialGroupsUpdated);
		for (SocialGroup socialGroup : socialGroupsUpdated) {
			ActivityType activityType = RouteCompletionSocialGroupOrganizer.activityType(socialGroup);
			assertEquals(expectedGroupSizes.count(activityType), socialGroup.membershipCount().intValue());
			assertEquals(routeTargeted.id(), socialGroup.context());
		}
	}

	@Test
	public void testNothing() {
		// ensures the standard setup will work when the minimum completions are found
	}

	@Test
	public void testTwoCompletionsSameActivityType() {
		ActivityType activityType = ActivityType.BIKE;
		completionAdded(activityType);
		completionAdded(activityType);
	}

	@Test
	public void testTwoActivityTypes() {
		completionAdded(ActivityType.BIKE);
		completionAdded(ActivityType.RUN);
	}

	/**
	 * 
	 */
	private void completionAdded(ActivityType activityType) {
		this.completions.add(RouteCompletionTestUtil.with(routeTargeted).activityType(activityType)
				.activityTypeReported(activityType).build());
		this.expectedGroupSizes.add(activityType);
	}
}
