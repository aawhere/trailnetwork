/**
 * 
 */
package com.aawhere.route.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.Interval;

import com.aawhere.activity.ActivityType;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceTestUtilBase;
import com.aawhere.route.Route;
import com.aawhere.route.RouteId;
import com.aawhere.route.RouteProvider;
import com.aawhere.route.RouteTestUtils;
import com.aawhere.route.event.RouteEvent.Builder;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

/**
 * @author aroller
 * 
 */
public class RouteEventTestUtil
		extends ServiceTestUtilBase<RouteEvents, RouteEvent, RouteEventId>
		implements RouteEventProvider {

	private RouteProvider routeProvider = RouteTestUtils.instance();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.event.RouteEventProvider#routeEvent(com.aawhere.route.RouteId)
	 */
	@Override
	public RouteEvents routeEvents(RouteId routeId) {
		return RouteEvents.create().build();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.route.event.RouteEventProvider#routeEvent(com.aawhere.route.event.RouteEventId)
	 */
	@Override
	public RouteEvent routeEvent(RouteEventId id) throws EntityNotFoundException {
		return RouteEventUnitTest.instance(id, null, null, null).getEntitySampleWithId();
	}

	/**
	 * @return
	 */
	public static Set<XmlAdapter<?, ?>> adapters() {
		final Set<XmlAdapter<?, ?>> adapters = RouteTestUtils.adapters();
		adapters.add(new RouteEventXmlAdapter(instance()));
		// addition of statistic adapter causes circular reference infinite loop
		// adapters.add(new RouteEventXmlAdapter.ForStatistic(instance()));
		adapters.add(new RouteEventXmlAdapter.ForCompletion(instance()));
		return adapters;
	}

	/**
	 * @return
	 */
	public static RouteEventProvider instance() {
		return new RouteEventTestUtil();
	}

	public static RouteEventId eventId() {
		return RouteEventUtil.routeEventId(RouteTestUtils.routeId(), JodaTestUtil.interval());
	}

	public static RouteEventId eventId(RouteId routeId) {
		// FIXME: the interval being returned is creating duplicates
		return RouteEventUtil.routeEventId(routeId, JodaTestUtil.interval());
	}

	public static List<RouteEventId> eventIds(RouteId routeId, Integer count) {
		ArrayList<RouteEventId> ids = new ArrayList<>(count);
		for (int i = 0; i < count; i++) {
			ids.add(eventId(routeId));
		}
		return ids;
	}

	public static RouteEvent event() {
		return RouteEventUnitTest.instance(null, null, null, null).getEntitySampleWithId();
	}

	/**
	 * @param interval
	 * @return
	 */
	public static RouteEvent event(Interval interval) {
		return RouteEventUnitTest.instance(null, null, interval, null).getEntitySampleWithId();
	}

	public static RouteEvent event(Route route) {
		return RouteEventUnitTest.instance(null, route, null, null).getEntitySampleWithId();
	}

	public static RouteEvent event(Route route, Interval interval) {
		return RouteEventUnitTest.instance(null, route, interval, null).getEntitySampleWithId();
	}

	/**
	 * @param activityType
	 * @return
	 */
	public static RouteEvent event(Route route, ActivityType activityType) {
		return RouteEventUnitTest.instance(null, route, null, activityType).getEntitySampleWithId();
	}

	/**
	 * @return
	 */
	public static RouteEvent.Builder routeEventBuilder() {
		return new RouteEvent.Builder() {

			private Route route;

			/*
			 * (non-Javadoc)
			 * @see com.aawhere.route.event.RouteEvent.Builder#route(com.aawhere.route.Route)
			 */
			@Override
			public Builder route(Route route) {
				this.route = route;
				return super.route(route);
			}

			/*
			 * (non-Javadoc)
			 * @see com.aawhere.route.event.RouteEvent.Builder#build()
			 */
			@Override
			public RouteEvent build() {

				return RouteEventUnitTest.instance(building.id(), route, building.interval(), building.activityType())
						.getEntitySampleWithId();
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.event.RouteEventProvider#idsFunction()
	 */
	@Override
	public Function<Iterable<RouteEventId>, Map<RouteEventId, RouteEvent>> idsFunction() {
		return new Function<Iterable<RouteEventId>, Map<RouteEventId, RouteEvent>>() {

			@Override
			public Map<RouteEventId, RouteEvent> apply(Iterable<RouteEventId> input) {
				HashMap<RouteEventId, RouteEvent> events = Maps.newHashMap();
				for (RouteEventId routeEventId : input) {
					try {
						events.put(routeEventId, routeEvent(routeEventId));
					} catch (EntityNotFoundException e) {
						throw new RuntimeException(e);
					}
				}
				return events;
			}
		};
	}

	public Function<RouteEventId, RouteEvent> eventFunction() {
		return new Function<RouteEventId, RouteEvent>() {

			@Override
			public RouteEvent apply(RouteEventId input) {
				try {
					return (input != null) ? routeEvent(input) : null;
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceStandard#all(java.lang.Iterable)
	 */
	@Override
	public Map<RouteEventId, RouteEvent> all(Iterable<RouteEventId> ids) {
		Iterable<RouteEvent> events = Iterables.transform(ids, eventFunction());
		return IdentifierUtils.map(events);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceTestUtilBase#entity(com.aawhere.id.Identifier)
	 */
	@Override
	public RouteEvent entity(RouteEventId id) {
		try {
			return routeEvent(id);
		} catch (EntityNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

}
