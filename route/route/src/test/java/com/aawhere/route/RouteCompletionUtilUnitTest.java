/**
 * 
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.ActivityUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.test.TestUtil;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * @see RouteCompletionUtil
 * 
 * @author aroller
 * 
 */
public class RouteCompletionUtilUnitTest {

	@Test
	public void testPreferredOrderCoursesFirst() {
		RouteCompletion course = RouteCompletionTestUtil.create().isCourse(true).build();
		RouteCompletion notACourse = RouteCompletionTestUtil.create().isCourse(false).build();
		List<RouteCompletion> preferredFirst = RouteCompletionUtil.preferredFirst(Lists
				.newArrayList(notACourse, course));
		assertEquals(course, Iterables.getFirst(preferredFirst, null));
	}

	@Test
	public void testActivityStats() {
		Ratio UNCOMMON_INCLUDED = MeasurementUtil.ratio(0.25);
		Ratio UNCOMMON_EXCLUDED = MeasurementUtil.ratio(0.251);

		ActivityType COMMON = ActivityType.SKATE;
		ActivityType UNCOMMON = ActivityType.SKI;
		Integer expectedTotal = 4;
		RouteCompletion routeCompletion1 = RouteCompletionTestUtil.withActivityType(COMMON).build();
		RouteCompletion routeCompletion2 = RouteCompletionTestUtil.withActivityType(COMMON).build();
		RouteCompletion routeCompletion3 = RouteCompletionTestUtil.withActivityType(COMMON).build();
		RouteCompletion routeCompletion4 = RouteCompletionTestUtil.withActivityType(UNCOMMON).build();
		ArrayList<RouteCompletion> list = Lists.newArrayList(	routeCompletion1,
																routeCompletion2,
																routeCompletion3,
																routeCompletion4);
		ActivityStats allActivityStats = RouteCompletionUtil.activityStats(list, UNCOMMON_INCLUDED);
		assertEquals("total is incorrect", expectedTotal, allActivityStats.total());
		TestUtil.assertIterablesEquals(	"expecting all",
										Lists.newArrayList(COMMON, UNCOMMON),
										CategoricalStatistic.categories(allActivityStats.activityTypeStats()));
		TestUtil.assertNotEmpty(allActivityStats.distanceStats());
		TestUtil.assertSize(allActivityStats.activityTypeStats().size(), allActivityStats.distanceStats());
		ActivityStats commonActivityStats = RouteCompletionUtil.activityStats(list, UNCOMMON_EXCLUDED);
		assertEquals("total is incorrect", expectedTotal, commonActivityStats.total());
		TestUtil.assertIterablesEquals(	"activity types not filtered",
										Lists.newArrayList(COMMON),
										CategoricalStatistic.categories(commonActivityStats.activityTypeStats()));

	}

	/** IF enough is MTB then it shall be considered MTB */
	@Test
	public void testMtbBikeStats() {
		RouteCompletion routeCompletion1 = RouteCompletionTestUtil.withActivityType(ActivityType.MTB)
				.activityTypeReported(ActivityType.MTB).build();
		RouteCompletion routeCompletion2 = RouteCompletionTestUtil.withActivityType(ActivityType.BIKE)
				.activityTypeReported(ActivityType.BIKE).build();
		{
			ImmutableList<RouteCompletion> equalCompletions = ImmutableList.of(routeCompletion1, routeCompletion2);
			// if the ratio is equal then MTB is always chosen regardless of provided ratio
			assertBikeChosenStats(ActivityType.MTB, equalCompletions, MeasurementUtil.RATIO_ONE);
			assertBikeChosenStats(ActivityType.MTB, equalCompletions, MeasurementUtil.RATIO_ZERO);
		}
		{
			ImmutableList<RouteCompletion> moreBikeCompletions = ImmutableList
					.of(routeCompletion1, routeCompletion2, RouteCompletionTestUtil.withActivityType(ActivityType.BIKE)
							.build());
			final Ratio minRatioForMtb = MeasurementUtil.ratio(1, moreBikeCompletions.size());
			final Ratio minRatioForBikeChosen = QuantityMath.create(minRatioForMtb).plus(MeasurementUtil.ratio(0.001))
					.getQuantity();
			assertBikeChosenStats(ActivityType.MTB, moreBikeCompletions, minRatioForMtb);
			assertBikeChosenStats(ActivityType.BIKE, moreBikeCompletions, minRatioForBikeChosen);
		}
	}

	/**
	 * @param expectedWinner
	 * @param completions
	 * @param minRatioToChooseMtb
	 */
	private void assertBikeChosenStats(ActivityType expectedWinner, ImmutableList<RouteCompletion> completions,
			Ratio minRatioToChooseMtb) {
		CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> stats = ActivityTypeStatistic
				.counter(RouteCompletionUtil.activityTypes(completions)).build();

		Boolean mtbChosen = ActivityUtil.isMountainBike(stats, minRatioToChooseMtb);
		assertEquals(mtbChosen, expectedWinner.equals(ActivityType.MTB));
	}

	@Test
	public void testDuplicatesSameTimeSameDuration() {
		List<RouteCompletion> completions = RouteCompletionTestUtil.completionsDifferentPersonSameTimeSameDuration();
		assertTrue(RouteCompletionUtil.areDuplicates(completions.get(0), completions.get(1)));
	}

	@Test
	public void testDuplicatesSameTimeDifferentDuration() {
		final List<RouteCompletion> completions = RouteCompletionTestUtil
				.completionsDifferentPersonSameTimeDifferentDuration();
		assertFalse("different durations", RouteCompletionUtil.areDuplicates(completions.get(0), completions.get(1)));
	}

	@Test
	public void testDuplicatesDifferentTimes() {
		assertFalse("completions have different times", RouteCompletionUtil.areDuplicates(RouteCompletionTestUtil
				.routeCompletion(), RouteCompletionTestUtil.routeCompletion()));
	}

	@Test
	public void testSamePersonDifferentTime() {

		List<RouteCompletion> completions = RouteCompletionTestUtil.completionsSamePersonDifferentTimes();
		assertFalse(RouteCompletionUtil.areFromSamePersonSameTime(completions.get(0), completions.get(1)));
	}

	@Test
	public void testSamePersonSameTime() {
		List<RouteCompletion> completions = RouteCompletionTestUtil.completionsSamePersonOverlappingTimes();
		assertTrue(RouteCompletionUtil.areFromSamePersonSameTime(completions.get(0), completions.get(1)));
		assertTrue(RouteCompletionUtil.areDuplicates(completions.get(0), completions.get(1)));
	}

	@Test
	public void testConsecutive() {
		Pair<RouteCompletion, RouteCompletion> completionsConsecutiveRepeats = RouteCompletionTestUtil
				.completionsConsecutiveRepeats();
		RouteCompletion first = completionsConsecutiveRepeats.getLeft();
		RouteCompletion second = completionsConsecutiveRepeats.getRight();
		assertTrue("repeats are consecutive", RouteCompletionUtil.areConsecutive(first, second));
		assertFalse("wrong order", RouteCompletionUtil.areConsecutive(second, first));
		RouteCompletion firstOfAnother = RouteCompletionTestUtil.withRepeatCount(1, 2).build();
		assertFalse("not the same activity", RouteCompletionUtil.areConsecutive(firstOfAnother, second));
	}
}
