/**
 *
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ActivityType;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.Device;
import com.aawhere.app.DeviceTestUtil;
import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.person.Person;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.route.RouteCompletion.Builder;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class RouteCompletionTestUtil {
	static final ActivityType ACTIVITY_TYPE_EVENT1 = ActivityType.BIKE;

	static final int SUMPTIN = 100000;

	static final Duration fasterDuration = new Duration(SUMPTIN);
	static final Duration middleDuration = new Duration(SUMPTIN * 2);
	static final Duration slowerDuration = new Duration(SUMPTIN * 3);
	// these times must not overlap
	static final DateTime earlierDateTime = new DateTime(0);
	// fairly close to the earlier
	static final DateTime middleDateTime = earlierDateTime.plus(fasterDuration);
	// much later than the earlier and a lot later than the middle. allows for time to not overlap
	static final DateTime laterDateTime = middleDateTime.plus(slowerDuration);

	public static RouteCompletion routeCompletion() {
		return RouteCompletionUnitTest.create().getEntitySample();
	}

	public static Builder mutate(RouteCompletion completion) {
		return RouteCompletion.mutate(completion);
	}

	/**
	 * @return
	 */
	public static RouteCompletion early() {
		return standard(earlierDateTime, middleDuration);
	}

	public static RouteCompletion late() {
		return standard(laterDateTime, fasterDuration);
	}

	public static RouteCompletion middle() {
		return standard(middleDateTime, fasterDuration);
	}

	/**
	 * @param startTime
	 * @param duration
	 * @return
	 */
	private static RouteCompletion standard(final DateTime startTime, final Duration duration) {
		return RouteCompletionTestUtil.create().startTime(startTime).duration(duration)
				.activityTypeReported(ACTIVITY_TYPE_EVENT1).build();
	}

	/**
	 * @return
	 */
	private static Builder mutator() {
		return mutate(routeCompletion());
	}

	public static Builder withDuration(Duration duration) {
		return mutator().duration(duration);
	}

	public static Builder withActivityType(ActivityType type) {
		return mutator().activityType(type);
	}

	public static Builder withActivityTypeReported(ActivityType type) {
		return mutator().activityTypeReported(type);
	}

	/**
	 * Provides an activity containing a device with the application key given.
	 * 
	 * @param deviceKey
	 * @return
	 */
	public static RouteCompletion withDevice(final ApplicationKey deviceKey) {
		Device poorDevice = DeviceTestUtil.device(deviceKey);
		Activity poorActivity = ActivityTestUtil.createActivity(poorDevice);
		RouteCompletion poorCompletion = withActivity(poorActivity).build();
		return poorCompletion;
	}

	public static Builder withActivity(Activity activity) {
		RouteCompletionUnitTest test = RouteCompletionUnitTest.create();
		return mutate(test.activity(activity).getEntitySample());
	}

	/**
	 * Everything is random except the given parent.
	 * 
	 * @param id
	 * @return
	 */
	public static Builder with(Route route) {
		return RouteCompletion.adopt(route, mutator().build());
	}

	/**
	 * @param i
	 * @param j
	 * @return
	 */
	public static Builder withRepeatCount(int repeatCount, int repeatTotal) {
		RouteCompletionUnitTest test = RouteCompletionUnitTest.create();
		test.repeatCount(repeatCount, repeatTotal);
		return mutate(test.getEntitySample());
	}

	/**
	 * @return
	 */
	public static Builder create() {
		return mutate(routeCompletion());
	}

	/** Guarantees the times do not overlapp */
	public static List<RouteCompletion> completionsSamePersonDifferentTimes() {
		Person person = PersonTestUtil.personWithId();
		DateTime time = JodaTestUtil.createRandomTimestamp();
		Duration duration = JodaTestUtil.duration();
		ActivityType at = ActivityType.BIKE;
		RouteCompletion routeCompletion = RouteCompletionTestUtil.create().person(person).startTime(time)
				.duration(duration).activityType(at).build();
		// make sure the start time is after the first start time
		RouteCompletion routeCompletion2 = RouteCompletionTestUtil.create().person(person)
				.startTime(time.plus(duration).plus(duration)).activityType(at).build();
		return Lists.newArrayList(routeCompletion, routeCompletion2);
	}

	/** the first in the list is the course and preferred. */
	public static List<RouteCompletion> completionsDifferentPersonSameTimeSameDuration() {
		DateTime time = new DateTime();
		Duration duration = JodaTestUtil.duration();
		RouteCompletion left = RouteCompletionTestUtil.create().startTime(time).isCourse(true).duration(duration)
				.build();
		RouteCompletion right = RouteCompletionTestUtil.create().startTime(time).isCourse(false).duration(duration)
				.build();
		return Lists.newArrayList(left, right);
	}

	public static List<RouteCompletion> completionsDifferentPersonSameTimeDifferentDuration() {
		DateTime time = new DateTime();
		RouteCompletion left = RouteCompletionTestUtil.create().startTime(time).build();
		RouteCompletion right = RouteCompletionTestUtil.create().startTime(time).build();
		return Lists.newArrayList(left, right);
	}

	/**
	 * Provides two completions from the same person with overlapping times. The first in the list
	 * is the preferred since it is a course
	 */
	public static List<RouteCompletion> completionsSamePersonOverlappingTimes() {
		Person person = PersonTestUtil.personWithId();
		DateTime time = new DateTime();
		DateTime time2 = time.plus(1000);
		Duration duration = new Duration(100000);
		// different times, but intervals overlap
		RouteCompletion routeCompletion = RouteCompletionTestUtil.create().person(person).startTime(time)
				.isCourse(true).duration(duration).build();
		RouteCompletion routeCompletion2 = RouteCompletionTestUtil.create().person(person).startTime(time2)
				.isCourse(false).duration(duration).build();
		return Lists.newArrayList(routeCompletion, routeCompletion2);
	}

	public static Pair<RouteCompletion, RouteCompletion> completionsConsecutiveRepeats() {
		Activity activity = ActivityTestUtil.createActivity();
		DateTime time = DateTime.now();
		RouteCompletion first = RouteCompletionTestUtil.withRepeatCount(1, 2).activity(activity).startTime(time)
				.build();
		// times must be consecutive.
		RouteCompletion second = RouteCompletionTestUtil.withRepeatCount(2, 2).activity(activity)
				.startTime(time.plus(10000)).build();
		return Pair.of(first, second);
	}

	public static List<RouteCompletion> completionsSamePersonConsecutiveTimes() {
		Person person = PersonTestUtil.personWithId();
		DateTime time = new DateTime();
		Duration duration = new Duration(100000);
		// these will overlap for an instant
		DateTime time2 = time.plus(duration).minus(1);
		ActivityType activityType = ActivityTestUtil.activityType();
		// different times, but intervals overlap
		RouteCompletion routeCompletion = RouteCompletionTestUtil.create().activityType(activityType).person(person)
				.startTime(time).duration(duration).build();
		RouteCompletion routeCompletion2 = RouteCompletionTestUtil.create().activityType(activityType).person(person)
				.startTime(time2).duration(duration).build();
		return Lists.newArrayList(routeCompletion, routeCompletion2);
	}

	/**
	 * @param duplicate
	 */
	public static void assertDuplicate(RouteCompletion duplicate) {
		assertNull(duplicate.socialCountRank());
		assertNull(duplicate.socialRaceRank());
		assertNull(duplicate.socialOrder());
		assertNull(duplicate.personalOrder());
		assertNull(duplicate.personalRaceRank());
		assertNotNull(duplicate.duplicateId());
		TestUtil.assertSize(1, duplicate.activityIds());
	}
}
