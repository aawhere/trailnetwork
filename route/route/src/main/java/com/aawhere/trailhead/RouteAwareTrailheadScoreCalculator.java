/**
 * 
 */
package com.aawhere.trailhead;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.route.Route;
import com.aawhere.route.RouteId;
import com.aawhere.route.RouteUtil;
import com.aawhere.route.Routes;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.Iterables;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Simple calculator providing the cumulative score of all {@link Routes} connected to a
 * {@link Trailhead}.
 * 
 * @see TrailheadScoreCalculator
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteAwareTrailheadScoreCalculator
		implements TrailheadScoreCalculator {

	@XmlElement
	@ApiModelProperty(value = "The trailhead for whom the score is being calculated")
	@Nullable
	private Trailhead trailhead;

	@XmlAttribute
	private Integer score;
	@XmlAttribute
	private Integer numberOfRoutes;
	@XmlAttribute
	private Integer numberOfMains;
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private HashMap<RouteId, Integer> routeScores = new HashMap<>();
	@XmlElement
	private TrailheadStatsScoreCalculator statsScoreCalculator;

	/**
	 * Used to construct all instances of TrailheadScoreCalculator.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteAwareTrailheadScoreCalculator> {

		private Set<Route> routes = new HashSet<>();

		public Builder() {
			super(new RouteAwareTrailheadScoreCalculator());
		}

		public Builder routes(Iterable<Route> routes) {
			Iterables.addAll(this.routes, routes);
			return this;
		}

		/** @see #trailhead */
		public Builder trailhead(@Nullable Trailhead trailhead) {
			building.trailhead = trailhead;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("routes", this.routes);

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public RouteAwareTrailheadScoreCalculator build() {
			RouteAwareTrailheadScoreCalculator built = super.build();
			Integer score = Trailhead.DEFAULT_SCORE;
			// the base of the score starts with the activity count influenced by the number of
			// people
			// trailhead may be getting created right now so it doesn't provide any valuable
			// information
			if (built.trailhead != null) {
				built.statsScoreCalculator = TrailheadStatsScoreCalculator.create().trailhead(built.trailhead).build();
				score += built.statsScoreCalculator.score();
			}
			// the route completions tops off the score by cumulating all scores from the routes
			// that start at the trailhead
			{
				built.numberOfRoutes = routes.size();
				Integer numberOfMains = 0;
				Iterable<Route> filteredRoutes = RouteUtil.routesWithScore(this.routes);
				for (Route route : filteredRoutes) {
					score += route.getScore();
					built.routeScores.put(route.id(), route.score());
					if (BooleanUtils.isTrue(route.isMain())) {
						numberOfMains++;
					}
				}
				built.numberOfMains = numberOfMains;
			}
			built.score = score;
			return built;
		}
	}// end Builder

	/** Use {@link Builder} to construct TrailheadScoreCalculator */
	private RouteAwareTrailheadScoreCalculator() {
	}

	public static Builder create() {
		return new Builder();
	}

	@Override
	public Integer score() {
		return score;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.trailhead.TrailheadScoreCalculator#numberOfMains()
	 */
	public Integer numberOfMains() {
		return this.numberOfMains;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.trailhead.TrailheadScoreCalculator#numberOfRoutes()
	 */
	public Integer numberOfRoutes() {
		return this.numberOfRoutes;
	}

	/** @see #trailhead */
	public Trailhead trailhead() {
		return this.trailhead;
	}

	@Override
	@Nullable
	public TrailheadId trailheadId() {
		if (trailhead != null) {
			return trailhead.id();
		} else {
			return null;
		}
	}
}
