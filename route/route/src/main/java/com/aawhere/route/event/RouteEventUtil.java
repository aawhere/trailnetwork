/**
 * 
 */
package com.aawhere.route.event;

import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aawhere.joda.time.IntervalOverlapPredicate;
import com.aawhere.joda.time.IntervalUtil;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.route.RouteId;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class RouteEventUtil {

	/**
	 * 
	 */
	static final String ID_TIME_PATTERN = "YYMMddHH";
	static final DateTimeFormatter ID_TIME_FORMAT = DateTimeFormat.forPattern(ID_TIME_PATTERN);

	public static Iterable<Interval> intervals(Iterable<RouteEvent> events) {
		return Iterables.transform(events, intervalFunction());
	}

	public static Function<RouteEvent, Interval> intervalFunction() {
		return new Function<RouteEvent, Interval>() {

			@Override
			public Interval apply(RouteEvent input) {
				return (input != null) ? input.interval() : null;
			}
		};
	}

	public static Function<RouteEvent, DateTime> startTimeFunction() {
		return Functions.compose(IntervalUtil.startTimeFuncton(), intervalFunction());
	}

	/**
	 * Finds any {@link RouteEvents} that overlap the given {@link Interval}.
	 * 
	 * @see IntervalOverlapPredicate
	 * 
	 * @param events
	 * @param interval
	 * @return
	 */
	public static Iterable<RouteEvent> findOverlaps(Iterable<RouteEvent> events, Interval interval) {
		Predicate<RouteEvent> predicate = Predicates.compose(	IntervalOverlapPredicate.build(interval),
																intervalFunction());
		return Iterables.filter(events, predicate);
	}

	/**
	 * Events are best identified by the time interval they span and the route they completed during
	 * that time..
	 * 
	 * FIXME:Include time zone to provide a date/time relative to the location of the route.
	 * 
	 * @param routeId
	 * @param currentOverlapInterval
	 * @return
	 */
	public static RouteEventId routeEventId(RouteId routeId, Interval overlapInterval) {
		String dateString = ID_TIME_FORMAT.print(overlapInterval.getStart());
		String idValue = new StringBuilder(dateString).toString();
		return new RouteEventId(routeId, idValue);

	}

	public static Ordering<RouteEvent> orderingByDate() {
		return Ordering.natural().onResultOf(startTimeFunction());
	}

	/**
	 * Returns a copy of the given {@link RouteEvent} with the given interval expanding the event
	 * interval. The given event is returned unchanged if the interval does not affect the existing
	 * event's interval.
	 * 
	 * @param event
	 * @param currentOverlapInterval
	 * @return
	 */
	public static RouteEvent expanded(RouteEvent event, Interval otherInterval) {
		if (event.interval().contains(otherInterval)) {
			return event;
		} else {
			return RouteEvent.clone(event).interval(IntervalUtil.expanded(event.interval(), otherInterval)).build();
		}
	}

	/**
	 * Provides the generic function interface for the less generic {@link RouteEventProvider}.
	 * 
	 * @param provider
	 * @return
	 */
	public static Function<RouteEventId, RouteEvent> providerFunction(final RouteEventProvider provider) {
		return new Function<RouteEventId, RouteEvent>() {

			@Override
			public RouteEvent apply(RouteEventId input) {

				try {
					return (input != null) ? provider.routeEvent(input) : null;
				} catch (EntityNotFoundException e) {
					return null;
				}

			}
		};
	}

	/**
	 * Same as {@link #routeIds(Iterable)} but ensures there are no duplicates.
	 * 
	 * @see #routeIds(Iterable)
	 * @param routeEventIds
	 * @return
	 */
	public static Set<RouteId> routeIdsUnique(Iterable<RouteEventId> routeEventIds) {
		return Sets.newHashSet(routeIds(routeEventIds));
	}

	/**
	 * Transforms the routeEventIds into just the parent routeid. Duplicates are possible and
	 * probable given the set of events, nulls are not.
	 * 
	 * @see #routeIdFunction()
	 * @param routeEventIds
	 *            provides the routeId parent.
	 * @return
	 */
	public static Iterable<RouteId> routeIds(Iterable<RouteEventId> routeEventIds) {
		return Iterables.filter(Iterables.transform(routeEventIds, routeIdFunction()), Predicates.notNull());
	}

	public static Function<RouteEventId, RouteId> routeIdFunction() {
		return new Function<RouteEventId, RouteId>() {
			@Override
			public RouteId apply(RouteEventId input) {
				return (input != null) ? input.getParent() : null;
			}
		};
	}

	/**
	 * @param children
	 * @return
	 */
	public static Iterable<SocialGroupId> socialGroupIds(Iterable<RouteEvent> events) {
		return Iterables.transform(events, socialGroupIdFunction());
	}

	public static Function<RouteEvent, SocialGroupId> socialGroupIdFunction() {
		return new Function<RouteEvent, SocialGroupId>() {

			@Override
			public SocialGroupId apply(RouteEvent input) {
				return (input != null) ? input.socialGroupId() : null;
			}
		};
	}
}
