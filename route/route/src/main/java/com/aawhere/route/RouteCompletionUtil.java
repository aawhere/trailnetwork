/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.route.RouteCompletion.FIELD.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityIdentifierUtil;
import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategoryStatistic;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatsUtil;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationStats;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.BuilderUtil;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.number.IntMathExtended;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.person.PersonId;
import com.aawhere.route.RouteCompletion.Builder;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventIdProvider;
import com.aawhere.route.event.RouteEventStatistic;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class RouteCompletionUtil {

	private static Logger LOG = LoggerFactory.getLogger(RouteCompletionUtil.class);
	/**
	 * Ordering given the more preferred completion for a single activity.
	 * 
	 * 
	 * @see #preferredFirst(ActivityId, Iterable)
	 */
	private static final Ordering<RouteCompletion> BETTER_COMPLETIONS_FOR_ACTIVITY_FIRST = coursesFirst()
			.compound(higherCourseActivityCountFirst()).compound(higherCoursesCompletedCountFirst())
			.compound(shorterDurationFirst());

	/**
	 * 
	 * @param completions
	 * @return
	 */
	public static Iterable<RouteCompletionId> ids(Iterable<RouteCompletion> completions) {
		return IdentifierUtils.ids(completions);
	}

	public static Iterable<ActivityId> activityIdsFromIds(Iterable<RouteCompletionId> completionIds) {
		return Iterables.transform(completionIds, activityIdFromIdFunction());
	}

	public static Iterable<ActivityId> activityIds(Iterable<RouteCompletion> completions) {
		return Iterables.transform(completions, activityIdFunction());
	}

	public static Iterable<ActivityId> activityIdsIncludingDuplicates(Iterable<RouteCompletion> completions) {
		return Iterables.concat(Iterables.transform(completions, activityIdsFunction()));
	}

	public static RouteCompletion faster(RouteCompletion left, RouteCompletion right) {
		return Iterables.getFirst(orderByFastest().sortedCopy(Lists.newArrayList(left, right)), null);
	}

	public static Ordering<RouteCompletion> orderByFastest() {
		return shorterDurationFirst();
	}

	/**
	 * Basically this sorts by {@link RouteCompletion#startTime()}.
	 * 
	 * @return
	 */
	public static Ordering<RouteCompletion> orderedByOccurrence() {
		return Ordering.natural().nullsLast().onResultOf(startTimeFunction());
	}

	/**
	 * the opposite of {@link #orderedByOccurrence()} providing the most recent completion first.
	 * 
	 * @return
	 */
	public static Ordering<RouteCompletion> orderedByMostRecent() {
		return Ordering.natural().reverse().nullsLast().onResultOf(startTimeFunction());
	}

	public static Function<RouteCompletion, ActivityId> activityIdFunction() {
		return new Function<RouteCompletion, ActivityId>() {

			@Override
			public ActivityId apply(RouteCompletion input) {
				return (input == null) ? null : input.activityId();
			}
		};
	}

	public static Function<RouteCompletionId, ActivityId> activityIdFromIdFunction() {
		return new Function<RouteCompletionId, ActivityId>() {

			@Override
			public ActivityId apply(RouteCompletionId input) {
				return (input != null) ? input.activityId() : null;
			}
		};
	}

	/**
	 * The ActivityId is encoded in the RouteCompletionId.
	 * 
	 * @param input
	 * @return
	 */
	public static ActivityId activityId(RouteCompletionId input) {
		return input.activityId();
	}

	public static Function<RouteCompletion, List<ActivityId>> activityIdsFunction() {
		return new Function<RouteCompletion, List<ActivityId>>() {

			@Override
			public List<ActivityId> apply(RouteCompletion input) {
				return (input != null) ? input.activityIds() : null;
			}
		};
	}

	public static Function<RouteCompletion, ActivityType> activityTypeFunction() {
		return new Function<RouteCompletion, ActivityType>() {

			@Override
			public ActivityType apply(RouteCompletion input) {
				return (input == null) ? null : input.activityType();
			}
		};
	}

	public static Function<RouteCompletion, ActivityType> activityTypeReportedFunction() {
		return new Function<RouteCompletion, ActivityType>() {

			@Override
			public ActivityType apply(RouteCompletion input) {
				return (input == null) ? null : input.activityTypeReported();
			}
		};
	}

	/** used to keep only completions matching the given id. */
	public static Predicate<RouteCompletion> activityIdPredicate(ActivityId activityId) {
		return Predicates.compose(Predicates.equalTo(activityId), activityIdFunction());
	}

	/**
	 * Keyed by the activity type referencing every completion that has that activity type.
	 * 
	 * @param completions
	 * @return
	 */
	public static ImmutableListMultimap<ActivityType, RouteCompletion> activityTypesMapped(
			Iterable<RouteCompletion> completions) {
		return Multimaps.index(completions, activityTypeFunction());
	}

	public static Iterable<ActivityType> activityTypes(Iterable<RouteCompletion> completions) {
		// nulls shouldn't happen, but if they do it provides little value to return it.
		return Iterables.filter(Iterables.transform(completions, activityTypeFunction()), Predicates.notNull());
	}

	/**
	 * @param completions
	 * @return
	 */
	public static Iterable<ActivityType> activityTypesReported(Iterable<RouteCompletion> completions) {
		return Iterables.transform(completions, activityTypeReportedFunction());
	}

	public static CategoricalStatsCounter<ApplicationStats, Application> applicationsCounter(
			Iterable<RouteCompletion> completions) {
		Iterable<ActivityId> activityIds = Iterables.transform(completions, activityIdFunction());
		return ActivityIdentifierUtil.applicationCounter(activityIds);
	}

	/**
	 * Provides the activity stats for those completions provided. Filters out activity types that
	 * don't match the given significance ratio.
	 * 
	 * @param completions
	 * @param significantActivityTypePercent
	 * @return the activity stats complete with activity types, applications and distance categories
	 *         counted. This will report a total of zero if nothing is provided
	 */
	@Nonnull
	public static ActivityStats activityStats(@Nonnull Iterable<RouteCompletion> completions,
			Ratio significantActivityTypePercent) {

		com.aawhere.activity.ActivityStats.Builder builder = ActivityStats.create();
		final Iterable<ActivityType> activityTypes = activityTypes(completions);
		// see TN-576 577
		final Predicate<ActivityTypeStatistic> percentPredicate = ActivityTypeStatistic.percentPredicate(Range
				.atLeast(significantActivityTypePercent));
		CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> activityTypeCounter = ActivityTypeStatistic
				.counter(activityTypes).predicate(percentPredicate).build();
		builder.activityTypeCounter(activityTypeCounter);

		// distance
		RouteCompletion first = Iterables.getFirst(completions, null);
		if (first != null) {
			// activity types are 1:1 with distance category for routes since a route is a fixed
			// length
			// just copy the stats from activity types so they match
			Length distance = first.routeDistance();
			SortedSet<ActivityTypeDistanceCategoryStatistic> distanceStats = ActivityTypeDistanceCategoryStatistic
					.distanceCategoriesFromActivityTypes(distance, activityTypeCounter.stats());
			builder.distanceStats(distanceStats);
		}

		// application
		CategoricalStatsCounter<ApplicationStats, Application> applicationsCounter = RouteCompletionUtil
				.applicationsCounter(completions);
		builder.applicationCounter(applicationsCounter);
		return builder.build();
	}

	static Function<RouteCompletion.Builder, RouteCompletion> buildingFunction() {
		return BuilderUtil.buildingFunction();
	}

	/**
	 * Provides the parent routes when given completions.
	 * 
	 * @param completions
	 * @return
	 */
	public static Iterable<RouteId> routeIds(Iterable<RouteCompletion> completions) {
		return Iterables.transform(completions, routeIdFunction());
	}

	private enum RouteIdFunction implements Function<RouteCompletion, RouteId> {
		INSTANCE;
		@Override
		@Nullable
		public RouteId apply(@Nullable RouteCompletion completion) {
			return (completion != null) ? completion.routeId() : null;
		}

		@Override
		public String toString() {
			return "RouteIdFunction";
		}
	}

	public static Function<RouteCompletion, RouteId> routeIdFunction() {
		return RouteIdFunction.INSTANCE;
	}

	private enum RouteIdFromIdFunction implements Function<RouteCompletionId, RouteId> {
		INSTANCE;
		@Override
		@Nullable
		public RouteId apply(@Nullable RouteCompletionId id) {
			return (id != null) ? id.routeId() : null;
		}

		@Override
		public String toString() {
			return "RouteIdFromIdFunction";
		}
	}

	public static Function<RouteCompletionId, RouteId> routeIdFromIdFunction() {
		return RouteIdFromIdFunction.INSTANCE;
	}

	public static Function<RouteCompletion, Boolean> isCourseFunction() {
		return new Function<RouteCompletion, Boolean>() {
			@Override
			public Boolean apply(RouteCompletion input) {
				return (input == null) ? null : input.isCourse();
			}

			@Override
			public String toString() {
				return RouteCompletion.FIELD.IS_COURSE;
			}
		};
	}

	public static Predicate<RouteCompletion> isCoursePredicate() {
		return Predicates.compose(Predicates.equalTo(Boolean.TRUE), isCourseFunction());
	}

	/**
	 * Provides the personIds from the completions with no guarantee of uniqueness.
	 * 
	 * @param completions
	 */
	public static Iterable<PersonId> personIds(Iterable<RouteCompletion> completions) {
		return Iterables.transform(completions, personIdFunction());
	}

	public static Iterable<Map.Entry<PersonId, Integer>>
			personalTotalsIdentified(Iterable<RouteCompletion> completions) {
		return Iterables.transform(completions, personalTotalIdentifiedFunction());
	}

	/**
	 * Provides a pair of PersonId representing the {@link RouteCompletion#personalTotal()}
	 * completions made.
	 * 
	 * @return
	 */
	public static Function<RouteCompletion, Map.Entry<PersonId, Integer>> personalTotalIdentifiedFunction() {
		return new Function<RouteCompletion, Map.Entry<PersonId, Integer>>() {

			@Override
			public Entry<PersonId, Integer> apply(RouteCompletion input) {
				return (input != null) ? Pair.of(input.personId(), input.personalTotal()) : null;
			}
		};
	}

	public static Function<RouteCompletion, PersonId> personIdFunction() {
		return new Function<RouteCompletion, PersonId>() {

			@Override
			public PersonId apply(RouteCompletion input) {
				return (input == null) ? null : input.personId();
			}
		};
	}

	/**
	 * Provides a mutating {@link Builder} if the routeCompletion provided is not null.
	 * 
	 * @return
	 */
	public static Function<RouteCompletion, ? extends RouteCompletion.Builder> mutatorFunction() {
		return new Function<RouteCompletion, RouteCompletion.Builder>() {

			@Override
			public RouteCompletion.Builder apply(RouteCompletion input) {
				return (input == null) ? null : RouteCompletion.mutate(input);
			}
		};
	}

	/**
	 * Returns only those completions where {@link RouteCompletion#isCourse()} is true;
	 * 
	 * @param completions
	 * @return
	 */
	public static Iterable<RouteCompletion> courses(Iterable<RouteCompletion> completions) {
		return Iterables.filter(completions, isCoursePredicate());
	}

	public static Function<RouteCompletion, Integer> personalRaceRankFunction() {
		return new Function<RouteCompletion, Integer>() {

			@Override
			public Integer apply(RouteCompletion input) {
				return (input == null) ? null : input.personalRaceRank();
			}
		};
	}

	/** Filters to keep only those that match the given race rank */
	public static Predicate<RouteCompletion> personalRaceRankPredicate(Integer raceRank) {
		return Predicates.compose(Predicates.equalTo(raceRank), personalRaceRankFunction());
	}

	public static Predicate<RouteCompletion> personalBestPredicate() {
		return personalRaceRankPredicate(RouteCompletion.PERSONAL_BEST_RANK);
	}

	/**
	 * @param completions
	 * @return
	 */
	public static Iterable<RouteCompletion> personalBests(Iterable<RouteCompletion> completions) {
		return Iterables.filter(completions, personalBestPredicate());
	}

	/**
	 * Filters completions resulting in completions from the given {@link Application}.
	 * 
	 * @param applicationKey
	 * @return
	 */
	public static Predicate<RouteCompletion> applicationPredicate(ApplicationKey applicationKey) {
		return Predicates.compose(Predicates.equalTo(applicationKey), applicationKeyFunction());
	}

	public static Function<RouteCompletion, ApplicationKey> applicationKeyFunction() {
		return Functions.compose(ActivityIdentifierUtil.applicationKeyFunction(), activityIdFunction());
	}

	/**
	 * Chooses the best Route completion for the same activity.
	 * 
	 * A RouteCompletion is a consolidated set of timing completion results of the same activity
	 * completing different courses. Those courses are similar enough to be part of the same route,
	 * but not exact resulting in different completion duration and other important metrics. In some
	 * cases an activity may be considered a course when compared against another course, but may
	 * not be a course when compared to another (i.e. A & B may be reciprocals, B & C may be
	 * reciprocals, but A & C may not be reciprocals but through transitive relationships they are
	 * consider reciprocals).
	 * 
	 * An activity that may not be a course, because it leads from another location for instance,
	 * may complete many courses. The better activity is the one that completes more courses.
	 * 
	 * This will provide the RouteCompletions of the same {@link Activity} in order of the most
	 * preferred...a series of comparisons that will determine. The code is the best place to
	 * understand the order of preferences.
	 * 
	 * *
	 * <ol>
	 * <li>courses are ideal completions. Some activiites may be reciprocal to some, but not all so
	 * when we receiving a timing we should keep those that are courses.</li>
	 * <li>a course that has more activities completing it shows it likely has a better signal or
	 * started from a better place than another.</li>
	 * <li>all courses and activities must have completed another activity in order to be a
	 * completion. the more the merrier</li>
	 * <li>all other quality issues aside, pick the faster one for the race rank.</li>
	 * </ol>
	 * 
	 * 
	 * @param activityId
	 *            the activityId that must be in the completion otherwise it won't be compared
	 * @return
	 * 
	 */
	public static List<RouteCompletion> preferredFirst(ActivityId activityId, Iterable<RouteCompletion> completions) {

		Iterable<RouteCompletion> filteredCompletions = Iterables.filter(completions, activityIdPredicate(activityId));
		return preferredFirst(filteredCompletions);
		// all devices for the same activity will be the same
		// signal quality for the same activity is the same.
	}

	/**
	 * Chooses the best {@link RouteCompletion} regardless of the activity.
	 * 
	 * @see #preferredFirst(ActivityId, Iterable)
	 * @param filteredCompletions
	 * @return
	 */
	public static List<RouteCompletion> preferredFirst(Iterable<RouteCompletion> filteredCompletions) {
		Ordering<RouteCompletion> ordering = BETTER_COMPLETIONS_FOR_ACTIVITY_FIRST;
		return ordering.sortedCopy(filteredCompletions);
	}

	public static Ordering<RouteCompletion> shorterDurationFirst() {
		return Ordering.natural().nullsLast().onResultOf(durationFunction());
	}

	public static Function<RouteCompletion, Duration> durationFunction() {
		return new Function<RouteCompletion, Duration>() {

			@Override
			public Duration apply(RouteCompletion input) {
				return (input == null) ? null : input.duration();
			}
		};
	}

	public static Function<RouteCompletion, DateTime> startTimeFunction() {
		return new Function<RouteCompletion, DateTime>() {

			@Override
			public DateTime apply(RouteCompletion input) {
				return (input != null) ? input.startTime() : null;
			}
		};
	}

	public static Ordering<RouteCompletion> coursesFirst() {
		return Ordering.natural().reverse().nullsLast().onResultOf(isCourseFunction());
	}

	public static Function<RouteCompletion, Integer> coursesCompletedCountFunction() {
		return new Function<RouteCompletion, Integer>() {

			@Override
			public Integer apply(RouteCompletion input) {
				return (input == null) ? null : input.coursesCompletedCount();

			}
		};

	}

	public static Function<RouteCompletion, Integer> courseActivityCountFunction() {
		return new Function<RouteCompletion, Integer>() {

			@Override
			public Integer apply(RouteCompletion input) {
				return (input == null) ? null : input.courseActivityCount();

			}
		};

	}

	public static Ordering<RouteCompletion> higherCoursesCompletedCountFirst() {
		return Ordering.natural().reverse().nullsLast().onResultOf(coursesCompletedCountFunction());
	}

	public static Ordering<RouteCompletion> higherCourseActivityCountFirst() {
		return Ordering.natural().reverse().nullsLast().onResultOf(courseActivityCountFunction());
	}

	public static Map<RouteCompletionId, RouteCompletion> map(Iterable<RouteCompletion> completions) {
		return Maps.uniqueIndex(completions, idFunction());
	}

	private static Function<RouteCompletion, RouteCompletionId> idFunction() {
		return idFunction();
	}

	/**
	 * Merges the given {@link RouteCompletion} into the one that we're keeping by mutating the
	 * keeper and absorbing the values worth keeping from the other.
	 * 
	 * {@link RouteCompletions} represent an activity finishing a course, but there are many course
	 * candidates so there are many completions with the same ActivityId, similar data that all need
	 * to be represented by one...the cumulation of others. This will merge all of those given into
	 * one that will be adopted by the given parent (necessary to create a new id since the others
	 * may have come from a different parent). The intention of this method is to keep the best of
	 * every attribute making the completion look good.
	 * 
	 * Although this is a good opportunity to do validation, only warnings will be logged to avoid
	 * stopping processing of something that may not be a game stopper.
	 * 
	 * @param keeping
	 *            the completion that is being kept and will be mutated
	 * @param absorbing
	 *            the completion that is providing valuable information that will be transferred
	 *            into the keeper
	 * @return the mutated completion provided as keeping
	 */
	public static RouteCompletion merged(final RouteCompletion keeping, final RouteCompletion absorbing) {
		final RouteCompletionId keepingId = keeping.id();
		final RouteCompletionId absorbingId = absorbing.id();
		Assertion.e().eq(keeping.activityId(), absorbing.activityId());
		Assertion.e().eq(keeping.repeatCount(), absorbing.repeatCount());

		Builder mutator = RouteCompletion.mutate(keeping);
		mutator.isCourse(keeping.isCourse() || absorbing.isCourse());
		mutator.courseActivityCount(IntMathExtended.max(keeping.courseActivityCount(), absorbing.courseActivityCount()));
		mutator.coursesCompletedCount(IntMathExtended.max(	keeping.coursesCompletedCount(),
															absorbing.coursesCompletedCount()));
		mutator.duration(Ordering.natural().min(keeping.duration(), absorbing.duration()));
		// start time...perhaps it should be chosen with the duration?

		// the remainder is just for logging when there are differences
		if (LOG.isLoggable(Level.WARNING)) {
			// conditionally logs a warning if the values are provided, but not equal. Nulls are
			// fine and expected
			Function<Object[], Boolean> log = new Function<Object[], Boolean>() {
				@Override
				public Boolean apply(Object[] input) {
					final Object left = input[1];
					final Object right = input[2];
					if (left != null && right != null) {
						if (!left.equals(right)) {
							LOG.warning(input[0] + " is not equal during merging: " + keepingId + " came with " + left
									+ " merging with " + absorbingId + " which has " + right);
							return true;
						}
					}
					return false;
				}
			};

			log.apply(new Object[] { PERSONAL_TOTAL, keeping.personalTotal(), absorbing.personalTotal() });
			log.apply(new Object[] { PERSONAL_ORDER, keeping.personalOrder(), absorbing.personalOrder() });
			log.apply(new Object[] { PERSONAL_RACE_RANK, keeping.personalRaceRank(), absorbing.personalRaceRank() });
			log.apply(new Object[] { SOCIAL_ORDER, keeping.socialOrder(), absorbing.socialOrder() });
			log.apply(new Object[] { SOCIAL_RACE_RANK, keeping.socialRaceRank(), absorbing.socialRaceRank() });
			log.apply(new Object[] { SOCIAL_COUNT_RANK, keeping.socialCountRank(), absorbing.socialCountRank() });
			log.apply(new Object[] { ACTIVITY_TYPE, keeping.activityType(), absorbing.activityType() });
			log.apply(new Object[] { PERSON_ID, keeping.personId(), absorbing.personId() });

		}
		return mutator.build();
	}

	/**
	 * Detects start times to be equal with a small margin. The likelihood of two tracks crossing a
	 * line within milliseconds of each other is unlikely. A better way is to inspect the activities
	 * directly during timing processing.
	 * 
	 * @param routeCompletion
	 * @param previous
	 * @return
	 */
	public static Boolean areDuplicates(RouteCompletion left, RouteCompletion right) {
		boolean result = false;
		if (left != null && right != null) {
			result = (JodaTimeUtil.equalsWithinTolerance(left.startTime(), right.startTime()) && JodaTimeUtil
					.equalsWithinTolerance(left.duration(), right.duration(), 10));
			result |= areFromSamePersonSameTime(left, right);
		}

		return result;
	}

	/**
	 * The same person can't be in two places at once so this will indicate if two completions
	 * during the same time are coming from the same person. If so then returns true.
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static Boolean areFromSamePersonSameTime(RouteCompletion left, RouteCompletion right) {
		return left.interval().overlaps(right.interval()) && left.personId().equals(right.personId())
		// must allow for repeats of completions which will have the same finish time as the
		// start of the next +/-.
				&& !(areConsecutive(left, right));
	}

	/**
	 * Consecutive completions are fine and expected when doing repeats of a route. This returns
	 * true if the times are consecutive based on their repeats setup from the timing relation.
	 * 
	 * @see #areFromSamePersonSameTime(RouteCompletion, RouteCompletion)
	 * @see #areConsecutiveTimes(RouteCompletion, RouteCompletion, int)
	 * 
	 * @param left
	 * @param right
	 * @param allowedOverlap
	 * @return
	 */
	public static boolean areConsecutive(RouteCompletion left, RouteCompletion right) {
		return left.activityId().equals(right.activityId()) &&
		// repeat totals indicate they are repeats
				left.repeatTotal() != null && right.repeatTotal() != null && left.repeatTotal() > 1
				// totals indicate they are repeats from the same
				&& left.repeatTotal() == right.repeatTotal()
				// left must be before right
				&& left.repeatCount() < right.repeatCount();
	}

	/**
	 * Provides the start/finish {@link Interval} from {@link RouteCompletion#interval()}.
	 * 
	 * @see #intervalFunction()
	 * @see RouteCompletion#interval()
	 * 
	 * @return
	 */
	public static Iterable<Interval> intervals(Iterable<RouteCompletion> completions) {
		return Iterables.transform(completions, intervalFunction());
	}

	public static Function<RouteCompletion, Interval> intervalFunction() {
		return new Function<RouteCompletion, Interval>() {

			@Override
			public Interval apply(RouteCompletion input) {
				return input.interval();
			}
		};
	}

	/**
	 * @see #eventsCounted(Multimap, Integer)
	 * @param events
	 * @return
	 */
	public static SortedSet<RouteEventStatistic> eventsCounted(Multimap<RouteEvent, RouteCompletionId> events) {
		Integer total = events.size();
		return eventsCounted(events, total);
	}

	/**
	 * * Given a multimap of events mapped to the completions that created the event this will
	 * provide statistics to describe the summary of the events.
	 * 
	 * @param events
	 * @param total
	 * @return
	 */
	public static SortedSet<RouteEventStatistic> eventsCounted(Multimap<RouteEvent, RouteCompletionId> events,
			Integer total) {
		TreeSet<RouteEventStatistic> stats = new TreeSet<>();
		for (Entry<RouteEvent, Collection<RouteCompletionId>> entry : events.asMap().entrySet()) {
			RouteEventStatistic statistic = RouteEventStatistic.create().category(entry.getKey().id())
					.count(entry.getValue().size()).total(total).build();
			stats.add(statistic);
		}
		return stats;
	}

	/**
	 * Provides the eventIds from the completions without nulls.
	 * 
	 * @param events
	 * @return
	 */
	public static Iterable<RouteEventId> eventIds(Iterable<RouteEventIdProvider> completions) {
		final Iterable<RouteEventId> eventIds = Iterables.transform(completions, eventIdFunction());
		return Iterables.filter(eventIds, Predicates.notNull());
	}

	public static Function<RouteEventIdProvider, RouteEventId> eventIdFunction() {
		return new Function<RouteEventIdProvider, RouteEventId>() {

			@Override
			public RouteEventId apply(RouteEventIdProvider input) {
				return (input != null) ? input.routeEventId() : null;
			}
		};
	}

	public static Function<RouteCompletion, Integer> routeScoreFunction() {
		return new Function<RouteCompletion, Integer>() {

			@Override
			public Integer apply(RouteCompletion input) {
				return (input != null) ? input.routeScore() : null;
			}
		};
	}

	/**
	 * @see RouteCompletion#isRouteWorthy()
	 * @return
	 */
	public static Function<RouteCompletion, Boolean> scoreIsSignificantFunction() {
		return new Function<RouteCompletion, Boolean>() {

			@Override
			public Boolean apply(RouteCompletion input) {
				return (input != null) ? input.isRouteWorthy() : null;
			}
		};
	}

	/**
	 * Filter for leaving only significant scores.
	 * 
	 * @see #scoreIsSignificantFunction()
	 * @return
	 */
	public static Predicate<RouteCompletion> scoreIsSignificantPredicate() {
		return Predicates.compose(Predicates.equalTo(true), scoreIsSignificantFunction());
	}

	/**
	 * @see #scoreIsSignificantPredicate()
	 * @param completions
	 * @return
	 */
	public static Iterable<RouteCompletion> scoreIsSignificant(Iterable<RouteCompletion> completions) {
		return Iterables.filter(completions, scoreIsSignificantPredicate());
	}

	/**
	 * Filters the completions keeping only those that match the given activity type.
	 * 
	 * @param completions
	 * @param activityType
	 * @return
	 */
	public static Iterable<RouteCompletion> activityTypeCompletions(Iterable<RouteCompletion> completions,
			ActivityType activityType) {
		return Iterables.filter(completions,
								Predicates.compose(Predicates.equalTo(activityType), activityTypeFunction()));
	}

	public static ImmutableList<RelativeStatistic> relativeStatsForRoute(RouteCompletion routeCompletion) {
		RouteId featureId = routeCompletion.routeId();
		return relativeStats(routeCompletion, featureId);
	}

	/**
	 * provides all related statistics for the route completion. RouteCompletion must be processed
	 * so an activity type is required as is a person and distance.
	 * 
	 * @param routeCompletion
	 * @param featureId
	 * @return
	 */
	private static ImmutableList<RelativeStatistic> relativeStats(RouteCompletion routeCompletion,
			Identifier<?, ?> featureId) {
		InvalidArgumentException.required(routeCompletion.activityType(), RouteCompletionField.Absolute.ACTIVITY_TYPE);
		InvalidArgumentException.required(routeCompletion.personId(), RouteCompletionField.Absolute.PERSON_ID);
		InvalidArgumentException
				.required(routeCompletion.routeDistance(), RouteCompletionField.Absolute.ROUTE_DISTANCE);
		return RelativeStatsUtil.relativeStats(	featureId,
												routeCompletion.personId(),
												routeCompletion.activityType(),
												routeCompletion.routeDistance());
	}

	public static ImmutableList<RelativeStatistic> relativeStatsForStartTrailhead(RouteCompletion routeCompletion) {
		return relativeStats(routeCompletion, routeCompletion.startTrailheadId());
	}

	private enum TrailheadRelativeStatsFunction implements Function<RouteCompletion, Iterable<RelativeStatistic>> {
		INSTANCE;
		@Override
		@Nullable
		public Iterable<RelativeStatistic> apply(@Nullable RouteCompletion routeCompletion) {
			return (routeCompletion != null) ? RouteCompletionUtil.relativeStatsForStartTrailhead(routeCompletion)
					: null;
		}

		@Override
		public String toString() {
			return "TrailheadRelativeStatsFunction";
		}
	}

	public static Function<RouteCompletion, Iterable<RelativeStatistic>> trailheadRelativeStatsFunction() {
		return TrailheadRelativeStatsFunction.INSTANCE;
	}

	private enum RouteRelativeStatsFunction implements Function<RouteCompletion, Iterable<RelativeStatistic>> {
		INSTANCE;
		@Override
		@Nullable
		public Iterable<RelativeStatistic> apply(@Nullable RouteCompletion routeCompletion) {
			return (routeCompletion != null) ? RouteCompletionUtil.relativeStatsForRoute(routeCompletion) : null;
		}

		@Override
		public String toString() {
			return "RouteRelativeStatsFunction";
		}
	}

	public static Function<RouteCompletion, Iterable<RelativeStatistic>> routeRelativeStatsFunction() {
		return RouteRelativeStatsFunction.INSTANCE;
	}

	private enum ActivityTypeRequiredPredicate implements Predicate<RouteCompletion> {
		INSTANCE;
		@Override
		@Nullable
		public boolean apply(@Nullable RouteCompletion completion) {
			return (completion != null) ? completion.activityType() != null : false;
		}

		@Override
		public String toString() {
			return "ActivityTypeRequiredPredicate";
		}
	}

	public static Predicate<RouteCompletion> activityTypeRequiredPredicate() {
		return ActivityTypeRequiredPredicate.INSTANCE;
	}

}
