/**
 * 
 */
package com.aawhere.route;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.DeviceSignalActivitySelector;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.route.event.RouteNameSelector;
import com.aawhere.xml.XmlNamespace;

/**
 * Communicates the details that went into the update of a route.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteUpdateDetails {

	@XmlElement
	private Route updated;
	@XmlElement
	private Route original;
	@XmlElement
	private RouteScoreCalculator scoreCalculator;
	@XmlElement
	private RouteCompletionOrganizer organizer;
	@XmlElement
	private RouteNameSelector nameSelector;
	@XmlElement
	private DeviceSignalActivitySelector courseSelector;
	@XmlElement
	private RouteMergeDetails mergeDetails;

	/** The number of tasks posted to the queue for updating social groups for events. */
	@XmlAttribute
	private Integer socalGroupForEventCompletionsTaskCount;

	/**
	 * Used to construct all instances of RouteUpdateDetails.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteUpdateDetails> {

		public Builder() {
			super(new RouteUpdateDetails());
		}

		public Builder socalGroupForEventCompletionsTaskCount(Integer socalGroupForEventCompletionsTaskCount) {
			building.socalGroupForEventCompletionsTaskCount = socalGroupForEventCompletionsTaskCount;
			return this;
		}

		public Builder original(Route original) {
			building.original = original;
			return this;
		}

		public Builder updated(Route updated) {
			building.updated = updated;
			return this;
		}

		public Builder organizer(RouteCompletionOrganizer organizer) {
			building.organizer = organizer;
			return this;
		}

		public Builder scoreCalculator(RouteScoreCalculator scoreCalculator) {
			building.scoreCalculator = scoreCalculator;
			return this;
		}

		public Builder nameSelector(RouteNameSelector nameSelector) {
			building.nameSelector = nameSelector;
			return this;
		}

		public Builder courseSelector(DeviceSignalActivitySelector courseSelector) {
			building.courseSelector = courseSelector;
			return this;
		}

		@Override
		public RouteUpdateDetails build() {
			RouteUpdateDetails built = super.build();
			return built;
		}

		/**
		 * @param mergeDetails
		 */
		public Builder mergeDetails(RouteMergeDetails mergeDetails) {
			building.mergeDetails = mergeDetails;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteUpdateDetails */
	private RouteUpdateDetails() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return the updated
	 */
	public Route updated() {
		return this.updated;
	}

	/**
	 * @return the original
	 */
	public Route original() {
		return this.original;
	}

	/**
	 * @return the scoreCalculator
	 */
	public RouteScoreCalculator scoreCalculator() {
		return this.scoreCalculator;
	}

	/**
	 * @return the organizer
	 */
	public RouteCompletionOrganizer organizer() {
		return this.organizer;
	}

	/**
	 * @return the nameSelector
	 */
	public RouteNameSelector nameSelector() {
		return this.nameSelector;
	}

	/**
	 * @return the courseSelector
	 */
	public DeviceSignalActivitySelector courseSelector() {
		return this.courseSelector;
	}

}
