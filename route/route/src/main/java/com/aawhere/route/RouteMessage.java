/**
 *
 */
package com.aawhere.route;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.EntityMessage;
import com.aawhere.persist.FilterSort;
import com.aawhere.persist.PersistMessage;
import com.aawhere.swagger.DocSupport;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Provides personalized messages for {@link Route}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlTransient
public enum RouteMessage implements Message {

	ID_NAME(EntityMessage.ID_NAME.getValue()),
	ID_DESCRIPTION(EntityMessage.ID_DESCRIPTION.getValue()),
	/** Used when the name is not provided. */
	UNKNOWN("Unknown"),
	ROUTE_NAME("Route"),
	DEFAULT_ROUTE_NAME("{NAME_FORMAT, select,"//
			+ "START_TH {"//
			+ "{DISTANCE_FORMATTED} {ACTIVITY_TYPE} from {TH_START}"//
			+ "} START_AND_FINISH_TH {"//
			+ "{DISTANCE_FORMATTED} {ACTIVITY_TYPE} from {TH_START} to {TH_FINISH}"//
			// other is required even though we don't need it.
			+ "} other {"//
			+ "{DISTANCE_FORMATTED} {ACTIVITY_TYPE}"//
			+ "}}",//
			Param.NAME_FORMAT,
			Param.DISTANCE_FORMATTED,
			Param.ACTIVITY_TYPE,
			Param.TH_START,
			Param.TH_FINISH),

	ROUTE_DESCRIPTION("A recommended set of roads, paths and trails to follow in a specific direction."),
	NAME_NAME("Route Name"),
	NAME_DESCRIPTION("A common title people used to describe the route."),
	DESCRIPTION_NAME(""),
	DESCRIPTION_DESCRIPTION(""),
	GEO_CELLS_NAME("GeoCells"),
	GEO_CELLS_DESCRIPTION("List of GeoCells for route."),
	TRAILHEAD_NAME("Trailhead"),
	TRAILHEAD_DESCRIPTION("The route's trailhead. The starting location of the route."),
	DATE_CREATED_NAME(EntityMessage.DATE_CREATED_NAME.getValue()), /* */
	DATE_CREATED_DESCRIPTION(EntityMessage.DATE_CREATED_DESCRIPTION.getValue()), /* */
	DATE_UPDATED_NAME(EntityMessage.DATE_UPDATED_NAME.getValue()), /* */
	DATE_UPDATED_DESCRIPTION(EntityMessage.DATE_UPDATED_DESCRIPTION.getValue()), /* */
	/** Used to query an area. */
	SEARCH_AREA("Search Area"),
	ACTIVITY_TYPE_NAME("Activity Type"),
	ACTIVITY_TYPE_DESCRIPTION("The activity type of the route best describes (i.e. Hiking, Biking, etc.)"),
	TRACK_SUMMARY_NAME("TrackSummary"),
	TRACK_SUMMARY_DESCRIPTION("Reference to the TrackSummary associated with this route's Exemplar Activity"),
	MAP_KEY_NAME("Map Key"),
	MAP_KEY_DESCRIPTION("Uniquely identifies which map this route belongs to."),
	SCORE_NAME("Score"),
	SCORE_DESCRIPTION("The score assigned to this route. Useful when sorting routes by relevancy."),
	/** Indicates the score for the requested courseId is not high enough to become a route. */
	SCORE_TOO_LOW(
			"{COURSE_ID} has a score of {SCORE}, but must be higher than {MIN_SCORE} to be a Route.",
			Param.COURSE_ID,
			Param.SCORE,
			Param.MIN_SCORE),
	/** @see CoursesNotRelatedException */
	COURSES_NOT_RELATED("The activity {COURSE_ID} is not yet related to any route.", Param.COURSE_ID),
	/** When looking for a course for a route provided by an application. */
	COURSE_BY_APPLICATION_NOT_FOUND(
			"The route {ROUTE_ID} has no relation to any activities from {APPLICATION}",
			Param.ROUTE_ID,
			Param.APPLICATION);

	private ParamKey[] parameterKeys;
	private String message;

	private RouteMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		/** Activity ID of a potential course */
		COURSE_ID,
		/**  */
		ROUTE_ID,
		/** Any representation of an application */
		APPLICATION,
		/** Route#score indicates preference compared to other routes. */
		SCORE,
		/** The minimum score for this course to be considered a route. */
		MIN_SCORE,
		/** True if the route has a finish trailhead (TH) that is different from the start trailhead */
		NAME_FORMAT,
		/** Distance formatted with unit (example: 5.1 mi or 5.1 miles) */
		DISTANCE_FORMATTED,
		/** The activity type */
		ACTIVITY_TYPE,
		/** The name (Message) of the start trailhead */
		TH_START,
		/** The name (Message) of the finish trailhead */
		TH_FINISH, ;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static class Doc {
		public static final String DESCRIPTION = "Common paths for Persons to perform Activities.";
		public static final String ALLOWABLE_VALUES = RouteField.Absolute.COURSE_COMPLETION_STATS
				+ DocSupport.Allowable.NEXT + RouteField.Absolute.COMPLETION_STATS + DocSupport.Allowable.NEXT
				+ RouteField.Absolute.SOCIAL_COMPLETION_STATS + DocSupport.Allowable.NEXT
				+ RouteField.Absolute.BOUNDING_BOX + DocSupport.Allowable.NEXT + RouteField.Absolute.START_TRAILHEAD_ID
				+ DocSupport.Allowable.NEXT + RouteField.Absolute.FINISH_TRAILHEAD_ID + DocSupport.Allowable.NEXT
				+ RouteField.Absolute.SOCIAL_GROUP_IDS;
		public static final String SORT_DEFAULT = RouteField.Absolute.SCORE + PersistMessage.Doc.DESCENDING;
		public static final String SORT_ALLOWABLE = SORT_DEFAULT + DocSupport.Allowable.NEXT + FilterSort.NONE_FIELD;

		public static class Id {
			public static final String DESCRIPTION = "Uniquely identifies a Route.";
		}
	}
}
