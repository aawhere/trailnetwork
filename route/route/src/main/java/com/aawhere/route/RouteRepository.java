/**
 *
 */
package com.aawhere.route;

import java.util.Set;

import javax.annotation.Nullable;

import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Repository;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.util.rb.Message;

/**
 * Storage {@link Repository} for {@link Route} available only to friendlies for modifying the
 * datastore.
 * 
 * @author Brian Chapman
 * 
 */
interface RouteRepository
		extends CompleteRepository<Routes, Route, RouteId> {

	/**
	 * @deprecated use {@link #update(RouteId)}
	 * @param route
	 * @param score
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Deprecated
	Route setScore(RouteId routeId, Integer score) throws EntityNotFoundException;

	/**
	 * * @deprecated use {@link #update(RouteId)} Provided the {@link RouteId} this will update the
	 * aliases to include the given aliases with those already stored.
	 * 
	 * @see #aliasesSet(RouteId, Set)
	 * 
	 * @param id
	 * @param aliases
	 * @throws EntityNotFoundException
	 */
	@Deprecated
	Route aliasesAdded(RouteId id, Set<RouteId> aliases) throws EntityNotFoundException;

	/**
	 * Over-writes aliases to match that which is given. * @deprecated use {@link #update(RouteId)}
	 * 
	 * @see #aliasesAdded(RouteId, Set)
	 * 
	 * @param id
	 * @param aliases
	 * @return
	 * @throws EntityNotFoundException
	 */
	Route aliasesSet(RouteId id, Set<RouteId> aliases) throws EntityNotFoundException;

	/**
	 * Provided the route this will set the mainId that is the representative for it. * @deprecated
	 * use {@link #update(RouteId)}
	 * 
	 * @param id
	 * @param mainId
	 * @return
	 * @throws EntityNotFoundException
	 */
	Route setMainId(RouteId id, RouteId mainId) throws EntityNotFoundException;

	/**
	 * The number of alternates if this is a main. Null if it is not a main. * @deprecated use
	 * {@link #update(RouteId)}
	 * 
	 * @param id
	 * @param alternateCount
	 * @return
	 * @throws EntityNotFoundException
	 */
	Route setAlternateCount(RouteId id, @Nullable Integer alternateCount) throws EntityNotFoundException;

	/**
	 * Used to update the name of a route that has been previously created * @deprecated use
	 * {@link #update(RouteId)}
	 * 
	 * @param id
	 * @param name
	 * @return
	 * @throws EntityNotFoundException
	 */
	Route setName(RouteId id, Message name) throws EntityNotFoundException;

	/**
	 * Assigns the start to the given id * @deprecated use {@link #update(RouteId)}
	 * 
	 * @param id
	 * @param start
	 * @return
	 * @throws EntityNotFoundException
	 */
	Route setStart(RouteId id, TrailheadId start) throws EntityNotFoundException;

	/**
	 * This will update the persistent {@link Route#finish()}. * @deprecated use
	 * {@link #update(RouteId)}
	 * 
	 * @param id
	 * @param bestId
	 * @return the updated route
	 */
	Route setFinish(RouteId id, TrailheadId bestId) throws EntityNotFoundException;

	/**
	 * Thie indication that the route has completions the require updating of rank and order. * @deprecated
	 * use {@link #update(RouteId)}
	 * 
	 * @param routeId
	 * @param isStale
	 * @return
	 * @throws EntityNotFoundException
	 */
	Route setStale(RouteId routeId, Boolean isStale) throws EntityNotFoundException;

	/**
	 * Provides a connected builder that will persist upon calling the {@link Route.Builder#build()}
	 * .
	 * 
	 * @param routeId
	 * @return
	 * @throws EntityNotFoundException
	 */
	Route.Builder update(RouteId routeId) throws EntityNotFoundException;

}
