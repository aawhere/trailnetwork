/**
 * 
 */
package com.aawhere.route;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.xml.XmlNamespace;

/**
 * Placeholder allowing for specific format transformation of routes into points/polylines that
 * represent the routes. This returns null so nothing will be produced. Your Register your prefered
 * adapter during runtime using JAXB standards.
 * 
 * @author aroller
 * 
 */
public class RoutePointsXmlAdapter
		extends XmlAdapter<RoutePointsXmlAdapter.Tracks, Collection<Route>> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Collection<Route> unmarshal(Tracks v) throws Exception {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Tracks marshal(Collection<Route> v) throws Exception {
		return null;
	}

	/**
	 * Simple wrapper since JAXB doesn't like interfaces to be the result of adaptation. Annoying!
	 * It does give us a chance to place the media type though.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Tracks {
		public final String mediaType;
		public final Collection<RoutePoints> routePoints;

		Tracks() {
			this(null, null);
		}

		/**
		 * @param mediaType
		 *            the content type explaining what will be found in the
		 *            {@link RoutePoints#points}.
		 * 
		 * @param routePoints
		 *            the details of the formatted points referenced by the route id.
		 */
		public Tracks(String mediaType, Collection<RoutePoints> routePoints) {
			super();
			this.mediaType = mediaType;
			this.routePoints = routePoints;
		}

	}

}