/**
 * 
 */
package com.aawhere.route;

import javax.annotation.Nullable;

import com.aawhere.activity.ActivityId;

import com.google.common.base.Function;

/**
 * Provides the {@link ActivityId} courseId from Route.
 * 
 * @author aroller
 * 
 */
public class RouteToCourseIdFunction
		implements Function<Route, ActivityId> {

	public static RouteToCourseIdFunction build() {
		return new RouteToCourseIdFunction();
	}

	/** Use {@link Builder} to construct RouteToCourseIdFunction */
	private RouteToCourseIdFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public ActivityId apply(@Nullable Route input) {
		return input.courseId();
	}

}
