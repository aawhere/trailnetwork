/**
 * 
 */
package com.aawhere.route;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.spatial.ActivitySpatialRelation;
import com.aawhere.activity.spatial.ActivitySpatialRelationId;
import com.aawhere.activity.spatial.ActivitySpatialRelationsUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.trailhead.Trailheads;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

/**
 * A user story specializing in looking at common routes that start and finish from the same
 * {@link Trailheads} and follow similar paths.
 * 
 * The goal is to reduce the number of routes that look the same on the map, but have small
 * alterations that keep them from being part of the same timing reciprocal group.
 * 
 * The result is a route that represents the group. This is done through popularity (score). The
 * others, if similar enough, will be identified as alternates off the main route. Take china camp
 * for example. There is one main route that everyone does...almost exclusively in the same
 * direction. If a route is created in the opposite direction then that would be an alternate =
 * opposite direction. Often we will ride off the main loop near the top bridge and take the trail
 * to the road, then we return. That would be an alternate off the main route qualified as a bit
 * longer. Sometimes we continue from the top branch and continue up to the Nike missile tower and
 * return back to the main loop. That is a bit longer and may be considered it's own route or an
 * alternate dependening upon it's popularity and % deviation. Back near the road sometimes people
 * bail from the bottom trails and take the road back to the trailhead. These would be considered
 * alternates as well.
 * 
 * Notice: This will mutate the routes updating the score and main id for those that need it. Those
 * mutations will be placed in {@link #mutated()} and should be updated in the repository. Make sure
 * you provide good quality routes.
 * 
 * TN-429
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class AlternateRouteGroupOrganizer {

	/**
	 * Used to construct all instances of AlternateRouteGroupOrganizer.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<AlternateRouteGroupOrganizer> {
		private Function<ActivitySpatialRelationId, ActivitySpatialRelation> spatialProvider;
		/**
		 * The routes to be considered...they must be provided in order of highest score first.
		 * 
		 */
		private Iterable<Route> routes;
		private Ratio minOverlapForAlternate;
		private Integer minNumberOfCoursesToScore;

		public Builder() {
			super(new AlternateRouteGroupOrganizer());
		}

		public Builder routes(Iterable<Route> routes) {
			this.routes = routes;
			return this;
		}

		/** @see #minNumberOfCoursesToScore */
		public Builder minNumberOfCoursesToScore(Integer minNumberOfCoursesToScore) {
			this.minNumberOfCoursesToScore = minNumberOfCoursesToScore;
			return this;
		}

		public Builder spatialProvider(Function<ActivitySpatialRelationId, ActivitySpatialRelation> spatialProvider) {
			this.spatialProvider = spatialProvider;
			return this;
		}

		public Builder minOverlapForAlternate(Ratio ratio) {
			this.minOverlapForAlternate = ratio;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("routes", this.routes);
			Assertion.exceptions().notNull("spatialProvider", this.spatialProvider);
			Assertion.exceptions().notNull("minOverlapForAlternate", this.minOverlapForAlternate);

		}

		@Override
		public AlternateRouteGroupOrganizer build() {
			AlternateRouteGroupOrganizer built = super.build();
			organize(built);
			built.mutated = Collections.unmodifiableSet(built.mutated);
			built.groups = Collections.unmodifiableList(built.groups);
			return built;
		}

		/**
		 * @param built
		 */
		private void organize(AlternateRouteGroupOrganizer built) {
			// the route is the alternate, the route id is the main it's an alternate for
			built.groups = new ArrayList<>();
			// the repeat queue that loses at least one route per iteration (as the chosen main)
			// routes not the main or alternate is added back into this
			final LinkedList<Route> possibleMains = Lists.newLinkedList(this.routes);
			// FIXME:Remove this log statement
			LoggerFactory.getLogger(getClass()).warning(possibleMains + " being organized into groups");
			// hopefully they came in order, but if not this will do it...and will break ties by
			// ordering by completions
			Collections.sort(possibleMains, RouteUtil.highScoreComparator());

			// iterate through all routes...each either being determined a main or an alternate for
			// a main
			// a main is picked by being the most popular or those remaining
			// an alternate is picked if it is reciprocal within tolerance
			// the most popular becomes a main and all others are possible alternates
			Route mostPopularRemaining;
			while ((mostPopularRemaining = possibleMains.poll()) != null) {
				organizeGroup(built, possibleMains, mostPopularRemaining);
			}
			updateMainGroups(built);
		}

		/**
		 * Iterates through each group giving the highest scoring member of the group the honor of
		 * representing the others by assigning {@link Route#mainId()} for all routes to the
		 * winner's routeid. Scores are updated to reflect any changes that may have happened to the
		 * route.
		 * 
		 * @param built
		 */
		public void updateMainGroups(AlternateRouteGroupOrganizer built) {
			final List<Routes> groups = built.groups();
			for (Routes group : groups) {

				final List<Route> groupRoutesHighestScoreFirst = scoresUpdated(built, group);
				RouteId mainId = null;
				Iterator<Route> iterator = groupRoutesHighestScoreFirst.iterator();
				// every group has at least a main;
				do {
					Route member = iterator.next();
					// first in popularity is the main
					if (mainId == null) {
						mainId = member.getId();
						// main must emit the cumulative score since it represents
						// the whole group when
						// searching by mains
						Integer scoreCumulated = RouteUtil.scoreCumulated(groupRoutesHighestScoreFirst);
						member = Route.mutate(member).mainId(mainId).score(scoreCumulated).build();
						built.mutated.add(member);
					}// else route will manage removing alternate count
					if (!mainId.equals(member.mainId())) {
						member = Route.mutate(member).mainId(mainId).build();
						built.mutated.add(member);
					}
					if (BooleanUtils.isTrue(member.isMain())) {
						final int myself = 1;
						// only significant routes should be reported
						final Integer numberOfAlternates = Math.max(0,
																	Iterables.size(RouteUtil
																			.scoreIsSignficantRoutes(group)) - myself);
						if (!numberOfAlternates.equals(member.alternateCount())) {
							member = Route.mutate(member).alternateCount(numberOfAlternates).build();
							built.mutated.add(member);
						}
					}

				} while (iterator.hasNext());
			}
		}

		public List<Route> scoresUpdated(AlternateRouteGroupOrganizer built, Routes group) {
			Routes.Builder scoredRoutesBuilder = Routes.create();
			// update the scores and add to the new collection
			{
				// re-calculate the score for every route since previous scores
				// may have cumulations
				for (Route route : group) {
					if (route.completionStats() != null) {
						RouteScoreCalculator routeScoreCalculator = RouteScoreCalculator.create().route(route)
								.courseCountMin(this.minNumberOfCoursesToScore).build();
						if (!routeScoreCalculator.score().equals(route.score())) {
							route = Route.mutate(route).score(routeScoreCalculator.score()).build();
							built.mutated.add(route);
						}
					}
					scoredRoutesBuilder.add(route);
				}
			}

			List<Route> groupRoutes = scoredRoutesBuilder.sort().build().getCollection();
			return groupRoutes;
		}

		/**
		 * @param built
		 * @param possibleMains
		 * @param mostPopular
		 */
		private void organizeGroup(AlternateRouteGroupOrganizer built, final LinkedList<Route> possibleMains,
				Route mostPopular) {
			ArrayList<Route> group = new ArrayList<Route>();
			group.add(mostPopular);
			ArrayList<Route> notAlternates = new ArrayList<>();
			Route possibleAlternate;
			while ((possibleAlternate = possibleMains.poll()) != null) {

				ActivitySpatialRelationId spatialId = new ActivitySpatialRelationId(mostPopular.courseId(),
						possibleAlternate.courseId());
				ActivitySpatialRelation relation = spatialProvider.apply(spatialId);
				Ratio overlap = relation.overlap();
				Ratio inverseOverlap;
				if (QuantityMath.create(overlap).greaterThan(minOverlapForAlternate)) {
					// now check the inverse because it may be a lot more than the course
					ActivitySpatialRelationId inverseId = ActivitySpatialRelationsUtil.inverseId(spatialId);
					ActivitySpatialRelation inverseRelation = spatialProvider.apply(inverseId);
					inverseOverlap = inverseRelation.overlap();
					if (QuantityMath.create(inverseRelation.overlap()).greaterThan(minOverlapForAlternate)) {
						group.add(possibleAlternate);
					} else {
						notAlternates.add(possibleAlternate);
					}
				} else {
					notAlternates.add(possibleAlternate);
					inverseOverlap = null;
				}
				final Pair<Ratio, Ratio> spatialRelationPair = Pair.of(overlap, inverseOverlap);
				final Pair<RouteId, Pair<Ratio, Ratio>> possibleAlternateSpatialRelation = Pair.of(possibleAlternate
						.id(), spatialRelationPair);
				built.spatialRelations.put(mostPopular.id(), possibleAlternateSpatialRelation);
			}
			built.groups.add(Routes.create().addAll(group).build());
			possibleMains.addAll(notAlternates);
		}
	}// end Builder

	/**
	 * Used for reporting purposes this provides insight into why a relation may or may not be made.
	 */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Multimap<RouteId, Pair<RouteId, Pair<Ratio, Ratio>>> spatialRelations = HashMultimap.create();

	@XmlElement
	private List<Routes> groups;
	/**
	 * Provides those routes that have been mutated because a property (score, mainId) has been
	 * changed.
	 * 
	 */
	@XmlElement
	private Set<Route> mutated = new HashSet<Route>();

	/** Use {@link Builder} to construct AlternateRouteGroupOrganizer */
	private AlternateRouteGroupOrganizer() {
	}

	public List<Routes> groups() {
		return this.groups;
	}

	public Set<Route> mutated() {
		return mutated;
	}
}
