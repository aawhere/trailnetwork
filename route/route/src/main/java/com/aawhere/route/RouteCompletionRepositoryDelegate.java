/**
 * 
 */
package com.aawhere.route;

import java.util.Map;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.RepositoryDelegate;

/**
 * @author aroller
 * 
 */
public class RouteCompletionRepositoryDelegate
		extends RepositoryDelegate<RouteCompletionRepository, RouteCompletions, RouteCompletion, RouteCompletionId>
		implements RouteCompletionRepository {

	/**
	 * @param repository
	 */
	protected RouteCompletionRepositoryDelegate(RouteCompletionRepository repository) {
		super(repository);
	}

	public RouteCompletion.Builder mutate(RouteCompletionId id) throws EntityNotFoundException {
		return RouteCompletion.mutate(load(id));
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.route.RouteCompletionRepository#setIsCourse(com.aawhere.route.RouteCompletionId,
	 * java.lang.Boolean)
	 */
	@Override
	public RouteCompletion setIsCourse(RouteCompletionId completionId, Boolean isCourse) throws EntityNotFoundException {
		return update(mutate(completionId).isCourse(isCourse));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteCompletionRepository#merge(com.aawhere.route.RouteCompletion)
	 */
	@Override
	public RouteCompletion merge(RouteCompletion replacement) {
		RouteCompletion result;
		try {
			RouteCompletion existing = load(replacement.id());
			result = RouteCompletionUtil.merged(existing, replacement);

		} catch (EntityNotFoundException e) {
			result = replacement;
		}
		return store(result);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteCompletionRepository#mergeAll(java.lang.Iterable)
	 */
	@Override
	public Map<RouteCompletionId, RouteCompletion> mergeAll(Iterable<RouteCompletion> entities) {
		throw new UnsupportedOperationException("children ids method is not available on general repositories");
	}

}
