/**
 *
 */
package com.aawhere.route;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.set.ListOrderedSet;
import org.apache.commons.lang3.BooleanUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.activity.Activities;
import com.aawhere.activity.ActivitiesFromIdsXmlAdapter;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportUtil;
import com.aawhere.activity.ActivityProvider;
import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityUtil;
import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.activity.ApplicationActivitiesReferenceXmlAdapter;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.app.account.Account;
import com.aawhere.field.DictionaryIndex;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierToIdentifiableXmlAdapter;
import com.aawhere.identity.IdentityManager;
import com.aawhere.joda.time.DurationUtil;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.xml.CountFromIntegerXmlAdapter;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.ServicedBy;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.person.Person;
import com.aawhere.person.PersonAccountProvider;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonProvider;
import com.aawhere.person.group.SocialGroupMembership.PersonXmlAdapter;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventIdProvider;
import com.aawhere.route.event.RouteEventProvider;
import com.aawhere.route.event.RouteEventUtil;
import com.aawhere.track.TrackSummary;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.util.rb.Message;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.xml.XmlNamespace;
import com.aawhere.xml.bind.OptionalPassThroughXmlAdapter;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfFalse;
import com.googlecode.objectify.condition.IfNotNull;
import com.googlecode.objectify.condition.IfNull;
import com.googlecode.objectify.condition.IfTrue;
import com.googlecode.objectify.condition.PojoIf;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Keeps track of all completions from beginning to end of the route. Derived from
 * {@link ActivityLeaderboard} {@link Ranking}, this organizes the information to satisfy the
 * requirements of {@link Route} and the large numbers of completions that may need to be retrieved.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Cache
@Dictionary(domain = RouteCompletion.FIELD.DOMAIN, indexes = { RouteCompletion.SocialOrderIndex.class,
		RouteCompletion.SocialRaceIndex.class, RouteCompletion.SocialCountIndex.class,
		RouteCompletion.PersonCompletionsByStartTimeIndex.class,
		RouteCompletion.ActivityCompletionsByStartTimeIndex.class, RouteCompletion.PersonalTotalForPerson.class })
public class RouteCompletion
		extends StringBaseEntity<RouteCompletion, RouteCompletionId>
		implements SelfIdentifyingEntity, Cloneable, RouteEventIdProvider {
	static final Integer DEFAULT_REPEAT_COUNT = 1;
	static final Integer DEFAULT_REPEAT_TOTAL = 1;
	public static final Integer PERSONAL_BEST_RANK = 1;
	/** WW-288 showed people results is better with 1. */
	public static final Integer MIN_TOTAL_FOR_FAVORITE = 1;

	@ApiModelProperty(value = "The trailhead where the route starts")
	@XmlElement
	@Field(xmlAdapter = StartXmlAdapter.class)
	private TrailheadId startTrailheadId;

	@XmlAttribute(name = FIELD.ROUTE_SCORE)
	@Field(key = FIELD.ROUTE_SCORE)
	private Integer routeScore;

	@XmlElement
	@Field(xmlAdapter = DistanceXmlAdapter.class)
	@XmlJavaTypeAdapter(DistanceXmlAdapter.class)
	@ApiModelProperty("The length of the Route from start to finish.")
	private Length routeDistance;

	/**
	 * The rank, in accordance to completion duration, that this person compares to all other
	 * person's completions of the same {@link #transportMode}. Personal ranks are not included so
	 * only the personal best is counted for the social rank.
	 * 
	 * @see Ranking#getRank()
	 */
	@XmlElement(name = FIELD.SOCIAL_RACE_RANK)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Index({ IfPersonalBest.class })
	@Field(key = FIELD.SOCIAL_RACE_RANK, indexedFor = "TN-572")
	private Integer socialRaceRank;

	/**
	 * The rank, in accordance to completion duration, that this completion compares to all other
	 * completions by this person.
	 * 
	 * @see Ranking#getPersonalRank()
	 * 
	 */
	@XmlElement(name = FIELD.PERSONAL_RACE_RANK)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Field(key = FIELD.PERSONAL_RACE_RANK)
	private Integer personalRaceRank;

	/**
	 * When ordered by date this is the number for the first attempt ever done starting at 1 to the
	 * most recent being the highest number incrementing by 1.
	 * 
	 * @see Ranking#getPersonalAttemptOrder()
	 */
	@Field(key = FIELD.PERSONAL_ORDER)
	@XmlElement(name = FIELD.PERSONAL_ORDER)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	private Integer personalOrder;

	/**
	 * Comparison to others regarding the number of times this route has been completed. First place
	 * is an achievement known as "The Trailblazer".
	 */
	@XmlElement(name = FIELD.SOCIAL_COUNT_RANK)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Index(IfMostRecent.class)
	@Field(key = FIELD.SOCIAL_COUNT_RANK, indexedFor = "TN-572")
	private Integer socialCountRank;

	/**
	 * Numbers which occurrence this completion is compared to all others in order of start time
	 * where 1 is the first and the most recent is the total number of completions.
	 * 
	 */
	@XmlElement(name = FIELD.SOCIAL_ORDER)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Index(IfNotNull.class)
	@Field(key = FIELD.SOCIAL_ORDER, indexedFor = "TN-580")
	private Integer socialOrder;

	/**
	 * the total number of attempts made by the person. This is repeated for every completion for
	 * convenience.
	 * 
	 */
	@Field(key = FIELD.PERSONAL_TOTAL, indexedFor = "TN-701")
	@XmlElement(name = FIELD.PERSONAL_TOTAL)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Index(IfMostRecent.class)
	private Integer personalTotal;

	/**
	 * The duration taken for this completion to go from the route's beginning to end.
	 * TODO:Determine if this index is needed since we can use social rank instead.
	 * 
	 * @see Completion#duration()
	 * */
	@XmlElement(name = FIELD.DURATION)
	@Field(key = FIELD.DURATION)
	private Duration duration;

	/**
	 * The moment when the participant reached the starting line to being the completion of this
	 * route. Social Order is for a single Route while this provides universal order.
	 */
	@XmlElement(name = FIELD.START_TIME)
	@Field(key = FIELD.START_TIME, indexedFor = "WW-118")
	@Index
	private DateTime startTime;

	/**
	 * The person that represents the {@link Account} that comes from the {@link Activity} from
	 * 
	 * @see ActivityTiming#attempt()
	 * @see ActivityReferenceIds#getAccount()
	 */
	@XmlElement(name = FIELD.PERSON_ID)
	@Field(key = FIELD.PERSON_ID, indexedFor = "TN-572", xmlAdapter = RouteCompletion.PersonIdXmlAdapter.class)
	@Index
	private PersonId personId;

	/**
	 * Any {@link #personId()} with a {@link RouteCompletion} with overlapping {@link #interval()}
	 * are put into the same {@link RouteEvent}.
	 * 
	 */
	@XmlElement(name = FIELD.ROUTE_EVENT_ID)
	@Field(key = FIELD.ROUTE_EVENT_ID, indexedFor = "TN-535", xmlAdapter = RouteCompletion.RouteEventIdXmlAdapter.class)
	@Index(IfNotNull.class)
	@IgnoreSave(IfNull.class)
	private RouteEventId routeEventId;

	/**
	 * The activities that completed the route. These are made unique during processing and ordered
	 * by the "best" activity first. Best meaning the best representation of the route based on the
	 * activities provided. Signal quality, number of completions, more popular person. The details
	 * of this completion are always from the first in the list or in some cases a mix of
	 * information from multiple.
	 * 
	 * The activities included in this list may be there for a variety of reasons.
	 * 
	 * <ul>
	 * <li>An exact duplicate was detected...meaning it is an upload of the same exact track log to
	 * the same or different applications.</li>
	 * <li>Two activities from the same person were detected to overlap in time. You can't be in two
	 * places at once, but you can record with multiple devices.</li>
	 * </ul>
	 * 
	 * Notice that the activities in this list may come from different persons. Rather than
	 * complicating things further by listing multiple persons, we will pick the "best" person to
	 * represent the list of activiites. This might be the person with more activities, the person
	 * that has used the device more often, or any other information we can get our hands on to
	 * choose the representative. If there are multiple account this may indicate that those
	 * accounts should actually represent the same person and might encourage in automatic merging.
	 * 
	 * Since the {@link #id()} contains the {@link #activityId()} in it, then this list must not be
	 * changed without proper management of other completions that are represented in this list.
	 * 
	 * Having multiple ids admittedly complicates the management of this completion, but it removes
	 * the noise that is common with people uploading to multiple places. It provides a view of a
	 * person's activities across the web from a single place. It also simplifies queries by only
	 * looking in a single column for existing activities.
	 * 
	 * Purposely naming singluar for better nomenclature to client's perscpective of the most common
	 * scenario at the cost of confusion within this class.
	 * 
	 * @see ActivityTiming#attempt()
	 */
	@Field(key = FIELD.ACTIVITY_ID, parameterType = ActivityId.class, indexedFor = "TN-572",
			xmlAdapter = RouteCompletion.ActivitiesXmlAdapter.class)
	@Index
	private ListOrderedSet<ActivityId> activityId;

	/**
	 * Activity names that have been provided by a person per the rules of
	 * {@link ActivityImportUtil#namedByPerson(com.aawhere.app.ApplicationKey, String)}. Other names
	 * are not provided here. Multiple names may be provided just as {@link #activityIds()} allows
	 * this completion to be represented by multiple activities.
	 * 
	 * This is cached here to avoid reloading activities to discover the names for auto-naming of
	 * routes.
	 * 
	 */
	private List<String> activityNames;

	/**
	 * Indicates this completion duplicate another so it is not participating.
	 * 
	 * @see Builder#duplicates(RouteCompletion)
	 */
	@XmlElement
	@IgnoreSave(IfNull.class)
	private RouteCompletionId duplicate;

	/**
	 * The total distance the participant traveled beyond this route completion. Activity#distance -
	 * Route#distance. This extra distance could be performed before and/or after this route
	 * completion or may even include other route completions. If {@link #isCourse} is true then
	 * this should be almost zero.
	 * 
	 * @see #beyondDuration
	 * @see Route#distance()
	 * @see TrackSummary#getDistance()
	 */
	@XmlElement
	private Length beyondDistance;

	/**
	 * The total duration the participant performed the activity beyond this route completion.
	 * Activity#duration() - {@link #duration()}. If {@link #isCourse} is true then this should be
	 * almost zero.
	 * 
	 * @see #beyondDistance
	 * @see #duration()
	 * @see TrackSummary#getElapsedDuration().
	 */
	@XmlElement
	private Duration beyondDuration;

	/**
	 * The sport this person was performing when completing the route as determined by
	 * {@link RouteCompletionOrganizer}. The sport may be the same as {@link #activityTypeReported}
	 * or it may be translated into more generalized activity types that match
	 * {@link Route#completionStats()} (TN-698).
	 * 
	 * @see #transportMode
	 * */
	@XmlElement(name = FIELD.ACTIVITY_TYPE)
	@Field(key = FIELD.ACTIVITY_TYPE, indexedFor = "WW-127")
	@Index(IfNotNull.class)
	private ActivityType activityType;

	/**
	 * The {@link ActivityType} as reported by {@link Activity#getActivityType()}.
	 * 
	 * @since TN-698
	 */
	@XmlElement
	private ActivityType activityTypeReported;

	/**
	 * Sometimes a route may be repeated in a single {@link #activityId}. This indicates which time
	 * this completion represents. The count starts at #1 and increments in order of date/time.
	 * 
	 * Null indicates {@link #repeatTotal} = 1 which is the 99% so just go null for less storage and
	 * confusion on export. Extra laps will be the exception and stored and exported with clarity.
	 */
	@IgnoreSave(IfDefaultRepeatTotal.class)
	@XmlElement(name = FIELD.REPEAT_COUNT)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Field(key = FIELD.REPEAT_COUNT)
	private Integer repeatCount;

	/**
	 * Indicates the number of times a single participant repeated this route during a single
	 * {@link #activityId()}. Since this is uncommon null indicates total Laps = 1.
	 */
	@IgnoreSave(IfDefaultRepeatTotal.class)
	@XmlElement(name = FIELD.REPEAT_TOTAL)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Field(key = FIELD.REPEAT_TOTAL)
	private Integer repeatTotal;

	/**
	 * Indicates if this completion is a isCourse representation. In other words is this a
	 * {@link ActivityTiming#reciprocal()} or in the {@link ActivityLeaderboard#getReciprocalIds()}.
	 * This completion contributes to the {@link Route#courseCompletionStats()}.
	 */
	@XmlAttribute
	@Field(key = FIELD.IS_COURSE, indexedFor = "TN-572")
	@Index(IfTrue.class)
	// disallows changing your mind about a course. a new route would likely be necessary to do so
	@IgnoreSave(IfFalse.class)
	private Boolean isCourse;

	/**
	 * If this {@link RouteCompletion} {@link #isCourse()}, then the number of
	 * {@link ActivityTiming#attempt()}s that completed this course will be reported. This number
	 * could be right than the actual number of completions since only one completion is counted for
	 * every activity so any {@link RouteCompletion#repeatCount}>1 will not be counted. Null
	 * otherwise.
	 * 
	 */
	@XmlElement(name = FIELD.COURSE_ACTIVITY_COUNT)
	@Field(key = FIELD.COURSE_ACTIVITY_COUNT)
	@IgnoreSave(IfNull.class)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	private Integer courseActivityCount;

	/**
	 * All activities included must have completed at least 1 course, but likely more. For every
	 * completion this will report the number of courses the {@link #activityId()} completed.
	 * 
	 */
	@XmlElement(name = FIELD.COURSES_COMPLETED_COUNT)
	@Field(key = FIELD.COURSES_COMPLETED_COUNT)
	@IgnoreSave(IfNull.class)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	private Integer coursesCompletedCount;

	@Field(key = FIELD.ROUTE_ID, indexedFor = "TN-572", xmlAdapter = RouteCompletion.RouteXmlAdapter.class)
	public RouteId routeId() {
		return (RouteId) getParentId();
	}

	@Override
	protected Class<? extends Identifier<?, ?>> parentIdType() {
		return RouteId.class;
	}

	/** @see #startTrailheadId */
	public TrailheadId startTrailheadId() {
		return this.startTrailheadId;
	}

	@XmlElement(name = RouteField.START_TRAILHEAD)
	@XmlJavaTypeAdapter(StartXmlAdapter.class)
	private TrailheadId getStartTrailheadIdXml() {
		return startTrailheadId();
	}

	/**
	 * @return the routeScore
	 */
	public Integer routeScore() {
		return this.routeScore;
	}

	/**
	 * @return the activityId
	 */
	public ActivityId activityId() {
		return Iterables.getFirst(this.activityId, null);
	}

	/**
	 * a simplifier when you just want the first, and best, activity id to represent this
	 * completion.
	 * 
	 * @see #activityIds()
	 * @return
	 */
	public ActivityId getActivityId() {
		return activityId();
	}

	@XmlElement(name = FIELD.ACTIVITY_ID)
	public List<ActivityId> getActivityIds() {
		return activityIds();
	}

	public List<ActivityId> activityIds() {
		return (this.activityId != null) ? this.activityId.asList() : null;
	}

	/**
	 * @see #activityNames
	 * @return
	 */
	public List<String> activityNames() {
		return this.activityNames;
	}

	@Nullable
	public Boolean activityNamesAvailable() {
		return CollectionUtils.isNotEmpty(activityNames);
	}

	@XmlElement(name = FIELD.ACTIVITIES)
	@XmlJavaTypeAdapter(RouteCompletion.ActivitiesXmlAdapter.class)
	public List<ActivityId> getActivities() {
		return activityIds();
	}

	/**
	 * For XML transformation, provides ApplicationActivityReference for ActivityIds related to this
	 * RouteCompletion.
	 * 
	 * @return
	 */
	@XmlElement(name = FIELD.APPLICATION_REFERENCES)
	@XmlJavaTypeAdapter(ApplicationActivitiesReferenceXmlAdapter.class)
	private List<ActivityId> getActivitiesForApplicationReferenceXml() {
		return activityIds();
	}

	/**
	 * @return the isCourse
	 */
	public Boolean isCourse() {
		// could be null, but we always know the answer since null means false
		return BooleanUtils.isTrue(this.isCourse);
	}

	/**
	 * @return the courseActivityCount
	 */
	public Integer courseActivityCount() {
		return this.courseActivityCount;
	}

	/**
	 * @return the coursesCompletedCount
	 */
	public Integer coursesCompletedCount() {
		return this.coursesCompletedCount;
	}

	/**
	 * @return the startTime
	 */
	public DateTime startTime() {
		return this.startTime;
	}

	@XmlElement
	public DateTime getFinishTime() {
		return finishTime();
	}

	public DateTime finishTime() {
		return startTime.plus(duration);
	}

	public Interval interval() {
		return new Interval(startTime(), duration());
	}

	/**
	 * @return the duration
	 */
	public Duration duration() {
		return this.duration;
	}

	/**
	 * Provides the repeatCount number. Internal storage of null is always returned as
	 * {@link #DEFAULT_REPEAT_COUNT}.
	 * 
	 * @return the repeatCount
	 */
	public Integer repeatCount() {
		return If.nil(this.repeatCount).use(DEFAULT_REPEAT_COUNT);
	}

	/**
	 * Provides the total number of laps, usually 1. Internal storage of null is always returned as
	 * {@link #DEFAULT_REPEAT_TOTAL}.
	 * 
	 * @return
	 */
	public Integer repeatTotal() {
		return If.nil(this.repeatTotal).use(DEFAULT_REPEAT_TOTAL);
	}

	/**
	 * @return the socialRaceRank
	 */
	public Integer socialRaceRank() {
		return this.socialRaceRank;
	}

	public Integer socialCountRank() {
		return this.socialCountRank;
	}

	public Integer socialOrder() {
		return this.socialOrder;
	}

	/**
	 * @return the personalOrder
	 */
	public Integer personalOrder() {
		return this.personalOrder;
	}

	/**
	 * @return the personalTotal
	 */
	public Integer personalTotal() {
		return this.personalTotal;
	}

	/**
	 * @return the personalRaceRank
	 */
	public Integer personalRaceRank() {
		return this.personalRaceRank;
	}

	/**
	 * @return
	 */
	public Boolean isPersonalBest() {
		return PERSONAL_BEST_RANK.equals(this.personalRaceRank);
	}

	/**
	 * @see #isPersonalBest()
	 * @see #isRouteWorthy()
	 * 
	 * @return
	 */
	public Boolean isPersonBestForSignificantRoute() {
		return isRouteWorthy() && isPersonalBest();
	}

	/**
	 * A favorite is a route that has been done more than once. The more times a route has been done
	 * the more favorite that route is, in other words order by Personal Total. So for every route
	 * that a person has completed more than once this will return true when
	 * {@link #personalOrder()} = {@link #personalTotal()} and {@link #personalTotal()} > 1.
	 * 
	 * * TN-758 Only routes with significant score should be included. Reversed for TN-804
	 * 
	 * @return
	 */
	public Boolean isPopularAndMostRecent() {
		return personalTotal != null && personalTotal >= MIN_TOTAL_FOR_FAVORITE && personalOrder.equals(personalTotal);
	}

	/**
	 * @return the personId
	 */
	public PersonId personId() {
		return this.personId;
	}

	/**
	 * Synonym for {@link #personId()}, but also conforms to xml standards so this can provide the
	 * person to the external client in the API using {@link OptionalPassThroughXmlAdapter}.
	 * 
	 * @return
	 */
	@XmlJavaTypeAdapter(RouteCompletion.PersonIdXmlAdapter.class)
	@XmlElement(name = FIELD.PERSON)
	public PersonId getPerson() {
		return personId();
	}

	/**
	 * @return the activityType
	 */
	public ActivityType activityType() {
		return this.activityType;
	}

	public Length routeDistance() {
		return routeDistance;
	}

	/**
	 * @return the activityTypeReported
	 */
	public ActivityType activityTypeReported() {
		return this.activityTypeReported;
	}

	/**
	 * @return the beyondDistance
	 */
	public Length beyondDistance() {
		return this.beyondDistance;
	}

	/**
	 * @return the beyondDuration
	 */
	public Duration beyondDuration() {
		return this.beyondDuration;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@Override
	@Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM, indexedFor = "TN-362")
	public RouteCompletionId getId() {
		return super.getId();
	}

	/**
	 * Explicit call for {@link #getParentId()}
	 * 
	 */
	@XmlElement(name = RouteCompletionField.ROUTE_ID)
	public RouteId getRouteId() {
		return routeId();
	}

	@XmlElement(name = RouteField.DOMAIN)
	@XmlJavaTypeAdapter(RouteCompletion.RouteXmlAdapter.class)
	public RouteId getRouteXml() {
		return routeId();
	}

	/**
	 * @return the routeEventId
	 */
	@XmlJavaTypeAdapter(RouteCompletion.RouteEventIdXmlAdapter.class)
	public RouteEventId getRouteEvent() {
		return this.routeEventId;
	}

	/**
	 * @return the routeEventId
	 */
	@Override
	public RouteEventId routeEventId() {
		return this.routeEventId;
	}

	/**
	 * Used to construct all instances of RouteCompletion.
	 */
	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<RouteCompletion, Builder, RouteCompletionId> {

		@Deprecated
		private PersonAccountProvider personProvider;
		@Deprecated
		private ActivityProvider activityProvider;
		private Completion completion;
		private Activity activity;
		private Route route;

		public Builder() {
			super(new RouteCompletion());
		}

		Builder(RouteCompletion mutant) {
			super(mutant);
		}

		/**
		 * @deprecated use {@link #person(PersonId)}
		 * @param personProvider
		 * @return
		 */
		@Deprecated
		public Builder personProvider(PersonAccountProvider personProvider) {
			this.personProvider = personProvider;
			return this;
		}

		/**
		 * Making available for use in {@link RouteCompletionOrganizer}.
		 * 
		 * @see com.aawhere.persist.BaseEntity.Builder#id()
		 */
		@Override
		protected RouteCompletionId id() {
			return super.id();
		}

		/**
		 * Assigns this activity id to be the only one. Call add when the intention is otherwise.
		 */
		private Builder activity(ActivityId activityId) {
			building.activityId = null;
			return activityAdded(activityId);
		}

		/**
		 * Indicates this is not participating because it duplicate another RouteCompletion. This
		 * remains available for reference and debugging purposes, but it should not participate
		 * directly in completion boards instead participating indirectly through
		 * {@link #activityAdded(ActivityId)}
		 * 
		 * @return
		 */
		public Builder duplicates(RouteCompletionId duplicate) {
			building.duplicate = duplicate;
			unparticipate();
			return this;
		}

		/**
		 * Removes those items in
		 * 
		 */
		void unparticipate() {
			building.socialCountRank = null;
			building.socialOrder = null;
			building.socialRaceRank = null;
			building.personalOrder = null;
			building.personalRaceRank = null;
			building.personalTotal = null;
			building.activityId = new ListOrderedSet<ActivityId>();
			building.activityId.add(id().activityId());
			building.routeScore = null;
		}

		/**
		 * @param type
		 * @return
		 */
		public Builder activityType(ActivityType type) {
			building.activityType = type;
			return this;
		}

		public Builder activityTypeReported(ActivityType type) {
			building.activityTypeReported = type;
			return this;
		}

		public Builder activity(Activity activity) {
			this.activity = activity;
			activity(activity.id());
			if (ActivityImportUtil.namedByPerson(activity)) {
				activityNameAdded(activity.getName());
			}
			if (building.activityTypeReported == null) {
				activityTypeReported(activity.getActivityType());
			}
			if (building.duration == null) {
				duration(activity.getTrackSummary().getElapsedDuration());
			}
			if (building.startTime == null) {
				startTime(activity.getTrackSummary().getStartTime());
			}

			return this;
		}

		Builder activityNameAdded(Message name) {
			String string = name.toString();
			if (building.activityNames == null) {
				activityName(string);
			} else {
				building.activityNames.add(string);
			}
			return this;

		}

		/**
		 * Resets the name list and assigns the given as the only name. This provides no filtering
		 * of default names which is done by {@link #activity(Activity)}.
		 * 
		 * @param name
		 * @return
		 */
		Builder activityName(String name) {
			building.activityNames = new ArrayList<String>();
			if (ActivityUtil.nameKnown(name)) {
				building.activityNames.add(name);
			}
			return this;
		}

		ActivityId activityId() {
			return building.activityId();
		}

		/**
		 * @param person
		 * @return
		 */
		public Builder person(Person person) {
			return person(person.id());
		}

		/**
		 * @param personId
		 * @return
		 */
		public Builder person(final PersonId personId) {
			building.personId = personId;
			return this;
		}

		Builder personalRaceRank(final Integer rank) {
			building.personalRaceRank = rank;
			return this;
		}

		Builder personalTotal(final Integer total) {
			building.personalTotal = total;
			return this;
		}

		Builder personalOrder(final Integer order) {
			building.personalOrder = order;
			return this;
		}

		Builder socialRaceRank(final Integer rank) {
			building.socialRaceRank = rank;
			return this;
		}

		Builder socialOrder(final Integer order) {
			building.socialOrder = order;
			return this;
		}

		Builder socialCountRank(final Integer rank) {
			building.socialCountRank = rank;
			return this;
		}

		public Builder duration(final Duration duration) {
			building.duration = duration;
			return this;
		}

		public Builder startTime(DateTime startTime) {
			building.startTime = startTime;
			return this;
		}

		/**
		 * @deprecated use {@link #activity(Activity)}
		 * @param activityProvider
		 * @return
		 */
		@Deprecated
		public Builder activityProvider(ActivityProvider activityProvider) {
			this.activityProvider = activityProvider;
			return this;
		}

		/**
		 * @param count
		 */
		public Builder courseActivityCount(Integer count) {
			building.courseActivityCount = count;
			return this;
		}

		public Builder coursesCompletedCount(Integer coursesCompletedCount) {
			building.coursesCompletedCount = coursesCompletedCount;
			return this;
		}

		/**
		 * 
		 * @param route
		 * @return
		 */
		public Builder route(Route route) {
			this.route = route;
			if (route.start() != null) {
				building.startTrailheadId = route.start();
			}
			if (route.score() != null && building.routeScore == null) {
				routeScore(route.score());
			}
			if (route.distance() != null && building.routeDistance == null) {
				routeDistance(route.distance());
			}
			parentId(route.id());

			return this;
		}

		public Builder routeScore(Integer routeScore) {
			building.routeScore = routeScore;
			return this;
		}

		public Builder routeDistance(Length routeDistance) {
			building.routeDistance = routeDistance;
			return this;
		}

		public Builder completion(Completion completion) {
			this.completion = completion;
			return this;
		}

		/**
		 * @see IfDefaultRepeatTotal
		 * @param repeatCount
		 * @return
		 */
		public Builder repeatCount(Integer repeatCount) {
			if (building.repeatCount != null) {
				// repeatCount is part of the id so changing it is not allowed
				Assertion.exceptions().eq(building.repeatCount, repeatCount);
			}
			building.repeatCount = repeatCount;
			return this;
		}

		/**
		 * @see IfDefaultRepeatTotal
		 * @param repeatTotal
		 * @return
		 */
		public Builder repeatTotal(Integer repeatTotal) {

			building.repeatTotal = repeatTotal;
			return this;
		}

		public Builder isCourse(Boolean course) {
			building.isCourse = course;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();

			Assertion.exceptions().assertTrue("route", hasParentId());

		}

		/** Noun for the verb */
		public RouteCompletion routeCompletion() {
			return build();
		}

		public Builder prepare() {
			// calling validate manually so we may process all since repeatCount is needed for the
			// id
			validate();

			if (completion != null) {

				ActivityTiming timing = completion.timing();
				if (this.activity == null) {
					try {
						this.activity = this.activityProvider.getActivity(timing.attempt().getActivityId());
					} catch (EntityNotFoundException e1) {
						throw ToRuntimeException.wrapAndThrow(e1);
					}
				}
				activity(activity);

				building.isCourse = BooleanUtils.isTrue(timing.reciprocal());

				// extra distance traveled by the activity or zero if route is longer.
				building.beyondDistance = QuantityMath.create(activity.getTrackSummary().getDistance())
						.minus(this.route.distance()).max(MeasurementUtil.ZERO_LENGTH).getQuantity();
				building.beyondDuration = DurationUtil.max(	activity.getTrackSummary().getElapsedDuration()
																	.minus(building.duration),
															JodaTimeUtil.ZERO_DURATION);

				building.duration = completion.duration();
				building.startTime = completion.start().getTimestamp();

				if (building.personId == null) {
					try {
						building.personId = this.personProvider.person(completion.timing().attempt().getAccountId())
								.id();
					} catch (EntityNotFoundException e) {
						LoggerFactory.getLogger(getClass()).severe(building.activityId
								+ " has a reference to a bad account" + e.getMessage());
						throw ToRuntimeException.wrapAndThrow(e);
					}
				}

				this.completion = null;
			} else {
				Assertion.assertNotEmpty("activityIds", building.activityId);
				Assertion.exceptions().notNull("personId", building.personId);
				Assertion.exceptions().notNull("duration", building.duration);

			}

			if (!hasId()) {
				setId(new RouteCompletionId(building.routeId(), building.activityId(), building.repeatCount));
			}
			return this;
		}

		@Override
		public RouteCompletion build() {
			prepare();
			return super.build();
		}

		/**
		 * Friendly access for {@link RouteCompletionOrganizer}.
		 * 
		 * @return
		 */
		PersonId personId() {
			return building.personId;
		}

		/**
		 * @param newParent
		 * @return
		 */
		private Builder adopt(Route newParent) {
			return route(newParent).removeId();
		}

		/**
		 * Adds the activity id to those that exist already (if any).
		 * 
		 * @param activityId
		 * @return
		 */
		public Builder activityAdded(ActivityId activityId) {
			if (building.activityId == null) {
				building.activityId = new ListOrderedSet<ActivityId>();
			}
			building.activityId.add(activityId);
			return this;
		}

		/**
		 * @param eventId
		 * @return
		 */
		public Builder routeEventId(RouteEventId eventId) {
			building.routeEventId = eventId;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteCompletion */
	private RouteCompletion() {
	}

	public static Builder create() {
		return new Builder();
	}

	static Builder mutate(RouteCompletion mutant) {
		return new Builder(mutant);
	}

	static Builder clone(RouteCompletion original) {
		try {
			return new Builder((RouteCompletion) original.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * accepts a new parent while inheriting all other attributes besides the id.
	 * 
	 * @param newParent
	 * @param adoptee
	 * @return
	 */
	static Builder adopt(Route newParent, RouteCompletion adoptee) {
		return clone(adoptee).adopt(newParent);
	}

	public static class FIELD {
		public static final String DOMAIN = "routeCompletion";
		public static final String ROUTE_ID = Route.FIELD.ROUTE_ID;
		public static final String PERSON_ID = Person.FIELD.PERSON_ID;
		public static final String PERSON = Person.FIELD.DOMAIN;
		public static final String ACTIVITY_ID = ActivityWebApiUri.ACTIVITY_ID;
		public static final String ACTIVITY_IDS = ActivityWebApiUri.ACTIVITY_IDS;
		public static final String ACTIVITIES = Activities.FIELD.ACTIVITIES;
		public static final String IS_COURSE = "isCourse";
		public static final String COURSE_ACTIVITY_COUNT = "courseActivityCount";
		public static final String COURSES_COMPLETED_COUNT = "coursesCompletedCount";
		public static final String START_TIME = "startTime";
		public static final String DURATION = "duration";
		public static final String ACTIVITY_TYPE = Activity.FIELD.ACTIVITY_TYPE;
		public static final String PERSONAL_ORDER = "personalOrder";
		public static final String PERSONAL_RACE_RANK = "personalRaceRank";
		public static final String PERSONAL_TOTAL = "personalTotal";
		public static final String SOCIAL_ORDER = "socialOrder";
		public static final String SOCIAL_RACE_RANK = "socialRaceRank";
		public static final String SOCIAL_COUNT_RANK = "socialCountRank";
		public static final String REPEAT_COUNT = "repeatCount";
		public static final String REPEAT_TOTAL = "repeatTotal";
		public static final String ROUTE_EVENT_ID = RouteEvent.FIELD.ROUTE_EVENT_ID;
		public static final String APPLICATION_REFERENCE = Activity.FIELD.APPLICATION_REFERENCE;
		public static final String APPLICATION_REFERENCES = "applicationReferences";
		public static final String ROUTE_SCORE = "routeScore";

		public static class KEY {
			public static final FieldKey ACTIVITY_ID = new FieldKey(DOMAIN, FIELD.ACTIVITY_ID);
			public static final FieldKey PERSON_ID = new FieldKey(DOMAIN, FIELD.PERSON_ID);
			public static final FieldKey ROUTE_ID = new FieldKey(DOMAIN, FIELD.ROUTE_ID);
			public static final FieldKey IS_COURSE = new FieldKey(DOMAIN, FIELD.IS_COURSE);
			public static final FieldKey START_TIME = new FieldKey(DOMAIN, FIELD.START_TIME);
			public static final FieldKey ACTIVITY_TYPE = new FieldKey(DOMAIN, FIELD.ACTIVITY_TYPE);
			public static final FieldKey DURATION = new FieldKey(DOMAIN, FIELD.DURATION);
			public static final FieldKey SOCIAL_ORDER = new FieldKey(DOMAIN, FIELD.SOCIAL_ORDER);
			public static final FieldKey SOCIAL_RACE_RANK = new FieldKey(DOMAIN, FIELD.SOCIAL_RACE_RANK);
			public static final FieldKey SOCIAL_COUNT_RANK = new FieldKey(DOMAIN, FIELD.SOCIAL_COUNT_RANK);
			public static final FieldKey PERSONAL_TOTAL = new FieldKey(DOMAIN, FIELD.PERSONAL_TOTAL);
			public static final FieldKey ROUTE_EVENT_ID = new FieldKey(DOMAIN, FIELD.ROUTE_EVENT_ID);

			/** This should be replaced with TN-779 */
			public static class VALUE {
				public static final String SOCIAL_RACE_RANK = DOMAIN + FieldUtils.KEY_DILIM + FIELD.SOCIAL_RACE_RANK;
				public static final String START_TIME = DOMAIN + FieldUtils.KEY_DILIM + FIELD.START_TIME;
				public static final String PERSONAL_TOTAL = DOMAIN + FieldUtils.KEY_DILIM + FIELD.PERSONAL_TOTAL;
				public static final String ROUTE_ID = DOMAIN + FieldUtils.KEY_DILIM + FIELD.ROUTE_ID;
				public static final String PERSON_ID = DOMAIN + FieldUtils.KEY_DILIM + FIELD.PERSON_ID;
			}
		}

		public static class OPTION {
			/** Displays the route in the completion if this is included. */
			public static final String ROUTE = new FieldKey(DOMAIN, Route.FIELD.DOMAIN).getAbsoluteKey();
			public static final String ROUTE_EVENT = new FieldKey(DOMAIN, RouteEvent.FIELD.DOMAIN).getAbsoluteKey();
			public static final String PERSON = new FieldKey(DOMAIN, Person.FIELD.DOMAIN).getAbsoluteKey();
			public static final String TOUCH = new FieldKey(DOMAIN, SystemWebApiUri.TOUCH).getAbsoluteKey();
			public static final String STALE = new FieldKey(DOMAIN, SystemWebApiUri.STALE).getAbsoluteKey();

		}
	}

	/**
	 * indicates if the completion given is the fastest for the person. In other words, is
	 * {@link RouteCompletion#personalRaceRank()} = 1.
	 * 
	 * @author aroller
	 * 
	 */
	public static class IfPersonalBest
			extends PojoIf<RouteCompletion> {
		@Override
		public boolean matchesPojo(RouteCompletion completion) {
			return completion.isPersonalBest();
		}
	}

	/**
	 * Used to index only popular routes, but the most recent to highlight different routes rather
	 * than every completion.
	 * 
	 * @author aroller
	 * 
	 */
	public static class IfMostRecent
			extends PojoIf<RouteCompletion> {

		@Override
		public boolean matchesPojo(RouteCompletion completion) {
			return completion.isPopularAndMostRecent();
		}

	}

	/** Avoids indexing parts of completions when the route score is not significant */
	public static class IfRouteIsWorthy
			extends PojoIf<RouteCompletion> {

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.condition.If#matchesPojo(java.lang.Object)
		 */
		@Override
		public boolean matchesPojo(RouteCompletion routeCompletion) {
			return routeCompletion.isRouteWorthy();
		}

	}

	public static class IfDefaultRepeatTotal
			extends PojoIf<RouteCompletion> {

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.condition.If#matchesPojo(java.lang.Object)
		 */
		@Override
		public boolean matchesPojo(RouteCompletion completion) {
			boolean matches = false;
			if (completion != null) {
				matches = DEFAULT_REPEAT_TOTAL.equals(completion.repeatTotal);
			}
			return matches;
		}

	}

	/**
	 * Composite index used for combination of filters provided for various views of completions
	 * (Explore,Social,Race).
	 * 
	 * 
	 * @author aroller
	 * 
	 */
	public static class SocialOrderIndex
			extends DictionaryIndex {
		public SocialOrderIndex() {
			super(FIELD.KEY.ACTIVITY_TYPE, FIELD.KEY.PERSON_ID, FIELD.KEY.SOCIAL_ORDER);
		}
	}

	public static class SocialRaceIndex
			extends DictionaryIndex {
		public SocialRaceIndex() {
			super(FIELD.KEY.ACTIVITY_TYPE, FIELD.KEY.PERSON_ID, FIELD.KEY.ROUTE_EVENT_ID, FIELD.KEY.SOCIAL_RACE_RANK);
		}
	}

	public static class SocialCountIndex
			extends DictionaryIndex {
		public SocialCountIndex() {
			super(FIELD.KEY.PERSON_ID, FIELD.KEY.SOCIAL_COUNT_RANK);
		}
	}

	public static class PersonalTotalForPerson
			extends DictionaryIndex {
		public PersonalTotalForPerson() {
			super(FIELD.KEY.PERSON_ID, FIELD.KEY.PERSONAL_TOTAL);
		}
	}

	/**
	 * Allows for the listing of completions for a person sorting by most recent. WW-118
	 * 
	 * @author aroller
	 * 
	 */
	public static class PersonCompletionsByStartTimeIndex
			extends DictionaryIndex {
		public PersonCompletionsByStartTimeIndex() {
			super(FIELD.KEY.PERSON_ID, FIELD.KEY.START_TIME);
		}
	}

	/**
	 * WW-120 provides sigificant completions for an activity. Notice: This is in a zig zag index
	 * with {@link PersonCompletionsByStartTimeIndex}, but such a combination provides no practical
	 * purpose.
	 * */
	public static class ActivityCompletionsByStartTimeIndex
			extends DictionaryIndex {
		public ActivityCompletionsByStartTimeIndex() {
			super(FIELD.KEY.ACTIVITY_ID, FIELD.KEY.START_TIME);
		}
	}

	private static final long serialVersionUID = -3442651141380921848L;

	/**
	 * @return
	 */
	public RouteCompletionId duplicateId() {
		return duplicate;
	}

	/**
	 * @return
	 */
	public Boolean isRouteWorthy() {
		return this.routeScore != null && RouteUtil.scoreIsSignificant(this.routeScore);
	}

	/** Passes through route with a refresh if capable. */
	@Singleton
	public static class RouteXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<Route, RouteId> {

		public RouteXmlAdapter() {
			super();
		}

		@Inject
		public RouteXmlAdapter(RouteProvider provider) {
			super(provider);
		}

		/** This accepts ids since the field it's associated with is the id to support bulk loading. */
		public RouteXmlAdapter(Function<RouteId, Route> provider) {
			super(provider);
		}
	}

	@Singleton
	public static class RouteEventIdXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<RouteEvent, RouteEventId> {
		public RouteEventIdXmlAdapter() {
		}

		public RouteEventIdXmlAdapter(Function<RouteEventId, RouteEvent> provider) {
			super(provider);
		}

		@Inject
		public RouteEventIdXmlAdapter(RouteEventProvider provider) {
			super(RouteEventUtil.providerFunction(provider));
		}
	}

	public static class PersonIdXmlAdapter
			extends PersonXmlAdapter {
		public PersonIdXmlAdapter() {
		}

		public PersonIdXmlAdapter(Function<PersonId, Person> provider, IdentityManager identityManager) {
			super(provider, identityManager);
		}

		@Inject
		public PersonIdXmlAdapter(PersonProvider provider, IdentityManager identityManager) {
			super(provider, identityManager);
		}
	}

	@Singleton
	public static class ActivitiesXmlAdapter
			extends ActivitiesFromIdsXmlAdapter {

		public ActivitiesXmlAdapter() {
			super();
		}

		@Inject
		public ActivitiesXmlAdapter(ActivityProvider provider) {
			super(provider);
		}

		public ActivitiesXmlAdapter(Function<ActivityId, Activity> provider) {
			super(provider);
		}

	}

	@Singleton
	@ServicedBy(RouteTrailheadProvider.class)
	public static class StartXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<Trailhead, TrailheadId> {
		public StartXmlAdapter() {
		}

		public StartXmlAdapter(Function<TrailheadId, Trailhead> provider) {
			super(provider);
		}

		@Inject
		public StartXmlAdapter(RouteTrailheadProvider provider) {
			super(provider.idFunction());
		}
	}

	@Singleton
	public static class DistanceXmlAdapter
			extends OptionalPassThroughXmlAdapter<RouteStats> {
		public DistanceXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public DistanceXmlAdapter(Show show) {
			super(show);
		}
	}

}
