/**
 * 
 */
package com.aawhere.route.event;

import java.util.Map.Entry;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.person.PersonId;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.route.RouteCompletion;
import com.aawhere.route.RouteCompletionUtil;
import com.aawhere.route.RouteCompletions;

/**
 * Given the Event, the {@link RouteCompletions} that completed the event, this provides the
 * information needed to create social groups.
 * 
 * @author aroller
 * 
 */
public class RouteEventSocialGroupOrganizer {

	private RouteEvent event;
	private SocialGroup socialGroup;

	/**
	 * Used to construct all instances of RouteEventSocialGroupOrganizer.
	 */
	public static class Builder
			extends ObjectBuilder<RouteEventSocialGroupOrganizer> {

		private Iterable<RouteCompletion> completions;

		public Builder() {
			super(new RouteEventSocialGroupOrganizer());
		}

		public Builder event(RouteEvent event) {
			building.event = event;
			return this;
		}

		public Builder socialGroup(SocialGroup socialGroup) {
			building.socialGroup = socialGroup;
			return this;
		}

		public Builder completions(Iterable<RouteCompletion> completions) {
			this.completions = completions;
			return this;
		}

		@Override
		public RouteEventSocialGroupOrganizer build() {
			RouteEventSocialGroupOrganizer built = super.build();
			SocialGroup.Builder socialGroupBuilder;
			if (built.socialGroup == null) {
				socialGroupBuilder = SocialGroup.create();
			} else {
				socialGroupBuilder = SocialGroup.clone(built.socialGroup);
			}

			socialGroupBuilder.category(RouteEventField.DOMAIN);
			socialGroupBuilder.context(built.event.id());

			int membershipCount = 0;
			Iterable<Entry<PersonId, Integer>> personalTotalsIdentified = RouteCompletionUtil
					.personalTotalsIdentified(completions);
			for (Entry<PersonId, Integer> entry : personalTotalsIdentified) {

				membershipCount++;
			}

			return built;
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct RouteEventSocialGroupOrganizer */
	private RouteEventSocialGroupOrganizer() {
	}

}
