/**
 * 
 */
package com.aawhere.route;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.activity.ActivityProvider;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadUtil;
import com.aawhere.trailhead.Trailheads;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;

/**
 * Given the organization of the {@link RouteProximityRelator} and knowledge about existing
 * trailheads this will recommend modifications to the trailhead management to modify or merge
 * existing trailheads or create one if none are suitable.
 * 
 * {@link #outOfBounds()} identifies those Routes that are outside the center of the search area
 * that provided the routes and trailheads. Route resolving close to the edge could result in an
 * incomplete set of routes when choosing trailheads since the trailhead radius may exceed the
 * bounds. The safety zone is in the middle of the search area so the {@link #outOfBounds()} items
 * should be considered in a focused search.
 * 
 * More Info:
 * 
 * https://aawhere.jira.com/wiki/display/AAWHERE/Trailhead
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteTrailheadChooser {

	/**
	 * Used to construct all instances of RouteTrailheadChooser.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteTrailheadChooser> {

		private ActivityProvider activityProvider;
		private Length maxTrailheadRadius;

		public Builder() {
			super(new RouteTrailheadChooser());
		}

		public Builder routes(Iterable<Route> routes) {
			building.routes = Routes.create().addAll(routes).build();
			return this;
		}

		public Builder trailheads(Iterable<Trailhead> trailheads) {
			building.trailheads = Trailheads.create().addAll(trailheads).build();
			return this;
		}

		public Builder maxTrailheadRadius(Length max) {
			this.maxTrailheadRadius = max;
			return this;
		}

		public Builder searchArea(BoundingBox searchArea) {
			building.searchArea = searchArea;
			return this;
		}

		public Builder activityProvider(ActivityProvider activityProvider) {
			this.activityProvider = activityProvider;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("trailheads", building.trailheads);
			Assertion.exceptions().notNull("routes", building.routes);
			Assertion.exceptions().notNull("activityProvider", this.activityProvider);
			Assertion.exceptions().notNull("searchArea", building.searchArea);

		}

		@Override
		public RouteTrailheadChooser build() {
			RouteTrailheadChooser built = super.build();
			Set<Route> unrelated = Sets.newHashSet(built.routes);
			GeoCoordinate searchLocation = built.searchArea.getCenter();
			// first division makes it a radius
			// second division makes it half the size of the search area to limit edge cases
			QuantityMath<Length> limitRadius = QuantityMath.create(built.searchArea.getDiagonal()).dividedBy(2)
					.dividedBy(2);

			// the goal is to find a trailhead for every route within the search area
			// create a new relator for all unrelated and find the trailhead for that group
			while (!unrelated.isEmpty()) {

				RouteProximityRelator relator = RouteProximityRelator.create().activityProvider(this.activityProvider)
						.maxRadiusOfArea(maxTrailheadRadius).routes(unrelated).build();

				GeoCoordinate location = relator.bestLocation();
				Length distanceAway = GeoMath.length(searchLocation, location);
				// associate a trailhead if within the confidence zone
				if (limitRadius.greaterThanEqualTo(distanceAway)) {
					Trailhead trailhead;
					// always search for those in range to make sure merge candidates are handled
					Set<Trailhead> trailheadsWithinProximity = Sets.newHashSet(TrailheadUtil
							.trailheadsWithinProximity(built.trailheads, location, maxTrailheadRadius));
					// keep the trailhead already assigned if it is in the area
					trailhead = IdentifierUtils.filter(trailheadsWithinProximity, relator.bestRelative().start());
					// too many trailheads within the area. keep the best, merge the rest
					if (trailhead != null && trailheadsWithinProximity.size() > 1) {

						trailheadsWithinProximity.remove(trailhead);
						built.mergeCandidates
								.putAll(trailhead.id(), IdentifierUtils.idsFrom(trailheadsWithinProximity));
					}
					// if trailhead is still null, just choose the nearest
					if (trailhead == null) {
						trailhead = TrailheadUtil.nearest(trailheadsWithinProximity, location);
					}
					Trailhead.Builder trailheadMutator;
					// no existing trailheads to be found within a reasonable distance
					if (trailhead == null) {
						trailheadMutator = Trailhead.create();
					} else {
						trailheadMutator = Trailhead.mutate(trailhead);
					}
					// the location of the selected trailhead is location of the best related routes
					trailheadMutator.location(location);

					// updating trailhead statistics here can cause discrepancies
					// all routes should be organized, but only public routes affect statistics
					// update statistics in one place
					trailhead = trailheadMutator.build();
					boolean added = built.trailheadRelators.put(trailhead, relator);
					if (!added) {
						throw new IllegalStateException(trailhead.id() + " not being allowed to associate to "
								+ relator);
					}
				} else {
					// not in range...log it.
					built.outOfBounds.put(relator, distanceAway);
				}
				// refresh the unrelated guaranteed to be reduced by at minimum 1 each iteration
				unrelated = relator.unrelated();

			}
			// Combine all related routes from all relators and associate to the single trailhead
			for (Trailhead trailhead : built.trailheadRelators.keySet()) {
				List<RouteProximityRelator> relators = built.trailheadRelators.get(trailhead);
				com.aawhere.route.Routes.Builder builder = Routes.create();
				for (RouteProximityRelator relator : relators) {
					builder.addAll(relator.related());
				}
				built.trailheadRelations.put(trailhead, builder.sort().build());
			}
			built.trailheadRelations = Collections.unmodifiableMap(built.trailheadRelations);
			built.outOfBounds = Collections.unmodifiableMap(built.outOfBounds);
			built.mergeCandidates = Multimaps.unmodifiableSetMultimap(built.mergeCandidates);
			built.trailheadRelators = Multimaps.unmodifiableListMultimap(built.trailheadRelators);

			return built;
		}
	}// end Builder

	/** Use {@link Builder} to construct RouteTrailheadChooser */
	private RouteTrailheadChooser() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Provides knowledge of what routes and trailheads are provided so the choosing will not
	 * approach the search area boundaries.
	 */
	@XmlElement
	private BoundingBox searchArea;

	@XmlElement
	private Trailheads trailheads;

	@XmlElement
	private Routes routes;

	/** Those relators that are out of the {@link #searchArea} so no action is recommended. */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Map<RouteProximityRelator, Length> outOfBounds = new HashMap<>();

	/** Maps all the routes to a trailhead to be updated by a service. */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private ListMultimap<Trailhead, RouteProximityRelator> trailheadRelators = LinkedListMultimap.create();

	/** Identifies which trailheads (values) should be merged with the remaining trailhead (key). */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private SetMultimap<TrailheadId, TrailheadId> mergeCandidates = HashMultimap.create();

	/**
	 * a view of {@link #trailheadRelators()} associating the related Routes to across multiple
	 * relators to a single trailhead.
	 */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Map<Trailhead, Routes> trailheadRelations = new HashMap<>();

	/**
	 * @return the mergeCandidates
	 */
	public SetMultimap<TrailheadId, TrailheadId> mergeCandidates() {
		return this.mergeCandidates;
	}

	public Map<Trailhead, Routes> trailheadRelations() {
		return trailheadRelations;
	}

	/**
	 * @return the trailheadRelators
	 */
	public ListMultimap<Trailhead, RouteProximityRelator> trailheadRelators() {
		return this.trailheadRelators;
	}

	@XmlElementWrapper(name = "relators")
	public Collection<RouteProximityRelator> getRelators() {
		return this.trailheadRelators.values();
	}

	/**
	 * @return the outOfBounds
	 */
	public Map<RouteProximityRelator, Length> outOfBounds() {
		return this.outOfBounds;
	}

	public Routes routes() {
		return this.routes;
	}

	/**
	 * Provides all trailheads given to this.
	 * 
	 * @return the trailheads
	 */
	public Trailheads trailheads() {
		return this.trailheads;
	}

	/** Provides any trailheads provided, but reference no routes. */
	@XmlElement
	public Set<TrailheadId> getOrphanTrailheads() {
		Set<TrailheadId> trailheadIds = Sets.newHashSet(TrailheadUtil.trailheadIds(trailheads));
		Set<TrailheadId> relatedTrailheadIds = Sets.newHashSet(TrailheadUtil.trailheadIds(this.trailheadRelators
				.keySet()));
		return Sets.difference(trailheadIds, relatedTrailheadIds);
	}
}
