/**
 * 
 */
package com.aawhere.route.event;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.FilterSort;
import com.aawhere.persist.PersistMessage;
import com.aawhere.swagger.DocSupport;
import com.aawhere.swagger.DocSupport.Allowable;
import com.aawhere.text.format.custom.ClassFormat;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

public enum RouteEventMessage implements Message {

	/** Comments go here. */
	MESSAGE_1("something {FIRST} for {WITH_FORMAT}.", Param.FIRST, Param.WITH_FORMAT);

	private ParamKey[] parameterKeys;
	private String message;

	private RouteEventMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		FIRST, WITH_FORMAT(new ClassFormat());

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static final class Doc {
		/**
		 * Options to expand for RouteCompletion. Preceiding comma allows for nothing selected.
		 * SHouldn't be necessary, but is.
		 */
		public static final String EXPANDS = DocSupport.Allowable.NEXT + RouteEvent.FIELD.KEY.VALUE.ROUTE_ID;
		public static final String ORDER_BY_ALLOWABLE = RouteEventField.Absolute.SCORE + PersistMessage.Doc.DESCENDING
				+ Allowable.NEXT + FilterSort.NONE_FIELD;
		public static final String ORDER_BY_DESCRIPTION = "Sorts by " + RouteEventField.SCORE + " unless "
				+ FilterSort.NONE_FIELD;
	}
}
