/**
 * 
 */
package com.aawhere.route;

import java.util.Map;

import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;

/**
 * @author aroller
 * 
 */
public interface RouteCompletionRepository
		extends CompleteRepository<RouteCompletions, RouteCompletion, RouteCompletionId> {

	/**
	 * Sets the {@link RouteCompletion#isCourse()} property regardless of what already exists.
	 * 
	 * @param completionId
	 * @param isCourse
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteCompletion setIsCourse(RouteCompletionId completionId, Boolean isCourse) throws EntityNotFoundException;

	/**
	 * Calls {@link #store(RouteCompletion)} after merging with an existing completion, if it
	 * exists, using {@link RouteCompletionUtil#merged(RouteCompletion, RouteCompletion)}. If there
	 * is no existing completion then this is simply a store.
	 * 
	 * @param replacement
	 * @return
	 */
	public RouteCompletion merge(RouteCompletion replacement);

	/**
	 * Similar to {@link #merge(RouteCompletion)}, but works in a batch with multiple completions
	 * using {@link #storeAll(Iterable)}. Batch loads any existing that match an incoming and merges
	 * those while simply storing the new.
	 * 
	 * @param all
	 * @return the merged or created entities matching the size of the collection that went in
	 */
	public Map<RouteCompletionId, RouteCompletion> mergeAll(Iterable<RouteCompletion> all);
}
