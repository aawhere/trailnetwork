/**
 * 
 */
package com.aawhere.route;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.trailhead.TrailheadId;

/**
 * Used for updated trailheads, either start or finish, without knowing necessarily which one is
 * being updated. Useful when trailheads change in general.
 * 
 * @author aroller
 * 
 */
public interface RouteTrailheadMutator {
	/**
	 * Modifies the appropriate trailhead for the route.
	 * 
	 * @throws EntityNotFoundException
	 */
	Route setTrailhead(RouteId routeId, TrailheadId trailheadId) throws EntityNotFoundException;
}
