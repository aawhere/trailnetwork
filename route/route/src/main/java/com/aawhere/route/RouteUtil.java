/**
 * 
 */
package com.aawhere.route;

import java.util.HashMap;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityUtil;
import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.IdentifierApplicationDataId;
import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.field.FieldKey;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.number.NumberUtilsExtended;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterUtil;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupUtil;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.util.rb.Message;
import com.aawhere.view.ViewCountId;
import com.aawhere.xml.bind.OptionalPassThroughXmlAdapter;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.BiMap;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;

/**
 * @author aroller
 * 
 */
public class RouteUtil {

	private static final Ordering<Route> SCORE_COMPARATOR = Ordering.natural().reverse().nullsLast()
			.onResultOf(scoreFunction()).compound(courseCompletionCountHighestFirstOrdering())
			.compound(completionCountHighestFirstOrdering());
	/**
	 * Ranks routes based on a series of attributes that help choose the most senior Route, likely
	 * to be used when determing which route to keep around.
	 * <ol>
	 * <li>more aliases means less senior routes aren't as difficult to remove</li>
	 * <li>any score means it's been around long enough to be updated and likely referenced. higher
	 * is better</li>
	 * 
	 * <li>first is better...at least it provides some order</li>
	 * <li>in the unlikely chance of a date tie, this will break the tie and provide consistency</li>
	 * <ol>
	 * 
	 */
	private static final Ordering<Route> SENIORITY_ORDERING = routesWithMoreAliasesFirst()
			.compound(highScoreComparator()).compound(dateCreatedOldestFirst())
			.compound(IdentifierUtils.identifableOrdering());

	/** Given the routes this will return only those that are mains. */
	public static Routes mainsOnly(Routes routes) {
		return Routes.create().addAll(Collections2.filter(routes.all(), RouteIsMainPredicate.build())).build();
	}

	/**
	 * True if the route start/finish at the same point. No other information is considered (such as
	 * loop, out and back, etc).
	 * 
	 * @param route
	 * @return
	 */
	public static Boolean isReturnTrip(Route route) {
		return route.start().equals(route.finish());
	}

	/**
	 * INdicates if the message given is the default or otherwise not provide (null or empty
	 * string).
	 * 
	 * @param message
	 * @return
	 */
	public static Boolean isNameEmpty(Message message) {
		return message == null || RouteMessage.UNKNOWN.equals(message) || StringUtils.isEmpty(message.getValue());
	}

	/**
	 * Returns a comparator that will provide the highest score first. For equal scores the ordering
	 * will go to {@link #courseCompletionCountHighestFirstOrdering()}, then
	 * {@link #completionCountHighestFirstOrdering()}.
	 * 
	 * @return
	 */
	public static Ordering<? super Route> highScoreComparator() {
		return SCORE_COMPARATOR;
	}

	public static Iterable<Route> orderedByHighScore(Iterable<Route> routes) {
		return highScoreComparator().sortedCopy(routesWithScore(routes));
	}

	public static Iterable<RouteId> ids(Iterable<Route> routes) {
		return IdentifierUtils.ids(routes);
	}

	/**
	 * The scores from all the routes are combined and returned as a single, larger score equal to
	 * the sum of all scores.
	 * 
	 * @param routes
	 * @return
	 */
	public static Integer scoreCumulated(Iterable<Route> routes) {
		return NumberUtilsExtended.sum(Iterables.transform(routes, scoreFunction()));
	}

	public static Function<Route, Integer> scoreFunction() {
		return RouteScoreFunction.build();
	}

	public static Predicate<Route> scoreNotNullPredicate() {
		return Predicates.compose(Predicates.notNull(), scoreFunction());
	}

	public static Iterable<TrailheadId> startsIds(Iterable<Route> routes) {
		Function<Route, TrailheadId> function = startIdFunction();
		return Iterables.transform(routes, function);
	}

	/**
	 * @return
	 */
	public static Function<Route, TrailheadId> startIdFunction() {
		Function<Route, TrailheadId> function = new Function<Route, TrailheadId>() {

			@Override
			public TrailheadId apply(Route route) {
				if (route == null) {
					return null;
				}
				return route.start();
			}
		};
		return function;
	}

	/** Routes have course ids, this will produce it. */
	public static Function<Route, ActivityId> courseIdFunction() {
		return RouteToCourseIdFunction.build();
	}

	public static Function<Route, RouteId> routeIdFunction() {
		return IdentifierUtils.idFunction();
	}

	public static Iterable<ActivityId> courseIds(Iterable<Route> routes) {
		return Iterables.transform(routes, courseIdFunction());
	}

	public static BiMap<Route, ActivityId> courseIdsRouteMap(Iterable<Route> routes) {
		return HashBiMap.create(Maps.toMap(routes, courseIdFunction()));
	}

	public static BiMap<RouteId, ActivityId> courseIdsRouteIdMap(Iterable<Route> routes) {
		// TN-712 avoiding adding duplicates since it can happen in an unfinished system
		HashMap<ActivityId, RouteId> activityIdMap = new HashMap<>();
		for (Route route : routes) {
			RouteId duplicate = activityIdMap.put(route.courseId(), route.id());
			if (duplicate != null) {
				// course id in duplicate places
				LoggerFactory.getLogger(RouteUtil.class).warning("TN-712 course " + route.courseId()
						+ " is a course for route " + route.id() + " and " + duplicate);
			}
		}
		return ImmutableBiMap.copyOf(activityIdMap).inverse();
	}

	public static Iterable<Route> isStale(Iterable<Route> routes) {
		return Iterables.filter(routes, isStalePredicate());
	}

	public static Predicate<Route> isStalePredicate() {
		return Predicates.compose(Predicates.equalTo(Boolean.TRUE), isStaleFunction());
	}

	public static Function<Route, Boolean> isStaleFunction() {
		return new Function<Route, Boolean>() {

			@Override
			public Boolean apply(Route input) {
				return (input == null) ? null : input.isStale();
			}
		};
	}

	public static RouteTrailheadMutator startMutator(final RouteRepository repository) {
		return new RouteTrailheadMutator() {

			@Override
			public Route setTrailhead(RouteId routeId, TrailheadId trailheadId) throws EntityNotFoundException {
				return repository.setStart(routeId, trailheadId);
			}
		};
	}

	public static RouteTrailheadMutator finishMutator(final RouteRepository repository) {
		return new RouteTrailheadMutator() {

			@Override
			public Route setTrailhead(RouteId routeId, TrailheadId trailheadId) throws EntityNotFoundException {
				return repository.setFinish(routeId, trailheadId);
			}
		};
	}

	/**
	 * Provides the {@link Route#aliases()}
	 * 
	 * @return
	 */
	public static Function<Route, Set<RouteId>> aliasesFunction() {
		return new Function<Route, Set<RouteId>>() {
			@Override
			public Set<RouteId> apply(Route input) {
				return (input == null) ? null : input.aliases();
			}
		};
	}

	/**
	 * provides the number of {@link Route#aliases()}.
	 * 
	 * @return
	 */
	public static Function<Route, Integer> aliasesCountFunction() {
		return Functions.compose(CollectionUtilsExtended.sizeFunction(), aliasesFunction());
	}

	/** More aliases first. */
	public static Ordering<Route> routesWithMoreAliasesFirst() {
		return Ordering.natural().reverse().onResultOf(aliasesCountFunction());
	}

	/**
	 * Streams the aliases from the routes given without any minimization of duplication. Nulls,
	 * which are common, are filtered out.
	 */
	public static Iterable<RouteId> aliases(Iterable<Route> routes) {
		Iterable<RouteId> routeIds = ids(routes);
		Iterable<RouteId> aliases = Iterables.concat(Iterables.filter(	Iterables.transform(routes, aliasesFunction()),
																		Predicates.notNull()));
		return Iterables.concat(routeIds, aliases);
	}

	/**
	 * @see #aliases(Iterable)
	 * @param routes
	 * @return
	 */
	public static ImmutableSet<RouteId> aliasesUnique(Iterable<Route> routes) {
		return ImmutableSet.<RouteId> builder().addAll(aliases(routes)).build();
	}

	/**
	 * Cumulates all of the stats given in the routes into a single stats representation, like for a
	 * Route Alternate Group or Trailhead.
	 * 
	 * @param routes
	 * @return
	 */
	public static ActivityStats completionCumulationStats(Iterable<Route> routes) {
		Iterable<ActivityStats> stats = Iterables.transform(routes, completionStatsFunction());

		return (stats == null) ? null : ActivityUtil.cumulation(stats);
	}

	public static Function<Route, ActivityStats> completionStatsFunction() {
		return new Function<Route, ActivityStats>() {

			@Override
			public ActivityStats apply(Route input) {
				return (input == null) ? null : input.completionStats();
			}
		};
	}

	/**
	 * Orders the routes by the number of completions.
	 * 
	 * @see #courseCompletionCountHighestFirstOrdering()
	 * @return
	 */
	public static Ordering<Route> completionCountHighestFirstOrdering() {
		Function<Route, Integer> totalCountFunction = Functions.compose(ActivityStats.totalFunction(),
																		completionStatsFunction());
		return Ordering.natural().reverse().nullsLast().onResultOf(totalCountFunction);
	}

	private enum CourseCompletionStatsFunction implements Function<Route, ActivityStats> {
		INSTANCE;
		@Override
		@Nullable
		public ActivityStats apply(@Nullable Route route) {
			return (route != null) ? route.courseCompletionStats() : null;
		}

		@Override
		public String toString() {
			return "CourseCompletionStatsFunction";
		}
	}

	/**
	 * Orders the routes based on the route with the highest course completions first.
	 * 
	 * @see #completionCountHighestFirstOrdering()
	 * @return
	 */
	public static Ordering<Route> courseCompletionCountHighestFirstOrdering() {
		Function<Route, Integer> totalCountFunction = Functions.compose(ActivityStats.totalFunction(),
																		courseCompletionStatsFunction());
		return Ordering.natural().reverse().nullsLast().onResultOf(totalCountFunction);

	}

	public static Function<Route, ActivityStats> courseCompletionStatsFunction() {
		return CourseCompletionStatsFunction.INSTANCE;
	}

	public static BoundingBox boundingBox(Iterable<Route> routes) {
		return BoundingBoxUtil.combinedProviders(routes);
	}

	/** Used to display an embedded route on demand using options. */
	public static class RouteOptionalXmlAdapter
			extends OptionalPassThroughXmlAdapter<Route> {

		/**
		 * Default not to show.
		 */
		public RouteOptionalXmlAdapter() {
		}

		/**
		 * @param show
		 */
		public RouteOptionalXmlAdapter(boolean show) {
			super(show);
		}

	}

	public static Set<RouteId> routeIds(Filter filter) {
		final FieldKey fieldKey = Route.FIELD.KEY.ROUTE_ID;
		return FilterUtil.idsFromFilterConditions(filter, fieldKey, RouteId.class);
	}

	/**
	 * @param routes
	 * @return
	 */
	public static Iterable<Route> routesWithScore(Iterable<Route> routes) {
		return Iterables.filter(routes, scoreNotNullPredicate());
	}

	public static Ordering<Route> dateCreatedOldestFirst() {
		Ordering<Route> dateCreatedOldestFirst = EntityUtil.dateCreatedOldestFirst();
		return dateCreatedOldestFirst;
	}

	/**
	 * Picks the most senior route when trying to pick a best route for merging or when two aliases
	 * are found.
	 * 
	 * Seniority is determined by score first, then by oldest.
	 * 
	 * @see #SENIORITY_ORDERING
	 * 
	 * @param routes
	 * @return
	 */
	@Nullable
	public static Route mostSeniorRoute(@Nonnull Iterable<Route> routes) {
		// pick the best route helps everyone focus on merging into the same reducing aliases
		// first by score if available. if not then oldest first
		Iterable<Route> orderedBySeniority = SENIORITY_ORDERING.sortedCopy(routes);
		Route senior = Iterables.getFirst(orderedBySeniority, null);
		// if sorting by score isn't valuable...pick the oldest
		if (!scoreIsSignificant(senior.score())) {
			orderedBySeniority = EntityUtil.dateCreatedOldestFirst(routes);
			senior = Iterables.getFirst(orderedBySeniority, null);
		}
		return senior;
	}

	/**
	 * @see #viewCountId(RouteId)
	 * @return
	 */
	public static Function<RouteId, ViewCountId> viewCountIdFunction() {
		return new Function<RouteId, ViewCountId>() {

			@Override
			public ViewCountId apply(RouteId input) {
				return viewCountId(input);
			}

		};
	}

	/***
	 * Converts the given {@link RouteId} to a {@link ViewCountId} for view counts.
	 * 
	 * @param input
	 * @return
	 */
	@Nullable
	public static ViewCountId viewCountId(@Nullable RouteId input) {
		return (input == null) ? null : new ViewCountId(input);
	}

	/**
	 * Provides a stream of view ids representing the route ids. Nulls may be included so filter
	 * nulls from the given if desired.
	 * 
	 * @see #viewCountIdFunction()
	 * 
	 * @param routeIds
	 * @return
	 */
	@Nonnull
	public static Iterable<ViewCountId> viewCountIds(@Nonnull Iterable<RouteId> routeIds) {
		return Iterables.transform(routeIds, viewCountIdFunction());
	}

	/**
	 * Converts a view count id into the corresponding route id.
	 * 
	 * @param viewCountId
	 * @return
	 */
	public static RouteId routeId(ViewCountId viewCountId) {
		return new RouteId(viewCountId.countingIdValue());
	}

	/**
	 * A standard place to check if a Route score is worthy.
	 * 
	 * @param routeScore
	 * @return
	 */
	public static Boolean scoreIsSignificant(@Nullable Integer routeScore) {
		return routeScore != null && routeScore > Route.DEFAULT_SCORE;
	}

	private enum ScoreIsSignificantFunction implements Function<Integer, Boolean> {
		INSTANCE;
		@Override
		@Nullable
		public Boolean apply(@Nullable Integer score) {
			return scoreIsSignificant(score);
		}

		@Override
		public String toString() {
			return "ScoreIsSignificantFunction";
		}
	}

	public static Function<Integer, Boolean> scoreIsSignificantFunction() {
		return ScoreIsSignificantFunction.INSTANCE;
	}

	public static Iterable<Route> scoreIsSignficantRoutes(Iterable<Route> routes) {
		return Iterables.filter(routes, scoreIsSignificantPredicate());
	}

	public static Predicate<Route> scoreIsSignificantPredicate() {
		Function<Route, Boolean> function = Functions.compose(scoreIsSignificantFunction(), scoreFunction());
		return Predicates.compose(Predicates.equalTo(Boolean.TRUE), function);
	}

	public RouteProvider provider(final Iterable<Route> routes) {
		return new RouteProvider.Default(routes);
	}

	/** provides an "external key" to be used to find routes in document storage. */
	public static ApplicationDataId applicationDataId(RouteId routeId) {
		return new IdentifierApplicationDataId<RouteId, Route>(routeId, Route.class);
	}

	/**
	 * Provides the route ids from a list of social groups....when that social group has context of
	 * route id.
	 * 
	 * @param best
	 * @return
	 */
	public static Iterable<RouteId> socialGroupRouteIds(Iterable<SocialGroup> socialGroups) {
		return SocialGroupUtil.contextIds(socialGroups, RouteId.class);

	}

	public static Iterable<Message> names(Routes routes) {
		return Iterables.filter(Iterables.transform(routes, nameFunction()), Predicates.notNull());
	}

	private enum NameFunction implements Function<Route, Message> {
		INSTANCE;
		@Override
		@Nullable
		public Message apply(@Nullable Route route) {
			return (route != null) ? route.name() : null;
		}

		@Override
		public String toString() {
			return "NameFunction";
		}
	}

	public static Function<Route, Message> nameFunction() {
		return NameFunction.INSTANCE;
	}

}
