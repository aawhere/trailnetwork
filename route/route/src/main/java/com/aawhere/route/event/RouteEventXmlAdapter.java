/**
 * 
 */
package com.aawhere.route.event;

import java.util.Map;

import com.aawhere.id.IdentifierToIdentifiableXmlAdapter;
import com.aawhere.id.IdentifiersToIdentifiablesXmlAdapterProvider;

import com.google.common.base.Function;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Provides the {@link RouteEvent} when given a {@link RouteEventId}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class RouteEventXmlAdapter
		extends IdentifierToIdentifiableXmlAdapter<RouteEvent, RouteEventId>
		implements IdentifiersToIdentifiablesXmlAdapterProvider<RouteEventId, RouteEvent> {

	private Function<Iterable<RouteEventId>, Map<RouteEventId, RouteEvent>> batchProvider;

	/**
	 * 
	 */
	public RouteEventXmlAdapter() {
		super();
	}

	/**
	 * @param provider
	 */
	@Inject
	public RouteEventXmlAdapter(RouteEventProvider provider) {
		this(RouteEventUtil.providerFunction(provider));
		this.batchProvider = provider.idsFunction();
	}

	public RouteEventXmlAdapter(Function<RouteEventId, RouteEvent> provider) {
		super(provider);
	}

	@Singleton
	public static class ForCompletion
			extends RouteEventXmlAdapter {

		/**
		 * 
		 */
		public ForCompletion() {
			super();
		}

		/**
		 * @param provider
		 */
		@Inject
		public ForCompletion(RouteEventProvider provider) {
			super(provider);
		}

		public ForCompletion(Function<RouteEventId, RouteEvent> batchLoaded) {
			super(batchLoaded);
		}
	}

	@Singleton
	public static class ForStatistic
			extends RouteEventXmlAdapter {

		/**
		 * 
		 */
		public ForStatistic() {
			super();
		}

		ForStatistic(Function<RouteEventId, RouteEvent> loaded) {
			super(loaded);
		}

		/**
		 * @param provider
		 */
		@Inject
		public ForStatistic(RouteEventProvider provider) {
			super(provider);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.id.IdentifiersToIdentifiablesXmlAdapterProvider#xmlAdapterPreloaded(java.lang
	 * .Iterable)
	 */
	@Override
	public Map<RouteEventId, RouteEvent> xmlAdapterPreloaded(Iterable<RouteEventId> ids) {
		return batchProvider.apply(ids);

	}

}
