/**
 * 
 */
package com.aawhere.route;

import com.aawhere.route.Route.StartXmlAdapter;
import com.aawhere.trailhead.TrailheadIdXmlAdapter;
import com.aawhere.trailhead.TrailheadProvider;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author aroller
 * @deprecated use {@link StartXmlAdapter}
 */
@Singleton
public class StartTrailheadIdXmlAdapter
		extends TrailheadIdXmlAdapter {

	/**
	 * 
	 */
	public StartTrailheadIdXmlAdapter() {
		super();
	}

	/**
	 * @param provider
	 */
	@Inject
	public StartTrailheadIdXmlAdapter(TrailheadProvider provider) {
		super(provider);
	}

}
