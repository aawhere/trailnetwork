/**
 * 
 */
package com.aawhere.route;

import com.aawhere.id.IdentifierToIdentifiableXmlAdapter;

import com.google.common.base.Function;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * provides the {@link Route} given the {@link RouteId} using the function provided.
 * 
 * @see IdentifierToIdentifiableXmlAdapter
 * @author aroller
 * 
 */
@Singleton
public class RouteIdXmlAdapter
		extends IdentifierToIdentifiableXmlAdapter<Route, RouteId> {

	/**
	 * 
	 */
	public RouteIdXmlAdapter() {
	}

	RouteIdXmlAdapter(Function<RouteId, Route> provider) {
		super(provider);
	}

	/**
	 * @param provider
	 */
	@Inject
	public RouteIdXmlAdapter(RouteProvider provider) {
		super(provider);
	}

}
