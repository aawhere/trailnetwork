/**
 * 
 */
package com.aawhere.route.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

/**
 * Calculates the score of a {@link RouteEvent} given the criteria that may influence a score.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteEventScoreCalculator {

	/**
	 * @return the routeEventId
	 */
	public RouteEventId getRouteEventId() {
		return this.routeEventId;
	}

	/**
	 * @return the startTime
	 */
	public DateTime getStartTime() {
		return this.startTime;
	}

	/**
	 * @return the numberOfParticipants
	 */
	public Integer getNumberOfParticipants() {
		return this.numberOfParticipants;
	}

	/**
	 * @return the routeScore
	 */
	public Integer getRouteScore() {
		return this.routeScore;
	}

	/**
	 * @return the routeEventScore
	 */
	public Integer getRouteEventScore() {
		return this.routeEventScore;
	}

	/**
	 * @return the weeksSinceEvent
	 */
	public Integer getWeeksSinceEvent() {
		return this.weeksSinceEvent;
	}

	/**
	 * @return the dateFactor
	 */
	public Integer getDateFactor() {
		return this.dateFactor;
	}

	/**
	 * @return the participantFactor
	 */
	public Integer getParticipantFactor() {
		return this.participantFactor;
	}

	/**
	 * @return the routeFactor
	 */
	public Integer getRouteFactor() {
		return this.routeFactor;
	}

	static final int WEEKS_TO_NON_SIGNIFICANT = 500;
	static final int PARTICIPANTS_MAX_TO_INFLUENCE = 100;
	static final int PARTICIPANTS_MIN_TO_INFLUENCE = 5;
	static final int ROUTE_SCORE_MAX_INFLUENCE = 100;
	static final int ROUTE_SCORE_DEFAULT = 1;

	@XmlElement
	private RouteEventId routeEventId;

	@XmlElement
	private DateTime startTime;
	@XmlAttribute
	private Integer numberOfParticipants;
	@XmlAttribute
	private Integer routeScore;
	@XmlAttribute
	private Integer routeEventScore;
	@XmlAttribute
	private Integer weeksSinceEvent;
	@XmlAttribute
	private Integer dateFactor;
	@XmlAttribute
	private Integer participantFactor;
	@XmlAttribute
	private Integer routeFactor;

	/**
	 * Used to construct all instances of RouteEventScoreCalculator.
	 * 
	 * This calculates a single score intended to show someone the most relevant courses based on
	 * the information we have.
	 * 
	 * The date being the most important since events are better fresh. Number of participants
	 * should influence greatly, but has a max to avoid over-importantce vs. date.
	 * 
	 * Number of views, votes, etc should be brought into increase score based on "what's hot".
	 * 
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteEventScoreCalculator> {

		public Builder() {
			super(new RouteEventScoreCalculator());
		}

		public Builder routeScore(Integer routeScore) {
			building.routeScore = routeScore;
			return this;
		}

		public Builder numberOfParticipants(Integer numberOfParticipants) {
			building.numberOfParticipants = numberOfParticipants;
			return this;
		}

		public Builder startTime(DateTime startTime) {
			building.startTime = startTime;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("numberOfParticipants", building.numberOfParticipants);
			Assertion.exceptions().notNull("startTime", building.startTime);

		}

		@Override
		public RouteEventScoreCalculator build() {
			RouteEventScoreCalculator built = super.build();

			if (built.numberOfParticipants < PARTICIPANTS_MIN_TO_INFLUENCE) {
				built.routeEventScore = RouteEvent.SCORE_DEFAULT;
			} else {
				final DateTime endTime = DateTime.now();
				Duration duration = new Duration(built.startTime, endTime);
				// duration.toPeriod().toStandardWeeks() returns zero???
				built.weeksSinceEvent = duration.toStandardDays().toStandardWeeks().getWeeks();

				// the farther back in time the lower the score, but don't zero it out
				built.dateFactor = Math.max(1, WEEKS_TO_NON_SIGNIFICANT - built.weeksSinceEvent);
				built.participantFactor = Math.min(built.numberOfParticipants, PARTICIPANTS_MAX_TO_INFLUENCE);

				built.routeFactor = Math.max(ROUTE_SCORE_DEFAULT, Math.min(	If.nil(built.routeScore)
																					.use(ROUTE_SCORE_DEFAULT),
																			ROUTE_SCORE_MAX_INFLUENCE));
				built.routeEventScore = built.routeFactor * built.participantFactor * built.dateFactor;
			}
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteEventScoreCalculator */
	private RouteEventScoreCalculator() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * A special case situation allowing the creator to identify this after only if not previously
	 * identified. It avoids mutation, but allows the calculator to identify the event being
	 * calculated for reporting purposes.
	 * 
	 * @param routeEventId
	 */
	public void identify(RouteEventId routeEventId) {
		if (this.routeEventId == null) {
			this.routeEventId = routeEventId;
		} else {
			throw new IllegalStateException(this.routeEventId + " already assigned when assigning " + routeEventId);
		}
	}

	/**
	 * @return the routeEventScore
	 */
	public Integer routeEventScore() {
		return this.routeEventScore;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.routeEventId == null) ? 0 : this.routeEventId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RouteEventScoreCalculator other = (RouteEventScoreCalculator) obj;
		if (this.routeEventId == null) {
			if (other.routeEventId != null)
				return false;
		} else if (!this.routeEventId.equals(other.routeEventId))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RouteEventScoreCalculator [routeEventId=" + this.routeEventId + ", routeEventScore="
				+ this.routeEventScore + "]";
	}

}
