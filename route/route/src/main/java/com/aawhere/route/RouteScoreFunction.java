/**
 * 
 */
package com.aawhere.route;

import javax.annotation.Nullable;

import com.google.common.base.Function;

/**
 * Produces the {@link Route#getScore()} from the {@link Route}.
 * 
 * @author aroller
 * @deprecated use {@link RouteUtil#scoreFunction()}
 */
@Deprecated
public class RouteScoreFunction
		implements Function<Route, Integer> {

	public static RouteScoreFunction build() {
		return new RouteScoreFunction();
	}

	/** Use {@link Builder} to construct RouteScoreFunction */
	private RouteScoreFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public Integer apply(@Nullable Route input) {
		if (input == null) {
			return null;
		} else {
			return input.getScore();
		}
	}

}
