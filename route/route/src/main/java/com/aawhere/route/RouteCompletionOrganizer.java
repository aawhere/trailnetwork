/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.route.RouteCompletionUtil.*;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.Interval;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.ActivityUtil;
import com.aawhere.collections.FunctionsExtended;
import com.aawhere.collections.ListUtilsExtended;
import com.aawhere.joda.time.IntervalUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.BuilderUtil;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.route.RouteCompletion.Builder;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventScoreCalculator;
import com.aawhere.route.event.RouteEventStatistic;
import com.aawhere.route.event.RouteEventUtil;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultiset;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;

/**
 * A stateful manager of all {@link RouteCompletions} for a single {@link Route} comparing each of
 * the participatingCompletions to each other. Provided the participatingMutators this will update
 * each of the given {@link RouteCompletion}s with the results.
 * 
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteCompletionOrganizer {

	/** Route being organized */
	@XmlElement
	private Route route;
	@XmlAttribute
	private Integer total;

	@XmlAttribute
	private Integer minNumberOfCoursesToScore;

	@XmlElement
	private RouteStats completionStats;
	@XmlElement
	private ActivityStats courseStats;
	@XmlElement
	private ActivityStats socialStats;
	@XmlElement
	private Ratio significantActivityTypePercent = ActivityUtil.MIN_ACTIVITY_TYPE_PERCENT;
	@XmlElement(name = "participatingCompletion")
	@XmlElementWrapper
	private List<RouteCompletion> participatingCompletions;
	@XmlElement(name = "courseCompletion")
	@XmlElementWrapper
	private List<RouteCompletion> courseCompletions;
	@XmlElement(name = "personalBest")
	@XmlElementWrapper
	private List<RouteCompletion> personalBests;
	@XmlElement(name = "duplicateCompletion")
	@XmlElementWrapper
	private LinkedList<RouteCompletion> duplicateCompletions;
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Multimap<RouteEvent, RouteCompletionId> events;
	@XmlElement(name = "routeEventScoreCalculator")
	private Set<RouteEventScoreCalculator> routeEventScoreCalculators;
	@XmlElement
	private RouteScoreCalculator routeScoreCalculator;

	/**
	 * {@link SocialGroup} is created to relate people since they all completed the same route.
	 * Additionally, those people that completed the same event participate in yet another group.
	 */
	@XmlElement
	private Set<SocialGroup> socialGroups;

	/**
	 * Used to construct all instances of RouteCompletionOrganizer.
	 */
	public static class Organizer
			extends ObjectBuilder<RouteCompletionOrganizer> {
		static final Integer MIN_TOTAL_FOR_SOCIAL_COUNT_RANK = 2;
		private LinkedHashMap<RouteCompletionId, RouteCompletion.Builder> participatingMutators;
		private PersonalOrganizer personalOrganizer;
		private Function<ActivityId, Integer> attemptTimingsCounter;
		private Function<ActivityId, Integer> courseTimingsCounter;
		private Iterable<RouteCompletion> completions;
		private Function<ActivityId, Person> personProvider;

		/**
		 * An ordered list, by date, that provides all existing events previously created by earlier
		 * organization. This does not contain events created during this organization, but is
		 * provided to ensure that existing ids will be chosen.
		 * 
		 */
		private List<RouteEvent> existingEvents;

		// courseCompletions can be recorded if needed see #participatingCompletions(Iterable)
		// private HashMap<ActivityId, RouteCompletion> courseCompletions;

		private Organizer() {
			super(new RouteCompletionOrganizer());
		};

		/**
		 * run after personal race rank has already been assigned, this will determine the social
		 * Race Rank for the personal best of each. Secondary personal ranks receive a comparison to
		 * where they would be if this completion was their personal best.
		 * 
		 */
		void organizeSocialRaceRank() {

			final Function<Builder, RouteCompletion> buildingFunction = buildingFunction();
			List<Builder> orderedByFastest = orderByFastest().onResultOf(buildingFunction)
					.sortedCopy(participatingMutators.values());

			Integer socialRank = 1;
			Integer numberOfCompletions = orderedByFastest.size();
			for (int i = 0; i < numberOfCompletions; i++) {
				Builder builder = orderedByFastest.get(i);
				builder.socialRaceRank(socialRank);
				RouteCompletion completion = buildingFunction.apply(builder);

				// the social rank is not affected by personal repeats
				if (completion.isPersonalBest()) {
					socialRank++;
				}
			}

		}

		void organizeSocialOrder() {
			// an ordered view of this.personalOrganizer.personalTotals
			// built during the first iteration of assigning social order
			LinkedHashMultiset<PersonId> personalTotalsOrderedByMostRecent = LinkedHashMultiset
					.create(this.personalOrganizer.personalTotals.size());

			final Function<Builder, RouteCompletion> buildingFunction = buildingFunction();
			List<Builder> orderedByMostRecent = orderedByMostRecent().onResultOf(buildingFunction)
					.sortedCopy(participatingMutators.values());
			int socialOrder = orderedByMostRecent.size();
			int numberOfCompletions = orderedByMostRecent.size();

			for (int i = 0; i < numberOfCompletions; i++) {
				Builder builder = orderedByMostRecent.get(i);
				builder.socialOrder(socialOrder--);
				PersonId personId = builder.personId();
				if (!personalTotalsOrderedByMostRecent.contains(personId)) {
					if (this.personalOrganizer.personalTotals.contains(personId)) {
						personalTotalsOrderedByMostRecent.add(	personId,
																this.personalOrganizer.personalTotals.count(personId));
					}
				}
			}

			ImmutableMultiset<PersonId> socialCountOrderedByCountAndMostRecent = Multisets
					.copyHighestCountFirst(personalTotalsOrderedByMostRecent);
			HashMap<PersonId, Integer> socialCountRanks = new HashMap<>();
			int rank = 1;
			for (Multiset.Entry<PersonId> entry : socialCountOrderedByCountAndMostRecent.entrySet()) {
				socialCountRanks.put(entry.getElement(), rank++);
			}

			for (int i = 0; i < numberOfCompletions; i++) {
				Builder builder = orderedByMostRecent.get(i);
				PersonId personId = builder.personId();
				Integer socialCountRank = socialCountRanks.get(personId);
				// null is fine and desired.
				builder.socialCountRank(socialCountRank);
			}
		}

		class EventOrganizer {
			private RouteCompletionOrganizer organizer;
			private Iterator<RouteEvent> existingEventsIterator;
			private RouteEvent currentExistingEvent;
			private Multimap<ActivityType, RouteCompletionId> overlappingCompletionsPerActivityType;
			private Interval currentOverlapInterval;

			public EventOrganizer(RouteCompletionOrganizer organizer) {

				this.organizer = organizer;
				organizer.routeEventScoreCalculators = Sets.newHashSet();

				organizer.events = HashMultimap.create();
				// expands to the interval
				this.currentOverlapInterval = new Interval(Integer.MIN_VALUE, Integer.MIN_VALUE + 1);
				this.overlappingCompletionsPerActivityType = HashMultimap.create();
				this.currentExistingEvent = null;
				this.existingEventsIterator = existingEvents.iterator();
				organize();
			}

			private void organize() {
				// completions are in order of time
				// look for consecutive activities with overlapping times.
				for (RouteCompletion completion : organizer.participatingCompletions) {
					Interval currentInterval = completion.interval();

					// forwards to the next existing if needed and available
					while (existingEventsIterator.hasNext()
							&& (currentExistingEvent == null || currentInterval.getStart().isAfter(currentExistingEvent
									.interval().getEnd()))) {
						currentExistingEvent = existingEventsIterator.next();
					}

					final RouteCompletionId completionId = completion.id();
					if (currentInterval.overlaps(currentOverlapInterval)) {
						currentOverlapInterval = IntervalUtil.expanded(currentOverlapInterval, currentInterval);
					} else {
						eventCreated();
						overlappingCompletionsPerActivityType.clear();
						currentOverlapInterval = currentInterval;
					}
					overlappingCompletionsPerActivityType.put(completion.activityType(), completionId);
				}
				// since events are lazy created, do one last time if last completion is pending
				this.eventCreated();
			}

			/**
			 * Creates an event if the overlapping completions supports it.
			 * 
			 */
			private void eventCreated() {
				for (ActivityType activityType : this.overlappingCompletionsPerActivityType.keySet()) {
					Collection<RouteCompletionId> overlappingCompletions = this.overlappingCompletionsPerActivityType
							.get(activityType);

					// not overlapping so start looking for the next overlapping interval
					if (overlappingCompletions.size() > 1) {
						// create a new event id or use the existing if available
						// notice that route score may not yet be assigned
						RouteEventScoreCalculator routeEventScoreCalculator = RouteEventScoreCalculator.create()
								.routeScore(organizer.route.score()).startTime(currentOverlapInterval.getStart())
								.numberOfParticipants(overlappingCompletions.size()).build();
						RouteEvent event;
						if (currentExistingEvent != null
								&& currentOverlapInterval.overlaps(currentExistingEvent.interval())) {
							// worth noting the event.
							event = RouteEventUtil.expanded(currentExistingEvent, currentOverlapInterval);
							event = RouteEvent.clone(event).score(routeEventScoreCalculator.routeEventScore()).build();

						} else {
							// worth noting the event.
							event = RouteEvent
									.create()
									.interval(currentOverlapInterval)
									.route(organizer.route)
									.activityType(activityType)
									.score(routeEventScoreCalculator.routeEventScore())
									.completionsCount(MeasurementUtil.count(routeEventScoreCalculator
											.getNumberOfParticipants())).build();

						}
						routeEventScoreCalculator.identify(event.id());
						organizer.routeEventScoreCalculators.add(routeEventScoreCalculator);
						organizer.events.putAll(event, overlappingCompletions);

						for (RouteCompletionId routeCompletionId : overlappingCompletions) {
							Builder mutator = participatingMutators.get(routeCompletionId);
							mutator.routeEventId(event.id());
						}

						// setup the current to look for matches of it's own
					}
				}
			}
		}

		/**
		 * specializes in organizing the participatingMutators by the person which the
		 * participatingCompletions were completed by.
		 * 
		 * @author aroller
		 * 
		 */
		class PersonalOrganizer {

			private ListMultimap<PersonId, Builder> organizedByPerson;
			/**
			 * keeps track of those that have completed the route more than once.
			 */
			private Multiset<PersonId> personalTotals = HashMultiset.create();

			/**
			 * @throws EntityNotFoundException
			 * 
			 */
			public PersonalOrganizer() throws EntityNotFoundException {
				// this will be right than all the participatingMutators, but initialize ready for
				// 1:1 person
				// to completion
				this.organizedByPerson = LinkedListMultimap.create();
				for (Builder mutator : participatingMutators.values()) {
					organizedByPerson.put(mutator.personId(), mutator);
				}
			}

			PersonalOrganizer organize() {

				Set<Entry<PersonId, Collection<Builder>>> entries = organizedByPerson.asMap().entrySet();
				for (Entry<PersonId, Collection<Builder>> entry : entries) {
					// ListMultimap doesn't expose the list, but it's in there
					// this collection will be modified, not copied, for performance reasons
					List<Builder> allForPerson = (List<Builder>) entry.getValue();
					PersonId personId = entry.getKey();
					Integer personalTotal = allForPerson.size();
					this.personalTotals.add(personId, personalTotal);
					organizePersonalRace(allForPerson, personalTotal);
					organizePersonalOrder(allForPerson, personalTotal);
				}
				return this;
			}

			/**
			 * @param allForPerson
			 * @param personalTotal
			 */
			private void organizePersonalOrder(List<Builder> allForPerson, Integer personalTotal) {
				if (personalTotal > 1) {
					Ordering<RouteCompletion.Builder> orderedByOccurrence = RouteCompletionUtil.orderedByOccurrence()
							.onResultOf(buildingFunction());
					Collections.sort(allForPerson, orderedByOccurrence);
				}

				final int allForPersonCount = allForPerson.size();
				for (int i = 0; i < allForPersonCount; i++) {
					Builder builder = allForPerson.get(i);
					builder.personalOrder(i + 1);
				}
			}

			/**
			 * @param allForPerson
			 * @param personalTotal
			 */
			private void organizePersonalRace(List<Builder> allForPerson, Integer personalTotal) {
				if (personalTotal > 1) {
					Ordering<RouteCompletion.Builder> orderedByFastest = RouteCompletionUtil.orderByFastest()
							.onResultOf(buildingFunction());
					Collections.sort(allForPerson, orderedByFastest);
				}

				final int allForPersonCount = allForPerson.size();
				for (int i = 0; i < allForPersonCount; i++) {
					Builder builder = allForPerson.get(i);
					builder.personalRaceRank(i + 1);
					builder.personalTotal(personalTotal);
				}
			}

			Multiset<PersonId> personTotals() {
				return Multisets.copyHighestCountFirst(this.personalTotals);
			}
		}

		private void countStats(RouteCompletionOrganizer organizer) {
			final ActivityStats completionActivityStats = RouteCompletionUtil
					.activityStats(organizer.participatingCompletions, organizer.significantActivityTypePercent);
			// extend the activity stats to include route specific events...like events
			// the total comes from completion for consistency and to provide the percentage
			// relative to all
			SortedSet<RouteEventStatistic> eventStats = RouteCompletionUtil.eventsCounted(	organizer.events,
																							completionActivityStats
																									.total());
			organizer.completionStats = RouteStats.routeStatsCreator().copy(completionActivityStats)
					.routeEventStats(eventStats).build();
			organizer.courseStats = RouteCompletionUtil.activityStats(	organizer.courseCompletions,
																		organizer.significantActivityTypePercent);
			organizer.personalBests = Lists.newArrayList(RouteCompletionUtil
					.personalBests(organizer.participatingCompletions));
			organizer.socialStats = RouteCompletionUtil.activityStats(	organizer.personalBests,
																		organizer.significantActivityTypePercent);

		}

		private void routeScored(RouteCompletionOrganizer organizer) {
			RouteScoreCalculator.Builder scoreCalculatorBuilder = RouteScoreCalculator.create()
					.boundsDiagonalLength(organizer.route.boundingBox().getDiagonal())
					.courseCountMin(organizer.minNumberOfCoursesToScore);
			scoreCalculatorBuilder.completionStats(organizer.completionStats()).courseStats(organizer.courseStats())
					.socialStats(organizer.socialStats()).finishesAtStart(organizer.route.isFinishAtStart());
			organizer.routeScoreCalculator = scoreCalculatorBuilder.build();
			Integer score = organizer.routeScoreCalculator.score();
			for (Builder mutator : this.participatingMutators.values()) {
				mutator.routeScore(score);
			}
		}

		private void eventsCounted(RouteCompletionOrganizer organizer) {

			new EventOrganizer(organizer);

		}

		private void activityTypesDetermined(RouteCompletionOrganizer organizer) {

			// first count them all as reported
			final Iterable<ActivityType> activityTypesReported = Iterables.transform(RouteCompletionUtil
					.activityTypesReported(organizer.participatingCompletions), ActivityUtil.roadIsBikeFunction());
			CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> counter = ActivityTypeStatistic
					.counter(activityTypesReported).build();
			// aggregate the stats and choose if it will be biking or mountain biking
			ActivityType bikeGeneralized = ActivityUtil.bikeGeneralized(counter.stats(ActivityType.BIKE),
																		counter.stats(ActivityType.MTB),
																		counter.stats(ActivityType.ROAD),
																		organizer.significantActivityTypePercent);
			HashMap<ActivityType, ActivityType> activityTypeMap = new HashMap<ActivityType, ActivityType>();
			activityTypeMap.put(ActivityType.ROAD, bikeGeneralized);
			activityTypeMap.put(ActivityType.BIKE, bikeGeneralized);
			activityTypeMap.put(ActivityType.MTB, bikeGeneralized);
			activityTypeMap.put(ActivityType.HIKE, ActivityType.WALK);
			Function<ActivityType, ActivityType> activityTypeTransformer = FunctionsExtended.forMap(activityTypeMap);
			Collection<Builder> mutators = this.participatingMutators.values();
			final Function<Builder, RouteCompletion> buildingFunction = buildingFunction();
			// for every completion that is participating provide the derived activity type
			for (Builder mutator : mutators) {
				ActivityType activityTypeReported = buildingFunction.apply(mutator).activityTypeReported();
				mutator.activityType(activityTypeTransformer.apply(activityTypeReported));
			}
		}

		/**
		 * The main method to start the organizing and chain the following methods.
		 * 
		 * @return
		 * @throws EntityNotFoundException
		 */
		public RouteCompletionOrganizer organize() throws EntityNotFoundException {
			RouteCompletionOrganizer built = super.build();
			built.participatingCompletions = Lists.newLinkedList();
			built.duplicateCompletions = Lists.newLinkedList();
			built.courseCompletions = Lists.newLinkedList();
			this.participatingMutators = new LinkedHashMap<>();
			// Must duplicates use time and duration so sorting by both puts them together for easy
			// detection.
			final List<RouteCompletion> orderedByStartTime = RouteCompletionUtil.orderedByOccurrence()
					.compound(RouteCompletionUtil.shorterDurationFirst())
					.sortedCopy(Iterables.filter(this.completions, Predicates.notNull()));
			// duplicates must be removed first...order by time and compare consecutive duplicates
			for (RouteCompletion currentCompletion : orderedByStartTime) {
				// only add the completion if it is not a duplicate
				final Builder currentMutator = RouteCompletion.mutate(currentCompletion);
				// updates the route details like trailhead
				currentMutator.route(built.route);
				// update with the latest person...could have been made an alias
				// must do this now before duplicate detection
				Person person = personProvider.apply(currentMutator.activityId());
				if (person == null) {
					throw new EntityNotFoundException(currentMutator.personId());
				}
				currentMutator.person(person);
				// all participatingCompletions activities must complete at least one course,
				// but more is better...find now in case duplicate decisions must be made
				currentMutator.coursesCompletedCount(courseTimingsCounter.apply(currentCompletion.activityId()));

				// detect and remove duplicates from participation
				RouteCompletion previous;
				boolean currentIsParticipating;

				if (!built.participatingCompletions.isEmpty()
						&& RouteCompletionUtil.areDuplicates(	previous = Iterables
																		.getLast(built.participatingCompletions),
																currentCompletion)) {

					// determine which to keep...
					List<RouteCompletion> preferredFirst = RouteCompletionUtil.preferredFirst(Lists
							.newArrayList(previous, currentCompletion));
					RouteCompletion preferred = preferredFirst.get(0);

					// checking for same, rather than id, since mutating
					currentIsParticipating = preferred == currentCompletion;

					Builder previousMutator = Iterables.getLast(this.participatingMutators.values());
					if (currentIsParticipating) {
						previousMutator.duplicates(currentMutator.id());
						currentMutator.activityAdded(previousMutator.activityId());
						// must remove previous mutator and replace with current
						this.participatingMutators.remove(previousMutator.id());
						ListUtilsExtended.removeLast(built.participatingCompletions);
						built.duplicateCompletions.add(previousMutator.build());
					} else {
						// order is retained so adding the "duplicate" knows that the already added
						// activityId for the completion
						// is preferred. There is no order to preference beyond the first
						previousMutator.activityAdded(currentCompletion.activityId());
						// reference back to the winner and unparticipate
						currentMutator.duplicates(previousMutator.id());
						// we are done mutating this one so it must be built
						built.duplicateCompletions.add(currentMutator.build());
						// do not add to the participation list
						// do not build previous mutator...it's still participating
					}

				} else {
					// not a duplicate so participate
					currentIsParticipating = true;
				}

				if (currentIsParticipating) {
					built.participatingCompletions.add(currentCompletion);
					this.participatingMutators.put(currentMutator.id(), currentMutator);
					if (currentCompletion.isCourse()) {
						built.courseCompletions.add(currentCompletion);
						// this makes a datastore call
						Integer count = attemptTimingsCounter.apply(currentCompletion.activityId());
						currentMutator.courseActivityCount(count);
					}
				}
			}
			// determine the acitivty types first in case that affects anybody downstream
			activityTypesDetermined(built);
			eventsCounted(built);
			this.personalOrganizer = new PersonalOrganizer().organize();
			organizeSocialRaceRank();
			organizeSocialOrder();
			countStats(built);
			routeScored(built);
			built.participatingCompletions = BuilderUtil.buildAll(this.participatingMutators.values());
			return built;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("participatingCompletions", this.completions);
			Assertion.exceptions().notNull("personProvider", this.personProvider);
			Assertion.exceptions().notNull("attemptTimingsCounter", this.attemptTimingsCounter);
			Assertion.exceptions().notNull("courseTimingsCounter", this.courseTimingsCounter);
			Assertion.exceptions().notNull("route", building.route);
			Assertion.exceptions().notNull("existingEvents", this.existingEvents);

		}

		@Override
		public RouteCompletionOrganizer build() {
			try {
				return organize();
			} catch (EntityNotFoundException e) {
				throw ToRuntimeException.wrapAndThrow(e);
			}
		}

		/**
		 * @param children
		 * @return
		 */
		public Organizer completions(Iterable<RouteCompletion> completions) {
			this.completions = completions;
			return this;
		}

		public Organizer existingEvents(Iterable<RouteEvent> routeEvents) {
			this.existingEvents = RouteEventUtil.orderingByDate().sortedCopy(routeEvents);
			return this;
		}

		public Organizer significantActivityTypePercent(Ratio significantActivityTypePercent) {
			building.significantActivityTypePercent = significantActivityTypePercent;
			return this;
		}

		public Organizer minNumberOfCoursesToScore(Integer minNumberOfCoursesToScore) {
			building.minNumberOfCoursesToScore = minNumberOfCoursesToScore;
			return this;
		}

		/**
		 * @param attemptTimingsCounter
		 *            the attemptTimingsCounter to set
		 * @return
		 */
		public Organizer attemptTimingsCounter(Function<ActivityId, Integer> attemptTimingsCounter) {
			this.attemptTimingsCounter = attemptTimingsCounter;
			return this;
		}

		public Organizer courseTimingsCounter(Function<ActivityId, Integer> courseTimingsCounter) {
			this.courseTimingsCounter = courseTimingsCounter;
			return this;
		}

		/**
		 * @param instance
		 * @return
		 */
		public Organizer personProvider(Function<ActivityId, Person> provider) {
			this.personProvider = provider;
			return this;
		}

		/**
		 * @param route
		 * @return
		 */
		public Organizer route(Route route) {
			building.route = route;
			return this;
		}

	}// end Builder

	public static Organizer create() {
		return new Organizer();
	}

	/** Use {@link Builder} to construct RouteCompletionOrganizer */
	private RouteCompletionOrganizer() {
	}

	public List<RouteCompletion> completions() {
		return this.participatingCompletions;
	}

	/**
	 * @return the duplicateCompletions
	 */
	public LinkedList<RouteCompletion> duplicateCompletions() {
		return this.duplicateCompletions;
	}

	/**
	 * @return the socialStats
	 */
	public ActivityStats socialStats() {
		return this.socialStats;
	}

	/**
	 * @return the courseCompletions
	 */
	public List<RouteCompletion> courses() {
		return this.courseCompletions;
	}

	/**
	 * @return the personalBests
	 */
	public Iterable<RouteCompletion> getPersonalBests() {
		return this.personalBests;
	}

	/**
	 * @return the completionStats
	 */
	public RouteStats completionStats() {
		return this.completionStats;
	}

	/**
	 * @return the courseStats
	 */
	public ActivityStats courseStats() {
		return this.courseStats;
	}

	/**
	 * @return the routeScoreCalculator
	 */
	public RouteScoreCalculator routeScoreCalculator() {
		return this.routeScoreCalculator;
	}

	/**
	 * @return
	 * 
	 */
	public Multimap<RouteEvent, RouteCompletionId> events() {
		return this.events;
	}

	/**
	 * @return the routeEventScoreCalculators
	 */
	public Set<RouteEventScoreCalculator> routeEventScoreCalculators() {
		return this.routeEventScoreCalculators;
	}

}
