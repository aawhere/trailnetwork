/**
 * 
 */
package com.aawhere.route;

import java.net.HttpURLConnection;

import com.aawhere.activity.ActivityId;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

import com.google.common.collect.Lists;

/**
 * Indicates that the courses given do not yet have any relation to a route.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.NOT_FOUND)
public class CoursesNotRelatedException
		extends BaseException {

	public static final int CODE = HttpURLConnection.HTTP_NOT_FOUND;
	private static final long serialVersionUID = 4409278432923725215L;

	public CoursesNotRelatedException(Iterable<ActivityId> courseIds) {
		super(CompleteMessage.create(RouteMessage.COURSES_NOT_RELATED)
				.addParameter(RouteMessage.Param.COURSE_ID, courseIds).build());
	}

	public CoursesNotRelatedException(ActivityId... courseIds) {
		this(Lists.newArrayList(courseIds));
	}
}
