/**
 * 
 */
package com.aawhere.route;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteCompletions
		extends BaseFilterEntities<RouteCompletion> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntities#getCollection()
	 */
	@XmlElement(name = RouteCompletion.FIELD.DOMAIN)
	@Override
	public List<RouteCompletion> getCollection() {
		return super.getCollection();
	}

	/**
	 * Used to construct all instances of RouteCompletions.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, RouteCompletion, RouteCompletions> {

		public Builder() {
			super(new RouteCompletions());
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteCompletions */
	private RouteCompletions() {
	}

	public static Builder create() {
		return new Builder();
	}

}
