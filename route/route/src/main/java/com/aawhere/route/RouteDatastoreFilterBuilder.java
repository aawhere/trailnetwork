/**
 *
 */
package com.aawhere.route;

import static com.aawhere.route.Route.FIELD.KEY.*;

import java.util.Set;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationUtil;
import com.aawhere.field.FieldKey;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.geocell.GeoCellFilterBuilder;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.search.SearchUtils;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.google.common.collect.Lists;

/**
 * A filter used to request and describe a subset of {@link Routes} in the system. Always
 * {@link #orderByScore()} unless {@link #orderByNone()} is called.
 * 
 * @author Brian Chapman
 * 
 */
public class RouteDatastoreFilterBuilder
		extends Filter.BaseFilterBuilder<RouteDatastoreFilterBuilder> {

	/**
	 * 
	 */
	private static final FieldKey START_AREA_KEY = Route.FIELD.KEY.START_AREA_CELLS;
	public static final Boolean MAINS_ONLY = Boolean.TRUE;
	public static final Boolean MAINS_AND_ALTERNATES = Boolean.FALSE;
	private GeoCellFilterBuilder startArea;
	private Integer maxNumberOfCells = 20;

	public RouteDatastoreFilterBuilder() {
		super();
	}

	private RouteDatastoreFilterBuilder(Filter filter) {
		super(filter);
	}

	public RouteDatastoreFilterBuilder start(Trailhead trailhead) {
		// order delegated
		return start(trailhead.getId());
	}

	/** Must be assigned before the first call to {@link #startArea()} or it will be ignored. */
	public RouteDatastoreFilterBuilder maxNumberOfCells(Integer max) {
		this.maxNumberOfCells = max;
		return this;
	}

	/**
	 * When searching for routes from multiple trailheads this using the IN filter so there is a
	 * limit.
	 * 
	 * @param trailheadIds
	 * @return
	 */
	public RouteDatastoreFilterBuilder start(Iterable<TrailheadId> trailheadIds) {

		return endpoint(trailheadIds, Route.FIELD.KEY.START_ID);
	}

	public RouteDatastoreFilterBuilder finish(Set<TrailheadId> trailheadIds) {
		// no index for finish right now
		orderByNone();
		return endpoint(trailheadIds, Route.FIELD.KEY.FINISH_ID);
	}

	public RouteDatastoreFilterBuilder activityTypes(ActivityType... activityTypes) {
		// order delegated
		return activityTypes(Lists.newArrayList(activityTypes));
	}

	public RouteDatastoreFilterBuilder activityTypes(Iterable<ActivityType> activityTypes) {

		return addCondition(FilterCondition.create().field(Route.FIELD.KEY.ACTIVITY_TYPE).operator(FilterOperator.IN)
				.value(activityTypes).build());
	}

	public RouteDatastoreFilterBuilder applications(Iterable<Application> applications) {

		return applicationKeys(ApplicationUtil.keys(applications));
	}

	public RouteDatastoreFilterBuilder applicationKeys(ApplicationKey... keys) {

		return applicationKeys(Lists.newArrayList(keys));
	}

	public RouteDatastoreFilterBuilder applicationKeys(Iterable<ApplicationKey> keys) {

		return addCondition(FilterCondition.create().field(Route.FIELD.KEY.APPLICATION_KEY).operator(FilterOperator.IN)
				.value(keys).build());
	}

	/**
	 * @param trailheadIds
	 * @param key
	 * @return
	 */
	private RouteDatastoreFilterBuilder endpoint(Iterable<TrailheadId> trailheadIds, FieldKey key) {
		FilterCondition<Iterable<TrailheadId>> condition = new FilterCondition.Builder<Iterable<TrailheadId>>()
				.field(key).operator(FilterOperator.IN).value(trailheadIds).build();

		addCondition(condition);
		return this;
	}

	/** Filters the results to make sure the score is at least that which is given. */
	public RouteDatastoreFilterBuilder scoreAtLeast(Integer minimumScore) {
		return addCondition(FilterCondition.create().field(SCORE).operator(FilterOperator.GREATER_THAN_OR_EQUALS)
				.value(minimumScore).build());
	}

	public RouteDatastoreFilterBuilder scoreMoreThanDefault() {
		return addCondition(FilterCondition.create().field(SCORE).operator(FilterOperator.GREATER_THAN)
				.value(Route.DEFAULT_SCORE).build());
	}

	/**
	 * condiitionally adds the {@link #scoreMoreThanDefault()} if the score is not yet assigned.
	 * This is intended to be run automatically with the {@link #build()} since many routes are
	 * provided without value at the default score.
	 * 
	 * @return
	 */
	public RouteDatastoreFilterBuilder scoreMoreThanDefaultIfNotFiltered() {
		if (!building.containsCondition(SCORE)) {
			scoreMoreThanDefault();
		}
		return this;
	}

	public RouteDatastoreFilterBuilder start(TrailheadId trailheadId) {

		return endpoint(trailheadId, Route.FIELD.KEY.START_ID);
	}

	public RouteDatastoreFilterBuilder finish(TrailheadId trailheadId) {
		// no sort currently supported
		orderByNone();
		return endpoint(trailheadId, Route.FIELD.KEY.FINISH_ID);
	}

	public RouteDatastoreFilterBuilder distanceCategory(ActivityTypeDistanceCategory... categories) {
		return distanceCategories(Lists.newArrayList(categories));
	}

	public RouteDatastoreFilterBuilder distanceCategories(Iterable<ActivityTypeDistanceCategory> categories) {
		return addCondition(FilterCondition.create().field(DISTANCE_CATEGORY).operator(FilterOperator.IN)
				.value(categories).build());
	}

	/**
	 * @param trailheadId
	 * @param key
	 * @return
	 */
	private RouteDatastoreFilterBuilder endpoint(TrailheadId trailheadId, FieldKey key) {
		FilterCondition<TrailheadId> condition = new FilterCondition.Builder<TrailheadId>().field(key)
				.operator(FilterOperator.EQUALS).value(trailheadId).build();
		addCondition(condition);
		return this;
	}

	/**
	 * @return
	 */
	public static RouteDatastoreFilterBuilder create() {
		return new RouteDatastoreFilterBuilder();
	}

	public static RouteDatastoreFilterBuilder clone(Filter filter) {

		return new RouteDatastoreFilterBuilder(Filter.cloneForBuilder(filter));
	}

	public RouteDatastoreFilterBuilder stale() {
		orderByNone();
		return addCondition(FilterCondition.create().field(STALE).operator(FilterOperator.EQUALS).value(Boolean.TRUE)
				.build());
	}

	/**
	 * Equals {@link ActivityId} of the course the route represents.
	 * 
	 * @param courseId
	 * @return
	 */
	public RouteDatastoreFilterBuilder courseId(ActivityId courseId) {
		// no sort necessary since the result is likely 1
		orderByNone();
		return addCondition(FilterCondition.create().field(Route.FIELD.KEY.COURSE_ID).operator(FilterOperator.EQUALS)
				.value(courseId).build());
	}

	/**
	 * Used to find any route that has a course id matching one of those given.
	 * 
	 * @param courseIds
	 * @return
	 */
	public RouteDatastoreFilterBuilder courseIdsIn(Iterable<ActivityId> courseIds) {
		// multiple returned, but sorting is unlikely needed
		orderByNone();
		return addCondition(FilterCondition.create().field(Route.FIELD.KEY.COURSE_ID).operator(FilterOperator.IN)
				.value(courseIds).build());
	}

	/**
	 * Equals the given {@link RouteId} to be filtered against the {@link Route#aliases()}.
	 * 
	 * @param routeId
	 * @return
	 */
	public RouteDatastoreFilterBuilder alias(RouteId routeId) {
		// this is for lookup purposes so no order by
		orderByNone();
		return addCondition(FilterCondition.create().field(Route.FIELD.KEY.ALIASES).operator(FilterOperator.EQUALS)
				.value(routeId).build());
	}

	/**
	 * When searching for multiple aliases this will find any route that matches any of them.
	 * 
	 * @param aliasIds
	 * @return
	 */
	public RouteDatastoreFilterBuilder aliases(Iterable<RouteId> routeIds) {
		orderByNone();
		return addCondition(FilterCondition.create().field(Route.FIELD.KEY.ALIASES).operator(FilterOperator.IN)
				.value(routeIds).build());
	}

	/**
	 * The group for {@link RouteId} given in order of the most popular...which should be the main.
	 * Alternates will follow, all with {@link Route#mainId()} equaling the route id given.
	 * 
	 * @param mainRouteId
	 * @return
	 */
	public RouteDatastoreFilterBuilder group(RouteId mainRouteId) {
		// a route may have many so provide each in order of score
		return addCondition(FilterCondition.create().field(Route.FIELD.KEY.MAIN_ID).operator(FilterOperator.EQUALS)
				.value(mainRouteId).build());
	}

	/**
	 * Applies a filter if mains only =true. No filter is applied if false since it will accept
	 * mains and not, but this will require two different indexes depending upon other items you
	 * have included.
	 * 
	 * @param mainsOnly
	 * @return
	 */
	public RouteDatastoreFilterBuilder mains(Boolean mainsOnly) {

		// this must be omitted otherwise you'd get only alternates
		if (mainsOnly) {
			addCondition(FilterCondition.create().field(Route.FIELD.KEY.MAIN).operator(FilterOperator.EQUALS)
					.value(mainsOnly).build());
		}
		return this;
	}

	public RouteDatastoreFilterBuilder orderByScore() {
		if (!building.hasSortOrder()) {
			orderBy(Route.FIELD.KEY.SCORE, Direction.DESCENDING);
		}
		return this;
	}

	/**
	 * @return
	 */
	public RouteDatastoreFilterBuilder courseIncluded() {
		return addOption(Route.FIELD.OPTION.COURSES);
	}

	/**
	 * @return
	 */
	public RouteDatastoreFilterBuilder mainsOnly() {
		return mains(MAINS_ONLY);
	}

	/**
	 * @return
	 * 
	 */
	public RouteDatastoreFilterBuilder coursesIncluded() {
		return addOption(Route.FIELD.OPTION.COURSES);
	}

	public RouteDatastoreFilterBuilder coursesInline() {
		return addOption(Route.FIELD.OPTION.COURSE_INLINE);
	}

	/**
	 * If {@link Filter#hasBounds()} this will provide the routes that have have an
	 * {@link GeoCellFilterBuilder#intersection(com.aawhere.measure.BoundingBox)} with
	 * {@link Route#startArea()}.
	 * 
	 * If no bounds exists this does nothing. Call hasBounds if you wish to know if this will do
	 * something.
	 * 
	 * Multiple calls will be ignored beyond the first.
	 * 
	 * @return
	 */
	public RouteDatastoreFilterBuilder startIntersection() {
		startArea();
		return this;
	}

	/**
	 * Activities provide a spatial buffer. this will provide those routes that have starts
	 * intersecting the activity's buffer.
	 * 
	 * @param activity
	 * @return
	 */
	public RouteDatastoreFilterBuilder startIntersection(Activity activity) {
		startArea().intersection(activity.getTrackSummary().getSpatialBuffer());
		intersectionSort();
		return this;
	}

	public RouteDatastoreFilterBuilder startIntersection(GeoCells geoCells) {
		startArea().intersection(geoCells);
		intersectionSort();
		return this;
	}

	/**
	 * Adds the bounding box to the query against the start cells if not already added.
	 * 
	 * @param boundingBox
	 */
	public RouteDatastoreFilterBuilder startIntersection(BoundingBox boundingBox) {
		startArea().intersection(boundingBox);
		intersectionSort();
		return this;
	}

	private void intersectionSort() {
		// TN-756 disabled sorting by score with bounds...use search service.
		orderByNone();
	}

	public GeoCellFilterBuilder startArea() {

		if (this.startArea == null) {
			this.startArea = GeoCellFilterBuilder.mutate(building).key(START_AREA_KEY)
					.maxNumberOfCells(this.maxNumberOfCells).build();
		}
		return this.startArea;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.ObjectBuilder#build()
	 */
	@Override
	public Filter build() {
		SearchUtils.isDatastoreFilter(this);
		// call start area every time so filter attributes can be added autoamtically
		startArea();
		if (startArea != null) {
			startArea.build();
			startArea = null;
		}

		Filter built = super.build();

		return built;
	}

}
