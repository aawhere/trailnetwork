/**
 * 
 */
package com.aawhere.route;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.ActivityType;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.person.PersonId;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroups;
import com.aawhere.route.event.RouteEvents;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Manages the {@link SocialGroups} created to support a {@link Route} using
 * {@link RouteCompletions}, {@link RouteEvents} and whatever else is relevant.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@ApiModel(
		description = "Focusing on a single Route, this associates the Persons that completed the Route(RouteCompletions) to a SocialGroup so they are related. SocialGroups created are limited by ActivityType.")
public class RouteCompletionSocialGroupOrganizer {

	@ApiModelProperty(value = "The Route completed by the SocialGroupMemberships.")
	@XmlElement
	private Route routeTargeted;
	@ApiModelProperty(value = "SocialGroups previously created.")
	@XmlElement
	private SocialGroups socialGroupsExisting;
	@ApiModelProperty("SocialGroups after memberships associated.")
	@XmlElement
	private SocialGroups socialGroupsUpdated;

	/**
	 * Used to construct all instances of RouteSocialGroupOrganizer.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteCompletionSocialGroupOrganizer> {

		/**
		 * A connected function that will persist the social groups (create or update), register the
		 * memerships and return the updated group.
		 * 
		 */
		private Function<Entry<SocialGroup, ? extends Iterable<Entry<PersonId, Integer>>>, SocialGroup> membershipUpdateFunction;
		private Multimap<SocialGroup, Entry<PersonId, Integer>> membershipsToBeRegistered;
		private Iterable<RouteCompletion> completions;

		public Builder() {
			super(new RouteCompletionSocialGroupOrganizer());
		}

		public Builder socialGroupsExisting(@Nullable SocialGroups existingSocialGroups) {
			building.socialGroupsExisting = existingSocialGroups;
			// this technique ensures the client is calling,but handles the null case which is
			// common from route. This will get past the null assertion now during validation
			if (building.socialGroupsExisting == null) {
				building.socialGroupsExisting = SocialGroups.create().build();
			}
			return this;
		}

		public Builder completions(Iterable<RouteCompletion> completions) {
			this.completions = completions;
			return this;
		}

		/**
		 * @see #membershipUpdateFunction
		 * @param membershipUpdateFunction
		 * @return
		 */
		public
				Builder
				membershipUpdateFunctions(
						Function<Entry<SocialGroup, ? extends Iterable<Entry<PersonId, Integer>>>, SocialGroup> membershipUpdateFunction) {
			this.membershipUpdateFunction = membershipUpdateFunction;
			return this;
		}

		/**
		 * Converts the completions into social groups and related memberships.
		 * 
		 * @param organizer
		 */
		private void matchGroupsToMemberships(RouteCompletionSocialGroupOrganizer organizer) {
			membershipsToBeRegistered = HashMultimap.create();
			Route route = organizer.routeTargeted;
			if (route.isScoreSignificant()) {

				final ImmutableListMultimap<ActivityType, RouteCompletion> activityTypesMapped = RouteCompletionUtil
						.activityTypesMapped(completions);
				// for each activity type
				for (ActivityType activityType : activityTypesMapped.keySet()) {

					final String completedCategory = RouteCompletionSocialGroupOrganizer
							.completedCategory(activityType);
					SocialGroup socialGroup = SocialGroup.create().context(route.id()).category(completedCategory)
							.score(route.score()).build();

					// keep only those matching the activity type
					Iterable<RouteCompletion> activityTypeCompletions = activityTypesMapped.get(activityType);

					// register the persons in the group
					final Set<Entry<PersonId, Integer>> personIds = Sets.newHashSet(RouteCompletionUtil
							.personalTotalsIdentified(activityTypeCompletions));
					if (!personIds.isEmpty()) {
						membershipsToBeRegistered.putAll(socialGroup, personIds);
					}
				}
			}
		}

		private void socialGroupMemershipsPersisted(RouteCompletionSocialGroupOrganizer organizer) {
			SocialGroups.Builder updatedSocialGroups = SocialGroups.create();
			for (SocialGroup socialGroup : membershipsToBeRegistered.keySet()) {
				Collection<Entry<PersonId, Integer>> pendingMemberships = membershipsToBeRegistered.get(socialGroup);
				Pair<SocialGroup, Collection<Entry<PersonId, Integer>>> pair = Pair.of(socialGroup, pendingMemberships);
				SocialGroup updatedGroup = membershipUpdateFunction.apply(pair);
				updatedSocialGroups.add(updatedGroup);
			}
			organizer.socialGroupsUpdated = updatedSocialGroups.build();
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.exceptions().notNull("routeTargeted", building.routeTargeted);
			Assertion.exceptions().notNull("socialGroupsExisting", building.socialGroupsExisting);
			Assertion.exceptions().notNull("membershipUpdateFunction", this.membershipUpdateFunction);
			Assertion.exceptions().notNull("completions", this.completions);
		}

		@Override
		public RouteCompletionSocialGroupOrganizer build() {
			RouteCompletionSocialGroupOrganizer built = super.build();
			matchGroupsToMemberships(built);
			socialGroupMemershipsPersisted(built);
			return built;
		}

		/**
		 * @param routeTargeted
		 * @return
		 */
		public Builder routeTargeted(Route routeTargeted) {
			building.routeTargeted = routeTargeted;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct RouteSocialGroupOrganizer */
	private RouteCompletionSocialGroupOrganizer() {
	}

	/**
	 * @return
	 */
	public SocialGroups socialGroupsExisting() {
		return this.socialGroupsExisting;
	}

	/**
	 * @return the routeTargeted
	 */
	public Route routeTargeted() {
		return this.routeTargeted;
	}

	/**
	 * @return the updatedSocialGroups
	 */
	public SocialGroups socialGroupsUpdated() {
		return this.socialGroupsUpdated;
	}

	/**
	 * @param socialGroup
	 * @return
	 */
	public static ActivityType activityType(SocialGroup socialGroup) {
		String category = socialGroup.category();
		String[] parts = IdentifierUtils.splitCompositeIdValue(category);
		return ActivityType.fromString(parts[1]);
	}

	public static String completedCategory(@Nonnull ActivityType activityType) {
		return IdentifierUtils.createCompositeIdValue(RouteCompletionField.DOMAIN, activityType.toLowerCase());
	}

}
