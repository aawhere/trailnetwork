/**
 * 
 */
package com.aawhere.route.event;

import static com.aawhere.route.event.RouteEvent.FIELD.KEY.*;

import com.aawhere.activity.ActivityType;
import com.aawhere.measure.geocell.GeoCellFilterBuilder;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseFilterBuilder;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.route.RouteId;

/**
 * {@link Filter} for {@link RouteEvent}.
 * 
 * @author aroller
 * 
 */
public class RouteEventFilterBuilder
		extends BaseFilterBuilder<RouteEventFilterBuilder> {

	RouteEventFilterBuilder() {

	}

	RouteEventFilterBuilder(Filter filter) {
		super(filter);
	}

	private GeoCellFilterBuilder startArea;

	public RouteEventFilterBuilder routeId(RouteId routeId) {
		return setParent(routeId);
	}

	public RouteEventFilterBuilder orderByScoreHighestFirst() {
		return orderBy(SCORE, Direction.DESCENDING);
	}

	public static RouteEventFilterBuilder create() {
		return new RouteEventFilterBuilder();
	}

	public static RouteEventFilterBuilder clone(Filter filter) {
		return new RouteEventFilterBuilder(Filter.cloneForBuilder(filter));
	}

	/**
	 * Limts to the given activity type.
	 * 
	 * @param activityType
	 * @return
	 */
	public RouteEventFilterBuilder activityType(ActivityType activityType) {
		return addCondition(FilterCondition.create().field(ACTIVITY_TYPE).operator(FilterOperator.EQUALS)
				.value(activityType).build());
	}

	public GeoCellFilterBuilder startArea() {
		if (this.startArea == null) {
			this.startArea = GeoCellFilterBuilder.mutate(building).key(START_AREA).build();
		}
		orderByNone();
		return this.startArea;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Filter.BaseBuilder#build()
	 */
	@Override
	public Filter build() {

		// make this the default.
		if (!building.isOrderByNone()) {
			orderByScoreHighestFirst();
		}
		if (this.startArea != null) {
			this.startArea.build();
		}
		return super.build();
	}

}
