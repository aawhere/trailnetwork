/**
 * 
 */
package com.aawhere.route;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * A specialty xml adapter that takes the given route and loads the route from the id during a
 * marshal. This is necessary to upgrade the empty shell of a route loaded by Objectify as the
 * parent.
 * 
 * @author aroller
 * 
 */
@Singleton
public class RouteXmlAdapter
		extends XmlAdapter<Route, Route> {

	final private RouteIdXmlAdapter idAdapter;

	/**
	 * 
	 */
	public RouteXmlAdapter() {
		this.idAdapter = new RouteIdXmlAdapter();
	}

	@Inject
	public RouteXmlAdapter(@Nonnull RouteIdXmlAdapter idAdapter) {
		this.idAdapter = idAdapter;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Route unmarshal(Route route) throws Exception {
		return route;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Route marshal(Route route) throws Exception {
		return (route != null) ? idAdapter.marshal(route.id()) : null;
	}

	@Singleton
	public static class ForCompletion
			extends RouteXmlAdapter {

		public ForCompletion() {
			super();
		}

		@Inject
		public ForCompletion(RouteIdXmlAdapter idAdapter) {
			super(idAdapter);
		}

	}

	@Singleton
	public static class ForEvent
			extends RouteXmlAdapter {

		public ForEvent() {
			super();
		}

		@Inject
		public ForEvent(RouteIdXmlAdapter idAdapter) {
			super(idAdapter);
		}

	}
}
