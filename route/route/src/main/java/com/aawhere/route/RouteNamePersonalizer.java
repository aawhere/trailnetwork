/**
 *
 */
package com.aawhere.route;

import java.util.Locale;

import javax.measure.quantity.Length;

import com.aawhere.activity.ActivityMessage;
import com.aawhere.activity.ActivityType;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.personalize.BasePersonalizer;
import com.aawhere.personalize.Personalizer;
import com.aawhere.personalize.QuantityDisplayPersonalizer;
import com.aawhere.route.RouteMessage.Param;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadMessage;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessageDisplay;
import com.aawhere.util.rb.MessageFormatter;
import com.aawhere.util.rb.MessagePersonalizer;
import com.aawhere.util.rb.ParamWrappingCompleteMessage;

/**
 * A {@link Personalizer} for {@link Route#name}s to be transformed into a {@link MessageDisplay} by
 * {@link RouteNameXmlAdapter}.
 * 
 * @author Brian Chapman
 * 
 */
public class RouteNamePersonalizer
		extends BasePersonalizer<MessageDisplay> {

	/** A convenience method for constructing a builder */
	public static Builder create() {
		return new Builder();
	}

	private RouteTrailheadProvider trailheadProvider;
	private MessagePersonalizer messagePersonalizer;
	private QuantityDisplayPersonalizer quantityPersonalizer;

	/** Use {@link Builder} to construct RouteNamePersonalizer */
	private RouteNamePersonalizer() {
	}

	/**
	 * Used to construct all instances of MessagePersonalizer.
	 */
	public static class Builder
			extends BasePersonalizer.Builder<RouteNamePersonalizer, Builder> {

		public Builder() {
			super(new RouteNamePersonalizer());
		}

		public Builder routeTrailheadProvider(RouteTrailheadProvider trailheadProvider) {
			building.trailheadProvider = trailheadProvider;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.personalize.BasePersonalizer.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.personalize.BasePersonalizer.Builder#build()
		 */
		@Override
		public RouteNamePersonalizer build() {
			RouteNamePersonalizer built = super.build();
			built.messagePersonalizer = MessagePersonalizer.create().setPreferences(built.prefs).build();
			built.quantityPersonalizer = new QuantityDisplayPersonalizer.Builder().setPreferences(built.prefs).build();
			return built;
		}

	}// end Builder

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#personalize(java.lang.Object)
	 */
	@Override
	public void personalize(Object object) {
		RouteNameXml routeXml = (RouteNameXml) object;

		if (trailheadProvider != null
				&& (routeXml.name.equals(RouteMessage.UNKNOWN) || routeXml.name.equals(ActivityMessage.UNKNOWN))) {
			final Locale locale = getLocale();
			final String value = defaultRouteName(routeXml);
			result = MessageDisplay.create().value(value).locale(locale).build();
		} else {
			messagePersonalizer.personalize(routeXml.name);
			result = messagePersonalizer.getResult();
		}

	}

	protected String defaultRouteName(RouteNameXml routeXml) {

		quantityPersonalizer.personalize(routeXml.distance);
		String distanceFormatted = quantityPersonalizer.getResult().getFormatted();

		Trailhead start = null;
		Trailhead finish = null;
		Message startName;
		Message finishName;
		Message activityType = routeXml.completionStats.activityTypeStats().first().category().nounAdjectivePhrase;
		try {
			start = trailheadProvider.entity(routeXml.startThId);
			startName = start.name();
			finish = trailheadProvider.entity(routeXml.finishThId);
			finishName = finish.name();
		} catch (EntityNotFoundException e) {
			startName = TrailheadMessage.UNKNOWN;
			finishName = TrailheadMessage.UNKNOWN;
		}

		// Although this is part of the ICU i18n package, it doesn't appear to support different
		// Locale
		// and assumes Locale.US all the time.
		// TODO: determine if it is appropriate to always covert to title case.
		String startNameFormatted = formatMessage(startName);
		String finishNameFormatted = formatMessage(finishName);
		DefaultNameFormat nameFormat;

		if (start.id().equals(finish.id())) {
			nameFormat = DefaultNameFormat.START_TH;
		} else {
			nameFormat = DefaultNameFormat.START_AND_FINISH_TH;
		}

		// Don't show the trailheads if they are unknown.
		if (startName.equals(TrailheadMessage.UNKNOWN)) {
			nameFormat = DefaultNameFormat.NO_TH;
		}

		// the activity type is always singluar.
		CompleteMessage activityTypeMessage = ParamWrappingCompleteMessage.create(activityType)
				.paramWrapped(ActivityType.Param.COUNT, new Integer(1)).build();

		CompleteMessage nameCompleteMessage = CompleteMessage.create(RouteMessage.DEFAULT_ROUTE_NAME)//
				.addParameter(Param.NAME_FORMAT, nameFormat)//
				.addParameter(Param.DISTANCE_FORMATTED, distanceFormatted)//
				.addParameter(Param.TH_FINISH, finishNameFormatted)//
				.addParameter(Param.TH_START, startNameFormatted)//
				.addParameter(Param.ACTIVITY_TYPE, activityTypeMessage).build();

		return MessageFormatter.create().message(nameCompleteMessage).locale(getLocale()).build().getFormattedMessage();
	}

	private String formatMessage(Message message) {
		return MessageFormatter.create().message(message).locale(getLocale()).build().getFormattedMessage();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#handlesType()
	 */
	@Override
	public Class<?> handlesType() {
		return Message.class;
	}

	public static enum DefaultNameFormat {
		START_AND_FINISH_TH, START_TH, NO_TH;
	}

	/**
	 * A data transfer object to work around JaxB and allow name to be marshalled and unmarshalled.
	 * 
	 * @author Brian Chapman
	 * 
	 */
	public static class RouteNameXml {
		private TrailheadId startThId;
		private TrailheadId finishThId;
		private Message name;
		private Length distance;
		private RouteStats completionStats;

		/**
		 * @return the startThId
		 */
		public TrailheadId startThId() {
			return this.startThId;
		}

		/**
		 * @return the name
		 */
		public Message name() {
			return this.name;
		}

		/**
		 * @return the finishThId
		 */
		public TrailheadId finishThId() {
			return this.finishThId;
		}

		/**
		 * @return the distance
		 */
		public Length distance() {
			return this.distance;
		}

		/**
		 * @return the completionStats
		 */
		public RouteStats completionStats() {
			return this.completionStats;
		}

		/**
		 * Used to construct all instances of RouteNamePersonalizer.RouteNameXml.
		 */
		public static class Builder
				extends ObjectBuilder<RouteNamePersonalizer.RouteNameXml> {

			public Builder() {
				super(new RouteNamePersonalizer.RouteNameXml());
			}

			Builder(RouteNameXml xml) {
				super(xml);
			}

			public Builder startTrailheadId(TrailheadId thId) {
				building.startThId = thId;
				return this;
			}

			public Builder finishTrailheadId(TrailheadId thId) {
				building.finishThId = thId;
				return this;
			}

			public Builder name(Message name) {
				building.name = name;
				return this;
			}

			public Builder distance(Length distance) {
				building.distance = distance;
				return this;
			}

			public Builder completionStats(RouteStats completionStats) {
				building.completionStats = completionStats;
				return this;
			}

		}// end Builder

		/** Use {@link Builder} to construct RouteNamePersonalizer.RouteNameXml */
		private RouteNameXml() {
		}

		public static Builder create() {
			return new Builder();
		}

		static Builder mutate(RouteNameXml xml) {
			return new Builder(xml);
		}

	}

}
