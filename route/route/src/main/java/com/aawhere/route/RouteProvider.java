/**
 * 
 */
package com.aawhere.route;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceStandard;
import com.aawhere.persist.ServiceStandardBase;

import com.google.common.base.Function;

/**
 * Anything that can provide a route when provided with a {@link RouteId}.
 * 
 * @author aroller
 * 
 */
public interface RouteProvider
		extends Function<RouteId, Route>, ServiceStandard<Routes, Route, RouteId> {

	public Route route(RouteId id) throws EntityNotFoundException;

	public static class Default
			extends ServiceStandardBase<Routes, Route, RouteId>
			implements RouteProvider {

		public Default() {
			super();
		}

		public Default(Function<RouteId, Route> idFunction) {
			super(idFunction);
		}

		public Default(Iterable<Route> entities) {
			super(entities);
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.common.base.Function#apply(java.lang.Object)
		 */
		@Override
		public Route apply(RouteId input) {
			return idFunction().apply(input);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.route.RouteProvider#route(com.aawhere.route.RouteId)
		 */
		@Override
		public Route route(RouteId id) throws EntityNotFoundException {
			return apply(id);
		}

	}
}
