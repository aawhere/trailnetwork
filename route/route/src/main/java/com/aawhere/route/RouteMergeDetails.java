/**
 * 
 */
package com.aawhere.route;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.view.ViewCount;
import com.aawhere.xml.XmlNamespace;

/**
 * Provides information about decisions made during the merging of multiple routes.
 * 
 * package mutation allowing service to modify to reduce code and maintenance since it is mostly an
 * internal transport object and api reporting tool.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteMergeDetails
		implements Serializable {

	@XmlElement
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	Iterable<RouteId> routesToMerge;

	@XmlElement
	Collection<Route> mergeCandidates;

	@XmlElement
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	Set<RouteId> routeIdsInTheGroup;

	@XmlElement
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	Iterable<RouteId> aliases;

	@XmlElement
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	Iterable<RouteId> aliasesWorthKeeping;

	@XmlElement
	Collection<ViewCount> aliasViews;

	@XmlElement
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	Set<RouteId> parentsLeavingOrphans = new HashSet<RouteId>();

	@XmlElement
	Route keeper;

	@XmlElement
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	Set<RouteId> aliasesExisting;

	@XmlElement
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	Set<RouteId> aliasesViewedEnough = new HashSet<RouteId>();

	/** Use {@link Builder} to construct RouteMergeDetails */
	RouteMergeDetails() {
	}

	private static final long serialVersionUID = -4976025350368374592L;

	/**
	 * @return
	 */
	public Route keeper() {
		return keeper;
	}

}
