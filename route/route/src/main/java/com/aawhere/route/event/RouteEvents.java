/**
 * 
 */
package com.aawhere.route.event;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.persist.Entities;
import com.aawhere.route.Route;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class RouteEvents
		extends BaseFilterEntities<RouteEvent> {

	/**
	 * Used to construct all instances of RouteEvents.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, RouteEvent, RouteEvents> {

		public Builder() {
			super(new RouteEvents());
		}

		@Override
		public RouteEvents build() {
			RouteEvents built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteEvents */
	private RouteEvents() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntities#getCollection()
	 */
	@Override
	@XmlElement(name = RouteEvent.FIELD.DOMAIN)
	public List<RouteEvent> getCollection() {
		return super.getCollection();
	}

}
