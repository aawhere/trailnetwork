/**
 *
 */
package com.aawhere.route;

import com.aawhere.identity.IdentityManager;
import com.aawhere.personalize.xml.PersonalizedXmlAdapter;
import com.aawhere.route.RouteNamePersonalizer.RouteNameXml;
import com.aawhere.util.rb.MessageDisplay;
import com.aawhere.util.rb.StringMessage;

import com.google.inject.Inject;

/**
 * @author Brian Chapman
 * 
 */
public class RouteNameXmlAdapter
		extends PersonalizedXmlAdapter<MessageDisplay, RouteNameXml> {

	public RouteNameXmlAdapter() {
		super(new RouteNamePersonalizer.Builder());
	}

	public RouteNameXmlAdapter(RouteNamePersonalizer plizer) {
		super(plizer);
	}

	@Inject
	public RouteNameXmlAdapter(IdentityManager identityManager, RouteTrailheadProvider trailheadProvider) {
		super(new RouteNamePersonalizer.Builder().setPreferences(identityManager.preferences())
				.routeTrailheadProvider(trailheadProvider).build());
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public RouteNameXml showUnmarshal(MessageDisplay xml) throws Exception {
		RouteNameXml route = RouteNameXml.create().name(new StringMessage(xml.value())).build();
		return route;
	}
}