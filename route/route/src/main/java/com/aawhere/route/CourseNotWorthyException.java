/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.route.RouteMessage.*;
import static com.aawhere.route.RouteMessage.Param.*;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityIdentifier;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates that a course requested is not worthy of becoming a route. Provides appropriate
 * explanations of why.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.ACCEPTED)
public class CourseNotWorthyException
		extends BaseException {

	private static final long serialVersionUID = 1L;
	private Integer score;

	/**
	 * 
	 */
	CourseNotWorthyException(Integer score, CompleteMessage message) {
		super(message);
		this.score = score;
	}

	CourseNotWorthyException(CompleteMessage message) {
		super(message);
	}

	public static CourseNotWorthyException lowScore(ActivityIdentifier courseOrLeaderboardId, Integer score,
			Integer minScore) {
		return new CourseNotWorthyException(score, CompleteMessage.create(SCORE_TOO_LOW)
				.addParameter(COURSE_ID, courseOrLeaderboardId).addParameter(SCORE, score)
				.addParameter(MIN_SCORE, minScore).build());
	}

	/**
	 * @return the score
	 */
	public Integer score() {
		return this.score;
	}

	/**
	 * Use this when the course is not found in any reference of {@link RouteCompletion} to a
	 * {@link Route}.
	 * 
	 * @param courseId
	 * @return
	 */
	public static CourseNotWorthyException notRelated(ActivityId courseId) {
		return new CourseNotWorthyException(CompleteMessage.create(COURSES_NOT_RELATED).param(COURSE_ID, courseId)
				.build());
	}
}
