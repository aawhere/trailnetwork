/**
 * 
 */
package com.aawhere.route.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityType;
import com.aawhere.field.DictionaryIndex;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierToIdentifiableXmlAdapter;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.Count;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupField;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.person.group.SocialGroupProvider;
import com.aawhere.person.group.SocialGroupRelated;
import com.aawhere.route.Route;
import com.aawhere.route.RouteId;
import com.aawhere.route.RouteProvider;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.util.rb.Message;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.PojoIf;

/**
 * A completion of a {@link Route} at a specific Time {@link Interval}.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Cache
@Dictionary(domain = RouteEvent.FIELD.DOMAIN, indexes = { RouteEvent.AreaActivityTypeIndex.class,
		RouteEvent.ActivityTypeForRouteByScoreIndex.class })
public class RouteEvent
		extends StringBaseEntity<RouteEvent, RouteEventId>
		implements SelfIdentifyingEntity, Cloneable, SocialGroupRelated {

	static final Integer SCORE_DEFAULT = 0;

	@XmlElement
	private Message name;

	@XmlElement(name = FIELD.START_TIME)
	@Field(key = FIELD.START_TIME)
	private DateTime startTime;

	@XmlElement(name = FIELD.DURATION)
	@Field
	private Duration duration;

	@XmlElement(name = RouteEventField.SOCIAL_GROUP_ID)
	@Field(xmlAdapter = RouteEvent.SocialGroupXmlAdapter.class)
	private SocialGroupId socialGroupId;

	/**
	 * A combination of the route score, number of participants in the group, number of votes,
	 * shares, saves, and how recent the event has taken place and anything else that may influence
	 * the general person's wish to see this.
	 * 
	 * TN-768 selective index causes inconsistencies.
	 */
	@Field(key = FIELD.SCORE, indexedFor = "TN-708")
	@Index
	@XmlAttribute
	private Integer score = SCORE_DEFAULT;

	@XmlElement
	private Count completionsCount;

	@XmlElement(name = FIELD.ACTIVITY_TYPE)
	@Field(key = FIELD.ACTIVITY_TYPE, indexedFor = "TN-706")
	@Index(IfSignificant.class)
	private ActivityType activityType;

	@XmlElement
	@Field(key = FIELD.START_AREA, indexedFor = "TN-706")
	@Index(IfSignificant.class)
	private GeoCells startArea;

	/**
	 * Used to construct all instances of RouteEvent.
	 */
	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<RouteEvent, Builder, RouteEventId> {

		public Builder() {
			super(new RouteEvent());
		}

		/**
		 * @param routeEvent
		 */
		Builder(RouteEvent routeEvent) {
			super(routeEvent);
		}

		public Builder route(Route route) {
			route(route.id());
			building.startArea = route.startArea();
			return this;
		}

		public Builder route(RouteId routeId) {
			super.parentId(routeId);
			return this;
		}

		public Builder activityType(ActivityType activityType) {
			building.activityType = activityType;
			return this;
		}

		public Builder interval(Interval interval) {
			building.startTime = interval.getStart();
			building.duration = interval.toDuration();
			return this;
		}

		public Builder socialGroupId(SocialGroupId groupId) {
			building.socialGroupId = groupId;
			return this;
		}

		public Builder name(Message name) {
			building.name = name;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("activityType", building.activityType);
			// Assertion.exceptions().notNull("startArea", building.startArea);
			Assertion.exceptions().notNull("score", building.score);
			// start and route must be validated before building
		}

		@Override
		public RouteEvent build() {

			// these are here since the id must be set before building.
			// validate is called by super#build()
			Assertion.exceptions().notNull("route", building.routeId());
			Assertion.exceptions().notNull("startTime", building.startTime);

			if (!hasId()) {
				setId(RouteEventUtil.routeEventId(building.routeId(), building.interval()));
			}
			RouteEvent built = super.build();
			return built;
		}

		/**
		 * @param routeEventScore
		 * @return
		 */
		public Builder score(Integer routeEventScore) {
			building.score = routeEventScore;
			return this;
		}

		public Builder completionsCount(Integer numberOfParticipants) {
			return completionsCount(MeasurementUtil.count(numberOfParticipants));
		}

		public Builder completionsCount(Count numberOfParticipants) {
			building.completionsCount = numberOfParticipants;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteEvent */
	private RouteEvent() {
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder clone(RouteEvent event) {
		try {
			return mutate((RouteEvent) event.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	static Builder mutate(RouteEvent routeEvent) {
		return new Builder(routeEvent);
	}

	/**
	 * @return the interval
	 */
	public Interval interval() {
		return new Interval(this.startTime, this.duration);
	}

	@XmlElement
	public DateTime getFinishTime() {
		return interval().getEnd();
	}

	/**
	 * @return the routeId
	 */
	@Field(key = FIELD.ROUTE_ID, indexedFor = "TN-572", xmlAdapter = RouteEvent.RouteXmlAdapter.class)
	public RouteId routeId() {
		return (RouteId) getParentId();
	}

	/**
	 * @return the activityType
	 */
	public ActivityType activityType() {
		return this.activityType;
	}

	/**
	 * @return the groupId
	 */
	@Override
	public SocialGroupId socialGroupId() {
		return this.socialGroupId;
	}

	@XmlElement(name = SocialGroupField.DOMAIN)
	@XmlJavaTypeAdapter(RouteEvent.SocialGroupXmlAdapter.class)
	public SocialGroupId getSocialGroupIdXml() {
		return socialGroupId;
	}

	/**
	 * notice the adapter type in field is for Route, not Route Id to match with {@link #route}
	 * 
	 * @return
	 */
	@XmlElement(name = FIELD.ROUTE_ID)
	public RouteId getRouteId() {
		return routeId();
	}

	@XmlElement(name = Route.FIELD.DOMAIN)
	@XmlJavaTypeAdapter(RouteEvent.RouteXmlAdapter.class)
	private RouteId getRouteXml() {
		return routeId();
	}

	@Override
	protected Class<? extends Identifier<?, ?>> parentIdType() {
		return RouteId.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@com.aawhere.field.annotation.Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM,
			indexedFor = "TN-362")
	@Override
	public RouteEventId getId() {
		return super.getId();
	}

	@XmlTransient
	public static class FIELD
			extends BaseEntity.BASE_FIELD {
		public static final String DOMAIN = "routeEvent";
		public static final String START_AREA = Route.FIELD.START_AREA;
		public static final String START_TIME = "startTime";
		public static final String FINISH_TIME = "startTime";
		public static final String DURATION = "duration";
		public static final String ROUTE_ID = RouteWebApiUri.ROUTE_ID;
		public static final String ROUTE_EVENT_ID = RouteEventWebApiUri.ROUTE_EVENT_ID;
		public static final String ACTIVITY_TYPE = Activity.FIELD.ACTIVITY_TYPE;
		public static final String SCORE = SystemWebApiUri.SCORE;

		public static class KEY {
			public static final FieldKey ROUTE_ID = new FieldKey(DOMAIN, FIELD.ROUTE_ID);
			public static final FieldKey START_AREA = new FieldKey(DOMAIN, FIELD.START_AREA);
			public static final FieldKey SCORE = new FieldKey(DOMAIN, FIELD.SCORE);
			public static final FieldKey START_TIME = new FieldKey(DOMAIN, FIELD.START_TIME);
			public static final FieldKey START_AREA_CELLS = new FieldKey(KEY.START_AREA, GeoCells.FIELD.CELLS);
			public static final FieldKey ACTIVITY_TYPE = new FieldKey(DOMAIN, FIELD.ACTIVITY_TYPE);

			public static final class VALUE {
				public static final String SCORE = DOMAIN + FieldUtils.KEY_DILIM + FIELD.SCORE;
				public static final String ROUTE_ID = DOMAIN + FieldUtils.KEY_DILIM + FIELD.ROUTE_ID;
			}
		}

		public static class OPTION {
			public static final String ROUTE = new FieldKey(DOMAIN, Route.FIELD.DOMAIN).getAbsoluteKey();
			public static final String TOUCH = SystemWebApiUri.TOUCH;
		}
	}

	private static final long serialVersionUID = 7059577445406080322L;

	/**
	 * @return
	 */
	public GeoCells startArea() {
		return this.startArea;
	}

	/**
	 * @return the name
	 */
	public Message name() {
		return this.name;
	}

	/**
	 * @return the startTime
	 */
	public DateTime startTime() {
		return this.startTime;
	}

	/**
	 * Used to reduce costs and noise keeping those events with enough participants or for other
	 * reasons.
	 */
	public static class IfSignificant
			extends PojoIf<RouteEvent> {
		@Override
		public boolean matchesPojo(RouteEvent event) {
			return event.score != null && event.score >= SCORE_DEFAULT;
		}

	}

	/**
	 * Allows sorting by score, area and activity type. TN-706
	 * 
	 * @author aroller
	 * 
	 */
	public static class AreaActivityTypeIndex
			extends DictionaryIndex {
		public AreaActivityTypeIndex() {
			super(FIELD.KEY.ACTIVITY_TYPE, FIELD.KEY.START_AREA_CELLS, FIELD.KEY.SCORE);
		}
	}

	public static class ActivityTypeForRouteByScoreIndex
			extends DictionaryIndex {
		/**
		 * 
		 */
		public ActivityTypeForRouteByScoreIndex() {
			super(FIELD.KEY.ACTIVITY_TYPE, FIELD.KEY.ROUTE_ID, FIELD.KEY.SCORE);
		}
	}

	/**
	 * @return
	 */
	public Integer score() {
		return score;
	}

	/**
	 * @return the completionsCount
	 */
	public Count completionsCount() {
		return this.completionsCount;
	}

	/** Passes through route with a refresh if capable. */
	@Singleton
	public static class RouteXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<Route, RouteId> {

		public RouteXmlAdapter() {
			super();
		}

		@Inject
		public RouteXmlAdapter(RouteProvider provider) {
			super(provider);
		}

		/** This accepts ids since the field it's associated with is the id to support bulk loading. */
		public RouteXmlAdapter(Function<RouteId, Route> provider) {
			super(provider);
		}
	}

	/**
	 * @return
	 */
	public Boolean hasSocialGroup() {
		return this.socialGroupId != null;
	}

	/**
	 * Inidciates if the score is better than the default.
	 * 
	 * @return
	 */
	public Boolean scoreIsSignficant() {
		return !SCORE_DEFAULT.equals(score);
	}

	@Singleton
	public static class SocialGroupXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<SocialGroup, SocialGroupId> {
		public SocialGroupXmlAdapter() {
		}

		public SocialGroupXmlAdapter(Function<SocialGroupId, SocialGroup> provider) {
			super(provider);
		}

		@Inject
		public SocialGroupXmlAdapter(SocialGroupProvider socialGroupProvider) {
			super(socialGroupProvider.idFunction());
		}
	}
}
