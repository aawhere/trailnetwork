/**
 *
 */
package com.aawhere.route;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections4.CollectionUtils;

import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.activity.RelativeStatisticDescriptionPersonalizer;
import com.aawhere.lang.Assertion;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.PersonProvider;
import com.aawhere.personalize.BasePersonalizer;
import com.aawhere.personalize.NotHandledException;
import com.aawhere.personalize.PersonalizedBase;
import com.aawhere.personalize.xml.LocaleXmlAdapter;
import com.aawhere.personalize.xml.XmlLang;
import com.aawhere.route.RouteDescriptionMessage.Param;
import com.aawhere.route.RouteDescriptionPersonalizer.RouteDescription;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageFormatter;
import com.aawhere.util.rb.ParamWrappingCompleteMessage;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.Lists;

/**
 * @author Brian Chapman
 * 
 */
public class RouteDescriptionPersonalizer
		extends BasePersonalizer<RouteDescription> {

	private final Logger logger = LoggerFactory.getLogger(RouteDescriptionPersonalizer.class);

	private RouteTrailheadProvider trailheadProvider;

	private PersonProvider personProvider;

	/**
	 * Used to construct all instances of BaseDurationPersonalizer.
	 */
	/* @formatter:off */
	public static class Builder extends BasePersonalizer.Builder<RouteDescriptionPersonalizer,
	Builder> {
	/* @formatter:on */
		public Builder() {
			super(new RouteDescriptionPersonalizer());
		}

		public Builder trailheadProvider(RouteTrailheadProvider provider) {
			building.trailheadProvider = provider;
			return this;
		}

		/** @see #personProvider */
		public Builder personProvider(PersonProvider personProvider) {
			building.personProvider = personProvider;
			return this;
		}

		@Override
		public void validate() {
			super.validate();
			Assertion.assertNotNull("TrailheadProvider", building.trailheadProvider);
		}

		@Override
		public RouteDescriptionPersonalizer build() {

			return super.build();
		}

	}

	/** Use {@link Builder} to construct BaseDurationPersonalizer */
	protected RouteDescriptionPersonalizer() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#personalize(java.lang.Object)
	 */
	@Override
	public void personalize(Object context) {
		if (context instanceof Route) {
			result = new RouteDescription();
			personalizeRoute(((Route) context));
		} else {
			throw new NotHandledException(handlesType(), context);
		}

	}

	private void personalizeRoute(Route route) {
		formatCompletionStats(route);
		formatTrailheadName(route);
		formatActivityTypes(route);
		formatVariations(route);
		formatRelativeStats(route);
		result.lang = getLocale();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#handlesType()
	 */
	@Override
	public Class<?> handlesType() {
		return Route.class;
	}

	private void formatCompletionStats(Route route) {
		if (route.socialCompletionStats() == null || route.completionStats() == null) {
			return;
		}
		RouteDescriptionMessage completionStatsMessage = RouteDescriptionMessage.COMPLETION_STATS;

		CompleteMessage completionStatsCompleteMessage = ParamWrappingCompleteMessage.create(completionStatsMessage)
				.paramWrapped(Param.PERSON_COUNT, route.socialCompletionStats().total())
				.paramWrapped(Param.COMPLETION_COUNT, route.completionStats().total()).build();
		String formatted = formatMessage(completionStatsCompleteMessage);
		result.completionStats = formatted;
	}

	private void formatTrailheadName(Route route) {
		Trailhead th;
		TrailheadId trailheadId = route.start();
		try {
			th = trailheadProvider.entity(trailheadId);
		} catch (EntityNotFoundException e) {
			logger.warning("Trailhead " + trailheadId.toString() + " was not found for route "
					+ route.getId().toString());
			return;
		}
		String trailheadName = formatMessage(CompleteMessage.create(th.name()).build());
		result.trailheadName = formatMessage(ParamWrappingCompleteMessage
				.create(RouteDescriptionMessage.STARTS_AT_TRAILHEAD).paramWrapped(Param.TRAILHEAD_NAME, trailheadName)
				.build());
	}

	private void formatActivityTypes(Route route) {
		if (route.completionStats() == null || route.completionStats().activityTypeStats() == null) {
			return;
		}
		// FIXME: toLowerCase should be delegated to the ActivityType. Nouns in german are Title
		// case. Fix unit test to use the same method for testing
		String formattedPrimaryActivityType = formatMessage(CompleteMessage.create(route.completionStats()
				.activityTypeStats().first().category()).build()).toLowerCase();
		result.activityTypes = formatMessage(ParamWrappingCompleteMessage
				.create(RouteDescriptionMessage.ACTIVITY_TYPES)
				.paramWrapped(Param.ACTIVITY_TYPE_COUNT, route.completionStats().activityTypeStats().size())
				.paramWrapped(Param.PRIMARY_ACTIVITY_TYPE, formattedPrimaryActivityType).build());
	}

	private void formatRelativeStats(Route route) {
		List<RelativeStatistic> relativeCompletionStats = route.relativeCompletionStats();
		if (CollectionUtils.isNotEmpty(relativeCompletionStats)) {
			if (personProvider != null) {
				for (RelativeStatistic relativeStatistic : relativeCompletionStats) {
					com.aawhere.util.rb.CompleteMessage.Builder completeMessageBuilder = RelativeStatisticDescriptionPersonalizer
							.params(relativeStatistic, this.personProvider);
					// null builder indicates a message won't be made (i.e. ALL)
					if (completeMessageBuilder != null) {
						completeMessageBuilder.param(Param.COMPLETION_COUNT, relativeStatistic.count());
						Layer layer = relativeStatistic.layer();
						if (layer.personal) {
							completeMessageBuilder.setMessage(RouteDescriptionMessage.RELATIVE_COMPLETIONS_PERSON);
						} else {
							completeMessageBuilder.setMessage(RouteDescriptionMessage.RELATIVE_COMPLETIONS);
						}
						CompleteMessage completeMessage = completeMessageBuilder.build();
						if (result.relativeStats == null) {
							result.relativeStats = Lists.newArrayList();
						}
						result.relativeStats.add(formatMessage(completeMessage));
					}
				}
			} else {
				throw new IllegalStateException(relativeCompletionStats.size()
						+ " relative stats found, but no person provider");
			}
		}
	}

	private void formatVariations(Route route) {
		Integer alternateCount = route.alternateCount();
		if (alternateCount == null) {
			alternateCount = 0;
		}
		result.variations = formatMessage(ParamWrappingCompleteMessage.create(RouteDescriptionMessage.VARIATIONS)
				.paramWrapped(Param.VARIATION_COUNT, alternateCount).build());
	}

	private String formatMessage(CompleteMessage message) {
		return MessageFormatter.create().message(message).locale(getLocale()).build().getFormattedMessage();
	}

	/**
	 * Provides a {@link PersonalizedBase} description of a route, in perspective of end user.
	 * Provides a customized paragraph explaining all about the route, but also provides individual
	 * sentences that may be targeted by a client application in snippets.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static class RouteDescription
			extends PersonalizedBase
			implements Serializable {

		protected String completionStats;
		protected String trailheadName;
		protected String activityTypes;
		protected String variations;
		@XmlAttribute(name = XmlLang.ATTRIBUTE_NAME, required = false)
		@XmlJavaTypeAdapter(LocaleXmlAdapter.class)
		protected Locale lang;
		/** If provided this shows information relating to the filter. */
		List<String> relativeStats;

		private static final long serialVersionUID = -56070682236512819L;
	}

}
