/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.route.RouteCompletion.FIELD.KEY.*;

import java.util.Set;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.lang.Assertion;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseFilterBuilder;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonUtil;
import com.aawhere.route.event.RouteEventId;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class RouteCompletionFilterBuilder
		extends BaseFilterBuilder<RouteCompletionFilterBuilder> {

	public static RouteCompletionFilterBuilder create() {
		return new RouteCompletionFilterBuilder();
	}

	public static RouteCompletionFilterBuilder clone(Filter filter) {
		return new RouteCompletionFilterBuilder(Filter.cloneForBuilder(filter));
	}

	/** @see #create() */
	private RouteCompletionFilterBuilder() {
	}

	/**
	 * @see #clone(Filter)
	 * @param mutant
	 */
	private RouteCompletionFilterBuilder(Filter mutant) {
		super(mutant);
	}

	public RouteCompletionFilterBuilder route(Route route) {
		return routeId(route.id());
	}

	/** Provides the courses for a given route. */
	public RouteCompletionFilterBuilder courses() {
		return addCondition(coursesCondition());
	}

	/**
	 * @return
	 */
	private FilterCondition<Object> coursesCondition() {
		return FilterCondition.create().field(IS_COURSE).operator(FilterOperator.EQUALS).value(Boolean.TRUE).build();
	}

	/**
	 * This will search across all completions that match a given activity id and will run a
	 * predicate against the isCourse to limit the results to only those completions that are
	 * courses. Typically it would be one hence the predicate, but multiples can happen until the
	 * system merges the routes.
	 * 
	 * @param activityId
	 * @return
	 */
	public RouteCompletionFilterBuilder course(ActivityId activityId) {
		coursesPredicate();
		return activity(activityId);

	}

	/**
	 * Provides the courses for a given route, but using a {@link Predicate} Typically combined with
	 * other filters that do not have nor need a complex index.
	 */
	public RouteCompletionFilterBuilder coursesPredicate() {
		return addPredicate(RouteCompletionUtil.isCoursePredicate());
	}

	/** Assigns the parent available for this query. */
	public RouteCompletionFilterBuilder routeId(RouteId routeId) {
		return setParent(routeId);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Filter.BaseBuilder#convertParent(java.lang.String)
	 */
	@Override
	protected RouteId convertParent(String parentIdValue) {
		return new RouteId(parentIdValue);
	}

	public RouteCompletionFilterBuilder activity(Activity activity) {
		return activity(activity.id());
	}

	public RouteCompletionFilterBuilder activity(ActivityId... activityId) {
		return activityIds(Lists.newArrayList(activityId));
	}

	/**
	 * @param activityIds
	 * @return
	 */
	public RouteCompletionFilterBuilder activityIds(Iterable<ActivityId> activityIds) {
		return addCondition(FilterCondition.create().field(ACTIVITY_ID).operator(FilterOperator.IN).value(activityIds)
				.build());
	}

	/**
	 * Looks for all aliases.
	 * 
	 * NOTE: there is no filter for a single person id since it is misleading to believe you will
	 * get complete results without considering the aliases.
	 * 
	 * @param person
	 * @return
	 */
	public RouteCompletionFilterBuilder person(Person person) {
		return personIds(person.aliases());
	}

	public RouteCompletionFilterBuilder persons(Iterable<Person> persons) {

		return personIds(PersonUtil.aliases(persons));
	}

	public RouteCompletionFilterBuilder persons(RouteId routeId, Iterable<Person> persons) {
		return personIds(routeId, PersonUtil.aliases(persons));
	}

	public RouteCompletionFilterBuilder personIds(RouteId routeId, Iterable<PersonId> personIds) {
		return personIds(personIds).routeId(routeId);
	}

	public RouteCompletionFilterBuilder personIds(Iterable<PersonId> personIds) {
		// last moment to enforce uniqueness
		Set<PersonId> set = SetUtilsExtended.asSet(personIds);
		// empty IN means everything...client should avoid this so hard stop
		Assertion.assertNotEmpty("persons", set);
		return addCondition(FilterCondition.create().field(PERSON_ID).operator(FilterOperator.IN).value(set).build());
	}

	/**
	 * sorts by duration ascending so the completion with the shortest duration
	 * 
	 * @return
	 */
	public RouteCompletionFilterBuilder orderByFastest() {
		return orderBySocialRaceRank();
	}

	/**
	 * 
	 * 
	 * /** orders by start time descending so the most recent.
	 * 
	 * @return
	 */
	public RouteCompletionFilterBuilder orderByMostRecentlyRecorded() {
		return orderBy(START_TIME, Direction.DESCENDING);
	}

	/**
	 * Orders by social order similar to {@link #orderByMostRecentlyRecorded()} which is more
	 * accurate. Social Order is calculated compared to others within a single route so
	 * {@link #orderByMostRecentlyRecorded()} is used to calculate this value.
	 * 
	 * @return
	 */
	public RouteCompletionFilterBuilder orderBySocialOrder() {
		return orderBy(SOCIAL_ORDER, Direction.DESCENDING);
	}

	/**
	 * Similar to {@link #orderByFastest()}, this is the calculated rank compared to others and each
	 * person receives only 1 of these..their person best.
	 * 
	 * @return
	 */
	public RouteCompletionFilterBuilder orderBySocialRaceRank() {
		return orderBy(SOCIAL_RACE_RANK, Direction.ASCENDING);
	}

	/**
	 * Provides the most recent completions for each person in order of how many times they have
	 * completed this route.
	 * 
	 * @return
	 */
	public RouteCompletionFilterBuilder orderBySocialCountRank() {
		return orderBy(SOCIAL_COUNT_RANK, Direction.ASCENDING);
	}

	/**
	 * Provides the Highest Personal total first.
	 * 
	 * @return
	 * 
	 */
	public RouteCompletionFilterBuilder orderByPersonalTotal() {
		return orderBy(PERSONAL_TOTAL, Direction.DESCENDING);
	}

	/**
	 * @param eventId
	 * @return
	 */
	public RouteCompletionFilterBuilder routeEventId(RouteEventId eventId) {
		routeId(eventId.getParent());
		return addCondition(FilterCondition.create().field(ROUTE_EVENT_ID).operator(FilterOperator.EQUALS)
				.value(eventId).build());
	}

	/**
	 * any completion with a route event. Essentially, routeEventId != null.
	 * 
	 * @return
	 * 
	 */
	public RouteCompletionFilterBuilder routeEventAssociated() {
		return addPredicateCondition(FilterCondition.create().field(ROUTE_EVENT_ID).operator(FilterOperator.NOT_EQUALS)
				.value(null).build());
	}

	/**
	 * TN-778 provides the best ranks for a person ordered by social race rank.
	 * 
	 * @param person
	 * @return
	 */
	public RouteCompletionFilterBuilder bestRanksForPerson(Person person) {
		return person(person).orderBySocialRaceRank();
	}
}
