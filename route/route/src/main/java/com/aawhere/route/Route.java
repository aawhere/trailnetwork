/**
 *
 */
package com.aawhere.route;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityProvider;
import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.ActivityTypeDistanceCategoryStatistic;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.DeviceSignalActivitySelector;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingWebApiUri;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationKeyStats;
import com.aawhere.app.ApplicationStats;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.field.DictionaryIndex;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.EmbeddedDictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifiable;
import com.aawhere.id.IdentifierToIdentifiableXmlAdapter;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxProvider;
import com.aawhere.measure.calc.CategoricalStatisticField;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsOptionalXmlAdapter;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.Filter;
import com.aawhere.persist.LongBaseEntity;
import com.aawhere.persist.Repository;
import com.aawhere.persist.ServicedBy;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.route.RouteNamePersonalizer.RouteNameXml;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventStatistic;
import com.aawhere.route.event.RouteEvents;
import com.aawhere.search.Searchable;
import com.aawhere.track.TrackSummary;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadWebApiUri;
import com.aawhere.util.rb.Message;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.xml.XmlNamespace;
import com.aawhere.xml.bind.OptionalPassThroughXmlAdapter;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfNotEmpty;
import com.googlecode.objectify.condition.IfNotNull;
import com.googlecode.objectify.condition.IfTrue;
import com.googlecode.objectify.condition.PojoIf;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * An {@link Activity} that has been selected as a quintessential activity for a particualar trail
 * (route).
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Searchable(fieldKeys = { RouteField.NAME, RouteField.ID })
@Entity
@Cache
@Dictionary(domain = Route.FIELD.DOMAIN, messages = RouteMessage.class, indexes = {
		Route.MainsForTrailheadStartIndex.class, Route.RoutesForTrailheadStartActivityTypeIndex.class,
		Route.MainsForTrailheadStartActivityTypeIndex.class, Route.AllButMainIndex.class, Route.AllIndex.class })
@ApiModel(description = RouteMessage.Doc.DESCRIPTION)
public class Route
		extends LongBaseEntity<Route, RouteId>
		implements Cloneable, Identifiable<RouteId>, BoundingBoxProvider {

	static final Integer DEFAULT_SCORE = 0;
	private static final long serialVersionUID = -454823081218723807L;

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(Route route) {
		return new Builder(route);
	}

	public static Builder clone(Route route) {
		try {
			return new Builder((Route) route.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	@ApiModelProperty(value = "Where the Route begins.")
	@Field(key = FIELD.START_ID, indexedFor = "TN-426", xmlAdapter = Route.StartXmlAdapter.class, searchable = true)
	@Index
	@XmlElement(name = FIELD.START_ID)
	private TrailheadId start;

	@Field(key = FIELD.FINISH_ID, indexedFor = "TN-561", xmlAdapter = Route.FinishXmlAdapter.class)
	@Index
	@XmlElement(name = FIELD.FINISH_ID)
	private TrailheadId finish;

	/**
	 * The score is the popularity rating for a route.
	 */
	@XmlAttribute
	@com.aawhere.field.annotation.Field(key = FIELD.SCORE, indexedFor = "TN-587", dataType = FieldDataType.NUMBER)
	@Index
	private Integer score;

	@Field(key = FIELD.COURSE_ID, indexedFor = "TN-428", xmlAdapter = Route.ActivityIdXmlAdapter.class)
	@XmlElement(name = FIELD.COURSE_ID)
	@Index
	@Nullable
	private ActivityId courseId;

	/**
	 * Statistics related to courses that helped make this a route. Multiple courses included would
	 * have {@link ActivityTiming#reciprocal()} relationships to at least one other.
	 */
	@XmlElement(name = FIELD.COURSE_COMPLETION_STATS)
	@XmlJavaTypeAdapter(CourseStatsXmlAdapter.class)
	@Field(key = FIELD.COURSE_COMPLETION_STATS, xmlAdapter = CourseStatsXmlAdapter.class)
	@EmbeddedDictionary
	private ActivityStats courseCompletionStats;

	/**
	 * Counts every completion that is not a duplicate.
	 * 
	 */
	@XmlElement(name = FIELD.COMPLETION_STATS)
	@Field(key = FIELD.COMPLETION_STATS, xmlAdapter = CompletionStatsXmlAdapter.class)
	@EmbeddedDictionary
	@XmlJavaTypeAdapter(CompletionStatsXmlAdapter.class)
	private RouteStats completionStats;

	/**
	 * Counts only one completion per person.
	 * 
	 */
	@XmlElement(name = FIELD.SOCIAL_COMPLETION_STATS)
	@Field(key = FIELD.SOCIAL_COMPLETION_STATS, xmlAdapter = SocialCompletionStatsXmlAdapter.class)
	@EmbeddedDictionary
	@XmlJavaTypeAdapter(SocialCompletionStatsXmlAdapter.class)
	private ActivityStats socialCompletionStats;

	/**
	 * When true, this indicates there are a significant number of {@link RouteEvents} to offer or
	 * {@link RouteCompletions} for a single {@link RouteEvent}. A subjective descriptor meaning
	 * there may be many factors considered to determine if this is true or not, but in general a
	 * person looking for events should be satisfied with the {@link RouteEvents} offered if linking
	 * from this route...specifically that one or more {@link RouteEvents} has a significant number
	 * of {@link RouteCompletions} since a {@link RouteEvent} may contain only two participants
	 * which would indicate a friendship, not an Event from the perspective of the user.
	 * 
	 * If true, this route may also make a good route, but is one that has events on it too. A route
	 * that is dominated by events is scored low to filter those routes to a less prominent spot on
	 * the TrailNetwork. If
	 * 
	 * 
	 * 
	 */
	@XmlAttribute(name = FIELD.EVENT)
	@Index({ IfNotNull.class, IfTrue.class })
	@Field(key = FIELD.EVENT, indexedFor = "TN-671")
	private Boolean isEvent;

	@XmlElement(name = RouteField.BOUNDING_BOX)
	@Field(key = GeoWebApiUri.BOUNDING_BOX, xmlAdapter = Route.BoundingBoxXmlAdapter.class)
	@XmlJavaTypeAdapter(Route.BoundingBoxXmlAdapter.class)
	private BoundingBox boundingBox;

	@XmlElement
	private Length distance;

	/**
	 * If this {@link #isMain()} then the number of alternates is reported here, otherwise null.
	 */
	@XmlAttribute
	@Nullable
	private Integer alternateCount;

	/**
	 * indicates that a completion has been created or updated for this route, but not yet updated
	 * to be compared to the other completions
	 */
	@Field(key = FIELD.STALE, indexedFor = "TN-683")
	@Index(IfTrue.class)
	@XmlAttribute(name = FIELD.STALE)
	private Boolean stale;

	/**
	 * Routes that are similar in space that can be represented by a main route will indicate such
	 * by having that {@link RouteId} in this field. This is different than aliases since this route
	 * is still different enough to keep around and is considered an "Alternate".
	 * 
	 * 
	 * If this field is null or missing then this indicates Route alternates have not yet been
	 * determined by {@link AlternateRouteGroupOrganizer}.
	 * 
	 */
	@Field(key = FIELD.MAIN_ID, indexedFor = "TN-429", searchable = true)
	@Index
	@XmlElement
	@Nullable
	private RouteId mainId;

	/**
	 * Indicates if this route is a main route. If so then {@link #id()} equals {@link #mainId()}.
	 * Default is null so those without a main assigned can be found. This is assigned by
	 * {@link Builder#mainId(RouteId)}.
	 */
	@Field(key = FIELD.MAIN, indexedFor = "TN-429")
	@Index
	@XmlAttribute
	@Nullable
	private Boolean main;

	/** The start area of the course. TN-446 */
	@Field(key = FIELD.START_AREA, indexedFor = "TN-446")
	@XmlElement(name = FIELD.START_AREA)
	@Nullable
	@XmlJavaTypeAdapter(GeoCellsOptionalXmlAdapter.class)
	@Index
	private GeoCells startArea;

	/**
	 * Routes previously created, but since determined to be a duplicate to this route and have now
	 * been removed.
	 * 
	 * TN-429
	 * 
	 * @since 15
	 * 
	 */
	@Field(key = FIELD.ALIASES, parameterType = RouteId.class, indexedFor = "TN-429")
	@Index(IfNotEmpty.class)
	@XmlElement
	private Set<RouteId> aliases;

	/**
	 * Temporary home for Relationships of this route to others.
	 */
	@XmlElement
	private List<RouteRelation> routeRelations;

	/**
	 * The displayable common name of the route. If the name is unknown, it is RouteMessage.UNKNOWN.
	 * This is a message, rather than a string to support a default, localized, name.
	 */
	// Xml is on #nameXml()
	@XmlTransient
	@Field(key = FIELD.NAME)
	private Message name;

	@XmlElement
	@Field(parameterType = SocialGroup.class)
	private Set<SocialGroupId> socialGroupIds;

	/**
	 * Searches for Routes separated into layers:
	 * 
	 * <ul>
	 * <li>All</li>
	 * <li>Activity Type- Only Routes completed during a specific sport</li>
	 * <li>Person - Only Routes completed by the Person specified</li>
	 * <li>Person + Activity Type - Only Routes complete by a Person doing a specific Activity Type</li>
	 * </ul>
	 * Each of the Routes will contain the number of completions related to the Person or Activity
	 * Type or both. These numbers will match the number of results from searching RouteCompletions.
	 * <p>
	 * The appropriate results are returned automatically based on the filters provided. So if a
	 * Person ID for Aaron Roller is provided as a filter then the Routes Aaron completed will be
	 * returned and the counts will represent the number of times he completed that Route (biking,
	 * running, hiking, etc).
	 * </p>
	 * <p>
	 * If a Person ID for Aaron Roller and MTB are provided then only Mountain Bike Rides for Aaron
	 * will be returned and the count will be the total number of mountain bike rides he did on the
	 * route and no information about the number of runs will be provided in such a case. </p
	 * <p>
	 * Exclusion of any of the layers defaults to the TrailNetwork activities targeting the general
	 * public. Adding an Activity Type filter will limit to only those Routes that have been
	 * completed a significant number of times. <br/>
	 * These statics relate to the Completion Stats and Social Completion Stats available for
	 * display, but are not used for filtering.
	 * </p>
	 * 
	 * Other filters will limit the results, but not change the counts:
	 * <ul>
	 * <li>Distance Category - Routes grouped into ranges by their distance relative to the
	 * ActivityType</li>
	 * <li>Start Trailhead - only those routes that begin at the specified trailhead.</li>
	 * <li>Main ID - Similar Routes are grouped and the Main ID is the Route that best represents
	 * the Group</li>
	 * <li>Bounds - Geo bounds limiting to only those routes that start within the bounds</li>
	 * <li>Query - keyword search matching the Route names</li>
	 * <li>Standard filters such as limit and page</li>
	 * </ul>
	 * 
	 * 
	 */
	@ApiModelProperty(
			value = "Number of times a Route is completed for the filters specified",
			notes = "This is available only when searching for routes. The results are related to those results from a search response.",
			required = false)
	@XmlElement
	@XmlJavaTypeAdapter(RelativeCompletionStatsXmlAdapter.class)
	@Field(parameterType = RelativeStatistic.class, xmlAdapter = RelativeCompletionStatsXmlAdapter.class)
	@Ignore
	@Nullable
	private List<RelativeStatistic> relativeCompletionStats;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@com.aawhere.field.annotation.Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM,
			indexedFor = "TN-362", searchable = true)
	@Override
	public RouteId getId() {
		return super.getId();
	}

	/**
	 * @return the score
	 */
	public Integer getScore() {
		return this.score;
	}

	public TrailheadId start() {
		return this.start;
	}

	/**
	 * @return the startArea
	 */
	public GeoCells startArea() {
		return this.startArea;
	}

	@XmlElement(name = FIELD.START)
	@XmlJavaTypeAdapter(Route.StartXmlAdapter.class)
	public TrailheadId getStartForXml() {
		return this.start;
	}

	@Field
	private Trailhead startTrailhead() {
		throw new UnsupportedOperationException("only for field declaration");
	}

	@XmlElement(name = BaseEntity.BASE_FIELD.DESCRIPTION)
	@XmlJavaTypeAdapter(value = RouteDescriptionXmlAdapter.class)
	@Field(key = BaseEntity.BASE_FIELD.DESCRIPTION, xmlAdapter = RouteDescriptionXmlAdapter.class)
	private Route getRouteDescriptionForXml() {
		return this;
	}

	public Boolean isFinishAtStart() {
		return this.start != null && this.start.equals(this.finish);
	}

	public Integer score() {
		return this.score;
	}

	/**
	 * @return the boundingBox
	 */
	@Override
	public BoundingBox boundingBox() {
		return this.boundingBox;
	}

	/**
	 * @return the distance
	 */
	public Length distance() {
		return this.distance;
	}

	public TrailheadId finish() {
		return this.finish;
	}

	@XmlElement(name = FIELD.FINISH)
	@XmlJavaTypeAdapter(Route.FinishXmlAdapter.class)
	public TrailheadId getFinishForXml() {
		return this.finish;
	}

	/**
	 * @deprecated use {@link #completionStats()}
	 * @deprecated use {@link #socialCompletionStats()}
	 * @return
	 */
	@Deprecated
	public Integer contributingActivities() {
		return (this.completionStats == null) ? null : this.completionStats.total();
	}

	/**
	 * @return the alternateCount
	 */
	public Integer alternateCount() {
		return this.alternateCount;
	}

	/**
	 * @deprecated use {@link #completionStats()}
	 * @return the applicationStats
	 */
	@Deprecated
	public SortedSet<ApplicationStats> applicationStats() {
		return (this.completionStats == null) ? null : this.completionStats.applicationStats();
	}

	public Message name() {
		return this.name;
	}

	@SuppressWarnings("unused")
	private RouteNameXml getNameXml() {
		RouteNameXml xml = RouteNameXml.create().startTrailheadId(this.start()).finishTrailheadId(this.finish())
				.completionStats(this.completionStats()).name(this.name()).distance(this.distance()).build();
		return xml;
	}

	@XmlElement(name = FIELD.NAME)
	@XmlJavaTypeAdapter(value = RouteNameXmlAdapter.class)
	private void setNameXml(RouteNameXml route) {
		this.name = route.name();
	}

	/**
	 * @see #aliases
	 * @return the aliases
	 */
	public Set<RouteId> aliases() {
		if (this.aliases == null) {
			this.aliases = ImmutableSet.of();
		}
		return this.aliases;
	}

	/**
	 * @return the courseId
	 */
	@XmlJavaTypeAdapter(Route.ActivityIdXmlAdapter.class)
	public ActivityId getCourse() {
		return this.courseId;
	}

	public ActivityId courseId() {
		return this.courseId;
	}

	/**
	 * @return
	 */
	public ActivityStats courseCompletionStats() {
		return this.courseCompletionStats;
	}

	/**
	 * @return the socialCompletionStats
	 */
	public ActivityStats socialCompletionStats() {
		return this.socialCompletionStats;
	}

	/**
	 * @return the routeRelations
	 */
	public List<RouteRelation> routeRelations() {
		return this.routeRelations;
	}

	@Override
	protected void prePersist() {
		super.prePersist();
		// don't persist the default message.
		this.name = If.equal(this.name, RouteMessage.UNKNOWN).use(null);
	}

	@Override
	public void postLoad() {
		super.postLoad();
		// set the default if null
		this.name = If.nil(this.name).use(RouteMessage.UNKNOWN);
		if (this.courseCompletionStats != null) {
			this.courseCompletionStats.postLoad();
		}
		if (this.socialCompletionStats != null) {
			this.socialCompletionStats.postLoad();
		}
		if (this.completionStats != null) {
			this.completionStats.postLoad();
		}

	}

	/**
	 * @see #mainId
	 * @return
	 */
	@Nullable
	public RouteId mainId() {
		return mainId;
	}

	@Nonnull
	public Boolean isMain() {
		return BooleanUtils.isTrue(this.main);
	}

	@Nonnull
	public Boolean isMainKnown() {
		return this.main != null;
	}

	/**
	 * @return the socialGroups
	 */
	public Set<SocialGroupId> socialGroupIds() {
		return this.socialGroupIds;
	}

	/**
	 * @return the isEvent
	 */
	public Boolean isEvent() {
		return this.isEvent;
	}

	/**
	 * @deprecated use {@link #completionStats()}
	 * @return the activityTypeStats
	 */
	@Deprecated
	public SortedSet<ActivityTypeStatistic> activityTypeStats() {

		return (this.completionStats == null) ? null : this.completionStats.activityTypeStats();
	}

	/**
	 * @return the distanceCategories
	 */
	public List<ActivityTypeDistanceCategory> distanceCategories() {
		if (this.completionStats != null) {
			SortedSet<ActivityTypeDistanceCategoryStatistic> distanceStats = this.completionStats.distanceStats();
			if (distanceStats != null) {
				return ImmutableList.<ActivityTypeDistanceCategory> builder()
						.addAll(ActivityTypeDistanceCategoryStatistic.categories(distanceStats)).build();
			}
		}
		return null;
	}

	/**
	 * Indicates if this route needs to be updated.
	 * 
	 * @return the stale
	 */
	public Boolean isStale() {
		return this.stale;
	}

	public RouteStats completionStats() {
		return this.completionStats;
	}

	public List<RelativeStatistic> relativeCompletionStats() {
		if (this.relativeCompletionStats == null) {
			return null;
		}
		return ImmutableList.<RelativeStatistic> builder().addAll(relativeCompletionStats).build();
	}

	/**
	 * Used to construct all instances of Route.
	 */
	@XmlTransient
	public static class Builder
			extends LongBaseEntity.Builder<Route, Builder, RouteId> {

		/**
		 * Course used during building to set up multiple fields if not already set.
		 */
		private Activity course;
		private boolean isParentShell = false;

		public Builder() {
			super(new Route());

		}

		Builder(Route mutatee) {
			super(mutatee);
			if (mutatee.aliases != null) {
				building.aliases = Sets.newHashSet(mutatee.aliases());
			}
		}

		/**
		 * exclusively for {@link Route#parent(RouteId)}
		 * 
		 * @param id
		 */
		private Builder(RouteId id) {
			this();
			setId(id);
			this.isParentShell = true;
		}

		public Builder score(Integer score) {
			building.score = score;
			return this;
		}

		/**
		 * Adds the given completion count to the list of {@link Route#relativeCompletionStats}.
		 * Nulls are ignored. creates a new list if the field is null.
		 * 
		 * This is intended to be added on after general construction so the list remains mutable
		 * and external access will receive {@link ImmutableList}.
		 * 
		 * @param stat
		 * @return
		 */
		public Builder addRelativeStat(RelativeStatistic stat) {
			if (stat != null) {
				if (building.relativeCompletionStats == null) {
					building.relativeCompletionStats = Lists.newArrayList(stat);
				} else {
					building.relativeCompletionStats.add(stat);
				}
			}
			return this;
		}

		public Builder scoreDefault() {
			return score(DEFAULT_SCORE);
		}

		/**
		 * @param trailheadId
		 * @return
		 */
		public Builder start(Trailhead start) {
			building.start = start.id();
			building.startArea = start.area();
			return this;
		}

		public Builder start(TrailheadId start) {
			building.start = start;
			return this;
		}

		public Builder name(Message name) {
			building.name = name;
			return this;
		}

		public Builder finish(Trailhead finish) {
			building.finish = finish.id();
			return this;
		}

		public Builder finish(TrailheadId finish) {
			building.finish = finish;
			return this;
		}

		public Builder course(Activity activity) {
			this.course = activity;
			return this;
		}

		public Builder alternateCount(Integer numberOfAlternates) {
			building.alternateCount = numberOfAlternates;
			return this;
		}

		Builder addAliases(RouteId... ids) {
			addAliases(Arrays.asList(ids));
			return this;
		}

		public Builder socialGroups(Iterable<SocialGroupId> socialGroups) {
			building.socialGroupIds = SetUtilsExtended.asSet(socialGroups);
			return this;
		}

		public Builder routeRelations(List<RouteRelation> routeRelations) {
			building.routeRelations = routeRelations;
			return this;
		}

		/**
		 * Adds all the aliases given to those existing.
		 * 
		 * @param ids
		 * @return
		 */
		Builder addAliases(Collection<RouteId> ids) {
			if (CollectionUtils.isNotEmpty(ids)) {
				if (building.aliases == null) {
					building.aliases = Sets.newHashSet(ids);
				} else {
					building.aliases().addAll(ids);
				}
				if (hasId()) {
					// we are adding aliases so we better have a route id by now
					building.aliases.add(id());
				}
			}
			return this;
		}

		@Override
		public void validate() {
			if (!this.isParentShell) {
				super.validate();
				Assertion.e().assertMinimumNotNull(1, building.courseId, this.course);
				Assertion.assertNotNull("start", building.start);
				Assertion.assertNotNull("finish", building.finish);
			}

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.BaseEntity.Builder#build()
		 */
		@Override
		public Route build() {

			final Route built = super.build();
			if (built.aliases == null) {
				built.aliases = ImmutableSet.of();
			} else {
				built.aliases = ImmutableSet.copyOf(built.aliases);
			}

			// only manage if mainId has been assigned
			// if not yet assigned then leave main and mainId null.
			if (built.id() != null) {
				if (built.mainId == null) {
					// keep main consistent with main id
					// null means not known
					built.main = null;
				} else {
					built.main = built.id().equals(built.mainId);
				}
			} else {
				built.main = null;
			}

			if (BooleanUtils.isNotTrue(built.main)) {
				// only a main can have alternates
				built.alternateCount = null;
			}

			// assign properties of course if available
			if (this.course != null) {
				built.courseId = course.id();
				built.distance = this.course.getTrackSummary().getDistance();
				built.boundingBox = this.course.boundingBox();
				built.startArea = GeoCellUtil.withAncestors(GeoCellUtil.geoCells(course.getTrackSummary().getStart()
						.getLocation(), Trailhead.MAX_RESOLUTION));
				// TN-927 null name may mean no good name could be determined
			}

			if (built.score == null) {
				built.score = DEFAULT_SCORE;
			}

			return built;
		}

		/**
		 * @param mainId
		 * @return
		 */
		public Builder mainId(RouteId mainId) {
			building.mainId = mainId;
			return this;
		}

		/**
		 * @param courseCompletionStats
		 */
		public Builder courseCompletionStats(ActivityStats coursesSummary) {
			building.courseCompletionStats = coursesSummary;
			return this;
		}

		/**
		 * Only allowed after {@link #courseCompletionStats(ActivityStats)} has been called. It
		 * completes the stats by providing the device types which is calculated at a different
		 * time.
		 * 
		 * @see DeviceSignalActivitySelector
		 * @see RouteUpdateDetails
		 * @param counter
		 * @return
		 */
		public Builder courseCompletionDeviceTypeStats(
				CategoricalStatsCounter<ApplicationKeyStats, ApplicationKey> counter) {
			building.courseCompletionStats.completeDeviceTypes(counter);
			return this;
		}

		public Builder completionStats(RouteStats completionsStats) {
			building.completionStats = completionsStats;
			return this;
		}

		public Builder socialCompletionStats(ActivityStats socialCompletionStats) {
			building.socialCompletionStats = socialCompletionStats;
			return this;
		}

		/**
		 * Indicates if this is stale or not.
		 * 
		 * @param stale
		 * @return
		 */
		public Builder stale(Boolean stale) {
			building.stale = stale;
			return this;
		}

		/**
		 * Over-writes the aliases regardless of what is in there.
		 * 
		 * @see #addAliases(Collection)
		 * 
		 * @param aliases
		 * @return
		 */
		Builder setAliases(Set<RouteId> aliases) {
			building.aliases = aliases;
			building.aliases.add(id());
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct Route */
	private Route() {
	}

	public static class FIELD {
		public static final String SCORE = SystemWebApiUri.SCORE;
		public static final String DOMAIN = "route";
		public static final String START = "startTrailhead";
		public static final String START_AREA = "startArea";
		public static final String START_ID = "startTrailheadId";
		public static final String FINISH = "finishTrailhead";
		public static final String FINISH_ID = "finishTrailheadId";
		public static final String COURSE_ID = "courseId";
		public static final String MAIN = "main";
		public static final String EVENT = "event";
		public static final String MAIN_ID = "mainId";
		public static final String ALIASES = "aliases";
		public static final String NAME = "name";
		public static final String DISTANCE_CATEGORY = "distanceCategory";
		public static final String STALE = "stale";
		public static final String COMPLETION_STATS = "completionStats";
		public static final String COURSE_COMPLETION_STATS = "courseCompletionStats";
		public static final String SOCIAL_COMPLETION_STATS = "socialCompletionStats";
		public static final String COURSE = ActivityTiming.FIELD.COURSE;
		public static final String ROUTE_ID = RouteWebApiUri.ROUTE_ID;
		public static final String ROUTE_EVENT_PARTICIPATION_STATS = RouteEventStatistic.FIELD.DOMAIN;

		public static class KEY {

			public static final FieldKey ROUTE_ID = new FieldKey(DOMAIN, BaseEntity.BASE_FIELD.ID);
			public static final FieldKey SCORE = new FieldKey(DOMAIN, FIELD.SCORE);
			public static final FieldKey START_ID = new FieldKey(DOMAIN, FIELD.START_ID);
			public static final FieldKey START_AREA = new FieldKey(DOMAIN, FIELD.START_AREA);
			public static final FieldKey START_AREA_CELLS = new FieldKey(KEY.START_AREA, GeoCells.FIELD.CELLS);
			public static final FieldKey FINISH_ID = new FieldKey(DOMAIN, FIELD.FINISH_ID);
			public static final FieldKey COURSE_ID = new FieldKey(DOMAIN, FIELD.COURSE_ID);
			public static final FieldKey ALIASES = new FieldKey(DOMAIN, FIELD.ALIASES);
			public static final FieldKey MAIN_ID = new FieldKey(DOMAIN, FIELD.MAIN_ID);
			public static final FieldKey MAIN = new FieldKey(DOMAIN, FIELD.MAIN);
			public static final FieldKey STALE = new FieldKey(DOMAIN, FIELD.STALE);

			public static final FieldKey COMPLETION_STATS = new FieldKey(DOMAIN, FIELD.COMPLETION_STATS);
			public static final FieldKey COMPLETION_STATS_TOTAL = new FieldKey(COMPLETION_STATS,
					CategoricalStatisticField.TOTAL);

			public static final FieldKey COMPLETION_STATS_PERCENT = new FieldKey(COMPLETION_STATS,
					CategoricalStatisticField.PERCENT);

			public static final FieldKey ACTIVITY_TYPE_STATS = new FieldKey(COMPLETION_STATS,
					ActivityStats.FIELD.ACTIVITY_TYPE_STATS);
			public static final FieldKey APPLICATION_STATS = new FieldKey(COMPLETION_STATS,
					ActivityStats.FIELD.APPLICATION_STATS);
			public static final FieldKey ACTIVITY_TYPE = new FieldKey(ACTIVITY_TYPE_STATS,
					ActivityStats.FIELD.KEY.ACTIVITY_TYPES);
			/**
			 * This is referencing {@link Application}, not key, because of
			 * ApplicationTranslatorFactory
			 */
			public static final FieldKey APPLICATION_KEY = new FieldKey(APPLICATION_STATS,
					ActivityStats.FIELD.KEY.APPLICATION);

			public static final FieldKey COURSE_COMPLETION_STATS = new FieldKey(DOMAIN, FIELD.COURSE_COMPLETION_STATS);

			public static final FieldKey COURSES_APPLICATION_STATS = new FieldKey(COURSE_COMPLETION_STATS,
					ActivityStats.FIELD.APPLICATION_STATS);

			public static final FieldKey DISTANCE_CATEGORY = new FieldKey(DOMAIN, FIELD.DISTANCE_CATEGORY);

		}

		/**
		 * Options corresponding to {@link Filter#getOptions()}.
		 * 
		 * @author aroller
		 * 
		 */
		public static class OPTION {

			/**
			 * Provided in a filter the {@link Repository} must include a full sized Activity in the
			 * {@link Route#course}.
			 * 
			 * @deprecated use
			 * 
			 */
			@Deprecated
			public static final String START_INLINE = new FieldKey(new FieldKey(FIELD.DOMAIN, "start"),
					SystemWebApiUri.INLINE).getAbsoluteKey();
			@Deprecated
			public static final String FINISH_INLINE = new FieldKey(new FieldKey(FIELD.DOMAIN, "finish"),
					SystemWebApiUri.INLINE).getAbsoluteKey();
			/**
			 * Provides the courses in the support entities of the Routes object.
			 */
			@Deprecated
			public static final String COURSES = new FieldKey(DOMAIN, "courses").getAbsoluteKey();
			public static final String COURSE_INLINE = new FieldKey(new FieldKey(DOMAIN, FIELD.COURSE),
					SystemWebApiUri.INLINE).getAbsoluteKey();
			/**
			 * Includes the trailheads affecting the route search in the {@link Routes} support
			 * entities list. This is the most efficient compared to inline since it uses batch
			 * processing and does not repeat duplicate trailheads.
			 */
			@Deprecated
			public static final String TRAILHEADS = new FieldKey(DOMAIN, TrailheadWebApiUri.TRAILHEADS)
					.getAbsoluteKey();

			/**
			 * Indicator to include gpolyline track points.
			 * */
			public static final String TRACK_GPOLYLINE = new FieldKey(new FieldKey(DOMAIN, TrackSummary.FIELD.DOMAIN),
					GeoWebApiUri.GPOLYLINE).getAbsoluteKey();

			/** Indicates the route update should be forced. */
			public static final String FORCE = SystemWebApiUri.FORCE;
			public static final String TIMINGS = ActivityTimingWebApiUri.TIMINGS;
			public static final String STALE = RouteWebApiUri.STALE;
			public static final String DELETE_PASSKEY = "passkey";
		}
	}

	/**
	 * Provides the abilty to filter by mains and start together. Indexed for TN-527
	 * 
	 * @author aroller
	 * 
	 */
	public static class MainsForTrailheadStartIndex
			extends DictionaryIndex {
		private static final long serialVersionUID = -859638968060521995L;

		public MainsForTrailheadStartIndex() {
			super(FIELD.KEY.MAIN, FIELD.KEY.START_ID);
		}
	}

	/** TN-576 */
	public static class RoutesForTrailheadStartActivityTypeIndex
			extends DictionaryIndex {
		private static final long serialVersionUID = -859638968060521995L;

		public RoutesForTrailheadStartActivityTypeIndex() {
			super(FIELD.KEY.START_ID, FIELD.KEY.ACTIVITY_TYPE);
		}
	}

	/** TN-527 TN-576 */
	public static class MainsForTrailheadStartActivityTypeIndex
			extends DictionaryIndex {
		private static final long serialVersionUID = -859638968060521995L;

		public MainsForTrailheadStartActivityTypeIndex() {
			super(FIELD.KEY.MAIN, FIELD.KEY.START_ID, FIELD.KEY.ACTIVITY_TYPE);
		}
	}

	public static class AllIndex
			extends DictionaryIndex {
		private static final long serialVersionUID = -859638968060521995L;

		public AllIndex() {
			super(FIELD.KEY.MAIN, FIELD.KEY.MAIN_ID, FIELD.KEY.START_ID, FIELD.KEY.SCORE, FIELD.KEY.START_AREA_CELLS);
		}
	}

	public static class AllButMainIndex
			extends DictionaryIndex {
		private static final long serialVersionUID = -9175626111847309319L;

		public AllButMainIndex() {
			super(Sets.difference(new AllIndex().keys(), Sets.newHashSet(FIELD.KEY.MAIN)));
		}
	}

	static class IfScoreIsSignficant
			extends PojoIf<Route> {

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.condition.If#matchesPojo(java.lang.Object)
		 */
		@Override
		public boolean matchesPojo(Route route) {
			return RouteUtil.scoreIsSignificant(route.score);
		}

	}

	@Singleton
	public static class ActivityIdXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<Activity, ActivityId> {
		public ActivityIdXmlAdapter() {
		}

		public ActivityIdXmlAdapter(Function<ActivityId, Activity> provider) {
			super(provider);
		}

		@Inject
		public ActivityIdXmlAdapter(ActivityProvider provider) {
			super(provider.idFunction());
		}
	}

	@Singleton
	@ServicedBy(RouteTrailheadProvider.class)
	public static class StartXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<Trailhead, TrailheadId> {
		public StartXmlAdapter() {
		}

		public StartXmlAdapter(Function<TrailheadId, Trailhead> provider) {
			super(provider);
		}

		@Inject
		public StartXmlAdapter(RouteTrailheadProvider provider) {
			super(provider.idFunction());
		}
	}

	@Singleton
	@ServicedBy(RouteTrailheadProvider.class)
	public static class FinishXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<Trailhead, TrailheadId> {
		public FinishXmlAdapter() {
		}

		public FinishXmlAdapter(Function<TrailheadId, Trailhead> provider) {
			super(provider);
		}

		@Inject
		public FinishXmlAdapter(RouteTrailheadProvider provider) {
			super(provider.idFunction());
		}
	}

	@Singleton
	public static class CourseStatsXmlAdapter
			extends OptionalPassThroughXmlAdapter<ActivityStats> {
		public CourseStatsXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public CourseStatsXmlAdapter(Show show) {
			super(show);
		}

	}

	@Singleton
	public static class CompletionStatsXmlAdapter
			extends OptionalPassThroughXmlAdapter<RouteStats> {
		public CompletionStatsXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public CompletionStatsXmlAdapter(Show show) {
			super(show);
		}
	}

	@Singleton
	public static class RelativeCompletionStatsXmlAdapter
			extends OptionalPassThroughXmlAdapter<RelativeStatistic> {
		public RelativeCompletionStatsXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public RelativeCompletionStatsXmlAdapter(Show show) {
			super(show);
		}
	}

	@Singleton
	public static class SocialCompletionStatsXmlAdapter
			extends OptionalPassThroughXmlAdapter<ActivityStats> {
		public SocialCompletionStatsXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public SocialCompletionStatsXmlAdapter(Show show) {
			super(show);
		}
	}

	@Singleton
	public static class ActivityTypesXmlAdapter
			extends OptionalPassThroughXmlAdapter<ActivityType> {
		public ActivityTypesXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		public ActivityTypesXmlAdapter(Show show) {
			super(show);
		}
	}

	@Singleton
	public static class BoundingBoxXmlAdapter
			extends com.aawhere.measure.BoundingBoxXmlAdapter {
		public BoundingBoxXmlAdapter() {
			super(Visibility.SHOW);
		}

		@Inject
		public BoundingBoxXmlAdapter(Show show) {
			super(show);
		}
	}

	/**
	 * @return
	 */
	public Boolean isScoreInsignificant() {
		return !isScoreSignificant();
	}

	public Boolean isScoreSignificant() {
		return RouteUtil.scoreIsSignificant(score);
	}

	/**
	 * For those composed children this will provide an empty shell of a Route with the given id.
	 * 
	 * @param routeId
	 * @return
	 */
	public static Route parent(RouteId routeId) {
		return new Builder(routeId).build();
	}

}
