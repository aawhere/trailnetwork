/**
 * 
 */
package com.aawhere.route;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.activity.ActivityNameSelector;
import com.aawhere.activity.ActivityStats;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.trailhead.RouteAwareTrailheadScoreCalculator;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.xml.XmlNamespace;

/**
 * Used to organize and communicate the decisions made to update a single trailhead.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteTrailheadOrganizer {

	@XmlElement
	private Trailhead original;
	@XmlElement
	private Trailhead updated;
	@XmlElement
	private RouteAwareTrailheadScoreCalculator scoreCalculator;
	@XmlElement
	private ActivityNameSelector nameSelector;
	@XmlElement
	private ActivityStats cumulativeCompletionStats;
	@XmlElement
	private ActivityStats filteredCompletionStats;

	/**
	 * Indicates the trailhead being organized should be found on the public browsing map.
	 */
	@XmlAttribute
	private Boolean publicTrailhead;

	/** @see #publicTrailhead */
	public Boolean publicTrailhead() {
		return this.publicTrailhead;
	}

	/**
	 * @return the original
	 */
	public Trailhead original() {
		return this.original;
	}

	/**
	 * @return the updated
	 */
	public Trailhead updated() {
		return this.updated;
	}

	/**
	 * @return the scoreCalculator
	 */
	public RouteAwareTrailheadScoreCalculator scoreCalculator() {
		return this.scoreCalculator;
	}

	/**
	 * @return the nameSelector
	 */
	public ActivityNameSelector nameSelector() {
		return this.nameSelector;
	}

	/**
	 * @return the cumulativeCompletionStats
	 */
	public ActivityStats cumulativeCompletionStats() {
		return this.cumulativeCompletionStats;
	}

	/**
	 * @return the filteredCompletionStats
	 */
	public ActivityStats filteredCompletionStats() {
		return this.filteredCompletionStats;
	}

	/**
	 * Used to construct all instances of RouteTrailheadOrganizer.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteTrailheadOrganizer> {

		public Builder() {
			super(new RouteTrailheadOrganizer());
		}

		/**
		 * @param updated
		 *            the updated to set
		 */
		public Builder updated(Trailhead updated) {
			building.updated = updated;
			return this;
		}

		/**
		 * @param scoreCalculator
		 *            the scoreCalculator to set
		 */
		public Builder scoreCalculator(RouteAwareTrailheadScoreCalculator scoreCalculator) {
			building.scoreCalculator = scoreCalculator;
			return this;
		}

		/**
		 * @param nameSelector
		 *            the nameSelector to set
		 */
		public Builder nameSelector(ActivityNameSelector nameSelector) {
			building.nameSelector = nameSelector;
			return this;
		}

		/**
		 * @param cumulativeCompletionStats
		 *            the cumulativeCompletionStats to set
		 */
		public Builder cumulativeCompletionStats(ActivityStats cumulativeCompletionStats) {
			building.cumulativeCompletionStats = cumulativeCompletionStats;
			return this;
		}

		/**
		 * @param filteredCompletionStats
		 *            the filteredCompletionStats to set
		 */
		public Builder filteredCompletionStats(ActivityStats filteredCompletionStats) {
			building.filteredCompletionStats = filteredCompletionStats;
			return this;
		}

		/**
		 * @param original
		 *            the original to set
		 */
		public Builder original(Trailhead original) {
			building.original = original;
			return this;
		}

		/** @see #publicTrailhead */
		public Builder publicTrailhead(Boolean publicTrailhead) {
			building.publicTrailhead = publicTrailhead;
			return this;
		}
	}// end Builder

	/** Use {@link Builder} to construct RouteTrailheadOrganizer */
	private RouteTrailheadOrganizer() {
	}

	public static Builder create() {
		return new Builder();
	}

}
