/**
 *
 */
package com.aawhere.route;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.id.Identifier;
import com.aawhere.id.LongIdentifier;
import com.aawhere.persist.ServicedBy;
import com.aawhere.xml.XmlNamespace;

import com.wordnik.swagger.annotations.ApiModel;

/**
 * The {@link Identifier} for {@link Route}
 * 
 * @see Route#getId()
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@ServicedBy(RouteProvider.class)
@ApiModel(description = RouteMessage.Doc.Id.DESCRIPTION)
public class RouteId
		extends LongIdentifier<Route> {
	private static final long serialVersionUID = -6391442879863162922L;

	/**
	 * @param kind
	 */
	RouteId() {
		super(Route.class);
	}

	public RouteId(Long id) {
		super(id, Route.class);
	}

	/**
	 * Provided to any client that may have a Long formatted as a String.
	 * 
	 * @param value
	 * @param kind
	 */
	public RouteId(String value) {
		super(value, Route.class);
	}

}
