/**
 * 
 */
package com.aawhere.route.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Supplier;

/**
 * {@link CategoricalStatistic} for {@link RouteEventId} to count {@link RouteEvents}.
 * 
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = RouteEventStatistic.FIELD.DOMAIN)
public class RouteEventStatistic
		extends CategoricalStatistic<RouteEventId> {

	@XmlElement(name = RouteEvent.FIELD.ROUTE_EVENT_ID)
	@Field(key = FIELD.ROUTE_EVENT_ID, dataType = FieldDataType.ATOM)
	private RouteEventId routeEventId;

	/**
	 * Used to construct all instances of RouteEventStatistic.
	 */
	@XmlTransient
	public static class Builder
			extends CategoricalStatistic.Builder<RouteEventStatistic, RouteEventId, Builder> {

		public Builder() {
			super(new RouteEventStatistic());
		}

		@Override
		public RouteEventStatistic build() {
			RouteEventStatistic built = super.build();
			return built;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.calc.CategoricalStatistic.Builder#category(java.lang.Object)
		 */
		@Override
		public Builder category(RouteEventId category) {
			building.routeEventId = category;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteEventStatistic */
	private RouteEventStatistic() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.calc.CategoricalStatistic#category()
	 */
	@Override
	public RouteEventId category() {
		return routeEventId;
	}

	/**
	 * @return the routeEventId
	 */
	@XmlJavaTypeAdapter(RouteEventXmlAdapter.ForStatistic.class)
	@XmlElement(name = RouteEvent.FIELD.DOMAIN)
	public RouteEventId getRouteEvent() {
		return this.routeEventId;
	}

	public static Supplier<Builder> builderSupplier() {
		return new Supplier<Builder>() {

			@Override
			public Builder get() {
				return create();
			}
		};
	}

	/**
	 * Provides a counter to be used and configured by the client. This just makes it cleaner and
	 * assigns the given categories as well as the {@link #builderSupplier()}.
	 * 
	 * @param categories
	 * @return
	 */
	public static CategoricalStatsCounter.Builder<RouteEventStatistic, RouteEventId, Builder> counter(
			Iterable<RouteEventId> categories) {
		CategoricalStatsCounter.Builder<RouteEventStatistic, RouteEventId, Builder> builder = CategoricalStatsCounter
				.create();
		builder.categories(categories);
		builder.statsBuilder(builderSupplier());
		return builder;
	}

	public static class FIELD {
		public static final String DOMAIN = "routeEventStatistic";
		public static final String ROUTE_EVENT_ID = RouteEvent.FIELD.ROUTE_EVENT_ID;

		public static class KEY {
			public static final FieldKey ROUTE_EVENT_ID = new FieldKey(FIELD.DOMAIN, FIELD.ROUTE_EVENT_ID);
		}

		public static class OPTION {
			public static final String ROUTE_EVENT = new FieldKey(DOMAIN, RouteEvent.FIELD.DOMAIN).getAbsoluteKey();
		}
	}

	private static final long serialVersionUID = -57177582530901941L;
}
