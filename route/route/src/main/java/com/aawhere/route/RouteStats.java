/**
 *
 */
package com.aawhere.route;

import java.util.SortedSet;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.activity.ActivityStats;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.measure.xml.CountFromIntegerXmlAdapter;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventStatistic;
import com.aawhere.xml.XmlNamespace;

import com.googlecode.objectify.annotation.Ignore;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE, name = RouteStats.FIELD.DOMAIN)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = RouteStats.FIELD.DOMAIN)
public class RouteStats
		extends ActivityStats {

	/**
	 * Describes the number of RouteCompletions participating in a {@link RouteEvent}. The total
	 * inherits from the {@link #total()}.
	 * 
	 */
	@Nullable
	@XmlElement(name = FIELD.ROUTE_EVENT_STATS)
	@Field(key = FIELD.ROUTE_EVENT_STATS, parameterType = RouteEventStatistic.class)
	/* Ignoring because events has no limit and is too heavy TN-726 */
	@Ignore
	private SortedSet<RouteEventStatistic> routeEventStats;
	@Nullable
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	private Integer routeEventsCompletionsCount;
	@Nullable
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	private Integer routeEventsCount;

	/**
	 * Used to construct all instances of RouteStats.
	 */
	@XmlTransient
	public static class Builder
			extends ActivityStats.AbstractBuilder<RouteStats, Builder> {

		public Builder() {
			super(new RouteStats());
		}

		public Builder routeEventStats(SortedSet<RouteEventStatistic> routeEventStats) {
			building.routeEventStats = routeEventStats;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.activity.ActivityStats.AbstractBuilder#copy(com.aawhere.activity.ActivityStats
		 * )
		 */
		@Override
		public Builder copy(ActivityStats from) {
			if (from instanceof RouteStats) {
				RouteStats fromRouteStats = (RouteStats) from;
				building.routeEventStats = fromRouteStats.routeEventStats;
			}
			return super.copy(from);

		}

		@Override
		public RouteStats build() {
			RouteStats built = super.build();
			if (built.routeEventStats != null) {
				built.routeEventsCompletionsCount = (built.routeEventStats != null) ? CategoricalStatsCounter
						.sum(built.routeEventStats) : null;
				built.routeEventsCount = built.routeEventStats.size();
			}
			return built;
		}

	}// end Builder

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityStats#postLoad()
	 */
	@Override
	public void postLoad() {
		super.postLoad();
		CategoricalStatistic.postLoad(this.routeEventStats, total());
	}

	/**
	 * @return the routeEventsCompletionsCount
	 */
	public Integer routeEventsCompletionsCount() {
		return this.routeEventsCompletionsCount;
	}

	/**
	 * @return the routeEventsCount
	 */
	public Integer routeEventsCount() {
		return this.routeEventsCount;
	}

	/**
	 * The ratio of {@link #getRouteEventCumulativeStatsCount()} over {@link #total()}.
	 * 
	 * @return
	 */
	@XmlElement
	public Ratio getRouteEventCompletionsRatio() {
		return routeEventCompletionsRatio();
	}

	/**
	 * @return
	 */
	public Ratio routeEventCompletionsRatio() {
		return (this.routeEventsCompletionsCount != null) ? MeasurementUtil.ratio(	this.routeEventsCompletionsCount,
																					total()) : null;
	}

	/**
	 * @return the routeEventStats
	 */
	public SortedSet<RouteEventStatistic> routeEventStats() {
		return this.routeEventStats;
	}

	/** Use {@link Builder} to construct RouteStats */
	private RouteStats() {
	}

	public static ActivityStats.Builder create() {
		throw new UnsupportedOperationException("use routeStatsCreator or call Activity stats directly");
	}

	public static Builder routeStatsCreator() {
		return new Builder();
	}

	@XmlTransient
	public static final class FIELD {
		public static final String DOMAIN = "routeStats";
		public static final String ROUTE_EVENT_STATS = "routeEventStats";

		public static final class KEY {
			public static final FieldKey ROUTE_EVENT_STATS = new FieldKey(FIELD.DOMAIN, FIELD.ROUTE_EVENT_STATS);
		}
	}

	private static final long serialVersionUID = 6005252840548284030L;

}
