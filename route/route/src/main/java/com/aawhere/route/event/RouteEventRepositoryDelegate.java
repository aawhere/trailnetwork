/**
 * 
 */
package com.aawhere.route.event;

import java.util.Map;

import org.joda.time.Interval;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.RepositoryDelegate;
import com.aawhere.route.event.RouteEvent.Builder;
import com.aawhere.util.rb.Message;

/**
 * @author aroller
 * 
 */
public class RouteEventRepositoryDelegate
		extends RepositoryDelegate<RouteEventRepository, RouteEvents, RouteEvent, RouteEventId>
		implements RouteEventRepository {

	/**
	 * @param repository
	 */
	public RouteEventRepositoryDelegate(RouteEventRepository repository) {
		super(repository);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.route.event.RouteEventRepository#update(com.aawhere.route.event.RouteEventId,
	 * org.joda.time.Interval)
	 */
	@Override
	public RouteEvent interval(RouteEventId id, Interval interval) throws EntityNotFoundException {
		return update(mutate(id).interval(interval));
	}

	/**
	 * @param routeEvent
	 * @return
	 * @throws EntityNotFoundException
	 */
	private Builder mutate(RouteEventId id) throws EntityNotFoundException {
		return RouteEvent.mutate(load(id));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.event.RouteEventRepository#names(java.util.Map)
	 */
	@Override
	public Map<RouteEventId, RouteEvent> names(Map<RouteEventId, Message> names) {

		Map<RouteEventId, RouteEvent> all = loadAll(names.keySet());
		for (RouteEvent event : all.values()) {
			RouteEvent.mutate(event).name(names.get(event.id())).build();
		}
		return super.storeAll(all.values());
	}

}
