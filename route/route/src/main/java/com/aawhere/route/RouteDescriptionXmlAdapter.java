/**
 *
 */
package com.aawhere.route;

import com.aawhere.identity.IdentityManager;
import com.aawhere.lang.NotImplementedException;
import com.aawhere.person.PersonProvider;
import com.aawhere.personalize.DisabledPersonalizer;
import com.aawhere.personalize.xml.PersonalizedXmlAdapter;
import com.aawhere.route.RouteDescriptionPersonalizer.RouteDescription;

import com.google.inject.Inject;

/**
 * Provides the localized description of a route through an XmlAdapter.
 * 
 * @author Brian Chapman
 * 
 */
public class RouteDescriptionXmlAdapter
		extends PersonalizedXmlAdapter<RouteDescription, Route> {

	public RouteDescriptionXmlAdapter() {
		super(new DisabledPersonalizer.Builder(), Visibility.HIDE);
	}

	public RouteDescriptionXmlAdapter(RouteDescriptionPersonalizer plizer, Visibility visibility) {
		super(plizer, visibility);
	}

	@Inject
	public RouteDescriptionXmlAdapter(IdentityManager identityManager, RouteTrailheadProvider trailheadProvider,
			PersonProvider personProvider, Show show) {
		super(new RouteDescriptionPersonalizer.Builder().trailheadProvider(trailheadProvider)
				.personProvider(personProvider).setPreferences(identityManager.preferences()).build(), show);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.xml.PersonalizedXmlAdapter#unmarshal(com.aawhere.personalize.
	 * Personalized)
	 */
	@Override
	public Route showUnmarshal(RouteDescription display) throws Exception {
		throw new NotImplementedException("Cannot unmarshal a " + RouteDescription.class.getSimpleName() + " into a "
				+ Route.class.getSimpleName() + ".");
	}

}