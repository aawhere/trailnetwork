/**
 * 
 */
package com.aawhere.route;

import javax.annotation.Nullable;

import org.apache.commons.lang3.BooleanUtils;

import com.google.common.base.Predicate;

/**
 * Given a Route this will simple return result of {@link Route#isMain()}.
 * 
 * @author aroller
 * 
 */
public class RouteIsMainPredicate
		implements Predicate<Route> {

	public static RouteIsMainPredicate build() {
		return new RouteIsMainPredicate();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(@Nullable Route input) {
		if (input == null) {
			return false;
		}
		return BooleanUtils.isTrue(input.isMain());
	}

}
