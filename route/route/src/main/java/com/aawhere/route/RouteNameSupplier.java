/**
 * 
 */
package com.aawhere.route;

import org.apache.commons.collections.CollectionUtils;

import com.aawhere.activity.ActivityMessage;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.util.rb.Message;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

/**
 * When a name can't be crowd-sourced with success this produces a valuable description based on the
 * attributes relating to the route. Since this is enhanced by language it is localized version.
 * 
 * @author aroller
 * 
 */
public class RouteNameSupplier
		implements Supplier<Message> {

	private Route route;
	private Trailhead finish;
	private Trailhead start;
	private Message name;

	/**
	 * Used to construct all instances of RouteNameSupplier.
	 */
	public static class Builder
			extends ObjectBuilder<RouteNameSupplier> {

		public Builder() {
			super(new RouteNameSupplier());
		}

		public Builder start(Trailhead start) {
			building.start = start;
			return this;
		}

		public Builder finish(Trailhead finish) {
			building.finish = finish;
			return this;
		}

		public Builder route(Route route) {
			building.route = route;
			return this;
		}

		@Override
		public RouteNameSupplier build() {

			RouteNameSupplier built = super.build();
			if (CollectionUtils.isNotEmpty(built.route.courseCompletionStats().activityTypeStats())) {
				if (CollectionUtils.isNotEmpty(built.route.distanceCategories())) {
					ActivityTypeStatistic activityTypeStats = built.route.courseCompletionStats().activityTypeStats()
							.iterator().next();
					ActivityTypeDistanceCategory distanceCategory = ActivityTypeDistanceCategory
							.valueOf(activityTypeStats.category(), built.route.distance());

				}
			}
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteNameSupplier */
	private RouteNameSupplier() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public Message get() {
		return name;
	}

	public static Supplier<? extends Message> unknown() {
		return Suppliers.ofInstance(ActivityMessage.UNKNOWN);
	}

	public static Supplier<? extends Message> nul() {
		return Suppliers.ofInstance(null);
	}
}
