/**
 * 
 */
package com.aawhere.route;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.activity.ActivityType;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupRelator;
import com.aawhere.trailhead.TrailheadUtil;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * Using social, spatial, trailheads, alternates and other criteria this finds the best Routes
 * related to the one route of interest.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteRelator {

	@XmlElement
	private ActivityType activityType;

	@XmlElement
	private List<RouteRelation> routeRelations;

	@XmlElement
	private SocialGroupRelator socialGroupRelator;
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private List<Route> diverse;
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private List<Route> sameTrailhead;
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private List<Route> alternates;

	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private List<Route> nearby;

	@XmlElement
	private List<SocialGroup> socialGroupsRelated;
	@XmlElement
	private Route routeTargeted;
	@XmlElement
	private ActivityType activityTypeTargeted;

	/**
	 * Used to construct all instances of RouteRelator.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteRelator> {

		private Function<Iterable<RouteId>, Map<RouteId, Route>> idsFunction;

		private Set<RouteId> alternatesOfTarget;

		public Builder alternatesOfTarget(Iterable<RouteId> alternates) {
			this.alternatesOfTarget = SetUtilsExtended.asSet(alternates);
			return this;
		}

		public Builder idsFunction(Function<Iterable<RouteId>, Map<RouteId, Route>> function) {
			this.idsFunction = function;
			return this;
		}

		/**
		 * Required, unless {@link #socialGroupRelator(SocialGroupRelator)} is provided then this
		 * will come from it.
		 * 
		 * @param socialGroups
		 * @return
		 */
		public Builder socialGroups(List<SocialGroup> socialGroups) {
			building.socialGroupsRelated = socialGroups;
			return this;
		}

		public Builder socialGroupRelator(SocialGroupRelator socialGroupRelator) {
			building.socialGroupsRelated = socialGroupRelator.groupsMatchingTargetCategory();
			building.socialGroupRelator = socialGroupRelator;
			SocialGroup socialGroupTargeted = socialGroupRelator.socialGroupTargeted();
			ActivityType activityType = RouteCompletionSocialGroupOrganizer.activityType(socialGroupTargeted);
			building.activityType = activityType;
			return this;
		}

		public Builder targetRoute(Route targetRoute) {
			building.routeTargeted = targetRoute;
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("alternates", this.alternatesOfTarget);
			Assertion.exceptions().notNull("idsFunction", this.idsFunction);
			Assertion.exceptions().notNull("targetRoute", building.routeTargeted);
			Assertion.exceptions().notNull("socialGroups", building.socialGroupsRelated);

		}

		@Override
		public RouteRelator build() {
			RouteRelator built = super.build();
			built.sameTrailhead = Lists.newArrayList();
			built.alternates = Lists.newArrayList();
			built.diverse = Lists.newArrayList();
			built.routeRelations = Lists.newArrayList();
			built.nearby = Lists.newArrayList();

			Iterable<RouteId> routeIds = RouteUtil.socialGroupRouteIds(built.socialGroupsRelated);
			Iterable<Route> routeCandidates = idsFunction.apply(routeIds).values();
			for (Route routeCandidate : routeCandidates) {
				if (!routeCandidate.equals(built.routeTargeted)) {
					boolean spatiallySimilar = false;
					if (alternatesOfTarget.contains(routeCandidate.id())) {
						built.alternates.add(routeCandidate);
						spatiallySimilar = true;
					}
					if (routeCandidate.start().equals(built.routeTargeted.start())) {
						built.sameTrailhead.add(routeCandidate);
						spatiallySimilar = true;
					}

					// if the route candidate starts in the bounding box of the target it's
					// considered nearby
					BoundingBox boundingBox = built.routeTargeted.boundingBox();
					GeoCoordinate startCoordinate = TrailheadUtil.coordinateFrom(routeCandidate.start());
					// the complex calculation checks if the length to the closest point of the
					// bounding box is less the bounding box diagonal with a max of 10km
					if (BoundingBoxUtil.contains(boundingBox, startCoordinate)
							|| (QuantityMath.create(boundingBox.getDiagonal())
									.min(MeasurementUtil.createLengthInMeters(10000))
									.greaterThan(GeoMath.length(startCoordinate, BoundingBoxUtil
											.closestPoint(boundingBox, startCoordinate))))) {
						built.nearby.add(routeCandidate);
						spatiallySimilar = true;
					}
					if (!spatiallySimilar) {
						built.diverse.add(routeCandidate);
						// FIXME: make the size limit an option
						if (built.activityType != null && built.routeRelations.size() < 10) {
							RouteRelation relation = RouteRelation.create().activityType(built.activityType)
									.relatedRouteId(routeCandidate.id()).build();
							built.routeRelations.add(relation);
						}
					}
				}

			}

			return built;
		}

		public Builder() {
			super(new RouteRelator());
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteRelator */
	private RouteRelator() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return
	 */
	public List<Route> alternates() {
		return this.alternates;
	}

	/**
	 * @return the diverse
	 */
	public List<Route> diverse() {
		return this.diverse;
	}

	/**
	 * @return the sameTrailhead
	 */
	public List<Route> sameTrailhead() {
		return this.sameTrailhead;
	}

	/**
	 * @return the activityType
	 */
	public ActivityType activityType() {
		return this.activityType;
	}

	/**
	 * @return
	 */
	public List<RouteRelation> routeRelations() {
		return this.routeRelations;
	}

	/**
	 * @return
	 */
	public List<Route> nearby() {
		return this.nearby;
	}
}
