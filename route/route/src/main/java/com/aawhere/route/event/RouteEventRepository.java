/**
 * 
 */
package com.aawhere.route.event;

import java.util.Map;

import org.joda.time.Interval;

import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.util.rb.Message;

/**
 * Persistent access to {@link RouteEvents}.
 * 
 * @author aroller
 * 
 */
public interface RouteEventRepository
		extends CompleteRepository<RouteEvents, RouteEvent, RouteEventId> {

	RouteEvent interval(RouteEventId id, Interval interval) throws EntityNotFoundException;

	/** Updates each event with the given name and returns the map of updated Events. */
	Map<RouteEventId, RouteEvent> names(Map<RouteEventId, Message> names);
}
