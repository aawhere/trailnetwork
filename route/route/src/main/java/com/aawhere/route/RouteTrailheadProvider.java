/**
 * 
 */
package com.aawhere.route;

import com.aawhere.trailhead.TrailheadProvider;

/**
 * Provides trailheads specifically for routes. It's implementation handles calls differently than
 * the default {@link TrailheadProvider}.
 * 
 * @author aroller
 * 
 */
public interface RouteTrailheadProvider
		extends TrailheadProvider {

}
