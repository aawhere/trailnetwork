/**
 * 
 */
package com.aawhere.route;

import com.aawhere.id.Identifier;
import com.aawhere.id.StringIdentifier;

/**
 * Represents a "Map" that this route belongs to.
 * 
 * @author Brian Chapman
 * 
 */
public class MapKey
		extends StringIdentifier<MapKey> {

	private static final long serialVersionUID = -7265646142538183244L;

	public MapKey(String value) {
		super(value, MapKey.class);
	}

	public MapKey(Identifier<?, ?> id) {
		this(id.getKind().getSimpleName() + "-" + id.getValue().toString());
	}

	@SuppressWarnings("unused")
	private MapKey() {
		super(MapKey.class);
	}

}
