/**
 * 
 */
package com.aawhere.route.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.id.StringIdentifierWithParent;
import com.aawhere.persist.ServicedBy;
import com.aawhere.route.Route;
import com.aawhere.route.RouteId;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@ServicedBy(RouteEventProvider.class)
public class RouteEventId
		extends StringIdentifierWithParent<RouteEvent, RouteId> {

	/**
	 * 
	 */
	private static final Class<RouteEvent> KIND = RouteEvent.class;
	private static final long serialVersionUID = -8115796579979791423L;

	/**
	 * 
	 */
	RouteEventId() {
		super(KIND);
	}

	public RouteEventId(String value) {
		super(value, RouteId.class, KIND);
	}

	@Deprecated
	RouteEventId(Long parentValue, String value) {
		super(value, new RouteId(parentValue), KIND);
	}

	@Deprecated
	public RouteEventId(RouteId parent, String value) {
		super(value, parent, KIND);
	}

	public RouteEventId(String value, RouteId parent) {
		super(value, parent, KIND);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.id.StringIdentifierWithParent#getParent()
	 */
	@Override
	@XmlElement(name = Route.FIELD.ROUTE_ID)
	public RouteId getParent() {
		return super.getParent();
	}

	@Override
	protected void setParent(RouteId routeId) {
		super.setParent(routeId);
	}
}
