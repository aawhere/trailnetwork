/**
 * 
 */
package com.aawhere.route.event;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections4.CollectionUtils;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityNameSelector;
import com.aawhere.activity.ActivityNameSelector.PhraseScore;
import com.aawhere.activity.ActivityNameSelector.StringSimilarityRanker;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.route.RouteCompletion;
import com.aawhere.util.rb.Message;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Supplier;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

/**
 * Works with the {@link ActivityNameSelector} to determine the best name to give for route and
 * related events.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteNameSelector {

	/**
	 * The general purpose selector for all activities that have completed this route. This can be
	 * very noisy, but is available to help when more specific selectors don't provide a name with
	 * confidence.
	 */
	@XmlElement
	private ActivityNameSelector completionsNameSelector;
	/**
	 * The ideal name for the Route since these are all reciprocals, or transitive reciprocals of
	 * each other.
	 */
	@XmlElement
	private ActivityNameSelector coursesNameSelector;

	/** Each event may have their own name. */
	@XmlElement
	private HashMap<RouteEventId, ActivityNameSelector> eventsNameSelectors = Maps.newHashMap();
	/**
	 * Course names can be polluted with event names. This offers a description about the course
	 * without specific events.
	 */
	@XmlElement
	private ActivityNameSelector coursesWithoutEventsNameSelector;
	/**
	 * Confidence requires some score. Use this to determine if a score is big enough to be
	 * considered confident enough to use.
	 * 
	 */
	@XmlAttribute
	private Integer minScoreForConfident;

	/** The chosen name from the criteria supplied. */
	@XmlElement
	private Message routeName;

	/**
	 * Used to construct all instances of RouteNameSelector.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteNameSelector> {

		private Iterable<RouteCompletion> completions;
		/**
		 * When all else fails this will provide the default name to use if no selectors are
		 * confident enough to provide a name.
		 * 
		 */
		private Supplier<? extends Message> defaultRouteNameSupplier;
		/**
		 * references useful names corresponding to the completions. activity id is used to enforce
		 * uniqueness since multiple completions may reference the same activity, but it's not worth
		 * double points.
		 */
		private LinkedList<String> completionNames = Lists.newLinkedList();
		/**
		 * For completions with events, this provides the subset of {@link #completionNames} for
		 * each event. Think of Garmin Marathon as the route name, Garmin Marathon 2010, Garmin
		 * Marathon 2011 as the event names.
		 */
		private Multimap<RouteEventId, String> eventsNames = HashMultimap.create();
		/**
		 * The subset of {@link #completionNames} for those completions that are courses. Courses
		 * provide the absolute most accurate name possibilities since they are an exact match.
		 * Imagine the guy that did the Miwok Loop and Muir Beach Loops in a single ride as a
		 * completion of the Miwok Loop Route. The name may be "Helpful", but may not be specific
		 * enough. Only course completions can be used as endpoint name candidates since they
		 * guarantee the same start/end trailheads. Having the entire activity could be used to get
		 * the start point compared to the course start points which would be a nice enhancement
		 * that may take a bit more processing.
		 */
		private LinkedList<String> courseNames = Lists.newLinkedList();

		/**
		 * Providing a more specific name for a route may be valuable without event names. Angel
		 * Island route may result in Hark the Harold Angels Run just because there was a big event
		 * there.
		 */
		private HashMap<ActivityId, String> courseNamesWithoutEvents = Maps.newHashMap();

		public Builder() {
			super(new RouteNameSelector());
		}

		public Builder completions(Iterable<RouteCompletion> courseCompletions) {
			this.completions = courseCompletions;
			return this;
		}

		public Builder minScoreForConfident(Integer minScoreForConfident) {
			building.minScoreForConfident = minScoreForConfident;
			return this;
		}

		public Builder defaultRouteNameSupplier(Supplier<? extends Message> defaultRouteNameSupplier) {
			this.defaultRouteNameSupplier = defaultRouteNameSupplier;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("completions", this.completions);
			Assertion.exceptions().notNull("minScoreForConfident", building.minScoreForConfident);
			Assertion.exceptions().notNull("defaultRouteNameSupplier", this.defaultRouteNameSupplier);
		}

		@Override
		public RouteNameSelector build() {
			RouteNameSelector built = super.build();
			for (RouteCompletion completion : this.completions) {
				ActivityId activityId = completion.activityId();
				// names are already filtered so those provided are meaningful
				List<String> activityNames = completion.activityNames();
				if (completion.activityNamesAvailable()) {
					for (String activityName : activityNames) {
						completionNames.add(activityName);
						if (completion.isCourse()) {
							courseNames.add(activityName);
						}

						// if there is an event then separate the names to discover the event.
						final RouteEventId routeEventId = completion.routeEventId();
						if (routeEventId != null) {
							eventsNames.put(routeEventId, activityName);
						} else if (completion.isCourse()) {
							courseNamesWithoutEvents.put(activityId, activityName);
						}
					}
				}

			}
			built.completionsNameSelector = ActivityNameSelector.create().names(completionNames).build();
			built.coursesNameSelector = ActivityNameSelector.create().names(courseNames).build();
			built.coursesWithoutEventsNameSelector = ActivityNameSelector.create()
					.names(courseNamesWithoutEvents.values()).build();

			// each event needs it's own selector
			for (RouteEventId routeEventId : eventsNames.keySet()) {
				Collection<String> eventNames = eventsNames.get(routeEventId);
				ActivityNameSelector eventNameSelector = ActivityNameSelector.create().names(eventNames).build();
				built.eventsNameSelectors.put(routeEventId, eventNameSelector);
			}

			Message routeName;

			// if a name can be provided without the pollution of events that is best since
			// an event name may have nothing to do with the route.
			if (built.isConfidentAboutName(built.coursesWithoutEventsNameSelector.ranker())) {
				routeName = built.coursesWithoutEventsNameSelector.bestName();

			} else if (built.isConfidentAboutName(built.coursesNameSelector.ranker())) {
				// including the events will provide all that courses can offer.
				routeName = built.completionsNameSelector.bestName();
			} else if (built.isConfidentAboutName(built.completionsNameSelector.ranker())) {
				// completions can come from all over the place to do a course
				// but if the courses can't provide then this might still be better than the default
				routeName = built.completionsNameSelector.bestName();
			} else {
				routeName = this.defaultRouteNameSupplier.get();
			}
			built.routeName = routeName;
			return built;
		}

	}// end Builder

	public Boolean isConfidentAboutName(StringSimilarityRanker rankerOfInterest) {
		final Collection<PhraseScore> highestScores = rankerOfInterest.scores().values();
		return CollectionUtils.isNotEmpty(highestScores)
				&& (highestScores.iterator().next().score() >= this.minScoreForConfident);
	}

	/**
	 * Indicates if courses by themselves are sufficient for finding a name. The alternate would be
	 * to get all completions for the {@link #completionsNameSelector()}.
	 * 
	 * @return
	 */
	@XmlAttribute
	public Boolean isCourseConfidentAboutName() {
		return isConfidentAboutName(this.coursesNameSelector.ranker());
	}

	/** Use {@link Builder} to construct RouteNameSelector */
	private RouteNameSelector() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return the completionsNameSelector
	 */
	public ActivityNameSelector completionsNameSelector() {
		return this.completionsNameSelector;
	}

	/**
	 * @return the coursesNameSelector
	 */
	public ActivityNameSelector coursesNameSelector() {
		return this.coursesNameSelector;
	}

	/**
	 * @return the coursesWithoutEventsNameSelector
	 */
	public ActivityNameSelector coursesWithoutEventsNameSelector() {
		return this.coursesWithoutEventsNameSelector;
	}

	/**
	 * @return the eventsNameSelectors
	 */
	public Map<RouteEventId, ActivityNameSelector> eventsNameSelectors() {
		return Collections.unmodifiableMap(this.eventsNameSelectors);
	}

	/** Provides the best routeName based on the information from all the selectors. */
	public Message routeName() {
		return this.routeName;
	}

	public Message getRouteName() {
		return routeName();
	}

	/**
	 * Provides the best names for the event..possibly one selected for the event or the default
	 * name for the route in general. If no good name is possible then the event won't be provided.
	 * 
	 * @return
	 */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	public Map<RouteEventId, Message> getEventNames() {
		return eventNames();
	}

	public Map<RouteEventId, Message> eventNames() {
		Set<Entry<RouteEventId, ActivityNameSelector>> entries = this.eventsNameSelectors.entrySet();
		HashMap<RouteEventId, Message> names = Maps.newHashMap();

		// for each entry choose the event name if worthy, otherwise the routeName
		for (Entry<RouteEventId, ActivityNameSelector> entry : entries) {
			ActivityNameSelector selector = entry.getValue();
			Message selectedName;
			if (isConfidentAboutName(selector.ranker())) {
				selectedName = selector.bestName();
			} else {
				selectedName = routeName;
			}
			if (selectedName != null) {
				names.put(entry.getKey(), selectedName);
			}
		}
		return names;

	}
}
