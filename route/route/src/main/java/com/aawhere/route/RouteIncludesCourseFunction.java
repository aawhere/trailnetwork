/**
 * 
 */
package com.aawhere.route;

import com.aawhere.activity.ActivityProvider;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.persist.EntityNotFoundException;

import com.google.common.base.Function;
import com.googlecode.objectify.annotation.Load;

/**
 * Transforms the provided route to include the entire course represented by the course id.
 * 
 * Although this could be done by {@link Load} annotation, that is a burden placed on every query
 * where this is a burden placed only on those that required the full activity.
 * 
 * @author aroller
 * 
 */
public class RouteIncludesCourseFunction
		implements Function<Route, Route> {

	private ActivityProvider activityProvider;

	public static RouteIncludesCourseFunction build(ActivityProvider activityProvider) {
		RouteIncludesCourseFunction function = new RouteIncludesCourseFunction();
		function.activityProvider = activityProvider;
		return function;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Route apply(Route input) {
		try {
			return Route.mutate(input).course(activityProvider.getActivity(input.courseId())).build();
		} catch (EntityNotFoundException e) {
			// we have some bad data...fail miserably so we know about it.
			throw ToRuntimeException.wrapAndThrow(e);
		}

	}

}
