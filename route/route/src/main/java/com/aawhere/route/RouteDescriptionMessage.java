/**
 *
 */
package com.aawhere.route;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.ActivityType;
import com.aawhere.person.PersonDescriptionMessage;
import com.aawhere.route.RouteDescriptionPersonalizer.RouteDescription;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Support specifically for {@link RouteDescription}.
 * 
 * @author aroller
 * 
 */
public enum RouteDescriptionMessage implements Message {

	/** Sentence describing the route's completion stats. */
	COMPLETION_STATS(
			"{COMPLETION_COUNT_VALUE, plural, "
					+ "=0{This route has never been completed.} "
					+ "one{This route has been completed {COMPLETION_COUNT_XX}one time{XX} by {PERSON_COUNT_XX}{PERSON_COUNT_VALUE, plural, one{one person} other{# people}}{XX}.} "
					+ "other{This route has been completed {COMPLETION_COUNT_XX}# times{XX} by {PERSON_COUNT_XX}{PERSON_COUNT_VALUE, plural, one{one person} other{# people}}{XX}.}}",
			Param.COMPLETION_COUNT,
			Param.PERSON_COUNT),
	/** Sentence describing where the route starts from */
	STARTS_AT_TRAILHEAD("It starts at the {TRAILHEAD_NAME_XX}{TRAILHEAD_NAME}{XX} trailhead.", Param.TRAILHEAD_NAME),
	ACTIVITY_TYPES(
			"{ACTIVITY_TYPE_COUNT_VALUE, plural, " //
					+ "one{The primary use is {ACTIVITY_TYPE_COUNT_XX}{PRIMARY_ACTIVITY_TYPE}{XX}.} "
					+ "other{The primary use is {ACTIVITY_TYPE_COUNT_XX}{PRIMARY_ACTIVITY_TYPE}{XX} but others use the trail as well.}}",
			Param.PRIMARY_ACTIVITY_TYPE,
			Param.ACTIVITY_TYPE_COUNT),
	VARIATIONS("{VARIATION_COUNT_VALUE, plural, " //
			+ "=0{} "
			+ "one{There is {VARIATION_COUNT_XX}one variation{XX} of this route.} "
			+ "other{There are {VARIATION_COUNT_XX}# variations{XX} of this route.}}", Param.VARIATION_COUNT),
	/**
	 * How many times a specific person completed a route. This uses the activity type as the past
	 * tense verb in the sentence.
	 * 
	 * Aaron Roller mountain biked this route 3 times.
	 * 
	 * The activity is replaced with the general term if the activity type is not known.
	 * 
	 * Aaron Roller completed this route 3 times.
	 */
	RELATIVE_COMPLETIONS_PERSON("{COMPLETION_COUNT_VALUE, plural, " //
			+ "other{{PERSON} {ACTIVITY} this route # times.}"//
			+ "}", Param.COMPLETION_COUNT, PersonDescriptionMessage.Param.PERSON, ActivityType.Param.ACTIVITY),
	/**
	 * When a specific person is not known this describes the number of times a route has been
	 * completed. Activity is the past tense verb.
	 * 
	 * TODO: Add the route name so this message can be displayed independently.
	 */
	RELATIVE_COMPLETIONS("{COMPLETION_COUNT_VALUE, plural, " //
			+ "other{This route has been {ACTIVITY} # times.}"//
			+ "}", Param.COMPLETION_COUNT, ActivityType.Param.ACTIVITY),

	;

	private ParamKey[] parameterKeys;
	private String message;

	private RouteDescriptionMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		/** the number of times the route has been completed */
		COMPLETION_COUNT,
		/** a Count measurement used to report person count in general. */
		PERSON_COUNT,
		/** Describe the person. */
		PERSON_NAME,
		/** */
		TRAILHEAD_NAME,
		/** */
		VARIATION_COUNT,
		/** the activity type with the highest % completions */
		PRIMARY_ACTIVITY_TYPE,
		/** the total number of significant activity types that make up this route */
		ACTIVITY_TYPE_COUNT;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}
}