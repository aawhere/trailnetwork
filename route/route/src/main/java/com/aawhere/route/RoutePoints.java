/**
 * 
 */
package com.aawhere.route;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.xml.XmlNamespace;

/**
 * Provides a simple way to export the "trackpoints" for a route in built, cacheable manner. This
 * will avoid having to rebuild complicated formats, allow for caching of the entire object and
 * allow for the identification of such objects by including the id.
 * 
 * @author aroller
 * 
 */
@SuppressWarnings("serial")
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
public class RoutePoints
		implements Serializable {
	public final RouteId routeId;
	public final String points;

	RoutePoints() {
		this(null, null);
	}

	public RoutePoints(RouteId routeId, String points) {
		super();
		this.routeId = routeId;
		this.points = points;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RoutePoints [routeId=" + this.routeId + ", points=" + this.points + "]";
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.routeId == null) ? 0 : this.routeId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoutePoints other = (RoutePoints) obj;
		if (this.routeId == null) {
			if (other.routeId != null)
				return false;
		} else if (!this.routeId.equals(other.routeId))
			return false;
		return true;
	}

}
