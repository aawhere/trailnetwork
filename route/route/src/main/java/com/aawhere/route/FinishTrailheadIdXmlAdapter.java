/**
 * 
 */
package com.aawhere.route;

import com.aawhere.route.Route.FinishXmlAdapter;
import com.aawhere.trailhead.TrailheadIdXmlAdapter;
import com.aawhere.trailhead.TrailheadProvider;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @deprecated use {@link FinishXmlAdapter}
 * @author aroller
 * 
 */
@Singleton
public class FinishTrailheadIdXmlAdapter
		extends TrailheadIdXmlAdapter {

	/**
	 * 
	 */
	public FinishTrailheadIdXmlAdapter() {
		super();
	}

	/**
	 * @param provider
	 */
	@Inject
	public FinishTrailheadIdXmlAdapter(TrailheadProvider provider) {
		super(provider);
	}

}
