/**
 * 
 */
package com.aawhere.route;

import java.util.Set;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.RepositoryDelegate;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.util.rb.Message;

/**
 * @author aroller
 * 
 */
class RouteRepositoryDelegate
		extends RepositoryDelegate<RouteRepository, Routes, Route, RouteId>
		implements RouteRepository {

	/**
	 * Used to construct all instances of RouteRepositoryDelegate.
	 */
	public static class Builder
			extends ObjectBuilder<RouteRepositoryDelegate> {

		public Builder(RouteRepository repository) {
			super(new RouteRepositoryDelegate(repository));
		}

		@Override
		public RouteRepositoryDelegate build() {
			RouteRepositoryDelegate built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create(RouteRepository repository) {
		return new Builder(repository);
	}

	public static RouteRepositoryDelegate build(RouteRepository repository) {
		return create(repository).build();
	}

	/**
	 * Use {@link Builder} to construct RouteRepositoryDelegate
	 * 
	 * @param repository
	 */
	private RouteRepositoryDelegate(RouteRepository repository) {
		super(repository);
	}

	@Override
	public Route setScore(RouteId routeId, Integer score) throws EntityNotFoundException {
		return update(mutate(routeId).score(score).build());
	}

	@Override
	public Route setName(RouteId routeId, Message name) throws EntityNotFoundException {
		return update(mutate(routeId).name(name).build());
	}

	@Override
	public Route aliasesAdded(RouteId id, Set<RouteId> aliases) throws EntityNotFoundException {
		Route route = load(id);
		if (!route.aliases().containsAll(aliases)) {

			route = update(Route.mutate(route).addAliases(aliases).stale(true).build());
		}
		return route;
	}

	@Override
	public Route setMainId(RouteId id, RouteId mainId) throws EntityNotFoundException {
		return update(mutate(id).mainId(mainId).build());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setStart(com.aawhere.route.RouteId,
	 * com.aawhere.trailhead.TrailheadId)
	 */
	@Override
	public Route setStart(RouteId id, TrailheadId start) throws EntityNotFoundException {
		return update(mutate(id).start(start).build());
	}

	/**
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	private com.aawhere.route.Route.Builder mutate(RouteId id) throws EntityNotFoundException {
		return Route.mutate(this.repository.load(id));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setFinish(com.aawhere.route.RouteId,
	 * com.aawhere.trailhead.TrailheadId)
	 */
	@Override
	public Route setFinish(RouteId id, TrailheadId bestId) throws EntityNotFoundException {
		return update(mutate(id).finish(bestId).build());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setAlternateCount(com.aawhere.route.RouteId,
	 * java.lang.Integer)
	 */
	@Override
	public Route setAlternateCount(RouteId id, Integer alternateCount) throws EntityNotFoundException {
		return update(mutate(id).alternateCount(alternateCount).build());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setCompletionsPending(com.aawhere.route.RouteId,
	 * java.lang.Boolean)
	 */
	@Override
	public Route setStale(RouteId routeId, Boolean stale) throws EntityNotFoundException {
		return update(mutate(routeId).stale(true));
	}

	/**
	 * @param id
	 * @param aliases
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Override
	public Route aliasesSet(RouteId id, Set<RouteId> aliases) throws EntityNotFoundException {
		final Route loaded = load(id);

		if (!aliases.equals(loaded.aliases())) {
			return update(Route.mutate(loaded).setAliases(aliases).stale(true).build());
		}
		// default is to return the loaded as it was found if mutation wasn't worth it
		return loaded;
	}

	/*
	 * @see com.aawhere.route.RouteRepository#update(com.aawhere.route.RouteId)
	 */
	@Override
	public Route.Builder update(RouteId routeId) throws EntityNotFoundException {
		return new Route.Builder(load(routeId)) {
			/*
			 * @see com.aawhere.route.Route.Builder#build()
			 */
			@Override
			public Route build() {
				return update(super.build());
			}
		};
	}
}
