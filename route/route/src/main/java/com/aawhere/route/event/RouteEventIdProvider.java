/**
 * 
 */
package com.aawhere.route.event;

/**
 * @author aroller
 * 
 */
public interface RouteEventIdProvider {

	RouteEventId routeEventId();

}
