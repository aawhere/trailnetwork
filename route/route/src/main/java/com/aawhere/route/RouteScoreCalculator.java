/**
 * 
 */
package com.aawhere.route;

import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.timing.ActivityTimingRelationUserStory;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.xml.XmlNamespace;

/**
 * Purpose-built calculator specializing in calculating the appropriate "score" for a route given
 * the criteria that can affect it's relevance and popularity.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteScoreCalculator {

	static final int COURSE_COUNT_MIN = 5;
	static final Integer LOWEST_SCORE = Route.DEFAULT_SCORE;
	static final Integer MIN_SCORE_EVENT_DOMINATED = LOWEST_SCORE + 1;
	static final Length BOUNDS_LENGTH_MIN_DEFAULT = ActivityTimingRelationUserStory.BOUNDS_LENGTH_MIN_DEFAULT;
	@XmlElement
	private ActivityStats courseStats;
	@XmlElement
	private ActivityStats completionStats;
	@XmlElement
	private ActivityStats socialStats;
	@XmlAttribute
	private Integer score = LOWEST_SCORE;
	@XmlAttribute
	private Integer minimumSocialCountForScoreInflation = 10;
	@XmlAttribute
	private Integer minimumSocialCountForScore = 2;
	/**
	 * TN-790 ensures the number of courses is significant compared to the total completions. Some
	 * "segments" are inflated by the large number of completions because they are on a busy route,
	 * but they are not very good courses themselves. Golden Gate Bridge being a good example.
	 * 
	 */
	@XmlElement
	private Ratio minimumCourseSocialCountRatio = MeasurementUtil.ratio(5, 100);
	/** TN-790 the actual ratio of courses to social completions */
	@XmlElement
	public Ratio courseSocialCompletionRatio;
	/** Requires at least this many courses to be considered for scoring. */
	@XmlAttribute
	private Integer courseCountMin = COURSE_COUNT_MIN;
	@XmlAttribute
	private Double personalCompletionsMultiplicationFactor = 0.5;
	@XmlAttribute
	private Integer numberOfCourses;
	@XmlAttribute
	private Integer numberOfSocialRankings;
	@XmlAttribute
	private int numberOfPersonalRankings;
	@XmlAttribute
	private Boolean finishesAtStart;
	@XmlElement
	private String expression;

	/**
	 * TN-476 must have a minimum footprint for a score.
	 */
	@XmlElement
	private Length boundsDiagonalLengthMin = BOUNDS_LENGTH_MIN_DEFAULT;
	@XmlElement
	private Length boundsDiagonalLength;

	/**
	 * Used to construct all instances of RouteScoreCalculator.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteScoreCalculator> {

		public Builder() {
			super(new RouteScoreCalculator());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("socialStats", building.socialStats);
			Assertion.exceptions().notNull("completionStats", building.completionStats);
			Assertion.exceptions().notNull("courseStats", building.courseStats);
			Assertion.exceptions().notNull("finishesAtStart", building.finishesAtStart);
			Assertion.exceptions().notNull("boundsDiagonalLength", building.boundsDiagonalLength);

		}

		@Override
		public RouteScoreCalculator build() {
			RouteScoreCalculator built = super.build();

			// TODO:Score higher more routes lead from the trailhead
			StringBuilder expression = new StringBuilder();
			// Routes of too small footprint create noise TN-476
			if (QuantityMath.create(built.boundsDiagonalLength).greaterThan(built.boundsDiagonalLengthMin)) {
				// TODO:Score based on length of course? TARGET the average for the sport?
				built.numberOfCourses = built.courseStats.total();
				// if it's not validated by another then it's not worthy
				// 1 references self, two represents a validation
				// FIXME: use an algebra equation to make this more readable.
				// commons math dertivative structure? it has no toString method
				// conditionals could be hidden in methods
				// courseCount().plusSocialCompletionCounts().timesSomethingElse().dividedByEtc();

				built.numberOfSocialRankings = built.socialStats.total();
				// if it's not validated by more than one person it's not a route
				if (built.numberOfSocialRankings >= built.minimumSocialCountForScore) {
					// TN-790 percentage of courses to social completions must meet min to score
					built.courseSocialCompletionRatio = MeasurementUtil.ratio(	built.numberOfCourses,
																				built.numberOfSocialRankings);

					if (built.numberOfCourses >= built.courseCountMin
							&& QuantityMath.create(built.courseSocialCompletionRatio)
									.greaterThan(built.minimumCourseSocialCountRatio)) {

						// TN-931 moved the scoring here to avoid introducing noise to public routes
						int rankingsScore = built.numberOfCourses;
						expression.append(rankingsScore);
						rankingsScore += built.numberOfSocialRankings;
						expression.append("+").append(built.numberOfSocialRankings);
						// magnify score only if there is a large variety of people
						if (built.numberOfSocialRankings >= built.minimumSocialCountForScoreInflation) {
							built.numberOfPersonalRankings = built.completionStats.total()
									- built.numberOfSocialRankings;
							// only inflate the score if the social count exceeds the personal
							if (built.numberOfSocialRankings > built.numberOfPersonalRankings) {
								// if people return often then it shows a favorite
								rankingsScore += (built.numberOfPersonalRankings * built.personalCompletionsMultiplicationFactor);
								expression.append(" + (").append(built.numberOfPersonalRankings).append(" * ")
										.append(built.personalCompletionsMultiplicationFactor).append(")");
							}
							// only count completions if not a one-way segment TN-727
							if (!built.finishesAtStart) {
								Integer maxScoreForOneWay = built.minimumSocialCountForScoreInflation;
								// reset score ignoring completions that aren't courses
								rankingsScore = maxScoreForOneWay;
								expression.insert(0, "min(").append(",").append(maxScoreForOneWay).append(")");
							}
							// reciprocal count should magnify the number of ranking
							// it avoids small sections that were repeated once from rising the
							// to
							// the
							// top
							rankingsScore *= built.numberOfCourses;
							// wrap the expression to indicate this multiplying is against all
							// already
							// calculated.
							expression.insert(0, "(").append(")").append(" * ").append(built.numberOfCourses);

							if (built.completionStats instanceof RouteStats) {
								RouteStats routeCompletionStats = (RouteStats) built.completionStats;
								Ratio routeEventCumulativeStatsRatio = routeCompletionStats
										.getRouteEventCompletionsRatio();
								if (routeEventCumulativeStatsRatio != null) {
									Ratio percentRemaining = MeasurementUtil
											.percentRemaining(routeEventCumulativeStatsRatio);
									rankingsScore *= percentRemaining.doubleValue(ExtraUnits.RATIO);
									// avoid going to zero, just make it super low.
									rankingsScore = Math.max(rankingsScore, MIN_SCORE_EVENT_DOMINATED);
									expression.append(" * ").append(percentRemaining);
								}
							}
						}
						built.score = rankingsScore;
					}
				}
				built.expression = expression.toString();
			}
			return built;
		}

		public Builder completionStats(ActivityStats completionStats) {
			building.completionStats = completionStats;
			return this;
		}

		public Builder socialStats(ActivityStats socialStats) {
			building.socialStats = socialStats;
			return this;
		}

		public Builder courseStats(ActivityStats courseStats) {
			building.courseStats = courseStats;
			return this;
		}

		public Builder finishesAtStart(Boolean finishesAtStart) {
			building.finishesAtStart = finishesAtStart;
			return this;
		}

		public Builder minimumCourseSocialCompletionRatio(Ratio ratio) {
			building.minimumCourseSocialCountRatio = ratio;
			return this;
		}

		public Builder courseCountMin(Integer min) {
			if (min != null) {
				building.courseCountMin = min;
			}
			return this;
		}

		/**
		 * @param boundsDiagonalLength
		 * @return
		 */
		public Builder boundsDiagonalLength(Length boundsDiagonalLength) {
			building.boundsDiagonalLength = boundsDiagonalLength;
			return this;
		}

		public Builder route(Route route) {
			boundsDiagonalLength(route.boundingBox().getDiagonal());
			finishesAtStart(route.isFinishAtStart());
			completionStats(route.completionStats());
			courseStats(route.courseCompletionStats());
			socialStats(route.socialCompletionStats());
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteScoreCalculator */
	private RouteScoreCalculator() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return the score
	 */
	public Integer score() {
		return this.score;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return expression;
	}
}
