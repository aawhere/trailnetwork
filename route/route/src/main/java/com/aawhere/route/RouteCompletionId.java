/**
 * 
 */
package com.aawhere.route;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.StringIdentifierWithParent;
import com.aawhere.lang.If;
import com.aawhere.xml.XmlNamespace;

/**
 * The {@link RouteCompletionId} is the {@link ActivityId} for the {@link ActivityTiming#attempt()}
 * that completed the route. The id is a composite id with the iteration (lap number). Most
 * activities have only a single iteration, but some activities will repeat the same route twice.
 * 
 * This identifier is only unique within the system when combined with it's {@link RouteId} parent.
 * 
 * The resulting identifier form is:
 * 
 * {routeId}-{activityId}-{iteration}
 * 
 * @see RouteCompletion
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteCompletionId
		extends StringIdentifierWithParent<RouteCompletion, RouteId> {

	private static Class<RouteId> PARENT_TYPE = RouteId.class;
	private static Class<RouteCompletion> KIND = RouteCompletion.class;
	private Pair<ActivityId, Integer> childPair;

	RouteCompletionId() {
		super(RouteCompletion.class);
	}

	public RouteCompletionId(RouteId parent, String value) {
		super(value, parent, RouteCompletion.class);
	}

	public RouteCompletionId(String value, RouteId parent) {
		super(value, parent, RouteCompletion.class);
	}

	/**
	 * External references use the route id as a composite with the child. The datastore keeps them
	 * separate so we must also. This constructor is solely for parsing with the parent id embedded.
	 * 
	 * @param value
	 */
	public RouteCompletionId(String value) {
		super(value, PARENT_TYPE, KIND);
	}

	/**
	 * Required for the datstore this receives the route id value, then the child value and puts it
	 * altogether.
	 * 
	 * @deprecated
	 * @param parentValue
	 * @param value
	 */
	@Deprecated
	RouteCompletionId(Long parentValue, String value) {
		this(new RouteId(parentValue), value);
	}

	/**
	 * Creates the id using the route id and the {@link Route#courseId()}, common to represent the
	 * route.
	 * 
	 * @param parentWithCourse
	 */
	public RouteCompletionId(Route parentWithCourse) {
		this(parentWithCourse.id(), parentWithCourse.courseId());
	}

	public RouteCompletionId(RouteId parent, ActivityId attemptId) {
		this(parent, attemptId, null);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public RouteCompletionId(RouteId parent, ActivityId attemptId, Integer repeatCount) {
		super(compositiveIdValue(attemptId, repeatCount), parent, KIND);
	}

	/**
	 * Builds the child id with the optional iteration appended if more than one.
	 * 
	 * @param attemptId
	 * @param iteration
	 * @return
	 */
	private static String compositiveIdValue(ActivityId attemptId, Integer iteration) {

		final String activityPart;
		if (iteration == null || iteration.equals(RouteCompletion.DEFAULT_REPEAT_COUNT)) {
			activityPart = attemptId.getValue();
		} else {
			activityPart = IdentifierUtils.createCompositeIdValue(attemptId, iteration);
		}
		return activityPart;
	}

	private static final long serialVersionUID = -21654295000002453L;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.id.HasParent#getParent()
	 */
	@Override
	public RouteId getParent() {
		return super.parent();
	}

	/*
	 * @see com.aawhere.id.StringIdentifierWithParent#setParent(com.aawhere.id.Identifier)
	 */
	@Override
	protected void setParent(RouteId parent) {
		super.setParent(parent);
	}

	@XmlElement(name = Route.FIELD.ROUTE_ID)
	public RouteId getRouteId() {
		return getParent();
	}

	public RouteId routeId() {
		return getParent();
	}

	/**
	 * @return the iteration
	 */
	public Integer iteration() {
		return If.nil(parts().getRight()).use(RouteCompletion.DEFAULT_REPEAT_COUNT);
	}

	/**
	 * @return
	 */
	private Pair<ActivityId, Integer> parts() {
		// sure wish I could make this more readable. see unit tests for validation
		if (this.childPair == null) {
			String[] parts = IdentifierUtils.splitCompositeIdValue(getValue());
			String activityIdValue;
			Integer iteration;
			final int activityIdOnly = 2;
			if (parts.length == activityIdOnly) {
				activityIdValue = getValue();
				iteration = null;
			} else {
				activityIdValue = IdentifierUtils.createCompositeIdValue(Arrays.copyOfRange(parts, 0, 2));
				iteration = Integer.valueOf(2);
			}
			this.childPair = Pair.of(new ActivityId(activityIdValue), iteration);
		}
		return this.childPair;
	}

	/**
	 * @return the activityId
	 */
	public ActivityId activityId() {
		return parts().getLeft();
	}

	/**
	 * @return the activityId
	 */
	public ActivityId getActivityId() {
		return activityId();
	}

	/**
	 * @return the iteration
	 */
	public Integer getIteration() {
		return iteration();
	}

}
