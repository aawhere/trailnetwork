package com.aawhere.route;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.swagger.DocSupport;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * @author aroller
 * 
 */
public enum RouteCompletionMessage implements Message {

	/** Indicates skipping of completion since derived data should not compete. */
	DERIVED_SKIPPED("{ACTIVITY_ID} skipped because it is derived.", Param.ACTIVITY_ID),
	/** Used when a new creation is made in the datastore. */
	CREATED("Completion created for {ACTIVITY_ID} lap {LAP}.", Param.ACTIVITY_ID, Param.LAP),
	/** When a record has already been found in the datastore. */
	FOUND("Completion found for {ACTIVITY_ID} lap {LAP}.", Param.ACTIVITY_ID, Param.LAP)

	;

	private ParamKey[] parameterKeys;
	private String message;

	private RouteCompletionMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		ACTIVITY_ID, COMPLETION_ID, LAP;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static final class Doc {
		/** Options to expand for RouteCompletion. */
		public static final String COMPLETION_EXPANDS = RouteCompletionField.Absolute.ROUTE_ID
				+ DocSupport.Allowable.NEXT + RouteCompletionField.Absolute.PERSON_ID;
	}
}
