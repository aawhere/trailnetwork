/**
 * 
 */
package com.aawhere.route.event;

import java.util.Map;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceStandard;
import com.aawhere.route.RouteId;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public interface RouteEventProvider
		extends ServiceStandard<RouteEvents, RouteEvent, RouteEventId> {

	/** Finds the existing route event. */
	RouteEvent routeEvent(RouteEventId id) throws EntityNotFoundException;

	/** Finds the existing route event. */
	RouteEvents routeEvents(RouteId routeId) throws EntityNotFoundException;

	/** Provides a function that batch loads the events when given ids. */
	public Function<Iterable<RouteEventId>, Map<RouteEventId, RouteEvent>> idsFunction();

}
