/**
 * 
 */
package com.aawhere.route;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.activity.ActivityType;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteRelation
		implements Serializable {

	// TODO:make this an entity that uses RouteId as it's id to identify itself

	@XmlElement
	private ActivityType activityType;
	@XmlElement
	private RouteId relatedRouteId;

	// TODO: expand why this is related
	/**
	 * Used to construct all instances of RouteRelation.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteRelation> {

		public Builder() {
			super(new RouteRelation());
		}

		public Builder activityType(ActivityType activityType) {
			building.activityType = activityType;
			return this;
		}

		public Builder relatedRouteId(RouteId relatedRouteId) {
			building.relatedRouteId = relatedRouteId;
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("activityType", building.activityType);
			Assertion.exceptions().notNull("relatedRouteId", building.relatedRouteId);
		}

		@Override
		public RouteRelation build() {
			RouteRelation built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct RouteRelation */
	private RouteRelation() {
	}

	/**
	 * @return the activityType
	 */
	public ActivityType activityType() {
		return this.activityType;
	}

	/**
	 * @return the relatedRouteId
	 */
	public RouteId relatedRouteId() {
		return this.relatedRouteId;
	}

	/*
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.activityType == null) ? 0 : this.activityType.hashCode());
		result = prime * result + ((this.relatedRouteId == null) ? 0 : this.relatedRouteId.hashCode());
		return result;
	}

	/*
	 * FIXME:this should be removed when this becomes an entity
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RouteRelation other = (RouteRelation) obj;
		if (this.activityType != other.activityType)
			return false;
		if (this.relatedRouteId == null) {
			if (other.relatedRouteId != null)
				return false;
		} else if (!this.relatedRouteId.equals(other.relatedRouteId))
			return false;
		return true;
	}

	private static final long serialVersionUID = 8897957775992953407L;

}
