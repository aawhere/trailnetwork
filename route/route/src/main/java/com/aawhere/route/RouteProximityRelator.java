/**
 * 
 */
package com.aawhere.route;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityProvider;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

/**
 * Finds a single group of related {@link Routes} for the purpose of sharing a single
 * {@link Trailhead}. The highest scoring route is considered the {@link #bestRelative()} and it's
 * start will provide the {@link #bestLocation()}.
 * 
 * Other routes that are within {@link #maxRadiusOfArea()} will be added to the {@link #related()}.
 * All others that are not will be added to {@link #unrelated()}.
 * 
 * @see RouteTrailheadChooser
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RouteProximityRelator {

	/**
	 * Used to construct all instances of RouteProximityRelator.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<RouteProximityRelator> {

		private Iterable<Route> routes;
		private ActivityProvider activityProvider;

		public Builder() {
			super(new RouteProximityRelator());
		}

		public Builder routes(Iterable<Route> routes) {
			this.routes = routes;
			return this;
		}

		public Builder activityProvider(ActivityProvider activityProvider) {
			this.activityProvider = activityProvider;
			return this;
		}

		public Builder maxRadiusOfArea(Length maxRadiusOfArea) {
			building.maxRadiusOfArea = maxRadiusOfArea;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("routes", this.routes);
			Assertion.exceptions().notNull("activityProvider", this.activityProvider);
			Assertion.exceptions().notNull("maxRadiusOfArea", building.maxRadiusOfArea);

		}

		@Override
		public RouteProximityRelator build() {

			RouteProximityRelator built = super.build();

			built.relations = ArrayListMultimap.create();

			List<Route> possibleRelations = new LinkedList<>();
			HashSet<Route> confirmedRelated = new HashSet<>();
			HashSet<Route> unrelatedRoutes = new HashSet<>();
			// all given routes should be considered unrelated until proven otherwise
			// sorting by score puts the best route at the top of the queue
			Iterables.addAll(possibleRelations, routes);
			Collections.sort(possibleRelations, RouteUtil.highScoreComparator());

			// sort all into related or unrelated after determining the best
			for (Route route : possibleRelations) {
				GeoCoordinate routeLocation = location(route);
				if (built.bestLocation == null) {
					built.bestLocation = routeLocation;
					built.bestRoute = route;
				}
				Length distanceFromProposedTrailhead = GeoMath.length(routeLocation, built.bestLocation);
				// add a record for validation and logging
				built.relations.put(built.bestRoute, Pair.of(route.id(), distanceFromProposedTrailhead));
				// if the current route is within the inhabitable zone
				if (QuantityMath.create(distanceFromProposedTrailhead).lessThanEqualTo(built.maxRadiusOfArea)) {
					confirmedRelated.add(route);
				} else {
					unrelatedRoutes.add(route);
				}
			}

			built.related = Routes.create().addAll(confirmedRelated).sort().build();
			built.unrelated = Collections.unmodifiableSet(unrelatedRoutes);
			return built;
		}

		/**
		 * @param unrelatedRoute
		 * @return
		 */
		private GeoCoordinate location(Route unrelatedRoute) {
			Activity course;
			try {
				course = activityProvider.getActivity(unrelatedRoute.courseId());
			} catch (EntityNotFoundException e) {
				// this is messed up...notify an admin
				throw ToRuntimeException.wrapAndThrow(e);
			}
			return course.getTrackSummary().getStart().getLocation();
		}
	}// end Builder

	/** Use {@link Builder} to construct RouteProximityRelator */
	private RouteProximityRelator() {
	}

	public static Builder create() {
		return new Builder();
	}

	@XmlElement
	private GeoCoordinate bestLocation;

	@XmlElement
	private Route bestRoute;

	/** All routes related to the {@link #TARGET} ordered by highest score first. */
	private Routes related;

	/** The Routes that are too far away from any of the {@link #related()}. */
	private Set<Route> unrelated;

	/**
	 * The limit of how large a trailhead area may be. This differs from
	 * {@link #maxDistanceToTarget} by twice as much. See class notes.
	 * 
	 */
	@XmlElement
	private Length maxRadiusOfArea;

	/**
	 * Keeps track of all attempts to relate a TARGET to unrelated for validation and reporting.
	 */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Multimap<Route, Entry<RouteId, Length>> relations;

	/**
	 * @return the related
	 */
	public Routes related() {
		return this.related;
	}

	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	public Set<RouteId> getRelatedIds() {
		return Sets.newHashSet(IdentifierUtils.ids(this.related));
	}

	/**
	 * @return the unrelated
	 */
	public Set<Route> unrelated() {
		return this.unrelated;
	}

	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	public Set<RouteId> getUnrelatedIds() {
		return IdentifierUtils.idsFrom(this.unrelated);
	}

	/**
	 * @return the bestLocation
	 */
	public GeoCoordinate bestLocation() {
		return this.bestLocation;
	}

	/**
	 * @return the relations
	 */
	public Multimap<Route, Entry<RouteId, Length>> relations() {
		return this.relations;
	}

	/**
	 * Provides the best representation of {@link #related()}...the first since they are sorted by
	 * score.
	 * 
	 * @return
	 */
	public Route bestRelative() {
		return bestRoute;
	}

	/**
	 * @return the maxRadiusOfArea
	 */
	public Length maxRadiusOfArea() {
		return this.maxRadiusOfArea;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.valueOf(related());
	}

}
