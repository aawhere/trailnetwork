package com.aawhere.route;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxProvider;
import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

/**
 * Represents an ordered collection of Routes
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class Routes
		extends BaseFilterEntities<Route>
		implements BoundingBoxProvider {

	private static final long serialVersionUID = -6871594197064818556L;

	/** Use {@link Builder} to construct Routes */
	private Routes() {
	}

	@XmlElement(name = Route.FIELD.DOMAIN)
	@Override
	public List<Route> getCollection() {
		return super.getCollection();
	}

	/**
	 * Intended for transformation of routes into an optional format this returns the routes for use
	 * of transformation.
	 * 
	 * @return
	 * 
	 */
	@XmlJavaTypeAdapter(RoutePointsXmlAdapter.class)
	private Collection<Route> getTracks() {
		return all();
	}

	/**
	 * Used to construct all instances of Routes.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, Route, Routes> {
		private boolean sort = false;

		public Builder() {
			super(new Routes());
		}

		private Builder(Routes routes) {
			super(routes, routes.getCollection());
		}

		public Builder sort() {
			this.sort = true;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public Routes build() {

			if (sort) {
				// sorting before building sorts the collection by reference
				Collections.sort(building.getCollection(), RouteUtil.highScoreComparator());
			}
			final Routes built = super.build();

			built.boundingBox = RouteUtil.boundingBox(built);
			return built;
		}
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(Routes routes) {
		return new Builder(routes);
	}

	/**
	 * Provides the complete boundary for all routes contained within.
	 * 
	 */
	@XmlElement(name = BoundingBox.FIELD.NAME)
	private BoundingBox boundingBox;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.BoundingBoxProvider#bounds()
	 */
	@Override
	public BoundingBox boundingBox() {
		return this.boundingBox;
	}
}
