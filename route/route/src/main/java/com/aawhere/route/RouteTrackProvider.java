/**
 * 
 */
package com.aawhere.route;

import javax.annotation.Nonnull;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;

/**
 * Anything that can provide a {@link Track} when given the {@link Route} it represents.
 * 
 * @author aroller
 * 
 */
public interface RouteTrackProvider {

	/**
	 * Given the route this will return the corresponding Track.
	 * 
	 * @param route
	 * @return
	 */
	@Nonnull
	public Iterable<Trackpoint> track(@Nonnull Route route) throws EntityNotFoundException;
}
