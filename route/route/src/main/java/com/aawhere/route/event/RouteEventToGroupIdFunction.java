/**
 * 
 */
package com.aawhere.route.event;

import com.aawhere.person.group.SocialGroupId;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class RouteEventToGroupIdFunction
		implements Function<RouteEvent, SocialGroupId> {

	public static RouteEventToGroupIdFunction build() {
		return new RouteEventToGroupIdFunction();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public SocialGroupId apply(RouteEvent input) {
		if (input == null) {
			return null;
		}
		return input.socialGroupId();
	}

}
