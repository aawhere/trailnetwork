/**
 *
 */
package com.aawhere.route;

import static com.aawhere.id.IdentifierUtils.*;
import static com.aawhere.lang.number.NumberUtilsExtended.*;
import static com.aawhere.route.RouteUtil.*;
import static com.aawhere.test.TestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import javax.measure.quantity.Length;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportException;
import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityImportServiceTestUtil;
import com.aawhere.activity.ActivityService;
import com.aawhere.activity.ActivityServiceTestUtil;
import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.ActivityUtil;
import com.aawhere.activity.DeviceSignalActivitySelector;
import com.aawhere.activity.ExampleTracksImportServiceTestUtil;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.ScreeningResults;
import com.aawhere.activity.timing.ActivityTimingServiceTestUtil;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationKeyStats;
import com.aawhere.app.DeviceTestUtil;
import com.aawhere.app.KnownApplication;
import com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCellComparison;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityServiceUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilteredResponse;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonService;
import com.aawhere.person.Persons;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.person.group.SocialGroups;
import com.aawhere.queue.QueueTestUtil;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.route.Route.IfScoreIsSignficant;
import com.aawhere.route.RouteServiceTestUtil.Builder;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventServiceTestUtil;
import com.aawhere.route.event.RouteNameSelector;
import com.aawhere.search.FieldNotSearchableException;
import com.aawhere.search.MaxIndexAttemptsExceeded;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.search.UnknownIndexException;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ServiceTest;
import com.aawhere.track.ExampleTracks;
import com.aawhere.trailhead.RouteAwareTrailheadScoreCalculator;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadMessage;
import com.aawhere.trailhead.TrailheadService;
import com.aawhere.trailhead.TrailheadServiceTestUtil;
import com.aawhere.trailhead.TrailheadTestUtil;
import com.aawhere.trailhead.TrailheadUtil;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * @see RouteService
 * @author Brian Chapman
 * 
 */
@Category(ServiceTest.class)
public class RouteServiceTest {

	private RouteServiceTestUtil routeTestUtil;
	private RouteService routeService;
	private RouteCompletionService completionService;
	private TrailheadService trailheadService;
	private TrailheadServiceTestUtil trailheadTestUtil;
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private ActivityTimingServiceTestUtil timingServiceUtil;
	private ExampleTracksImportServiceTestUtil examples;

	@Before
	public void setUpServices() {
		helper.setUp();
		final Builder utilBuilder = RouteServiceTestUtil.create().examplesPrepared();
		routeTestUtil = utilBuilder.build();
		this.examples = this.routeTestUtil.exampleTracksUtil();
		routeService = routeTestUtil.getRouteService();
		trailheadTestUtil = routeTestUtil.getTrailheadServiceTestUtil();
		trailheadService = trailheadTestUtil.service();
		timingServiceUtil = this.routeTestUtil.getTimingRelationServiceTestUtil();
		this.completionService = routeService.completionService;
		GpxDocumentProducer.register();
	}

	@After
	public void tearDownService() {
		helper.tearDown();
	}

	@Test
	public void testStoreRoute() {
		Route route = routeTestUtil.createPersistedRoute();
		assertNotNull(route.getId());
	}

	@Test
	public void testGetRoute() throws EntityNotFoundException {
		Route route = routeTestUtil.createPersistedRoute();
		Route retreived = routeService.route(route.getId());
		assertNotNull(retreived);
		assertEquals(route, retreived);
	}

	/**
	 * During the transition of routes there will be routes that identify courses, but no
	 * completions. This will ensure the completion exists if it identifies the course id.
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	public void testUpdateCreatesMissingCompletionForCourse() throws EntityNotFoundException {
		Route route = this.routeTestUtil.routeCreated();
		RouteCompletionId courseCompletionId = new RouteCompletionId(route);
		// remove it forcefully to simulate current state of v26.
		this.routeTestUtil.getRouteCompletionRepository().delete(courseCompletionId);
		try {
			this.completionService.completion(courseCompletionId);
			fail("why did we find a completion that shouldn't exist?");
		} catch (EntityNotFoundException e) {
			RouteUpdateDetails routeUpdated = this.routeService.routeUpdated(route.id());
			assertEquals(routeUpdated.updated().id(), this.completionService.completion(courseCompletionId).routeId());
		}

	}

	@Test
	public void testRouteUpdated() throws BaseException {
		Route created = assertRouteCreated();
		RouteUpdateDetails updateDetails = this.routeService.routeUpdated(created.id());
		assertEquals(created.getEntityVersion(), updateDetails.original().getEntityVersion());
		final Route updated = updateDetails.updated();
		assertNotEquals(created.getEntityVersion(), updated.getEntityVersion());
		assertEquals("no change in courses during first update", created.courseId(), updated.courseId());
		assertNotNull(updated.completionStats());
		assertNotNull(updated.courseCompletionStats());
		assertNotNull(updated.socialCompletionStats());
		assertNotNull(updated.distanceCategories());
		assertFalse(updated.isStale());
		Integer expectedStatsCount = 1;
		assertEquals(expectedStatsCount, updated.completionStats().total());
		assertEquals("the one completion is a course", expectedStatsCount, updated.courseCompletionStats().total());
		assertEquals(expectedStatsCount, updated.socialCompletionStats().total());
		// activity types can be consilated (i.e. hike -> walk).
		// Activity course =
		// this.activityServiceTestUtil().getActivityService().activity(updated.courseId());
		// assertEquals(course.getActivityType(),
		// updated.completionStats().activityTypeStats().first().category());
		assertEquals(Route.DEFAULT_SCORE, updated.score());
		assertNull(	"completions aren't pending so heavy processing should be avoided",
					this.routeService.routeUpdated(updated.id()).organizer());
	}

	@Test
	public void testRouteNameUpdatedCourses() throws BaseException {
		Boolean isCourse = true;
		assertRouteNameUpdated(isCourse);
	}

	/** proves the naming will fallback to completions if courses aren't used. */
	@Test
	public void testRouteNameUpdatedCompletions() throws BaseException {
		Boolean isCourse = false;
		assertRouteNameUpdated(isCourse);
	}

	/**
	 * @param isCourse
	 * @throws EntityNotFoundException
	 */
	private void assertRouteNameUpdated(Boolean isCourse) throws EntityNotFoundException {
		Route route = this.routeTestUtil.createPersistedRoute();
		String name = "Holy Roller";
		RouteCompletionServiceTestUtil completionServiceTestUtil = RouteCompletionServiceTestUtil.create()
				.routeUtil(routeTestUtil).build();
		for (int i = 0; i < 20; i++) {
			Activity activity = this.routeTestUtil.getImportTestUtil().getActivityServiceTestUtil()
					.createPersistedActivity(name);
			completionServiceTestUtil.routeCompletionBuilder(route, activity).isCourse(isCourse).build();
		}
		RouteUpdateDetails updated = this.routeService.routeUpdated(route.id());
		RouteNameSelector nameSelector = updated.nameSelector();
		assertEquals("route name is incorrect ", name, nameSelector.routeName().toString());
		assertEquals("courses should be used", isCourse, nameSelector.isCourseConfidentAboutName());
	}

	/**
	 * TN-927 Default names aren't wiping out previously badly named routes
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	public void testRouteNameUpdatedNotConfident() throws EntityNotFoundException {
		Route route = this.routeTestUtil.createPersistedRoute();
		Message original = route.name();
		assertNotNull(original);
		RouteUpdateDetails routeUpdated = this.routeService.routeUpdated(route.id());
		assertFalse("there should be no confidence", routeUpdated.nameSelector().isCourseConfidentAboutName());
		TestUtil.assertNullExplanation("no confidence, no name", routeUpdated.nameSelector().routeName());
		TestUtil.assertNullExplanation(	"the previous name should have been replaced by null, the default name supplier",
										routeUpdated.updated().name());
	}

	@Test
	public void testRouteUpdatedNotFoundNoOrphans() {
		final RouteId lookingFor = RouteTestUtils.routeId();
		try {
			this.routeService.routeUpdated(lookingFor);
			fail("how did we find" + lookingFor);
		} catch (EntityNotFoundException e) {
			assertEquals("exception is reporting the wrong id", lookingFor, e.getId());
			assertEquals("in this case the queue should be passified", WebApiQueueUtil.ACCEPTED, e.getStatusCode());
		}
	}

	@Test
	public void testRouteUpdateForce() throws EntityNotFoundException {
		Route route = this.routeTestUtil.routeUpdated();
		RouteUpdateDetails detailsNotForced = this.routeService.routeUpdated(route.id());
		assertNull(detailsNotForced.updated());
		RouteUpdateDetails detailsForced = this.routeService.routeUpdated(route.id(), SystemWebApiUri.FORCED);
		assertNotNull(detailsForced.updated());
	}

	@Test
	public void testRouteUpdatedNotFoundWithOrphans() {
		final Route route = this.routeTestUtil.createPersistedRoute();
		// create an orphaned completion
		RouteCompletion routeCompletion = this.routeTestUtil.routeCompletion(route);
		this.routeTestUtil.getRouteRepository().delete(route);
		final RouteId lookingFor = route.id();
		try {
			this.routeService.routeUpdated(lookingFor);
			fail("should have been removed" + lookingFor);
		} catch (EntityNotFoundException e) {
			assertEquals("exception is reporting the wrong id", lookingFor, e.getId());
			assertEquals(	"the completion orphan should be found and logged and this should stick in the queue",
							HttpStatusCode.NOT_FOUND,
							e.getStatusCode());
			// the orphans should have been removed
			try {
				completionService.completion(routeCompletion.id());
				fail("completion should have been removed");
			} catch (EntityNotFoundException e1) {
				// properly removed so subsequent request should pass
			}

		}
	}

	/**
	 * A route was merged with another, there is record of it, but completions remain as children of
	 * the removed. Removal of the orphans and re-assigning to the parent should be handled and
	 * re-queue for another attempt until clean.
	 * 
	 * @throws CoursesNotRelatedException
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	public void testRouteUpdatedNotFoundWithOrphansForAlias() throws EntityNotFoundException,
			CoursesNotRelatedException {
		Activity winnerActivity = this.examples.line();
		Route winner = routeTestUtil.createPersistedRoute(winnerActivity);
		Activity aliasActivity = this.examples.lineReciprocal();
		Route alias = routeTestUtil.createPersistedRoute(aliasActivity);
		RouteCompletion orphan = this.routeTestUtil.routeCompletion(alias);
		// remove the alias leaving the completion orphaned
		routeTestUtil.getRouteRepository().aliasesAdded(winner.id(), Sets.newHashSet(alias.id()));
		routeTestUtil.getRouteRepository().delete(alias);

		try {
			this.routeService.routeUpdated(alias.id());
			fail("alias was deleted");
		} catch (EntityNotFoundException e) {
			// expected...let's make sure the orphans are gone, but now related
			// to the winner
			Iterable<RouteCompletion> winnerCompletions = completionService.completions(winner.id());
			TestUtil.assertContains("orphan did not get adopted by new parent " + winner.id(),
									orphan.activityId(),
									RouteCompletionUtil.activityIds(winnerCompletions));
			try {
				completionService.completion(orphan.id());
				fail("orphan not removed");
			} catch (Exception e1) {
				// great. orphan is gone, but related to new parent.
				assertEquals(	"all is cleaned up, but re-visit so it can make sure",
								HttpStatusCode.NOT_FOUND,
								e.getStatusCode());
			}
		}
	}

	@Test
	public void testPendingRoutes() {
		Route routeCreated = this.routeTestUtil.routeCreated();
		assertTrue(routeCreated.isStale());
		Route routeUpdated = this.routeTestUtil.routeUpdated();
		assertFalse(routeUpdated.isStale());
		Filter filter = RouteDatastoreFilterBuilder.create().stale().build();
		Routes routes = routeService.routes(filter);
		TestUtil.assertContainsOnly(routeCreated, routes);
		assertEquals("update found a different number pending", routes.size(), routeService.routesUpdated().intValue());
	}

	/**
	 * TN-850 exposed an inefficient way of bulk loading routes. This ensures that routes being
	 * searched for will be included in the iterable if the route is found directly or if it is an
	 * alias. It will also ensure a route requested that is missing is ignored.
	 * 
	 * @throws EntityNotFoundException
	 */

	@Test
	public void testRoutesBulkLoaded() throws EntityNotFoundException {
		Route exists1 = this.routeTestUtil.createPersistedRoute();
		Route exists2 = this.routeTestUtil.createPersistedRoute();
		Route expectedFromAlias = this.routeTestUtil.createPersistedRoute();
		RouteId alias = RouteTestUtils.routeId();
		this.routeTestUtil.getRouteRepository().aliasesAdded(expectedFromAlias.id(), Sets.newHashSet(alias));
		RouteId missing = RouteTestUtils.routeId();
		assertEquals("alias isn't found from " + alias, expectedFromAlias, this.routeService.route(alias));
		HashSet<RouteId> requestIds = Sets.newHashSet(exists1.id(), exists2.id(), alias, missing);
		Iterable<Route> found = this.routeService.routes(requestIds);
		TestUtil.assertIterablesEquals(	"routes bulk loaded should find by alias",
										Sets.newHashSet(exists1, exists2, expectedFromAlias),
										found);
	}

	@Test
	public void testPersonsMerged() throws BaseException {
		RouteCompletion routeCompletion = this.routeTestUtil.routeCompletionRelated();
		RouteCompletion routeCompletion2 = this.routeTestUtil.routeCompletionRelated();
		RouteCompletion routeCompletion3 = this.routeTestUtil.routeCompletionRelated();
		PersonService personService = this.routeTestUtil.getImportTestUtil().getPersonServiceTestUtil()
				.getPersonService();
		final ArrayList<PersonId> persons = Lists.newArrayList(routeCompletion.personId(), routeCompletion2.personId());
		Person merged = personService.merged(persons);
		assertEquals("can't find by alias", merged, personService.person(routeCompletion.getPerson()));
		assertEquals("can't find by alias", merged, personService.person(routeCompletion2.getPerson()));

		Integer completionsUpdated = this.routeService.personsMerged(persons);

		assertEquals(I(2), completionsUpdated);
		Iterable<RouteCompletion> completionsForWinner = this.completionService.completions(merged.id());
		Iterable<RouteCompletion> completionsForPersons = this.completionService.completionsForPersons(persons);
		TestUtil.assertIterablesEquals(	"should be the same result since they are the same person now",
										completionsForWinner,
										completionsForPersons);
		TestUtil.assertDoesntContain(routeCompletion3, completionsForWinner);
	}

	@Test
	public void testPersonsMergedNotFound() {

	}

	@Test
	public void testBoundsIntersectionWithItself() {
		Route route = routeTestUtil.createPersistedRoute(this.examples.line());
		Route decoy = routeTestUtil.createPersistedRoute(this.examples.distant());
		assertFalse(GeoCellComparison.left(route.startArea()).right(decoy.startArea()).intersects());
		Filter filter = RouteDatastoreFilterBuilder.create().setBounds(route.boundingBox().getBounds()).build();
		Routes routes = routeService.routes(filter);
		TestUtil.assertContainsOnly(route, routes);
	}

	@Test
	public void testTrailheadIntersections() throws EntityNotFoundException {
		Route route = routeTestUtil.createPersistedRouteAndTrailheads(this.examples.line());
		Trailhead start = trailheadService.trailhead(route.start());
		Trailhead finish = trailheadService.trailhead(route.finish());

		{
			final Routes startAreaFilter = this.routeService.routes(RouteDatastoreFilterBuilder.create()
					.startIntersection(start.area()).build());
			TestUtil.assertContainsOnly(route, startAreaFilter);
			final Routes finishAreaFilter = this.routeService.routes(RouteDatastoreFilterBuilder.create()
					.startIntersection(finish.area()).build());
			TestUtil.assertDoesntContain(route, finishAreaFilter);
		}

	}

	@Test
	public void testBoundsLargeArea() {
		Route route = routeTestUtil.createPersistedRoute(this.examples.line());
		routeTestUtil.createPersistedRoute(this.examples.distant());
		GeoCells lowRes = GeoCellUtil.filterToResolution(GeoCellUtil.withAncestors(Lists.newArrayList(TrailheadUtil
				.geoCellFrom(route.start()))), Resolution.THREE);
		{
			Filter cellsFilter = RouteDatastoreFilterBuilder.create().startIntersection(lowRes).filter();
			TestUtil.assertContainsOnly(route, this.routeService.routes(cellsFilter));
		}
		// now by bounds
		{
			final GeoCellBoundingBox box = GeoCellBoundingBox.createGeoCell().setGeoCells(lowRes).build();
			Filter bboxFilter = RouteDatastoreFilterBuilder.create().startIntersection(box).build();
			TestUtil.assertContainsOnly(route, this.routeService.routes(bboxFilter));
			Filter boundsFilter = RouteDatastoreFilterBuilder.create().setBounds(box.getBounds()).filter();
			TestUtil.assertContainsOnly(route, this.routeService.routes(boundsFilter));
		}
	}

	@Test
	public void testTrailheadUpdated() throws EntityNotFoundException, CourseNotWorthyException,
			CoursesNotRelatedException {
		final ActivityId course = this.examples.line().id();
		final ActivityId reciprocal = this.examples.lineReciprocal().id();
		final ActivityId validation1 = this.examples.beforeAndAfter().id();
		final ActivityId validation2 = this.examples.lineBefore().id();
		final ActivityId validation3 = this.examples.lineLonger().id();
		final ActivityId validation4 = this.examples.lineLongerReturn().id();
		this.routeTestUtil.importAndRelateCompletions(	course,
														reciprocal,
														validation1,
														validation2,
														validation3,
														validation4);
		Route route = routeService.route(course);
		route = routeService.routeUpdated(route.id()).updated();
		final Integer expectedCourseRankingCount = 6;
		assertEquals("all 6 activities complete the course", expectedCourseRankingCount, route.completionStats()
				.total());
		final Integer expectedNumberOfCourses = 2;
		assertEquals("only course and reciprocal expected", expectedNumberOfCourses, route.courseCompletionStats()
				.total());

		final RouteTrailheadOrganizer organizer = this.routeService.trailheadUpdated(route.start());
		Trailhead trailheadUpdated = organizer.updated();
		assertNotNull(trailheadUpdated.numberOfAlternates());
		final ActivityStats startRouteCompletionStats = trailheadUpdated.routeCompletionsStats();
		// assertNull("TN-865 stopped updating route completions", startRouteCompletionStats);
		assertNotNull(startRouteCompletionStats);
		assertEquals(	"number of activities completing the route is wrong",
						expectedCourseRankingCount,
						startRouteCompletionStats.total());
	}

	/**
	 * TN-886 requires non-public trailheads to be updated too. If no public routes exist, then find
	 * the private routes and use it for the statistics.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testTrailheadUpdatedNonPublicRoutes() throws BaseException {
		Route route = this.routeTestUtil.createPersistedRouteAndTrailheads();
		route = this.routeService.setScore(route.id(), Route.DEFAULT_SCORE);
		RouteTrailheadOrganizer trailheadOrganizer = this.routeService.trailheadUpdated(route.start());
		Trailhead updated = trailheadOrganizer.updated();
		assertEquals("non public route gave trailhead a score", Trailhead.DEFAULT_SCORE, updated.score());
		assertFalse("the non public route is the only so the trailhead should not be public",
					trailheadOrganizer.publicTrailhead());
	}

	@Test
	public void testTrailheadUpdateNotFoundNoOrphans() {
		try {
			this.routeService.trailheadUpdated(TrailheadTestUtil.trailheadId());
			fail("can't find what isn't there");
		} catch (EntityNotFoundException e) {
			assertEquals("can't find a trailhead no big whoop", WebApiQueueUtil.ACCEPTED, e.getStatusCode());
		}
	}

	@Test
	@Ignore("this uses the queue to clean out unwanted orphans, but all is moving to process TN-889")
	public void testTrailheadUpdateNotFoundWithOrphans() throws BaseException {
		Route route = this.routeTestUtil.createPersistedRouteAndTrailheads(this.examples.line());
		final TrailheadId trailheadId = route.start();
		// give it a new course so it will get a new trailhead
		Activity differentCourse = this.examples.realCourse();
		this.routeTestUtil.getRouteRepository().update(route.id()).course(differentCourse).build();

		// remove all trailheads to force not found
		this.routeTestUtil.getTrailheadServiceTestUtil().deleteAll();

		try {
			this.routeService.trailheadUpdated(trailheadId);
			fail(trailheadId + "should have been removed");
		} catch (EntityNotFoundException e) {
			assertEquals(	"orphans found so we need to re-visit until no more orphans",
							HttpStatusCode.NOT_FOUND,
							e.getStatusCode());
			Route updated = this.routeService.route(route.id());
			assertEquals("new course didn't get assigned", differentCourse.id(), updated.courseId());
			TestUtil.assertNotEquals(	"route should have been updated and found different trailhead",
										trailheadId,
										updated.start());
		}

	}

	@Test(expected = EntityNotFoundException.class)
	public void testDeleteRouteMissingHasOrphanCompletions() throws EntityNotFoundException {
		Route route = RouteTestUtils.createRouteWithId();
		RouteCompletion routeCompletion = this.routeTestUtil.routeCompletion(route);
		routeService.routeDeleted(route.id());
		routeService.completionService.completion(routeCompletion.id());
	}

	/**
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testDeleteRouteOnly() {
		Route route = this.routeTestUtil.createPersistedRoute();
		final RouteId routeId = route.id();
		assertRouteDeleted(routeId);
	}

	@Test
	public void testDeleteRoutes() throws BaseException {
		RouteCompletion routeCompletion1 = this.routeTestUtil.routeCompletionRelated();
		RouteCompletion routeCompletion2 = this.routeTestUtil.routeCompletionRelated();
		RouteCompletion routeCompletion3 = this.routeTestUtil.routeCompletionRelated();
		RouteId route1 = routeCompletion1.routeId();
		RouteId route2 = routeCompletion2.routeId();
		RouteId route3 = routeCompletion3.routeId();
		Filter filter = EntityServiceUtil.deletionConfirmed(Filter.create().idsIn(ImmutableList.of(route1, route3)))
				.build();
		Integer routesDeleted = this.routeService.routesDeleted(filter);
		assertEquals("wrong number of routes deleted", 2, routesDeleted.intValue());
		assertFalse("route 1 expected to be gone", this.routeService.exists(route1));
		assertTrue("route 2 expected to be gone", this.routeService.exists(route2));
		assertFalse("route 3 expected to be gone", this.routeService.exists(route3));
	}

	/**
	 * @param routeId
	 * @throws EntityNotFoundException
	 */
	public void assertRouteDeleted(final RouteId routeId) {
		this.routeService.routeDeleted(routeId);
		try {
			this.routeService.route(routeId);
			fail("route should have been deleted");
		} catch (EntityNotFoundException e) {
			// good
		}
	}

	@Test(expected = EntityNotFoundException.class)
	public void testDeleteRouteWithCompletions() throws EntityNotFoundException {
		RouteCompletion routeCompletion = this.routeTestUtil.routeCompletion();
		assertRouteDeleted(routeCompletion.routeId());
		this.completionService.completion(routeCompletion.id());
	}

	@Test
	public void testCoursesIncluded() {
		Routes created = routeTestUtil.createPersistedRoutes();
		Integer expectedCount = created.size();
		Filter filter = RouteDatastoreFilterBuilder.create().coursesIncluded().setLimit(expectedCount).build();
		Routes found = routeService.routes(filter);
		TestUtil.assertSize(expectedCount, found);
		final Activities activities = found.support(Activities.class);
		assertNotNull(activities);
		// this test doesn't use real persisted activities apparently
		// assertNotNull(found.boundingBox());
		// TestUtil.assertSize(expectedCount, activities);
	}

	@Test
	public void testCourseCompletedRouteCompletion() throws EntityNotFoundException {
		Activity course = this.examples.line();
		Activity attempt = this.examples.lineBefore();
		LinkedList<ActivityTiming> timings = this.timingServiceUtil.importAndRelate(course.id(), attempt.id());
		ActivityTiming courseTiming = timings.get(0);
		RouteCompletions completionsForTiming = this.routeService.completionsUpdated(courseTiming.id());
		TestUtil.assertSize("the attempt is created during normal processing", 1, completionsForTiming);
		RouteCompletion attemptCompletion = Iterables.getFirst(completionsForTiming, null);
		final RouteId routeId = attemptCompletion.routeId();
		Route route = routeService.route(routeId);
		assertTrue("completion is created, but not ranked or ordered", route.isStale());
		RouteCompletions completionsFound = this.routeService.completionService.completions(route.id(), Filter.create()
				.filter());
		TestUtil.assertSize("one for attempt, one for course", 2, completionsFound);
		assertEquals("wrong course assigned", course.id(), route.courseId());
	}

	/**
	 * When the course has not yet been created, but it's reciprocal course has a route already then
	 * the existing reciprocal route should be used and the course assigned to it.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testCourseCompletedRouteReciprocalFound() throws EntityNotFoundException {
		Activity course = this.examples.line();
		Activity reciprocal = this.examples.lineReciprocal();
		Activity attempt = this.examples.lineBefore();
		this.timingServiceUtil.importAndRelate(course.id(), reciprocal.id(), attempt.id());
		RouteCompletions completionsForReciprocal = this.routeService.completionsUpdated(new ActivityTimingId(
				reciprocal.id(), attempt.id()));
		TestUtil.assertSize(1, completionsForReciprocal);
		Route routeForReciprocal = routeService.route(Iterables.getFirst(completionsForReciprocal, null).routeId());
		RouteCompletions completionsForCourse = this.routeService.completionsUpdated(new ActivityTimingId(course.id(),
				reciprocal.id()));
		TestUtil.assertSize("only the attempt is created", 1, completionsForCourse);
		Route routeForCourse = routeService.route(Iterables.getFirst(completionsForCourse, null).routeId());
		assertEquals("since they are reciprocals they should share the same route", routeForReciprocal, routeForCourse);
		TestUtil.assertSize("attempt, course and reciprocal should all have completions",
							3,
							this.routeService.completionService.completions(routeForCourse.id(), Filter.create()
									.filter()));
	}

	public Route route(RouteId routeId) throws EntityNotFoundException {
		return routeService.route(routeId);
	}

	/**
	 * @throws EntityNotFoundException
	 * @see #testCourseCompletedRouteReciprocalFound()
	 */
	@Test
	public void testCourseCompletedRouteReciprocalMerge() throws EntityNotFoundException {
		Activity course = this.examples.line();
		Activity reciprocal = this.examples.lineReciprocal();
		Activity attempt = this.examples.lineBefore();
		this.timingServiceUtil.importAndRelate(course.id(), reciprocal.id(), attempt.id());
		RouteCompletions completionsForAttempAgainstCourse = this.routeService.completionsUpdated(new ActivityTimingId(
				course.id(), attempt.id()));
		Route routeForCourse = route(Iterables.getFirst(completionsForAttempAgainstCourse, null).routeId());
		RouteCompletions completionsForAttempAgainstReciprocal = this.routeService
				.completionsUpdated(new ActivityTimingId(reciprocal.id(), attempt.id()));
		Route routeForReciprocal = route(Iterables.getFirst(completionsForAttempAgainstReciprocal, null).routeId());
		TestUtil.assertNotEquals(	"they are only tied together by an attempt, not each other since the reciprocal timing not yet processed",
									routeForCourse,
									routeForReciprocal);

		{
			RouteCompletions completionsForCourseReciprocal = this.routeService
					.completionsUpdated(new ActivityTimingId(course.id(), reciprocal.id()));
			assertEquals(routeForCourse, route(Iterables.getOnlyElement(completionsForCourseReciprocal).routeId()));
		}
		{
			Route routeForCourseAfterFusion = this.routeService.route(routeForCourse.id());
			assertEquals("no merge yet...", routeForCourse.id(), routeForCourseAfterFusion.id());
			TestUtil.assertContains(routeForReciprocal.id(), routeForCourseAfterFusion.aliases());
			assertTrue("update is pending", routeForCourseAfterFusion.isStale());
		}
		{
			Route routeForReciprocalAfterFusion = this.routeService.route(routeForReciprocal.id());
			TestUtil.assertContains(routeForCourse.id(), routeForReciprocalAfterFusion.aliases());
			assertEquals("no merge yet...", routeForReciprocal.id(), routeForReciprocalAfterFusion.id());
			assertTrue("update is pending", routeForReciprocalAfterFusion.isStale());
		}
		{
			RouteCompletions completionsForReciprocalCourse = this.routeService
					.completionsUpdated(new ActivityTimingId(reciprocal.id(), course.id()));
			assertEquals(routeForReciprocal, route(Iterables.getOnlyElement(completionsForReciprocalCourse).routeId()));
		}

		this.routeTestUtil.scoreForMerge(routeForCourse, routeForReciprocal);
		Route merged = this.routeService.routesMerged(Sets.newHashSet(routeForCourse.id(), routeForReciprocal.id()))
				.keeper();
		assertEquals("expected score to dictate the winner of the merge", routeForCourse, merged);
		TestUtil.assertSize(3, this.routeService.completionService.completions(routeForCourse.id(), Filter.create()
				.filter()));
		assertEquals("the merge should have made an alias", routeForCourse, routeService.route(routeForReciprocal.id()));
		TestUtil.assertEmpty(	"merge should remove completions",
								routeTestUtil.getRouteCompletionRepository().children(routeForReciprocal.id()));
		assertEquals("expecting alias now", routeForCourse, this.routeService.route(routeForReciprocal.id()));

	}

	/**
	 * Provides a route for any activity requested, not just those that actually have made matches.
	 * This allows the system to mimic a route for any activity providing ease of use for users,
	 * partners and developers.
	 * 
	 * @throws ActivityImportException
	 * @throws EntityNotFoundException
	 * @throws CoursesNotRelatedException
	 * 
	 */
	@Test(expected = CoursesNotRelatedException.class)
	public void testRouteForActivityThatIsNotCourse() throws EntityNotFoundException, ActivityImportException,
			CoursesNotRelatedException {
		// no course yet..
		Activity activity = this.activityServiceTestUtil().createPersistedActivity();
		assertRouteForNonCourse(activity.id());
	}

	@Test
	public void testRouteCreated() throws BaseException {
		// runs the tests
		assertRouteCreated();
	}

	/**
	 * available for others to use a basically created route. 1 completion exists because the course
	 * completes the route.
	 * 
	 * @return
	 * @throws BaseException
	 */
	private Route assertRouteCreated() throws BaseException {
		Activity activity = this.importTestUtil().createPersistedActivity();
		Route route = this.routeService.routeCreated(activity);
		assertNotNull(route.start());
		assertNotNull(route.finish());
		assertEquals(activity.id(), route.courseId());
		assertNull("upon initial creation the route id is not available for assignment", route.mainId());
		assertFalse(route.isMainKnown());
		assertNull(route.mainId());
		assertNull(route.completionStats());
		assertNull(route.courseCompletionStats());
		assertNull(route.socialCompletionStats());
		assertNull(route.distanceCategories());
		assertTrue("the course should have been assigned as a completion", route.isStale());
		assertEquals("must be able to find by the course id", route, routeService.route(activity.id()));
		TestUtil.assertSize("confirm that the completion was created", 1, completionService.completions(route.id()));

		assertEquals(activity.boundingBox(), route.boundingBox());
		assertNull(route.alternateCount());
		assertEquals(activity.getTrackSummary().getDistance(), route.distance());
		assertNotNull(route.score());
		assertEquals(Route.DEFAULT_SCORE, route.score());

		return route;
	}

	@Test(expected = CoursesNotRelatedException.class)
	public void testRouteForActivityThatIsNotCourseNotYetImported() throws EntityNotFoundException,
			ActivityImportException, CoursesNotRelatedException {
		assertRouteForNonCourse(this.examples.line().id());
	}

	@Test(expected = ActivityImportException.class)
	public void testRouteForActivityThatWontImport() throws EntityNotFoundException, ActivityImportException,
			CoursesNotRelatedException {
		assertRouteForNonCourse(ActivityTestUtil.createApplicationActivityId());
	}

	/**
	 * @param activity
	 * @throws EntityNotFoundException
	 * @throws ActivityImportException
	 * @throws CoursesNotRelatedException
	 */
	private void assertRouteForNonCourse(ActivityId activityId) throws EntityNotFoundException,
			ActivityImportException, CoursesNotRelatedException {
		Route route = this.routeService.route(activityId);
		assertEquals("even not worthy routes deserve to mimic a route", activityId, route.courseId());
		assertNull("the not worthy route must not be persisted", route.id());
	}

	@Test
	public void testBoundsIncluded() {
		routeTestUtil.createPersistedRoutes();
		Filter filter = RouteDatastoreFilterBuilder.create().build();
		Routes found = routeService.routes(filter);
		assertNotNull(found.boundingBox());
	}

	@Test
	public void testRoutesFilterBounds() throws EntityNotFoundException {
		assertRoutesBoundsFilter(null);
	}

	/**
	 * @throws EntityNotFoundException
	 */
	private void assertRoutesBoundsFilter(
			Function<Pair<Route, RouteDatastoreFilterBuilder>, RouteDatastoreFilterBuilder> addFilterFunction)
			throws EntityNotFoundException {
		RouteDatastoreFilterBuilder filterBuilder = RouteDatastoreFilterBuilder.create();
		Activity course = this.examples.line();
		Route route = routeTestUtil.createPersistedRouteAndTrailheads(course);
		if (addFilterFunction != null) {
			filterBuilder = addFilterFunction.apply(Pair.of(route, filterBuilder));
		}
		Filter filter = filterBuilder.build();

		Trailhead trailhead = this.trailheadService.trailhead(route.start());

		final BoundingBox courseBounds = GeoCellBoundingBox.createGeoCell()
				.setGeoCells(course.getTrackSummary().getSpatialBuffer()).build();
		Boolean contains = GeoCellComparison.left(GeoCellUtil.generateGeoCells(courseBounds, false))
				.right(TrailheadUtil.geoCellFrom(trailhead)).contains();
		assertTrue(courseBounds + " deoesn't contain trailhead " + trailhead.area(), contains);
		Routes routes = this.routeService.routes(filter, courseBounds);
		TestUtil.assertContains(route, routes);
		TestUtil.assertSize(1, routes);
	}

	@Test
	public void testGetRoutes_Trailhead() {
		Route route = routeTestUtil.createPersistedRoute();
		Routes routes = routeService.routes(route.start());
		assertEquals(1, routes.size());
	}

	@Test
	public void testGetRoutesByWrongTrailhead() {
		Route route = routeTestUtil.createPersistedRoute();
		Routes routes = routeService.routes(route.finish());
		TestUtil.assertSize(0, routes);
	}

	@Test
	public void testSetScore() throws EntityNotFoundException {
		Route route = RouteServiceTestUtil.create().build().createPersistedRoute();
		final Integer score = route.getScore();
		assertNotNull("score is required to create", score);
		Integer updatedScore = score + TestUtil.generateRandomInt();
		Route updatedRoute = routeService.setScore(route.getId(), updatedScore);
		assertEquals(updatedScore, updatedRoute.getScore());
		Route found = routeService.route(route.getId());
		assertEquals(updatedScore, found.getScore());
	}

	/**
	 * Creates an alias on a route and then finds by that alias.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testAlias() throws EntityNotFoundException {
		RouteId alias = RouteTestUtils.routeId();
		// first, prove finding by alias is not possible
		try {
			routeService.route(alias);
			fail("what the?");
		} catch (Exception e) {
			// as expected...continue
		}
		Route route = RouteTestUtils.createRoute();

		Route withAlias = Route.mutate(route).addAliases(alias).build();
		Route storedWithAlias = routeTestUtil.store(withAlias);
		assertEquals(route, storedWithAlias);
		TestUtil.assertContains(alias, storedWithAlias.aliases());
		Route foundFromAlias = routeService.route(alias);
		assertEquals(storedWithAlias, foundFromAlias);

	}

	/**
	 * 
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testMergeTwoCandidates() throws EntityNotFoundException {
		Route keeper = this.routeTestUtil.createPersistedRoute();
		Route loser = this.routeTestUtil.createPersistedRoute();
		this.routeTestUtil.scoreForMerge(keeper, loser);

		Route merged = this.routeService.routesMerged(ids(Lists.newArrayList(loser, keeper))).keeper();
		assertEquals(keeper.id(), merged.id());

		// now, make sure keeper is still around.
		Route found = this.routeService.route(keeper.id());
		assertEquals(keeper.id(), found.id());
		TestUtil.assertContains(loser.id(), found.aliases());

		Route foundByAlias = this.routeService.route(loser.id());
		assertEquals("requesting by an alias should return the keeper", keeper, foundByAlias);
	}

	/**
	 * TN-826 this ensures routes not similar won't get merged.
	 * 
	 * @throws EntityNotFoundException
	 * @throws CoursesNotRelatedException
	 */

	@Test(expected = IllegalArgumentException.class)
	public void testMergeBadRoutes() throws EntityNotFoundException, CoursesNotRelatedException {
		Activity course = this.routeTestUtil.exampleTracksUtil().line();
		Activity bad = this.routeTestUtil.exampleTracksUtil().lineBefore();
		Route courseRoute = this.routeTestUtil.createPersistedRouteAndTrailheads(course);
		Route badRoute = this.routeTestUtil.createPersistedRouteAndTrailheads(bad);
		this.routeService.routesMerged(ImmutableList.of(courseRoute.id(), badRoute.id()));
	}

	/**
	 * Routes with the same course id should be the same route. TN-712
	 */
	@Test
	public void testMergeTwoRoutesWithSameCourse() {
		Activity course = this.examples.line();
		Route first = this.routeTestUtil.createPersistedRoute(course);
		Route second = this.routeTestUtil.createPersistedRoute(course);
		RouteMergeDetails routesMerged = this.routeService.routesMerged(Lists.newArrayList(first.id()));
		TestUtil.assertContainsAll(Lists.newArrayList(first.id(), second.id()), routesMerged.keeper().aliases());
	}

	/**
	 * Shows that when only entities are not found an error will be reported since it is likely
	 * something is not working correctly.
	 */
	@Test
	public void testMergeEntityNotFound() {
		RouteMergeDetails mergeDetails = this.routeService.routesMerged(Sets.newHashSet(RouteTestUtils.routeId()));
		TestUtil.assertEmpty("none should be found and reported", mergeDetails.mergeCandidates);
	}

	@Test
	public void testMergeEntityNotFoundWhenOneIs() throws EntityNotFoundException {
		Route keeper = this.routeTestUtil.routeUpdated();
		TestUtil.assertContainsOnly(keeper.id(), keeper.aliases());
		Route merged = this.routeService.routesMerged(Sets.newHashSet(keeper.id(), RouteTestUtils.routeId())).keeper();
		assertEquals(	"the keeper should have been found with no modification since the other is not found",
						keeper,
						merged);
		assertEquals("keeper is being modified", keeper.getEntityVersion(), merged.getEntityVersion());
	}

	@Test
	public void testMergeSoloDoesNothing() {
		Route keeper = this.routeTestUtil.routeUpdated();
		Route merged = routeService.routesMerged(IdentifierUtils.ids(keeper)).keeper();
		assertEquals(keeper, merged);
		assertEquals(keeper.getEntityVersion(), merged.getEntityVersion());
	}

	/**
	 * if the route has an alias and that alias is found as a route it should be merged.
	 * 
	 * @throws EntityNotFoundException
	 * @throws CoursesNotRelatedException
	 * 
	 */
	@Test
	public void testMergingSoloFindsLeftoverAlias() throws EntityNotFoundException, CoursesNotRelatedException {
		Route keeper = this.routeTestUtil.createPersistedRoute(this.examples.line());
		Route loser = this.routeTestUtil.createPersistedRoute(this.examples.lineReciprocal());
		this.routeTestUtil.scoreForMerge(keeper, loser);
		keeper = this.routeTestUtil.getRouteRepository().aliasesAdded(keeper.id(), Sets.newHashSet(loser.id()));
		assertEquals("the loser remains available", loser, this.routeService.route(loser.id()));
		Route merged = routeService.routesMerged(IdentifierUtils.ids(keeper)).keeper();
		assertEquals("after merge, alias should be respected", keeper, this.routeService.route(loser.id()));
		assertEquals(keeper, merged);
	}

	/**
	 * When an alias is referenced by two, the merge should find both even when only given one and
	 * merge them.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testMergeOfSharedAliasByAliasId() throws EntityNotFoundException {
		testMergeOfSharedAlias(1);
	}

	@Test
	public void testMergeOfSharedAliasByKeeperId() throws EntityNotFoundException {
		testMergeOfSharedAlias(2);
	}

	@Test
	public void testMergeOfSharedAliasByLoserId() throws EntityNotFoundException {
		testMergeOfSharedAlias(3);
	}

	public void testMergeOfSharedAlias(int i) throws EntityNotFoundException {

		Route keeper = this.routeTestUtil.createPersistedRoute();
		Route loser = this.routeTestUtil.createPersistedRoute();

		RouteId aliasId = RouteTestUtils.routeId();
		this.routeTestUtil.scoreForMerge(keeper, loser);
		final HashSet<RouteId> aliasIds = Sets.newHashSet(aliasId);

		final HashSet<RouteId> mergeIds = Sets.newHashSet();
		switch (i) {
			case 1:
				mergeIds.add(aliasId);
				break;
			case 2:
				mergeIds.add(keeper.id());
				break;
			case 3:
				mergeIds.add(loser.id());
				break;
			default:
				throw new InvalidChoiceException(i);
		}
		keeper = this.routeTestUtil.getRouteRepository().aliasesAdded(keeper.id(), aliasIds);
		loser = this.routeTestUtil.getRouteRepository().aliasesAdded(loser.id(), aliasIds);
		assertTrue(keeper.isStale());
		assertTrue(loser.isStale());

		// searching by routeId should post tasks
		Route routeForAlias = routeService.route(aliasId);
		assertEquals("even if multiple found, the high score should be provided", keeper, routeForAlias);
		assertTrue("should have been marked for update", routeService.route(keeper.id()).isStale());
		assertTrue("should have been marked for update", routeService.route(loser.id()).isStale());

		// task calls update, update calls merge, we are testing merge only for
		// simplicity
		Route updated = routeService.routesMerged(mergeIds).keeper();
		assertEquals("keeper should have been the recipient of the update", keeper, updated);
		assertEquals("loser should have been merged with keepr", keeper, this.routeService.route(loser.id()));
	}

	/**
	 * @return
	 */
	private ActivityServiceTestUtil activityTestUtil() {
		return importTestUtil().getActivityServiceTestUtil();
	}

	private ActivityImportService importService() {
		return importTestUtil().getActivityImportService();
	}

	/**
	 * @return
	 */
	private ActivityImportServiceTestUtil importTestUtil() {
		return routeTestUtil.getTimingRelationServiceTestUtil().getImportTestUtil();
	}

	@Test
	public void testWorthy() throws EntityNotFoundException, CourseNotWorthyException, CoursesNotRelatedException {
		Route route = worthyRoute();
		assertWorthy(route);
	}

	@Test
	public void testWorthyHandlesMissingTrailhead() throws EntityNotFoundException, CourseNotWorthyException,
			CoursesNotRelatedException {
		Route route = worthyRoute();
		assertWorthy(route);
		// Now lets simulate a missing trailhead. It should gracefully recover.
		trailheadTestUtil.deleteAll();
		assertWorthy(this.routeTestUtil.routeUpdated(route.id()).updated());

	}

	/**
	 * Provides the best application when the convenience of already having the course associated
	 * from the application.
	 * 
	 * @throws CoursesNotRelatedException
	 */
	@Test
	public void testBestActivityForApplicationRouteCourse() throws EntityNotFoundException, CourseNotWorthyException,
			CoursesNotRelatedException {
		Route worthyRoute = worthyRoute();

		final ApplicationKey expectedKey = KnownApplication.UNKNOWN.key;
		Activity courseRepresentative = this.routeService.courseRepresentative(worthyRoute.id(), expectedKey);
		assertNotNull(courseRepresentative);
		assertEquals(expectedKey, courseRepresentative.id().getApplicationKey());
	}

	/**
	 * Searches the completions for a matching course.
	 * 
	 * @throws EntityNotFoundException
	 * @throws CourseNotWorthyException
	 * @throws CoursesNotRelatedException
	 */
	@Test
	public void testBestActivityForApplicationNotRouteCourse() throws EntityNotFoundException,
			CourseNotWorthyException, CoursesNotRelatedException {
		final ApplicationKey expectedKey = KnownApplication.UNKNOWN.key;
		Activity decoy = this.activityServiceTestUtil().createPersistedActivity();
		activityServiceTestUtil().createPersistedActivity(decoy.id());
		Route route = worthyRoute();
		this.routeTestUtil.getRouteRepository().update(route.id()).course(decoy).build();

		Activity courseRepresentative = this.routeService.courseRepresentative(route.id(), expectedKey);
		assertNotNull(courseRepresentative);
		assertEquals(expectedKey, courseRepresentative.id().getApplicationKey());
	}

	/**
	 * Validates that {@link IfScoreIsSignficant} is working properly keeping low scoring routes out
	 * of the results when sorting by score.
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	@Ignore("TN-804 and querying search service has done away with this")
	public void testLowScoringRoutesNotIndex() throws EntityNotFoundException {
		Route included = this.routeTestUtil.createPersistedRoute();
		Route excluded = this.routeTestUtil.createPersistedRoute();
		included = routeService.setScore(included.id(), Route.DEFAULT_SCORE + 1);
		excluded = routeService.setScore(excluded.id(), Route.DEFAULT_SCORE);
		assertEquals(Route.DEFAULT_SCORE, excluded.score());
		Filter filter = RouteDatastoreFilterBuilder.create().orderByScore().build();
		Routes routes = routeService.routes(filter);
		TestUtil.assertContainsOnly(included, routes);

		TestUtil.assertSize("not sorting by score should discover excluded",
							2,
							routeService.routes(RouteDatastoreFilterBuilder.create().orderByNone().filter()));
	}

	/**
	 * @return
	 */
	private ActivityServiceTestUtil activityServiceTestUtil() {
		return this.routeTestUtil.getTimingRelationServiceTestUtil().getImportTestUtil().getActivityServiceTestUtil();
	}

	@Test(expected = EntityNotFoundException.class)
	public void testBestActivityForApplicationNotFound() throws EntityNotFoundException, CourseNotWorthyException,
			CoursesNotRelatedException {
		Route worthyRoute = worthyRoute();
		this.routeService.courseRepresentative(worthyRoute.id(), KnownApplication.TWO_PEAK_ACTIVITIES.key);
	}

	@Test
	public void testWorthyHandlesIncorrectTrailhead() throws EntityNotFoundException, CourseNotWorthyException,
			CoursesNotRelatedException {
		Route route = worthyRoute();
		TrailheadId expectedStart = route.start();
		TrailheadId expectedFinish = route.finish();
		assertWorthy(route);
		// Now let's give the incorrect trailheads to the route
		{
			Trailhead falseStart = this.trailheadTestUtil.persisted();
			this.routeTestUtil.getRouteRepository().setStart(route.id(), falseStart.id());
			Trailhead falseFinish = this.trailheadTestUtil.persisted();
			Route wrongTrailheads = this.routeTestUtil.getRouteRepository().setFinish(route.id(), falseFinish.id());
			assertEquals("just checking that false start stuck around", falseStart.id(), wrongTrailheads.start());
		}

		Route corrected = routeTestUtil.routeUpdated(route.id()).updated();
		assertWorthy(corrected);
		assertEquals("the false start was not corrected", expectedStart, corrected.start());
		assertEquals("the false finish was not corrected", expectedFinish, corrected.finish());
	}

	@Test
	public void testDevicePersistedAndRetreived() throws EntityNotFoundException {
		Activity line = this.examples.line();
		ApplicationKey lineDevice = DeviceTestUtil.GOOD_DEVICE;
		line = activityTestUtil().setDevice(line.id(), lineDevice);
		ActivityService activityService = activityServiceTestUtil().getActivityService();

		Activity activity = activityService.activity(line.getId());
		assertNotNull(activity.getDevice());
		assertEquals(line.getDevice().getApplication(), activity.getDevice().getApplication());
	}

	@Test
	public void testDeviceCourseChosen() throws EntityNotFoundException, CoursesNotRelatedException {
		Activity line = this.examples.line();
		final ApplicationKey lineDevice = DeviceTestUtil.GOOD_DEVICE;
		line = activityTestUtil().setDevice(line.id(), lineDevice);
		Activity reciprocal = this.examples.lineReciprocal();
		final ApplicationKey reciprocalDevice = DeviceTestUtil.GREAT_DEVICE;
		reciprocal = activityTestUtil().setDevice(reciprocal.id(), reciprocalDevice);

		RouteUpdateDetails routeUpdated = this.routeTestUtil.routeUpdated(line.id(), reciprocal.id(), this.examples
				.beforeAndAfter().id());
		final DeviceSignalActivitySelector courseSelector = routeUpdated.courseSelector();
		assertNotNull("the course selector was not assigned", courseSelector);
		assertEquals("reciprocal was given the better device", reciprocal.id(), courseSelector.best().id());
		TestUtil.assertContainsOnly("only courses should be in there",
									ids(courseSelector.bestFirst()),
									line.id(),
									reciprocal.id());
		CategoricalStatsCounter<ApplicationKeyStats, ApplicationKey> deviceTypeStats = routeUpdated.courseSelector()
				.deviceTypeStats();
		TestUtil.assertSize(2, deviceTypeStats.stats());
		// just eye-balling these numbers.
		assertEquals(I(1), deviceTypeStats.stats(lineDevice).count());
		assertEquals(I(1), deviceTypeStats.stats(reciprocalDevice).count());
		SortedSet<ApplicationKeyStats> statsFromRoute = routeUpdated.updated().courseCompletionStats()
				.deviceTypeStats();
		assertNotNull(statsFromRoute);
	}

	private void assertWorthy(Route worthyRoute) throws EntityNotFoundException {
		assertNotNull("the leaderboard should be worthy", worthyRoute);
		Route found = routeService.route(worthyRoute.id());
		assertNotNull(found);
		assertEquals(worthyRoute, found);
		Trailhead start = trailheadService.trailhead(worthyRoute.start());
		Trailhead finish = trailheadService.trailhead(worthyRoute.finish());
		assertTrailhead(start);
		assertTrailhead(finish);
	}

	private void assertTrailhead(Trailhead th) throws EntityNotFoundException {
		Trailhead startFound = trailheadService.trailhead(th.id());
		assertEquals(th, startFound);
		assertEquals(th.score() + " was not equal to " + startFound.score(), th.score(), startFound.score());
		// assertNotNull(th.name());
	}

	/**
	 * This does the majority of main testing because setting up a main can be a pain. If it gets
	 * too ugly we can create a specialized service test that does a cleaner setup and isolated
	 * testing.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testAlternateOrganizer() throws EntityNotFoundException {

		Activity line = this.examples.line();
		Trailhead start = this.routeTestUtil.getTrailheadServiceTestUtil().persisted(line.getTrackSummary().getStart()
				.getLocation());
		Trailhead finish = this.routeTestUtil.getTrailheadServiceTestUtil().persisted(line.getTrackSummary()
				.getFinish().getLocation());

		Route group1Route1 = Route.create().course(line).start(start).finish(finish)
				.score(RouteTestUtils.scoreWorthy()).build();
		Activity line2 = this.examples.line();
		Route group1Route2 = Route.create().course(line2).start(start).finish(finish)
				.score(RouteTestUtils.scoreWorthy()).build();
		Activity deviation = this.examples.deviation();
		Route group2SoloRoute = Route.create().course(deviation).start(start).finish(finish)
				.score(RouteTestUtils.scoreWorthy()).build();
		group1Route1 = routeTestUtil.store(group1Route1);
		group1Route2 = routeTestUtil.store(group1Route2);
		group2SoloRoute = routeTestUtil.store(group2SoloRoute);

		List<Routes> mainGroups = new ArrayList<Routes>(routeService.alternatesOrganized(start.id()).groups());
		TestUtil.assertSize("expecting group1 with 2, group2 solo", 2, mainGroups);
		// make sure we hit the datastore because we aren't managing scores.
		Routes group1 = routeService.routesForMain(group1Route1.id(), RouteDatastoreFilterBuilder.create().build());
		TestUtil.assertSize(2, group1);
		TestUtil.assertContains(group1Route1, group1);
		TestUtil.assertContains(group1Route2, group1);
		Iterator<Route> group1Iterator = group1.iterator();
		// verify main for group1
		Route group1Main = group1Iterator.next();
		assertTrue("is main", group1Main.isMain());
		assertEquals("main id", group1Main.id(), group1Main.mainId());
		assertEquals("alternate count", 1, group1Main.alternateCount().intValue());

		// verify alternate
		Route group1Alternate = group1Iterator.next();
		assertNull("alternates don't have count", group1Alternate.alternateCount());
		assertFalse("alternate is not a main", group1Alternate.isMain());
		assertEquals("wrong main id", group1Main.id(), group1Alternate.mainId());

		// main 2 is alone so it is a main
		Route main2AfterOrganization = routeService.route(group2SoloRoute.id());
		assertEquals("main with no alternates deserves a zero", new Integer(0), main2AfterOrganization.alternateCount());
		assertFalse("alternate is main", group1Alternate.isMain());
		assertTrue("main2 is not main", main2AfterOrganization.isMain());
		final int expectedNumberOfGroups = 2;
		TestUtil.assertSize(expectedNumberOfGroups, mainGroups);

		// searching for the groups should find the same as the update produced
		Filter filter = RouteDatastoreFilterBuilder.create().build();
		FilteredResponse<Routes> foundGroups = routeService.routesGroupedByMain(start.id(), filter);
		TestUtil.assertCollectionEquals(mainGroups, foundGroups.getItems());

		// search for mains by bbox
		GeoCellBoundingBox mainBounds = GeoCellBoundingBox.createGeoCell()
				.setGeoCells(line.getTrackSummary().getSpatialBuffer()).build();
		{

			Filter mainsBoundsFilter = RouteDatastoreFilterBuilder.create().setBounds(mainBounds.getBounds())
					.mains(true).build();
			Routes mainsByBox = routeService.routes(mainsBoundsFilter);

			TestUtil.assertContains(group1Main, mainsByBox.all());
			TestUtil.assertContains(group2SoloRoute, mainsByBox.all());
			TestUtil.assertDoesntContain(group1Alternate, mainsByBox.all());
		}

		{
			Filter allBoundsFilter = RouteDatastoreFilterBuilder.create().setBounds(mainBounds.getBounds()).build();
			Routes allByBox = routeService.routes(allBoundsFilter);
			TestUtil.assertContains(group1Route1, allByBox.all());
			TestUtil.assertContains(group1Route2, allByBox.all());
			TestUtil.assertContains(group2SoloRoute, allByBox.all());
		}
		// search for all by bbox
	}

	@Test(expected = EntityNotFoundException.class)
	public void testAlternatesNotFound() throws EntityNotFoundException {
		routeService.routesForMain(RouteTestUtils.routeId(), Filter.create().filter());
	}

	/**
	 * The alternates method provides routes ordered by score. Zero scored routes won't be found
	 * because they are not added to the search index.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testAlternateForNoScoringRoute() throws EntityNotFoundException {
		Route route = this.routeTestUtil.createPersistedRoute();
		route = this.routeService.setScore(route.id(), Route.DEFAULT_SCORE);
		assertEquals(I(0), route.score());
		Routes routesForMain = this.routeService.routesForMain(route.id(), Filter.create().build());
		TestUtil.assertEmpty("zero scoring routes aren't indexed in public searches", routesForMain);
	}

	@Test
	public void testRoutesByBounds() {
		Activity line = this.examples.line();
		Activity beforeAndAfter = this.examples.beforeAndAfter();
		Route small = this.routeTestUtil.createPersistedRouteAndTrailheads(line);
		Route big = this.routeTestUtil.createPersistedRouteAndTrailheads(beforeAndAfter);
		Filter filter = RouteDatastoreFilterBuilder.create().build();
		final Length areaRadius = MeasurementUtil.createLengthInMeters(100);
		BoundingBox areaAtStartOfBig = BoundingBoxUtil.boundedBy(ExampleTracks.P_MINUS_1.getLocation(), areaRadius);
		Routes routesAtEndOfBig = routeService.routes(filter, areaAtStartOfBig);
		TestUtil.assertContains(big, routesAtEndOfBig);
		TestUtil.assertSize(1, routesAtEndOfBig);
		BoundingBox areaAtStartOfSmall = BoundingBoxUtil.boundedBy(ExampleTracks.P1.getLocation(), areaRadius);

		final Routes routesAtStartOfSmall = routeService.routes(filter, areaAtStartOfSmall);
		TestUtil.assertSize(1, routesAtStartOfSmall);
		TestUtil.assertContains(small, routesAtStartOfSmall);
		// now look for both given a big area
		final BoundingBox boundingBox = GeoCellBoundingBox.createGeoCell()
				.setGeoCells(beforeAndAfter.getTrackSummary().getSpatialBuffer()).build();
		TestUtil.assertSize(2, routeService.routes(filter, boundingBox));
	}

	@Test
	public void testTrailheadScore() throws EntityNotFoundException, CourseNotWorthyException,
			CoursesNotRelatedException {
		Route worthyRoute = worthyRoute();
		Trailhead start = this.trailheadService.trailhead(worthyRoute.start());
		Routes routes = Routes.create().add(worthyRoute).build();
		RouteAwareTrailheadScoreCalculator calculator = routeService.trailheadUpdated(start, routes).scoreCalculator();
		assertEquals("calculator disagrees with route", worthyRoute.score(), calculator.score());
		assertEquals(	"trailhead score not persisted",
						worthyRoute.score(),
						trailheadService.trailhead(worthyRoute.start()).score());
	}

	/**
	 * Trailheads with no routes should still receive the score of their number of activities.
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	public void testTrailheadScoreOnlyActivities() throws EntityNotFoundException {
		Trailhead persisted = this.routeTestUtil.getTrailheadServiceTestUtil().persisted();
		assertNotNull("test requires start stats", persisted.activityStartStats());
		RouteTrailheadOrganizer organizer = routeService.trailheadUpdated(persisted, Routes.create().build());
		TestUtil.assertGreaterThan(Trailhead.DEFAULT_SCORE + 1, organizer.scoreCalculator().score());
	}

	@Test
	@Ignore("TN-804 made personal routes and search service.  query by activity type stats is no longer possible")
	public void testActivityTypeStats() throws EntityNotFoundException, CourseNotWorthyException,
			ActivityImportException, CoursesNotRelatedException {
		final ActivityId courseId = this.examples.line().id();
		// this ensures that at least one activity type of interest is available
		// for distance
		// categories.
		// don't use bike because bike can switch to MTB or road if they
		// override
		final ActivityType courseActivityType = ActivityType.RUN;
		Activity course = this.activityServiceTestUtil().setActivityType(courseId, courseActivityType);
		assertEquals("activity type didn't get assigned", courseActivityType, course.getActivityType());
		this.routeTestUtil.importAndRelateCompletions(courseId, this.examples.lineTimeShift().id(), this.examples
				.beforeAndAfter().id(), this.examples.lineLonger().id(), this.examples.loopDouble().id(), this.examples
				.loop().id());
		Route route = this.routeTestUtil.route(courseId, this.examples.lineTimeShift().id(), this.examples
				.beforeAndAfter().id(), this.examples.lineLonger().id(), this.examples.loopDouble().id(), this.examples
				.loop().id());

		// find the route again to make sure we get the latest storage
		// information
		route = routeTestUtil.routeUpdated(route.id()).updated();
		final SortedSet<ActivityTypeStatistic> activityTypeStats = route.completionStats().activityTypeStats();
		assertNotNull(activityTypeStats);
		TestUtil.assertSize(Range.atLeast(2), activityTypeStats);
		// TN-576 store only significant amounts
		Range<Ratio> minPercent = Range.atLeast(ActivityUtil.MIN_ACTIVITY_TYPE_PERCENT);
		boolean courseActivityTypeFound = false;
		for (ActivityTypeStatistic stats : activityTypeStats) {
			assertRange(stats.toString(), minPercent, stats.percent());
			TestUtil.assertNotEquals("hikes are walks", ActivityType.HIKE, stats.category());
			courseActivityTypeFound = courseActivityTypeFound || stats.category().equals(courseActivityType);
		}
		assertTrue("course activity type didn't make the list", courseActivityTypeFound);
		final List<ActivityTypeDistanceCategory> distanceCategories = route.distanceCategories();
		// distance categories can be null if empty.
		assertNotNull("distance categories should be assigned", distanceCategories);
		// bike was assigned to line earlier. Line is long...it should match
		// century.
		TestUtil.assertContains(ActivityTypeDistanceCategory.valueOf(courseActivityType, ExampleTracks.LINE_LENGTH),
								distanceCategories);
		TestUtil.assertNotEmpty(distanceCategories);
		for (ActivityTypeDistanceCategory activityTypeDistanceCategory : distanceCategories) {
			Routes routes = routeService.routes(RouteDatastoreFilterBuilder.create()
					.distanceCategory(activityTypeDistanceCategory).build());
			TestUtil.assertContains(route, routes);
		}

	}

	@Test
	public void testTrailheadMerge() throws EntityNotFoundException, CourseNotWorthyException,
			CoursesNotRelatedException {
		final Route worthyRoute = worthyRoute();
		final TrailheadId replaced = worthyRoute.start();
		// the replacment must be nearby or else nearby logic (alias) won't work
		final Trailhead replacement = trailheadTestUtil.persisted(TrailheadTestUtil.createNearbyTrailhead(replaced)
				.location());
		routeService.merge(replacement.id(), Sets.newHashSet(replaced));
		final Route updatedRoute = routeService.route(worthyRoute.id());
		assertEquals("replacement not reflected by route start", replacement.id(), updatedRoute.start());
		final TrailheadId finish = worthyRoute.finish();
		assertEquals("finish got replaced?", finish, updatedRoute.finish());

		// what the heck, update the finish too
		routeService.merge(replacement.id(), Sets.newHashSet(finish));
		final Route updatedFinishRoute = routeService.route(worthyRoute.id());
		assertEquals("finish didn't get replaced", replacement.id(), updatedFinishRoute.finish());
		assertEquals("start replacment got changed", replacement.id(), updatedFinishRoute.start());

		try {
			trailheadService.trailhead(replaced);
			fail(replaced + " SHould have been deleted");
		} catch (EntityNotFoundException e) {
			// this is good. we wanted it to be gone
			assertEquals("the replacement should remain", replacement, trailheadService.trailhead(replacement.id()));
		}

		assertEquals(	"searching for old should provide the new replacmeent",
						replacement,
						trailheadService.nearest(replaced));
		// now, we should still be able to find the route using the old
		// trailheadId
		final Routes routes = routeService.routes(replaced);
		TestUtil.assertSize(1, routes);
		TestUtil.assertContains(worthyRoute, routes);
	}

	/**
	 * Just makes sure the mechanics work. Leaves the logic up to the
	 * {@link RouteTrailheadChooserUnitTest}.
	 * 
	 * @throws CourseNotWorthyException
	 * @throws EntityNotFoundException
	 * @throws CoursesNotRelatedException
	 */
	@Test
	public void testTrailheadsOrganized() throws EntityNotFoundException, CourseNotWorthyException,
			CoursesNotRelatedException {
		Route worthyRoute = worthyRoute();
		// make sure worthy route is the winner.
		this.routeTestUtil.getRouteRepository().setScore(worthyRoute.id(), worthyRoute.score() * worthyRoute.score());

		Trailhead trailhead = trailheadService.trailhead(worthyRoute.start());
		// create a bogus trailhead and route that should be out of bounds
		Activity outOfBoundsCourse = this.routeTestUtil.exampleTracksUtil().returnLineOnly();
		Route outOfBoundsRoute = this.routeTestUtil.createPersistedRouteAndTrailheads(outOfBoundsCourse);
		Trailhead outOfBounds = trailheadService.trailhead(outOfBoundsRoute.start());
		TestUtil.assertNotEquals("make sure these trailheads are different", trailhead, outOfBounds);
		TestUtil.assertNotEquals(worthyRoute, outOfBoundsRoute);

		final BoundingBox boundingBox = outOfBoundsCourse.getTrackSummary().getBoundingBoxForSpatialBuffer();
		BoundingBox searchArea = BoundingBoxUtil.boundedBy(trailhead.location(), boundingBox.getDiagonal());

		// create another bogus trailhead that should be merged.
		GeoCoordinate withinRadius = GeoMath.coordinateFrom(trailhead.location(),
															MeasurementUtil.EAST,
															TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS);
		Trailhead mergeCandidate = Trailhead.create().location(withinRadius).name(new StringMessage("mergeCandidate"))
				.build();
		trailheadService.store(mergeCandidate);

		RouteTrailheadChooser chooser = routeService.trailheadsOrganized(searchArea);

		// verify those expected to find are found
		TestUtil.assertContains(trailhead, chooser.trailheads());
		TestUtil.assertContains(outOfBounds, chooser.trailheads());
		TestUtil.assertContains(mergeCandidate, chooser.trailheads());
		TestUtil.assertContains(worthyRoute, chooser.routes());
		TestUtil.assertContains(outOfBoundsRoute, chooser.routes());

		// make sure the worthy route is good
		Map<Trailhead, Routes> trailheadRelations = chooser.trailheadRelations();
		TestUtil.assertSize(1, trailheadRelations.keySet());
		TestUtil.assertContains(trailhead, trailheadRelations.keySet());
		Routes routes = trailheadRelations.get(trailhead);
		TestUtil.assertSize(1, routes);
		assertEquals(worthyRoute, Iterables.getFirst(routes, null));

		// merge should have been found
		Set<TrailheadId> mergeCandidates = chooser.mergeCandidates().get(trailhead.id());
		TestUtil.assertContains(mergeCandidate.id(), mergeCandidates);
		TestUtil.assertSize(1, chooser.mergeCandidates().keySet());

		Set<RouteProximityRelator> actualOutOfBounds = chooser.outOfBounds().keySet();
		TestUtil.assertSize(1, actualOutOfBounds);
		RouteProximityRelator outOfBoundsRelator = actualOutOfBounds.iterator().next();
		assertEquals(outOfBoundsRoute, outOfBoundsRelator.bestRelative());

		// now go by RouteId and make sure similar ScreeningResults
		RouteTrailheadChooser trailheadsOrganizedById = routeService.trailheadsOrganized(worthyRoute.start());
		assertEquals(	"organizing by id found different results",
						worthyRoute,
						Iterables.getFirst(trailheadsOrganizedById.trailheadRelations().get(trailhead), null));

		Trailhead finalCheckTrailhead = routeService.trailhead(worthyRoute);
		assertEquals(trailhead, finalCheckTrailhead);
		TestUtil.assertGreaterThan(Trailhead.DEFAULT_SCORE, finalCheckTrailhead.score());
		assertNotNull(finalCheckTrailhead.name());
		TestUtil.assertNotEquals(TrailheadMessage.UNKNOWN, finalCheckTrailhead.name());
	}

	/**
	 * Provides a route with two courses and another completion for a total of 3 completions, the
	 * line being the main course of the route returned.
	 * 
	 * @return
	 * @throws EntityNotFoundException
	 * @throws CourseNotWorthyException
	 * @throws CoursesNotRelatedException
	 */
	private Route worthyRoute() throws EntityNotFoundException, CourseNotWorthyException, CoursesNotRelatedException {
		return this.routeTestUtil.route(this.examples.line().id(), this.examples.lineReciprocal().id(), this.examples
				.lineBefore().id());
	}

	/**
	 * This ensures the worthy route provides a useful and complete route. Numbers are hard-coded to
	 * make you think about changing any of those numbers.
	 * 
	 * @throws EntityNotFoundException
	 * @throws CourseNotWorthyException
	 * @throws CoursesNotRelatedException
	 */
	@Test
	public void testWorthyRoute() throws EntityNotFoundException, CourseNotWorthyException, CoursesNotRelatedException {
		Route worthyRoute = worthyRoute();
		Iterable<Activity> courses = this.completionService.courses(worthyRoute.id());
		final Integer expectedSize = 2;
		TestUtil.assertSize(expectedSize, courses);
		assertEquals(expectedSize, worthyRoute.courseCompletionStats().total());
		TestUtil.assertRange(Range.greaterThan(Route.DEFAULT_SCORE), worthyRoute.score());
		TestUtil.assertSize(worthyRoute.completionStats().total(),
							routeService.completionService.completions(worthyRoute.id()));
	}

	@Test
	public void testCompletionsUpdatedForCourse() throws EntityNotFoundException {
		// only import timings so service can find the relationships
		final ActivityId courseId = this.examples.line().id();
		final ActivityId completion1 = this.examples.lineBefore().id();
		final ActivityId completion2 = this.examples.beforeAndAfter().id();
		this.routeTestUtil.getTimingRelationServiceTestUtil().importAndRelate(	courseId,
																				completion1,
																				completion2,
																				this.examples.deviation().id());

		Collection<RouteCompletion> completionsUpdated = this.routeService.completionsUpdated(courseId);
		TestUtil.assertSize("course isn't included in the completion creation results since it is the responsibility of another method",
							2,
							completionsUpdated);
		Iterable<ActivityId> activityIds = RouteCompletionUtil.activityIds(completionsUpdated);
		TestUtil.assertContains(completion1, activityIds);
		TestUtil.assertContains(completion2, activityIds);
		// only finds those completions where the activity id is participating.
		RouteCompletions completionsForCourseFound = this.completionService.completions(courseId,
																						RouteCompletionFilterBuilder
																								.create().build());
		TestUtil.assertSize("course completion wasn't created", 1, completionsForCourseFound);
		Iterable<ActivityId> activityIdsFound = RouteCompletionUtil.activityIds(completionsForCourseFound);
		TestUtil.assertContains(courseId, activityIdsFound);
	}

	/**
	 * Creates a leaderboard with two activities, one completes the other. Then the third that
	 * completes both of them is provided to the leaderboard smart search.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testCompletionsFromLeaderboardsCompletesBoth() throws BaseException {
		// one timing will be found
		final ActivityId lineId = this.examples.line().id();
		final ActivityId lineLongerId = this.examples.lineLonger().id();
		Pair<? extends List<ActivityTiming>, ? extends List<RouteCompletion>> imported = this.routeTestUtil
				.importAndRelateCompletions(lineId, lineLongerId);
		List<RouteCompletion> completions = imported.getRight();
		TestUtil.assertSize("just the line was completed", 1, completions);

		// now, import another and hit the leaderboard course analysis method
		// directly for
		// relationships
		// this guy completes both of the above, however, only line has been a
		// confirmed
		// leaderboard.
		Activity beforeAndAfterActivity = this.examples.beforeAndAfter();
		ScreeningResults completed = routeService.completionCandidatesCourseScreener(beforeAndAfterActivity);
		TestUtil.assertEmpty("nothing should exist yet", completed.existing());
		assertEquals("expecting to complete line only since lineLonger is not yet a course ", 1, completed.passers()
				.size());
		assertEquals("nothing should have failed ", 0, completed.failers().size());
	}

	@Test
	public void testCourseScreeningTimingsExist() throws EntityNotFoundException {
		final ActivityId lineId = this.examples.line().id();
		final ActivityId lineLongerId = this.examples.lineLonger().id();
		Pair<? extends List<ActivityTiming>, ? extends List<RouteCompletion>> imported = this.routeTestUtil
				.importAndRelateCompletions(lineId, lineLongerId);
		ScreeningResults courseScreener = routeService.completionCandidatesCourseScreener(lineLongerId);
		TestUtil.assertContainsOnly(imported.getLeft().iterator().next().id(), courseScreener.existing());
		TestUtil.assertEmpty(courseScreener.getPassingScreeners());
	}

	/**
	 * Two activiites are created and one timing where the longest completes the shortest. Then we
	 * add one in the middle to make sure the one that it completes is found.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testCompletionsFromLeaderboardsInBetween() throws BaseException {
		Pair<? extends List<ActivityTiming>, ? extends List<RouteCompletion>> results = this.routeTestUtil
				.importAndRelateCompletions(this.examples.line().id(), this.examples.beforeAndAfter().id());

		TestUtil.assertSize(1, results.getRight());

		Activity between = this.examples.lineLonger();
		ScreeningResults completed = this.routeService.completionCandidatesCourseScreener(between);
		assertEquals(1, completed.passers().size());
		assertEquals(0, completed.failers().size());
		QueueTestUtil.assertQueueContains(timingServiceUtil.getActivityTimingRelationService()
				.getTimingCourseAttemptQueue(), 1, between.id().toString());
	}

	@Test
	public void testPersonsForRoute() throws BaseException {
		RouteCompletion completion = routeTestUtil.routeCompletionRelated();
		RouteCompletion completionForAnotherRoute = routeTestUtil.routeCompletionRelated();
		TestUtil.assertNotEquals(completionForAnotherRoute.routeId(), completion.routeId());
		Persons personsForCompletion1 = routeService.persons(completion.routeId(), Filter.create().filter());
		TestUtil.assertContainsOnly(completion.personId(), IdentifierUtils.ids(personsForCompletion1));
		TestUtil.assertNotEmpty(personsForCompletion1.support());
	}

	@Test
	public void testAliasesNotWorthKeeping() throws EntityNotFoundException {

		// setting up our own util so we can control the number of views
		// required
		final Builder testUtilBuilder = RouteServiceTestUtil.create();
		testUtilBuilder.options().minNumberOfViewsToKeepAlias(1);
		RouteServiceTestUtil util = testUtilBuilder.build();

		Route keeper = util.createPersistedRoute();
		// still exists so it should remain
		Route loser = util.createPersistedRoute();
		util.scoreForMerge(keeper, loser);

		// orphaned completions is worth keeping the alias
		RouteCompletion routeCompletion = util.routeCompletion();
		RouteId missingParentId = routeCompletion.getRouteId();
		util.getRouteRepository().delete(missingParentId);

		// doesn't exist, but existed long enough to be bookmarked
		RouteId enoughViewsToKeep = RouteTestUtils.routeId();
		util.getRouteService().viewed(enoughViewsToKeep);

		// no completions, no views, doesn't exist
		RouteId notWorthKeeping = RouteTestUtils.routeId();

		util.getRouteRepository().aliasesAdded(	keeper.id(),
												Sets.newHashSet(missingParentId, notWorthKeeping, enoughViewsToKeep));

		Route merged = util.getRouteService().routesMerged(Sets.newHashSet(keeper.id(), loser.id())).keeper();
		TestUtil.assertContains("the view count didn't keep it around", enoughViewsToKeep, merged.aliases());
		TestUtil.assertContains("should have kept the parent of the orphans", missingParentId, merged.aliases());
		TestUtil.assertContains("this guy is just getting merged...don't remove so quickly",
								loser.id(),
								merged.aliases());
		TestUtil.assertDoesntContain(notWorthKeeping, merged.aliases());

	}

	@Test
	public void testSocialGroupEvents() throws EntityNotFoundException {
		Route route = this.routeTestUtil.createPersistedRoute();
		RouteEventServiceTestUtil eventServiceTestUtil = RouteEventServiceTestUtil.create()
				.routeTestUtil(routeTestUtil).build();
		RouteEvent routeEvent = eventServiceTestUtil.routeEventBuilder(route).socialGroupId(null).build();
		final ArrayList<RouteCompletion> completions = Lists.newArrayList();
		final RouteEventId routeEventId = routeEvent.id();
		for (int i = 0; i < 3; i++) {
			RouteCompletion routeCompletion = routeTestUtil.routeCompletion(route);
			routeCompletion = routeTestUtil.mutate(routeCompletion.id()).routeEventId(routeEventId).build();
			completions.add(routeCompletion);
		}
		SocialGroup socialGroup = this.routeService.eventCompletionsSocialGroupUpdated(routeEventId);
		Integer expectedMembershipCount = completions.size();
		final SocialGroupId socialGroupId = socialGroup.id();
		assertEquals(	"membership count doesn't reflect the completions",
						expectedMembershipCount,
						socialGroup.membershipCount());
		assertEquals(	"group id didn't get updated in event",
						socialGroupId,
						eventServiceTestUtil.service().routeEvent(routeEventId).socialGroupId());
		TestUtil.assertSize(expectedMembershipCount, this.routeTestUtil.getPersonTestUtil().membershipService()
				.memberIds(socialGroupId));
		SocialGroups foundSocialGroupsForRoute = routeService.eventSocialGroups(route.id(), Filter.create().filter());
		TestUtil.assertContainsOnly(socialGroup, foundSocialGroupsForRoute);

		// now delete the route and watch the social groups disappear
		this.routeService.routeDeleted(route.id());

		try {
			routeService.eventService.routeEvent(routeEventId);
			fail(routeEventId + " wasn't deleted for " + route.id());
		} catch (EntityNotFoundException e) {
			try {
				routeService.socialGroupService.groupService().socialGroup(socialGroupId);
				fail(socialGroupId + " social group failed to be deleted with " + route.id());
			} catch (EntityNotFoundException e1) {
				// good
			}
		}

	}

	@Test
	public void testSocialGroupsRouteCompletions() throws EntityNotFoundException {
		Route route = this.routeTestUtil.createPersistedRoute();
		// just test one activity type...testing more gets complicated and messy
		ActivityType activityType = route.completionStats().activityTypeStats().first().category();
		RouteCompletion routeCompletion1 = RouteCompletionTestUtil.create().route(route).activityType(activityType)
				.build();
		RouteCompletion routeCompletion2 = RouteCompletionTestUtil.create().route(route).activityType(activityType)
				.build();
		RouteCompletion routeCompletion3 = RouteCompletionTestUtil.create().route(route).activityType(activityType)
				.build();

		final ArrayList<RouteCompletion> completions = Lists.newArrayList(	routeCompletion1,
																			routeCompletion2,
																			routeCompletion3);
		SocialGroups groups = this.routeService.completionSocialGroupMembershipsUpdated(route, completions)
				.socialGroupsUpdated();
		TestUtil.assertSize(1, groups);
		TestUtil.assertNotEmpty(groups);
		SocialGroups found = this.routeService.completionSocialGroups(route.id());
		TestUtil.assertIterablesEquals("found", groups, found);
		assertEquals("wrong membership count", completions.size(), groups.iterator().next().membershipCount()
				.intValue());
		Route updatedRoute = this.routeService.route(route.id());
		TestUtil.assertIterablesEquals(	"route must store ids",
										updatedRoute.socialGroupIds(),
										IdentifierUtils.ids(groups));

		// FIXME:test this better...this test only creates one route so no relations are made
		// now test the RouteRelator using the social groups
		Collection<RouteRelator> relatorsUpdated = this.routeService.relatorsUpdated(route.id());
		TestUtil.assertSize(1, relatorsUpdated);
		RouteRelator relator = relatorsUpdated.iterator().next();
		assertEquals("the relator should represent the activity type", activityType, relator.activityType());
		assertNotNull(relator.routeRelations());
		TestUtil.assertEmpty(relator.routeRelations());

	}

	@Test
	public void testIndexAll() throws MaxIndexAttemptsExceeded {
		Route route = routeTestUtil.createPersistedRoute();
		routeService.indexAllRoutes();
		QueueTestUtil.assertQueueContains(	routeTestUtil.getImportTestUtil().getQueueFactory()
													.getQueue(SearchWebApiUri.SEARCH_QUEUE_NAME),
											1,
											route.id().getValue().toString());
	}

	@Test
	public void testIndexRoute() throws MaxIndexAttemptsExceeded, EntityNotFoundException, FieldNotSearchableException,
			UnknownIndexException {
		Route route = routeTestUtil.createPersistedRoute();
		SearchDocuments indexedSearchDocuments = routeService.indexRoute(route.getId());
		SearchDocuments searchDocuments = routeService.searchService.searchDocuments(RouteSearchFilterBuilder.create()
				.build());
		TestUtil.assertSize(1, searchDocuments);
		TestUtil.assertContains(searchDocuments.iterator().next(), indexedSearchDocuments);

	}

	@Test
	public void testQueryRouteName() {
		Route expected = this.routeTestUtil.createPersistedRoute();
		Route notExpected = this.routeTestUtil.createPersistedRoute();
		String expectedName = expected.name().toString();
		TestUtil.assertNotEquals(expectedName, notExpected.name().toString());
		Filter filter = Filter.create().setQuery(expectedName).build();
		Routes routes = routeService.routes(filter);
		TestUtil.assertContainsOnly(expected, routes);
	}

}
