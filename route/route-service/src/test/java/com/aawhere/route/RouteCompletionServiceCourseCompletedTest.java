/**
 * 
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ExampleTracksImportServiceTestUtil;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Iterables;

/**
 * A service test specializing in testing many cases of convering {@link ActivityTiming} into a
 * {@link RouteCompletion}.
 * 
 * @author aroller
 * 
 */
public class RouteCompletionServiceCourseCompletedTest {

	private RouteServiceTestUtil routeUtil;
	private ExampleTracksImportServiceTestUtil examples;
	private RouteCompletionService service;
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private Route route;
	private Activity course;
	private Activity attempt;
	private Integer expectedSize = 1;
	private Boolean isCourseExpected = false;
	private Boolean existing = false;

	@Before
	public void setUp() {
		helper.setUp();
		this.routeUtil = RouteServiceTestUtil.create().examplesPrepared().build();
		this.examples = this.routeUtil.exampleTracksUtil();
		this.service = this.routeUtil.getRouteCompletionService();
		this.route = this.routeUtil.createPersistedRoute();
		this.course = this.examples.line();
	}

	@After
	public void test() throws EntityNotFoundException {
		assertNotNull("assign an attempt", attempt);
		LinkedList<ActivityTiming> timings = this.routeUtil.getTimingRelationServiceTestUtil()
				.importAndRelate(course.id(), attempt.id());
		final ActivityTiming timingCompleted = Iterables.getFirst(timings, null);
		assertTrue("course completed", timingCompleted.courseCompleted());

		// create a copy first before the test copy to make sure pre-existing doesn't flaw anything
		if (existing) {
			this.service.courseCompleted(route, timingCompleted);
		}
		Collection<RouteCompletion> routeCompletions = this.service.courseCompleted(route, timingCompleted);
		TestUtil.assertSize(expectedSize, routeCompletions);
		assertEquals("timing isn't reciprocal as expected", this.isCourseExpected, timingCompleted.reciprocal());
		for (RouteCompletion routeCompletion : routeCompletions) {
			assertEquals("isCourse", isCourseExpected, routeCompletion.isCourse());
			assertEquals("wrong attempt id", this.attempt.id(), routeCompletion.activityId());
		}
		TestUtil.assertSize("whoops, searching found the wrong count",
							expectedSize,
							this.service.completions(route.id(), Filter.create().filter()));

		helper.tearDown();
	}

	@Test
	public void testCourseCompletedSingleLapNonExisting() {
		this.attempt = this.examples.lineBefore();

	}

	@Test
	public void testCourseCompletedDoubleLapNonExisting() {
		this.attempt = this.examples.loopDouble();
		this.expectedSize = 2;
	}

	@Test
	public void testCourseCompletedDoubleLapExisting() {
		testCourseCompletedDoubleLapNonExisting();
		this.existing = true;
	}

	@Test
	public void testCourseCompletedSingleLapExisting() {
		testCourseCompletedSingleLapNonExisting();
		this.existing = true;
	}

	@Test
	public void testCourseCompletedReciprocalNonExisting() {
		this.attempt = this.examples.lineReciprocal();
		this.isCourseExpected = true;
	}

	@Test
	public void testCourseCompletedReciprocalExisting() {
		testCourseCompletedReciprocalNonExisting();
		this.existing = true;
	}

}
