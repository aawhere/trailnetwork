/**
 * 
 */
package com.aawhere.route.event;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ExampleTracksImportServiceTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityServiceBaseTest;
import com.aawhere.persist.Filter;
import com.aawhere.route.Route;
import com.aawhere.route.RouteServiceTestUtil;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class RouteEventServiceTest
		extends EntityServiceBaseTest<RouteEventService, RouteEvents, RouteEvent, RouteEventId> {

	private RouteEventServiceTestUtil eventUtil;
	private RouteServiceTestUtil routeUtil;
	private RouteEventService service;
	private ExampleTracksImportServiceTestUtil examples;

	@Before
	public void setUp() {
		super.setUpServices();
		this.routeUtil = RouteServiceTestUtil.create().build();
		setUp(this.routeUtil);
	}

	/**
	 * @param routeTestUtil
	 */
	private void setUp(RouteServiceTestUtil routeTestUtil) {
		this.routeUtil = routeTestUtil;
		this.eventUtil = RouteEventServiceTestUtil.create().routeTestUtil(routeTestUtil).build();
		this.service = this.routeUtil.getRouteEventService();
	}

	/**
	 * Call this before doing anything else and it will re-setup the items. Lazy call avoids a
	 * heavier setup for other methods not needing examples.
	 * 
	 * @return
	 */
	private ExampleTracksImportServiceTestUtil examples() {
		setUp(RouteServiceTestUtil.create().examplesPrepared().build());
		this.examples = this.routeUtil.exampleTracksUtil();
		return examples;
	}

	@Test
	public void testOrderByScore() {
		Integer highScore = 100000;

		RouteEvent higher = this.eventUtil.routeEventBuilder().score(highScore).build();
		RouteEvent lower = this.eventUtil.routeEventBuilder().score(highScore - 1).build();

		Filter filter = RouteEventFilterBuilder.create().orderByScoreHighestFirst().build();

		RouteEvents routeEvents = this.service.routeEvents(filter);
		TestUtil.assertCollectionEquals(Lists.newArrayList(higher, lower), routeEvents);
	}

	/**
	 * TN-768 Zero score events must still be indexed since they are referenced from Routes...and
	 * that's a good thing so let's make zero score available.
	 */
	@Test
	public void testZeroScoreIsAvailableDuringSort() {
		RouteEvent event = this.eventUtil.routeEventBuilder().score(RouteEvent.SCORE_DEFAULT).build();
		RouteEvents routeEvents = this.service.routeEvents(RouteEventFilterBuilder.create().orderByScoreHighestFirst()
				.build());
		TestUtil.assertContains(event, routeEvents);
	}

	@Test
	public void testUpdated() throws EntityNotFoundException {
		RouteEvent routeEvent = this.eventUtil.routeEventBuilder().build();
		RouteEvent found = this.service.routeEvent(routeEvent.id());
		Integer createdVersion = found.getEntityVersion();
		Integer numberOfParticipants = 33;
		Collection<RouteEvent> routeEventsUpdated = this.service.routeEventsUpdated(Lists.newArrayList(RouteEvent
				.mutate(found).completionsCount(numberOfParticipants).build()));
		TestUtil.assertSize(1, routeEventsUpdated);
		RouteEvent updated = this.service.routeEvent(routeEvent.id());
		assertEquals(numberOfParticipants, MeasurementUtil.integer(updated.completionsCount()));
		TestUtil.assertNotEquals(createdVersion, updated.getEntityVersion());
	}

	@Test
	public void testFilterByActivityType() {
		RouteEvent bikeEvent = this.eventUtil.routeEventBuilder().activityType(ActivityType.BIKE).build();
		RouteEvent runEvent = this.eventUtil.routeEventBuilder().activityType(ActivityType.RUN).build();
		TestUtil.assertContainsOnly(bikeEvent,
									this.service.routeEvents(RouteEventFilterBuilder.create()
											.activityType(bikeEvent.activityType()).orderByScoreHighestFirst().build()));
		TestUtil.assertContainsOnly(runEvent,
									this.service.routeEvents(RouteEventFilterBuilder.create()
											.activityType(runEvent.activityType()).orderByScoreHighestFirst().build()));

	}

	@Test
	public void testEventsForRoute() throws EntityNotFoundException {
		Route route1 = this.routeUtil.createPersistedRoute();
		Route route2 = this.routeUtil.createPersistedRoute();
		RouteEvent route1Bike = this.eventUtil.routeEventBuilder(route1).activityType(ActivityType.BIKE).build();
		RouteEvent route1Run = this.eventUtil.routeEventBuilder(route1).activityType(ActivityType.RUN).build();
		RouteEvent route2Bike = this.eventUtil.routeEventBuilder(route2).activityType(ActivityType.BIKE).build();
		final Filter bikeFilter = RouteEventFilterBuilder.create().activityType(ActivityType.BIKE)
				.orderByScoreHighestFirst().filter();
		final Filter runFilter = RouteEventFilterBuilder.create().activityType(ActivityType.RUN)
				.orderByScoreHighestFirst().filter();
		TestUtil.assertContainsOnly(route1Bike, this.service.routeEvents(route1.id(), bikeFilter));
		TestUtil.assertContainsOnly(route1Run, this.service.routeEvents(route1.id(), runFilter));
		TestUtil.assertEmpty("no runs for route2", this.service.routeEvents(route2.id(), runFilter));
		TestUtil.assertContainsOnly(route2Bike, this.service.routeEvents(route2.id(), bikeFilter));
	}

	@Test
	public void testFilterByStartArea() {
		this.examples();
		Activity line = examples.line();
		RouteEvent event = this.eventUtil.routeEventBuilder(this.routeUtil.createPersistedRoute(line)).build();
		// ensures the start is on the opposite side of the line.
		Activity returnLineOnly = examples.returnLineOnly();
		final Route returnRoute = this.routeUtil.createPersistedRoute(returnLineOnly);
		RouteEvent returnEvent = this.eventUtil.routeEventBuilder(returnRoute).build();
		Filter returnFilter = RouteEventFilterBuilder.create().startArea().intersection(returnRoute.startArea())
				.build();
		TestUtil.assertContainsOnly("the return starts far enough away only it should be found",
									returnEvent,
									this.service.routeEvents(returnFilter));
		// the return line bounds both starts.
		TestUtil.assertContainsAll(	Lists.newArrayList(event, returnEvent),
									this.service.routeEvents(RouteEventFilterBuilder.create().startArea()
											.intersection(returnRoute.boundingBox()).build()));
	}

	@Test
	public void testMerge() throws EntityNotFoundException {
		RouteEvent loser = this.eventUtil.routeEventBuilder().build();
		RouteEvent winner = this.eventUtil.routeEventBuilder().build();
		service.merged(loser.getRouteId(), winner.getRouteId());
		RouteEvents won = service.routeEvents(winner.getRouteId());
		TestUtil.assertContainsOnly(winner, won);
		try {
			assertNull(service.routeEvent(loser.getId()));
		} catch (EntityNotFoundException e) {
			TestUtil.assertEmpty(service.routeEvents(loser.getRouteId()));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#service()
	 */
	@Override
	public RouteEventService service() {
		return service;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#entity()
	 */
	@Override
	public RouteEvent entity() {
		return RouteEventTestUtil.event();
	}
}
