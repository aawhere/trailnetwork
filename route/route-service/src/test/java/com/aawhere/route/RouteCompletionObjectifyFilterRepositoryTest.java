/**
 * 
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.persist.BaseObjectifyFilterRepositoryTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class RouteCompletionObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<RouteCompletionId, RouteCompletion, RouteCompletionObjectifyRepository, RouteCompletionRepository, RouteCompletions, RouteCompletions.Builder> {

	private RouteCompletionObjectifyRepository repository;
	private RouteServiceTestUtil testUtil;

	@Before
	public void setUp() {
		this.testUtil = RouteServiceTestUtil.create().build();
		this.repository = testUtil.getRouteCompletionRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected RouteCompletion createEntity() {
		return this.testUtil.routeCompletion();
	}

	/** Synonym for {@link #createPersistedEntity()}. */
	public RouteCompletion routeCompletion() {
		return createPersistedEntity();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected RouteCompletionRepository getRepository() {
		return this.repository;
	}

	public Route route(RouteCompletion completion) throws EntityNotFoundException {
		return this.testUtil.getRouteService().route(completion.routeId());
	}

	@Test
	public void testAllForParent() throws EntityNotFoundException {
		RouteCompletion routeCompletion = routeCompletion();
		final Route parent = route(routeCompletion);
		RouteCompletion routeCompletion2 = this.testUtil.routeCompletion(parent);
		assertEquals(parent, route(routeCompletion2));
		RouteCompletion differentParent = routeCompletion();
		TestUtil.assertNotEquals(	"need another completion from a different parent to prove the limit works",
									parent.id(),
									differentParent.routeId());
		Filter filter = RouteCompletionFilterBuilder.create().route(parent).filter();
		assertFilter("children from one parent", 2, filter);
		assertFilter(	"a brother from another mother",
						1,
						RouteCompletionFilterBuilder.create().routeId(differentParent.routeId()).filter());
	}

	@Test
	public void testFindByActivityId() throws EntityNotFoundException {
		RouteCompletion routeCompletion = routeCompletion();
		RouteCompletion routeCompletion2 = routeCompletion();

		final ActivityId activityId = routeCompletion.activityId();

		Filter filter = RouteCompletionFilterBuilder.create().route(route(routeCompletion)).activity(activityId)
				.filter();
		Filter filter2 = RouteCompletionFilterBuilder.create().route(route(routeCompletion2))
				.activity(routeCompletion2.activityId()).filter();
		assertFilter("activity id", 1, filter);

		RouteCompletion completionWithSameActivityId = this.testUtil.routeCompletion(ActivityTestUtil
				.createActivity(activityId));

		assertFilter("another activity id in the index from a different parent", 1, filter);

		assertFilter("another activity id", 1, filter2);

		assertFilter("wrong activity id", 0, RouteCompletionFilterBuilder.create().route(route(routeCompletion))
				.activity(ActivityTestUtil.createActivity()).build());

		assertFilter("wrong route id", 0, RouteCompletionFilterBuilder.create().route(route(routeCompletion2))
				.activity(activityId).filter());

		RouteCompletions allForActivityId = assertFilter("activity id across parents", 2, RouteCompletionFilterBuilder
				.create().activity(activityId).filter());
		TestUtil.assertContainsAll(Sets.newHashSet(routeCompletion, completionWithSameActivityId), allForActivityId);
	}

}
