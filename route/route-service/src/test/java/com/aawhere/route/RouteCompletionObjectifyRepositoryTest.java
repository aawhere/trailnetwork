/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.id.IdentifierUtils.*;
import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.lang.If;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class RouteCompletionObjectifyRepositoryTest
		extends RouteCompletionUnitTest {

	private RouteCompletionObjectifyRepository repository;
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private RouteServiceTestUtil testUtil;

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();
		this.repository = (RouteCompletionObjectifyRepository) getRepository();
	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.route.RouteCompletionUnitTest#createRepository()
	 */
	@Override
	protected RouteCompletionRepository createRepository() {
		this.testUtil = RouteServiceTestUtil.create().build();
		return testUtil.getRouteCompletionRepository();
	}

	@Test
	public void testGetAllChildren() {
		Route route = this.testUtil.createPersistedRoute();
		Route route2 = this.testUtil.createPersistedRoute();
		RouteCompletion routeCompletion1 = testUtil.routeCompletion(route);
		RouteCompletion routeCompletion2 = testUtil.routeCompletion(route);
		// this is the decoy
		testUtil.routeCompletion(route2);

		Iterable<RouteCompletion> children = repository.children(route.id());
		final HashSet<RouteCompletion> expectedChildren = Sets.newHashSet(routeCompletion1, routeCompletion2);
		TestUtil.assertContainsAll(expectedChildren, children);
		TestUtil.assertSize(expectedChildren.size(), children);

	}

	@Test
	public void testGetAllChildrenIds() throws EntityNotFoundException {
		RouteCompletion completion1 = this.testUtil.routeCompletion();
		Route route = this.testUtil.getRouteService().route(completion1.routeId());
		RouteCompletion completion2 = this.testUtil.routeCompletion(route);
		// decoy
		this.testUtil.routeCompletion();
		Iterable<RouteCompletionId> childrenIds = repository.childrenIds(route.id());
		TestUtil.assertSize(2, childrenIds);
		TestUtil.assertContains(completion1.id(), childrenIds);
		TestUtil.assertContains(completion2.id(), childrenIds);
	}

	@Test
	public void testMergeAll() throws EntityNotFoundException {
		RouteCompletion existing1 = testUtil.routeCompletion();
		final Route route = this.testUtil.getRouteService().route(existing1.routeId());
		RouteCompletion existing2 = testUtil.routeCompletion(route);
		repository.setIsCourse(existing2.id(), true);
		// existing 3
		testUtil.routeCompletion(route);
		RouteCompletion nonExisting = RouteCompletionTestUtil.with(route).build();
		Integer replacementCourseActivityCount = If.nil(existing2.courseActivityCount()).use(10) + 1;
		// replaces existing two, but iscourse should remain from existing2 and the count should
		// come from this
		RouteCompletion replacement2 = RouteCompletion.clone(existing2).isCourse(false)
				.courseActivityCount(replacementCourseActivityCount).build();

		ImmutableList<RouteCompletion> incoming = ImmutableList.of(nonExisting, replacement2);
		Map<RouteCompletionId, RouteCompletion> merged = getRepository().mergeAll(incoming);
		TestUtil.assertSize("only those ", incoming.size(), merged.values());
		RouteCompletion merged2 = merged.get(existing2.id());
		assertTrue("courses should remain a course", merged2.isCourse());
		assertEquals(	"courses counts should always choose the higher",
						replacementCourseActivityCount,
						merged2.courseActivityCount());

		// now, load them all to make sure all are created
		Iterable<RouteCompletion> children = this.repository.children(route.id());
		TestUtil.assertSize("three existed, 1 merged + 1 new", 4, children);
		final Function<RouteCompletion, RouteCompletionId> idFunction = idFunction();
		Maps.uniqueIndex(children, idFunction);
	}
}
