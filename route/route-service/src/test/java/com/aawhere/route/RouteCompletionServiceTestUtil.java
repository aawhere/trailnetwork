/**
 * 
 */
package com.aawhere.route;

import com.aawhere.activity.Activity;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * @author aroller
 * 
 */
public class RouteCompletionServiceTestUtil {

	private RouteServiceTestUtil routeUtil;
	private RouteCompletionService service;
	private RouteCompletionObjectifyRepository repository;

	/**
	 * Used to construct all instances of RouteCompletionServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<RouteCompletionServiceTestUtil> {

		public Builder() {
			super(new RouteCompletionServiceTestUtil());
		}

		public Builder routeUtil(RouteServiceTestUtil routeUtil) {
			building.routeUtil = routeUtil;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("routeUtil", building.routeUtil);
		}

		@Override
		public RouteCompletionServiceTestUtil build() {
			RouteCompletionServiceTestUtil built = super.build();
			built.repository = built.routeUtil.getRouteCompletionRepository();
			built.service = built.routeUtil.getRouteCompletionService();
			return built;
		}

	}// end Builder

	/**
	 * @return the service
	 */
	public RouteCompletionService service() {
		return this.service;
	}

	/**
	 * @return the repository
	 */
	public RouteCompletionObjectifyRepository repository() {
		return this.repository;
	}

	/** Use {@link Builder} to construct RouteCompletionServiceTestUtil */
	private RouteCompletionServiceTestUtil() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Provides a connected builder that will persist during the build providing the
	 * {@link RouteCompletionTestUtil#routeCompletion()} seed.
	 * 
	 * @see #routeCompletionBuilder(RouteCompletion)
	 * @return
	 */
	public RouteCompletion.Builder routeCompletionBuilder() {
		return routeCompletionBuilder(RouteCompletionTestUtil.with(this.routeUtil.createPersistedRoute()).build());
	}

	public RouteCompletion.Builder routeCompletionBuilder(Route route) {
		return routeCompletionBuilder(RouteCompletionTestUtil.with(route).build());
	}

	public RouteCompletion.Builder routeCompletionBuilder(RouteCompletion seed) {

		final RouteCompletion.Builder builder = new RouteCompletion.Builder(seed) {
			/*
			 * (non-Javadoc)
			 * @see com.aawhere.route.RouteCompletion.Builder#build()
			 */
			@Override
			public RouteCompletion build() {
				final RouteCompletion built = super.build();
				return repository.store(built);

			}
		};
		return builder;
	}

	/**
	 * @param route
	 * @param activity
	 * @return
	 */
	public RouteCompletion.Builder routeCompletionBuilder(Route route, Activity activity) {
		return routeCompletionBuilder(RouteCompletionTestUtil.with(route).activity(activity).build());
	}
}
