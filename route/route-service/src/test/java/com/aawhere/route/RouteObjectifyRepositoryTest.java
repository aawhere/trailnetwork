package com.aawhere.route;

import org.junit.After;
import org.junit.Before;
import org.junit.experimental.categories.Category;

import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.category.PersistenceTest;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 *
 */

/**
 * Tests the {@link RouteObjectifyRepository}.
 * 
 * @author Brian Chapman
 * 
 */
@Category(PersistenceTest.class)
public class RouteObjectifyRepositoryTest
		extends RouteUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();
	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.route.RouteUnitTest#createRepository()
	 */
	@Override
	protected RouteRepository createRepository() {
		RouteServiceTestUtil testUtil = RouteServiceTestUtil.create().build();
		return testUtil.getRouteRepository();
	}

	@Override
	public Route getEntitySample() {
		Route route = super.getEntitySample();
		return route;
	}

	public static RouteObjectifyRepositoryTest getInstance() {
		RouteObjectifyRepositoryTest instance = new RouteObjectifyRepositoryTest();
		try {
			instance.setUpObjectify();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		instance.baseSetUp();
		instance.setUp();
		return instance;
	}

}
