/**
 *
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.all.RouteServiceNestedField;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocument.Field;
import com.aawhere.search.SearchDocuments;
import com.aawhere.test.TestUtil;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadTestUtil;

/**
 * @author Brian Chapman
 * 
 */
public class RouteSearchDocumentProducerUnitTest {

	private Route route;

	@Before
	public void setup() {
		route = RouteTestUtils.createRouteWithId();

	}

	/**
	 * Public routes use only a route and trailhead. RelativeStats is created based on provided
	 * information.
	 * 
	 * @throws FieldNotRegisteredException
	 */
	@Test
	public void testPublicRoutes() throws FieldNotRegisteredException {
		RouteSearchDocumentProducer producer = RouteSearchDocumentProducer.create().route(route).build();
		SearchDocuments docs = producer.searchDocuments();
		// one for the route and each activity type
		int expectedActivityTypeCount = route.completionStats().activityTypeStats().size();
		int expectedDocumentCountForAll = 1;
		Integer expectredDocsSize = expectedDocumentCountForAll + expectedActivityTypeCount;
		TestUtil.assertSize("not all documents produced", expectredDocsSize, docs);
		int fieldCountWithActivityType = 0;
		for (SearchDocument searchDocument : docs) {
			List<Field> fields = searchDocument.getFields();
			String fieldsString = fields.toString();
			TestUtil.assertContains(fieldsString, RouteField.ID);
			TestUtil.assertContains(fieldsString, RouteField.NAME);
			if (searchDocument.index().contains(Layer.ACTIVITY_TYPE.name())) {
				fieldCountWithActivityType++;
				TestUtil.assertContains(fieldsString,
										RouteServiceNestedField.ROUTE__RELATIVE_COMPLETION_STATS__ACTIVITY_TYPE);
			}
		}

		assertEquals("wrong number of actvity types", fieldCountWithActivityType, expectedActivityTypeCount);
	}

	@Test
	public void testTrailhead() {
		Trailhead startTrailhead = TrailheadTestUtil.createNearbyTrailhead(route.start());
		RouteSearchDocumentProducer producer = RouteSearchDocumentProducer.create().route(route)
				.startTrailhead(startTrailhead).build();
		String fieldString = producer.searchDocuments().iterator().next().getFields().toString();
		TestUtil.assertContains(fieldString, startTrailhead.name().toString());
	}

	@Test
	public void testPersonRoutes() {
		RelativeStatistic stats = RelativeStatistic.create().personId(PersonTestUtil.personId())
				.activityType(ActivityType.BIKE).count(10).featureId(route.id()).build();
		RouteSearchDocumentProducer producer = RouteSearchDocumentProducer.create().route(route).personalStats(stats)
				.build();
		SearchDocuments searchDocuments = producer.searchDocuments();
		TestUtil.assertSize(1, searchDocuments);
		String fieldString = searchDocuments.iterator().next().fields().toString();
		TestUtil.assertContains(fieldString, stats.personId().toString());
		TestUtil.assertContains(fieldString, RouteSearchDocumentProducer.PERSON_ID.toString());
	}

}