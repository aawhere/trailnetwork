/**
 *
 */
package com.aawhere.route;

import static junit.framework.Assert.*;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportServiceTestUtil;
import com.aawhere.activity.ActivityService;
import com.aawhere.activity.ExampleTracksImportServiceTestUtil;
import com.aawhere.activity.spatial.ActivitySpatialRelationServiceTestUtil;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingServiceTestUtil;
import com.aawhere.app.DeviceTestUtil;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;
import com.aawhere.person.PersonService;
import com.aawhere.person.PersonServiceTestUtil;
import com.aawhere.queue.QueueFactory;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventObjectifyRepository;
import com.aawhere.route.event.RouteEventService;
import com.aawhere.search.GaeSearchService;
import com.aawhere.search.GaeSearchTestUtils;
import com.aawhere.search.SearchService;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadServiceTestUtil;
import com.aawhere.trailhead.TrailheadUtil;
import com.aawhere.view.ViewCountObjectifyRepository;
import com.google.common.collect.Iterables;

/**
 * @author Brian Chapman
 * 
 */
public class RouteServiceTestUtil {

	private RouteEventObjectifyRepository routeEventRepository;
	private RouteService routeService;
	private ActivityTimingServiceTestUtil timingRelationServiceTestUtil;
	private TrailheadServiceTestUtil trailheadServiceTestUtil;
	private RouteCompletionObjectifyRepository routeCompletionRepository;
	private RouteCompletionService routeCompletionService;
	private RouteEventService routeEventService;
	private FieldAnnotationDictionaryFactory fieldAnnotationDictionaryFactory;
	/** The repository to be injected...one of the following */
	private RouteRepository routeRepository;
	private RouteObjectifyRepository routeObjectifyRepository;
	private RouteSearchRepository routeSearchRepository;
	public GaeSearchService searchService;

	/**
	 * @return the routeService
	 */
	public RouteService getRouteService() {
		return this.routeService;
	}

	/**
	 * @return the routeRepository
	 */
	public RouteRepository getRouteRepository() {
		return this.routeRepository;
	}

	public RouteObjectifyRepository getRouteObjectifyRepository() {
		return routeObjectifyRepository;
	}

	public RouteSearchRepository getRouteSearchRepository() {
		return routeSearchRepository;
	}

	public SearchService getSearchService() {
		return this.searchService;
	}

	/**
	 * @return the routeEventRepository
	 */
	public RouteEventObjectifyRepository getRouteEventRepository() {
		return this.routeEventRepository;
	}

	/**
	 * @return the routeCompletionRepository
	 */
	public RouteCompletionObjectifyRepository getRouteCompletionRepository() {
		return this.routeCompletionRepository;
	}

	/**
	 * @return the routeCompletionService
	 */
	public RouteCompletionService getRouteCompletionService() {
		return this.routeCompletionService;
	}

	/**
	 * @return the timingRelationServiceTestUtil
	 */
	public ActivityTimingServiceTestUtil getTimingRelationServiceTestUtil() {
		return this.timingRelationServiceTestUtil;
	}

	/**
	 * @return the routeEventService
	 */
	public RouteEventService getRouteEventService() {
		return this.routeEventService;
	}

	public ExampleTracksImportServiceTestUtil exampleTracksUtil() {
		return this.timingRelationServiceTestUtil.getExamples();
	}

	/**
	 * @return the trailheadServiceTestUtil
	 */
	public TrailheadServiceTestUtil getTrailheadServiceTestUtil() {
		return this.trailheadServiceTestUtil;
	}

	/**
	 * Used to construct all instances of RouteServiceTestUtils.
	 */
	public static class Builder
			extends ObjectBuilder<RouteServiceTestUtil> {

		private ActivityTimingServiceTestUtil.Builder timingRelationServiceTestUtilBuilder;
		private TrailheadServiceTestUtil.Builder trailheadServiceTestUtilBuilder;
		private RouteService.Options options = new RouteService.Options().minNumberOfCoursesToKeep(1)
				.minNumberOfViewsToKeepAlias(0);
		private RouteCompletionService.Options completionsOptions = new RouteCompletionService.Options()
				.minNumberOfCoursesToScore(2);

		public Builder() {
			super(new RouteServiceTestUtil());
			this.timingRelationServiceTestUtilBuilder = ActivityTimingServiceTestUtil.create();
			this.trailheadServiceTestUtilBuilder = TrailheadServiceTestUtil.create();
		}

		/**
		 * @return the timingRelationServiceTestUtilBuilder
		 */
		public ActivityTimingServiceTestUtil.Builder getTimingRelationServiceTestUtilBuilder() {
			return this.timingRelationServiceTestUtilBuilder;
		}

		/**
		 * @return the trailheadServiceTestUtilBuilder
		 */
		public TrailheadServiceTestUtil.Builder getTrailheadServiceTestUtilBuilder() {
			return this.trailheadServiceTestUtilBuilder;
		}

		public RouteService.Options options() {
			return options;
		}

		public RouteCompletionService.Options completionsOptions() {
			return this.completionsOptions;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public RouteServiceTestUtil build() {

			com.aawhere.activity.ActivityImportServiceTestUtil.Builder importUtilBuilder = this.timingRelationServiceTestUtilBuilder
					.getImportUtilBuilder();
			importUtilBuilder.getPersonServiceTestUtilBuilder().register(RouteId.class);
			importUtilBuilder.getPersonServiceTestUtilBuilder().register(RouteCompletionId.class);
			importUtilBuilder.getPersonServiceTestUtilBuilder().register(RouteEventId.class);
			importUtilBuilder.getPersonServiceTestUtilBuilder().register(RouteId.class);
			// to share import services (without sharing dependencies) the import util must be
			// configured and built separately
			final com.aawhere.activity.spatial.ActivitySpatialRelationServiceTestUtil.Builder spatialUtilbuilder = ActivitySpatialRelationServiceTestUtil
					.create().useExternalImportUtil(importUtilBuilder);
			building.timingRelationServiceTestUtil = this.timingRelationServiceTestUtilBuilder.build();
			ActivityImportServiceTestUtil importServiceTestUtil = building.timingRelationServiceTestUtil
					.getImportTestUtil();
			QueueFactory queueFactory = importServiceTestUtil.getQueueFactory();
			ActivitySpatialRelationServiceTestUtil spatialUtil = spatialUtilbuilder
					.setImportServiceUtil(importServiceTestUtil).build();
			FieldAnnotationDictionaryFactory annotationDictionaryFactory = this.timingRelationServiceTestUtilBuilder
					.getAnnotationDictionaryFactory();
			annotationDictionaryFactory.getDictionary(Route.class);
			building.trailheadServiceTestUtil = this.trailheadServiceTestUtilBuilder
					.dictionaryFactory(annotationDictionaryFactory).build();
			FieldDictionaryFactory factory = annotationDictionaryFactory.getFactory();
			annotationDictionaryFactory.getFactory().register(annotationDictionaryFactory.getDictionary(Route.class));
			building.fieldAnnotationDictionaryFactory = annotationDictionaryFactory;
			building.routeCompletionRepository = new RouteCompletionObjectifyRepository(factory,
					Synchronization.SYNCHRONOUS);
			building.searchService = GaeSearchTestUtils.createGaeSearchService(annotationDictionaryFactory);
			building.routeObjectifyRepository = new RouteObjectifyRepository(factory, Synchronization.SYNCHRONOUS);
			building.routeSearchRepository = new RouteSearchRepository(building.routeObjectifyRepository,
					building.searchService, building.trailheadServiceTestUtil.service());
			// This line matches that in RouteModule injection
			building.routeRepository = building.routeSearchRepository;
			building.routeEventRepository = new RouteEventObjectifyRepository(factory, Synchronization.SYNCHRONOUS);
			PersonServiceTestUtil personServiceTestUtil = importServiceTestUtil.getActivityServiceTestUtil()
					.getTrackTestUtil().getDocumentTestUtil().getPersonServiceTestUtil();

			final ActivityService activityService = building.timingRelationServiceTestUtil.getImportTestUtil()
					.getActivityServiceTestUtil().getActivityService();
			final PersonService personService = personServiceTestUtil.getPersonService();
			building.routeEventService = new RouteEventService(building.routeEventRepository,
					personServiceTestUtil.membershipService(), queueFactory);

			building.routeCompletionService = new RouteCompletionService(building.routeCompletionRepository,
					building.routeEventService,
					building.timingRelationServiceTestUtil.getActivityTimingRelationService(), activityService,
					personService, this.completionsOptions);

			building.routeService = new RouteService(building.routeRepository, building.routeCompletionService,
					building.routeEventService, building.trailheadServiceTestUtil.service(), activityService,
					building.timingRelationServiceTestUtil.getActivityTimingRelationService(),
					spatialUtil.spatialService(), personService, personServiceTestUtil.membershipService(),
					DeviceTestUtil.REPOSITORY, new ViewCountObjectifyRepository(), queueFactory, options,
					building.searchService, annotationDictionaryFactory);
			return super.build();
		}

		/**
		 * @return
		 */
		public Builder examplesPrepared() {
			this.timingRelationServiceTestUtilBuilder.getImportUtilBuilder().examplesPrepared();
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteServiceTestUtils */
	private RouteServiceTestUtil() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Persists a random Route loosely related to anything using unit tests to populate the route.
	 * 
	 * @see #routeCreated()
	 * @see RouteTestUtils#createRoute()
	 * 
	 * @return
	 */
	public Route createPersistedRoute() {
		Activity course = getImportTestUtil().getActivityServiceTestUtil().createPersistedActivity();
		return createPersistedRoute(course);

	}

	public Route createPersistedRoute(Activity course) {

		Route createRoute = RouteTestUtils.createRoute(course);

		return persisted(createRoute);

	}

	/**
	 * Imports an activity and uses the route service to create the most basic route.
	 * 
	 * 
	 * @see RouteService#routeCreated(Activity)
	 * @return
	 */
	public Route routeCreated() {
		try {
			Activity activity = getImportTestUtil().createPersistedActivity();
			return this.routeService.routeCreated(activity);
		} catch (BaseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Provides a basic {@link #routeCreated()} that has then called
	 * {@link RouteService#routeUpdated(RouteId)} to further populate desired fields.
	 * 
	 * @return
	 */
	public Route routeUpdated() {
		try {
			return this.routeService.routeUpdated(routeCreated().id()).updated();
		} catch (EntityNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return
	 */
	public ActivityImportServiceTestUtil getImportTestUtil() {
		return getTimingRelationServiceTestUtil().getImportTestUtil();
	}

	public PersonServiceTestUtil getPersonTestUtil() {
		return getImportTestUtil().getPersonServiceTestUtil();
	}

	public Route createPersistedRouteAndTrailheads() throws BaseException {
		Activity course = this.timingRelationServiceTestUtil.getImportTestUtil().createPersistedActivity();
		return createPersistedRouteAndTrailheads(course);
	}

	public Route createPersistedRouteAndTrailheads(Activity course) {

		Route route = createPersistedRoute(course);
		TrailheadId startId = route.start();
		Trailhead start = getTrailheadServiceTestUtil().persisted(TrailheadUtil.coordinateFrom(startId));
		assertEquals(startId, start.id());
		TrailheadId finishId = route.finish();
		Trailhead finish = getTrailheadServiceTestUtil().persisted(TrailheadUtil.coordinateFrom(finishId));
		assertEquals(finishId, finish.id());
		return route;

	}

	/** Stores the route given without validation or relation */
	public Route persisted(Route route) {
		return store(route);
	}

	/**
	 * @return
	 */
	public Routes createPersistedRoutes() {
		Routes routes = RouteTestUtils.createRoutes();
		return Routes.create().addAll(routeRepository.storeAll(routes).values()).build();
	}

	/**
	 * Provides a more valid route completion which uses the service to create a completion and
	 * associated data structures.
	 * 
	 * @param activity
	 * @return
	 */
	public RouteCompletion routeCompletionRelated(Activity activity) {
		Route createPersistedRoute = createPersistedRoute();
		return routeCompletionRelated(createPersistedRoute, activity);
	}

	/**
	 * @param route
	 * @param activity
	 * @return
	 */
	public RouteCompletion routeCompletionRelated(Route route, Activity activity) {
		return this.routeCompletionService.courseCompletion(route, activity);
	}

	/**
	 * Creates a persisted random routeCompletion, but with the given activity as the activityId of
	 * the completion.
	 * 
	 * @param activityId
	 * @return
	 */
	public RouteCompletion routeCompletion(Activity activity) {
		Route createPersistedRoute = createPersistedRoute(activity);
		RouteCompletion routeCompletion = RouteCompletionTestUtil.with(createPersistedRoute).activity(activity).build();
		return getRouteCompletionRepository().store(routeCompletion);
	}

	public RouteCompletion routeCompletion(Route route) {
		RouteCompletion routeCompletion = RouteCompletionTestUtil.with(route).routeCompletion();
		return getRouteCompletionRepository().store(routeCompletion);
	}

	/**
	 * Given ids it compares every id against the other creating a timing for each which all are
	 * returned regardless if completed is true or false. the timings are then used to create
	 * {@link RouteCompletions} and those too are returned, but RouteCompletions are only created if
	 * a timing course completed.
	 * 
	 * @param activityIds
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Pair<? extends List<ActivityTiming>, ? extends List<RouteCompletion>> importAndRelateCompletions(
			ActivityId... activityIds) throws EntityNotFoundException {
		LinkedList<ActivityTiming> timings = getTimingRelationServiceTestUtil().importAndRelate(activityIds);
		LinkedList<RouteCompletion> completions = new LinkedList<RouteCompletion>();
		for (ActivityTiming activityTimingRelation : timings) {
			final RouteCompletions completionsUpdated = this.routeService.completionsUpdated(activityTimingRelation
					.id());
			Iterables.addAll(completions, completionsUpdated);
		}
		// most tests want scoring routes...let's fake it here.
		Iterable<RouteId> routeIds = RouteCompletionUtil.routeIds(completions);
		for (RouteId routeId : routeIds) {
			routeRepository.update(routeId).score(RouteTestUtils.scoreWorthy()).build();
		}
		return Pair.of(timings, completions);
	}

	/**
	 * Provides the updated route for the given activity ids, the first being the course of
	 * interest.
	 * 
	 * @return
	 * @throws EntityNotFoundException
	 * @throws CoursesNotRelatedException
	 */
	public Route route(ActivityId... activityIds) throws EntityNotFoundException, CoursesNotRelatedException {
		return routeUpdated(activityIds).updated();
	}

	public RouteUpdateDetails routeUpdated(ActivityId... activityIds) throws EntityNotFoundException,
			CoursesNotRelatedException {
		importAndRelateCompletions(activityIds);
		// this could move into the service if found to be valuable in main code
		ActivityId courseId = activityIds[0];
		Route route = this.routeService.route(courseId);
		return this.routeService.routeUpdated(route.id());
	}

	/**
	 * Store a route in the repository by either creating or updating if it already exists.
	 * 
	 * 
	 * @param route
	 * @return
	 */
	Route store(Route route) {
		if (route.id() == null) {
			return this.routeRepository.store(route);
		} else {
			this.routeRepository.persist(route);
			return route;
		}
	}

	/**
	 * @return
	 */
	public RouteCompletion routeCompletion() {
		return routeCompletion(createPersistedRoute());
	}

	/**
	 * @param i
	 */
	public Set<Route> createPersistedRoutes(Integer numberToCreate) {
		Set<Route> routes = new HashSet<Route>();
		for (int i = 0; i < numberToCreate; i++) {
			routes.add(createPersistedRoute());
		}
		return routes;
	}

	/**
	 * Calls the update, but gives it a stale setting prior to ensure a full update occurs.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteUpdateDetails routeUpdated(RouteId id) throws EntityNotFoundException {
		getRouteRepository().setStale(id, true);
		return routeService.routeUpdated(id);
	}

	/**
	 * @return
	 * @throws BaseException
	 */
	public RouteCompletion routeCompletionRelated() throws BaseException {
		return routeCompletionRelated(getImportTestUtil().createPersistedActivity());
	}

	/**
	 * assigns scores in a manner that the keeper will be chosen during a merge
	 * 
	 * @param keeper
	 * @param loser
	 * @throws EntityNotFoundException
	 */
	public void scoreForMerge(Route keeper, Route loser) throws EntityNotFoundException {
		int highScore = 10;
		getRouteRepository().setScore(keeper.id(), highScore);
		getRouteRepository().setScore(loser.id(), highScore - 1);
	}

	/**
	 * Provides a connected builder that will persist the built object.
	 * 
	 * @param route
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteCompletion.Builder mutate(RouteCompletionId id) throws EntityNotFoundException {
		return this.routeCompletionRepository.update(id);
	}

	/**
	 * provides a connected builder allowing you to update any property of a persisted route.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Route.Builder mutate(RouteId id) throws EntityNotFoundException {
		return this.routeRepository.update(id);
	}

	/**
	 * @return
	 */
	public FieldAnnotationDictionaryFactory fieldAnnotationDictionaryFactory() {
		return this.fieldAnnotationDictionaryFactory;
	}

}
