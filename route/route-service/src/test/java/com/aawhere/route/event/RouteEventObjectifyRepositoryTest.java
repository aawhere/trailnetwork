/**
 * 
 */
package com.aawhere.route.event;

import org.junit.After;
import org.junit.Before;

import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.route.RouteServiceTestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author aroller
 * 
 */
public class RouteEventObjectifyRepositoryTest
		extends RouteEventUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();

	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.route.event.RouteEventUnitTest#createRepository()
	 */
	@Override
	protected RouteEventRepository createRepository() {
		RouteServiceTestUtil util = RouteServiceTestUtil.create().build();
		return util.getRouteEventRepository();
	}
}
