package com.aawhere.route;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.test.TestUtil;

public class RouteSearchFilterBuilderUnitTest {

	private RouteSearchFilterBuilder filterBuilder;
	private Layer expectedLayer;
	private ActivityType activityTypeExpected;

	@Before
	public void setUp() {
		this.filterBuilder = RouteSearchFilterBuilder.create();
	}

	@After
	public void test() {
		if (this.activityTypeExpected != null) {
			this.filterBuilder.activityType(activityTypeExpected);
		}
		Filter filter = this.filterBuilder.build();
		String string = filter.toString();
		if (this.activityTypeExpected != null) {
			TestUtil.assertContains(string, activityTypeExpected.toString());
		}
		// this confirms the layer
		TestUtil.assertContains(string, RouteSearchUtil.indexName(expectedLayer));
	}

	@Test
	public void testActivityTypeLayer() {
		expectedLayer = Layer.ACTIVITY_TYPE;
		activityTypeExpected = ActivityType.BIKE;

	}

	@Test
	public void testAllLayer() {
		this.expectedLayer = Layer.ALL;
	}

	@Test
	public void testIndexProvidedDirectly() {
		// auto would find ALL if left index wasn't provided
		this.expectedLayer = Layer.ACTIVITY_TYPE;
		filterBuilder.addSearchIndex(RouteSearchUtil.indexName(expectedLayer));
	}

	@Test
	public void testPersonLayerDetected() {
		this.expectedLayer = Layer.PERSON;
		filterBuilder.personId(PersonTestUtil.personId());
	}

	@Test
	public void testPersonActivityTypeLayerDetected() {
		this.expectedLayer = Layer.PERSON_ACTIVITY_TYPE;
		filterBuilder.personId(PersonTestUtil.personId());
		activityTypeExpected = ActivityType.BIKE;
	}

	@Test
	public void testLayerFieldDirect() {
		this.expectedLayer = Layer.PERSON_ACTIVITY_TYPE;

		FilterCondition<Object> condition = FilterCondition.create().field(RouteSearchDocumentProducer.LAYER)
				.value(this.expectedLayer).build();
		Filter filter = Filter.create().addCondition(condition).build();
		filterBuilder = RouteSearchFilterBuilder.clone(filter);

	}

	/**
	 * Distance category also implies activity type. the client should only need to select the
	 * distance category to receive Activity Type layer behavior.
	 * 
	 */
	@Test
	public void testDistanceCategoryProvidesActivityType() {
		FilterCondition<Object> condition = FilterCondition.create()
				.field(RouteSearchDocumentProducer.DISTANCE_CATEGORY).value(ActivityTypeDistanceCategory.CENTURY)
				.build();
		this.filterBuilder = RouteSearchFilterBuilder.clone(Filter.create().addCondition(condition).build());
		this.expectedLayer = Layer.ACTIVITY_TYPE;
	}
}
