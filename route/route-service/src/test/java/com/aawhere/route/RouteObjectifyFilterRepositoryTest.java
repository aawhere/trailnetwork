/**
 *
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationStats;
import com.aawhere.app.ApplicationTestUtil;
import com.aawhere.app.KnownApplication;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.persist.BaseObjectifyFilterRepositoryTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.route.Route.MainsForTrailheadStartActivityTypeIndex;
import com.aawhere.route.Route.RoutesForTrailheadStartActivityTypeIndex;
import com.aawhere.test.TestUtil;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadTestUtil;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

/**
 * @author Brian Chapman
 * 
 */
public class RouteObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<RouteId, Route, RouteObjectifyRepository, RouteRepository, Routes, Routes.Builder> {

	private RouteObjectifyRepository routeRepository;
	private RouteServiceTestUtil routeTestUtil;

	@Before
	public void setUp() {

		routeTestUtil = RouteServiceTestUtil.create().build();
		routeRepository = routeTestUtil.getRouteObjectifyRepository();
	}

	@Test
	public void testFilterStartTrailhead() throws EntityNotFoundException {
		Route route = routeTestUtil.createPersistedRoute();

		final Integer limit = 1;

		Filter routesFilter = new RouteDatastoreFilterBuilder().setLimit(limit).start(route.start()).build();

		Routes result = getFilterRepository().filter(routesFilter);
		assertFilter(result, 1);
	}

	@Test
	public void testFilterFinishTrailhead() throws EntityNotFoundException {
		Route route = routeTestUtil.createPersistedRoute();

		final Integer limit = 1;

		Filter routesFilter = new RouteDatastoreFilterBuilder().setLimit(limit).finish(route.finish()).build();

		Routes result = getFilterRepository().filter(routesFilter);
		assertFilter(result, 1);
	}

	@Test
	@Ignore("TN-804 moved to search service")
	public void testFilterActivityTypeValue() {
		ActivityTypeStatistic activityTypeStats;
		Route route;
		do {
			route = routeTestUtil.createPersistedRoute();
			SortedSet<ActivityTypeStatistic> activityTypeCounts = route.activityTypeStats();
			activityTypeStats = Iterables.getFirst(activityTypeCounts, null);
		} while (activityTypeStats == null);

		final ActivityType category = activityTypeStats.category();
		Filter filter = RouteDatastoreFilterBuilder.create().start(route.start()).activityTypes(category).build();
		Routes routes = routeRepository.filter(filter);
		TestUtil.assertContains(route, routes);
	}

	@Test
	@Ignore("TN-804 moved to search service")
	public void testFilterSingleActivityType() throws EntityNotFoundException {
		Route route = routeTestUtil.createPersistedRoute();
		ActivityTypeStatistic expected = ActivityTypeStatistic.create().type(ActivityType.BIKE).count(5).total(10)
				.build();
		TreeSet<ActivityTypeStatistic> expecteds = new TreeSet<>();
		expecteds.add(expected);

		Route updated = updateActivityTypes(route, expecteds);
		assertEquals("activity type counts", expecteds, updated.activityTypeStats());
		assertActivityTypes(Sets.newHashSet(updated), ActivityType.BIKE);
		assertActivityTypes(new HashSet<Route>(), ActivityType.RUN);
		assertActivityTypes(Sets.newHashSet(updated), ActivityType.BIKE, ActivityType.RUN);
		assertActivityTypes(new HashSet<Route>(), ActivityType.WALK, ActivityType.RUN);
	}

	@Test
	@Ignore("TN-804 moved to search service")
	public void testFilterMultipleActivityType() throws EntityNotFoundException {
		Route route = routeTestUtil.createPersistedRoute();

		final Set<ActivityType> activityTypes = ActivityTypeStatistic.activityTypes(route.activityTypeStats());
		assertActivityTypes(Sets.newHashSet(route), activityTypes);
		assertActivityTypes(Sets.newHashSet(route), Iterables.getFirst(activityTypes, null));
		// all the other possibilities shouldn't find the ones persisted
		final SetView<ActivityType> allOthers = Sets.difference(Sets.newHashSet(ActivityType.values()), activityTypes);
		assertActivityTypes(new HashSet<Route>(), allOthers);
	}

	@Test
	@Ignore("TN-804 moved to search service")
	public void testFilterMultipleRoutesWithSameActivityType() throws EntityNotFoundException {
		Route route1 = routeTestUtil.createPersistedRoute();
		Route route2 = routeTestUtil.createPersistedRoute();
		ActivityTypeStatistic expected = ActivityTypeStatistic.create().type(ActivityType.BIKE).count(5).total(10)
				.build();
		TreeSet<ActivityTypeStatistic> expecteds = new TreeSet<>();
		expecteds.add(expected);
		Route updated1 = updateActivityTypes(route1, expecteds);
		Route updated2 = updateActivityTypes(route2, expecteds);
		assertActivityTypes(Sets.newHashSet(updated1, updated2), expected.type());
		assertActivityTypes(new HashSet<Route>(), ActivityType.RUN);
	}

	/**
	 * @param route
	 * @param stats
	 * @return
	 */
	private Route updateActivityTypes(Route route, TreeSet<ActivityTypeStatistic> stats) {
		return this.routeRepository.update(Route.mutate(route)
				.completionStats(RouteStats.routeStatsCreator().activityTypeStats(stats).build()).build());
	}

	private Route updateApplications(Route route, TreeSet<ApplicationStats> stats) {
		return this.routeRepository.update(Route.mutate(route)
				.completionStats(RouteStats.routeStatsCreator().applicationStats(stats).build()).build());
	}

	/**
	 * Simple validation filter to ensure indexes are avialable.
	 * 
	 * @see RoutesForTrailheadStartActivityTypeIndex
	 * */
	@Test
	@Ignore("TN-804 moved to search service")
	public void testStartAndActivityType() {
		Pair<Route, Filter> pair = activityTypeFilter();
		Routes actual = this.routeRepository.filter(pair.getRight());
		TestUtil.assertContains(pair.getLeft(), actual);
	}

	/**
	 * Persists two to ensure only one can be found at a time if requested. Then finds both.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	@Ignore("TN-804 moved to search service")
	public void testApplication() throws EntityNotFoundException {
		Route route1 = routeTestUtil.createPersistedRoute();
		ApplicationStats stats1 = ApplicationTestUtil.applicationStats(KnownApplication.GARMIN_CONNECT.asApplication());
		TreeSet<ApplicationStats> stats1Set = Sets.newTreeSet();
		stats1Set.add(stats1);
		route1 = updateApplications(route1, stats1Set);

		Set<Application> applications1 = Sets.newHashSet(CategoricalStatistic.categories(stats1Set));

		Route route2 = routeTestUtil.createPersistedRoute();
		ApplicationStats stats2 = ApplicationTestUtil.applicationStats(KnownApplication.EVERY_TRAIL.asApplication());
		TreeSet<ApplicationStats> stats2Set = Sets.newTreeSet();
		stats2Set.add(stats2);
		route2 = updateApplications(route2, stats2Set);
		Set<Application> applications2 = Sets.newHashSet(CategoricalStatistic.categories(stats2Set));

		Filter filter1 = RouteDatastoreFilterBuilder.create().applications(applications1).build();
		Routes found1 = this.routeRepository.filter(filter1);
		TestUtil.assertContains(route1, found1);
		TestUtil.assertSize(1, found1);

		// ensure one can be found
		Filter filter2 = RouteDatastoreFilterBuilder.create().applications(applications2).build();
		Routes found2 = this.routeRepository.filter(filter2);
		TestUtil.assertContains(route2, found2);
		TestUtil.assertSize(1, found2);

		// ensure multiple can be found
		Filter filterBoth = RouteDatastoreFilterBuilder.create().applications(Sets.union(applications1, applications2))
				.build();
		Routes foundBoth = this.routeRepository.filter(filterBoth);
		TestUtil.assertSize(2, foundBoth);
		TestUtil.assertContains(route1, foundBoth);
		TestUtil.assertContains(route2, foundBoth);
	}

	/**
	 * @see pushes the indexes to the max
	 */
	@Test
	@Ignore("this test is a problem since the subqueries go beyond the limits.  We must rethink the combined")
	public void testAllIndex() {
		Route route1 = routeTestUtil.createPersistedRoute();
		Filter filter = RouteDatastoreFilterBuilder.create().start(route1.start())
				.activityTypes(ActivityTypeStatistic.activityTypes(route1.activityTypeStats()))

				// .activityTypePercent(0, 10, 20)
				.applications(ApplicationStats.categories(route1.applicationStats())).mains(route1.isMain()).build();
		Routes routes = this.routeRepository.filter(filter);
		TestUtil.assertContains(route1, routes);
	}

	@Test
	public void testMainsApplicationIndex() {
		Route route1 = routeTestUtil.createPersistedRoute();
		RouteDatastoreFilterBuilder.create()
				.activityTypes(ActivityTypeStatistic.activityTypes(route1.activityTypeStats()))
				.applications(ApplicationStats.categories(route1.applicationStats())).mainsOnly().build();
	}

	/**
	 * @see MainsForTrailheadStartActivityTypeIndex
	 */
	@Test
	@Ignore("TN-804 moved to search service")
	public void testStartActivityMains() {
		Pair<Route, Filter> pair = activityTypeFilter();
		Filter filter = RouteDatastoreFilterBuilder.clone(pair.getRight()).mainsOnly().build();
		Routes actual = this.routeRepository.filter(filter);
		assertFalse("make sure not a main", pair.getLeft().isMain());
		// route isn't a main.
		TestUtil.assertEmpty(actual);
	}

	public Pair<Route, Filter> activityTypeFilter() {
		Route route = routeTestUtil.createPersistedRoute();
		Filter filter = RouteDatastoreFilterBuilder.create()
				.activityTypes(ActivityTypeStatistic.activityTypes(route.activityTypeStats())).start(route.start())
				.build();
		// doesn't matter where, just that it is there to be ignored.
		filter = TrailheadTestUtil.addLocationFilter(filter, GeoCellTestUtil.A_THRU_F_WITH_PARENTS);
		FieldAnnotationTestUtil.getAnnoationFactory(this.routeRepository.getFieldDictionaryFactory())
				.getDictionary(Trailhead.class);

		return Pair.of(route, filter);
	}

	public void assertActivityTypes(Set<Route> expected, ActivityType... activityTypes) {
		assertActivityTypes(expected, Sets.newHashSet(activityTypes));
	}

	public void assertActivityTypes(Set<Route> expected, Iterable<ActivityType> activityTypes) {
		Filter filter = RouteDatastoreFilterBuilder.create().activityTypes(activityTypes).build();
		Routes found = getFilterRepository().filter(filter);
		assertEquals(expected, Sets.newHashSet(found));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected Route createEntity() {
		return routeTestUtil.createPersistedRoute();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected RouteRepository getRepository() {
		return this.routeRepository;
	}
}
