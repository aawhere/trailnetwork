/**
 * 
 */
package com.aawhere.route.event;

import org.joda.time.Interval;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.route.Route;
import com.aawhere.route.RouteServiceTestUtil;

/**
 * @author aroller
 * 
 */
public class RouteEventServiceTestUtil {

	private RouteServiceTestUtil util;
	private RouteEventService service;
	private RouteEventObjectifyRepository repository;

	/**
	 * @return the service
	 */
	public RouteEventService service() {
		return this.service;
	}

	/**
	 * @return the repository
	 */
	public RouteEventObjectifyRepository repository() {
		return this.repository;
	}

	public RouteEvent routeEvent() {
		return routeEventBuilder().build();

	}

	/**
	 * @param plus
	 * @return
	 */
	public RouteEvent routeEvent(Interval interval) {
		return routeEventBuilder().interval(interval).build();
	}

	/**
	 * Used to construct all instances of RouteEventServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<RouteEventServiceTestUtil> {

		public Builder() {
			super(new RouteEventServiceTestUtil());
		}

		public Builder routeTestUtil(RouteServiceTestUtil routeServiceTestUtil) {
			building.util = routeServiceTestUtil;
			return this;
		}

		@Override
		public RouteEventServiceTestUtil build() {
			RouteEventServiceTestUtil built = super.build();
			built.service = built.util.getRouteEventService();
			built.repository = (RouteEventObjectifyRepository) built.util.getRouteEventRepository();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct RouteEventServiceTestUtil */
	private RouteEventServiceTestUtil() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @param bike
	 */
	public RouteEvent.Builder routeEventBuilder() {
		// it's nice to use the persisted route, not the bogus one
		final Route route = this.util.createPersistedRoute();
		return routeEventBuilder(route);
	}

	/**
	 * @param route
	 * @return
	 */
	public RouteEvent.Builder routeEventBuilder(final Route route) {
		final RouteEvent event = RouteEventTestUtil.event(route);
		return routeEventBuilder(event);
	}

	/**
	 * @param event
	 * @return
	 */
	private RouteEvent.Builder routeEventBuilder(final RouteEvent event) {
		final RouteEvent.Builder builder = new RouteEvent.Builder(event) {
			/*
			 * (non-Javadoc)
			 * @see com.aawhere.route.event.RouteEvent.Builder#build()
			 */
			@Override
			public RouteEvent build() {
				final RouteEvent built = super.build();
				return repository.store(built);
			}
		};
		return builder;
	}

}
