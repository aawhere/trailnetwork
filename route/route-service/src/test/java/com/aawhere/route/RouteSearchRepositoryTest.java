package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.field.FieldKey;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityTestUtil;
import com.aawhere.persist.Filter;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocument.Field;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.PersistenceTest;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadTestUtil;
import com.google.common.collect.ImmutableList;

@Category(PersistenceTest.class)
public class RouteSearchRepositoryTest
		extends RouteObjectifyRepositoryTest {

	private RouteServiceTestUtil routeServiceTestUtil;
	private SearchService searchService;

	@Override
	protected RouteRepository createRepository() {
		this.routeServiceTestUtil = RouteServiceTestUtil.create().build();
		this.searchService = routeServiceTestUtil.getSearchService();
		return new RouteSearchRepository(routeServiceTestUtil.getRouteObjectifyRepository(), searchService,
				routeServiceTestUtil.getTrailheadServiceTestUtil().service());
	}

	@Override
	public void assertStored(Route sample) {
		// FIXME: validate sample is in the index using the filter builder
		super.assertStored(sample);

		ImmutableList<Filter> routeIdFilterForAllIndexes = routeIdFilterForAllIndexes(sample);
		EntityTestUtil.assertSizeForEach(1, routeIdFilterForAllIndexes, getRepository());
	}

	private ImmutableList<Filter> routeIdFilterForAllIndexes(Route sample) {
		com.google.common.collect.ImmutableList.Builder<Filter> filterListBuilder = ImmutableList.builder();
		RouteId routeId = sample.id();
		Filter filter = RouteSearchFilterBuilder.create().routeId(routeId).build();
		filterListBuilder.add(filter);
		Set<ActivityType> activityTypes = ActivityTypeStatistic.activityTypes(sample.completionStats()
				.activityTypeStats());
		for (ActivityType activityType : activityTypes) {
			Filter activityTypeFilter = RouteSearchFilterBuilder.create().activityType(activityType).routeId(routeId)
					.build();
			filterListBuilder.add(activityTypeFilter);
		}

		// TODO: validate person

		ImmutableList<Filter> routeIdFilterForAllIndexes = filterListBuilder.build();
		return routeIdFilterForAllIndexes;
	}

	@Override
	protected void assertDeleted(Route route) throws EntityNotFoundException {
		super.assertDeleted(route);
		EntityTestUtil.assertSizeForEach(0, routeIdFilterForAllIndexes(route), getRepository());
	}

	/**
	 * when a route is once worthy, but then is demoted to not be worthy the existing documents need
	 * to be removed. This can happen to all docs or in more common cases when an activity type was
	 * previously worthy, but then not. TN-880
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testOrphansRemoved() throws EntityNotFoundException {
		Route route = getEntitySamplePersisted();
		ActivityType oldActivityType = ActivityType.BIKE;
		// setup the old entries that will leave the orphans
		CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> counter = ActivityTypeStatistic
				.counter(ImmutableList.of(oldActivityType, oldActivityType, oldActivityType)).build();
		RouteStats oldStats = RouteStats.routeStatsCreator().activityTypeStats(counter.stats()).build();
		route = getRepository().update(route.id()).completionStats(oldStats).build();
		Filter oldActivityTypeFilter = RouteSearchFilterBuilder.create().routeId(route.id())
				.activityType(oldActivityType).build();
		TestUtil.assertContainsOnly(route, getRepository().filter(oldActivityTypeFilter));
		// the activity type switches to MTB so BIKE has to be removed.
		{
			ActivityType newActivityType = ActivityType.MTB;
			CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> newCounter = ActivityTypeStatistic
					.counter(ImmutableList.of(newActivityType, newActivityType, newActivityType)).build();
			RouteStats newStats = RouteStats.routeStatsCreator().activityTypeStats(newCounter.stats()).build();
			// this update should add the new search document entries for MTB and remove the old for
			// BIKE
			route = getRepository().update(route.id()).completionStats(newStats).build();
			TestUtil.assertEmpty(getRepository().filter(oldActivityTypeFilter));
			Filter newActivityTypeFilter = RouteSearchFilterBuilder.create().routeId(route.id())
					.activityType(newActivityType).build();
			TestUtil.assertContainsOnly(route, getRepository().filter(newActivityTypeFilter));
		}
	}

	/**
	 * Tests that a field is updated when the route is updated. Start id was chosen specifically to
	 * investigate a bug related to the start id (TN-891).
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	public void testStartIdUpdated() throws EntityNotFoundException {
		Route route = getEntitySamplePersisted();
		TrailheadId orginalStartTrailheadId = route.start();
		assertStartId(orginalStartTrailheadId);

		TrailheadId mutatedStartTrailheadId = TrailheadTestUtil.trailheadId();
		route = getRepository().update(route.id()).start(mutatedStartTrailheadId).build();
		assertStartId(mutatedStartTrailheadId);

		TrailheadId startAfterSet = TrailheadTestUtil.trailheadId();
		// use the set method on the repository
		getRepository().setStart(route.id(), startAfterSet);
		assertStartId(startAfterSet);
	}

	public void assertStartId(TrailheadId expectedStartTrailheadId) {
		FieldKey startTrailheadIdFieldKey = RouteSearchDocumentProducer.START_TRAILHEAD_ID;
		Filter searchFilter = RouteSearchFilterBuilder.create().addField(startTrailheadIdFieldKey).build();
		SearchDocuments searchDocuments = this.searchService.searchDocuments(searchFilter);
		SearchDocument document = searchDocuments.iterator().next();
		assertNotNull(document);
		Field startField = SearchUtils.findField(document, startTrailheadIdFieldKey);
		assertNotNull(document.fields().toString(), startField);
		assertEquals(expectedStartTrailheadId, startField.value());
	}
}
