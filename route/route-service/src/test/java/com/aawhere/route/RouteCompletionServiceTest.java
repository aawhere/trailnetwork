/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.lang.number.NumberUtilsExtended.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportException;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.ExampleTracksImportServiceTestUtil;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingServiceTestUtil;
import com.aawhere.collections.IterablesExtended;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonServiceTestUtil;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventTestUtil;
import com.aawhere.test.TestUtil;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class RouteCompletionServiceTest {

	private RouteServiceTestUtil routeUtil;
	private ExampleTracksImportServiceTestUtil examples;
	private RouteCompletionService service;
	private RouteService routeService;
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private RouteCompletionObjectifyRepository repository;
	private RouteCompletionServiceTestUtil completionUtil;

	@Before
	public void setUp() {
		helper.setUp();
		this.routeUtil = RouteServiceTestUtil.create().examplesPrepared().build();
		this.examples = this.routeUtil.exampleTracksUtil();
		this.service = this.routeUtil.getRouteCompletionService();
		this.routeService = this.routeUtil.getRouteService();
		this.repository = this.routeUtil.getRouteCompletionRepository();
		this.completionUtil = RouteCompletionServiceTestUtil.create().routeUtil(routeUtil).build();
	}

	@After
	public void tearDownService() {
		helper.tearDown();
	}

	@Test
	public void testAllTogether() throws EntityNotFoundException, CourseNotWorthyException, ActivityImportException,
			CoursesNotRelatedException {
		// FIXME:this setup should be done and each of the tests is their own method?
		Activity course = examples.line();
		ActivityId courseId = course.id();
		// goes around twice crossing over line twice
		final Activity loop = examples.loopDouble();
		final Activity reciprocal = examples.lineReciprocal();
		final ActivityId reciprocalId = reciprocal.id();
		final ActivityId loopId = loop.id();
		final ActivityId lineBeforeId = examples.lineBefore().id();
		final ActivityId beforeAndAfterId = examples.beforeAndAfter().id();

		RouteCompletionOrganizer organizer = this.routeUtil.routeUpdated(	courseId,
																			reciprocalId,
																			lineBeforeId,
																			examples.lineLonger().id(),
																			beforeAndAfterId,
																			loopId).organizer();
		Route route = this.routeService.route(courseId);
		RouteId routeId = route.id();
		RouteCompletions completions = service.completions(route.id(), Filter.create().build());
		final Integer expectedNumberOfCompletions = 7;
		TestUtil.assertSize("6 activities, but double loop gets two completions " + organizer,
							expectedNumberOfCompletions,
							completions);
		Integer expectedNumberOfCourses = 2;
		// let's confirm a few of expected completions..
		final RouteCompletion courseCompletion = service.completion(new RouteCompletionId(route.id(), courseId));
		assertTrue("course should be identified as a course", courseCompletion.isCourse());
		assertTrue(	"reciprocal should also complete",
					service.completion(new RouteCompletionId(route.id(), reciprocalId)).isCourse());
		assertEquals(	"everyone completes course but itself. second repeat of double loop doesn't count",
						I(expectedNumberOfCompletions - 2),
						courseCompletion.courseActivityCount());
		assertTrue(courseCompletion.isCourse());
		assertEquals("course completed it's reciprocal", I(1), courseCompletion.coursesCompletedCount());
		final Integer firstLap = 1;
		final Integer secondLap = 2;
		final RouteCompletion firstLapCompletion = service.completion(new RouteCompletionId(route.id(), loopId, 1));
		assertEquals("lap 1 for the loop", firstLap, firstLapCompletion.repeatCount());
		assertFalse("loop is not a course", firstLapCompletion.isCourse());
		assertFalse("nobody completed the double loop", firstLapCompletion.isCourse());
		assertNull(firstLapCompletion.courseActivityCount());
		assertEquals(expectedNumberOfCourses, firstLapCompletion.coursesCompletedCount());
		final RouteCompletion secondLapCompletion = service.completion(new RouteCompletionId(route.id(), loopId,
				secondLap));
		assertEquals("lap 2 for the loop", secondLap, secondLapCompletion.repeatCount());
		JodaTestUtil.assertBefore(firstLapCompletion.startTime(), secondLapCompletion.startTime());
		assertFalse("loop is not a course", secondLapCompletion.isCourse());

		assertNotNull("route is provided since it is the parent", courseCompletion.routeId());

		// filter those that claim to be courses
		RouteCompletions courses = service
				.completions(routeId, RouteCompletionFilterBuilder.create().courses().build());
		TestUtil.assertIterablesEquals(	"courses",
										Sets.newHashSet(courseId, reciprocalId),
										RouteCompletionUtil.activityIds(courses));

		Route routeByCourseId = this.routeService.route(courseId);
		assertEquals("looking for a route that matches a course should find the route", route, routeByCourseId);
		// now test sorting
		RouteCompletions mostRecentFirst = service.completions(routeId, RouteCompletionFilterBuilder.create()
				.orderByMostRecentlyRecorded().filter());
		TestUtil.assertSize("sorting shouldn't lose any", expectedNumberOfCompletions, mostRecentFirst);
		// testing for most recent is not known because examples adjusts the timestamps.

		for (RouteCompletion routeCompletion : completions) {
			String message = routeCompletion.toString();
			assertNotNull(message, routeCompletion.socialRaceRank());
			assertNotNull(message, routeCompletion.personalRaceRank());
			assertNotNull(message, routeCompletion.personalOrder());
			// social count rank is only assigned if mutliple counts
			if (routeCompletion.personalTotal() >= RouteCompletionOrganizer.Organizer.MIN_TOTAL_FOR_SOCIAL_COUNT_RANK) {
				assertNotNull(message, routeCompletion.socialCountRank());
			}

			assertNotNull(message, routeCompletion.personalTotal());
			assertNotNull(message, routeCompletion.socialOrder());

			if (routeCompletion.isCourse()) {
				assertNotNull(routeCompletion.courseActivityCount());
			}
			assertNotNull(routeCompletion.coursesCompletedCount());

		}

		// test that a persons in the completion can be found by name
		// TN-682 We need search service before we can continue with this.
		// {
		// PersonId courseCompletionPersonId = courseCompletion.personId();
		// Person courseCompletionPerson = this.routeUtil.getRouteService().personService
		// .person(courseCompletionPersonId);
		// final RouteId nameSearchRouteId = courseCompletion.routeId();
		// final String matchingName = courseCompletionPerson.name();
		// Persons personsMatchingName = this.service.personsMatchingName(nameSearchRouteId,
		// matchingName);
		// // the likelihood of a double matching account alias is so low because of
		// // TestUtil.random alphanumeric
		// TestUtil.assertContainsOnly(courseCompletionPerson, personsMatchingName);
		// // confirm it works for query on a filter
		// TestUtil.assertContainsOnly(courseCompletionPerson,
		// this.routeService.persons( nameSearchRouteId,
		// Filter.create().setQuery(matchingName)
		// .build()));
		// final String nonMatchingName = "|";
		// TestUtil.assertEmpty(this.service.personsMatchingName(nameSearchRouteId,
		// nonMatchingName));
		// TestUtil.assertEmpty(this.routeService.persons(nameSearchRouteId,
		// Filter.create().setQuery(nonMatchingName)
		// .build()));
		// }

		// query by person and sort by order
		{
			RouteCompletion courseForPersonSort = courseCompletion;
			Person expectedPersonForSort = personUtil().getPersonService().person(courseForPersonSort.personId());
			RouteCompletions mostRecentForPerson = this.service.completions(RouteCompletionFilterBuilder.create()
					.person(expectedPersonForSort).orderBySocialOrder().build());
			TestUtil.assertContainsOnly(courseForPersonSort, mostRecentForPerson);
		}
	}

	@Test
	public void testMergeOfTwoCompletions() throws EntityNotFoundException {
		RouteCompletion from = this.routeUtil.routeCompletion(this.examples.line());
		RouteCompletion into = this.routeUtil.routeCompletion(this.examples.lineReciprocal());
		this.service.merge(from.routeId(), route(into));
		final Iterable<RouteCompletion> mergedCompletions = this.service.completions(into.routeId());
		TestUtil.assertSize(2, mergedCompletions);
		TestUtil.assertContains(into, mergedCompletions);
		TestUtil.assertContains(new RouteCompletionId(into.getRouteId(), from.activityId()),
								IdentifierUtils.ids(mergedCompletions));
		TestUtil.assertEmpty("merged didn't remove orphans", this.service.completions(from.routeId()));
	}

	@Test
	public void testMergeOfTwoCompletionsWithSameActivityId() throws EntityNotFoundException {

		final Activity activity = this.examples.line();
		RouteCompletion from = this.routeUtil.routeCompletion(activity);
		RouteCompletion into = this.routeUtil.routeCompletion(activity);
		assertEquals(from + " not matching " + into, from.activityId(), into.activityId());
		this.service.merge(from.routeId(), route(into));
		final Iterable<RouteCompletion> mergedCompletions = this.service.completions(into.routeId());
		TestUtil.assertSize("activity id must be unique", 1, mergedCompletions);
		RouteCompletion mergedCompletion = mergedCompletions.iterator().next();
		TestUtil.assertNotEquals("the completion id should have changed", from, mergedCompletion);

		TestUtil.assertContains("all things equal, keep the existing completion", into, mergedCompletions);
		TestUtil.assertDoesntContain(from, mergedCompletions);
		TestUtil.assertEmpty("merged didn't remove orphans", this.service.completions(from.routeId()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testMergeOfCompletelyDifferentRoutes() throws EntityNotFoundException {
		Activity line = this.examples.line();
		Activity realCourse = this.examples.realCourse();

		RouteCompletion usual = this.routeUtil.routeCompletion(line);
		RouteCompletion wayOff = this.routeUtil.routeCompletion(realCourse);
		this.service.merge(wayOff.getRouteId(), route(usual));
	}

	/**
	 * Handy since route completion does not have a fully populated route, but only a route id.
	 * 
	 * @param completion
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Route route(RouteCompletion completion) throws EntityNotFoundException {
		return routeService.route(completion.routeId());
	}

	@Test
	public void testMergeOfTwoCompletionsWithSameActivityCourseWins() throws EntityNotFoundException {
		final Activity activity = this.examples.line();

		RouteCompletion from = this.routeUtil.routeCompletion(activity);
		from = this.routeUtil.getRouteCompletionRepository()
				.update(RouteCompletion.mutate(from).isCourse(true).build());
		RouteCompletion into = this.routeUtil.routeCompletion(activity);
		this.service.merge(from.routeId(), route(into));
		final Iterable<RouteCompletion> mergedCompletions = this.service.completions(into.routeId());
		TestUtil.assertContainsOnly("id should have been inheritied", into, mergedCompletions);
		assertTrue("is course was an attribute of from, but now is kept", mergedCompletions.iterator().next()
				.isCourse());
	}

	/**
	 * @return
	 */
	private ActivityTimingServiceTestUtil timingTestUtil() {
		return this.routeUtil.getTimingRelationServiceTestUtil();
	}

	@Test
	public void testPersonCompletions() throws EntityNotFoundException {
		final Activity line = this.examples.line();
		RouteCompletion target = this.routeUtil.routeCompletionRelated(line);
		this.personUtil().getPersonService().person(target.personId());
		RouteCompletion decoy = this.routeUtil.routeCompletionRelated(this.examples.lineBefore());
		TestUtil.assertNotEquals(target.personId(), decoy.personId());
		final Filter emptyFilter = Filter.create().build();
		TestUtil.assertContainsOnly(target, service.completions(target.personId(), emptyFilter));
		// now merge the persons and try again
		Person merged = personUtil().getPersonService().merged(Lists.newArrayList(target.personId(), decoy.personId()));
		TestUtil.assertIterablesEquals(	"aliases now, the results should be the same",
										service.completions(target.personId(), emptyFilter),
										service.completions(decoy.personId(), emptyFilter));

	}

	@Test
	public void testPersonCompletionsWithEvents() throws BaseException {
		RouteCompletion completionWithEvent = this.routeUtil.routeCompletionRelated();
		completionWithEvent = this.completionUtil.routeCompletionBuilder(completionWithEvent)
				.routeScore(RouteTestUtils.scoreWorthy()).routeEventId(RouteEventTestUtil.eventId()).build();
		final PersonId person = completionWithEvent.getPerson();
		RouteCompletion completionWithoutEvent = this.completionUtil.routeCompletionBuilder()
				.routeScore(RouteTestUtils.scoreWorthy()).person(person).routeEventId(null).build();
		RouteCompletion completionForOther = this.completionUtil.routeCompletionBuilder()
				.routeScore(RouteTestUtils.scoreWorthy()).routeEventId(completionWithEvent.getRouteEvent()).build();

		RouteCompletions completionsWithEvents = this.service.completionsWithEvents(person, Filter.create().build());
		TestUtil.assertContains(completionWithEvent, completionsWithEvents);
		TestUtil.assertDoesntContain(completionWithoutEvent, completionsWithEvents);
		TestUtil.assertDoesntContain(completionForOther, completionsWithEvents);
	}

	@Test
	public void testPersonalAllTimeMostPopular() throws BaseException {
		RouteCompletion mostPopular = this.routeUtil.routeCompletionRelated();
		RouteCompletion lessPopular = this.routeUtil.routeCompletionRelated();
		RouteCompletion notPopularEnough = this.routeUtil.routeCompletionRelated();
		RouteCompletion notMostRecent = this.routeUtil.routeCompletionRelated();
		RouteCompletion wrongPerson = this.routeUtil.routeCompletionRelated();
		RouteCompletionObjectifyRepository repository = this.routeUtil.getRouteCompletionRepository();
		Integer higher = RouteCompletion.MIN_TOTAL_FOR_FAVORITE * 3;
		Integer lower = RouteCompletion.MIN_TOTAL_FOR_FAVORITE * 2;
		Integer tooLow = RouteCompletion.MIN_TOTAL_FOR_FAVORITE - 1;
		PersonId targetPersonId = mostPopular.personId();
		mostPopular = repository.update(RouteCompletion.mutate(mostPopular).personalTotal(higher).personalOrder(higher)
				.routeScore(RouteTestUtils.scoreWorthy()).person(targetPersonId).build());
		lessPopular = repository.update(RouteCompletion.mutate(lessPopular).personalTotal(lower).personalOrder(lower)
				.routeScore(RouteTestUtils.scoreWorthy()).person(targetPersonId).build());
		notPopularEnough = repository.update(RouteCompletion.mutate(notPopularEnough).personalTotal(tooLow)
				.routeScore(RouteTestUtils.scoreWorthy()).personalOrder(tooLow).person(targetPersonId).build());
		notMostRecent = repository.update(RouteCompletion.mutate(notMostRecent).personalTotal(higher)
				.routeScore(RouteTestUtils.scoreWorthy()).personalOrder(lower).person(targetPersonId).build());
		wrongPerson = repository.update(RouteCompletion.mutate(wrongPerson).personalTotal(higher).personalOrder(higher)
				.routeScore(RouteTestUtils.scoreWorthy()).build());
		final Filter filter = Filter.create().build();
		// Personal
		RouteCompletions mostPopularAllTime = this.service.mostPopularAllTime(targetPersonId, filter);
		final ArrayList<RouteCompletion> expectedForTarget = Lists.newArrayList(mostPopular, lessPopular);
		TestUtil.assertIterablesEquals("only expecting most and less popular", expectedForTarget, mostPopularAllTime);
		assertEquals("most popular should be first", mostPopular, Iterables.getFirst(mostPopularAllTime, null));
		// GROUP
		// TN-756 objectify 5 introduced a failure to this sorting
		// RouteCompletions mostPopularForGroup = this.service.mostPopularAllTime(Lists
		// .newArrayList(targetPersonId, wrongPerson.personId()), filter);
		// Iterable<RouteCompletion> expectedForGroup = Iterables.concat( expectedForTarget,
		// Lists.newArrayList(wrongPerson));
		// TestUtil.assertIterablesEquals( "same as other, but should include wrong person",
		// expectedForGroup,
		// mostPopularForGroup);
		// // ALL
		// RouteCompletions mostPopularForAll =
		// this.service.completions(RouteCompletionFilterBuilder.create()
		// .orderByPersonalTotal().filter());
		// TestUtil.assertIterablesEquals(
		// "same as group, but we didn't ask for anyone in particular",
		// expectedForGroup,
		// mostPopularForAll);

	}

	/**
	 * TN-778
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testBestRanksForPerson() throws EntityNotFoundException {
		// should come first
		Person person = routeUtil.getPersonTestUtil().persistedPerson();
		PersonId personId = person.id();
		RouteCompletion rank1 = this.completionUtil.routeCompletionBuilder().routeScore(RouteTestUtils.scoreWorthy())
				.socialRaceRank(1).personalRaceRank(1).person(person).build();
		// should come second
		RouteCompletion rank2 = this.completionUtil.routeCompletionBuilder().routeScore(RouteTestUtils.scoreWorthy())
				.socialRaceRank(2).personalRaceRank(1).person(personId).build();
		RouteCompletion notPersonalBest = this.completionUtil.routeCompletionBuilder()
				.routeScore(RouteTestUtils.scoreWorthy()).socialRaceRank(2).personalRaceRank(2).person(personId)
				.build();
		RouteCompletion wrongPerson = this.completionUtil.routeCompletionBuilder()
				.routeScore(RouteTestUtils.scoreWorthy()).socialRaceRank(1).personalRaceRank(1).build();
		// demonstrates what happens with low scoring completions
		RouteCompletion lowScore = this.completionUtil.routeCompletionBuilder().routeScore(Route.DEFAULT_SCORE)
				.socialRaceRank(1).personalRaceRank(1).person(personId).build();
		RouteCompletions completionsRankingBest = this.service.completionsRankingBest(personId);
		TestUtil.assertContains(rank1, completionsRankingBest);
		TestUtil.assertContains(rank2, completionsRankingBest);
		TestUtil.assertDoesntContain(notPersonalBest, completionsRankingBest);
		TestUtil.assertDoesntContain(wrongPerson, completionsRankingBest);
		// previously ignored, these are now included since TN-804
		TestUtil.assertContains(lowScore, completionsRankingBest);

	}

	@Test(expected = EntityNotFoundException.class)
	public void testPersonNotFoundCompletions() throws EntityNotFoundException {
		this.service.completions(PersonTestUtil.createPersonId(), Filter.create().filter());
	}

	/**
	 * @return
	 * 
	 */
	private PersonServiceTestUtil personUtil() {
		return routeUtil.getTimingRelationServiceTestUtil().getImportTestUtil().getActivityServiceTestUtil()
				.getTrackTestUtil().getDocumentTestUtil().getPersonServiceTestUtil();
	}

	@Test
	public void testRepeat() throws EntityNotFoundException, ActivityImportException, CoursesNotRelatedException {
		Activity course = this.examples.line();
		Activity loopDouble = this.examples.loopDouble();
		timingTestUtil().importAndRelate(course.id(), loopDouble.id());

		final ActivityTimingId timingId = new ActivityTimingId(course.id(), loopDouble.id());
		Integer expectedCompletionsCount = 2;
		ActivityTiming timing = this.routeUtil.getTimingRelationServiceTestUtil().getActivityTimingRelationService()
				.getTiming(timingId);
		TestUtil.assertSize("the timing didn't find a double match", expectedCompletionsCount, timing.completions());
		RouteCompletions completions = this.routeService.completionsUpdated(timingId);
		TestUtil.assertSize("both completions of the loop should be created", expectedCompletionsCount, completions);

		List<RouteCompletion> sortedCopy = RouteCompletionUtil.orderedByOccurrence().sortedCopy(completions);
		RouteCompletion first = Iterables.getFirst(sortedCopy, null);
		RouteCompletion last = Iterables.getLast(sortedCopy);
		TestUtil.assertNotEquals(last.id(), first.id());
		TestUtil.assertContains(last.id().toString(), first.id().toString());

		Route route = this.routeService.route(course.id());
		this.routeUtil.getRouteService().routeUpdated(route.id()).organizer();
		// reload all to make sure course is included
		completions = this.service.completions(route.id(), Filter.create().filter());
		TestUtil.assertSize(3, completions);

		// reload them to ensure storage/retriaval is correct since IfDefaultRepeatTotal, etc
		first = this.service.completion(first.id());
		last = this.service.completion(last.id());
		assertEquals(1, first.repeatCount().intValue());
		assertEquals(2, last.repeatCount().intValue());

		assertEquals(2, first.repeatTotal().intValue());
		assertEquals(2, last.repeatTotal().intValue());

		assertEquals(1, first.personalOrder().intValue());
		assertEquals(2, last.personalOrder().intValue());

		assertEquals(2, first.personalTotal().intValue());
		assertEquals(2, last.personalTotal().intValue());
	}

	/**
	 * Granular test making sure the completions and course completions returned are only course
	 * completions from multiple routes.
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	public void testCompletionsByRouteIds() throws EntityNotFoundException {
		// setup two routes, each with two courses and one completion that is not a course
		RouteCompletion courseCompletion1Route1 = this.completionUtil.routeCompletionBuilder().isCourse(true).build();
		final Route route1 = route(courseCompletion1Route1);
		RouteCompletion courseCompletion2Route1 = this.completionUtil.routeCompletionBuilder(route1).isCourse(true)
				.build();
		RouteCompletion completion1Route1 = this.completionUtil.routeCompletionBuilder(route1).isCourse(false).build();

		RouteCompletion courseCompletion1Route2 = this.completionUtil.routeCompletionBuilder().isCourse(true).build();
		final Route route2 = route(courseCompletion1Route2);
		RouteCompletion courseCompletion2Route2 = this.completionUtil.routeCompletionBuilder(route2).isCourse(true)
				.build();
		RouteCompletion completion1Route2 = this.completionUtil.routeCompletionBuilder(route2).isCourse(false).build();

		ArrayList<RouteCompletion> route1Courses = Lists.newArrayList(courseCompletion1Route1, courseCompletion2Route1);
		ArrayList<RouteCompletion> route2Courses = Lists.newArrayList(courseCompletion1Route2, courseCompletion2Route2);
		TestUtil.assertIterablesEquals("route 1", route1Courses, service.courseCompletions(route1.id()));
		TestUtil.assertIterablesEquals("route 2", route2Courses, service.courseCompletions(route2.id()));
		final ArrayList<RouteId> bothRouteIds = Lists.newArrayList(route1.id(), route2.id());
		TestUtil.assertIterablesEquals(	"route 1&2",
										Iterables.concat(route1Courses, route2Courses),
										service.courseCompletions(bothRouteIds));
		final Iterable<RouteCompletion> route1Completions = IterablesExtended.concat(route1Courses, completion1Route1);
		TestUtil.assertIterablesEquals("route 1 all", route1Completions, service.completions(route1.id()));
		final Iterable<RouteCompletion> route2Completions = IterablesExtended.concat(route2Courses, completion1Route2);
		TestUtil.assertIterablesEquals("route 2 all", route2Completions, service.completions(route2.id()));
		TestUtil.assertIterablesEquals(	"route 1&2 all",
										Iterables.concat(route1Completions, route2Completions),
										service.completions(bothRouteIds));

	}

	@Test
	public void testCourseIds() throws EntityNotFoundException {
		RouteCompletion course1 = this.routeUtil.routeCompletion();
		this.repository.setIsCourse(course1.id(), true);
		RouteCompletion course2 = this.routeUtil.routeCompletion();
		this.repository.setIsCourse(course2.id(), true);
		RouteCompletion nonCourse = this.routeUtil.routeCompletion();
		this.repository.setIsCourse(nonCourse.id(), false);
		{
			final Iterable<ActivityId> allGiven = this.service.courseIds(Lists.newArrayList(course1.routeId(),
																							course2.routeId(),
																							nonCourse.routeId()));
			TestUtil.assertContains(course1.activityId(), allGiven);
			TestUtil.assertContains(course2.activityId(), allGiven);
			TestUtil.assertDoesntContain(nonCourse.activityId(), allGiven);
		}

		{
			final Iterable<ActivityId> course1Given = this.service.courseIds(Lists.newArrayList(course1.routeId(),

			nonCourse.routeId()));
			TestUtil.assertContains(course1.activityId(), course1Given);
			TestUtil.assertDoesntContain(course2.activityId(), course1Given);
			TestUtil.assertDoesntContain(nonCourse.activityId(), course1Given);
		}

	}

	@Test
	public void testPersonIds() throws BaseException {
		RouteCompletion completion1 = this.routeUtil.routeCompletionRelated();
		RouteCompletion completion2 = this.routeUtil.routeCompletionRelated();
		RouteCompletion completion3 = this.routeUtil.routeCompletionRelated();
		final ArrayList<PersonId> personsFor1And2 = Lists.newArrayList(completion1.personId(), completion2.personId());
		Iterable<RouteCompletion> completionsForPersons1And2 = this.service.completionsForPersons(personsFor1And2);
		TestUtil.assertContainsOnly("only searched for 1 and 2", completionsForPersons1And2, completion1, completion2);
		TestUtil.assertContainsOnly("only searched for 2 and 3", service.completionsForPersons(Lists
				.newArrayList(completion2.personId(), completion3.personId())), completion2, completion3);
		TestUtil.assertSize("searched for 4, but one was a duplicate",
							3,
							service.completionsForPersons(Lists.newArrayList(	completion1.personId(),
																				completion2.personId(),
																				completion3.personId(),
																				completion1.personId())));

		Iterable<RouteId> routeIdsFor1And2 = service.routeIds(personsFor1And2);
		TestUtil.assertContainsOnly("expecting only 1 and 2",
									routeIdsFor1And2,
									completion1.routeId(),
									completion2.routeId());
	}

	@Test(expected = EntityNotFoundException.class)
	public void testPersonIdsNotFound() throws EntityNotFoundException {
		this.service.completionsForPersons(Lists.newArrayList(PersonTestUtil.createPersonId()));
	}

	@Test(expected = EntityNotFoundException.class)
	public void testPersonIdsOneNotFound() throws EntityNotFoundException {
		RouteCompletion found = this.routeUtil.routeCompletion();
		RouteCompletion decoy = this.routeUtil.routeCompletion();
		PersonId notFoundPersonId = PersonTestUtil.createPersonId();
		TestUtil.assertNotEquals(decoy.personId(), notFoundPersonId);
		this.service.completions(found.personId(), notFoundPersonId);
	}

	@Test(expected = InvalidArgumentException.class)
	public void testPersonIdsNoneGiven() throws EntityNotFoundException {
		// must persist to prove none are coming back
		this.routeUtil.routeCompletion();
		TestUtil.assertEmpty(this.service.completions());
	}

	@Test(expected = InvalidArgumentException.class)
	public void testRouteIdsForPersonNoneGiven() throws EntityNotFoundException {
		this.routeUtil.routeCompletion();
		final List<PersonId> emptyList = Collections.emptyList();
		TestUtil.assertEmpty(this.service.routeIds(emptyList));
	}

	/**
	 * Ensures the index is properly storing personal totals only for the most recent and that a
	 * query can be written. WW-160
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testPersonalTotalOrder() throws EntityNotFoundException {
		Person person1 = this.personUtil().persistedPerson();
		Person person2 = this.personUtil().persistedPerson();
		Integer person1PersonalTotal = RouteCompletion.MIN_TOTAL_FOR_FAVORITE + 10;
		Integer person2PersonalTotal = person1PersonalTotal - 1;
		Route route = this.routeUtil.createPersistedRoute();
		Route routeDecoy = this.routeUtil.createPersistedRoute();
		Integer person1CountRank = 1;
		RouteCompletion mostRecentCompletionPerson1 = this.completionUtil.routeCompletionBuilder(route).person(person1)
				.socialCountRank(person1CountRank).personalTotal(person1PersonalTotal)
				.personalOrder(person1PersonalTotal).build();
		// only the most recent order is indexed
		RouteCompletion leastRecentCompletionPerson1 = this.completionUtil.routeCompletionBuilder(route)
				.socialCountRank(person1CountRank).person(person1).personalTotal(person1PersonalTotal)
				.personalOrder(person1PersonalTotal - 1).build();
		// this should be second in the list since personal total is less
		RouteCompletion mostRecentCompletionPerson2 = this.completionUtil.routeCompletionBuilder(route).person(person2)
				.socialCountRank(person1CountRank + 1).personalTotal(person2PersonalTotal)
				.personalOrder(person2PersonalTotal).build();
		// only the most recent order is indexed

		// ensures the route is being respected in the query.
		RouteCompletion decoyCompletionPerson1 = this.completionUtil.routeCompletionBuilder(routeDecoy).person(person1)
				.socialCountRank(person1CountRank).personalTotal(person1PersonalTotal)
				.personalOrder(person1PersonalTotal).build();

		Filter filter = RouteCompletionFilterBuilder.create().orderBySocialCountRank().route(route).build();
		RouteCompletions completions = this.service.completions(filter);
		TestUtil.assertSize(2, completions);
		assertEquals(mostRecentCompletionPerson1, completions.iterator().next());
		assertEquals(mostRecentCompletionPerson2, Iterables.getLast(completions));
		TestUtil.assertDoesntContain(leastRecentCompletionPerson1, completions);
		TestUtil.assertDoesntContain(decoyCompletionPerson1, completions);
	}

	/**
	 * Provides validation that events are being created. Logic testing is left up to
	 * {@link RouteCompletionOrganizerUnitTest}.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	@Ignore("having a difficult time creating test data that has activity type, location and time intersetctions. try again later.")
	public
			void testEvent() throws EntityNotFoundException {
		Activity line = this.examples.line();
		Activity lineBefore = this.examples.lineBefore();
		Route route = this.routeUtil.createPersistedRoute(line);
		ActivityType activityType = ActivityTestUtil.activityType();
		RouteCompletion course = this.completionUtil.routeCompletionBuilder(RouteCompletionTestUtil.withActivity(line)
				.route(route).activityTypeReported(activityType).build()).build();
		RouteCompletion completion = this.completionUtil.routeCompletionBuilder(RouteCompletionTestUtil.with(route)
				.activityTypeReported(activityType).startTime(line.getTrackSummary().getStartTime())
				.duration(line.getTrackSummary().getElapsedDuration()).activity(lineBefore).build()).build();
		JodaTestUtil.assertInterval(course.interval()).overlaps(completion.interval());
		assertEquals(	"something's wrong with the test classes assigning activity type " + activityType,
						course.activityTypeReported(),
						completion.activityTypeReported());
		RouteCompletionOrganizer completionsOrganized = this.service.completionsOrganized(route);
		SortedSet<ActivityTypeStatistic> activityTypeStats = completionsOrganized.completionStats().activityTypeStats();
		TestUtil.assertSize("more than one activity type " + activityType, 1, activityTypeStats);
		TestUtil.assertSize(2, completionsOrganized.events().entries());
		final Set<RouteEvent> events = completionsOrganized.events().keySet();
		TestUtil.assertSize(1, events);
		RouteEvent event = events.iterator().next();
		assertEquals(route.startArea(), event.startArea());
	}

	@Test
	@Ignore("WW-153 this stopped working when the parent was added by routeEventId.  The query should work so getting past tests to find out. ")
	public
			void testEventRaceFilter() {
		Route route = this.routeUtil.createPersistedRoute();
		RouteEventId eventId = RouteEventTestUtil.eventId();
		RouteEventId eventId2 = RouteEventTestUtil.eventId();
		RouteCompletion completionWithEventFaster = this.completionUtil.routeCompletionBuilder(route)
				.routeEventId(eventId).socialRaceRank(1).personalRaceRank(RouteCompletion.PERSONAL_BEST_RANK).build();
		RouteCompletion completionWithEventSlower = this.completionUtil.routeCompletionBuilder(route)
				.routeEventId(eventId).socialRaceRank(2).personalRaceRank(RouteCompletion.PERSONAL_BEST_RANK).build();
		RouteCompletion completionWithNoEvent = this.completionUtil.routeCompletionBuilder(route).routeEventId(null)
				.personalRaceRank(RouteCompletion.PERSONAL_BEST_RANK).build();
		RouteCompletion completionWithEvent2 = this.completionUtil.routeCompletionBuilder(route).routeEventId(eventId2)
				.personalRaceRank(RouteCompletion.PERSONAL_BEST_RANK).build();
		Filter filter = RouteCompletionFilterBuilder.create().routeEventId(eventId).orderBySocialRaceRank().build();
		RouteCompletions completions = this.service.completions(filter);
		TestUtil.assertContains(completionWithEventFaster, completions);
		TestUtil.assertContains(completionWithEventSlower, completions);
		TestUtil.assertDoesntContain(completionWithNoEvent, completions);
		TestUtil.assertDoesntContain(completionWithEvent2, completions);
		TestUtil.assertSize(2, completions);
	}

	@Test
	public void testRaceFilter() {
		Route route = this.routeUtil.createPersistedRoute();
		// only the personal best is indexed
		RouteCompletion completion1st = this.completionUtil.routeCompletionBuilder(route)
				.personalRaceRank(RouteCompletion.PERSONAL_BEST_RANK).socialRaceRank(1).build();
		RouteCompletion completion1stSecondBest = this.completionUtil.routeCompletionBuilder(route)
				.personalRaceRank(RouteCompletion.PERSONAL_BEST_RANK + 1).socialRaceRank(1).build();
		RouteCompletion completion2nd = this.completionUtil.routeCompletionBuilder(route)
				.personalRaceRank(RouteCompletion.PERSONAL_BEST_RANK).socialRaceRank(2).build();
		RouteCompletion completion3rd = this.completionUtil.routeCompletionBuilder(route).socialRaceRank(3)
				.personalRaceRank(RouteCompletion.PERSONAL_BEST_RANK).build();

		RouteCompletion completionWrongRoute = this.completionUtil.routeCompletionBuilder()
				.socialRaceRank(RouteCompletion.PERSONAL_BEST_RANK + 2).build();
		Filter filter = RouteCompletionFilterBuilder.create().route(route).orderByFastest().build();
		RouteCompletions completions = this.service.completions(filter);
		TestUtil.assertSize(3, completions);
		final Iterator<RouteCompletion> completionsIterator = completions.iterator();
		assertEquals(completion1st, completionsIterator.next());
		assertEquals(completion2nd, completionsIterator.next());
		assertEquals(completion3rd, completionsIterator.next());
		TestUtil.assertDoesntContain(completion1stSecondBest, completions);
		TestUtil.assertDoesntContain(completionWrongRoute, completions);
	}
}
