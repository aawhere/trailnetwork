package com.aawhere.route;

import static com.aawhere.route.RouteSearchDocumentProducer.*;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.activity.RelativeStatsUtil;
import com.aawhere.all.RouteServiceNestedField;
import com.aawhere.field.FieldKey;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseFilterBuilder;
import com.aawhere.persist.FilterCondition;
import com.aawhere.person.PersonId;
import com.aawhere.route.RouteSearchDocumentProducer.Builder;
import com.aawhere.trailhead.TrailheadId;

public class RouteSearchFilterBuilder
		extends BaseFilterBuilder<RouteSearchFilterBuilder> {

	private static final FieldKey RELATIVE_STATS_EXPAND_KEY = RouteField.Key.RELATIVE_COMPLETION_STATS;

	@Override
	public Filter build() {
		seachIndex();
		Filter built = super.build();

		return built;
	}

	/**
	 * Automatically adds the search index based on the contents of the filter.
	 * 
	 */
	@SuppressWarnings("rawtypes")
	private void seachIndex() {

		Layer layer;
		if (!building.hasSearchIndexes()) {

			Collection<FilterCondition> layerCondition = removed(RouteServiceNestedField.Key.ROUTE__RELATIVE_COMPLETION_STATS__LAYER);
			if (CollectionUtils.isNotEmpty(layerCondition)) {
				layer = (Layer) layerCondition.iterator().next().getValue();
			} else {
				Boolean containsPerson = building.containsCondition(PERSON_ID);
				Boolean containsActivityType = building.containsCondition(ACTIVITY_TYPE)
						|| building.containsCondition(DISTANCE_CATEGORY);
				// distance category for Route defies the standard since the distance category is
				// 1:1 with the activity type so no need for a separate index
				Boolean containsDistance = false;
				layer = RelativeStatsUtil.layer(containsPerson, containsActivityType, containsDistance);
			}
			if (relativeStatsExpandRequested(building)) {
				relativeStatsExpanded(layer);
			}
			String indexName = RouteSearchUtil.indexName(layer);
			addSearchIndex(indexName);
		}
	}

	public RouteSearchFilterBuilder allIndexes() {
		Layer[] values = Layer.values();
		for (Layer layer : values) {
			addSearchIndex(RouteSearchUtil.indexName(layer));
		}
		return this;
	}

	/**
	 * Call this when {@link RouteSearchFilterBuilder#relativeStatsExpandRequested(Filter)} so the
	 * {@link RouteSearchDocumentProducer#RELATIVE_COUNT} and other necessary fields will be
	 * provided.
	 * 
	 * @return
	 */
	private RouteSearchFilterBuilder relativeStatsExpanded(Layer layer) {

		addField(RouteSearchDocumentProducer.RELATIVE_COUNT.getAbsoluteKey());
		if (layer.personal) {
			addField(RouteSearchDocumentProducer.PERSON_ID.getAbsoluteKey());
		}
		if (layer.activityTyped) {
			addField(RouteSearchDocumentProducer.ACTIVITY_TYPE.getAbsoluteKey());
		}
		return this;
	}

	public static Boolean relativeStatsExpandRequested(Filter filter) {
		return filter.containsExpand(RELATIVE_STATS_EXPAND_KEY);
	}

	public RouteSearchFilterBuilder activityType(ActivityType activityType) {
		return addCondition(FilterCondition.create().field(ACTIVITY_TYPE).value(activityType).build());
	}

	public RouteSearchFilterBuilder distanceCategory(ActivityTypeDistanceCategory distanceCategory) {
		return addCondition(FilterCondition.create().field(DISTANCE_CATEGORY).value(distanceCategory).build());
	}

	public RouteSearchFilterBuilder mainId(RouteId mainId) {
		return addCondition(FilterCondition.create().field(MAIN_ID).value(mainId).build());
	}

	public RouteSearchFilterBuilder routeId(RouteId routeId) {
		return addCondition(FilterCondition.create().field(ROUTE_ID).value(routeId).build());
	}

	public RouteSearchFilterBuilder start(TrailheadId trailheadId) {
		return addCondition(FilterCondition.create().field(START_TRAILHEAD_ID).value(trailheadId).build());
	}

	public static RouteSearchFilterBuilder create() {
		return new RouteSearchFilterBuilder();
	}

	public static RouteSearchFilterBuilder clone(Filter filter) {
		return new RouteSearchFilterBuilder(Filter.cloneForBuilder(filter));
	}

	private RouteSearchFilterBuilder(Filter filter) {
		super(filter);
	}

	/** Use {@link Builder} to construct RouteSearchFilterBuilder */
	private RouteSearchFilterBuilder() {
		this(new Filter());
	}

	public RouteSearchFilterBuilder personId(PersonId personId) {
		return addCondition(FilterCondition.create().field(PERSON_ID).value(personId).build());

	}

}
