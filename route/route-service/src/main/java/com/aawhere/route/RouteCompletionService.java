/**
 * 
 */
package com.aawhere.route;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityService;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.ScreeningResults;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.app.ApplicationKey;
import com.aawhere.io.ConnectionTimeoutException;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilteredResponse;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonMessage;
import com.aawhere.person.PersonService;
import com.aawhere.person.Persons;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventService;
import com.aawhere.route.event.RouteEvents;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class RouteCompletionService
		extends EntityService<RouteCompletions, RouteCompletion, RouteCompletionId> {

	private static final Logger LOG = LoggerFactory.getLogger(RouteCompletionService.class);
	final private RouteCompletionObjectifyRepository repository;
	final ActivityTimingService timingService;
	final ActivityService activityService;
	final PersonService personService;
	final RouteEventService routeEventService;
	final Options options;
	/**
	 * FIXME:remove completion service from route and pass this in from the constructor.
	 * 
	 * Right now it is assigned by RouteService during construction.
	 */
	RouteService routeService;

	/**
	 * @param repository
	 */
	@Inject
	public RouteCompletionService(RouteCompletionObjectifyRepository repository, RouteEventService routeEventService,
			ActivityTimingService timingService, ActivityService activityService, PersonService personService,
			Options options) {
		super(repository);
		this.repository = repository;
		this.activityService = activityService;
		this.timingService = timingService;
		this.personService = personService;
		this.routeEventService = routeEventService;
		this.options = options.copy();
	}

	/**
	 * Provides the route completions for the route given limited to the restrictions of the filter.
	 * 
	 */
	public RouteCompletions completions(RouteId routeId, Filter filter) {
		return this.repository.filter(RouteCompletionFilterBuilder.clone(filter).setParent(routeId).filter());
	}

	public RouteCompletion completion(RouteCompletionId completionId) throws EntityNotFoundException {
		return this.repository.load(completionId);
	}

	/**
	 * provides all children in a sustainable {@link Iterable} so this may return thousands of
	 * children.
	 * 
	 * @param routeId
	 * @return
	 */
	public Iterable<RouteCompletion> completions(RouteId routeId) {
		return this.repository.children(routeId);
	}

	Person person(PersonId personId) throws EntityNotFoundException {
		return personService.person(personId);
	}

	/**
	 * @param routeIds
	 */
	public Iterable<RouteCompletion> completions(Iterable<RouteId> routeIds) {
		return Iterables.concat(Iterables.transform(routeIds, completionsByParentFunction()));
	}

	public Function<RouteId, Iterable<RouteCompletion>> completionsByParentFunction() {
		return new Function<RouteId, Iterable<RouteCompletion>>() {

			@Override
			public Iterable<RouteCompletion> apply(RouteId input) {
				return completions(input);
			}
		};
	}

	/**
	 * Provides all route Ids for a given person. This has no guarantee of uniqueness as that is
	 * left up to the client.
	 */
	public Iterable<RouteId> routeIds(Iterable<PersonId> personIds) throws EntityNotFoundException {
		// FIXME:This should be a projection query? can you do that with parents?
		return RouteCompletionUtil.routeIds(completionsForPersons(personIds));
	}

	/**
	 * @see #routeIds(Iterable)
	 * @param personIds
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Iterable<RouteId> routeIds(PersonId... personIds) throws EntityNotFoundException {
		return routeIds(ImmutableSet.copyOf(personIds));
	}

	public Iterable<RouteCompletion> completionsForPersons(Iterable<PersonId> personId) throws EntityNotFoundException {
		if (Iterables.isEmpty(personId)) {
			throw InvalidArgumentException.required(PersonMessage.ID_NAME);
		}
		// call to ensure persons exists and to get aliases.
		final Persons persons = personService.persons(personId);
		// FIXME:This should be a projection query? can you do that with parents?
		// filter builder handles the aliases.
		return this.repository.entityPaginator(RouteCompletionFilterBuilder.create().persons(persons).build());
	}

	public Iterable<RouteCompletion> completions(PersonId... personId) throws EntityNotFoundException {
		return completionsForPersons(ImmutableSet.copyOf(personId));
	}

	public RouteCompletions completions(PersonId personId, Filter filter) throws EntityNotFoundException {
		Person person = personService.person(personId);
		return completions(RouteCompletionFilterBuilder.clone(filter).person(person).build());
	}

	public RouteCompletions completionsWithEvents(PersonId personId, Filter originalFilter)
			throws EntityNotFoundException {
		Filter filter = RouteCompletionFilterBuilder.clone(originalFilter).person(personService.person(personId))
				.orderByMostRecentlyRecorded().routeEventAssociated().build();
		return completions(filter);
	}

	/**
	 * Provides only the {@link RouteCompletionId} related to a course completion of a Route.
	 * 
	 * @see #courseCompletions(RouteId)
	 * @param routeId
	 * @return
	 */
	public Iterable<RouteCompletionId> courseCompletionIds(RouteId routeId) {
		return ids(RouteCompletionFilterBuilder.create().routeId(routeId).courses().build());
	}

	/**
	 * Provides a stream of {@link RouteCompletionId} for each course of the {@link RouteId}
	 * provided.
	 * 
	 * @param routeIds
	 * @return
	 */
	public Iterable<RouteCompletionId> courseCompletionIds(Iterable<RouteId> routeIds) {
		return Iterables.concat(Iterables.transform(routeIds, courseCompletionIdsFunction()));
	}

	/** Provides a stream of courses for the given route, which could be thousands. */
	public Iterable<RouteCompletion> courseCompletions(RouteId routeId) {
		return completions(RouteCompletionFilterBuilder.create().routeId(routeId).courses().build());
	}

	public Function<RouteId, Iterable<RouteCompletion>> courseCompletionsFunction() {
		return new Function<RouteId, Iterable<RouteCompletion>>() {
			@Override
			public Iterable<RouteCompletion> apply(RouteId input) {
				return (input == null) ? null : courseCompletions(input);
			}
		};
	}

	/**
	 * @see #courseCompletionIds(RouteId)
	 * @return
	 */
	public Function<RouteId, Iterable<RouteCompletionId>> courseCompletionIdsFunction() {
		return new Function<RouteId, Iterable<RouteCompletionId>>() {
			@Override
			public Iterable<RouteCompletionId> apply(RouteId input) {
				return (input != null) ? courseCompletionIds(input) : null;
			}
		};
	}

	/**
	 * Provides a stream of courses for the routes given.
	 * 
	 * @see #courseCompletions(RouteId)
	 * @see #courseCompletionsFunction()
	 * @param parents
	 * @return
	 */
	public Iterable<RouteCompletion> courseCompletions(Iterable<RouteId> parents) {
		return Iterables.concat(Iterables.transform(parents, courseCompletionsFunction()));
	}

	/**
	 * Provides any {@link RouteCompletion} which the activity of interest has completed regardless
	 * of is course or not.
	 * 
	 * @param activityId
	 * @return
	 * @throws EntityNotFoundException
	 *             when the activity provided does not exist.
	 */
	public RouteCompletions completions(ActivityId activityId, Filter filter) throws EntityNotFoundException {
		// simply does an exist check
		this.activityService.activity(activityId);
		return completions(RouteCompletionFilterBuilder.clone(filter).activity(activityId).build());
	}

	/**
	 * provides the completions using the filter. It will apply any standard operations provided by
	 * {@link RouteCompletionFilterBuilder}.
	 * 
	 * @param filter
	 * @return
	 */
	public RouteCompletions completions(Filter filter) {
		return repository.filter(RouteCompletionFilterBuilder.clone(filter).build());
	}

	/**
	 * Provides a stream of all completions for a route resulting in the rules of a social race
	 * (only the personal best is given in order of race rank).
	 * 
	 * @param routeId
	 * @return
	 */
	public Iterable<RouteCompletion> completionsRace(RouteId routeId) {
		return repository.entityPaginator(RouteCompletionFilterBuilder.create().routeId(routeId)
				.orderBySocialRaceRank().build());
	}

	/**
	 * Provides just the person ids related to all completions for a given parent. This guarantees
	 * no duplicates.
	 * 
	 * @param parent
	 * @return
	 */
	public Iterable<PersonId> personIds(RouteId parent) {
		// FIXME:use a projection query!!!!
		return Sets.newHashSet(RouteCompletionUtil.personIds(completions(parent)));
	}

	/**
	 * Creates a {@link RouteCompletion} given the {@link Completion} from the
	 * {@link ActivityTiming}.
	 * 
	 * @param route
	 * @param courseCompletion
	 * @return
	 */
	public Collection<RouteCompletion> courseCompleted(Route route, ActivityTiming timing) {
		return courseCompleted(route, Lists.newArrayList(timing));
	}

	/**
	 * Creates route completions for every timing that is related to the same route. Working in bulk
	 * operations this reduces {@link ConnectionTimeoutException} and race conditions finding routes
	 * for the same course.
	 * 
	 * @param route
	 * @param timings
	 * @return
	 */
	public Collection<RouteCompletion> courseCompleted(Route route, Iterable<ActivityTiming> timings) {
		ImmutableSet.Builder<RouteCompletion> allRouteCompletions = ImmutableSet.builder();
		for (ActivityTiming timing : timings) {
			ImmutableList<RouteCompletion> routeCompletions;
			try {
				routeCompletions = routeCompletions(route, timing);
			} catch (EntityNotFoundException e) {
				throw ToRuntimeException.wrapAndThrow(e);
			}
			allRouteCompletions.addAll(routeCompletions);
		}

		ImmutableSet<RouteCompletion> routeCompletions = allRouteCompletions.build();
		// the repository handles merging and transactions
		int attempts = 0;
		final int maxAttempts = 5;
		ConcurrentModificationException exception = null;

		// TN-694 continue trying to store catching concurrent exception until max attempts reached
		// notice the return in the middle and exception exit if the end is reached
		// TODO:consider putting this in the ObjectifyRepository
		do {
			try {
				Map<RouteCompletionId, RouteCompletion> stored = this.repository.mergeAll(routeCompletions);
				// =========== Successful Exit ======================
				return stored.values();
			} catch (ConcurrentModificationException e) {
				attempts++;
				LOG.warning(attempts + " attempts made to store " + routeCompletions);
				exception = e;
			}
		} while (attempts < maxAttempts);
		// =========== Failing Exit ======================
		throw new ToRuntimeException(exception, HttpStatusCode.UNAVAILABLE);
	}

	/**
	 * Converts a single {@link ActivityTimingRelation}, which may have multiple {@link Completion}
	 * of a course, into a {@link RouteCompletion}. The {@link RouteCompletion} nor the
	 * {@link ActivityTimingRelation} is not persisted allowing for bulk operations.
	 * 
	 * @param route
	 *            the representative of the course. If not provided this will find or create it.
	 * @param timing
	 * @return the {@link RouteCompletion} in order of their
	 *         {@link ActivityTimingRelation#completions()}.
	 * @throws EntityNotFoundException
	 *             when the course can't be found
	 */
	public ImmutableList<RouteCompletion> routeCompletions(@Nullable Route route, @Nonnull ActivityTiming timing)
			throws EntityNotFoundException {
		Builder<RouteCompletion> results = ImmutableList.builder();
		if (timing.courseCompleted()) {
			if (route == null) {
				route = routeService.routeManaged(timing);
			}
			// assuming the route completion will be stored...this ensures the route is stale
			routeService.markedForUpdate(route);
			List<Completion> courseCompletions = timing.completions();
			Integer lapTotal = courseCompletions.size();
			// each completion is considered a lap
			for (int i = 0; i < lapTotal; i++) {
				Integer lap = i + 1;

				Activity attempt = activityService.activity(timing.id().getAttemptId());
				Person person;
				try {
					person = personService.person(attempt.getAccountId());
				} catch (EntityNotFoundException e) {
					// an activity was created in the system, but not it's corresponding account?
					// let's force an admin to deal with this problem
					throw ToRuntimeException.wrapAndThrow(e);
				}

				Completion courseCompletion = courseCompletions.get(i);
				final RouteCompletion routeCompletion = RouteCompletion.create().route(route)
						.completion(courseCompletion).repeatCount(lap).repeatTotal(lapTotal)
						.isCourse(timing.reciprocal()).activity(attempt).person(person).build();
				results.add(routeCompletion);
			}
		} else {
			LOG.warning(timing + " given to create routeCompletions, but it didn't complete the Course!");
		}
		return results.build();
	}

	/**
	 * Used when initially creating a route the result will be the single completion associated to
	 * the route.
	 * 
	 * @param route
	 * @param course
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteCompletion courseCompletion(Route route, Activity course) {
		RouteCompletion completion;
		try {
			completion = RouteCompletion.create().isCourse(true).repeatCount(1).repeatTotal(1).route(route)
					.activity(course).person(personService.person(course.getAccountId())).build();
		} catch (EntityNotFoundException e) {
			// something else is wrong with activity-account, not our problem
			throw ToRuntimeException.wrapAndThrow(e);
		}
		return repository.store(completion);
	}

	/**
	 * @see RouteCompletionFilterBuilder#bestRanksForPerson(Person)
	 * @param personId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteCompletions completionsRankingBest(PersonId personId) throws EntityNotFoundException {
		return completions(RouteCompletionFilterBuilder.create().bestRanksForPerson(person(personId)).build());
	}

	/**
	 * When two routes have been determined to be the same, this will take all completions from the
	 * route to merge and "move" give them the new parent.
	 * 
	 * TN-826 now protecting against merging unrelated routes
	 * 
	 * @param id
	 * @param into
	 */
	void merge(RouteId routeToMerge, Route into) {
		Iterable<RouteCompletion> orphaned = this.repository.children(routeToMerge);
		try {
			ScreeningResults screeningResults = timingService.attemptsScreened(into.courseId(), RouteCompletionUtil
					.activityIds(orphaned));
			Multimap<TimingCompletionCategory, ActivityTimingId> failers = screeningResults.failers();
			if (!failers.isEmpty()) {
				throw new IllegalArgumentException(routeToMerge + " requested to merge into " + into.id()
						+ ", but it has completions not that didn't pass screening: " + failers);
			}
		} catch (EntityNotFoundException e) {
			// merging into a route that has a dead activity. someone else should deal with this
			// upstream
			throw ToRuntimeException.wrapAndThrow(e);
		}

		// adoptions are the new children. existing children will be dealt with by the repository
		LinkedList<RouteCompletion> adoptions = new LinkedList<RouteCompletion>();
		// The id's will retain the old parent information after mutating the completion
		LinkedList<RouteCompletionId> orphanIds = new LinkedList<RouteCompletionId>();
		for (RouteCompletion orphan : orphaned) {
			// id is non-mutable so it remains identifying the orphan
			orphanIds.add(orphan.id());

			// keep everying of the completion, but change to the new parent
			RouteCompletion adopted = RouteCompletion.adopt(into, orphan).build();
			adoptions.add(adopted);
		}

		// existing adoptions will be merged by the repository
		this.repository.mergeAll(adoptions);
		// the orphan ids reference the old parent
		this.repository.deleteAllFromIds(orphanIds);
	}

	/**
	 * Organizes all children for the given route providing rank, order and cumulative statistics.
	 * 
	 * @param route
	 * @return
	 * @throws EntityNotFoundException
	 */
	RouteCompletionOrganizer completionsOrganized(Route route) throws EntityNotFoundException {

		Iterable<RouteCompletion> children = this.repository.children(route.id());
		// TODO:Remove this block once Route transition has been performed
		// ensure the route#course has a corresponding completion
		try {
			completion(new RouteCompletionId(route));
		} catch (EntityNotFoundException e) {
			Activity course = activityService.activity(route.courseId());
			RouteCompletion courseCompletion = courseCompletion(route, course);
			children = Iterables.concat(Lists.newArrayList(courseCompletion), children);
		}
		RouteEvents existingEvents = routeEventService.routeEvents(route.id());
		RouteCompletionOrganizer organizer = RouteCompletionOrganizer.create().completions(children).route(route)
				.minNumberOfCoursesToScore(options.minNumberOfCoursesToScore).existingEvents(existingEvents)
				.personProvider(activityService.personFunction())
				.attemptTimingsCounter(this.timingService.attemptsCountFunction())
				.courseTimingsCounter(this.timingService.coursesCountFunction()).organize();
		repository.storeAll(Iterables.concat(organizer.completions(), organizer.duplicateCompletions()));
		routeEventService.routeEventsUpdated(organizer.events().keySet());
		return organizer;
	}

	/**
	 * FIXME: actually look for the best by inspecting the number of attempts that completed the
	 * course, signal quality, etc.
	 * 
	 * @param routeId
	 * @param applicationKey
	 * @return
	 */
	public RouteCompletion courseBestForApp(RouteId routeId, ApplicationKey applicationKey) {
		Iterable<RouteCompletion> courseCompletions = courseCompletions(routeId);
		return Iterables.getFirst(	Iterables.filter(	courseCompletions,
														RouteCompletionUtil.applicationPredicate(applicationKey)),
									null);
	}

	/**
	 * @see #courseIds(Iterable)
	 * @param routeId
	 * @return
	 */
	public Iterable<ActivityId> courseIds(RouteId routeId) {
		return courseIds(Lists.newArrayList(routeId));
	}

	/**
	 * Provides course ids that is found as a child of any of the route ids provided. Course ids are
	 * ActivityIds that {@link RouteCompletion#isCourse()}; Activities, by definition, are a course
	 * of only a single {@link Route} so in an updated datastore there should not be any duplicates.
	 * 
	 * 
	 * @param routeIds
	 * @return
	 */
	public Iterable<ActivityId> courseIds(Iterable<RouteId> routeIds) {
		return RouteCompletionUtil.activityIdsFromIds(courseCompletionIds(routeIds));
	}

	/**
	 * Provides just the activity ids found related to the given route in the completions. This
	 * could be either as a course or attempt.
	 * 
	 * @param routeId
	 * @return
	 */
	public Iterable<ActivityId> activityIds(RouteId routeId) {
		// FIXME: make this a projection query
		return RouteCompletionUtil.activityIds(completions(routeId));
	}

	/**
	 * An effecient way to retrieve all coures for a collection of routes since the call to the
	 * datastore will be delayed
	 * 
	 * @param ids
	 * @return
	 */
	public Iterable<Activity> courses(Iterable<RouteId> ids) {
		return Iterables.concat(Iterables.transform(ids, coursesFunction()));
	}

	/**
	 * Provides all courses identified from the
	 * {@link RouteCompletionService#courseCompletions(RouteId)}.
	 * 
	 * @param routeId
	 * @return
	 */
	public Iterable<Activity> courses(RouteId routeId) {
		Iterable<ActivityId> activityIds = courseIds(routeId);
		return activityService.activities(activityIds);
	}

	/**
	 * calls {@link #courses(RouteId)} when {@link Function#apply(Object)} is invoked.
	 * 
	 * @return
	 */
	public Function<RouteId, Iterable<Activity>> coursesFunction() {
		return new Function<RouteId, Iterable<Activity>>() {

			@Override
			public Iterable<Activity> apply(RouteId input) {
				return (input == null) ? null : courses(input);
			}
		};
	}

	/**
	 * Removes the completions given from the system.
	 * 
	 * @param routeId
	 */
	void delete(Iterable<RouteCompletion> completions) {
		this.repository.deleteAll(completions);
	}

	/**
	 * Provided the person id and the filter to provide options and pages this will add the filters
	 * necessary to find the favorites. only one completion per route is returned, the most recent.
	 * The completions will be ordered by Highest Personal Total first.
	 * 
	 * @param personId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteCompletions mostPopularAllTime(Iterable<PersonId> personIds, Filter filter)
			throws EntityNotFoundException {

		filter = RouteCompletionFilterBuilder.clone(filter).persons(personService.persons(personIds))
				.orderByPersonalTotal().filter();
		return completions(filter);
	}

	public RouteCompletions mostPopularAllTime(PersonId personId, Filter filter) throws EntityNotFoundException {
		return mostPopularAllTime(Lists.newArrayList(personId), filter);
	}

	/**
	 * @param personId
	 * @param completionFilter
	 * @return
	 * @throws EntityNotFoundException
	 */
	public FilteredResponse<RouteId> routeIds(PersonId personId, Filter completionFilter)
			throws EntityNotFoundException {
		Filter filter = RouteCompletionFilterBuilder.clone(completionFilter).person(personService.person(personId))
				.build();
		return routeIds(filter);
	}

	/**
	 * Provides only the route ids associated with the completion results of the given filter.
	 * 
	 * @param filter
	 * @return
	 */
	public FilteredResponse<RouteId> routeIds(Filter filter) {
		// FIXME:use a projection query since route id is the parent and indexed?
		return FilteredResponse.create(Sets.newHashSet(RouteCompletionUtil.routeIds(completions(filter)))).build();
	}

	/**
	 * Touches each completion to force an update, typically for an index update, using a batch for
	 * each route given.
	 * 
	 * @param idPaginator
	 * @return
	 */
	Integer touched(Iterable<RouteId> ids) {
		Integer count = 0;
		for (RouteId routeId : ids) {
			this.repository.storeAll(this.repository.children(routeId));
			count++;
		}
		return count;
	}

	/**
	 * Pass this into the constructor if you wish to configure the service in a different way than
	 * the default. Add properties as needed via the mutators. A {@link Options#copy()} is made upon
	 * assignment protecting further mutation.
	 * 
	 */
	@Singleton
	public static class Options
			implements Cloneable {

		/**
		 * @see RouteScoreCalculator.Builder#minimumNumberOfCourses(Integer)
		 */
		private Integer minNumberOfCoursesToScore = RouteScoreCalculator.COURSE_COUNT_MIN;

		/**
		 * @param minNumberOfCoursesToScore
		 *            the minNumberOfCoursesToScore to set
		 */
		public Options minNumberOfCoursesToScore(Integer minNumberOfCoursesToScore) {
			this.minNumberOfCoursesToScore = minNumberOfCoursesToScore;
			return this;
		}

		private Options copy() {
			try {
				return (Options) super.clone();
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Provides the completions that match the given {@link RouteEventId}.
	 * 
	 * @param routeEventId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 *             when the event given is not found
	 */
	public RouteCompletions completions(RouteEventId routeEventId, Filter filter) throws EntityNotFoundException {
		this.routeEventService.existsEnforced(routeEventId);
		return completions(RouteCompletionFilterBuilder.clone(filter).routeEventId(routeEventId).build());
	}

	/**
	 * Provides a stream of {@link RouteCompletion} matching the given {@link RouteEventId}.
	 * 
	 * @param routeEventId
	 * @return
	 */
	public Iterable<RouteCompletion> completions(RouteEventId routeEventId) {
		return this.repository
				.entityPaginator(RouteCompletionFilterBuilder.create().routeEventId(routeEventId).build());
	}

	/**
	 * @param routeId
	 */
	public void completionsDeleted(RouteId routeId) {
		Iterable<RouteCompletionId> childrenIds = this.repository.childrenIds(routeId);
		this.repository.deleteAllFromIds(childrenIds);
	}

}
