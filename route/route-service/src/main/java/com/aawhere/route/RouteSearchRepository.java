package com.aawhere.route;

import java.util.Set;

import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.route.Route.Builder;
import com.aawhere.search.GaeSearchRepository;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadService;
import com.aawhere.util.rb.Message;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Marries the {@link RouteObjectifyRepository} for the datastore with the {@link SearchService}
 * providing the best search possible and maintaining updates and deletions in each.
 * 
 * @author aroller
 * 
 */
@Singleton
public class RouteSearchRepository
		extends GaeSearchRepository<Routes, Route, RouteId>
		implements RouteRepository {

	private static final ImmutableSet<String> indexNames = ImmutableSet.of(	RouteSearchUtil.indexName(Layer.ALL),
																			RouteSearchUtil
																					.indexName(Layer.ACTIVITY_TYPE));
	final RouteObjectifyRepository datastoreRepository;
	final TrailheadService trailheadService;

	@Inject
	public RouteSearchRepository(RouteObjectifyRepository datastoreRepository, SearchService searchService,
			TrailheadService trailheadService) {
		super(datastoreRepository, searchService);
		this.datastoreRepository = datastoreRepository;
		this.trailheadService = trailheadService;
	}

	@Override
	protected Iterable<RouteId> idsFromSearchDocumentIds(Iterable<String> searchDocumentIds) {
		return RouteSearchUtil.routeIds(searchDocumentIds);
	}

	@Override
	protected Function<RouteId, Iterable<Filter>> deleteAllFromIdsFilterFunction() {
		return new Function<RouteId, Iterable<Filter>>() {

			@Override
			public Iterable<Filter> apply(RouteId routeId) {
				return ImmutableList.of(RouteSearchFilterBuilder.create().allIndexes().routeId(routeId).build());
			}
		};
	}

	@Override
	protected Iterable<SearchDocument> searchDocuments(Iterable<Route> routes) {
		return RouteSearchUtil.searchDocuments(routes, this.trailheadService);
	}

	@Override
	protected Routes entities(Iterable<Route> routes, SearchDocuments searchDocuments) {

		com.aawhere.route.Routes.Builder builder = Routes.create();
		Filter filter = searchDocuments.getFilter();
		if (filter.containsOption(SearchWebApiUri.SEARCH)) {
			builder.addSupport(searchDocuments);
		}
		if (RouteSearchFilterBuilder.relativeStatsExpandRequested(filter)) {
			RouteSearchUtil.countsAdded(routes, searchDocuments);
		}
		return builder.addAll(routes).setFilter(filter).build();
	}

	@Override
	public Route update(Route entity) {
		// TODO:Index should inspect changes to indicate which fields should trigger a search index
		// may be buggy so only do if writes become too great
		return index(datastoreRepository.update(entity));
	}

	@Override
	public Route setScore(RouteId routeId, Integer score) throws EntityNotFoundException {
		// TN-894 index must look for document differences
		// changing the score can demote a route
		Route beforeMutation = load(routeId);
		Route updated = datastoreRepository.setScore(routeId, score);
		return index(updated, beforeMutation);
	}

	@Override
	public Route aliasesAdded(RouteId id, Set<RouteId> aliases) throws EntityNotFoundException {
		return index(datastoreRepository.aliasesAdded(id, aliases));
	}

	@Override
	public Route aliasesSet(RouteId id, Set<RouteId> aliases) throws EntityNotFoundException {
		return index(datastoreRepository.aliasesSet(id, aliases));
	}

	@Override
	public Route setMainId(RouteId id, RouteId mainId) throws EntityNotFoundException {
		return index(datastoreRepository.setMainId(id, mainId));
	}

	@Override
	public Route setAlternateCount(RouteId id, Integer alternateCount) throws EntityNotFoundException {
		// not in search index
		return datastoreRepository.setAlternateCount(id, alternateCount);
	}

	@Override
	public Route setName(RouteId id, Message name) throws EntityNotFoundException {
		return index(datastoreRepository.setName(id, name));
	}

	@Override
	public Route setStart(RouteId id, TrailheadId start) throws EntityNotFoundException {
		return index(datastoreRepository.setStart(id, start));
	}

	@Override
	public Route setFinish(RouteId id, TrailheadId bestId) throws EntityNotFoundException {
		// not in search index
		return datastoreRepository.setFinish(id, bestId);
	}

	@Override
	public Route setStale(RouteId routeId, Boolean isStale) throws EntityNotFoundException {
		// not in search index
		return datastoreRepository.setStale(routeId, isStale);
	}

	@Override
	public Builder update(RouteId routeId) throws EntityNotFoundException {
		final Route previous = load(routeId);
		return new Builder(load(routeId)) {
			@Override
			public Route build() {
				Route updated = datastoreRepository.update(super.build());
				index(updated, previous);
				return updated;
			}
		};
	}

	@Override
	protected Filter prepareForDatastore(Filter filter) {
		return RouteDatastoreFilterBuilder.clone(filter).build();
	}

	@Override
	protected Filter prepareForSearch(Filter filter) {
		return RouteSearchFilterBuilder.clone(filter).build();
	}

	@Override
	protected ImmutableSet<String> indexNames() {
		// activity type distance is with activity type. Person layers are not managed by this
		// repository.
		return indexNames;
	}
}
