/**
 *
 */
package com.aawhere.route;

import static com.aawhere.id.IdentifierUtils.*;
import static com.google.common.collect.Sets.*;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.aawhere.app.IdentityTranslators;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.measure.geocell.MeasureTranslators;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Work;

/**
 * @see RouteCompletionRepository
 * @see RouteCompletion
 * @see RouteCompletionService
 * @author aroller
 * 
 */
@Singleton
public class RouteCompletionObjectifyRepository
		extends
		ObjectifyFilterRepository<RouteCompletion, RouteCompletionId, RouteCompletions, RouteCompletions.Builder>
		implements RouteCompletionRepository {

	private RouteCompletionRepositoryDelegate delegate;

	static {
		IdentityTranslators.add(ObjectifyService.factory());
		MeasureTranslators.add(ObjectifyService.factory());
		ObjectifyService.register(RouteCompletion.class);
	}

	@Inject
	protected RouteCompletionObjectifyRepository(FieldDictionaryFactory fieldDictionaryFactory) {
		this(fieldDictionaryFactory, SYNCHRONIZATION_DEFAULT);
	}

	/**
	 * @param filterRepository
	 */
	protected RouteCompletionObjectifyRepository(FieldDictionaryFactory fieldDictionaryFactory,
			Synchronization synchronization) {
		super(fieldDictionaryFactory, synchronization);
		delegate = new RouteCompletionRepositoryDelegate(this);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteCompletionRepository#merge(com.aawhere.route.RouteCompletion)
	 */
	@Override
	public RouteCompletion merge(final RouteCompletion replacement) {
		return ofy().transact(new Work<RouteCompletion>() {
			@Override
			public RouteCompletion run() {
				return delegate.merge(replacement);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.route.RouteCompletionRepository#setIsCourse(com.aawhere.route.RouteCompletionId,
	 * java.lang.Boolean)
	 */
	@Override
	public RouteCompletion setIsCourse(RouteCompletionId completionId, Boolean isCourse) throws EntityNotFoundException {
		return delegate.setIsCourse(completionId, isCourse);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteCompletionRepository#mergeAll(java.lang.Iterable)
	 */
	@Override
	public Map<RouteCompletionId, RouteCompletion> mergeAll(Iterable<RouteCompletion> entities) {
		// FIXME: validate all from the same parent
		RouteCompletion first = Iterables.getFirst(entities, null);
		// no first then nothing to do
		if (first != null) {
			final RouteId parentId = first.routeId();
			final Function<RouteCompletion, RouteCompletionId> idFunction = idFunction();
			final ImmutableMap<RouteCompletionId, RouteCompletion> incomingMap = Maps.uniqueIndex(entities, idFunction);
			// the entities that will be stored containing all those coming in and those
			// will be replaced with their merged equiavalent next.
			final Map<RouteCompletionId, RouteCompletion> storingMap = Maps.newHashMap(incomingMap);

			return ofy().transact(new Work<Map<RouteCompletionId, RouteCompletion>>() {
				@Override
				public Map<RouteCompletionId, RouteCompletion> run() {
					// getting only children ids instead of children for cost reasons and
					// performance
					Set<RouteCompletionId> existingIds = Sets.newHashSet(childrenIds(parentId));
					Set<RouteCompletionId> mergeCandidatesIds = intersection(existingIds, storingMap.keySet());
					// now load all the children that need to be merged
					Map<RouteCompletionId, RouteCompletion> existingCompletions = loadAll(mergeCandidatesIds);
					for (RouteCompletion existing : existingCompletions.values()) {
						RouteCompletion incoming = incomingMap.get(existing.id());
						// mutating the "existing" so "merged" is the same reference
						RouteCompletion merged = RouteCompletionUtil.merged(existing, incoming);
						// replace the incoming with that which is merged
						storingMap.put(incoming.id(), merged);
					}
					// the storing map now contains the new incoming and those incoming absorbed
					// into those which already existed
					return RouteCompletionObjectifyRepository.super.storeAll(storingMap.values());
				}
			});
		}
		return Collections.emptyMap();
	}

	/**
	 * provides a connected building that will persist upon building.
	 * 
	 * @return
	 */
	RouteCompletion.Builder create() {
		return new RouteCompletion.Builder() {
			/*
			 * @see com.aawhere.persist.BaseEntities.Builder#build()
			 */
			@Override
			public RouteCompletion build() {
				return update(super.build());
			}
		};
	}

	RouteCompletion.Builder update(RouteCompletionId completionId) throws EntityNotFoundException {
		return new RouteCompletion.Builder(load(completionId)) {
			/*
			 * @see com.aawhere.route.RouteCompletion.Builder#build()
			 */
			@Override
			public RouteCompletion build() {
				return update(super.build());
			}
		};
	}
}
