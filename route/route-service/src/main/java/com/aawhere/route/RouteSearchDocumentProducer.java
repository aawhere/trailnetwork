/**
 *
 */
package com.aawhere.route;

import java.util.Set;
import java.util.SortedSet;

import javax.annotation.Nonnull;

import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.ActivityTypeStatistic;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.activity.RelativeStatsUtil;
import com.aawhere.all.RouteServiceNestedField;
import com.aawhere.field.FieldKey;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.search.GaeSearchQueryProducer;
import com.aawhere.search.GaeSearchUtil;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocument.Field;
import com.aawhere.search.SearchDocuments;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadUtil;
import com.aawhere.util.rb.Message;
import com.google.common.collect.ImmutableList;

/**
 * A {@link SearchDocument} for Routes. Public routes stats are used directly from route to crate
 * the search document details such as number of completions and the related score. Activity type
 * completions indicate popularity for a specific type of activity so Route-ACTIVITY_TYPE is a
 * different index with entries for the routes for activity types.
 * 
 * If {@link Builder#personalStats} is provided then this will produce search documents for the
 * person identified in the releative stats. The person has their own indexes created since a
 * personal effort will be different than the public effort as a whole.
 * 
 * @author Brian Chapman
 * 
 */
public class RouteSearchDocumentProducer {

	static final FieldKey ROUTE_NAME = RouteField.Key.NAME;
	static final FieldKey RELATIVE_COUNT = RouteServiceNestedField.Key.ROUTE__RELATIVE_COMPLETION_STATS__COUNT;
	static final FieldKey LOCATION = GaeSearchQueryProducer.FIELD.KEY.LOCATION;
	static final FieldKey START_TRAILHEAD_ID = RouteField.Key.START_TRAILHEAD_ID;
	static final FieldKey MAIN_ID = RouteField.Key.MAIN_ID;
	static final FieldKey DISTANCE_CATEGORY = RouteServiceNestedField.Key.ROUTE__RELATIVE_COMPLETION_STATS__DISTANCE;
	static final FieldKey ACTIVITY_TYPE = RouteServiceNestedField.Key.ROUTE__RELATIVE_COMPLETION_STATS__ACTIVITY_TYPE;
	static final FieldKey ROUTE_ID = RouteField.Key.ID;
	static final FieldKey PERSON_ID = RouteServiceNestedField.Key.ROUTE__RELATIVE_COMPLETION_STATS__PERSON_ID;
	static final FieldKey LAYER = RouteServiceNestedField.Key.ROUTE__RELATIVE_COMPLETION_STATS__LAYER;
	static final FieldKey START_TRAILHEAD_NAME = RouteServiceNestedField.Key.ROUTE__START_TRAILHEAD__NAME;
	/**
	 * The purpose for this whole class. May contain none if the route isn't worthy
	 * 
	 */
	@Nonnull
	private SearchDocuments searchDocuments;

	/**
	 * Used to construct all instances of RouteSearchDocumentProducer.
	 */
	public static class Builder
			extends ObjectBuilder<RouteSearchDocumentProducer> {

		private Route route;
		private RelativeStatistic personalStats;
		/** Provides supporting information like trailhead name. */
		private Trailhead startTrailhead;

		/** @see #startTrailhead */
		public Builder startTrailhead(Trailhead startTrailhead) {
			this.startTrailhead = startTrailhead;
			return this;
		}

		public Builder() {
			super(new RouteSearchDocumentProducer());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.search.SearchDocument.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("route", this.route);
			Assertion.exceptions().notNull("routeId", this.route.id());

		}

		/** @see #route */
		public Builder route(Route route) {
			this.route = route;
			return this;
		}

		/** @see #personalStats */
		public Builder personalStats(RelativeStatistic personalStats) {
			this.personalStats = personalStats;
			Assertion.exceptions().notNull("PERSON_ID", personalStats.personId());
			return this;
		}

		@Override
		public RouteSearchDocumentProducer build() {
			RouteSearchDocumentProducer built = super.build();
			SearchDocuments.Builder searchDocumentsBuilder = SearchDocuments.create();

			// its only personal or general. currently not both, but it could be no problem
			if (this.personalStats != null) {
				personalDocument(searchDocumentsBuilder);
			} else if (RouteUtil.scoreIsSignificant(route.score()) && route.completionStats() != null) {
				allSearchDocument(searchDocumentsBuilder);
				activityTypeSearchDocuments(searchDocumentsBuilder);
			}

			built.searchDocuments = searchDocumentsBuilder.build();
			return built;

		}

		/**
		 * using {@link #personalStats} this will build the search document specialized for the
		 * person.
		 * 
		 * @param searchDocumentsBuilder
		 * @return
		 */
		private SearchDocument personalDocument(com.aawhere.search.SearchDocuments.Builder searchDocumentsBuilder) {
			Integer score = relativeScoreForPersonalStats();
			com.aawhere.search.SearchDocument.Builder documentBuilder = commonBuilder(personalStats);
			documentBuilder.addField(Field.create().key(PERSON_ID).value(personalStats.personId()).build());
			SearchDocument searchDocument = documentBuilder.rank(score).build();
			searchDocumentsBuilder.add(searchDocument);
			return searchDocument;
		}

		/**
		 * The {@link #personalStats} will indicate a category that will limit it's result counts
		 * that are available in route. So the score will be compared to the other people completing
		 * the route of the same type and the total number
		 * 
		 * @return
		 */
		private Integer relativeScoreForPersonalStats() {
			// TN-862 shows how zero scoring routes are causing equality...and are common
			// inflate the route score with the count
			final Integer count = personalStats.count();
			// adding 100 to all routes ensures the percentages used later make for a nice
			// distribution..it won't affect the large scoring routes much, but will protect against
			// zero scoring routes from flattenting everything out to zero
			// count ensures the personal popularity is used
			int routeScore = 100 + route.score() + count;
			final Integer total;
			// categorical score is when activity types should apply the percentage of completions
			// compared to the total of those in the category
			final Integer categoricalScore;
			if (personalStats.hasActivityType()) {
				SortedSet<ActivityTypeStatistic> activityTypeStats = route.completionStats().activityTypeStats();
				ActivityTypeStatistic statsForCategory = ActivityTypeStatistic
						.statsForCategory(activityTypeStats, personalStats.activityType());
				if (statsForCategory != null) {
					// the total relative for the given is not the total, but the count in this
					// category
					total = statsForCategory.count();
					categoricalScore = RelativeStatsUtil.score(routeScore, statsForCategory.getPercent());
				} else {
					total = route.completionStats().total();
					// totals are missing non-significant completions of a category
					// get the lowest score
					categoricalScore = routeScore;
				}
			} else {
				total = route.completionStats().total();
				categoricalScore = routeScore;
			}
			// if the total is smaller than the count, then just use the count.
			// the ratio will be 100% of a low score because scores lower than count can happen
			// for events, etc
			Ratio ratioOfTotal = MeasurementUtil.ratio(count, Math.max(count, total));
			Integer score = RelativeStatsUtil.score(categoricalScore, ratioOfTotal);
			return score;
		}

		private void activityTypeSearchDocuments(SearchDocuments.Builder searchDocumentsBuilder) {
			SortedSet<ActivityTypeStatistic> allActivityTypeStats = this.route.completionStats().activityTypeStats();
			for (ActivityTypeStatistic activityTypeStats : allActivityTypeStats) {
				ActivityType category = activityTypeStats.category();
				Integer count = activityTypeStats.count();
				RelativeStatistic relativeStats = statsBuilder(Layer.ACTIVITY_TYPE, count).activityType(category)
						.build();
				Integer relativeScore = RelativeStatsUtil.score(route.score(), activityTypeStats.getPercent());
				SearchDocument searchDocument = commonBuilder(relativeStats).rank(relativeScore).build();
				searchDocumentsBuilder.add(searchDocument);
			}
		}

		private void allSearchDocument(SearchDocuments.Builder searchDocumentsBuilder) {
			Layer layer = Layer.ALL;
			ActivityStats completionStats = route.completionStats();
			Integer total = completionStats.total();
			RelativeStatistic relativeStats = statsBuilder(layer, total).build();
			SearchDocument.Builder allRouteBuilder = commonBuilder(relativeStats);
			allRouteBuilder.rank(route.score());
			SearchDocument searchDocument = allRouteBuilder.build();
			searchDocumentsBuilder.add(searchDocument);

		}

		private com.aawhere.activity.RelativeStatistic.Builder statsBuilder(Layer layer, Integer total) {
			return RelativeStatistic.create().featureId(route.id()).layer(layer).count(total);
		}

		/**
		 * Populates the new builder with all those items from Route that are common regarldess of
		 * activity type.
		 */
		private SearchDocument.Builder commonBuilder(RelativeStatistic stats) {
			SearchDocument.Builder docBuilder = new SearchDocument.Builder();
			docBuilder.index(RouteSearchUtil.indexName(stats.layer()));
			docBuilder.id(stats.id());
			routeIds(docBuilder);
			docBuilder.addField(Field.create().key(RELATIVE_COUNT).value(stats.count()).build());
			activityType(stats, docBuilder);
			name(docBuilder);
			main(docBuilder);
			location(docBuilder);
			trailhead(docBuilder);
			return docBuilder;
		}

		/**
		 * Adds the route id and aliases so the document will be related to any current or past
		 * route.
		 * 
		 * @param docBuilder
		 */
		public void routeIds(SearchDocument.Builder docBuilder) {
			Set<RouteId> aliases = RouteUtil.aliasesUnique(ImmutableList.of(route));
			for (RouteId routeId : aliases) {
				docBuilder.addField(SearchDocument.Field.create().key(ROUTE_ID).value(routeId).build());
			}

		}

		private void activityType(RelativeStatistic stats, SearchDocument.Builder docBuilder) {
			if (stats.hasActivityType()) {
				ActivityType activityType = stats.activityType();
				docBuilder.addField(Field.create().key(ACTIVITY_TYPE).value(activityType).build());

				ActivityTypeDistanceCategory distanceCategory = ActivityTypeDistanceCategory
						.valueOfWithDefault(activityType, route.distance());
				docBuilder.addField(Field.create().key(DISTANCE_CATEGORY).value(distanceCategory).build());
			}
		}

		private void name(SearchDocument.Builder docBuilder) {
			// name may be unknown or not provided at all
			if (!RouteUtil.isNameEmpty(route.name())) {
				String routeName = route.name().getValue();
				FieldKey nameField = ROUTE_NAME;
				docBuilder.addField(GaeSearchUtil.suggestWords(routeName, nameField));
				docBuilder.addField(Field.create().key(nameField).value(routeName).build());
			}
		}

		private void location(SearchDocument.Builder docBuilder) {
			GeoCoordinate startPoint = TrailheadUtil.coordinateFrom(route.start());
			docBuilder.addField(SearchDocument.Field.create().key(LOCATION).value(startPoint).build());
		}

		private void main(SearchDocument.Builder docBuilder) {
			RouteId mainId = route.mainId();
			if (mainId != null) {
				docBuilder.addField(SearchDocument.Field.create().key(MAIN_ID).value(mainId).build());
			}
		}

		private void trailhead(SearchDocument.Builder docBuilder) {
			docBuilder.addField(SearchDocument.Field.create().key(START_TRAILHEAD_ID).value(route.start()).build());
			if (startTrailhead != null) {
				Message trailheadName = startTrailhead.name();
				if (!TrailheadUtil.isNameEmpty(trailheadName)) {
					docBuilder.addField(Field.create().key(START_TRAILHEAD_NAME).value(trailheadName).build());
					GaeSearchUtil.suggestWords(trailheadName.getValue(), START_TRAILHEAD_NAME);
				}
			}
		}

	}// end Builder

	public SearchDocuments searchDocuments() {
		return searchDocuments;
	}

	/** Use {@link Builder} to construct RouteSearchDocument */
	private RouteSearchDocumentProducer() {
	}

	public static Builder create() {
		return new Builder();
	}

}
