/**
 * 
 */
package com.aawhere.route.event;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.person.group.SocialGroupMembershipService;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.route.RouteId;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.util.rb.Message;
import com.aawhere.ws.rs.SystemWebApiUri;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class RouteEventService
		extends EntityService<RouteEvents, RouteEvent, RouteEventId>
		implements RouteEventProvider {

	final private RouteEventObjectifyRepository repository;
	private Queue queue;
	final SocialGroupMembershipService socialGroupMembershipService;

	/**
	 * 
	 */
	@Inject
	public RouteEventService(RouteEventObjectifyRepository repository, SocialGroupMembershipService socialGroupService,
			QueueFactory queueFactory) {
		super(repository);
		this.repository = repository;
		this.queue = queueFactory.getQueue(RouteWebApiUri.ROUTE_QUEUE_NAME);
		this.socialGroupMembershipService = socialGroupService;
	}

	public RouteEvents routeEvents(Filter filter) {
		return this.repository.filter(RouteEventFilterBuilder.clone(filter).build());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.route.event.RouteEventProvider#routeEvent(com.aawhere.route.event.RouteEventId)
	 */
	@Override
	public RouteEvent routeEvent(RouteEventId id) throws EntityNotFoundException {
		return this.repository.load(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.event.RouteEventProvider#routeEvent(com.aawhere.route.RouteId)
	 */
	@Override
	public RouteEvents routeEvents(RouteId routeId) throws EntityNotFoundException {
		// FIXME: must validate route exists
		return this.repository.childrenEntities(routeId);
	}

	public RouteEvents routeEvents(RouteId routeId, Filter filter) throws EntityNotFoundException {
		// FIXME:must verify route exists
		return routeEvents(RouteEventFilterBuilder.clone(filter).routeId(routeId).build());
	}

	/**
	 * updates the given events and returns those that are updated.
	 * 
	 * @param keySet
	 * @return
	 */
	public Collection<RouteEvent> routeEventsUpdated(Iterable<RouteEvent> keySet) {
		return this.repository.storeAll(keySet).values();
	}

	/**
	 * Provides the events related to the given routeEventId. Specifically returns the events for
	 * the same route and activity type.
	 * 
	 * @param routeEventId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteEvents routeEvents(RouteEventId routeEventId, Filter filter) throws EntityNotFoundException {
		RouteEvent routeEvent = routeEvent(routeEventId);
		return routeEvents(RouteEventFilterBuilder.clone(filter).routeId(routeEvent.getRouteId())
				.activityType(routeEvent.activityType()).build());
	}

	/**
	 * Batch updates the given names.
	 * 
	 * @param eventNames
	 * @return
	 */
	@Nonnull
	public Map<RouteEventId, RouteEvent> namesUpdated(@Nonnull Map<RouteEventId, Message> eventNames) {
		return this.repository.names(eventNames);
	}

	/**
	 * Handles the merge by deleting the events related to the loser and leaving the events related
	 * to the keeper. Further processing will be necessary to update the events.
	 * 
	 * @param loser
	 * @param keeper
	 */
	public void merged(RouteId loser, RouteId keeper) {
		Iterable<RouteEvent> children = this.repository.children(loser);
		repository.deleteAll(children);
	}

	public Integer routeEventsUpdated(Filter filter) {
		filter = RouteEventFilterBuilder.clone(filter).build();
		Integer count;
		if (filter.containsOption(RouteEvent.FIELD.OPTION.TOUCH)) {
			count = this.repository.touched(filter);
		} else {
			Set<RouteId> routeIds = RouteEventUtil.routeIdsUnique(this.repository.idPaginator(filter));
			count = RouteWebApiUri.routeUpdateTasks(routeIds, SystemWebApiUri.FORCED, queue);
		}
		return count;
	}

	/**
	 * Simply updates the socialGroupId for the RouteEvent identified.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteEvent socialGroupAssigned(RouteEventId routeEventId, SocialGroupId socialGroupId)
			throws EntityNotFoundException {
		return this.repository.update(routeEventId).socialGroupId(socialGroupId).build();
	}

	/**
	 * @param routeId
	 */
	public void routeEventsDeleted(RouteId routeId) {
		Iterable<RouteEvent> children = this.repository.children(routeId);
		socialGroupMembershipService.socialGroupsDeleted(RouteEventUtil.socialGroupIds(children));
		repository.deleteAll(children);
	}

}
