/**
 *
 */
package com.aawhere.route;

import java.util.Set;

import com.aawhere.activity.Activity;
import com.aawhere.app.IdentityTranslators;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.measure.geocell.MeasureTranslators;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.aawhere.route.Route.Builder;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.util.rb.Message;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * The GAE way to persist {@link Activity} using {@link ObjectifyRepository}.
 * 
 * @author Aaron Roller
 * 
 */
@Singleton
public class RouteObjectifyRepository
		extends ObjectifyFilterRepository<Route, RouteId, Routes, Routes.Builder>
		implements RouteRepository {

	private RouteRepositoryDelegate delegate;

	static {
		IdentityTranslators.add(ObjectifyService.factory());
		MeasureTranslators.add(ObjectifyService.factory());
		ObjectifyService.register(Route.class);
	}

	@Inject
	public RouteObjectifyRepository(FieldDictionaryFactory factory) {
		this(factory, SYNCHRONIZATION_DEFAULT);
	}

	public RouteObjectifyRepository(FieldDictionaryFactory factory, Synchronization synchronization) {
		super(factory, synchronization);
		this.delegate = RouteRepositoryDelegate.create(this).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setScore(com.aawhere.route.Route, java.lang.Integer)
	 */
	@Override
	public Route setScore(RouteId routeId, Integer score) throws EntityNotFoundException {
		return this.delegate.setScore(routeId, score);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#addAliases(com.aawhere.route.RouteId, java.util.Set)
	 */
	@Override
	public Route aliasesAdded(final RouteId id, final Set<RouteId> aliases) throws EntityNotFoundException {
		// ofy().transact's run method doesn't allow reporting of an exception
		// guice @Transactional annotation would be nice
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try {
			Route result = delegate.aliasesAdded(id, aliases);
			txn.commit();
			return result;
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}

	}

	@Override
	public Route aliasesSet(final RouteId id, final Set<RouteId> aliases) throws EntityNotFoundException {
		// ofy().transact's run method doesn't allow reporting of an exception
		// guice @Transactional annotation would be nice
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		try {
			Route result = delegate.aliasesSet(id, aliases);
			txn.commit();
			return result;
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setMainId(com.aawhere.route.RouteId,
	 * com.aawhere.route.RouteId)
	 */
	@Override
	public Route setMainId(RouteId id, RouteId mainId) throws EntityNotFoundException {
		return this.delegate.setMainId(id, mainId);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.objectify.ObjectifyRepository#update(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public Route update(Route entity) {
		return super.update(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setName(com.aawhere.route.RouteId,
	 * com.aawhere.util.rb.Message)
	 */
	@Override
	public Route setName(RouteId id, Message name) throws EntityNotFoundException {
		return this.delegate.setName(id, name);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setStart(com.aawhere.route.RouteId,
	 * com.aawhere.trailhead.TrailheadId)
	 */
	@Override
	public Route setStart(RouteId id, TrailheadId start) throws EntityNotFoundException {
		return this.delegate.setStart(id, start);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setFinish(com.aawhere.route.RouteId,
	 * com.aawhere.trailhead.TrailheadId)
	 */
	@Override
	public Route setFinish(RouteId id, TrailheadId bestId) throws EntityNotFoundException {
		return delegate.setFinish(id, bestId);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setAlternateCount(com.aawhere.route.RouteId,
	 * java.lang.Integer)
	 */
	@Override
	public Route setAlternateCount(RouteId id, Integer alternateCount) throws EntityNotFoundException {
		return delegate.setAlternateCount(id, alternateCount);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteRepository#setCompletionsPending(com.aawhere.route.RouteId,
	 * java.lang.Boolean)
	 */
	@Override
	public Route setStale(RouteId routeId, Boolean completionsArePending) throws EntityNotFoundException {
		return delegate.setStale(routeId, completionsArePending);
	}

	/*
	 * @see com.aawhere.route.RouteRepository#update(com.aawhere.route.RouteId)
	 */
	@Override
	public Builder update(RouteId routeId) throws EntityNotFoundException {
		return delegate.update(routeId);
	}

}
