/**
 *
 */
package com.aawhere.route;

import com.aawhere.route.event.RouteEventObjectifyRepository;
import com.aawhere.route.event.RouteEventProvider;
import com.aawhere.route.event.RouteEventRepository;
import com.aawhere.route.event.RouteEventService;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

/**
 * @author brian
 * 
 */
public class RouteModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(RouteRepository.class).to(RouteSearchRepository.class);
		bind(RouteEventRepository.class).to(RouteEventObjectifyRepository.class);
		bind(RouteEventProvider.class).to(RouteEventService.class);
		bind(RouteCompletionRepository.class).to(RouteCompletionObjectifyRepository.class);
		bind(RouteProvider.class).to(RouteService.class);

	}

	/**
	 */
	@Provides
	public RouteTrailheadProvider routeTrailheadProvider(RouteService routeService) {
		return routeService.trailheadProvider();
	}

}
