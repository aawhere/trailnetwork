package com.aawhere.route;

/**
 *
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportException;
import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityService;
import com.aawhere.activity.ActivityStats;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityUtil;
import com.aawhere.activity.DeviceSignalActivitySelector;
import com.aawhere.activity.spatial.ActivitySpatialRelation;
import com.aawhere.activity.spatial.ActivitySpatialRelationId;
import com.aawhere.activity.spatial.ActivitySpatialRelationService;
import com.aawhere.activity.spatial.ActivitySpatialRelationsUtil;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.ScreeningResults;
import com.aawhere.activity.timing.ActivityTimingRelationUserStory;
import com.aawhere.activity.timing.ActivityTimingRelationUtil;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.app.KnownApplication;
import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.collections.IterablesExtended;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.persist.DeletionConfirmationException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.persist.FilteredResponse;
import com.aawhere.persist.RepositoryUtil;
import com.aawhere.persist.ServiceGeneralBase;
import com.aawhere.persist.ServiceStandardBase;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonMessage;
import com.aawhere.person.PersonService;
import com.aawhere.person.Persons;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.person.group.SocialGroupMembershipService;
import com.aawhere.person.group.SocialGroupRelator;
import com.aawhere.person.group.SocialGroupUtil;
import com.aawhere.person.group.SocialGroups;
import com.aawhere.queue.BatchQueue;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.Task;
import com.aawhere.route.Route.Builder;
import com.aawhere.route.Route.FIELD;
import com.aawhere.route.RouteCompletion.FIELD.OPTION;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventService;
import com.aawhere.route.event.RouteEventWebApiUri;
import com.aawhere.route.event.RouteEvents;
import com.aawhere.route.event.RouteNameSelector;
import com.aawhere.search.MaxIndexAttemptsExceeded;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.track.TrackInfo;
import com.aawhere.track.Trackpoint;
import com.aawhere.trailhead.RouteAwareTrailheadScoreCalculator;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadDatastoreFilterBuilder;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadProvider;
import com.aawhere.trailhead.TrailheadService;
import com.aawhere.trailhead.TrailheadUtil;
import com.aawhere.trailhead.TrailheadWebApiUri;
import com.aawhere.trailhead.Trailheads;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.view.ViewCount;
import com.aawhere.view.ViewCountId;
import com.aawhere.view.ViewCountRepository;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.common.base.Function;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Provides access to {@link Route} and its persisted data.
 * 
 * @author Brian Chapman
 */
@Singleton
public class RouteService
		extends EntityService<Routes, Route, RouteId>
		implements RouteTrackProvider, RouteProvider {

	private static final Logger LOG = LoggerFactory.getLogger(RouteService.class);
	/**
	 * Keeps activity type display and filtering simple by keeping only those significant
	 * 
	 * @deprecated moved to {@link RouteCompletionOrganizer}
	 */
	@Deprecated
	static final Ratio MIN_ACTIVITY_TYPE_PERCENT = MeasurementUtil.ratio(3, 100);

	/**
	 * The minimum radius allowed for organizing trailheads since it needs a reasonable cushion
	 * around the trailhead itself to properly organize them.
	 */
	private static final Length MIN_TRAILHEADS_ORGANIZATION_SEARCH_RADIUS = QuantityMath
			.create(TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS).times(5).getQuantity();

	final private RouteRepository repository;
	final ActivityService activityService;
	final TrailheadService trailheadService;
	final ActivityTimingService timingService;
	final ActivitySpatialRelationService spatialService;
	final PersonService personService;
	final SocialGroupMembershipService socialGroupService;
	/**
	 * @deprecated use the completion service directly. it now has a reference to route service.
	 */
	@Deprecated
	final RouteCompletionService completionService;
	final RouteEventService eventService;
	final ActivityImportService importService;
	final private Queue queue;
	final private QueueFactory queueFactory;
	final private Options options;
	final private DeviceCapabilitiesRepository deviceCapabilitiesRepository;
	final private ViewCountRepository viewCountRepository;
	final RouteTrailheadProvider trailheadProvider;
	final SearchService searchService;

	@Inject
	public RouteService(RouteRepository routeRepository, RouteCompletionService completionService,
			RouteEventService eventService, TrailheadService trailheadService, ActivityService activityService,
			ActivityTimingService timingService, ActivitySpatialRelationService spatialService,
			PersonService personService, SocialGroupMembershipService socialGroupService,
			DeviceCapabilitiesRepository deviceCapabilitiesRepository, ViewCountRepository viewCountRepository,
			QueueFactory queueFactory, Options options, SearchService searchService,
			FieldAnnotationDictionaryFactory dictionaryFactory) {
		super(routeRepository);
		this.repository = routeRepository;
		this.completionService = completionService;
		this.eventService = eventService;
		this.trailheadService = trailheadService;
		this.activityService = activityService;
		this.spatialService = spatialService;
		this.personService = personService;
		this.timingService = timingService;
		this.socialGroupService = socialGroupService;
		this.viewCountRepository = viewCountRepository;
		this.importService = timingService.getImportService();
		this.queueFactory = queueFactory;
		this.queue = queueFactory.getQueue(RouteWebApiUri.ROUTE_QUEUE_NAME);
		this.options = options.copy();
		this.deviceCapabilitiesRepository = deviceCapabilitiesRepository;
		trailheadProvider = new RouteTrailheadProviderService();
		this.searchService = searchService;

		this.completionService.routeService = this;

	}

	/**
	 * Retrieve an {@link Route} from the {@link RouteRepository}
	 * 
	 * @param routeId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Override
	public Route route(RouteId routeId) throws EntityNotFoundException {
		Route result;
		try {
			result = repository.load(routeId);
		} catch (EntityNotFoundException e) {
			// maybe it is an alias?
			// specifically calling repository direct to avoid unnessary common
			// sorting
			Routes routes = repository.filter(RouteDatastoreFilterBuilder.create().alias(routeId).build());
			// throws entity not found exception if none
			result = RepositoryUtil.finderResult(routeId, routes);
			// multiple aliases means the resulting routes should be aliases of
			// the same route
			if (routes.size() > 1) {
				getLogger().warning(routeId + " found in multiple aliases... " + routes + " merging into "
						+ result.id());
				result = markedForMerge(RouteUtil.ids(routes));
			}
		}
		return result;
	}

	/**
	 * increments the {@link ViewCount} for the given person id.
	 * 
	 * @param personId
	 * @return
	 */
	public Integer viewed(RouteId routeId) {
		return this.viewCountRepository.increment(new ViewCountId(routeId));
	}

	/**
	 * Provides the {@link ViewCount} for the personId given. If none is found then a non-persisted
	 * blank will be provided indicated zero.
	 * 
	 * @param personId
	 * @return
	 */
	public ViewCount viewCount(RouteId routeId) {
		try {
			return this.viewCountRepository.load(new ViewCountId(routeId));
		} catch (EntityNotFoundException e) {
			return ViewCount.create().counting(routeId).build();
		}
	}

	/**
	 * Given the courseId, this will return the route that represents the course if the course is
	 * part of a group that is worthy of being a route. This method uses {@link RouteCompletions} to
	 * determine if their is a related {@link RouteCompletion#isCourse()}. If not then the activity
	 * poses as a route with no {@link RouteCompletions}. If the activity has not yet been imported
	 * it will be now. In such a case, or any time an activity isn't identified as a course by a
	 * route completion, this activity will be converted into a temporary route that will provide
	 * all details of the route except the route id. This allows easy access to view any activity as
	 * a route even if no other activity has or ever will relate to it.
	 * 
	 * @param courseId
	 * @return
	 * @throws EntityNotFoundException
	 * @throws ActivityImportException
	 * @throws CoursesNotRelatedException
	 */
	public Route route(ActivityId courseId) throws ActivityImportException, EntityNotFoundException,
			CoursesNotRelatedException {
		// important to call this to avoid processing if not found
		this.importService.getActivity(courseId);
		Route result;
		// a route may exist with courseId exact match
		Routes routesById = routes(RouteDatastoreFilterBuilder.create().courseId(courseId).filter());
		if (routesById.isEmpty()) {
			// no exact match so look at completions for a matching course
			// completion
			Filter filter = RouteCompletionFilterBuilder.create().course(courseId).build();
			RouteCompletions completions = this.completionService.completions(filter);
			if (completions.isEmpty()) {
				throw new CoursesNotRelatedException(courseId);
			} else {
				Iterable<RouteId> routeIds = RouteCompletionUtil.routeIds(completions);
				if (completions.size() > 1) {
					LOG.info(routeIds + " each has a completion with a course of " + courseId);
					result = markedForMerge(routeIds);
				} else { // must be 1
					result = route(Iterables.getOnlyElement(routeIds));
				}
			}
		} else if (routesById.size() > 1) {
			LOG.warning(routesById + " each claim their course to be " + courseId);
			result = markedForMerge(RouteUtil.ids(routesById));
		} else {
			// one found in Route#courseId
			result = Iterables.getOnlyElement(routesById);
		}
		return result;
	}

	/**
	 * @see #routeCreated(Activity)
	 * @param courseId
	 * @return
	 * @throws EntityNotFoundException
	 */
	Route routeCreated(ActivityId courseId) throws EntityNotFoundException {
		return routeCreated(activityService.activity(courseId));
	}

	/**
	 * Creates a mimimal route given the details of the course.
	 * 
	 * @param course
	 * @return
	 */
	Route routeCreated(Activity course) {
		Pair<Trailhead, Trailhead> endpoints = endpoints(course);
		Route route = Route.create().start(endpoints.getLeft()).finish(endpoints.getRight()).course(course)
		// marking completions pending now avoids the courseCompletion
		// from doing so
				.stale(true).build();
		route = this.repository.store(route);
		route = courseCompletion(route, course);

		return route;
	}

	/**
	 * Searches for any existing completions available for a giving timing. Typically zero or 1 is
	 * returned, but multiple can be returned if a route was completed more than once per a single
	 * activity. If the given course is not yet identified to be a completed course then it will be
	 * reported as {@link CoursesNotRelatedException}.
	 * 
	 * @param timingId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteCompletions completions(ActivityTimingId timingId) throws EntityNotFoundException,
			CoursesNotRelatedException {
		Route route = route(timingId.getCourseId());
		return this.completionService.completions(RouteCompletionFilterBuilder.create().route(route)
				.activity(timingId.getAttemptId()).build());
	}

	/**
	 * Provides the completions that have events for a given route. This is here since it needs to
	 * validate route exists.
	 * 
	 * @see RouteCompletionService#completionsWithEvents(PersonId, Filter)
	 */
	public RouteCompletions completionsWithEvents(RouteId routeId, Filter originalFilter)
			throws EntityNotFoundException {
		existsEnforced(routeId);
		Filter filter = RouteCompletionFilterBuilder.clone(originalFilter).routeId(routeId).routeEventAssociated()
				.build();
		return completionService.completions(filter);
	}

	/**
	 * When a timing has detected one or more successful completions of a course this is called to
	 * ensure {@link RouteCompletions} are created, in order, for every
	 * {@link ActivityTiming#completions()}. The completions may complete different routes. Since
	 * the {@link ActivityTiming#course()} is considered a route this will search across all
	 * completions for a {@link RouteCompletion#isCourse()} to see if an existing route has already
	 * been created. If no Route has been created for the completed course then one is created now
	 * using the course details. If multiple routes are found representing the given course then
	 * those routes must be {@link #routesMerged(Iterable)}. No ranking or ordering is done to the
	 * {@link RouteCompletions} created or updated from this timing, but instead the
	 * {@link Route#isStale()} is flagged so some other process can detect the route is stale and
	 * needs update. This will reduce unnecessary expensive social updating every time this method
	 * is called.
	 * 
	 * @param timing
	 * @return
	 * @throws ActivityImportException
	 * @throws EntityNotFoundException
	 */
	public RouteCompletions completionsUpdated(ActivityTimingId activityTimingRelationId)
			throws EntityNotFoundException {
		ActivityTiming timing = this.timingService.getTiming(activityTimingRelationId);
		// TODO:put in a stale check here to avoid updating the completions if
		// the timing has not
		// changed.

		RouteCompletions result;
		// if bogus timing just return an empty set. exception could explain
		// more,but is it
		// necessary?
		if (!timing.courseCompleted()) {
			result = RouteCompletions.create().build();
		} else {

			Route route = routeManaged(timing);
			route = markedForUpdate(route);
			result = RouteCompletions.create().addAll(completionService.courseCompleted(route, timing)).build();
		}
		return result;
	}

	/**
	 * Route starts are persisted spatially. This will look for those Routes that have starts
	 * intersecting the Activity's spatial buffer. Ordered by score the most popular routes will be
	 * provided first also filtering out those without a significant score.
	 * 
	 * @param activity
	 * @return
	 */
	public Iterable<RouteId> routeIdsIntersecting(Activity activity) {
		Filter filter = RouteDatastoreFilterBuilder.create().startIntersection(activity).orderByNone().build();
		return this.repository.idPaginator(filter);
	}

	/**
	 * For a given attempt, this will do a smart search by looking at the route locations for those
	 * routes that have endpoint intersections. All courses matched will be inspected for a match
	 * with the given attempt. Each attempt of every found course is a potential match with the
	 * given attempt so each attempt for the course will be attempted as a course. TODO:every course
	 * of any matched attempt would be worth comparing.
	 * 
	 * @see #reciprocalGroupTimingsQueued() for comparing attempts in the same group
	 * @param attempt
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ScreeningResults completionCandidatesCourseScreener(final ActivityId attempt) throws EntityNotFoundException {
		return completionCandidatesCourseScreener(activityService.activity(attempt));
	}

	ScreeningResults completionCandidatesCourseScreener(final Activity attempt) {
		ScreeningResults screeningResults = null;
		if (!attempt.isGone()) {
			// no point in screening if the attempt isn't even long enough to
			// complete a minimum
			// course
			if (ActivityTimingRelationUserStory.isLongEnough(attempt)) {
				// FIXME: this could be greatly reduced if Route had geocells.
				// FIXME: id only query works, but then zero score routes are included TN-823
				// Iterable<RouteId> routeIds = routeIdsIntersecting(attempt);
				Iterable<Route> intersectingRoutes = repository.entityPaginator(RouteDatastoreFilterBuilder.create()
						.startIntersection(attempt).orderByScore().build());
				Iterable<RouteId> routeIds = IdentifierUtils.ids(intersectingRoutes);
				Iterable<ActivityId> courseIds = this.completionService.courseIds(routeIds);

				screeningResults = timingService.coursesScreened(attempt, courseIds);
				Map<ActivityTimingId, ActivityTimingRelationCandidateScreener> passers = screeningResults.passers();
				// completions timings have a relatively high success rate.
				final Iterable<ActivityTimingId> passingIds = Iterables.transform(	passers.values(),
																					ActivityTimingRelationCandidateScreener
																							.candidateIdFunction());
				final Queue timingQueue = timingService.getTimingCourseAttemptQueue();

				timingService.queueTimings(passingIds, timingQueue);

				if (LOG.isLoggable(Level.INFO)) {
					LOG.info(screeningResults.passers().size() + " passed screening of existing routes");
					LOG.info(screeningResults.failers().size() + " failed screening of existing routes");
					if (LOG.isLoggable(Level.FINE)) {
						LOG.fine("failing details: " + screeningResults.failers());
					}
				}

			}
		}
		return screeningResults;
	}

	/**
	 * Provides the route if a route is available for the given course. This does not create and
	 * update any items.
	 * 
	 * @see #routeManaged(ActivityTiming)
	 * @param activityTimingRelationId
	 * @return
	 * @throws EntityNotFoundException
	 * @throws CoursesNotRelatedException
	 */
	public Route route(ActivityTimingId activityTimingRelationId) throws EntityNotFoundException,
			CoursesNotRelatedException {
		return route(Iterables.getLast(completions(activityTimingRelationId)).routeId());
	}

	/**
	 * Finds or creates a route when given a timing relationship. This will mark for merge two
	 * routes if both are found and the timing is reciprocal.
	 * 
	 * This marks the route stale since it is assuming you are doing something that will require
	 * updating.
	 * 
	 * @param timing
	 * @return
	 * @throws EntityNotFoundException
	 *             when the course can't be found
	 */
	Route routeManaged(ActivityTiming timing) throws EntityNotFoundException {
		ActivityId courseId = timing.course().getActivityId();
		Route reciprocalRoute = null;
		// the reciprocal, if it exists, makes a fine route too.
		if (BooleanUtils.isTrue(timing.reciprocal())) {
			try {
				reciprocalRoute = route(timing.attempt().getActivityId());
			} catch (CoursesNotRelatedException e) {
				// recipriocal timing not yet persisted
				// when it is created it will be related to the route
				// created/found
			}
		}
		Route route;
		try {
			route = route(courseId);
			// the reciprocal route is found then make them aliases of each
			// other
			if (reciprocalRoute != null && !reciprocalRoute.equals(route)) {
				markedForMerge(IdentifierUtils.ids(route, reciprocalRoute));
			}
		} catch (CoursesNotRelatedException e) {
			// a route for our course doesn't yet exist
			// if the reciprocal exists use it instead of creating a new one
			final Activity course = activityService.activity(courseId);
			if (reciprocalRoute != null) {
				route = reciprocalRoute;
				// add the course to the reciprocal route for future reference
				courseCompletion(route, course);
			} else {
				route = routeCreated(course);
			}

		}
		if (!route.isStale()) {
			repository.setStale(route.id(), true);
		}
		return route;
	}

	/**
	 * @param route
	 * @param course
	 * @return
	 * @throws EntityNotFoundException
	 */
	private Route courseCompletion(Route route, final Activity course) {
		// not realated yet, but since reciprocal let's relate now
		completionService.courseCompletion(route, course);
		// assuming the given route has some recent data
		if (BooleanUtils.isNotTrue(route.isStale())) {
			try {
				route = this.repository.setStale(route.id(), true);
			} catch (EntityNotFoundException e) {
				throw ToRuntimeException.wrapAndThrow(e);
			}
		}
		return route;
	}

	/**
	 * Marks the completions pending in the datastore if not yet.
	 * 
	 * @param route
	 * @return
	 * @throws EntityNotFoundException
	 */
	Route markedForUpdate(Route route) {
		// the route is stale since a new completion has arrived
		// only update once to avoid unnecessary writing
		if (BooleanUtils.isNotTrue(route.isStale())) {
			try {
				route = this.repository.setStale(route.id(), true);
			} catch (EntityNotFoundException e) {
				LOG.warning(route.id() + " is not found and can't be marked for update");
			}
		}
		return route;
	}

	/**
	 * Iterates
	 * 
	 * @param routes
	 * @return
	 */
	Integer markedForUpdate(Iterable<RouteId> routeIds) {
		Integer count = 0;
		Iterable<Route> routes = routes(routeIds);
		for (Route route : routes) {
			markedForUpdate(route);
			count++;
		}
		return count;
	}

	/**
	 * Routes may be created legitimately and later determined to be the same. We must handle
	 * merging while keeping the alias around when searching since someone or some entity may have a
	 * stale reference. This is a thorough search for any routes that may share aliases which those
	 * will be merged even if not provided as a candidate. Basically, this method will merge those
	 * routes provided and any other through transitive relationships. TN-429
	 * 
	 * @since 15
	 * @param routeToMerge
	 *            includes any route that is to be merged...must be at least one.
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteMergeDetails routesMerged(Iterable<RouteId> routesToMerge) {
		RouteMergeDetails details = new RouteMergeDetails();
		details.routesToMerge = routesToMerge;

		// minimize and count to avoid wasted efforts
		// calling the service will find any route that may be an alias of
		// another
		HashSet<Route> mergeCandidates = Sets.newHashSet(routes(routesToMerge));
		routeMergeValidated(mergeCandidates);
		details.mergeCandidates = mergeCandidates;

		// any route that claims it's course to be the course of another should
		// be merged TN-712
		details.mergeCandidates.addAll(routes(RouteDatastoreFilterBuilder.create()
				.courseIdsIn(RouteUtil.courseIds(mergeCandidates)).filter()));

		// transitive relationships through aliases should all be considered in
		// the same group
		HashSet<RouteId> routeIdsInTheGroup = Sets.newHashSet(RouteUtil.ids(mergeCandidates));
		details.routeIdsInTheGroup = routeIdsInTheGroup;

		// aliases are part of the group
		Iterables.addAll(routeIdsInTheGroup, RouteUtil.aliases(mergeCandidates));
		// any of those given are part of the group even if they aren't found
		Iterables.addAll(routeIdsInTheGroup, routesToMerge);

		// we have to filter directly...calling #routes(ids) does the alias
		// lookup
		Iterables.addAll(mergeCandidates, routes(RouteDatastoreFilterBuilder.create().aliases(routeIdsInTheGroup)
				.filter()));
		// not every route will be an alias, this returns only those found
		Iterables.addAll(mergeCandidates, repository.entities(routeIdsInTheGroup));
		if (!mergeCandidates.isEmpty()) {
			Route keeper = RouteUtil.mostSeniorRoute(mergeCandidates);
			Set<RouteId> aliasesWorthKeeping = aliasesWorthKeeping(details);

			try {
				keeper = repository.aliasesSet(keeper.id(), aliasesWorthKeeping);
				// if the aliases were different then keeper is stale
				if (keeper.isStale() && aliasesWorthKeeping.size() > 1) {
					getLogger().info(keeper + " with existing aliases " + keeper.aliases() + " merged with "
							+ aliasesWorthKeeping);
				}
			} catch (EntityNotFoundException e1) {
				throw new RuntimeException(e1);
			}
			details.keeper = keeper;
			// merged every alias into the route
			// this will try to merge with routes that don't exist anymore, but
			// it's good for
			// cleanup of routes and completions just in case
			for (RouteId aliasId : routeIdsInTheGroup) {
				if (!aliasId.equals(keeper.id())) {
					// re-assign all completions to the keeper, delete the old
					completionService.merge(aliasId, keeper);
					eventService.merged(aliasId, keeper.id());
					// remove the old route...it's no longer needed
					repository.delete(aliasId);
					// FIXME:remove events?
					// FIXME:remove social groups?
				}
			}
		} else {
			LOG.warning(routesToMerge + " were not found during a merge");
		}
		return details;

	}

	/**
	 * 
	 * @param routeId
	 * @throws EntityNotFoundException
	 */
	public void routeDeleted(RouteId routeId) {
		try {
			Route route = route(routeId);
			repository.delete(route);
			// if no route is found then social groups can't be known
			Set<SocialGroupId> socialGroupIds = route.socialGroupIds();
			socialGroupService.socialGroupsDeleted(socialGroupIds);

			// all routes in the group should be updated since the scores may change.
			RouteId mainId = route.mainId();
			mainGroupMarkedStale(mainId);
		} catch (Exception e) {
			LOG.warning(routeId + " requested to be deleted, but is has problems...continuing to delete relations "
					+ e.getMessage());
		}

		// completions can be found by id even if route is missing
		this.completionService.completionsDeleted(routeId);

		this.eventService.routeEventsDeleted(routeId);

	}

	public Integer routesDeleted(Filter filter) throws DeletionConfirmationException {
		return super.deleted(filter);
	}

	/*
	 * @see com.aawhere.persist.EntityService#deleted(com.aawhere.id.Identifier)
	 */
	@Override
	protected void deleted(RouteId id) {
		routeDeleted(id);
	}

	/**
	 * @param mainId
	 */
	public void mainGroupMarkedStale(RouteId mainId) {
		Filter filter = RouteDatastoreFilterBuilder.create().group(mainId).addOption(Route.FIELD.OPTION.STALE).build();
		routesUpdated(filter);
	}

	/**
	 * Inspects the given set of aliases to see if it is worth keeping around. Reasons for this
	 * include:
	 * <ul>
	 * <li>The route exists</li>
	 * <li>Completions remain referencing the route.</li>
	 * <li>Significant number of times {@link #viewed(RouteId)}.</li>
	 * </ul>
	 * Notice no inspection is done of each route's aliases since it is assumed all aliases are
	 * already provided in the set...meaning this is a helper method for
	 * {@link #routesMerged(Iterable)}. TN-697
	 * 
	 * @param routeIdsInTheGroup
	 * @return
	 */
	private Set<RouteId> aliasesWorthKeeping(RouteMergeDetails details) {
		Set<RouteId> routeIdsInTheGroup = details.routeIdsInTheGroup;

		HashSet<RouteId> keepers = Sets.newHashSet();
		details.aliasesWorthKeeping = keepers;

		Set<RouteId> idsExisting = repository.idsExisting(routeIdsInTheGroup);
		keepers.addAll(idsExisting);

		details.aliasesExisting = idsExisting;
		// non-existing might still need to stick around
		final Set<RouteId> nonExisting = Sets.difference(routeIdsInTheGroup, keepers);

		// if it's been viewed enough times...keep it
		Collection<ViewCount> views = this.viewCountRepository.loadAll(RouteUtil.viewCountIds(nonExisting)).values();
		details.aliasViews = views;

		for (ViewCount viewCount : views) {
			if (viewCount.count() >= options.minNumberOfViewsToKeepAlias) {
				final RouteId routeId = RouteUtil.routeId(viewCount.id());
				keepers.add(routeId);
				details.aliasesViewedEnough.add(routeId);
			}
		}

		SetView<RouteId> notViewedEnough = Sets.difference(routeIdsInTheGroup, keepers);

		// any completions around still? Keep them
		for (RouteId routeId : notViewedEnough) {
			Filter filter = RouteCompletionFilterBuilder.create().setLimit(1).routeId(routeId).filter();
			RouteCompletions completions = this.completionService.completions(filter);
			if (!completions.isEmpty()) {
				keepers.add(routeId);
				details.parentsLeavingOrphans.add(routeId);
			}
		}

		return keepers;
	}

	/**
	 * Any of the aliases given will be identified as aliases of each other so that during the next
	 * {@link #routeUpdated(RouteId)} all routes will be {@link #routesMerged(Iterable)}.
	 * 
	 * @param aliases
	 * @return the most senior route from the bunch given
	 */
	Route markedForMerge(final Iterable<RouteId> aliases) {
		HashSet<Route> routes = Sets.newHashSet(routes(aliases));
		final HashSet<RouteId> uniqueAliases = Sets.newHashSet(aliases);
		LOG.info(aliases + " are being marked for merge");
		for (RouteId routeId : uniqueAliases) {
			try {
				final Route route = this.repository.aliasesAdded(routeId, uniqueAliases);
				routes.add(markedForUpdate(route));
			} catch (EntityNotFoundException e) {
				// the alias isn't found...just ignore since it may have been
				// deleted already
			}
		}

		// TN-907 merging moved to pipeline processing
		// queue.add(RouteWebApiUri.routeMergeTask(aliases));
		// sorting is important to enforce named task duplicate management
		return RouteUtil.mostSeniorRoute(routes);
	}

	/**
	 * Ensures that the routes provided in the group are similar enough to be merged TN-826
	 * 
	 * @param routes
	 */
	private void routeMergeValidated(Set<Route> routes) {
		ImmutableSet<ActivityId> courseIds = ImmutableSet.<ActivityId> builder().addAll(RouteUtil.courseIds(routes))
				.build();

		ImmutableSet<ActivitySpatialRelationId> idCombinations = ActivitySpatialRelationsUtil.idCombinations(courseIds);
		for (ActivitySpatialRelationId activitySpatialRelationId : idCombinations) {
			try {
				Pair<ActivitySpatialRelation, ActivitySpatialRelation> spatialBiRelation = spatialService
						.spatialBiRelation(activitySpatialRelationId);
				routeMergeValidated(spatialBiRelation.getLeft());
				routeMergeValidated(spatialBiRelation.getRight());
			} catch (EntityNotFoundException e) {
				throw new IllegalArgumentException(activitySpatialRelationId
						+ " trying to merge with non existing activity " + routes, e);
			}
		}
	}

	private void routeMergeValidated(ActivitySpatialRelation relation) {
		Ratio minOverlap = MeasurementUtil.ratio(0.95);
		if (QuantityMath.create(relation.overlap()).lessThan(minOverlap)) {
			throw new IllegalArgumentException(relation.id() + " has an overlap of " + relation.overlap()
					+ ", but for a merge needs at least" + minOverlap);
		}

	}

	/**
	 * Provides the routes limited by the filter conditions,etc. Sorts by score always and
	 * exclusively.
	 * 
	 * @param filter
	 * @return
	 */
	public Routes routes(Filter filter) {
		// rebuilding the filter with the builder ensures defaults are assigned
		// properly
		Routes routes = repository.filter(filter);

		// provides the activities and bounding box based on provided options
		final List<String> options = filter.getOptions();
		if (CollectionUtils.isNotEmpty(options)) {
			if (options.contains(Route.FIELD.OPTION.COURSES)) {
				LOG.warning(Route.FIELD.OPTION.COURSES + " option is deprecated and will be removed");
				Activities activities = this.activityService.getActivities(RouteUtil.courseIds(routes));
				routes = Routes.mutate(routes).addSupport(activities).build();
			}
			if (options.contains(Route.FIELD.OPTION.TRAILHEADS)) {
				LOG.warning(Route.FIELD.OPTION.TRAILHEADS + " option is deprecated and will be removed");
				Iterable<TrailheadId> startsIds = RouteUtil.startsIds(routes);
				Trailheads trailheads = trailheadService.trailheads(startsIds);
				routes = Routes.mutate(routes).addSupport(trailheads).build();
			}
		}

		return routes;
	}

	/**
	 * Provides the routes the person has completed, in order of highest personal total.
	 * 
	 * @param personId
	 * @param completionFilter
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Routes favoriteRoutes(PersonId personId, Filter completionFilter) throws EntityNotFoundException {
		RouteCompletions completions = completionService.completions(personId, completionFilter);
		return routes(completions);
	}

	/**
	 * Given the completions this will find those associates routes, in bulk, and return them as the
	 * {@link Routes} result. The given completions will be assigned to the {@link Routes#support()}
	 * .
	 * 
	 * @param completions
	 * @return
	 */
	public Routes routes(RouteCompletions completions) {
		Iterable<RouteId> routeIds = RouteCompletionUtil.routeIds(completions);
		Iterable<Route> routes = routes(routeIds);
		return Routes.create().addAll(routes).addSupport(completions).build();
	}

	/**
	 * Shortcut for route-main=true
	 * 
	 * @param filter
	 * @return
	 */
	public Routes mains(Filter filter) {
		return routes(RouteDatastoreFilterBuilder.clone(filter).mains(RouteDatastoreFilterBuilder.MAINS_ONLY).build());
	}

	/**
	 * ensures route with an alias is provided.
	 */
	@Override
	public Route entity(RouteId id) throws EntityNotFoundException {
		return route(id);
	}

	/**
	 * @see #routeFunction()
	 */
	@Override
	public Function<RouteId, Route> idFunction() {
		return routeFunction();
	}

	/**
	 * provides routes by id using {@link #route(RouteId)} which handles aliases
	 * 
	 * @return
	 */
	public Function<RouteId, Route> routeFunction() {
		return new Function<RouteId, Route>() {

			@Override
			public Route apply(RouteId input) {
				try {
					return (input == null) ? null : route(input);
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/**
	 * Provides the routes given an iterable of ids.
	 * 
	 * @see #iterable(Iterable)
	 * @see #route(ActivityId)
	 * @param ids
	 * @return the route if found, otherwise it is ignored. Nulls are filtered out.
	 */
	public Iterable<Route> routes(final Iterable<RouteId> ids) {
		return iterable(ids);

	}

	/**
	 * Provides the filter this will find trailheads related to that filter within the given
	 * bounding box, then return all the routes sorted by score related to those matching
	 * trailheads.
	 * 
	 * @deprecated bounds has moved to {@link Filter#getBounds()}
	 * 
	 * @param combinedFilter
	 * @param bounds
	 * @return
	 */
	@Deprecated
	public Routes routes(Filter routeFilter, @Nullable BoundingBox bounds) {
		return routes(routeFilter, bounds, RouteDatastoreFilterBuilder.MAINS_AND_ALTERNATES);
	}

	/**
	 * Analogous to {@link #routes(Filter, BoundingBox)} , but returns only mains.
	 * 
	 * @deprecated mains should be an attribute. bounds has moved to filter builder
	 * @param trailheadFilter
	 * @param bounds
	 * @return
	 */
	@Deprecated
	public Routes mains(Filter routeFilter, @Nullable BoundingBox bounds) {
		return routes(routeFilter, bounds, RouteDatastoreFilterBuilder.MAINS_ONLY);
	}

	/**
	 * Gives the results of the routes when provided the route filter.
	 * 
	 * @param routeFilter
	 * @param bounds
	 * @param mains
	 * @return
	 */
	public Routes routes(Filter routeFilter, @Nullable BoundingBox bounds, Boolean mains) {
		final RouteDatastoreFilterBuilder builder = RouteDatastoreFilterBuilder.clone(routeFilter);
		final Integer maxCount = Filter.Builder.MAX_VALUES_FOR_IN_OPERATOR;
		Trailheads trailheads = null;
		if (bounds != null) {
			trailheads = trailheadService.trailheads(bounds, maxCount);
			builder.start(IdentifierUtils.ids(trailheads));
		}
		Filter filter = builder.mains(mains).build();
		Routes routes = routes(filter);
		return routes;
	}

	/**
	 * This will update the routes using the queue. TODO:Pass a parameter switch to execute
	 * different tasks. FIXME: this should be protected, but route web api is no the same package
	 * 
	 * @return
	 */
	public Integer routesUpdated(Filter filter) {
		if (filter.containsOption(Route.FIELD.OPTION.STALE)) {
			Iterable<RouteId> idPaginator = this.repository.idPaginator(filter);
			int count = 0;
			for (RouteId routeId : idPaginator) {
				try {
					repository.setStale(routeId, true);
				} catch (EntityNotFoundException e) {
					LOG.warning(routeId + " wasn't made stale");
				}
				count++;
			}
			return count;
		} else if (filter.containsOption(Route.FIELD.OPTION.TIMINGS)) {
			Iterable<RouteId> idPaginator = this.repository.idPaginator(filter);
			return timingsUpdateQueued(idPaginator);
		} else {
			Iterable<RouteId> pagianator = this.repository.idPaginator(filter);
			// force if the option is given, otherwise pass null for the default
			Boolean force = null;
			force = filter.containsOption(Route.FIELD.OPTION.FORCE);
			return RouteWebApiUri.routeUpdateTasks(pagianator, force, queue);
		}
	}

	/**
	 * Queues the tasks for all routes indicating stale.
	 * 
	 * @return
	 */
	public Integer routesUpdated() {
		return routesUpdated(RouteDatastoreFilterBuilder.create().stale().build());
	}

	Integer routesUpdated(Iterable<RouteId> pagianator) {
		return RouteWebApiUri.routeUpdateTasks(pagianator, SystemWebApiUri.FORCED_DEFAULT, queue);
	}

	/**
	 * @deprecated use {@link #routes(TrailheadId)} since from the start is the default and normal
	 *             case.
	 * @param start
	 * @return
	 */
	@Deprecated
	public Routes routesFromStart(Trailhead start) {
		Filter filter = new RouteDatastoreFilterBuilder().start(start).build();
		return routes(filter);
	}

	public Function<Trailhead, Routes> routesFromStartFunction() {
		return new Function<Trailhead, Routes>() {

			@Override
			@Nullable
			public Routes apply(@Nullable Trailhead input) {
				return routes(input);
			}
		};
	}

	public Function<Trailhead, Routes> mainsFromStartFunction() {
		return new Function<Trailhead, Routes>() {

			@Override
			@Nullable
			public Routes apply(@Nullable Trailhead input) {
				return mains(input);
			}
		};
	}

	/**
	 * @param trailheadId
	 * @return
	 */
	public Routes routes(TrailheadId trailheadId, Filter filter) {
		return routes(routesForTrailheadFilter(trailheadId, RouteDatastoreFilterBuilder.clone(filter)).build());
	}

	public Routes routes(TrailheadId trailheadId) {
		return routes(routesForTrailheadFilter(trailheadId, RouteDatastoreFilterBuilder.create()).build());
	}

	/**
	 * @see #routes(TrailheadId)
	 * @param trailhead
	 * @return
	 */
	public Routes routes(Trailhead trailhead) {
		return routes(routesForTrailheadFilter(trailhead.id(), RouteDatastoreFilterBuilder.create()).build());
	}

	private RouteDatastoreFilterBuilder routesForTrailheadFilter(TrailheadId trailheadId,
			RouteDatastoreFilterBuilder filterBuilder) {
		TrailheadId representative;
		try {
			representative = trailheadService.nearest(trailheadId).id();
		} catch (EntityNotFoundException e) {
			// well, give them what they asked for which will likely result in
			// nothing
			representative = trailheadId;
		}
		return filterBuilder.start(representative).setLimitToMax();
	}

	public Routes routes(Trailhead trailhead, Filter filter) {
		return routes(trailhead.id(), filter);
	}

	/**
	 * @see #mains(TrailheadId)
	 * @param trailhead
	 * @return
	 */
	public Routes mains(Trailhead trailhead) {
		return mains(trailhead.id());
	}

	/**
	 * Set the score on the route.
	 * 
	 * @see Route
	 * @param key
	 * @param score
	 * @throws EntityNotFoundException
	 */
	Route setScore(RouteId route, Integer score) throws EntityNotFoundException {
		return repository.setScore(route, score);
	}

	/**
	 * Finds the trailheads for the course using existing trailheads if possible.
	 * 
	 * @param course
	 * @return
	 */
	Pair<Trailhead, Trailhead> endpoints(Activity course) {
		// create or receive a trailhead in the area. merging will happen using
		// trailheadsOrganized
		Trailhead start = start(course);
		// use the same trailhead if they are close or get a new one if not.
		// this avoids creating two of the same since asynch finding may cause a
		// race
		// condition
		Trailhead finish = finish(course, start);
		return Pair.of(start, finish);
	}

	/**
	 * provides either the given start as the finish if it is within the area or calls the function
	 * to create/find a new finish if not. This is useful to re-use a recently created start
	 * trailhead without a race condition in an asynchronous persistence environment.
	 * 
	 * @param course
	 * @param start
	 * @return
	 */
	private Trailhead finish(Activity course, Trailhead start) {
		Trailhead finish;
		GeoCoordinate finishLocation = course.getTrackSummary().getFinish().getLocation();
		if (TrailheadUtil.contains(start, finishLocation)) {
			finish = start;
		} else {
			finish = finish(course);
		}
		return finish;
	}

	/**
	 * @param course
	 * @return
	 */
	private Trailhead start(Activity course) {
		return trailhead(course.getTrackSummary().getStart().getLocation());
	}

	/**
	 * Inspects an existing route to see if it has {@link Route#isStale()}. If so then it will
	 * organize the completions and update the attributes of the Route.
	 * 
	 * @param route
	 * @return
	 * @throws EntityNotFoundException
	 * @throws CourseNotWorthyException
	 */
	public RouteUpdateDetails routeUpdated(RouteId routeId) throws EntityNotFoundException {
		return routeUpdated(routeId, false);
	}

	public RouteUpdateDetails routeUpdated(RouteId routeId, @Nullable Boolean force) throws EntityNotFoundException {
		// FIXME:this method is HUGE
		Route route;
		try {
			// find the route by it's id only..no alias
			route = repository.load(routeId);
		} catch (EntityNotFoundException notFoundException) {
			// somehow a route was removed without any record in an alias. This
			// isn't supposed to be
			// possible because of transactions during merge, but it's happening
			Set<RouteCompletion> orphans = Sets.newHashSet(completionService.completions(routeId));
			if (orphans.isEmpty()) {
				// can't find the route, but no orphans either so no big deal.
				throw EntityNotFoundException.forQueue(routeId);
			} else {
				// there are orphans..remove them and check back to make sure
				// they are gone
				try {
					// find the alias if there is one
					Route aliasOf = route(routeId);
					LOG.warning(routeId + " found orphans " + orphans + " belonging to " + aliasOf);
					// attempt a merge again since the last one didn't clear the
					// completions
					completionService.merge(routeId, aliasOf);

				} catch (Exception e) {
					// no alias...this is unexpected, but happens. Just clean up
					// the best possible
					LOG.warning(orphans.size() + " completions orphaned for " + routeId + ": "
							+ RouteCompletionUtil.ids(orphans));
					// ensure the orphans will get a second change to find a
					// parent
					for (ActivityId orphan : Sets.newHashSet(RouteCompletionUtil.activityIds(orphans))) {
						completionsUpdated(orphan);
					}
				}
				// remove all orphans in a big batch
				completionService.delete(orphans);
				// throw the exception that will make another attempt...we
				// should be clean
				throw notFoundException;
			}

		}

		return routeUpdated(route, force);
	}

	RouteUpdateDetails routeUpdated(Route route, Boolean force) throws EntityNotFoundException {
		RouteUpdateDetails.Builder details = RouteUpdateDetails.create();
		details.original(route);
		// the merge inspects for merge of aliases
		RouteMergeDetails mergeDetails = routesMerged(Lists.newArrayList(route.id()));
		details.mergeDetails(mergeDetails);
		route = mergeDetails.keeper;

		// replace the routeId with the winner of the merge
		RouteId routeId = route.id();
		// updating the completions is extensive and expensive. only do it when
		// new completions have
		// been created by new/updated timings
		if (BooleanUtils.isTrue(route.isStale()) || BooleanUtils.isTrue(force)) {

			RouteCompletionOrganizer organizer = this.completionService.completionsOrganized(route);
			Integer courseCompletionCount = organizer.courseStats().total();
			if (courseCompletionCount < options.minNumberOfCoursesToKeep) {
				LOG.warning(routeId + " being deleted because it has insufficient course completions "
						+ courseCompletionCount);
				routeDeleted(routeId);
			} else {
				details.organizer(organizer);
				// cloning helps report the before/after instead of mutation
				final Builder mutator = repository.update(routeId);
				mutator.completionStats(organizer.completionStats()).socialCompletionStats(organizer.socialStats())
						.courseCompletionStats(organizer.courseStats());

				// set the score
				{
					final RouteScoreCalculator scoreCacluator = organizer.routeScoreCalculator();
					details.scoreCalculator(scoreCacluator);
					mutator.score(scoreCacluator.score());
				}
				final List<RouteCompletion> courseCompletions = organizer.courses();

				// update information from the courses
				{

					// need complete activity information
					Iterable<Activity> courses = activities(courseCompletions);
					DeviceSignalActivitySelector courseSelector = DeviceSignalActivitySelector.create()
							.activities(courses).capabilitiesRepository(this.deviceCapabilitiesRepository).build();
					details.courseSelector(courseSelector);
					Activity course = activityService.activity(courseSelector.best().id());
					mutator.courseCompletionDeviceTypeStats(courseSelector.deviceTypeStats());
					Pair<Trailhead, Trailhead> endpoints = endpoints(course);

					// name the route...endpoints must consider all routes from the
					// trailhead
					RouteNameSelector nameSelector = nameSelector(courseCompletions, courses);
					if (!nameSelector.isCourseConfidentAboutName()) {
						Iterable<RouteCompletion> completions = completionService.completions(routeId);
						nameSelector = nameSelector(completions, activities(completions));
					}
					this.eventService.namesUpdated(nameSelector.eventNames());
					details.nameSelector(nameSelector);
					mutator.course(course).name(nameSelector.routeName()).start(endpoints.getLeft())
							.finish(endpoints.getRight());
				}

				// distances come wtih completion stats now
				mutator.stale(false).completionStats(organizer.completionStats())
						.socialCompletionStats(organizer.socialStats()).courseCompletionStats(organizer.courseStats());
				// this call persists...it's a connected mutator
				Route updated = mutator.build();
				details.updated(updated);

				/*
				 * TN-858 disabled pending map reduce improvements
				 * queue.add(RouteWebApiUri.routeCompletionSocialGroupUpdatedTask(routeId));
				 * details.
				 * socalGroupForEventCompletionsTaskCount(eventCompletionsSocialGroupUpdatedTasksPosted
				 * (organizer .events().keySet()));
				 */
			}

		}

		return details.build();
	}

	/**
	 * @param events
	 * @return
	 */
	private Integer eventCompletionsSocialGroupUpdatedTasksPosted(Iterable<RouteEvent> events) {
		BatchQueue queue = BatchQueue.create().setQueue(this.queue).build();
		// should we limit the events that get posted? event#scoreIsSignificant?
		for (RouteEvent routeEvent : events) {
			queue.add(RouteEventWebApiUri.socialGroupForCompletionsPutTask(routeEvent.id()));
		}
		return queue.finish();
	}

	/**
	 * provided the RouteCompletions, ideally one for each participant, this will register the group
	 * memberships for the SocialGroup that relates all people that have Completed the route.
	 * Uniqueness will be enforced by SocialGroupMembership service and the datastore via the key.
	 * 
	 * @param personIds
	 * @throws EntityNotFoundException
	 */
	RouteCompletionSocialGroupOrganizer completionSocialGroupMembershipsUpdated(Route route,
			Iterable<RouteCompletion> completions) throws EntityNotFoundException {

		final RouteCompletionSocialGroupOrganizer.Builder organizerBuilder = RouteCompletionSocialGroupOrganizer
				.create();
		Set<SocialGroupId> socialGroupIds = route.socialGroupIds();
		if (CollectionUtils.isNotEmpty(socialGroupIds)) {
			SocialGroups existingSocialGroups = socialGroupService.groupService().socialGroups(socialGroupIds);
			organizerBuilder.socialGroupsExisting(existingSocialGroups);
		}
		organizerBuilder.routeTargeted(route);
		organizerBuilder.completions(completions);
		organizerBuilder.membershipUpdateFunctions(this.socialGroupService.membershipsRegisteredFunction());
		organizerBuilder.socialGroupsExisting(completionSocialGroups(route.id()));
		final RouteCompletionSocialGroupOrganizer organizer = organizerBuilder.build();
		// update the social group ids in route
		this.repository.update(route.id()).socialGroups(IdentifierUtils.ids(organizer.socialGroupsUpdated())).build();
		return organizer;
	}

	/**
	 * Organizes the social groups for completions of the route.
	 * 
	 * @see #completionSocialGroupMembershipsUpdated(Route, Iterable)
	 * @param routeId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteCompletionSocialGroupOrganizer completionSocialGroupMembershipsUpdated(RouteId routeId)
			throws EntityNotFoundException {
		Iterable<RouteCompletion> completions = completionService.completionsRace(routeId);
		final RouteCompletionSocialGroupOrganizer organizer = completionSocialGroupMembershipsUpdated(	route(routeId),
																										completions);
		// update the related routes
		queue.add(RouteWebApiUri.routeRelatedUpdatedTask(routeId));
		return organizer;
	}

	/**
	 * Tasks are posted to call {@link #completionSocialGroupMembershipsUpdated(RouteId)} for each
	 * route matching the filter.
	 * 
	 * @param filter
	 * @return
	 */
	public Integer completionSocialGroupMembershipsUpdated(Filter filter) {
		Iterable<RouteId> idPaginator = repository.idPaginator(filter);
		BatchQueue queue = BatchQueue.create().setQueue(this.queue).build();
		for (RouteId routeId : idPaginator) {
			queue.add(RouteWebApiUri.routeCompletionSocialGroupUpdatedTask(routeId));
		}
		return queue.finish();
	}

	/**
	 * Registers memberships for the persons to completion the route completions in the event given.
	 * The social group may be created and then assigned to the RouteEvent if none has been created
	 * yet.
	 * 
	 * @param routeEventId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public SocialGroup eventCompletionsSocialGroupUpdated(RouteEventId routeEventId) throws EntityNotFoundException {
		RouteEvent routeEvent = this.eventService.routeEvent(routeEventId);

		Iterable<RouteCompletion> completions = this.completionService.completions(routeEventId);
		// notice the personal totals represents the route, not the event
		Iterable<Entry<PersonId, Integer>> personalTotals = RouteCompletionUtil.personalTotalsIdentified(completions);
		SocialGroup group;

		// every event gets a social group so we can detect personal relationships for all
		if (routeEvent.hasSocialGroup()) {
			final SocialGroupId socialGroupId = routeEvent.socialGroupId();
			try {
				group = socialGroupService.membershipsRegistered(socialGroupId, personalTotals);
			} catch (EntityNotFoundException e) {
				LOG.warning(routeEventId + " reference missing social group " + socialGroupId);
				group = null;
			}
		} else {
			group = null;
		}
		// if not group has been assigned for any reason it's time to create one
		if (group == null) {
			group = SocialGroup.create().category(RouteEvent.FIELD.DOMAIN).context(routeEventId)
					.score(routeEvent.score()).build();
			group = socialGroupService.groupService().socialGroupPersisted(group);
			group = socialGroupService.membershipsRegistered(group.id(), personalTotals);
			routeEvent = eventService.socialGroupAssigned(routeEventId, group.id());
		}

		return group;
	}

	/**
	 * @param completions
	 * @return
	 */
	private Iterable<Activity> activities(final Iterable<RouteCompletion> completions) {
		return activityService.activities(RouteCompletionUtil.activityIds(completions));
	}

	/**
	 * @param completions
	 * @param activities
	 * @return
	 */
	private RouteNameSelector nameSelector(final Iterable<RouteCompletion> completions, Iterable<Activity> activities) {
		// this value is purely empirical
		final int minScoreForConfident = 25;
		RouteNameSelector nameSelector = RouteNameSelector.create().completions(completions)
				.defaultRouteNameSupplier(RouteNameSupplier.nul()).minScoreForConfident(minScoreForConfident).build();
		return nameSelector;
	}

	/**
	 * Updates those completions that match the route filter. This filter only works with routes as
	 * the focus. Individul completions should be updated individully?
	 * 
	 * @param filter
	 * @return
	 */
	public Integer completionsUpdated(Filter filter) {
		Integer result;
		Filter routeFilter = RouteDatastoreFilterBuilder.clone(filter).build();
		Iterable<RouteId> idPaginator = this.repository.idPaginator(routeFilter);
		if (filter.containsCondition(OPTION.TOUCH)) {
			result = this.completionService.touched(idPaginator);
			// these don't make stale...do that separately using OPTION.STALE
			// with route
		} else {
			BatchQueue queue = BatchQueue.create().setQueue(this.queue).build();
			for (RouteId routeId : idPaginator) {
				queue.add(RouteWebApiUri.routeCompletionUpdatedTask(routeId));
			}
			result = queue.finish();
		}
		return result;
	}

	/**
	 * Directly updates every attempt of every course that is related to this route. This is a
	 * potentially large method since it will find many of the same activity
	 * 
	 * @param routeId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Collection<RouteCompletion> completionsUpdated(RouteId routeId) throws EntityNotFoundException {
		Route route = route(routeId);
		Iterable<ActivityId> activityIds = completionService.courseIds(routeId);
		// the map reduces each attempt to a single completion
		HashMap<ActivityId, ActivityTiming> timings = Maps.newHashMap();
		for (ActivityId courseId : activityIds) {
			Iterable<ActivityTiming> timingsForCourse = this.timingService.timingsCompletedForCourse(courseId);
			for (ActivityTiming activityTimingRelation : timingsForCourse) {
				timings.put(activityTimingRelation.attempt().getActivityId(), activityTimingRelation);
			}

		}
		repository.setStale(routeId, true);
		// FIXME: the minimization logic includes nothing to pick the best
		// consider a sorted multimap where the values are sorted by best pick
		return this.completionService.courseCompleted(route, timings.values());

	}

	/**
	 * When the completions for a specific person within a certain route is needed, this will find
	 * the aliases for person and route and search by those aliases.
	 * 
	 * @param routeId
	 * @param personId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 *             when either the route or person are not found
	 */
	public RouteCompletions completions(RouteId routeId, PersonId personId, Filter filter)
			throws EntityNotFoundException {
		Person person = personService.person(personId);
		return completionService.completions(RouteCompletionFilterBuilder.clone(filter).person(person)
				.route(route(routeId)).build());
	}

	/**
	 * Posts a completion update task for all timings related to the given activity id as a course.
	 * 
	 * @param courseId
	 * @throws EntityNotFoundException
	 */
	public Collection<RouteCompletion> completionsUpdated(ActivityId activityId) throws EntityNotFoundException {
		Iterable<ActivityTiming> timings = this.timingService.timingsCompletedForCourse(activityId);
		ActivityTiming first = Iterables.getFirst(timings, null);
		if (first != null) {
			Route route = routeManaged(first);
			return this.completionService.courseCompleted(route, timings);
		} else {
			return Collections.emptyList();
		}

	}

	/**
	 * Given a RouteId this posts every combination of course ids to the timings queue ensuring that
	 * all identified courses have reciprocals already created.
	 * 
	 * @param routeId
	 * @return the number of tasks queued, but these may be different types of tasks.
	 * @throws EntityNotFoundException
	 */
	public Integer timingsUpdated(RouteId routeId) throws EntityNotFoundException {

		Iterable<ActivityId> courseIds = completionService.courseIds(routeId);
		Integer count = timingService.queueAttempCandidates(courseIds);
		count += timingService.queueTimings(ActivityTimingRelationUtil.timingIds(courseIds),
											timingService.getTimingCourseAttemptQueue());
		return count;

	}

	/**
	 * Posts tasks to call {@link #timingsUpdated(RouteId)}.
	 * 
	 * @param routeIds
	 * @return
	 */
	public Integer timingsUpdateQueued(RouteId... routeIds) {
		return timingsUpdateQueued(Lists.newArrayList(routeIds));
	}

	/**
	 * @see #timingsUpdateQueued(RouteId...)
	 * @param routeIds
	 * @return
	 */
	public Integer timingsUpdateQueued(Iterable<RouteId> routeIds) {
		BatchQueue batchQueue = BatchQueue.create().setQueue(queue).build();
		for (RouteId routeId : routeIds) {
			Task task = RouteWebApiUri.routesTimingsAttemptDiscoveryTask(routeId);
			batchQueue.add(task);
		}
		return batchQueue.finish();
	}

	/**
	 * Posts tasks for every timing id given in a batch.
	 * 
	 * @param timingIds
	 * @return the number posted.
	 */
	Integer completionsUpdated(Iterable<ActivityTimingId> timingIds) {
		BatchQueue queue = BatchQueue.create().setQueue(this.queue).build();
		for (ActivityTimingId activityTimingRelationId : timingIds) {
			queue.add(RouteWebApiUri.routeCompletionForTimingTask(activityTimingRelationId));
		}
		return queue.finish();
	}

	/**
	 * Temporary method targeting activities participating in the leaderboard to be updated. TN-666
	 */
	public Route routeUpdatedWithActivityBounds(RouteId routeId) throws EntityNotFoundException {
		final Iterable<RouteCompletion> courseCompletions = this.completionService.courseCompletions(routeId);

		Iterable<Activity> attempts = activityService.activities(RouteCompletionUtil.activityIds(courseCompletions));
		Iterable<Activity> activitiesWithNull = Iterables.filter(	attempts,
																	ActivityUtil.nullTrackSummaryBoundsPredicate());
		for (Activity activity : activitiesWithNull) {
			// could be batch, but not that many will be posted comparitively
			importService.queueActivityForUpdate(activity.id());
		}
		// return the original because changes will be asynch
		return route(routeId);
	}

	/**
	 * Provides a persistent trailhead for the finish course point.
	 * 
	 * @see #trailhead(GeoCoordinate)
	 * @param course
	 * @return
	 */
	private Trailhead finish(Activity course) {
		return trailhead(course.getTrackSummary().getFinish().getLocation());
	}

	/**
	 * Provides a perisent trailhead in the area near the given course endpoint. It may be the start
	 * or the finish. This will use the best means to find the ideal trailhead now by being
	 * efficient if possible and thorough when necessary.
	 * 
	 * @param course
	 * @return
	 */
	private Trailhead trailhead(GeoCoordinate location) {
		return trailheadService.nearestCreated(location);
	}

	public Trailhead trailhead(Route route) throws EntityNotFoundException {
		return this.trailheadProvider.trailhead(route.start());
	}

	/**
	 * Organizes alternates from the given trailhead providing a {@link List} of routes with each
	 * routes being part of the same group...in order of priority. The returned items don't yet have
	 * the mainId assigned, but it is assigned in the datastore. This is called by the workflow at
	 * some period to update a trailhead when necessary. FIXME: I wish this were package protected,
	 * but RouteWebApi is not in the same directory.
	 * 
	 * @see AlternateRouteGroupOrganizer
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public AlternateRouteGroupOrganizer alternatesOrganized(TrailheadId trailheadId) throws EntityNotFoundException {
		// collect all routes public/private. separate them as needed, but they should all be
		// organized
		Filter filter = RouteDatastoreFilterBuilder.create().start(trailheadId).orderByNone().setLimitToNone().build();
		Iterable<Route> routes = repository.entityPaginator(filter);

		// TODO:put this in a better place
		// the overlap required in reciprocal to be considered an alternate
		final double minOverlapRatio = 2.0 / 3.0;
		final AlternateRouteGroupOrganizer organizer = new AlternateRouteGroupOrganizer.Builder().routes(routes)
				.minOverlapForAlternate(MeasurementUtil.ratio(minOverlapRatio))
				.minNumberOfCoursesToScore(options.minNumberOfCoursesToScore)
				.spatialProvider(this.spatialService.idFunction()).build();
		// update the routes that have been mutated
		Set<Route> mutated = organizer.mutated();
		if (!mutated.isEmpty()) {
			repository.storeAll(mutated);
		}
		return organizer;
	}

	/**
	 * Given a routeId this will return all routes that are part of the group starting with the
	 * route's main ordered of score. The main is identifiable by {@link Route#isMain()}. If the
	 * given {@link Route#isScoreInsignificant()} then the route will be returned by itself.
	 * 
	 * This uses the search service. Use the {@link RouteDatastoreFilterBuilder} directly if you
	 * wish otherwise.
	 * 
	 * @param routeId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Routes routesForMain(RouteId routeId, Filter filter) throws EntityNotFoundException {
		Route route = route(routeId);

		final RouteSearchFilterBuilder filterClone = RouteSearchFilterBuilder.clone(filter).mainId(route.mainId());
		// routes with no score won't be found since they aren't part of the
		// score index
		if (route.isScoreInsignificant()) {
			// order by nothing since there is likely only 1
			filterClone.orderByNone();
		} else {
			filterClone.orderBy(RouteField.Key.SCORE, Direction.DESCENDING);
		}
		return routes(filterClone.build());
	}

	public Iterable<Route> routesForMain(RouteId routeId) {
		return routes(RouteDatastoreFilterBuilder.create().group(routeId).build());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteTrackProvider#track(com.aawhere.route.Route)
	 */
	@Override
	@Nonnull
	public Iterable<Trackpoint> track(@Nonnull Route route) throws EntityNotFoundException {
		return activityService.getTrack(route.courseId());
	}

	public TrackInfo trackInfo(RouteId routeId) throws EntityNotFoundException {
		return activityService.trackInfo(route(routeId).courseId());
	}

	/**
	 * Returns the track from the selected course to represent.
	 * 
	 * @param routeId
	 * @return
	 */
	public Iterable<Trackpoint> track(RouteId routeId) throws EntityNotFoundException {
		return track(route(routeId));
	}

	/**
	 * @return
	 */
	private Logger getLogger() {
		return LOG;
	}

	/**
	 * Given the {@link TrailheadId} this will return only the mains representing that trailhead in
	 * order of {@link Route#score}.
	 * 
	 * @deprecated use your own Filter Builder
	 * @param start
	 * @return
	 */
	@Deprecated
	public Routes mains(TrailheadId start, Filter filter) {
		return routes(routesForTrailheadFilter(start, RouteDatastoreFilterBuilder.clone(filter).mains(true)).build());
	}

	/**
	 * @deprecated use the filter
	 * @param start
	 * @return
	 */
	@Deprecated
	public Routes mains(TrailheadId start) {
		return mains(start, RouteDatastoreFilterBuilder.create().build());
	}

	/**
	 * Provides the Route groups of main/alternates starting from the given {@link Trailhead} that
	 * have been persisted in the system already (processed by
	 * {@link #alternatesOrganized(TrailheadId)}. Returned is a collection of {@link Routes} in
	 * order of the most popular main route first and subsequent {@link Routes} are less popular
	 * consecutively. Each {@link Routes} has alternates in order of popularity starting with the
	 * main route and followed by alternates. There is no filter provided since all routes from this
	 * trailhead will be provided.
	 * 
	 * @param trailheadId
	 * @return
	 */
	public FilteredResponse<Routes> routesGroupedByMain(TrailheadId start, Filter filter) {
		Routes mains = mains(start, filter);
		// mains are sorted by priority, so the groups will be too.
		ArrayList<Routes> groups = new ArrayList<>(mains.size());
		// for each main, find all the routes with the main id (the main
		// included).
		for (Route main : mains) {
			try {
				groups.add(routesForMain(main.id(), filter));
			} catch (EntityNotFoundException e) {
				LOG.warning(e.getMessage()
						+ " not found when we just found it...likely some merge is racing against this so another try might clear it up.");
				throw e.comeBackLater();
			}
		}
		return FilteredResponse.create(groups).build();
	}

	/**
	 * Provides the group for this Route if one exists.
	 * 
	 * @param routeId
	 * @return all social groups associated to the route or null if none have been associated yet
	 * @throws EntityNotFoundException
	 */
	@Nullable
	public SocialGroups completionSocialGroups(@Nonnull RouteId routeId) throws EntityNotFoundException {
		return this.socialGroupService.groupService().socialGroups(route(routeId).socialGroupIds());
	}

	/**
	 * @return the trailheadService
	 */
	public TrailheadService trailheadService() {
		return this.trailheadService;
	}

	public PersonService personService() {
		return personService;
	}

	/**
	 * Handy method for retrieving a course from a route. Notice {@link EntityNotFoundException} is
	 * not thrown since this case should not happen.
	 * 
	 * @param route
	 * @return
	 */
	public Activity course(Route route) {
		try {
			return activityService.getActivity(route.courseId());
		} catch (EntityNotFoundException e) {
			// a course id should never be missing. if valid we have left
			// problems
			throw new IllegalStateException(route.id() + " declares a missing course " + route.courseId());
		}
	}

	/**
	 * Searches for references of the start/finish trailheads and updates each of them from the
	 * orginal to the replacement.
	 * 
	 * @param original
	 * @param replacement
	 */
	private void trailheadChanged(TrailheadId original, TrailheadId replacement) {
		{
			Filter filter = RouteDatastoreFilterBuilder.create().start(original).orderByNone().build();
			RouteTrailheadMutator trailheadMutator = RouteUtil.startMutator(repository);
			trailheadChanged(original, replacement, filter, trailheadMutator);
		}
		{
			Filter filter = RouteDatastoreFilterBuilder.create().finish(original).orderByNone().build();
			RouteTrailheadMutator trailheadMutator = RouteUtil.finishMutator(repository);
			trailheadChanged(original, replacement, filter, trailheadMutator);
		}
	}

	/**
	 * Runs the filter finding those routes
	 * 
	 * @param original
	 * @param replacement
	 * @param filter
	 * @param trailheadMutator
	 */
	private void trailheadChanged(TrailheadId original, TrailheadId replacement, Filter filter,
			RouteTrailheadMutator trailheadMutator) {
		Iterable<Route> routes = repository.entityPaginator(filter);

		for (Route route : routes) {
			try {
				trailheadMutator.setTrailhead(route.getId(), replacement);
			} catch (EntityNotFoundException e) {
				// although wierd, not impossible...log and continue
				getLogger().warning(route.id() + " not found when iterating results from start=  " + original);
			}
		}
	}

	/**
	 * Posts a route/trailhead update task for every trailhead found. The
	 * 
	 * @see #trailheadUpdated(TrailheadId)
	 * @see RouteWebApiUri#routeTrailheadUpdateTask(com.aawhere.id.Identifier)
	 * @see #trailheadsOrganized(TrailheadId)
	 * @param filter
	 * @return
	 */
	public Integer trailheadsUpdated(Filter filter) {
		BatchQueue batchQueue = BatchQueue.create().setQueue(queue).build();
		for (TrailheadId id : trailheadService.trailheadIdsPaginator(filter)) {
			batchQueue.add(TrailheadWebApiUri.routeTrailheadUpdateTask(id));
		}
		return batchQueue.finish();
	}

	/**
	 * Given those items of {@link Routes} that can affect a trailhead this will update the
	 * trailhead with the latest information. Name and score.
	 * 
	 * @see #trailheadsUpdated(Filter)
	 * @param id
	 * @return
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteTrailheadOrganizer trailheadUpdated(TrailheadId trailheadId) throws EntityNotFoundException {
		// interest in updated the trailhead exactly matching that which is
		// given, not the alias.
		// called to confirm the exact existence of this trailhead
		try {
			Trailhead trailhead = trailheadService.trailhead(trailheadId);
			return trailheadUpdated(trailhead, null);
		} catch (EntityNotFoundException e) {
			throw EntityNotFoundException.forQueue(e);
		}

	}

	/**
	 * Delegates update functions to appropriate tools that update the name, score, route stats and
	 * reports the results after updated the trailhead with the service.
	 * 
	 * @param original
	 * @param routes
	 * @return
	 * @throws EntityNotFoundException
	 */
	RouteTrailheadOrganizer trailheadUpdated(Trailhead original, @Nullable Routes routes)
			throws EntityNotFoundException {

		TrailheadId trailheadId = original.id();

		Boolean publicTrailhead;
		// routes provided are optional...so find associated routes if needed
		if (routes == null) {
			Filter filter = RouteDatastoreFilterBuilder.create().start(original).setLimitToMax().scoreMoreThanDefault()
					.orderByScore().build();
			routes = routes(filter);
			// TN-886 Segregate public and personal routes from trailheads
			// if a trailhead has any public route then do not include non-scoring routes
			// if a trailhead has no public routes then process everything so the personal routes
			// will benefit from aggregate information
			if (routes.isEmpty()) {
				routes = routes(RouteDatastoreFilterBuilder.create().start(original).setLimitToMax().build());
				publicTrailhead = false;
			} else {
				// routes exist so this trailhead should be shared with all
				publicTrailhead = true;
			}
		} else {
			// indicate for score calculator to answer this question
			publicTrailhead = null;
		}
		RouteAwareTrailheadScoreCalculator calculator = RouteAwareTrailheadScoreCalculator.create().routes(routes)
				.trailhead(original).build();
		if (publicTrailhead == null) {
			publicTrailhead = TrailheadUtil.scoreIsSignificantForPublic(calculator.score());
		}
		Trailhead.Builder trailheadMutator = trailheadService.trailheadMutator(original.id());
		// right now score separates public from non public
		if (publicTrailhead) {
			trailheadMutator.score(calculator.score());
		} else {
			trailheadMutator.score(Trailhead.DEFAULT_SCORE);
		}

		// regardless of public or not, provide the information for the benefit of any personal
		// viewer
		trailheadMutator.numberOfMains(calculator.numberOfMains());
		trailheadMutator.numberOfRoutes(calculator.numberOfRoutes());

		RouteTrailheadOrganizer.Builder organizer = RouteTrailheadOrganizer.create();
		organizer.publicTrailhead(publicTrailhead);
		organizer.original(original);
		organizer.scoreCalculator(calculator);

		// TN-917 moved trailhead name updating into map reduce.

		// TN-865 attempted to move stats calculation to trailhead processing, but consistency must
		// remain between route and trailhead
		ActivityStats completionCumulationStats = RouteUtil.completionCumulationStats(routes);
		organizer.cumulativeCompletionStats(completionCumulationStats);
		trailheadMutator.routeCompletionStats(completionCumulationStats);
		return organizer.updated(trailheadMutator.build()).build();
	}

	/**
	 * Organizes the trailheads around the given trailhead.
	 * 
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteTrailheadChooser trailheadsOrganized(@Nonnull TrailheadId trailheadId) throws EntityNotFoundException {
		final RouteTrailheadChooser trailheadsOrganized = trailheadsOrganized(GeoCellUtil.boundingBox(trailheadId
				.getValue()));
		return trailheadsOrganized;
	}

	/**
	 * Provided a large search area relative to {@link TrailheadUtil#MAXIMUM_TRAILHEAD_RADIUS} this
	 * will gather all {@link Trailheads} and {@link Routes} in the area and organize them leaving
	 * only a single trailhead within the max radius. It will update the routes to ensure that each
	 * route has the chosen trailhead associated to it. Trailheads determined to be too close will
	 * be merged and updates will happen to route start and finish references.
	 * 
	 * @see RouteTrailheadChooser
	 * @param routeId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteTrailheadChooser trailheadsOrganized(@Nonnull BoundingBox searchArea) throws EntityNotFoundException {

		if (searchArea == null) {
			throw InvalidArgumentException.required(RouteMessage.SEARCH_AREA);
		}
		// create a search area that will include just enough trailheads
		// the diaganal is the diameter so divide by 2
		if (QuantityMath.create(searchArea.getDiagonal()).dividedBy(2)
				.lessThan(MIN_TRAILHEADS_ORGANIZATION_SEARCH_RADIUS)) {
			searchArea = BoundingBoxUtil.boundedBy(searchArea.getCenter(), MIN_TRAILHEADS_ORGANIZATION_SEARCH_RADIUS);
		}

		// finding the routes in the area will lead to finding most trailheads too
		Filter routeAreaFilter = RouteDatastoreFilterBuilder.create().startIntersection(searchArea).orderByScore()
				.setLimitToMax().build();
		Iterable<Route> routesByArea = ImmutableList.<Route> builder()
				.addAll(repository.entityPaginator(routeAreaFilter)).build();

		// find all trailheads in the area..with or without score. let the organizer minimize those
		// not needed
		TrailheadDatastoreFilterBuilder trailheadFilterBuilder = TrailheadDatastoreFilterBuilder.create();
		trailheadFilterBuilder.area().intersection(searchArea);
		trailheadFilterBuilder.orderByNone();
		Filter trailheadFilter = trailheadFilterBuilder.build();

		// trailheads come from routes, ensured unique since there will be many repeats
		ImmutableSet<TrailheadId> areaTrailheadIds = ImmutableSet.<TrailheadId> builder()
				.addAll(RouteUtil.startsIds(routesByArea)).addAll(trailheadService.ids(trailheadFilter)).build();
		Iterable<Trailhead> trailheadsInArea = trailheadService.iterable(areaTrailheadIds);

		// it's all about getting the chooser setup
		RouteTrailheadChooser chooser = RouteTrailheadChooser.create().routes(routesByArea)
				.activityProvider(activityService).maxTrailheadRadius(TrailheadUtil.MAXIMUM_TRAILHEAD_RADIUS)
				.searchArea(searchArea).trailheads(trailheadsInArea).build();

		Map<Trailhead, Routes> trailheadRelations = chooser.trailheadRelations();
		Set<Trailhead> chosenTrailheads = trailheadRelations.keySet();

		for (Trailhead trailhead : chosenTrailheads) {
			// store will create or update alike. Scores updated by chooser
			trailheadService.store(trailhead);
			Routes related = trailheadRelations.get(trailhead);
			for (Route route : related) {
				this.repository.setStart(route.id(), trailhead.id());
			}
		}

		// merge trailheads by removing the old and replacing with the new
		// area merging has already been considered so now it's just id
		// replacement and cleanup
		SetMultimap<TrailheadId, TrailheadId> mergeCandidates = chooser.mergeCandidates();
		for (TrailheadId bestTrailhead : mergeCandidates.keySet()) {
			merge(bestTrailhead, mergeCandidates.get(bestTrailhead));
		}

		return chooser;
	}

	/**
	 * Provides the best {@link Activity} from the {@link Application} provided.
	 * 
	 * @param routeId
	 * @param applicationKey
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Activity courseRepresentative(RouteId routeId, @Nullable ApplicationKey applicationKey)
			throws EntityNotFoundException {
		Activity courseRepresentative;
		final Route route = route(routeId);
		if (applicationKey == null) {
			courseRepresentative = course(route);
		} else {
			ActivityId courseId = route.courseId();
			if (courseId.getApplicationKey().equals(applicationKey)) {
				courseRepresentative = activityService.getActivity(courseId);
			} else {
				RouteCompletion bestCompletion = this.completionService.courseBestForApp(routeId, applicationKey);
				if (bestCompletion == null) {
					throw new EntityNotFoundException(CompleteMessage
							.create(RouteMessage.COURSE_BY_APPLICATION_NOT_FOUND)
							.addParameter(RouteMessage.Param.ROUTE_ID, routeId)
							.addParameter(RouteMessage.Param.APPLICATION, KnownApplication.byKey(applicationKey))
							.build(), routeId);
				} else {
					courseRepresentative = activityService.getActivity(bestCompletion.activityId());
				}
			}
		}
		return courseRepresentative;
	}

	/**
	 * Updates all route start/finish references to the given best {@link Trailhead}. Removes the
	 * others leaving the built in "alias" to find the best in it's place should future requests be
	 * made.
	 * 
	 * @param best
	 * @param mergeCandidates
	 */
	void merge(TrailheadId best, Set<TrailheadId> mergeCandidates) {
		for (TrailheadId mergeCandidate : mergeCandidates) {
			trailheadChanged(mergeCandidate, best);
		}
		trailheadService.delete(mergeCandidates);
	}

	/**
	 * This should be called when a person has been merged so that the completions will reflect the
	 * update and ensure social and personal stats are updated accordingly. The
	 * {@link RouteCompletionOrganizer} updates the completions with the new person ids when
	 * {@link #routesUpdated()} is called. This delay also avoids overly aggressive processing every
	 * time a person is merged.
	 * 
	 * @param personIds
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Integer personsMerged(Iterable<PersonId> personIds) throws EntityNotFoundException {
		if (IterablesExtended.isEmpty(personIds)) {
			throw InvalidArgumentException.required(PersonMessage.ID_NAME);
		}
		Iterable<RouteId> routeIds;
		try {
			routeIds = completionService.routeIds(personIds);
		} catch (EntityNotFoundException e) {
			Set<PersonId> aliasIdsExisting = personService.aliasIdsExisting(personIds);
			RouteCompletions completionsForExisting = completionService.completions(RouteCompletionFilterBuilder
					.create().personIds(aliasIdsExisting).build());
			routeIds = RouteCompletionUtil.routeIds(completionsForExisting);
			LOG.warning(personIds + " caused " + e.getMessage() + ", for those existing we are updating " + routeIds);
		}
		return markedForUpdate(routeIds);
	}

	/**
	 * Provides a unique set of Persons resulting from the filter provided. If the
	 * {@link Filter#hasQuery()} then that query is used with
	 * {@link #personsMatchingName(RouteId, String)} to provide only those matching a
	 * name...allowing for a lookup.
	 * 
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Persons persons(RouteId routeId, Filter filter) {
		Persons persons;
		filter = RouteCompletionFilterBuilder.clone(filter).routeId(routeId).build();
		final RouteCompletions completions = completionService.completions(filter);
		HashSet<PersonId> personIds = Sets.newHashSet(RouteCompletionUtil.personIds(completions));
		try {
			persons = personService.persons(personIds, completions);
		} catch (EntityNotFoundException e) {
			Iterable<RouteId> routeIds = RouteCompletionUtil.routeIds(completions);
			markedForMerge(routeIds);
			routesUpdated(routeIds);
			throw e.comeBackLater();
		}

		return persons;
	}

	public FieldDictionary dictionary() {
		return this.repository.dictionary();
	}

	/**
	 * Since each route has a course as a 1:1 relationship that best represents the route, this will
	 * return the map of keys. You may use BiMap since it is guaranteed that the values will be
	 * unique. Not done so for efficiency sake up
	 * 
	 * @param routeIds
	 * @return
	 */
	public BiMap<RouteId, ActivityId> courseIds(Iterable<RouteId> routeIds) {
		RouteObjectifyRepository repo = (RouteObjectifyRepository) this.repository;
		// FIXME: This should use a projection query.
		Map<RouteId, Route> routeMap = repo.loadAll(routeIds);
		return CollectionUtilsExtended.transformValuesIntoBiMap(routeMap, RouteUtil.courseIdFunction());
	}

	/**
	 * @return the queue
	 */
	Queue queue() {
		return this.queue;
	}

	/**
	 * Provided the target route this will provide the completions related to it or an alias.
	 * 
	 * @param routeId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 *             when no route can be found given the id
	 */
	public RouteCompletions completions(RouteId routeId, Filter filter) throws EntityNotFoundException {
		return this.completionService.completions(route(routeId).id(), filter);
	}

	/**
	 * @return the completionService
	 */
	public RouteCompletionService getCompletionService() {
		return this.completionService;
	}

	/**
	 * Pass this into the constructor if you wish to configure the service in a different way than
	 * the default. Add properties as needed via the mutators. A {@link Options#copy()} is made upon
	 * assignment protecting further mutation.
	 */
	@Singleton
	public static class Options
			implements Cloneable {

		public Integer minNumberOfCoursesToScore = RouteScoreCalculator.COURSE_COUNT_MIN;
		/**
		 * If a Route has only a single course then nuke it.
		 */
		private Integer minNumberOfCoursesToKeep = 2;
		/**
		 * If a route has been viewed this many times then it should remain available as an alias.
		 */
		private Integer minNumberOfViewsToKeepAlias = 100;

		public Options minNumberOfCoursesToKeep(Integer min) {
			this.minNumberOfCoursesToKeep = min;
			return this;
		}

		public Options minNumberOfViewsToKeepAlias(Integer minNumberOfViewsToKeepAlias) {
			this.minNumberOfViewsToKeepAlias = minNumberOfViewsToKeepAlias;
			return this;
		}

		private Options copy() {
			try {
				return (Options) super.clone();
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Route apply(RouteId input) {
		try {
			return (input != null) ? route(input) : null;
		} catch (EntityNotFoundException e) {
			return null;
		}
	}

	public RouteTrailheadProvider trailheadProvider() {
		return this.trailheadProvider;
	}

	/**
	 * Overrides the default behavior for {@link TrailheadProvider} with ways to handle the
	 * route..updating and such.
	 * 
	 * @see RouteService#trailheadProvider()
	 * @author aroller
	 */
	@Singleton
	private class RouteTrailheadProviderService
			extends ServiceStandardBase<Trailheads, Trailhead, TrailheadId>
			implements RouteTrailheadProvider {

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.ServiceGeneralBase#idFunction()
		 */
		@Override
		public Function<TrailheadId, Trailhead> idFunction() {
			return new Function<TrailheadId, Trailhead>() {

				@Override
				public Trailhead apply(TrailheadId input) {
					try {
						return (input != null) ? trailhead(input) : null;
					} catch (EntityNotFoundException e) {
						return null;
					}
				}
			};
		}

		/**
		 * This must override the default behavior of {@link ServiceGeneralBase#all(Iterable)} which
		 * does individual gets instead of bulk. By using the trailhead service we will not be
		 * cleaning up route as does the other overriding functions {@link #nearest(TrailheadId)},
		 * {@link #trailhead(TrailheadId)}, etc.
		 */
		@Override
		public Map<TrailheadId, Trailhead> all(Iterable<TrailheadId> ids) {
			return trailheadService.all(ids);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.trailhead.TrailheadProvider#trailhead(com.aawhere.trailhead
		 * .TrailheadId)
		 */
		@Override
		public Trailhead trailhead(TrailheadId trailheadId) throws EntityNotFoundException {
			return nearest(trailheadId);
		}

		/**
		 * {@link TrailheadProvider} methods provided by Route with the assumption that the id being
		 * called came from a route, not from the public. This assumption will then attempt to
		 * update datastore references. WARNING: if you call this with any old trailhead id then we
		 * will waste a lot of resources looking for your bogus id.
		 */
		@Override
		public Trailhead nearest(TrailheadId trailheadId) throws EntityNotFoundException {
			try {
				Trailhead result = trailheadService.nearest(trailheadId);
				TrailheadId replacementId = result.id();
				if (!replacementId.equals(trailheadId)) {
					getLogger().warning(trailheadId + " is stale and being replaced by " + replacementId);
					trailheadChanged(trailheadId, replacementId);
				}
				return result;
			} catch (EntityNotFoundException e) {
				getLogger().severe(trailheadId + " nor anything nearby is found.  queing updates");
				// even the nearest isn't found...let's repair for future and
				// fail this one
				routesUpdated(RouteDatastoreFilterBuilder.create().start(trailheadId).build());
				routesUpdated(RouteDatastoreFilterBuilder.create().finish(trailheadId).build());
				throw e;
			}
		}

	}

	/**
	 * Provides the RouteRelator details used to calculate the best related routes given a variety
	 * of information like social relationships and spatial relations.
	 * 
	 * @param focus
	 * @return the relators, if any are found to be worth processing...empty otherwise
	 * @throws EntityNotFoundException
	 */
	@Nonnull
	public Collection<RouteRelator> relatorsUpdated(@Nonnull RouteId focus) throws EntityNotFoundException {
		ImmutableList.Builder<RouteRelator> relators = ImmutableList.builder();
		Route route = route(focus);
		if (route.isScoreSignificant()) {
			Iterable<Route> routesForMain = routesForMain(route.id());
			Set<ActivityType> activityTypes = CategoricalStatistic.categories(route.completionStats()
					.activityTypeStats());

			ArrayList<RouteRelation> routeRelations = Lists.newArrayList();
			// right now there are plenty of one-way routes with high completion counts so score is
			// more
			// important
			SocialGroupRelator.Options options = SocialGroupRelator.Options.create().groupScoreWeight(2)
					.inCommonCountWeight(1);
			final Set<SocialGroupId> socialGroupIds = route.socialGroupIds();
			if (socialGroupIds != null) {
				for (SocialGroupId socialGroupId : socialGroupIds) {
					SocialGroupRelator socialGroupRelator = socialGroupService.groupsInCommon(socialGroupId, options);
					RouteRelator routeRelator = RouteRelator.create()
							.alternatesOfTarget(IdentifierUtils.ids(routesForMain)).idsFunction(idsFunction())
							.socialGroupRelator(socialGroupRelator).targetRoute(route).build();
					relators.add(routeRelator);
					// add the route relations to the final if the activity type is part of the
					// stats
					if (activityTypes.contains(routeRelator.activityType())) {
						routeRelations.addAll(routeRelator.routeRelations());
					}
				}
			}

			if (!routeRelations.isEmpty()) {
				route = repository.update(route.id()).routeRelations(routeRelations).build();
			}
		}
		return relators.build();

	}

	/**
	 * @param routeId
	 * @param filter
	 * @return
	 */
	public SocialGroups eventSocialGroups(RouteId routeId, Filter filter) {
		RouteEvents routeEvents = this.eventService.routeEvents(filter);
		Iterable<SocialGroupId> socialGroupIdsRelated = SocialGroupUtil.socialGroupIdsRelated(routeEvents);
		return socialGroupService.groupService().socialGroups(socialGroupIdsRelated);
	}

	/**
	 * Provides {@link SearchDocuments} directly from the {@link SearchService} catering to
	 * {@link Routes} using the {@link RouteSearchFilterBuilder}.
	 * 
	 * @param filter
	 * @return
	 */
	public SearchDocuments searchDocuments(Filter filter) {
		return searchService.searchDocuments(RouteSearchFilterBuilder.clone(filter).build());
	}

	/**
	 * Add all the routes to the search index. Updates routes if they already exist in the index.
	 * Does not delete routes that only exist in the index but exists in the datastore, so this is
	 * not a sync method.
	 * 
	 * @throws MaxIndexAttemptsExceeded
	 * 
	 * @return the number of routes updated.
	 */
	public Integer indexAllRoutes() throws MaxIndexAttemptsExceeded {
		Filter filter = Filter.create().orderBy(new FieldKey(Route.FIELD.DOMAIN, FIELD.SCORE), Direction.DESCENDING)
				.build();
		Iterable<RouteId> routeIds = repository.idPaginator(filter);
		return SearchWebApiUri.indexTasks(	routeIds,
											RouteWebApiUri.ROUTES,
											queueFactory.getQueue(SearchWebApiUri.SEARCH_QUEUE_NAME));
	}

	public SearchDocuments indexRoute(RouteId routeId) throws MaxIndexAttemptsExceeded, EntityNotFoundException {
		Route route = route(routeId);
		SearchDocuments routeDocuments = RouteSearchDocumentProducer.create().route(route).build().searchDocuments();
		searchService.index(routeDocuments);
		return routeDocuments;
	}

}
