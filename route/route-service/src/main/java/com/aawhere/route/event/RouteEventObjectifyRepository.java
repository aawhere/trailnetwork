/**
 *
 */
package com.aawhere.route.event;

import java.util.Map;

import org.joda.time.Interval;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.persist.ObjectifyFilterRepositoryDelegate;
import com.aawhere.util.rb.Message;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author aroller
 * 
 */
@Singleton
public class RouteEventObjectifyRepository
		extends ObjectifyFilterRepository<RouteEvent, RouteEventId, RouteEvents, RouteEvents.Builder>
		implements RouteEventRepository {

	static {
		ObjectifyService.register(RouteEvent.class);
	}

	final private RouteEventRepositoryDelegate delegate = new RouteEventRepositoryDelegate(this);

	/**
	 * @param fieldDictionaryFactory
	 */
	@Inject
	public RouteEventObjectifyRepository(FieldDictionaryFactory fieldDictionaryFactory) {
		super(new Delegate(fieldDictionaryFactory));
	}

	public RouteEventObjectifyRepository(FieldDictionaryFactory fieldDictionaryFactory, Synchronization synchronization) {
		super(new Delegate(fieldDictionaryFactory), synchronization);
	}

	private static class Delegate
			extends ObjectifyFilterRepositoryDelegate<RouteEvents, RouteEvents.Builder, RouteEvent, RouteEventId> {

		/**
		 * @param fieldDictionaryFactory
		 */
		public Delegate(FieldDictionaryFactory fieldDictionaryFactory) {
			super(fieldDictionaryFactory);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.route.event.RouteEventRepository#update(com.aawhere.route.event.RouteEventId,
	 * org.joda.time.Interval)
	 */
	@Override
	public RouteEvent interval(RouteEventId id, Interval interval) throws EntityNotFoundException {
		return this.delegate.interval(id, interval);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.event.RouteEventRepository#names(java.util.Map)
	 */
	@Override
	public Map<RouteEventId, RouteEvent> names(Map<RouteEventId, Message> names) {
		return this.delegate.names(names);
	}

	/**
	 * Updates the mutating event upon building.
	 * 
	 * @return
	 * @throws EntityNotFoundException
	 */
	public RouteEvent.Builder update(RouteEventId routeEventId) throws EntityNotFoundException {
		return new RouteEvent.Builder(load(routeEventId)) {
			@Override
			public RouteEvent build() {
				return update(super.build());

			}
		};
	}

}
