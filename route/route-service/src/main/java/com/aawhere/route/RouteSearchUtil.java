package com.aawhere.route;

import java.util.Map;

import com.aawhere.activity.ActivitySearchUtil;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.activity.RelativeStatsUtil;
import com.aawhere.field.DomainWrongException;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.route.RouteSearchDocumentProducer.Builder;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchUtils;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadProvider;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

public class RouteSearchUtil {

	public static RouteId routeId(RelativeStatistic relativeStats) {
		return routeId(relativeStats.id());
	}

	public static RouteId routeId(String searchDocumentId) throws DomainWrongException {
		String featureIdForDomain = RelativeStatsUtil.featureIdForDomain(searchDocumentId, RouteField.DOMAIN);
		if (featureIdForDomain != null) {
			return new RouteId(featureIdForDomain);
		} else {
			return null;
		}
	}

	public static RouteId routeId(SearchDocument searchDocument) {
		return routeId(searchDocument.id());
	}

	public static Function<String, RouteId> routeIdFromSearchDocumentIdFunction() {
		return new Function<String, RouteId>() {

			@Override
			public RouteId apply(String input) {
				return routeId(input);
			}
		};
	}

	public static Iterable<RouteId> routeIds(Iterable<String> searchDocumentIds) {
		return Iterables.transform(searchDocumentIds, routeIdFromSearchDocumentIdFunction());
	}

	/**
	 * Provides all the search documents for all the routes given.
	 * 
	 * @param routes
	 * @return
	 */
	public static Iterable<SearchDocument> searchDocuments(Iterable<Route> routes, TrailheadProvider trailheadProvider) {
		return Iterables.concat(Iterables.transform(routes, searchDocumentFunction(trailheadProvider)));
	}

	public static Function<Route, Iterable<SearchDocument>> searchDocumentFunction(
			final TrailheadProvider trailheadProvider) {
		return new Function<Route, Iterable<SearchDocument>>() {

			@Override
			public Iterable<SearchDocument> apply(Route input) {
				Builder builder = RouteSearchDocumentProducer.create();
				try {
					Trailhead start = trailheadProvider.trailhead(input.start());
					builder.startTrailhead(start);
				} catch (EntityNotFoundException e) {
					LoggerFactory.getLogger(getClass()).warning(input + " has a bad trailhead id " + e.getMessage());
					// trailhead name is optional...skip it
				}
				return builder.route(input).build().searchDocuments();
			}
		};
	}

	/**
	 * Provides the appropriate unique index name given the {@link Layer}.
	 * 
	 * @param layer
	 * @return
	 */
	public static String indexName(Layer layer) {
		return SearchUtils.indexName(RouteField.DOMAIN, layer.name());
	}

	/**
	 * Given the routes and corresponding {@link SearchDocuments} that have count information this
	 * will mutate the route and add the documents. This method should be protected.
	 * 
	 * The mutation happens immediately, however, the {@link RelativeStatistic} provided has a
	 * {@link #relativeStatsLazySupplier(SearchDocument)} assigned allowing for delayed processing
	 * without blocking
	 * 
	 * @param routesMap
	 * @param searchDocuments
	 * @return
	 */
	public static Iterable<Route> countsAdded(Iterable<Route> routes, Iterable<SearchDocument> searchDocuments) {
		// the routes may be duplicate and all are the same.
		// the search documents are unique and must find their corresponding map
		Map<RouteId, Route> routesMap = IdentifierUtils.map(ImmutableSet.<Route> builder().addAll(routes).build());
		// we need to force mutation so iterating now.
		for (SearchDocument searchDocument : searchDocuments) {
			RouteId routeId = routeId(searchDocument);
			Route route = routesMap.get(routeId);
			if (route != null) {
				RelativeStatistic routeCompletionCount = relativeStats(searchDocument);
				Route.mutate(route).addRelativeStat(routeCompletionCount).build();
			} else {
				LoggerFactory.getLogger(RouteSearchUtil.class).warning(searchDocument.id() + " provided missing route "
						+ routeId);
			}
		}
		return routesMap.values();
	}

	private static RelativeStatistic relativeStats(SearchDocument searchDocument) {
		return ActivitySearchUtil.relativeStats(searchDocument,
												RouteSearchDocumentProducer.PERSON_ID,
												RouteSearchDocumentProducer.ACTIVITY_TYPE,
												RouteSearchDocumentProducer.DISTANCE_CATEGORY,
												RouteSearchDocumentProducer.RELATIVE_COUNT);
	}

}
