/**
 * 
 */
package com.aawhere.trailhead;

import static org.junit.Assert.*;
import net.opengis.kml.v_2_2_0.PlacemarkType;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.doc.kml.KmlUnitTest;
import com.aawhere.measure.calc.GeoMathTestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.trailhead.TrailheadKml.Builder;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class TrailheadKmlUnitTest
		extends KmlUnitTest<TrailheadKml, PlacemarkType> {

	private Trailhead trailhead;
	private TrailheadKml kml;
	private Builder builder;

	@Before
	public void setUp() {
		trailhead = TrailheadTestUtil.createTrailhead(GeoMathTestUtil.EARTH_ORIGIN);
		builder = TrailheadKml.create(trailhead);
	}

	@Test
	public void testSimpleTrailhead() {
		build();

	}

	@Test
	public void testStandardLook() {
		builder.standardLook();

	}

	private void build() {
		if (this.builder != null) {

			this.kml = builder.build();
			assertNotNull(kml);
			assertNotNull(kml.get());
			assertNotNull(kml.get().getValue().getId());
			this.builder = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.kml.KmlUnitTest#main()
	 */
	@Override
	protected TrailheadKml main() {
		build();
		return kml;
	}

}
