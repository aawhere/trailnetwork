/**
 * 
 */
package com.aawhere.trailhead;

import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractFeatureType;
import net.opengis.kml.v_2_2_0.DocumentType;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.doc.kml.KmlUnitTest;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;

/**
 * Tests the multiple {@link Trailheads} going into a single kml doc.
 * 
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class TrailheadsKmlUnitTest
		extends KmlUnitTest<TrailheadsKml, DocumentType> {

	private TrailheadsKml kml;
	private TrailheadsKml.Builder builder;
	private Trailheads trailheads;

	@Before
	public void setUp() {
		this.trailheads = TrailheadTestUtil.trailheads();
		builder = TrailheadsKml.create(trailheads);
	}

	@Test
	public void testAll() {
		List<JAXBElement<? extends AbstractFeatureType>> abstractFeatureGroup = main().get().getValue()
				.getAbstractFeatureGroup();
		TestUtil.assertSize(trailheads.size(), abstractFeatureGroup);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.kml.KmlUnitTest#main()
	 */
	@Override
	protected TrailheadsKml main() {
		if (this.builder != null) {
			kml = this.builder.build();
			this.builder = null;
		}
		return kml;
	}

}
