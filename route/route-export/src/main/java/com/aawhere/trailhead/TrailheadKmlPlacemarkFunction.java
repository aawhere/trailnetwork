/**
 * 
 */
package com.aawhere.trailhead;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.PlacemarkType;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;

/**
 * Converts the provided {@link Trailhead} into the corresponding KML {@link PlacemarkType} to be
 * displayed in Google Earth and other kml viewers.
 * 
 * TODO: move this to route-export
 * 
 * @author aroller
 * 
 */
public class TrailheadKmlPlacemarkFunction
		implements Function<Trailhead, JAXBElement<PlacemarkType>> {

	/**
	 * Used to construct all instances of TrailheadKmlPlacemarkFunction.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadKmlPlacemarkFunction> {

		public Builder() {
			super(new TrailheadKmlPlacemarkFunction());
		}

		public TrailheadKmlPlacemarkFunction build() {
			TrailheadKmlPlacemarkFunction built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrailheadKmlPlacemarkFunction */
	private TrailheadKmlPlacemarkFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public JAXBElement<PlacemarkType> apply(@Nullable Trailhead trailhead) {
		return TrailheadKml.create(trailhead).standardLook().build().get();
	}

}
