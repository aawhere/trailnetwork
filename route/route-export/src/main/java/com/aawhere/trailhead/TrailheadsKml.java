/**
 * 
 */
package com.aawhere.trailhead;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.DocumentType;
import net.opengis.kml.v_2_2_0.PlacemarkType;

import com.aawhere.doc.kml.Kml;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.doc.kml.StyleKml;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Produces all the trailheads given into a single document well organized for browsing the
 * trailheads.
 * 
 * @author aroller
 * 
 */
public class TrailheadsKml
		implements Kml<DocumentType> {

	private static final long serialVersionUID = 1L;

	private DocumentType placemarks;

	/**
	 * Used to construct all instances of TrailheadsKml.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadsKml> {

		private Iterable<Trailhead> trailheads;
		private String styleUrl;

		public Builder() {
			super(new TrailheadsKml());
		}

		public TrailheadsKml build() {
			TrailheadsKml built = super.build();
			// TODO:create sub folders organized by area cells
			// TODO:disallow expansion of folders
			built.placemarks = KmlUtil.FACTORY.createDocumentType();
			final com.aawhere.doc.kml.StyleKml.Builder styleBuilder = TrailheadKml.create().standardLook().style();

			this.styleUrl = KmlUtil.styleUrl(styleBuilder.id());
			StyleKml styleKml = styleBuilder.build();
			built.placemarks.getAbstractStyleSelectorGroup().add(styleKml.get());
			Function<Trailhead, JAXBElement<PlacemarkType>> function = function();
			Iterable<JAXBElement<PlacemarkType>> elements = Iterables.transform(trailheads, function);
			Iterables.addAll(built.placemarks.getAbstractFeatureGroup(), elements);
			return built;
		}

		/**
		 * Used internally, but could be useful externally also.
		 * 
		 * @return
		 */
		public Function<Trailhead, JAXBElement<PlacemarkType>> function() {
			return new Function<Trailhead, JAXBElement<PlacemarkType>>() {
				@Override
				@Nullable
				public JAXBElement<PlacemarkType> apply(@Nullable Trailhead input) {
					return TrailheadKml.create(input).styleUrl(styleUrl).build().get();
				}
			};
		}

		/**
		 * @param trailheads
		 * @return
		 */
		public Builder trailheads(Iterable<Trailhead> trailheads) {
			this.trailheads = trailheads;

			return this;
		}

	}// end Builder

	public static Builder create(Iterable<Trailhead> trailheads) {
		return new Builder().trailheads(trailheads);
	}

	/** Use {@link Builder} to construct TrailheadsKml */
	private TrailheadsKml() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public JAXBElement<DocumentType> get() {
		return KmlUtil.FACTORY.createDocument(placemarks);
	}

}
