/**
 * 
 */
package com.aawhere.trailhead;

import java.net.URI;
import java.net.URISyntaxException;

import javax.annotation.Nullable;

import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.LinkType;
import net.opengis.kml.v_2_2_0.NetworkLinkType;
import net.opengis.kml.v_2_2_0.ObjectFactory;

import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.queue.WebApiEndPoint;
import com.aawhere.route.RouteWebApiUri;

import com.google.common.base.Function;
import com.google.common.net.MediaType;

/**
 * Converts the given trailhead to a folder that contains the trailhead placemark and a network link
 * to get routes related to the trailhead.
 * 
 * @author aroller
 * 
 */
public class TrailheadRoutesKmlFolderFunction
		implements Function<Trailhead, FolderType> {

	private TrailheadKmlPlacemarkFunction placemarkFunction;
	private ObjectFactory factory;

	/**
	 * Used to construct all instances of TrailheadRoutesKmlFolderFunction.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadRoutesKmlFolderFunction> {

		public Builder() {
			super(new TrailheadRoutesKmlFolderFunction());
		}

		public TrailheadRoutesKmlFolderFunction build() {
			TrailheadRoutesKmlFolderFunction built = super.build();
			built.factory = KmlUtil.FACTORY;
			built.placemarkFunction = TrailheadKmlPlacemarkFunction.create().build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	public static TrailheadRoutesKmlFolderFunction build() {
		return create().build();
	}

	/** Use {@link Builder} to construct TrailheadRoutesKmlFolderFunction */
	private TrailheadRoutesKmlFolderFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public FolderType apply(@Nullable Trailhead input) {
		FolderType folder = new FolderType();
		folder.setName(input.getId().getValue());
		folder.getAbstractFeatureGroup().add(TrailheadKml.create(input).build().get());
		NetworkLinkType networkLink = factory.createNetworkLinkType();
		URI endpoint;
		try {
			endpoint = new WebApiEndPoint.Builder().setEndPoint(RouteWebApiUri.TRAILHEADS_TRAILHEAD_ID_ROUTES_MAINS)
					.absolute().values(input.getId()).setMediaType(MediaType.KML).build().getUri();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
		LinkType link = factory.createLinkType();
		link.setHref(endpoint.toString());
		networkLink.setUrl(link);
		folder.getAbstractFeatureGroup().add(factory.createNetworkLink(networkLink));
		return folder;
	}

}
