/**
 * 
 */
package com.aawhere.trailhead;

import static com.aawhere.doc.kml.KmlUtil.*;

import java.io.StringWriter;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.PlacemarkType;

import com.aawhere.doc.kml.Kml;
import com.aawhere.doc.kml.KmlStandardIcon;
import com.aawhere.doc.kml.StyleKml;
import com.aawhere.doc.kml.geom.GeoCoordinateToKmlPointFunction;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.style.AlphaChannel;
import com.aawhere.style.Color;

import com.google.common.net.MediaType;
import com.googlecode.jatl.Html;

/**
 * Provides a {@link PlacemarkType} that represents a {@link Trailhead} using it's location. The
 * style is offered for manipulating the marker for visual clarity. the {@link #changeStyle()} is
 * offered for animation of the styles.
 * 
 * @author aroller
 * 
 */
public class TrailheadKml
		implements Kml<PlacemarkType> {
	public static class Theme {
		public static final Color SHOW_TRAILHEAD_COLOR = Color.GREEN;
		public static final Color SHOW_TRAILHEAD_LABEL_COLOR = Color.WHITE;
		public static final Color HIDE_TRAILHEAD_LABEL_COLOR = AlphaChannel.TRANSPARENT
				.apply(SHOW_TRAILHEAD_LABEL_COLOR);
	}

	private static final long serialVersionUID = -3771527045502250378L;
	private PlacemarkType placemark;
	private StyleKml style;

	/**
	 * Used to construct all instances of TrailheadKml.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadKml> {

		private Trailhead trailhead;
		private StyleKml.Builder style;
		private PlacemarkType marker = FACTORY.createPlacemarkType();
		private String trailheadId;

		public Builder() {
			super(new TrailheadKml());
		}

		public Builder trailhead(Trailhead trailhead) {
			this.trailhead = trailhead;
			final String idValue = trailhead.getId().toString();
			this.trailheadId = id(idValue);
			marker.setId(trailheadId);
			return this;
		}

		/**
		 * @param idValue
		 * @return
		 */
		protected String id(final String idValue) {
			return IdentifierUtils.createCompositeIdValue(Trailhead.FIELD.DOMAIN, idValue);
		}

		public Builder standardLook() {
			com.aawhere.doc.kml.StyleKml.Builder styleBuilder = style();
			styleBuilder.icon().icon(KmlStandardIcon.CIRCLE).color(TrailheadKml.Theme.SHOW_TRAILHEAD_COLOR);
			styleBuilder.label().color(TrailheadKml.Theme.HIDE_TRAILHEAD_LABEL_COLOR);
			return this;
		}

		public Builder styleUrl(String styleUrl) {
			marker.setStyleUrl(styleUrl);
			return this;
		}

		public StyleKml.Builder style() {
			if (trailheadId == null) {
				trailheadId = id("all");
			}
			if (style == null) {
				this.style = StyleKml.create();
				this.style.id(trailheadId);
			}
			return style;
		}

		public TrailheadKml build() {
			TrailheadKml built = super.build();
			// trailhead may be null for changes
			if (style != null) {
				built.style = style.build();
				marker.getAbstractStyleSelectorGroup().add(built.style.get());
			}

			if (trailhead != null) {

				marker.setAbstractGeometryGroup(GeoCoordinateToKmlPointFunction.build().element(trailhead.location()));
				marker.setName(String.valueOf(trailhead.name()));

				// description for pop-up
				StringWriter writer = new StringWriter();
				new Html(writer) {
					{
						Html p = p();
						p.a().href(RouteWebApiUri.routesForTrailheadEndpoint(trailhead.id(), MediaType.KML).toString())
								.text("Tour").end();
						p.text(" all routes from this trailhead.").end();
						p.text("(" + trailhead.score() + ")");
						done();
					}
				};

				marker.setDescription(writer.toString());
			}
			built.placemark = marker;
			marker = null;
			return built;
		}
	}// end Builder

	public static Builder create(Trailhead trailhead) {
		return new Builder().trailhead(trailhead);
	}

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrailheadKml */
	private TrailheadKml() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.kml.ObjectKml#element()
	 */
	@Override
	public JAXBElement<PlacemarkType> get() {
		return FACTORY.createPlacemark(placemark);
	}

	public StyleKml.Builder changeStyle() {
		return style.change();
	}

	/**
	 * Provide
	 * 
	 * @return
	 */
	public Builder change() {
		Builder change = new Builder();
		change.marker.setTargetId(this.placemark.getId());
		return change;
	}

	/**
	 * @return
	 */
	public com.aawhere.doc.kml.StyleKml.Changer changeStyle2() {
		return StyleKml.Changer.create(style);
	}
}
