/**
 * 
 */
package com.aawhere.trailhead;

import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.KmlType;

import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.doc.kml.KmlFolderTypeToElementFunction;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.collect.Lists;

/**
 * The main utility that translates {@link Trailheads} into KML objects for viewing.
 * 
 * @author aroller
 * @deprecated use {@link TrailheadsKml}
 */
@Deprecated
public class TrailheadKmlConverter {

	private KmlDocument document;

	/**
	 * Used to construct all instances of TrailheadKmlConverter.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadKmlConverter> {

		private Trailheads trailheads;

		public Builder() {
			super(new TrailheadKmlConverter());
		}

		public Builder trailheads(Trailheads trailheads) {
			this.trailheads = trailheads;
			return this;
		}

		public TrailheadKmlConverter build() {
			TrailheadKmlConverter built = super.build();

			TrailheadRoutesKmlFolderFunction function = TrailheadRoutesKmlFolderFunction.build();
			List<FolderType> placemarks = Lists.transform(this.trailheads.getCollection(), function);

			List<JAXBElement<FolderType>> elements = Lists
					.transform(placemarks, KmlFolderTypeToElementFunction.build());

			FolderType folder = KmlUtil.FACTORY.createFolderType();
			folder.getAbstractFeatureGroup().addAll(elements);
			built.document = KmlDocument.create().setMain(KmlUtil.FACTORY.createFolder(folder)).build();
			return built;
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrailheadKmlConverter */
	private TrailheadKmlConverter() {
	}

	/**
	 * @return the document
	 */
	public KmlDocument document() {
		return this.document;
	}

	public JAXBElement<KmlType> element() {
		return this.document.getElement();
	}
}
