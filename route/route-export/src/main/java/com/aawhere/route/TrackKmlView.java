/**
 * 
 */
package com.aawhere.route;

import static com.aawhere.doc.kml.KmlUtil.*;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.USCustomarySystem;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractViewType;
import net.opengis.kml.v_2_2_0.AltitudeModeEnumType;
import net.opengis.kml.v_2_2_0.LookAtType;

import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.doc.kml.ViewKml;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.AngleMath;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.Track;
import com.aawhere.track.TrackSummary;

/**
 * Provides a {@link AbstractViewType} looking at the entire {@link Track} from it's start (or
 * optionally end). The {@link TrackSummary} provides a {@link BoundingBox} so the view is scaled
 * appropriately to the size and
 * 
 * TODO:abstract this into BoundingBoxViewKml
 * 
 * @author aroller
 * 
 */
public class TrackKmlView
		implements ViewKml<LookAtType> {

	public static enum Focus {
		/** where the route begines */
		START,
		/** Where the route ends, sometimes near the start */
		FINISH,
		/** The centerpoint of the boundinbox */
		CENTER,
		/** Useful when the start and finish are together and an overview of the route is desired. */
		FARTHEST_FROM_START

	}

	private LookAtType view;

	/**
	 * Used to construct all instances of RouteKmlView.
	 */
	public static class Builder
			extends ObjectBuilder<TrackKmlView> {

		private TrackSummary summary;
		private Focus focus = Focus.START;

		public Builder() {
			super(new TrackKmlView());
		}

		public Builder summary(TrackSummary summary) {
			this.summary = summary;
			return this;
		}

		public Builder focus(Focus focus) {
			this.focus = focus;
			return this;
		}

		public TrackKmlView build() {
			TrackKmlView built = super.build();
			BoundingBox box = summary.getBoundingBox();

			GeoCoordinate focusPoint;
			switch (this.focus) {
				case CENTER:
					focusPoint = box.getCenter();
					break;
				case FINISH:
					focusPoint = summary.getFinish().getLocation();
					break;
				case FARTHEST_FROM_START:
					focusPoint = BoundingBoxUtil.farthestPoint(box, summary.getStart().getLocation());
					break;
				case START:
				default:
					focusPoint = summary.getStart().getLocation();
					break;
			}
			LookAtType camera = FACTORY.createLookAtType();
			camera.setAltitudeModeGroup(FACTORY.createAltitudeMode(AltitudeModeEnumType.RELATIVE_TO_GROUND));

			GeoCoordinate farthestPoint = BoundingBoxUtil.farthestPoint(box, focusPoint);
			Angle heading = GeoMath.heading(focusPoint, farthestPoint);
			camera.setHeading(heading.doubleValue(USCustomarySystem.DEGREE_ANGLE));

			QuantityMath<Length> length = QuantityMath.create(box.getDiagonal());

			double tilt;
			Length aboveGround;
			if (length.greaterThan(MeasurementUtil.createLengthInMeters(10000))) {
				tilt = 65;

				aboveGround = MeasurementUtil.createLengthInMeters(1500);
			} else if (length.greaterThan(MeasurementUtil.createLengthInMeters(50000))) {
				aboveGround = MeasurementUtil.createLengthInMeters(2000);
				tilt = 75;
			} else {
				aboveGround = MeasurementUtil.createLengthInMeters(1000);
				tilt = 60;
			}
			// relocate the viewpoint to be behind the actual point. it's easier to see the start
			focusPoint = GeoMath.coordinateFrom(focusPoint,
												AngleMath.create(heading).reverse().getQuantity(),
												aboveGround);
			camera.setLatitude(focusPoint.getLatitude().getInDecimalDegrees());
			camera.setLongitude(focusPoint.getLongitude().getInDecimalDegrees());

			camera.setTilt(tilt);
			camera.setRange(aboveGround.doubleValue(MetricSystem.METRE));
			// now add the known ground level from the start
			Length altitude = QuantityMath.create(aboveGround).getQuantity();
			// basically, set the view relative to the size of the activity with a max of 10K
			camera.setAltitude(altitude.doubleValue(MetricSystem.METRE));

			built.view = camera;
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct RouteKmlView */
	private TrackKmlView() {
	}

	/**
	 * @return the view
	 */
	public AbstractViewType getView() {
		return this.view;
	}

	/**
	 * @deprecated use standard {@link #get()}
	 * @return
	 */
	public JAXBElement<? extends AbstractViewType> getElement() {
		return get();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public JAXBElement<LookAtType> get() {
		return KmlUtil.FACTORY.createLookAt(view);
	}

	private static final long serialVersionUID = 6354845045822740516L;
}
