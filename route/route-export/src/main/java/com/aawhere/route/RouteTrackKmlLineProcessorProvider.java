/**
 * 
 */
package com.aawhere.route;

import javax.annotation.Nonnull;

import net.opengis.kml.v_2_2_0.MultiGeometryType;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.kml.TrackKmlProcessor;

/**
 * Processes {@link MultiGeometryType} line strings when given a {@link RouteTrackProvider} that
 * will retrieve the track.
 * 
 * @author aroller
 * 
 */
public class RouteTrackKmlLineProcessorProvider
		implements RouteTrackKmlLineProvider {

	private RouteTrackProvider trackProvider;

	/**
	 * Used to construct all instances of RouteTrackKmlLineProcessorProvider.
	 */
	public static class Builder
			extends ObjectBuilder<RouteTrackKmlLineProcessorProvider> {

		public Builder() {
			super(new RouteTrackKmlLineProcessorProvider());
		}

		public Builder provider(RouteTrackProvider provider) {
			building.trackProvider = provider;
			return this;
		}

		public RouteTrackKmlLineProcessorProvider build() {
			RouteTrackKmlLineProcessorProvider built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct RouteTrackKmlLineProcessorProvider */
	private RouteTrackKmlLineProcessorProvider() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteTrackKmlLineProvider#line(com.aawhere.route.Route)
	 */
	@Override
	public MultiGeometryType line(Route route) throws EntityNotFoundException {
		return TrackKmlProcessor.create().setTrack(trackProvider.track(route)).build().getTrackLineStrings();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteTrackProvider#track(com.aawhere.route.Route)
	 */
	@Override
	@Nonnull
	public Iterable<Trackpoint> track(@Nonnull Route route) throws EntityNotFoundException {
		return this.trackProvider.track(route);
	}

}
