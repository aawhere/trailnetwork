/**
 * 
 */
package com.aawhere.route;

import net.opengis.kml.v_2_2_0.MultiGeometryType;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.track.Track;

/**
 * Anything that can provide the {@link MultiGeometryType} line string for a {@link Track} related
 * to a {@link Route}.
 * 
 * @author aroller
 * 
 */
public interface RouteTrackKmlLineProvider
		extends RouteTrackProvider {

	public MultiGeometryType line(Route route) throws EntityNotFoundException;
}
