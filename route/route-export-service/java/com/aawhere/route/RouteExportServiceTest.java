/**
 * 
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityExportService;
import com.aawhere.activity.ActivityService;
import com.aawhere.activity.ExampleTracksImportServiceTestUtil;
import com.aawhere.activity.timing.ActivityTimingRelationServiceTestUtil;
import com.aawhere.cache.CacheService;
import com.aawhere.cache.CacheTestUtil;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.SimpleDocumentProducer;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.document.DocumentServiceTestUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.route.RouteServiceTestUtil.Builder;
import com.aawhere.test.TestUtil;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.xml.bind.JAXBTestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class RouteExportServiceTest {

	RouteServiceTestUtil routeTestUtil;
	RouteExportService exportService;
	RouteService routeService;
	LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	ExampleTracksImportServiceTestUtil examples;
	CacheService cacheService;

	@Before
	public void setUpServices() {
		helper.setUp();
		final Builder utilBuilder = RouteServiceTestUtil.create();
		utilBuilder.examplesPrepared();
		routeTestUtil = utilBuilder.build();
		this.examples = this.routeTestUtil.exampleTracksUtil();
		routeService = routeTestUtil.getRouteService();
		final ActivityTimingRelationServiceTestUtil timingRelationServiceTestUtil = routeTestUtil
				.getTimingRelationServiceTestUtil();
		final ActivityService activityService = timingRelationServiceTestUtil.getImportTestUtil()
				.getActivityServiceTestUtil().getActivityService();
		// FIXME: This is begging for a test util.
		ActivityExportService activityExportService = new ActivityExportService(activityService,
				timingRelationServiceTestUtil.getImportTestUtil().getActivityServiceTestUtil().getTrackTestUtil()
						.getTrackService());
		this.cacheService = CacheTestUtil.cacheService();
		final DocumentServiceTestUtil documentTestUtil = routeTestUtil
				.getImportTestUtil().getActivityServiceTestUtil().getTrackTestUtil().getDocumentTestUtil();
		documentTestUtil.getDocumentFactory().register(new SimpleDocumentProducer(DocumentFormatKey.MAP_THUMBNAIL_PNG));
		this.exportService = new RouteExportService(routeService, activityService,
				timingRelationServiceTestUtil.getLeaderboardService(), activityExportService, documentTestUtil
						.getDocumentService(), cacheService);

	}

	@After
	public void tearDownService() {
		helper.tearDown();
	}


	@Test
	public void testGpolylinesFromRoutes() {

		assertNull(this.cacheService.get("junk"));
		Activity line = this.examples.line();
		Route lineRoute = this.routeTestUtil.createPersistedRoute(line);
		assertEquals("the util is passing back junk", lineRoute.courseId(), line.id());
		final HashSet<Route> lineRouteOnly = Sets.newHashSet(lineRoute);
		Map<Route, RoutePoints> gpolylinesForRoutes = this.exportService.gpolylinesForRoutes(lineRouteOnly);
		TestUtil.assertSize(1, gpolylinesForRoutes.values());
		RoutePoints points = Iterables.getFirst(gpolylinesForRoutes.values(), null);
		assertEquals("line id", points.routeId, lineRoute.id());
		assertNotNull("golyline", points.points);
		assertEquals(	"polyline isnt' consistent",
						points.points,
						Iterables.getFirst(this.exportService.gpolylinesForRoutes(lineRouteOnly).values(), null).points);
		List<ArchivedDocument> all = documentTestUtil().findAll();
		TestUtil.assertSize("one for the track, two for the polyline", 2, all);

		Activity beforeAndAfter = this.examples.beforeAndAfter();
		TestUtil.assertSize(3, documentTestUtil().findAll());
		Route beforeAndAfterRoute = this.routeTestUtil.createPersistedRoute(beforeAndAfter);
		final HashSet<Route> bothRoutes = Sets.newHashSet(lineRoute, beforeAndAfterRoute);
		Map<Route, RoutePoints> both = this.exportService.gpolylinesForRoutes(bothRoutes);
		TestUtil.assertSize("both route points should be provided", 2, both.values());

		// these come from the cache so it isn't a complete test
		Collection<RouteId> bothIds = Collections2.transform(bothRoutes, RouteUtil.routeIdFunction());
		Map<RouteId, RoutePoints> gpolylinesForRouteIds = this.exportService.gpolylinesforRouteIds(bothIds);
		TestUtil.assertContainsAll(IdentifierUtils.ids(both.keySet()), gpolylinesForRouteIds.keySet());
		TestUtil.assertContainsAll(both.values(), gpolylinesForRouteIds.values());
	}

	/**
	 * @return
	 */
	private DocumentServiceTestUtil documentTestUtil() {
		return this.routeTestUtil.getTimingRelationServiceTestUtil().getImportTestUtil().getActivityServiceTestUtil()
				.getTrackTestUtil().getDocumentTestUtil();
	}

	@Test
	public void testGpolylinesXml() {
		RoutePointsGpolylineXmlAdapter adapter = new RoutePointsGpolylineXmlAdapter(this.exportService);
		Activity line = this.examples.line();
		Route route = this.routeTestUtil.createPersistedRoute(line);
		Routes routes = Routes.create().add(route).build();
		JAXBTestUtil<Routes> xmlUtil = JAXBTestUtil.create(routes).adapter(RouteTestUtils.adapters())
				.adapter(adapter, RoutePointsXmlAdapter.class).build();
		xmlUtil.assertContains(route.id().toString());
		xmlUtil.assertContains(GeoWebApiUri.POLYLINE_ENCODED.toString());
		TestUtil.assertSize("one track doc, one gpolyline", 2, documentTestUtil().findAll());
	}
}
