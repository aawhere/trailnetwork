/**
 * 
 */
package com.aawhere.route;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.Activity;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.io.IoException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.test.category.WebClientTest;

/**
 * Anything that should be in the {@link RouteExportServiceTest}, but has to hit a web client. The
 * {@link WebClientTest} category annotation only works on classes, not methods so this is separated
 * and the help from {@link RouteExportServiceTest} is provided by delegation.
 * 
 * @author aroller
 * 
 */
@Category(WebClientTest.class)
public class RouteExportServiceWebClientTest {

	RouteExportServiceTest serviceTest;

	@Before
	public void setUpServices() {
		this.serviceTest = new RouteExportServiceTest();
		this.serviceTest.setUpServices();
	}

	@After
	public void tearDown() {
		this.serviceTest.tearDownService();
	}

	@Test
	public void testStaticMaps() throws EntityNotFoundException, IoException {
		Activity line = this.serviceTest.routeTestUtil.exampleTracksUtil().line();
		Route route = this.serviceTest.routeTestUtil.createPersistedRoute(line);
		Document mapThumbnail = this.serviceTest.exportService.mapThumbnail(route.id());
		assertNotNull(mapThumbnail);
		// now prove that datstore caching is happening.
		ArchivedDocument archivedDocument = this.serviceTest.exportService.documentService
				.getArchivedDocument(RouteUtil.applicationDataId(route.id()), DocumentFormatKey.MAP_THUMBNAIL_PNG);
		assertNotNull(archivedDocument);
		assertNotNull(archivedDocument.getDocument());
		assertNotNull(archivedDocument.getDocument().toByteArray());
		// now prove that retrieving will find the cache.
		Document mapThumbnail2 = this.serviceTest.exportService.mapThumbnail(route.id());
		assertNotNull(mapThumbnail2.toByteArray());
		assertEquals(DocumentFormatKey.MAP_THUMBNAIL_PNG, mapThumbnail2.getDocumentFormat());

	}

}
