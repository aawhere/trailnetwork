/**
 * 
 */
package com.aawhere.trailhead;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.KmlType;

import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.WmsBoundingBoxFormat;
import com.aawhere.persist.Filter;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Converts {@link Trailhead} data into portable formats for external use through web services.
 * 
 * @author aroller
 * 
 */
@Singleton
public class TrailheadExportService {

	private TrailheadService service;

	/**
	 * 
	 */
	@Inject
	public TrailheadExportService(TrailheadService service) {
		this.service = service;
	}

	/**
	 * Provides the trailheads found within the given bounds.
	 * 
	 * @param bounds
	 * @return
	 */
	private JAXBElement<KmlType> trailheads(Filter filter, BoundingBox bounds) {
		Trailheads trailheads = service.trailheads(filter, bounds);
		return trailheads(trailheads);
	}

	/** Provides all trailheads matching the given filter. */
	public JAXBElement<KmlType> trailheads(Filter filter, @Nullable String bbox) {
		if (bbox != null) {
			BoundingBox bounds = WmsBoundingBoxFormat.create().setString(bbox).build().getBoundingBox();
			return trailheads(filter, bounds);
		} else {
			return trailheads(service.trailheads(filter));
		}
	}

	/**
	 * @param trailheads
	 * @return
	 */
	private JAXBElement<KmlType> trailheads(Trailheads trailheads) {
		return KmlDocument.create().setMain(TrailheadsKml.create(trailheads).build().get()).build().element();
	}

}
