/**
 * 
 */
package com.aawhere.route;

import java.util.Collection;

import com.aawhere.route.Route.FIELD.OPTION;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Transforms the {@link Routes} into {@link RoutePoints}.
 * 
 * @see OPTION#TRACK_GPOLYLINE
 * @author aroller
 * 
 */
@Singleton
public class RoutePointsGpolylineXmlAdapter
		extends RoutePointsXmlAdapter {

	@Inject
	private RouteExportService exportService;

	/**
	 * @param exportService2
	 */
	public RoutePointsGpolylineXmlAdapter(RouteExportService exportService) {
		this.exportService = exportService;
	}

	RoutePointsGpolylineXmlAdapter() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RoutePointsXmlAdapter#marshal(java.util.Collection)
	 */
	@Override
	public Tracks marshal(Collection<Route> routes) throws Exception {
		return new Tracks(RouteWebApiUri.POLYLINE_ENCODED.toString(), this.exportService.gpolylinesForRoutes(routes)
				.values());
	}

}
