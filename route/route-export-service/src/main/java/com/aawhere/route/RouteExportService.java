/**
 *
 */
package com.aawhere.route;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import net.opengis.kml.v_2_2_0.MultiGeometryType;

import org.apache.commons.configuration.Configuration;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityExportService;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityService;
import com.aawhere.activity.export.kml.ActivityKmlUtil;
import com.aawhere.cache.CacheService;
import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.geo.map.StaticMapSvg;
import com.aawhere.identity.IdentityManager;
import com.aawhere.lang.If;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.track.TrackInfo;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.story.DataTableTrackUserStory;
import com.aawhere.track.story.TrackUserStories;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.web.api.geo.GeoWebApiUri;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.BiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Maps.EntryTransformer;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.visualization.datasource.datatable.DataTable;

/**
 * Translates {@link Route} and friends produced from {@link RouteService} into export formats like
 * {@link KmlDocument}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class RouteExportService
		implements RouteTrackKmlLineProvider {

	/**
	 *
	 */
	private static final int MAX_TRAILHEADS = 20;
	private static final Function<RouteId, String> GPOLYLINE_CACHE_KEY_FUNCTION = gpolylineRouteIdCacheKeyFunction();
	final RouteService routeService;
	final ActivityService activityService;
	final ActivityExportService activityExportService;
	final CacheService cacheService;
	final DocumentService documentService;

	/**
	 *
	 */
	@Inject
	public RouteExportService(RouteService routeService, ActivityService activityService,
			ActivityExportService activityExportService, DocumentService documentService, CacheService cacheService) {
		this.routeService = routeService;
		this.activityService = activityService;
		this.activityExportService = activityExportService;
		this.cacheService = cacheService;
		this.documentService = documentService;
	}

	public String mapSvg(RouteId routeId) throws EntityNotFoundException {
		TrackInfo trackInfo = routeService.trackInfo(routeId);
		return StaticMapSvg.create().trackInfo(trackInfo).build().svg();

	}

	/**
	 * @param trailheadId
	 * @return
	 */
	public KmlDocument routesFromStartKml(TrailheadId trailheadId) {

		Routes routes = routeService.routes(trailheadId);
		List<ActivityId> courseIds = Lists.transform(routes.getCollection(), RouteToCourseIdFunction.build());
		// not necessarily the best way for routes, but it's a good start.
		Activities activities = activityService.getActivities(courseIds);
		return ActivityKmlUtil.activitiesKml(activities);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteTrackKmlLineProvider#line(com.aawhere.route.Route)
	 */
	@Override
	public MultiGeometryType line(Route route) throws EntityNotFoundException {
		return RouteTrackKmlLineProcessorProvider.create().provider(routeService).build().line(route);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.route.RouteTrackProvider#track(com.aawhere.route.Route)
	 */
	@Override
	@Nonnull
	public Iterable<Trackpoint> track(@Nonnull Route route) throws EntityNotFoundException {
		return routeService.track(route);
	}

	/**
	 * Identify the route of interest and this will give the encoded polyline for the activity.
	 * 
	 * @see ActivityExportService#gpolyline(ActivityId)
	 * 
	 * @param routeId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public String gpolyline(RouteId routeId) throws EntityNotFoundException {
		return cacheService.get(routeId, GPOLYLINE_CACHE_KEY_FUNCTION, gpolylinePointsFunction()).points;
	}

	public Map<RouteId, RoutePoints> gpolylinesforRouteIds(Collection<RouteId> ids) {
		return cacheService.getAll(ids, GPOLYLINE_CACHE_KEY_FUNCTION, gpolylinePointsMapFunction());
	}

	public Map<Route, RoutePoints> gpolylinesForRoutes(Collection<Route> routes) {
		return cacheService.getAll(routes, gpolylineRouteCacheKeyFunction(), gpolylineRoutePointsMapFunction());
	}

	private static Function<Route, String> gpolylineRouteCacheKeyFunction() {
		return Functions.compose(gpolylineRouteIdCacheKeyFunction(), RouteUtil.routeIdFunction());
	}

	private static Function<RouteId, String> gpolylineRouteIdCacheKeyFunction() {
		return new Function<RouteId, String>() {
			@Override
			public String apply(RouteId input) {
				return new StringBuilder(input.getQualifiedString()).append(GeoWebApiUri.GPOLYLINE).toString();
			}
		};
	}

	/**
	 * Provided the route id this will do the work to find the documents and get the polylines
	 * cached as a route. Working with bulk this will request the course ids from route service,
	 * then provide those to the activity export service for either pulling from the file cache or
	 * creating and caching for a subsequent call. The resulting map in a healthy system should
	 * return equal amount as requested, but will ignore missing activities or others that can't be
	 * created.
	 * 
	 * @return
	 */
	private Function<Iterable<RouteId>, Map<RouteId, RoutePoints>> gpolylinePointsMapFunction() {
		return new Function<Iterable<RouteId>, Map<RouteId, RoutePoints>>() {

			@Override
			public Map<RouteId, RoutePoints> apply(Iterable<RouteId> routeIds) {
				if (routeIds == null) {
					return null;
				}
				BiMap<RouteId, ActivityId> courseIdsForRoute = routeService.courseIds(routeIds);
				Map<ActivityId, String> gpolylines;
				gpolylines = activityExportService.gpolylines(courseIdsForRoute.values());
				Map<RouteId, String> routePolylines = CollectionUtilsExtended.swapKeys(courseIdsForRoute, gpolylines);
				return Maps.transformEntries(routePolylines, pointsEntryTransformer());
			}
		};
	}

	/**
	 * The result of polyline creation is a string, but the cache wants the full RoutePoints object
	 * so it is self-describing.
	 */
	private Maps.EntryTransformer<RouteId, String, RoutePoints> pointsEntryTransformer() {
		return new EntryTransformer<RouteId, String, RoutePoints>() {

			@Override
			public RoutePoints transformEntry(RouteId key, String value) {
				return new RoutePoints(key, value);
			}
		};
	}

	/**
	 * Specialty function that handles the happy, but likley case, where the Routes are requesting
	 * their polylines. The routes already identify their courses so no need to go to the service
	 * and make a query. The call to the {@link ActivityExportService#gpolylines(Iterable)} is made
	 * and the resulting polylines returned mapped to the Routes.
	 * 
	 * @return
	 */
	private Function<Iterable<Route>, Map<Route, RoutePoints>> gpolylineRoutePointsMapFunction() {
		return new Function<Iterable<Route>, Map<Route, RoutePoints>>() {

			@Override
			public Map<Route, RoutePoints> apply(Iterable<Route> routes) {
				if (routes == null) {
					return null;
				}
				BiMap<Route, ActivityId> courseIds = RouteUtil.courseIdsRouteMap(routes);
				Map<ActivityId, String> gpolylines = activityExportService.gpolylines(courseIds.values());
				// maps the route to the polyline
				Map<Route, String> routePolylines = CollectionUtilsExtended.swapKeys(courseIds, gpolylines);
				return Maps.transformEntries(routePolylines, new EntryTransformer<Route, String, RoutePoints>() {
					@Override
					public RoutePoints transformEntry(Route route, String gpoyline) {
						return new RoutePoints(route.id(), gpoyline);
					}
				});
			}
		};
	}

	private Function<RouteId, RoutePoints> gpolylinePointsFunction() {
		return new Function<RouteId, RoutePoints>() {

			@Override
			public RoutePoints apply(RouteId routeId) {
				try {
					return (routeId == null) ? null : new RoutePoints(routeId,
							activityExportService.gpolyline(routeService.route(routeId).courseId()));
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/**
	 * @param routeId
	 * @param locale
	 * @param numberOfTrackpoints
	 * @param preferences
	 * @return
	 * @throws EntityNotFoundException
	 */
	// TODO: pass IdentityManager to this method instead of Locale.
	public DataTable dataTable(@Nonnull RouteId routeId, IdentityManager identityManager,
			@Nullable Integer numberOfTrackpoints) throws EntityNotFoundException {
		Route route = this.routeService.route(routeId);
		Activity course = this.routeService.course(route);
		// final Configuration preferences = identityManagerProvider.get().preferences();
		final Configuration preferences = identityManager.preferences();
		numberOfTrackpoints = If.nil(numberOfTrackpoints).use(1000);
		Iterable<Trackpoint> track = this.routeService.track(route);
		TrackUserStories.Builder storiesBuilder = new TrackUserStories.Builder(track).addStandardFilters();

		DataTableTrackUserStory story = storiesBuilder.setUserStory(new DataTableTrackUserStory.Builder()
				.setMaxNumberOfTrackpoints(numberOfTrackpoints).setPreferences(preferences)
				.setSummary(course.getTrackSummary()).setSeekerChainBuilder(storiesBuilder.getSeekerChainBuilder()));
		storiesBuilder.build();
		return story.getResult();
	}

}
