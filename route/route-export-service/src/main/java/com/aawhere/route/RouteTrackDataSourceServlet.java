/**
 *
 */
package com.aawhere.route;

import static com.aawhere.route.RouteWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.servlet.http.HttpServletRequest;

import com.aawhere.identity.AccessScopeValidatorFactory;
import com.aawhere.identity.IdentityFactory;
import com.aawhere.identity.IdentityManagerProvider;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.number.NumberUtilsExtended;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.PersonService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.visualization.datasource.DataSourceServlet;
import com.google.visualization.datasource.datatable.DataTable;
import com.google.visualization.datasource.query.Query;

/**
 * Servlet that provides tracks as a Google Data Source.
 * 
 * @{link <a
 *        href="http://code.google.com/apis/chart/interactive/docs/dev/dsl_get_started.html">http:
 *        //code.google.com/apis/chart/interactive/docs/dev/dsl_get_started.html</a>
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class RouteTrackDataSourceServlet
		extends DataSourceServlet {

	private static final long serialVersionUID = -729518934515373842L;
	private RouteExportService routeService;
	private PersonService personService;
	private IdentityFactory identityFactory;
	private AccessScopeValidatorFactory accessScopeValidatorFactory;

	public static final String PATH = SLASH + DATASOURCE_HTTP_PATH + SLASH + ROUTES;

	@Inject
	public RouteTrackDataSourceServlet(RouteExportService service, PersonService personService,
			IdentityFactory identityFactory, AccessScopeValidatorFactory accessScopeValidatorFactory) {
		this.routeService = service;
		this.personService = personService;
		this.identityFactory = identityFactory;
		this.accessScopeValidatorFactory = accessScopeValidatorFactory;
	}

	@Override
	public DataTable generateDataTable(Query query, HttpServletRequest request) {
		Long routeIdValue = new Long(request.getParameter(RouteWebApiUri.ROUTE_ID));

		Integer numberOfTrackpoints = NumberUtilsExtended.parseInteger(request.getParameter("maxNumberOfTrackpoints"));
		RouteId routeId = new RouteId(routeIdValue);
		try {
			// FIXME:why is the identity manager provider here? why isn't identity manager just
			// injected? This is outside Guice?
			IdentityManagerProvider identityManagerProvider = new IdentityManagerProvider(request, personService,
					identityFactory, accessScopeValidatorFactory);
			return routeService.dataTable(routeId, identityManagerProvider.get(), numberOfTrackpoints);
		} catch (EntityNotFoundException e) {
			throw ToRuntimeException.wrapAndThrow(e);
		}
	}

	@Override
	protected boolean isRestrictedAccessMode() {
		return false;
	}
}