package com.aawhere.all.process;

import java.util.Date;

import com.aawhere.lang.ObjectBuilder;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;

/**
 * Just a passthrough {@link MapOnlyMapper} used to load and save an {@link Entity}, commonly needed
 * to update indexes.
 * 
 * @see PassThroughMapper
 * 
 * @author aroller
 * 
 */
public class EntityTouchMapper
		extends MapOnlyMapper<Entity, Entity> {

	private String dateUpdatedProperty;

	/**
	 * Used to construct all instances of TouchMapper.
	 */
	public static class Builder
			extends ObjectBuilder<EntityTouchMapper> {

		public Builder() {
			super(new EntityTouchMapper());
		}

		/** @see #dateUpdatedProperty */
		public Builder dateUpdatedProperty(String dateUpdatedProperty) {
			building.dateUpdatedProperty = dateUpdatedProperty;
			return this;
		}

		@Override
		public EntityTouchMapper build() {
			EntityTouchMapper built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TouchMapper */
	private EntityTouchMapper() {
	}

	private static final long serialVersionUID = -8032138449965967860L;

	@Override
	public void map(Entity value) {
		if (dateUpdatedProperty != null) {
			value.setProperty(dateUpdatedProperty, new Date());
		}
		emit(value);
	}

}
