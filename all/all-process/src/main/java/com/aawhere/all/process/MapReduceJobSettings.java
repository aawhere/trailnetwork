/**
 * 
 */
package com.aawhere.all.process;

import java.io.Serializable;

import javax.annotation.Nullable;
import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.lang.ObjectBuilder;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.tools.mapreduce.MapReduceSettings;
import com.google.appengine.tools.mapreduce.MapSettings;
import com.google.appengine.tools.pipeline.JobSetting;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Standard configuration for {@link MapReduceSettings} allowing customization, but handling those
 * items that are common across all processsing.
 * 
 * FIXME: MapReduceSettings does not support a version for a module. See {@link MapSettings} v0.8.1
 * Line 253.
 * 
 * <pre>
 *   // TODO(user): we may want to support providing a version for a module
 *      final String requestedModule = module;
 *      version = runWithRetries(new Callable<String>() {
 *      @Override public String call() {
 *      return modulesService.getDefaultVersion(requestedModule);
 *    }
 * </pre>
 * 
 * 
 * 
 * @author aroller
 * 
 */
public class MapReduceJobSettings
		implements Serializable {

	public static final int REDUCER_COUNT_DEFAULT = 20;
	public static final int INPUT_SHARD_COUNT_DEFAULT = 20;

	private static final long serialVersionUID = 6341023665920830854L;
	private MapReduceSettings settings;
	private String queueName;
	private String bucketName;
	private Options options;

	/**
	 * Used to construct all instances of AllMapReduceJobSettings.
	 */
	public static class Builder
			extends ObjectBuilder<MapReduceJobSettings> {

		public Builder() {
			super(new MapReduceJobSettings());
		}

		/** @see #options */
		public Builder options(@Nullable Options options) {
			building.options = options;
			return this;
		}

		@Override
		public MapReduceJobSettings build() {
			MapReduceJobSettings built = super.build();
			MapReduceSettings.Builder builder = new MapReduceSettings.Builder();
			built.queueName = "jobs";
			built.bucketName = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName();
			built.settings = builder.build();
			if (built.options == null) {
				built.options = new Options();
			}
			return built;
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct AllMapReduceJobSettings */
	private MapReduceJobSettings() {
	}

	public String bucketName() {
		return bucketName;
	}

	/** @see #options */
	public Options options() {
		return this.options;
	}

	/**
	 * @return the settings
	 */
	public MapReduceSettings settings() {
		return this.settings;
	}

	public JobSetting[] jobSettings() {
		return new JobSetting[] { new JobSetting.OnQueue(queueName) };
	}

	/**
	 * Appends the given settings to those provided from {@link #jobSettings()}
	 * 
	 * @param jobSettings
	 * @return
	 */
	public JobSetting[] jobSettings(JobSetting... jobSettings) {
		// module name does not work on the development server
		return ArrayUtils.addAll(jobSettings(), jobSettings);
	}

	/**
	 * Having options separate allows for web injectionables.
	 * 
	 * @author aroller
	 * 
	 */
	@ApiModel(description = "Custom settings for controlling Map Reduce processes.")
	public static class Options
			implements Serializable {

		@ApiModelProperty(value = "The number of reducers (and outputs) shards")
		@QueryParam("reducerCount")
		private Integer reducerCount = REDUCER_COUNT_DEFAULT;
		@ApiModelProperty(value = "The number of input (map) shards")
		@QueryParam("inputCount")
		private Integer inputCount = INPUT_SHARD_COUNT_DEFAULT;

		private static final long serialVersionUID = 4111540313944944218L;

		public Integer inputCount() {
			return inputCount;
		}

		public Integer reducerCount() {
			return reducerCount;
		}
	}
}
