/**
 * 
 */
package com.aawhere.all.process;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * @author aroller
 * 
 */
public class PipelineWebApiUri
		extends SystemWebApiUri {

	/**
	 * Provides the URI that will reference the pipeline status for the given job id.
	 * 
	 * @param jobId
	 * @return
	 */
	public static URI pipelineStatus(String jobId) {
		return UriBuilder.fromUri(API_PUBLIC_HOST_DEFAULT_PATH).path("_ah/pipeline/status").queryParam("root", jobId)
				.build();
	}

}
