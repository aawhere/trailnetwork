package com.aawhere.all.process;

import java.io.Serializable;

import javax.annotation.Nonnull;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;
import com.google.appengine.tools.mapreduce.mappers.IdentityMapper;
import com.google.appengine.tools.mapreduce.mappers.KeyProjectionMapper;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

/**
 * Emits exactly what it receives and does nothing. Useful for touching entities.
 * 
 * @see IdentityMapper
 * @see KeyProjectionMapper
 * @author aroller
 * 
 * @param <E>
 */
public class PassThroughMapper<E>
		extends MapOnlyMapper<E, E> {

	private static final long serialVersionUID = -8452102682037602415L;

	/**
	 * Selectively chooses which entities to emit. The default is to always return true.
	 * 
	 * Note:This must be serializable.
	 */
	@Nonnull
	private Predicate<E> filter;

	/**
	 * Used to construct all instances of PassThroughMapper.
	 */
	public static class Builder<E>
			extends ObjectBuilder<PassThroughMapper<E>> {

		public Builder() {
			super(new PassThroughMapper<E>());
		}

		/** @see #filter */
		public Builder<E> filter(Predicate<E> filter) {
			building.filter = filter;
			return this;
		}

		@Override
		public PassThroughMapper<E> build() {
			PassThroughMapper<E> built = super.build();
			if (built.filter == null) {
				built.filter = Predicates.alwaysTrue();
			}
			Assertion.exceptions().assertInstanceOf("filter", built.filter, Serializable.class);
			return built;
		}

	}// end Builder

	public static <E> Builder<E> create() {
		return new Builder<E>();
	}

	/** Use {@link Builder} to construct PassThroughMapper */
	private PassThroughMapper() {
	}

	@Override
	public void map(E value) {

		if (filter.apply(value)) {
			emit(value);
		}

	}

}
