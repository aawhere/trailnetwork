package com.aawhere.all.process;

import com.aawhere.person.PersonAccountProvider;
import com.aawhere.person.PersonProvider;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Provides static access to all Singletons used by processors.
 * 
 * This behaves differently than most Guice singletons since our services that implement the
 * providers are not serializable...and they must be in order to be injected into the {@link Mapper}
 * and {@link Reducer} classes. A {@link #singleton} is provided at static level to be injected by
 * Guice
 * 
 * @author aroller
 * 
 */
@Singleton
public class AllProcessProviders {

	private static AllProcessProviders singleton;

	public final PersonProvider personProvider;
	public final PersonAccountProvider personAccountProvider;

	@Inject
	public AllProcessProviders(PersonProvider personProvider, PersonAccountProvider personAccountProvider) {
		this.personProvider = personProvider;
		this.personAccountProvider = personAccountProvider;
	}

	/**
	 * Provides Guice or test injection of the single instance of this that will be served to all
	 * via {@link #singleton()}.
	 * 
	 * @param incoming
	 */
	@Inject
	public static final AllProcessProviders singleton(AllProcessProviders incoming) {
		if (singleton == null) {
			singleton = incoming;
		}
		return singleton;
	}

	/**
	 * Provides the only instance of this.
	 * 
	 * @return
	 */
	public static final AllProcessProviders singleton() {
		return singleton;
	}

}
