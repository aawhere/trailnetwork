/**
 * 
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.Activity;
import com.aawhere.app.account.Account;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.lang.Assertion;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.xml.XmlNamespace;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.wordnik.swagger.annotations.ApiModel;

/**
 * Summary Statistics for each {@link Application}. Coupled with many entities in the system such as
 * {@link Account}, {@link Activity}, Route, or other entities that may provide valuable insight to
 * the contributions of a particular {@link Application}.
 * 
 * @author aroller
 * 
 */
@Cache
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Dictionary(domain = "ApplicationSummary")
@ApiModel(description = "Summary Statstics for an Application.")
public class ApplicationSummary
		extends StringBaseEntity<ApplicationSummary, ApplicationSummaryId>
		implements SelfIdentifyingEntity {

	private static final long serialVersionUID = 8886537333820048854L;

	@XmlAttribute
	@Field
	private Integer accountCount;
	@XmlAttribute
	@Field
	private Integer activityCount;

	/**
	 * Used to construct all instances of ApplicationSummary.
	 */
	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<ApplicationSummary, Builder, ApplicationSummaryId> {

		public Builder() {
			super(new ApplicationSummary());
		}

		/**
		 * @param mutant
		 */
		Builder(ApplicationSummary mutant) {
			super(mutant);
		}

		public Builder accountCount(Integer accountCount) {
			building.accountCount = accountCount;
			return this;
		}

		public Builder activityCount(Integer activityCount) {
			building.activityCount = activityCount;
			return this;
		}

		public Builder application(ApplicationKey application) {
			setId(new ApplicationSummaryId(application));
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.exceptions().assertTrue("applicationKey must provide the ID", hasId());
			super.validate();

		}

		@Override
		public ApplicationSummary build() {
			ApplicationSummary built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ApplicationSummary */
	private ApplicationSummary() {
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the accountCount
	 */
	public Integer accountCount() {
		return this.accountCount;
	}

	/**
	 * @return the activityCount
	 */
	public Integer activityCount() {
		return this.activityCount;
	}

	/*
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@Override
	@Field
	public ApplicationSummaryId getId() {
		return super.getId();
	}
}
