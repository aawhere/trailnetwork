/**
 * 
 */
package com.aawhere.app;

import com.aawhere.id.StringIdentifier;

/**
 * @author aroller
 * 
 */
public class ApplicationSummaryId
		extends StringIdentifier<ApplicationSummary> {

	private static final long serialVersionUID = 1661743172076137132L;

	/**
	 * 
	 */
	public ApplicationSummaryId() {
		super(ApplicationSummary.class);
	}

	/**
	 * @param value
	 */
	public ApplicationSummaryId(String value) {
		super(value, ApplicationSummary.class);

	}

	/**
	 * @param applicationKey
	 */
	public ApplicationSummaryId(ApplicationKey applicationKey) {
		this(applicationKey.getValue());
	}

	public ApplicationKey getApplicationKey() {
		return new ApplicationKey(getValue());
	}

}
