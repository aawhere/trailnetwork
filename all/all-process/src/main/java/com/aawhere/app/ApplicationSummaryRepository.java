/**
 * 
 */
package com.aawhere.app;

import com.aawhere.persist.RepositoryUpdateable;

/**
 * @author aroller
 * 
 */
public interface ApplicationSummaryRepository
		extends RepositoryUpdateable<ApplicationSummary, ApplicationSummaryId, String, ApplicationSummary.Builder> {

}
