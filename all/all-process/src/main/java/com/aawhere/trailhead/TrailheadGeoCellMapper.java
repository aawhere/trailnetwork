package com.aawhere.trailhead;

import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.Resolution;
import com.google.appengine.tools.mapreduce.Mapper;

public class TrailheadGeoCellMapper
		extends Mapper<Trailhead, GeoCell, TrailheadId> {

	private static final Resolution resolution = Resolution.NINE;

	@Override
	public void map(Trailhead value) {
		emit(GeoCellUtil.geoCell(value.location(), resolution), value.id());
	}

	private static final long serialVersionUID = -3973367497106569129L;
}
