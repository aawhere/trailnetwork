/**
 * 
 */
package com.aawhere.geo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.BaseEntities;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class GeoCellSummaries
		extends BaseEntities<GeoCellSummary> {

	/**
	 * Used to construct all instances of GeoCellSummaries.
	 */
	@XmlTransient
	public static class Builder
			extends BaseEntities.Builder<Builder, GeoCellSummary, GeoCellSummaries> {

		public Builder() {
			super(new GeoCellSummaries());
		}

		@Override
		public GeoCellSummaries build() {
			GeoCellSummaries built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct GeoCellSummaries */
	private GeoCellSummaries() {
	}

	/*
	 * @see com.aawhere.persist.BaseEntities#getCollection()
	 */
	@Override
	@XmlElement(name = GeoCellSummaryField.DOMAIN)
	public List<GeoCellSummary> getCollection() {
		return super.getCollection();
	}

}
