/**
 *
 */
package com.aawhere.geo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.configuration.Configuration;

import com.aawhere.field.FieldValueProvider;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.identity.IdentityManager;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.xml.CountFromIntegerXmlAdapter;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.personalize.QuantityPersonalized;
import com.aawhere.xml.XmlNamespace;
import com.aawhere.xml.bind.OptionalFunctionXmlAdapter;

import com.google.common.base.Functions;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.ApiModel;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = "geoCellSummary")
@ApiModel("Statistics related to area.")
public class GeoCellSummary
		extends StringBaseEntity<GeoCellSummary, GeoCellSummaryId>
		implements SelfIdentifyingEntity {

	private static final long serialVersionUID = -4867573097221978052L;

	/**
	 * Used to construct all instances of GeoCellSummary.
	 */
	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<GeoCellSummary, Builder, GeoCellSummaryId> {

		public Builder() {
			super(new GeoCellSummary());
		}

		public Builder geoCell(GeoCell geoCell) {
			setId(new GeoCellSummaryId(geoCell));
			return this;
		}

		@Override
		public GeoCellSummary build() {
			GeoCellSummary built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct GeoCellSummary */
	private GeoCellSummary() {
	}

	@Field(xmlAdapter = GeoCellSummary.ActivityCountXmlAdapter.class)
	private Integer activityStartCount() {
		throw new UnsupportedOperationException("FIXME:Allow virtual field declarations in Dictionary annotation");
	}

	@XmlElement(name = GeoCellSummaryField.ACTIVITY_START_COUNT)
	@XmlJavaTypeAdapter(GeoCellSummary.ActivityCountXmlAdapter.class)
	private GeoCell getActivityCountXml() {
		return geoCell();
	}

	public GeoCell geoCell() {

		return id().geCell();

	}

	@Singleton
	public static class ActivityCountXmlAdapter
			extends OptionalFunctionXmlAdapter<QuantityPersonalized, GeoCell> {

		public ActivityCountXmlAdapter() {
		}

		@Inject
		public ActivityCountXmlAdapter(FieldValueProvider fieldValueProvider, IdentityManager identityManager) {
			// see Application.AccountCountXmlAdapter
			super(Functions.compose(new CountFromIntegerXmlAdapter(identityManager).function(),
									GeoCellSummaryUtil.activityStartCountProviderFunction(fieldValueProvider)),
					Visibility.SHOW);
		}

	}
}
