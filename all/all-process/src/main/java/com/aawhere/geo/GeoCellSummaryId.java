/**
 * 
 */
package com.aawhere.geo;

import com.aawhere.id.StringIdentifier;
import com.aawhere.measure.geocell.GeoCell;

/**
 * @author aroller
 * 
 */
public class GeoCellSummaryId
		extends StringIdentifier<GeoCellSummary> {

	private static final Class<GeoCellSummary> KIND = GeoCellSummary.class;

	/**
	 * 
	 */
	public GeoCellSummaryId() {
		super(KIND);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public GeoCellSummaryId(String value) {
		super(value, KIND);
	}

	public GeoCellSummaryId(GeoCell geoCell) {
		this(geoCell.getCellAsString());
	}

	private static final long serialVersionUID = 6128479073348191595L;

	/**
	 * @return
	 */
	public GeoCell geCell() {
		return new GeoCell(getValue());
	}
}
