/**
 * 
 */
package com.aawhere.geo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.field.FieldValueProvider;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsExploder;
import com.aawhere.measure.geocell.Resolution;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * @author aroller
 * 
 */
public class GeoCellSummaryUtil {

	public static Function<GeoCell, GeoCellSummary> summaryFunction() {
		return new Function<GeoCell, GeoCellSummary>() {

			@Override
			public GeoCellSummary apply(GeoCell input) {
				return (input != null) ? GeoCellSummary.create().geoCell(input).build() : null;
			}
		};
	}

	/**
	 * @param fieldValueProvider
	 * @return
	 */
	public static Function<GeoCell, Integer> activityStartCountProviderFunction(FieldValueProvider fieldValueProvider) {
		return FieldValueProvider.Util.function(GeoCellSummaryField.Key.ACTIVITY_START_COUNT, fieldValueProvider);
	}

	public static Function<GeoCellSummary, GeoCell> geoCellFunction() {
		return new Function<GeoCellSummary, GeoCell>() {

			@Override
			public GeoCell apply(GeoCellSummary input) {
				return (input != null) ? input.geoCell() : null;
			}
		};
	}

	/**
	 * @param geoCells
	 * @return
	 */
	public static Iterable<GeoCellSummary> summariesIterable(Iterable<GeoCell> geoCells) {
		return Iterables.transform(geoCells, summaryFunction());
	}

	@Nonnull
	public static GeoCellSummaries summaries(@Nonnull GeoCells geoCells) {
		return GeoCellSummaries.create().addAll(summariesIterable(geoCells)).build();
	}

	@Nonnull
	public static GeoCellSummaries summaries(@Nonnull GeoCells geoCells, @Nullable Resolution explodeTo) {

		GeoCells exploded = (explodeTo != null) ? GeoCellsExploder.explode().these(geoCells).to(explodeTo).exploded()
				: geoCells;
		return summaries(exploded);

	}

	/**
	 * @param summaries
	 * @return
	 */
	public static Iterable<GeoCell> geoCellsIterable(Iterable<GeoCellSummary> summaries) {
		return Iterables.transform(summaries, geoCellFunction());
	}

	public static GeoCells geoCells(Iterable<GeoCellSummary> summaries) {
		return GeoCells.create().addAll(geoCellsIterable(summaries)).build();
	}

}
