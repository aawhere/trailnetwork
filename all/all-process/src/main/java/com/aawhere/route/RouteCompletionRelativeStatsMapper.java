package com.aawhere.route;

import java.util.logging.Logger;

import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatsCounterReducer;
import com.aawhere.activity.RelativeStatsUtil;
import com.aawhere.collections.PredicatesExtended;
import com.aawhere.log.LoggerFactory;
import com.aawhere.trailhead.Trailhead;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;

/**
 * Given a route completion this emits all {@link RelativeStatistic} for both the {@link Route} and
 * the {@link Trailhead}. The {@link RelativeStatistic} is the key intended for
 * {@link RelativeStatsCounterReducer} as the recipient.
 * 
 * @author aroller
 * 
 */
public class RouteCompletionRelativeStatsMapper
		extends Mapper<RouteCompletion, RelativeStatistic, RouteCompletionId> {

	private static final Logger LOG = LoggerFactory.getLogger(RouteCompletionRelativeStatsMapper.class);

	private static final long serialVersionUID = 5449843607832726472L;

	private Function<RouteCompletion, Iterable<RelativeStatistic>> relativeStatsFunction;

	public RouteCompletionRelativeStatsMapper(Function<RouteCompletion, Iterable<RelativeStatistic>> function) {
		this.relativeStatsFunction = function;

	}

	public static RouteCompletionRelativeStatsMapper routeMapper() {
		return new RouteCompletionRelativeStatsMapper(RouteCompletionUtil.routeRelativeStatsFunction());
	}

	public static RouteCompletionRelativeStatsMapper trailheadMapper() {
		Function<RouteCompletion, Iterable<RelativeStatistic>> trailheadRelativeStatsFunction = RouteCompletionUtil
				.trailheadRelativeStatsFunction();
		final Predicate<RelativeStatistic> personalOnlyPredicate = RelativeStatsUtil.personalOnlyPredicate();
		Function<Iterable<RelativeStatistic>, Iterable<RelativeStatistic>> personFilteredFunction = PredicatesExtended
				.resultFilteringFunction(personalOnlyPredicate);
		Function<RouteCompletion, Iterable<RelativeStatistic>> personalStatsFunction = Functions
				.compose(personFilteredFunction, trailheadRelativeStatsFunction);
		return new RouteCompletionRelativeStatsMapper(personalStatsFunction);
	}

	@Override
	public void map(RouteCompletion routeCompletion) {
		if (routeCompletion.activityType() != null) {
			if (routeCompletion.routeDistance() != null) {
				for (RelativeStatistic relativeStats : relativeStatsFunction.apply(routeCompletion)) {
					emit(relativeStats, routeCompletion.id());
				}
			} else {

				LOG.warning(routeCompletion.id() + " ignored because it has no distance");

			}
		} else {
			LOG.warning(routeCompletion.id() + " ignored because it has no activity type");
		}

	}

}
