package com.aawhere.activity;

import java.util.Map.Entry;

import javax.measure.quantity.Length;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.person.PersonId;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.common.collect.ImmutableList;

/**
 * Extracts the ActivityId, {@link ActivityType} and start {@link GeoCoordinate} from Activity along
 * with the PersonId to provide {@link RelativeStatistic} with the corresponding for the the account
 * and paired with the start representing the start. For each activity given this produces a pair
 * for every {@link RelativeStatistic.Layer} so there are more calls to {@link #emit(Pair)}
 * 
 * @author aroller
 * 
 */
public class ActivityStartRelativeStatisticMapper
		extends Mapper<Entry<Activity, PersonId>, GeoCell, RelativeStatistic> {

	private static final long serialVersionUID = 3387507376619558556L;

	/**
	 * Converts those activity types that we always switch over to another.
	 * 
	 */

	@Override
	public void map(Entry<Activity, PersonId> activityPerson) {

		Activity activity = activityPerson.getKey();
		ActivityType activityType = ActivityUtil.activityTypeStatsTransformers().apply(activity.getActivityType());
		Length distance = activity.getTrackSummary().getDistance();
		ImmutableList<RelativeStatistic> all = RelativeStatsUtil.relativeStats(	activity.id(),
																				activityPerson.getValue(),
																				activityType,
																				distance);
		// TODO:Make resolution configurable
		GeoCell startGeoCell = ActivityUtil.startGeoCell(activity, Resolution.NINE);
		for (RelativeStatistic relativeStats : all) {
			emit(startGeoCell, relativeStats);
		}

	}

}
