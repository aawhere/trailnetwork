package com.aawhere.activity;

import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.id.Identifier;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;

/**
 * Aggregates the results of {@link RelativeStatsFeatureIdMapper} into {@link ActivityStats} by
 * grouping all of those items that are "non-personal". The total comes from {@link Layer#ALL}
 * {@link RelativeStatistic} , then the more specific details like {@link Layer#ACTIVITY_TYPE} into
 * {@link ActivityStats#activityTypeStats()}.
 * 
 * It ignores any {@link Layer#personal} relative stats if provided so it's better not to provide
 * them.
 * 
 * @author aroller
 * 
 */
public class RelativeStatsActivityStatsReducer<I extends Identifier<?, ?>>
		extends Reducer<I, RelativeStatistic, RelativeStatsActivityStatsAggregator<I>> {

	private static final long serialVersionUID = 6321568309979098525L;

	@Override
	public void reduce(I key, ReducerInput<RelativeStatistic> relativeStats) {
		RelativeStatsActivityStatsAggregator<I> aggregator = RelativeStatsActivityStatsAggregator.create(key)
				.statsIterator(relativeStats).build();

		emit(aggregator);
	}
}
