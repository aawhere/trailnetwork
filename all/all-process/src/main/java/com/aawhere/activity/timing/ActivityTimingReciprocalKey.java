package com.aawhere.activity.timing;

import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.ActivityId;
import com.aawhere.collections.MapEntryUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.StringIdentifier;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;

/**
 * Special key used during processing to show equivalence between reciprocals so that timing ids of
 * the same two activities will be considered equal regardless of their order. This is done by
 * sorting the two activity ids and just using the id with the natural order rather than providing
 * custom equality methods.
 * 
 * This allows grouping of timings so the {@link ActivityTimingRelation#reciprocal()} can be
 * answered if the two timings are part of the same map reduce.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingReciprocalKey
		extends StringIdentifier<ActivityTimingId> {

	private static final long serialVersionUID = -3188862526631697917L;

	public ActivityTimingReciprocalKey(ActivityTimingId id) {
		super(key(id), ActivityTimingId.class);
	}

	public ActivityTimingReciprocalKey(ActivityTiming timing) {
		this(timing.id());
	}

	private static String key(ActivityTimingId id) {
		Pair<ActivityId, ActivityId> pair = ActivityTimingRelationUtil.split(id);
		List<ActivityId> list = MapEntryUtil.list(pair);
		List<ActivityId> sortedCopy = Ordering.natural().sortedCopy(list);
		return IdentifierUtils.createCompositeIdValue(sortedCopy.get(0), sortedCopy.get(1));
	}

	private enum KeyFunction implements Function<ActivityTiming, ActivityTimingReciprocalKey> {
		INSTANCE;
		@Override
		@Nullable
		public ActivityTimingReciprocalKey apply(@Nullable ActivityTiming relation) {
			return (relation != null) ? new ActivityTimingReciprocalKey(relation) : null;
		}

		@Override
		public String toString() {
			return "KeyFunction";
		}
	}

	public static Function<ActivityTiming, ActivityTimingReciprocalKey> keyFunction() {
		return KeyFunction.INSTANCE;
	}
}
