package com.aawhere.activity;

import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.Iterators;

/**
 * Recipient of {@link RelativeStatsFeatureIdMapper} this receives the relative stats related to the
 * feature counting all the matches that are found to be the same. The values are conted and
 * assigned to the {@link RelativeStatistic#count()}.
 * 
 * @author aroller
 * 
 *         <I> the identifier that is the value to be counted. This does nothing with the value.
 */
public class RelativeStatsCounterReducer<I>
		extends Reducer<RelativeStatistic, I, RelativeStatistic> {

	private static final long serialVersionUID = 4214555623515828046L;

	@Override
	public void reduce(RelativeStatistic key, ReducerInput<I> ids) {
		int numberOfActivities = Iterators.size(ids);
		emit(RelativeStatistic.clone(key).count(numberOfActivities).build());
	}

}
