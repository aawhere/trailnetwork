package com.aawhere.activity;

import com.aawhere.id.Identifier;
import com.aawhere.lang.ObjectBuilder;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.common.base.Predicate;

/**
 * Simply maps the {@link RelativeStatistic} into it's
 * {@link RelativeStatistic#featureIdForDomain(String, String)} to allow reduction of stats to
 * interact with the same feature (reducing calls to load the feature).
 * 
 * This assumes all {@link RelativeStatistic} are part of the same domain.
 * 
 * @author aroller
 * 
 */
public class RelativeStatsFeatureIdMapper<I extends Identifier<?, ?>>
		extends Mapper<RelativeStatistic, I, RelativeStatistic> {

	private static final long serialVersionUID = -6554991775803356087L;

	/**
	 * The idType that the relative stats represents. Used to build the identifier.
	 * 
	 */
	private Class<I> idType;

	/** Filters out specific statistics. */
	private Predicate<RelativeStatistic> predicate;

	/**
	 * Used to construct all instances of RelativeStatsFeatureIdMapper.
	 */
	public static class Builder<I extends Identifier<?, ?>>
			extends ObjectBuilder<RelativeStatsFeatureIdMapper<I>> {
		public Builder() {
			super(new RelativeStatsFeatureIdMapper<I>());
		}

		/** @see #predicate */
		public Builder<I> predicate(Predicate<RelativeStatistic> predicate) {
			building.predicate = predicate;
			return this;
		}

		/** @see #idType */
		public Builder<I> idType(Class<I> idType) {
			building.idType = idType;
			return this;
		}

		@Override
		public RelativeStatsFeatureIdMapper<I> build() {
			RelativeStatsFeatureIdMapper<I> built = super.build();
			return built;
		}

	}// end Builder

	public static <I extends Identifier<?, ?>> Builder<I> create(Class<I> idType) {
		return new Builder<I>().idType(idType);
	}

	/** Use {@link Builder} to construct RelativeStatsFeatureIdMapper */
	private RelativeStatsFeatureIdMapper() {
	}

	@Override
	public void map(RelativeStatistic stats) {
		if (this.predicate == null || this.predicate.apply(stats)) {
			I featureId = RelativeStatsUtil.featureId(stats, idType);
			emit(featureId, stats);
		}
	}
}
