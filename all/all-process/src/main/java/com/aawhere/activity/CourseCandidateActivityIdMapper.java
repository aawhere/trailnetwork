package com.aawhere.activity;

import com.aawhere.activity.timing.ActivityTimingRelationUserStory;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.Resolution;
import com.google.appengine.tools.mapreduce.Mapper;

/**
 * Receiving an activity this emits the id of the activity and criteria that will put it into shards
 * with other activities that will make it a likely {@link CourseCandidate} (spatial buffer exact
 * matches, start/finish points).
 * 
 * TN-914 improved the efficiency of this by making the keys more granular
 * 
 * @author aroller
 * 
 */
public class CourseCandidateActivityIdMapper
		extends Mapper<Activity, String, ActivityId> {

	private static final long serialVersionUID = -1588252074208139837L;
	/**
	 * Gives the standard area for the single geocell that will represent the start/finish area. The
	 * higher resolution will reduce the number of possible matches and increase performance, but
	 * miss finding actual routes. The lower resolution will produce more possibilities and find
	 * more routes, but also provide a lot of wasted effort inspecting matches that won't happen.
	 * 
	 * This can (and should) be made configurable allowing for an admin/developer to compare results
	 * when processing with different resolutions.
	 */
	private static final Resolution ENDPOINT_RESOLUTION = Resolution.EIGHT;

	@Override
	public void map(Activity activity) {

		if (ActivityTimingRelationUserStory.isCourseCandidate(activity)) {
			GeoCell startCell = GeoCellUtil.geoCell(activity.getTrackSummary().getStart().getLocation(),
													ENDPOINT_RESOLUTION);
			GeoCell finishCell = GeoCellUtil.geoCell(	activity.getTrackSummary().getFinish().getLocation(),
														ENDPOINT_RESOLUTION);
			String spatialBuffer = activity.getTrackSummary().getSpatialBuffer().toString();
			String key = IdentifierUtils.createCompositeIdValue(startCell.toString(),
																finishCell.toString(),
																spatialBuffer.toString());
			emit(key, activity.id());
		}
	}

}
