package com.aawhere.activity;

import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ReciprocalGroupId;
import com.google.appengine.tools.mapreduce.Mapper;

/**
 * A simple mapper that extracts the {@link ReciprocalGroupId} and {@link ActivityTimingRelation}
 * from {@link ReciprocalTiming} to be used with {@link ActivityTimingToRouteCompletionReducer} and
 * such.
 * 
 * @see ActivityTimingToRouteCompletionReducer
 * @see CourseCandidateTimingMapper
 * 
 * @author aroller
 * 
 */
public class CourseCandidateReciprocalTimingMapper
		extends Mapper<ReciprocalTiming, ReciprocalGroupId, ActivityTiming> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1646336817529970179L;

	@Override
	public void map(ReciprocalTiming reciprocalTiming) {
		emit(reciprocalTiming.getKey(), reciprocalTiming.getValue());
	}

}
