package com.aawhere.activity.timing;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;

import com.aawhere.persist.EntityUtil;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

/**
 * The last step in timing discovery process (ActivityTimingDiscoveryMapper), this will receive at
 * most two reciprocals if they are part of the same discovery and update the reciprocal knowledge
 * if they do not already know. It will only emit timings that need to be persisted.
 * 
 * Receiving timings, either recently processed or previously existing (indicated by
 * {@link EntityUtil#isPersisted(com.aawhere.persist.BaseEntity)}), this will inspect the timings
 * given and pass it on if persistence is necessary.
 * 
 * Persistence is necessary if never been persisted before OR if course is completed, the reciprocal
 * was not previously known but it is known now.
 * 
 * Failing timings never need to be stored because reciprocals aren't updated for failing
 * timings...it is implied in the failure that it is not a reciprocal.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingReciprocalReducer
		extends Reducer<ActivityTimingReciprocalKey, ActivityTiming, ActivityTiming> {

	private static final long serialVersionUID = 1616151488615696630L;

	@Override
	public void reduce(ActivityTimingReciprocalKey key, ReducerInput<ActivityTiming> timingsInput) {
		ImmutableList<ActivityTiming> timings = ImmutableList.<ActivityTiming> builder().addAll(timingsInput).build();

		reduce(timings);

	}

	void reduce(ImmutableList<ActivityTiming> timings) {
		if (timings.size() > 2) {
			Iterable<DateTime> createDates = Iterables.transform(	timings,
																	EntityUtil.<ActivityTiming> dateCreatedFunction());
			throw new IllegalArgumentException("someone is providing multiple copies of timings " + timings
					+ " created " + Iterables.toString(createDates));
		}
		// guaranteed to have at least one...otherwise how did we get here?
		ActivityTiming left = timings.get(0);
		boolean persistLeft = !EntityUtil.isPersisted(left);

		if (timings.size() == 2) {
			ActivityTiming right = timings.get(1);
			boolean persistRight = !EntityUtil.isPersisted(right);
			Pair<ActivityTiming, ActivityTiming> reciprocalsUpdated = ActivityTimingRelationUtil
					.reciprocalsUpdated(left, right);

			if (reciprocalsUpdated.getLeft() != null) {
				persistLeft = true;
				left = reciprocalsUpdated.getLeft();
			}

			if (reciprocalsUpdated.getRight() != null) {
				persistRight = true;
				right = reciprocalsUpdated.getRight();
			}
			if (persistRight) {
				emit(right);
			}
		}

		// persistence is delayed until after reciprocity check
		if (persistLeft) {
			emit(left);
		}
	}
}
