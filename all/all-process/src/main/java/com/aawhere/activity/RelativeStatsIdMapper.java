package com.aawhere.activity;

import com.aawhere.activity.RelativeStatistic;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.Reducer;

/**
 * Simply extracts the {@link RelativeStatistic#id()} using it as the value. RelativeStats uses the
 * id as equality so it is the same as emitting itself, but the id by itself has a smaller footprint
 * so it is used as the value acting as a counter.
 * 
 * The likely recipient is a {@link Reducer} to count those that are similar using the key and
 * counting the values.
 * 
 * @see RelativeStatsCounterReducer
 * 
 * @author aroller
 * 
 */
public class RelativeStatsIdMapper
		extends Mapper<RelativeStatistic, RelativeStatistic, String> {

	private static final long serialVersionUID = -5441733143275009618L;

	@Override
	public void map(RelativeStatistic value) {
		emit(value, value.id());
	}

}
