package com.aawhere.activity;

import java.util.LinkedList;

import com.aawhere.activity.timing.ActivityTiming;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;

/**
 * Reducing by a common set of geocells all the activities have matched this will emit one
 * CourseCandidate for every pair. Each course candidate hence represents two {@link ActivityTiming}
 * entities.
 * 
 * So if A,B,C come in the iterator, this will emit:
 * 
 * A-B,A-C,B-C.
 * 
 * The course candidates all share the same group id so they can be related later during candidate
 * processing.
 * 
 * @author aroller
 * 
 */
public class ActivityGeoCellsCourseCandidateReducer
		extends Reducer<String, ActivityId, CourseCandidate> {

	@Override
	public void reduce(final String key, ReducerInput<ActivityId> activitiesIterator) {

		// TN-914 Reading all into memory is not sustainable
		// find another way to mix and match
		LinkedList<ActivityId> queue = Lists.newLinkedList();
		Iterators.addAll(queue, activitiesIterator);

		while (!queue.isEmpty()) {
			ActivityId activity1 = queue.removeFirst();
			for (ActivityId activity2 : queue) {
				if (!activity1.equals(activity2)) {
					emit(new CourseCandidate(key, activity1, activity2));
				}
			}

		}
	}

	private static final long serialVersionUID = 8332013018361753421L;
}
