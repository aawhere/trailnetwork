package com.aawhere.activity;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.all.process.AllProcessProviders;
import com.aawhere.app.account.AccountId;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;

/**
 * Activities are related only to their main accounts. The {@link ActivityAccountIdMapper} will lead
 * to associate all {@link Activities} to the same {@link AccountId} so the person can be retrieved
 * only once for the whole lot.
 * 
 * @author aroller
 * 
 */
public class ActivityAccountIdPersonIdReducer
		extends Reducer<AccountId, Activity, Pair<Activity, PersonId>> {

	private static final long serialVersionUID = 793163938019099937L;

	@Override
	public void reduce(AccountId accountId, ReducerInput<Activity> values) {
		try {
			Person person = AllProcessProviders.singleton().personAccountProvider.person(accountId);
			PersonId personId = person.id();

			while (values.hasNext()) {
				Activity activity = values.next();
				emit(Pair.of(activity, personId));
			}
		} catch (EntityNotFoundException e) {
			LoggerFactory.getLogger(getClass()).severe(accountId + " couldn't find person " + e.getMessage());
		}

	}

}
