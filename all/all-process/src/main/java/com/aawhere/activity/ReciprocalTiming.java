package com.aawhere.activity;

import java.io.Serializable;

import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingRelationUtil;
import com.aawhere.activity.timing.ReciprocalGroupId;
import com.aawhere.id.IdentifierUtils;
import com.google.appengine.tools.mapreduce.KeyValue;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

/**
 * A transporter for one {@link ActivityTimingRelation} that is
 * {@link ActivityTimingRelation#reciprocal()} represented by the {@link ReciprocalGroupId}. This
 * works well with results of {@link ActivityTimingRelationUtil#reciprocalGroups(Iterable)}.
 * 
 * @author aroller
 * 
 */
public class ReciprocalTiming
		extends KeyValue<ReciprocalGroupId, ActivityTiming>
		implements Serializable, Supplier<ActivityTiming> {

	public ReciprocalTiming(ReciprocalGroupId id, ActivityTiming timing) {
		super(id, timing);
	}

	@Override
	public String toString() {
		return IdentifierUtils.createCompositeIdValue(getKey(), getValue().id());
	}

	private static final long serialVersionUID = -7899156837389458754L;

	@SuppressWarnings("unchecked")
	public static Function<ReciprocalTiming, ActivityTiming> timingFunction() {
		return (Function<ReciprocalTiming, ActivityTiming>) function();
	}

	private static Function<? extends Supplier<ActivityTiming>, ActivityTiming> function() {
		return Suppliers.<ActivityTiming> supplierFunction();
	}

	@Override
	public ActivityTiming get() {
		return getValue();
	}
}
