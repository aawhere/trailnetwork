package com.aawhere.activity;

import com.aawhere.app.account.AccountId;
import com.google.appengine.tools.mapreduce.Mapper;

public class ActivityAccountIdMapper
		extends Mapper<Activity, AccountId, Activity> {

	private static final long serialVersionUID = -6413552826254612933L;

	@Override
	public void map(Activity value) {
		emit(value.getAccountId(), value);
	}

}
