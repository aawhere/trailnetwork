package com.aawhere.activity;

import java.io.Serializable;

import com.aawhere.activity.timing.ActivityTimingId;
import com.google.appengine.tools.mapreduce.KeyValue;

/**
 * Transport object used during the {@link RouteDiscoveryPipeline}.
 * 
 * * The key is An id that fuses like activities together so they may find each other again after
 * some intermediate processing.
 * 
 * @author aroller
 * 
 */
public class CourseCandidate
		extends KeyValue<String, ActivityTimingId>
		implements Serializable {

	/** Use {@link Builder} to construct CourseCandidate */
	public CourseCandidate(String id, ActivityId activity1, ActivityId activity2) {
		super(id, new ActivityTimingId(activity1, activity2));
	}

	private static final long serialVersionUID = -8043987720884969615L;

}
