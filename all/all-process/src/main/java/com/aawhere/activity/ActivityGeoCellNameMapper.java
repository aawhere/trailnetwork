package com.aawhere.activity;

import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.app.account.AccountId;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.util.rb.Message;
import com.google.appengine.tools.mapreduce.Mapper;

/**
 * Maps the activity name to the geocell start so that a common name can be collected to describe a
 * trailhead. The activity name is emitted whole so that the reducer can use the
 * {@link ActivityNameSelector} to determine the best fit.
 * 
 * @author aroller
 * 
 */
public class ActivityGeoCellNameMapper
		extends Mapper<Activity, String, Map.Entry<AccountId, String>> {

	private Resolution resolution = Trailhead.MAX_RESOLUTION;

	// TODO:allow configurable resolution

	private static final long serialVersionUID = 6589952236670486267L;

	@Override
	public void map(Activity activity) {
		Message name = activity.getName();
		if (name != null) {
			String nameString = name.toString();
			// only project those names that will be useful to process
			if (ActivityImportUtil.namedByPerson(activity.getApplication(), nameString)) {
				GeoCell geoCell = GeoCellUtil.geoCell(activity.getTrackSummary().getStart().getLocation(), resolution);
				Pair<AccountId, String> value = Pair.of(activity.getAccountId(), nameString);
				emit(geoCell.getCellAsString(), value);
			}
		}
	}

}
