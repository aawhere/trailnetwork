package com.aawhere.activity;

import com.aawhere.activity.timing.ActivityTimingId;
import com.google.appengine.tools.mapreduce.Mapper;

/**
 * Simply extracts the timing id from the course candidate providing better distribution of course
 * candidate shards to the reducer that will look for reciprocals for a single course. The reducer
 * will receive a single call for each timing making the work fo the reducer much smaller than if
 * the timings were grouped by reciprocal right now. This provides better reporting and sharding at
 * the cost of many more lightweight sorts.
 * 
 * TN-935 showed how course candidates created from geocells create poor distribution
 * 
 * @author aroller
 * 
 */
public class CourseCandidateCourseIdMapper
		extends Mapper<CourseCandidate, ActivityTimingId, CourseCandidate> {

	private static final long serialVersionUID = -7349520357979176394L;

	@Override
	public void map(CourseCandidate value) {
		emit(value.getValue(), value);

	}

}
