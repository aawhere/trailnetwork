package com.aawhere.process.mapreduce;

import java.util.List;

import com.aawhere.log.LoggerFactory;
import com.google.appengine.tools.mapreduce.GoogleCloudStorageFileSet;
import com.google.appengine.tools.mapreduce.MapReduceResult;
import com.google.appengine.tools.pipeline.Job1;
import com.google.appengine.tools.pipeline.Value;

public class GoogleCloudStorageFileSetLoggerJob
		extends Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> {

	private static final long serialVersionUID = 2162325631479463674L;

	@Override
	public Value<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {
		GoogleCloudStorageFileSet outputResult = fileSetResult.getOutputResult();
		List<String> fileNames = outputResult.getFileNames();
		LoggerFactory.getLogger(getClass()).fine("Cloud files found " + fileNames.toString());
		return null;
	}

}
