package com.aawhere.process.mapreduce.outputs;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.google.appengine.tools.mapreduce.Output;
import com.google.appengine.tools.mapreduce.OutputWriter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

/**
 * An In-Memory output that delegates the outputs to a function that will deal with the output. This
 * allows existing methods to receive output and handle the results without having to create a
 * specific {@link OutputWriter} for each. The results are in batches of the size selected so the
 * Output is pooled in memory until the limit is reached of slice or shard ends.
 * 
 * @author aroller
 * 
 * @param <O>
 */
public class DelegatingOutput<O>
		extends Output<O, Void> {

	private OutputWriterDelegate<O> delegate;
	private int batchSize = 100;

	public DelegatingOutput(OutputWriterDelegate<O> delegate) {
		this.delegate = delegate;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1185816072655823864L;

	@Override
	public List<? extends OutputWriter<O>> createWriters(int numShards) {
		Builder<DelegatingOutputWriter> writers = ImmutableList.builder();
		for (int i = 0; i < numShards; i++) {
			writers.add(new DelegatingOutputWriter());
		}
		return writers.build();
	}

	@Override
	public Void finish(Collection<? extends OutputWriter<O>> writers) throws IOException {
		// returns void, nothing to do
		return null;
	}

	public class DelegatingOutputWriter
			extends OutputWriter<O> {
		/** The buffer to contain the values until write is called. */
		private ArrayList<O> pool;
		private int size;

		public DelegatingOutputWriter() {
			try {
				flush();
			} catch (IOException e) {
				// this would be ridiculous
				throw new RuntimeException(e);
			}
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = -1196514598346491073L;

		@Override
		public void write(O value) throws IOException {
			pool.add(value);
			size++;
			if (size >= batchSize) {
				flush();
			}
		}

		private void flush() throws IOException {
			if (CollectionUtils.isNotEmpty(pool)) {
				delegate.write(Collections.unmodifiableList(pool));
			}
			pool = new ArrayList<>(batchSize);
			size = 0;
		}

		@Override
		public void endShard() throws IOException {
			flush();
			super.endShard();
		}

		@Override
		public void endSlice() throws IOException {
			flush();
			super.endSlice();
		}
	}

	/**
	 * Allows a client to provide a specific way of handling batches of output values in a specific
	 * manner left up to the client.
	 * 
	 * @author aroller
	 * 
	 * @param <O>
	 */
	public static interface OutputWriterDelegate<O>
			extends Serializable {
		/**
		 * Writes all the values to the output. Asynchronous processes must handle retries on their
		 * own if they won't report a failure.
		 * 
		 * @param values
		 *            the batch to be written
		 * @throws IOException
		 *             when any exception occurs. Runtimes can just be runtimes and need not be
		 *             converted
		 */
		void write(List<O> values) throws IOException;
	}

}
