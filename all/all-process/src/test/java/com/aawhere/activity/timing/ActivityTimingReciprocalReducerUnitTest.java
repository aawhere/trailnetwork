package com.aawhere.activity.timing;

import static com.aawhere.activity.timing.ActivityTimingTestUtil.*;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

public class ActivityTimingReciprocalReducerUnitTest {

	/** No expected emission because it is already persisted. */
	@Test
	public void testOnlyFailurePersisted() {
		ActivityTiming persisted = ActivityTimingTestUtil.persisted(ActivityTimingTestUtil.failure());
		ImmutableList<ActivityTiming> input = ImmutableList.of(persisted);
		final HashSet<ActivityTiming> expected = new HashSet<ActivityTiming>();
		assertReducer(input, expected);
	}

	@Test
	public void testOnlyPasserPersisted() {
		assertReducer(ImmutableList.of(persisted(createWithOneCompletion())), new HashSet<ActivityTiming>());
	}

	/**
	 * Expected single emission since the given failure is not yet persisted
	 * 
	 */
	@Test
	public void testOnlyFailureNotPersisted() {
		ActivityTiming failure = ActivityTimingTestUtil.failure();
		assertReducer(ImmutableList.of(failure), Sets.newHashSet(failure));
	}

	@Test
	public void testTwoFailuresNotPersisted() {
		ActivityTiming failure = ActivityTimingTestUtil.failure();

		ImmutableList<ActivityTiming> input = ImmutableList.of(	failure,
																ActivityTimingTestUtil.reciprocalFailure(failure));
		assertReducer(input, Sets.newHashSet(input));
	}

	@Test
	public void testLeftPassedRightFailedBothNotPersisted() {
		ActivityTiming passer = ActivityTimingTestUtil.createWithOneCompletion();
		ActivityTiming failer = ActivityTimingTestUtil.reciprocalFailure(passer);
		Map<ActivityTimingId, ActivityTiming> emitted = assertReducer(	ImmutableList.of(passer, failer),
																		Sets.newHashSet(passer, failer));
		assertFalse("reciprocal should be known", emitted.get(passer.id()).reciprocal());
	}

	@Test
	public void testRightPassedLeftFailedBothNotPersisted() {
		ActivityTiming passer = ActivityTimingTestUtil.createWithOneCompletion();
		ActivityTiming failer = ActivityTimingTestUtil.reciprocalFailure(passer);
		Map<ActivityTimingId, ActivityTiming> emitted = assertReducer(	ImmutableList.of(failer, passer),
																		Sets.newHashSet(passer, failer));
		assertEquals("reciprocal should be known", Boolean.FALSE, emitted.get(passer.id()).reciprocal());
	}

	@Test
	public void testReciprocalsBothNotPersisted() {
		ActivityTiming passer = ActivityTimingTestUtil.createWithOneCompletion();
		ActivityTiming reciprocal = ActivityTimingTestUtil.reciprocal(passer);
		Map<ActivityTimingId, ActivityTiming> emitted = assertReducer(	ImmutableList.of(passer, reciprocal),
																		Sets.newHashSet(passer, reciprocal));
		assertTrue("reciprocal should be known", emitted.get(passer.id()).reciprocal());
		assertTrue("reciprocal should be known", emitted.get(reciprocal.id()).reciprocal());
	}

	@Test
	public void testReciprocalsAlreadyKnownBothPersisted() {
		ActivityTiming left = persisted(createWithOneCompletion());
		ActivityTiming right = persisted(ActivityTimingTestUtil.reciprocal(left));
		// this ensures left has reciprocal assigned to it.
		left = ActivityTimingRelationUtil.reciprocalsUpdated(left, right).getLeft();
		assertReducer(ImmutableList.of(left, right), Sets.<ActivityTiming> newHashSet());
	}

	@Test
	public void testReciprocalsAlreadyKnownRightBothPersisted() {
		ActivityTiming left = persisted(createWithOneCompletion());
		ActivityTiming right = persisted(ActivityTimingTestUtil.reciprocal(left));

		assertReducer(ImmutableList.of(left, right), Sets.<ActivityTiming> newHashSet(left));
	}

	@Test
	public void testReciprocalsAlreadyKnownLeftBothPersisted() {
		ActivityTiming right = persisted(createWithOneCompletion());
		ActivityTiming left = persisted(ActivityTimingTestUtil.reciprocal(right));
		assertReducer(ImmutableList.of(left, right), Sets.<ActivityTiming> newHashSet(right));
	}

	@Test
	public void testReciprocalsNeitherKnownBothPersisted() {
		ActivityTiming right = persisted(createWithOneCompletion());
		ActivityTiming left = persisted(ActivityTiming.create().courseId(right.attemptId()).attemptId(right.courseId())
				.setCompletions(right.completions()).build());
		assertReducer(ImmutableList.of(left, right), Sets.<ActivityTiming> newHashSet(right, left));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNotReciprocals() {
		assertReducer(	ImmutableList.of(createWithOneCompletion(), createWithOneCompletion()),
						new HashSet<ActivityTiming>());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTooManyProvided() {
		assertReducer(ImmutableList.of(createWithOneCompletion(), failure(), failure()), new HashSet<ActivityTiming>());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testNotEnough() {
		assertReducer(ImmutableList.<ActivityTiming> of(), new HashSet<ActivityTiming>());
	}

	public Map<ActivityTimingId, ActivityTiming> assertReducer(ImmutableList<ActivityTiming> input,
			final HashSet<ActivityTiming> expected) {
		final HashMap<ActivityTimingId, ActivityTiming> emitted = new HashMap<ActivityTimingId, ActivityTiming>();
		final ActivityTimingReciprocalReducer reducer = new ActivityTimingReciprocalReducer() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void emit(ActivityTiming value) {
				TestUtil.assertContains(value, expected);
				expected.remove(value);
				emitted.put(value.id(), value);
			};
		};

		reducer.reduce(input);
		TestUtil.assertEmpty("expected emission was not made", expected);
		return emitted;
	}
}
