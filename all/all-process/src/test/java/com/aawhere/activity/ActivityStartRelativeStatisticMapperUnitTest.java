package com.aawhere.activity;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.all.process.AllProcessTestUtil;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonTestUtil;
import com.google.appengine.tools.mapreduce.MapperTester;

/**
 * @see ActivityStartRelativeStatisticMapper
 * @see MapperTester
 * 
 * @author aroller
 * 
 */
public class ActivityStartRelativeStatisticMapperUnitTest {

	@Before
	public void setUp() {
		AllProcessTestUtil.setUp();
	}

	@Test
	public void testActivities() {
		Activities activities = ActivityTestUtil.createActivities();
		HashMap<Activity, PersonId> map = new HashMap<Activity, PersonId>();
		for (Activity activity : activities) {
			try {
				map.put(activity, PersonTestUtil.personAccountProvider().person(activity.getAccountId()).getId());
			} catch (EntityNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
		Set<Entry<Activity, PersonId>> entries = map.entrySet();
		// the mapper creates a stat for every layer
		int expectedNumberOfLayers = Layer.values().length;
		Integer expectedNumberOfEmitCalls = expectedNumberOfLayers * activities.size();
		MapperTester<Entry<Activity, PersonId>, GeoCell, RelativeStatistic> tester = MapperTester
				.create(ActivityStartRelativeStatisticMapper.class).inputs(entries)
				.expectedEmitCalls(expectedNumberOfEmitCalls).build();

	}
}
