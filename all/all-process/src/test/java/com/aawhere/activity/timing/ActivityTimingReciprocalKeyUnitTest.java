package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.activity.ActivityId;
import com.aawhere.app.KnownApplication;

public class ActivityTimingReciprocalKeyUnitTest {

	private static final ActivityId left = new ActivityId(KnownApplication.UNKNOWN.key, "1");
	private static final ActivityId right = new ActivityId(KnownApplication.UNKNOWN.key, "2");
	private static final ActivityTimingId inOrder = new ActivityTimingId(left, right);
	private static final ActivityTimingReciprocalKey IN_ORDER_KEY = new ActivityTimingReciprocalKey(inOrder);
	private static final ActivityTimingId outOfOrder = new ActivityTimingId(right, left);
	private static final ActivityTimingReciprocalKey OUT_OF_ORDER_KEY = new ActivityTimingReciprocalKey(outOfOrder);

	@Test
	public void testInOrder() {
		ActivityTimingReciprocalKey provided = IN_ORDER_KEY;
		assertKey(provided);
	}

	@Test
	public void testOutOfOrder() {
		assertKey(OUT_OF_ORDER_KEY);
	}

	@Test
	public void testEquality() {
		assertEquals(IN_ORDER_KEY, OUT_OF_ORDER_KEY);
		assertEquals(IN_ORDER_KEY.hashCode(), OUT_OF_ORDER_KEY.hashCode());
	}

	public void assertKey(ActivityTimingReciprocalKey provided) {
		assertEquals(IN_ORDER_KEY.getValue(), provided.getValue());
	}

}
