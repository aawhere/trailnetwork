package com.aawhere.activity;

import java.util.HashSet;

import org.junit.Test;

import com.aawhere.app.account.AccountId;
import com.aawhere.test.TestUtil;
import com.google.appengine.tools.mapreduce.MapperTester;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

public class ActivityAccountIdMapperUnitTest {

	@Test
	public void testMultipleActivities() {

		HashSet<Activity> activities = Sets.newHashSet(ActivityTestUtil.createActivities());

		MapperTester<Activity, AccountId, Activity> testMapper = MapperTester.create(ActivityAccountIdMapper.class)
				.inputs(activities).build();

		TestUtil.assertIterablesEquals(	"activities didn't get mapped",
										activities,
										Iterables.concat(testMapper.emitted().asMap().values()));
		TestUtil.assertIterablesEquals("account ids are wrong", ActivityUtil.accountIds(activities), testMapper
				.emitted().keySet());
	}

}
