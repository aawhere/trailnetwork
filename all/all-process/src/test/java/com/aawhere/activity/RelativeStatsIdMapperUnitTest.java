package com.aawhere.activity;

import java.util.List;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.google.appengine.tools.mapreduce.MapperTester;

public class RelativeStatsIdMapperUnitTest {

	@Test
	public void testPersonActivityTypes() {
		List<RelativeStatistic> personActivityTypes = RelativeStatsTestUtil.personActivityTypes();
		MapperTester<RelativeStatistic, RelativeStatistic, String> mapperTester = MapperTester
				.create(RelativeStatsIdMapper.class).inputs(personActivityTypes).build();
		TestUtil.assertIterablesEquals("values", RelativeStatsUtil.ids(personActivityTypes), mapperTester.emitted()
				.values());
		TestUtil.assertIterablesEquals("keys", personActivityTypes, mapperTester.emitted().keySet());
	}
}
