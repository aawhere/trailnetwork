package com.aawhere.activity;

import java.util.List;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.google.appengine.tools.mapreduce.MapperTester;
import com.google.common.base.Functions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

public class RelativeStatsFeatureIdMapperUnitTest {

	@Test
	public void testActivities() {
		List<RelativeStatistic> personActivityTypes = RelativeStatsTestUtil.personActivityTypes();

		RelativeStatsFeatureIdMapper<ActivityId> mapper = RelativeStatsFeatureIdMapper.create(ActivityId.class).build();
		MapperTester<RelativeStatistic, ActivityId, RelativeStatistic> tester = MapperTester.create(mapper)
				.inputs(personActivityTypes).keyType(ActivityId.class).build();

		Iterable<String> featureIds = RelativeStatsUtil.featureIds(personActivityTypes);
		TestUtil.assertIterablesEquals(	"feature ids",
										featureIds,
										Iterables.transform(tester.emitted().keySet(), Functions.toStringFunction()));
	}

	@Test
	public void testPersonalExcluded() {
		RelativeStatistic bike = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.BIKE).build();
		RelativeStatistic all = RelativeStatsTestUtil.allBuilder().build();
		RelativeStatistic personal = RelativeStatsTestUtil.personActivityType();
		ImmutableList<RelativeStatistic> list = ImmutableList.of(personal, all, bike);
		RelativeStatsFeatureIdMapper<ActivityId> personExcludedMapper = RelativeStatsFeatureIdMapper
				.create(ActivityId.class).predicate(RelativeStatsUtil.personalExcludedPredicate()).build();
		MapperTester<RelativeStatistic, ActivityId, RelativeStatistic> tester = MapperTester
				.create(personExcludedMapper).keyType(ActivityId.class).inputs(list).expectedEmitCalls(2).build();

		TestUtil.assertContains(all, tester.emitted().values());
		TestUtil.assertContains(bike, tester.emitted().values());
	}
}
