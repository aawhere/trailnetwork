package com.aawhere.all.process;

import com.aawhere.person.PersonTestUtil;

public class AllProcessTestUtil {

	/**
	 * This provides stateless services and AllProcessProviders uses static availability so having
	 * only one across all tests is appropriate.
	 * 
	 */
	public static final AllProcessProviders providers = AllProcessProviders.singleton(new AllProcessProviders(
			PersonTestUtil.instance(), PersonTestUtil.personAccountProvider()));

	/**
	 * Call this in your setup to ensure the {@link #providers} is initialized
	 * 
	 */
	public static void setUp() {
		// actually just ensures that static initialization happens

	}
}
