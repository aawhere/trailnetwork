/**
 * 
 */
package com.aawhere.app;

import static org.junit.Assert.*;

import org.junit.Before;

import com.aawhere.app.ApplicationSummary.Builder;
import com.aawhere.persist.BaseEntityBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.MockRepository;
import com.aawhere.persist.RepositoryUpdateable;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class ApplicationSummaryUnitTest
		extends BaseEntityBaseUnitTest<ApplicationSummary, Builder, ApplicationSummaryId, ApplicationSummaryRepository> {

	private ApplicationKey applicationKey;
	private Integer activityCount;
	private Integer accountCount;
	private Integer accountCountMutated;

	/**
	 * 
	 */
	public ApplicationSummaryUnitTest() {
		super(true);
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		this.applicationKey = KnownApplication.UNKNOWN.key;
		this.accountCount = TestUtil.generateRandomInt();
		this.accountCountMutated = TestUtil.generateRandomInt();
		this.activityCount = TestUtil.generateRandomInt();
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected ApplicationSummaryRepository createRepository() {
		return new ApplicationSummaryMockRepository();
	}

	/*
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.accountCount(accountCount);
		builder.activityCount(activityCount);
		builder.application(applicationKey);
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertSample(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertSample(ApplicationSummary sample) {
		super.assertSample(sample);
		assertEquals(this.activityCount, sample.activityCount());
		assertEquals(this.applicationKey, sample.getId().getApplicationKey());
	}

	/*
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertCreation(ApplicationSummary sample) {
		super.assertCreation(sample);
		// see assertSample for non-mutated values
		assertEquals(this.accountCount, sample.accountCount());
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.Identifier)
	 */
	@Override
	protected ApplicationSummary mutate(ApplicationSummaryId id) throws EntityNotFoundException {
		return getRepository().update(id).accountCount(this.accountCountMutated).build();
	}

	/*
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(ApplicationSummary mutated) {
		super.assertMutation(mutated);
		assertEquals(this.accountCountMutated, mutated.accountCount());
	}

	/**
	 * FIXME: this should be provided by the Mock when it integrates {@link RepositoryUpdateable}.
	 * 
	 * @author aroller
	 * 
	 */
	public static class ApplicationSummaryMockRepository
			extends MockRepository<ApplicationSummary, ApplicationSummaryId>
			implements ApplicationSummaryRepository {

		/*
		 * @see com.aawhere.persist.RepositoryUpdateable#update(com.aawhere.id.Identifier)
		 */
		@Override
		public Builder update(ApplicationSummaryId id) throws EntityNotFoundException {
			return new Builder(load(id)) {
				/*
				 * @see com.aawhere.app.ApplicationSummary.Builder#build()
				 */
				@Override
				public ApplicationSummary build() {
					return update(super.build());
				}
			};
		}
	}
}
