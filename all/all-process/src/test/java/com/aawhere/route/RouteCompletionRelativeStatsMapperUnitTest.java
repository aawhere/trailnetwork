package com.aawhere.route;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import com.aawhere.activity.RelativeStatistic;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.test.TestUtil;
import com.google.appengine.tools.mapreduce.MapperTester;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Sets;

public class RouteCompletionRelativeStatsMapperUnitTest {
	@Test
	public void testMultipleActivities() {

		List<RouteCompletion> completions = RouteCompletionTestUtil.completionsSamePersonConsecutiveTimes();
		// one for person, person_activitty, person_distance
		int expectedCallsPerCompletion = 3;
		MapperTester<RouteCompletion, RelativeStatistic, RouteCompletionId> testMapper = MapperTester
				.create(RouteCompletionRelativeStatsMapper.class).inputs(completions)
				.mapper(RouteCompletionRelativeStatsMapper.trailheadMapper())
				.expectedEmitCalls(completions.size() * expectedCallsPerCompletion).build();

		ImmutableMultimap<RelativeStatistic, RouteCompletionId> emitted = testMapper.emitted();
		Set<RouteCompletionId> ids = Sets.newHashSet(emitted.values());
		TestUtil.assertIterablesEquals("completions missing", ids, IdentifierUtils.ids(completions));
		ImmutableCollection<Entry<RelativeStatistic, RouteCompletionId>> entries = emitted.entries();
		for (Entry<RelativeStatistic, RouteCompletionId> entry : entries) {
			RouteCompletionId completionId = entry.getValue();
			RouteCompletion routeCompletion = IdentifierUtils.filter(completions, completionId);
			assertNotNull(completionId + " not found", routeCompletion);
			RelativeStatistic relativeStatistic = entry.getKey();

		}
	}

}
