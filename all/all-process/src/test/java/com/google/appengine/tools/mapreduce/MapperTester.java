package com.google.appengine.tools.mapreduce;

import java.util.List;

import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;

import com.aawhere.collections.Index;
import com.aawhere.collections.MultimapsExtended;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.reflect.GenericUtils;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;

/**
 * Helper to be used when testing mapper overrides. The emit method should call {@link #emitted} put
 * method. Then map should be called with the inputs and validation can be tested again the emitted
 * results.
 * 
 * @author aroller
 * 
 * @param <I>
 * @param <K>
 * @param <V>
 */
public class MapperTester<I, K, V> {

	private ImmutableListMultimap<K, V> emitted;

	/**
	 * Used to construct all instances of TestMapper.
	 */
	public static class Builder<I, K, V>
			extends ObjectBuilder<MapperTester<I, K, V>> {
		private Class<? extends Mapper<I, K, V>> mapperType;
		private Iterable<I> inputs;
		private ArgumentCaptor<K> keyCaptor;
		private ArgumentCaptor<V> valueCaptor;
		private Integer expectedEmitCalls;
		/** Optionally provided if configurations must be made */
		private Mapper<I, K, V> mapper;
		private Class<K> keyType;

		public Builder() {
			super(new MapperTester<I, K, V>());
		}

		@Override
		protected void validate() {
			Assertion.exceptions().notNull("inputs", this.inputs);
			super.validate();
		}

		/** @see #expectedEmitCalls */
		public Builder<I, K, V> expectedEmitCalls(Integer expectedEmitCalls) {
			this.expectedEmitCalls = expectedEmitCalls;
			return this;
		}

		@Override
		public MapperTester<I, K, V> build() {
			MapperTester<I, K, V> built = super.build();
			if (this.expectedEmitCalls == null) {
				this.expectedEmitCalls = Iterables.size(this.inputs);
			}

			if (this.keyType == null) {
				this.keyType = GenericUtils.getTypeArgument(this.mapperType, Index.SECOND);
			}
			Class<V> valueType = GenericUtils.getTypeArgument(this.mapperType, Index.THIRD);
			this.keyCaptor = ArgumentCaptor.forClass(keyType);
			this.valueCaptor = ArgumentCaptor.forClass(valueType);

			Mapper<I, K, V> mock;
			if (mapper == null) {
				mock = Mockito.mock(mapperType);
				// create a partial stub testing the real map method
				Mockito.doAnswer(Answers.CALLS_REAL_METHODS.get()).when(mock).map(Mockito.<I> any());
			} else {
				// spy on the object and explicity capture emit
				mock = Mockito.spy(this.mapper);
				Mockito.doAnswer(Answers.RETURNS_DEEP_STUBS.get()).when(mock)
						.emit(keyCaptor.capture(), valueCaptor.capture());
			}

			// pass in the inputs
			for (I input : this.inputs) {
				mock.map(input);
			}

			// verify emit has been called by map the expected number of times
			Mockito.verify(mock, VerificationModeFactory.times(this.expectedEmitCalls)).emit(	keyCaptor.capture(),
																								valueCaptor.capture());
			List<K> allKeys = keyCaptor.getAllValues();
			List<V> allValues = valueCaptor.getAllValues();
			ListMultimap<K, V> multimap = MultimapsExtended.multimap(allKeys, allValues);

			built.emitted = ImmutableListMultimap.copyOf(multimap);
			return built;
		}

		private Builder<I, K, V> mapper(Class<? extends Mapper<I, K, V>> mapperType) {
			this.mapperType = mapperType;
			return this;
		}

		public Builder<I, K, V> inputs(Iterable<I> inputs) {
			this.inputs = inputs;
			return this;

		}

		public Builder<I, K, V> keyType(Class<K> keyType) {
			this.keyType = keyType;
			return this;
		}

		/**
		 * Optional mapper to spy on it if you have special configurations.
		 * 
		 * @param mapper
		 * @return
		 */
		@SuppressWarnings("unchecked")
		public Builder<I, K, V> mapper(Mapper<I, K, V> mapper) {
			this.mapper = mapper;
			this.mapperType = (Class<? extends Mapper<I, K, V>>) mapper.getClass();
			return this;
		}

	}// end Builder

	private static <I, K, V> Builder<I, K, V> create() {
		return new Builder<I, K, V>();
	}

	public static <I, K, V> Builder<I, K, V> create(Class<? extends Mapper<I, K, V>> mapperType) {

		return MapperTester.<I, K, V> create().mapper(mapperType);
	}

	public static <I, K, V> Builder<I, K, V> create(Mapper<I, K, V> mapper) {

		return MapperTester.<I, K, V> create().mapper(mapper);
	}

	/** Use {@link Builder} to construct TestMapper */
	private MapperTester() {
	}

	public ImmutableMultimap<K, V> emitted() {
		return emitted;
	}
}
