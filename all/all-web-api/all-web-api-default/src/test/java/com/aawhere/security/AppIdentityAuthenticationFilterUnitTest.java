/**
 *
 */
package com.aawhere.security;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.aawhere.identity.IdentityManagerProvider;

/**
 * Unit Test for AppIdentityAuthenticationFilter
 * 
 * @author Brian Chapman
 * 
 */
public class AppIdentityAuthenticationFilterUnitTest {

	private static final String TN_LIVE_APPID = "tn-live";

	@Test
	public void filterTest() throws IOException, ServletException {
		String userId = "1234";
		HttpServletRequest request = mock(HttpServletRequest.class);
		ArgumentCaptor<HttpServletRequest> captor = ArgumentCaptor.forClass(HttpServletRequest.class);
		when(request.getHeader(AppIdentityAuthenticationFilter.APPID_HEADER_NAME)).thenReturn(TN_LIVE_APPID);
		when(request.getHeader(IdentityManagerProvider.ACCOUNT_ID_HEADER_NAME)).thenReturn(userId);

		FilterChain chain = mock(FilterChain.class);
		AppIdentityAuthenticationFilter filter = new AppIdentityAuthenticationFilter();

		filter.doFilter(request, null, chain);
		verify(chain, times(1)).doFilter(captor.capture(), any(HttpServletResponse.class));
		HttpServletRequest filteredRequest = captor.getValue();
		assertTrue(filteredRequest.isUserInRole(SecurityRoles.TRUSTED_APP));
		assertEquals(AuthScheme.TN_LIVE.toString(), filteredRequest.getAuthType());
		assertEquals(userId, filteredRequest.getUserPrincipal().getName());
	}
}