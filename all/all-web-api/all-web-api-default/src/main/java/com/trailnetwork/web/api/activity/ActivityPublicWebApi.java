package com.trailnetwork.web.api.activity;

import static com.aawhere.activity.ActivityWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.Path;

import com.aawhere.activity.ActivityExportService;
import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityProcessService;
import com.aawhere.activity.export.kml.ActivityKmlExportService;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.route.RouteService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.activity.ActivityWebApi;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

/**
 * The JAX-RS way to publish endpoints written in {@link ActivityWebApi} allowing for others to
 * override and extend the endpoints in other web modules.
 * 
 * @author aroller
 * 
 */
@Singleton
@Path(ACTIVITIES)
@Api(value = SLASH + ACTIVITIES, description = "GPS Tracks recorded for an Account.",
		position = ApiDocumentationSupport.Position.ACTIVITIES)
public class ActivityPublicWebApi
		extends ActivityWebApi {

	@Inject
	public ActivityPublicWebApi(ActivityTimingService timingRelationService,
			ActivityImportService activityImportHistoryService, FieldAnnotationDictionaryFactory dictionaryFactory,
			ActivityImportService importService, ActivityKmlExportService kmlExportService,
			ActivityExportService activityExportService, RouteService routeService,
			ActivityProcessService activityProcessService) {
		super(timingRelationService, activityImportHistoryService, dictionaryFactory, importService, kmlExportService,
				activityExportService, routeService, activityProcessService);
	}

	// declare all methods in parent
}
