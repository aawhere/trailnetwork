/**
 * 
 */
package com.trailnetwork.web.api.activity.timing;

import static com.aawhere.activity.timing.ActivityTimingWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.Path;

import com.aawhere.activity.export.kml.ActivityKmlExportService;
import com.aawhere.activity.timing.ActivityTimingProcessService;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.route.RouteService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.activity.timing.ActivityTimingWebApi;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

/**
 * The JAX-RS way to publish endpoints written in {@link ActivityTimingWebApi} allowing for others
 * to override and extend the endpoints in other web modules.
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(SLASH + ACTIVITIES_TIMINGS_PATH)
@Api(value = ACTIVITIES_TIMINGS_PATH, description = ActivityTimingWebApi.DESCRIPTION,
		position = ApiDocumentationSupport.Position.TIMINGS)
public class ActivityTimingPublicApi
		extends ActivityTimingWebApi {
	/**
	 * 
	 */
	@Inject
	public ActivityTimingPublicApi(ActivityTimingService service, RouteService routeService,
			ActivityKmlExportService kmlExportService, ActivityTimingProcessService timingProcessService) {
		super(service, routeService, kmlExportService, timingProcessService);
	}

}
