package com.trailnetwork.web.api.app.account;

import static com.aawhere.app.account.AccountWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.Path;

import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityProcessService;
import com.aawhere.app.account.AccountService;
import com.aawhere.person.PersonService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.app.account.AccountWebApi;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

@Singleton
@Path(SLASH + ACCOUNTS)
@Api(value = SLASH + ACCOUNTS, description = "Users for Persons at Applications.",
		position = ApiDocumentationSupport.Position.APPLICATIONS)
public class AccountPublicWebApi
		extends AccountWebApi {

	@Inject
	public AccountPublicWebApi(AccountService accountService, PersonService personService,
			ActivityImportService importService, ActivityProcessService activityProcessService) {
		super(accountService, personService, importService, activityProcessService);
	}

}
