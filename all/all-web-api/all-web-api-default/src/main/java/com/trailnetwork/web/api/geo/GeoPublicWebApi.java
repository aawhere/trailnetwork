/**
 *
 */
package com.trailnetwork.web.api.geo;

import static com.aawhere.web.api.geo.GeoWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.inject.Inject;
import javax.ws.rs.Path;

import com.aawhere.activity.ActivityProcessService;
import com.aawhere.geo.GeoCellProcessService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.geo.GeoWebApi;

import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

/**
 * @see com.aawhere.web.api.geo.GeoWebApi
 * @author Brian Chapman
 * 
 */
@Path(SLASH + GEO)
@Api(value = SLASH + GEO, description = "Geo-spatial support information.",
		position = ApiDocumentationSupport.Position.GEO)
@Singleton
public class GeoPublicWebApi
		extends GeoWebApi {

	@Inject
	public GeoPublicWebApi(ActivityProcessService activityProcessService, GeoCellProcessService geoCellProcessService) {
		super(activityProcessService, geoCellProcessService);
	}

	// declare all methods in parent

}
