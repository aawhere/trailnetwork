/**
 *
 */
package com.trailnetwork.web.api.search;

import javax.ws.rs.Path;

import com.aawhere.person.PersonProcessService;
import com.aawhere.route.RouteService;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.trailhead.TrailheadService;
import com.aawhere.web.api.search.SearchWebApi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

/**
 * @author brian
 * 
 */
@Singleton
@Path(SearchWebApiUri.SEARCH)
@Api(value = SearchWebApiUri.SEARCH, description = "Search TrailNetwork entity data.")
public class SearchPublicWebApi
		extends SearchWebApi {

	@Inject
	public SearchPublicWebApi(SearchService searchService, RouteService routeService,
			TrailheadService trailheadService, PersonProcessService personProcessService) {
		super(searchService, routeService, trailheadService, personProcessService);
	}
}
