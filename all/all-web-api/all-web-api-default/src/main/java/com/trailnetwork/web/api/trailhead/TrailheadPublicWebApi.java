/**
 *
 */
package com.trailnetwork.web.api.trailhead;

import static com.aawhere.trailhead.TrailheadWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.Path;

import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.route.RouteExportService;
import com.aawhere.route.RouteService;
import com.aawhere.trailhead.TrailheadExportService;
import com.aawhere.trailhead.TrailheadProcessService;
import com.aawhere.trailhead.TrailheadService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.search.SearchWebApi;
import com.aawhere.web.api.trailhead.TrailheadWebApi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

/**
 * @see SearchWebApi
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(SLASH + TRAILHEADS)
@Api(value = SLASH + TRAILHEADS, description = "Meeting places where Routes start & finish.",
		position = ApiDocumentationSupport.Position.TRAILHEADS)
public class TrailheadPublicWebApi
		extends TrailheadWebApi {

	@Inject
	public TrailheadPublicWebApi(TrailheadService trailheadService, RouteService routeService,
			TrailheadExportService exportService, RouteExportService routeExportService,
			TrailheadProcessService trailheadProcessService, FieldAnnotationDictionaryFactory dictionaryFactory) {
		super(trailheadService, routeService, exportService, routeExportService, trailheadProcessService,
				dictionaryFactory);
	}

	// declare all methods in parent
}
