/**
 *
 */
package com.trailnetwork.web.api.person.group;

import static com.aawhere.person.group.SocialGroupWebApiUri.*;

import javax.ws.rs.Path;

import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupMembershipService;
import com.aawhere.person.group.SocialGroupService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.person.group.SocialGroupWebApi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

/**
 * @See {@link SocialGroupWebApi}
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(SOCIAL_GROUPS)
@Api(value = SocialGroup.FIELD.SOCIAL_GROUPS, description = "Persons related through their Activities.",
		position = ApiDocumentationSupport.Position.SOCIAL_GROUPS)
public class SocialGroupPublicWebApi
		extends SocialGroupWebApi {

	/**
	 *
	 */
	@Inject
	public SocialGroupPublicWebApi(SocialGroupService groupService, SocialGroupMembershipService socialGroupService) {
		super(groupService, socialGroupService);
	}

	// declare all methods in parent

}
