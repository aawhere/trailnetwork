/**
 *
 */
package com.trailnetwork.web.api.route;

import static com.aawhere.route.RouteWebApiUri.*;

import javax.ws.rs.Path;

import com.aawhere.route.RouteCompletionService;
import com.aawhere.route.RouteExportService;
import com.aawhere.route.RouteMessage;
import com.aawhere.route.RouteProcessService;
import com.aawhere.route.RouteService;
import com.aawhere.route.event.RouteEventService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.route.RouteWebApi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

/**
 * @see RouteWebApi
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(ROUTES)
@Api(value = ROUTES, description = RouteMessage.Doc.DESCRIPTION, position = ApiDocumentationSupport.Position.ROUTES)
public class RoutePublicWebApi
		extends RouteWebApi {

	/**
	 *
	 */
	@Inject
	public RoutePublicWebApi(RouteService routeService, RouteExportService exportService,
			RouteCompletionService completionService, RouteEventService routeEventService,
			RouteProcessService routeProcessService) {
		super(routeService, exportService, completionService, routeEventService, routeProcessService);
	}
}
