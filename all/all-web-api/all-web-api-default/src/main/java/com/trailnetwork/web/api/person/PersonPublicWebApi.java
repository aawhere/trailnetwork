/**
 *
 */
package com.trailnetwork.web.api.person;

import static com.aawhere.person.PersonWebApiUri.*;

import javax.ws.rs.Path;

import com.aawhere.activity.ActivityService;
import com.aawhere.person.PersonProcessService;
import com.aawhere.person.PersonService;
import com.aawhere.person.group.SocialGroupMembershipService;
import com.aawhere.route.RouteService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.person.PersonWebApi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

/**
 * @see PersonWebApi
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(PERSONS)
@Api(value = PERSONS, description = "Individuals that have one or more Accounts.",
		position = ApiDocumentationSupport.Position.PERSONS)
public class PersonPublicWebApi
		extends PersonWebApi {

	/**
	 *
	 */
	@Inject
	public PersonPublicWebApi(PersonService personService, SocialGroupMembershipService socialGroupService,
			ActivityService activityService, RouteService routeService, PersonProcessService personProcessService) {
		super(personService, socialGroupService, activityService, routeService, personProcessService);
	}

	// declare all methods in parent
}
