package com.trailnetwork.web.api.app;

import static com.aawhere.app.ApplicationWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.Path;

import com.aawhere.activity.ActivityProcessService;
import com.aawhere.app.account.AccountProcessService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.app.ApplicationWebApi;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;

@Singleton
@Path(SLASH + APPLICATIONS)
@Api(value = SLASH + APPLICATIONS, description = "Websites and Device Apps that provide Activities.",
		position = ApiDocumentationSupport.Position.APPLICATIONS)
public class ApplicationPublicWebApi
		extends ApplicationWebApi {

	@Inject
	public ApplicationPublicWebApi(AccountProcessService accountProcessService,
			ActivityProcessService activityProcessService) {
		super(accountProcessService, activityProcessService);
	}

}
