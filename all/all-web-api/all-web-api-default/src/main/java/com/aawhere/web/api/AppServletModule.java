package com.aawhere.web.api;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.ext.Provider;

import com.aawhere.activity.ActivitiesFromIdsXmlAdapter;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityApplicationXmlAdapter;
import com.aawhere.activity.ActivityFromIdXmlAdapter;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ApplicationActivitiesReferenceXmlAdapter;
import com.aawhere.activity.ApplicationActivityReferenceXmlAdapter;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationXmlAdapter;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountApplicationXmlAdapter;
import com.aawhere.field.FieldValueProvider;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactoryProvider;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.field.type.DataTypeAdapterFactoryProvider;
import com.aawhere.id.IdentifierTranslators;
import com.aawhere.identity.IdentityManager;
import com.aawhere.identity.IdentityManagerProvider;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsOptionalXmlAdapter;
import com.aawhere.persist.FieldPropertyEntityUtil.FieldPropertyEntityProvider;
import com.aawhere.persist.FilterConditionSupportedTypeRegistry;
import com.aawhere.persist.FilterConditionSupportedTypeRegistryProvider;
import com.aawhere.persist.RegisteredEntities;
import com.aawhere.persist.RegisteredEntitiesObjectifyFactory;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;
import com.aawhere.personalize.LocaleProvider;
import com.aawhere.route.FinishTrailheadIdXmlAdapter;
import com.aawhere.route.Route;
import com.aawhere.route.RouteCompletion;
import com.aawhere.route.RouteId;
import com.aawhere.route.RoutePointsGpolylineXmlAdapter;
import com.aawhere.route.RoutePointsXmlAdapter;
import com.aawhere.route.RouteTrackDataSourceServlet;
import com.aawhere.route.RouteXmlAdapter;
import com.aawhere.route.StartTrailheadIdXmlAdapter;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventStatistic;
import com.aawhere.route.event.RouteEventXmlAdapter;
import com.aawhere.search.GaeSearchService;
import com.aawhere.search.SearchService;
import com.aawhere.track.TrackFormatXmlAdapter;
import com.aawhere.track.TrackFormatXmlAdapterFactory;
import com.aawhere.track.TrackSummary;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadIdXmlAdapter;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.xml.bind.OptionalXmlAdapterFactory;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.servlet.ServletModule;
import com.google.inject.servlet.ServletScopes;
import com.googlecode.objectify.cache.AsyncCacheFilter;
import com.thetransactioncompany.cors.CORSFilter;

/**
 * Configures the servlet module to find resources and {@link Provider}s in the system. TODO: Make
 * this abstract so other Web APIs can take advantage of this general work.
 * 
 * @author Aaron Roller
 */
public class AppServletModule
		extends ServletModule {

	private final String OBJECTIFY_ASYNC_CACHE_FILTER_PATH = "/*";

	/*
	 * Position matters!!
	 */
	@Override
	protected void configureServlets() {
		bind(ObjectifyRepository.Synchronization.class).toInstance(Synchronization.ASYNCHRONOUS);
		bind(AsyncCacheFilter.class).in(Singleton.class);
		bind(FilterConditionSupportedTypeRegistry.class).toProvider(FilterConditionSupportedTypeRegistryProvider.class);
		bind(FieldAnnotationDictionaryFactory.class).toProvider(FieldAnnotationDictionaryFactoryProvider.class);
		bind(FieldValueProvider.class).to(FieldPropertyEntityProvider.class);
		bind(DataTypeAdapterFactory.class).toProvider(DataTypeAdapterFactoryProvider.class);
		bind(TrailheadIdXmlAdapter.class).in(Singleton.class);
		bind(Locale.class).toProvider(LocaleProvider.class);

		filter(OBJECTIFY_ASYNC_CACHE_FILTER_PATH).through(AsyncCacheFilter.class);
		bind(SearchService.class).to(GaeSearchService.class).in(Singleton.class);
		bindGoogleChartsDataSource();
		filterCors();
		bind(RegisteredEntities.class).to(RegisteredEntitiesObjectifyFactory.class);
		requestStaticInjection(IdentifierTranslators.class);
		bind(IdentityManager.class).toProvider(IdentityManagerProvider.class).in(ServletScopes.REQUEST);
	}

	@Provides
	protected RegisteredEntitiesObjectifyFactory registeredEntities() {
		RegisteredEntitiesObjectifyFactory factory = new RegisteredEntitiesObjectifyFactory();
		// FIXME: THere has to be a way to register these in each module
		factory.register(ActivityId.class);
		factory.register(RouteId.class);
		factory.register(TrailheadId.class);
		factory.register(RouteEventId.class);
		return factory;
	}

	@Provides
	public OptionalXmlAdapterFactory optionalXmlAdapterFactory(
			// ROUTE
			StartTrailheadIdXmlAdapter startTrailheadIdXmlAdapter,
			FinishTrailheadIdXmlAdapter finishTrailheadIdXmlAdapter,
			RouteXmlAdapter routeXmlAdapter,
			RouteXmlAdapter.ForCompletion routeXmlAdapterForCompletion,
			RouteXmlAdapter.ForEvent routeXmlAdapterForEvent,
			RoutePointsGpolylineXmlAdapter routePointsGpolylineXmlAdapter,
			// ROUTE EVENT
			RouteEventXmlAdapter routeEventXmlAdapter,
			RouteEventXmlAdapter.ForCompletion routeEventXmlAdapterForCompletion,
			RouteEventXmlAdapter.ForStatistic routeEventXmlAdapterForStatistic,
			// ACTIVITY
			ActivityFromIdXmlAdapter activityXmlAdapter, ActivitiesFromIdsXmlAdapter activitiesXmlAdapter,
			ApplicationActivityReferenceXmlAdapter applicationActivityReferenceXmlAdapter,
			ApplicationActivitiesReferenceXmlAdapter applicationActivitiesReferenceXmlAdapter,
			// APPLICATION
			ApplicationXmlAdapter applicationXmlAdapter, ActivityApplicationXmlAdapter activityApplicationXmlAdapter,
			AccountApplicationXmlAdapter accountApplicationXmlAdapter,
			// TRACKS
			TrackFormatXmlAdapterFactory trackFormatFactory) {
		final GeoCellsOptionalXmlAdapter geoCellsAdapter = new GeoCellsOptionalXmlAdapter(true);
		final TrackFormatXmlAdapter<?> gpolylineAdapter = trackFormatFactory
				.get(GeoWebApiUri.POLYLINE_ENCODED_MEDIA_TYPE);
		OptionalXmlAdapterFactory factory = OptionalXmlAdapterFactory
				.create()
				.register(GeoCells.FIELD.DOMAIN, geoCellsAdapter)
				.register(TrackSummary.FIELD.OPTION.SPATIAL_BUFFER, geoCellsAdapter)
				// TRAILHEAD
				.register(Trailhead.FIELD.OPTION.AREA, geoCellsAdapter)
				// ROUTE
				.register(Trailhead.FIELD.DOMAIN, startTrailheadIdXmlAdapter)
				.register(Trailhead.FIELD.DOMAIN, finishTrailheadIdXmlAdapter)
				.register(Route.FIELD.OPTION.START_INLINE, startTrailheadIdXmlAdapter)
				.register(Route.FIELD.OPTION.FINISH_INLINE, finishTrailheadIdXmlAdapter)
				.register(Route.FIELD.OPTION.COURSE_INLINE, activityXmlAdapter)
				.register(Route.FIELD.DOMAIN, routeXmlAdapter)
				.register(GeoWebApiUri.GPOLYLINE, routePointsGpolylineXmlAdapter, RoutePointsXmlAdapter.class)
				.register(	Route.FIELD.OPTION.TRACK_GPOLYLINE,
							routePointsGpolylineXmlAdapter,
							RoutePointsXmlAdapter.class)
				// ROUTE Completion
				.register(RouteCompletion.FIELD.OPTION.ROUTE, routeXmlAdapterForCompletion)
				.register(Route.FIELD.DOMAIN, routeXmlAdapterForCompletion)
				// ROUTE EVENT
				// when including route with route completion a circular reference occurs...use
				// specifics
				.register(RouteEvent.FIELD.DOMAIN, routeEventXmlAdapter)
				.register(RouteEvent.FIELD.DOMAIN, routeEventXmlAdapterForCompletion)
				.register(RouteEvent.FIELD.DOMAIN, routeEventXmlAdapterForStatistic)
				.register(RouteCompletion.FIELD.OPTION.ROUTE_EVENT, routeEventXmlAdapterForCompletion)
				.register(RouteEventStatistic.FIELD.OPTION.ROUTE_EVENT, routeEventXmlAdapterForStatistic)
				.register(RouteEvent.FIELD.OPTION.ROUTE, routeXmlAdapterForEvent)
				.register(Route.FIELD.DOMAIN, routeXmlAdapterForEvent)
				// APPLICATION
				.register(Application.FIELD.DOMAIN, applicationXmlAdapter)
				.register(Application.FIELD.DOMAIN, activityApplicationXmlAdapter)
				.register(Application.FIELD.DOMAIN, accountApplicationXmlAdapter)
				.register(Activity.FIELD.OPTIONS.APPLICATION, activityApplicationXmlAdapter)
				.register(Account.FIELD.OPTIONS.APPLICATION, accountApplicationXmlAdapter)

				// ACTIVITY
				.register(Activity.FIELD.DOMAIN, activityXmlAdapter)
				.register(Activity.FIELD.DOMAIN, activitiesXmlAdapter)
				.register(OptionalXmlAdapterFactory.ALWAYS, applicationActivityReferenceXmlAdapter)
				.register(OptionalXmlAdapterFactory.ALWAYS, applicationActivitiesReferenceXmlAdapter)
				// TRACKS
				.register(TrackSummary.FIELD.OPTION.GOOGLE_POLYLINE, gpolylineAdapter, TrackFormatXmlAdapter.class)

				.build();
		return factory;
	}

	/**
	 * Cross site Origin Policy Filter
	 * 
	 * @See http://software.dzhuvinov.com/cors-filter-installation.html
	 */
	private void filterCors() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("cors.supportedMethods", "GET, POST, HEAD, PUT, DELETE, OPTIONS");
		bind(CORSFilter.class).in(Singleton.class);
		filter("/*").through(CORSFilter.class, params);
	}

	/**
	 * Google Charts DataSource Servlet
	 */
	private void bindGoogleChartsDataSource() {
		bind(RouteTrackDataSourceServlet.class).in(Singleton.class);
		serve(RouteTrackDataSourceServlet.PATH).with(RouteTrackDataSourceServlet.class);
	}

}
