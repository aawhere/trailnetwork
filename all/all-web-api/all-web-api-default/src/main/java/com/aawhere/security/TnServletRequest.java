/**
 *
 */
package com.aawhere.security;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

import com.google.api.client.util.Lists;

/**
 * @author brian
 */
public class TnServletRequest
		extends HttpServletRequestWrapper {

	/**
	 * Used to construct all instances of TnServletRequest.
	 */
	public static class Builder
			extends ObjectBuilder<TnServletRequest> {

		public Builder userId(String userId) {
			building.userId = userId;
			return this;
		}

		public Builder roles(List<String> roles) {
			building.roles.addAll(roles);
			return this;
		}

		public Builder role(String role) {
			building.roles.add(role);
			return this;
		}

		public Builder authScheme(AuthScheme scheme) {
			building.authScheme = scheme;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("request", building.request);
		}

		public Builder(HttpServletRequest request) {
			super(new TnServletRequest(request));
		}

	}// end Builder

	/** Use {@link Builder} to construct TnServletRequest */
	private TnServletRequest(HttpServletRequest request) {
		super(request);
		this.request = request;
	}

	public static Builder create(HttpServletRequest request) {
		return new Builder(request);
	}

	private String userId;
	private final List<String> roles = Lists.newArrayList();
	private final HttpServletRequest request;
	private AuthScheme authScheme;

	@Override
	public boolean isUserInRole(String role) {
		return this.roles.contains(role) || this.request.isUserInRole(role);
	}

	@Override
	public Principal getUserPrincipal() {
		if (this.userId == null) {
			return null;
		}

		// make an anonymous implementation to just return our user
		return new TnPrincipal.Builder().userId(userId).build();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServletRequestWrapper#getAuthType()
	 */
	@Override
	public String getAuthType() {
		return this.authScheme.name();
	}

}