package com.aawhere.web.api.geo;

import static com.aawhere.web.api.geo.GeoWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.KmlType;

import com.aawhere.activity.ActivityProcessService;
import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.geo.GeoCellProcessService;
import com.aawhere.geo.GeoCellSummaries;
import com.aawhere.geo.GeoCellSummaryField;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxXmlAdapter;
import com.aawhere.measure.BoundingBoxXmlAdapter.BoundingBoxXml;
import com.aawhere.measure.BoundingBoxes;
import com.aawhere.measure.GeoExportService;
import com.aawhere.measure.GeoKmlExportService;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.Filter;
import com.aawhere.persist.PersistMessage;
import com.aawhere.swagger.DocSupport;
import com.aawhere.web.api.ApiDocumentationSupport;

import com.trailnetwork.web.api.activity.ActivityPublicWebApi;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * Provides utilities to assist clients with Geographic functions and data type conversions.
 * 
 * Public endpoints are declared here, but published at {@link ActivityPublicWebApi} to allow for
 * administrative extension since JAX-RS extension does not allow duplicate path declarations.
 * 
 * @author aroller
 * 
 */
abstract public class GeoWebApi {

	protected static final Integer MAX_GEOCELL_RESULT_SIZE = 32;
	protected final ActivityProcessService activityProcessService;
	protected final GeoCellProcessService geoCellProcessService;

	/**
	 *
	 */
	@Inject
	public GeoWebApi(ActivityProcessService activityProcessService, GeoCellProcessService geoCellProcessService) {
		this.activityProcessService = activityProcessService;
		this.geoCellProcessService = geoCellProcessService;
	}

	/**
	 * Provides a GeoCellBoundingBox when given a bounds. The bounds may be a sw|ne coordinate
	 * string or space delimited geocells.
	 * 
	 * @param boundingBox
	 * @return
	 * @throws Exception
	 */
	@Path(SLASH + BY_BOUNDS_PATH)
	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Given any bounds this will return the adjusted bounds.", response = BoundingBoxXml.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public BoundingBoxXml getGeoCellBoundingBoxFromBoundingBox(@PathParam(BOUNDS) GeoCellBoundingBox boundingBox)
			throws Exception {
		return marshallBoundingBox(normalizeGeoCellBoundingBox(boundingBox));
	}

	/**
	 * Given a standard bounds this returns bounding boxes based on appropriate resolution of
	 * {@link GeoCell}. as a "best fit" within the bounding box. If an exact representation is
	 * required then calling an endpoint that returns a single box is required.
	 * 
	 * @param boundingBox
	 * @return
	 * @throws Exception
	 */
	@Path(SLASH + BOXES_BY_BOUNDS_PATH)
	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Multiple GeoCell boxes representing the area provided.",
			response = BoundingBoxes.class,
			notes = "Given a standard bounds this returns bounding boxes based on appropriate resolution of  {@link GeoCell}. as a \"best fit\" within the bounding box. If an exact representation is required then calling an endpoint that returns a single box is required.",
			position = ApiDocumentationSupport.Position.PUBLIC)
	public
			BoundingBoxes getGeoCellBoundingBoxesFromBoundingBox(@PathParam(BOUNDS) GeoCellBoundingBox boundingBox)
					throws Exception {
		return GeoExportService.create().build().geoCellBoundingBoxes(boundingBox);
	}

	/**
	 * An endpoint specializing in receiving Google Earth bounding box requests in the parameter.
	 * This is different since the bounds is not in the path. If no parameter is provided the World
	 * bounds are used.
	 * 
	 * @see #getGeoCellBoundingBoxesFromBoundingBox(GeoCellBoundingBox)
	 * @param bbox
	 *            the Google Earth formatted BBOX=minX,minY,maxX,maxY
	 * @return
	 * @throws Exception
	 */
	@Path(SLASH + BOXES)
	@GET
	@Produces(ActivityWebApiUri.KML_CONTENT_TYPE)
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public JAXBElement<KmlType> getGeoCellBoundingBoxesKmlFromGoogleEarthBoundingBox(@QueryParam(BBOX) String bbox)
			throws Exception {

		return GeoKmlExportService.create().build().geoCellBoundingBoxes(null, bbox).getElement();
	}

	/**
	 * @see #getGeoCellBoundingBoxesFromBoundingBox(GeoCellBoundingBox)
	 * @param boundingBox
	 * @param bbox
	 * @return
	 * @throws Exception
	 */
	@Path(SLASH + BOXES_BY_BOUNDS_PATH)
	@GET
	@Produces(KML_CONTENT_TYPE)
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public JAXBElement<KmlType> getGeoCellBoundingBoxesKmlFromBoundingBox(
			@PathParam(BOUNDS) GeoCellBoundingBox boundingBox) throws Exception {

		return GeoKmlExportService.create().build().geoCellBoundingBoxes(boundingBox).getElement();
	}

	/**
	 * Provides GeoCells that approximately match the bounding box.
	 * 
	 * @param boundingBox
	 * @return
	 */
	@Path(SLASH + CELLS_BY_BOUNDS_PATH)
	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Converts a bounding box into related GeoCells.", response = GeoCells.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public GeoCells getGeoCellsFromBoundingBox(@PathParam(BOUNDS) GeoCellBoundingBox boundingBox) {
		return normalizeGeoCellBoundingBox(boundingBox).getGeoCells();
	}

	/**
	 * @deprecated use {@link #boundingBoxFromGeoCells(GeoCells)}
	 * @param geoCells
	 * @return
	 * @throws Exception
	 */
	@Path(SLASH + BOUNDS_BY_CELLS_PATH)
	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	@Deprecated
	public BoundingBoxXml getGeoBoundingBoxFromGeoCells(@PathParam(CELLS) GeoCells geoCells) throws Exception {
		GeoCellBoundingBox box = new GeoCellBoundingBox.Builder().setGeoCells(geoCells).build();
		return marshallBoundingBox(box);
	}

	/**
	 * Given geocells this will return the corresponding bounding box for those geocells.
	 * 
	 * 
	 * @param geoCells
	 * @return
	 * @throws Exception
	 */
	@Path(SLASH + CELLS_CELL_PARAM_BOXES_PATH)
	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Bounding Box containing all GeoCells given.", response = BoundingBox.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public BoundingBoxXml boundingBoxFromGeoCells(@PathParam(CELLS) GeoCells geoCells) throws Exception {
		GeoCellBoundingBox box = new GeoCellBoundingBox.Builder().setGeoCells(geoCells).build();
		return marshallBoundingBox(box);
	}

	@GET
	@Path(SLASH + CELLS_CELLS_PARAM_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Summaries for the Cells given.", response = GeoCellSummaries.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	@ApiImplicitParams({ @ApiImplicitParam(value = PersistMessage.Doc.EXPAND_DESCRIPTION, name = Filter.FIELD.EXPAND,
			allowMultiple = true, allowableValues = GeoCellSummaryField.Absolute.ACTIVITY_START_COUNT,
			dataType = DocSupport.DataType.STRING, paramType = DocSupport.ParamType.QUERY) })
	public GeoCellSummaries cellsGet(@ApiParam("The area of interest.") @PathParam(CELLS) GeoCells geoCells,
			@ApiParam("Explode cells to higher resolution") @QueryParam(RESOLUTION) String resolution) {
		return geoCellProcessService.summaries(geoCells, Resolution.fromString(resolution));
	}

	// TODO: move this to a service.
	private BoundingBoxXml marshallBoundingBox(BoundingBox box) throws Exception {
		return (new BoundingBoxXmlAdapter()).marshal(box);
	}

	// TODO: move this to a service.
	private GeoCellBoundingBox normalizeGeoCellBoundingBox(GeoCellBoundingBox box) {
		return new GeoCellBoundingBox.Builder().setOther(box).maxGeoCells(MAX_GEOCELL_RESULT_SIZE).build();
	}

}
