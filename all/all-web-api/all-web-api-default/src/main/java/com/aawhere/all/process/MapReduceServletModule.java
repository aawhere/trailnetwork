package com.aawhere.all.process;

import com.google.appengine.tools.mapreduce.MapReduceServlet;
import com.google.inject.servlet.ServletModule;

public class MapReduceServletModule
		extends ServletModule {

	@Override
	protected void configureServlets() {
		super.configureServlets();
		serve("/mapreduce/*").with(new MapReduceServlet());
	}
}
