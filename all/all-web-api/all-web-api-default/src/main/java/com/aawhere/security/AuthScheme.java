/**
 *
 */
package com.aawhere.security;

/**
 * @author brian
 * 
 */
public enum AuthScheme {
	TN_LIVE, GOOGLE_AUTH;
}
