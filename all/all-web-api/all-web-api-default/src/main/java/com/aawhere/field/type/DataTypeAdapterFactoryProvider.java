/**
 *
 */
package com.aawhere.field.type;

import com.aawhere.measure.GeoCoordinateDataTypeGeoAdapter;
import com.aawhere.measure.QuantityDataTypeNumberAdapter;
import com.aawhere.measure.geocell.GeoCellDataTypeTextAdapter;

import com.google.inject.Provider;

/**
 * @author brian
 * 
 */
public class DataTypeAdapterFactoryProvider
		implements Provider<DataTypeAdapterFactory> {

	@Override
	public DataTypeAdapterFactory get() {
		DataTypeAdapterFactory factory = new DataTypeAdapterFactory();
		factory.register(new QuantityDataTypeNumberAdapter(), FieldDataType.NUMBER);
		factory.register(new GeoCoordinateDataTypeGeoAdapter(), FieldDataType.GEO);
		factory.register(new GeoCellDataTypeTextAdapter(), FieldDataType.TEXT);
		return factory;
	}
}
