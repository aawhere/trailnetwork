/**
 *
 */
package com.aawhere.persist;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.persist.FilterConditionSupportedTypeRegistry;
import com.aawhere.persist.FilterOperator;

import com.google.inject.Provider;

/**
 * Google Guice Provider for {@link FilterConditionSupportedTypeRegistryProvider}, which is a
 * Singleton in the web application.
 * 
 * @author Brian Chapman
 * 
 */
public class FilterConditionSupportedTypeRegistryProvider
		implements Provider<FilterConditionSupportedTypeRegistry> {

	@Override
	public FilterConditionSupportedTypeRegistry get() {
		FilterConditionSupportedTypeRegistry factory = new FilterConditionSupportedTypeRegistry.Builder().build();
		factory.register(GeoCoordinate.class, FilterOperator.IN, BoundingBox.class);
		return factory;
	}
}
