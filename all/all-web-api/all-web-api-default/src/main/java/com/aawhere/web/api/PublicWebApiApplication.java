package com.aawhere.web.api;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.security.AppIdentityModule;
import com.aawhere.security.LoggingContainerRequestFilter;

import com.google.inject.Module;
import com.sun.jersey.api.core.ResourceConfig;

/**
 * Services the public requests providing limited access to those methods appropriate for public
 * consumption along with the associated security.
 * 
 * @see AppIdentityModule
 * @See {@link PublicAppGuiceServletContextListener}
 * @author aroller
 * 
 */
public class PublicWebApiApplication
		extends WebApiApplication {

	public PublicWebApiApplication() {
		// this package separates the public endpoints so they can be avoided from automatic
		// detection when included in other modules so the API classes may be overriden without
		// conflicting enpoint destinations
		super(ArrayUtils.addAll(packages(), "com.trailnetwork"));
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void init() {

		super.init();
		if (LoggingContainerRequestFilter.isLoggable()) {
			addPropertyIfNull(ResourceConfig.PROPERTY_CONTAINER_REQUEST_FILTERS, new LoggingContainerRequestFilter());
		}
		// List filters = this.getContainerRequestFilters();
		// filters.add(PutSecurityConstraintFilter.class);
	}

	/** Provides all common modules for all apps. */
	public static Module[] getModules() {
		return ArrayUtils.add(getModules(PublicWebApiApplication.class), new AppIdentityModule());
	}
}
