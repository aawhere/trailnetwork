/**
 *
 */
package com.aawhere.web.api;

import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.aawhere.personalize.Personalizer;

/**
 * Our interception of the JAXB processing done by Jersey. This will use {@link Personalizer}s to
 * localize and i18n according to the user preferences from their HTTP header or their user account.
 * 
 * @author Aaron Roller
 * 
 */
@Provider
@Produces({ MediaType.APPLICATION_XML })
public class JaxbXmlMarshallerContextResolver
		extends BaseJaxbMarshallerContextResolver {

	public JaxbXmlMarshallerContextResolver(@Context UriInfo uriInfo) {
		super(uriInfo);
	}

	@Override
	protected Marshaller getMarshaller(Class<?> type) {
		Marshaller marshaller;
		try {
			JAXBContext jc = WebApiApplication.getJaxBContext();
			marshaller = jc.createMarshaller();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
		return marshaller;
	}

}
