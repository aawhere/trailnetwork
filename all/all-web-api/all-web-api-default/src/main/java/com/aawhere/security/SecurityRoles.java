/**
 *
 */
package com.aawhere.security;

import com.aawhere.identity.IdentityRole;

/**
 * Security Roles for servelet security settings and Jersey @RolesAllowed settings.
 * 
 * @author Brian Chapman
 * @deprecated use {@link IdentityRole}
 */
@Deprecated
public class SecurityRoles {

	/**
	 * Appengine automatically assigns this role to logged in admins of appspot.com. On the dev
	 * server you can use localhost:8080/_ah/login to choose between admin and standard user.
	 * 
	 * @deprecated use {@link IdentityRole.Name#ADMIN}
	 */

	@Deprecated
	public static final String ADMIN = IdentityRole.Name.ADMIN;

	/**
	 * Authorized API "trusted" apps get access to parts of the API that normal authenticated API
	 * users would not. The main user of this role is the TN Web server. *
	 * 
	 * @deprecated use {@link IdentityRole.Name#TRUSTED_APP}
	 */
	@Deprecated
	public static final String TRUSTED_APP = IdentityRole.Name.TRUSTED_APP;
	/**
	 * * @deprecated use {@link IdentityRole.Name#USER}
	 */
	@Deprecated
	public static final String USER = IdentityRole.Name.USER;

}
