/**
 *
 */
package com.aawhere.field.annotation;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.web.api.WebApiApplication;

import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Provides and configures a dictionary factory when requested.
 * 
 * @author Brian Chapman
 * 
 */
public class FieldAnnotationDictionaryFactoryProvider
		implements Provider<FieldAnnotationDictionaryFactory> {

	FieldDictionaryFactory dictionaryFactory;

	/**
	 * Setup Field Dictionary
	 */
	@Inject
	FieldAnnotationDictionaryFactoryProvider(FieldDictionaryFactory dictionaryFactory) {
		this.dictionaryFactory = dictionaryFactory;
	}

	@Override
	public FieldAnnotationDictionaryFactory get() {
		FieldAnnotationDictionaryFactory annoationDictionaryFactory = new FieldAnnotationDictionaryFactory(
				dictionaryFactory);
		WebApiApplication.registerFieldAnnotations(annoationDictionaryFactory);
		return annoationDictionaryFactory;
	}
}
