/**
 *
 */
package com.aawhere.security;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.aawhere.log.LoggerFactory;
import com.aawhere.web.api.security.InvalidAuthenticationException;

import com.google.appengine.api.oauth.OAuthRequestException;
import com.google.appengine.api.oauth.OAuthService;
import com.google.appengine.api.oauth.OAuthServiceFactory;
import com.google.appengine.api.users.User;
import com.google.common.net.HttpHeaders;

/**
 * Jersey filter to decode the request's oauth token and set the Principal name to the userId if
 * available. If the token is not present, passthrough.
 * 
 * If the token is invalid, an {@link InvalidAuthenticationException} is thrown.
 * 
 * @author Brian Chapman
 * 
 */
public class OAuthAuthenticationFilter
		implements Filter {

	private static Logger logger = LoggerFactory.getLogger(OAuthAuthenticationFilter.class);

	private String scope = "https://www.googleapis.com/auth/userinfo.email";
	private final String EXPECTED_CLIENT_ID = "288962579085.apps.googleusercontent.com";

	private void validateRequest(OAuthService oauth) throws OAuthRequestException {
		// String clientId = oauth.getClientId(scope);
		// logger.info("ClientId: " + clientId);
		// if (clientId != EXPECTED_CLIENT_ID) {
		// throw new InvalidAuthenticationException();
		// }
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {

		logger.info("Executing OAuthAuthenticationFilter");
		HttpServletRequest httpRequest = (HttpServletRequest) request;

		// Lack of header indicates that this request assumes the user or roles already defined.
		// Note that this currently overrides any previous authentication, which could include the
		// appId trusted authentication or container provided authentication.
		if (httpRequest.getHeader(HttpHeaders.AUTHORIZATION) != null) {

			// TODO: WW-757 can or should this factory call be moved to the class level?
			OAuthService oauth = OAuthServiceFactory.getOAuthService();

			try {
				validateRequest(oauth);
				final User user = oauth.getCurrentUser(scope);
				final String userId = user.getUserId();

				if (userId != null) {
					httpRequest = TnServletRequest.create(httpRequest).userId(userId).role(SecurityRoles.USER).build();
				}

			} catch (OAuthRequestException e) {
				throw new InvalidAuthenticationException(e);
			}
		}

		chain.doFilter(httpRequest, response);

	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
