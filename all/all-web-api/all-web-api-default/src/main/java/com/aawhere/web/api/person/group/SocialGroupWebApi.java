/**
 *
 */
package com.aawhere.web.api.person.group;

import static com.aawhere.person.group.SocialGroupWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.app.account.Account;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.person.Person;
import com.aawhere.person.Persons;
import com.aawhere.person.group.OptionsField;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.person.group.SocialGroupMembership;
import com.aawhere.person.group.SocialGroupMembershipId;
import com.aawhere.person.group.SocialGroupMembershipMessage;
import com.aawhere.person.group.SocialGroupMembershipService;
import com.aawhere.person.group.SocialGroupMemberships;
import com.aawhere.person.group.SocialGroupRelator;
import com.aawhere.person.group.SocialGroupService;
import com.aawhere.person.group.SocialGroups;
import com.aawhere.swagger.DocSupport;
import com.aawhere.web.api.app.account.AccountWebApi;
import com.aawhere.web.api.person.PersonWebApi;

import com.google.inject.Inject;
import com.sun.jersey.api.core.InjectParam;
import com.trailnetwork.web.api.activity.ActivityPublicWebApi;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

/**
 * Provides access to {@link SocialGroups} and their {@link SocialGroupMemberships} when accessing
 * groups directly.
 * 
 * If accessing groups by {@link Person} use {@link PersonWebApi}. If accessing groups by
 * {@link Account} use {@link AccountWebApi}.
 * 
 * Public endpoints are declared here, but published at {@link ActivityPublicWebApi} to allow for
 * administrative extension since JAX-RS extension does not allow duplicate path declarations.
 * 
 * @author aroller
 * 
 */
abstract public class SocialGroupWebApi {

	protected final SocialGroupService groupService;
	protected final SocialGroupMembershipService membershipService;

	/**
	 *
	 */
	@Inject
	public SocialGroupWebApi(SocialGroupService groupService, SocialGroupMembershipService socialGroupService) {
		this.groupService = groupService;
		this.membershipService = socialGroupService;
	}

	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Find SocialGroups using the standard filter.", response = SocialGroups.class)
	public SocialGroups groups(@InjectParam Filter filter) {
		return groupService.socialGroups(filter);
	}

	@GET
	@Path(SLASH + SOCIAL_GROUP_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Retrieve a SocialGroup by it's SocialGroupId.", response = SocialGroup.class)
	public SocialGroup group(@PathParam(SOCIAL_GROUP_ID) SocialGroupId id) throws EntityNotFoundException {
		return groupService.socialGroup(id);
	}

	@GET
	@Path(SLASH + SOCIAL_GROUP_ID_PERSONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The Persons with SocialGroupMemberships in a SocialGroup.", response = SocialGroup.class)
	public Persons members(@PathParam(SOCIAL_GROUP_ID) SocialGroupId id, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return membershipService.members(id, filter);
	}

	/* ==================== Memberships =============== */

	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@Path(SLASH + MEMBERSHIPS)
	@ApiOperation(value = "Access to SocialGroupMemberships using Filters.", response = SocialGroupMemberships.class)
	// TN-779 reference the constant fields generated
			@ApiImplicitParams({ @ApiImplicitParam(name = "conditions",
					defaultValue = "socialGroup-context = {Identifer}", paramType = DocSupport.ParamType.QUERY,
					dataType = "Identifier") })
			public SocialGroupMemberships memberships(@ApiParam(required = false) @InjectParam Filter filter) {
		return this.membershipService.memberships(filter);
	}

	@GET
	@Path(SLASH + MEMBERSHIPS_MEMBERSHIP_ID_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The SocialGroupMembership matching it's SocialGroupMembershipId.",
			response = SocialGroupMembership.class)
	@ApiResponses({ @ApiResponse(message = EntityNotFoundException.Doc, code = EntityNotFoundException.CODE) })
	public
			SocialGroupMembership
			membership(
					@ApiParam(value = "The RouteId-PersonId combination.") @PathParam(MEMBERSHIP_ID) SocialGroupMembershipId socialGroupMembershipId)
					throws EntityNotFoundException {
		return membershipService.membership(socialGroupMembershipId);
	}

	@GET
	@Path(SLASH + SOCIAL_GROUP_ID_MEMBERSHIPS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Filter the Memberships for a SocialGroup.", response = SocialGroupMemberships.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = Filter.FIELD.EXPAND,
			allowableValues = SocialGroupMembershipMessage.Doc.EXAPAND_ALLOWABLE, allowMultiple = true,
			paramType = DocSupport.ParamType.QUERY, dataType = DocSupport.DataType.STRING) })
	public SocialGroupMemberships memberships(@PathParam(SOCIAL_GROUP_ID) SocialGroupId id, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return this.membershipService.memberships(id, filter);
	}

	@GET
	@Path(SLASH + SOCIAL_GROUP_ID_RELATORS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Finds SocialGroups in common through participation of common Persons.",
			response = SocialGroupRelator.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = OptionsField.IN_COMMON_MINIMUM, allowableValues = "range[1, 100]",
			paramType = DocSupport.ParamType.QUERY, dataType = DocSupport.DataType.INTEGER) })
	@ApiResponses({ @ApiResponse(message = EntityNotFoundException.Doc, code = EntityNotFoundException.CODE) })
	public SocialGroupRelator relatorGet(@PathParam(SOCIAL_GROUP_ID) SocialGroupId socialGroupId,
			@InjectParam SocialGroupRelator.Options options) throws EntityNotFoundException {
		return this.membershipService.groupsInCommon(socialGroupId, options);
	}
}
