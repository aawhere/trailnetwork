package com.aawhere.security;

import com.google.inject.servlet.ServletModule;

public class AppIdentityModule
		extends ServletModule {

	@Override
	protected void configureServlets() {

		super.configureServlets();
		filter("/*").through(AppIdentityAuthenticationFilter.class);
	}

}
