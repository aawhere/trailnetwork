/**
 *
 */
package com.aawhere.security;

import java.security.Principal;

import com.aawhere.lang.ObjectBuilder;

/**
 * @author brian
 * 
 */
public class TnPrincipal
		implements Principal {

	/**
	 * Used to construct all instances of TnPrincipal.
	 */
	public static class Builder
			extends ObjectBuilder<TnPrincipal> {

		public Builder userId(String userId) {
			building.userId = userId;
			return this;
		}

		public Builder() {
			super(new TnPrincipal());
		}

	}// end Builder

	private String userId;

	/** Use {@link Builder} to construct TnPrincipal */
	private TnPrincipal() {
	}

	public static Builder create() {
		return new Builder();
	}

	@Override
	public String getName() {
		return userId;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Name: " + getName();
	}
};
