package com.aawhere.web.api;

import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.aawhere.personalize.Personalizer;

import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;

/**
 * Our interception of the JAXB processing done by Jersey. This will use {@link Personalizer}s to
 * localize and i18n according to the user preferences from their HTTP header or their user account.
 * 
 * @author Aaron Roller
 * 
 */
@Provider
@Produces({ MediaType.APPLICATION_JSON })
public class JaxbJsonMarshallerContextResolver
		extends BaseJaxbMarshallerContextResolver {

	public JaxbJsonMarshallerContextResolver(@Context UriInfo uriInfo) {
		super(uriInfo);
	}

	@Override
	protected Marshaller getMarshaller(Class<?> type) {
		JSONMarshaller marshaller;
		try {
			JSONJAXBContext jc = WebApiApplication.getJsonJaxBContext();
			marshaller = jc.createJSONMarshaller();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
		return (Marshaller) marshaller;
	}
}
