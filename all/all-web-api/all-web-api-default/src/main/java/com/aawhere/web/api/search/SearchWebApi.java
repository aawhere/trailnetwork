package com.aawhere.web.api.search;

import static com.aawhere.ws.rs.SystemWebApiUri.*;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.persist.Filter;
import com.aawhere.persist.PersistMessage;
import com.aawhere.person.Person;
import com.aawhere.person.PersonProcessService;
import com.aawhere.route.RouteService;
import com.aawhere.search.FieldNotSearchableException;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchService;
import com.aawhere.search.UnknownIndexException;
import com.aawhere.swagger.DocSupport;
import com.aawhere.swagger.DocSupport.Allowable;
import com.aawhere.trailhead.TrailheadService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.google.inject.Inject;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Provides a web api interface for search related requests
 * 
 * @author Brian Chapman
 * 
 */
abstract public class SearchWebApi {

	final protected SearchService searchService;
	final protected RouteService routeService;
	final protected TrailheadService trailheadService;
	final protected PersonProcessService personProcessService;

	// FIXME: The map features must come from a constant somewhere.
	protected final String INDEXES_LIST = Person.FIELD.DOMAIN + Allowable.NEXT //
			+ "route-ALL" + Allowable.NEXT //
			+ "route-ACTIVITY_TYPE" + DocSupport.Allowable.NEXT //
			+ "route-PERSON" + Allowable.NEXT//
			+ "route-PERSON_ACTIVITY_TYPE" + DocSupport.Allowable.NEXT //
			+ "trailhead-ALL" + Allowable.NEXT //
			+ "trailhead-ACTIVITY_TYPE" + Allowable.NEXT //
			+ "trailhead-ACTIVITY_TYPE_DISTANCE" + DocSupport.Allowable.NEXT //
			+ "trailhead-PERSON" + Allowable.NEXT //
			+ "trailhead-PERSON_ACTIVITY_TYPE" + Allowable.NEXT //
			+ "trailhead-PERSON_ACTIVITY_TYPE_DISTANCE";

	@Inject
	public SearchWebApi(SearchService searchService, RouteService routeService, TrailheadService trailheadService,
			PersonProcessService personProcessService) {
		this.searchService = searchService;
		this.routeService = routeService;
		this.trailheadService = trailheadService;
		this.personProcessService = personProcessService;
	}

	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Search for information in the TrailNetwork.", response = SearchDocuments.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	@ApiImplicitParams({
			@ApiImplicitParam(value = "Search for keywords.", name = Filter.FIELD.QUERY, allowMultiple = false,
					dataType = DocSupport.DataType.STRING, paramType = DocSupport.ParamType.QUERY),
			@ApiImplicitParam(name = Filter.FIELD.BOUNDS, value = PersistMessage.Doc.FILTER_BOUNDS_DESCRIPTION,
					paramType = DocSupport.ParamType.QUERY, dataType = DocSupport.DataType.STRING),
			@ApiImplicitParam(name = Filter.FIELD.SEARCH_INDEXES, allowMultiple = false,
					allowableValues = INDEXES_LIST, required = true, paramType = DocSupport.ParamType.QUERY,
					dataType = DocSupport.DataType.STRING_ARRAY) })
	public SearchDocuments search(@InjectParam Filter filter) throws FieldNotSearchableException, UnknownIndexException {
		return this.searchService.searchDocuments(filter);
	}

}
