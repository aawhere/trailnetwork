package com.aawhere.all.process;

import com.google.appengine.tools.pipeline.impl.servlets.PipelineServlet;
import com.google.inject.servlet.ServletModule;

/**
 * The pipeline servlet is used to report status of processing data.
 * 
 * @author aroller
 * 
 */
public class PipelineServletModule
		extends ServletModule {

	@Override
	protected void configureServlets() {
		super.configureServlets();
		serve("/_ah/pipeline/*").with(new PipelineServlet());
	}
}
