/**
 *
 */
package com.aawhere.web.api.trailhead;

import static com.aawhere.route.RouteWebApiUri.*;
import static com.aawhere.trailhead.TrailheadWebApiUri.*;
import static com.aawhere.web.api.geo.GeoWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.KmlType;

import com.aawhere.field.FieldDictionary;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilteredResponse;
import com.aawhere.route.RouteExportService;
import com.aawhere.route.RouteService;
import com.aawhere.route.Routes;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadExportService;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadProcessService;
import com.aawhere.trailhead.TrailheadService;
import com.aawhere.trailhead.Trailheads;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.google.inject.Inject;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * 
 * @author Brian Chapman
 * 
 */
abstract public class TrailheadWebApi {

	final protected TrailheadService trailheadService;
	final protected RouteService routeService;
	final protected TrailheadExportService exportService;
	final protected RouteExportService routeExportService;
	final protected TrailheadProcessService trailheadProcessService;

	private FieldAnnotationDictionaryFactory dictionaryFactory;

	@Inject
	public TrailheadWebApi(TrailheadService trailheadService, RouteService routeService,
			TrailheadExportService exportService, RouteExportService routeExportService,
			TrailheadProcessService trailheadProcessService, FieldAnnotationDictionaryFactory dictionaryFactory) {
		this.trailheadService = trailheadService;
		this.routeService = routeService;
		this.exportService = exportService;
		this.routeExportService = routeExportService;
		this.dictionaryFactory = dictionaryFactory;
		this.dictionaryFactory.getDictionary(Trailhead.class);
		this.trailheadProcessService = trailheadProcessService;

	}

	/**
	 * Uses the standard filter.
	 * 
	 * @see Filter#getBounds()
	 * @param filter
	 * @return
	 */
	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Trailheads matching the Filter",
			notes = "TODO:explain the Activity Type and Person filters", tags = SearchWebApiUri.SEARCH,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public Trailheads trailheadsGet(@InjectParam Filter filter) {
		return trailheadService.trailheads(filter);
	}

	@GET
	@Produces(KML_CONTENT_TYPE)
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public JAXBElement<KmlType> trailheadsGetKml(@InjectParam Filter filter, @QueryParam(BBOX) String bbox) {
		return exportService.trailheads(filter, bbox);
	}

	/**
	 * Provides an {@link Trailhead} given the trailheadId of this API. It may return a trailhead of
	 * a different id if the id given is stale and has since been replaced with another trailhead.
	 * It may also indicate that there is not a suitable trailhead in the vicinity of that which was
	 * given.
	 * 
	 * 
	 * @param trailheadId
	 *            the id of the trailhead for this system.
	 * @return the Trailhead identified by the trailheadId
	 * @throws EntityNotFoundException
	 *             when no trailhead is found in the vicinity identified by the trailhead id.
	 */
	@GET
	@Path(SLASH + TRAILHEAD_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "A Trailhead by ID", tags = SearchWebApiUri.SEARCH,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public Trailhead trailhead(@PathParam(TRAILHEAD_ID) TrailheadId trailheadId) throws EntityNotFoundException {
		return trailheadService.nearest(trailheadId);
	}

	// ------------------------------------------------------------------------
	// ----- ROUTES -------
	// ------------------------------------------------------------------------
	@GET
	@Path(SLASH + TRAILHEAD_ID_ROUTES)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Filtered Routes starting from the trailhead",
			position = ApiDocumentationSupport.Position.PUBLIC)
	public Routes routesForTrailheadGet(@PathParam(TRAILHEAD_ID) TrailheadId trailheadId, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return routeService.routes(trailheadId, filter);
	}

	/**
	 * @deprecated alternates are better as a "main group" query param
	 * @param trailheadId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Deprecated
	@GET
	@Path(SLASH + TRAILHEAD_ID_ROUTES_ALTERNATES)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public FilteredResponse<Routes> routeGroupsForTrailheadGet(@PathParam(TRAILHEAD_ID) TrailheadId trailheadId,
			@InjectParam Filter filter) throws EntityNotFoundException {
		return routeService.routesGroupedByMain(trailheadId, filter);
	}

	/**
	 * @deprecated mains is better as a query param
	 * @param trailheadId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Deprecated
	@GET
	@Path(SLASH + TRAILHEAD_ID_ROUTES_MAINS)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public Routes
			routeMainsForTrailheadGet(@PathParam(TRAILHEAD_ID) TrailheadId trailheadId, @InjectParam Filter filter)
					throws EntityNotFoundException {
		return routeService.mains(trailheadId, filter);
	}

	/**
	 * @deprecated bounds is in {@link Filter#getBounds()}
	 * @param bounds
	 * @param filter
	 * @return
	 */
	@Deprecated
	@GET
	@Path(SLASH + ROUTES)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public Routes routesFilteredByTrailhead(@QueryParam(BOUNDS) GeoCellBoundingBox bounds, @InjectParam Filter filter) {
		return routeService.routes(filter, bounds);

	}

	/**
	 * @deprecated bounds is now in filter, main is better as a query param
	 * @param bounds
	 * @param filter
	 * @return
	 */
	@Deprecated
	@GET
	@Path(SLASH + ROUTES_MAINS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Filtered Routes ")
	public Routes routeMainsFilteredByTrailhead(@QueryParam(BOUNDS) GeoCellBoundingBox bounds,
			@InjectParam Filter filter) {
		return routeService.mains(filter, bounds);

	}

	@GET
	@Path(SLASH + DICTIONARY)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public FieldDictionary getDictionary() {
		return dictionaryFactory.getDictionary(Trailhead.class);
	}

}