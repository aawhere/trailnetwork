/**
 *
 */
package com.aawhere.personalize;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.joda.time.xml.DateTimeXmlAdapter;
import com.aawhere.joda.time.xml.DurationXmlAdapter;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.xml.CountFromIntegerXmlAdapter;
import com.aawhere.measure.xml.QuantityXmlAdapter;
import com.aawhere.personalize.xml.PersonalizedXmlAdapter;
import com.aawhere.route.RouteDescriptionXmlAdapter;
import com.aawhere.route.RouteNameXmlAdapter;
import com.aawhere.trailhead.TrailheadDescriptionXmlAdapter;
import com.aawhere.util.rb.CompleteMessagePersonalizer.CompleteMessagePersonalizedXmlAdapter;
import com.aawhere.util.rb.CompleteMessageXmlAdapter;
import com.aawhere.util.rb.MessagePersonalizedXmlAdapter;
import com.google.common.collect.Maps;
import com.google.inject.Injector;

/**
 * Factory for creating Personalized Adapters based on the current request. Many will extend
 * {@link PersonalizedXmlAdapter}, however, those that wish to replace a non-personalizing xml
 * adapter must extend that other adapter. In that case they will use the
 * {@link PersonalizedXmlAdapter} as a delegate to marshal or perhaps just the {@link Personalizer}
 * itself.
 * 
 * @author Brian Chapman
 * 
 */
public class PersonalizedAdapterFactory {

	/**
	 * Map containing the personalized adapter types. The key is always the personalized adapter
	 * being registered. The value is often the same, use {@link #add(Class)} to achieve this, but
	 * if you wish to replace an existing adapter with this personalized version then use put and
	 * the value is the targeted replacement.
	 * 
	 */
	private static Map<Class<? extends XmlAdapter<?, ?>>, Class<? extends XmlAdapter<?, ?>>> adapters = new HashMap<>();

	private static void add(Class<? extends PersonalizedXmlAdapter<?, ?>> adapter) {
		adapters.put(adapter, adapter);
	}

	{
		add(QuantityXmlAdapter.class);
		add(DateTimeXmlAdapter.class);
		add(DurationXmlAdapter.class);
		add(CountFromIntegerXmlAdapter.class);
		add(RouteDescriptionXmlAdapter.class);
		add(TrailheadDescriptionXmlAdapter.class);
		add(MessagePersonalizedXmlAdapter.class);
		add(RouteNameXmlAdapter.class);
		adapters.put(CompleteMessagePersonalizedXmlAdapter.class, CompleteMessageXmlAdapter.class);
	}

	private Map<Class<? extends XmlAdapter<?, ?>>, XmlAdapter<?, ?>> builtAdapters = Maps.newHashMap();

	/**
	 * Used to construct all instances of PersonalizedAdapterFactory.
	 */
	public static class Builder
			extends ObjectBuilder<PersonalizedAdapterFactory> {

		private Injector injector;

		public Builder injector(Injector injector) {
			this.injector = injector;
			return this;
		}

		public Builder() {
			super(new PersonalizedAdapterFactory());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public PersonalizedAdapterFactory build() {
			PersonalizedAdapterFactory built = super.build();
			for (Class<? extends XmlAdapter<?, ?>> adapterType : PersonalizedAdapterFactory.adapters.keySet()) {
				Class<? extends XmlAdapter<?, ?>> beingReplaced = PersonalizedAdapterFactory.adapters.get(adapterType);
				built.builtAdapters.put(beingReplaced, injector.getInstance(adapterType));
			}
			return built;
		}
	}// end Builder

	/** Use {@link Builder} to construct PersonalizedAdapterFactory */
	private PersonalizedAdapterFactory() {
	}

	public static Builder create() {
		return new Builder();
	}

	public Map<Class<? extends XmlAdapter<?, ?>>, XmlAdapter<?, ?>> getAdapters() {
		return builtAdapters;
	}

}
