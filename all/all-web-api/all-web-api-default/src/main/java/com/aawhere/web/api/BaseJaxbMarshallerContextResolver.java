/**
 *
 */
package com.aawhere.web.api;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ContextResolver;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldExpansionNotPossibleException;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.xml.FieldXmlAdapterService;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.xml.NumberPersonalizedXmlAdapter;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterConditionXmlAdapter;
import com.aawhere.persist.FilterUtil;
import com.aawhere.personalize.PersonalizedAdapterFactory;
import com.aawhere.personalize.Personalizer;
import com.aawhere.xml.bind.ExceptionThrowingMarshaller;
import com.aawhere.xml.bind.OptionalXmlAdapterFactory;
import com.google.inject.Inject;
import com.google.inject.Injector;

/**
 * Our interception of the JAXB processing done by Jersey. This will use {@link Personalizer}s to
 * localize and i18n according to the user preferences from their HTTP header or their user account.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class BaseJaxbMarshallerContextResolver
		implements ContextResolver<Marshaller> {

	private UriInfo uriInfo;
	@Inject
	private FilterConditionXmlAdapter filterConditionXmlAdapter;
	@Inject
	private OptionalXmlAdapterFactory optionalXmlAdapterFactory;

	@Inject
	private Injector injector;

	@Inject
	private FieldDictionaryFactory dictionaryFactory;

	public BaseJaxbMarshallerContextResolver(@Context UriInfo uriInfo) {
		this.uriInfo = uriInfo;
	}

	@Override
	public Marshaller getContext(Class<?> type) {
		Locale locale = injector.getInstance(Locale.class);
		Marshaller marshaller = getMarshaller(type);
		if (locale != null) {
			setPersonalizedAdapters(marshaller, locale);
		}

		setOptionalAdapters(marshaller);

		injector.getInstance(Locale.class);
		try {
			setFieldAdapters(marshaller, type);
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).severe(type.getSimpleName() + " marshaller adaptation failed "
					+ e.getMessage());
			marshaller = new ExceptionThrowingMarshaller(marshaller, e);
		}

		return marshaller;
	}

	protected abstract Marshaller getMarshaller(Class<?> type);

	private void setOptionalAdapters(Marshaller marshaller) {
		List<String> options = FilterUtil.listParsed(uriInfo.getQueryParameters().get(Filter.FIELD.OPTIONS));
		List<XmlAdapter<?, ?>> adapters = optionalXmlAdapterFactory.adapter(options);

		for (@SuppressWarnings("rawtypes")
		XmlAdapter optionalXmlAdapter : adapters) {
			marshaller.setAdapter(optionalXmlAdapterFactory.type(optionalXmlAdapter), optionalXmlAdapter);
		}
	}

	private void setFieldAdapters(Marshaller marshaller, Class<?> type) throws FieldExpansionNotPossibleException,
			FieldNotRegisteredException {
		// the adapter service modifies the marshaller directly
		FieldXmlAdapterService.create().typeBeingMarshalled(type).dictionaryFactory(dictionaryFactory)
				.injector(injector).fieldsRequested(uriInfo.getQueryParameters().get(Filter.FIELD.EXPAND))
				.marshaller(marshaller).buildWithExceptions();
		// adapter setup for non-localized
		marshaller.setAdapter(this.filterConditionXmlAdapter);
	}

	private void setPersonalizedAdapters(Marshaller marshaller, Locale locale) {
		NumberFormat formatter = NumberFormat.getNumberInstance(locale);
		formatter.setMaximumFractionDigits(1);
		marshaller.setAdapter(	NumberPersonalizedXmlAdapter.class,
								new NumberPersonalizedXmlAdapter.Builder().formatter(formatter).build());

		PersonalizedAdapterFactory personalizedAdapterFactory = PersonalizedAdapterFactory.create().injector(injector)
				.build();
		Map<Class<? extends XmlAdapter<?, ?>>, XmlAdapter<?, ?>> adapters = personalizedAdapterFactory.getAdapters();
		for (Entry<Class<? extends XmlAdapter<?, ?>>, XmlAdapter<?, ?>> entry : adapters.entrySet()) {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			Class<XmlAdapter<?, ?>> key = (Class) entry.getKey();
			marshaller.setAdapter(key, entry.getValue());
		}

	}

}