package com.aawhere.web.api.activity.timing;

import static com.aawhere.activity.timing.ActivityTimingWebApiUri.*;
import static com.aawhere.web.api.geo.GeoWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.KmlType;

import com.aawhere.activity.export.kml.ActivityKmlExportService;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingProcessService;
import com.aawhere.activity.timing.ActivityTimingRelations;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.field.FieldDictionary;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.route.CoursesNotRelatedException;
import com.aawhere.route.RouteCompletions;
import com.aawhere.route.RouteService;
import com.aawhere.route.RouteWebApiUri;
import com.google.inject.Inject;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Serves content to the web api for {@link ActivityTimingRelations}. Abstract class for public and
 * admin backends.
 * 
 * @author aroller
 * 
 */
public abstract class ActivityTimingWebApi {

	public static final String DESCRIPTION = "Completion attempts for two activities";
	final protected ActivityTimingService timingService;
	final protected ActivityKmlExportService kmlExportService;
	final protected RouteService routeService;
	final protected ActivityTimingProcessService timingProcessService;

	/**
	 * 
	 */
	public ActivityTimingWebApi(ActivityTimingService service, RouteService routeService,
			ActivityKmlExportService kmlExportService, ActivityTimingProcessService timingProcessService) {
		this.timingService = service;
		this.kmlExportService = kmlExportService;
		this.routeService = routeService;
		this.timingProcessService = timingProcessService;

	}

	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Timings matching the filter")
	public ActivityTimingRelations filterGet(@InjectParam Filter filter) {
		return timingService.timings(filter);
	}

	@GET
	@Path(SLASH + TIMINGS_ID_PARAM)
	@Produces(KML_CONTENT_TYPE)
	@ApiOperation(value = "Google Earth debugger of the attempt to complete a course.")
	public JAXBElement<KmlType> getTimingKml(@PathParam(TIMINGS_ID) ActivityTimingId id) throws EntityNotFoundException {
		return this.kmlExportService.getCourseAttemptDeviationKml(id).getElement();
	}

	@GET
	@Path(SLASH + TIMINGS_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Retrieves the stored timing or processes and stores the timing if not yet done. ",
			notes = "This will import either activity if not already in the system ")
	public ActivityTiming getTiming(@PathParam(TIMINGS_ID) ActivityTimingId id,
			@DefaultValue(TRUE) @QueryParam(SCREEN) Boolean screen) throws EntityNotFoundException {
		return this.timingService.getOrProcessTiming(id, screen, false);
	}

	@GET
	@Path(SLASH + RouteWebApiUri.TIMINGS_ID_ROUTES_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "RouteCompletions representing a timing")
	public RouteCompletions completionsGet(@PathParam(TIMINGS_ID) ActivityTimingId id) throws EntityNotFoundException,
			CoursesNotRelatedException {
		return this.routeService.completions(id);
	}

	@GET
	@Path(SLASH + DICTIONARY)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Definitions of the fields used to represent timings.")
	public FieldDictionary getTimingsDictionary() {
		return this.timingService.getDictionary();
	}

}
