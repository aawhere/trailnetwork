/**
 *
 */
package com.aawhere.web.api.app.account;

import static com.aawhere.activity.ActivityWebApiUri.*;
import static com.aawhere.app.account.AccountWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.activity.Activities;
import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityProcessService;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountService;
import com.aawhere.app.account.Accounts;
import com.aawhere.field.FieldDictionary;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.person.Person;
import com.aawhere.person.PersonService;
import com.aawhere.security.SecurityRoles;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.google.inject.Inject;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Publishes {@link Account} information.
 * 
 * @author aroller
 * 
 */
abstract public class AccountWebApi {

	protected final AccountService accountService;
	protected final ActivityImportService importService;
	protected final PersonService personService;
	protected final ActivityProcessService activityProcessService;

	@Inject
	public AccountWebApi(AccountService accountService, PersonService personService,
			ActivityImportService importService, ActivityProcessService activityProcessService) {
		this.accountService = accountService;
		this.importService = importService;
		this.personService = personService;
		this.activityProcessService = activityProcessService;
	}

	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Accounts matching the filter.", position = ApiDocumentationSupport.Position.PUBLIC)
	public Accounts filterGet(@InjectParam Filter filter) {
		return this.accountService.accounts(filter);
	}

	@GET
	@Path(SLASH + ACCOUNT_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Account matching the AccountId.", position = ApiDocumentationSupport.Position.PUBLIC)
	public Account id(@PathParam(ACCOUNT_ID) AccountId id) throws EntityNotFoundException {
		return this.accountService.account(id);
	}

	@GET
	@Path(SLASH + ACCOUNT_ID_PERSONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The Person who owns the Account.", position = ApiDocumentationSupport.Position.PUBLIC)
	public Person personGet(@PathParam(ACCOUNT_ID) AccountId id) throws EntityNotFoundException {
		return this.personService.person(id);
	}

	@RolesAllowed({ SecurityRoles.ADMIN, SecurityRoles.TRUSTED_APP })
	@PUT
	@Path(SLASH + ACCOUNT_ID_PERSONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Update or Create the Account and Person who owns the Account.",
			notes = "Query parameters are available for the account to be updated.")
	public Person personPut(@PathParam(ACCOUNT_ID) AccountId id, @InjectParam Account account)
			throws EntityNotFoundException {
		return this.personService.person(Account.clone(account).id(id).build()).getKey();
	}

	@GET
	@Path(SLASH + ACCOUNT_ID_ACTIVITIES_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Paginates all Activities related to an Account most recent first",
			response = Activities.class, position = ApiDocumentationSupport.Position.PUBLIC)
	public Activities activitiesGet(@PathParam(ACCOUNT_ID) AccountId id, @InjectParam Filter filter)
			throws BaseException {
		return importService.getActivityService().activities(id, filter);
	}

	@GET
	@Path(SLASH + DICTIONARY)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The fields associated with the Account domain.",
			position = ApiDocumentationSupport.Position.PUBLIC)
	public FieldDictionary dictionary() {
		return this.accountService.getDictionary();
	}

}
