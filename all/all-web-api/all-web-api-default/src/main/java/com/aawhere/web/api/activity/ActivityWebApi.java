/**
 *
 */
package com.aawhere.web.api.activity;

import static com.aawhere.activity.ActivityWebApiUri.*;
import static com.aawhere.activity.timing.ActivityTimingWebApiUri.*;
import static com.aawhere.web.api.ApiDocumentationSupport.*;
import static com.aawhere.web.api.geo.GeoWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.KmlType;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityExportService;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportException;
import com.aawhere.activity.ActivityImportFailures;
import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityMessage;
import com.aawhere.activity.ActivityProcessService;
import com.aawhere.activity.ActivityService;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.ActivityTypeDistanceCategory;
import com.aawhere.activity.export.kml.ActivityKmlExportService;
import com.aawhere.activity.timing.ActivityTimingRelations;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.route.CoursesNotRelatedException;
import com.aawhere.route.Route;
import com.aawhere.route.RouteCompletionService;
import com.aawhere.route.RouteCompletions;
import com.aawhere.route.RouteService;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.util.rb.Messages;
import com.aawhere.util.rb.StatusMessage;
import com.aawhere.util.rb.StatusMessages;
import com.aawhere.util.rb.VerbTense;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.sun.jersey.api.core.InjectParam;
import com.trailnetwork.web.api.activity.ActivityPublicWebApi;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

/**
 * An Activity is a single "session" with a start/finish and typically a single activity type.
 * 
 * Public endpoints are declared here, but published at {@link ActivityPublicWebApi} to allow for
 * administrative extension since JAX-RS extension does not allow duplicate path declarations.
 * 
 * @author Aaron Roller
 * 
 */
abstract public class ActivityWebApi {

	/**
	 * max queue size
	 */
	public static final int MAX_TIMING_RELATIONS_QUEUE_SIZE = 5000;
	/**
	 *
	 */
	final protected ActivityService activityService;
	final protected ActivityImportService activityImportHistoryService;
	final protected FieldAnnotationDictionaryFactory dictionaryFactory;
	final protected ActivityImportService importService;
	final protected ActivityTimingService timingRelationService;
	final protected ActivityKmlExportService kmlExportService;
	final protected ActivityExportService activityExportService;
	final protected RouteService routeService;
	final protected RouteCompletionService completionService;
	final protected ActivityProcessService activityProcessService;

	public ActivityWebApi(ActivityTimingService timingRelationService,
			ActivityImportService activityImportHistoryService, FieldAnnotationDictionaryFactory dictionaryFactory,
			ActivityImportService importService, ActivityKmlExportService kmlExportService,
			ActivityExportService activityExportService, RouteService routeService,
			ActivityProcessService activityProcessService) {
		this.activityService = importService.getActivityService();
		this.timingRelationService = timingRelationService;
		this.activityImportHistoryService = activityImportHistoryService;
		this.dictionaryFactory = dictionaryFactory;
		this.importService = importService;
		this.kmlExportService = kmlExportService;
		this.activityExportService = activityExportService;
		this.routeService = routeService;
		this.completionService = routeService.getCompletionService();
		this.activityProcessService = activityProcessService;
	}

	/**
	 * Providing the application's activity id this will return the TrailNetwork representation of
	 * the activity. If the activity is already cached this will return quickly otherwise it will
	 * retrieve and process the activity and return it complete.
	 * 
	 * TrailNetwork activity ids can be provided in the standard tn-12345 format, but this also will
	 * assume any id provided without the application qualifier is referring to the TrailNetwork and
	 * will return an activity of that id.
	 * 
	 * @param id
	 * @return
	 * @throws BaseException
	 */
	@GET
	@Path(SLASH + ACTIVITY_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Provides a single Activity retrieved by it's ActivityId.",
			notes = "Provides the Activity known to this system. If not yet retrieved from the Application, this will make an attempt to retrieve the Activity when requested and store in the system archive for future faster retrieval."
					+ EXAMPLE_ACTIVITY_REFERENCE_PHRASE, response = Activity.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public
			Activity
			getActivity(
					@PathParam(ACTIVITY_ID) @ApiParam(
							required = true,
							value = "The ActivityId indicating the Application and the ID for the Activity at the Application.",
							defaultValue = EXAMPLE_ACTIVITY_ID) ActivityId id) throws EntityNotFoundException,
					ActivityImportException {
		return importService.getActivity(id);
	}

	@GET
	@Path(SLASH + ACTIVITY_ID_PARAM)
	@Produces(GeoWebApiUri.POLYLINE_ENCODED_MEDIA_TYPE)
	@ApiOperation(value = "The Google Maps Encoded Path of the Track.", response = String.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public String getActivityGoogleMapPolyline(@PathParam(ACTIVITY_ID) ActivityId activityId)
			throws EntityNotFoundException, ActivityImportException {
		return this.activityExportService.gpolyline(activityId);
	}

	@GET
	@Path(SLASH + ACTIVITY_ID_PARAM)
	@Produces({ KML_CONTENT_TYPE })
	@ApiOperation("The Google Earth representation of an Activity.")
	public JAXBElement<KmlType> getActivityKml(@PathParam(ACTIVITY_ID) ActivityId activityId)
			throws EntityNotFoundException, ActivityImportException {
		return kmlExportService.getActivityDocument(activityId);
	}

	/**
	 * Retrieves the history of the import when providing the {@link ActivityId} in the complete
	 * format {key}-{ID}.
	 * 
	 * @param id
	 * @return
	 */
	@GET
	@Path(SLASH + ACTIVITY_ID_IMPORTS_PATH)
	@Produces({ MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Import history of failed attempts to import an activity.",
			notes = "Successful imports have no entry.")
	public ActivityImportFailures getActivityImportsHistory(@PathParam(ACTIVITY_ID) ActivityId id) {
		return activityImportHistoryService.getActivityHistory(id);
	}

	/**
	 * This will provide {@link ActivityImportMilestones} matching the provided filter.
	 * 
	 * @param filter
	 * @return
	 */
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, NONE_MEDIA_TYPE })
	@Path(IMPORTS)
	public ActivityImportFailures importGet(@InjectParam Filter filter) {
		return importService.filter(filter);
	}

	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Paginates Activities limited by the Filter.", response = Activities.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public Activities filterGet(@InjectParam Filter filter) {
		Activities activities = activityService.getActivities(filter);
		return activities;
	}

	@GET
	@Path(SLASH + DICTIONARY)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Describes the Fields and sub-dictionaries of an Activity.",
			response = FieldDictionary.class, position = ApiDocumentationSupport.Position.PUBLIC)
	public FieldDictionary getDictionary() {
		return dictionaryFactory.getDictionary(Activity.class);
	}

	@GET
	@Path(SLASH + RouteWebApiUri.ACTIVITY_ID_ROUTE_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "The Route exactly matching the Activity",
			notes = "If the Activity identified by ActivityId is a Course for any Route, meaning it is an exact match of the Route, then the route will be provided.  If no Course match is found then the response indicates not found.",
			response = Route.class)
	@ApiResponses({
			@ApiResponse(code = CoursesNotRelatedException.CODE,
					message = "When no RouteCompletion has a course reference matching the ActivityId."),
			@ApiResponse(code = CoursesNotRelatedException.CODE,
					message = "When no Activity is found matching the ActivityId.") })
	public
			Route routeGet(@PathParam(ACTIVITY_ID) ActivityId activityId) throws EntityNotFoundException,
					ActivityImportException, CoursesNotRelatedException {
		return this.routeService.route(activityId);
	}

	@GET
	@Path(SLASH + RouteWebApiUri.ACTIVITY_ID_ROUTE_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documention coming soon...", response = RouteCompletions.class)
	public RouteCompletions completionsGet(@PathParam(ACTIVITY_ID) ActivityId activityId, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return this.completionService.completions(activityId, filter);
	}

	@GET
	@Path(SLASH + FIELDS)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	public Activity.FIELD fields() {
		return new Activity.FIELD();
	}

	@GET
	@Path(SLASH + TYPES)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The ActivityTypes available in the System.",
			notes = ActivityMessage.Type.Doc.FULL_DESCRIPTION, position = ApiDocumentationSupport.Position.PUBLIC)
	public Messages activityTypes() {
		return ActivityType.messages();
	}

	@GET
	@Path(SLASH + TYPES + SLASH + ACTIVITY_TYPE_PARAM + SLASH + NOUNS)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Localized version of the noun given the count for plurality",
			position = ApiDocumentationSupport.Position.PUBLIC)
	public StatusMessage activityTypes(@PathParam(ACTIVITY_TYPE) ActivityType activityType,
			@QueryParam(COUNT) Integer count) {
		return ActivityType.nounPhraseStatusMessage(activityType, count);
	}

	@GET
	@Path(SLASH + TYPES + SLASH + ACTIVITY_TYPE_PARAM + SLASH + VERBS + SLASH + TENSE_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Localized version of the past, present, future tense verb for an activity type",
			position = ApiDocumentationSupport.Position.PUBLIC)
	public StatusMessage activityTypeVerbs(@PathParam(ACTIVITY_TYPE) ActivityType activityType,
			@PathParam(TENSE) VerbTense tense) {
		return ActivityType.verbStatusMessage(activityType, tense);
	}

	@GET
	@Path(SLASH + TYPES + SLASH + VERBS + SLASH + TENSE_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Localized versions of the past, present, future tense verb for all activity types ",
			position = ApiDocumentationSupport.Position.PUBLIC)
	public StatusMessages activityTypeVerbs(@PathParam(TENSE) VerbTense tense) {
		return ActivityType.verbStatusMessages(tense);
	}

	@GET
	@Path(SLASH + TYPES + SLASH + DISTANCES)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Used for filter Routes by distance for each ActivityType. ",
			response = ActivityTypeDistanceCategory.class, position = ApiDocumentationSupport.Position.PUBLIC)
	public Messages activityTypeDistanceCategories() {
		return ActivityTypeDistanceCategory.messages();
	}

}
