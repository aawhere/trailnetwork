/**
 * 
 */
package com.aawhere.web.api.doc;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import scala.Option;
import scala.collection.JavaConversions;
import scala.collection.immutable.List;
import scala.collection.mutable.ListBuffer;

import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.annotation.AnnotationProcessorUtil;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterSort;
import com.aawhere.persist.FilterUtil;
import com.aawhere.persist.PersistMessage;
import com.aawhere.personalize.PersonalizeMessage;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.swagger.DocSupport;
import com.aawhere.swagger.DocSupport.AllowMultiple;
import com.aawhere.swagger.DocSupport.Required;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.ws.ErrorResponse;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.Authorization;
import com.wordnik.swagger.jaxrs.MutableParameter;
import com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader;
import com.wordnik.swagger.model.AllowableValues;
import com.wordnik.swagger.model.Operation;
import com.wordnik.swagger.model.Parameter;
import com.wordnik.swagger.model.ResponseMessage;
import com.wordnik.swagger.reader.ClassReader;

/**
 * Provides the ability to override the default Swagger documentation. Use this to customize
 * parameters and operations by intercepting incoming and providing customized outgoing.
 * 
 * The class is complicated by working with Scala.
 * 
 * @author aroller
 * 
 */
public class ApiSwaggerClassReader
		extends DefaultJaxrsApiReader
		implements ClassReader {

	private boolean publicApi = false;

	/**
	 * 
	 */
	public ApiSwaggerClassReader() {
		this(ApiSwaggerSpecFilter.PRIVATE);
	}

	private ApiSwaggerClassReader(boolean publicApi) {

		this.publicApi = publicApi;
		Parameter limit = new Parameter(Filter.FIELD.LIMIT,
				scala.Option.apply(PersistMessage.Doc.FILTER_LIMIT_DESCRIPTION),
				scala.Option.apply(PersistMessage.Doc.FILTER_LIMIT_DEFAULT), Required.FALSE, AllowMultiple.FALSE,
				DocSupport.DataType.INTEGER, ALLOWABLE_VALUES_NONE, DocSupport.ParamType.QUERY,
				scala.Option.apply(StringUtils.EMPTY));
		Parameter page = new Parameter(Filter.FIELD.PAGE,
				scala.Option.apply(PersistMessage.Doc.FILTER_PAGE_DESCRIPTION),
				scala.Option.apply(PersistMessage.Doc.FILTER_PAGE_DEFAULT), Required.FALSE, AllowMultiple.FALSE,
				DocSupport.DataType.INTEGER, ALLOWABLE_VALUES_NONE, DocSupport.ParamType.QUERY, scala.Option.apply(""));
		Parameter conditions = new Parameter(Filter.FIELD.CONDITIONS,
				scala.Option.apply(PersistMessage.Doc.FILTER_CONDITIONS_DESCRIPTION),
				scala.Option.apply(StringUtils.EMPTY), Required.FALSE, AllowMultiple.TRUE,
				FilterCondition.class.getName(), ALLOWABLE_VALUES_NONE, DocSupport.ParamType.QUERY,
				scala.Option.apply(""));
		Parameter options = new Parameter(Filter.FIELD.OPTIONS,
				scala.Option.apply(PersistMessage.Doc.FILTER_OPTIONS_DESCRIPTION),
				scala.Option.apply(StringUtils.EMPTY), Required.FALSE, AllowMultiple.TRUE, DocSupport.DataType.STRING,
				ALLOWABLE_VALUES_NONE, DocSupport.ParamType.QUERY, scala.Option.apply(""));
		Parameter fields = new Parameter(Filter.FIELD.FIELDS,
				scala.Option.apply(PersistMessage.Doc.FILTER_FIELDS_DESCRIPTION),
				scala.Option.apply(StringUtils.EMPTY), Required.FALSE, AllowMultiple.TRUE, DocSupport.DataType.STRING,
				ALLOWABLE_VALUES_NONE, DocSupport.ParamType.QUERY, scala.Option.apply(""));
		Parameter query = new Parameter(Filter.FIELD.QUERY,
				scala.Option.apply(PersistMessage.Doc.FILTER_QUERY_DESCRIPTION), scala.Option.apply(StringUtils.EMPTY),
				Required.FALSE, AllowMultiple.FALSE, DocSupport.DataType.STRING, ALLOWABLE_VALUES_NONE,
				DocSupport.ParamType.QUERY, scala.Option.apply(""));
		Parameter bounds = new Parameter(Filter.FIELD.BOUNDS,
				scala.Option.apply(PersistMessage.Doc.FILTER_BOUNDS_DESCRIPTION),
				scala.Option.apply(StringUtils.EMPTY), Required.FALSE, AllowMultiple.FALSE, DocSupport.DataType.STRING,
				ALLOWABLE_VALUES_NONE, DocSupport.ParamType.QUERY, scala.Option.apply(""));
		{
			ImmutableList.Builder<Parameter> filterParameterBuilder = ImmutableList.<Parameter> builder();
			filterParameterBuilder.add(page);
			filterParameterBuilder.add(limit);
			if (!publicApi) {
				filterParameterBuilder.add(conditions);
				filterParameterBuilder.add(options);
			}
			FILTER_PARAMETERS = filterParameterBuilder.build();
		}
		{
			ImmutableList.Builder<Parameter> searchFilterParameterBuilder = ImmutableList.<Parameter> builder();
			searchFilterParameterBuilder.add(query);
			searchFilterParameterBuilder.add(bounds);
			if (!publicApi) {
				searchFilterParameterBuilder.add(fields);
			}
			SEARCH_FILTER_PARAMETERS = searchFilterParameterBuilder.build();
		}
		Parameter acceptLocalization = new Parameter(PersonalizeMessage.Doc.ACCEPT_I18_NAME,
				scala.Option.apply("Expands all items that may be personalized"),
				scala.Option.apply(StringUtils.EMPTY), PersonalizeMessage.Doc.ACCEPT_I18_REQUIRED, AllowMultiple.FALSE,
				PersonalizeMessage.Doc.ACCEPT_I18N_DATA_TYPE, ALLOWABLE_VALUES_NONE,
				PersonalizeMessage.Doc.ACCEPT_I18N_PARAM_TYPE, scala.Option.apply(StringUtils.EMPTY));

		Parameter acceptLanguage = new Parameter(HttpHeaders.ACCEPT_LANGUAGE,
				scala.Option.apply("If localization is chosen, this is the language"),
				scala.Option.apply(ApiDocumentationSupport.LANGUAGE_DEFAULT), Required.FALSE, AllowMultiple.FALSE,
				ApiDocumentationSupport.LANGUAGE_DATA_TYPE, ALLOWABLE_VALUES_NONE, DocSupport.ParamType.HEADER,
				scala.Option.apply(StringUtils.EMPTY));
		LANGAUGE_PARAMETERS = ImmutableList.of(acceptLocalization, acceptLanguage);
	}

	/**
	 * 
	 */
	private static final AllowableValues ALLOWABLE_VALUES_NONE = new AllowableValues() {
	};
	static Logger logger = LoggerFactory.getLogger(ApiSwaggerClassReader.class);
	private final java.util.List<Parameter> FILTER_PARAMETERS;
	private final java.util.List<Parameter> SEARCH_FILTER_PARAMETERS;
	private final java.util.List<Parameter> LANGAUGE_PARAMETERS;

	/**
	 * This is where all declared parameters are passed in for processing. We can intercept and
	 * replace parameters as needed.
	 * 
	 * 
	 * @see com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader#processParamAnnotations(com.wordnik
	 *      .swagger.jaxrs.MutableParameter, java.lang.annotation.Annotation[])
	 */
	@Override
	public List<Parameter> processParamAnnotations(MutableParameter mutable, Annotation[] paramAnnotations) {
		java.util.List<Parameter> parameters = Lists.newArrayList();

		if (Filter.class.getName().equals(mutable.dataType())) {
			parameters.addAll(FILTER_PARAMETERS);
		} else {
			// look for query parameters on embedded objects
			Class<?> dataTypeClass;
			try {
				dataTypeClass = Class.forName(mutable.dataType());
				parameters.addAll(beanInfoParameters(dataTypeClass));
			} catch (ClassNotFoundException e) {
				// not much reflection we can do
			}

		}
		parseApiParamAnnotationForEnum(mutable);

		// if nothing else better was found then use what was given.
		if (parameters.isEmpty()) {
			List<Parameter> processParamAnnotations = super.processParamAnnotations(mutable, paramAnnotations);
			Collection<Parameter> asJavaCollection = JavaConversions.asJavaCollection(processParamAnnotations);
			parameters.addAll(asJavaCollection);
		}
		return JavaConversions.asScalaBuffer(parameters).toList();
	}

	/*
	 * @see
	 * com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader#parseApiParamAnnotation(com.wordnik
	 * .swagger.jaxrs.MutableParameter, com.wordnik.swagger.annotations.ApiParam)
	 */
	@Override
	public void parseApiParamAnnotation(MutableParameter param, ApiParam annotation) {
		super.parseApiParamAnnotation(param, annotation);
		parseApiParamAnnotationForEnum(param);
	}

	/**
	 * If the param represents and enum type, this will provide the allowable values.
	 * 
	 * @param param
	 * @param annotation
	 */
	public void parseApiParamAnnotationForEnum(MutableParameter param) {
		try {
			Class<?> dataTypeClass = Class.forName(param.dataType());

			if (dataTypeClass.isEnum()) {
				AllowableValues allowableValues = allowableValues(dataTypeClass);
				param.allowableValues_$eq(allowableValues);
				// swagger UI doesn't produce a model so just the simple name relays most everything
				// you need to understand what is being offered.
				param.dataType_$eq(dataTypeClass.getSimpleName());

				Option<String> descriptionFromModel = SwaggerUtil.descriptionFromModel(dataTypeClass);
				if (descriptionFromModel != null) {
					param.description_$eq(descriptionFromModel);
				}

			}
		} catch (ClassNotFoundException e) {
			// no problem...just pass it through
		}
	}

	/**
	 * provides our best representation of allowable values. If an enumeration then the values of
	 * the enumeration are given.
	 * 
	 * @param dataTypeClass
	 * @return
	 */
	public AllowableValues allowableValues(Class<?> dataTypeClass) {
		AllowableValues allowableValues = ALLOWABLE_VALUES_NONE;
		if (dataTypeClass.isEnum()) {
			allowableValues = SwaggerUtil.allowableValues(Arrays.asList(dataTypeClass.getEnumConstants()));
		}
		return allowableValues;
	}

	/**
	 * @param dataTypeClass
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public java.util.List<Parameter> beanInfoParameters(Class<?> dataTypeClass) {
		java.util.List<Parameter> parameters = Lists.newArrayList();
		// if query parameters are available then document them
		Map<Member, QueryParam> queryParamMembers = SwaggerUtil.dataTypeWithQueryParam(dataTypeClass);
		if (queryParamMembers != null) {
			Object instance;

			try {
				Constructor<?> constructor = dataTypeClass.getConstructor();
				constructor.setAccessible(true);
				instance = constructor.newInstance();
			} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException
					| IllegalArgumentException | InvocationTargetException e) {
				// no way to get the default value
				instance = null;
			}

			for (Entry<Member, QueryParam> queryParamMemberEntry : queryParamMembers.entrySet()) {
				final Member member = queryParamMemberEntry.getKey();
				final QueryParam queryParam = queryParamMemberEntry.getValue();

				String description = StringUtils.EMPTY;
				AllowableValues allowableValues = ALLOWABLE_VALUES_NONE;
				String paramType = DocSupport.ParamType.QUERY;
				boolean nonNull = false;
				if (member instanceof AnnotatedElement) {
					ApiParam apiParam = ((AnnotatedElement) member).getAnnotation(ApiParam.class);
					if (apiParam != null) {
						description = apiParam.value();
						final String allowableString = apiParam.allowableValues();
						if (allowableString != null) {
							allowableValues = toAllowableValues(allowableString, paramType);
						}
					}
					nonNull = ((AnnotatedElement) member).getAnnotation(Nonnull.class) != null;

				}

				if (member instanceof Field) {
					Field field = (Field) member;

					String defaultValue = null;
					if (instance != null) {
						Object instanceValue;
						try {
							field.setAccessible(true);
							instanceValue = field.get(instance);
							if (instanceValue != null) {
								defaultValue = String.valueOf(instanceValue);
							}
						} catch (IllegalArgumentException | IllegalAccessException e) {
							throw new RuntimeException(e);
						}
					}

					boolean required = false;
					if (defaultValue == null) {
						if (nonNull) {
							// nonnull with no default indicates it is required to be
							// assigned?
							required = true;
						}
						defaultValue = StringUtils.EMPTY;
					}
					if (allowableValues == ALLOWABLE_VALUES_NONE && field.getType().isEnum()) {
						@SuppressWarnings({ "rawtypes" })
						Class<Enum> enumClass = (Class<Enum>) field.getType();
						allowableValues = allowableValues(enumClass);
					}
					String parameterDataType = super.processDataType(field.getType(), field.getGenericType());
					boolean allowMultiple = Iterable.class.isAssignableFrom(field.getType());

					Parameter parameter = new Parameter(queryParam.value(), scala.Option.apply(description),
							scala.Option.apply(defaultValue), required, allowMultiple, parameterDataType,
							allowableValues, paramType, scala.Option.apply(StringUtils.EMPTY));
					parameters.add(parameter);
				}

			}
		}
		return parameters;
	}

	/*
	 * @see com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader#addLeadingSlash(java.lang.String)
	 */
	@Override
	public String addLeadingSlash(String e) {
		if (!e.startsWith(SystemWebApiUri.SLASH)) {
			e = SystemWebApiUri.SLASH + e;
		}
		return super.addLeadingSlash(e);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader#parseOperation(java.lang.reflect.Method
	 * , com.wordnik.swagger.annotations.ApiOperation, scala.collection.immutable.List,
	 * java.lang.String, scala.collection.immutable.List, scala.collection.mutable.ListBuffer)
	 */
	@Override
	public Operation parseOperation(final Method method, final ApiOperation apiOperation,
			List<ResponseMessage> apiResponses, final String isDeprecated, final List<Parameter> parentParams,
			ListBuffer<Method> parentMethods) {

		Class<? extends Exception>[] exceptionTypes = (Class<? extends Exception>[]) method.getExceptionTypes();
		if (ArrayUtils.isNotEmpty(exceptionTypes)) {
			java.util.List<ResponseMessage> responses = Lists.newArrayList();
			for (int i = 0; i < exceptionTypes.length; i++) {
				Class<? extends Exception> exceptionType = exceptionTypes[i];
				StatusCode statusCodeAnnotation = exceptionType.getAnnotation(StatusCode.class);
				if (statusCodeAnnotation != null) {
					// we must throw exceptions that can be localized and report a status code
					// InvalidArgumentException.required( statusCodeAnnotation,
					// new StringMessage(StatusCode.class.getSimpleName() + " for "
					// + exceptionType.getName() + " on "
					// + apiOperation.nickname()));
					String errorCode = AnnotationProcessorUtil.exceptionConstantName(exceptionType);
					String message = statusCodeAnnotation.message();
					String displayMessage = errorCode + " : " + message;
					ResponseMessage responseMessage = new ResponseMessage(statusCodeAnnotation.value().value,
							displayMessage, Option.apply(ErrorResponse.class.getSimpleName()));
					responses.add(responseMessage);
				}
			}
			apiResponses = JavaConversions.asScalaBuffer(responses).toList();
		}
		ApiOperation apiOperationOverride = apiOperationOverride(apiOperation, method);
		Operation operation = super.parseOperation(	method,
													apiOperationOverride,
													apiResponses,
													isDeprecated,
													parentParams,
													parentMethods);
		// search methods provide different
		boolean searchMethod = SearchWebApiUri.SEARCH.equals(apiOperation.tags());

		// remove localization parameters if added in parameter inspection
		// parameter inspection doesn't know about produces so we must do such here
		Collection<String> produces = JavaConversions.asJavaCollection(operation.produces());
		ArrayList<Parameter> parameters = Lists.newArrayList();
		parameters.addAll(paramsForReturnType(apiOperationOverride, searchMethod));
		// adding the parameters in the methods last since filter is better at the end
		parameters.addAll(JavaConversions.asJavaCollection(operation.parameters()));
		if (searchMethod) {
			parameters.addAll(SEARCH_FILTER_PARAMETERS);
		}
		if (produces.contains(MediaType.APPLICATION_XML) || produces.contains(MediaType.APPLICATION_JSON)) {
			parameters.addAll(LANGAUGE_PARAMETERS);
		}

		String responseClass = operation.responseClass();
		if (responseClass == null) {
			responseClass = method.getReturnType().getName();
		}
		List<Parameter> parametersReplacement = JavaConversions.asScalaBuffer(parameters).toList();
		operation = new Operation(operation.method(), operation.summary(), operation.notes(),
				operation.responseClass(), operation.nickname(), operation.position(), operation.produces(),
				operation.consumes(), operation.protocols(), operation.authorizations(), parametersReplacement,
				operation.responseMessages(), operation.deprecated());
		return operation;
	}

	/**
	 * Automatically adds parameters based on the response type for a single value. Expand is
	 * automatically generated based on the {@link com.aawhere.field.annotation.Field#xmlAdapter()}.
	 * 
	 * @param apiOperationOverride
	 * @return
	 */
	private java.util.List<Parameter> paramsForReturnType(ApiOperation apiOperation, boolean searchMethod) {
		Builder<Parameter> parameters = ImmutableList.builder();

		Class<?> response = apiOperation.response();
		Class<?> entityTypeContained = EntityUtil.entityTypeContained(response);
		boolean responseIsContainer = false;
		// if the response type is a container of entities then it should emit the behavior of
		// the
		// entity...and conditions should be offered
		if (entityTypeContained != null) {
			responseIsContainer = true;
			response = entityTypeContained;
		}

		Dictionary dictionary = AnnotatedDictionaryUtils.dictionaryFrom(response);
		if (dictionary != null) {
			ArrayList<FieldDefinition<?>> expandableDefinitions = Lists.newArrayList();
			ArrayList<FieldDefinition<?>> indexedDefinitions = Lists.newArrayList();
			FieldDictionary fieldDictionary = new FieldAnnotationDictionaryFactory().getDictionary(response);
			Set<FieldDefinition<?>> fields = fieldDictionary.getFields();

			// add a filter condition for each field with an index
			for (FieldDefinition<?> fieldDefinition : fields) {
				// if searchmethod then look for searchable otherwise look for index
				if (responseIsContainer
						&& ((searchMethod && fieldDefinition.getSearchable()) || (!searchMethod && fieldDefinition
								.hasIndexedFor()))) {
					indexedDefinitions.add(fieldDefinition);
				}
				if (fieldDefinition.hasXmlAdapterType()) {
					expandableDefinitions.add(fieldDefinition);
				}
			}

			if (!indexedDefinitions.isEmpty()) {
				// provide some predictable order to the fields.
				java.util.List<FieldDefinition<?>> sortedIndexedDefinitions = FieldUtils
						.sortedByKey(indexedDefinitions);
				for (FieldDefinition<?> indexedDefinition : sortedIndexedDefinitions) {
					Parameter condition = new Parameter(indexedDefinition.getKey().getAbsoluteKey(),
							scala.Option.apply(String.valueOf(indexedDefinition.getDescription())),
							scala.Option.apply(StringUtils.EMPTY), Required.FALSE, AllowMultiple.FALSE,
							indexedDefinition.getType().getName(), allowableValues(indexedDefinition.getType()),
							DocSupport.ParamType.QUERY, scala.Option.apply(StringUtils.EMPTY));
					parameters.add(condition);

				}

				// search doesn't offer sorting...add those manually
				if (!searchMethod) {
					// now add a single parameter offering to sort by any indexed field (+/-)
					String dataType = FilterSort.class.getName();
					java.util.List<FilterSort> bothDirections = FilterUtil.bothDirections(FieldUtils
							.keys(sortedIndexedDefinitions));

					Iterable<Serializable> withNone = Iterables.concat(	Lists.newArrayList(FilterSort.NONE_FIELD),
																		bothDirections);
					AllowableValues allowableValues = SwaggerUtil.allowableValues(withNone);

					Parameter filterSortParameter = new Parameter(Filter.FIELD.ORDER_BY, scala.Option.apply(String
							.valueOf(PersistMessage.Doc.FILTER_ORDER_BY_DESCRIPTION)),
							scala.Option.apply(StringUtils.EMPTY), Required.FALSE, AllowMultiple.FALSE, dataType,
							allowableValues, DocSupport.ParamType.QUERY, scala.Option.apply(StringUtils.EMPTY));
					parameters.add(filterSortParameter);
				}
			}
			// add the expand parameter if any expandable fields were detected
			if (!expandableDefinitions.isEmpty()) {
				Parameter expandable = expandParameter(expandableDefinitions);
				parameters.add(expandable);
			}
		}
		return parameters.build();

	}

	/**
	 * @param expandableDefinitions
	 * @return
	 */
	public Parameter expandParameter(ArrayList<FieldDefinition<?>> expandableDefinitions) {
		Iterable<FieldKey> keys = FieldUtils.keys(FieldUtils.sortedByKey(expandableDefinitions));
		final AllowableValues allowableValues = SwaggerUtil.allowableValues(keys);
		Parameter expandable = new Parameter(Filter.FIELD.EXPAND,
				scala.Option.apply(PersistMessage.Doc.EXPAND_DESCRIPTION), scala.Option.apply(StringUtils.EMPTY),
				Required.FALSE, AllowMultiple.TRUE, PersistMessage.Doc.FILTER_EXPAND_DATA_TYPE, allowableValues,
				DocSupport.ParamType.QUERY, scala.Option.apply(StringUtils.EMPTY));
		return expandable;
	}

	/**
	 * Provides an annotation with overrides from the default given the context of the call,
	 * otherwise just passes the given operation values through.
	 * 
	 * Response has a bug where it requires us to declare what it can already find out
	 * 
	 * @see ApiResponse#response()
	 * 
	 * @param apiOperation
	 * @param method
	 * @return
	 */
	public ApiOperation apiOperationOverride(final ApiOperation apiOperation, final Method method) {
		ApiOperation apiOperationOverride = new ApiOperation() {

			@Override
			public Class<? extends Annotation> annotationType() {
				return apiOperation.annotationType();
			}

			@Override
			public String value() {
				return apiOperation.value();
			}

			@Override
			public String tags() {
				return apiOperation.tags();
			}

			@Override
			public String responseContainer() {
				return apiOperation.responseContainer();
			}

			@Override
			public Class<?> response() {
				// there is a bug where if the response isn't provided it's just null.
				Class<?> response = apiOperation.response();
				if (Void.class.isAssignableFrom(response)) {
					response = findSubresourceType(method);
				}
				return response;
			}

			@Override
			public String protocols() {
				return apiOperation.protocols();
			}

			@Override
			public String produces() {
				return apiOperation.produces();
			}

			@Override
			public int position() {
				return apiOperation.position();
			}

			@Override
			public String notes() {
				return apiOperation.notes();
			}

			@Override
			public String nickname() {
				return apiOperation.nickname();
			}

			@Override
			public String httpMethod() {
				return apiOperation.httpMethod();
			}

			@Override
			public boolean hidden() {
				return apiOperation.hidden();
			}

			@Override
			public String consumes() {
				return apiOperation.consumes();
			}

			@Override
			public Authorization[] authorizations() {
				return apiOperation.authorizations();
			}
		};
		return apiOperationOverride;
	}

	/*
	 * @see
	 * com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader#findSubresourceType(java.lang.reflect
	 * .Method)
	 */
	@Override
	public Class<?> findSubresourceType(Method method) {
		final Class<?> type = super.findSubresourceType(method);
		return type;

	}

	public static class Public
			extends ApiSwaggerClassReader {
		public Public() {
			super(ApiSwaggerSpecFilter.PUBLIC);
		}
	}

}
