/**
 * 
 */
package com.aawhere.web.api.app;

import static com.aawhere.app.ApplicationWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.activity.ActivityProcessService;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationMessage;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.app.account.AccountProcessService;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilteredResponse;
import com.aawhere.swagger.DocSupport;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * You know, specializes in {@link Application}s which is managed by an Enum
 * {@link KnownApplication}.
 * 
 * @author aroller
 * 
 */

abstract public class ApplicationWebApi {

	final protected AccountProcessService accountProcessService;
	final protected ActivityProcessService activityProcessService;

	public ApplicationWebApi(AccountProcessService accountProcessService, ActivityProcessService activityProcessService) {
		this.accountProcessService = accountProcessService;
		this.activityProcessService = activityProcessService;
	}

	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "All Applications (no filtering).", response = Application.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public FilteredResponse<Application> applicationsGet() throws UnknownApplicationException {
		return FilteredResponse.create(KnownApplication.all()).build();
	}

	@GET
	@Path(SLASH + APPLICATION_KEY_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "A single Applicaation identified by it's ApplicationKey.", response = Application.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	@ApiImplicitParams({ @ApiImplicitParam(value = "Show selective fields.", name = Filter.FIELD.EXPAND,
			allowMultiple = true, allowableValues = ApplicationMessage.Doc.ALLOWABLE,
			dataType = DocSupport.DataType.STRING, paramType = DocSupport.ParamType.QUERY) })
	public Application applicationGet(@PathParam(APPLICATION_KEY) ApplicationKey applicationKey)
			throws UnknownApplicationException {
		return KnownApplication.byKey(applicationKey);
	}

}
