package com.aawhere.web.api;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.ext.Provider;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringAdapterFactory;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.spi.StringReader;
import com.sun.jersey.spi.StringReaderProvider;

/**
 * Ties together our {@link StringAdapterFactory} and {@link StringAdapter} workers with Jersey's
 * {@link StringReaderProvider} and {@link StringReader} to provide better adaptation of strings
 * into parameters. TN-868
 * 
 * @see StringAdapterFactory
 * @see StringAdapter
 * @see StringReaderProvider
 * @see StringReader
 * @author aroller
 * 
 */
@Provider
@Singleton
public class StringAdapterStringReaderProvider
		implements StringReaderProvider<Object> {

	private StringAdapterFactory stringAdapterFactory;

	@Inject
	public StringAdapterStringReaderProvider(StringAdapterFactory singleton) {
		this.stringAdapterFactory = singleton;

	}

	@Override
	public StringReader<Object> getStringReader(final Class<?> type, final Type genericType, Annotation[] annotations) {
		final StringAdapter<Object> adapter = this.stringAdapterFactory.getAdapterOrNull(genericType, type);
		if (adapter == null) {
			return null;
		} else {
			// notice these don't work well as singletons because the adapter requires a type which
			// comes from the request
			return new StringReader<Object>() {

				@Override
				public Object fromString(String value) {
					try {
						return adapter.unmarshal(value, type);
					} catch (BaseException e) {
						throw ToRuntimeException.wrapAndThrow(e);
					}
				}

			};
		}
	}
}
