package com.aawhere.web.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.aawhere.identity.IdentityFactory;
import com.aawhere.identity.IdentityManager;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;
import com.sun.jersey.core.provider.jaxb.AbstractJAXBProvider;

/**
 * Enforces security for any object being transformed that matches
 * {@link IdentityManager#isAccessControlled(Class)} by calling
 * {@link IdentityManager#enforceAccess(Object, IdentityFactory)}. This essentially acts as a filter
 * nulling values that are not authorized to be shown.
 * 
 * {@link EntitiesMessageBodyWriter} also performs this action against the collections.
 * 
 * @author aroller
 * 
 */
public class AccessControlledMessageBodyWriter
		extends AbstractJAXBProvider<Object> {

	@Inject
	private IdentityFactory identityFactory;
	@Inject
	private Injector injector;

	public AccessControlledMessageBodyWriter(Providers ps, MediaType mediaType) {
		super(ps, mediaType);
	}

	@Provider
	@Produces(MediaType.APPLICATION_XML)
	public static class Xml
			extends AccessControlledMessageBodyWriter {
		public Xml(@Context Providers ps, @Context UriInfo uriInfo) {
			super(ps, MediaType.APPLICATION_XML_TYPE);
		}
	}

	@Provider
	@Produces(MediaType.APPLICATION_JSON)
	public static class Json
			extends AccessControlledMessageBodyWriter {
		public Json(@Context Providers ps, @Context UriInfo uriInfo) {
			super(ps, MediaType.APPLICATION_JSON_TYPE);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.web.api.EntitiesMessageBodyWriter#writeTo(com.aawhere.persist.BaseEntities,
		 * javax.xml.bind.Marshaller, java.io.OutputStream)
		 */
		@Override
		protected void writeTo(Object t, Marshaller m, OutputStream entityStream) throws JAXBException {
			// copied from RootElementJSONProvider
			JSONMarshaller jsonMarshaller = JSONJAXBContext.getJSONMarshaller(m, getJAXBContext(t.getClass()));
			if (isFormattedOutput())
				jsonMarshaller.setProperty(JSONMarshaller.FORMATTED, true);
			jsonMarshaller.marshallToJSON(t, new OutputStreamWriter(entityStream,
					getCharset(MediaType.APPLICATION_JSON_TYPE)));
		}
	}

	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return false;
	}

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		boolean contains = IdentityManager.isAccessControlled(type);
		return contains;

	}

	@Override
	public Object readFrom(Class<Object> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException,
			WebApplicationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void writeTo(Object t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException,
			WebApplicationException {
		IdentityManager identityManager = injector.getInstance(IdentityManager.class);
		identityManager.enforceAccess(t);
		try {
			writeTo(t, getMarshaller(type, mediaType), entityStream);
		} catch (JAXBException e) {
			throw new WebApplicationException(e);
		}
	}

	/**
	 * @param t
	 * @param m
	 * @param entityStream
	 * @throws JAXBException
	 */
	protected void writeTo(Object t, Marshaller m, OutputStream entityStream) throws JAXBException {
		m.marshal(t, entityStream);
	}

}
