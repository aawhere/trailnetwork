/**
 * 
 */
package com.aawhere.web.api;

import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionaries;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.util.rb.MessageFactory;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.common.collect.BiMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Provides information describing the entire system not specific to any single domain.
 * 
 * @author aroller
 * 
 */
@Singleton
@Path(SYSTEM)
@Api(value = SLASH + SYSTEM, description = "Operational information about the API.",
		position = ApiDocumentationSupport.Position.SYSTEM)
public class SystemWebApi {

	@Inject
	private FieldDictionaryFactory dictionaryFactory;

	/**
	 * @deprecated use {@link #dictionaries()}
	 * @return
	 */
	@GET
	@Path(SLASH + DICTIONARY)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML })
	@ApiOperation(value = "Deprecated.  Use " + SLASH + DICTIONARIES, response = FieldDictionaries.class)
	@Deprecated
	public FieldDictionaries getDictionary() {
		return dictionaryFactory.getAllDictionaries();
	}

	@GET
	@Path(SLASH + DICTIONARIES)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML })
	@ApiOperation(value = "All FieldDictionaries in the system.", response = FieldDictionaries.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public FieldDictionaries dictionaries() throws FieldNotRegisteredException {
		return dictionaryFactory.getAllDictionaries();
	}

	@GET
	@Path(SLASH + DICTIONARIES_DOMAIN_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML })
	@ApiOperation(value = "The FieldDictionary for a single domain (or model).", response = FieldDictionary.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public FieldDictionary dictionaries(@PathParam(DOMAIN) String domain) throws FieldNotRegisteredException {
		return dictionaryFactory.getDictionary(domain);
	}

	@SuppressWarnings("rawtypes")
	@GET
	@Path(SLASH + FIELDS_FIELD_KEY_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML })
	@ApiOperation(value = "FieldDefinition for the given FieldKey", response = FieldDefinition.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public FieldDefinition field(@PathParam(FIELD_KEY) String absoluteFieldKey) throws FieldNotRegisteredException {
		return dictionaryFactory.findDefinition(absoluteFieldKey);
	}

	@GET
	@Path(SLASH + "messages")
	@Produces(MediaType.TEXT_PLAIN)
	@ApiOperation(value = "I18n messages used in the system during responses.")
	public String getMessages() throws Exception {
		BiMap<String, Enum<?>> all = MessageFactory.getInstance().getAll();
		return all.toString();
	}

	@GET
	@Produces({ MediaType.TEXT_PLAIN })
	@ApiOperation(value = "Details about this version and build of the API.", response = String.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public String versionGet() {
		return SystemWebApiUri.info();
	}
}
