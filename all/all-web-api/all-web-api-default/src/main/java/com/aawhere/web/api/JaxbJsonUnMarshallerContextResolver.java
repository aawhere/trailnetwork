/**
 *
 */
package com.aawhere.web.api;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.aawhere.personalize.Personalizer;

import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.json.impl.JSONUnmarshallerImpl;

/**
 * Our interception of the JAXB processing done by Jersey. This will use {@link Personalizer}s to
 * localize and i18n according to the user preferences from their HTTP header or their user account.
 * 
 * @author Aaron Roller
 * 
 */
@Provider
@Produces({ MediaType.APPLICATION_JSON })
public class JaxbJsonUnMarshallerContextResolver
		implements ContextResolver<Unmarshaller> {

	public JaxbJsonUnMarshallerContextResolver() {
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.ContextResolver#getContext(java.lang.Class)
	 */
	@Override
	public Unmarshaller getContext(Class<?> type) {
		JAXBContext context;
		try {
			context = WebApiApplication.getJaxBContext();
			return (new JSONUnmarshallerImpl(context, JSONConfiguration.natural().build()));
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}
}
