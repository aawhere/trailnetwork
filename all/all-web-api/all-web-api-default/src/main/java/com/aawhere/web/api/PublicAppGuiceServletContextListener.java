package com.aawhere.web.api;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 * The hook into the system to configure public facing system.
 * 
 * @see PublicWebApiApplication
 * @see AdminAppGuiceServletContextListener
 * @author aroller
 * 
 */
public class PublicAppGuiceServletContextListener
		extends GuiceServletContextListener {

	public PublicAppGuiceServletContextListener() {
	}

	@Override
	protected Injector getInjector() {
		return Guice.createInjector(PublicWebApiApplication.getModules());
	}
}