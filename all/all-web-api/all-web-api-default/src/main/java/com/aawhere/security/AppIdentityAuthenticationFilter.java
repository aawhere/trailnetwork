/**
 *
 */
package com.aawhere.security;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.aawhere.identity.IdentityManagerProvider;
import com.aawhere.identity.IdentityRole;
import com.google.appengine.labs.repackaged.com.google.common.collect.Sets;
import com.google.inject.Singleton;
import com.googlecode.charts4j.collect.Lists;

/**
 * A Servlet Fitler to determine if we are talking to a trusted app or not.
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class AppIdentityAuthenticationFilter
		implements Filter {

	final static String APPID_HEADER_NAME = IdentityManagerProvider.APPID_HEADER_NAME;
	private final Set<String> trustedAppIds = Sets.newHashSet("tn-live");
	private final List<String> trustedSecurityRoles = Lists.newArrayList();

	{
		trustedSecurityRoles.add(IdentityRole.Name.TRUSTED_APP);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
			ServletException {

		HttpServletRequest httpReq = (HttpServletRequest) req;
		String appId = httpReq.getHeader(APPID_HEADER_NAME);

		if (trustedAppIds.contains(appId)) {
			// We're from a trusted server
			String accountId = httpReq.getHeader(IdentityManagerProvider.ACCOUNT_ID_HEADER_NAME);
			httpReq = TnServletRequest.create(httpReq).userId(accountId).roles(trustedSecurityRoles)
					.authScheme(AuthScheme.TN_LIVE).build();
			httpReq.setAttribute(IdentityManagerProvider.ACCOUNT_ID_HEADER_NAME, accountId);
		}

		chain.doFilter(httpReq, res);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

}