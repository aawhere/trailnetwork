/**
 *
 */
package com.aawhere.personalize;

import java.util.Locale;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Allows for injection of the Locale derived from the current request.
 * 
 * @author Brian Chapman
 * 
 */
public class LocaleProvider
		implements Provider<Locale> {

	private HttpHeaders requestHeaders;

	@Inject
	public LocaleProvider(@Context HttpHeaders requestHeaders) {
		this.requestHeaders = requestHeaders;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.Provider#get()
	 */
	@Override
	public Locale get() {
		return PersonalizeUtil.locale(requestHeaders);
	}

}
