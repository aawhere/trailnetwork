/**
 * 
 */
package com.aawhere.web.api.doc;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.persist.Filter;
import com.aawhere.swagger.DocSupport;
import com.aawhere.web.api.ApiDocumentationSupport;

import com.wordnik.swagger.core.filter.SwaggerSpecFilter;
import com.wordnik.swagger.model.ApiDescription;
import com.wordnik.swagger.model.Operation;
import com.wordnik.swagger.model.Parameter;

/**
 * General filter used to ignore anything that can't be ignored by means of a hidden attribute.
 * 
 * This is a one stop shop since the maven plugin only supports a single entry.
 * 
 * @author aroller
 * 
 */
public class ApiSwaggerSpecFilter
		implements SwaggerSpecFilter {

	public static final boolean PUBLIC = true;

	public static final boolean PRIVATE = !PUBLIC;

	/**
	 * Public api indicates only those marked with position !=
	 * {@link ApiDocumentationSupport.Position#PRIVATE} are included.
	 */
	private boolean publicApi = false;

	/**
	 * 
	 */
	public ApiSwaggerSpecFilter() {

	}

	/**
	 * Allows an implementing class to override the default display of all methods to create a
	 * simpler api docs.
	 * 
	 * @param publicApi
	 */
	protected ApiSwaggerSpecFilter(Boolean publicApi) {
		this.publicApi = BooleanUtils.isTrue(publicApi);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.wordnik.swagger.core.filter.SwaggerSpecFilter#isOperationAllowed(com.wordnik.swagger.
	 * model.Operation, com.wordnik.swagger.model.ApiDescription, java.util.Map, java.util.Map,
	 * java.util.Map)
	 */
	@Override
	public boolean isOperationAllowed(Operation operation, ApiDescription arg1, Map<String, List<String>> arg2,
			Map<String, String> arg3, Map<String, List<String>> arg4) {

		boolean allowed = true;
		// TN-827 limit to those declared as public.
		if (this.publicApi) {
			if (operation.position() == ApiDocumentationSupport.Position.PRIVATE) {
				allowed = false;
			}
			if (operation.deprecated() != null && operation.deprecated().nonEmpty()
					&& Boolean.TRUE.toString().equals(operation.deprecated().get())) {
				// Should this always be displayed in the private API? TN-827
				allowed = false;
			}
		}
		return allowed;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.wordnik.swagger.core.filter.SwaggerSpecFilter#isParamAllowed(com.wordnik.swagger.model
	 * .Parameter, com.wordnik.swagger.model.Operation, com.wordnik.swagger.model.ApiDescription,
	 * java.util.Map, java.util.Map, java.util.Map)
	 */
	@Override
	public boolean isParamAllowed(Parameter param, Operation arg1, ApiDescription arg2, Map<String, List<String>> arg3,
			Map<String, String> arg4, Map<String, List<String>> arg5) {
		if (param.dataType().equals(Filter.class.getSimpleName())) { // ignoring Filter parameters
			return false;

		} else if (param.paramAccess().nonEmpty() && DocSupport.ParamAccess.HIDDEN.equals(param.paramAccess().get())) {
			return false;
		} else if (SwaggerUtil.dataTypeWithQueryParam(param.dataType()) != null) {
			// query parameters are intercepted in ApiSwaggerClassReader
			// letting them pass now makes a json input text field
			return false;
		} else {
			return true;

		}
	}

	public static class Public
			extends ApiSwaggerSpecFilter {
		/**
		 * 
		 */
		public Public() {
			super(true);
		}
	}

}
