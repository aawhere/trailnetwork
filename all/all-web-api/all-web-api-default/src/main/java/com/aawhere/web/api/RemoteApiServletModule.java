package com.aawhere.web.api;

import com.google.apphosting.utils.remoteapi.RemoteApiServlet;
import com.google.inject.servlet.ServletModule;

/**
 * @see RemoteApiServlet
 * @author aroller
 * 
 */
public class RemoteApiServletModule
		extends ServletModule {

	@Override
	protected void configureServlets() {
		super.configureServlets();
		bind(com.google.apphosting.utils.remoteapi.RemoteApiServlet.class).toInstance(new RemoteApiServlet());
		serve("/remote_api").with(com.google.apphosting.utils.remoteapi.RemoteApiServlet.class);
	}
}
