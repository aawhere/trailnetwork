/**
 *
 */
package com.aawhere.web.api.person;

import static com.aawhere.person.PersonWebApiUri.*;
import static com.aawhere.route.RouteWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.activity.Activities;
import com.aawhere.activity.ActivityService;
import com.aawhere.all.AllProcessNestedField;
import com.aawhere.app.account.Accounts;
import com.aawhere.field.FieldMessage;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.PersistMessage;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonProcessService;
import com.aawhere.person.PersonService;
import com.aawhere.person.Persons;
import com.aawhere.person.group.SocialGroupMembershipService;
import com.aawhere.person.group.SocialGroups;
import com.aawhere.route.RouteCompletion;
import com.aawhere.route.RouteCompletionMessage;
import com.aawhere.route.RouteCompletionService;
import com.aawhere.route.RouteCompletions;
import com.aawhere.route.RouteId;
import com.aawhere.route.RouteService;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.route.event.RouteEventWebApiUri;
import com.aawhere.swagger.DocSupport;
import com.aawhere.swagger.DocSupport.Allowable;
import com.aawhere.view.ViewCount;
import com.aawhere.web.api.ApiDocumentationSupport;

import com.google.inject.Inject;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * Provides a web api interface for {@link Person} and {@link Persons} and their related
 * {@link Accounts}
 * 
 * @author aroller
 * 
 */
abstract public class PersonWebApi {

	final protected PersonService personService;
	final protected PersonProcessService personProcessService;
	final protected SocialGroupMembershipService socialGroupService;
	final protected ActivityService activityService;
	final protected RouteCompletionService completionService;
	final protected RouteService routeService;

	/**
	 *
	 */
	@Inject
	public PersonWebApi(PersonService personService, SocialGroupMembershipService socialGroupService,
			ActivityService activityService, RouteService routeService, PersonProcessService personProcessService) {
		this.personService = personService;
		this.personProcessService = personProcessService;
		this.socialGroupService = socialGroupService;
		this.activityService = activityService;
		this.routeService = routeService;
		this.completionService = routeService.getCompletionService();
	}

	@GET
	@Path(SLASH + PERSON_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Find a Person by the PersonId.", response = Person.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public Person person(@PathParam(PERSON_ID) PersonId personId) throws EntityNotFoundException {
		return this.personService.person(personId);
	}

	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Find Persons with the standard Filter.", response = Persons.class,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public Persons persons(@InjectParam Filter filter) {
		return this.personService.persons(filter);
	}

	/**
	 * TN-679
	 * 
	 * @since 27
	 * @param personId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	@GET
	@Path(SLASH + PERSON_ID_ACTIVITIES_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "FIXME:move this to ActivityWebApi Provides the Activities for the Person matching the Filter.",
			response = Activities.class, position = ApiDocumentationSupport.Position.PUBLIC)
	public Activities activities(@PathParam(PERSON_ID) PersonId personId, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return this.activityService.activities(personId, filter);
	}

	// TN-779 Fields must be constants..this kind of reference is not
	// sustainable
	@GET
	@Path(SLASH + RouteWebApiUri.PERSON_ID_ROUTES_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Provides the RouteCompletions for a Person across all Routes.",
			notes = "Allows the focus on how a Person has Completed Routes. Sorting provides results for different use cases. When sorting only Routes with a significant score is provided.\n"
					+ "- **Most Recent** sorts by Start Time descending and provides the most complete list when sorting.\n"
					+ "- **Best RaceRank** provides the completions with the lowest number for SocialRaceRank ascending for the person.\n"
					+ " - Only the PersonalBest is provided for each Route.\n"
					+ "- **Most Common** gives Routes completed the most for a person using PersonalTotal descending.\n"

			, response = RouteCompletions.class, position = ApiDocumentationSupport.Position.PUBLIC)
	@ApiImplicitParams({
			@ApiImplicitParam(name = Filter.FIELD.ORDER_BY, required = false, paramType = DocSupport.ParamType.QUERY,
					dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE,
					allowableValues = RouteCompletion.FIELD.KEY.VALUE.SOCIAL_RACE_RANK + Allowable.NEXT
							+ RouteCompletion.FIELD.KEY.VALUE.START_TIME + PersistMessage.Doc.DESCENDING
							+ Allowable.NEXT + AllProcessNestedField.ROUTE_COMPLETION__PERSONAL_TOTAL
							+ PersistMessage.Doc.DESCENDING),
			@ApiImplicitParam(name = Filter.FIELD.EXPAND, allowMultiple = true,
					allowableValues = RouteCompletionMessage.Doc.COMPLETION_EXPANDS,
					paramType = DocSupport.ParamType.QUERY, dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE) })
	public RouteCompletions completions(@ApiParam(value = "Identify the Person",
			defaultValue = ApiDocumentationSupport.EXAMPLE_PERSON_ID) @PathParam(PERSON_ID) PersonId personId,
			@InjectParam Filter filter) throws EntityNotFoundException {
		return this.completionService.completions(personId, filter);
	}

	@GET
	@Path(SLASH + RouteEventWebApiUri.PERSON_ID_ROUTES_EVENTS_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The Completions of RouteEvent for a Person.", response = RouteCompletions.class)
	public RouteCompletions completionsWithEvents(@PathParam(PERSON_ID) PersonId personId, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return this.completionService.completionsWithEvents(personId, filter);
	}

	@GET
	@Path(SLASH + RouteWebApiUri.PERSON_ID_ROUTE_ID_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "FIXME:move this to completions", response = RouteCompletions.class)
	public RouteCompletions completions(@PathParam(PERSON_ID) PersonId personId, @PathParam(ROUTE_ID) RouteId routeId,
			@InjectParam Filter filter) throws EntityNotFoundException {
		return this.routeService.completions(routeId, personId, filter);
	}

	@GET
	@Path(SLASH + PERSON_ID_VIEWS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The number of times a Person's profile page has been viewed", response = ViewCount.class)
	public ViewCount viewCountGet(@PathParam(PERSON_ID) PersonId personId) {
		return this.personService.viewCount(personId);
	}

	@GET
	@Path(SLASH + PERSON_ID_GROUPS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The SocialGroups in which the Person has a SocialGroupMembership.",
			response = SocialGroups.class)
	public SocialGroups groups(@PathParam(PERSON_ID) PersonId personId, @InjectParam Filter filter) {
		return socialGroupService.groups(personId, filter);
	}

}
