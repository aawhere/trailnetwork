/**
 *
 */
package com.aawhere.security;

import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import com.aawhere.log.LoggerFactory;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

/**
 * A logging filter used to verify the results of the {@link AppIdentityAuthenticationFilter}.
 * Manually registered to ensure order of execution.
 * 
 * @author Brian Chapman
 * 
 */
public class LoggingContainerRequestFilter
		implements ContainerRequestFilter {

	private static final Level LOG_LEVEL = Level.FINE;
	private static final Logger logger = LoggerFactory.getLogger(LoggingContainerRequestFilter.class);

	/*
	 * (non-Javadoc)
	 * @see com.sun.jersey.spi.container.ContainerRequestFilter#filter(com.sun.jersey.spi.container.
	 * ContainerRequest)
	 */
	@Override
	public ContainerRequest filter(ContainerRequest request) {
		if (isLoggable()) {
			StringBuffer message = new StringBuffer();
			message.append("ContainerRequest intercepted: " + request.getPath() + ", " + request.getMethod());
			newLine(message);

			for (Entry<String, List<String>> entry : request.getRequestHeaders().entrySet()) {
				String key = entry.getKey();
				List<String> values = entry.getValue();
				message.append("Header: " + key + " : " + StringUtils.join(values, ", "));
				newLine(message);
			}

			if (request.getSecurityContext() == null) {
				message.append("request.getSecurityContext() is null");
				newLine(message);
			}
			if (request.getSecurityContext().getUserPrincipal() == null) {
				message.append("request.getSecurityContext().getUserPrincipal() is null");
				newLine(message);
			}

			if (request.getSecurityContext() != null) {
				message.append("request.getSecurityContext().isUserInRole(SecurityRoles.ADMIN)) : "
						+ request.getSecurityContext().isUserInRole(SecurityRoles.ADMIN));
				newLine(message);
				message.append("request.getSecurityContext().isUserInRole(SecurityRoles.TRUSTED)) : "
						+ request.getSecurityContext().isUserInRole(SecurityRoles.TRUSTED_APP));
				newLine(message);
			}

			if (request.getSecurityContext() != null && request.getSecurityContext().getUserPrincipal() != null) {
				message.append("request.getSecurityContext().getUserPrincipal().toString() : "
						+ request.getSecurityContext().getUserPrincipal().toString());
				newLine(message);
				message.append("request.getSecurityContext().getUserPrincipal().getName() : "
						+ request.getSecurityContext().getUserPrincipal().getName());
				newLine(message);
			}
			if (request.getAuthenticationScheme() != null) {
				message.append("request.getAuthenticationScheme() : " + request.getAuthenticationScheme());
				newLine(message);
			}
			logger.log(LOG_LEVEL, message.toString());
		}
		return request;
	}

	public static boolean isLoggable() {
		return logger.isLoggable(LOG_LEVEL);
	}

	private void newLine(StringBuffer sb) {
		sb.append(System.getProperty("line.separator"));
	}

}
