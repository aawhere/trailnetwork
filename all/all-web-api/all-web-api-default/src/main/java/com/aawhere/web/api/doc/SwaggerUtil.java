/**
 * 
 */
package com.aawhere.web.api.doc;

import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Nullable;
import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;

import scala.Option;
import scala.collection.JavaConversions;
import scala.collection.immutable.List;

import com.aawhere.lang.annotation.AnnotationUtilsExtended;
import com.google.common.base.Functions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.model.AllowableListValues;
import com.wordnik.swagger.model.AllowableValues;

/**
 * 
 * @author aroller
 * 
 * 
 * 
 */
public class SwaggerUtil {

	/**
	 * @see #dataTypeWithQueryParam(Class)
	 * @param dataType
	 * @return the map of parameters for the members found in the bean or null if none found
	 */
	public static Map<Member, QueryParam> dataTypeWithQueryParam(String dataType) {
		try {
			return dataTypeWithQueryParam(Class.forName(dataType));
		} catch (ClassNotFoundException e) {
			return null;
		}
	}

	/**
	 * If the given datatype has query params this will return the map of the query params
	 * corresponding to the declaring member or null otherwise.
	 * 
	 * @param dataType
	 * @return the query params or null if none found
	 */
	@Nullable
	public static Map<Member, QueryParam> dataTypeWithQueryParam(Class<?> dataTypeClass) {
		// only one success path, otherwise null is returned
		Map<Member, QueryParam> queryParams = null;
		if (dataTypeClass != null) {
			ApiModel apiModel = dataTypeClass.getAnnotation(ApiModel.class);

			if (apiModel != null) {
				Map<Member, QueryParam> params = AnnotationUtilsExtended.getInstance().findMembers(	QueryParam.class,
																									dataTypeClass);
				if (!params.isEmpty()) {
					queryParams = params;
				}
			}
		}
		// default is null
		return queryParams;
	}

	/**
	 * Converts any iterable of items to it's corresponding allowable values in scala. This will
	 * tostring every item given.
	 * 
	 * @param items
	 * @return
	 */
	public static <T> AllowableValues allowableValues(Iterable<T> items) {

		final ArrayList<String> javaList = Lists.newArrayList(Iterables.transform(items, Functions.toStringFunction()));
		// precede the list with an option to choose nothing.
		javaList.add(0, StringUtils.EMPTY);

		List<String> keysList = JavaConversions.asScalaBuffer(javaList).toList();
		final AllowableValues allowableValues = new AllowableListValues(keysList, "LIST");
		return allowableValues;
	}

	/**
	 * Scrapes the class for a description at the model level and returns the option or null if no
	 * description is found.
	 * 
	 * @param withApiModelAnnotation
	 * @return
	 */
	@Nullable
	public static Option<String> descriptionFromModel(Class<?> withApiModelAnnotation) {
		Option<String> result = null;
		ApiModel modelAnnotation = withApiModelAnnotation.getAnnotation(ApiModel.class);
		if (modelAnnotation != null) {
			String description = modelAnnotation.description();
			if (description != null) {
				result = Option.apply(description);
			}
		}
		return result;
	}
}
