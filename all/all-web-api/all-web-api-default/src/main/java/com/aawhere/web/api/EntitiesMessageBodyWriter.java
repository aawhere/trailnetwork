/**
 * 
 */
package com.aawhere.web.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.xml.FieldXmlAdapterService;
import com.aawhere.identity.IdentityManager;
import com.aawhere.persist.BaseEntities;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.Filter;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;
import com.sun.jersey.core.provider.jaxb.AbstractJAXBProvider;

/**
 * A specialty collection writer used to batch collection optional elements included in multiple
 * entities improving efficiently and response times.
 * 
 * It indicates {@link #handles(Class)} types of {@link BaseEntities}. It then uses the
 * {@link FieldXmlAdapterService} to bulk inspect the collection of {@link BaseEntity} for
 * {@link Field#xmlAdapter()} that match fields in the {@link Filter.FIELD#EXPAND}.
 * 
 * @see FieldXmlAdapterService
 * 
 * @author aroller
 * 
 */
public class EntitiesMessageBodyWriter
		extends AbstractJAXBProvider<BaseEntities<?>> {

	private UriInfo uriInfo;

	@Inject
	private Injector injector;

	@Inject
	private FieldDictionaryFactory dictionaryFactory;

	/**
	 * 
	 */
	public EntitiesMessageBodyWriter(Providers ps, UriInfo uriInfo, MediaType mediaType) {
		super(ps, mediaType);
		this.uriInfo = uriInfo;
	}

	@Provider
	@Produces(MediaType.APPLICATION_XML)
	public static class Xml
			extends EntitiesMessageBodyWriter {
		public Xml(@Context Providers ps, @Context UriInfo uriInfo) {
			super(ps, uriInfo, MediaType.APPLICATION_XML_TYPE);
		}
	}

	@Provider
	@Produces(MediaType.APPLICATION_JSON)
	public static class Json
			extends EntitiesMessageBodyWriter {
		public Json(@Context Providers ps, @Context UriInfo uriInfo) {
			super(ps, uriInfo, MediaType.APPLICATION_JSON_TYPE);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.web.api.EntitiesMessageBodyWriter#writeTo(com.aawhere.persist.BaseEntities,
		 * javax.xml.bind.Marshaller, java.io.OutputStream)
		 */
		@Override
		protected void writeTo(BaseEntities<?> t, Marshaller m, OutputStream entityStream) throws JAXBException {
			// copied from RootElementJSONProvider
			JSONMarshaller jsonMarshaller = JSONJAXBContext.getJSONMarshaller(m, getJAXBContext(t.getClass()));
			if (isFormattedOutput()) {
				jsonMarshaller.setProperty(JSONMarshaller.FORMATTED, true);
			}
			jsonMarshaller.marshallToJSON(t, new OutputStreamWriter(entityStream,
					getCharset(MediaType.APPLICATION_JSON_TYPE)));
		}
	}

	/**
	 * is writeable if it extends {@link BaseEntities}.
	 * 
	 * @see javax.ws.rs.ext.MessageBodyWriter#isWriteable(java.lang.Class, java.lang.reflect.Type,
	 *      java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType)
	 */
	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return handles(type);
	}

	/**
	 * @param type
	 * @return
	 */
	public static Boolean handles(Class<?> type) {
		return BaseEntities.class.isAssignableFrom(type);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.MessageBodyReader#isReadable(java.lang.Class, java.lang.reflect.Type,
	 * java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType)
	 */
	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.MessageBodyReader#readFrom(java.lang.Class, java.lang.reflect.Type,
	 * java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType,
	 * javax.ws.rs.core.MultivaluedMap, java.io.InputStream)
	 */
	@Override
	public BaseEntities<?> readFrom(Class<BaseEntities<?>> type, Type genericType, Annotation[] annotations,
			MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException, WebApplicationException {
		throw new UnsupportedOperationException("this class is only for writing");
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.MessageBodyWriter#writeTo(java.lang.Object, java.lang.Class,
	 * java.lang.reflect.Type, java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType,
	 * javax.ws.rs.core.MultivaluedMap, java.io.OutputStream)
	 */
	@Override
	public void writeTo(BaseEntities<?> t, Class<?> type, Type genericType, Annotation[] annotations,
			MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
			throws IOException, WebApplicationException {
		IdentityManager identityManager = injector.getInstance(IdentityManager.class);
		try {
			final List<String> options = this.uriInfo.getQueryParameters().get(Filter.FIELD.EXPAND);
			Marshaller m = getMarshaller(type, mediaType);
			// the adapter service modifies the marshaller directly
			// TN-884 add access control for those objects being included via the injector
			FieldXmlAdapterService.create().dictionaryFactory(dictionaryFactory).injector(injector)
					.fieldsRequested(options).marshaller(m).entities(t).buildWithExceptions();
			// access control for top level entities
			identityManager.enforceAccessNow(t);
			writeTo(t, m, entityStream);
		} catch (JAXBException e) {
			throw new WebApplicationException(e);
		} catch (Exception e) {
			// throwing exceptions here doesn't get handled by ExceptionMapper.
			throw BaseExceptionMapper.makeWebApplicationException(e);
		}
	}

	/**
	 * @param t
	 * @param m
	 * @param entityStream
	 * @throws JAXBException
	 */
	protected void writeTo(BaseEntities<?> t, Marshaller m, OutputStream entityStream) throws JAXBException {
		m.marshal(t, entityStream);
	}

}
