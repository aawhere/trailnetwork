package com.aawhere.web.api;

import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.reflections.Reflections;

import com.aawhere.activity.ActivityExportModule;
import com.aawhere.activity.ActivityType;
import com.aawhere.activity.imprt.csv.CsvActivityDocumentProducer;
import com.aawhere.all.process.MapReduceServletModule;
import com.aawhere.all.process.PipelineServletModule;
import com.aawhere.di.ActivityImportModule;
import com.aawhere.di.ActivityModule;
import com.aawhere.di.ActivityTimingRelationModule;
import com.aawhere.di.AppstatsFilterModule;
import com.aawhere.di.CacheModule;
import com.aawhere.di.DocumentModule;
import com.aawhere.di.IdentityModule;
import com.aawhere.di.SearchModule;
import com.aawhere.di.TrackModule;
import com.aawhere.di.TrailheadModule;
import com.aawhere.doc.text.TextDocumentProducer;
import com.aawhere.doc.track.axm_1_0.AxmDocumentProducer;
import com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.json.JsonUtils;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ResourceUtils;
import com.aawhere.lang.string.StringAdapterModule;
import com.aawhere.log.LoggerFactory;
import com.aawhere.queue.GaeQueueModule;
import com.aawhere.queue.QueueModule;
import com.aawhere.route.RouteModule;
import com.aawhere.util.rb.MessageFactory;
import com.aawhere.ws.rs.JerseyModule;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.ws.rs.WebApplication;
import com.aawhere.xml.bind.JaxbUtil;
import com.google.inject.Module;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.json.JSONJAXBContext;

/**
 * Used to register APplication Resources and {@link Provider}s with the Jersey Servlet.
 * 
 * Enter required classes in the {@link AppServletModule}
 * 
 * @author Brian Chapman
 */
@ApplicationPath(SystemWebApiUri.SLASH)
public class WebApiApplication
		extends WebApplication {

	/**
	 * The single declaration of packages to be scanned by this class's parent
	 * {@link PackagesResourceConfig}. see static initializer.
	 */
	public static final String[] PACKAGES;

	static {
		registerDocumentProducers();
		registerMessageEnums();
		Properties properties = ResourceUtils.loadProperties(WebApiApplication.class);
		String packagesPropertyName = "packages";
		String packagesProperty = properties.getProperty(packagesPropertyName);
		Assertion.assertNotNull(packagesPropertyName, packagesProperty);
		// the ; separator matches maven swagger plugin's locations property
		// https://github.com/kongchen/swagger-maven-plugin#required-parameters
		PACKAGES = StringUtils.split(packagesProperty, ";");
	}

	/*
	 * Load pre-compiled annotation data. You can also use a isProduction() method to determine if
	 * it is a production or released artifact to collect compiled classes and otherwise use runtime
	 * scanning for quicker development cycles.
	 */
	private static final Reflections reflections;

	public static final Class<?>[] JAXB_CLASSES;

	public static final String JERSEY_PATH = SystemWebApiUri.ROOT_CONTEXT;
	private static final String JERSEY_SERVLET_FILTER = JERSEY_PATH + "/*";

	private static JAXBContext jaxbContext;
	private static JSONJAXBContext jsonJaxbContext;

	static {

		reflections = Reflections.collect();
		JAXB_CLASSES = JaxbUtil.getJaxBClasses(reflections);
		// Useful for troubleshooting
		Logger logger = LoggerFactory.getLogger(WebApiApplication.class);
		logger.info("Num Jaxb Classes: " + JAXB_CLASSES.length);
		if (logger.isLoggable(Level.FINEST)) {
			logger.fine("Jaxb Classes: " + StringUtils.join(JAXB_CLASSES, System.lineSeparator()));
		}
	}

	/** Provides all common modules for all apps. */
	public static Module[] getModules() {
		Class<WebApiApplication> applicationType = WebApiApplication.class;
		return getModules(applicationType);
	}

	/**
	 * Provides common modules for all apps and creates the JerseyModule for the app given.
	 * 
	 * @param applicationType
	 * @return
	 */
	public static Module[] getModules(Class<? extends WebApiApplication> applicationType) {
		return new Module[] { /* controlled via system property, see AppstatsFilter */
				new AppstatsFilterModule(),
				JerseyModule.create().application(applicationType).path(JERSEY_SERVLET_FILTER).build(),
				// TODO:initialize remote api with a system property
				new RemoteApiServletModule(), new AppServletModule(), new TrackModule(), new DocumentModule(),
				new IdentityModule(), new ActivityTimingRelationModule(), new ActivityModule(),
				new ActivityExportModule(), new TrailheadModule(), new RouteModule(), new ActivityImportModule(),
				new QueueModule(), new GaeQueueModule(), new CacheModule(), new StringAdapterModule(),
				new SearchModule(),
				// although the pipeline and map reduce are for processing, it is useful to report
				// status on a server not consumed by the processing
				new PipelineServletModule(), new MapReduceServletModule() };
	}

	/**
	 * @deprecated this is moved to {@link ActivityImportModule}.
	 */
	@Deprecated
	public static void registerDocumentProducers() {
		TextDocumentProducer.register();
		GpxDocumentProducer.register();
		AxmDocumentProducer.register();
		com.aawhere.doc.DocumentFactory.getInstance().register(new CsvActivityDocumentProducer.Builder().build());
	}

	/**
	 * Register all implementing classes of Message in the MessageFactory
	 */
	private static void registerMessageEnums() {
		// TODO: use reflections to find these. The difficulty is the strictness of the register
		// class and getting the type checking to accept the class.
		MessageFactory.getInstance().register(ActivityType.class);
	}

	/**
	 * Produces the package string that Jersey uses to search for Providers
	 * 
	 * @param classes
	 * @return
	 */
	public static String[] packages() {
		return PACKAGES;
	}

	/**
	 * Create a global, cached, context for use by the system.
	 * 
	 * @return
	 */
	public static JAXBContext getJaxBContext() {
		if (jaxbContext == null) {
			try {
				jaxbContext = JAXBContext.newInstance(WebApiApplication.JAXB_CLASSES);
			} catch (JAXBException e) {
				throw new RuntimeException(e);
			}
		}
		return jaxbContext;
	}

	/**
	 * Create a global, cached, context for use by the system.
	 * 
	 * @return
	 */
	public static JSONJAXBContext getJsonJaxBContext() {
		if (jsonJaxbContext == null) {
			try {
				jsonJaxbContext = JsonUtils.getJSONJAXBContext(WebApiApplication.JAXB_CLASSES);
			} catch (JAXBException e) {
				throw new RuntimeException(e);
			}
		}
		return jsonJaxbContext;
	}

	public static void registerFieldAnnotations(FieldAnnotationDictionaryFactory factory) {
		Set<Class<?>> dictionaries = reflections.getTypesAnnotatedWith(Dictionary.class);
		for (Class<?> dictionary : dictionaries) {
			factory.getDictionary(dictionary);
		}
	}

	public WebApiApplication() {
		super(packages());
		// Do initialization in the #init method
	}

	/**
	 * allows implementors to pass custom packages.
	 * 
	 * @param packages
	 */
	protected WebApiApplication(String... packages) {
		super(packages);
	}

}