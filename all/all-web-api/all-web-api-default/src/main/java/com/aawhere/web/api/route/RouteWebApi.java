/**
 *
 */
package com.aawhere.web.api.route;

import static com.aawhere.activity.ActivityWebApiUri.*;
import static com.aawhere.route.RouteWebApiUri.*;
import static com.aawhere.route.event.RouteEventWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityType;
import com.aawhere.app.ApplicationKey;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldMessage;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.person.Persons;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroups;
import com.aawhere.route.Route;
import com.aawhere.route.RouteCompletion;
import com.aawhere.route.RouteCompletionId;
import com.aawhere.route.RouteCompletionMessage;
import com.aawhere.route.RouteCompletionService;
import com.aawhere.route.RouteCompletions;
import com.aawhere.route.RouteExportService;
import com.aawhere.route.RouteField;
import com.aawhere.route.RouteId;
import com.aawhere.route.RouteMessage;
import com.aawhere.route.RouteProcessService;
import com.aawhere.route.RouteService;
import com.aawhere.route.Routes;
import com.aawhere.route.event.RouteEvent;
import com.aawhere.route.event.RouteEventField;
import com.aawhere.route.event.RouteEventFilterBuilder;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventMessage;
import com.aawhere.route.event.RouteEventService;
import com.aawhere.route.event.RouteEvents;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.swagger.DocSupport;
import com.aawhere.view.ViewCount;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.ws.rs.MediaTypes;

import com.google.inject.Inject;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

/**
 * @author aroller
 */
abstract public class RouteWebApi {

	final protected RouteService service;
	final protected RouteExportService exportService;
	final protected RouteCompletionService completionService;
	final protected RouteEventService eventService;
	final protected RouteProcessService routeProcessService;

	/**
	 *
	 */
	@Inject
	public RouteWebApi(RouteService routeService, RouteExportService exportService,
			RouteCompletionService completionService, RouteEventService routeEventService,
			RouteProcessService routeProcessService) {
		this.service = routeService;
		this.exportService = exportService;
		this.completionService = completionService;
		this.eventService = routeEventService;
		this.routeProcessService = routeProcessService;
	}

	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Search for Routes by Location, Name, Person and Activity Type",
			notes = RouteField.Doc.RELATIVE_COMPLETION_STATS_JAVADOC, tags = SearchWebApiUri.SEARCH,
			position = ApiDocumentationSupport.Position.PUBLIC)
	public Routes routesGet(@InjectParam Filter filter) {
		Routes routes = service.routes(filter);
		return routes;
	}

	@Path(SLASH + SearchWebApiUri.SEARCH)
	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Search for Routes requesting specific fields",
			notes = "An efficient way to find limited Route (and related) information by requesting the specific fields.",
			tags = SearchWebApiUri.SEARCH, position = ApiDocumentationSupport.Position.PUBLIC, response = Routes.class)
	public
			SearchDocuments searchDocumentsGet(@InjectParam Filter filter) {
		// TODO: add fields that can be requested (use @Searchable annotation)
		// TODO: automatically tag as search if Searchable is true
		// TODO: Use Response for the suggested type, but actually pass through search documents
		return service.searchDocuments(filter);
	}

	@GET
	@Path(SLASH + ROUTE_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Retrieves a Route by RouteId", notes = "Standard access of a Route by it's RouteId.",
			response = Route.class, position = 1)
	@ApiResponses({ @ApiResponse(code = EntityNotFoundException.CODE, message = EntityNotFoundException.Doc) })
	@ApiImplicitParams({ @ApiImplicitParam(name = Filter.FIELD.EXPAND, allowMultiple = true,
			allowableValues = RouteMessage.Doc.ALLOWABLE_VALUES,

			paramType = DocSupport.ParamType.QUERY, dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE), })
	public Route get(@PathParam(ROUTE_ID) @ApiParam(defaultValue = ApiDocumentationSupport.EXAMPLE_ROUTE_ID,
			required = true, value = RouteMessage.Doc.Id.DESCRIPTION) RouteId routeId) throws EntityNotFoundException {
		return service.route(routeId);
	}

	// @GET
	// @Path(SLASH + ROUTE_ID_PARAM + "/description")
	// @Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML })
	// public RouteDescription routeDescription(@PathParam(ROUTE_ID) RouteId
	// routeId, @Context
	// HttpHeaders requestHeaders)
	// throws EntityNotFoundException {
	// Locale locale = requestHeaders.getAcceptableLanguages().get(0);
	// return service.routeDescription(routeId, locale);
	// }

	@GET
	@Path(SLASH + ROUTE_ID_PARAM)
	@Produces(MediaTypes.SVG_IMAGE)
	@ApiOperation(
			value = "Static SVG map with route path overlayed",
			notes = "SVG object with a static image and route path overlayed. To embed in HTML use the form &lt;object type=\"image/svg+xml\" data=\"url\"&gt;&lt;/object&gt;",
			response = String.class)
	@ApiResponses({ @ApiResponse(code = EntityNotFoundException.CODE, message = EntityNotFoundException.Doc) })
	public
			String getSvgMap(@PathParam(ROUTE_ID) @ApiParam(defaultValue = ApiDocumentationSupport.EXAMPLE_ROUTE_ID,
					required = true, value = RouteMessage.Doc.Id.DESCRIPTION) RouteId routeId)
					throws EntityNotFoundException {
		return exportService.mapSvg(routeId);
	}

	@GET
	@Path(SLASH + ROUTE_ID_COURSE_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The best example of a Route at an Application", response = Activity.class)
	public Activity applicationActivityGet(@PathParam(ROUTE_ID) RouteId routeId,
			@QueryParam(APPLICATION_KEY) ApplicationKey applicationKey) throws EntityNotFoundException {
		return service.courseRepresentative(routeId, applicationKey);
	}

	@GET
	@Path(SLASH + ROUTE_ID_PARAM)
	@Produces(GeoWebApiUri.POLYLINE_ENCODED_MEDIA_TYPE)
	@ApiOperation(value = "DOCME: Documentation Coming Soon...", response = String.class)
	public String polylineGet(@PathParam(ROUTE_ID) RouteId routeId) throws EntityNotFoundException {
		return exportService.gpolyline(routeId);
	}

	@GET
	@Path(SLASH + ROUTE_ID_ALTERNATES_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Similar Routes to the given",
			notes = "Allowing filtering by Person, ActivityType, Route Names and bounds the results will be returned with most popular first.",
			tags = SearchWebApiUri.SEARCH)
	public
			Routes alternates(@PathParam(ROUTE_ID) RouteId routeId, @InjectParam Filter filter)
					throws EntityNotFoundException {
		return service.routesForMain(routeId, filter);
	}

	@GET
	@Path(SLASH + ROUTE_ID_EVENTS_EVENT_ID_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Participations of the RouteEvent identfied.", response = RouteCompletions.class)
	public RouteCompletions eventCompletionsGet(@PathParam(ROUTE_ID) RouteId routeId,
			@PathParam(EVENT_ID) String eventIdValue, @InjectParam Filter filter) throws EntityNotFoundException {
		return this.completionService.completions(new RouteEventId(routeId, eventIdValue), filter);
	}

	// ======================= COMPLETIONS ===================================

	@GET
	@Path(SLASH + COMPLETIONS)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Activities that completed a Route.", response = RouteCompletions.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = Filter.FIELD.EXPAND, allowMultiple = true,
			allowableValues = RouteCompletionMessage.Doc.COMPLETION_EXPANDS, paramType = DocSupport.ParamType.QUERY,
			dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE),

	})
	public RouteCompletions completionsGet(@InjectParam Filter filter) {
		return this.completionService.completions(filter);
	}

	@GET
	@Path(SLASH + COMPLETIONS_ROUTE_COMPLETION_ID_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "A RouteCompletion given the composite Route-Completion ID", response = RouteCompletion.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = Filter.FIELD.EXPAND, allowMultiple = true,
			allowableValues = RouteCompletionMessage.Doc.COMPLETION_EXPANDS, paramType = DocSupport.ParamType.QUERY,
			dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE),

	})
	public RouteCompletion completionsGet(@PathParam(ROUTE_COMPLETION_ID) RouteCompletionId routeCompletionId)
			throws EntityNotFoundException {
		return this.completionService.completion(routeCompletionId);
	}

	@GET
	@Path(SLASH + ROUTE_ID_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "RouteCompletion for the RouteCompletionId", response = RouteCompletions.class)
	@ApiResponses({ @ApiResponse(code = EntityNotFoundException.CODE, message = "When the route is not found.") })
	@ApiImplicitParams({ @ApiImplicitParam(name = Filter.FIELD.EXPAND, allowMultiple = true,
			allowableValues = RouteCompletionMessage.Doc.COMPLETION_EXPANDS, paramType = DocSupport.ParamType.QUERY,
			dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE),

	})
	public RouteCompletions completionsGet(@PathParam(ROUTE_ID) RouteId routeId, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return this.service.completions(routeId, filter);
	}

	@GET
	@Path(SLASH + ROUTE_ID_COMPLETIONS_COMPLETION_ID_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...", response = RouteCompletion.class)
	public RouteCompletion completionsGet(@PathParam(ROUTE_ID) RouteId routeId,
			@PathParam(COMPLETION_ID) String completionsIdValue) throws EntityNotFoundException {
		return this.completionService.completion(new RouteCompletionId(routeId, completionsIdValue));
	}

	@GET
	@Path(SLASH + ROUTE_ID_EVENTS_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...", response = RouteCompletions.class)
	public RouteCompletions completionsWithEventsGet(@PathParam(ROUTE_ID) RouteId routeId, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return this.service.completionsWithEvents(routeId, filter);
	}

	/**
	 * Provides persons that have completed the route identified in the results of a completions
	 * results identified by the filter.
	 * 
	 * @param routeId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	@GET
	@Path(SLASH + ROUTE_ID_COMPLETIONS_PERSONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The Persons that completed the Route.", response = Persons.class)
	public Persons completionsPersonsGet(@PathParam(ROUTE_ID) RouteId routeId, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return this.service.persons(routeId, filter);
	}

	@GET
	@Path(SLASH + ROUTE_ID_COMPLETIONS_SOCIAL_GROUPS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "All SocialGroups for those Persons that completed the Route.",
			notes = "The Persons that appear in RouteCompletions are all part of a SocialGroup with a context=RouteId and criteria=RouteCompletion.  The members in the group would be the persons that appear in RouteCompletions when sorting by route-socialRaceRank.",
			response = SocialGroup.class)
	@ApiResponse(message = EntityNotFoundException.Doc, code = EntityNotFoundException.CODE)
	public
			SocialGroups completionsSocialGroupsGet(@PathParam(ROUTE_ID) @ApiParam(value = "One specific RouteId.",
					defaultValue = ApiDocumentationSupport.EXAMPLE_ROUTE_ID) RouteId routeId)
					throws EntityNotFoundException {
		return this.service.completionSocialGroups(routeId);
	}

	@GET
	@Path(SLASH + ROUTE_ID_EVENTS_SOCIAL_GROUPS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Filtered SocialGroups for RouteEvents that completed the Route.",
			notes = "SocialGroup for Persons who completed the Route at the same time. Many RouteEvents may take place for a Route so a Filter option is provided.  The filter is against the RouteEvent entity only.",
			response = SocialGroup.class)
	@ApiResponse(message = EntityNotFoundException.Doc, code = EntityNotFoundException.CODE)
	public
			SocialGroups
			eventsSocialGroupsGet(@PathParam(ROUTE_ID) @ApiParam(value = "The Route Completed during the Event.",
					defaultValue = ApiDocumentationSupport.EXAMPLE_ROUTE_ID) RouteId routeId, @InjectParam Filter filter)
					throws EntityNotFoundException {
		return this.service.eventSocialGroups(routeId, filter);
	}

	@GET
	@Path(SLASH + ROUTE_ID_VIEWS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The number of times a Route has been viewed.", response = ViewCount.class)
	public ViewCount viewCountGet(@PathParam(ROUTE_ID) RouteId routeId) {
		return this.service.viewCount(routeId);
	}

	@GET
	@Path(SLASH + DICTIONARY)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...", response = FieldDictionary.class)
	public FieldDictionary getDictionary() {
		return this.service.dictionary();
	}

	@GET
	@Path(SLASH + ROUTE_ID_EVENTS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "The events for a specific Route.",
			notes = "The ActivityType of interest is required since Events don't mix", response = RouteEvents.class)
	public RouteEvents eventsGet(@PathParam(ROUTE_ID) RouteId routeId, @InjectParam Filter filter,
			@QueryParam(RouteEventField.Absolute.ACTIVITY_TYPE) @ApiParam(
					value = "Events are always of the same ActivityType", required = true) ActivityType activityType)
			throws EntityNotFoundException {
		// FIXME:THis could be handled generically by an adapter. TN-784
		if (activityType != null) {
			filter = RouteEventFilterBuilder.clone(filter).activityType(activityType).build();
		}
		return eventService.routeEvents(routeId, filter);
	}

	@GET
	@Path(SLASH + ROUTE_ID_EVENTS_EVENT_ID_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "A single RouteEvent identified by the RouteId and EventId separately.",
			response = RouteEvent.class)
	public RouteEvent eventGet(@PathParam(ROUTE_ID) RouteId routeId,
			@ApiParam(value = "The Event portion of the id.") @PathParam(EVENT_ID) String eventIdValue)
			throws EntityNotFoundException {
		return this.eventService.routeEvent(new RouteEventId(routeId, eventIdValue));
	}

	@GET
	@Path(SLASH + ROUTE_ID_EVENTS_EVENT_ID_EVENTS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "RouteEvents for the same Route and ActivityType of the given.", response = RouteEvents.class)
	public
			RouteEvents eventsGet(@PathParam(ROUTE_ID) RouteId routeId, @PathParam(EVENT_ID) String eventIdValue,
					@InjectParam Filter filter) throws EntityNotFoundException {
		return this.eventService.routeEvents(new RouteEventId(routeId, eventIdValue), filter);
	}

	@GET
	@Path(SLASH + EVENTS)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "RouteEvents matching the filter parameters.", response = RouteEvents.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = Filter.FIELD.ORDER_BY, paramType = DocSupport.ParamType.QUERY,
					dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE,
					allowableValues = RouteEventMessage.Doc.ORDER_BY_ALLOWABLE),
			@ApiImplicitParam(name = Filter.FIELD.EXPAND, allowMultiple = true,
					allowableValues = RouteEventMessage.Doc.EXPANDS, paramType = DocSupport.ParamType.QUERY,
					dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE), })
	public RouteEvents getEvent(@InjectParam Filter filter) {
		return eventService.routeEvents(filter);
	}

	@GET
	@Path(SLASH + ROUTES_EVENTS_PATH + SLASH + ROUTE_EVENT_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "RouteEvent identified by RouteEventId.", response = RouteEvent.class)
	public RouteEvent get(@PathParam(ROUTE_EVENT_ID) RouteEventId routeEventId) throws EntityNotFoundException {
		return eventService.routeEvent(routeEventId);
	}

	/**
	 * Synonym to {@link RouteWebApi#eventCompletionsGet(com.aawhere.route.RouteId, String, Filter)}
	 * 
	 * @param routeEventId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	@GET
	@Path(SLASH + EVENTS + SLASH + ROUTE_EVENT_ID_COMPLETIONS)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "RouteCompletions during an Event.", response = RouteCompletions.class)
	public RouteCompletions completionsGet(@PathParam(ROUTE_EVENT_ID) RouteEventId routeEventId,
			@InjectParam Filter filter) throws EntityNotFoundException {
		return completionService.completions(routeEventId, filter);
	}

}
