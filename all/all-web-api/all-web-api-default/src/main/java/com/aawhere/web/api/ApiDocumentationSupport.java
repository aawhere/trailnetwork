/**
 * 
 */
package com.aawhere.web.api;

import static com.aawhere.ws.rs.UriConstants.*;

import com.aawhere.app.KnownApplication;
import com.aawhere.i18n.LocaleUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.swagger.DocSupport;

/**
 * A general place to provide examples, constants and other references that are not part of the
 * application, but useful for the documentation generation. All items must be constants for use in
 * annotations.
 * 
 * @author aroller
 * 
 */
public class ApiDocumentationSupport {

	public static final String LINK_BEGIN = "<span class=\"link\">";
	public static final String LINK_END = "</span>";
	public static final String TRAIL_NETWORK_URL = "http://www.trailnetwork.com";
	public static final String ACTIVITY = "activity";

	public static final String TRAIL_NETWORK_ACTIVITY_URL_BASE = TRAIL_NETWORK_URL + SLASH + ACTIVITY;
	public static final String EXAMPLE_ACTIVITY_REMOTE_ID = "23855336";
	/**
	 * Tennessee Valley http://www.trailnetwork.com/explore/route/5875639005478912
	 */
	public static final String EXAMPLE_ROUTE_ID = "5875639005478912";
	/** Aaron Roller */
	public static final String EXAMPLE_PERSON_ID = "6507166345199616";
	/**
	 * An example of an activity id that goes with the theme of other data structures creating a
	 * complete view of the activities.
	 * 
	 */
	public static final String EXAMPLE_ACTIVITY_ID = KnownApplication.GARMIN_CONNECT_KEY
			+ IdentifierUtils.COMPOSITE_ID_SEPARATOR + EXAMPLE_ACTIVITY_REMOTE_ID;

	public static final String EXAMPLE_ACTIVITY_TN_URL = TRAIL_NETWORK_ACTIVITY_URL_BASE + SLASH + EXAMPLE_ACTIVITY_ID;
	public static final String EXAMPLE_ACTIVITY_TN_LINK = LINK_BEGIN + EXAMPLE_ACTIVITY_TN_URL + LINK_END;
	public static final String EXAMPLE_ACTIVITY_APPLICATION_URL = "http://connect.garmin.com/activity/"
			+ EXAMPLE_ACTIVITY_REMOTE_ID;
	public static final String EXAMPLE_ACTIVITY_APPLICATION_LINK = LINK_BEGIN + EXAMPLE_ACTIVITY_APPLICATION_URL
			+ LINK_END;

	public static final String EXAMPLE_ACTIVITY_REFERENCE_PHRASE = "The example is the Tennssee Valley Activity available from the TrailNetwork("
			+ EXAMPLE_ACTIVITY_TN_LINK
			+ ") or Garmin Connect ("
			+ LINK_BEGIN
			+ EXAMPLE_ACTIVITY_APPLICATION_URL
			+ LINK_END + ").";

	public static final String LANGUAGE_DATA_TYPE = "Locale";
	// FIXME:These languages should be provided by something better...ideally automatically
	public static final String LANGUAGE_ALLOWABLE = LocaleUtil.ENGLISH_US + DocSupport.Allowable.NEXT
			+ LocaleUtil.DEUTSCH_DE + DocSupport.Allowable.NEXT + LocaleUtil.DEUTSCH_CH;
	public static final String LANGUAGE_DEFAULT = LocaleUtil.ENGLISH_US;

	public static class Position {
		private static final int AFTER = 1;
		public static final int ROUTES = AFTER;
		public static final int TRAILHEADS = AFTER + ROUTES;
		public static final int ACTIVITIES = AFTER + TRAILHEADS;
		public static final int TIMINGS = AFTER + ACTIVITIES;
		public static final int COMPLETIONS = AFTER + TIMINGS;
		public static final int EVENTS = AFTER + COMPLETIONS;
		public static final int PERSONS = AFTER + EVENTS;
		public static final int ACCOUNTS = AFTER + PERSONS;
		public static final int SOCIAL_GROUPS = AFTER + ACCOUNTS;
		public static final int SOCIAL_GROUPS_MEMBERSHIPS = AFTER + SOCIAL_GROUPS;
		public static final int APPLICATIONS = AFTER + SOCIAL_GROUPS_MEMBERSHIPS;
		public static final int TRACKS = AFTER + APPLICATIONS;
		public static final int DOCUMENTS = AFTER + TRACKS;
		public static final int GEO = AFTER + DOCUMENTS;
		public static final int SYSTEM = AFTER + GEO;
		public static final int SEARCH = AFTER + SYSTEM;

		/**
		 * Supporting a hack to create a simplified Public API, any operation with the default
		 * position not specified will be considered "non-public". Public operations must specify
		 * their position relative to the others. TN-827
		 * 
		 * @see #PUBLIC
		 */
		public static final int PRIVATE = 0;
		/**
		 * Used to indicate the endpoint should be public and included in the simple API. TN-827
		 * 
		 * @see #PRIVATE
		 */
		public static final int PUBLIC = AFTER;
	}

}
