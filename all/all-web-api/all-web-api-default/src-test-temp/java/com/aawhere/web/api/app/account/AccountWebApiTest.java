/**
 * 
 */
package com.aawhere.web.api.app.account;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.TestUtil;
import com.aawhere.test.category.WebApiTest;
import com.aawhere.web.api.activity.ActivityWebApiTest;
import com.aawhere.web.api.client.WebApiClientHelper;
import com.aawhere.web.api.client.WebApiClientHelper.Builder.OutputType;

import com.trailnetwork.api.Account;
import com.trailnetwork.api.AccountFields;
import com.trailnetwork.api.Accounts;
import com.trailnetwork.api.Activities;
import com.trailnetwork.api.Activity;
import com.trailnetwork.api.FieldDictionary;
import com.trailnetwork.api.Filter;
import com.trailnetwork.api.FilterOperator;
import com.trailnetwork.api.TrailNetworkApi;

/**
 * @author aroller
 * 
 */
@Category(WebApiTest.class)
public class AccountWebApiTest extends com.aawhere.web.api.WebApiTest {



	@Test
	public void testById() {
		Activity activity = ActivityWebApiTest.getHeart();

		String accountId = activity.getAccountId().getValue();
		assertNotNull("account can't be null", accountId);
		Account foundAccount = TrailNetworkApi.accounts().accountId(accountId).getAsAccountXml();
		assertEquals("finder finding wrong account", accountId, foundAccount.getId());

	}

	@Test
	public void testDictionary() {
		FieldDictionary dictionary = TrailNetworkApi.accounts().dictionary().getAsFieldDictionaryXml();
		assertNotNull(dictionary);
	}

	@Test
	public void testFields() {
		assertNotNull("aliases", getFields().getAliases());
	}

	public AccountFields getFields() {
		return TrailNetworkApi.accounts().fields().getAsAccountFieldsXml();
	}

	@Test
	@Ignore("the filter post isn't being respected, but debugging is being hindered by TN-226")
	public void testFilterAliases() {
		Activity activity = ActivityWebApiTest.getHeart();
		Activity bonusActivity = ActivityWebApiTest.getCalevaras1();

		WebApiClientHelper helper = new WebApiClientHelper.Builder().setLogOutput(OutputType.SYSTEM_OUT).build();
		Filter filter = new Filter();
		com.trailnetwork.api.FilterCondition aliasCondition = new com.trailnetwork.api.FilterCondition();
		AccountFields fields = TrailNetworkApi.accounts(helper.getClient()).fields().getAsAccountFieldsXml();
		aliasCondition.setField(fields.getApplicationKEY());
		aliasCondition.setOperator(FilterOperator.EQUALS);
		String expectedAlias = activity.getAccount().getAliases().get(0);
		aliasCondition.setValue("2p");
		filter.setPage(1);
		filter.getCondition().add(aliasCondition);
		Accounts accounts = TrailNetworkApi.accounts(helper.getClient()).postXmlAsAccounts(filter);
		assertNotNull(accounts);
		assertEquals("should have found only one by that alias", 1, accounts.getAccount().size());
		TestUtil.assertContains("alias not found", expectedAlias, accounts.getAccount().get(0).getAliases());
	}

	@Test
	public void testByAccountId() {
		Activity heart = ActivityWebApiTest.getHeart();
		Activities activities = TrailNetworkApi.accounts().accountIdActivities(heart.getAccountId().getValue())
				.getAsActivitiesXml();
		List<Activity> activityList = activities.getActivity();
		for (Activity activity : activityList) {
			assertEquals(heart.getId() + " isn't from this account", heart.getAccountId().getValue(), activity
					.getAccountId().getValue());
		}
	}



}
