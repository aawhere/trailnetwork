/**
 *
 */
package com.aawhere.web.api.admin;

import java.util.Set;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.category.ManualTest;
import com.aawhere.test.category.WebApiTest;
import com.aawhere.web.api.TestFixtures;

import com.google.common.collect.Sets;

import com.trailnetwork.api.TrailNetworkApi;

/**
 * These tests are intended to be run manually to help with testing of the
 * system.
 * 
 * @author Brian Chapman
 * 
 */
@Category({WebApiTest.class,ManualTest.class})
public class FixtureWebApiTest {

	private final String GC_APP_KEY = TestFixtures.GC_APP_KEY;
	Set<Long> gcPleasantonRidgeGoldenEagle = Sets.newHashSet(215840109L,
			214194594L, 200429138L);

	@Test
	public void importPleasantonRidgeTestFixtures() {
		for (Long id : gcPleasantonRidgeGoldenEagle) {
			TrailNetworkApi.activities().activityId(TestFixtures.createId(GC_APP_KEY, String.valueOf(id)));
		}
	}
}
