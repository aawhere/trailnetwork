/**
 *
 */
package com.aawhere.web.api.activity;

import static org.junit.Assert.*;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.ws.rs.WebApplicationException;

import net.opengis.kml.v_2_2_0.AbstractFeatureType;
import net.opengis.kml.v_2_2_0.KmlType;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityIdFixture;
import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.log.LoggerFactory;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ManualTest;
import com.aawhere.test.category.WebApiTest;
import com.aawhere.test.category.WebClientTest;
import com.aawhere.web.api.TestFixtures;
import com.aawhere.web.api.WebApiTestUtil;
import com.aawhere.web.api.client.WebApiClientHelper;
import com.aawhere.web.api.client.WebApiClientHelper.Builder.OutputType;

import com.trailnetwork.api.Activities;
import com.trailnetwork.api.Activity;
import com.trailnetwork.api.ActivityFields;
import com.trailnetwork.api.ActivityImportHistory;
import com.trailnetwork.api.ActivityImportMilestone;
import com.trailnetwork.api.BoundingBox;
import com.trailnetwork.api.Document;
import com.trailnetwork.api.FieldDictionary;
import com.trailnetwork.api.Filter;
import com.trailnetwork.api.FilterCondition;
import com.trailnetwork.api.FilterOperator;
import com.trailnetwork.api.MeasureApiUtil;
import com.trailnetwork.api.Message;
import com.trailnetwork.api.TrailNetworkApi;
import com.trailnetwork.api.TrailNetworkApi.Activities.ActivityId;
import com.trailnetwork.api.TrailNetworkApi.Activities.Dictionary;

import com.google.common.net.MediaType;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * @see ActivityWebApi
 * 
 * @author Aaron Roller
 */
@Category(WebApiTest.class)
public class ActivityWebApiTest extends com.aawhere.web.api.WebApiTest {

	public static final String BAD_ID = ActivityIdFixture.GC_NOT_FOUND_ID;
	public static final com.trailnetwork.api.ActivityId GC_NOT_FOUND_ID = TestFixtures
			.appIdGc(ActivityIdFixture.GC_NOT_FOUND_ID);
	public static final String METER_UNIT = "m";
	Set<Long> activityIds;

	@BeforeClass
	public static void setUpClass() {
		LoggerFactory.getLogger(ActivityWebApiTest.class).info("Running tests against: " + TrailNetworkApi.BASE_URI);
		//although ideally this would be a prerequisite, but not available.
		//this also is the best method to wake up an app engine server.
		Dictionary dictionary = TrailNetworkApi.activities().dictionary();
		assertNotNull("if the dictionary isn't availalble, then don't test anything",dictionary);
	}

	@Test
	@Category({ WebApiTest.class, WebClientTest.class })
	public void testGetActivityById() throws Exception {
		Activity actual = getHeart();

		assertNotNull(actual);
		assertNotNull(actual.getId());
		com.trailnetwork.api.Activity found = TrailNetworkApi.activities().activityId(actual.getId().toString())
				.getAsActivityXml();
		assertActivity(actual, found);
	}

	/**
	 * An attempt to update an activity that does not implement the ability to update should respond
	 * with a bad method 405. Garmin Connect activities are currently not updateable.
	 * 
	 */
	@Test
	@Ignore("puts are failing for web-api-deploy")
	public void testUpdateGarminConnectActivityNotSupported() {
		Activity heart = getHeart();
		try {
			TrailNetworkApi.activities().activityId(heart.getId()).putAsActivityXml();
		} catch (WebApplicationException e) {
			WebApiTestUtil.assertStatusCode(HttpStatusCode.BAD_METHOD, e);
		}
	}

	@Test
	@Ignore("geocells requires a parameter which we can't set")
	public void testQueryByGeoCell() {
		Activity heart = getHeart();
		// FIXME:Fields must provide nested fields
		final String cells = heart.getTrack().getSpatialBuffer().getValue();
		Activities activities = TrailNetworkApi.activities().cellsCells(cells).getAsActivitiesXml();
		TestUtil.assertNotEmpty(activities.getActivity());
		// TODO:post the filter limiting to heart.id guaranteeing the result
		if (activities.getFilter().isFinalPage()) {
			boolean found = false;
			for (Activity activity : activities.getActivity()) {
				if (activity.getId().equals(heart.getId())) {
					found = true;
				}
			}
			assertTrue("heart wasn't found in it's own cells " + cells, found);
		}

	}


	@Test
	@Ignore("geocells requires a parameter which we can't set")
	public void testQueryByGeoCellKml() {
		Activity heart = getHeart();
		// FIXME:Fields must provide nested fields
		final String cells = heart.getTrack().getSpatialBuffer().getValue();
		AbstractFeatureType document = TrailNetworkApi.activities().cellsCells(cells)
				.getAsVndGoogleEarthKmlXml(KmlType.class).getAbstractFeatureGroup().getValue();

		assertNotNull("not much of a test, but it didn't even pass this", document);

	}
	
	

	/**
	 * 2peak sections are able to be updated.
	 * 
	 */
	@Test
	@Ignore("puts are failing for web-api-deploy")
	public void testUpdate2peakSection() {
		final com.trailnetwork.api.ActivityId id = TestFixtures.TWO_PEAK_SECTION;
		// create it first
		getActivity(id);
		try {
			// then attempt an update
			TrailNetworkApi.activities().activityId(id.getValue()).putAsActivityXml();
			fail("updating an activity that was not modified shouldn't pass.");
		} catch (WebApplicationException e) {
			// wouldn't this be nice, but jersey treats 300 errors like a success.
			WebApiTestUtil.assertStatusCode(HttpStatusCode.NOT_MODIFIED, e);
		} catch (ClientHandlerException e) {
			// not much we can check here since this is a client error that screwed up the response.

		}
	}

	/**
	 * a manually invoked test to coordinate editing at 2peak. You run this and it should import
	 * into your database, then fail the update. Next you modify at 2peak and this should succeed
	 * fully. Running it again will fail until you modify the section at 2peak.
	 */
	@Test
	@Category(ManualTest.class)
	@Ignore("manual test isn't ignored at the method level")
	public void testUpdate2PeakUpdatedSection() {
		final com.trailnetwork.api.ActivityId id = TestFixtures.activityId(TestFixtures.TWO_PEAK_SECTION_KEY, "98");
		Document createdDocument = TrailNetworkApi.activities().activityIdDocuments(id.getValue()).getAsDocumentXml();
		// then attempt an update
		TrailNetworkApi.activities().activityId(id.getValue()).putAsActivityXml();
		// there's no guarantee an activity will be updated, but the document must be since it will
		// be retrieved.
		Document updatedDocument = TrailNetworkApi.activities().activityIdDocuments(id.getValue()).getAsDocumentXml();
		TestUtil.assertGreaterThan(createdDocument.getVersion(), updatedDocument.getVersion());
	}

	/**
	 * Taking a small activity this will request for an import, wait for it to process and verify it
	 * has processed. Since this requires the activity not be imported this test will only work once
	 * per integration test server startup and won't really test anything against staging since it
	 * has a persistent database across tests.
	 * 
	 * If this test fails randomly it is because the timing isn't long enough and/or the queue is
	 * getting backed up and the activity won't process in time.
	 * 
	 * @throws InterruptedException
	 * 
	 * 
	 */
	@Test(timeout = 30000)
	@Ignore("this test is timing out on staging.")
	@Category({ WebApiTest.class, WebClientTest.class })
	public void testAsynchImport() throws InterruptedException {
		// this is specifically hidden from other since it is purpose built for this test.
		String activityId = "gc-5363701";
		ActivityImportHistory xml = TrailNetworkApi.activities().activityIdImports(activityId)
				.getAsActivityImportHistoryXml();
		List<ActivityImportMilestone> milestones = xml.getActivitiesImport();
		assertTrue(milestones.size() >= 1);
		boolean success = false;
		do {
			// sleep to allow the import to happen
			Thread.sleep(1000);
			try {
				Activity activity = TrailNetworkApi.activities().activityId(activityId).getAsActivityXml();
				assertNotNull(activity);
				assertEquals(activityId, activity.getId());
				success = true;
			} catch (Exception e) {

			}
		} while (!success);

	}

	@Test
	public void testGetActivityByApplicationIdWithParamApplicationActivityId() {
		Activity actual = getHeart();
		String appActivityId = actual.getId();
		Activity found = TrailNetworkApi.activities().activityId(appActivityId).getAsActivityXml();
		assertActivity(actual, found);
	}

	/**
	 * 
	 * This test isn't perfect since the exception should be handled,but the MIME type is coming
	 * back as plaintext and we want XML. Of course the xml wouldn't be in our desired format so we
	 * need to move the messages to the headers instead of the response.
	 * 
	 * "https://aawhere.jira.com/browse/TN-128" should be cleaner than this...
	 * 
	 */
	@Test
	public void testGarminConnectActivityNotFound() {

		ActivityId request = TrailNetworkApi.activities().activityId(GC_NOT_FOUND_ID.getValue());
		assertActivityHttpStatus(HttpStatus.SC_ACCEPTED, request);
		assertActivityHttpStatus(HttpStatus.SC_ACCEPTED, request);

	}

	@Test
	@Ignore("2peak isn't serving consistently: https://aawhere.jira.com/builds/browse/AAWHERE-COMPLETE-450/test")
	public void testGet2peakActivity() {
		String expectedActivityId = TestFixtures.TWO_PEAK_ACTIVITY_ID_1;
		Activity activity = TrailNetworkApi.activities().activityId(expectedActivityId).getAsActivityXml();
		assertNotNull(activity);
		assertEquals(expectedActivityId, activity.getId());
		Activity foundActivity = TrailNetworkApi.activities().activityId(activity.getId().toString())
				.getAsActivityXml();

		assertEquals(activity.getId(), foundActivity.getId());

	}

	/**
	 * Calls the web service without an application key so a request won't even be recorded. This
	 * must always report 400.
	 * 
	 */
	@Test
	public void testMissingApplicationKeyActivityId() {
		int expected = java.net.HttpURLConnection.HTTP_BAD_REQUEST;
		ActivityId request = TrailNetworkApi.activities().activityId(BAD_ID.toString());
		assertActivityHttpStatus(expected, request);
		// second call to confirm idempotent
		assertActivityHttpStatus(expected, request);

	}

	/**
	 * @param expected
	 * @param request
	 */
	private void assertActivityHttpStatus(int expected, ActivityId request) {
		try {
			Activity activity = request.getAsJsonQs2(Activity.class);
			fail("the activity should not be found!" + activity.getId());
		} catch (WebApplicationException w) {
			assertEquals(expected, w.getResponse().getStatus());
		} catch (UniformInterfaceException e) {
			assertEquals(expected, e.getResponse().getStatus());
			assertTrue(e.getMessage().contains(BAD_ID.toString()));
		} catch (ClientHandlerException e) {
			assertTrue(expected < HttpStatus.SC_MULTIPLE_CHOICES);
		}
	}

	@Test
	@Ignore("The Autogenerated client doesn't send json or doesnt format it correctly. Ignoring until we can investigate. Maybe Jersey 2 will fix.")
	public
			void testFilterJson() {
		Filter filter = testFilterSetup();
		Client client = new WebApiClientHelper.Builder().build().getClient();
		Activities activities = TrailNetworkApi.activities(client).postXmlAsActivities(filter);
		testFilterAssert(activities, filter);
	}

	@Test
	@Ignore("post isn't playing well with filters for some reason. They are being ignored")
	public void testFilterXml() {
		Filter filter = testFilterSetup();
		Client client = new WebApiClientHelper.Builder().setLogOutput(OutputType.SYSTEM_OUT).build().getClient();

		Activities activities = TrailNetworkApi.activities(client).postXmlAsActivities(filter);
		testFilterAssert(activities, filter);
	}

	@Test
	@Ignore("post isn't playing well with filters for some reason. They are being ignored")
	public void testFilterByIdCondition() {
		Activity heart = getHeart();
		Filter filter = new Filter();

		com.trailnetwork.api.FieldDictionary dictionary = TrailNetworkApi.activities().dictionary()
				.getAsFieldDictionaryXml();
		Message domain = dictionary.getDomain();
		FilterCondition condition = new FilterCondition();
		condition.setField(domain + "-id");
		condition.setOperator(FilterOperator.EQUALS);
		condition.setValue(heart.getId().toString());
		filter.getCondition().add(condition);
		Client client = new WebApiClientHelper.Builder().setLogOutput(OutputType.SYSTEM_OUT).build().getClient();
		Activities activities = TrailNetworkApi.activities(client).postXmlAsActivities(filter);
		assertEquals(1, activities.getActivity().size());
	}

	@Test
	public void testActivitiesDictionary() {
		FieldDictionary dictionary = TrailNetworkApi.activities().dictionary().getAsFieldDictionaryXml();
		assertNotNull(dictionary.getDomain());
		assertNotNull(dictionary.getDomainKey());
	}

	@Test
	public void testActivityFields() {
		ActivityFields fields = TrailNetworkApi.activities().fields().getAsActivityFieldsXml();
		assertNotNull("id", fields.getId());
		assertNotNull("name", fields.getName());
		assertNotNull("application", fields.getApplicationKey());
	}

	@Test
	public void testBoundsFilterXmlXml() {
		assertBoundsFilter(new XmlToXmlFilterDelegate());
	}

	@Test
	@Ignore
	public void testBoundsFilterXmlJson() {
		assertBoundsFilter(new XmlToJsonFilterDelegate());
	}

	@Test
	@Ignore
	public void testBoundsFilterJsonJson() {
		assertBoundsFilter(new JsonToJsonFilterDelegate());
	}

	@Test
	@Ignore
	public void testBoundsFilterJsonXml() {
		assertBoundsFilter(new JsonToXmlFilterDelegate());
	}

	@Test
	@Deprecated
	public void testImportHistoryGetByAppId() {
		// this shouldn't re-import or make any changes if the activity already exists.
		getHeart();

		ActivityImportHistory history = TrailNetworkApi.activities()
				.activityIdImportsHistory(TestFixtures.GC_HEART_1.getValue()).getAsActivityImportHistoryXml();
		assertHeartHistory(history);
	}

	@Test
	public void testImporstHistoryByActivityId() {
		// this shouldn't re-import or make any changes if the activity already exists.
		Activity heart = getHeart();

		ActivityImportHistory history = TrailNetworkApi.activities().activityIdImportsHistory(heart.getId())
				.getAsActivityImportHistoryXml();
		assertHeartHistory(history);
	}

	/** Tests a single request of an activity to be imported asynchronously. */
	@Test
	public void testImportActivity() {
		String appKey = TestFixtures.appKeyValue(TestFixtures.gcHeart1);
		ActivityImportHistory milestonesXml = TrailNetworkApi.activities().activityIdImports(appKey)
				.getAsActivityImportHistoryXml();

		assertNotNull(milestonesXml);
		List<ActivityImportMilestone> milestones = milestonesXml.getActivitiesImport();
		assertEquals(	"this request should only return what was requested and not mix with the database",
						1,
						milestones.size());
		ActivityImportMilestone milestoneXml = milestones.get(0);
		com.trailnetwork.api.ActivityId activityId = milestoneXml.getActivityId();
		assertNotNull(activityId);
		assertEquals(appKey, activityId.getValue());
	}

	/** Tests a single request of an activity to be imported asynchronously. */
	@Test
	public void testImportActivities() {
		String keys = StringUtils.join(	Arrays.asList(	TestFixtures.appKeyValue(TestFixtures.gcHeart1),
														TestFixtures.appKeyValue(TestFixtures.gcCalevaras1)),
										',');
		ActivityImportHistory milestonesXml = TrailNetworkApi.activities().activityIdImports(keys)
				.getAsActivityImportHistoryXml();

		assertNotNull(milestonesXml);
		List<ActivityImportMilestone> milestones = milestonesXml.getActivitiesImport();
		assertEquals("requested imports", 2, milestones.size());

	}

	/**
	 * Verifies proper guidance when an invalid application is provided. Very little other
	 * validation is done before queuing.
	 */
	@Test
	public void testImportUnknownApplication() {

		try {
			ActivityImportHistory milestones = TrailNetworkApi.activities()
					.activityIdImports("junk-" + TestUtil.generateRandomAlphaNumeric()).getAsActivityImportHistoryXml();
			fail("we provided a bad app, it should have failed gracefully");

		} catch (ClientHandlerException c) {
			// fine. the client doesn't know how to handle 300 errors.
			// assume this is o.k.
		} catch (WebApplicationException w) {
			assertEquals(HttpStatus.SC_MULTIPLE_CHOICES, w.getResponse().getStatus());

		}

		catch (UniformInterfaceException e) {
			// this went away with the new 1.1.5 client
			assertEquals(HttpStatus.SC_MULTIPLE_CHOICES, e.getResponse().getStatus());
		}

	}

	@Test
	@Ignore("having problems with posting filters")
	public void testImportHistoryFilterPost() {
		getHeart();
		Filter filter = new Filter();
		FilterCondition condition = new FilterCondition();
		// TODO:reference the dictionary to get the field name.
		String APPLICATION_ACTIVITY_ID_KEY = "activityRelation-activityId";
		condition.setField(APPLICATION_ACTIVITY_ID_KEY);
		condition.setOperator(FilterOperator.EQUALS);
		com.trailnetwork.api.ActivityId expectedAppActivityId = TestFixtures.appIdGc(TestFixtures.gcHeart1);
		condition.setValue(expectedAppActivityId.getValue());
		Client client = new WebApiClientHelper.Builder()
		// .setLogOutput(OutputType.SYSTEM_OUT)
				.build().getClient();
		ActivityImportHistory history = TrailNetworkApi.imports(client).postAsActivityImportHistoryXml(filter);
		assertTrue(history.getActivitiesImport().size() > 1);

	}

	@Ignore("TN-328 keep getting a status 411 on appengine, passes on local dev.")
	@Test
	public void testUpdateActivityRequiresAuthenticatedUser() {
		try {
			TrailNetworkApi.activities().activityId("doesn't matter").putAsActivityXml();
		} catch (WebApplicationException e) {

			WebApiTestUtil.assertNotAuthorized(e);
		}
	}

	@Test
	public void testNoContent() {
		com.trailnetwork.api.TrailNetworkApi.Activities activities = TrailNetworkApi.activities();
		String emptyString = activities.getAsNone(String.class);
		assertEquals("empty string shows nothing is coming back", StringUtils.EMPTY, emptyString);
		// must have been a 200 or similar.
	}

	/**
	 * A simple test to make sure the GET filtering is working (unfortunately without setting
	 * parameters.
	 */
	@Test
	public void testImportHistoryFilterGet() {
		// Since Milestones are only reported for failures, we need to create a failure.
		testGarminConnectActivityNotFound();
		Filter filter = new Filter().withLimit(1);
		ActivityImportHistory history = TrailNetworkApi.imports().getAsActivityImportHistoryXml().withFilter(filter);
		List<ActivityImportMilestone> milestones = history.getActivitiesImport();
		assertTrue(milestones.size() >= 1);
	}

	@Test
	@Ignore
	public void testFilterConditionsDistance() {
		Activity heart = getHeart();
		Activity c1 = getCalevaras1();
		// This, of course assumes we don't have any 0 distance tracks or negative distance tracks
		// in the system.
		String value = 0 + " " + METER_UNIT;

		FilterCondition condition1 = new FilterCondition().withField("track-distance")
				.withOperator(FilterOperator.LESS_THAN).withValue(value);
		FilterCondition condition2 = new FilterCondition().withField("track-distance")
				.withOperator(FilterOperator.GREATER_THAN).withValue(value);

		Filter filter1 = new Filter().withCondition(condition1);
		Activities activities1 = TrailNetworkApi.activities().getAsActivitiesXml().withFilter(filter1);
		assertEquals(0, activities1.getActivity().size());

		Filter filter2 = new Filter().withCondition(condition2);
		Activities activities2 = TrailNetworkApi.activities().getAsActivitiesXml().withFilter(filter2);
		TestUtil.assertGreaterThan(2, activities2.getActivity().size());

	}

	/**
	 * Verify that using an extension serves up the correct media type.
	 */
	@Test
	public void testActivityKml() {
		URI url = ActivityWebApiUri.getActivityByApplicationId(new com.aawhere.activity.ActivityId(
				TestFixtures.GC_HEART_1.getValue()), MediaType.KML);
		Client client = TrailNetworkApi.createClient();
		WebResource resource = client.resource(TrailNetworkApi.BASE_URI);
		ClientResponse response = resource.path(url.toString()).get(ClientResponse.class);
		String kmlDoc = response.getEntity(String.class);
		TestUtil.assertContains(kmlDoc, "<kml");
	}

	public static Activity getCalevaras1() {
		return getGcActivity(TestFixtures.gcCalevaras1);
	}

	public static Activity getCalevaras2() {
		return getGcActivity(TestFixtures.gcCalevaras2);
	}

	/**
	 * @return
	 */
	public static Activity getHeart() {
		Long gcActivityId = TestFixtures.gcHeart1;
		return getGcActivity(gcActivityId);
	}

	/**
	 * @param gcActivityId
	 * @return
	 */
	public static Activity getGcActivity(Long gcActivityId) {
		return getGcActivity(gcActivityId.toString());
	}

	public static Activity getActivity(com.trailnetwork.api.ActivityId id) {
		return TrailNetworkApi.activities().activityId(id.getValue()).getAsActivityXml();
	}

	public static Activity getGcActivity(String gcActivityId) {
		return TrailNetworkApi.activities().activityId(TestFixtures.createId(TestFixtures.GC_APP_KEY, gcActivityId))
				.getAsActivityXml();
	}

	private void assertActivity(Activity actual, Activity found) {
		assertNotNull(found);
		assertEquals(actual.getId(), found.getId());
		WebApiTestUtil.assertEntity(found);
	}

	/**
	 * @param expectedAppActivityId
	 * @param history
	 */
	private void assertHeartHistory(ActivityImportHistory history) {
		List<ActivityImportMilestone> milestones = history.getActivitiesImport();
		assertEquals("expecting no failures " + milestones, 0, milestones.size());

	}

	public void assertBoundsFilter(FilterDelegate filterDelegate) {
		Activity heart = getHeart();
		BoundingBox boundingBox = heart.getTrack().getBoundingBox();
		Filter filter = new Filter();
		FilterCondition condition = new FilterCondition().withField("geoCells").withOperator(FilterOperator.IN)
				.withValue(MeasureApiUtil.boundingBoxToGeoCellsString(boundingBox));
		filter.withCondition(condition);
		Activities result = filterDelegate.makeRequest(filter);
		List<Activity> activities = result.getActivity();
		assertFalse(activities.isEmpty());
		Filter resultFilter = result.getFilter();
		assertNotNull(resultFilter);
	}

	private void testFilterAssert(Activities activities, Filter filter) {
		assertNotNull(activities);
		Filter responseFilter = activities.getFilter();
		assertNotNull(responseFilter);
		assertEquals(filter.getClass(), responseFilter.getClass());
		assertNotNull(activities.getActivity());
		assertTrue(	"Expected at most " + filter.getLimit().toString() + ", received " + responseFilter.getPageTotal(),
					filter.getLimit() >= responseFilter.getPageTotal());
		assertEquals(filter.getPage(), responseFilter.getPage());
	}

	private Filter testFilterSetup() {
		// import at least 3 activities.
		getHeart();
		getCalevaras1();
		getCalevaras2();

		// test the second page of at least 3.
		Integer limitPerPage = 2;
		Integer pageValue = 2;
		Filter filter = new Filter();
		filter.setPage(pageValue);
		filter.setLimit(limitPerPage);
		return filter;
	}

	public static interface FilterDelegate {

		abstract Activities makeRequest(Filter filter);
	}

	public static class XmlToXmlFilterDelegate
			implements FilterDelegate {
		@Override
		public Activities makeRequest(Filter filter) {

			return TrailNetworkApi.activities().postXmlAsActivities(filter);
		}
	}

	public static class XmlToJsonFilterDelegate
			implements FilterDelegate {
		@Override
		public Activities makeRequest(Filter filter) {

			return TrailNetworkApi.activities().postXmlAsActivities(filter);
		}
	}

	public static class JsonToXmlFilterDelegate
			implements FilterDelegate {
		@Override
		public Activities makeRequest(Filter filter) {

			return TrailNetworkApi.activities().postXmlAsActivities(filter);
		}
	}

	public static class JsonToJsonFilterDelegate
			implements FilterDelegate {
		@Override
		public Activities makeRequest(Filter filter) {

			return TrailNetworkApi.activities().postJsonQs2(filter, Activities.class);
		}
	}

}
