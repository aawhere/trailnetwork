/**
 * 
 */
package com.aawhere.web.api.activity.timing;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.TestUtil;
import com.aawhere.test.category.WebApiTest;

import com.trailnetwork.api.ActivityTiming;
import com.trailnetwork.api.TrailNetworkApi;

/**
 * @author aroller
 * 
 */
@Category(WebApiTest.class)
public class ActivityLeaderboardWebApiTest  extends com.aawhere.web.api.WebApiTest {


	@Test
	public void testCsv() {
		ActivityTiming timing = ActivityTimingRelationWebApiTestUtil.getCourseAttemptTiming();

		String csv = TrailNetworkApi.activitiesLeaderboards()
				.leaderboardId(timing.getCourse().getActivityId().getValue())
				.getAsTextCsv(String.class);
		TestUtil.assertContains(csv, ",");
		TestUtil.assertContains(csv, timing.getAttempt().getActivityId().getValue());
	}
}
