/**
 * 
 */
package com.aawhere.web.api.admin;

import static org.junit.Assert.*;

import javax.ws.rs.WebApplicationException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.TestUtil;
import com.aawhere.test.category.WebApiTest;
import com.aawhere.web.api.WebApiTestUtil;

import com.trailnetwork.api.TrailNetworkApi;

/**
 * @author brian
 * 
 */
@Category(WebApiTest.class)
public class AdminWebApiTest  extends com.aawhere.web.api.WebApiTest {

	@Test
	public void testPermitAll() {
		// Looking to see that an exception is not thrown
		String response = TrailNetworkApi.admin().securityPermitAll().getAs(String.class);
		assertNotNull(response);
	}

	@Test
	public void testDenyAll() {
		try {
			TrailNetworkApi.admin().securityDenyAccess().getAs(String.class);
		} catch (WebApplicationException e) {

			WebApiTestUtil.assertNotAuthorized(e);
		}
	}

	@Test
	public void testAdmin() {
		try {
			TrailNetworkApi.admin().securityAdminRoleOnly().getAs(String.class);
		} catch (WebApplicationException e) {

			WebApiTestUtil.assertNotAuthorized(e);
		}
		// TODO: login as an admin, then test again.
	}

	@Ignore("This test needs to be able to test logged out and logged in states")
	@Test
	public void testWhoAmI() {
		String whoami = TrailNetworkApi.admin().securityWhoami().getAs(String.class);
		TestUtil.assertContains(whoami, "Is admin? true");
	}
}
