/**
 *
 */
package com.aawhere.web.api.geo;

import static org.junit.Assert.*;
import net.opengis.kml.v_2_2_0.DocumentType;
import net.opengis.kml.v_2_2_0.KmlType;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.TestUtil;
import com.aawhere.test.category.WebApiTest;

import com.google.common.collect.Range;
import com.trailnetwork.api.BoundingBox;
import com.trailnetwork.api.BoundingBoxes;
import com.trailnetwork.api.GeoCells;
import com.trailnetwork.api.TrailNetworkApi;

/**
 * Tests client interaction of {@link TrailNetworkApi#geo()}. Detailed functional testing is better
 * done in unit tests, but this just makes sure the endpoint is available and all functions are
 * working, especially transformation to the document via the web container.
 * 
 * @author aroller
 * 
 */
@Category(WebApiTest.class)
public class GeoWebApiTest extends com.aawhere.web.api.WebApiTest {

	/**
	 * Just the bare minimum of boxes that should be returned to be reasonable.
	 */
	private static final int MIN_BOXES = 10;
	String boundsString = "a b 8 9";
	String bboxString = "-123,37,-122,38";

	@Test
	public void testGeoCellsByBoundingBox() {
		BoundingBox geoCellBoundsByCells = TrailNetworkApi.geo().boundsCellsCells(boundsString).getAsBoundingBoxXml();
		GeoCells geoCells = geoCellBoundsByCells.getGeoCells();
		assertCells(boundsString, geoCells);

		GeoCells geoCellBoundsByBounds = TrailNetworkApi.geo().cellsBoundsBounds(geoCellBoundsByCells.getBounds())
				.getAsGeoCellsXml();
		assertCells(boundsString, geoCellBoundsByBounds);
	}

	@Test
	public void testBoxesXmlByCells() {
		BoundingBoxes boxes = TrailNetworkApi.geo().boundsBoundsBoxes(boundsString).getAsBoundingBoxesXml();
		assertBoxes(boxes);
	}

	/**
	 * @param boxes
	 */
	private void assertBoxes(BoundingBoxes boxes) {
		// ten seems like the bare minimumum
		TestUtil.assertSize(Range.atLeast(MIN_BOXES), boxes.getBoundingBox());
	}

	@Test
	public void testBoxesJsonByCells() {
		BoundingBoxes boxes = TrailNetworkApi.geo().boundsBoundsBoxes(boundsString).getAsBoundingBoxesJson();
		assertBoxes(boxes);
	}

	@Test
	public void testBoxesKmlByBbox() {

		KmlType kml = TrailNetworkApi.geo().boxes().getAsVndGoogleEarthKmlXml(bboxString, KmlType.class);
		assertKmlBoxes(kml);
	}

	/**
	 * @param kml
	 */
	private void assertKmlBoxes(KmlType kml) {
		// 10 is just a guess
		TestUtil.assertSize(Range.atLeast(MIN_BOXES),
							((DocumentType) kml.getAbstractFeatureGroup().getValue()).getAbstractFeatureGroup());
	}

	@Test
	public void testBoxesKmlByBounds() {
		KmlType kml = TrailNetworkApi.geo().boundsBoundsBoxes(boundsString).getAsVndGoogleEarthKmlXml(KmlType.class);
		assertKmlBoxes(kml);
	}

	private void assertCells(String boundsString, GeoCells cells) {
		assertNotNull(cells);
		assertNotNull(cells.getValue());
	}

}