/**
 * 
 */
package com.aawhere.web.api.activity.timing;

import static org.junit.Assert.*;

import com.aawhere.activity.ActivityIdFixture;
import com.aawhere.test.TestUtil;
import com.aawhere.web.api.TestFixtures;
import com.aawhere.web.api.activity.ActivityWebApiTestUtil;

import com.trailnetwork.api.ActivityId;
import com.trailnetwork.api.ActivityTiming;
import com.trailnetwork.api.TrailNetworkApi;

/**
 * Useful for hitting the {@link ActivityTimingRelationWebApi} during testing.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationWebApiTestUtil {

	static ActivityId courseId = TestFixtures.appIdGc(ActivityIdFixture.TennesseeValley.MARINCELLO_CLIMB);
	static ActivityId attemptId = TestFixtures.appIdGc(ActivityIdFixture.TennesseeValley.MIWOK_LOOP_SPIKE_FINISH);

	/**
	 * Provides the standard timing that has one completion. Also provides a few assertions so acts
	 * as a test also.
	 * 
	 * @return
	 */
	static public ActivityTiming getCourseAttemptTiming() {
		ActivityTiming relation = TrailNetworkApi.activities()
				.courseAttemptTimings(courseId.getValue(), attemptId.getValue()).getAsActivityTimingXml();
		assertNotNull(relation);
		ActivityWebApiTestUtil.assertActivityIdEquals("course", courseId, relation.getCourse().getActivityId());
		ActivityWebApiTestUtil.assertActivityIdEquals("particpation", attemptId, relation.getAttempt().getActivityId());
		TestUtil.assertNotEmpty(relation.getCompletion());
		return relation;
	}

}
