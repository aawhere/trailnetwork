/**
 * 
 */
package com.aawhere.web.api.activity;

import static org.junit.Assert.*;

import com.trailnetwork.api.ActivityFields;
import com.trailnetwork.api.ActivityId;
import com.trailnetwork.api.TrailNetworkApi;

/**
 * @author aroller
 *
 */
public class ActivityWebApiTestUtil {

	public static void assertActivityIdEquals(String context, ActivityId expected, ActivityId actual) {
		assertEquals("identifiers not equal for " + context, expected.getValue(), actual.getValue());
	}

	/**
	 * Provides the Fields used for query.
	 * 
	 * @return
	 * 
	 */
	public static ActivityFields fields() {
		return TrailNetworkApi.activities().fields().getAsActivityFieldsXml();
	}

}
