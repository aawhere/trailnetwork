/**
 *
 */
package com.aawhere.web.api;

import static org.junit.Assert.*;

import javax.ws.rs.WebApplicationException;

import com.aawhere.net.HttpStatusCode;
import com.aawhere.persist.BaseEntity;

import com.trailnetwork.api.Entity;
import com.trailnetwork.api.StringIdEntity;
import com.trailnetwork.api.Trailhead;

/**
 * Commonly used tests against {@link BaseEntity}
 * 
 * @author Brian Chapman
 * 
 */
public class WebApiTestUtil {

	public static void assertEntity(Entity entity) {
		assertNotNull(entity.getDateCreated());
		assertNotNull(entity.getDateUpdated());
	}

	public static void assertStatusCode(HttpStatusCode expected, WebApplicationException exception) {
		assertNotNull("no status if no exception", exception);
		assertNotNull("tell me what you expect", expected);
		assertEquals(expected.value, exception.getResponse().getStatus());
	}

	public static void assertNotAuthorized(WebApplicationException exception) {
		assertStatusCode(HttpStatusCode.FORBIDDEN, exception);
	}

	/**
	 * @param e
	 */
	public static void assertNotFound(WebApplicationException e) {
		assertStatusCode(HttpStatusCode.NOT_FOUND, e);
	}

	/**Compares only the ids of the two given.
	 * @param first
	 * @param foundTrailhead
	 */
	public static <E extends StringIdEntity> void assertEntityEquals(E expected, E actual) {
		assertEquals(	"provided entity not equals for " + expected.getClass().getSimpleName(),
						expected.getId(),
						actual.getId());
	}
}
