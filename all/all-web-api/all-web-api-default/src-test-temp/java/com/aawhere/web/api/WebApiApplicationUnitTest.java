/**
 * 
 */
package com.aawhere.web.api;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author brian
 * 
 */
public class WebApiApplicationUnitTest {

	@Test
	public void testNewInstance() {
		// should create
		WebApiApplication app = new WebApiApplication();
		assertNotNull(app);
	}

	/**
	 * Packages cannot be empty or null;
	 */
	@Test
	public void testPackages() {
		String packages = WebApiApplication.packagesAsString();
		assertNotNull(packages);
		assertTrue(packages != "");
	}

	@Test
	public void testGetClasses() {
		WebApiApplication application = new WebApiApplication();
		Set<Class<?>> classes = application.getClasses();
		assertNotNull(classes);
		TestUtil.assertNotEmpty(classes);
		Set<Class<?>> rootClasses = application.getRootResourceClasses();
		assertNotNull(rootClasses);
		TestUtil.assertNotEmpty(rootClasses);
	}
}
