/**
 * 
 */
package com.aawhere.web.api;

import org.junit.BeforeClass;

import com.trailnetwork.api.TrailNetworkApi;

/**Base Test for other Web Api Tests to take advantage of common functionality.
 * 
 * @author aroller
 *
 */
public class WebApiTest {

	@BeforeClass
	public static void setUpBaseClass(){
		//just make a small call to wake up a server since appengine sleeps.
		TrailNetworkApi.activities().dictionary().getAsFieldDictionaryXml();
	}
}
