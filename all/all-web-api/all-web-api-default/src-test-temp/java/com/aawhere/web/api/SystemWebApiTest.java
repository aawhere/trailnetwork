/**
 *
 */
package com.aawhere.web.api;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.TestUtil;
import com.aawhere.test.category.WebApiTest;

import com.trailnetwork.api.FieldDictionaries;
import com.trailnetwork.api.FieldDictionary;
import com.trailnetwork.api.TrailNetworkApi;

/**
 * @author aroller
 * 
 */
@Category(WebApiTest.class)
public class SystemWebApiTest {

	@Test
	public void testDictionary() {
		// must hit routes dictionary to make sure something is registered. lazy registration
		TrailNetworkApi.activities().dictionary().getAsFieldDictionaryXml();
		FieldDictionaries dictionaries = TrailNetworkApi.system().dictionary().getAsFieldDictionariesXml();
		TestUtil.assertGreaterThan(0, dictionaries.getDictionary().size());
		FieldDictionary dictionary = dictionaries.getDictionary().get(0);
		assertNotNull(dictionary.getDomainKey());
		// assertNotNull(dictionary.getDomain());
	}
}
