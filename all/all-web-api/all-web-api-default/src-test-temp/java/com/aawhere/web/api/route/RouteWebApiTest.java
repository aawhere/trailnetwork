/**
 * 
 */
package com.aawhere.web.api.route;

import static org.junit.Assert.*;

import java.util.List;

import javax.ws.rs.WebApplicationException;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.category.WebApiTest;
import com.aawhere.web.api.WebApiTestUtil;

import com.trailnetwork.api.Route;
import com.trailnetwork.api.Routes;
import com.trailnetwork.api.TrailNetworkApi;

/**
 * @author aroller
 *
 */
@Category(WebApiTest.class)
public class RouteWebApiTest extends com.aawhere.web.api.WebApiTest {

	@Test
	public void testRoutes() {
		Routes routesXml = TrailNetworkApi.routes().getAsRoutesXml();
		List<Route> routes = routesXml.getRoute();
		assertNotNull(routes);
	}

	@Test
	public void testRouteNotFound() {
		try {
			TrailNetworkApi.routes().routeId("-1");
		} catch (WebApplicationException e) {
			WebApiTestUtil.assertNotFound(e);
		}
	}
}
