/**
 *
 */
package com.aawhere.web.api;

import java.util.Set;

import com.aawhere.activity.ActivityIdFixture;
import com.aawhere.app.KnownApplication;

import com.google.common.collect.Sets;
import com.trailnetwork.api.ActivityId;

/**
 * @author Brian Chapman
 * 
 */
public class TestFixtures {
	public static final String TWO_PEAK_APP_KEY = KnownApplication.TWO_PEAK_ACTIVITY_KEY;
	public static final String TWO_PEAK_SECTION_KEY = KnownApplication.TWO_PEAK_SECTIONS.key.getValue();
	public static final String GC_APP_KEY = KnownApplication.GARMIN_CONNECT.key.getValue();
	public static Long gcHeart1 = 3006961L;
	public static Long gcCalevaras1 = 135741825L;
	public static Long gcCalevaras2 = 40920798L;
	public static Long gcCollier1 = 111616083L;
	public static Long gcPolomeres1 = 59491489L;
	public static Long gcPolomeres2 = 73855080L;
	public static Long gcPolomeres3 = 73855327L;
	public static Long gcPolomeres4 = 59491613L;
	public static Long gcPleasantonSanRamon1 = 135741865L;
	public static Long gcPleasantonSanRamon2 = 40758954L;
	public static Long gcPleasantonSanRamon3 = 40758983L;
	public static Long gcPleasantonSanRamon4 = 59491378L;
	public static Long gcPleasantonSanRamon5 = 59491548L;
	public static Long gcPleasantonSanRamon6 = 40758996L;
	public static ActivityId TWO_PEAK_SECTION = activityId(TWO_PEAK_SECTION_KEY, ActivityIdFixture.TWO_PEAK_SECTION_ID);
	public static final String TWO_PEAK_ACTIVITY_ID_1 = createId(TWO_PEAK_APP_KEY, "199970336");

	public Set<Long> getGarminConnectIds() {
		return Sets.newHashSet(	gcHeart1,
								gcCalevaras1,
								gcCalevaras2,
								gcCollier1,
								gcPolomeres1,
								gcPolomeres2,
								gcPolomeres3,
								gcPolomeres4,
								gcPleasantonSanRamon1,
								gcPleasantonSanRamon2,
								gcPleasantonSanRamon3,
								gcPleasantonSanRamon4,
								gcPleasantonSanRamon5,
								gcPleasantonSanRamon6);
	}

	public static String appKeyValue(Object idValue) {
		String appKey = GC_APP_KEY;
		return createId(appKey, idValue.toString());
	}



	public static ActivityId activityId(String appKey, String idValue) {
		final ActivityId activityId = new ActivityId();
		activityId.setValue(createId(appKey, idValue));
		return activityId;
	}

	/**
	 * @param appKey
	 * @param id
	 * @return
	 */
	public static String createId(String appKey, String id) {
		return appKey + "-" + id;
	}

	public static ActivityId appIdGc(Object idValue) {
		return activityId(GC_APP_KEY, idValue.toString());
	}

	public static final ActivityId GC_HEART_1 = appIdGc(gcHeart1);
}
