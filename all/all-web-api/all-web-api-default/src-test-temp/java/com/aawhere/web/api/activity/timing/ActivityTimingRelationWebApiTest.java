/**
 * 
 */
package com.aawhere.web.api.activity.timing;

import static com.aawhere.web.api.activity.timing.ActivityTimingRelationWebApiTestUtil.*;
import static org.junit.Assert.*;

import java.util.List;

import javax.ws.rs.WebApplicationException;

import net.opengis.kml.v_2_2_0.KmlType;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.TestUtil;
import com.aawhere.test.category.WebApiTest;
import com.aawhere.web.api.WebApiTestUtil;

import com.trailnetwork.api.ActivityId;
import com.trailnetwork.api.ActivityLeaderboard;
import com.trailnetwork.api.ActivityLeaderboards;
import com.trailnetwork.api.ActivityTiming;
import com.trailnetwork.api.ActivityTimings;
import com.trailnetwork.api.Filter;
import com.trailnetwork.api.Ranking;
import com.trailnetwork.api.TrailNetworkApi;

import com.google.common.collect.Range;

/**
 * Tests {@link TimingWebApi} endpoints.
 * 
 * @author aroller
 * 
 */
@Category(WebApiTest.class)
public class ActivityTimingRelationWebApiTest  extends com.aawhere.web.api.WebApiTest {

	
	
	@Test
	public void testGetBasicTiming() {
		ActivityTiming timing = getCourseAttemptTiming();
		assertNotNull("timing", timing);
		// other tests performed by Util itself.
	}

	@Test
	public void testTimings() {
		// ensures at least one timing will be available
		getCourseAttemptTiming();
		ActivityTimings xml = TrailNetworkApi.activitiesTimings().getAsActivityTimingsXml();
		List<ActivityTiming> relations = xml.getActivityTiming();
		TestUtil.assertNotEmpty(relations);
		// can't test for the existence of the created timing because the results may be polluted
		// with others in the datastore
	}

	@Ignore("TN-412, need authentication to succeed")
	@Test
	public void testPutTiming() {
		// ensures at least one timing will be available
		ActivityTiming timing = getCourseAttemptTiming();
		// This does an update, and may modify the timing if a change is detected. A second call is
		// made to ensure we don't update everytime.
		ActivityTiming retreived = TrailNetworkApi.activitiesTimings().activityTimingId(timing.getId())
				.putAsActivityTimingXml();
		assertNotNull(retreived);
		ActivityTiming retreivedAgain = TrailNetworkApi.activitiesTimings().activityTimingId(timing.getId())
				.putAsActivityTimingXml();
		assertNotNull(retreived);
		assertEquals(retreived.getVersion(), retreivedAgain.getVersion());
	}

	@Test
	public void testCourseTimings() {
		ActivityTiming timing = getCourseAttemptTiming();
		final ActivityId expectedActivityId = timing.getAttempt().getActivityId();
		ActivityTimings relations = TrailNetworkApi.activities()
				.activityIdCourseTimings(timing.getCourse().getActivityId().getValue()).getAsActivityTimingsXml();
		assertTimingsContainsActivity(expectedActivityId, relations, true);
	}

	/**
	 * @param expectedActivityId
	 * @param relations
	 */
	private void assertTimingsContainsActivity(final ActivityId expectedActivityId, ActivityTimings relations,
			Boolean isLookingForAttempt) {
		List<ActivityTiming> list = relations.getActivityTiming();
		TestUtil.assertNotEmpty(list);
		Filter filter = relations.getFilter();
		// if the database does not yet contain many other
		if (filter.getPageTotal() < filter.getLimit()) {
			Boolean found = false;
			final String expectedIdValue = expectedActivityId.getValue();
			for (ActivityTiming ActivityTiming : list) {
				final ActivityId actualActivityId;
				if (isLookingForAttempt) {
					actualActivityId = ActivityTiming.getAttempt().getActivityId();
				} else {
					actualActivityId = ActivityTiming.getCourse().getActivityId();
				}
				if (actualActivityId.getValue().equals(expectedIdValue)) {
					found = true;
				}
			}
			assertTrue(expectedIdValue + " not found", found);
		}
	}

	@Test
	public void testAttemptTimings() {
		ActivityTiming timing = getCourseAttemptTiming();
		ActivityTimings attemptTimings = TrailNetworkApi.activities()
				.activityIdAttemptTimings(timing.getAttempt().getActivityId().getValue()).getAsActivityTimingsXml();
		assertTimingsContainsActivity(timing.getCourse().getActivityId(), attemptTimings, false);
	}

	@Test
	public void testTimingsKmlByContentType() {
		ActivityTiming timing = getCourseAttemptTiming();
		KmlType kml = TrailNetworkApi.activitiesTimings().activityTimingId(timing.getId())
				.getAsVndGoogleEarthKmlXml(KmlType.class);
		assertNotNull(kml);

	}

	@Test
	public void testTimingsKmlByUrl() {
		ActivityTiming timing = getCourseAttemptTiming();
		KmlType kml = TrailNetworkApi.activitiesTimings().kmlActivityTimingId(timing.getId())
				.getAsVndGoogleEarthKmlXml(KmlType.class);
		assertNotNull(kml);

	}

	@Test
	public void testLeaderboards() {
		ActivityTiming timing = getCourseAttemptTiming();
		// this forces the creation and storage of the leaderboard. we can't wait for the workflow
		// to complete
		assertNotNull(TrailNetworkApi.activities().courseLeaderboards(timing.getCourse().getActivityId().getValue())
				.getAsActivityLeaderboardXml());

		// now there should be at least one
		ActivityLeaderboards leaderboards = TrailNetworkApi.activitiesLeaderboards().getAsActivityLeaderboardsXml();
		assertNotNull(leaderboards);
		final List<ActivityLeaderboard> leaderboardList = leaderboards.getLeaderboard();
		assertTrue(leaderboardList.size() >= 1);
		for (ActivityLeaderboard activityLeaderboard : leaderboardList) {
			ActivityLeaderboard courseLeaderboard = TrailNetworkApi.activities()
					.courseLeaderboards(activityLeaderboard.getId()).getAsActivityLeaderboardXml();
			assertEquals(activityLeaderboard.getId(), courseLeaderboard.getId());
			List<Ranking> rankings = courseLeaderboard.getRanking();
			TestUtil.assertSize(Range.greaterThan(0), rankings);
		}
	}



	@Ignore("TN-328 keep getting a status 411 on appengine, passes on local dev.")
	@Test
	public void testPutTimingNotAuthorized() {
		try {
			TrailNetworkApi.activitiesTimings().activityTimingId("doesn't matter").putAsActivityTimingXml();
		} catch (WebApplicationException e) {
			WebApiTestUtil.assertNotAuthorized(e);
		}
	}

	@Test
	@Ignore("TN-328 keep getting a status 411 on appengine, passes on local dev.")
	public void testUpdateTimingsRequiresAuthenticatedUser() {
		try {
			TrailNetworkApi.activities().activityIdTimings("doesn't matter").putAsTextPlain(String.class);
		} catch (WebApplicationException e) {

			WebApiTestUtil.assertNotAuthorized(e);
		}
	}
}
