/**
 * 
 */
package com.aawhere.web.api.trailhead;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.category.WebApiTest;
import com.aawhere.web.api.WebApiTestUtil;

import com.trailnetwork.api.Route;
import com.trailnetwork.api.TrailNetworkApi;
import com.trailnetwork.api.Trailhead;
import com.trailnetwork.api.Trailheads;

import com.google.common.collect.Iterables;

/**
 * @author aroller
 *
 */
@Category(WebApiTest.class)
public class TrailheadWebApiTest extends com.aawhere.web.api.WebApiTest {

	
	
	@Test
	public void testTrailheads(){
		Trailheads trailheads = TrailNetworkApi.trailheads().getAsTrailheadsXml();
		assertNotNull(trailheads);
		List<Trailhead> trailheadList = trailheads.getTrailhead();
		if(!trailheadList.isEmpty()){
			Trailhead first = Iterables.getFirst(trailheadList, null);
			String trailheadId = first.getId();
			assertNotNull(first);
			Trailhead foundTrailhead = TrailNetworkApi.trailheads().trailheadId(first.getId()).getAsTrailheadXml();
			WebApiTestUtil.assertEntityEquals(first,foundTrailhead);
			List<Route> routes = TrailNetworkApi.trailheads().trailheadIdRoutes(trailheadId).getAsRoutesXml().getRoute();
			if(!routes.isEmpty()){
				Route route = routes.get(0);
				assertEquals("wrong id found", first.getId(), route.getStartTrailheadId().getValue());
			}
		}
	}
}
