/**
 *
 */
package com.aawhere.web.api;

/**
 * Expected strings contained in the Json representation of entities translated
 * by JaxB. External clients rely on these strings being stable.
 * 
 * @author Brian Chapman
 * 
 */
public class JsonProperties {

	public static class Identifier {
		public static final String ID = "id";
	}

	public static class Routes {
		public static final String ARRAY_OF_ROUTES = "route";
	}
}
