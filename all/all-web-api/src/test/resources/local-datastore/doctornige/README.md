gc-doctornige (gc-4166732) has 230 activities that make up 19 good routes all from the same trailhead.  It is a good amount of processing for testing personal routes.

This contains only activities with no routes or any other processing completed.