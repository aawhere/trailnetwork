/**
 *
 */
package com.aawhere.web.api.geo;

import static com.aawhere.web.api.geo.GeoWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.activity.ActivityProcessService;
import com.aawhere.geo.GeoCellProcessService;
import com.aawhere.geo.GeoCellSummaryField;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.persist.Filter;
import com.aawhere.swagger.DocSupport;
import com.aawhere.web.api.ApiDocumentationSupport;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * @see com.aawhere.web.api.geo.GeoWebApi
 * @author Brian Chapman
 * 
 */
@Path(SLASH + GEO)
@Api(value = SLASH + GEO, description = "Geo-spatial support information.",
		position = ApiDocumentationSupport.Position.GEO)
@Singleton
public class GeoAdminWebApi
		extends GeoWebApi {

	@Inject
	public GeoAdminWebApi(ActivityProcessService activityProcessService, GeoCellProcessService geoCellProcessService) {
		super(activityProcessService, geoCellProcessService);
	}

	@PUT
	@Path(SLASH + CELLS)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates information related to GeoCells", response = String.class)
	@ApiImplicitParams({ @ApiImplicitParam(value = "Optionally invokes an update process.",
			name = Filter.FIELD.OPTIONS, allowMultiple = false,
			allowableValues = GeoCellSummaryField.ACTIVITY_START_COUNT, dataType = DocSupport.DataType.STRING,
			paramType = DocSupport.ParamType.QUERY) })
	public String cellsPut(@InjectParam Filter filter) {
		if (filter.containsOption(GeoCellSummaryField.ACTIVITY_START_COUNT)) {
			return activityProcessService.activitiesPerGeoCell();
		}
		return null;
	}

	@Path(SLASH + BY_BOUNDS_PATH)
	@POST
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@Consumes({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public GeoCellBoundingBox postGeoCellsFromBoundingBox(@PathParam(BOUNDS) GeoCellBoundingBox boundingBox) {
		return boundingBox;
	}

}
