/**
 *
 */
package com.aawhere.web.api.person.group;

import static com.aawhere.person.group.SocialGroupWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.person.group.SocialGroupMembershipService;
import com.aawhere.person.group.SocialGroupService;
import com.aawhere.web.api.ApiDocumentationSupport;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * @See {@link SocialGroupWebApi}
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(SOCIAL_GROUPS)
@Api(value = SocialGroup.FIELD.SOCIAL_GROUPS, description = "Persons related through their Activities.",
		position = ApiDocumentationSupport.Position.SOCIAL_GROUPS)
public class SocialGroupAdminWebApi
		extends SocialGroupWebApi {

	/**
	 *
	 */
	@Inject
	public SocialGroupAdminWebApi(SocialGroupService groupService, SocialGroupMembershipService socialGroupService) {
		super(groupService, socialGroupService);
	}

	@PUT
	@Path(SLASH + SOCIAL_GROUP_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Encourages the system to update the SocialGroup identified.", response = SocialGroup.class)
	public SocialGroup groupUpdated(@PathParam(SOCIAL_GROUP_ID) SocialGroupId groupId) throws EntityNotFoundException {
		return membershipService.groupUpdated(groupId);
	}

}
