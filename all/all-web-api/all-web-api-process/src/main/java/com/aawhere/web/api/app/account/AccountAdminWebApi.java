/**
 *
 */
package com.aawhere.web.api.app.account;

import static com.aawhere.activity.ActivityWebApiUri.*;
import static com.aawhere.app.account.AccountWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.activity.AccountImportManager;
import com.aawhere.activity.ActivityImportCrawler;
import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityProcessService;
import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountField;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountService;
import com.aawhere.app.account.AccountUtil;
import com.aawhere.app.account.Accounts;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.persist.Filter;
import com.aawhere.person.PersonService;
import com.aawhere.queue.ThrottleQueue;
import com.aawhere.swagger.DocSupport;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * Publishes {@link Account} information.
 * 
 * @author aroller
 * 
 */
@Singleton
@Path(SLASH + ACCOUNTS)
@Api(value = SLASH + ACCOUNTS, description = "Users for Persons at Applications.",
		position = ApiDocumentationSupport.Position.APPLICATIONS)
public class AccountAdminWebApi
		extends AccountWebApi {

	@Inject
	public AccountAdminWebApi(AccountService accountService, PersonService personService,
			ActivityImportService importService, ActivityProcessService activityProcessService) {
		super(accountService, personService, importService, activityProcessService);
	}

	@POST
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@Consumes({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Accounts matching the Filter.")
	public Accounts filterPost(Filter filter) {
		return this.accountService.accounts(filter);
	}

	@PUT
	@Path(SLASH + ACCOUNT_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Updates the Account details from the Application",
			notes = "Refreshes the account from the Application if indicated to do so OR else it uses the provided details to directly update an account. Only those details provided will be updated allowing for partial updates.")
	public
			Account
			accountPut(
					@PathParam(ACCOUNT_ID) AccountId id,
					@ApiParam(value = "Refreshes account from details provided by Application") @DefaultValue(TRUE) @QueryParam(SystemWebApiUri.UPDATE) Boolean refresh,
					@InjectParam Account account) throws BaseException {
		if (BooleanUtils.isTrue(refresh)) {
			return this.importService.accountUpdated(id);
		} else {
			return this.accountService.accountDetailsUpdated(Account.clone(account).id(id).build());
		}
	}

	@PUT
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates the Accounts matching the filter.",
			notes = "Posts tasks that calls for an update of each Account resulting from the filter.  See "
					+ ACCOUNTS_ID_PARAM_PATH, response = Integer.class)
	@ApiImplicitParams({ @ApiImplicitParam(value = "Touch re-saves the Account in the datastore to update indexes.",
			name = Filter.FIELD.OPTIONS, allowMultiple = false, allowableValues = SystemWebApiUri.TOUCH
					+ DocSupport.Allowable.NEXT + AccountField.ACTIVITY_COUNT, dataType = DocSupport.DataType.STRING,
			paramType = DocSupport.ParamType.QUERY) })
	public String put(@InjectParam Filter filter) throws BaseException {
		if (filter.containsOption(AccountField.ACTIVITY_COUNT)) {
			return this.activityProcessService.activitiesPerAccount();
		}
		return String.valueOf(this.accountService.accountsUpdated(filter));
	}

	@PUT
	@Path(SLASH + ACCOUNT_ID_ACTIVITIES_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ThrottleQueue(queues = ActivityWebApiUri.ACTIVITY_IMPORT_QUEUE_NAME)
	@ApiOperation(
			value = "Imports activities for the Account.",
			response = ActivityImportCrawler.class,
			notes = "Asynchronously crawls the identified account after retrieving the first page. \nBy default only new Activities are retreived, but the Deep Option will retrieve all Activiites for the user exhaustively.")
	@ApiImplicitParams({ @ApiImplicitParam(value = "Deep crawls all activities.", name = Filter.FIELD.OPTIONS,
			allowMultiple = true, allowableValues = SystemWebApiUri.DEEP, dataType = DocSupport.DataType.STRING,
			paramType = DocSupport.ParamType.QUERY) })
	public
			AccountImportManager activitiesPut(@PathParam(ACCOUNT_ID) AccountId id, @InjectParam Filter filter)
					throws BaseException {
		return this.importService.activitiesCrawled(id, filter);
	}

	@PUT
	@Path(SLASH + ACTIVITIES)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Posts an import task for the Accounts matching the filter.",
			response = Integer.class,
			notes = "The tasks will be ignored if already posted during the same calendar day. Target a specific application by filtering on the Account ID.  conditions=account-id > gc&conditions=account-id < mm")
	@ApiImplicitParams({
	// FIXME:this deep thing is spreading to multiple methods, but not
	// referencing the same source
	@ApiImplicitParam(value = "Deep crawls all activities.", name = Filter.FIELD.OPTIONS, allowMultiple = true,
			allowableValues = SystemWebApiUri.DEEP, dataType = DocSupport.DataType.STRING,
			paramType = DocSupport.ParamType.QUERY) })
	public
			String activitiesPut(@InjectParam Filter filter) throws BaseException {
		return String.valueOf(this.importService.activitiesCrawledForAccounts(filter));
	}

	/**
	 * Allows bulk import of remote ids given as a csv for a specific {@link ApplicationKey}.
	 * 
	 * @param csv
	 * @param applicationKey
	 * @param filter
	 * @return
	 * @throws BaseException
	 */
	@PUT
	@Path(SLASH + ACTIVITIES)
	@Consumes({ TEXT_CSV_MEDIA_TYPE })
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Asynchronous bulk import of Accounts from an Application.")
	public
			String
			accountsPostCsv(
					@ApiParam(value = "List of remoteIds without the application prefix. One account per line.") String csv,
					@ApiParam(value = "The Application Key where the Accounts reside.") @QueryParam(APPLICATION_KEY) final ApplicationKey applicationKey,
					@InjectParam Filter filter) throws BaseException {
		Iterable<AccountId> accountIds = AccountUtil.accountIdsFromCsv(applicationKey, csv);
		return String.valueOf(importService.activitiesCrawledForAccounts(accountIds, filter));
	}

}
