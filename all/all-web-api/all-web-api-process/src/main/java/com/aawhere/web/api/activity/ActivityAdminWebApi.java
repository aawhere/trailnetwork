/**
 *
 */
package com.aawhere.web.api.activity;

import static com.aawhere.activity.ActivityWebApiUri.*;
import static com.aawhere.activity.timing.ActivityTimingWebApiUri.*;
import static com.aawhere.doc.DocumentWebApiUri.*;
import static com.aawhere.web.api.geo.GeoWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import java.lang.reflect.Array;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.KmlType;

import org.apache.commons.lang.StringUtils;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityExportService;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityIdentifierUtil;
import com.aawhere.activity.ActivityImportCrawler;
import com.aawhere.activity.ActivityImportFailures;
import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityProcessService;
import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.activity.export.kml.ActivityKmlExportService;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.ScreeningResults;
import com.aawhere.activity.timing.ActivityTimingRelations;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.activity.timing.ActivityTimingWebApiUri;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.queue.ThrottleQueue;
import com.aawhere.route.RouteService;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.swagger.DocSupport;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * An Activity is a single "session" with a start/finish and typically a single activity type.
 * 
 * @author Aaron Roller
 * 
 */
@Singleton
@Path(ACTIVITIES)
@Api(value = SLASH + ACTIVITIES, description = "GPS Tracks recorded for an Account.",
		position = ApiDocumentationSupport.Position.ACTIVITIES)
public class ActivityAdminWebApi
		extends ActivityWebApi {

	@Inject
	public ActivityAdminWebApi(ActivityTimingService timingRelationService,
			ActivityImportService activityImportHistoryService, FieldAnnotationDictionaryFactory dictionaryFactory,
			ActivityImportService importService, ActivityKmlExportService kmlExportService,
			ActivityExportService activityExportService, RouteService routeService,
			ActivityProcessService activityProcessService) {
		super(timingRelationService, activityImportHistoryService, dictionaryFactory, importService, kmlExportService,
				activityExportService, routeService, activityProcessService);
	}

	/**
	 * Specifically setup for the workflow so this will throttle the queue if limits are exceeded.
	 * Pretty much the same as {@link #getActivity(ActivityId, Boolean)} otherwise.
	 */
	@POST
	@Path(SLASH + ACTIVITY_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	// TN-814 temporarily disabling this as a backup
	// @ThrottleQueue(queues = { ActivityTimingRelationWebApiUri.TIMINGS_QUEUE_NAME })
			@ApiOperation("Forces a re-attempt of a failed retrieval from an Application.")
			public
			Activity activityPost(@PathParam(ACTIVITY_ID) ActivityId id) throws BaseException {
		return importService.getActivity(new ActivityId(id.getValue()));
	}

	/**
	 * Standard way to import batches of activities from applications.
	 * 
	 * By cells one provides one or more geocells (comma separated) and the resolution desired for
	 * those cells to be exploded to (default is 8).
	 * 
	 * @param applicationKey
	 * @param cells
	 * @param resolution
	 * @return
	 * @throws BaseException
	 */
	@PUT
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Batch importing of Activities from an Application by area ",
			notes = "If application key is not provided, then this will default to updating activities found in the filter.",
			response = Activities.class// note activities is the response to offer the filters
	)
	@ApiImplicitParams({ @ApiImplicitParam(value = "DEEP:Crawls exhaustively.", name = Filter.FIELD.OPTIONS,
			allowMultiple = true, allowableValues = SystemWebApiUri.DEEP + DocSupport.Allowable.NEXT
					+ SystemWebApiUri.TOUCH, dataType = DocSupport.DataType.STRING,
			paramType = DocSupport.ParamType.QUERY) })
	public
			String put(@QueryParam(APPLICATION) ApplicationKey applicationKey, @QueryParam(CELLS) GeoCells cells,
					@DefaultValue(Resolution.VALUE.STRING.EIGHT) @QueryParam(RESOLUTION) Byte resolution,
					@InjectParam Filter filter) throws BaseException {

		if (filter.containsOption(SystemWebApiUri.TOUCH)) {
			return activityProcessService.touch(filter).toString();
		} else if (applicationKey != null) {
			return StringUtils.join(importService.cellCrawlerTasksQueued(	applicationKey,
																			cells,
																			Resolution.valueOf(resolution)),
									System.lineSeparator());
		} else {
			return String.valueOf(importService.updateActivities(filter));
		}
	}

	@PUT
	@Path(SLASH + ACTIVITY_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Forces a re-import of an Activity from an Application.", response = Activity.class)
	public Activity putActivity(@PathParam(ACTIVITY_ID) ActivityId id) throws BaseException {
		return importService.updateActivity(id);
	}

	/**
	 * Given the application specific ID (tn-1234, gc-3925, etc), this will asynchronously retrieve,
	 * store, process and notify the activity per the standard workflow.
	 * 
	 * @param activityIds
	 *            accepts one or more application activity ids separated by commas.
	 * @return the import milestone identifying the application details confirming receipt of the
	 *         request, but not verifying the activity will be successfully imported.
	 * @throws UnknownApplicationException
	 */
	@PUT
	@Path(SLASH + ACTIVITY_ID_IMPORTS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation("Provides the import failure for one or more activities and queues up the rest that haven't already failed.")
	public
			ActivityImportFailures importActivityById(@PathParam(ACTIVITY_ID) String activityIds)
					throws UnknownApplicationException {

		return importService.importActivities(ActivityIdentifierUtil.idValuesToList(activityIds));

	}

	@PUT
	@Consumes({ TEXT_TSV_MEDIA_TYPE })
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Asynchronous bulk import of Activities by IDs.",
			notes = "Provide fully qualified activity ids separated by whitespace OR the applicationKey and the remote ids at the application.")
	public
			ActivityImportFailures
			putCsv(@ApiParam(value = "The ids whitespace separated,") String csv,
					@ApiParam(value = "Application Key if remote ids provided.", required = false) @QueryParam(APPLICATION_KEY) final ApplicationKey applicationKey)
					throws UnknownApplicationException {
		Iterable<ActivityId> activityIds = ActivityIdentifierUtil.activityIdsFromWhitespaceSeparated(	csv,
																										applicationKey);
		return importService.importActivities(activityIds);
	}

	@GET
	@Produces(KML_CONTENT_TYPE)
	@ApiOperation(value = "Google Earth view of multiple Activities limited by the Filter.")
	public JAXBElement<KmlType> filterGetKml(@InjectParam Filter filter) {
		return kmlExportService.filterActivities(filter).getElement();
	}

	@PUT
	@Path(SLASH + APPLICATION_CELLS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ThrottleQueue(queues = { ActivityWebApiUri.ACTIVITY_IMPORT_QUEUE_NAME })
	@ApiOperation(value = "Imports the Activities located in the area of the given GeoCells.",
			response = ActivityImportCrawler.class)
	public ActivityImportCrawler
			cellsApplicationImport(@PathParam(APPLICATION_KEY) ApplicationKey applicationKey,
					@PathParam(CELLS) GeoCells cells, @InjectParam Filter filter,
					@QueryParam(RESOLUTION) Resolution resolution) throws BaseException {
		return importService.activitiesCrawled(applicationKey, cells, filter);
	}

	/**
	 * Given an area this will find all those timings candidates in an area that hasn't yet been
	 * processed.
	 * 
	 * @param cells
	 * @param filter
	 * @param resolution
	 * @return
	 */
	@PUT
	@Path(SLASH + TIMINGS_FOR_ACTIVITIES_IN_CELLS_PATH)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ThrottleQueue(queues = { ActivityTimingWebApiUri.TIMINGS_COURSE_ATTEMPT_QUEUE_NAME,
			ActivityTimingWebApiUri.TIMINGS_COURSE_ATTEMPT_SCREENING_QUEUE_NAME, ACTIVITY_IMPORT_QUEUE_NAME },
			maxSize = MAX_TIMING_RELATIONS_QUEUE_SIZE)
	public String cellsProcessTimings(@PathParam(CELLS) GeoCells cells) {
		return String.valueOf(timingRelationService.queueMissedTimingCandidates(cells));
	}

	// ============ TIMINGS (here because activities/{activityId}/....timings)=================
	/**
	 * Given the course this will return the timings the attempt achieved when riding that course.
	 * 
	 * @param course
	 * @param attempt
	 * @return
	 * @throws EntityNotFoundException
	 */
	@GET
	@Path(SLASH + TIMINGS_COURSE_ATTEMPT_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Provides the timing results for the course/attempt Activity ids.",
			notes = "A synonym for activities/timings/{activityTimingId}, this will find an existing timing or process a new one and store the results. If an activity is not yet imported an attempt to import will be made.",
			response = ActivityTiming.class)
	public
			ActivityTiming getTimingRelation(@PathParam(COURSE) ActivityId course,
					@PathParam(ATTEMPT) ActivityId attempt) throws EntityNotFoundException {
		return timingRelationService.getOrProcessTiming(course, attempt);
	}

	@GET
	@Path(SLASH + TIMINGS_ACTIVITY_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "View timings for an activity", response = ActivityTimingRelations.class)
	public ActivityTimingRelations getTimingRelations(@PathParam(ACTIVITY_ID) ActivityId activityId,
			@InjectParam Filter filter) throws EntityNotFoundException {
		return timingRelationService.getTimingRelations(activityId, filter);
	}

	@GET
	@Path(SLASH + TIMINGS_COURSE_ATTEMPT_PATH)
	@Produces({ KML_CONTENT_TYPE })
	@ApiOperation(value = "DOCME: Documention coming soon...", response = KmlType.class)
	public JAXBElement<KmlType> getTimingRelationKml(@PathParam(COURSE) ActivityId course,
			@PathParam(ATTEMPT) ActivityId attempt) throws EntityNotFoundException {
		return kmlExportService.getCourseAttemptDeviationKml(course, attempt).getElement();
	}

	/**
	 * Used by administrators and task queue to update the timing relationships discovered for a
	 * single activity of interest. The relations are created from both a course and attempt point
	 * of view.
	 * 
	 * @param activityId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@PUT
	@Path(SLASH + TIMINGS_ACTIVITY_PATH)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ThrottleQueue(queues = { ActivityTimingWebApiUri.TIMINGS_COURSE_ATTEMPT_SCREENING_QUEUE_NAME },
			maxSize = MAX_TIMING_RELATIONS_QUEUE_SIZE)
	@ApiOperation(value = "DOCME: Documention coming soon...", response = Integer.class)
	public String putTimingRelations(@PathParam(ACTIVITY_ID) ActivityId activityId) throws EntityNotFoundException {
		return timingRelationService.queueAttemptCandidates(activityId).toString();

	}

	@PUT
	@Path(SLASH + RouteWebApiUri.ACTIVITY_ID_ROUTES_TIMINGS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Looks for Routes that this Activity may complete", response = ScreeningResults.class)
	public ScreeningResults routeTimingsPut(@PathParam(ACTIVITY_ID) ActivityId activityId)
			throws EntityNotFoundException {
		return this.routeService.completionCandidatesCourseScreener(activityId);
	}

	@PUT
	@Path(SLASH + RouteWebApiUri.ACTIVITY_ID_ROUTE_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documention coming soon...", response = Integer.class)
	public String completionsPut(@PathParam(ACTIVITY_ID) ActivityId activityId) throws EntityNotFoundException {
		return String.valueOf(this.routeService.completionsUpdated(activityId));
	}

	/**
	 * Provides the original document given the activity id. The returned file is gzipped, but the
	 * file extension provides convenient naming.
	 * 
	 * Examples:
	 * 
	 * activities/gc-12345.gpx or activities/2p-12345.csv
	 * 
	 * 
	 * @param activityId
	 * @param extension
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Path(SLASH + ACTIVITY_ID_PARAM + FILE_EXTENSION_PARAM_SUFFIX)
	@GET
	@Produces(GZIP_MIME_TYPE)
	@ApiOperation(value = "DOCME: Documention coming soon...", response = Array.class)
	public byte[] getDocumentContents(@PathParam(ACTIVITY_ID) ActivityId activityId,
			@PathParam(FILE_EXTENSION) String extension) throws EntityNotFoundException {
		return activityService.getDocumentContents(activityId);
	}

	@Path(SLASH + ACTIVITY_ID_PARAM + SLASH + DOCUMENTS)
	@GET
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documention coming soon...", response = ArchivedDocument.class)
	public ArchivedDocument getDocument(@PathParam(ACTIVITY_ID) ActivityId activityId) throws EntityNotFoundException {
		return activityService.getArchivedDocument(activityId);
	}

}
