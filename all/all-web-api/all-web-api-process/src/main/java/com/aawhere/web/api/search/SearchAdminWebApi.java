/**
 *
 */
package com.aawhere.web.api.search;

import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonProcessService;
import com.aawhere.person.PersonWebApiUri;
import com.aawhere.route.RouteService;
import com.aawhere.search.MaxIndexAttemptsExceeded;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.search.UnknownIndexException;
import com.aawhere.swagger.DocSupport;
import com.aawhere.trailhead.TrailheadService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * @see SearchWebApi
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(SearchWebApiUri.SEARCH)
@Api(value = SearchWebApiUri.SEARCH, description = "Search TrailNetwork entity data.")
public class SearchAdminWebApi
		extends SearchWebApi {

	@Inject
	public SearchAdminWebApi(SearchService searchService, RouteService routeService, TrailheadService trailheadService,
			PersonProcessService personProcessService) {
		super(searchService, routeService, trailheadService, personProcessService);
	}

	@PUT
	@Path(SLASH + PersonWebApiUri.PERSONS)
	@ApiOperation(
			value = "Index all persons.",
			response = Integer.class,
			notes = "Indexes all persons known to the TrailNetwork by placing them on the search queue. Does not identify previously indexed persons that have since been deleted. Returns the number queued for indexing.")
	public
			Integer indexPersons() throws MaxIndexAttemptsExceeded {
		return personProcessService.indexAllPeople();
	}

	@PUT
	@Path(SLASH + PersonWebApiUri.PERSONS_PERSON_ID_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Index person.", response = SearchDocument.class)
	public SearchDocument indexPerson(@PathParam(PersonWebApiUri.PERSON_ID) PersonId personId)
			throws MaxIndexAttemptsExceeded, EntityNotFoundException {
		return personProcessService.indexPerson(personId);
	}

	@DELETE
	@ApiOperation(value = "Delete all documents from an index.", tags = SearchWebApiUri.SEARCH)
	@ApiImplicitParams(@ApiImplicitParam(name = Filter.FIELD.SEARCH_INDEXES, value = "The index to delete.",
			allowMultiple = false, allowableValues = INDEXES_LIST, paramType = DocSupport.ParamType.QUERY,
			dataType = DocSupport.DataType.STRING))
	public void delete(@InjectParam Filter filter) throws UnknownIndexException {
		searchService.delete(filter);
	}
}
