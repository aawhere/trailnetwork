/**
 *
 */
package com.aawhere.web.api.person;

import static com.aawhere.person.PersonWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import java.net.HttpURLConnection;
import java.util.HashMap;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.activity.ActivityService;
import com.aawhere.all.AllProcessNestedField;
import com.aawhere.identity.IdentityRole;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.PersistMessage;
import com.aawhere.person.Person;
import com.aawhere.person.PersonField;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonIds;
import com.aawhere.person.PersonProcessService;
import com.aawhere.person.PersonService;
import com.aawhere.person.group.SocialGroupMembershipService;
import com.aawhere.route.Route;
import com.aawhere.route.RouteCompletion;
import com.aawhere.route.RouteService;
import com.aawhere.security.SecurityRoles;
import com.aawhere.swagger.DocSupport;
import com.aawhere.swagger.DocSupport.Allowable;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;

/**
 * @see PersonWebApi
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(PERSONS)
@Api(value = PERSONS, description = "Individuals that have one or more Accounts.",
		position = ApiDocumentationSupport.Position.PERSONS)
public class PersonAdminWebApi
		extends PersonWebApi {

	/**
	 *
	 */
	@Inject
	public PersonAdminWebApi(PersonService personService, SocialGroupMembershipService socialGroupService,
			ActivityService activityService, RouteService routeService, PersonProcessService personProcessService) {
		super(personService, socialGroupService, activityService, routeService, personProcessService);
	}

	@PUT
	@RolesAllowed({ SecurityRoles.ADMIN, SecurityRoles.TRUSTED_APP })
	@Path(SLASH + PERSON_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Requests the system to update the Person.", response = Person.class)
	@ApiImplicitParams({
			@ApiImplicitParam(value = PersonField.Doc.IDENTITY_SHARED_DESCRIPTION,
					name = PersonField.Absolute.IDENTITY_SHARED, dataType = DocSupport.DataType.BOOLEAN,
					paramType = DocSupport.ParamType.QUERY),
			@ApiImplicitParam(value = PersonField.Doc.NAME_DESCRIPTION, name = PersonField.Absolute.NAME,
					dataType = DocSupport.DataType.STRING, paramType = DocSupport.ParamType.QUERY),
			@ApiImplicitParam(value = PersonField.Doc.EMAIL_DESCRIPTION, name = PersonField.Absolute.EMAIL,
					dataType = DocSupport.DataType.STRING, paramType = DocSupport.ParamType.QUERY),
			@ApiImplicitParam(value = PersonField.Doc.IDENTITY_ROLES_DESCRIPTION,
					name = PersonField.Absolute.IDENTITY_ROLES, dataType = DocSupport.DataType.STRING_ARRAY,
					paramType = DocSupport.ParamType.QUERY, allowableValues = IdentityRole.Name.ADMIN + Allowable.NEXT
							+ IdentityRole.Name.DEVELOPER + Allowable.NEXT + IdentityRole.Name.USER + Allowable.NEXT,
					allowMultiple = true) })
	public Person personPut(@PathParam(PERSON_ID) PersonId personId, @InjectParam Filter filter)
			throws EntityNotFoundException {
		return this.personService.updated(personId, filter);
	}

	@PUT
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates the Persons found in the Filter.", response = Integer.class)
	public String personsPut(@InjectParam Filter filter) {
		return String.valueOf(this.personService.updated(filter));
	}

	@PUT
	@Path(SLASH + PERSONS_IDS_MERGED_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Merges the Persons identified into one.", response = Person.class)
	public Person personsMergedPut(@PathParam(PERSONS_IDS) @ApiParam(value = "Two or more PersonIds.",
			allowMultiple = true) PersonIds personIds) throws EntityNotFoundException {
		return this.personService.merged(personIds);
	}

	// FIXME: figure out what this does and document it.
	@PUT
	@Path(SLASH + PERSONS_IDS_MERGED_NOTIFIED_PATH)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	public String personsMergedNotifiedPut(@PathParam(PERSONS_IDS) PersonIds personIds) throws EntityNotFoundException {
		HashMap<String, Integer> results = new HashMap<String, Integer>();
		results.put(Route.FIELD.DOMAIN, this.routeService.personsMerged(personIds));
		return results.toString();
	}

	@PUT
	@Path(SLASH + PERSON_ID_VIEWS_PATH)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Indicates the Person's profile has been viewed once.", response = Integer.class)
	@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "The updated view count including this one.")
	public String viewCountPut(@PathParam(PERSON_ID) PersonId personId) {
		return String.valueOf(this.personService.viewed(personId));
	}

}
