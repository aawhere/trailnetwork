/**
 * 
 */
package com.aawhere.web.api.app;

import static com.aawhere.app.ApplicationWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.activity.ActivityProcessService;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationField;
import com.aawhere.app.ApplicationMessage;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.app.account.AccountProcessService;
import com.aawhere.persist.Filter;
import com.aawhere.swagger.DocSupport;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * You know, specializes in {@link Application}s which is managed by an Enum
 * {@link KnownApplication}.
 * 
 * @author aroller
 * 
 */
@Singleton
@Path(SLASH + APPLICATIONS)
@Api(value = SLASH + APPLICATIONS, description = "Websites and Device Apps that provide Activities.",
		position = ApiDocumentationSupport.Position.APPLICATIONS)
public class ApplicationAdminWebApi
		extends ApplicationWebApi {

	@Inject
	public ApplicationAdminWebApi(AccountProcessService accountProcessService,
			ActivityProcessService activityProcessService) {
		super(accountProcessService, activityProcessService);
	}

	@PUT
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates statistics for each Application.", response = String.class)
	@ApiImplicitParams({ @ApiImplicitParam(value = "Options to selectively update portions of the system.",
			name = Filter.FIELD.OPTIONS, allowMultiple = false, allowableValues = ApplicationMessage.Doc.ALLOWABLE,
			dataType = DocSupport.DataType.STRING, paramType = DocSupport.ParamType.QUERY) })
	public String applicationsPut(@InjectParam Filter filter) throws UnknownApplicationException {
		if (filter.containsOption(ApplicationField.ACCOUNT_COUNT)) {
			return accountProcessService.accountsPerApplication();
		}
		if (filter.containsOption(ApplicationField.ACTIVITY_COUNT)) {
			return activityProcessService.activitiesPerApplication();
		}
		return null;
	}
}
