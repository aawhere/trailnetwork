/**
 *
 */
package com.aawhere.web.api.trailhead;

import static com.aawhere.route.RouteWebApiUri.*;
import static com.aawhere.trailhead.TrailheadWebApiUri.*;
import static com.aawhere.web.api.geo.GeoWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import java.net.URI;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.aawhere.activity.Activities;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.route.AlternateRouteGroupOrganizer;
import com.aawhere.route.RouteExportService;
import com.aawhere.route.RouteService;
import com.aawhere.route.RouteTrailheadChooser;
import com.aawhere.route.RouteTrailheadOrganizer;
import com.aawhere.trailhead.TrailheadExportService;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadProcessService;
import com.aawhere.trailhead.TrailheadService;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.xml.UrlDisplay;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * @see TrailheadWebApi
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(SLASH + TRAILHEADS)
@Api(value = SLASH + TRAILHEADS, description = "Meeting places where Routes start & finish.",
		position = ApiDocumentationSupport.Position.TRAILHEADS)
public class TrailheadAdminWebApi
		extends TrailheadWebApi {

	@Inject
	public TrailheadAdminWebApi(TrailheadService trailheadService, RouteService routeService,
			TrailheadExportService exportService, RouteExportService routeExportService,
			TrailheadProcessService trailheadProcessService, FieldAnnotationDictionaryFactory dictionaryFactory) {
		super(trailheadService, routeService, exportService, routeExportService, trailheadProcessService,
				dictionaryFactory);

	}

	/**
	 * Updates the trailhead details for the given trailhead. Use {@link #trailheadPut(TrailheadId)}
	 * for organizing trailheads in an area.
	 * 
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@PUT
	@Path(SLASH + TRAILHEAD_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Updates the trailhead details for the given trailhead. Use #trailheadPut(TrailheadId) for organizing trailheads in an area.")
	public
			RouteTrailheadOrganizer trailheadPut(@PathParam(TRAILHEAD_ID) TrailheadId trailheadId)
					throws EntityNotFoundException {
		return routeService.trailheadUpdated(trailheadId);
	}

	// ------------------------------------------------------------------------
	// ----- ROUTES -------
	// ------------------------------------------------------------------------

	/**
	 * Updates the groups starting from the given trailhead.
	 * 
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@PUT
	@Path(SLASH + TRAILHEAD_ID_ROUTES_ALTERNATES)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public AlternateRouteGroupOrganizer routeGroupsForTrailheadPut(@PathParam(TRAILHEAD_ID) TrailheadId trailheadId)
			throws EntityNotFoundException {
		return routeService.alternatesOrganized(trailheadId);
	}

	/**
	 * This will update all trailheads, merging adjacent trailheads as required.
	 * 
	 * @return
	 */
	@PUT
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "This will update all trailheads, merging adjacent trailheads as required.")
	public String trailheadsPut(@InjectParam Filter filter) {
		return String.valueOf(routeService.trailheadsUpdated(filter));
	}

	/**
	 * Updates the trailheads in the area of the given route. FIXME:determine if this method will
	 * remain and upgrade organized into a constant
	 * 
	 * @since 20
	 * */
	@PUT
	@Path(SLASH + ORGANIZED)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public RouteTrailheadChooser trailheadsOrganizedPut(@QueryParam(BOUNDS) GeoCellBoundingBox bounds)
			throws EntityNotFoundException {
		InvalidArgumentException.notNull(bounds, BOUNDS_MESSAGE);
		return routeService.trailheadsOrganized(bounds);
	}

	/**
	 * Given the trailhead id this will organize those trailheads around it minimizing duplicates,
	 * etc.
	 * 
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@PUT
	@Path(SLASH + TRAILHEAD_ID_ORGANIZED_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public RouteTrailheadChooser trailheadOrganizedPut(@PathParam(TRAILHEAD_ID) TrailheadId trailheadId)
			throws EntityNotFoundException {
		return routeService.trailheadsOrganized(trailheadId);
	}

	@PUT
	@Path(SLASH + JOBS_JOB_NAME_PATH)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Invokes selected processing jobs.", response = URI.class)
	public String jobsPut(@PathParam(JOB_NAME) TrailheadProcessService.JobName jobName) {
		return trailheadProcessService.jobStarted(jobName).toString();
	}

	@PUT
	@Path(SLASH + JOBS + SLASH + SystemWebApiUri.NAMES)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates the names for the trailheads given the activities matching the filter",
			response = Activities.class)
	public UrlDisplay namesJob(@InjectParam Filter filter) {
		return new UrlDisplay(trailheadProcessService.trailheadNamesPipeline(filter));
	}

}
