/**
 *
 */
package com.aawhere.web.api.route;

import static com.aawhere.route.RouteWebApiUri.*;
import static com.aawhere.route.event.RouteEventWebApiUri.*;
import static com.aawhere.trailhead.TrailheadWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.httpclient.HttpStatus;

import com.aawhere.activity.Activities;
import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.field.FieldMessage;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.DeletionConfirmationException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilteredResponse;
import com.aawhere.persist.PersistMessage;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.route.CourseNotWorthyException;
import com.aawhere.route.Route;
import com.aawhere.route.RouteCompletionService;
import com.aawhere.route.RouteCompletionSocialGroupOrganizer;
import com.aawhere.route.RouteDiscoveryPipeline;
import com.aawhere.route.RouteExportService;
import com.aawhere.route.RouteId;
import com.aawhere.route.RouteMergeDetails;
import com.aawhere.route.RouteMessage;
import com.aawhere.route.RouteProcessService;
import com.aawhere.route.RouteRelator;
import com.aawhere.route.RouteService;
import com.aawhere.route.RouteTrailheadOrganizer;
import com.aawhere.route.RouteUpdateDetails;
import com.aawhere.route.Routes;
import com.aawhere.route.event.RouteEventId;
import com.aawhere.route.event.RouteEventMessage;
import com.aawhere.route.event.RouteEventService;
import com.aawhere.search.SearchUtils;
import com.aawhere.swagger.DocSupport;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.web.api.trailhead.TrailheadWebApi;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.xml.UrlDisplay;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

/**
 * @see RouteWebApi
 * @author Brian Chapman
 * 
 */
@Singleton
@Path(ROUTES)
@Api(value = ROUTES, description = RouteMessage.Doc.DESCRIPTION, position = ApiDocumentationSupport.Position.ROUTES)
public class RouteAdminWebApi
		extends RouteWebApi {

	/**
	 *
	 */
	@Inject
	public RouteAdminWebApi(RouteService routeService, RouteExportService exportService,
			RouteCompletionService completionService, RouteEventService routeEventService,
			RouteProcessService routeProcessService) {
		super(routeService, exportService, completionService, routeEventService, routeProcessService);
	}

	/**
	 * A special method since cron requires a get protocol. Method is individually protected since
	 * it will update routes and is better left to the system than an impatient programmer.
	 * 
	 * @return
	 */
	// @RolesAllowed(ADMIN) - Dev is getting a 403. Try without authentication
	@GET
	@Path(SLASH + STALE)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	public String staleGet() {
		return stalePut();
	}

	@GET
	@Path(SLASH + SearchUtils.DATASTORE)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Admin ease to query datastore instead of search service.")
	@ApiImplicitParams(@ApiImplicitParam(name = Filter.FIELD.OPTIONS, paramType = DocSupport.ParamType.QUERY,
			dataType = DocSupport.DataType.STRING, defaultValue = SearchUtils.DATASTORE))
	public Routes routesDatastoreGet(@InjectParam Filter filter) {
		Routes routes = service.routes(filter);
		return routes;
	}

	@PUT
	@Path(SLASH + STALE)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates all Routes that are stale (called automatically periodically)")
	public String stalePut() {
		return String.valueOf(routeProcessService.routesStaleOrganizer());
	}

	@PUT
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates the Routes matching the Filter.", response = Integer.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = Filter.FIELD.ORDER_BY, allowMultiple = false,
					allowableValues = RouteMessage.Doc.SORT_ALLOWABLE, defaultValue = RouteMessage.Doc.SORT_DEFAULT,
					paramType = DocSupport.ParamType.QUERY, dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE),
			@ApiImplicitParam(name = Filter.FIELD.OPTIONS, value = "Trigger optional functions", allowMultiple = true,
					defaultValue = Route.FIELD.OPTION.FORCE, allowableValues = Route.FIELD.OPTION.FORCE
							+ DocSupport.Allowable.NEXT + Route.FIELD.OPTION.TIMINGS + DocSupport.Allowable.NEXT
							+ Route.FIELD.OPTION.STALE, paramType = DocSupport.ParamType.QUERY,
					dataType = PersistMessage.Doc.FILTER_OPTIONS_DATA_TYPE), })
	public String put(@InjectParam Filter filter) {
		return String.valueOf(service.routesUpdated(filter));
	}

	@DELETE
	@Path(SLASH + ROUTE_ID_PARAM)
	@ApiOperation(value = "Removes the Route and it's Associated Entities.")
	public void delete(@PathParam(ROUTE_ID) RouteId routeid) {
		service.routeDeleted(routeid);
	}

	@DELETE
	@ApiOperation(
			value = "Removes the Routes and it's Associated Entities.",
			notes = "Confirmation is required to send as an option.  Details are found in the code and purposely not documented nor set as a default for secrecy and integrity. No response, but Routes is declared so you can easily query.",
			response = Routes.class)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	public
			String delete(@InjectParam Filter filter) throws DeletionConfirmationException {
		return String.valueOf(service.routesDeleted(filter));

	}

	@PUT
	@Path(SLASH + JOBS + SLASH + RouteDiscoveryPipeline.JOB_NAME)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Discovers routes from Activities limited by the activity filter",
			notes = "Looking at the activities included, courses are discovered by comparing spatial coverage similarity. Routes are created when multiple courses are found.  Activities are searched for completions of the new routes and timings are stored.  The Routes are updated and results persisted in datastore and search service.",
			response = Activities.class)
	public
			UrlDisplay routeDiscoveryPipeline(@InjectParam Filter filter,
					@InjectParam MapReduceJobSettings.Options options) {
		return routeProcessService.discover(filter, options);
	}

	@PUT
	@Path(SLASH + JOBS + SLASH + "RouteTrailheadsOrganizedPipeline")
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Updates and organizes routes & trailheads", //
			notes = "Filtering is limited to those restrictions enforced by map reduce.  Common filters is startArea-cells and stale",
			response = Routes.class/* for filtering */)
	public
			UrlDisplay routesTrailheadsUpdated(@InjectParam Filter filter,
					@InjectParam MapReduceJobSettings.Options options) {
		return routeProcessService.routeTrailheadOrganizer(filter, options);
	}

	@PUT
	@Path(SLASH + ROUTE_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...", response = RouteUpdateDetails.class)
	public RouteUpdateDetails put(@PathParam(ROUTE_ID) RouteId routeId, @QueryParam(FORCE) Boolean force)
			throws EntityNotFoundException, CourseNotWorthyException {
		return service.routeUpdated(routeId, force);
	}

	@PUT
	@Path(SLASH + ROUTE_IDS_MERGED_PATH)
	@ApiOperation(value = "DOCME: Documentation Coming Soon...", response = RouteUpdateDetails.class)
	public RouteMergeDetails routesMergedPut(@PathParam(ROUTE_IDS) String routeIdsCsv) {
		return this.service.routesMerged(IdentifierUtils.idsFrom(routeIdsCsv, RouteId.class));
	}

	/**
	 * Updates the trailhead details for the given trailhead. Use {@link #trailheadPut(TrailheadId)}
	 * for organizing trailheads in an area.
	 * 
	 * @deprecated use {@link TrailheadWebApi}
	 * @param trailheadId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Deprecated
	@PUT
	@Path(SLASH + TRAILHEADS_TRAILHEAD_ID_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	public RouteTrailheadOrganizer trailheadPut(@PathParam(TRAILHEAD_ID) TrailheadId trailheadId)
			throws EntityNotFoundException {
		return service.trailheadUpdated(trailheadId);
	}

	// ======================= COMPLETIONS ===================================

	@PUT
	@Path(SLASH + COMPLETIONS)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public String completionPut(@InjectParam Filter filter) {
		return String.valueOf(this.service.completionsUpdated(filter));
	}

	@PUT
	@Path(SLASH + COMPLETIONS_SOCIAL_GROUPS_PATH)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Asynchronoushly updates SocialGroups for Routes matching filter", response = Integer.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = Filter.FIELD.CONDITIONS, allowMultiple = true,
			defaultValue = "route-score > 0", paramType = DocSupport.ParamType.QUERY, dataType = "FilterCondition"), })
	public String completionsSocialGroupsPut(@InjectParam Filter filter) {
		return String.valueOf(service.completionSocialGroupMembershipsUpdated(filter));
	}

	@PUT
	@Path(SLASH + ROUTE_ID_COMPLETIONS_SOCIAL_GROUPS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates the SocialGroup and Memberships for the Route.",
			notes = "Creates a new SocialGroup for every  ActivityType that has been completed many times.",
			response = RouteCompletionSocialGroupOrganizer.class)
	@ApiResponse(message = EntityNotFoundException.Doc, code = EntityNotFoundException.CODE)
	public RouteCompletionSocialGroupOrganizer completionsSocialGroupsPut(@PathParam(ROUTE_ID) @ApiParam(
			value = "One specific RouteId.", defaultValue = ApiDocumentationSupport.EXAMPLE_ROUTE_ID) RouteId routeId)
			throws EntityNotFoundException {
		return this.service.completionSocialGroupMembershipsUpdated(routeId);
	}

	@PUT
	@Path(SLASH + ROUTE_ID_COMPLETIONS_PATH)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...", response = String.class)
	public String completionsPut(@PathParam(ROUTE_ID) RouteId routeId) throws EntityNotFoundException {
		return String.valueOf(this.service.completionsUpdated(routeId));
	}

	@PUT
	@Path(SLASH + ROUTE_ID_TIMINGS_PATH)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Ensures all timings related to the Route have been discovered.",
			notes = "Post timings tasks for the Route ensuring timings have been thoroughly discovered.  The number of tasks posted is returned.",
			response = Integer.class)
	@ApiResponses({ @ApiResponse(message = "When the Route is not found.", code = EntityNotFoundException.CODE) })
	public
			String timingsPut(@PathParam(ROUTE_ID) RouteId id) throws EntityNotFoundException {
		return String.valueOf(service.timingsUpdated(id));
	}

	@PUT
	@Path(SLASH + ROUTE_ID_RELATED_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates the related Routes for a Route.", response = RouteRelator.class)
	public FilteredResponse<RouteRelator> relatedPut(@PathParam(ROUTE_ID) RouteId routeId)
			throws EntityNotFoundException {
		return FilteredResponse.create(this.service.relatorsUpdated(routeId)).build();
	}

	@PUT
	@Path(SLASH + ROUTE_ID_VIEWS_PATH)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Increments the number of times a Route has been viewed.", response = Integer.class)
	public String viewCountPut(@PathParam(ROUTE_ID) RouteId routeId) {
		return String.valueOf(this.service.viewed(routeId));
	}

	@PUT
	@Path(SLASH + EVENTS_ROUTE_EVENT_ID_COMPLETIONS_SOCIAL_GROUPS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates the SocialGroup for completions of the RouteEvent.", response = SocialGroup.class)
	@ApiResponses({
			@ApiResponse(code = HttpStatus.SC_NO_CONTENT,
					message = "When the RouteEvent has too low of a score to be in a SocialGroup."),
			@ApiResponse(code = EntityNotFoundException.CODE, message = "The RouteEvent is not found.") })
	public SocialGroup eventCompletionsSocialGroupPut(@PathParam(ROUTE_EVENT_ID) @ApiParam(
			value = "The composite Route-Event id.") RouteEventId routeEventId) throws EntityNotFoundException {
		return this.service.eventCompletionsSocialGroupUpdated(routeEventId);
	}

	@PUT
	@Path(SLASH + EVENTS)
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	@ApiOperation(
			value = "Updates the RouteEvents matching the filter.",
			response = Integer.class,
			notes = "Provides multiple ways to update a subset of events. A standard simple touch is useful for updating indexes, otherwise the associated Route will be requested to update since Events are managed during completion processing.")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Filter.FIELD.ORDER_BY, paramType = DocSupport.ParamType.QUERY,
					dataType = FieldMessage.Doc.FIELD_KEY_DATA_TYPE,
					allowableValues = RouteEventMessage.Doc.ORDER_BY_ALLOWABLE,
					value = RouteEventMessage.Doc.ORDER_BY_DESCRIPTION),
			@ApiImplicitParam(name = Filter.FIELD.OPTIONS, paramType = DocSupport.ParamType.QUERY,
					dataType = DocSupport.DataType.STRING, allowableValues = SystemWebApiUri.TOUCH,
					value = "Pick the type of update.") })
	public
			String putEvents(@InjectParam Filter filter) {

		return String.valueOf(eventService.routeEventsUpdated(filter));
	}

}
