package com.aawhere.web.api.activity.timing;

import static com.aawhere.activity.timing.ActivityTimingWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.aawhere.activity.ApplicationImportInstructionsNotFoundException;
import com.aawhere.activity.export.kml.ActivityKmlExportService;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingProcessService;
import com.aawhere.activity.timing.ActivityTimingRelations;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.io.EndpointNotFoundException;
import com.aawhere.io.IoException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.PersistMessage;
import com.aawhere.route.RouteCompletions;
import com.aawhere.route.RouteService;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.swagger.DocSupport;
import com.aawhere.util.rb.StatusMessages;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.xml.UrlDisplay;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.InjectParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * Serves content to the web api for {@link ActivityTimingRelations}.
 * 
 * @author aroller
 * 
 */
@Singleton
@Path(SLASH + ACTIVITIES_TIMINGS_PATH)
@Api(value = ACTIVITIES_TIMINGS_PATH, description = ActivityTimingWebApi.DESCRIPTION,
		position = ApiDocumentationSupport.Position.TIMINGS)
public class ActivityTimingAdminWebApi
		extends ActivityTimingWebApi {

	/**
	 * 
	 */
	@Inject
	public ActivityTimingAdminWebApi(ActivityTimingService service, RouteService routeService,
			ActivityKmlExportService kmlExportService, ActivityTimingProcessService timingProcessService) {
		super(service, routeService, kmlExportService, timingProcessService);

	}

	@PUT
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates the timings (based on the option chosen) matching the results of the filter",
			notes = "<b>Options</b><ul><li>Touch will load/reload without doing any processing.</li></ul>")
	@ApiImplicitParams({ @ApiImplicitParam(name = Filter.FIELD.OPTIONS, allowableValues = SystemWebApiUri.TOUCH,
			required = true, allowMultiple = false, paramType = DocSupport.ParamType.QUERY,
			dataType = PersistMessage.Doc.FILTER_OPTIONS_DATA_TYPE) })
	public UrlDisplay timingsPut(@InjectParam Filter filter) {
		UrlDisplay result;
		if (filter.containsOption(SystemWebApiUri.TOUCH)) {
			result = timingProcessService.touchActivityTimingRelation(filter);
		} else {
			result = null;
		}
		return result;
	}

	/**
	 * Screen is the parameter that indicates if this is screen only or if full processing should be
	 * done.
	 */
	@PUT
	@Path(SLASH + TIMINGS_ID_PARAM)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Updates the timing matching the id.")
	public
			ActivityTiming
			putTiming(
					@PathParam(TIMINGS_ID) ActivityTimingId id,
					@ApiParam("efficiently screens before deep processing") @DefaultValue(FALSE) @QueryParam(SCREEN) Boolean screen)
					throws EntityNotFoundException {
		return this.timingService.getOrProcessTiming(id, screen, Boolean.TRUE);
	}

	@PUT
	@Path(SLASH + RouteWebApiUri.TIMINGS_ID_ROUTES_COMPLETIONS_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Creates a route completion for the course completed timing")
	public RouteCompletions completionsPut(@PathParam(TIMINGS_ID) ActivityTimingId id) throws EntityNotFoundException {
		return this.routeService.completionsUpdated(id);
	}

	@GET
	@Path(SLASH + TIMINGS_COURSE_ALERT_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Webhook alerting course dependencies when a timing is created or updated")
	public StatusMessages getTimingCourseAlert(@PathParam(TIMINGS_ID) ActivityTimingId timingsRelationId)
			throws EntityNotFoundException, EndpointNotFoundException, ApplicationImportInstructionsNotFoundException,
			UnknownApplicationException, IoException {
		return timingService.notifyCourseApplicationCourseCompleted(timingsRelationId);
	}

	@GET
	@Path(SLASH + TIMINGS_ATTEMPT_ALERT_PATH)
	@Produces({ JSON_MEDIA_TYPE, MediaType.APPLICATION_XML, NONE_MEDIA_TYPE })
	@ApiOperation(value = "Webhook alerting attempt dependencies when a timing is created or updated")
	public StatusMessages getTimingAttemptAlert(@PathParam(TIMINGS_ID) ActivityTimingId timingsRelationId)
			throws EntityNotFoundException, EndpointNotFoundException, ApplicationImportInstructionsNotFoundException,
			UnknownApplicationException, IoException {
		return timingService.notifyAttemptApplicationCourseCompleted(timingsRelationId);
	}

}
