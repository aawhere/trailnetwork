package com.aawhere.web.api;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.all.process.AllProcessModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 * The hook from web.xml that kicks off Guice configuration of the entire system.
 * 
 * @see PublicAppGuiceServletContextListenerS
 * @see WebApiApplication
 * @author aroller
 * 
 */
public class AdminAppGuiceServletContextListener
		extends GuiceServletContextListener {

	public AdminAppGuiceServletContextListener() {
	}

	@Override
	protected Injector getInjector() {
		Module[] modules = ArrayUtils.addAll(WebApiApplication.getModules(), new AllProcessModule());
		return Guice.createInjector(modules);
	}
}