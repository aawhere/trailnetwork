/**
 *
 */
package com.aawhere.web.api.doc.archive;

import static com.aawhere.activity.ActivityWebApiUri.*;
import static com.aawhere.doc.DocumentWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;
import static com.aawhere.ws.rs.UriConstants.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aawhere.app.ApplicationDataIdImpl;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.web.api.ApiDocumentationSupport;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Documents are the original file received from devices or other applications. These documents
 * contain data that create tracks and other supporting data.
 * 
 * 
 * @author Aaron Roller
 * 
 */
@Singleton
@Path(SLASH + DOCUMENTS)
@Api(value = SLASH + DOCUMENTS, description = "Raw data for Tracks.",
		position = ApiDocumentationSupport.Position.DOCUMENTS)
public class DocumentWebApi {

	private DocumentService service;

	/**
	 * @param service
	 */
	@Inject
	public DocumentWebApi(DocumentService service) {
		super();
		this.service = service;
	}

	/**
	 * Retrieves the original document when provided the id. No transformation or customization will
	 * occur. The document type will be notified in the Response <i>Content-Type</i> Header.
	 * 
	 * 
	 * @param documentId
	 *            the identifier used by this API.
	 * @return the document in its original form. The MediaType is reported in the Content-Type
	 *         response header.
	 * @throws EntityNotFoundException
	 */
	@Path(SLASH + DOC_ID_PARAM)
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public ArchivedDocument docById(@PathParam(DOC_ID) ArchivedDocumentId documentId) throws EntityNotFoundException {
		return service.getArchivedDocument(documentId);
	}

	/**
	 * @deprecated this has been moved to {@link ActivityAdminWebApi} to follow standard rest
	 *             patterns.
	 * @param applicationDataId
	 * @param extension
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Path(SLASH + "activity/" + ACTIVITY_ID + FILE_EXTENSION_PARAM_SUFFIX)
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, NONE_MEDIA_TYPE })
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public ArchivedDocument getByApplication(@PathParam(APPLICATION_KEY) ApplicationDataIdImpl applicationDataId,
			@PathParam(FILE_EXTENSION) String extension) throws EntityNotFoundException {
		DocumentFormatKey format = DocumentFormatKey.byExtension(extension);
		return service.getArchivedDocument(applicationDataId, format);
	}

	/**
	 * Providing the document id this will return the document contents of the document in it's
	 * orignal form.
	 * 
	 * The content is returned in GZIP format. The suffix parameter is a convenience that will
	 * automatically name your file {id}.{ext} since many browsers and operating systems will assist
	 * with opening a gzip file while preserving the given file extension.
	 * 
	 * so 123456.csv will return a file 123456.csv.gz
	 * 
	 * @param documentId
	 * @param extension
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Path(SLASH + DOC_ID_PARAM + FILE_EXTENSION_PARAM_SUFFIX)
	@GET
	@Produces(GZIP_MIME_TYPE)
	@ApiOperation(value = "DOCME: Documentation Coming Soon...")
	public byte[] getDocumentContents(@PathParam(DOC_ID) ArchivedDocumentId documentId,
			@PathParam(FILE_EXTENSION) String extension) throws EntityNotFoundException {
		return service.getEncodedData(documentId);
	}
}
