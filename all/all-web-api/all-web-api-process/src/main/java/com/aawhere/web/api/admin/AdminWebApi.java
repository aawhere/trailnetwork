/**
 *
 */
package com.aawhere.web.api.admin;

import static com.aawhere.web.api.geo.GeoWebApiUri.*;
import static com.aawhere.ws.rs.SystemWebApiUri.*;

import java.net.URISyntaxException;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.QueueTimeoutException;
import com.aawhere.queue.QueueUtil;
import com.aawhere.queue.Task;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.security.SecurityRoles;
import com.aawhere.ws.rs.AdminWebApiUtil;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * Endpoints for administering the system. Not public.
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
@Path("admin")
public class AdminWebApi {

	private QueueFactory queueFactory;

	@Inject
	public AdminWebApi(QueueFactory queueFactory) {
		this.queueFactory = queueFactory;
	}

	@GET
	@Path("queue/empty")
	@Produces(MediaType.TEXT_PLAIN)
	public String waitForEmptyQueues(@QueryParam("timeoutMillis") @DefaultValue("10000") Integer timeoutInMillis,
			@QueryParam("throwOnFailure") @DefaultValue("false") Boolean throwOnFailure) throws QueueTimeoutException {
		String durationInMillis;
		try {
			durationInMillis = String.valueOf(QueueUtil.waitForEmptyQueues(queueFactory, timeoutInMillis));
		} catch (QueueTimeoutException e) {
			if (throwOnFailure) {
				throw (e);
			} else {
				// just report the timeout given.
				durationInMillis = String.valueOf(timeoutInMillis);
			}
		}
		return durationInMillis;
	}

	@GET
	@Path("security/whoami")
	@Produces({ MediaType.TEXT_PLAIN, NONE_MEDIA_TYPE })
	public String whoami(@Context SecurityContext sc) {
		String newLine = "\n";
		StringBuffer sb = new StringBuffer();
		if (sc.getUserPrincipal() != null) {
			sb.append("Name: ").append(sc.getUserPrincipal().getName());
		}
		sb.append(newLine);
		sb.append("Is Secure (SSL): ").append(sc.isSecure());
		sb.append(newLine);
		sb.append("Is admin? ").append(sc.isUserInRole(SecurityRoles.ADMIN.toLowerCase()));
		sb.append(newLine);
		sb.append("Auth Scheme: ").append(sc.getAuthenticationScheme());
		sb.append(newLine);
		sb.append("Is Trusted App? ").append(sc.isUserInRole(SecurityRoles.TRUSTED_APP));
		return sb.toString();
	}

	/**
	 * Posts any task to the queue. Give a uri, optional method and queue name and this will post
	 * your request to that queue for background processing. Puts are protected by default
	 * 
	 * @throws URISyntaxException
	 */
	@PUT
	@Path(QUEUE)
	@ApiOperation(
			value = QUEUE,
			notes = "Asynchronously posts any task to the queue of the name given.  Valueable when an operation is taking longer than then request limit (60 seconds). The response shows the tasks posted (if not too many to show).")
	public
			String
			queue(@DefaultValue(WebApiQueueUtil.DEFAULT_QUEUE_NAME) @QueryParam("name") @ApiParam(required = true,
					value = "The name of the appengine destination queue.") String name,
					@QueryParam("uri") @ApiParam(
							required = true,
							value = "The uri that starts from the web api root context with no leading slash. Template parameters, {templateName}, may be included for replacements like cells.") String encodedUri,
					@ApiParam(required = true,
							value = "The HTTP Method that should be called when executing from the queue") @DefaultValue(HttpMethod.PUT) @QueryParam("method") Task.Method method,
					@ApiParam(
							value = "The geocells representing an area you wish to explode.  The template "
									+ CELLS_PARAM
									+ " that must appear in the uri will be replaced with the exploded cells of higher resolution.",
							required = false, allowMultiple = true) @QueryParam(CELLS) GeoCells cells,
					@QueryParam(RESOLUTION) @ApiParam(
							required = false,
							value = "the GeoCell resolution that should be exploded to. If not given then the cells provided will be exploded one level higher than the cell.") Resolution expandTo)
					throws URISyntaxException {
		return AdminWebApiUtil.queue(name, encodedUri, method, cells, expandTo, queueFactory);

	}

	@PermitAll
	@GET
	@Path("security/permit-all")
	public String testPermitted() {
		return "Everyone is allowed here";
	}

	@RolesAllowed(SecurityRoles.ADMIN)
	@GET
	@Path("security/admin-role-only")
	public String testAdmin() {
		return "You are an admin. Only admins are allowed here";
	}

	@DenyAll
	@GET
	@Path("security/deny-access")
	public String testDeny() {
		return "No one is allowed here. You just broke the security of the system.";
	}
}
