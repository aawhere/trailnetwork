/**
 * 
 */
package com.aawhere.queue;

import static org.junit.Assert.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.UriBuilder;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.ws.rs.MediaTypeExtensionMapper;
import com.aawhere.ws.rs.SystemWebApiUri;

import com.google.common.net.MediaType;

/**
 * @author Brian Chapman
 * 
 */
public class WebApiEndPointUnitTest {

	public final String HOST = "http://example.com";
	public final String PATH = "/my/path";

	@Test
	public void testWebApiEndPointWithPath() throws URISyntaxException {
		URI pathUri = new URI(PATH);
		assertEndPoint(pathUri, SystemWebApiUri.ROOT_CONTEXT + PATH);
	}

	@Test
	@Ignore("don't support someone sending the host and path without the context...they should use this to build")
	public void testWebApiEndPointWithHost() throws URISyntaxException {
		URI hostUri = UriBuilder.fromUri(HOST).path(PATH).build();
		assertEndPoint(hostUri, HOST + SystemWebApiUri.ROOT_CONTEXT + PATH);
	}

	@Test
	public void testAbsolute() throws URISyntaxException {
		WebApiEndPoint endPoint = new WebApiEndPoint.Builder().setEndPoint(PATH).absolute().build();
		// can't find this constant anywhere
		final String searchItem = "http:";
		TestUtil.assertStartsWith(endPoint.toString(), searchItem);
	}

	@Test
	public void testEscapeCharacters() throws URISyntaxException {

		String q = "?";
		String amp = "&";
		WebApiEndPoint endPoint = new WebApiEndPoint.Builder().setEndPoint(PATH + q + "Something" + amp
				+ "somethingElse").build();
		TestUtil.assertContains(endPoint.toString(), amp);
		TestUtil.assertContains(endPoint.toString(), q);
	}

	@Test
	public void testMediaTypeExtension() {
		URI uri = UriBuilder.fromUri(PATH).build();
		final MediaType mediaType = MediaType.APPLICATION_XML_UTF_8;
		WebApiEndPoint endPoint = new WebApiEndPoint.Builder().setEndPoint(uri).setMediaType(mediaType).build();
		String expectedExtension = MediaTypeExtensionMapper.build().getExtensionFor(mediaType);
		String actual = endPoint.toString();
		assertNotNull(actual);
		assertNotNull(expectedExtension);
		TestUtil.assertEndsWith(actual, expectedExtension);
	}

	@Test
	public void testValues() throws URISyntaxException {
		Integer expected = 30;
		WebApiEndPoint endPoint = new WebApiEndPoint.Builder().setEndPoint("some/{thing}/between").values(expected)
				.build();

		TestUtil.assertContains(endPoint.toString(), expected.toString());

	}

	private void assertEndPoint(URI uri, String expected) {
		WebApiEndPoint endPoint = (new WebApiEndPoint.Builder()).setEndPoint(uri).build();
		assertEquals(expected, endPoint.toString());
	}
}
