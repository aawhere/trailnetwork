/**
 *
 */
package com.aawhere.ws.rs;

import static org.junit.Assert.*;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author Brian Chapman
 * 
 */
public class SystemWebApiUriUnitTest {

	@Test
	public void testWebApiUriConstants() {
		TestUtil.assertMavenFilteredProperty(SystemWebApiUri.ROOT_CONTEXT);
		TestUtil.assertMavenFilteredProperty(SystemWebApiUri.APP_VERSION);
		TestUtil.assertMavenFilteredProperty(SystemWebApiUri.APP_NAME);
		TestUtil.assertMavenFilteredProperty(SystemWebApiUri.HOST_URL);
		TestUtil.assertMavenFilteredProperty(SystemWebApiUri.API_BASE_PATH);
		TestUtil.assertMavenFilteredProperty(SystemWebApiUri.API_PUBLIC_BASE_PATH);
		TestUtil.assertContains(SystemWebApiUri.API_BASE_PATH, SystemWebApiUri.ROOT_CONTEXT);
		TestUtil.assertContains(SystemWebApiUri.API_BASE_PATH, SystemWebApiUri.HOST_URL);
		String info = SystemWebApiUri.info();
		TestUtil.assertContains(info, SystemWebApiUri.HOST_URL);
		TestUtil.assertContains(info, SystemWebApiUri.APP_NAME);
		TestUtil.assertContains(info, SystemWebApiUri.APP_VERSION);
		TestUtil.assertContains(info, SystemWebApiUri.BUILD_TIME);
		TestUtil.assertContains(info, SystemWebApiUri.BUILD_USER);
		TestUtil.assertContains(info, "git");
	}

	@Test
	public void testAbsolute() {
		String path = "junk";
		UriBuilder uriBuilder = SystemWebApiUri.endpoint(path);
		URI uri = uriBuilder.build();
		TestUtil.assertContains(uri.getPath(), path);
		TestUtil.assertStartsWith(uri.getPath(), SystemWebApiUri.ROOT_CONTEXT);

		assertTrue(uri.getPath(), uri.isAbsolute());
	}

}
