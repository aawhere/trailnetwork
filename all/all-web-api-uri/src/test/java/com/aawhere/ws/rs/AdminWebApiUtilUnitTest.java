/**
 * 
 */
package com.aawhere.ws.rs;

import static org.junit.Assert.*;

import java.net.URISyntaxException;

import org.junit.Test;

import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.QueueTestUtil;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.web.api.geo.GeoWebApiUri;

/**
 * @author aroller
 * 
 */
public class AdminWebApiUtilUnitTest {

	@Test
	public void testQueue() throws URISyntaxException {
		final QueueFactory queueFactory = QueueTestUtil.getQueueFactory();
		final String uri = "test/something";
		String results = AdminWebApiUtil.queue(null, uri, null, null, null, queueFactory);
		TestUtil.assertContains(results, uri);
		Queue queue = queueFactory.getQueue(WebApiQueueUtil.DEFAULT_QUEUE_NAME);
		QueueTestUtil.assertQueueContains(queue, 1, uri);
	}

	@Test
	public void testCellsQueue() throws URISyntaxException {
		final QueueFactory queueFactory = QueueTestUtil.getQueueFactory();
		final String uriBase = "test/map?cells=";
		final String uri = uriBase + GeoWebApiUri.CELLS_PARAM;
		GeoCell cell = GeoCellTestUtil.A;
		GeoCells cells = GeoCells.create().add(cell).build();
		String results = AdminWebApiUtil.queue(null, uri, null, cells, null, queueFactory);
		TestUtil.assertContains(results, uriBase);
		Queue queue = queueFactory.getQueue(WebApiQueueUtil.DEFAULT_QUEUE_NAME);
		QueueTestUtil.assertQueueContains(queue, GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL, uriBase + cell);
	}

	@Test
	public void testParamReplacement() {
		String given = "here=[there]";
		String actual = AdminWebApiUtil.decode(given);
		String expected = "here={there}";
		assertEquals(expected, actual);
	}

	@Test
	public void testAndReplacement() {
		String given = "me" + AdminWebApiUtil.AND_TOKEN + "you";
		String actual = AdminWebApiUtil.decode(given);
		String expected = "me&you";
		assertEquals(expected, actual);
	}
}
