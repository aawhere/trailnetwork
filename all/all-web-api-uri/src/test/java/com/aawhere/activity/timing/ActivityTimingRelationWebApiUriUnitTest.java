/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.persist.Filter;
import com.aawhere.queue.Task;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBStringReader;

/**
 * Test {@link ActivityTimingWebApiUri}
 * 
 * @author Brian Chapman
 * 
 */
public class ActivityTimingRelationWebApiUriUnitTest {

	@Test
	public void testTaskForUpdateStaleTimings() {
		int limit = 2;
		int page = 1;
		Filter filter = Filter.create().setLimit(limit).setPage(page).build();
		Task task = ActivityTimingWebApiUri.taskForUpdateStaleTimings(filter);
		TestUtil.assertContains(task.getStringPayload(), "page");
		TestUtil.assertContains(task.getStringPayload(), "<filter");
		JAXBStringReader<Filter> jaxbReader = JAXBStringReader.create(task.getStringPayload(), Filter.class).build();
		Filter receivedFilter = jaxbReader.getElement();
		assertEquals(filter.toString(), receivedFilter.toString());

	}
}
