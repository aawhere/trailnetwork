/**
 * 
 */
package com.aawhere.activity;

import java.net.URI;

import org.junit.Test;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.ws.rs.MediaTypeExtensionMapper;
import com.aawhere.ws.rs.UriConstants;

import com.google.common.net.MediaType;

/**
 * @author brian
 * 
 */
public class ActivityWebApiUriBuilderUnitTest {

	@Test
	public void testGetActivityByApplicationIdWithMediaType() {
		Identifier<?, ?> id = new IdentifierTestUtil.ChildId(TestUtil.generateRandomAlphaNumeric());
		MediaType kmlType = MediaType.KML;
		String extension = MediaTypeExtensionMapper.build().getExtensionFor(kmlType);
		URI uri = ActivityWebApiUri.getActivityByApplicationId(id, kmlType);
		TestUtil.assertContains(uri.toString(), UriConstants.SUFFIX_SEPARATOR + extension);
		TestUtil.assertContains(uri.toString(), id.getValue().toString());
	}

	@Test
	public void testGetActivityByApplicationIdWithInvalidMediaType() {
		Identifier<?, ?> id = new IdentifierTestUtil.ChildId(TestUtil.generateRandomAlphaNumeric());
		MediaType anyType = MediaType.ANY_APPLICATION_TYPE;
		String extension = MediaTypeExtensionMapper.build().getExtensionFor(anyType);
		URI uri = ActivityWebApiUri.getActivityByApplicationId(id, anyType);
		TestUtil.assertContains(uri.toString(), id.getValue().toString());
		TestUtil.assertDoesntContain(uri.toString(), UriConstants.SUFFIX_SEPARATOR + extension);
	}
}
