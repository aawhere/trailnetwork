/**
 * 
 */
package com.aawhere.activity;

/**
 * @author aroller TODO:move the Test fixtures for Web API Testing into this common place.
 * 
 */
public class ActivityIdFixture {
	/** Known to not exist at GC */
	public static final String GC_NOT_FOUND_ID = "1";
	/** The heart activity */
	public static final String GC_ACTIVITY_ID = "3006961";
	/** A general activity at 2peak */
	public static final String TWO_PEAK_ACTIVITY_ID = "199970336";
	public static final String TWO_PEAK_SECTION_ID = "98";

	public static final String GC_PRIVATE_ID = "321447900";
	public static final String EVERY_TRAIL = "2121681";

	/**
	 * <a href=
	 * "https://aawhere.jira.com/wiki/display/AAWHERE/Hill+Climb+Case+Study#HillClimbCaseStudy-TennesseeValleyTrailhead"
	 * >See Wiki</a>
	 * 
	 * @author aroller
	 * 
	 */
	public static final class TennesseeValley {
		public static final String MARINCELLO_CLIMB = "303046413";
		public static final String MIWOK_LOOP_SPIKE_FINISH = "287401319";
	}
}
