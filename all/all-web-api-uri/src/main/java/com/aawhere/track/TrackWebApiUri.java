/**
 * 
 */
package com.aawhere.track;

import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * @author aroller
 * 
 */
public class TrackWebApiUri
		extends SystemWebApiUri {

	public static final String TRACKS = "tracks";
}
