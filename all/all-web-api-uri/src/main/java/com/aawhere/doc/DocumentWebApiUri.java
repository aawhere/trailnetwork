/**
 * 
 */
package com.aawhere.doc;

import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * @author aroller
 * 
 */
public class DocumentWebApiUri
		extends SystemWebApiUri {

	public static final String DOCUMENTS = "documents";
	public static final String DOC_ID = "documentId";
	public static final String DOC_ID_PARAM = PARAM_BEGIN + DOC_ID + PARAM_END;
}
