/**
 * 
 */
package com.aawhere.web.api.geo;

import javax.ws.rs.core.MediaType;

import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.ws.rs.MediaTypes;
import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * Constants used for Building web API uri.
 * 
 * @author aroller
 * 
 */
public class GeoWebApiUri
		extends SystemWebApiUri {

	public static final String GEO = "geo";
	public static final String GEO_CELL = "geocell";
	/**
	 * A simple string representing a boundary in standard minLat,minLon|maxLat,maxLon or a list of
	 * geocells (comma or space).
	 * 
	 */
	// no dependency on measure, but see BoundingBox.FIELD#BOUNDS
	public static final String BOUNDS = "bounds";
	/** Used to report missing arguments. */
	public static final Message BOUNDS_MESSAGE = new StringMessage(BOUNDS);
	public static final String BOUNDS_PARAM = PARAM_BEGIN + BOUNDS + PARAM_END;
	public static final String BY_BOUNDS_PATH = BOUNDS + SLASH + BOUNDS_PARAM;
	public static final String CELLS = "cells";
	public static final String CELLS_PARAM = PARAM_BEGIN + CELLS + PARAM_END;
	public static final String CELLS_CELLS_PARAM_PATH = CELLS + SLASH + CELLS_PARAM;

	public static final String CELLS_BY_BOUNDS_PATH = CELLS + SLASH + BY_BOUNDS_PATH;
	public static final String BY_CELLS_PATH = CELLS + SLASH + CELLS_PARAM;
	public static final String BOUNDS_BY_CELLS_PATH = BOUNDS + SLASH + BY_CELLS_PATH;
	/**
	 * Indicates a bounding box is the object of interest. This differs from bounds which is a
	 * string-only object representing a bounds.
	 */
	public static final String BOXES = "boxes";
	public static final String BOXES_BY_BOUNDS_PATH = BY_BOUNDS_PATH + SLASH + BOXES;
	public static final String CELLS_CELL_PARAM_BOXES_PATH = CELLS_CELLS_PARAM_PATH + SLASH + BOXES;

	/** Google's MediaType doesn't provide a string constant to reference required for annotations */
	public static final String KML_CONTENT_TYPE = "application/vnd.google-earth.kml+xml";

	/** constants must be provided for use in annotations. */

	public static final String DISTANCES = "distances";
	public static final String GPOLYLINE = MediaTypes.GPOLYLINE;
	public static final MediaType POLYLINE_ENCODED = MediaTypes.POLYLINE_ENCODED;
	public static final String POLYLINE_ENCODED_MEDIA_TYPE = MediaTypes.POLYLINE_ENCODED_MEDIA_TYPE;
	public static final String KML = "kml";
	public static final String KML_PATH = KML + SLASH;
	/** The KML version of a bounding box. */
	public static final String BBOX = "BBOX";
	public static final String RESOLUTION = "resolution";
	public static final String BOUNDING_BOX = "boundingBox";
}
