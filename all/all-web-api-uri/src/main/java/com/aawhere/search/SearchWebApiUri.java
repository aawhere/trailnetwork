/**
 *
 */
package com.aawhere.search;

import static com.aawhere.ws.rs.UriConstants.PARAM_BEGIN;
import static com.aawhere.ws.rs.UriConstants.PARAM_END;
import static com.aawhere.ws.rs.UriConstants.SLASH;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.aawhere.id.Identifier;
import com.aawhere.persist.Filter;
import com.aawhere.queue.BatchQueue;
import com.aawhere.queue.EndPoint;
import com.aawhere.queue.Queue;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Method;
import com.aawhere.queue.WebApiEndPoint;

/**
 * Web URI constants for the Search subsystem.
 * 
 * @author Brian Chapman
 * 
 */
public class SearchWebApiUri {

	public static final String SEARCH_QUEUE_NAME = "search";
	public static final String INDEX = "index";
	public static final String SEARCH = "search";
	public static final String SEARCH_ENDPOINT = SEARCH + SLASH;
	public static final String INDEXES_PARAM = Filter.FIELD.SEARCH_INDEXES;
	public static final String INDEX_PARAM = PARAM_BEGIN + INDEX + PARAM_END;
	public static final String SEARCH_ID = "searchId";
	public static final String SEARCH_ID_PARAM = PARAM_BEGIN + SEARCH_ID + PARAM_END;

	/**
	 * Posts the given index to the queue to be indexed.
	 * 
	 * @param ids
	 * @param force
	 *            will respect the stale flag if true, false will update regardless
	 * @param queue
	 *            is the destination queue where the task will be posted.
	 * @return the number queued
	 */
	public static Integer indexTasks(Iterable<? extends Identifier<?, ?>> ids, String domainEndpoint, Queue queue) {
		BatchQueue batchQueue = BatchQueue.create().setQueue(queue).build();
		for (Identifier<?, ?> routeId : ids) {
			batchQueue.add(indexTask(routeId, domainEndpoint));
		}
		return batchQueue.finish();
	}

	/** Indexes the person when given the id. */
	public static Task indexTask(Identifier<?, ?> id, String domainEndpoint) {
		URI uri = UriBuilder.fromPath(SEARCH_ENDPOINT + domainEndpoint + SLASH + SEARCH_ID_PARAM).build(id);
		EndPoint endPoint = new WebApiEndPoint.Builder().setEndPoint(uri).build();
		return Task.create().setEndPoint(endPoint).setMethod(Method.PUT).build();
	}

}
