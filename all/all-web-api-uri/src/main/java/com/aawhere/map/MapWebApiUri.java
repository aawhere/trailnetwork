/**
 * 
 */
package com.aawhere.map;

import static com.aawhere.person.PersonWebApiUri.*;

import com.aawhere.route.RouteWebApiUri;

/**
 * 
 * @author aroller
 * 
 */
public class MapWebApiUri
		extends RouteWebApiUri {
	public static final String MAPS = "maps";
	public static final String LAYERS = "layers";
	public static final String FEATURES = "features";
	public static final String MAPS_FEATURES_PATH = MAPS + SLASH + FEATURES;
	public static final String MAP_FEATURE_ID = "mapFeatureId";
	public static final String MAP_LAYER_ID = "mapLayerId";
	public static final String MAP_LAYER_ID_PARAM = PARAM_BEGIN + MAP_LAYER_ID + PARAM_END;
	public static final String MAP_FEATURE_ID_PARAM = PARAM_BEGIN + MAP_FEATURE_ID + PARAM_END;
	public static final String PERSON_ID_MAPS_FEATURES_PATH = PERSON_ID_PARAM + SLASH + MAPS_FEATURES_PATH;

	public static final String MAPS_ROUTES_PATH = MAPS + SLASH + ROUTES;
	public static final String PERSON_ID_MAPS_ROUTES_PATH = PERSON_ID_PARAM + SLASH + MAPS_ROUTES_PATH;

}
