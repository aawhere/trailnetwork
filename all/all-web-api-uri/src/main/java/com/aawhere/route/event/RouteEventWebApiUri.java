/**
 * 
 */
package com.aawhere.route.event;

import static com.aawhere.person.PersonWebApiUri.*;
import static com.aawhere.person.group.SocialGroupWebApiUri.*;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.aawhere.id.StringIdentifier;
import com.aawhere.queue.Task;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.route.RouteWebApiUri;

/**
 * @author aroller
 * 
 */
public class RouteEventWebApiUri
		extends RouteWebApiUri {

	public static final String EVENTS = "events";
	/**
	 * When the route is provided in a different manner this represents only the event part of the
	 * {@link #ROUTE_EVENT_ID}.
	 */
	public static final String EVENT_ID = "eventId";
	public static final String EVENT_ID_PARAM = PARAM_BEGIN + EVENT_ID + PARAM_END;
	public static final String ROUTE_EVENT_ID = "routeEventId";
	public static final String ROUTE_EVENT_ID_PARAM = PARAM_BEGIN + ROUTE_EVENT_ID + PARAM_END;
	public static final String ROUTE_EVENT_ID_COMPLETIONS = ROUTE_EVENT_ID_PARAM + SLASH + COMPLETIONS;
	public static final String EVENTS_ROUTE_EVENT_ID_COMPLETIONS_SOCIAL_GROUPS_PATH = EVENTS + SLASH
			+ ROUTE_EVENT_ID_COMPLETIONS + SLASH + SOCIAL_GROUPS;
	public static final String ROUTE_EVENT_ID_EVENTS = ROUTE_EVENT_ID_PARAM + SLASH + EVENTS;
	public static final String ROUTE_ID_EVENTS_PATH = ROUTE_ID_PARAM + SLASH + EVENTS;
	public static final String ROUTE_ID_EVENTS_EVENT_ID_PATH = ROUTE_ID_EVENTS_PATH + SLASH + EVENT_ID_PARAM;
	public static final String ROUTE_ID_EVENTS_EVENT_ID_EVENTS_PATH = ROUTE_ID_EVENTS_EVENT_ID_PATH + SLASH + EVENTS;
	public static final String ROUTE_ID_EVENTS_COMPLETIONS_PATH = ROUTE_ID_EVENTS_PATH + SLASH + COMPLETIONS;
	public static final String ROUTE_ID_EVENTS_EVENT_ID_COMPLETIONS_PATH = ROUTE_ID_EVENTS_EVENT_ID_PATH + SLASH
			+ COMPLETIONS;
	public static final String ROUTES_EVENTS_PATH = ROUTES + SLASH + EVENTS;
	public static final String ROUTES_EVENTS_COMPLETIONS_PATH = ROUTES + SLASH + EVENTS + SLASH + COMPLETIONS;
	public static final String PERSON_ID_ROUTES_EVENTS_COMPLETIONS_PATH = PERSON_ID_PARAM + SLASH
			+ ROUTES_EVENTS_COMPLETIONS_PATH;
	public static final String ROUTE_ID_EVENTS_SOCIAL_GROUPS_PATH = ROUTE_ID_EVENTS_PATH + SLASH + SOCIAL_GROUPS;

	protected RouteEventWebApiUri() {
	}

	/**
	 * @param id
	 * @return
	 */
	public static Task socialGroupForCompletionsPutTask(StringIdentifier<?> routeEventId) {
		URI uri = UriBuilder.fromPath(ROUTES + SLASH + EVENTS_ROUTE_EVENT_ID_COMPLETIONS_SOCIAL_GROUPS_PATH)
				.build(routeEventId);
		return WebApiQueueUtil.putTask(uri);
	}

}
