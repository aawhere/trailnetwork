/**
 *
 */
package com.aawhere.route;

import static com.aawhere.activity.ActivityWebApiUri.*;
import static com.aawhere.activity.timing.ActivityTimingWebApiUri.*;
import static com.aawhere.person.PersonWebApiUri.*;
import static com.aawhere.person.group.SocialGroupWebApiUri.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.annotation.Nullable;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.queue.BatchQueue;
import com.aawhere.queue.EndPoint;
import com.aawhere.queue.Queue;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Method;
import com.aawhere.queue.WebApiEndPoint;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.trailhead.TrailheadWebApiUri;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.common.collect.Sets;
import com.google.common.net.MediaType;

/**
 * Used to declare and build all URIs related to the Web API for Routes.
 * 
 * This is best used as a global static import.
 * 
 * @author aroller
 * 
 */
public class RouteWebApiUri
		extends TrailheadWebApiUri {

	public static final String ROUTES = "routes";
	public static final String ALTERNATES = "alternates";

	public static final String ROUTE_ID = "routeId";
	public static final String ROUTE_IDS = "routeIds";
	public static final String ROUTE_ID_PARAM = PARAM_BEGIN + ROUTE_ID + PARAM_END;
	public static final String ROUTE_IDS_PARAM = PARAM_BEGIN + ROUTE_IDS + PARAM_END;
	public static final String TRAILHEAD_ID_ROUTES = TRAILHEAD_ID_PARAM + SLASH + ROUTES;
	public static final String ACTIVITY_ID_ROUTE_PATH = ACTIVITY_ID_PARAM + SLASH + ROUTES;
	public static final String ROUTE_ID_COURSE_PATH = ROUTE_ID_PARAM + SLASH + COURSES;
	public static final String ROUTES_ROUTE_ID_PATH = ROUTES + SLASH + ROUTE_ID_PARAM;
	public static final String ROUTES_ENDPOINT = ROUTES + SLASH;

	/**
	 * Route groups are mains with their alternates encompassed in Routes, but together in a
	 * collection
	 */
	public static final String TRAILHEAD_ID_ROUTES_ALTERNATES = TRAILHEAD_ID_ROUTES + SLASH + ALTERNATES;
	public static final String ROUTE_ID_ALTERNATES_PATH = ROUTE_ID_PARAM + SLASH + ALTERNATES;
	public static final String ROUTE_ID_VIEWS_PATH = ROUTE_ID_PARAM + SLASH + VIEWS;
	public static final String ROUTE_ID_PERSONS_GROUPS_PATH = ROUTE_ID_PARAM + SLASH + PERSONS + SLASH + GROUPS;
	public static final String ROUTE_ID_RELATED_PATH = ROUTE_ID_PARAM + SLASH + RELATED;

	public static final String ROUTE_ID_TRAILHEADS_PATH = ROUTE_ID_PARAM + SLASH + TRAILHEADS;
	public static final String ROUTE_ID_ACTIVITIES_TYPES_PATH = ROUTE_ID_PARAM + SLASH + ACTIVITIES + SLASH + TYPES;
	public static final String ROUTE_ID_NAMES_PATH = ROUTE_ID_PARAM + SLASH + NAMES;
	public static final String ROUTE_IDS_MERGED_PATH = ROUTE_IDS_PARAM + SLASH + MERGED;
	public static final String ROUTE_ID_GROUPS = ROUTE_ID_PARAM + SLASH + GROUPS;
	/** Provided to indicate the sub=group of routes that are identified as "main" routes. */
	public static final String MAINS = "mains";
	public static final String TRAILHEAD_ID_ROUTES_MAINS = TRAILHEAD_ID_ROUTES + SLASH + MAINS;
	public static final String ROUTES_MAINS_PATH = ROUTES + SLASH + MAINS;

	public static final String TRAILHEADS_TRAILHEAD_ID_ROUTES_MAINS = TRAILHEADS + SLASH + TRAILHEAD_ID_ROUTES + SLASH
			+ MAINS;

	public static final String ROUTE_QUEUE_NAME = "7-routes";
	/** path to provide the routes for a trailhead by ID */
	public static final String TRAILHEADS_TRAILHEAD_ID_ROUTES = TRAILHEADS + SLASH + TRAILHEAD_ID_ROUTES;

	public static final String COMPLETIONS = "completions";
	public static final String COMPLETIONS_COUNTED = COMPLETIONS + "Counted";
	public static final String ROUTE_COMPLETION_ID = "routeCompletionId";
	public static final String ROUTE_COMPLETION_ID_PARAM = PARAM_BEGIN + ROUTE_COMPLETION_ID + PARAM_END;

	/** The part of the RouteCompletionId without the route. */
	public static final String COMPLETION_ID = "completionId";

	public static final String COMPLETION_ID_PARAM = PARAM_BEGIN + COMPLETION_ID + PARAM_END;
	public static final String COMPLETIONS_ROUTE_COMPLETION_ID_PATH = COMPLETIONS + SLASH + ROUTE_COMPLETION_ID_PARAM;

	public static final String ROUTES_COMPLETIONS_PATH = ROUTES + SLASH + COMPLETIONS;

	public static final String ROUTE_ID_TIMINGS_PATH = ROUTE_ID_PARAM + SLASH + TIMINGS;
	public static final String ROUTE_ID_COMPLETIONS_PATH = ROUTE_ID_PARAM + SLASH + COMPLETIONS;
	public static final String ROUTE_ID_COMPLETIONS_COMPLETION_ID_PATH = ROUTE_ID_COMPLETIONS_PATH + SLASH
			+ COMPLETION_ID_PARAM;

	public static final String ROUTE_ID_COMPLETIONS_PERSONS_PATH = ROUTE_ID_COMPLETIONS_PATH + SLASH + PERSONS;
	public static final String COMPLETIONS_SOCIAL_GROUPS_PATH = COMPLETIONS + SLASH + SOCIAL_GROUPS;
	public static final String ROUTES_COMPLETIONS_SOCIAL_GROUPS_PATH = ROUTES + SLASH + COMPLETIONS_SOCIAL_GROUPS_PATH;
	public static final String ROUTE_ID_COMPLETIONS_SOCIAL_GROUPS_PATH = ROUTE_ID_COMPLETIONS_PATH + SLASH
			+ SOCIAL_GROUPS;
	public static final String ROUTE_ID_COMPLETIONS_PERSONS_GROUPS_MEMBERSHIPS_PATH = ROUTE_ID_COMPLETIONS_SOCIAL_GROUPS_PATH
			+ SLASH + MEMBERSHIPS;
	public static final String PERSON_ID_ROUTE_ID_COMPLETIONS_PATH = PERSON_ID_PARAM + SLASH + ROUTES_ROUTE_ID_PATH
			+ SLASH + COMPLETIONS;
	public static final String PERSON_ID_ROUTES_COMPLETIONS_PATH = PERSON_ID_PARAM + SLASH + ROUTES_COMPLETIONS_PATH;

	public static final String TIMINGS_ID_ROUTES_COMPLETIONS_PATH = TIMINGS_ID_PARAM + SLASH + ROUTES_COMPLETIONS_PATH;
	public static final String ACTIVITY_ID_ROUTE_COMPLETIONS_PATH = ACTIVITY_ID_ROUTE_PATH + SLASH + COMPLETIONS;
	public static final String ACTIVITY_ID_ROUTES_TIMINGS_PATH = ACTIVITY_ID_ROUTE_PATH + SLASH + TIMINGS;

	/**
	 * All static methods.
	 * 
	 */
	protected RouteWebApiUri() {

	}

	public static URI getRoute(Identifier<?, ?> routeId) {
		return UriBuilder.fromPath(SLASH + ROUTES + SLASH + ROUTE_ID_PARAM).build(routeId.getValue());
	}

	public static URI getRoutes() {
		return UriBuilder.fromPath(SLASH + ROUTES).build();
	}

	public static Task routeGroupsUpdateTask(Identifier<Long, ?> routeId) {
		URI uri = UriBuilder.fromPath(ROUTES + SLASH + ROUTE_ID_GROUPS).build(routeId);
		EndPoint endPoint = new WebApiEndPoint.Builder().setEndPoint(uri).build();
		return Task.create().setEndPoint(endPoint).setMethod(Method.PUT).build();
	}

	public static Task routeUpdateTask(Identifier<Long, ?> routeId) {
		return routeUpdateTask(routeId, null);
	}

	/** Updates the route when given the id. */
	public static Task routeUpdateTask(Identifier<Long, ?> routeId, @Nullable Boolean force) {
		URI uri = UriBuilder.fromPath(ROUTES + SLASH + ROUTE_ID_PARAM).queryParam(FORCE, BooleanUtils.isTrue(force))
				.build(routeId);
		EndPoint endPoint = new WebApiEndPoint.Builder().setEndPoint(uri).build();
		return Task.create().setEndPoint(endPoint).setMethod(Method.PUT).build();
	}

	/**
	 * Posts the given routes to the queue to be updated.
	 * 
	 * @param ids
	 * @param force
	 *            will respect the stale flag if true, false will update regardless
	 * @param queue
	 *            is the destination queue where the task will be posted.
	 * @return the number queued
	 */
	public static Integer routeUpdateTasks(Iterable<? extends Identifier<Long, ?>> ids, Boolean force, Queue queue) {
		BatchQueue batchQueue = BatchQueue.create().setQueue(queue).build();
		for (Identifier<Long, ?> route : ids) {
			batchQueue.add(routeUpdateTask(route, force));
		}
		return batchQueue.finish();
	}

	public static Task routeMergeTask(Iterable<? extends Identifier<Long, ?>> routeIds) {
		// listing by ordered key ensures the unique task will be enforced for a set of keys
		List<? extends Identifier<Long, ?>> ordered = IdentifierUtils.idsOrdered(Sets.newHashSet(routeIds));
		URI uri = UriBuilder.fromPath(ROUTES + SLASH + ROUTE_IDS_MERGED_PATH).build(SystemWebApiUri.listParam(ordered));
		return WebApiQueueUtil.putTask(uri);
	}

	public static Task routeCompletionForTimingTask(Identifier<String, ?> timingId, long millisDelay) {
		URI uri = UriBuilder.fromPath(TIMINGS_BASE_PATH + TIMINGS_ID_ROUTES_COMPLETIONS_PATH).build(timingId);
		return WebApiQueueUtil.putTaskBuilder(uri).setCountdownMillis(millisDelay).build();
	}

	public static Task routeCompletionForCourseTask(Identifier<String, ?> courseId) {
		URI uri = UriBuilder.fromPath(ACTIVITY_BASE_PATH + ACTIVITY_ID_ROUTE_COMPLETIONS_PATH).build(courseId);
		return WebApiQueueUtil.putTask(uri);
	}

	public static Task routeCompletionForTimingTask(Identifier<String, ?> timingId) {
		return routeCompletionForTimingTask(timingId, 0);
	}

	/** Posts a request for routes to find good course candidates based on the completions. */
	public static Task routesTimingsForAttemptTask(Identifier<String, ?> activityId) {
		URI uri = UriBuilder.fromPath(ACTIVITY_BASE_PATH + ACTIVITY_ID_ROUTES_TIMINGS_PATH).build(activityId);
		return WebApiQueueUtil.putTask(uri);
	}

	public static Task routesTimingsAttemptDiscoveryTask(Identifier<Long, ?> routeId) {
		URI uri = UriBuilder.fromPath(ROUTES + SLASH + ROUTE_ID_TIMINGS_PATH).build(routeId);
		return WebApiQueueUtil.putTask(uri);
	}

	public static URI routesForTrailheadEndpoint(Identifier<String, ?> trailheadId, MediaType mediaType) {
		try {
			return new WebApiEndPoint.Builder().setEndPoint(TRAILHEADS_TRAILHEAD_ID_ROUTES).absolute()
					.setMediaType(mediaType).values(trailheadId).build().getUri();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Updates a specific route's completions.
	 * 
	 * @param routeId
	 * @return
	 */
	public static Task routeCompletionUpdatedTask(Identifier<Long, ?> routeId) {
		URI uri = UriBuilder.fromPath(ROUTES_ENDPOINT + ROUTE_ID_COMPLETIONS_PATH).build(routeId);
		return WebApiQueueUtil.putTask(uri);
	}

	/**
	 * Posts a task for the social group to be updated to represent the completions for a route.
	 * 
	 * @param routeId
	 * @return
	 */
	public static Task routeCompletionSocialGroupUpdatedTask(Identifier<Long, ?> routeId) {
		URI uri = UriBuilder.fromPath(ROUTES_ENDPOINT + ROUTE_ID_COMPLETIONS_SOCIAL_GROUPS_PATH).build(routeId);
		return WebApiQueueUtil.putTask(uri);
	}

	/**
	 * Posts a task for the social group to be updated to represent the completions for a route.
	 * 
	 * @param routeId
	 * @return
	 */
	public static Task routeRelatedUpdatedTask(Identifier<Long, ?> routeId) {
		URI uri = UriBuilder.fromPath(ROUTES_ENDPOINT + ROUTE_ID_RELATED_PATH).build(routeId);
		return WebApiQueueUtil.putTask(uri);
	}

}
