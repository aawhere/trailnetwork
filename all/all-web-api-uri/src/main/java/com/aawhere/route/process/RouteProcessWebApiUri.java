/**
 * 
 */
package com.aawhere.route.process;

import static com.aawhere.web.api.geo.GeoWebApiUri.GEO_CELL;

import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * Constants for building web api uris.
 * 
 * @author Brian Chapman
 * 
 */
public class RouteProcessWebApiUri
		extends SystemWebApiUri {

	public static final String APP_CONNECT = "app/gc";
	public static final String IMPORT = "import";
	public static final String CRAWL = "crawl";

	public static final String GEO_CELL_PARAM = PARAM_BEGIN + GEO_CELL + PARAM_END;

	public static final String APP_CONNECT_FILTER_PATH = APP_CONNECT + SLASH + FILTER;
	public static final String APP_CONNECT_CRAWL_PATH = APP_CONNECT + SLASH + CRAWL;
	public static final String APP_CONNECT_CRAWL_GEO_CELL = APP_CONNECT + SLASH + GEO_CELL;
	public static final String APP_CONNECT_CRAWL_GEO_CELL_PATH = APP_CONNECT_CRAWL_GEO_CELL + SLASH + GEO_CELL_PARAM;

}
