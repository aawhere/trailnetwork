/**
 * 
 */
package com.aawhere.activity.timing;

import com.aawhere.id.Identifier;
import com.aawhere.queue.Task;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Used with {@link Iterables} this will transform the given Timing Id into a Task for queuing.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingReleationCandidateTaskFunction
		implements Function<Identifier<String, ?>, Task> {

	public static ActivityTimingReleationCandidateTaskFunction build() {
		return new ActivityTimingReleationCandidateTaskFunction();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Task apply(Identifier<String, ?> input) {
		return ActivityTimingWebApiUri.taskForCourseAttempt(input).build();
	}

}
