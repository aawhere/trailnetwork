/**
 *
 */
package com.aawhere.activity;

import static com.aawhere.app.account.AccountWebApiUri.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.app.ApplicationWebApiUri;
import com.aawhere.app.account.AccountWebApiUri;
import com.aawhere.id.Identifier;
import com.aawhere.persist.Filter;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Method;
import com.aawhere.queue.WebApiEndPoint;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.route.process.RouteProcessWebApiUri;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.common.net.MediaType;

/**
 * Used to assist WebApi classes and other Services to find API endpoints for Activity methods.
 * 
 * I sure wish we could use the {@link UriBuilder} to build the constants, but then they don't work
 * from Annotations which need Constant Expressions since they are dealt with during compile time,
 * not runtime.
 * 
 * Referencing constants makes it difficult to read, hover over the Constant in eclipse and you will
 * see the value.
 * 
 * TODO: Split this up into other more appropriate WebApiUri classes.
 * 
 * @see #main(String[])
 * 
 * @author aroller
 * 
 */
public class ActivityWebApiUri
		extends GeoWebApiUri {

	public static final String APPLICATION = ApplicationWebApiUri.APPLICATION;
	public static final String ACTIVITIES = "activities";
	/** Useful when paired with {@link #ACTIVITIES} to represent Activity Type */
	public static final String TYPES = "types";
	public static final String RELATIONS = "relations";
	public static final String ACTIVITY_ID = "activityId";
	public static final String FIRST = "first";
	public static final String SECOND = "second";

	/**
	 * @see #IMPORTS
	 */
	public static final String IMPORT = "import";
	/** Represents ActivityImportMilestones. */
	public static final String IMPORTS = "imports";
	public static final String HISTORY = "history";
	public static final String SOURCE_ACTIVITY_ID_PARAM = PARAM_BEGIN + FIRST + PARAM_END;
	public static final String TARGET_ACTIVITY_ID_PARAM = PARAM_BEGIN + SECOND + PARAM_END;
	public static final String FIRST_SECOND_PATH = SOURCE_ACTIVITY_ID_PARAM + SLASH + TARGET_ACTIVITY_ID_PARAM;

	public static final String ACTIVITY_ID_PARAM = PARAM_BEGIN + ACTIVITY_ID + PARAM_END;
	public static final String APPLICATION_KEY = "applicationKey";
	public static final String APPLICATION_KEY_PARAM = PARAM_BEGIN + APPLICATION_KEY + PARAM_END;
	public static final String ACTIVITY_IMPORT_QUEUE_NAME = "2-activityImport";
	public static final Method ACTIVITY_IMPORT_METHOD_FOR_BLOCKING = Method.POST;
	public static final Method ACTIVITY_IMPORT_METHOD_FOR_UPDATE = Method.PUT;
	public static final Method ACTIVITY_IMPORT_METHOD_FOR_BYPASS = Method.GET;

	public static final String APPLICATION_GPX_XML = "application/gpx+xml";
	public static final String GPX_EXTENSION = "gpx";
	/**
	 * Intnded to be used with other paths when referencing the url directly so that this + the
	 * other paths would act as JAX-RS does.
	 */
	public static final String ACTIVITY_BASE_PATH = ACTIVITIES + SLASH;
	/** Intended to go with the Activity base path */
	public static final String ADMIN_PROCESS_PATH = ADMIN + SLASH + PROCESS;

	public static final String ACTIVITY_ID_IMPORTS_PATH = ACTIVITY_ID_PARAM + SLASH + IMPORTS;

	private static UriBuilder GET_ACTIVITY_BY_ID_BUILDER = UriBuilder.fromPath(SLASH + ACTIVITIES)
			.path(ACTIVITY_ID_PARAM);

	public static final String ACTIVITIES_TYPES_PATH = ACTIVITIES + SLASH + TYPES;

	public static final String ACTIVITY_TYPE = "activityType";
	public static final String ACTIVITY_TYPE_PARAM = PARAM_BEGIN + ACTIVITY_TYPE + PARAM_END;
	public static final String APPLICATION_CELLS_PATH = APPLICATION_KEY_PARAM + SLASH + CELLS_CELLS_PARAM_PATH;
	/** used to retrieve or put activities for a specific account. */
	public static final String ACCOUNT_ID_ACTIVITIES_PATH = AccountWebApiUri.ACCOUNT_ID_PARAM + SLASH + ACTIVITIES;

	public static final String APP_PROCESS_APP_CONNECT_FILTER_PATH = ADMIN_PROCESS_PATH + SLASH
			+ RouteProcessWebApiUri.APP_CONNECT_FILTER_PATH;
	public static final String ACTIVITY_IDS = ACTIVITY_ID + "s";

	public static URI getActivityByApplicationId(Identifier<String, ?> id) {
		return getActivityByApplicationId(id, MediaType.ANY_APPLICATION_TYPE);
	}

	public static URI getActivityByApplicationId(Identifier<?, ?> id, MediaType mediaType) {
		// FIXME:Use WebApiEndPoint to build this
		String extension = MEDIA_TYPE_MAPPER.getExtensionFor(mediaType);
		if (extension != null) {
			extension = SUFFIX_SEPARATOR + extension;
		} else {
			extension = "";
		}
		return UriBuilder.fromUri(GET_ACTIVITY_BY_ID_BUILDER.build(id.getValue()).toString() + extension).build();
	}

	/**
	 * Given the required parameters this will create the endpoint that will call for crawling by
	 * cell for a given application and the page requested.
	 * 
	 * @param applicationKey
	 * @param geoCells
	 * @param page
	 * @return
	 */
	public static Task applicationCellsEndpoint(@Nonnull String applicationKey, @Nonnull String geoCells,
			@Nullable Integer page, @Nullable Boolean deep) {

		final UriBuilder uriBuilder = UriBuilder.fromPath(ACTIVITIES + SLASH + APPLICATION_CELLS_PATH);
		if (BooleanUtils.isTrue(deep)) {
			uriBuilder.queryParam(DEEP, deep);
		}
		if (page != null) {
			uriBuilder.queryParam(FILTER_PAGE, page);
		}
		URI uri = uriBuilder.build(applicationKey, geoCells);
		return WebApiQueueUtil.putTask(uri);

	}

	/**
	 * @see #accountEndpoint(Identifier, Integer)
	 * @param accountId
	 */
	public static Task accountEndpoint(Identifier<String, ?> accountId) {
		return accountEndpoint(accountId, null);
	}

	public static Task accountEndpoint(Identifier<String, ?> accountId, Integer page) {
		return accountEndpoint(accountId, page, null);
	}

	/** Creates a task to import activities for the given accountId. */
	public static Task accountEndpoint(Identifier<String, ?> accountId, Integer page, @Nullable Boolean deep) {
		UriBuilder uriBuilder = UriBuilder.fromPath(ACCOUNTS + SLASH + ACCOUNT_ID_ACTIVITIES_PATH);
		if (page != null) {
			uriBuilder.queryParam(FILTER_PAGE, page);
		}
		if (BooleanUtils.isTrue(deep)) {
			uriBuilder.queryParam(Filter.FIELD.OPTIONS, SystemWebApiUri.DEEP);
		}
		return WebApiQueueUtil.putTask(uriBuilder.build(accountId));

	}

	/**
	 * Given the activity id and one of the methods related to how the activity is to be imported,
	 * this will provide the appropriate task.
	 * 
	 * @see #ACTIVITY_IMPORT_METHOD_FOR_BLOCKING
	 * @see #ACTIVITY_IMPORT_METHOD_FOR_BYPASS
	 * @see #ACTIVITY_IMPORT_METHOD_FOR_UPDATE
	 * 
	 * @param activityId
	 * @param method
	 * @return
	 */
	public static Task activityImportTask(Identifier<String, ?> activityId, Method method) {
		String path = ActivityWebApiUri.getActivityByApplicationId(activityId).toString();
		WebApiEndPoint endPoint;
		try {
			endPoint = (new WebApiEndPoint.Builder()).setEndPoint(path).build();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
		return Task.create().setEndPoint(endPoint).setMethod(method).build();
	}

	/**
	 * Simple task to update an activity that's already been imported or perhaps already failed
	 * importing.
	 * 
	 * 
	 * 
	 * @param activityId
	 * @return
	 */
	public static Task activityUpdated(Identifier<String, ?> activityId) {
		URI uri = UriBuilder.fromPath(ACTIVITIES + SLASH + ACTIVITY_ID_PARAM).build(activityId);
		return WebApiQueueUtil.putTask(uri);
	}

	public static final class Public {
		public static final String ACTIVITY_TYPES = API_PUBLIC_BASE_PATH + SLASH
				+ ActivityWebApiUri.ACTIVITIES_TYPES_PATH;
	}

}
