/**
 * 
 */
package com.aawhere.activity.relation;

import java.net.URI;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.UriBuilder;

import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.id.Identifier;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;

/**
 * Web Endpoints for Activity Relations and such.
 * 
 * @author aroller
 * 
 */
@Deprecated
public class ActivityRelationWebApiUri
		extends ActivityWebApiUri {

	public static final String RELATIONS_ACTIVITY_PATH = ACTIVITY_ID_PARAM + SLASH + RELATIONS;

	public static final String ACTIVITY_RELATIONS_END_POINT = ACTIVITY_BASE_PATH + RELATIONS_ACTIVITY_PATH;
	public static final String RELATIONS_ACTIVITY_PAIR_PATH = SOURCE_ACTIVITY_ID_PARAM + SLASH
			+ TARGET_ACTIVITY_ID_PARAM + SLASH + RELATIONS;

	public static final String RELATIONS_PAIR_BY_APP_PATH = APPLICATION + SLASH + SOURCE_ACTIVITY_ID_PARAM + SLASH
			+ TARGET_ACTIVITY_ID_PARAM + SLASH + RELATIONS;
	public static final String RELATIONS_STALE_COUNT = RELATIONS + SLASH + STALE + SLASH + COUNT;
	/**
	 *
	 */
	public static final String ACTIVITY_RELATIONS_PAIR_END_POINT = ACTIVITY_BASE_PATH + RELATIONS_ACTIVITY_PAIR_PATH;
	/** General access to all timings */

	/**
	 * The queue where the activities are placed to find all possible relations.
	 * 
	 */
	/* package */static final String RELATIONS_QUEUE_NAME = "relations";
	/**
	 * The queue where bi-directional relatioships are placed for two related activities.
	 * 
	 */
	/* package */static final String RELATION_ENTRIES_QUEUE_NAME = "relationEntries";
	/* package */static final String LINE_STRING_QUEUE_NAME = "lineString";
	/* package */static final String BUFFERED_LINE_STRING_QUEUE_NAME = "bufferedLineString";

	/**
	 * Asynchronously notifies the system to process relationships for an Activity's Track
	 * identified by the given {@link ActivityId}.
	 * 
	 * 
	 * 
	 * @see #createRelations(ActivityId) which will be called upon queue consumption
	 * @see ActivityWebApiUri#queueToCreateRelationships(ActivityId) for the endpoint called
	 * 
	 * @param activityId
	 * @throws EntityNotFoundException
	 *             when there is no {@link Activity} matching the given {@link ActivityId}
	 */
	static public void queueToCreateRelationships(Identifier<?, ?> activityId, QueueFactory queueFactory) {

		String uri = getCreateRelationsQueueEndpoint(activityId).toString();
		queue(uri, RELATIONS_QUEUE_NAME, queueFactory);

	}

	static public Queue getRelationsQueue(QueueFactory queueFactory) {
		Queue queue = queueFactory.getQueue(RELATIONS_QUEUE_NAME);
		return queue;
	}

	/**
	 * Asynchronously notifies the system to process the bi-directional relationship between the
	 * source and target.
	 * 
	 * @param sourceId
	 * @param targetId
	 */
	static public <ActivityIdT extends Identifier<?, ?>> void queueToCreateRelationships(ActivityIdT sourceId,
			ActivityIdT targetId, QueueFactory queueFactory) {
		String uri = getCreateRelationsQueueEndpoint(sourceId, targetId).toString();
		queue(uri, RELATION_ENTRIES_QUEUE_NAME, queueFactory);
	}

	public static URI getCreateRelationsQueueEndpoint(Identifier<?, ?> activityId) {
		return UriBuilder.fromPath(ACTIVITY_RELATIONS_END_POINT).build(activityId.getValue());
	}

	public static <ActivityIdT extends Identifier<?, ?>> URI getCreateRelationsQueueEndpoint(ActivityIdT sourceId,
			ActivityIdT targetId) {
		return UriBuilder.fromPath(ACTIVITY_RELATIONS_PAIR_END_POINT).build(sourceId.getValue(), targetId.getValue());
	}

}
