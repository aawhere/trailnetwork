/**
 * 
 */
package com.aawhere.activity.timing;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import javax.annotation.Nonnull;
import javax.ws.rs.core.UriBuilder;

import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.id.Identifier;
import com.aawhere.persist.Filter;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Builder;
import com.aawhere.queue.Task.Method;
import com.aawhere.queue.WebApiEndPoint;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.util.rb.KeyedMessageFormat;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.xml.bind.JAXBStringWriter;
import com.google.common.base.Charsets;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;

/**
 * Simple string constants used for identifying resources on the web both for endpoints and data
 * names.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingWebApiUri
		extends ActivityWebApiUri {

	/** @deprecated use #TIMINGS_ID */
	@Deprecated
	public static final String TIMINGS_RELATION_ID = "activityTimingId";
	@Deprecated
	public static final String TIMINGS_RELATION_ID_PARAM = PARAM_BEGIN + TIMINGS_RELATION_ID + PARAM_END;
	/**
	 * Timings here has activity removed since it is intended to be used in conjunction with
	 * activities/timings
	 */
	public static final String TIMINGS = "timings";
	public static final String TIMINGS_DICTIONARY_PATH = TIMINGS + SLASH + DICTIONARY;
	public static final String ACTIVITIES_TIMINGS_PATH = ACTIVITIES + SLASH + TIMINGS;
	public static final String TIMINGS_ID = TIMINGS_RELATION_ID;
	public static final String TIMINGS_ID_PARAM = PARAM_BEGIN + TIMINGS_ID + PARAM_END;
	public static final String TIMINGS_ID_PATH = TIMINGS + SLASH + TIMINGS_ID_PARAM;

	public static final String TIMINGS_BASE_PATH = ACTIVITY_BASE_PATH + TIMINGS + SLASH;
	public static final String TIMINGS_ID_END_POINT = TIMINGS_BASE_PATH + TIMINGS_ID_PARAM;

	public static final String COURSE = "course";
	public static final String COURSES = "courses";
	public static final String ATTEMPT = "attempt";
	public static final String COURSE_PARAM = PARAM_BEGIN + COURSE + PARAM_END;
	public static final String ATTEMPT_PARAM = PARAM_BEGIN + ATTEMPT + PARAM_END;
	public static final String SCREEN = "screen";
	public static final String FORCE_STALE = "forceStale";

	/**
	 * Used to retrieve reciprocals from ActivityLeaderboard
	 * 
	 */
	public static final String RECIPROCALS = "reciprocals";
	public static final String RECIPROCAL_ID = "reciprocalId";
	public static final String RECIPROCAL_ID_PARAM = PARAM_BEGIN + RECIPROCAL_ID + PARAM_END;

	/** Notifyies the parties interested in the course that it has another qualifying attempt. */
	public static final String TIMINGS_COURSE_ALERT_PATH = TIMINGS_RELATION_ID_PARAM + SLASH + COURSE + SLASH + ALERT;
	/** Notifies the parties interested in the attempt that it has participated in a course. */
	public static final String TIMINGS_ATTEMPT_ALERT_PATH = TIMINGS_RELATION_ID_PARAM + SLASH + ATTEMPT + SLASH + ALERT;
	/** The endpoint added to the queue for {@link #TIMINGS_COURSE_ALERT_PATH} */
	public static final String TIMINGS_COURSE_ALERT_END_POINT = TIMINGS_BASE_PATH + TIMINGS_COURSE_ALERT_PATH;
	/** The endpoint added to the queue for {@link #TIMINGS_ATTEMPT_ALERT_PATH} */
	public static final String TIMINGS_ATTEMPT_ALERT_END_POINT = TIMINGS_BASE_PATH + TIMINGS_ATTEMPT_ALERT_PATH;

	public static final String TIMINGS_PATH = TIMINGS;
	/** Provides stored timings related to an activity regardless of course or attempt. */
	public static final String TIMINGS_ACTIVITY_PATH = ACTIVITY_ID_PARAM + SLASH + TIMINGS;
	/** provides stored timings related to an activity if it is the course. */
	public static final String TIMINGS_COURSE_PATH = ACTIVITY_ID_PARAM + SLASH + COURSE + SLASH + TIMINGS;
	/** provides stored timings related to an activity if it is the attempt. */
	public static final String TIMINGS_ATTEMPT_PATH = ACTIVITY_ID_PARAM + SLASH + ATTEMPT + SLASH + TIMINGS;

	public static final String TIMINGS_FOR_ACTIVITIES_IN_CELLS_PATH = CELLS_CELLS_PARAM_PATH + SLASH + TIMINGS;
	public static final String TIMINGS_FOR_ACTIVITIES_IN_CELLS_ENDPOINT = ACTIVITIES + SLASH + CELLS_CELLS_PARAM_PATH
			+ SLASH + TIMINGS;

	/**
	 * Processes the timings, stores and retrieves the results between a given course and attempt.
	 */
	public static final String TIMINGS_COURSE_ATTEMPT_PATH = COURSE_PARAM + SLASH + ATTEMPT_PARAM + SLASH + TIMINGS;
	/** Added to the queue to process a timings match */
	public static final String TIMINGS_COURSE_ATTEMPT_END_POINT = ACTIVITY_BASE_PATH + TIMINGS_COURSE_ATTEMPT_PATH;
	public static final String TIMINGS_END_POINT = ACTIVITY_BASE_PATH + TIMINGS_ACTIVITY_PATH;

	@Deprecated
	public static final String UPDATE_STALE_TIMINGS = SystemWebApiUri.UPDATE;
	public static final String UPDATE_STALE_TIMINGS_END_POINT = TIMINGS_BASE_PATH + UPDATE_STALE_TIMINGS;

	public static final String TIMINGS_QUEUE_NAME = "3-timings";

	/**
	 * The queue where course/attempt likely matches are placed for processing.
	 * 
	 * @see #TIMINGS_COURSE_ATTEMPT_PATH
	 * 
	 */
	public static final String TIMINGS_COURSE_ATTEMPT_SCREENING_QUEUE_NAME = "4-timingsWeak";

	/**
	 * The queue where course/attempt likely matches are placed for processing.
	 * 
	 * @see #TIMINGS_COURSE_ATTEMPT_PATH
	 * 
	 */
	public static final String TIMINGS_COURSE_ATTEMPT_QUEUE_NAME = "5-timingsStrong";
	/**
	 * Called when course/attempt matches.
	 * 
	 */
	public static final String TIMINGS_COURSE_COMPLETED_QUEUE_NAME = "6-timingsCompleted";

	/**
	 * When a likely match is found since a relation has a high percentage match for the course
	 * indicating the attempt likely traveled the course exactly.
	 * 
	 * @deprecated use bulk add procedures. call
	 *             {@link #taskForCourseAttempt(Identifier, Identifier)} instead
	 * @param courseActivityId
	 * @param attemptActivityId
	 */
	@Deprecated
	static public <ActivityIdT extends Identifier<?, ?>> void queueToCreateTimings(ActivityIdT courseActivityId,
			ActivityIdT attemptActivityId, QueueFactory queueFactory) {
		URI uri = UriBuilder.fromPath(TIMINGS_COURSE_ATTEMPT_END_POINT).build(	courseActivityId.getValue(),
																				attemptActivityId.getValue());
		queue(uri.toString(), TIMINGS_COURSE_ATTEMPT_QUEUE_NAME, queueFactory);

	}

	static Builder taskForCourseAttempt(Identifier<?, ?> timingId) {
		return taskForCourseAttempt(timingId, Boolean.TRUE);
	}

	/**
	 * TODO : getting to the point where a builder would be convenient.
	 * 
	 * @param timingId
	 * @param screen
	 * @param forceStale
	 * @param allowRepeats
	 * @return
	 */
	static Builder taskForCourseAttempt(Identifier<?, ?> timingId, Boolean allowRepeats) {
		URI uri = UriBuilder.fromPath(TIMINGS_ID_END_POINT).build(timingId.getValue());

		final Builder builder = Task.create();
		// builder.addParam(new StringParameter(SCREEN, screen));
		return builder.setEndPoint(new WebApiEndPoint.Builder().setEndPoint(uri).build()).setMethod(Method.GET)
				.setAllowRepeats(allowRepeats);
	}

	static Builder taskForCourseUpdate(Identifier<?, ?> timingId) {
		URI uri = UriBuilder.fromPath(TIMINGS_ID_END_POINT).build(timingId.getValue());

		final Builder builder = Task.create();
		return builder.setEndPoint(new WebApiEndPoint.Builder().setEndPoint(uri).build()).setMethod(Method.PUT);
	}

	/**
	 * Queue a task to process stale timings.
	 * 
	 * @param filter
	 * @return
	 */
	public static Task taskForUpdateStaleTimings(Filter filter) {
		URI uri = UriBuilder.fromPath(UPDATE_STALE_TIMINGS_END_POINT).build();

		final Builder builder = Task.create();
		// TODO: move into the setPayload method itself.
		JAXBStringWriter writer = JAXBStringWriter.create(filter).build();
		builder.setPayload(writer.getXml(), MediaType.APPLICATION_XML_UTF_8);
		return builder.setEndPoint(new WebApiEndPoint.Builder().setEndPoint(uri).build())
				.addHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML_UTF_8.toString())
				.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_UTF_8.toString())
				.addHeader(HttpHeaders.ACCEPT_ENCODING, Charsets.UTF_8.toString()).setMethod(Method.PUT)
				.setAllowRepeats(true).build();
	}

	/**
	 * Instigates the processing of Timings by finding those likely matches and sending them into
	 * the {@link #queueToCreateTimings(Identifier, Identifier, QueueFactory)}
	 * 
	 * @deprecated use #timingAttemptDiscoveryTask
	 * @param courseActivityId
	 */
	@Deprecated
	static public <ActivityIdT extends Identifier<?, ?>> void queueToCreateTimings(ActivityIdT courseActivityId,
			QueueFactory queueFactory) {

		URI uri = UriBuilder.fromPath(TIMINGS_END_POINT).build(courseActivityId.getValue());

		WebApiQueueUtil.queue(uri.toString(), TIMINGS_QUEUE_NAME, queueFactory, Method.PUT);

	}

	/**
	 * Looks for other activities that may complete the given course.
	 * 
	 * @param courseActivityId
	 * @return
	 */
	public static Task timingAttemptsDiscovery(Identifier<String, ?> courseActivityId) {
		URI uri = UriBuilder.fromPath(TIMINGS_END_POINT).build(courseActivityId.getValue());
		return WebApiQueueUtil.putTask(uri);
	}

	/**
	 * Useful method to create the endpoint that will be placed into the #
	 * 
	 * @param alertEndpoint
	 * @param timingRelationId
	 * @return
	 */
	public static String buildTimingRelationAlertEndpoint(String alertEndpoint, Identifier<?, ?> timingRelationId) {
		return KeyedMessageFormat.create(alertEndpoint).put(TIMINGS_RELATION_ID, timingRelationId).build()
				.getFormattedMessage();
	}

	/**
	 * Given the timingRelationId this will queue using the standard application Notifications
	 * queue. One for the course and one for the attempt.
	 * 
	 * It builds a URL that calls back with the timing id...each to their own endpoint:
	 * {@link #TIMINGS_COURSE_ALERT_END_POINT} and {@link #TIMINGS_ATTEMPT_ALERT_END_POINT}.
	 * 
	 * @param timingRelationId
	 *            our single identifier for timing relations.
	 * @param queueFactory
	 */
	public static void queueToNotifyApplicationsCourseCompleted(Identifier<?, ?> timingRelationId,
			QueueFactory queueFactory) {
		queueToNotifyCourseApplicationCourseCompleted(timingRelationId, queueFactory);

		queueToNotifyAttempApplicationCourseCompleted(timingRelationId, queueFactory);
	}

	/**
	 * @param timingRelationId
	 * @param queueFactory
	 */
	public static void queueToNotifyAttempApplicationCourseCompleted(Identifier<?, ?> timingRelationId,
			QueueFactory queueFactory) {
		String attemptEndpoint = buildTimingRelationAlertEndpoint(TIMINGS_ATTEMPT_ALERT_END_POINT, timingRelationId);
		queueToNotifyApplication(attemptEndpoint, queueFactory);
	}

	/**
	 * @param timingRelationId
	 * @param queueFactory
	 */
	public static void queueToNotifyCourseApplicationCourseCompleted(Identifier<?, ?> timingRelationId,
			QueueFactory queueFactory) {
		String courseEndpoint = buildTimingRelationAlertEndpoint(TIMINGS_COURSE_ALERT_END_POINT, timingRelationId);
		queueToNotifyApplication(courseEndpoint, queueFactory);
	}

	/**
	 * @param completeEndpoint
	 * @param queueFactory
	 */
	public static void queueToNotifyApplication(String completeEndpoint, QueueFactory queueFactory) {
		queue(completeEndpoint, TIMINGS_COURSE_COMPLETED_QUEUE_NAME, queueFactory);
	}

	/**
	 * Uses the task queue to call the remote clients to notify the course is ready.
	 * 
	 * @throws MalformedURLException
	 */
	static public <ActivityIdT extends Identifier<?, ?>> URL buildCallbackApplicationCourseCompleted(
			@Nonnull String callbackUrl, @Nonnull ActivityIdT courseActivityId, @Nonnull ActivityIdT attemptActivityId)
			throws MalformedURLException {
		// the don't want to be notified if they didn't provide.

		// replace the parameters with the ids
		callbackUrl = new KeyedMessageFormat.Builder().message(callbackUrl).put(ATTEMPT, attemptActivityId)
				.put(COURSE, courseActivityId).build().getFormattedMessage();

		return new URL(callbackUrl);

	}

}
