/**
 * 
 */
package com.aawhere.jts.geom;

import javax.ws.rs.core.MediaType;

import com.aawhere.ws.rs.MediaTypes;

/**
 * JTS uses geometries in Well Known Text. These are the content-types to be used when publishing
 * the WKT in web services.
 * 
 * I wasn't able to find any pre-defined types for these.
 * 
 * https://en.wikipedia.org/wiki/Well_Known_Text https://aawhere.jira.com/browse/TN-208
 * 
 * @author aroller
 * 
 */
public class WktContentType {

	public static final String WELL_KNOWN_TEXT_MEDIA_TYPE = "application/wkt+text";
	public static final String LINE_STRING_MEDIA_TYPE = "application/wkt.line-string+text";
	public static final String POLYGON_MEDIA_TYPE = "application/wkt.polygon+text";
	public static final String MULTI_LINE_STRING_MEDIA_TYPE = "application/wkt.multi-line-string+text";
	public static final MediaType WELL_KNOWN_TEXT = MediaTypes.create(WELL_KNOWN_TEXT_MEDIA_TYPE);
	public static final MediaType MULTI_LINE_STRING = MediaTypes.create(MULTI_LINE_STRING_MEDIA_TYPE);
	public static final MediaType LINE_STRING = MediaTypes.create(LINE_STRING_MEDIA_TYPE);

	/** The mime type to be sent as the Accept/Content-Type param values. */
	public final String value;

	private WktContentType(String value) {
		this.value = value;
	}
}
