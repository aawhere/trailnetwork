/**
 * 
 */
package com.aawhere.person.group;

import com.aawhere.person.PersonWebApiUri;

/**
 * @author aroller
 * 
 */
public class SocialGroupWebApiUri
		extends PersonWebApiUri {

	public static final String SOCIAL_GROUPS = "socialGroups";
	public static final String SOCIAL_GROUP_ID = "socialGroupId";
	public static final String RELATORS = "relators";
	public static final String SOCIAL_GROUP_ID_PARAM = PARAM_BEGIN + SOCIAL_GROUP_ID + PARAM_END;
	public static final String GROUPS_SOCIAL_GROUP_ID_PATH = GROUPS + SLASH + SOCIAL_GROUP_ID_PARAM;
	public static final String SOCIAL_GROUP_ID_PERSONS_PATH = SOCIAL_GROUP_ID_PARAM + SLASH + PERSONS;

	public static final String MEMBERSHIPS = "memberships";
	public static final String MEMBERSHIP_ID = "socialGroupMembershipId";
	public static final String MEMBERSHIP_ID_PARAM = PARAM_BEGIN + MEMBERSHIP_ID + PARAM_END;
	public static final String MEMBERSHIPS_MEMBERSHIP_ID_PATH = MEMBERSHIPS + SLASH + MEMBERSHIP_ID_PARAM;
	public static final String SOCIAL_GROUP_ID_RELATORS_PATH = SOCIAL_GROUP_ID_PARAM + SLASH + RELATORS;
	public static final String SOCIAL_GROUP_ID_MEMBERSHIPS_PATH = SOCIAL_GROUP_ID_PARAM + SLASH + MEMBERSHIPS;
	public static final String SOCIAL_GROUPS_MEMBERSHIPS_PATH = SOCIAL_GROUPS + SLASH + MEMBERSHIPS;

}
