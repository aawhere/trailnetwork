/**
 *
 */
package com.aawhere.person;

import static com.aawhere.activity.ActivityWebApiUri.*;

import java.io.FilterWriter;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.LongIdentifier;
import com.aawhere.persist.Filter;
import com.aawhere.queue.BatchQueue;
import com.aawhere.queue.EndPoint;
import com.aawhere.queue.Queue;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Method;
import com.aawhere.queue.WebApiEndPoint;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * @author aroller
 * 
 */
public class PersonWebApiUri
		extends SystemWebApiUri {

	public static final String PERSONS = "persons";
	public static final String PERSONS_ENDPOINT = PERSONS + SLASH;
	public static final String PERSON_ID = "personId";
	public static final String PERSONS_IDS = "personsIds";
	public static final String PERSON_ID_PARAM = PARAM_BEGIN + PERSON_ID + PARAM_END;
	public static final String PERSONS_IDS_PARAM = PARAM_BEGIN + PERSONS_IDS + PARAM_END;
	public static final String PERSON_ID_GROUPS_PATH = PERSON_ID_PARAM + SLASH + GROUPS;
	public static final String PERSON_ID_ACTIVITIES_PATH = PERSON_ID_PARAM + SLASH + ACTIVITIES;
	public static final String PERSON_ID_VIEWS_PATH = PERSON_ID_PARAM + SLASH + VIEWS;
	public static final String PERSONS_IDS_MERGED_PATH = PERSONS_IDS_PARAM + SLASH + MERGED;
	public static final String PERSONS_IDS_MERGED_NOTIFIED_PATH = PERSONS_IDS_PARAM + SLASH + MERGED + SLASH + NOTIFIED;
	public static final String PERSONS_ID_INDEX_PATH = PERSON_ID_PARAM + SLASH + SearchWebApiUri.INDEX;

	public static final String PERSON_QUEUE_NAME = RouteWebApiUri.ROUTE_QUEUE_NAME;
	public static final String PERSONS_PERSON_ID_PATH = PERSONS + SLASH + PERSON_ID_PARAM;

	/** Posts the person to be updated. */
	public static Task personUpdatedTask(Identifier<Long, ?> personId) {
		return WebApiQueueUtil.putTask(UriBuilder.fromPath(PERSONS_ENDPOINT + PERSON_ID_PARAM).build(personId));
	}

	/** Post this task when a merge has happened. This will notify any interested parties. */
	public static Task personsMergedNotifiedTask(Iterable<? extends Identifier<Long, ?>> personIds) {
		final URI uri = UriBuilder.fromPath(PERSONS_ENDPOINT + PERSONS_IDS_MERGED_NOTIFIED_PATH)
				.build(IdentifierUtils.idsJoined(personIds));
		// repeats are required since it is posted in a transaction
		return WebApiQueueUtil.putTaskBuilder(uri).setAllowRepeats(true).build();
	}

}
