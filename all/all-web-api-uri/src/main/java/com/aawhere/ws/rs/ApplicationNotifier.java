/**
 * 
 */
package com.aawhere.ws.rs;

import java.net.URISyntaxException;
import java.net.URL;

import com.aawhere.io.EndpointNotFoundException;
import com.aawhere.io.IoException;
import com.aawhere.io.ResourceGoneException;
import com.aawhere.io.UnauthorizedException;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.net.HttpStatusCode;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Simple Utility to make calls to remote Applications for notification purposes.
 * 
 * FIXME:Test this!!! AR 2013.06.28
 * 
 * @author aroller
 * 
 */
public class ApplicationNotifier {

	private ClientResponse response;

	/**
	 * Used to construct all instances of ApplicationNotifier.
	 */
	public static class Builder
			extends ObjectBuilder<ApplicationNotifier> {

		private int timeoutInMillis = 20 * 1000;

		public Builder() {
			super(new ApplicationNotifier());
		}

		public Builder setUrl(URL url) {
			building.url = url;
			return this;
		}

		/**
		 * @param timeoutInMillis
		 *            the timeoutInMillis to set
		 */
		public Builder setTimeoutInMillis(int timeoutInMillis) {
			this.timeoutInMillis = timeoutInMillis;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.exceptions().notNull("url", building.url);
			super.validate();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ApplicationNotifier build() {
			Client client = new Client();
			try {
				WebResource resource = client.resource(building.url.toURI());
				client.setReadTimeout(timeoutInMillis);
				building.response = resource.get(ClientResponse.class);
				client.destroy();
			} catch (URISyntaxException e) {
				// TODO:consider saving this instead of throwing it.
				throw new RuntimeException(e);
			}
			return super.build();

		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private URL url;

	/** Use {@link Builder} to construct ApplicationNotifier */
	private ApplicationNotifier() {
	}

	/**
	 * @return the response
	 */
	public ClientResponse getResponse() {
		return this.response;
	}

	public HttpStatusCode getStatus() {
		return HttpStatusCode.valueOf(this.response.getStatus());
	}

	/**
	 * Throws exceptions appropriate for communicating to the client what went wrong with this
	 * request.
	 * 
	 * @throws EndpointNotFoundException
	 *             , IoException
	 * 
	 */
	public void throwIfNecessary() throws EndpointNotFoundException, UnauthorizedException, ResourceGoneException,
			IoException {
		if (!this.isSuccessful()) {
			throw IoException.best(HttpStatusCode.valueOf(response.getStatus()), this.url.toString());
		}// else don't throw anything
	}

	/**
	 * @return
	 */
	public Boolean isSuccessful() {
		return getStatus().isOk();
	}

}
