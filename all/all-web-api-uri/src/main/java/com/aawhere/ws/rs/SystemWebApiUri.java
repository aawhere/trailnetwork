/**
 *
 */
package com.aawhere.ws.rs;

import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.ResourceUtils;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.util.rb.VerbTense;
import com.google.common.collect.Iterables;

/**
 * Useful constants related to defining WebApi paths with no specific ties to any single domain.
 * 
 * @author Brian Chapman
 * 
 */
public class SystemWebApiUri
		extends UriConstants {

	public static final MediaTypeExtensionMapper MEDIA_TYPE_MAPPER = MediaTypeExtensionMapper.build();

	public static final String ROOT_CONTEXT;
	/**
	 * The application's version number as defined by the build scripts for the particular
	 * environment defined by {@link SystemWebApiUri#APP_NAME}.
	 */

	public static final String APP_VERSION;

	public static final String GZIP_MIME_TYPE = "application/x-gzip";
	public static final String TEXT_CSV_MEDIA_TYPE = MediaTypes.TEXT_CSV;
	/**
	 * only defined in {@link com.google.common.net.MediaType#TSV_UTF_8}, but not available as a
	 * string.
	 */
	public static final String TEXT_TSV_MEDIA_TYPE = "text/tab-separated-values";

	/**
	 * This Media type declaration will put preference of JSON in front of others with less qs.
	 * TN-397
	 */
	public static final String JSON_MEDIA_TYPE = MediaType.APPLICATION_JSON + "; qs=2";
	public static final String NONE_MEDIA_TYPE = MediaTypes.APPLICATION_NONE;

	/** Supports the GIT maven plugin loading of properties files. */
	private static final class Git {
	}

	/**
	 * The key used to identify the environment in which this code is running (based on the build).
	 * KnownInstance defines possibilities used within the system.
	 */
	public static final String APP_NAME;
	public static final String API_BASE_PATH;
	public static final String API_PUBLIC_BASE_PATH;
	public static final String API_PUBLIC_HOST_PATH;
	/**
	 * The aboslute path to the environment without any module nor version information.
	 * http://dev.api.trailnetwork.com
	 */
	public static final String API_PUBLIC_HOST_DEFAULT_PATH;
	public static final String BUILD_USER;
	public static final String BUILD_TIME;
	public static final String BUILD_VERSION;
	public static final String ALL_PROPERTIES;
	public static final String APP_NAME_LOCAL = "development";
	public static final String APP_NAME_DEV = "tn-api-dev";
	public static final String APP_NAME_DEMO = "tn-api-demo";
	public static final String APP_NAME_LIVE = "tn-api-live";
	public static final String DATASOURCE_HTTP_PATH = "datasources";
	/** use {@link #endpoint(String)} for access to the host. */
	static final String HOST_URL;

	/**
	 * <pre>
	 * host-url=http://aroller.default.tn-api-dev.appspot.com
	 * root-context=/rest
	 * api-base-path=http://aroller.default.tn-api-dev.appspot.com/rest
	 * api-host-url-public=http://aroller.dev.api.trailnetwork.com
	 * api-base-path-public=http://aroller.dev.api.trailnetwork.com/rest
	 * all-web-api.gae.application.version=aroller
	 * all-web-api.gae.application.name=tn-api-dev
	 * build-time=2015-02-21T18:20:22+0000
	 * build-user=aroller
	 * build-version=41-SNAPSHOT
	 * </pre>
	 */
	static {

		Properties constants = ResourceUtils.loadProperties(SystemWebApiUri.class);
		ROOT_CONTEXT = constants.getProperty("root-context");
		APP_VERSION = constants.getProperty("all-web-api.gae.application.version");
		APP_NAME = constants.getProperty("all-web-api.gae.application.name");
		HOST_URL = constants.getProperty("host-url");
		API_BASE_PATH = constants.getProperty("api-base-path");
		API_PUBLIC_BASE_PATH = constants.getProperty("api-base-path-public");
		API_PUBLIC_HOST_PATH = constants.getProperty("api-host-url-public");

		API_PUBLIC_HOST_DEFAULT_PATH = constants.getProperty("api-base-path-public-default");
		BUILD_USER = constants.getProperty("build-user");
		BUILD_TIME = constants.getProperty("build-time");
		BUILD_VERSION = constants.getProperty("build-version");
		Properties git = ResourceUtils.loadProperties(Git.class);
		ALL_PROPERTIES = StringUtils.join(	Iterables.concat(constants.entrySet(), git.entrySet()),
											System.lineSeparator());
	}

	public static final String ADMIN = "admin";

	public static final String SYSTEM = "system";
	public static final String FILTER = "filter";
	public static final String FILTER_PAGE = "page";
	public static final String INLINE = "inline";
	public static final String DICTIONARY = "dictionary";
	public static final String DICTIONARIES = "dictionaries";
	public static final String DOMAIN = "domain";
	public static final String DOMAIN_PARAM = PARAM_BEGIN + DOMAIN + PARAM_END;
	public static final String DICTIONARIES_DOMAIN_PATH = DICTIONARIES + SLASH + DOMAIN_PARAM;
	public static final String FIELDS = "fields";
	public static final String FIELD_KEY = "fieldKey";
	public static final String FIELD_KEY_PARAM = PARAM_BEGIN + FIELD_KEY + PARAM_END;
	public static final String FIELDS_FIELD_KEY_PATH = FIELDS + SLASH + FIELD_KEY_PARAM;

	public static final String COUNT = "count";
	public static final String COUNT_PARAM = PARAM_BEGIN + COUNT + PARAM_END;
	public static final String SCORE = "score";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String ASYNCH = "asynch";
	public static final String QUEUE = "queue";
	public static final String ORGANIZED = "organized";
	public static final String RELATED = "related";

	public static final String NOUNS = "nouns";
	public static final String NOUNS_PARAM = PARAM_BEGIN + NOUNS + PARAM_END;
	public static final String VERBS = "verbs";
	public static final String VERBS_PARAM = PARAM_BEGIN + VERBS + PARAM_END;
	/** @see VerbTense */
	public static final String TENSE = "tense";
	public static final String TENSE_PARAM = PARAM_BEGIN + TENSE + PARAM_END;

	/**
	 * Nice suffix to another noun/adjective to indicate that notifications for that endpoint.
	 */
	public static final String NOTIFIED = "notified";
	public static final String MERGED = "merged";
	public static final String VIEWS = "views";
	public static final String PROCESS = "process";
	public static final String JOBS = "jobs";
	public static final String JOB_NAME = "jobName";
	public static final String JOB_NAME_PARAM = PARAM_BEGIN + JOB_NAME + PARAM_END;
	public static final String JOBS_JOB_NAME_PATH = JOBS + SLASH + JOB_NAME_PARAM;

	/** Indicates an action should be more complete than the default. */
	public static final String DEEP = "deep";
	public static final String NAMES = "names";
	/**
	 * General purpose word used when grouping of entities is needed and makes sense in the context.
	 */
	public static final String GROUPS = "groups";
	public static final String GROUP_ID = "groupId";
	public static final String GROUP_ID_PARAM = PARAM_BEGIN + GROUP_ID + PARAM_END;
	public static final String GROUPS_GROUP_ID_PATH = GROUPS + SLASH + GROUP_ID_PARAM;
	/** Used to bypass otherwise thoughtful validation and denials */
	public static final String FORCE = "force";
	/** Indicates the value of {@link #FORCE} is to be forced. */
	public static final Boolean FORCED = true;
	/**
	 * Indicates not to {@link #FORCE}.
	 */
	public static final Boolean FORCED_NOT = false;
	/**
	 * When the caller want's to indicate the default. This is null so the implementation can choose
	 * to go {@link #FORCED} or {@link #FORCED_NOT}.
	 */
	public static final Boolean FORCED_DEFAULT = null;

	public static final String SEARCH = "search";
	public static final String VERBOSE = "verbose";
	/** Used to notify applications and individuals that an event in the workflow has occurred. */
	public static final String ALERT = "alert";
	public static final String FILE_EXTENSION = "ext";
	public static final String FILE_EXTENSION_PARAM = PARAM_BEGIN + FILE_EXTENSION + PARAM_END;
	public static final String FILE_EXTENSION_PARAM_SUFFIX = SUFFIX_SEPARATOR + FILE_EXTENSION_PARAM;
	public static final String UPDATE = "update";

	public static final String URL = "url";

	public static final String LOCALE = "locale";

	public static final String ROLE_ADMIN = ADMIN;

	/**
	 * Used typically as an option to indicate the entities should be touched, possibly to update an
	 * index.
	 */
	public static final String TOUCH = "touch";
	/** A standard indicator that something is out of date. */
	public static final String STALE = "stale";

	public static final String LANGUAGE_NONE = "none";

	/**
	 * Common method to satisfy queue needs for this class.
	 * 
	 * @param uri
	 */
	static public void queue(String uri, String queueName, QueueFactory queueFactory) {
		WebApiQueueUtil.queue(uri, queueName, queueFactory);
	}

	/**
	 * Provides the absolute url without the {@link #ROOT_CONTEXT}
	 * 
	 * @return
	 */
	public static UriBuilder host() {
		return UriBuilder.fromPath(HOST_URL);
	}

	/**
	 * Provides the {@link #API_BASE_PATH} with the given web method.
	 * 
	 * @param webServicePath
	 * @return
	 */
	public static UriBuilder endpoint(String webServicePath) {
		return UriBuilder.fromPath(API_BASE_PATH).path(webServicePath);
	}

	/**
	 * indicates the environment is in local development or on the dev server where special behavior
	 * can be allowed to get past restrictions imposed on live servers.
	 * 
	 * @return
	 */
	public static Boolean isInDevelopment() {
		return APP_NAME.equals(APP_NAME_DEV) || APP_NAME.equals(APP_NAME_LOCAL);
	}

	/**
	 * Formats any iterable into a list value that is suitable as a param or query value.
	 * 
	 * @param routeIds
	 * @return
	 */
	public static String listParam(Iterable<?> items) {
		return StringUtils.join(items, ',');
	}

	/**
	 * @return
	 */
	public static String info() {
		return ALL_PROPERTIES;
	}
}
