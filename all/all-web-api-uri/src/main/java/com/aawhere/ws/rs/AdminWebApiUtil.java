/**
 * 
 */
package com.aawhere.ws.rs;

import java.net.URISyntaxException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsExploder;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.queue.BatchQueue;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Method;
import com.aawhere.queue.WebApiEndPoint;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.util.rb.KeyedMessageFormat;
import com.aawhere.web.api.geo.GeoWebApiUri;

/**
 * @author aroller
 * 
 */
public class AdminWebApiUtil {

	/**
	 * All {@link #TEMPLATE_END_TOKEN} will change to {@link #TEMPLATE_END}
	 * 
	 */
	private static final String TEMPLATE_END = "}";

	/**
	 * All {@link #TEMPLATE_START_TOKEN} will change to {@link #TEMPLATE_START}
	 * 
	 */
	private static final String TEMPLATE_START = "{";
	static final String TEMPLATE_START_TOKEN = "\\[";
	static final String TEMPLATE_END_TOKEN = "\\]";

	/**
	 * Passed from the web to and it will be changed to {@link #AND} in {@link #decode(String)}
	 * 
	 */
	static final String AND_TOKEN = "-and-";
	static final String AND = "&";

	/**
	 * @param name
	 * @param encodedUri
	 * @param method
	 * @param cells
	 * @return
	 * @throws URISyntaxException
	 */
	public static String queue(@Nullable String name, @Nonnull String encodedUri, @Nullable Method method,
			@Nullable GeoCells cells, @Nullable Resolution explodeTo, QueueFactory queueFactory)
			throws URISyntaxException {
		if (name == null) {
			name = WebApiQueueUtil.DEFAULT_QUEUE_NAME;
		}
		if (method == null) {
			method = Method.PUT;
		}
		StringBuilder result = new StringBuilder();
		String uri = decode(encodedUri);
		final Queue queue = queueFactory.getQueue(name);
		if (cells == null) {
			WebApiEndPoint endpoint = new WebApiEndPoint.Builder().setEndPoint(uri).build();
			Task task = Task.create().setEndPoint(endpoint).setMethod(method).build();
			queue.add(task);
			result.append(task.toString());
		} else {
			GeoCells exploded = GeoCellsExploder.explode().these(cells).to(explodeTo).exploded();
			BatchQueue batchQueue = BatchQueue.create().setQueue(queue).build();
			for (GeoCell explodedCell : exploded) {
				// replace the cells template parameter with the exploded cell
				String uriReplaced = KeyedMessageFormat.create(uri).put(GeoWebApiUri.CELLS, explodedCell).build()
						.getFormattedMessage();
				WebApiEndPoint endpoint = new WebApiEndPoint.Builder().setEndPoint(uriReplaced).build();
				Task task = Task.create().setEndPoint(endpoint).setMethod(method).build();
				batchQueue.add(task);
				// if the batch is big then limit the output.
				if (batchQueue.getNumberOfBatches() < 10) {
					result.append(uriReplaced);
					result.append(System.lineSeparator());
				}
			}
			Integer count = batchQueue.finish();
			result.insert(0, count + " geocell endpoints queued for " + uri);
		}
		return result.toString();
	}

	/**
	 * special URL escaping instead of the standard since question mark will be escaped, but we want
	 * to keep it. & is interpreted as & for the entire query and not the uri query so we provide
	 * our own custom add other characters here that may need to be encoded to pass admin operations
	 * 
	 * @param encodedUri
	 * @return
	 */
	static String decode(String encodedUri) {
		return encodedUri.replaceAll(AND_TOKEN, AND).replaceAll(" ", "+")
				.replaceAll(TEMPLATE_START_TOKEN, TEMPLATE_START).replaceAll(TEMPLATE_END_TOKEN, TEMPLATE_END);
	}

}
