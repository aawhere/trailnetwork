/**
 * 
 */
package com.aawhere.queue;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.UriBuilder;

import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.ws.rs.UriConstants;

import com.google.common.net.MediaType;

/**
 * @author Brian Chapman
 * 
 */
public class WebApiEndPoint
		extends EndPoint {

	public static class Builder
			extends EndPoint.AbstractBuilder<WebApiEndPoint, Builder> {

		private MediaType mediaType;
		private String endPoint;
		private boolean absolute = false;
		private Object[] values;

		/**
		 * @param building
		 */
		public Builder() {
			super(new WebApiEndPoint());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.queue.EndPoint.AbstractBuilder#setEndPoint(java.lang.String)
		 */
		@Override
		public Builder setEndPoint(String uri) throws URISyntaxException {
			this.endPoint = uri;
			return this;
		}

		@Override
		public Builder setEndPoint(URI endPoint) {
			this.endPoint = endPoint.toString();
			return dis;
		}

		/**
		 * Adds the optional extension when a media type is provided.
		 * */
		public Builder setMediaType(MediaType mediaType) {
			this.mediaType = mediaType;
			return this;
		}

		/**
		 * Adds the host and context to the beginning of the endpoint for absolute refernce from
		 * external sources.
		 */
		public Builder absolute() {
			this.absolute = true;
			return this;
		}

		/**
		 * Passed to the {@link UriBuilder#build(Object...)}
		 * 
		 * @param values
		 * @return
		 */
		public Builder values(Object... values) {
			this.values = values;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public WebApiEndPoint build() {
			if (this.mediaType != null) {
				String extension = SystemWebApiUri.MEDIA_TYPE_MAPPER.getExtensionFor(mediaType);
				if (extension != null) {
					endPoint = endPoint + UriConstants.SUFFIX_SEPARATOR + extension;
				}
			}
			UriBuilder uriBuilder;
			if (absolute) {
				uriBuilder = SystemWebApiUri.endpoint(endPoint);
			} else if (endPoint.contains("?")) {
				// FIXME:This is a hack to support ready urls that don't need template replacement
				// This is only to support Garmin Connect activity crawling I think AR 2013-11
				uriBuilder = UriBuilder.fromUri(SystemWebApiUri.ROOT_CONTEXT + SystemWebApiUri.SLASH + endPoint);
			} else {
				uriBuilder = UriBuilder.fromUri(SystemWebApiUri.ROOT_CONTEXT).path(endPoint);
			}
			super.setEndPoint(uriBuilder.build(values));
			return super.build();

		}
	}

	/* use Builder to create */
	private WebApiEndPoint() {

	}

	public static EndPoint.Builder create() {
		throw new UnsupportedOperationException(
				"you think you are getting a WebApiEndPoint, but actually you're not. this can be fixed by proper builder extension");
	}

	public static Builder createWeb() {
		return new Builder();
	}

}
