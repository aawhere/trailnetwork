/**
 * 
 */
package com.aawhere.queue;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

import org.apache.http.Header;

import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.activity.timing.ActivityTimingWebApiUri;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.queue.Task.Builder;
import com.aawhere.queue.Task.Method;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.common.collect.Sets;

/**
 * Utilities for working with {@link Queue} in the WebApi.
 * 
 * @author Brian Chapman
 * 
 */
public class WebApiQueueUtil {

	public static final String CRAWL_QUEUE_NAME = "1-crawl";
	public static final String DEFAULT_QUEUE_NAME = "default";
	public static final HttpStatusCode ACCEPTED = HttpStatusCode.ACCEPTED;

	/**
	 * Queue without payload. Typically used when all the relavant information is encoded in thez
	 * url.
	 * 
	 * @param uri
	 */
	public static void queue(String uri, String queueName, QueueFactory queueFactory) {
		queue(uri, queueName, queueFactory, new Header[] {});
	}

	public static void queue(String uri, String queueName, QueueFactory queueFactory, Header... headers) {
		final Method method = Task.Method.GET;
		queue(uri, queueName, queueFactory, method, headers);
	}

	/**
	 * A really standard way to build a task given the uri this will set it into the endpoint and
	 * give the put method. This does not allow repeats unless this code
	 * {@link SystemWebApiUri#isInDevelopment()}!!!
	 * 
	 * @param uri
	 * @return
	 */
	public static Task putTask(URI uri) {
		final Builder builder = putTaskBuilder(uri);
		return builder.build();
	}

	/**
	 * @param uri
	 * @return
	 */
	public static Builder putTaskBuilder(URI uri) {
		EndPoint endPoint = new WebApiEndPoint.Builder().setEndPoint(uri).build();
		boolean allowRepeats = repeatsAllowed();
		final Builder builder = Task.create().setEndPoint(endPoint).setMethod(Method.PUT).setAllowRepeats(allowRepeats);
		return builder;
	}

	/**
	 * Allows parts of the code to know if this system is allowing repeats or not.
	 * 
	 * @return
	 */
	public static Boolean repeatsAllowed() {
		return SystemWebApiUri.isInDevelopment();
	}

	/**
	 * Builds a task with the optional headers calling the given uri adding to the queue represented
	 * by the given name found in the given QueueFactory.
	 * 
	 * TODO:This could get ridiculous...consider another Builder to handle the many options and
	 * putting them together.
	 * 
	 * @param uri
	 * @param queueName
	 * @param queueFactory
	 * @param basicHeader
	 */
	public static void queue(String uri, String queueName, QueueFactory queueFactory, Method method, Header... headers) {
		WebApiEndPoint endPoint;
		try {
			endPoint = new WebApiEndPoint.Builder().setEndPoint(uri).build();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
		Builder taskBuilder = new Task.Builder().setEndPoint(endPoint).setMethod(method);
		for (int i = 0; i < headers.length; i++) {
			Header header = headers[i];
			taskBuilder.addHeader(header.getName(), header.getValue());
		}
		Task task = taskBuilder.build();
		Queue queue = queueFactory.getQueue(queueName);
		queue.add(task);
	}

	/**
	 * A set containing all queues known to this application.
	 * 
	 * @return
	 */
	public static Set<String> knownQueues() {
		return Sets.newHashSet(	CRAWL_QUEUE_NAME,
								ActivityWebApiUri.ACTIVITY_IMPORT_QUEUE_NAME,
								ActivityTimingWebApiUri.TIMINGS_QUEUE_NAME,
								ActivityTimingWebApiUri.TIMINGS_COURSE_ATTEMPT_QUEUE_NAME,
								ActivityTimingWebApiUri.TIMINGS_COURSE_COMPLETED_QUEUE_NAME,
								RouteWebApiUri.ROUTE_QUEUE_NAME);
	}
}
