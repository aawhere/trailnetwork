/**
 *
 */
package com.aawhere.trailhead;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.joda.time.Duration;

import com.aawhere.id.Identifier;
import com.aawhere.queue.EndPoint;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Method;
import com.aawhere.queue.WebApiEndPoint;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * Supporting URL for the Trailheads of the system.
 * 
 * @author aroller
 * 
 */
public class TrailheadWebApiUri
		extends GeoWebApiUri {
	public static final String TRAILHEAD_ID = "trailheadId";
	public static final String TRAILHEAD_ID_PARAM = PARAM_BEGIN + TRAILHEAD_ID + PARAM_END;

	public static final String TRAILHEADS = "trailheads";
	public static final String TRAILHEADS_TRAILHEAD_ID_PATH = TRAILHEADS + SLASH + TRAILHEAD_ID_PARAM;
	public static final String TRAILHEAD_ID_NAMES_PATH = TRAILHEAD_ID_PARAM + SLASH + NAMES;
	public static final String TRAILHEAD_ID_ORGANIZED_PATH = TRAILHEAD_ID_PARAM + SLASH + ORGANIZED;
	public static final String TRAILHEADS_ID_INDEX_PATH = TRAILHEAD_ID_PARAM + SLASH + SearchWebApiUri.INDEX;
	public static final String TRAILHEADS_ENDPOINT = TRAILHEADS + SLASH;

	/**
	 * Workflow begins with organizing the area trailheads around the given trailhead. It continues
	 * with oranizing the routes starting from the trailhead. It finishes with a call to update the
	 * trailhead name, score, etc. There is a delay set in the task to avoid processing before the
	 * routes are ready for update. It is protected against repeats so calling multiple times will
	 * be handled by the queue.
	 * 
	 * @param trailheadId
	 * @return
	 */
	public static Task trailheadUpdateWorkflowStart(Identifier<String, ?> trailheadId) {
		// delays this task assuming there may be related routes that are still being updated.
		final long delay;
		if (SystemWebApiUri.isInDevelopment()) {
			delay = Duration.standardMinutes(5).getMillis();
		} else {
			delay = Duration.standardHours(1).getMillis();
		}
		return Task.clone(trailheadsOrganized(trailheadId)).setCountdownMillis(delay).build();
	}

	/**
	 * Organizes the trailheads surrounding the given trailhead. Actually uses the id value as the
	 * bounds.
	 */
	public static Task trailheadsOrganized(Identifier<String, ?> trailheadId) {
		URI uri = UriBuilder.fromPath(TRAILHEADS + SLASH + TRAILHEAD_ID_ORGANIZED_PATH).build(trailheadId);
		return WebApiQueueUtil.putTask(uri);
	}

	public static Task routeTrailheadUpdateTask(Identifier<String, ?> trailheadId) {
		URI uri = UriBuilder.fromPath(TRAILHEADS_TRAILHEAD_ID_PATH).build(trailheadId);
		return WebApiQueueUtil.putTask(uri);
	}

	/** Used to queue a task that will update the trailheads for the area given. */
	public static Task routeTrailheadsUpdateTask(String bounds) {
		URI uri = UriBuilder.fromPath(TRAILHEADS).queryParam(BOUNDS, BOUNDS_PARAM).build(bounds);
		EndPoint endPoint = new WebApiEndPoint.Builder().setEndPoint(uri).build();
		return Task.create().setEndPoint(endPoint).setMethod(Method.PUT).build();
	}
}
