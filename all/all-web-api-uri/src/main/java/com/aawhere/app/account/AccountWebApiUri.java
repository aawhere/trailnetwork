/**
 * 
 */
package com.aawhere.app.account;

import static com.aawhere.person.PersonWebApiUri.*;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.StringIdentifier;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.Task;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * Constants used to communicate Accounts.
 * 
 * @author aroller
 * 
 */
public class AccountWebApiUri
		extends SystemWebApiUri {

	public static final String ACCOUNT = "account";
	public static final String ACCOUNTS = "accounts";
	public static final String ACCOUNT_ID = "accountId";
	public static final String ACCOUNT_ALIAS = "accountAlias";
	public static final String ACCOUNT_ID_PARAM = PARAM_BEGIN + ACCOUNT_ID + PARAM_END;
	public static final String ACCOUNT_ALIAS_PARAM = PARAM_BEGIN + ACCOUNT_ALIAS + PARAM_END;
	public static final String ACCOUNTS_ID_PARAM_PATH = ACCOUNTS + SLASH + ACCOUNT_ID_PARAM;
	public static final String ACCOUNT_ID_PERSONS_PATH = ACCOUNT_ID_PARAM + SLASH + PERSONS;
	public static final String ACCOUNT_QUEUE_NAME = RouteWebApiUri.ROUTE_QUEUE_NAME;

	public static Task accountUpdatedTask(Identifier<String, ?> accountId) {
		URI uri = UriBuilder.fromPath(ACCOUNTS + SLASH + ACCOUNT_ID_PARAM).build(accountId.getValue());
		return WebApiQueueUtil.putTask(uri);
	}

	/** Provides a specific queue based on the application given. */
	public static Queue
			getCrawlQueue(QueueFactory queueFactory, StringIdentifier<?> applicationKey, String... suffixes) {

		String[] all = ArrayUtils
				.addAll(new String[] { WebApiQueueUtil.CRAWL_QUEUE_NAME, applicationKey.toString() }, suffixes);
		String compositeIdValue = IdentifierUtils.createCompositeIdValue(all);

		String queueName = compositeIdValue;
		return queueFactory.getQueue(queueName);
	}

}
