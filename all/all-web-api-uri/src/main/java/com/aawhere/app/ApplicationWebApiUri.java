/**
 * 
 */
package com.aawhere.app;

import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * @author aroller
 * 
 */
public class ApplicationWebApiUri
		extends SystemWebApiUri {

	public static final String APPLICATION = "application";
	public static final String APPLICATIONS = APPLICATION + "s";
	public static final String APPLICATION_KEY = "applicationKey";
	public static final String APPLICATION_KEY_PARAM = PARAM_BEGIN + APPLICATION_KEY + PARAM_END;
	public static final String APPLICATION_REFERENCE = "applicationReference";

	/**
	 * Provides static references used in annotations to reference endpoints available to the
	 * public. All endpoints must be absolute starting with
	 * {@link SystemWebApiUri#API_PUBLIC_BASE_PATH}
	 */
	public static final class Public {
		public static final String APPLICATIONS = API_PUBLIC_BASE_PATH + SLASH + ApplicationWebApiUri.APPLICATIONS;
	}

}
