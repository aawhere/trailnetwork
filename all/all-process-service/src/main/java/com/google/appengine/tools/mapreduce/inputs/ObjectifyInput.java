package com.google.appengine.tools.mapreduce.inputs;

import java.io.DataInput;
import java.io.Serializable;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.impl.util.SerializationUtil;
import com.google.common.base.Function;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.OnLoad;

/**
 * An {@link Input} of {@link Entity} objects similar to {@link DataInput}, but uses
 * {@link Objectify} to translate the objects into their corresponding Pojo identified a <E>.
 * 
 * See {@link BaseDatastoreInput} for understanding the sharding strategy for
 * {@link DatastoreShardStrategy}. This is located in the same package for access to protected
 * methods.
 * 
 * The optional {@link #entityFunction} will transform the loaded entity into whatever may be
 * necessary after loading. Since objectify does not call the annotated {@link OnLoad} method.
 * 
 * This class should remain dependent only on objectify and datastore so it may be shared with
 * others. It has been written in a style consistent with other {@link Input} classes.
 * 
 * @author aroller
 * 
 * @param <E>
 *            the Pojo that will be translated from the {@link Entity}
 */
public class ObjectifyInput<E>
		extends BaseDatastoreInput<E, ObjectifyInput.Reader<E>> {

	private static final int AVG_ELEMENT_SIZE_DEFAULT = 1024;
	final private long avgElementSize;
	private EntityFunction<E> entityFunction;

	public ObjectifyInput(Class<E> entityType, int numberOfShards, EntityFunction<E> entityFunction) {
		this(entityType, null, numberOfShards, entityFunction);
	}

	public ObjectifyInput(Class<E> entityType, Query query, int numberOfShards, EntityFunction<E> entityFunction) {
		super(allEntityQuery(query, entityType), numberOfShards);
		if (Serializable.class.isAssignableFrom(entityType)) {
			QueryResultIterator<E> iterator = ObjectifyService.factory().begin().load().type(entityType).limit(1)
					.iterator();
			if (iterator.hasNext()) {
				byte[] serialize = SerializationUtil.serializeToByteArray((Serializable) iterator.next());
				this.avgElementSize = serialize.length;
			} else {
				this.avgElementSize = AVG_ELEMENT_SIZE_DEFAULT;
			}
		} else {
			this.avgElementSize = AVG_ELEMENT_SIZE_DEFAULT;
		}

		this.entityFunction = entityFunction;
	}

	public ObjectifyInput(Class<E> entityType, int numberOfShards) {
		this(entityType, null, numberOfShards, new EntityFunction<E>());
	}

	private static Query allEntityQuery(Query query, Class<?> entityType) {
		if (query != null) {
			return query;
		} else {
			String kind = Key.getKind(entityType);
			return new Query(kind);
		}
	}

	@Override
	protected Reader<E> createReader(Query query) {
		return new Reader<E>(query, this.avgElementSize, this.entityFunction);
	}

	public static class Reader<E>
			extends BaseDatastoreInputReader<E> {

		private long avgElementSize;

		public Reader(Query query, long avgElementSize, EntityFunction<E> entityFunction) {
			super(query, entityFunction);
			this.avgElementSize = avgElementSize;
		}

		@Override
		protected long getAvgElementSize() {
			return avgElementSize;
		}

		private static final long serialVersionUID = 8729643949502278243L;
	}

	public static class EntityFunction<E>
			implements Function<Entity, E>, Serializable {

		private static final long serialVersionUID = 8585161288506892190L;

		@Override
		public E apply(Entity input) {
			if (input == null) {
				return null;
			} else {
				Objectify objectify = ObjectifyService.factory().begin();
				@SuppressWarnings("unchecked")
				E pojo = (E) objectify.load().fromEntity(input);
				// WARNING: toPojo doesn't call OnLoad annotated methods
				return pojo;
			}
		}

	}

	private static final long serialVersionUID = 1850051793441819214L;

}
