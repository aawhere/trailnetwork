package com.google.appengine.tools.mapreduce.inputs;

import java.nio.ByteBuffer;

import com.google.appengine.tools.mapreduce.GoogleCloudStorageFileSet;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.MapReduceResult;
import com.google.appengine.tools.mapreduce.Marshallers;

public class MapReduceInputUtil {

	/**
	 * Unmarshalls serialized objects from the {@link GoogleCloudStorageFileSet}given.
	 * 
	 * @param fileSetResult
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <I> UnmarshallingInput<I> input(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) {
		Input<ByteBuffer> cloudFileInput = new GoogleCloudStorageLevelDbInput(fileSetResult.getOutputResult());
		UnmarshallingInput input = new UnmarshallingInput(cloudFileInput, Marshallers.getSerializationMarshaller());
		return input;
	}
}
