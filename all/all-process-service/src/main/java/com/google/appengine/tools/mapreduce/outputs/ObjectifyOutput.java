package com.google.appengine.tools.mapreduce.outputs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.mapreduce.Output;
import com.google.appengine.tools.mapreduce.OutputWriter;
import com.google.common.collect.ImmutableList;
import com.googlecode.objectify.ObjectifyService;

/**
 * Uses objectify to translate the entity into and {@link Entity}.
 * 
 * @author aroller
 * 
 */
public class ObjectifyOutput<O>
		extends Output<O, Void> {

	private static final long serialVersionUID = -4464080997561550255L;

	@Override
	public List<? extends OutputWriter<O>> createWriters(int numShards) {
		ImmutableList.Builder<ObjectifyOutputWriter<O>> writers = ImmutableList.builder();
		for (int i = 0; i < numShards; i++) {
			writers.add(new ObjectifyOutputWriter<O>());
		}
		return writers.build();
	}

	@Override
	public Void finish(Collection<? extends OutputWriter<O>> writers) throws IOException {
		// nothing to report
		return null;
	}

	public static class ObjectifyOutputWriter<O>
			extends OutputWriter<O> {

		private int batchSize = 100;
		private ArrayList<O> buffer = new ArrayList<O>(batchSize);

		private static final long serialVersionUID = -5654423133866578710L;

		@Override
		public void write(O value) throws IOException {
			if (buffer.size() >= batchSize) {
				flush();
			}
			buffer.add(value);
		}

		private void flush() {
			if (buffer != null && !buffer.isEmpty()) {
				// call now so if there is a failure the shard will retry.
				// let MapReduce handle asynchronous
				ObjectifyService.factory().begin().save().entities(buffer).now();
			}
			// creating new since save is async and reference remains
			buffer = new ArrayList<O>(batchSize);
		}

		@Override
		public void endShard() throws IOException {
			flush();
			super.endShard();
		}

		@Override
		public void endSlice() throws IOException {
			flush();
			super.endSlice();
		}
	}

}
