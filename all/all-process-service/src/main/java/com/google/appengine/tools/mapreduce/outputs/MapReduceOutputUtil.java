package com.google.appengine.tools.mapreduce.outputs;

import java.io.Serializable;

import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.gae.AppEngineUtil;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.tools.mapreduce.GoogleCloudStorageFileSet;
import com.google.appengine.tools.mapreduce.Marshallers;
import com.google.appengine.tools.pipeline.Job;
import com.google.common.net.MediaType;

public class MapReduceOutputUtil {

	/**
	 * Provides the standard file output separated into a heirarchy of directories. The number of
	 * outputs depends upon the number of reducer shards if resulting with reducer, or input shards
	 * if resulting from mapper . This fileset will then declare the number of input shards to read
	 * the files.
	 * 
	 * @param pipelineClass
	 * @param pipelineKey
	 * @param jobName
	 * @param bucketName
	 * @return
	 */
	public static MarshallingOutput<Serializable, GoogleCloudStorageFileSet> fileOutput(
			Class<? extends Job<?>> pipelineClass, String pipelineKey, String jobName, String bucketName) {
		String fileNamePattern = "Jobs/";
		// long names are causing problems with development server...and provide no value locally
		if (AppEngineUtil.atAppspot()) {
			fileNamePattern += pipelineClass.getSimpleName() + "/" + pipelineKey + "/" + jobName + "/";
		}
		fileNamePattern += jobName + "-%03d";
		GoogleCloudStorageLevelDbOutput cloudFileOutput = new GoogleCloudStorageLevelDbOutput(bucketName,
				fileNamePattern, MediaType.OCTET_STREAM.toString());

		return new MarshallingOutput<>(cloudFileOutput, Marshallers.getSerializationMarshaller());
	}

	public static MarshallingOutput<Serializable, GoogleCloudStorageFileSet> fileOutput(
			Class<? extends Job<?>> pipelineClass, Key pipelineKey, String jobName, String bucketName) {
		return fileOutput(pipelineClass, pipelineKey.getName(), jobName, bucketName);
	}

	public static MarshallingOutput<Serializable, GoogleCloudStorageFileSet> fileOutput(
			Class<? extends Job<?>> pipelineClass, Key pipelineKey, String jobName, MapReduceJobSettings settings) {
		return fileOutput(pipelineClass, pipelineKey.getName(), jobName, settings.bucketName());
	}

}
