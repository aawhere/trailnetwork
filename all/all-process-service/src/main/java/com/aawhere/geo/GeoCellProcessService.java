/**
 * 
 */
package com.aawhere.geo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;

import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class GeoCellProcessService {

	public GeoCellProcessService() {
	}

	/**
	 * Explodes the given geocells into the resolution given (if any) and returns summaries
	 * representing each of the geocells.
	 * 
	 * @param geoCells
	 * @param explodeTo
	 * @return
	 */
	@Nonnull
	public GeoCellSummaries summaries(@Nonnull GeoCells geoCells, @Nullable Resolution explodeTo) {

		return GeoCellSummaryUtil.summaries(geoCells, explodeTo);

	}
}
