package com.aawhere.search;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class SearchProcessService {

	@Inject
	private static SearchService searchService;

	public static SearchService searchService() {
		return searchService;
	}
}
