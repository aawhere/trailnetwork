package com.aawhere.all.process;

import java.io.Serializable;

import javax.annotation.Nonnull;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

/**
 * For situations where the Input is also used as the value and the key may come from the Input then
 * provide a function that will transform the value into the key and this will provide the mapper.
 * 
 * Note: The function needs to be {@link Serializable}
 * 
 * @author aroller
 * 
 * @param <K>
 *            the key for all values to be related during shuffling
 * @param <V>
 *            the value and the input where the key may be produced from the {@link #function}
 */
public class FunctionIdentityMapper<K, V>
		extends Mapper<V, K, V> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2411802792808153097L;
	/**
	 * Transforms the value into the key.
	 * 
	 */
	@Nonnull
	private Function<V, K> function;
	/**
	 * indicates if the value should be provided in the emission. This is optionally assigned and
	 * will default to always true if nothing is given.
	 * 
	 */
	private Predicate<V> filter;

	/**
	 * Used to construct all instances of FunctionIdentityMapper.
	 */
	public static class Builder<K, V>
			extends ObjectBuilder<FunctionIdentityMapper<K, V>> {

		public Builder() {
			super(new FunctionIdentityMapper<K, V>());
		}

		/** @see #filter */
		public Builder<K, V> filter(Predicate<V> filter) {
			building.filter = filter;
			Assertion.exceptions().assertInstanceOf("function", building.function, Serializable.class);

			return this;
		}

		/** @see #function */
		public Builder<K, V> function(Function<V, K> function) {
			building.function = function;
			Assertion.exceptions().assertInstanceOf("function", building.function, Serializable.class);

			return this;
		}

		@Override
		public FunctionIdentityMapper<K, V> build() {
			FunctionIdentityMapper<K, V> built = super.build();
			if (built.filter == null) {
				built.filter = Predicates.<V> alwaysTrue();
			}

			return built;
		}

	}// end Builder

	public static <K, V> Builder<K, V> create() {
		return new Builder<K, V>();
	}

	/** Use {@link Builder} to construct FunctionIdentityMapper */
	private FunctionIdentityMapper() {
	}

	@Override
	public void map(V value) {
		K key = function.apply(value);
		if (filter.apply(value)) {
			emit(key, value);
		}
	}

}
