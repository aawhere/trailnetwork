package com.aawhere.all.process;

import com.aawhere.activity.Activity;
import com.aawhere.id.Identifier;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.ObjectifyInputBuilder;
import com.aawhere.xml.UrlDisplay;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.tools.mapreduce.MapJob;
import com.google.appengine.tools.mapreduce.MapSpecification;
import com.google.appengine.tools.mapreduce.outputs.ObjectifyOutput;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class AllProcessService {

	public enum EntityType {
		ACTIVITY(Activity.class);

		public final Class<?> type;

		private EntityType(Class<?> type) {
			this.type = type;
		}
	}

	@Inject
	public AllProcessService() {
	}

	/**
	 * Touches (load+save) every entity that matches the given query
	 * 
	 * @param query
	 *            limits the entities
	 * @return the job url to be monitored.
	 */
	public <E extends BaseEntity<E, I>, I extends Identifier<?, E>> UrlDisplay touch(Query query, Class<E> entityType) {

		MapSpecification.Builder<E, E, Void> specBuilder = new MapSpecification.Builder<>();
		MapReduceJobSettings settings = MapReduceJobSettings.create().build();
		specBuilder.setInput(ObjectifyInputBuilder.<E, I> create(entityType).query(query).build().input());
		specBuilder.setMapper(PassThroughMapper.<E> create().build());
		specBuilder.setOutput(new ObjectifyOutput<E>());
		specBuilder.setJobName("Touch" + query.getKind());
		String jobId = MapJob.start(specBuilder.build(), settings.settings());
		return new UrlDisplay(PipelineWebApiUri.pipelineStatus(jobId));
	}
}
