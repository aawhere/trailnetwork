/**
 * 
 */
package com.aawhere.all.process;

import java.io.Serializable;

import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.MapReduceJob;
import com.google.appengine.tools.mapreduce.MapReduceSpecification;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.Output;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.inputs.DatastoreInput;
import com.google.appengine.tools.mapreduce.outputs.DatastoreOutput;

/**
 * Base class for {@link MapReduceJob} support class providing common functions.
 * 
 * 
 * @author aroller
 * 
 */
public abstract class EntityMapReduceProcess<K, V>
		extends MapReduceProcess<Entity, K, V, Entity, Void> {

	/** The entity type being selected */
	private Class<?> entityType;

	/**
	 * Used to construct all instances of MapReduceProcess.
	 */
	public static class Builder<P extends EntityMapReduceProcess<K, V>, K, V, B extends Builder<P, K, V, B>>
			extends AbstractObjectBuilder<P, B> {

		protected Builder(P process) {
			super(process);
		}

		private EntityMapReduceProcess<K, V> building() {
			return (building);
		}

		protected B kind(Class<?> entityType) {
			building().entityType = entityType;
			return dis;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("inputEntityKind", building().inputEntityKind());

		}

		@Override
		public P build() {
			P built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct MapReduceProcess */
	protected EntityMapReduceProcess() {
	}

	@Override
	protected Input<Entity> input() {
		return datastoreInput();
	}

	/**
	 * Allows overriding to provide a specialty datastore input (such as a Query).
	 * 
	 * @return
	 */
	protected DatastoreInput datastoreInput() {
		return new DatastoreInput(inputEntityKind(), inputShardCount());
	}

	@Override
	protected Output<Entity, Void> output() {
		return new DatastoreOutput();
	}

	/**
	 * provides the kind if {@link #entityType} was given during building.
	 * 
	 * @return
	 */
	public String inputEntityKind() {
		try {
			return ObjectifyKeyUtils.getKind(entityType);
		} catch (IllegalArgumentException e) {
			return entityType.getSimpleName();
		}
	}

	private static final long serialVersionUID = 6582563254887139163L;
}
