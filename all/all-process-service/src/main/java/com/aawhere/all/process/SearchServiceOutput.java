package com.aawhere.all.process;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nullable;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchProcessService;
import com.aawhere.search.SearchService;
import com.google.appengine.api.search.checkers.SearchApiLimits;
import com.google.appengine.tools.mapreduce.Output;
import com.google.appengine.tools.mapreduce.OutputWriter;
import com.google.appengine.tools.mapreduce.impl.MapReduceConstants;
import com.google.common.collect.ImmutableList;

/**
 * An {@link Output} that receives {@link SearchDocument} and does a
 * {@link SearchService#index(Iterable)} using a buffer.
 * 
 * @author aroller
 * 
 */
public class SearchServiceOutput
		extends Output<SearchDocument, Void> {

	/**
	 * Used to construct all instances of SearchServiceOutput.
	 */
	public static class Builder
			extends ObjectBuilder<SearchServiceOutput> {

		public Builder() {
			super(new SearchServiceOutput());
		}

		@Override
		public SearchServiceOutput build() {
			SearchServiceOutput built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct SearchServiceOutput */
	private SearchServiceOutput() {
	}

	/**
	 * 
	 */

	@Override
	public List<? extends OutputWriter<SearchDocument>> createWriters(int count) {
		ImmutableList.Builder<Writer> builder = ImmutableList.builder();
		for (int i = 0; i < count; i++) {
			builder.add(new Writer());
		}

		return builder.build();
	}

	@Override
	public Void finish(Collection<? extends OutputWriter<SearchDocument>> genericWriters) throws IOException {
		// nothing to report
		return null;
	}

	private static final long serialVersionUID = -6391384573003296579L;

	public static class Writer
			extends OutputWriter<SearchDocument> {
		/**
		 * The number of documents to be in the {@link #buffer} before flushing the buffer.
		 */
		private int maxBufferSize = SearchApiLimits.PUT_MAXIMUM_DOCS_PER_REQUEST - 1;
		private int failedAttemptCount = 0;
		/**
		 * The search documents separated by the index the document is part of since bulk indexing
		 * must be homogenous per index.
		 * 
		 */
		private LinkedList<SearchDocument> buffer = new LinkedList<SearchDocument>();
		private static final long serialVersionUID = -5904978036885802422L;

		@Override
		public void write(SearchDocument searchDocument) throws IOException {
			add(searchDocument, false);
		}

		@Override
		public boolean allowSliceRetry() {
			return failedAttemptCount < 5;
		}

		@Override
		public void endShard() throws IOException {
			add(null, true);
		}

		@Override
		public void endSlice() throws IOException {
			add(null, true);
		}

		@Override
		public long estimateMemoryRequirement() {
			return MapReduceConstants.DEFAULT_IO_BUFFER_SIZE;
		}

		/**
		 * Does all the processing to isolate it to a single place. I don't know if this must be
		 * thread safe so its synchronized just in case.
		 * 
		 * @param document
		 *            the document to be added or null if none to be added right now
		 * @param flush
		 *            true if you wish to force a flush now
		 * @throws IOException
		 */
		private void add(@Nullable SearchDocument document, boolean flush) throws IOException {
			if (document != null) {
				buffer.add(document);
			}
			if (!buffer.isEmpty() && (flush || buffer.size() >= maxBufferSize)) {
				try {
					// Notice this must not be asynchronous to properly report failures
					// Map Reduce is naturally "asynchronous" in it's shards and slices
					SearchProcessService.searchService().index(buffer);
					buffer.clear();
					this.failedAttemptCount = 0;
				} catch (Exception e) {
					LoggerFactory.getLogger(getClass()).warning(this.failedAttemptCount + " times failed due to "
							+ e.getMessage());
					this.failedAttemptCount++;
					if (e instanceof RuntimeException) {
						throw (RuntimeException) e;
					} else {
						throw new IOException(e);
					}
				}
			}

		}

	}

}
