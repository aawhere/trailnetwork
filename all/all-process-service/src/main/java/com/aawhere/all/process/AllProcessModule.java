/**
 *
 */
package com.aawhere.all.process;

import com.aawhere.activity.ActivityProcessService;
import com.aawhere.app.account.AccountProcessService;
import com.aawhere.person.PersonProcessService;
import com.aawhere.route.RouteProcessService;
import com.aawhere.search.SearchProcessService;
import com.aawhere.trailhead.TrailheadProcessService;
import com.google.inject.AbstractModule;

/**
 * @author brian
 * 
 */
public class AllProcessModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		// static availability is necessary to allow static access from map/reduce methods since
		// serialization does not allow service instance to exist within seralized workers
		requestStaticInjection(ActivityProcessService.class);
		requestStaticInjection(AccountProcessService.class);
		requestStaticInjection(RouteProcessService.class);
		requestStaticInjection(SearchProcessService.class);
		requestStaticInjection(TrailheadProcessService.class);
		requestStaticInjection(PersonProcessService.class);
		requestStaticInjection(AllProcessProviders.class);
	}

}
