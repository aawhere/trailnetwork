package com.aawhere.all.process;

import static com.aawhere.all.process.MapReduceJobSettings.*;

import java.io.Serializable;

import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.persist.Filter;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.MapReduceJob;
import com.google.appengine.tools.mapreduce.MapReduceSpecification;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.Output;
import com.google.appengine.tools.mapreduce.Reducer;

abstract public class MapReduceProcess<I, K, V, O, R>
		implements Serializable {

	private transient int inputShardCount = INPUT_SHARD_COUNT_DEFAULT;
	private transient int recuderShardCount = REDUCER_COUNT_DEFAULT;
	/**
	 * For inputs that allow filtering this provides select input items.
	 * 
	 */
	private transient Filter filter;

	/**
	 * Used to construct all instances of MapReduceProcess.
	 */
	public static class Builder<P extends MapReduceProcess<?, ?, ?, ?, ?>, B extends Builder<P, B>>
			extends AbstractObjectBuilder<P, B> {

		/** @see #filter */
		public B filter(Filter filter) {
			((MapReduceProcess<?, ?, ?, ?, ?>) building).filter = filter;
			return dis;
		}

		public Builder(P process) {
			super(process);
		}

		@Override
		public P build() {
			P built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct MapReduceProcess */
	protected MapReduceProcess() {
	}

	/**
	 * @return
	 */
	public MapReduceSpecification<I, K, V, O, R> spec() {
		MapReduceSpecification.Builder<I, K, V, O, R> specBuilder = new MapReduceSpecification.Builder<>();
		specBuilder.setInput(input());

		specBuilder.setMapper(mapper());
		specBuilder.setReducer(reducer());
		specBuilder.setOutput(output());
		specBuilder.setNumReducers(recuderShardCount);
		specBuilder.setJobName(jobName());
		final MapReduceSpecification<I, K, V, O, R> spec = specBuilder.build();
		return spec;
	}

	public String jobName() {
		return jobName(getClass());
	}

	/**
	 * @param settings
	 * @return
	 */
	public MapReduceJob<I, K, V, O, R> job(MapReduceJobSettings settings) {
		return new MapReduceJob<>(spec(), settings.settings());
	}

	public Filter filter() {
		return filter;
	}

	/**
	 * @return
	 */
	protected int inputShardCount() {
		return inputShardCount;
	}

	public static String jobName(Class<?> mapReduceProcessClass) {
		return mapReduceProcessClass.getSimpleName();
	}

	public abstract Mapper<I, K, V> mapper();

	public abstract Reducer<K, V, O> reducer();

	protected abstract Input<I> input();

	protected abstract Output<O, R> output();

	private static final long serialVersionUID = -70113036556639368L;

}
