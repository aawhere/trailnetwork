/**
 * 
 */
package com.aawhere.field;

import com.aawhere.lang.InvalidArgumentException;

import com.google.appengine.api.datastore.Entity;

/**
 * @author aroller
 * 
 */
public class FieldDatastoreUtil {

	public static Boolean hasField(Entity entity, FieldDefinition<?> definition) {
		return entity.hasProperty(definition.getColumnName());
	}

	/**
	 * @param <T>
	 *            The return type, which may be the raw form for the
	 *            {@link FieldDefinition#getType()}.
	 * @param entity
	 * @param definition
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T value(Entity entity, FieldDefinition<?> definition) {
		final String columnName = definition.getColumnName();
		if (entity.hasProperty(columnName)) {
			return (T) entity.getProperty(columnName);
		} else {
			throw InvalidArgumentException.notUnderstood(definition.getName(), columnName);
		}
	}

}
