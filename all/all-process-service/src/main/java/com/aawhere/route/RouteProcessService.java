/**
 * 
 */
package com.aawhere.route;

import javax.annotation.Nullable;

import com.aawhere.activity.ActivityProcessService;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.all.process.MapReduceJobSettings.Options;
import com.aawhere.all.process.PipelineWebApiUri;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.persist.Filter;
import com.aawhere.route.RouteTrailheadOrganizerPipeline.Builder;
import com.aawhere.trailhead.TrailheadService;
import com.aawhere.xml.UrlDisplay;
import com.google.appengine.tools.pipeline.PipelineService;
import com.google.appengine.tools.pipeline.PipelineServiceFactory;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class RouteProcessService {

	@Inject
	private static FieldDictionaryFactory fieldDictionaryFactory;
	@Inject
	public final ActivityProcessService activityProcessService;
	// static availability necessary for serializable workers
	@Inject
	private static RouteService routeService;
	@Inject
	private static RouteCompletionService routeCompletionService;
	@Inject
	private static ActivityTimingService activityTimingRelationService;
	@Inject
	private static TrailheadService trailheadService;

	@Inject
	public RouteProcessService(ActivityProcessService activityProcessService) {
		this.activityProcessService = activityProcessService;
	}

	/**
	 * Looks at activities with the goal of finding and creating new routes.
	 * 
	 * @param options
	 * 
	 * @return
	 */
	public UrlDisplay discover(Filter activityFilter, Options options) {

		PipelineService service = PipelineServiceFactory.newPipelineService();
		MapReduceJobSettings settings = MapReduceJobSettings.create().options(options).build();
		String pipelineId = service.startNewPipeline(RouteDiscoveryPipeline.create().activityFilter(activityFilter)
				.settings(settings).build(), settings.jobSettings());

		return new UrlDisplay(PipelineWebApiUri.pipelineStatus(pipelineId));
	}

	/**
	 * Kicks off the routes stale organizer if there are any stale. If not then null is returned and
	 * no process will begin.
	 * 
	 * @return
	 */
	@Nullable
	public UrlDisplay routesStaleOrganizer() {
		Filter filter = RouteDatastoreFilterBuilder.create().stale().setLimit(1).build();
		Iterable<RouteId> ids = routeService.ids(filter);
		if (!Iterables.isEmpty(ids)) {
			return routeTrailheadOrganizer(null, null);
		} else {
			return null;
		}

	}

	/**
	 * updates the routes and related trailheads matching the route filter. If not filter is
	 * provided then only stale routes are updated.
	 * 
	 * @param filter
	 * @param options
	 * @return
	 */
	public UrlDisplay routeTrailheadOrganizer(@Nullable Filter filter, @Nullable Options options) {

		PipelineService service = PipelineServiceFactory.newPipelineService();
		Builder builder = RouteTrailheadOrganizerPipeline.create();
		if (filter == null) {
			builder.stale();
		} else {
			builder.routeFilter(filter);
		}
		MapReduceJobSettings settings = new MapReduceJobSettings.Builder().options(options).build();
		builder.settings(settings);
		String pipelineId = service.startNewPipeline(builder.build(), settings.jobSettings());

		return new UrlDisplay(PipelineWebApiUri.pipelineStatus(pipelineId));
	}

	/**
	 * @return the fieldDictionaryFactory
	 */
	public static FieldDictionaryFactory fieldDictionaryFactory() {
		return fieldDictionaryFactory;
	}

	/**
	 * @return the routeService
	 */
	public static RouteService routeService() {
		return routeService;
	}

	public static RouteCompletionService routeCompletionService() {
		return routeCompletionService;
	}

	/**
	 * @return the activityTimingRelationService
	 */
	public static ActivityTimingService activityTimingRelationService() {
		return activityTimingRelationService;
	}

	/**
	 * @return the trailheadService
	 */
	public static TrailheadService trailheadService() {
		return trailheadService;
	}

}
