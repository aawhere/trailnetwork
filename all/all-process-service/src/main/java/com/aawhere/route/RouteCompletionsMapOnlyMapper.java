package com.aawhere.route;

import com.aawhere.id.IdentifierUtils;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;

/**
 * A substitute for an {@link Input} that converts the given route id into a {@link RouteCompletion}
 * .
 * 
 * @author aroller
 * 
 */
public class RouteCompletionsMapOnlyMapper
		extends MapOnlyMapper<Routes, RouteCompletion> {

	private static final long serialVersionUID = -6165253569481689744L;

	@Override
	public void map(Routes routes) {
		Iterable<RouteId> routeIds = IdentifierUtils.ids(routes);
		for (RouteId routeId : routeIds) {
			// the iterable does batch loads...I hope!
			Iterable<RouteCompletion> completions = RouteProcessService.routeCompletionService().completions(routeId);
			for (RouteCompletion routeCompletion : completions) {
				emit(routeCompletion);
			}

		}

	}

}
