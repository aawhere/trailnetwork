package com.aawhere.route;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.person.PersonId;
import com.google.appengine.tools.mapreduce.Mapper;

/**
 * Given the route completion this will build up the entire map layer scheme for the completion
 * including person, activity type & distance category. It emits a key for each spec of the layers.
 * I could emit for the general trailnetwork layer too, but currently does not because those
 * statistics are calculated prior to this step. The reducer will count each and make record of the
 * number of visits.
 * 
 * TODO: this could be used to provide the personal count in RouteCompletion.
 */
public class RouteCompletionRelativeStatisticMapper
		extends Mapper<RouteCompletion, RelativeStatistic, RouteCompletionId> {

	private static final long serialVersionUID = -2323603813239735651L;

	@Override
	public void map(RouteCompletion routeCompletionEntity) {
		ActivityType activityType = routeCompletionEntity.activityType();

		// some completions haven't yet been processed or aren't participating for other
		// reasons.
		// activity type is not assigned in such cases
		if (activityType != null && routeCompletionEntity.socialCountRank() != null) {
			RouteCompletionId routeCompletionId = routeCompletionEntity.id();
			PersonId personId = routeCompletionEntity.personId();
			RouteId routeId = routeCompletionId.routeId();
			// relative stats for routes...only personal because social routes are
			// indexed during update using completion stats
			{
				RelativeStatistic personActivityTypeStats = RelativeStatistic.create().activityType(activityType)
						.personId(personId).featureId(routeId).build();
				RelativeStatistic personStats = RelativeStatistic.create().personId(personId).featureId(routeId)
						.build();
				emit(personActivityTypeStats, routeCompletionId);
				emit(personStats, routeCompletionId);
			}

		}

	}
}
