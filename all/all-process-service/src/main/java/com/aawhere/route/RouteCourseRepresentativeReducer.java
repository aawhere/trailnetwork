package com.aawhere.route;

import com.aawhere.activity.ActivityId;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.Iterators;
import com.google.common.collect.UnmodifiableIterator;

/**
 * Organizing a stream of {@link RouteCompletions} into their {@link RouteId}, this compares all
 * {@link RouteCompletion} entities and picks the best {@link ActivityId} that
 * {@link RouteCompletion#isCourse()} to represent the group and find activities that complete the
 * route. TN-918 describes how finding timings for every course of a route is resource consuming and
 * doesn't provide a great advantage.
 * 
 * It has been shown that comparing more activities reduces routes that appear to be the same, but
 * perhaps that sort of coverage should be specifically targeted later in the processing by looking
 * for other similarities of routes.
 * 
 * @author aroller
 * 
 */
public class RouteCourseRepresentativeReducer
		extends Reducer<RouteId, RouteCompletion, ActivityId> {

	private static final long serialVersionUID = -3767481882764695069L;

	@Override
	public void reduce(RouteId key, ReducerInput<RouteCompletion> completions) {
		UnmodifiableIterator<RouteCompletion> coursesOnly = Iterators
				.filter(completions, RouteCompletionUtil.isCoursePredicate());
		// at least one is guaranteed, otherwise how did it get here?
		RouteCompletion representative = RouteCompletionUtil.higherCourseActivityCountFirst().max(coursesOnly);
		emit(representative.activityId());
	}

}
