package com.aawhere.route;

import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatsActivityStatsAggregator;
import com.aawhere.activity.RelativeStatsActivityStatsReducer;
import com.aawhere.activity.RelativeStatsCounterReducer;
import com.aawhere.activity.RelativeStatsFeatureIdMapper;
import com.aawhere.activity.RelativeStatsSearchDocumentsReducer;
import com.aawhere.activity.RelativeStatsUtil;
import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.all.process.SearchServiceOutput;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.process.mapreduce.outputs.DelegatingOutput;
import com.aawhere.search.SearchDocument;
import com.aawhere.trailhead.Trailhead;
import com.aawhere.trailhead.TrailheadId;
import com.aawhere.trailhead.TrailheadProcessService;
import com.aawhere.trailhead.TrailheadSearchDocumentProducer;
import com.google.appengine.tools.mapreduce.GoogleCloudStorageFileSet;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.MapReduceJob;
import com.google.appengine.tools.mapreduce.MapReduceResult;
import com.google.appengine.tools.mapreduce.MapReduceSpecification;
import com.google.appengine.tools.mapreduce.inputs.MapReduceInputUtil;
import com.google.appengine.tools.mapreduce.outputs.MapReduceOutputUtil;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Job1;
import com.google.appengine.tools.pipeline.Value;

/**
 * Reading {@link RouteCompletions} the {@link RelativeStatistic} for {@link Route} and
 * {@link Trailhead} are created so the number of completions can be counted for each.
 * 
 * 
 * Visual workflow
 * https://docs.google.com/a/aawhere.com/document/d/17SO2pmxjj3IAJdwfkyG-zflWXHqQ6becMVlOsDivyA0
 * /edit?usp=sharing
 * 
 * @author aroller
 * 
 */
public class TrailheadRouteCompletionStatsPipeline
		extends Job0<MapReduceResult<Void>> {

	private MapReduceJobSettings settings;
	private Input<RouteCompletion> routeCompletionInput;

	@Override
	public Value<MapReduceResult<Void>> run() throws Exception {
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> completionsToRelativeStats = futureCall(completionsToTrailheadRelativeStats(),
																										settings.jobSettings());

		FutureValue<MapReduceResult<Void>> relativeStatsActivityStats = futureCall(	relativeStatsActivtyStats(),
																					completionsToRelativeStats,
																					settings.jobSettings());
		// works from the result of the relative stats, but can't process until the trailheads are
		// updated in relativeStatsActivityStats output
		FutureValue<MapReduceResult<Void>> personalSearchDocuments = futureCall(relativeStatsPersonalSearchDocuments(),
																				completionsToRelativeStats,
																				settings.jobSettings(waitFor(relativeStatsActivityStats)));
		return personalSearchDocuments;
	}

	/**
	 * Used to construct all instances of RouteCompletionStatsPipeline.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadRouteCompletionStatsPipeline> {

		public Builder() {
			super(new TrailheadRouteCompletionStatsPipeline());
		}

		/** @see #routeCompletionInput */
		public Builder routeCompletionInput(Input<RouteCompletion> routeCompletionInput) {
			building.routeCompletionInput = routeCompletionInput;
			return this;
		}

		/** @see #settings */
		public Builder settings(MapReduceJobSettings settings) {
			building.settings = settings;
			return this;
		}

		@Override
		protected void validate() {
			Assertion.exceptions().notNull("routeCompletionInput", building.routeCompletionInput);
			super.validate();
		}

		@Override
		public TrailheadRouteCompletionStatsPipeline build() {
			TrailheadRouteCompletionStatsPipeline built = super.build();
			if (built.settings == null) {
				built.settings = MapReduceJobSettings.create().build();
			}
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Converts the route completions to {@link Trailhead} and {@link Route}
	 * {@link RelativeStatistic} and counts each category assigning the count before passing on the
	 * files.
	 * 
	 * @return
	 */
	private
			MapReduceJob<RouteCompletion, RelativeStatistic, RouteCompletionId, RelativeStatistic, GoogleCloudStorageFileSet>
			completionsToTrailheadRelativeStats() {
		String jobName = "RouteCompletionRelativeStatsCounted";
		MapReduceSpecification.Builder<RouteCompletion, RelativeStatistic, RouteCompletionId, RelativeStatistic, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
		specBuilder.setInput(routeCompletionInput);
		specBuilder.setMapper(RouteCompletionRelativeStatsMapper.trailheadMapper());
		specBuilder.setReducer(new RelativeStatsCounterReducer<RouteCompletionId>());
		specBuilder.setJobName(jobName);
		specBuilder.setOutput(MapReduceOutputUtil.fileOutput(	TrailheadRouteCompletionStatsPipeline.class,
																getPipelineKey().getName(),
																jobName,
																settings.bucketName()));
		specBuilder.setNumReducers(settings.options().reducerCount());
		MapReduceSpecification<RouteCompletion, RelativeStatistic, RouteCompletionId, RelativeStatistic, GoogleCloudStorageFileSet> activityPersonSpec = specBuilder
				.build();
		MapReduceJob<RouteCompletion, RelativeStatistic, RouteCompletionId, RelativeStatistic, GoogleCloudStorageFileSet> activityPersonJob = new MapReduceJob<>(
				activityPersonSpec, settings.settings());
		return activityPersonJob;
	}

	/**
	 * Groups the relative stats by the feature id (RouteId or TrailheadId). Once grouped during
	 * reduction they are converted to the ActivityStats
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> relativeStatsActivtyStats() {

		final String jobName = "RelativeStatsActivityStats";

		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public Value<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult)
					throws Exception {
				MapReduceSpecification.Builder<RelativeStatistic, TrailheadId, RelativeStatistic, RelativeStatsActivityStatsAggregator<TrailheadId>, Void> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<RelativeStatistic> input(fileSetResult));
				specBuilder.setMapper(RelativeStatsFeatureIdMapper.create(TrailheadId.class).build());
				specBuilder.setReducer(new RelativeStatsActivityStatsReducer<TrailheadId>());
				specBuilder.setJobName(jobName);
				DelegatingOutput<RelativeStatsActivityStatsAggregator<TrailheadId>> output = new DelegatingOutput<RelativeStatsActivityStatsAggregator<TrailheadId>>(
						TrailheadProcessService.routeCompletionStatsTrailheadUpdater());
				specBuilder.setOutput(output);
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceSpecification<RelativeStatistic, TrailheadId, RelativeStatistic, RelativeStatsActivityStatsAggregator<TrailheadId>, Void> activityPersonSpec = specBuilder
						.build();
				MapReduceJob<RelativeStatistic, TrailheadId, RelativeStatistic, RelativeStatsActivityStatsAggregator<TrailheadId>, Void> job = new MapReduceJob<>(
						activityPersonSpec, settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	private Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>
			relativeStatsPersonalSearchDocuments() {

		final String jobName = "RelativeStatsPersonalSearchDocuments";

		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public Value<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult)
					throws Exception {
				MapReduceSpecification.Builder<RelativeStatistic, TrailheadId, RelativeStatistic, SearchDocument, Void> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<RelativeStatistic> input(fileSetResult));
				specBuilder.setMapper(RelativeStatsFeatureIdMapper.create(TrailheadId.class)
						.predicate(RelativeStatsUtil.personalOnlyPredicate()).build());
				RelativeStatsSearchDocumentsReducer<TrailheadId, Trailhead> reducer = RelativeStatsSearchDocumentsReducer
						.<TrailheadId, Trailhead> create().featureProvider(TrailheadProcessService.trailheadFunction())
						.searchDocumentsFunction(TrailheadSearchDocumentProducer.personalSearchDocumentFunction())
						.build();
				specBuilder.setReducer(reducer);
				specBuilder.setJobName(jobName);

				specBuilder.setOutput(SearchServiceOutput.create().build());
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceSpecification<RelativeStatistic, TrailheadId, RelativeStatistic, SearchDocument, Void> activityPersonSpec = specBuilder
						.build();
				MapReduceJob<RelativeStatistic, TrailheadId, RelativeStatistic, SearchDocument, Void> job = new MapReduceJob<>(
						activityPersonSpec, settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/** Use {@link Builder} to construct RouteCompletionStatsPipeline */
	private TrailheadRouteCompletionStatsPipeline() {
	}

	private static final long serialVersionUID = 8966315671333432848L;
	public static final String NAME = "RouteCompletionStatsPipeline";
}
