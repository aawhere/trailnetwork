package com.aawhere.route;

import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.trailhead.TrailheadId;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;

public class TrailheadUpdatedMapOnlyMapper
		extends MapOnlyMapper<TrailheadId, Void> {

	private static final long serialVersionUID = -5739701336876052368L;

	@Override
	public void map(TrailheadId trailheadId) {
		try {
			RouteProcessService.routeService().trailheadUpdated(trailheadId);
		} catch (EntityNotFoundException e) {
			LoggerFactory.getLogger(getClass()).warning(e.getMessage());
		}
	}

}
