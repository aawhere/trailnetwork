/**
 * 
 */
package com.aawhere.route;

import javax.annotation.Nullable;

import com.aawhere.activity.ActivityType;
import com.aawhere.persist.DatastoreKeyUtil;
import com.aawhere.person.PersonId;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;

/**
 * Works with {@link Entity} to retrieve information for {@link RouteCompletion}.
 * 
 * @author aroller
 * 
 */
public class RouteCompletionDatastoreUtil {

	/**
	 * @param routeCompletionEntity
	 * @return
	 */
	public static RouteCompletionId routeCompletionId(Entity routeCompletionEntity) {
		return DatastoreKeyUtil.id(routeCompletionEntity.getKey(), RouteCompletionId.class);
	}

	/**
	 * @param routeCompletionEntity
	 * @return
	 */
	public static PersonId personId(Entity routeCompletionEntity) {
		Key personKey = (Key) routeCompletionEntity.getProperty(RouteCompletionField.PERSON_ID);
		return DatastoreKeyUtil.id(personKey, PersonId.class);
	}

	@Nullable
	public static ActivityType activityType(Entity routeCompletionEntity) {
		String value = (String) routeCompletionEntity.getProperty(RouteCompletionField.ACTIVITY_TYPE);
		if (value == null) {
			return null;
		} else {
			return ActivityType.valueOf(value);
		}
	}

	public static Integer routeScore(Entity routeCompletionEntity) {
		Long score = (Long) routeCompletionEntity.getProperty(RouteCompletionField.ROUTE_SCORE);
		if (score != null) {
			return score.intValue();
		} else {
			return Route.DEFAULT_SCORE;
		}

	}

}
