package com.aawhere.route;

import java.util.List;

import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.trailhead.TrailheadId;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;

/**
 * Right now simply calls {@link RouteService#alternatesOrganized(TrailheadId)} outputs the
 * {@link Routes} which are groups of {@link Route} entities related by {@link Route#mainId()}.
 * 
 * @author aroller
 * 
 */
public class RouteAlternatesOrganizerMapOnlyMapper
		extends MapOnlyMapper<TrailheadId, Routes> {

	private static final long serialVersionUID = -9109411859452465665L;

	@Override
	public void map(TrailheadId id) {
		try {
			// FIXME: the output should be storing, but instead the service is storing all
			AlternateRouteGroupOrganizer alternatesOrganized = RouteProcessService.routeService()
					.alternatesOrganized(id);
			List<Routes> groups = alternatesOrganized.groups();
			// FIXME:REmove this log
			LoggerFactory.getLogger(getClass()).warning(id + " found groups " + groups);
			for (Routes group : groups) {
				emit(group);
			}

		} catch (EntityNotFoundException e) {
			LoggerFactory.getLogger(getClass()).warning(e.getMessage());
		}

	}

}
