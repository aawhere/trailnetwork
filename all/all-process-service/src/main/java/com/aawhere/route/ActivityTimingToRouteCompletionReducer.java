package com.aawhere.route;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.ImmutableList;

/**
 * Converts the given timings into {@link RouteCompletion} entities and emits them. It is expected
 * that only {@link ActivityTimingRelation#isCourseCompleted()} will be true, but this protects from
 * creating {@link RouteCompletion} entities, but is a waste of time to provide them here since they
 * will be ignored.
 * 
 * The key should be one of the representations of a "route". RouteId, {@link ActivityId} of one of
 * a route's courses, Long value of the route id are all good examples.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingToRouteCompletionReducer<K>
		extends Reducer<K, ActivityTiming, RouteCompletion> {

	@Override
	public void reduce(K key, ReducerInput<ActivityTiming> timings) {

		// route will be found with the first timing...assign it so we look it up once
		Route route = null;
		while (timings.hasNext()) {

			ActivityTiming timing = timings.next();
			try {
				// typically size of 1, but an activity may complete a course twice, however, a
				// reciprocal course must complete each other exactly 1 time.
				// TODO:The timings should be put into another intermediate step
				// TODO:all courses of the same route should be compared against each other
				ImmutableList<RouteCompletion> routeCompletions = RouteProcessService.routeCompletionService()
						.routeCompletions(route, timing);
				for (RouteCompletion routeCompletion : routeCompletions) {
					// they all share the same route, so assign it here and save time looking it up
					// later
					if (route == null) {
						route = RouteProcessService.routeService().route(routeCompletion.routeId());
					}
					emit(routeCompletion);
				}
			} catch (EntityNotFoundException e) {
				// this is bad, but let's not ruin it for everyone
				LoggerFactory.getLogger(getClass()).warning(timing + " not converted to Route completion because "
						+ e.getMessage());
			}

		}
	}

	private static final long serialVersionUID = 2730286726689311319L;
}
