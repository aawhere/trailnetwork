package com.aawhere.route;

import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.trailhead.TrailheadId;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.reducers.KeyProjectionReducer;

/**
 * Updates the route and provides the trailhead ids affected by the updated route for further
 * processing. The reducer can be a {@link KeyProjectionReducer} since the {@link TrailheadId} just
 * needs to be passed on making the reducer scale down the trailhead count for uniqueness.
 * 
 * @author aroller
 * 
 */
public class RouteUpdateMapper
		extends Mapper<RouteId, TrailheadId, RouteId> {

	private static final long serialVersionUID = -6758807580928011765L;

	@Override
	public void map(RouteId routeId) {
		try {
			RouteUpdateDetails routeUpdatedDetails = RouteProcessService.routeService().routeUpdated(routeId, true);
			Route updated = routeUpdatedDetails.updated();

			// although updated shouldn't be null since the update is forced, it does come back null
			// sometimes.
			// if so then include the route with the original if it exists
			if (updated == null) {
				updated = routeUpdatedDetails.original();
			}
			if (updated != null) {
				emit(updated.start(), routeId);
				emit(updated.finish(), routeId);
			}
		} catch (EntityNotFoundException e) {
			LoggerFactory.getLogger(getClass()).warning(routeId + " is had problems updating " + e.getMessage());
		}

	}
}
