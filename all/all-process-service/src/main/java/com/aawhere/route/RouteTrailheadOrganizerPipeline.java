package com.aawhere.route;

import java.io.Serializable;

import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingCompletionReducer;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.all.process.SearchServiceOutput;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.DatastoreIdentifierInput;
import com.aawhere.persist.Filter;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.aawhere.search.SearchDocument;
import com.aawhere.trailhead.TrailheadId;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.tools.mapreduce.GoogleCloudStorageFileSet;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.MapJob;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;
import com.google.appengine.tools.mapreduce.MapReduceJob;
import com.google.appengine.tools.mapreduce.MapReduceResult;
import com.google.appengine.tools.mapreduce.MapReduceSpecification;
import com.google.appengine.tools.mapreduce.MapSpecification;
import com.google.appengine.tools.mapreduce.Output;
import com.google.appengine.tools.mapreduce.inputs.MapReduceInputUtil;
import com.google.appengine.tools.mapreduce.inputs.UnmarshallingInput;
import com.google.appengine.tools.mapreduce.outputs.MapReduceOutputUtil;
import com.google.appengine.tools.mapreduce.outputs.MarshallingOutput;
import com.google.appengine.tools.mapreduce.outputs.NoOutput;
import com.google.appengine.tools.mapreduce.reducers.KeyProjectionReducer;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Job1;
import com.google.appengine.tools.pipeline.Value;

/**
 * Using the {@link RouteService}, this replaces the legacy workflow of task queues that call to
 * update the routes and trailheads. Each job simply loads every related trailhead or route
 * datastore key and the mappers call the services to do the work as if it were the task queue.
 * 
 * The advantages being that each type is done processing before the others begin.
 * 
 * <pre>
 * TODO: Route Completions could be their own map reduce 
 * TODO: Route update could be delegated to
 * Objectify output 
 * TODO: Trailhead Organization could be reduced to a geo cell rather than each
 * trailhead. see TrailheadGeoCellMapper 
 * TODO: Alternates are a perfect example of Map Reduce. Read
 * All Routes, Extract the start, organize the result.  TN-890
 * </pre>
 * 
 * @author aroller
 * 
 */
public class RouteTrailheadOrganizerPipeline
		extends Job0<MapReduceResult<Void>> {
	private MapReduceJobSettings settings;

	private Filter routeFilter;

	/**
	 * Used to construct all instances of RouteTrailheadOrganizerPipeline.
	 */
	public static class Builder
			extends ObjectBuilder<RouteTrailheadOrganizerPipeline> {

		public Builder() {
			super(new RouteTrailheadOrganizerPipeline());
		}

		/** @see #routeFilter */
		public Builder routeFilter(Filter routeFilter) {
			building.routeFilter = routeFilter;
			return this;
		}

		public Builder stale() {
			routeFilter(RouteDatastoreFilterBuilder.create().stale().build());
			return this;
		}

		@Override
		public RouteTrailheadOrganizerPipeline build() {
			RouteTrailheadOrganizerPipeline built = super.build();
			if (built.settings == null) {
				built.settings = MapReduceJobSettings.create().build();
			}
			return built;
		}

		public Builder settings(MapReduceJobSettings settings) {
			building.settings = settings;
			return this;

		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct RouteTrailheadOrganizerPipeline */
	private RouteTrailheadOrganizerPipeline() {
	}

	@Override
	public FutureValue<MapReduceResult<Void>> run() throws Exception {
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> routeUpdatedFuture = futureCall(routesUpdatedJob(),
																								settings.jobSettings());
		// automatically waits for route update to finish to receive the trailhead ids
		// minimizes the trailheads in an area.
		// TODO: this could be streamlined by grouping into an area instead of organizing each and
		// every trailhead
		FutureValue<MapReduceResult<Void>> trailheadsOrganized = //
		futureCall(	job("TrailheadsOrganized", new RouteTrailheadOrganizerMapOnlyMapper()),
					routeUpdatedFuture,
					settings.jobSettings());
		// use the ids from the route update, but wait for the trailheads to be organized
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> alternatesOrganized = //
		futureCall(	alternateRoutesOrganizedJob(),
					routeUpdatedFuture,
					settings.jobSettings(waitFor(trailheadsOrganized)));

		// alternates provide good merge candidates since they are similar
		// ensure all courses of all alternate routes have been compared
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> alternateRouteTimingsFuture = //
		futureCall(routeTimingsDiscovered(), alternatesOrganized, settings.jobSettings());

		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> recriprocalsMatchedFuture = //
		futureCall(	RouteDiscoveryPipeline.reciprocalsMatched(settings),
					alternateRouteTimingsFuture,
					settings.jobSettings());

		// store the timings produced, but blocks noone
		futureCall(RouteDiscoveryPipeline.timingsStored(settings), recriprocalsMatchedFuture, settings.jobSettings());

		// converts the timings into route completions and stores them
		// merges are handled later when all completions are loaded
		FutureValue<MapReduceResult<Void>> alternateRouteCompletionsFuture = //
		futureCall(	RouteDiscoveryPipeline.timingAttemptsToRouteCompletions(settings),
					recriprocalsMatchedFuture,
					settings.jobSettings());

		// use the ids from the route update, but wait for the alternates to be organized since that
		// affects the score
		FutureValue<MapReduceResult<Void>> trailheadsUpdatedFuture = //
		futureCall(	job("TrailheadUpdated", new TrailheadUpdatedMapOnlyMapper()),
					routeUpdatedFuture,
					settings.jobSettings(waitFor(alternateRouteCompletionsFuture)));

		// load all the completions for the routes found during organizing so their personal
		// documents can be updated
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> routeCompletionsFuture = //
		futureCall(	routeCompletionsForRoutes(),
					alternatesOrganized,
					settings.jobSettings(waitFor(trailheadsUpdatedFuture)));

		// look for merge candidates of the completions provided.
		// They are only marked for merge here, not merged until next update
		// no output is provided
		// no need to block
		futureCall(RouteDiscoveryPipeline.routesMerged(settings, false), routeCompletionsFuture, settings.jobSettings());

		// the routes and trailheads are now up-to-date as will be the public search documents
		// triggered by the route repository
		// update the personal search documents
		FutureValue<MapReduceResult<Void>> routePersonalSearchDocsFuture = //
		futureCall(routePersonalSearchDocuments(), routeCompletionsFuture, settings.jobSettings());

		// create trailhead search documents from the same completions the personal route search
		// documents used
		// although this could process in parallel with personal Route search documents, the search
		// service is happier when not being bumrushed!
		FutureValue<MapReduceResult<Void>> trailheadPersonalSearchDocsFuture = //
		futureCall(	trailheadPersonalSearchDocuments(),
					routeCompletionsFuture,
					settings.jobSettings(waitFor(routePersonalSearchDocsFuture)));

		return trailheadPersonalSearchDocsFuture;
	}

	private Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> job(final String jobName,
			final MapOnlyMapper<TrailheadId, Void> mapper) {

		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public Value<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSet)
					throws Exception {
				MapSpecification.Builder<TrailheadId, Void, Void> specBuilder = new MapSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<TrailheadId> input(fileSet));
				specBuilder.setMapper(mapper);
				specBuilder.setOutput(new NoOutput<Void, Void>());
				specBuilder.setJobName(jobName);
				MapJob<TrailheadId, Void, Void> job = new MapJob<>(specBuilder.build(), settings.settings());
				return futureCall(job, settings.jobSettings());
			}
		};
	}

	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			alternateRoutesOrganizedJob() {

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return "RouteAlternatesOrganized";
			}

			@Override
			public Value<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSet) throws Exception {
				MapSpecification.Builder<TrailheadId, Routes, GoogleCloudStorageFileSet> specBuilder = new MapSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<TrailheadId> input(fileSet));
				specBuilder.setMapper(new RouteAlternatesOrganizerMapOnlyMapper());
				Output<? super Serializable, ? extends GoogleCloudStorageFileSet> output = MapReduceOutputUtil
						.fileOutput(RouteTrailheadOrganizerPipeline.class,
									getPipelineKey(),
									getJobDisplayName(),
									settings.bucketName());

				specBuilder.setOutput(output);
				specBuilder.setJobName(getJobDisplayName());
				MapJob<TrailheadId, Routes, GoogleCloudStorageFileSet> job = new MapJob<>(specBuilder.build(),
						settings.settings());
				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * Provided routes that are related (alternates) this will discover all timings related to all
	 * courses of routes in the same {@link Route#mainId()} group. TN-907
	 * 
	 * This is very similar to {@link RouteDiscoveryPipeline#timingAttemptsProcessed()}, but uses a
	 * more targeted approach to discovering related timings of courses of related routes.
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			routeTimingsDiscovered() {

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return "RouteTimingsDiscovered";
			}

			@Override
			public Value<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {

				MapReduceSpecification.Builder<Routes, ActivityTimingId, ActivityTiming, ActivityTiming, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<Routes> input(fileSetResult));
				specBuilder.setMapper(new RouteTimingsDiscoveryMapper());
				specBuilder.setReducer(new ActivityTimingCompletionReducer());
				specBuilder.setJobName(getJobDisplayName());
				MarshallingOutput<Serializable, GoogleCloudStorageFileSet> output = MapReduceOutputUtil
						.fileOutput(RouteTrailheadOrganizerPipeline.class,
									getPipelineKey(),
									getJobDisplayName(),
									settings.bucketName());
				specBuilder.setOutput(output);
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<Routes, ActivityTimingId, ActivityTiming, ActivityTiming, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * Updates the routes using the RouteService update process. The routes selected are controlled
	 * by a route filter. Common filters would be route-stale=true, route-startArea-cells=abcde.
	 * 
	 * The result is to pass on the trailheads affected by the route updates so those trailheads can
	 * also be updated. The trailhead ids are provided as file set.
	 * 
	 * @return
	 */
	private Job0<MapReduceResult<GoogleCloudStorageFileSet>> routesUpdatedJob() {
		return new Job0<MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = -3554565127574068443L;

			final String jobName = "RoutesUpdated";

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> run() throws Exception {

				MapReduceSpecification.Builder<RouteId, TrailheadId, RouteId, TrailheadId, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();

				specBuilder.setJobName(jobName);

				specBuilder.setInput(routeIdInput());
				specBuilder.setMapper(new RouteUpdateMapper());
				specBuilder.setReducer(new KeyProjectionReducer<TrailheadId, RouteId>());
				MarshallingOutput<Serializable, GoogleCloudStorageFileSet> output = MapReduceOutputUtil
						.fileOutput(RouteTrailheadOrganizerPipeline.class,
									getPipelineKey(),
									jobName,
									settings.bucketName());
				specBuilder.setOutput(output);
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<RouteId, TrailheadId, RouteId, TrailheadId, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());
				return futureCall(job, settings.jobSettings());
			}

		};

	}

	/**
	 * Given a fileset of Route Ids this will provide the completions for each.
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			routeCompletionsForRoutes() {
		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = -3554565127574068443L;

			@Override
			public String getJobDisplayName() {
				return "RouteCompletionsForRoutes";
			}

			@Override
			public FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSet) throws Exception {

				MapSpecification.Builder<Routes, RouteCompletion, GoogleCloudStorageFileSet> specBuilder = new MapSpecification.Builder<>();

				specBuilder.setJobName(getJobDisplayName());
				UnmarshallingInput<Routes> routeCompletionForRouteInput = MapReduceInputUtil.input(fileSet);
				specBuilder.setInput(routeCompletionForRouteInput);
				specBuilder.setMapper(new RouteCompletionsMapOnlyMapper());
				MarshallingOutput<Serializable, GoogleCloudStorageFileSet> fileOutput = MapReduceOutputUtil
						.fileOutput(RouteTrailheadOrganizerPipeline.class,
									getPipelineKey(),
									getJobDisplayName(),
									settings.bucketName());
				specBuilder.setOutput(fileOutput);
				MapJob<Routes, RouteCompletion, GoogleCloudStorageFileSet> job = new MapJob<>(specBuilder.build(),
						settings.settings());
				return futureCall(job, settings.jobSettings());
			}

		};

	}

	/**
	 * Given a fileset of route ids from a previous job this loads all the completions for each
	 * route, mapping each to their personal {@link RelativeStatistic} so the reducer can count how
	 * many personal completions there are for each route. Although this information is part of the
	 * completion, it's not available for activity type and the distance categories so the relative
	 * stat is needed.
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> routePersonalSearchDocuments() {
		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = -3554565127574068443L;

			final String jobName = "PersonalRouteCompletionsSearchDocs";

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public FutureValue<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSet)
					throws Exception {

				MapReduceSpecification.Builder<RouteCompletion, RelativeStatistic, RouteCompletionId, SearchDocument, Void> specBuilder = new MapReduceSpecification.Builder<>();

				specBuilder.setJobName(jobName);
				UnmarshallingInput<RouteCompletion> routeCompletionForRouteInput = MapReduceInputUtil.input(fileSet);
				specBuilder.setInput(routeCompletionForRouteInput);
				specBuilder.setMapper(new RouteCompletionRelativeStatisticMapper());
				specBuilder.setReducer(new RouteCompletionsRelativeStatisticSearchDocumentReducer());
				specBuilder.setOutput(SearchServiceOutput.create().build());
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<RouteCompletion, RelativeStatistic, RouteCompletionId, SearchDocument, Void> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());
				return futureCall(job, settings.jobSettings());
			}

		};

	}

	/**
	 * Kicks off the trailhead pipeline using the route completions resulting from earlier in the
	 * pipeline.
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> trailheadPersonalSearchDocuments() {
		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = -35545651274068443L;

			final String jobName = "PersonalTrailheadCompletionsSearchDocs";

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public FutureValue<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSet)
					throws Exception {

				UnmarshallingInput<RouteCompletion> input = MapReduceInputUtil.input(fileSet);
				TrailheadRouteCompletionStatsPipeline pipeline = TrailheadRouteCompletionStatsPipeline.create()
						.settings(settings).routeCompletionInput(input).build();
				return futureCall(pipeline, settings.jobSettings());
			}

		};

	}

	private Input<RouteId> routeIdInput() {
		// TN-756 Objectify 5 will produce a valid datastore query
		// FilterPredicates are not supported call Query.setFiler() instead.
		// Query query = ActivityProcessService.datastoreQuery(activityFilter);
		// add supported filters if conditions are provided
		Query query = new Query(ObjectifyKeyUtils.getKind(Route.class));

		return new DatastoreIdentifierInput<Route, RouteId>(query, settings.options().inputCount(), RouteId.class);
	}

	private static final long serialVersionUID = 8345780083135318871L;
}
