/**
 * 
 */
package com.aawhere.route;

import java.io.Serializable;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityField;
import com.aawhere.activity.ActivityGeoCellsCourseCandidateReducer;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityProcessService;
import com.aawhere.activity.CourseCandidate;
import com.aawhere.activity.CourseCandidateActivityIdMapper;
import com.aawhere.activity.CourseCandidateCourseIdMapper;
import com.aawhere.activity.CourseCandidateReciprocalTimingMapper;
import com.aawhere.activity.CourseCandidateTimingReciprocalGroupReducer;
import com.aawhere.activity.CourseCandidateTimingReducer;
import com.aawhere.activity.ReciprocalTiming;
import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingCompletionReducer;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingReciprocalKey;
import com.aawhere.activity.timing.ActivityTimingReciprocalReducer;
import com.aawhere.activity.timing.ActivityTimingRelationUtil;
import com.aawhere.activity.timing.CourseCompletionTimingDiscoveryMapper;
import com.aawhere.activity.timing.ReciprocalGroupId;
import com.aawhere.all.process.FunctionIdentityMapper;
import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.all.process.PassThroughMapper;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.Filter;
import com.aawhere.persist.ObjectifyInputBuilder;
import com.aawhere.trailhead.TrailheadActivityStartsPipeline;
import com.aawhere.trailhead.TrailheadNamePipeline;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.tools.mapreduce.GoogleCloudStorageFileSet;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.KeyValue;
import com.google.appengine.tools.mapreduce.MapJob;
import com.google.appengine.tools.mapreduce.MapReduceJob;
import com.google.appengine.tools.mapreduce.MapReduceResult;
import com.google.appengine.tools.mapreduce.MapReduceSpecification;
import com.google.appengine.tools.mapreduce.MapSpecification;
import com.google.appengine.tools.mapreduce.Output;
import com.google.appengine.tools.mapreduce.inputs.MapReduceInputUtil;
import com.google.appengine.tools.mapreduce.mappers.IdentityMapper;
import com.google.appengine.tools.mapreduce.outputs.MapReduceOutputUtil;
import com.google.appengine.tools.mapreduce.outputs.MarshallingOutput;
import com.google.appengine.tools.mapreduce.outputs.NoOutput;
import com.google.appengine.tools.mapreduce.outputs.ObjectifyOutput;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Job1;
import com.google.appengine.tools.pipeline.JobSetting;
import com.google.appengine.tools.pipeline.Value;

/**
 * The primary way to discover routes from activities. It starts by associating all activities that
 * share the same geocells into groups and inspects each to see if they complete each other
 * (reciprocals). All of those that are reciprocals are passed on as a group to create or associate
 * with a route...each being registered as a route completion that is a course representing the
 * route. With the route created the activities are searched for those candidates that may complete
 * the course minus those timings that have already been processed in the past. All timings are
 * stored and passing timings go on to be registered as route completions.
 * 
 * TODO:Look for merges since completions often associate two routes that should be one
 * 
 * TODO:Invoke the route update of pending routes since this will surely make some stale
 * 
 * @author aroller
 * 
 */
public class RouteDiscoveryPipeline
		extends Job0<MapReduceResult<Void>>
		implements Serializable {

	public static final String JOB_NAME = "RouteDiscovery";
	private Class<? extends RouteDiscoveryPipeline> pipelineClass = getClass();

	private Filter activityFilter;

	/**
	 * Used to construct all instances of CourseCandidateRoutePipeline.
	 */
	public static class Builder
			extends ObjectBuilder<RouteDiscoveryPipeline> {

		public Builder() {
			super(new RouteDiscoveryPipeline());
		}

		/** @see #settings */
		public Builder settings(MapReduceJobSettings settings) {
			building.settings = settings;
			return this;
		}

		@Override
		public RouteDiscoveryPipeline build() {
			RouteDiscoveryPipeline built = super.build();
			if (built.settings == null) {
				built.settings = MapReduceJobSettings.create().build();
			}
			if (built.activityFilter == null) {
				built.activityFilter = Filter.create().build();
			}
			built.jobSettings = built.settings.jobSettings();
			return built;
		}

		/** @see #activityFilter */
		public Builder activityFilter(Filter activityFilter) {
			building.activityFilter = activityFilter;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct CourseCandidateRoutePipeline */
	private RouteDiscoveryPipeline() {
	}

	private MapReduceJobSettings settings;
	private JobSetting[] jobSettings;

	private static final long serialVersionUID = -4538113867341147861L;

	@Override
	public Value<MapReduceResult<Void>> run() throws Exception {

		Query query = activityQuery();

		// account id counting messes up public trailheads.
		// re-add this when counter can know not to
		Boolean accountFiltered = activityFilter.containsCondition(ActivityField.ACCOUNT_ID);
		// TN-911 avoid stats if only filtered by account
		// area filters are fine, but other limiting filters will cause problems too
		if (!accountFiltered) {
			// independently counts the number of activities starting at trailheads. this will
			// create trailheads that will be organized later
			FutureValue<MapReduceResult<Void>> trailheadStartsFuture = futureCall(	activityTrailheadStartsJob(query),
																					jobSettings);
			// after the starts are counted, name the trailheads
			// FIXME:these two pipelines use the same activities. One input for both Mappers?
			TrailheadNamePipeline trailheadNamePipeline = TrailheadNamePipeline.create().filter(activityFilter)
					.settings(settings).build();
			futureCall(trailheadNamePipeline, waitFor(trailheadStartsFuture));

		}
		// load the activities, map them to their spatial buffer geocells and convert into
		// CourseCandidate
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> activities = //
		futureCall(activityGeoCellsCourseCandidateJob(query), jobSettings);

		// course candidates are distributed and the timings determined and reciprocals passed on
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> courseCandidateTimingReciprocalFuture = //
		futureCall(courseCandidateTimingReciprocal(), activities, settings.jobSettings());

		// reciprocals come in with their pre-determined group, but are split into more specific
		// groups
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> courseCandidateTimingReciprocalGroupsFuture = //
		futureCall(	courseCandidateTimingReciprocalGroups(),
					courseCandidateTimingReciprocalFuture,
					settings.jobSettings());

		// creates the Route COmpletions for all the courses found in the same reciprocal group
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> courseRouteCompletions = //
		futureCall(courseRouteCompletions(), courseCandidateTimingReciprocalGroupsFuture, jobSettings);

		// store the results of the route completions
		// no future variable assigned since this runs parallel and feeds/blocks noone
		futureCall(routeCompletionsStored(), courseRouteCompletions, settings.jobSettings());

		// look for potential merges from those activities that are courses in multiple routes
		futureCall(routesMerged(settings, false), courseRouteCompletions, settings.jobSettings());

		// pick a course representative from each route to find activity completions
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> routeCourseRepresentatives = //
		futureCall(routeCourseRepresenative(), courseRouteCompletions, jobSettings);

		// process attempts that may complete the single course representing a route
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> timingAttemptsFuture = //
		futureCall(timingAttemptsProcessed(), routeCourseRepresentatives, settings.jobSettings());

		// those timing attempts could have resulted in reciprocals...if so then update and include
		// in storage the reciprocals also selectively chooses who is worth passing on
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> staleTimings = //
		futureCall(reciprocalsMatched(settings), timingAttemptsFuture, settings.jobSettings());

		// store the timings produced, but blocks noone
		futureCall(timingsStored(settings), staleTimings, settings.jobSettings());

		// create and store the route completions for the passing timings
		// notice this is in parallel with timings store
		FutureValue<MapReduceResult<Void>> timingAttemptsToRouteCompletionsFuture = //
		futureCall(timingAttemptsToRouteCompletions(settings), staleTimings, settings.jobSettings());

		// all of these routes that were created or received completions are now stale...update them
		RouteTrailheadOrganizerPipeline routeTrailheadOrganizerPipeline = RouteTrailheadOrganizerPipeline.create()
				.settings(settings).stale().build();
		FutureValue<MapReduceResult<Void>> routesUpdatedFuture = //

		futureCall(	routeTrailheadOrganizerPipeline,
					settings.jobSettings(waitFor(timingAttemptsToRouteCompletionsFuture)));
		return routesUpdatedFuture;
	}

	/**
	 * Output the geocells from the activities matching the {@link #activityFilter}. The activities
	 * are then reduced to match the same geocells and a group id is produced to represent the group
	 * for later association and the reciprocals are provided as a {@link CourseCandidate}.
	 * 
	 * @return
	 */
	private Job0<MapReduceResult<GoogleCloudStorageFileSet>> activityGeoCellsCourseCandidateJob(final Query query) {
		return new Job0<MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = -3554565127574068443L;

			final String jobName = "ActivityGeoCellsCourseCandidate";

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> run() throws Exception {

				MapReduceSpecification.Builder<Activity, String, ActivityId, CourseCandidate, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();

				specBuilder.setJobName(jobName);
				Input<Activity> input = ObjectifyInputBuilder.create(Activity.class).query(query).settings(settings)
						.build().input();

				specBuilder.setInput(input);
				specBuilder.setMapper(new CourseCandidateActivityIdMapper());
				specBuilder.setReducer(new ActivityGeoCellsCourseCandidateReducer());
				specBuilder.setOutput(output(jobName, getPipelineKey()));
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<Activity, String, ActivityId, CourseCandidate, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());
				return futureCall(job, settings.jobSettings());
			}

		};

	}

	public Query activityQuery() {
		Query query = ActivityProcessService.datastoreQuery(activityFilter);
		return query;
	}

	/**
	 * Inspecting the activities limited by the filter this will update the number of activities
	 * starting from a trailhead. The trailheads will be created if not yet done. This is handy
	 * earlier than later since the trailheads will be organized and likely merge those that deserve
	 * to be closer.
	 * 
	 * @return
	 */
	private Job0<MapReduceResult<Void>> activityTrailheadStartsJob(Query query) {
		Input<Activity> input = ObjectifyInputBuilder.create(Activity.class).query(query).settings(settings).build()
				.input();

		return TrailheadActivityStartsPipeline.create().input(input).settings(settings).build();
	}

	/**
	 * Given the {@link CourseCandidate} this will create two {@link ActivityTimingRelation}
	 * representing the success/failure of each to complete each other. Grouped by the id in the
	 * CourseCandidate the resulting passing timings will make RouteCompletions.
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			courseCandidateTimingReciprocal() {

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return "CourseCandidateReciprocalTimings";
			}

			@Override
			public Value<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {

				// TODO:This can be reduced by pre-loading those timings already processed
				// the pre-check will be worth it when we can get failures too
				MapReduceSpecification.Builder<CourseCandidate, ActivityTimingId, CourseCandidate, ReciprocalTiming, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<CourseCandidate> input(fileSetResult));
				specBuilder.setMapper(new CourseCandidateCourseIdMapper());
				specBuilder.setReducer(new CourseCandidateTimingReducer());
				specBuilder.setJobName(getJobDisplayName());
				specBuilder.setOutput(output(getJobDisplayName(), getPipelineKey()));
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<CourseCandidate, ActivityTimingId, CourseCandidate, ReciprocalTiming, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			courseCandidateTimingReciprocalGroups() {

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return "CourseCandidateTimingReciprocalGroups";
			}

			@Override
			public Value<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {
				MapReduceSpecification.Builder<KeyValue<ReciprocalGroupId, ActivityTiming>, ReciprocalGroupId, ActivityTiming, ReciprocalTiming, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<ReciprocalTiming> input(fileSetResult));
				specBuilder.setMapper(new IdentityMapper<ReciprocalGroupId, ActivityTiming>());
				specBuilder.setReducer(new CourseCandidateTimingReciprocalGroupReducer());
				specBuilder.setJobName(getJobDisplayName());
				specBuilder.setOutput(output(getJobDisplayName(), getPipelineKey()));
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<KeyValue<ReciprocalGroupId, ActivityTiming>, ReciprocalGroupId, ActivityTiming, ReciprocalTiming, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * Receives the {@link ReciprocalTiming} objects in the files from
	 * {@link #courseCandidateTimingReciprocal()} and converts each to a {@link RouteCompletion}
	 * associated to the same route for the same {@link ReciprocalGroupId}.
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			courseRouteCompletions() {

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return "CourseRouteCompletions";
			}

			@Override
			public Value<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {

				MapReduceSpecification.Builder<ReciprocalTiming, ReciprocalGroupId, ActivityTiming, RouteCompletion, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<ReciprocalTiming> input(fileSetResult));

				specBuilder.setMapper(new CourseCandidateReciprocalTimingMapper());
				specBuilder.setReducer(new ActivityTimingToRouteCompletionReducer<ReciprocalGroupId>());
				specBuilder.setJobName(getJobDisplayName());
				specBuilder.setOutput(output(getJobDisplayName(), getPipelineKey()));
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<ReciprocalTiming, ReciprocalGroupId, ActivityTiming, RouteCompletion, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	private Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> routeCompletionsStored() {

		final String jobName = "RouteCompletionsStored";

		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public Value<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult)
					throws Exception {
				MapSpecification.Builder<RouteCompletion, RouteCompletion, Void> specBuilder = new MapSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<RouteCompletion> input(fileSetResult));
				specBuilder.setMapper(PassThroughMapper.<RouteCompletion> create().build());
				specBuilder.setJobName(jobName);
				specBuilder.setOutput(new ObjectifyOutput<RouteCompletion>());
				MapJob<RouteCompletion, RouteCompletion, Void> job = new MapJob<>(specBuilder.build(),
						settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * Handling the results of the route completions, this looks for Activities that are courses for
	 * more than one route...if found the two routes will flagged to be merged into one Route during
	 * organization.
	 * 
	 * @return
	 */
	static Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>> routesMerged(
			final MapReduceJobSettings settings, final Boolean emitFiles) {

		final String jobName = "RouteMergeDetector";

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public Value<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {

				MapReduceSpecification.Builder<RouteCompletion, ActivityId, RouteCompletion, RouteCompletion, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<RouteCompletion> input(fileSetResult));
				specBuilder.setMapper(new RouteCompletionCourseIdMapper());
				// FIXME: nobody is using the output from this and it is a passthrough so don't emit
				// for anyone?
				RouteCompletionMergeReducer.Builder merger = RouteCompletionMergeReducer.create();
				if (BooleanUtils.isFalse(emitFiles)) {
					merger.noOutput();
				}
				specBuilder.setReducer(merger.build());
				specBuilder.setJobName(jobName);
				if (emitFiles) {
					MarshallingOutput<Serializable, GoogleCloudStorageFileSet> output = MapReduceOutputUtil
							.fileOutput(RouteDiscoveryPipeline.class, getPipelineKey(), jobName, settings.bucketName());
					specBuilder.setOutput(output);
				} else {
					specBuilder.setOutput(new NoOutput<RouteCompletion, GoogleCloudStorageFileSet>());
				}
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<RouteCompletion, ActivityId, RouteCompletion, RouteCompletion, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * TN-918 Shows that too many timings are attempted to be found during the first discovery which
	 * can take a lot of time for not a lot of value since an activity only needs to complete one of
	 * the courses of a Route. Completing multiple helps select the representing course,but its a
	 * bonus not a necessity. This picks a representative for the route to go on and find
	 * completions for the route.
	 * 
	 * First all completions from the input are grouped by the route id, then the route completions
	 * provide the best representative for a the route to be passed on to discover timings that may
	 * complete the representing course.
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			routeCourseRepresenative() {

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 50625018094880774L;

			@Override
			public String getJobDisplayName() {
				return "RouteCourseRepresentative";
			}

			@Override
			public Value<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {

				MapReduceSpecification.Builder<RouteCompletion, RouteId, RouteCompletion, ActivityId, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<RouteCompletion> input(fileSetResult));
				specBuilder.setMapper(FunctionIdentityMapper.<RouteId, RouteCompletion> create()
						.function(RouteCompletionUtil.routeIdFunction()).build());
				specBuilder.setReducer(new RouteCourseRepresentativeReducer());
				specBuilder.setJobName(getJobDisplayName());
				specBuilder.setOutput(output(getJobDisplayName(), getPipelineKey()));
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<RouteCompletion, RouteId, RouteCompletion, ActivityId, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());
				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * Courses have been discovered...discover Activities (attempts) that may have completed the
	 * course. Process timings and pass the results on as files so processing may continue to work
	 * with the results (storing them, creating route completions for successes, etc).
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			timingAttemptsProcessed() {

		final String jobName = "CourseCompletionTimings";

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 50625018094880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public Value<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {

				MapReduceSpecification.Builder<ActivityId, ActivityTimingId, ActivityTiming, ActivityTiming, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<ActivityId> input(fileSetResult));
				specBuilder.setMapper(new CourseCompletionTimingDiscoveryMapper());
				specBuilder.setReducer(new ActivityTimingCompletionReducer());
				specBuilder.setJobName(jobName);
				specBuilder.setOutput(output(jobName, getPipelineKey()));
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<ActivityId, ActivityTimingId, ActivityTiming, ActivityTiming, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());
				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * Simple persistence of the timings produced during {@link #timingAttemptsProcessed()}.
	 * 
	 * @return
	 */
	static Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> timingsStored(
			final MapReduceJobSettings settings) {

		final String jobName = "TimingsStored";

		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 506250180904880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public Value<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult)
					throws Exception {
				MapSpecification.Builder<ActivityTiming, ActivityTiming, Void> specBuilder = new MapSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<ActivityTiming> input(fileSetResult));
				// all timings must be stored.
				specBuilder.setMapper(PassThroughMapper.<ActivityTiming> create().build());
				specBuilder.setJobName(jobName);
				specBuilder.setOutput(new ObjectifyOutput<ActivityTiming>());
				MapJob<ActivityTiming, ActivityTiming, Void> job = new MapJob<>(specBuilder.build(),
						settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * Receives incoming {@link ActivityTimingRelation} entities, maps them by the reciprocal key so
	 * it may determine if the timings are reciprocals. Emits only those timings that need to be
	 * stored as a timing or a completion.
	 * 
	 * @return
	 */
	static Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			reciprocalsMatched(final MapReduceJobSettings settings) {

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 506250180904880774L;

			@Override
			public String getJobDisplayName() {
				return "ReciprocalsMatched";
			}

			@Override
			public Value<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {
				MapReduceSpecification.Builder<ActivityTiming, ActivityTimingReciprocalKey, ActivityTiming, ActivityTiming, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<ActivityTiming> input(fileSetResult));

				specBuilder.setMapper(FunctionIdentityMapper.<ActivityTimingReciprocalKey, ActivityTiming> create()
						.function(ActivityTimingReciprocalKey.keyFunction()).build());

				specBuilder.setReducer(new ActivityTimingReciprocalReducer());
				specBuilder.setJobName(getJobDisplayName());
				MarshallingOutput<Serializable, GoogleCloudStorageFileSet> fileOutput = MapReduceOutputUtil
						.fileOutput(RouteDiscoveryPipeline.class,
									getPipelineKey(),
									getJobDisplayName(),
									settings.bucketName());
				specBuilder.setOutput(fileOutput);
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<ActivityTiming, ActivityTimingReciprocalKey, ActivityTiming, ActivityTiming, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());
				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * The timings that passed (completed a course) now must be converted to RouteCompletions. Each
	 * course was associated to a {@link Route} during {@link #courseCandidateTimingReciprocals()}
	 * so this will find those again and associate the timings with the route. The timings are
	 * stored so this is a leaf of the pipeline tree (or end cap in plumbing terminology).
	 * 
	 * @return
	 */
	static Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> timingAttemptsToRouteCompletions(
			final MapReduceJobSettings settings) {

		final String jobName = "TimingsToRouteCompletions";

		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 50625018094880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public Value<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult)
					throws Exception {

				MapReduceSpecification.Builder<ActivityTiming, ActivityId, ActivityTiming, RouteCompletion, Void> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil.<ActivityTiming> input(fileSetResult));
				// map the timing to it's course id for grouped reduction
				// only pass on the passers since RouteCompletions doesn't care about failers
				specBuilder.setMapper(FunctionIdentityMapper.<ActivityId, ActivityTiming> create()
						.function(ActivityTimingRelationUtil.courseIdFromTimingFunction())
						.filter(ActivityTimingRelationUtil.courseCompletedPredicate()).build());

				specBuilder.setReducer(new ActivityTimingToRouteCompletionReducer<ActivityId>());
				specBuilder.setJobName(jobName);
				specBuilder.setOutput(new ObjectifyOutput<RouteCompletion>());
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceJob<ActivityTiming, ActivityId, ActivityTiming, RouteCompletion, Void> job = new MapReduceJob<>(
						specBuilder.build(), settings.settings());
				return futureCall(job, settings.jobSettings());
			}
		};
	}

	private Output<Serializable, ? extends GoogleCloudStorageFileSet> output(String jobName, Key pipelineKey) {
		MarshallingOutput<Serializable, GoogleCloudStorageFileSet> output = MapReduceOutputUtil
				.fileOutput(pipelineClass, pipelineKey.getName(), jobName, settings.bucketName());
		return output;
	}
}
