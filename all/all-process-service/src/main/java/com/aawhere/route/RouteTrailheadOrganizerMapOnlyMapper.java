package com.aawhere.route;

import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.trailhead.TrailheadId;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;

/**
 * Simply calls {@link RouteService#trailheadUpdated(TrailheadId)}
 * 
 * @author aroller
 * 
 */
public class RouteTrailheadOrganizerMapOnlyMapper
		extends MapOnlyMapper<TrailheadId, Void> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6860937985575908112L;

	@Override
	public void map(TrailheadId trailheadId) {
		try {
			RouteProcessService.routeService().trailheadsOrganized(trailheadId);
		} catch (EntityNotFoundException e) {
			LoggerFactory.getLogger(getClass()).warning(e.getMessage());
		}
	}

}
