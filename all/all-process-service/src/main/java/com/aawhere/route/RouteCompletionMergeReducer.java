package com.aawhere.route;

import java.util.HashSet;

import com.aawhere.activity.ActivityId;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;

/**
 * {@link RouteCompletions} with {@link RouteCompletion#isCourse()} as true that have the same
 * {@link RouteCompletion#activityId()}, but different {@link RouteCompletion#routeId()} must merge
 * the two routes into one.
 * 
 * The result is a {@link RouteCompletionId} for all the routes that have the activity as a course.
 * Even though they are marked for merge, they are not yet merged so each route is emitted leaving
 * it up to a consumer to decide what, if anything, should be done with duplicates.
 * 
 * @author aroller
 * 
 */
public class RouteCompletionMergeReducer
		extends Reducer<ActivityId, RouteCompletion, RouteCompletion> {

	private Boolean emitResult = true;

	/**
	 * Used to construct all instances of RouteCompletionMergeReducer.
	 */
	public static class Builder
			extends ObjectBuilder<RouteCompletionMergeReducer> {

		public Builder() {
			super(new RouteCompletionMergeReducer());
		}

		public Builder noOutput() {
			building.emitResult = false;
			return this;
		}

		@Override
		public RouteCompletionMergeReducer build() {
			RouteCompletionMergeReducer built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct RouteCompletionMergeReducer */
	private RouteCompletionMergeReducer() {
	}

	private static final long serialVersionUID = 1502284236901390085L;

	@Override
	public void reduce(ActivityId activityId, ReducerInput<RouteCompletion> completions) {
		HashSet<RouteId> routeIds = new HashSet<RouteId>();
		while (completions.hasNext()) {
			RouteCompletion completion = completions.next();
			if (completion.isCourse()) {
				routeIds.add(completion.routeId());
			}
			if (emitResult) {
				// emit all completions for any use downstream
				emit(completion);
			}
		}
		// look for merges...courses found in multiple routes means those routes represent the same
		if (routeIds.size() > 1) {
			LoggerFactory.getLogger(getClass()).info(activityId + " found as courses in these routes to merge: "
					+ routeIds);
			RouteProcessService.routeService().markedForMerge(routeIds);
		}
	}

}
