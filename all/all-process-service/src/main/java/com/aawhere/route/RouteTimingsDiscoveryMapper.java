package com.aawhere.route;

import java.util.Set;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.timing.ActivityTimingCompletionReducer;
import com.aawhere.activity.timing.ActivityTimingDiscoveryMapper;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingRelationUtil;
import com.aawhere.log.LoggerFactory;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

/**
 * Receives routes that have already been confirmed to be related at some level (like part of the
 * same {@link Route#mainId()} group). This will compare all courses from all routes against each
 * other creating many possible timing matches. This also {@link #emitExistingTimings(ActivityId)}
 * so that {@link ActivityTimingCompletionReducer} may efficiently reduce new timings into
 * {@link ActivityTimingRelation} entities.
 * 
 * TN-907 introduced the need for this because similar routes remain indpendent, but part of the
 * same mainId group
 * 
 * @author aroller
 * 
 */
public class RouteTimingsDiscoveryMapper
		extends ActivityTimingDiscoveryMapper<Routes> {

	private static final long serialVersionUID = 4235733103845898218L;

	@Override
	public void map(Routes routes) {
		// here we are only looking for matches across groups, not within a route itself...that
		// should already have been done
		if (routes.size() > 1) {
			// TN-907 explains the exploding discovery when grabbing every course from every route
			// grab only the representing course from each route "may" provide enough coverage, but
			// will avoid exploding into too many results

			Set<ActivityId> courseIds = ImmutableSet.copyOf(RouteUtil.courseIds(routes));
			Iterable<ActivityTimingId> timingIds = ActivityTimingRelationUtil.timingIds(courseIds);
			LoggerFactory.getLogger(getClass()).warning(Iterables.toString(routes) + " looking for matches from "
					+ Iterables.toString(timingIds));
			for (ActivityTimingId activityTimingRelationId : timingIds) {
				emit(activityTimingRelationId);
			}

			// now find those that exist to avoid re-processing
			// nothing before guarantees activiy id is unique so do it now
			for (ActivityId activityId : courseIds) {
				emitExistingTimings(activityId);
			}
		}
	}
}
