package com.aawhere.route;

import java.util.logging.Logger;

import com.aawhere.activity.RelativeStatistic;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocuments;
import com.aawhere.trailhead.Trailhead;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.Iterators;

/**
 * Reducing the relative statistic to group the completions by the same criteria, this counts the
 * number of completions for that statistic and produces search documents. This currently works only
 * with personal stats, but it could be extended easily to count the totals for non-personals.
 * 
 * @author aroller
 * 
 */
public class RouteCompletionsRelativeStatisticSearchDocumentReducer
		extends Reducer<RelativeStatistic, RouteCompletionId, SearchDocument> {

	private static final long serialVersionUID = 2909637406609036554L;

	@Override
	public void reduce(RelativeStatistic relativeStatistic, ReducerInput<RouteCompletionId> routeCompletionIds) {
		int numberOfCompletions = Iterators.size(routeCompletionIds);
		// first, update the count of the relative stats by counting the number of completion ids
		// associated with the relative stat
		relativeStatistic = RelativeStatistic.clone(relativeStatistic).count(numberOfCompletions).build();
		final RouteService routeService = RouteProcessService.routeService();
		final RelativeStatistic incoming = relativeStatistic;

		RouteId routeId = RouteSearchUtil.routeId(relativeStatistic.id());
		try {
			final Route route = routeService.route(routeId);

			RouteSearchDocumentProducer.Builder searchDocumentBuilder = RouteSearchDocumentProducer.create();

			searchDocumentBuilder.personalStats(incoming);
			searchDocumentBuilder.route(route);

			try {
				Trailhead start = routeService.trailhead(route);
				searchDocumentBuilder.startTrailhead(start);
			} catch (EntityNotFoundException e) {
				LoggerFactory.getLogger(getClass()).warning(route + " provided a bad trailhead " + e.getMessage());
				// continue on...trailhead is optional
			}

			SearchDocuments searchDocuments = searchDocumentBuilder.build().searchDocuments();
			for (SearchDocument searchDocument : searchDocuments) {
				emit(searchDocument);
			}

			// TODO:This could be used to provide personal ranks also
			// TODO: this could provide a personal name for a route if the name of the
			// activity was included
		} catch (EntityNotFoundException e) {
			LoggerFactory.getLogger(getClass()).warning(relativeStatistic + " produced a bad route id "
					+ e.getMessage());
			routeService.routeDeleted(routeId);
		} catch (Exception e) {
			Logger logger = LoggerFactory.getLogger(getClass());
			logger.severe(relativeStatistic + " was skipped due to " + e.getMessage());
			logger.throwing(getClass().getName(), "reduce", e);
		}
	}

}
