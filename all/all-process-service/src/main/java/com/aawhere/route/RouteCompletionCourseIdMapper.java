package com.aawhere.route;

import com.aawhere.activity.ActivityId;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.reducers.KeyProjectionReducer;

/**
 * Used to find all routes that have an {@link ActivityId} as it's course. This can be used to
 * gather unique course ids or find routes that should be merged together (if an activity id is a
 * course in more than one route).
 * 
 * @see KeyProjectionReducer
 * 
 * @author aroller
 * 
 */
public class RouteCompletionCourseIdMapper
		extends Mapper<RouteCompletion, ActivityId, RouteCompletion> {

	@Override
	public void map(RouteCompletion routeCompletion) {
		emit(routeCompletion.activityId(), routeCompletion);
	}

	private static final long serialVersionUID = 8391721029210655300L;
}
