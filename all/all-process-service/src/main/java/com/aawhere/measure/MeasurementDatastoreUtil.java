/**
 * 
 */
package com.aawhere.measure;

import com.aawhere.field.FieldDatastoreUtil;
import com.aawhere.field.FieldDefinition;

import com.google.appengine.api.datastore.Entity;

/**
 * @author aroller
 * 
 */
public class MeasurementDatastoreUtil {

	public static Latitude latitude(Entity entity, FieldDefinition<Latitude> definition) {
		return MeasurementUtil.createLatitude((Double) FieldDatastoreUtil.value(entity, definition));
	}

	public static Longitude longitude(Entity entity, FieldDefinition<Longitude> definition) {
		return MeasurementUtil.createLongitude((Double) FieldDatastoreUtil.value(entity, definition));
	}

	public static GeoCoordinate geoCoordinate(Entity entity, FieldDefinition<Latitude> latitudeDefinition,
			FieldDefinition<Longitude> longitudeDefinition) {
		Latitude latitude = latitude(entity, latitudeDefinition);
		Longitude longitude = longitude(entity, longitudeDefinition);
		GeoCoordinate geoCoordinate = GeoCoordinate.create().setLatitude(latitude).setLongitude(longitude).build();
		return geoCoordinate;

	}
}
