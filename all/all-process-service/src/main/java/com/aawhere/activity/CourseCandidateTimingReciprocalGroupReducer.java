package com.aawhere.activity;

import java.util.Collection;
import java.util.Map.Entry;

import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingRelationUtil;
import com.aawhere.activity.timing.ReciprocalGroupId;
import com.aawhere.measure.geocell.GeoCells;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

/**
 * Receiving the Timings resulting from the {@link CourseCandidateTimingMapper}, this will group all
 * of the recripocal timings into {@link ActivityTimingRelationUtil#reciprocalGroups(Iterable)}
 * since not every timing will be part of the same group just because they came from the same
 * {@link GeoCells}. Therefore, the {@link GeoCells} are ignored and dropped in this reduction.
 * 
 * TN-910 exposed the problem when grouping by geocells without separation
 * 
 * @see CourseCandidateTimingMapper
 * @author aroller
 * 
 */
public class CourseCandidateTimingReciprocalGroupReducer
		extends Reducer<ReciprocalGroupId, ActivityTiming, ReciprocalTiming> {

	private static final long serialVersionUID = -450545376718763460L;

	@Override
	public void reduce(ReciprocalGroupId key, ReducerInput<ActivityTiming> timings) {

		// notice the incoming key is ignored and replaced with teh one from the results of the
		// utility. The key is necessary to group all together to get here in the first place

		// TN-910 Not all of the timings in the same cell group are reciprocals of the same course
		ImmutableSetMultimap<ReciprocalGroupId, ActivityTiming> reciprocalGroups = ActivityTimingRelationUtil
				.reciprocalGroups(ImmutableSet.copyOf(timings));
		ImmutableSet<Entry<ReciprocalGroupId, Collection<ActivityTiming>>> entrySet = reciprocalGroups.asMap()
				.entrySet();
		// emit each timing with it's reciprocal id so it can be regrouped later
		for (Entry<ReciprocalGroupId, Collection<ActivityTiming>> entry : entrySet) {
			ReciprocalGroupId reciprocalGroupId = entry.getKey();
			for (ActivityTiming timing : entry.getValue()) {
				emit(new ReciprocalTiming(reciprocalGroupId, timing));
			}
		}
	}

}
