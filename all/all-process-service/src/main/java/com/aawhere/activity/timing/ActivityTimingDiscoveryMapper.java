package com.aawhere.activity.timing;

import com.aawhere.activity.ActivityId;
import com.aawhere.route.RouteProcessService;
import com.google.appengine.tools.mapreduce.Mapper;

abstract public class ActivityTimingDiscoveryMapper<I>
		extends Mapper<I, ActivityTimingId, ActivityTiming> {

	private static final long serialVersionUID = -4466087494698023623L;

	protected void emitExistingTimings(ActivityId courseId) {
		// emit all timings that have been processed to avoid processing again
		Iterable<ActivityTiming> timings = RouteProcessService.activityTimingRelationService()
				.timingsForCourse(courseId);
		for (ActivityTiming existingTiming : timings) {
			emit(existingTiming.id(), existingTiming);
		}
	}

	protected void emit(ActivityTimingId timingId) {
		if (!ActivityTimingRelationUtil.selfTiming(timingId)) {
			// this is just a stub to indicate the desire for the timing
			ActivityTiming timing = ActivityTiming.create().notYetDecided(timingId).build();
			// perhaps emitting null would suffice?
			emit(timing.id(), timing);
		}
	}
}
