package com.aawhere.activity.timing;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityUtil;
import com.aawhere.route.RouteProcessService;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.ImmutableList;

/**
 * A reducer who's sole purpose is to find or process a timing if none can be found, and emit it's
 * result so it may create Route completions if completed and go to persistence in every case
 * 
 * This is setup to work with the results of {@link CourseCompletionTimingDiscoveryMapper}. It may
 * receive multiple timings, but it will reduce down to the one already persisted since that has
 * already been process or it will create one by running {@link ActivityTimingRelationUserStory}.
 * Only one timing for each id will be emitted.
 * 
 * @see CourseCompletionTimingDiscoveryMapper
 * 
 * @author aroller
 * 
 */
public class ActivityTimingCompletionReducer
		extends Reducer<ActivityTimingId, ActivityTiming, ActivityTiming> {

	private static final long serialVersionUID = 1907234979871234987L;

	@Override
	public void reduce(final ActivityTimingId timing, ReducerInput<ActivityTiming> timings) {
		// there will only be a few so no worries about loading
		ActivityTiming processedTiming = processed(timing, ImmutableList.<ActivityTiming> builder().addAll(timings)
				.build());
		emit(processedTiming);
	}

	/**
	 * Does all the work finding or creating the timing that is processed and answers
	 * {@link ActivityTimingRelation#courseCompleted()}.
	 * 
	 * @param timing
	 * @param types
	 * @return
	 */
	ActivityTiming processed(final ActivityTimingId timingId, ImmutableList<ActivityTiming> timings) {

		ActivityTiming processedTiming = EntityUtil.persisted(timings);
		if (processedTiming == null) {

			ActivityTimingId key = timingId;
			try {
				ActivityTimingRelationUserStory timingUserStory = RouteProcessService.activityTimingRelationService()
						.timingUserStory(key);
				processedTiming = timingUserStory.getResult();
			} catch (EntityNotFoundException e) {
				throw new RuntimeException(key + " produced " + e.getMessage());
			}

		}
		return processedTiming;
	}
}
