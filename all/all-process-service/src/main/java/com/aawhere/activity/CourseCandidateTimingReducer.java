package com.aawhere.activity;

import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingReciprocalProcessor;
import com.aawhere.activity.timing.ActivityTimingRelationUtil;
import com.aawhere.activity.timing.ReciprocalGroupId;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.Iterators;

/**
 * Course Candidates have been found from previous spatial organization. Here the candidate is
 * converted from the timing id to it's two timings and emitted if the relationship is proven to be
 * reciprocal.
 * 
 * @author aroller
 * 
 */
public class CourseCandidateTimingReducer
		extends Reducer<ActivityTimingId, CourseCandidate, ReciprocalTiming> {

	private static final long serialVersionUID = 1346744025689574685L;

	@Override
	public void reduce(ActivityTimingId key, ReducerInput<CourseCandidate> courseCandidates) {
		CourseCandidate courseCandidate = Iterators.getOnlyElement(courseCandidates);
		try {

			// TODO:move the persistence from the service to here for bulk writing

			// timings are persisted here...regardless of completion or reciprocal success
			ActivityTimingReciprocalProcessor processor = ActivityProcessService.activityTimingRelationService()
					.reciprocalsProcessed(courseCandidate.getValue());

			Boolean reciprocals = ActivityTimingRelationUtil.reciprocals(processor.timing(), processor.reciprocal());

			if (reciprocals) {
				// TN-910 keep all passing reciprocals as part of the same geocell group for now
				// the reducer must receive all in the group and separate then

				ReciprocalGroupId reciprocalGroupId = new ReciprocalGroupId(courseCandidate.getKey());
				emit(new ReciprocalTiming(reciprocalGroupId, processor.timing()));
				emit(new ReciprocalTiming(reciprocalGroupId, processor.reciprocal()));
			}
		} catch (EntityNotFoundException e) {
			LoggerFactory.getLogger(getClass()).warning(courseCandidate + " provided a bad activity id "
					+ e.getMessage());
		}
	}

}
