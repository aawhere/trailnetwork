/**
 * 
 */
package com.aawhere.activity.timing;

import com.aawhere.all.process.AllProcessService;
import com.aawhere.persist.Filter;
import com.aawhere.xml.UrlDisplay;
import com.google.appengine.api.datastore.Query;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Runs Map Reduce Jobs for Activity Timing relationships.
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class ActivityTimingProcessService {

	private ActivityTimingRelationObjectifyRepository timingRepository;
	private AllProcessService allProcessService;

	@Inject
	public ActivityTimingProcessService(ActivityTimingRelationObjectifyRepository repository,
			AllProcessService allProcessService) {
		this.timingRepository = repository;
		this.allProcessService = allProcessService;
	}

	/**
	 * TN-875 touches only succeeding timings
	 * 
	 * @return
	 */
	public UrlDisplay touchActivityTimingRelation(Filter filter) {
		Query query = timingRepository.datastoreQuery(filter);
		return allProcessService.touch(query, ActivityTiming.class);
	}

}
