/**
 * 
 */
package com.aawhere.activity;

import java.io.Serializable;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.field.FieldDefinition;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.FieldPropertyEntityUtil;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.Iterators;

/**
 * Counts the number of {@link Activities} for each {@link Application}.
 * 
 * @author aroller
 * 
 */
public class ActivityApplicationCounterMapReduce
		implements Serializable {

	private static final long serialVersionUID = -2204378158935312270L;

	private FieldDefinition<Integer> activityCountDefinition;

	/**
	 * Used to construct all instances of ActivityApplicationCounterMapReduce.
	 */
	public static class Builder
			extends ObjectBuilder<ActivityApplicationCounterMapReduce> {

		public Builder() {
			super(new ActivityApplicationCounterMapReduce());
		}

		public Builder activityCountDefinition(FieldDefinition<Integer> activityCountDefinition) {
			building.activityCountDefinition = activityCountDefinition;
			return this;
		}

		@Override
		public ActivityApplicationCounterMapReduce build() {
			ActivityApplicationCounterMapReduce built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityApplicationCounterMapReduce */
	private ActivityApplicationCounterMapReduce() {
	}

	/**
	 * A {@link Mapper} that maps the given {@link ActivityId} into {@link ApplicationKey}
	 * 
	 * @return
	 */
	public Mapper<Key, ApplicationKey, ActivityId> applicationKeyMapper() {
		return new Mapper<Key, ApplicationKey, ActivityId>() {

			private static final long serialVersionUID = 2654410824238052892L;

			@Override
			public void map(Key key) {
				ActivityId activityId = ActivityDatastoreUtil.activityId(key);
				ApplicationKey applicationKey = activityId.getApplicationKey();
				emit(applicationKey, activityId);
			}

		};
	}

	/**
	 * Given the applicationKey for a particular ActivityId this will produce the count for each.
	 * 
	 * @return
	 */
	public Reducer<ApplicationKey, ActivityId, Entity> applicationActivityCountReducer() {
		return new Reducer<ApplicationKey, ActivityId, Entity>() {

			private static final long serialVersionUID = 2627203335836747131L;

			@Override
			public void reduce(ApplicationKey key, ReducerInput<ActivityId> values) {
				int size = Iterators.size(values);
				Entity entity = FieldPropertyEntityUtil.entity(key, activityCountDefinition, size);
				emit(entity);
			}
		};
	}
}
