/**
 * 
 */
package com.aawhere.activity;

import java.io.Serializable;

import com.aawhere.app.ApplicationKey;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.tools.mapreduce.Counter;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;
import com.google.appengine.tools.mapreduce.ShardContext;
import com.googlecode.objectify.util.DatastoreUtils;

/**
 * @author aroller
 * 
 */
public class ActivityProcessUtil
		implements Serializable {

	/**
	 * 
	 */

	private static final long serialVersionUID = 4141693154043972751L;

	public static MapOnlyMapper<Entity, ApplicationKey> applicationKeyMapOnlyMapper() {
		return new MapOnlyMapper<Entity, ApplicationKey>() {

			private static final long serialVersionUID = 2654410824238052892L;

			@Override
			public void map(Entity activity) {
				ApplicationKey applicationKey = applicationKey(activity);
				emit(applicationKey);
			}

		};
	}

	/**
	 * @param activity
	 * @return
	 */
	public static ApplicationKey applicationKey(Entity activity) {
		final Key key = activity.getKey();
		String id = DatastoreUtils.getId(key);
		ApplicationKey applicationKey = new ActivityId(id).getApplicationKey();
		return applicationKey;
	}

	/**
	 * @param applicationKey
	 * @param context
	 * @return
	 */
	private static Counter applicationCounter(ApplicationKey applicationKey, final ShardContext context) {
		return context.getCounter(applicationKey.getValue());
	}

}
