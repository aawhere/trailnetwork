package com.aawhere.activity.timing;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityProcessService;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.route.RouteProcessService;

/**
 * Expands the given {@link ActivityId}, which represents a course, into all
 * {@link ActivityTimingRelationId} identifiers that may possibly complete the course. It also
 * discovers any timing that may already exist so a timing won't be processed again.
 * 
 * @author aroller
 * 
 */
public class CourseCompletionTimingDiscoveryMapper
		extends ActivityTimingDiscoveryMapper<ActivityId> {

	@Override
	public void map(ActivityId courseId) {
		try {
			Activity course = ActivityProcessService.activityService().activity(courseId);
			Iterable<ActivityId> attemptIdsForCourse = RouteProcessService.activityTimingRelationService()
					.attemptCandidateIdsForCourse(course);
			for (ActivityId attemptId : attemptIdsForCourse) {
				emit(new ActivityTimingId(courseId, attemptId));
			}
			emitExistingTimings(courseId);
		} catch (EntityNotFoundException e) {
			throw new RuntimeException(courseId + " course given, but not found" + e.getMessage());
		}
	}

	private static final long serialVersionUID = 1982734987234L;
}
