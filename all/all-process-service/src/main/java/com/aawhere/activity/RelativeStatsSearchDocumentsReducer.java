package com.aawhere.activity;

import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocuments;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.base.Function;

/**
 * Reducing relative stats to it's common featureId <I>, presumably from
 * {@link RelativeStatsFeatureIdMapper}, this loads the feature from the provided
 * {@link #featureProvider} and provides the feature along with the {@link RelativeStatistic}
 * resulting in {@link SearchDocuments} transformed by {@link #searchDocumentsFunction}.
 * 
 * @author aroller
 * 
 * @param <I>
 * @param <E>
 */
public class RelativeStatsSearchDocumentsReducer<I, E>
		extends Reducer<I, RelativeStatistic, SearchDocument> {

	private Function<I, E> featureProvider;
	private Function<Map.Entry<E, RelativeStatistic>, SearchDocument> searchDocumentsFunction;

	/**
	 * Used to construct all instances of RelativeStatsSearchDocumentsReducer.
	 */
	public static class Builder<I, E>
			extends ObjectBuilder<RelativeStatsSearchDocumentsReducer<I, E>> {

		public Builder() {
			super(new RelativeStatsSearchDocumentsReducer<I, E>());
		}

		/** @see #featureProvider */
		public Builder<I, E> featureProvider(Function<I, E> featureProvider) {
			building.featureProvider = featureProvider;
			return this;
		}

		public Builder<I, E> searchDocumentsFunction(
				Function<Map.Entry<E, RelativeStatistic>, SearchDocument> searchDocumentsFunction) {
			building.searchDocumentsFunction = searchDocumentsFunction;
			return this;
		}

		@Override
		protected void validate() {
			Assertion.exceptions().notNull("featureProvider", building.featureProvider);
			Assertion.exceptions().notNull("searchDocumentsFunction", building.searchDocumentsFunction);
			super.validate();
		}

		@Override
		public RelativeStatsSearchDocumentsReducer<I, E> build() {
			RelativeStatsSearchDocumentsReducer<I, E> built = super.build();
			return built;
		}

	}// end Builder

	public static <I, E> Builder<I, E> create() {
		return new Builder<I, E>();
	}

	/** Use {@link Builder} to construct RelativeStatsSearchDocumentsReducer */
	private RelativeStatsSearchDocumentsReducer() {
	}

	@Override
	public void reduce(I key, ReducerInput<RelativeStatistic> relativeStats) {
		E feature = this.featureProvider.apply(key);
		if (feature != null) {
			while (relativeStats.hasNext()) {
				RelativeStatistic relativeStatistic = relativeStats.next();
				Pair<E, RelativeStatistic> pair = Pair.of(feature, relativeStatistic);
				SearchDocument searchDocument = searchDocumentsFunction.apply(pair);
				if (searchDocument != null) {
					emit(searchDocument);
				}
			}
		}

	}

	private static final long serialVersionUID = 85476148457828090L;
}
