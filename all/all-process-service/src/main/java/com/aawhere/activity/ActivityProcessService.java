/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.all.process.AllProcessService;
import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.app.ApplicationField;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.account.AccountId;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.persist.Filter;
import com.aawhere.person.Persons;
import com.aawhere.xml.UrlDisplay;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.tools.mapreduce.MapReduceJob;
import com.google.appengine.tools.mapreduce.MapReduceSpecification;
import com.google.appengine.tools.mapreduce.inputs.DatastoreInput;
import com.google.appengine.tools.mapreduce.inputs.DatastoreKeyInput;
import com.google.appengine.tools.mapreduce.outputs.DatastoreOutput;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Runs {@link MapReduceJob}s to produce information about {@link Activity} relationships with
 * {@link Activities} and {@link Persons}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityProcessService {

	@Inject
	private static FieldDictionaryFactory fieldDictionaryFactory;
	/**
	 * Static service available for mappers to access extra information, but they cannot handle the
	 * injection since they must be serializable.
	 * 
	 */
	@Inject
	private static ActivityService activityService;

	@Inject
	private static ActivityObjectifyRepository activityRepository;

	@Inject
	private static ActivityTrackGeometryService activityTrackGeometryService;
	@Inject
	private static ActivityTimingService activityTimingRelationService;

	@Inject
	private static AllProcessService allProcessService;

	/**
	 * 
	 */
	public ActivityProcessService() {

	}

	/** Iterating through all activities this counts the activity for each Application. */
	public String activitiesPerApplication() {
		MapReduceSpecification.Builder<Key, ApplicationKey, ActivityId, Entity, Void> specBuilder = new MapReduceSpecification.Builder<Key, ApplicationKey, ActivityId, Entity, Void>();
		specBuilder.setInput(new DatastoreKeyInput(Activity.class.getSimpleName(),
				MapReduceJobSettings.INPUT_SHARD_COUNT_DEFAULT));
		FieldDefinition<Integer> fieldDefinition = fieldDictionaryFactory
				.getDefinition(ApplicationField.Key.ACTIVITY_COUNT);
		ActivityApplicationCounterMapReduce applicationCounterMapReduce = ActivityApplicationCounterMapReduce.create()
				.activityCountDefinition(fieldDefinition).build();

		specBuilder.setMapper(applicationCounterMapReduce.applicationKeyMapper());
		specBuilder.setReducer(applicationCounterMapReduce.applicationActivityCountReducer());
		specBuilder.setOutput(new DatastoreOutput());
		// just trying 2 out, no particular reason
		specBuilder.setNumReducers(MapReduceJobSettings.REDUCER_COUNT_DEFAULT);
		specBuilder.setJobName(ApplicationField.ACTIVITY_COUNT);

		return MapReduceJob.start(specBuilder.build(), MapReduceJobSettings.create().build().settings());
	}

	public String activitiesPerAccount() {
		// FIXME:a pattern is emerging...create a base MapReduce class.
		MapReduceSpecification.Builder<Entity, AccountId, ActivityId, Entity, Void> specBuilder = new MapReduceSpecification.Builder<Entity, AccountId, ActivityId, Entity, Void>();
		specBuilder.setInput(new DatastoreInput(Activity.class.getSimpleName(),
				MapReduceJobSettings.INPUT_SHARD_COUNT_DEFAULT));
		ActivitiesPerAccountMapReduce mapReduce = ActivitiesPerAccountMapReduce.create().build();
		specBuilder.setMapper(mapReduce.mapper());
		specBuilder.setReducer(mapReduce.reducer());
		specBuilder.setNumReducers(MapReduceJobSettings.REDUCER_COUNT_DEFAULT);
		specBuilder.setOutput(new DatastoreOutput());
		specBuilder.setJobName(mapReduce.jobName());
		final MapReduceSpecification<Entity, AccountId, ActivityId, Entity, Void> spec = specBuilder.build();
		return MapReduceJob.start(spec, MapReduceJobSettings.create().build().settings());
	}

	/**
	 * Counts the number of activities that start in a geocell.
	 * 
	 * @return
	 */
	public String activitiesPerGeoCell() {
		// FIXME:a pattern is emerging...create a base MapReduce class.
		MapReduceSpecification.Builder<Entity, GeoCell, ActivityId, Entity, Void> specBuilder = new MapReduceSpecification.Builder<Entity, GeoCell, ActivityId, Entity, Void>();
		specBuilder.setInput(new DatastoreInput(Activity.class.getSimpleName(),
				MapReduceJobSettings.INPUT_SHARD_COUNT_DEFAULT));
		GeoCellActivityCountMapReduce mapReduce = GeoCellActivityCountMapReduce.create()
				.fieldDictionaryFactory(fieldDictionaryFactory).build();
		specBuilder.setMapper(mapReduce.mapper());
		specBuilder.setReducer(mapReduce.reducer());
		specBuilder.setNumReducers(MapReduceJobSettings.REDUCER_COUNT_DEFAULT);
		specBuilder.setOutput(new DatastoreOutput());
		specBuilder.setJobName(mapReduce.jobName());
		return MapReduceJob.start(specBuilder.build(), MapReduceJobSettings.create().build().settings());
	}

	/**
	 * @return the fieldDictionaryFactory
	 */
	public static FieldDictionaryFactory fieldDictionaryFactory() {
		return fieldDictionaryFactory;
	}

	/**
	 * @return the activityService
	 */
	public static ActivityService activityService() {
		return activityService;
	}

	/**
	 * @return the activityTrackGeometryService
	 */
	public static ActivityTrackGeometryService activityTrackGeometryService() {
		return activityTrackGeometryService;
	}

	/**
	 * @return the activityTimingRelationService
	 */
	public static ActivityTimingService activityTimingRelationService() {
		return activityTimingRelationService;
	}

	public static Query datastoreQuery(Filter activityFilter) {
		return activityRepository.datastoreQuery(activityFilter);
	}

	/**
	 * Touches (loads/saves) all entities matching the filter.
	 * 
	 * @param filter
	 * @return
	 */
	public UrlDisplay touch(Filter filter) {
		Query query = datastoreQuery(filter);
		return allProcessService.touch(query, Activity.class);
	}

}
