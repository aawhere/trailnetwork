/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.all.process.EntityMapReduceProcess;
import com.aawhere.app.account.AccountField;
import com.aawhere.app.account.AccountId;
import com.aawhere.field.FieldDefinition;
import com.aawhere.persist.FieldPropertyEntityUtil;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.Iterators;

/**
 * @author aroller
 * 
 */
public class ActivitiesPerAccountMapReduce
		extends EntityMapReduceProcess<AccountId, ActivityId> {

	private FieldDefinition<Integer> accountActivityCountDefinition;
	private FieldDefinition<AccountId> activityAccountidDefinition;

	/**
	 * Used to construct all instances of ActivitiesPerAccountMapReduce.
	 */
	public static class Builder
			extends EntityMapReduceProcess.Builder<ActivitiesPerAccountMapReduce, AccountId, ActivityId, Builder> {

		public Builder() {
			super(new ActivitiesPerAccountMapReduce());
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();

		}

		@Override
		public ActivitiesPerAccountMapReduce build() {
			ActivitiesPerAccountMapReduce built = super.build();
			built.accountActivityCountDefinition = ActivityProcessService.fieldDictionaryFactory()
					.getDefinition(AccountField.Key.ACTIVITY_COUNT);
			built.activityAccountidDefinition = ActivityProcessService.fieldDictionaryFactory()
					.getDefinition(ActivityField.Key.ACCOUNT_ID);
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivitiesPerAccountMapReduce */
	private ActivitiesPerAccountMapReduce() {
	}

	@Override
	public Mapper<Entity, AccountId, ActivityId> mapper() {
		return new Mapper<Entity, AccountId, ActivityId>() {

			private static final long serialVersionUID = 6008158504055047256L;

			@Override
			public void map(Entity entity) {
				ActivityId activityId = ActivityDatastoreUtil.activityId(entity);
				AccountId accountId = ActivityDatastoreUtil.accountId(entity, activityAccountidDefinition);
				emit(accountId, activityId);
			}

		};
	}

	@Override
	public Reducer<AccountId, ActivityId, Entity> reducer() {
		return new Reducer<AccountId, ActivityId, Entity>() {

			private static final long serialVersionUID = 975252159783220553L;

			@Override
			public void reduce(AccountId key, ReducerInput<ActivityId> values) {
				emit(FieldPropertyEntityUtil.entity(key, accountActivityCountDefinition, Iterators.size(values)));
			}
		};
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2973997952999504272L;

	/**
	 * @return
	 */
	@Override
	public String jobName() {
		return this.accountActivityCountDefinition.getKey().getAbsoluteKey();
	}

	/*
	 * @see com.aawhere.all.process.EntityMapReduceProcess#inputEntityKind()
	 */
	@Override
	public String inputEntityKind() {
		return ActivityDatastoreUtil.activityKind();
	}

}
