/**
 * 
 */
package com.aawhere.activity;

import java.util.List;

import javax.measure.quantity.Length;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.Unit;

import com.aawhere.all.ActivityNestedField;
import com.aawhere.all.RouteNestedField;
import com.aawhere.app.account.AccountDatastoreUtil;
import com.aawhere.app.account.AccountId;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.MeasurementDatastoreUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.DatastoreKeyUtil;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.googlecode.objectify.util.DatastoreUtils;

/**
 * @author aroller
 * 
 */
public class ActivityDatastoreUtil {

	public static ActivityId activityId(Key key) {
		String id = DatastoreUtils.getId(key);
		return new ActivityId(id);
	}

	/**
	 * @param entity
	 * @return
	 */
	public static ActivityId activityId(Entity entity) {
		return activityId(entity.getKey());
	}

	/**
	 * @param entity
	 * @return
	 */
	public static AccountId accountId(Entity entity, FieldDefinition<AccountId> accountIdDef) {
		return AccountDatastoreUtil.accountId((Key) entity.getProperty(accountIdDef.getColumnName()));
	}

	/**
	 * @param activityEntity
	 * @param fieldDictionaryFactory
	 * @return
	 */
	public static List<String> spatialBufferGeoCellValuesList(Entity activityEntity,
			FieldDictionaryFactory fieldDictionaryFactory) {
		FieldDefinition<GeoCells> definition = fieldDictionaryFactory
				.getDefinition(RouteNestedField.ACTIVITY__TRACK__SPATIAL_BUFFER__CELLS);
		return spatialBufferGeoCellValuesList(activityEntity, definition);
	}

	/**
	 * @param activityEntity
	 * @param definition
	 * @return
	 */
	public static List<String> spatialBufferGeoCellValuesList(Entity activityEntity,
			FieldDefinition<GeoCells> definition) {
		@SuppressWarnings("unchecked")
		List<String> cells = (List<String>) activityEntity.getProperty(definition.getColumnName());
		return cells;
	}

	/**
	 * @param activityEntity
	 * @param fieldDictionaryFactory
	 * @return
	 */
	public static GeoCoordinate start(Entity activityEntity, FieldDictionaryFactory fieldDictionaryFactory) {
		FieldDefinition<Latitude> latitudeDefinition = fieldDictionaryFactory
				.getDefinition(com.aawhere.all.RouteNestedField.ACTIVITY__TRACK__START__LOCATION__LATITUDE);
		FieldDefinition<Longitude> longitudeDefinition = fieldDictionaryFactory
				.getDefinition(RouteNestedField.ACTIVITY__TRACK__START__LOCATION__LONGITUDE);
		GeoCoordinate geoCoordinate = MeasurementDatastoreUtil.geoCoordinate(	activityEntity,
																				latitudeDefinition,
																				longitudeDefinition);
		return geoCoordinate;
	}

	public static GeoCoordinate finish(Entity activityEntity, FieldDictionaryFactory fieldDictionaryFactory) {
		FieldDefinition<Latitude> latitudeDefinition = fieldDictionaryFactory
				.getDefinition(RouteNestedField.ACTIVITY__TRACK__FINISH__LOCATION__LATITUDE);
		FieldDefinition<Longitude> longitudeDefinition = fieldDictionaryFactory
				.getDefinition(RouteNestedField.ACTIVITY__TRACK__FINISH__LOCATION__LONGITUDE);
		return MeasurementDatastoreUtil.geoCoordinate(activityEntity, latitudeDefinition, longitudeDefinition);
	}

	/**
	 * @return
	 */
	public static String activityKind() {
		return ObjectifyKeyUtils.getKind(Activity.class);
	}

	/**
	 * @param activityEntity
	 * @param fieldDictionaryFactory
	 * @return
	 */
	public static AccountId accountId(Entity activityEntity, FieldDictionaryFactory fieldDictionaryFactory) {
		FieldDefinition<Object> definition = fieldDictionaryFactory.getDefinition(ActivityField.Key.ACCOUNT_ID);
		return DatastoreKeyUtil.id((Key) activityEntity.getProperty(definition.getColumnName()), AccountId.class);
	}

	/**
	 * @param activityEntity
	 * @return
	 */
	public static ActivityType activityType(Entity activityEntity) {
		return ActivityType.valueOf((String) activityEntity.getProperty(ActivityField.ACTIVITY_TYPE));
	}

	public static Length distance(Entity activityEntity, FieldDictionaryFactory fieldDictionaryFactory) {
		final Unit<Length> normalizedUnit = MeasurementUtil.getNormalizedUnit(Length.class);
		FieldDefinition<Length> distanceDefinition;
		distanceDefinition = fieldDictionaryFactory.getDefinition(RouteNestedField.ACTIVITY__TRACK__DISTANCE);
		Double value = (Double) activityEntity.getProperty(distanceDefinition.getColumnName());
		return QuantityFactory.getInstance(Length.class).create(value, normalizedUnit);

	}
}
