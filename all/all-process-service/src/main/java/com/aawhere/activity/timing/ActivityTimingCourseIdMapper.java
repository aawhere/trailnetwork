package com.aawhere.activity.timing;

import com.aawhere.activity.ActivityId;
import com.google.appengine.tools.mapreduce.Mapper;

public class ActivityTimingCourseIdMapper
		extends Mapper<ActivityTiming, ActivityId, ActivityTiming> {

	@Override
	public void map(ActivityTiming timing) {
		emit(timing.id().getCourseId(), timing);

	}

	private static final long serialVersionUID = 8200609735178907059L;
}
