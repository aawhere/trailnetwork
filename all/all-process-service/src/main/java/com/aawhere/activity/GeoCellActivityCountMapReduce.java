/**
 * 
 */
package com.aawhere.activity;

import java.io.Serializable;

import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.geo.GeoCellSummaryField;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.FieldPropertyEntityUtil;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.Iterators;

/**
 * Reduces the {@link Activity} into a {@link GeoCell} where it starts.
 * 
 * @author aroller
 * 
 */
public class GeoCellActivityCountMapReduce
		implements Serializable {

	private Resolution resolution;
	public FieldDefinition<Integer> activityStartCount;

	/**
	 * Used to construct all instances of ActivitiesPerGeoCellMapReduce.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCellActivityCountMapReduce> {
		private FieldDictionaryFactory fieldDictionaryFactory;

		public Builder() {
			super(new GeoCellActivityCountMapReduce());
		}

		public Builder resolution(Resolution resolution) {
			building.resolution = resolution;
			return this;
		}

		public Builder fieldDictionaryFactory(FieldDictionaryFactory fieldDictionaryFactory) {
			this.fieldDictionaryFactory = fieldDictionaryFactory;
			return this;
		}

		@Override
		public GeoCellActivityCountMapReduce build() {
			GeoCellActivityCountMapReduce built = super.build();
			if (built.resolution == null) {
				built.resolution = Resolution.FIVE;
			}

			built.activityStartCount = fieldDictionaryFactory
					.getDefinition(GeoCellSummaryField.Key.ACTIVITY_START_COUNT);
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivitiesPerGeoCellMapReduce */
	private GeoCellActivityCountMapReduce() {
	}

	public Mapper<Entity, GeoCell, ActivityId> mapper() {
		return new Mapper<Entity, GeoCell, ActivityId>() {
			private static final long serialVersionUID = -1208362991568169041L;

			@Override
			public void map(Entity entity) {
				GeoCoordinate geoCoordinate = ActivityDatastoreUtil.start(entity, ActivityProcessService
						.fieldDictionaryFactory());
				GeoCell geoCell = GeoCellUtil.geoCell(geoCoordinate, resolution);
				emit(geoCell, ActivityDatastoreUtil.activityId(entity));
			}
		};
	}

	public Reducer<GeoCell, ActivityId, Entity> reducer() {
		return new Reducer<GeoCell, ActivityId, Entity>() {

			private static final long serialVersionUID = -8475214245960632703L;

			@Override
			public void reduce(GeoCell key, ReducerInput<ActivityId> values) {
				Entity entity = FieldPropertyEntityUtil.entity(key, activityStartCount, Iterators.size(values));
				emit(entity);
			}
		};
	}

	private static final long serialVersionUID = 8772831109505661148L;

	/**
	 * @return
	 */
	public String jobName() {
		return activityStartCount.getKey().getAbsoluteKey();
	}
}
