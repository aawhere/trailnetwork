package com.aawhere.persist;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import com.aawhere.id.Identifier;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.InputReader;
import com.google.appengine.tools.mapreduce.ShardContext;
import com.google.appengine.tools.mapreduce.inputs.DatastoreKeyInput;
import com.google.appengine.tools.mapreduce.inputs.DatastoreKeyInputReader;
import com.google.appengine.tools.mapreduce.inputs.ForwardingInputReader;
import com.google.common.collect.ImmutableList;

/**
 * Delegates to {@link DatastoreKeyInput} and {@link DatastoreKeyInputReader} translating the
 * resulting {@link Key} into and {@link Identifier}.
 * 
 * @author aroller
 * 
 * @param <E>
 * @param <I>
 */
public class DatastoreIdentifierInput<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends Input<I> {

	private final DatastoreKeyInput keyInput;
	private final Class<I> idType;

	public DatastoreIdentifierInput(Query query, int shardCount, Class<I> idType) {
		this.keyInput = new DatastoreKeyInput(query, shardCount);
		this.idType = idType;
	}

	@Override
	public List<? extends InputReader<I>> createReaders() throws IOException {
		List<InputReader<Key>> keyReaders = this.keyInput.createReaders();
		ImmutableList.Builder<InputReader<I>> readers = ImmutableList.builder();
		for (InputReader<Key> keyReader : keyReaders) {
			InputReader<I> reader = new DatastoreIdentifierInputReader<>(keyReader, idType);
			readers.add(reader);
		}

		return readers.build();
	}

	public static class DatastoreIdentifierInputReader<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			extends InputReader<I> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6564553423057848858L;
		private final InputReader<Key> keyReader;
		private final Class<I> idType;

		public DatastoreIdentifierInputReader(InputReader<Key> keyReader, Class<I> idType) {
			this.keyReader = keyReader;
			this.idType = idType;
		}

		@Override
		public I next() throws IOException, NoSuchElementException {
			return DatastoreKeyUtil.id(keyReader.next(), idType);
		}

		/**
		 * The following methods are a straight copy of {@link ForwardingInputReader}, but type
		 * safety requires an InputReader of the same type not allowing the transition.
		 * 
		 * @return
		 */
		protected InputReader<Key> getDelegate() {
			return keyReader;
		}

		@Override
		public Double getProgress() {
			return getDelegate().getProgress();
		}

		@Override
		public void beginSlice() throws IOException {
			getDelegate().beginSlice();
		}

		@Override
		public void endSlice() throws IOException {
			getDelegate().endSlice();
		}

		@Override
		public void beginShard() throws IOException {
			getDelegate().beginShard();
		}

		@Override
		public void endShard() throws IOException {
			getDelegate().endShard();
		}

		@Override
		public long estimateMemoryRequirement() {
			return getDelegate().estimateMemoryRequirement();
		}

		@Override
		public void setContext(ShardContext context) {
			getDelegate().setContext(context);
		}

		@Override
		public ShardContext getContext() {
			return getDelegate().getContext();
		}
	}

	private static final long serialVersionUID = -7189818093146854493L;

}
