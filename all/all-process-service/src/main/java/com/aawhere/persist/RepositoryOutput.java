package com.aawhere.persist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.aawhere.id.Identifier;
import com.google.appengine.tools.mapreduce.Output;
import com.google.appengine.tools.mapreduce.OutputWriter;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;

public class RepositoryOutput<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends Output<E, Void> {

	private static final long serialVersionUID = -8172393736537926813L;
	private final Supplier<? extends Repository<E, I>> repositorySupplier;

	/** Use {@link Builder} to construct RepositoryOutput */
	public RepositoryOutput(Supplier<? extends Repository<E, I>> repositorySupplier) {
		this.repositorySupplier = repositorySupplier;
	}

	@Override
	public List<? extends OutputWriter<E>> createWriters(int numShards) {
		ImmutableList.Builder<RepositoryOutputWriter<E, I>> writers = ImmutableList.builder();
		for (int i = 0; i < numShards; i++) {
			writers.add(new RepositoryOutputWriter<E, I>(this.repositorySupplier));
		}
		return writers.build();
	}

	@Override
	public Void finish(Collection<? extends OutputWriter<E>> writers) throws IOException {
		// nothing to do...this returns void
		return null;
	}

	public static class RepositoryOutputWriter<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			extends OutputWriter<E> {
		private static final long serialVersionUID = 1093923859130909640L;
		private int batchSize = 100;
		private ArrayList<E> pool;
		private Supplier<? extends Repository<E, I>> repositorySupplier;

		public RepositoryOutputWriter(Supplier<? extends Repository<E, I>> repositorySupplier2) {
			this.repositorySupplier = repositorySupplier2;
			// flush will setup the pool
			flush();
		}

		@Override
		public void write(E value) throws IOException {
			if (pool.size() >= batchSize) {
				flush();
			}
			pool.add(value);
		}

		private void flush() {
			if (pool != null && !pool.isEmpty()) {
				repositorySupplier.get().storeAll(pool);
			}
			// use a new list instead of clearing since writing is async
			this.pool = new ArrayList<>(batchSize);
		}

		@Override
		public void endShard() throws IOException {
			flush();
			super.endShard();
		}

		@Override
		public void endSlice() throws IOException {
			flush();
			super.endSlice();
		}
	}

}
