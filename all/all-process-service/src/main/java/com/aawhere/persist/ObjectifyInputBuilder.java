package com.aawhere.persist;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.id.Identifier;
import com.aawhere.lang.ObjectBuilder;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.inputs.ObjectifyInput;
import com.googlecode.objectify.Objectify;

/**
 * Using the {@link ObjectifyInput} this provides a function that will finish loading the entities
 * provided by objectify.
 * 
 * @author aroller
 * 
 * @param <E>
 * @param <I>
 */
public class ObjectifyInputBuilder<E extends BaseEntity<E, I>, I extends Identifier<?, E>> {

	@Nonnull
	private Input<E> input;

	/**
	 * Used to construct all instances of EntityInput.
	 */
	public static class Builder<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			extends ObjectBuilder<ObjectifyInputBuilder<E, I>> {
		@Nonnull
		private Class<E> entityType;
		@Nullable
		private Query query;
		private MapReduceJobSettings settings;

		public Builder() {
			super(new ObjectifyInputBuilder<E, I>());
		}

		/** @see #settings */
		public Builder<E, I> settings(MapReduceJobSettings settings) {
			this.settings = settings;
			return this;
		}

		/** @see #query */
		public Builder<E, I> query(Query query) {
			this.query = query;
			return this;
		}

		/** @see #entityType */
		public Builder<E, I> entityType(Class<E> entityType) {
			this.entityType = entityType;
			return this;
		}

		@Override
		public ObjectifyInputBuilder<E, I> build() {
			ObjectifyInputBuilder<E, I> built = super.build();
			if (settings == null) {
				settings = MapReduceJobSettings.create().build();
			}
			built.input = new ObjectifyInput<E>(entityType, query, settings.options().inputCount(),
					new BaseEntityFunction<E, I>());

			return built;
		}
	}// end Builder

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Builder<E, I> create(Class<E> entityType) {
		return new Builder<E, I>().entityType(entityType);
	}

	/** Use {@link Builder} to construct EntityInput */
	private ObjectifyInputBuilder() {
	}

	/** @see #input */
	public Input<E> input() {
		return this.input;
	}

	/**
	 * Uses the objectify input, but applies our specific {@link BaseEntity#postLoad()} function
	 * after {@link Objectify#toPojo(Entity)} has been called. This is necessary for some entities,
	 * especially those with parents.
	 * 
	 * @author aroller
	 * 
	 * @param <E>
	 * @param <I>
	 */
	public static class BaseEntityFunction<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			extends ObjectifyInput.EntityFunction<E> {

		private static final long serialVersionUID = -5384736771365892194L;

		@Override
		public E apply(Entity input) {
			E entity = super.apply(input);
			entity.postLoad();
			return entity;
		}
	}

}
