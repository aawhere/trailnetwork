@XmlJavaTypeAdapters({
		@XmlJavaTypeAdapter(value = com.aawhere.util.rb.MessagePersonalizedXmlAdapter.class,
				type = com.aawhere.util.rb.Message.class),
		@XmlJavaTypeAdapter(value = DateTimeXmlAdapter.class, type = DateTime.class),
		@XmlJavaTypeAdapter(value = QuantityXmlAdapter.class, type = Quantity.class),
		@XmlJavaTypeAdapter(value = DurationXmlAdapter.class, type = Duration.class),
		@XmlJavaTypeAdapter(value = com.aawhere.personalize.xml.LocaleXmlAdapter.class) })
@XmlSchema(xmlns = @XmlNs(prefix = com.aawhere.xml.XmlNamespace.API_PREFIX,
		namespaceURI = com.aawhere.xml.XmlNamespace.API_VALUE))
package com.aawhere.person;

import javax.measure.quantity.Quantity;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.aawhere.joda.time.xml.DateTimeXmlAdapter;
import com.aawhere.joda.time.xml.DurationXmlAdapter;
import com.aawhere.measure.xml.QuantityXmlAdapter;

