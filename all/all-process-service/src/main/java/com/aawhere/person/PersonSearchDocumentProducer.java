/**
 *
 */
package com.aawhere.person;

import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang.StringUtils;

import com.aawhere.activity.ActivityType;
import com.aawhere.all.RouteNestedField;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountUtil;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.route.RouteCompletion;
import com.aawhere.route.RouteCompletionRepository;
import com.aawhere.route.RouteId;
import com.aawhere.search.GaeSearchUtil;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocument.Field;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Produces a {@link PersonSearchDocument} when provided a {@link Person}. Intended to be a
 * singleton that can be reused to create multiple {@link PersonSearchDocument}.
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class PersonSearchDocumentProducer {

	/**
	 * Used to construct all instances of PersonSearchDocumentProducer.
	 */
	public static class Builder
			extends ObjectBuilder<PersonSearchDocumentProducer> {

		public Builder() {
			super(new PersonSearchDocumentProducer());
		}

		public Builder routeCompletionRepository(RouteCompletionRepository routeCompletionRepository) {
			building.routeCompletionRepository = routeCompletionRepository;
			return this;
		}

		public Builder dictionaryFactory(FieldAnnotationDictionaryFactory dictionaryFactory) {
			building.dictionaryFactory = dictionaryFactory;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private RouteCompletionRepository routeCompletionRepository;
	private FieldAnnotationDictionaryFactory dictionaryFactory;

	/** Use {@link Builder} to construct PersonGaeDocumentProducer */
	private PersonSearchDocumentProducer() {
	}

	@Inject
	private PersonSearchDocumentProducer(RouteCompletionRepository completionRepo,
			FieldAnnotationDictionaryFactory factory) {
		this.routeCompletionRepository = completionRepo;
		this.dictionaryFactory = factory;
	}

	public SearchDocument create(@Nonnull Person person) {
		return create(person, null);
	}

	public SearchDocument create(@Nonnull Person person, @Nullable Iterable<Account> accounts) {
		FieldDictionary dictionary = dictionaryFactory.getDictionary(person.getClass());
		FieldDefinition<?> nameDef = null;
		try {
			nameDef = dictionary.findDefinition(Person.FIELD.NAME_WITH_ALIASES);
		} catch (FieldNotRegisteredException e) {
			throw new RuntimeException(e);
		}

		SearchDocument.Builder builder = new SearchDocument.Builder();
		builder.domain(dictionary.getDomainKey());
		builder.id(person.id());
		// TN-848 added aliases individually and name individually too. so everything in person with
		// aliases is now in the others so no need to search. leaving for client reference and
		// backwards compatibility
		builder.addField(personNameField(person, nameDef));
		// personNameSubstringsField(builder, person, nameDef);

		String name = person.name();
		if (PersonUtil.nameIsUseful(person, AccountUtil.aliases(accounts))) {
			FieldKey nameKey = PersonField.Key.NAME;
			builder.addField(Field.create().key(nameKey).value(name).build());
			builder.addField(GaeSearchUtil.suggestWords(name, nameKey));
		}

		if (accounts != null) {
			FieldKey accountKey = RouteNestedField.Key.PERSON__ACCOUNT__ALIAS;

			Set<String> aliases = AccountUtil.aliases(accounts);
			for (String alias : aliases) {
				if (alias != null) {
					builder.addField(Field.create().key(accountKey).value(alias).build());
					// aliases have the advantage of providing a last name that may be embedded in
					// the alias
					// aroller...someone may search for roller and find benefit in this
					builder.addField(GaeSearchUtil.suggestWordsVerbose(alias, accountKey));
				}
			}
		}
		int numberOfCompletions = personRouteCompletions(person, builder);
		builder.rank(numberOfCompletions);
		return builder.build();
	}

	private Field personNameField(Person person, FieldDefinition<?> nameDef) {

		// TN-848 added aliases and name individually. made this atom since the others provide the
		// substrings
		return Field.create().dataType(FieldDataType.ATOM).key(nameDef.getKey()).value(person.nameWithAliases())
				.build();

	}

	private Integer personRouteCompletions(Person person, SearchDocument.Builder builder) {
		FieldDictionary completionDictionary = dictionaryFactory.getDictionary(RouteCompletion.class);
		FieldKey personKey;
		FieldKey routeIdKey;
		FieldKey atKey;
		try {
			personKey = completionDictionary.findKey(RouteCompletion.FIELD.PERSON_ID);
			routeIdKey = completionDictionary.findKey(RouteCompletion.FIELD.ROUTE_ID);
			atKey = completionDictionary.findKey(RouteCompletion.FIELD.ACTIVITY_TYPE);
		} catch (FieldNotRegisteredException e) {
			// This should be present. Programming error.
			throw new RuntimeException(e);
		}
		FieldKey routeIdActivityTypeKey = new FieldKey(routeIdKey.getDomainKey(), routeIdKey.getRelativeKey()
				+ StringUtils.capitalize(atKey.getRelativeKey()));

		Filter completionsFilter = Filter
				.create()
				.addCondition(FilterCondition.create().field(personKey).operator(FilterOperator.EQUALS)
						.value(person.id()).build()).build();
		Iterable<RouteCompletion> completions = routeCompletionRepository.entityPaginator(completionsFilter);
		// TODO: WW-237 streamline query? use a sort by to limit to top
		// social rank, eliminating
		// the need for a set?
		int numberOfCompletions = 0;
		for (RouteCompletion completion : completions) {
			ActivityType at = completion.activityType();
			RouteId routeId = completion.routeId();

			if (at != null && routeId != null) {
				builder.addField(Field.create().dataType(FieldDataType.ATOM).key(routeIdKey).value(routeId).build());

				String routeActivityType = routeId.getValue() + FieldUtils.KEY_DILIM + at.toString();
				builder.addField(Field.create().dataType(FieldDataType.ATOM).key(routeIdActivityTypeKey)
						.value(routeActivityType).build());
				numberOfCompletions++;
			}

		}
		return numberOfCompletions;
	}

}
