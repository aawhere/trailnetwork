/**
 *
 */
package com.aawhere.person;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.queue.QueueFactory;
import com.aawhere.search.FieldNotSearchableException;
import com.aawhere.search.MaxIndexAttemptsExceeded;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.search.UnknownIndexException;
import com.google.inject.Inject;

/**
 * Location to run batch processes against people entities.
 * 
 * @author Brian Chapman
 * 
 */
public class PersonProcessService {

	private PersonSearchDocumentProducer personSearchDocumentProducer;
	private SearchService searchService;
	private QueueFactory queueFactory;
	@Inject
	private static PersonService personService;

	@Inject
	public PersonProcessService(PersonSearchDocumentProducer producer, SearchService searchService,
			QueueFactory queueFactory, PersonService personServiceSingleton) {
		this.personSearchDocumentProducer = producer;
		this.searchService = searchService;
		this.queueFactory = queueFactory;

		if (personService == null) {
			personService = personServiceSingleton;
		}
	}

	/**
	 * Add all the people to the search index. Updates people if they alread exist in the index.
	 * Does not delete people that only exist in the index but exists in the datastore, so this is
	 * not a sync method.
	 * 
	 * @throws MaxIndexAttemptsExceeded
	 * 
	 * @return the number of people updated.
	 */
	public Integer indexAllPeople() throws MaxIndexAttemptsExceeded {

		Iterable<PersonId> peopleIds = personService.ids(null);
		return SearchWebApiUri.indexTasks(	peopleIds,
											PersonWebApiUri.PERSONS,
											queueFactory.getQueue(SearchWebApiUri.SEARCH_QUEUE_NAME));
	}

	/**
	 * Index the person identified by {@link PersonId}.
	 * 
	 * @param personId
	 * @return the person (mainly to satisfy the /persons endpoint.
	 * @throws MaxIndexAttemptsExceeded
	 * @throws EntityNotFoundException
	 */
	public SearchDocument indexPerson(PersonId personId) throws MaxIndexAttemptsExceeded, EntityNotFoundException {
		Person person = personService.person(personId);
		if (person.identityShared()) {
			SearchDocument personDocument = personSearchDocumentProducer.create(person, personService.accounts(person));
			searchService.index(personDocument);
			return personDocument;
		} else {
			// WW-249 Don't index, but also don't throw a UnableToIndexException as it
			// will cause the queue to continue retrying. We intentionally do not
			// want this in the index.
			return null;
		}

	}

	/**
	 * Provides a report of search results matching the supplied filter for the persons index.
	 * 
	 * @param filter
	 * @return
	 * @throws FieldNotSearchableException
	 * @throws UnknownIndexException
	 */
	public SearchDocuments search(Filter filter) throws FieldNotSearchableException, UnknownIndexException {
		return searchService.query(filter, Person.FIELD.DOMAIN);
	}

	public static PersonService personService() {
		return personService;
	}
}
