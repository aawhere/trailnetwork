package com.aawhere.trailhead;

import java.util.Map;
import java.util.Map.Entry;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityGeoCellNameMapper;
import com.aawhere.activity.ActivityNameSelector;
import com.aawhere.activity.ActivityProcessService;
import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.app.account.AccountId;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.Filter;
import com.aawhere.persist.ObjectifyInputBuilder;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.tools.mapreduce.GoogleCloudStorageFileSet;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.KeyValue;
import com.google.appengine.tools.mapreduce.MapReduceJob;
import com.google.appengine.tools.mapreduce.MapReduceResult;
import com.google.appengine.tools.mapreduce.MapReduceSpecification;
import com.google.appengine.tools.mapreduce.inputs.MapReduceInputUtil;
import com.google.appengine.tools.mapreduce.mappers.IdentityMapper;
import com.google.appengine.tools.mapreduce.outputs.MapReduceOutputUtil;
import com.google.appengine.tools.mapreduce.outputs.ObjectifyOutput;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Job1;
import com.google.appengine.tools.pipeline.Value;

public class TrailheadNamePipeline
		extends Job0<MapReduceResult<Void>> {

	private static final long serialVersionUID = 8082408557813285524L;

	private Filter filter;
	private MapReduceJobSettings settings;

	/**
	 * Used to construct all instances of TrailheadNamePipeline.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadNamePipeline> {

		public Builder() {
			super(new TrailheadNamePipeline());
		}

		/** @see #filter */
		public Builder filter(Filter filter) {
			building.filter = filter;
			return this;
		}

		@Override
		protected void validate() {
			Assertion.exceptions().notNull("filter", building.filter);
			super.validate();
		}

		@Override
		public TrailheadNamePipeline build() {
			TrailheadNamePipeline built = super.build();
			return built;
		}

		public Builder settings(MapReduceJobSettings settings) {
			building.settings = settings;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrailheadNamePipeline */
	private TrailheadNamePipeline() {
	}

	@Override
	public Value<MapReduceResult<Void>> run() throws Exception {
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> activityGeoCellName = futureCall(	activityGeoCellName(),
																									settings.jobSettings());

		return futureCall(trailheadNameUpdated(), activityGeoCellName, settings.jobSettings());
	}

	/**
	 * Input is the filtered activities which has the name and start geo cell extracted, sent to a
	 * reducer which finds the trailhead for that geocell and emits the trailhead id/name key for
	 * each.
	 * 
	 * @return
	 */
	private
			MapReduceJob<Activity, String, Entry<AccountId, String>, KeyValue<TrailheadId, Entry<AccountId, String>>, GoogleCloudStorageFileSet>
			activityGeoCellName() {
		String jobName = "ActivityGeoCellName";
		MapReduceSpecification.Builder<Activity, String, Map.Entry<AccountId, String>, KeyValue<TrailheadId, Map.Entry<AccountId, String>>, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
		Query query = ActivityProcessService.datastoreQuery(filter);
		Input<Activity> input = ObjectifyInputBuilder.create(Activity.class).settings(settings).query(query).build()
				.input();
		specBuilder.setInput(input);
		specBuilder.setMapper(new ActivityGeoCellNameMapper());
		specBuilder.setReducer(new ActivityGeoCellNameTrailheadIdReducer());
		specBuilder.setJobName(jobName);
		specBuilder.setOutput(MapReduceOutputUtil.fileOutput(	TrailheadNamePipeline.class,
																getPipelineKey(),
																jobName,
																settings));
		specBuilder.setNumReducers(settings.options().reducerCount());
		return new MapReduceJob<>(specBuilder.build(), settings.settings());
	}

	/**
	 * Each trailhead id and it's corresponding names are reduced to the common trailhead id and run
	 * through the {@link ActivityNameSelector} to find the best name. The trailhead is mutated and
	 * output for updating.
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> trailheadNameUpdated() {

		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return "TrailheadNameUpdated";
			}

			@Override
			public FutureValue<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult)
					throws Exception {

				MapReduceSpecification.Builder<KeyValue<TrailheadId, Map.Entry<AccountId, String>>, TrailheadId, Map.Entry<AccountId, String>, Trailhead, Void> specBuilder = new MapReduceSpecification.Builder<>();
				specBuilder.setInput(MapReduceInputUtil
						.<KeyValue<TrailheadId, Map.Entry<AccountId, String>>> input(fileSetResult));
				specBuilder.setMapper(new IdentityMapper<TrailheadId, Map.Entry<AccountId, String>>());
				specBuilder.setReducer(new TrailheadIdNameReducer());
				specBuilder.setJobName(getJobDisplayName());
				specBuilder.setOutput(new ObjectifyOutput<Trailhead>());
				specBuilder.setNumReducers(settings.options().reducerCount());

				return futureCall(new MapReduceJob<>(specBuilder.build(), settings.settings()), settings.jobSettings());
			}
		};
	}

}
