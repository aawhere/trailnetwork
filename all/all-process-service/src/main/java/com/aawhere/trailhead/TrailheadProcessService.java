package com.aawhere.trailhead;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.annotation.Nullable;

import com.aawhere.activity.RelativeStatsActivityStatsAggregator;
import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.all.process.PipelineWebApiUri;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.ObjectifyInputBuilder;
import com.aawhere.persist.RepositoryOutput;
import com.aawhere.process.mapreduce.outputs.DelegatingOutput.OutputWriterDelegate;
import com.aawhere.route.RouteCompletion;
import com.aawhere.route.TrailheadRouteCompletionStatsPipeline;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.pipeline.PipelineService;
import com.google.appengine.tools.pipeline.PipelineServiceFactory;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class TrailheadProcessService {

	private static final RepositoryOutput<Trailhead, TrailheadId> REPOSITORY_OUTPUT = new RepositoryOutput<Trailhead, TrailheadId>(
			trailheadRepostorySupplier());

	public enum JobName {
		TRAILHEAD_ACTIVITY_STARTS_STATS, TRAILHEAD_ROUTE_COMPLETION_STATS
	}

	private static TrailheadService trailheadService;
	private static TrailheadRepository trailheadRepository;

	@Inject
	static void inject(TrailheadService trailheadServiceSingleton) {
		trailheadService = trailheadServiceSingleton;
	}

	@Inject
	static void inject(TrailheadRepository trailheadRepositorySingleton) {
		trailheadRepository = trailheadRepositorySingleton;
	}

	public static TrailheadService trailheadService() {
		return trailheadService;
	}

	public URI trailheadNamesPipeline(Filter filter) {
		PipelineService service = PipelineServiceFactory.newPipelineService();
		MapReduceJobSettings settings = MapReduceJobSettings.create().build();
		String jobId = service.startNewPipeline(TrailheadNamePipeline.create().filter(filter).settings(settings)
				.build(), settings.jobSettings());
		return PipelineWebApiUri.pipelineStatus(jobId);
	}

	public String trailheadStartsPipeline() {
		PipelineService service = PipelineServiceFactory.newPipelineService();
		String pipelineId = service.startNewPipeline(	TrailheadActivityStartsPipeline.create().build(),
														new MapReduceJobSettings.Builder().build().jobSettings());
		return pipelineId;
	}

	public URI jobStarted(JobName jobName) {
		String jobId;
		switch (jobName) {
			case TRAILHEAD_ACTIVITY_STARTS_STATS:
				jobId = trailheadStartsPipeline();
				break;
			case TRAILHEAD_ROUTE_COMPLETION_STATS: {
				PipelineService service = PipelineServiceFactory.newPipelineService();
				// TODO: provide a filter when it's supported.
				// see RouteTrailheadOrganizerPipeline to see how this is included automatically in
				// the processing
				Input<RouteCompletion> routeCompletionInput = ObjectifyInputBuilder.create(RouteCompletion.class)
						.build().input();
				jobId = service.startNewPipeline(	TrailheadRouteCompletionStatsPipeline.create()
															.routeCompletionInput(routeCompletionInput).build(),
													new MapReduceJobSettings.Builder().build().jobSettings());
				break;
			}
			default:
				throw new InvalidChoiceException(jobName);
		}
		return PipelineWebApiUri.pipelineStatus(jobId);
	}

	public static RepositoryOutput<Trailhead, TrailheadId> trailheadRepositoryOutput() {
		return REPOSITORY_OUTPUT;
	}

	private enum TrailheadRepositorySupplier implements Supplier<TrailheadRepository> {
		INSTANCE;
		@Override
		public TrailheadRepository get() {
			return trailheadRepository;
		}

		@Override
		public String toString() {
			return "TrailheadRepositorySupplier";
		}
	}

	private enum TrailheadFunction implements Function<TrailheadId, Trailhead> {
		INSTANCE;
		@Override
		@Nullable
		public Trailhead apply(@Nullable TrailheadId trailheadId) {
			try {
				return (trailheadId != null) ? trailheadService.trailhead(trailheadId) : null;
			} catch (EntityNotFoundException e) {
				return null;
			}
		}

		@Override
		public String toString() {
			return "TrailheadFunction";
		}
	}

	public static Function<TrailheadId, Trailhead> trailheadFunction() {
		return TrailheadFunction.INSTANCE;
	}

	private static Supplier<TrailheadRepository> trailheadRepostorySupplier() {
		return TrailheadRepositorySupplier.INSTANCE;
	}

	public static OutputWriterDelegate<RelativeStatsActivityStatsAggregator<TrailheadId>>
			activityStatsTrailheadUpdater() {
		return new OutputWriterDelegate<RelativeStatsActivityStatsAggregator<TrailheadId>>() {

			private static final long serialVersionUID = -1225486359232481077L;

			@Override
			public void write(List<RelativeStatsActivityStatsAggregator<TrailheadId>> values) throws IOException {
				trailheadService.trailheadActivityStartStatsUpdated(values);

			}
		};
	}

	public static OutputWriterDelegate<RelativeStatsActivityStatsAggregator<TrailheadId>>
			routeCompletionStatsTrailheadUpdater() {
		return new OutputWriterDelegate<RelativeStatsActivityStatsAggregator<TrailheadId>>() {

			private static final long serialVersionUID = -1225486359232481078L;

			@Override
			public void write(List<RelativeStatsActivityStatsAggregator<TrailheadId>> values) throws IOException {
				trailheadService.trailheadRouteCompletionStatsUpdated(values);

			}
		};
	}

}
