package com.aawhere.trailhead;

import com.aawhere.activity.ActivityStartRelativeStatisticMapper;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.measure.geocell.GeoCell;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;

/**
 * Given {@link RelativeStatistic} provided from {@link ActivityStartRelativeStatisticMapper} that
 * are keyed by their start {@link GeoCell} this will take the grouped relative Activity stats and
 * associate them with the nearby {@link Trailhead} and emit a new {@link RelativeStatistic} with
 * the {@link TrailheadId} as the featureId. Emits the same number as received, but uses the
 * reduction to associate the relative stats by area of the given geocell so the trailhead can be
 * retrieved once.
 * 
 * So each {@link RelativeStatistic} indicates an activity that started from the trailhead...there
 * will be duplicate ids.
 * 
 * @author aroller
 * 
 */
public class ActivityStartsRelativeStatisticTrailheadReducer
		extends Reducer<GeoCell, RelativeStatistic, RelativeStatistic> {

	private static final long serialVersionUID = 6918569584650573717L;

	@Override
	public void reduce(GeoCell key, ReducerInput<RelativeStatistic> activityRelativeStats) {

		Trailhead nearestCreated = TrailheadProcessService.trailheadService().nearestCreated(key);
		TrailheadId trailheadId = nearestCreated.id();

		while (activityRelativeStats.hasNext()) {
			RelativeStatistic activityStatistic = activityRelativeStats.next();
			RelativeStatistic trailheadStatistic = RelativeStatistic.clone(activityStatistic).resetId()
					.featureId(trailheadId).build();
			emit(trailheadStatistic);
		}

	}

}
