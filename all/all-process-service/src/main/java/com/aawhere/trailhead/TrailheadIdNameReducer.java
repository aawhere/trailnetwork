package com.aawhere.trailhead;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.aawhere.activity.ActivityNameSelector;
import com.aawhere.app.account.AccountId;
import com.aawhere.collections.MapEntryUtil;
import com.aawhere.collections.MapEntryUtil.KeySetPredicate;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.Person;
import com.aawhere.person.PersonProcessService;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Iterators;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.collect.UnmodifiableIterator;

/**
 * The trailhead has a bunch of names associated with it. This will use the
 * {@link ActivityNameSelector} to determine the best and update the trailhead with the name if a
 * worthy name is found.
 * 
 * @author aroller
 * 
 */
public class TrailheadIdNameReducer
		extends Reducer<TrailheadId, Map.Entry<AccountId, String>, Trailhead> {

	private static final long serialVersionUID = -9057350210353329879L;

	@Override
	public void reduce(TrailheadId trailheadId, final ReducerInput<Map.Entry<AccountId, String>> accountIdNames) {
		// TN-913 keep track of the accountids if the names fail, use account name
		KeySetPredicate<AccountId, String> keySetPredicate = MapEntryUtil.keySetPredicate();
		UnmodifiableIterator<Entry<AccountId, String>> filtered = Iterators.filter(accountIdNames, keySetPredicate);
		Iterator<String> names = Iterators.transform(filtered, MapEntryUtil.<AccountId, String> valueFunction());
		// TODO:the number of names could get too big. extract the logic of the selector into more
		// map reduce workers
		ActivityNameSelector selector = ActivityNameSelector.create().names(names).build();
		Message bestName = TrailheadUtil.bestName(selector);
		// TN-913 Use account id if name isn't discovered
		if (bestName == null) {
			// name with the most popular user of the trailhead
			Multiset<AccountId> accountIds = keySetPredicate.keys();
			if (!accountIds.isEmpty()) {
				ImmutableMultiset<AccountId> mostActiviveAccountIds = Multisets.copyHighestCountFirst(accountIds);
				AccountId accountId = mostActiviveAccountIds.iterator().next();
				try {
					Person person = PersonProcessService.personService().person(accountId);
					bestName = new StringMessage(person.name());
				} catch (EntityNotFoundException e) {
					LoggerFactory.getLogger(getClass()).warning(accountId + " was gonna be the trailhead name, but "
							+ e.getMessage());
				}
			}
		}
		if (bestName != null) {
			try {
				Trailhead trailhead = TrailheadProcessService.trailheadService().trailhead(trailheadId);
				if (!bestName.equals(trailhead.name())) {
					Trailhead mutated = Trailhead.mutate(trailhead).name(bestName).build();
					emit(mutated);
				} else {
					LoggerFactory.getLogger(getClass()).fine(trailheadId + " already has best name " + bestName
							+ " from " + selector.activityNames());
				}
			} catch (EntityNotFoundException e) {
				// Weird, but not worth ruining the pipeline
				LoggerFactory.getLogger(getClass()).warning(e.getMessage());
			}
		} else {
			LoggerFactory.getLogger(getClass()).fine(trailheadId + " has no name from total name count: "
					+ selector.activityNames().size());
		}

	}

}
