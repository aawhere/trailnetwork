package com.aawhere.trailhead;

import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.ActivityStats;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;

public class TrailheadActivityStatsMutatorMapper
		extends MapOnlyMapper<Entry<String, Pair<ActivityStats, ActivityStats>>, Trailhead> {

	private static final long serialVersionUID = 8223974416796516570L;

	@Override
	public void map(Entry<String, Pair<ActivityStats, ActivityStats>> value) {
		TrailheadId trailheadId = new TrailheadId(value.getKey());
		try {
			Trailhead trailhead = TrailheadProcessService.trailheadService().trailhead(trailheadId);
			emit(Trailhead.mutate(trailhead).activityStartStats(value.getValue().getLeft())
					.activityStartPersonStats(value.getValue().getRight()).build());

		} catch (EntityNotFoundException e) {
			// this can happen during a merge...don't go for the nearest
			LoggerFactory.getLogger(getClass()).warning(e.getMessage());
		}

	}

}
