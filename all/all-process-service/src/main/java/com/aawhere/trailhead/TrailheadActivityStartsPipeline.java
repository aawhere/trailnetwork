package com.aawhere.trailhead;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityAccountIdMapper;
import com.aawhere.activity.ActivityAccountIdPersonIdReducer;
import com.aawhere.activity.ActivityStartRelativeStatisticMapper;
import com.aawhere.activity.RelativeStatistic;
import com.aawhere.activity.RelativeStatsActivityStatsAggregator;
import com.aawhere.activity.RelativeStatsActivityStatsReducer;
import com.aawhere.activity.RelativeStatsCounterReducer;
import com.aawhere.activity.RelativeStatsFeatureIdMapper;
import com.aawhere.activity.RelativeStatsIdMapper;
import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.app.account.AccountId;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.persist.Filter;
import com.aawhere.person.PersonId;
import com.aawhere.process.mapreduce.outputs.DelegatingOutput;
import com.aawhere.search.SearchService;
import com.google.appengine.tools.mapreduce.GoogleCloudStorageFileSet;
import com.google.appengine.tools.mapreduce.Input;
import com.google.appengine.tools.mapreduce.MapReduceJob;
import com.google.appengine.tools.mapreduce.MapReduceResult;
import com.google.appengine.tools.mapreduce.MapReduceSpecification;
import com.google.appengine.tools.mapreduce.Marshallers;
import com.google.appengine.tools.mapreduce.inputs.GoogleCloudStorageLevelDbInput;
import com.google.appengine.tools.mapreduce.inputs.ObjectifyInput;
import com.google.appengine.tools.mapreduce.inputs.UnmarshallingInput;
import com.google.appengine.tools.mapreduce.outputs.MapReduceOutputUtil;
import com.google.appengine.tools.mapreduce.outputs.MarshallingOutput;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Job1;
import com.google.appengine.tools.pipeline.Value;

/**
 * Inspecting every {@link Activity} in the system (considered to be unique and worth counting) this
 * maps and reduces the Activity into it's spatial start, finds the corresponding trailhead and
 * counts the statistics outputing the results to the {@link SearchService} and
 * {@link TrailheadRepository}.
 * 
 * The flow is best understood visually.
 * 
 * https://drive.google.com/open?id=0B2JqvdK7_QrKWnpUSENXeEhIOEE&authuser=0
 * 
 * @author aroller
 * 
 */
public class TrailheadActivityStartsPipeline
		extends Job0<MapReduceResult<Void>> {

	private static final long serialVersionUID = -8347087282660233872L;

	private MapReduceJobSettings settings;

	private Input<Activity> input;

	/**
	 * Used to construct all instances of TrailheadActivityStartsPipeline.
	 */
	public static class Builder
			extends ObjectBuilder<TrailheadActivityStartsPipeline> {

		// FIXME: use the filter to create the Input when Objectify 5 is alive
		private Filter filter;

		/** @see #filter */
		public Builder filter(Filter filter) {
			this.filter = filter;
			return this;
		}

		/** @see #settings */
		public Builder settings(MapReduceJobSettings settings) {
			building.settings = settings;
			return this;
		}

		public Builder() {
			super(new TrailheadActivityStartsPipeline());
		}

		@Override
		public TrailheadActivityStartsPipeline build() {
			TrailheadActivityStartsPipeline built = super.build();

			if (built.settings == null) {
				built.settings = MapReduceJobSettings.create().build();
			}
			return built;
		}

		public Builder input(Input<Activity> input) {
			building.input = input;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct TrailheadActivityStartsPipeline */
	private TrailheadActivityStartsPipeline() {
	}

	@Override
	public FutureValue<MapReduceResult<Void>> run() throws Exception {

		// associate the person to the activity using relative stats and key by start
		// associates the activity to a small area, finds the corresponding trailhead

		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> activityPersonResults = futureCall(	activityToPerson(),
																									settings.jobSettings());
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> activityToTrailhead = futureCall(	activityToTrailheadJob(),
																									activityPersonResults,
																									settings.jobSettings());
		FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> trailheadStatsCounter = futureCall(	relativeStatsCounter(),
																									activityToTrailhead,
																									settings.jobSettings());
		// TODO:Combine search documents for persons with those from
		// TrailheadRouteCompletionsPipeline
		// emit the search documents, then use a search document mergereducer to combine the fields
		return futureCall(activityStatsForPublic(), trailheadStatsCounter, settings.jobSettings());

	}

	/**
	 * Provides the input. I must be serializable, but to support interfaces that don't declare
	 * serializable this method is type unsafe.
	 * 
	 * @param fileSetResult
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private <I> UnmarshallingInput<I> input(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) {
		Input<ByteBuffer> cloudFileInput = new GoogleCloudStorageLevelDbInput(fileSetResult.getOutputResult());
		UnmarshallingInput input = new UnmarshallingInput(cloudFileInput, Marshallers.getSerializationMarshaller());
		return input;
	}

	private MarshallingOutput<Serializable, GoogleCloudStorageFileSet> fileOutput(String jobName, String pipelineKey) {
		return MapReduceOutputUtil.fileOutput(getClass(), pipelineKey, jobName, settings.bucketName());
	}

	private MapReduceJob<Activity, AccountId, Activity, Pair<Activity, PersonId>, GoogleCloudStorageFileSet>
			activityToPerson() {
		String jobName = "ActivityToPerson";
		MapReduceSpecification.Builder<Activity, AccountId, Activity, Pair<Activity, PersonId>, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();
		if (input == null) {
			input = new ObjectifyInput<Activity>(Activity.class, settings.options().inputCount());
		}
		specBuilder.setInput(input);
		specBuilder.setMapper(new ActivityAccountIdMapper());
		specBuilder.setReducer(new ActivityAccountIdPersonIdReducer());
		specBuilder.setJobName(jobName);
		specBuilder.setOutput(fileOutput(jobName, getPipelineKey().getName()));
		specBuilder.setNumReducers(settings.options().reducerCount());
		MapReduceSpecification<Activity, AccountId, Activity, Pair<Activity, PersonId>, GoogleCloudStorageFileSet> activityPersonSpec = specBuilder
				.build();
		MapReduceJob<Activity, AccountId, Activity, Pair<Activity, PersonId>, GoogleCloudStorageFileSet> activityPersonJob = new MapReduceJob<>(
				activityPersonSpec, settings.settings());
		return activityPersonJob;
	}

	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			activityToTrailheadJob() {
		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = -3554565127574068443L;

			@Override
			public String getJobDisplayName() {
				return "ActivityToTrailheadStats";
			}

			@Override
			public FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {

				MapReduceSpecification.Builder<Entry<Activity, PersonId>, GeoCell, RelativeStatistic, RelativeStatistic, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();

				String jobName = "ActivityToTrailheadStats";
				specBuilder.setJobName(jobName);
				UnmarshallingInput<Pair<Activity, PersonId>> input = input(fileSetResult);
				specBuilder.setInput(input);
				specBuilder.setMapper(new ActivityStartRelativeStatisticMapper());
				specBuilder.setReducer(new ActivityStartsRelativeStatisticTrailheadReducer());
				specBuilder.setOutput(fileOutput(jobName, getPipelineKey().getName()));
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceSpecification<Entry<Activity, PersonId>, GeoCell, RelativeStatistic, RelativeStatistic, GoogleCloudStorageFileSet> spec = specBuilder
						.build();
				MapReduceJob<Entry<Activity, PersonId>, GeoCell, RelativeStatistic, RelativeStatistic, GoogleCloudStorageFileSet> job = new MapReduceJob<Entry<Activity, PersonId>, GeoCell, RelativeStatistic, RelativeStatistic, GoogleCloudStorageFileSet>(
						spec, settings.settings());
				return futureCall(job, settings.jobSettings());
			}

		};
	}

	private Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>
			relativeStatsCounter() {

		final String jobName = "RelativeStatsCounter";

		return new Job1<MapReduceResult<GoogleCloudStorageFileSet>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public FutureValue<MapReduceResult<GoogleCloudStorageFileSet>> run(
					MapReduceResult<GoogleCloudStorageFileSet> fileSetResult) throws Exception {
				MapReduceSpecification.Builder<RelativeStatistic, RelativeStatistic, String, RelativeStatistic, GoogleCloudStorageFileSet> specBuilder = new MapReduceSpecification.Builder<>();

				specBuilder.setJobName(jobName);
				UnmarshallingInput<RelativeStatistic> input = input(fileSetResult);
				specBuilder.setInput(input);
				specBuilder.setMapper(new RelativeStatsIdMapper());
				specBuilder.setReducer(new RelativeStatsCounterReducer<String>());
				specBuilder.setOutput(fileOutput(jobName, getPipelineKey().getName()));
				specBuilder.setNumReducers(settings.options().reducerCount());
				MapReduceSpecification<RelativeStatistic, RelativeStatistic, String, RelativeStatistic, GoogleCloudStorageFileSet> spec = specBuilder
						.build();
				MapReduceJob<RelativeStatistic, RelativeStatistic, String, RelativeStatistic, GoogleCloudStorageFileSet> job = new MapReduceJob<>(
						spec, settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	/**
	 * Relative stats are now counted. We first aggregate those related to non-personal to create
	 * activity stats for each trailhead. The result is the trailhead id and the activity stats that
	 * should be persisted.
	 * 
	 * @return
	 */
	private Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>> activityStatsForPublic() {

		final String jobName = "PublicActivityStats";

		return new Job1<MapReduceResult<Void>, MapReduceResult<GoogleCloudStorageFileSet>>() {

			private static final long serialVersionUID = 5062501809704880774L;

			@Override
			public String getJobDisplayName() {
				return jobName;
			}

			@Override
			public Value<MapReduceResult<Void>> run(MapReduceResult<GoogleCloudStorageFileSet> fileSetResult)
					throws Exception {
				MapReduceSpecification.Builder<RelativeStatistic, TrailheadId, RelativeStatistic, RelativeStatsActivityStatsAggregator<TrailheadId>, Void> specBuilder = new MapReduceSpecification.Builder<>();

				specBuilder.setJobName(jobName);
				UnmarshallingInput<RelativeStatistic> input = input(fileSetResult);
				specBuilder.setInput(input);
				specBuilder.setMapper(RelativeStatsFeatureIdMapper.create(TrailheadId.class).build());
				specBuilder.setReducer(new RelativeStatsActivityStatsReducer<TrailheadId>());
				specBuilder.setNumReducers(settings.options().reducerCount());
				DelegatingOutput<RelativeStatsActivityStatsAggregator<TrailheadId>> output = new DelegatingOutput<RelativeStatsActivityStatsAggregator<TrailheadId>>(
						TrailheadProcessService.activityStatsTrailheadUpdater());
				specBuilder.setOutput(output);
				MapReduceSpecification<RelativeStatistic, TrailheadId, RelativeStatistic, RelativeStatsActivityStatsAggregator<TrailheadId>, Void> spec = specBuilder
						.build();
				MapReduceJob<RelativeStatistic, TrailheadId, RelativeStatistic, RelativeStatsActivityStatsAggregator<TrailheadId>, Void> job = new MapReduceJob<>(
						spec, settings.settings());

				return futureCall(job, settings.jobSettings());
			}
		};
	}

	public static Builder create() {
		return new Builder();
	}

}
