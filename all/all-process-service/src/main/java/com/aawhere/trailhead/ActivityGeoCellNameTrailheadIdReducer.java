package com.aawhere.trailhead;

import java.util.Map;

import com.aawhere.app.account.AccountId;
import com.aawhere.measure.geocell.GeoCell;
import com.google.appengine.tools.mapreduce.KeyValue;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.appengine.tools.mapreduce.mappers.IdentityMapper;

/**
 * Reduces the given geocell and names associated with the geocell to the nearest trailhead. Passes
 * it on so another map/reduce can combine common geocells that share a trailhead so all names will
 * be considered for that single trailhead.
 * 
 * @see IdentityMapper
 * 
 * @author aroller
 * 
 */
public class ActivityGeoCellNameTrailheadIdReducer
		extends Reducer<String, Map.Entry<AccountId, String>, KeyValue<TrailheadId, Map.Entry<AccountId, String>>> {

	private static final long serialVersionUID = 4912746560988870504L;

	@Override
	public void reduce(String geoCell, ReducerInput<Map.Entry<AccountId, String>> names) {
		Trailhead trailhead = TrailheadProcessService.trailheadService().nearestCreated(new GeoCell(geoCell));
		TrailheadId id = trailhead.id();
		// although a waste to drop the trailhead now and later retrieve it again, serializing it
		// with every name would be a bigger waste.
		while (names.hasNext()) {
			emit(new KeyValue<>(id, names.next()));
		}

	}

}
