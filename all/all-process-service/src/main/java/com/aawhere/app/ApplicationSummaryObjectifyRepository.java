/**
 * 
 */
package com.aawhere.app;

import com.aawhere.app.ApplicationSummary.Builder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.objectify.ObjectifyRepository;

import com.googlecode.objectify.ObjectifyService;

/**
 * @author aroller
 * 
 */
public class ApplicationSummaryObjectifyRepository
		extends ObjectifyRepository<ApplicationSummary, ApplicationSummaryId>
		implements ApplicationSummaryRepository {

	static {
		ObjectifyService.register(ApplicationSummary.class);
	}

	/**
	 * 
	 */
	public ApplicationSummaryObjectifyRepository() {
	}

	/**
	 * @param synchronous
	 */
	public ApplicationSummaryObjectifyRepository(
			com.aawhere.persist.objectify.ObjectifyRepository.Synchronization synchronous) {
		super(synchronous);
	}

	/*
	 * @see com.aawhere.persist.RepositoryUpdateable#update(com.aawhere.id.Identifier)
	 */
	@Override
	public Builder update(ApplicationSummaryId id) throws EntityNotFoundException {
		return new ApplicationSummary.Builder(load(id)) {
			/*
			 * @see com.aawhere.app.ApplicationSummary.Builder#build()
			 */
			@Override
			public ApplicationSummary build() {
				return update(super.build());

			}
		};

	}

}
