/**
 * 
 */
package com.aawhere.app.account;

import java.io.Serializable;

import com.aawhere.app.ApplicationKey;
import com.aawhere.field.FieldDefinition;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.FieldPropertyEntityUtil;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.tools.mapreduce.Mapper;
import com.google.appengine.tools.mapreduce.Reducer;
import com.google.appengine.tools.mapreduce.ReducerInput;
import com.google.common.collect.Iterators;

/**
 * A map reduce implementation that acounts the number of accounts for an application.
 * 
 * @author aroller
 * 
 */
public class AccountApplicationCounterMapReduce
		implements Serializable {

	private FieldDefinition<Integer> accountCountDefinition;

	/**
	 * Used to construct all instances of ApplicationAccountMapReduce.
	 */
	public static class Builder
			extends ObjectBuilder<AccountApplicationCounterMapReduce> {

		public Builder() {
			super(new AccountApplicationCounterMapReduce());
		}

		public Builder accountCountDefinition(FieldDefinition<Integer> accountCountDefinition) {
			building.accountCountDefinition = accountCountDefinition;
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("accountCountDefinition", building.accountCountDefinition);

		}

		@Override
		public AccountApplicationCounterMapReduce build() {
			AccountApplicationCounterMapReduce built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ApplicationAccountMapReduce */
	private AccountApplicationCounterMapReduce() {
	}

	public Mapper<Key, ApplicationKey, AccountId> applicationKeyAccountIdMapper() {
		return new Mapper<Key, ApplicationKey, AccountId>() {

			private static final long serialVersionUID = -8024485795537563499L;

			@Override
			public void map(Key rawKey) {
				final AccountId accountId = AccountDatastoreUtil.accountId(rawKey);
				emit(accountId.getApplicationKey(), accountId);
			}
		};
	}

	public Reducer<ApplicationKey, AccountId, Entity> applicationAccountIdReducer() {
		return new Reducer<ApplicationKey, AccountId, Entity>() {

			private static final long serialVersionUID = 940903891849091043L;

			@Override
			public void reduce(ApplicationKey key, ReducerInput<AccountId> accountIds) {
				int count = Iterators.size(accountIds);
				Entity entity = FieldPropertyEntityUtil.entity(key, accountCountDefinition, count);
				emit(entity);
			}
		};
	}

	private static final long serialVersionUID = -7703688594658244515L;
}
