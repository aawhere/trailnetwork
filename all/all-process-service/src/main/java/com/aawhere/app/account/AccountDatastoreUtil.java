/**
 * 
 */
package com.aawhere.app.account;

import com.google.appengine.api.datastore.Key;

/**
 * @author aroller
 * 
 */
public class AccountDatastoreUtil {

	/**
	 * Datastore key in and accountId out. No validation is done.
	 * 
	 * @param key
	 * @return
	 */
	public static AccountId accountId(Key key) {
		String name = key.getName();
		return new AccountId(name);
	}

}
