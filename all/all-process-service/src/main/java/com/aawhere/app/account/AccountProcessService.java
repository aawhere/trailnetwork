/**
 * 
 */
package com.aawhere.app.account;

import com.aawhere.all.process.MapReduceJobSettings;
import com.aawhere.app.ApplicationField;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationSummaryField;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.person.PersonService;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.tools.mapreduce.MapReduceJob;
import com.google.appengine.tools.mapreduce.MapReduceSpecification;
import com.google.appengine.tools.mapreduce.inputs.DatastoreKeyInput;
import com.google.appengine.tools.mapreduce.outputs.DatastoreOutput;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Collects and aggregates information found in {@link Accounts}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class AccountProcessService {

	@Inject
	private static FieldDictionaryFactory fieldDictionaryFactory;
	@Inject
	private static PersonService personService;

	/**
	 * 
	 */
	public AccountProcessService() {
	}

	/**
	 * Demonstrates counting accounts using keys only.
	 * 
	 * @return
	 */
	public String accountsPerApplication() {
		MapReduceSpecification.Builder<Key, ApplicationKey, AccountId, Entity, Void> specBuilder = new MapReduceSpecification.Builder<Key, ApplicationKey, AccountId, Entity, Void>();
		specBuilder.setInput(new DatastoreKeyInput(Account.class.getSimpleName(), 1));
		final FieldDefinition<Integer> definition = fieldDictionaryFactory
				.getDefinition(ApplicationField.Key.ACCOUNT_COUNT);
		AccountApplicationCounterMapReduce accountMapReduce = AccountApplicationCounterMapReduce.create()
				.accountCountDefinition(definition).build();
		specBuilder.setMapper(accountMapReduce.applicationKeyAccountIdMapper());
		specBuilder.setReducer(accountMapReduce.applicationAccountIdReducer());
		specBuilder.setOutput(new DatastoreOutput());
		specBuilder.setNumReducers(1);
		specBuilder.setJobName(ApplicationSummaryField.Absolute.ACCOUNT_COUNT);
		return MapReduceJob.start(specBuilder.build(), MapReduceJobSettings.create().build().settings());
	}

	/**
	 * @return the personService
	 */
	public static PersonService personService() {
		return personService;
	}

	/**
	 * @return
	 */
	public static FieldDictionaryFactory fieldDictionaryFactory() {
		return fieldDictionaryFactory;
	}
}
