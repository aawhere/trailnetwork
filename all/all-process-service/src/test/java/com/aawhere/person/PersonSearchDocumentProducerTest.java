/**
 *
 */
package com.aawhere.person;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityType;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.Assertion;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.route.Route;
import com.aawhere.route.RouteCompletion;
import com.aawhere.route.RouteCompletionRepository;
import com.aawhere.route.RouteCompletionServiceTestUtil;
import com.aawhere.route.RouteCompletionTestUtil;
import com.aawhere.route.RouteServiceTestUtil;
import com.aawhere.route.RouteTestUtils;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchTestUtil;
import com.aawhere.search.SearchUtils;
import com.aawhere.test.category.ServiceTest;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Iterables;

/**
 * @author Brian Chapman
 * 
 */
@Category(ServiceTest.class)
public class PersonSearchDocumentProducerTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private FieldAnnotationDictionaryFactory dictionaryFactory;
	private Person person;
	private RouteCompletion completion1;
	private RouteCompletion completion2;
	RouteCompletionRepository completionRepository;

	@Before
	public void setup() {
		helper.setUp();
		dictionaryFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		person = PersonTestUtil.personWithId();
		Route route = RouteTestUtils.createRouteWithId();
		completion1 = RouteCompletionTestUtil.withActivityType(ActivityType.BIKE).person(person).route(route).build();
		completion2 = RouteCompletionTestUtil.withActivityType(ActivityType.BIKE).person(person).route(route).build();

		completionRepository = RouteCompletionServiceTestUtil.create().routeUtil(RouteServiceTestUtil.create().build())
				.build().repository();
		completionRepository.store(completion1);
		completionRepository.store(completion2);
	}

	@After
	public void tearDown() {
		helper.tearDown();
	}

	@Test
	public void testProducer() {

		PersonSearchDocumentProducer producer = PersonSearchDocumentProducer.create()
				.dictionaryFactory(dictionaryFactory).routeCompletionRepository(completionRepository).build();

		SearchDocument doc = producer.create(person);
		assertNotNull(doc);
		SearchTestUtil.assertContains(SearchUtils.findFields(doc, "person-nameWithAliases"), person.nameWithAliases());
		SearchTestUtil.assertContains(SearchUtils.findFields(doc, "routeCompletion-routeId"), completion1.routeId());
		SearchTestUtil.assertContains(SearchUtils.findFields(doc, "routeCompletion-routeId"), completion2.routeId());
		SearchTestUtil.assertContains(SearchUtils.findFields(doc, "routeCompletion-routeIdActivityType"), completion2
				.routeId().getValue().toString()
				+ "-" + completion2.activityType());

		assertTrue(Iterables.size(doc.fields()) > 4);
	}

	@Test
	public void testSearchDocumentFieldEquals() {
		SearchDocument.Field field1 = SearchDocument.Field.create().key("field1").dataType(FieldDataType.ATOM)
				.value("value1").build();
		SearchDocument.Field field2 = SearchDocument.Field.create().key("field1").dataType(FieldDataType.ATOM)
				.value("value1").build();
		SearchDocument.Field field3 = SearchDocument.Field.create().key("field1").dataType(FieldDataType.ATOM)
				.value("value2").build();

		assertTrue(field1.equals(field2));
		assertTrue(!field1.equals(field3));
	}

	@Test
	public void testProducerForPersonWithNoCompletions() {
		Person personWithNoCompletions = PersonTestUtil.personWithId();

		PersonSearchDocumentProducer producer = PersonSearchDocumentProducer.create()
				.dictionaryFactory(dictionaryFactory).routeCompletionRepository(completionRepository).build();

		SearchDocument doc = producer.create(personWithNoCompletions);
		assertNotNull(doc);
		SearchTestUtil.assertContains(	SearchUtils.findFields(doc, "person-nameWithAliases"),
										personWithNoCompletions.nameWithAliases());
		assertTrue(SearchUtils.findFields(doc, "routeCompletion-routeId").isEmpty());
		assertTrue(SearchUtils.findFields(doc, "routeCompletion-routeIdActivityType").isEmpty());
	}

	@Test
	public void testNullActivityType() {
		Person person = PersonTestUtil.personWithId();
		RouteCompletion completion1 = RouteCompletionTestUtil.routeCompletion();
		completion1 = RouteCompletionTestUtil.mutate(completion1).activityTypeReported(ActivityType.MTB).build();

		PersonSearchDocumentProducer producer = PersonSearchDocumentProducer.create()
				.dictionaryFactory(dictionaryFactory).routeCompletionRepository(completionRepository).build();

		SearchDocument doc = producer.create(person);
		assertNotNull(doc);
		Assertion.exceptions().assertEmpty(	"RouteIdActivityType",
											SearchUtils.findFields(doc, "routeCompletion-routeIdActivityType"));
	}
}
