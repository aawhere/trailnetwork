/**
 *
 */
package com.aawhere.person;

import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityType;
import com.aawhere.app.account.Account;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.queue.QueueTestUtil;
import com.aawhere.route.RouteCompletion;
import com.aawhere.route.RouteCompletionRepository;
import com.aawhere.route.RouteCompletionServiceTestUtil;
import com.aawhere.route.RouteCompletionTestUtil;
import com.aawhere.route.RouteServiceTestUtil;
import com.aawhere.search.FieldNotSearchableException;
import com.aawhere.search.GaeSearchTestUtils;
import com.aawhere.search.MaxIndexAttemptsExceeded;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocuments;
import com.aawhere.search.SearchService;
import com.aawhere.search.SearchWebApiUri;
import com.aawhere.search.UnknownIndexException;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ServiceTest;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Lists;

/**
 * @author Brian Chapman
 * 
 */
@Category(ServiceTest.class)
public class PersonProcessServiceTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private Person person;
	private RouteCompletion completion1;
	private RouteCompletion completion2;
	private RouteCompletionRepository completionRepository;
	private PersonSearchDocumentProducer producer;
	private SearchService searchService;
	private PersonService personService;
	private PersonServiceTestUtil personTestUtil;
	private PersonProcessService personProcessService;

	@Before
	public void setup() {
		helper.setUp();
		personTestUtil = new PersonServiceTestUtil.Builder().build();
		person = PersonTestUtil.person();
		personService = PersonServiceTestUtil.create().build().getPersonService();
		personService.store(person);
		completion1 = RouteCompletionTestUtil.withActivityType(ActivityType.BIKE).person(person).build();
		completion2 = RouteCompletionTestUtil.withActivityType(ActivityType.BIKE).person(person).build();

		completionRepository = RouteCompletionServiceTestUtil.create().routeUtil(RouteServiceTestUtil.create().build())
				.build().repository();
		completionRepository.store(completion1);
		completionRepository.store(completion2);
		FieldAnnotationDictionaryFactory dictionaryFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		producer = PersonSearchDocumentProducer.create().dictionaryFactory(dictionaryFactory)
				.routeCompletionRepository(completionRepository).build();
		searchService = GaeSearchTestUtils.createGaeSearchService(dictionaryFactory);
		personProcessService = new PersonProcessService(producer, searchService, personTestUtil.getQueueFactory(),
				personService);
	}

	@After
	public void tearDown() {
		helper.tearDown();
	}

	@Test
	public void testIndexAll() throws MaxIndexAttemptsExceeded {

		personProcessService.indexAllPeople();
		QueueTestUtil.assertQueueContains(	personTestUtil.getQueueFactory()
													.getQueue(SearchWebApiUri.SEARCH_QUEUE_NAME),
											1,
											person.id().getValue().toString());
	}

	@Test
	public void testIndexPerson() throws MaxIndexAttemptsExceeded, EntityNotFoundException,
			FieldNotSearchableException, UnknownIndexException {

		final String firstName = "Aaron";
		final String rolllerLastName = "Roller";
		final String name1 = firstName + " " + rolllerLastName;
		final String alias1 = "aroller";
		final String name2 = firstName + " Robber";
		final String alias2 = "arobber";
		Pair<Person, Account> roller = this.personTestUtil.persistedPersonWithNameAndAlias(name1, alias1);
		Pair<Person, Account> robber = this.personTestUtil.persistedPersonWithNameAndAlias(name2, alias2);

		// ensure query in filter is calling the same..this one tests proves
		// that account is
		// being
		// queried
		SearchDocument rollerDocument = producer.create(roller.getLeft(), Lists.newArrayList(roller.getRight()));
		SearchDocument robberDocument = producer.create(robber.getLeft(), Lists.newArrayList(robber.getRight()));
		searchService.index(rollerDocument);
		searchService.index(robberDocument);

		final SearchDocuments peopleMatchingLastName = personProcessService.search(Filter.create()
				.setQuery(rolllerLastName).build());
		TestUtil.assertContainsOnly(rollerDocument, peopleMatchingLastName);

		// TODO: WW-237 removing while implementing thin slice.
		String queryRequiringAlias = alias1;
		final SearchDocuments personsMatchingAlias = personProcessService.search(Filter.create()
				.setQuery(queryRequiringAlias).build());
		TestUtil.assertContainsOnly(rollerDocument, personsMatchingAlias);
		Filter responseFilter = personsMatchingAlias.getFilter();
		TestUtil.assertContains(responseFilter.getQuery(), queryRequiringAlias);
		final String noMatch = "Jesus";
		TestUtil.assertEmpty(searchService.query(Filter.create().setQuery(noMatch).build(), Person.FIELD.DOMAIN));
	}

	@Test
	public void testPersonWithNoCompletions() throws MaxIndexAttemptsExceeded, FieldNotSearchableException,
			UnknownIndexException {
		Person personWithNoCompletions = PersonTestUtil.personWithId();
		SearchDocument documentPersonWithNoCompletions = producer.create(personWithNoCompletions);
		searchService.index(documentPersonWithNoCompletions);

		SearchDocuments searchResults = personProcessService.search(Filter.create()
				.setQuery(personWithNoCompletions.nameWithAliases()).build());
		assertTrue(searchResults.size() == 1);
	}

}
