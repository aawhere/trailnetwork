/**
 * 
 */
package com.aawhere.geo;

import org.junit.Test;

import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class GeoCellSummaryUtilUnitTest {

	@Test
	public void testSummaries() {
		final GeoCells expected = GeoCellTestUtil.A_THRU_F_WITH_PARENTS;
		GeoCellSummaries summaries = GeoCellSummaryUtil.summaries(expected);
		TestUtil.assertIterablesEquals("summaries", expected, GeoCellSummaryUtil.geoCells(summaries));
	}

	@Test
	public void testSummariesExplodeTo() {
		GeoCellSummaries summaries = GeoCellSummaryUtil.summaries(	GeoCellUtil.geoCells(GeoCellTestUtil.A),
																	Resolution.TWO);
		TestUtil.assertSize(GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL, summaries);
	}

}
