/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.persist.AppEngineLocalServiceBaseTest;
import com.aawhere.persist.DatastoreKeyUtil;
import com.aawhere.persist.DatastoreUtil;
import com.aawhere.test.TestUtil;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;

/**
 * @author aroller
 * 
 */
public class ActivityDatastoreUtilUnitTest
		extends AppEngineLocalServiceBaseTest {

	private ActivityServiceTestUtil activityServiceTestUtil;
	private FieldDictionaryFactory fieldDefinitionFactory;
	private Activity activity;
	private ActivityId activityId;
	private Entity entity;

	@Before
	public void setUp() throws EntityNotFoundException {
		activityServiceTestUtil = ActivityServiceTestUtil.create().build();
		fieldDefinitionFactory = activityServiceTestUtil.getRepository().dictionaryFactory();
		this.activity = activityServiceTestUtil.createPersistedActivity();
		this.activityId = activity.id();
		this.entity = DatastoreServiceFactory.getDatastoreService().get(DatastoreKeyUtil.key(activityId));
	}

	@Test
	public void testActivityId() throws EntityNotFoundException {
		final ActivityId expected = activity.id();
		ActivityId actual = ActivityDatastoreUtil.activityId(entity);
		assertEquals(expected, actual);
		DatastoreUtil.value(entity, fieldDefinitionFactory.getDefinition(ActivityField.Key.ID));
	}

	@Test
	public void testSpatialBuffer() {
		List<String> cellStrings = ActivityDatastoreUtil.spatialBufferGeoCellValuesList(entity, fieldDefinitionFactory);
		assertNotNull(cellStrings);
		TestUtil.assertCollectionEquals(activity.getTrackSummary().getSpatialBuffer().getAll(),
										GeoCellUtil.convertToGeoCellsSet(cellStrings));
	}
}
