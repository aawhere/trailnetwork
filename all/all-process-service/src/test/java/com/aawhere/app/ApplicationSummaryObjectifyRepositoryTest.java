/**
 * 
 */
package com.aawhere.app;

import org.junit.After;
import org.junit.Before;

import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author aroller
 * 
 */
public class ApplicationSummaryObjectifyRepositoryTest
		extends ApplicationSummaryUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();
	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.app.ApplicationSummaryUnitTest#createRepository()
	 */
	@Override
	protected ApplicationSummaryRepository createRepository() {
		return new ApplicationSummaryObjectifyRepository(Synchronization.SYNCHRONOUS);
	}
}
