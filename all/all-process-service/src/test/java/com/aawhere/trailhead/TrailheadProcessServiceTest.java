package com.aawhere.trailhead;

import org.junit.After;
import org.junit.Before;

import com.aawhere.persist.GaePersistTestUtils;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class TrailheadProcessServiceTest {

	private TrailheadService trailheadService;
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private TrailheadServiceTestUtil util;

	@Before
	public void setUpServices() {
		helper.setUp();
		this.util = TrailheadServiceTestUtil.create().build();
		trailheadService = this.util.service();
		TrailheadProcessService.inject(trailheadService);
		TrailheadProcessService.inject(this.util.repository());

	}

	@After
	public void tearDownServices() {
		helper.tearDown();
	}

}
