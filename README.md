# Welcome to the TrailNetwork!!!

#Development Tools and Environment Setup

Instructions about how to setup the tools necessary for development are found in each section.  This is the recommended order of installation.

1. Bitbucket
1. Git
1. Java
1. Maven
1. Eclipse

#Bitbucket

The version control system.  You just need an account and access to the project and you can proceed with Git.

#Git

* Install [GIT](http://git-scm.com/) appropriate for you environment. 
* [SourceTree](http://www.sourcetreeapp.com/) is the recommended GUI.
* Clone the repository (available at the top of this page)
     * Recommended local destination */Users/{usersname}/dev/project/tn*
* Checkout the *dev* branch where all work is done. 
     * Issue branches are recommended and are done off the *dev* branch


#Eclipse

Eclipse Luna is the latest 
##Code Formatter

Using a common format is important allowing developers to merge files without whitespace conflicts.

`Eclipse Preferences->Java->Code Style->Formatter` then the `Import` button and choose `tools/src/main/resources/eclipse/formatter.xml`.  This allows you to CTRL+SHIFT+F resulting in a standard format across all developers that matches the maven formatter.
 
##Code Templates
 
 We use patterns that are best created by code templates in Eclipse.
 
`Eclipse Preferences->Java->Code Style->Code Templates` then the `Import` button and choose `tools/src/main/resources/eclipse/templates.xml`

1.  Builder - The builder pattern we use
2.  BuilderMutator - quick way to add properties 
3.  BuilderAssertion - quick way to add Assertions in the Builder validator
4.  Message - Enumeration used for Localization
 
##Eclipse Plugins

Google Plugins are not necessary and not recommended.  Although it is desirable to have Eclipse run the server for debugging, the plugin can cause problems that can make the project building misbehave.  All App Engine functions are available using Maven.

#Java Development Kit

**Version 1.7.0_71 is the recommended version**
JDK 8 is not supported until [Google App Engine supports it](https://code.google.com/p/googleappengine/issues/detail?id=9537). 

#Maven Builds

[Maven](http://maven.apache.org/) manages all builds.

## Maven Installation

Download and install version 3.2.5. Follow the installation in the README.txt included.

* Verify mvn --version 

```
Apache Maven 3.2.5 (12a6b3acb947671f09b81f49094c53f426d8cea1; 2014-12-14T18:29:23+01:00)
Maven home: /usr/local/apache-maven-3.2.5
Java version: 1.7.0_71, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.7.0_71.jdk/Contents/Home/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "mac os x", version: "10.10.1", arch: "x86_64", family: "mac"

```

##Maven Configurations

copy all files in [src/main/resources/maven/dot-m2] to your .m2 directory in your user profile on your computer.  That will give you access to Nexus repository for library downloads.

`cp -r tools/src/main/resources/maven/dot-m2/ ~/.m2`

##Dependency Management

- All dependencies are managed at the root pom.
- Repositorys are managed by Nexus.  Never add a repository to any pom.

##Nexus Repository Manager

Manages access to all maven libraries.  Access is managed by one of the administrators.  Snapshot and releases are deployed by Bamboo.

## Running the Build

	{trailnetwork root}/mvn install

The reactor projects will run along with unit tests and produce a success.  To run all the projects with all integration tests,

       mvn install -Dcomplete

The TrailNetwork project keeps it's documentation in Confluence, although the document regarding the development environment is moving to this [README](https://bitbucket.org/aawhere/trailnetwork "Bitbucket") . 


#More Information

See [TrailNetwork WIKI](https://aawhere.jira.com/wiki/display/AAWHERE/TrailNetwork).

Some good places to start include:

*  [Coding Standards](https://aawhere.jira.com/wiki/display/AAWHERE/Coding)
*  [Development Environment (Old)](https://aawhere.jira.com/wiki/display/AAWHERE/Development+Environment) (See child pages)
*  [TrailNetwork](https://aawhere.jira.com/wiki/display/AAWHERE/TrailNetwork)
*  [CommonTrack](https://aawhere.jira.com/wiki/display/AAWHERE/CommonTrack) (CommonTrack and TrailNetwork are essentially the same. These two links may be merged into one someday).
