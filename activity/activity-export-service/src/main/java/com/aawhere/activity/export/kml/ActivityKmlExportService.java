/**
 * 
 */
package com.aawhere.activity.export.kml;

import static com.aawhere.activity.export.kml.ActivityKmlUtil.*;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.KmlType;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportException;
import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityService;
import com.aawhere.activity.timing.ActivityTimingId;
import com.aawhere.activity.timing.ActivityTimingService;
import com.aawhere.activity.timing.ActivityTimingRelationUtil;
import com.aawhere.activity.timing.export.kml.ActivityTimingRelationUserStoryKml;
import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.track.Track;
import com.aawhere.track.TrackIncapableDocumentException;
import com.aawhere.track.match.CourseAttemptDeviationProcessor;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Ties together activity, it's relationships and {@link KmlType} for export purposes.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityKmlExportService {

	private ActivityService activityService;
	private ActivityImportService activityImportService;
	private ActivityTimingService timingService;

	/**
	 * 
	 */
	@Inject
	public ActivityKmlExportService(ActivityTimingService timingService, ActivityService activityService,
			ActivityImportService activityImportService) {
		this.activityService = activityService;
		this.activityImportService = activityImportService;
		this.timingService = timingService;
	}

	public KmlDocument filterActivities(Filter filter) {
		Activities activities = activityService.getActivities(filter);
		return activitiesKml(activities);
	}

	/**
	 * Provides the complete view for an Activity in KML format.
	 * 
	 * @param activityId
	 * @return
	 * @throws TrackIncapableDocumentException
	 * @throws EntityNotFoundException
	 * @throws ActivityImportException
	 */
	public JAXBElement<KmlType> getActivityDocument(ActivityId activityId) throws EntityNotFoundException,
			ActivityImportException {
		ActivityKml activityKml = getActivityKml(activityId);
		KmlDocument kmlDocument = KmlDocument.create().setMain(activityKml.getElement()).build();
		return kmlDocument.getElement();

	}

	/**
	 * @param activityId
	 * @return
	 * @throws ActivityImportException
	 * @throws EntityNotFoundException
	 */
	private ActivityKml getActivityKml(ActivityId activityId) throws ActivityImportException, EntityNotFoundException {
		Activity activity = activityImportService.getActivity(activityId);
		Track track = activityService.getTrack(activityId);
		ActivityKml activityKml = activityTrack(activity, track);
		return activityKml;
	}

	public KmlDocument getActivitiesDocument(Iterable<ActivityId> activityIds) throws ActivityImportException,
			EntityNotFoundException {
		FolderType folder = KmlUtil.FACTORY.createFolderType();

		for (ActivityId activityId : activityIds) {
			ActivityKml activityKml = getActivityKml(activityId);
			folder.getAbstractFeatureGroup().add(activityKml.getElement());
		}
		return new KmlDocument.Builder().setMain(KmlUtil.FACTORY.createFolder(folder)).build();

	}

	/**
	 * Provides the Animated KML for a given course against a given attempt.
	 * 
	 * @see CourseAttemptDeviationProcessor
	 * @return
	 * @throws EntityNotFoundException
	 * @throws TrackIncapableDocumentException
	 */
	public KmlDocument getCourseAttemptDeviationKml(ActivityId courseId, ActivityId attemptId)
			throws EntityNotFoundException {
		ActivityTimingRelationUserStoryKml kmlStory = (ActivityTimingRelationUserStoryKml) timingService
				.configureTimingsUserStory(courseId, attemptId, ActivityTimingRelationUserStoryKml.create());

		return kmlStory.getKmlDocument();

	}

	/**
	 * Provides the KML when the Timing ID is known.
	 * 
	 * @see #getCourseAttemptDeviationKml(ActivityId, ActivityId)
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	public KmlDocument getCourseAttemptDeviationKml(ActivityTimingId id) throws EntityNotFoundException {
		Pair<ActivityId, ActivityId> split = ActivityTimingRelationUtil.split(id);
		return getCourseAttemptDeviationKml(split.getLeft(), split.getRight());
	}

	/**
	 * @see ActivityKmlUtil#activitiesKml(Activities)
	 * @see ActivityService#getActivities(GeoCells, Filter)
	 * @param cells
	 * @param filter
	 * @return
	 */
	public KmlDocument getActivities(GeoCells cells, Filter filter) {
		return activitiesKml(activityService.getActivities(cells, filter));
	}
}
