/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.activity.imprt.csv.CsvActivityDocumentProducer;
import com.aawhere.activity.imprt.everytrail.EveryTrailDocumentUtil;
import com.aawhere.activity.imprt.mmf.MapMyFitnessDocumentProducer;
import com.aawhere.activity.imprt.rwgps.RWGpsDocumentProducers;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.SimpleDocumentProducer;
import com.aawhere.doc.garmin.activity.GarminConnectActivitySearchDocumentProducer;
import com.aawhere.doc.track.axm_1_0.AxmDocumentProducer;
import com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer;
import com.aawhere.doc.track.gpx._1._1.GpxxDocumentProducer;
import com.aawhere.track.TrackFormatXmlAdapterFactory;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
public class ActivityExportModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(TrackFormatXmlAdapterFactory.class).toProvider(TrackFormatXmlAdapterFactory.Provider.class)
				.in(Singleton.class);

	}

	@Provides
	public DocumentFactory getImportDocumentFactory(DeviceCapabilitiesRepository deviceCapabilitiesRepository) {
		final DocumentFactory.Builder factoryBuilder = new DocumentFactory.Builder();
		MapMyFitnessDocumentProducer.registerAll(factoryBuilder, deviceCapabilitiesRepository);
		EveryTrailDocumentUtil.registerAll(factoryBuilder);
		GarminConnectActivitySearchDocumentProducer.registerAll(factoryBuilder);
		RWGpsDocumentProducers.registerAll(factoryBuilder);
		DocumentFactory factory = factoryBuilder
				.register(new CsvActivityDocumentProducer.Builder().build())
				.register(new GpxDocumentProducer.Builder().build())
				// GPX 1.0 is covered by gpxx.register(new Gpx10DocumentProducer())
				.register(GpxxDocumentProducer.createGpxx().build())
				.register(ActivityExportUtil.gpolylineDocumentProducer())
				.register(new AxmDocumentProducer.Builder().build())
				// Google maps TN-732
				.register(new SimpleDocumentProducer(DocumentFormatKey.MAP_THUMBNAIL_PNG)).build();
		return factory;
	}
}
