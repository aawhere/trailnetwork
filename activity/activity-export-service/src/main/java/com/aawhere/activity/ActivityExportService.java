/**
 * 
 */
package com.aawhere.activity;

import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.doc.archive.ArchivedDocumentUtil;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.doc.text.TextDocument;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.track.Track;
import com.aawhere.track.TrackService;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.process.TrackProcessorUtil;
import com.aawhere.track.story.GoogleMapEncodedPathUserStory;
import com.aawhere.track.story.TrackUserStories;
import com.aawhere.track.story.TrackUserStories.Builder;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Provides a variety of export formats to satisfy the needs of Activity API formats.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityExportService {

	private ActivityService activityService;
	private TrackService trackService;

	@Inject
	public ActivityExportService(ActivityService activityService, TrackService trackService) {
		this.activityService = activityService;
		this.trackService = trackService;
	}

	/**
	 * Provided the document id this will retrieve the track from the document of interest, create
	 * the gpolyline and store
	 * 
	 * WARNING: This provides no efficiency in caching and will generate the polyline every time.
	 * Add some if this becomes a production level feature.
	 * 
	 * @param docId
	 * @param applicationDataId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public String googleMapsPolylineEncodedPath(ArchivedDocumentId docId) throws EntityNotFoundException {
		String gpolyline;
		Builder storiesBuilder = TrackUserStories.create(trackService.getTrack(docId));
		GoogleMapEncodedPathUserStory googleMapStories = storiesBuilder.addStandardFilters()
				.setUserStory(GoogleMapEncodedPathUserStory.create());
		storiesBuilder.build();
		gpolyline = googleMapStories.getResult();
		return gpolyline;
	}

	/**
	 * Provides the polylines for tracks identified by the archived document id. The tracks will
	 * come from the cache if they already exist there. All tracks are transformed into their
	 * polylines. Tracks not found by id are not reported.
	 * 
	 * @param docIds
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Map<ArchivedDocumentId, String> googleMapsPolylineEncodedPaths(Iterable<ArchivedDocumentId> docIds) {
		Map<ArchivedDocumentId, Track> tracks = trackService.getTracks(docIds);
		// tracks can come back null, but now is a good time to stop.
		tracks = Maps.filterValues(tracks, Predicates.notNull());
		return Maps.transformValues(tracks, TrackProcessorUtil.gpolylineFromTrackFunction());
	}

	/**
	 * @return
	 */
	private DocumentService documentService() {
		return this.trackService.getDocumentService();
	}

	/**
	 * @see #googleMapsPolylineEncodedPath(String, ActivityId)
	 * @param activityId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public String gpolyline(ActivityId activityId) throws EntityNotFoundException {
		Map<ActivityId, String> resultMap = gpolylines(Lists.newArrayList(activityId));
		if (resultMap.isEmpty()) {
			// confusing message if the archived document is actually missing
			throw new EntityNotFoundException(activityId);
		} else {
			return resultMap.get(activityId);
		}
	}

	/**
	 * The master of all polyline management this will look for archived documents that may have
	 * already been stored. For those not stored this will create the polyline and store them for
	 * the next call. The result being a map of the given activity id to the polyline that
	 * represents it via the {@link TrackSummary#getDocumentId()}.
	 * 
	 * If, for any reason, a document cannot be found or created then it is logged and the
	 * successful items are returned. A {@link Sets#difference(java.util.Set, java.util.Set)} can be
	 * done on those {@link ActivityId}s given and returned to find out what's missing if there is a
	 * size discrepancy. This shouldn't happen in a healthy system.
	 * 
	 * @param activityIds
	 * @return
	 */
	public Map<ActivityId, String> gpolylines(Iterable<ActivityId> activityIds) {
		// acts as a depleting queue to ensure all are retrieved or created.
		ImmutableSet<ActivityId> activityIdsSet = ImmutableSet.copyOf(activityIds);

		Iterable<ArchivedDocument> archivedDocuments = documentService()
				.getArchivedDocuments(activityIdsSet, DocumentFormatKey.GPOLYLINE);

		// TN-855 There is no guarantee that activityId and archivedDocument must be unique
		ImmutableListMultimap<ActivityId, ArchivedDocument> trackArchivedDocuments = Multimaps
				.index(archivedDocuments, ActivityUtil.archivedDocumentApplicationDataIdFunction());
		ListMultimap<ActivityId, String> storedPolylines = Multimaps
				.transformValues(trackArchivedDocuments, ArchivedDocumentUtil.textArchivedDocumentFunction());

		ImmutableMap.Builder<ActivityId, String> results = ImmutableMap.builder();
		for (ActivityId storedPolylineActivityId : storedPolylines.keySet()) {
			// just get the first. duplicates cause problems
			results.put(storedPolylineActivityId, storedPolylines.get(storedPolylineActivityId).get(0));
		}

		SetView<ActivityId> notStoredActivityIds = Sets.difference(activityIdsSet, storedPolylines.keySet());
		// all found have been removed so now let's create those not found.
		if (!notStoredActivityIds.isEmpty()) {
			// keep only those keys that are missing, but still mapped to the activity
			BiMap<ActivityId, ArchivedDocumentId> activityIdsForDocumentIds = activityService
					.trackArchivedDocumentIds(notStoredActivityIds);
			Set<ArchivedDocumentId> missingDocumentIds = activityIdsForDocumentIds.values();
			// create polyline paths in bulk
			Map<ArchivedDocumentId, String> polylinesForDocuments = googleMapsPolylineEncodedPaths(missingDocumentIds);
			Map<ActivityId, String> createdPolylines = CollectionUtilsExtended.swapKeys(activityIdsForDocumentIds,
																						polylinesForDocuments);
			results.putAll(createdPolylines);

			// store those polylines just created for later retrieval
			{
				Map<ActivityId, TextDocument> polylineDocumentsForActivities = Maps
						.transformValues(createdPolylines, polylineTextDocumentFunction());
				final DocumentService documentService = documentService();
				documentService.createDocuments(polylineDocumentsForActivities);

			}
			// log missing
			{
				SetView<ActivityId> documentsNotFound = Sets
						.difference(notStoredActivityIds, createdPolylines.keySet());
				// we didn't find all, but we did all we could so tell someone that might be looking
				if (!documentsNotFound.isEmpty()) {
					logger().warning(activityIdsSet + " were requested, but not able to find or create polylines");
				}
			}
		}

		return results.build();
	}

	/**
	 * Transforms the given polyline into the standard polyline text document to be archived.
	 * 
	 * @param polyline
	 * @return
	 */
	private Function<String, TextDocument> polylineTextDocumentFunction() {
		return new Function<String, TextDocument>() {

			@Override
			public TextDocument apply(String polyline) {
				return new TextDocument(polyline, DocumentFormatKey.GPOLYLINE);
			}
		};
	}

	/**
	 * @return
	 */
	private Logger logger() {
		return LoggerFactory.getLogger(getClass());
	}

}
