/**
 * 
 */
package com.aawhere.track;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.ActivityExportService;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Produces XmlAdapters matching the given media type that will transform a track document into a
 * formatted track.
 * 
 * @author aroller
 * 
 */
@Singleton
public class TrackFormatXmlAdapterFactory {

	/**
	 * Used to construct all instances of TrackFormatXmlAdapterFactory.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<TrackFormatXmlAdapterFactory> {

		public Builder() {
			super(new TrackFormatXmlAdapterFactory());
		}

		public Builder put(TrackFormatXmlAdapter<?> adapter) {
			Set<MediaType> acceptsMediaType = adapter.acceptsMediaType();
			for (MediaType mediaType : acceptsMediaType) {
				building.adapters.put(mediaType.toString(), adapter);
			}
			return this;
		}

		@Override
		public TrackFormatXmlAdapterFactory build() {
			TrackFormatXmlAdapterFactory built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct TrackFormatXmlAdapterFactory */
	private TrackFormatXmlAdapterFactory() {
	}

	public static Builder create() {
		return new Builder();
	}

	private Map<String, TrackFormatXmlAdapter<?>> adapters = new HashMap<>();

	public TrackFormatXmlAdapter<?> get(MediaType mediaType) {
		return get(mediaType.toString());
	}

	public TrackFormatXmlAdapter<?> get(String mediaType) {
		return adapters.get(mediaType);
	}

	public static class Provider
			implements com.google.inject.Provider<TrackFormatXmlAdapterFactory> {

		private TrackFormatXmlAdapterFactory factory;

		/**
		 * 
		 */
		@Inject
		public Provider(GoogleMapsEncodedPolylineTrackFormatXmlAdapter polylineEncoder,
				MultiLineStringTrackFormatXmlAdapter multiLineStringAdapter) {
			Builder builder = create();
			builder.put(polylineEncoder);
			builder.put(multiLineStringAdapter);
			this.factory = builder.build();
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.inject.Provider#get()
		 */
		@Override
		public TrackFormatXmlAdapterFactory get() {
			return this.factory;
		}

	}

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class StringFormat {

		/**
		 * required for JAXB
		 * 
		 */
		StringFormat() {
			this.id = null;
			this.mediaType = null;
			this.details = null;
		}

		/**
		 * 
		 */
		public StringFormat(ArchivedDocumentId id, String mediaType, String details) {
			this.id = id;
			this.mediaType = mediaType;
			this.details = details;
		}

		public final ArchivedDocumentId id;
		public final String mediaType;
		public final String details;
	}
}
