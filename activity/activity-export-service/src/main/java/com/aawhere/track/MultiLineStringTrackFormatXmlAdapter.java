/**
 * 
 */
package com.aawhere.track;

import java.util.Set;

import javax.ws.rs.core.MediaType;

import com.aawhere.activity.ActivityTrackGeometryService;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.jts.geom.WktContentType;
import com.aawhere.track.TrackFormatXmlAdapterFactory.StringFormat;

import com.google.appengine.labs.repackaged.com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class MultiLineStringTrackFormatXmlAdapter
		extends TrackFormatXmlAdapter<StringFormat> {

	@Inject
	private ActivityTrackGeometryService geometryService;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.TrackFormatXmlAdapter#acceptsMediaType()
	 */
	@Override
	public Set<MediaType> acceptsMediaType() {
		return Sets.newHashSet(	WktContentType.WELL_KNOWN_TEXT,
								WktContentType.MULTI_LINE_STRING,
								WktContentType.LINE_STRING);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.TrackFormatXmlAdapter#marshal(com.aawhere.doc.archive.ArchivedDocumentId)
	 */
	@Override
	public StringFormat marshal(ArchivedDocumentId id) throws Exception {
		return new StringFormat(id, WktContentType.MULTI_LINE_STRING_MEDIA_TYPE, geometryService
				.getTrackMultiLineString(id).toText());
	}

}
