/**
 * 
 */
package com.aawhere.track;

import static com.aawhere.web.api.geo.GeoWebApiUri.*;

import java.util.Set;

import javax.ws.rs.core.MediaType;

import com.aawhere.activity.ActivityExportService;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.track.TrackFormatXmlAdapterFactory.StringFormat;

import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Given a document id this will provide the track formatted in Google Encoded polyline style.
 * 
 * This is intended to reprsent the "document" in a format that the consumer would expect to see.
 * 
 * @see ActivityExportService#googleMapsPolylineEncodedPath(ArchivedDocumentId)
 * 
 * @author aroller
 * 
 */
@Singleton
public class GoogleMapsEncodedPolylineTrackFormatXmlAdapter
		extends TrackFormatXmlAdapter<StringFormat> {

	@Inject
	private ActivityExportService service;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.TrackFormatXmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public ArchivedDocumentId unmarshal(StringFormat v) throws Exception {
		return (v == null) ? null : v.id;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public StringFormat marshal(ArchivedDocumentId id) throws Exception {
		return (id == null) ? null : new StringFormat(id, POLYLINE_ENCODED_MEDIA_TYPE,
				service.googleMapsPolylineEncodedPath(id));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.TrackFormatXmlAdapter#acceptsMediaType()
	 */
	@Override
	public Set<MediaType> acceptsMediaType() {
		return Sets.newHashSet(POLYLINE_ENCODED);
	}

}
