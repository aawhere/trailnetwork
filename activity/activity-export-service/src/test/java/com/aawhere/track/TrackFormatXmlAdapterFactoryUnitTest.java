/**
 * 
 */
package com.aawhere.track;

import static org.junit.Assert.*;

import javax.ws.rs.core.MediaType;

import org.junit.Test;

import com.aawhere.track.GoogleMapsEncodedPolylineTrackFormatXmlAdapter;
import com.aawhere.track.TrackFormatXmlAdapter;
import com.aawhere.track.TrackFormatXmlAdapterFactory;

/**
 * @author aroller
 * 
 */
public class TrackFormatXmlAdapterFactoryUnitTest {

	/**
	 * Test method for
	 * {@link com.aawhere.track.TrackFormatXmlAdapterFactory#get(javax.ws.rs.core.MediaType)}.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetMediaType() throws Exception {

		TrackFormatXmlAdapterFactory factory = TrackFormatXmlAdapterFactory.create()
				.put(new TrackFormatXmlAdapter<String>()).build();
		TrackFormatXmlAdapter<?> adapter = factory.get(MediaType.WILDCARD_TYPE);
		assertNotNull(adapter);
		assertEquals(TrackFormatXmlAdapter.class, adapter.getClass());
		assertNull(adapter.marshal(null));
		assertNull(factory.get(MediaType.APPLICATION_ATOM_XML));
		assertSame(adapter, factory.get(MediaType.WILDCARD));
	}

}
