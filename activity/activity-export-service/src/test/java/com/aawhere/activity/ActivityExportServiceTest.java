/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.document.DocumentServiceTestUtil;
import com.aawhere.document.DocumentServiceTestUtil.Builder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class ActivityExportServiceTest {
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelperGaeBuilder()
			.withDatastoreService().build().getHelper();

	@After
	public void tearDown() {
		helper.tearDown();
	}

	private DocumentServiceTestUtil docServiceUtil;
	private ActivityExportService exportService;
	private DocumentService documentService;
	private ActivityImportServiceTestUtil importUtil;
	private ExampleTracksImportServiceTestUtil examplesUtil;

	@Before
	public void setUp() {
		helper.setUp();
		ActivityImportServiceTestUtil.Builder importUtilBuilder = ActivityImportServiceTestUtil.create().useTestQueue()
				.examplesPrepared();
		final ActivityServiceTestUtil.Builder activityServiceTestUtilBuilder = importUtilBuilder
				.getActivityServiceTestUtilBuilder();
		final Builder utilBuilder = activityServiceTestUtilBuilder.getTrackTestUtilBuilder()
				.getDocumentServiceTestUtilBuilder();
		utilBuilder.getDocumentFactoryBuilder().register(ActivityExportUtil.gpolylineDocumentProducer());
		this.importUtil = importUtilBuilder.build();
		this.examplesUtil = importUtil.getExamplesUtil();
		ActivityServiceTestUtil activityServiceTestUtil = this.importUtil.getActivityServiceTestUtil();
		this.exportService = new ActivityExportService(activityServiceTestUtil.getActivityService(),
				activityServiceTestUtil.getTrackTestUtil().getTrackService());
		this.docServiceUtil = activityServiceTestUtil.getTrackTestUtil().getDocumentTestUtil();
		this.documentService = this.docServiceUtil.getDocumentService();
	}

	/**
	 * This confirms file caching of gpolylines for activity id focused requests. It tests an
	 * individual, then a set of those including one already cached, one not cached and others that
	 * aren't going to be found.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testGooglePolyline() throws EntityNotFoundException {
		final DocumentFormatKey format = DocumentFormatKey.GPOLYLINE;

		Activity line = this.examplesUtil.line();
		Activity beforeAndAfter = this.examplesUtil.beforeAndAfter();
		final int numberOfTracksStored = 2;
		TestUtil.assertSize("two tracks should be stored", numberOfTracksStored, this.docServiceUtil.findAll());

		String encodedPath = exportService.gpolyline(line.id());
		assertNotNull("path", encodedPath);
		// polyline should now be stored
		List<ArchivedDocument> docsWithCache = this.docServiceUtil.findAll();
		int numberOfPolylinesCached = 1;
		TestUtil.assertSize(numberOfTracksStored + numberOfPolylinesCached, docsWithCache);
		ArchivedDocument linePolylineDoc = this.documentService.getArchivedDocument(line.id(), format);
		assertEquals("applicationDataId for line", line.id().getValue(), linePolylineDoc.getApplicationDataId());

		try {
			this.documentService.getArchivedDocument(beforeAndAfter.id(), format);
			fail("haven't called polyline on this one yet.");
		} catch (EntityNotFoundException e) {
			// good polyline isn't stored until needed
		}
		Set<ActivityId> bogusIds = ActivityTestUtil.createActivityIds();
		// a mixture of good and bad ids.
		final HashSet<ActivityId> expectedActivityIds = Sets.newHashSet(line.id(), beforeAndAfter.id());
		HashSet<ActivityId> createForIds = Sets.newHashSet(expectedActivityIds);
		createForIds.addAll(bogusIds);
		// notice the log warning for those not found
		// WARNING: [missing activity ids here] were requested, but not able to find or create
		// polylines
		Map<ActivityId, String> gpolylines = this.exportService.gpolylines(createForIds);
		TestUtil.assertSize("only the two with track docs should be returned", 2, gpolylines.entrySet());
		assertEquals("wrong activity ids found", expectedActivityIds, gpolylines.keySet());
		numberOfPolylinesCached++;
		// this proves line's gpolyline, which was already cached, is not created again
		TestUtil.assertSize("second polyline didn't cache well",
							numberOfTracksStored + numberOfPolylinesCached,
							this.docServiceUtil.findAll());
	}
}
