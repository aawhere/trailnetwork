/**
 * 
 */
package com.aawhere.activity.timing;

import com.aawhere.persist.FilterRepository;
import com.aawhere.persist.Repository;

/**
 * Persists {@link ActivityTiming}.
 * 
 * Notice the composite primary Key.
 * 
 * @author aroller
 * 
 */
public interface ActivityTimingRelationRepository
		extends Repository<ActivityTiming, ActivityTimingId>,
		FilterRepository<ActivityTimingRelations, ActivityTiming, ActivityTimingId> {

}
