/**
 * 
 */
package com.aawhere.activity.timing;

import java.util.Comparator;

import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.ComparatorResult;
import com.aawhere.lang.ObjectBuilder;

/**
 * Simply compares the {@link ActivityTiming.Completion#getDuration()}. To ensure durations of equal
 * values are included this will also compare start/finish times. If duration and start/finish times
 * are equal then this will return equals since timings with such equality are considered
 * duplicates.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationCompletionDurationComparator
		implements Comparator<ActivityTiming.Completion> {

	/**
	 * Used to construct all instances of ActivityTimingRelationCompletionDurationComparator.
	 */
	public static class Builder
			extends ObjectBuilder<ActivityTimingRelationCompletionDurationComparator> {

		public Builder() {
			super(new ActivityTimingRelationCompletionDurationComparator());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTimingRelationCompletionDurationComparator */
	private ActivityTimingRelationCompletionDurationComparator() {
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Completion left, Completion right) {
		int result = JodaTimeUtil.compareToWithinTolerance(left.getDuration(), right.getDuration());
		if (ComparatorResult.isEqualTo(result)) {
			if (!ActivityTimingRelationUtil.completionTimesEqual(left, right)) {
				// durations are equal, but times are not so avoid equality
				result = ComparatorResult.LESS_THAN.value;
			}
		}
		return result;
	}

}
