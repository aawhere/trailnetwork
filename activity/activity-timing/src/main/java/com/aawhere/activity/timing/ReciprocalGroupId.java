/**
 * 
 */
package com.aawhere.activity.timing;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.ActivityId;
import com.aawhere.id.StringIdentifier;
import com.aawhere.xml.XmlNamespace;

/**
 * Used to group multiple leaderboards together because they have common reciprocals. This is only
 * for internal use only. Posting externally could cause inconsistency since reciprocal groups can
 * change thier identifier and there is no redundancy built in yet.
 * 
 * TN-591 might help if the reciprocal group can be the parent, however, parent's can't be changed
 * so this is not a good candidate.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class ReciprocalGroupId
		extends StringIdentifier<ReciprocalGroupId.Group> {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public ReciprocalGroupId() {
		super(Group.class);
	}

	public ReciprocalGroupId(ActivityId activityId) {
		this(activityId.getValue());
	}

	public ReciprocalGroupId(String key) {
		super(key, Group.class);
	}

	public static class Group {
	}
}
