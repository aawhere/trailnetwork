/**
 * 
 */
package com.aawhere.activity.timing;

import javax.annotation.Nullable;

import com.aawhere.activity.ActivityId;

import com.google.common.base.Function;

/**
 * Returns the attempt Id from the timing id.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationIdToAttemptIdFunction
		implements Function<ActivityTimingId, ActivityId> {

	public static ActivityTimingRelationIdToAttemptIdFunction build() {
		return new ActivityTimingRelationIdToAttemptIdFunction();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public ActivityId apply(@Nullable ActivityTimingId input) {
		return input.getAttemptId();
	}

}
