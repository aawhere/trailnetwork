/**
 * 
 */
package com.aawhere.activity.timing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.ScreeningResults;
import com.aawhere.app.ApplicationKey;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.InvalidIdentifierException;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.bool.BooleanUtil;
import com.aawhere.log.LoggerFactory;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultiset;

/**
 * Helpers for {@link ActivityTiming} and friends.
 * 
 * @author aroller
 */
public class ActivityTimingRelationUtil {

	/**
	 * Since the {@link ActivityTimingId} is a composite of two {@link ActivityId}s (course,attempt)
	 * this will return each {@link ActivityId} in the pair.
	 * 
	 * @param activityTimingRelationId
	 * @return left has the course ActivityId and right has attemptId
	 */
	public static Pair<ActivityId, ActivityId> courseAttemptId(ActivityTimingId activityTimingRelationId) {
		return IdentifierUtils.splitCompositeId(activityTimingRelationId, ActivityId.class);
	}

	public static Iterable<ActivityId> courseIds(Iterable<ActivityTimingId> timingIds) {
		return Iterables.transform(timingIds, courseIdFunction());
	}

	private enum CourseIdFunction implements Function<ActivityTimingId, ActivityId> {
		INSTANCE;
		@Override
		@Nullable
		public ActivityId apply(@Nullable ActivityTimingId timingId) {
			return (timingId != null) ? timingId.getCourseId() : null;
		}

		@Override
		public String toString() {
			return "CourseIdFunction";
		}
	}

	public static Function<ActivityTimingId, ActivityId> courseIdFunction() {
		return CourseIdFunction.INSTANCE;
	}

	public static Function<ActivityTiming, ActivityId> courseIdFromTimingFunction() {
		Function<ActivityTiming, ActivityTimingId> idFunction = IdentifierUtils.idFunction();
		return Functions.compose(courseIdFunction(), idFunction);
	}

	/**
	 * Given a collection of Timings this will return the one with the shortest {@link Duration}.
	 * Provides the fastest of the given collection or null if it is empty.
	 */
	@Nullable
	public static ActivityTiming quickest(@Nonnull Iterable<ActivityTiming> relations) {
		ArrayList<ActivityTiming> sorted = Lists.newArrayList(relations);
		Collections.sort(sorted, ActivityTimingRelationDurationComparator.create().build());
		return Iterables.getFirst(sorted, null);

	}

	public static Ordering<ActivityTiming> timingOrderedByQuickest() {
		return Ordering.from(ActivityTimingRelationDurationComparator.create().build());
	}

	public static Ordering<Completion> completionOrderedByQuickest() {
		return Ordering.from(ActivityTimingRelationCompletionDurationComparator.create().build());
	}

	/**
	 * Given the course this will produce the timing where the course is also the attempt.
	 * 
	 * @param activity
	 *            the course that will also the be the attempt
	 * @return
	 */
	public static ActivityTiming selfTiming(Activity activity) {
		ActivityReferenceIds ids = ActivityReferenceIds.create().setActivity(activity).build();
		return ActivityTiming.create().setCourse(ids).setAttempt(ids)
				.addCompletion(activity.getTrackSummary().getStart(), activity.getTrackSummary().getFinish()).build();

	}

	/**
	 * @see #selfTiming(Activity)
	 * @param timingId
	 * @return true if the course is the same as the attempt. false otherwise
	 */
	public static Boolean selfTiming(ActivityTimingId timingId) {
		return timingId.getCourseId().equals(timingId.getAttemptId());
	}

	/**
	 * Provides the completion with the best duration or null if no completions available.
	 * 
	 * @param relation
	 * @return
	 */
	@Nullable
	public static Completion quickest(@Nonnull ActivityTiming relation) {

		List<Completion> completions = relation.getCompletions();
		if (completions.isEmpty()) {
			return null;
		} else {
			ArrayList<Completion> sorted = new ArrayList<>(completions);
			Collections.sort(sorted, ActivityTimingRelationCompletionDurationComparator.create().build());
			return sorted.get(0);
		}
	}

	/**
	 * @see #finishesEqual(Completion, Completion)
	 * @see #startsEqual(Completion, Completion)
	 * @param left
	 * @param right
	 * @return
	 */
	public static Boolean completionTimesEqual(Completion left, Completion right) {
		return startsEqual(left, right) && finishesEqual(left, right);
	}

	/**
	 * Compares the finish times for near equality.
	 * 
	 * @see JodaTimeUtil#equalsWithinTolerance(org.joda.time.DateTime, org.joda.time.DateTime)
	 * @param left
	 * @param right
	 * @return
	 */
	public static Boolean finishesEqual(Completion left, Completion right) {
		return JodaTimeUtil.equalsWithinTolerance(left.getFinish().getTimestamp(), right.getFinish().getTimestamp());
	}

	/**
	 * Compares the start times for near equality.
	 * 
	 * @see JodaTimeUtil#equalsWithinTolerance(org.joda.time.DateTime, org.joda.time.DateTime)
	 * @param left
	 * @param right
	 * @return
	 */
	public static Boolean startsEqual(Completion left, Completion right) {
		return JodaTimeUtil.equalsWithinTolerance(left.getStart().getTimestamp(), right.getStart().getTimestamp());
	}

	/**
	 * Failures are not necessarily a significant change since they communicate failures based on
	 * processing interesting to administrators, but not to end users. This helps separate.
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static Boolean equalsDeeplyIgnoreFailures(ActivityTiming left, ActivityTiming right) {
		// course and attempt intentionally left out...the are built into the id
		return left.equals(right) //
				&& left.isCourseCompleted().equals(right.isCourseCompleted())//
				&& left.getCompletions().equals(right.getCompletions())
				&& BooleanUtil.equals(left.reciprocal(), right.reciprocal());
	}

	/**
	 * Goes beyond the standard id equality check of entity and verifies the values inside are
	 * equal.
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static Boolean equalsDeeply(ActivityTiming left, ActivityTiming right) {
		return equalsDeeplyIgnoreFailures(left, right) && left.getFailures().equals(right.getFailures());
	}

	/**
	 * Given an {@link ActivityTimingId} this will return the pair of {@link ActivityId} where
	 * {@link Pair#left} is course and {@link Pair#right} is attempt.
	 * 
	 * @deprecated just use the {@link ActivityTimingId} itself. It has both.
	 * @param id
	 */
	@Deprecated
	public static Pair<ActivityId, ActivityId> split(ActivityTimingId id) {
		return Pair.of(id.courseId(), id.attemptId());

	}

	/**
	 * Given the {@link ActivityTimingId} this will return the id that represents its reciprocal
	 * relationship. In other words it swaps the course and attempt.
	 * 
	 * @param timingId
	 * @return
	 */
	public static ActivityTimingId reciprocalId(ActivityTimingId timingId) {
		return new ActivityTimingId(timingId.getAttemptId(), timingId.getCourseId());
	}

	/**
	 * Provides all completions in a single collection that is sorted by natural order, but allows
	 * for duplicates (since the same duration may occur more than once).
	 * 
	 * @return
	 */
	public static Multiset<ActivityTiming.Completion> completionsSorted(Iterable<ActivityTiming> timings) {
		// can't use Guava Function because multi-dimensional doesn't transform?
		Multiset<ActivityTiming.Completion> completions = TreeMultiset
				.create(ActivityTimingRelationCompletionDurationComparator.create().build());
		for (ActivityTiming activityTimingRelation : timings) {
			completions.addAll(activityTimingRelation.completions());
		}
		return completions;
	}

	public static Function<ActivityTiming, List<ActivityTiming.Completion>> completionsFunction() {
		return new Function<ActivityTiming, List<ActivityTiming.Completion>>() {

			@Override
			public List<ActivityTiming.Completion> apply(ActivityTiming input) {
				return (input == null) ? null : input.completions();
			}
		};
	}

	public static Iterable<ActivityTiming.Completion> completions(Iterable<ActivityTiming> timings) {
		Iterable<List<Completion>> firstPass = Iterables.transform(timings, completionsFunction());
		return Iterables.concat(firstPass);
	}

	public static ActivityId recriprocalId(ReciprocalGroupId groupId) {
		return new ActivityId(groupId.getValue());
	}

	/**
	 * Convenience to create an interval from the start/finsih.
	 * 
	 * @param completion
	 * @return
	 */
	public static Interval interval(Completion completion) {
		return new Interval(completion.start().getTimestamp(), completion.finish().getTimestamp());
	}

	/**
	 * Provides the quickest of those provided.
	 * 
	 * @param activityTimingRelation
	 * @param replaced
	 * @return
	 */
	public static ActivityTiming quickest(ActivityTiming... activityTimingRelations) {
		return quickest(Arrays.asList(activityTimingRelations));
	}

	public static Iterable<ActivityId> attemptIds(Iterable<ActivityTiming> relations) {
		return Iterables.transform(relations, attemptIdFromTimingFunction());
	}

	public static Function<Completion, ActivityTiming> timingsFromCompletionsFunction() {
		return ActivityTimingRelationFromCompletionFunction.create().build();
	}

	public static Iterable<ActivityId> attemptIdsFromCompletions(Iterable<Completion> completions) {
		Function<Completion, ActivityId> function = Functions.compose(	attemptIdFromTimingFunction(),
																		timingsFromCompletionsFunction());
		return Iterables.transform(completions, function);
	}

	public static Function<ActivityTiming, ActivityId> attemptIdFromTimingFunction() {
		return new Function<ActivityTiming, ActivityId>() {

			@Override
			public ActivityId apply(ActivityTiming timing) {
				return attemptId(timing);
			}

		};
	}

	public static ActivityId attemptId(ActivityTiming timing) {
		if (timing == null) {
			return null;
		}
		return timing.attempt().getActivityId();
	}

	/**
	 * A utility method that inspects timings, assuming to be of the same attempt but different
	 * courses, to look for the better of the two with the following priorities.
	 * 
	 * <pre>
	 * 1. reciproal = true is better 
	 * 2. quickest if recriprocal is equal
	 * </pre>
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static ActivityTiming better(ActivityTiming left, ActivityTiming right) {
		ActivityTiming better;
		if (BooleanUtil.equals(left.reciprocal(), right.reciprocal())) {
			better = quickest(left, right);
		} else {
			if (BooleanUtils.isTrue(left.reciprocal())) {
				better = left;
			} else if (BooleanUtils.isTrue(right.reciprocal())) {
				better = right;
			} else {
				better = quickest(left, right);
			}
		}
		return better;
	}

	/**
	 * Provides true iff each other's course is the other's attempt & courseCompleted by both. This
	 * ignores the {@link ActivityTiming#reciprocal()} flag since perhaps they don't realize it yet.
	 * 
	 * @param left
	 * @param right
	 */
	public static Boolean reciprocals(ActivityTiming left, ActivityTiming right) {
		return left != null && right != null && left.isCourseCompleted() && right.isCourseCompleted()
				&& left.attemptId().equals(right.courseId()) && left.courseId().equals(right.attemptId());
	}

	/**
	 * Given the activityIds this will provide a timing for every combination besides comparing to
	 * thyself. No nulls will be returned and any nulls given will be ignored.
	 * 
	 * @param activityIds
	 */
	public static Iterable<ActivityTimingId> timingIds(@Nonnull Iterable<ActivityId> activityIds) {
		// avoid nulls if given.
		activityIds = Iterables.filter(activityIds, Predicates.notNull());
		Iterable<ActivityTimingId> ids = Collections.emptyList();
		for (ActivityId courseId : activityIds) {
			ids = Iterables.concat(ids, Iterables.transform(activityIds, courseTimingIdFunction(courseId)));
		}
		return Iterables.filter(ids, Predicates.notNull());
	}

	/**
	 * Given the course Id, this will create a timing for every given activity id. If the courseId
	 * is equal to the input given this will return null.
	 * 
	 * @param courseId
	 * @return
	 */
	public static Function<ActivityId, ActivityTimingId> courseTimingIdFunction(final ActivityId courseId) {
		return new Function<ActivityId, ActivityTimingId>() {

			@Override
			public ActivityTimingId apply(ActivityId attemptId) {
				return timingId(courseId, attemptId);
			}

		};
	}

	/**
	 * Useful id creator when attempt or course could be null. If either is null OR they are equal
	 * to each other then the result will be null;
	 * 
	 * @param courseId
	 * @param attemptId
	 * @return
	 */
	public static ActivityTimingId timingId(final ActivityId courseId, ActivityId attemptId) {
		return (attemptId != null && courseId != null && !attemptId.equals(courseId)) ? new ActivityTimingId(courseId,
				attemptId) : null;
	}

	public static Function<ActivityId, ActivityTimingId> attemptTimingIdFunction(final ActivityId attemptId) {
		return new Function<ActivityId, ActivityTimingId>() {

			@Override
			public ActivityTimingId apply(ActivityId courseId) {
				return timingId(courseId, attemptId);
			}
		};
	}

	/**
	 * Given the attemp this will return the timing ids for every courseId given with the one
	 * attempt.
	 * 
	 * @param uniqueCourseIds
	 * @param attemptId
	 * @return
	 */
	public static Iterable<ActivityTimingId>
			attemptTimingIds(Iterable<ActivityId> uniqueCourseIds, ActivityId attemptId) {
		return Iterables.filter(Iterables.transform(uniqueCourseIds, attemptTimingIdFunction(attemptId)),
								Predicates.notNull());
	}

	public static Function<ActivityTimingId, ActivityTimingId> reciprocalIdFunction() {
		return new Function<ActivityTimingId, ActivityTimingId>() {

			@Override
			public ActivityTimingId apply(ActivityTimingId input) {
				return (input != null) ? reciprocalId(input) : null;
			}
		};
	}

	/**
	 * Provides the set of equal size containing a reciprocal for every {@link ActivityTimingId}
	 * given.
	 * 
	 * @param keySet
	 * @return
	 */
	public static Set<ActivityTimingId> reciprocalIds(Set<ActivityTimingId> keySet) {
		return ImmutableSet.copyOf(Iterables.transform(keySet, reciprocalIdFunction()));
	}

	/**
	 * @param line
	 * @param lineReciprocal
	 * @return
	 */
	public static ActivityTimingId timingId(Activity course, Activity attempt) {
		return new ActivityTimingId(course.id(), attempt.id());
	}

	/**
	 * Given timings that are attempted reciprocals of each other this will return them as a pair,
	 * updated to reflect their reciprocal relationships. The pair only includes the timings if they
	 * were updated. The returned results are clones of the original give so mutation does not
	 * happen.
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static Pair<ActivityTiming, ActivityTiming> reciprocalsUpdated(@Nullable ActivityTiming left,
			@Nullable ActivityTiming right) {
		ActivityTiming leftUpdated = null;
		ActivityTiming rightUpdated = null;

		if (left != null) {
			if (right != null) {
				Boolean reciprocals = reciprocals(left, right);
				Assertion.exceptions().eq(left.id(), reciprocalId(right.id()));
				if (ObjectUtils.notEqual(left.reciprocal(), reciprocals)) {
					// course not completed implies non-reciprocal...no need to update
					if (left.courseCompleted()) {
						leftUpdated = ActivityTiming.clone(left).reciprocal(reciprocals).build();
					}
				}
				if (ObjectUtils.notEqual(right.reciprocal(), reciprocals)) {
					// course not completed implies non-reciprocal...no need to update
					if (right.courseCompleted()) {
						rightUpdated = ActivityTiming.clone(right).reciprocal(reciprocals).build();
					}
				}
			}
		}
		return Pair.of(leftUpdated, rightUpdated);
	}

	/**
	 * For a stream of ids this maps each timing associated to the course it completed.
	 * 
	 * @param values
	 * @return
	 */
	public static ImmutableListMultimap<ActivityId, ActivityTimingId> coursesMapped(Iterator<ActivityTimingId> ids) {
		return Multimaps.index(ids, courseIdFunction());
	}

	/**
	 * Given timings this will group them by reciprocal relationships (including transitive
	 * reciprocity) so that all timings can be considered the same route.
	 * 
	 * @param coursesMapped
	 * @return
	 */
	public static ImmutableSetMultimap<ReciprocalGroupId, ActivityTiming> reciprocalGroups(
			Iterable<ActivityTiming> timings) {
		Iterable<ActivityTiming> reciprocals = reciprocalsOnly(timings);
		HashMap<ActivityId, ReciprocalGroupId> groupIds = new HashMap<ActivityId, ReciprocalGroupId>();
		ImmutableListMultimap<ActivityId, ActivityTiming> reciprocalsForCourses = Multimaps
				.index(reciprocals, courseIdFromTimingFunction());

		HashMultimap<ReciprocalGroupId, ActivityTiming> reciprocalGroups = HashMultimap.create();

		// iterate through every course assigning all timings to the same group
		// all timings of
		for (ActivityId courseId : reciprocalsForCourses.keySet()) {
			ReciprocalGroupId reciprocalGroupId = groupIds.get(courseId);
			if (reciprocalGroupId == null) {
				reciprocalGroupId = new ReciprocalGroupId(courseId);
				groupIds.put(courseId, reciprocalGroupId);

			}

			for (ActivityTiming activityTimingRelation : reciprocalsForCourses.get(courseId)) {
				// attempt is reciprocal, so it goes in the group.
				ActivityId attemptId = activityTimingRelation.id().getAttemptId();
				ReciprocalGroupId attemptReciprocalGroupId = groupIds.get(attemptId);
				if (attemptReciprocalGroupId == null) {
					groupIds.put(attemptId, reciprocalGroupId);
				} else if (!attemptReciprocalGroupId.equals(reciprocalGroupId)) {
					// when only half of a reciprocal comes in the multimap index doesn't pre-group
					// the result being a second group assigned so we must update all to share the
					// group. Keeping attempt since reciprocalGroupId just got created
					LoggerFactory.getLogger(ActivityTimingRelationUtil.class).warning(courseId + " has group id "
							+ reciprocalGroupId + ", but attempt " + attemptId + " already has "
							+ attemptReciprocalGroupId + " updating to keep " + attemptReciprocalGroupId);
					// those assigned during this iteration
					Set<ActivityTiming> incorrectlyAssigned = reciprocalGroups.removeAll(reciprocalGroupId);
					for (ActivityTiming wentIntoWrongGroup : incorrectlyAssigned) {
						reciprocalGroups.put(attemptReciprocalGroupId, wentIntoWrongGroup);
						groupIds.put(wentIntoWrongGroup.id().getCourseId(), attemptReciprocalGroupId);
						groupIds.put(wentIntoWrongGroup.id().getAttemptId(), attemptReciprocalGroupId);
					}
					// now that we're cleaned up, make sure anyone else uses the correct reciprocal
					// group
					reciprocalGroupId = attemptReciprocalGroupId;
				}
				reciprocalGroups.put(reciprocalGroupId, activityTimingRelation);
			}

		}

		return ImmutableSetMultimap.copyOf(reciprocalGroups);
	}

	/**
	 * Only those {@link ActivityTiming#reciprocal()} is true.
	 * 
	 * @param timings
	 * @return
	 */
	public static Iterable<ActivityTiming> reciprocalsOnly(Iterable<ActivityTiming> timings) {
		// the false match will weed out the null responses
		Predicate<ActivityTiming> reciprocalsOnlyPredicate = Predicates.compose(Predicates.equalTo(true),
																				reciprocalFunction());
		return Iterables.filter(timings, reciprocalsOnlyPredicate);
	}

	/**
	 * Returns the call to {@link ActivityTiming#reciprocal()} exactly as it is found.
	 * 
	 * @see #reciprocalsOnly(Iterable)
	 * @return
	 */
	public static Function<ActivityTiming, Boolean> reciprocalFunction() {
		return new Function<ActivityTiming, Boolean>() {

			@Override
			public Boolean apply(ActivityTiming input) {
				return (input != null) ? input.reciprocal() : null;
			}
		};
	}

	private enum CourseCompletedFunction implements Function<ActivityTiming, Boolean> {
		INSTANCE;
		@Override
		@Nullable
		public Boolean apply(@Nullable ActivityTiming timing) {
			return (timing != null) ? timing.courseCompleted() : null;
		}

		@Override
		public String toString() {
			return "CourseCompletedFunction";
		}
	}

	public static Function<ActivityTiming, Boolean> courseCompletedFunction() {
		return CourseCompletedFunction.INSTANCE;
	}

	/**
	 * Predicate that returns true if the {@link ActivityTimingRelation#courseCompleted()} is true.
	 * 
	 * @return
	 */
	public static Predicate<ActivityTiming> courseCompletedPredicate() {
		return Predicates.compose(Predicates.equalTo(Boolean.TRUE), courseCompletedFunction());
	}

	public static Iterable<ActivityTimingId> timingIds(ActivityId courseId, Iterable<ActivityId> attemptIds) {
		// nulls can happen for self-reference so avoid
		return Iterables
				.filter(Iterables.transform(attemptIds, courseTimingIdFunction(courseId)), Predicates.notNull());

	}

	/**
	 * Given a collection of groups of activity ids, this will provide timings for activity ids from
	 * one group against the activity ids of all other groups. It will not compare one group against
	 * itself. Since every ActivityId will be treated as a course and also as an attempt this will
	 * result in reciprocal timing ids for each.
	 * 
	 * This is especially useful in reciprocal processing when groups of reciprocals have been
	 * identified to be together AND each of the groups have some strong relationship to each other
	 * so the timings produced may find more matches.
	 * 
	 * @param activityIdGroups
	 * @return
	 */
	public static ImmutableSet<ActivityTimingId> timingsIdsAcrossGroups(
			Iterable<? extends Iterable<ActivityId>> activityIdGroups) {
		ImmutableSet.Builder<ActivityTimingId> results = ImmutableSet.builder();
		ImmutableList<Iterable<ActivityId>> allGroups = ImmutableList.<Iterable<ActivityId>> builder()
				.addAll(activityIdGroups).build();
		// each id of each group will produce a timing against each id of other groups
		for (int i = 0; i < allGroups.size(); i++) {
			Iterable<ActivityId> group = allGroups.get(i);
			for (ActivityId activityId : group) {
				for (int j = 0; j < allGroups.size(); j++) {
					// don't compare one group against itself
					if (i != j) {
						Iterable<ActivityId> otherGroup = allGroups.get(j);
						Iterable<ActivityTimingId> timingIds = timingIds(activityId, otherGroup);
						results.addAll(timingIds);
					}

				}
			}

		}
		return results.build();
	}

	public static Iterable<ActivityId> attemptIdsFromTimingIds(Iterable<ActivityTimingId> timingIds) {
		return Iterables.transform(timingIds, attemptIdFromTimingIdFunction());
	}

	private enum AttempIdFromTimingIdFunction implements Function<ActivityTimingId, ActivityId> {
		INSTANCE;
		@Override
		@Nullable
		public ActivityId apply(@Nullable ActivityTimingId timingId) {
			return (timingId != null) ? timingId.attemptId() : null;
		}

		@Override
		public String toString() {
			return "AttempIdFromTimingIdFunction";
		}
	}

	public static Function<ActivityTimingId, ActivityId> attemptIdFromTimingIdFunction() {
		return AttempIdFromTimingIdFunction.INSTANCE;
	}

	/**
	 * Given a map of failures to reasons, likely the result of {@link ScreeningResults#failers()},
	 * this will create timings for those failers so they can be recorded.
	 * 
	 * @param failers
	 * @return
	 */
	public static Iterable<ActivityTiming> timings(Multimap<TimingCompletionCategory, ActivityTimingId> failers) {
		Collection<Entry<TimingCompletionCategory, ActivityTimingId>> entries = failers.entries();
		LinkedList<ActivityTiming> relations = new LinkedList<ActivityTiming>();
		for (Entry<TimingCompletionCategory, ActivityTimingId> entry : entries) {
			relations.add(ActivityTiming.create().failure(entry.getValue(), entry.getKey()).build());
		}
		return relations;
	}

	private enum TimingIdFromStringFunction implements Function<String, ActivityTimingId> {
		INSTANCE;
		@Override
		@Nullable
		public ActivityTimingId apply(@Nullable String string) {
			return timingIdFromString(string);
		}

		@Override
		public String toString() {
			return "TimingIdFromStringFunction";
		}
	}

	public static Function<String, ActivityTimingId> timingIdFromStringFunction() {
		return TimingIdFromStringFunction.INSTANCE;
	}

	/**
	 * converts the string into an {@link ActivityTimingId} if the format is correct.
	 * 
	 * @param string
	 * @return
	 * @throws InvalidIdentifierException
	 */
	@Nullable
	public static ActivityTimingId timingIdFromString(String string) throws InvalidIdentifierException {
		if (string == null) {
			return null;
		}
		String[] parts = IdentifierUtils.splitCompositeIdValue(string);
		if (parts.length != 4) {
			throw InvalidIdentifierException.compositeFormat(string, "{courseId}-{attemptId}");
		}
		ActivityId courseId = new ActivityId(new ApplicationKey(parts[0]), parts[1]);
		ActivityId attemptId = new ActivityId(new ApplicationKey(parts[2]), parts[3]);
		return new ActivityTimingId(courseId, attemptId);
	}

}
