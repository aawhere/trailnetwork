/**
 * 
 */
package com.aawhere.activity.spatial;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.activity.Activity;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.JtsMeasureUtil;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.Trackpoint;

import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Point;

/**
 * Given a {@link MultiLineString} of one activity this will compare the start and finish from
 * another {@link Activity} with it's {@link TrackSummary}. If the start is not within range then
 * the finish is never checked.
 * 
 * @author aroller
 * 
 */
public class ActivitySpatialRelationEndpointVerifier {

	/**
	 * The indicator weather or not the given Activity's start points are within range of the line.
	 * 
	 */
	private Boolean startInRange;
	private Boolean endInRange;

	/**
	 * Used to construct all instances of ActivitySpatialRelationEndpointVerifier.
	 */
	public static class Builder
			extends ObjectBuilder<ActivitySpatialRelationEndpointVerifier> {
		private MultiLineString multiLineString;
		private Activity activity;
		private Length maxLengthAway;

		public Builder() {
			super(new ActivitySpatialRelationEndpointVerifier());
		}

		public Builder setMaxLengthAway(Length maxLengthAway) {
			this.maxLengthAway = maxLengthAway;
			return this;
		}

		public Builder activity(Activity activity) {
			this.activity = activity;
			return this;
		}

		public Builder multiLineString(MultiLineString multiLineString) {
			this.multiLineString = multiLineString;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("multiLineString", this.multiLineString);
			Assertion.exceptions().notNull("maxLengthAway", this.maxLengthAway);
			Assertion.exceptions().notNull("activity", this.activity);
		}

		public ActivitySpatialRelationEndpointVerifier build() {
			ActivitySpatialRelationEndpointVerifier built = super.build();
			final Trackpoint startTrackpoint = activity.getTrackSummary().getStart();
			Point start = JtsTrackGeomUtil.point(startTrackpoint);
			double maxArcLengthAway = JtsMeasureUtil.arcLength(maxLengthAway, startTrackpoint.getLocation());
			double startDistance = multiLineString.distance(start);
			if (startDistance < maxArcLengthAway) {
				built.startInRange = true;
				double endDistance = multiLineString.distance(JtsTrackGeomUtil.point(activity.getTrackSummary()
						.getFinish()));
				if (endDistance < maxArcLengthAway) {
					built.endInRange = true;
				} else {
					built.endInRange = false;
				}
			} else {
				built.startInRange = false;
			}
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivitySpatialRelationEndpointVerifier */
	private ActivitySpatialRelationEndpointVerifier() {
	}

	/**
	 * Indicates if the start and end are within the desired range.
	 * 
	 * @return
	 */
	public Boolean passed() {
		return BooleanUtils.isTrue(startInRange) && BooleanUtils.isTrue(endInRange);
	}

	/**
	 * True if the end is in range, false if it is verified to be out of range, null if never
	 * checked because start didn't qualify.
	 * 
	 * @see BooleanUtils#isTrue(Boolean)
	 * 
	 * @return the endInRange
	 */
	@Nullable
	public Boolean endInRange() {
		return this.endInRange;
	}

	/**
	 * True if the start is in range or false if it is not. Never null since it is always checked
	 * first.
	 * 
	 * @return the startInRange
	 */
	@Nonnull
	public Boolean startInRange() {
		return this.startInRange;
	}

}
