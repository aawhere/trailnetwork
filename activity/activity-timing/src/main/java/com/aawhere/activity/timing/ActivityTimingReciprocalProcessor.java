package com.aawhere.activity.timing;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.AttemptLineProvider;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.story.TrackUserStories;
import com.google.common.base.Function;

/**
 * Processes a reciprocal relationship in an efficient manner when provided {@link Builder#timings}
 * that may contain none, one or both timings. If they aren't reciprocals or if there is extra then
 * this will fail non-gracefully since the client is providing bad information.
 * 
 * The reciprocal will only be processed if the timing has passed screening at minimum.
 * 
 * The activities and tracks will be requested if needed to determine the reciprocal relationship.
 * The end result will be two timing results with {@link #timing} and {@link #reciprocal} picked
 * arbitrarily.
 * 
 * This will also indicate if the system should persist either timing by indicating
 * {@link #reciprocalUpdated} or {@link #timingUpdated}.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingReciprocalProcessor {

	@Nonnull
	private ActivityTiming timing;

	@Nullable
	private ActivityTiming reciprocal;

	/**
	 * The reciprocal of {@link #timing} if timing is passed or if the reciprocal was already
	 * processed.
	 */
	@Nonnull
	private Boolean timingUpdated = false;

	@Nonnull
	private Boolean reciprocalUpdated = false;

	/**
	 * Used to construct all instances of ActivityTimingReciprocalProcessor.
	 */
	public static class Builder
			extends ObjectBuilder<ActivityTimingReciprocalProcessor> {

		private static Logger logger = LoggerFactory.getLogger(ActivityTimingReciprocalProcessor.class);
		/** The timing being sought. */
		private ActivityTimingId timingId;

		/**
		 * based on the timings provided the course will be determined and match {@link #timing}'s
		 * course.
		 * 
		 */
		private Activity course;
		/**
		 * matches the {@link #timing}'s attempt
		 */
		private Activity attempt;
		/**
		 * the given timings that are known prior to this processing
		 */
		private ActivityTimingRelations timings;
		/** Provides the multiline string for screening if needed. */
		private AttemptLineProvider attemptLineProvider;
		/** provides the track for deep processing if needed. */
		private Function<Activity, Iterable<Trackpoint>> trackProvider;
		/**
		 * Retrieves the activities only if needed.
		 */
		private Function<ActivityId, Activity> activityProvider;

		public Builder() {
			super(new ActivityTimingReciprocalProcessor());
		}

		/** @see #timings */
		public Builder timings(ActivityTimingRelations timings) {
			this.timings = timings;
			return this;
		}

		public Builder timingId(ActivityTimingId timingId) {
			this.timingId = timingId;
			return this;
		}

		/**
		 * used internally to assign {@link ActivityTimingReciprocalProcessor#timing} when it has
		 * changed.
		 * 
		 * @param timing
		 */
		private void timing(ActivityTimingReciprocalProcessor built, ActivityTiming timing) {
			if (timing != null) {
				built.timing = timing;
				built.timingUpdated = true;
			}
		}

		private void reciprocal(ActivityTimingReciprocalProcessor built, ActivityTiming timing) {
			if (timing != null) {
				built.reciprocal = timing;
				built.reciprocalUpdated = true;
			}
		}

		public Builder attempLineProvider(AttemptLineProvider attemptLineProvider) {
			this.attemptLineProvider = attemptLineProvider;
			return this;
		}

		@Override
		public ActivityTimingReciprocalProcessor build() {
			ActivityTimingReciprocalProcessor built = super.build();
			// sets up timing and recriprocal from the given.
			if (timings.size() >= 1) {
				Iterator<ActivityTiming> iterator = timings.iterator();
				built.timing = iterator.next();
				this.timingId = built.timing.id();
				if (timings.size() == 2) {
					built.reciprocal = iterator.next();
					Assertion.e().eq(built.timing.id(), ActivityTimingRelationUtil.reciprocalId(built.reciprocal.id()));
				} else if (timings.size() > 2) {
					throw new IllegalArgumentException("timings may only be at most two, but were " + this.timings);
				}
			}
			if (continueProcessing(built)) {
				if (timings.size() <= 1) {

					// we're gonna need the activities to figure out the other
					this.course = this.activityProvider.apply(this.timingId.courseId());
					this.attempt = this.activityProvider.apply(this.timingId.attemptId());
				}

				if (built.timing == null) {
					ActivityTimingRelationCandidateScreener screener = screener(course, attempt);
					if (screener.failed()) {
						timing(built, screener.timingFailed());
					}
				}
				if (continueProcessing(built) && built.reciprocal == null) {
					// notice the course/attempt are reversed
					ActivityTimingRelationCandidateScreener screener = screener(attempt, course);
					if (screener.failed()) {
						reciprocal(built, screener.timingFailed());
					}
				}

				// does deep processing if needed
				deep(built);

				// this will provide updated timings if needed.
				Pair<ActivityTiming, ActivityTiming> reciprocalsUpdated = ActivityTimingRelationUtil
						.reciprocalsUpdated(built.timing, built.reciprocal);
				// null checks are built into the mutators
				timing(built, reciprocalsUpdated.getLeft());
				reciprocal(built, reciprocalsUpdated.getRight());
			}
			return built;
		}

		/**
		 * Looking at the availability of {@link ActivityTimingReciprocalProcessor#timing} and
		 * {@link ActivityTimingReciprocalProcessor#reciprocal}, and the
		 * {@link ActivityTiming#courseCompleted()} of each this will determine if we should
		 * continue to look for more answers. One failure indicates its not worth trying.
		 * 
		 * @param built
		 * @return
		 */
		boolean continueProcessing(ActivityTimingReciprocalProcessor built) {

			boolean keepGoing;
			if (built.timing == null && built.reciprocal == null) {
				// at least one has to fail to give up
				keepGoing = true;
			} else {
				if (built.timing != null) {
					if (built.reciprocal == null) {
						if (built.timing.courseCompleted()) {
							// there is a chance for reciprocal situation
							keepGoing = true;
						} else {
							// no chance for reciprocal so don't continue
							keepGoing = false;
						}
					} else {
						// timing and reciprocal are known...we are done
						keepGoing = false;
					}
				} else {
					// then built.reciprocal must not be null and timing is
					if (built.reciprocal.courseCompleted()) {
						keepGoing = true;
					} else {
						keepGoing = false;
					}
				}
			}
			return keepGoing;

		}

		/**
		 * screens those given. assigns the results to timing or reciprocal if not yet known.
		 * 
		 * @param course
		 * @param attempt
		 * @return
		 */
		private ActivityTimingRelationCandidateScreener screener(Activity course, Activity attempt) {
			return ActivityTimingRelationCandidateScreener.create().course(course).attempt(attempt)
					.attemptLineProvider(attemptLineProvider).build();
		}

		/**
		 * Provides deep processing to one or both until {@link #notFinished()} return false;
		 */
		private void deep(ActivityTimingReciprocalProcessor built) {
			// no need for deep if already known
			if (continueProcessing(built)) {
				// this is extra confusing so we can process most efficiently
				Activity course;
				Activity attempt;
				// indicates if the main pass is processing for the timing or the reciprocal
				boolean timingNeeded = built.timing == null;
				// always find timing first, then reciprocal
				// sometimes the timing is known so this will go deep on the reciprocal
				if (timingNeeded) {
					course = this.course;
					attempt = this.attempt;
				} else {
					// course/attempt will swap roles if only reciprocal is needed
					course = this.attempt;
					attempt = this.course;
				}

				ActivityTimingRelationUserStory.Builder timingUserStoryBuilder = ActivityTimingRelationUserStory
						.create();
				Iterable<Trackpoint> courseTrack = trackProvider.apply(course);
				Iterable<Trackpoint> attemptTrack = trackProvider.apply(attempt);
				timingUserStoryBuilder.setCourseTrack(courseTrack);
				timingUserStoryBuilder.setCourse(course);
				timingUserStoryBuilder.setAttempt(attempt);
				TrackUserStories.Builder userStoriesBuilder = TrackUserStories.create(attemptTrack);
				userStoriesBuilder.addStandardFilters();
				ActivityTimingRelationUserStory story = userStoriesBuilder.setUserStory(timingUserStoryBuilder);
				userStoriesBuilder.build();

				logMessage("main pass", story.getResult());

				if (timingNeeded) {
					timing(built, story.getResult());

					if (continueProcessing(built)) {
						ActivityTimingRelationUserStory reciprocalStory = story.reciprocalStory(attemptTrack);
						ActivityTiming reciprocalResult = reciprocalStory.getResult();
						logMessage("reciprocal pass", reciprocalResult);
						reciprocal(built, reciprocalResult);
					}
				} else {
					// processing happened, but it was for the reciprocal
					// logging already covered
					reciprocal(built, story.getResult());
				}
			}

		}

		public void logMessage(String context, ActivityTiming result) {
			Level level = Level.FINE;
			if (logger.isLoggable(level)) {
				logger.log(	level,
							result.id() + " reciprocal candidate deep processed courseCompleted="
									+ result.courseCompleted() + " reciprocal=" + result.reciprocal() + " for "
									+ context);
			}
		}

		public Builder trackProvider(Function<Activity, Iterable<Trackpoint>> trackFunction) {
			this.trackProvider = trackFunction;
			return this;
		}

		public Builder activityProvider(Function<ActivityId, Activity> idFunction) {
			this.activityProvider = idFunction;
			return this;
		}
	}// end Builder

	public static Builder create(ActivityTimingId timingId) {
		return new Builder().timingId(timingId);
	}

	/** Use {@link Builder} to construct ActivityTimingReciprocalProcessor */
	private ActivityTimingReciprocalProcessor() {
	}

	public ActivityTiming timing() {
		return this.timing;
	}

	public ActivityTiming reciprocal() {
		return this.reciprocal;
	}

	public Boolean timingUpdated() {
		return this.timingUpdated;
	}

	public Boolean reciprocalUpdated() {
		return this.reciprocalUpdated;
	}

}
