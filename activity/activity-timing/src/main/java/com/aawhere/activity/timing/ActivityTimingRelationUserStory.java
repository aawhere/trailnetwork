/**
 * 
 */
package com.aawhere.activity.timing;

import java.util.List;

import javax.measure.quantity.Length;

import com.aawhere.activity.Activity;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.BoundingBoxProvider;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.match.TrackTimingRelationCalculator;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingResult;
import com.aawhere.track.story.BaseTrackUserStory;
import com.aawhere.track.story.TrackSummaryUserStories;
import com.aawhere.track.story.TrackUserStories;
import com.google.common.base.Predicate;

/**
 * Given a Course Activity and it's track summary this will inspect an attempt's track to provide
 * {@link ActivityTiming} results of the attempt on the course.
 * 
 * TODO:Consider validating relationship for appropriate overlap
 * 
 * {@link #hasResult()} returns false if no valid race was found.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationUserStory
		extends BaseTrackUserStory<ActivityTiming> {

	/** TN-476 ...provide a field if you find the need for customization */
	public static final Length BOUNDS_LENGTH_MIN_DEFAULT = MeasurementUtil.createLengthInMeters(1000d);

	static final Length DEFAULT_BUFFER_WIDTH = TrackSummaryUserStories.DEFAULT_BUFFER_WIDTH;
	static final Length DEFAULT_MAX_DEVIATION_LENGTH = MeasurementUtil.createLengthInMeters(400);
	static final Double ACTIVITY_DISTANCE_ERROR_FACTOR = 1.1d;
	/** A constant to indicate how long the goal line is to be. */
	final static Length GOAL_LINE_LENGTH = MeasurementUtil.createLengthInMeters(75);
	/**
	 * Erroneously short activities dominate the matched courses. Require a minimum length to avoid
	 * bogus races.
	 */
	static final Length MIN_LENGTH_FOR_COURSE = MeasurementUtil.createLengthInMeters(1000);

	/**
	 * TODO:make this package protected
	 */
	private Activity course;
	private Activity attempt;
	private TrackTimingRelationCalculator calculator;
	/** Supports existing relations to be updated rather than created. */
	private ActivityTiming.Builder resultBuilder;
	private Iterable<Trackpoint> courseTrack;

	/**
	 * Lightweight validator to avoid processing the story that could take longer. TN-444 TN-338
	 */
	private ActivityTimingRelationCandidateScreener screener;

	/**
	 * Used to construct all instances of ActivityTimingRelationUserStory.
	 */
	public static class Builder
			extends BaseTrackUserStory.Builder<ActivityTimingRelationUserStory, Builder> {

		public Builder() {
			super(new ActivityTimingRelationUserStory());
		}

		protected Builder(ActivityTimingRelationUserStory story) {
			super(story);
		}

		Builder setCourse(Activity course) {
			building.course = course;
			return this;
		}

		public Builder screener(ActivityTimingRelationCandidateScreener screener) {
			if (screener != null) {
				building.screener = screener;
				setCourse(screener.course());
				setAttempt(screener.attempt());
			}
			return this;
		}

		Builder setAttempt(Activity attempt) {
			building.attempt = attempt;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.track.story.BaseTrackUserStory.Builder#configureProcessorBuilder(com.aawhere
		 * .track.story.BaseTrackUserStory)
		 */
		@Override
		protected void configureProcessorBuilder(ActivityTimingRelationUserStory built) {

		}

		/**
		 * @return
		 */
		protected com.aawhere.track.match.TrackTimingRelationCalculator.Builder createCalculatorBuilder() {
			return new TrackTimingRelationCalculator.Builder();
		}

		/**
		 * @param track
		 * @return
		 */
		public Builder setCourseTrack(Iterable<Trackpoint> track) {
			this.track = track;
			building.courseTrack = track;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.story.BaseTrackUserStory.Builder#validate()
		 */
		@Override
		public void validate() {
			Assertion.exceptions().notNull("courseTrack", this.track);
			Assertion.exceptions().notNull("course", building.course);
			Assertion.exceptions().notNull("attempt", building.attempt);
			// Assertion.exceptions().notNull("screener", building.screener);

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.story.BaseTrackUserStory.Builder#build()
		 */
		@Override
		public ActivityTimingRelationUserStory build() {
			final ActivityTimingRelationUserStory built = super.build();

			built.resultBuilder = new ActivityTiming.Builder();

			if (built.screener == null || built.screener.passed()) {
				buildCalculator(built);
			} else {
				built.resultBuilder.addFailure(built.screener.reason());
			}

			return built;
		}

		/**
		 * for children that want to calculator regardless of the pre-checks detecting failures.
		 * 
		 * @param built
		 */
		protected void buildCalculator(final ActivityTimingRelationUserStory built) {
			if (built.calculator == null) {
				TrackTimingRelationCalculator.Builder calculatorBuilder = createCalculatorBuilder();
				built.calculator = calculatorBuilder.setCourse(built.course.getTrackSummary())
						.setBufferWidth(DEFAULT_BUFFER_WIDTH).setMaxDeviationLength(DEFAULT_MAX_DEVIATION_LENGTH)
						.setCourseTrack(this.track).goalLineLength(GOAL_LINE_LENGTH)
						.registerWithProcessor(getProcessorBuilder()).build();

			}
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTimingRelationUserStory */
	protected ActivityTimingRelationUserStory() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.story.BaseTrackUserStory#process()
	 */
	@Override
	public void process() {

		resultBuilder.courseId(course);
		resultBuilder.attemptId(attempt);

		// pre-checks may not even run a calculator
		if (calculator != null) {
			List<TimingResult> raceResults = calculator.getRaceResults();
			for (TimingResult timingResult : raceResults) {
				resultBuilder.addResult(timingResult);
			}
		}
		ActivityTiming result = resultBuilder.build();
		super.setResult(result);
	}

	/**
	 * Explains who this story is about.
	 * 
	 * @return
	 */
	public ActivityTimingId about() {
		return new ActivityTimingId(course.getId(), attempt.getId());
	}

	/**
	 * @return the screenSucceeded
	 */
	public Boolean screenSucceeded() {
		return screener.passed();
	}

	/**
	 * @return the screener
	 */
	public ActivityTimingRelationCandidateScreener screener() {
		return this.screener;
	}

	/**
	 * @return the calculator
	 */
	protected TrackTimingRelationCalculator getCalculator() {
		return this.calculator;
	}

	/**
	 * Provides external use to know if this story will compare an activity with a given distance.
	 * 
	 * @param distance
	 * @return
	 */
	public static Boolean isLongEnough(Length distance) {
		return QuantityMath.create(distance).greaterThan(MIN_LENGTH_FOR_COURSE);
	}

	public static Boolean isLongEnough(Activity activity) {
		return isLongEnough(activity.getTrackSummary().getDistance());
	}

	/**
	 * Given an activity as an "Attempt" this will return the maximum length for a course since a
	 * course must always be shorter than the attempt that must complete the entire course. There is
	 * some tolerance given to accommodate distance calculation errors.
	 * 
	 * This distance is an advantage since it can be a very quick way to disqualify an attempt.
	 * 
	 * @param courseDistance
	 * @return
	 */
	public static Length maxLengthForCourse(Activity attempt) {
		// add a percentage fudge factor since cumulative mistakes can happen for longer activities
		return QuantityMath.create(attempt.getTrackSummary().getDistance()).times(ACTIVITY_DISTANCE_ERROR_FACTOR)
				.getQuantity();
	}

	/**
	 * Given an activity as an "Course" this will return the minimum length for a Attempt since a
	 * attempt must always be longer than the course to complete the entire course. There is some
	 * tolerance given to accommodate distance calculation errors.
	 * 
	 * This distance is an advantage since it can be a very quick way to disqualify an course when
	 * looking for courses for a given activity.
	 * 
	 * @param courseDistance
	 * @return
	 */
	public static Length minLengthForAttempt(Activity course) {
		// shorten by a percentage fudge factor since cumulative mistakes can happen for longer
		// activities
		return QuantityMath.create(course.getTrackSummary().getDistance()).times(1 / ACTIVITY_DISTANCE_ERROR_FACTOR)
				.getQuantity();
	}

	/**
	 * The attempt must be at least as long as the course, otherwise it couldn't possibly finish.
	 * TN-310
	 * 
	 * @param course
	 * @param attempt
	 * @return
	 */
	public static Boolean isLongEnough(Activity course, Activity attempt) {
		return QuantityMath.create(course.getTrackSummary().getDistance()).lessThan(maxLengthForCourse(attempt));
	}

	/**
	 * Given the courseTrack this will provide the rest as
	 * 
	 * @param courseTrack
	 * @return
	 */
	public ActivityTimingRelationUserStory reciprocalStory(Iterable<Trackpoint> attemptTrack) {
		TrackUserStories.Builder userStoriesBuilder = TrackUserStories.create(this.courseTrack);
		userStoriesBuilder.addStandardFilters();
		Builder timingUserStoryBuilder = create();
		timingUserStoryBuilder.setAttempt(course);
		timingUserStoryBuilder.setCourse(attempt);
		timingUserStoryBuilder.screener(screener);
		timingUserStoryBuilder.setCourseTrack(attemptTrack);
		ActivityTimingRelationUserStory story = userStoriesBuilder.setUserStory(timingUserStoryBuilder);
		userStoriesBuilder.build();
		return story;

	}

	/**
	 * @param course
	 * @return
	 */
	public static Boolean hasBigEnoughFootprint(BoundingBoxProvider course) {
		return QuantityMath.create(course.boundingBox().getDiagonal()).greaterThan(BOUNDS_LENGTH_MIN_DEFAULT);
	}

	/**
	 * Keeps only those activities that
	 * {@link ActivityTimingRelationUserStory#isLongEnough(Activity)} or
	 * {@link ActivityTimingRelationUserStory#hasBigEnoughFootprint(BoundingBoxProvider)}.
	 * 
	 * @return
	 */
	public static Predicate<Activity> candidateSizePredicate() {
		return new Predicate<Activity>() {

			@Override
			public boolean apply(Activity activity) {
				return hasBigEnoughFootprint(activity) && isLongEnough(activity);
			}
		};
	}

	/**
	 * Aggregates all the checks to consider the given activity as a potential course for a route.
	 * 
	 * @param courseCandidate
	 * @return
	 */
	public static Boolean isCourseCandidate(Activity courseCandidate) {
		return hasBigEnoughFootprint(courseCandidate) && isLongEnough(courseCandidate);
	}
}
