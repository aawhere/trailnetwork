/**
 * 
 */
package com.aawhere.activity.timing;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.Collections2;

/**
 * Multiple {@link ActivityTiming} usually as a result of filtering.
 * 
 * @author aroller
 * 
 */
@XmlRootElement(name = ActivityTiming.FIELD.DOMAIN_PLURAL)
@XmlType(name = ActivityTiming.FIELD.DOMAIN_PLURAL, namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class ActivityTimingRelations
		extends BaseFilterEntities<ActivityTiming> {

	/**
	 * Used to construct all instances of ActivityTimingRelations.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, ActivityTiming, ActivityTimingRelations> {

		public Builder() {
			super(new ActivityTimingRelations());
		}

		/**
		 * @param timings
		 * @return
		 */
		public Builder addCompletions(Collection<Completion> completions) {
			addAll(Collections2.transform(completions, ActivityTimingRelationFromCompletionFunction.create().build()));
			return dis;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTimingRelations */
	private ActivityTimingRelations() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntities#getCollection()
	 */
	@XmlElement(name = ActivityTiming.FIELD.DOMAIN)
	@Override
	public List<ActivityTiming> getCollection() {
		return super.getCollection();
	}
}
