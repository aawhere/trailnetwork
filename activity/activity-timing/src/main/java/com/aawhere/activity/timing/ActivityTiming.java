/**
 * 
 */
package com.aawhere.activity.timing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.app.account.Account;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.track.SimpleTrackpoint;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingResult;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Supplier;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfEmpty;
import com.googlecode.objectify.condition.PojoIf;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * When one track is nearly completely contained in another the timing results can be interesting
 * when compared to others that performed the similar route. This explains the performance during
 * that focused segment named {@link #course} explaining the performance of the {@link #attempt} .
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE, name = ActivityTiming.FIELD.DOMAIN)
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Cache
@Dictionary(domain = ActivityTiming.FIELD.DOMAIN, messages = ActivityTimingRelationMessage.class)
public class ActivityTiming
		extends StringBaseEntity<ActivityTiming, ActivityTimingId>
		implements SelfIdentifyingEntity, Supplier<List<ActivityTiming.Completion>>, Cloneable {

	private static final long serialVersionUID = -7105565558776429486L;

	/**
	 * Used to construct all instances of ActivityTimingRelation.
	 */
	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<ActivityTiming, Builder, ActivityTimingId> {

		public Builder() {
			super(new ActivityTiming());
		}

		/** Mutation builder available to trusted classes. */
		/* package */Builder(ActivityTiming timingRelation) {
			super(timingRelation);
		}

		/**
		 * @deprecated
		 * 
		 * @param attempt
		 * @return
		 */
		@Deprecated
		public Builder setAttempt(ActivityReferenceIds attempt) {
			attemptId(attempt.getActivityId());
			return this;
		}

		/** @see #attemptId */
		public Builder attemptId(ActivityId attemptId) {
			building.attemptId = attemptId;
			return this;
		}

		public Builder attemptId(Activity attempt) {
			return attemptId(attempt.id());
		}

		@Deprecated
		public Builder setCourse(ActivityReferenceIds course) {
			courseId(course.getActivityId());
			return this;
		}

		public Builder courseId(Activity course) {
			return courseId(course.id());
		}

		public Builder courseId(ActivityId courseId) {
			parentId(courseId);
			return this;
		}

		public Builder setCompletions(List<Completion> completions) {
			building.completions = completions;
			return this;
		}

		public Builder setFailures(Set<TimingCompletionCategory> failures) {
			building.failures = failures;
			return this;
		}

		public Builder failure(ActivityTimingId id, TimingCompletionCategory reason) {
			setId(id);
			addFailure(reason);
			return this;
		}

		/**
		 * When a course is completed the start and finish calculate the duration. All are available
		 * from {@link ActivityTiming#completions}.
		 * 
		 * @param start
		 * @param finish
		 * @return
		 */
		public Builder addCompletion(Trackpoint start, Trackpoint finish) {
			Completion completion = new Completion(building);
			completion.start = TrackUtil.simpleTrackpoint(start);
			completion.finish = TrackUtil.simpleTrackpoint(finish);
			building.completions.add(completion);
			return this;
		}

		public Builder addResult(TimingResult result) {
			if (result.raceCompleted()) {
				TrackSummary summary = result.getSummary();
				addCompletion(summary.getStart(), summary.getFinish());
			} else {
				addFailure(result.getReason());
			}
			return this;
		}

		public Builder reciprocal(Boolean reciprocal) {
			building.reciprocal = reciprocal;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			// course and attempt are null when failing for storage reasons.
			Assertion.exceptions().assertTrue("id.courseId", hasParentId());
			Assertion.exceptions().assertTrue("id.attemptId", hasId());

			// completions and failures can both be empty if screening is done
			if (CollectionUtils.isNotEmpty(building.completions)) {
				Assertion.exceptions().notNull("attemptId", building.attemptId);
				// it's nice to report errors along with the successes after processing.
				// failures aren't stored if completions is true
				// Assertion.exceptions().assertEmpty("failures must be empty if completions is not",
				// building.failures);
			} else {
				Assertion.e().assertNotEmpty(	"failures must be given if completions is empty",
												building.getCompositeId());

			}
			super.validate();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ActivityTiming build() {

			if (!hasId()) {
				setId(new ActivityTimingId(building.courseId(), building.attemptId));
			}
			return super.build();
		}

		/**
		 * @param failure
		 * @return
		 */
		public Builder addFailure(TimingCompletionCategory failure) {
			building.failures.add(failure);
			return this;
		}

		/**
		 * Retrieves all important data and assigns it to the given builder. Usually used with
		 * {@link ActivityTiming#mutate(ActivityTiming)}
		 * 
		 * @param given
		 */
		Builder setOther(ActivityTiming other) {
			setCompletions(other.completions);
			setFailures(other.failures);
			reciprocal(other.reciprocal);
			// course or attempt shouldn't change unless it's going to/from failure in which it will
			// be null
			if (hasId()) {
				Assertion.exceptions().assertTrue("ids must equal", id().equals(other.id()));
			} else {
				courseId(other.courseId());
				attemptId(other.attemptId());
			}
			return this;
		}

		/**
		 * A special allowance to create a timing with the id of a course and attempt without
		 * providing a success or failure yet. it is essentially an empty shell
		 * 
		 * @param id
		 * @return
		 */
		public Builder notYetDecided(ActivityTimingId id) {
			setId(id);
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @param mutant
	 * @return
	 */
	static Builder mutate(ActivityTiming mutant) {
		return new Builder(mutant);
	}

	static Builder clone(ActivityTiming original) {
		try {
			return mutate((ActivityTiming) original.clone());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
	}

	/**
	 * A sequence of {@link TimingResult}s in order of time that must be
	 * {@link TimingResult#raceCompleted()}.
	 */
	@XmlElement(name = Completion.FIELD.COMPLETION)
	@Nullable
	@IgnoreSave(IfEmpty.class)
	@Field(key = FIELD.COMPLETIONS, parameterType = Completion.class)
	private List<Completion> completions = new ArrayList<>();
	/**
	 * Lists all the failures found during the processing. Existence of failures does not indicate
	 * the lack of success.
	 * 
	 * This is never stored to save space and the failures, without more context explained by the
	 * KML debugger, do not explain much.
	 * 
	 */
	@XmlElement
	@Nullable
	@Ignore
	private Set<TimingCompletionCategory> failures = new HashSet<>();

	/**
	 * Indicates if this is a mutual completion where a timing completion is found when the
	 * course/attempt are switched. True means it is known to be reciprocal, false means it is known
	 * to not be reciprocal and null means it has not yet been determined.
	 */
	@XmlAttribute
	@Nullable
	private Boolean reciprocal;

	/**
	 * although this is also the id, there is no way to query the id across parents. This is
	 * necessary for us to find the courses that an attempt completes. For this reason the attempt
	 * is left null if not courses completed. This also provides us a way to query for any timing
	 * that has not been completed (sort by attemptId, attemptId > "0", etc).
	 * 
	 */
	@Field(indexedFor = "TN-591")
	@Index
	@Nonnull
	@IgnoreSave(IfCourseNotCompleted.class)
	@ApiModelProperty("Activity to complete the course.")
	private ActivityId attemptId;

	/** @see #attemptId */
	public ActivityId attemptId() {
		// the attempt id is always available with the id
		// #attempt id is not
		return getId().attemptId();
	}

	@Field(indexedFor = "TN-591")
	@ApiModelProperty(value = "Activity attempting to be completed.")
	public ActivityId courseId() {
		return getParentId();
	}

	@XmlElement
	public ActivityId getCourseId() {
		return courseId();
	}

	@XmlElement
	public ActivityId getAttemptId() {
		return attemptId;
	}

	@Override
	public ActivityId getParentId() {
		return (ActivityId) super.getParentId();
	}

	@Override
	protected Class<? extends Identifier<?, ?>> parentIdType() {
		return ActivityTimingId.PARENT_ID_TYPE;
	}

	/** Use {@link Builder} to construct ActivityTimingRelation */
	private ActivityTiming() {
	}

	@Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM, indexedFor = "TN-362")
	@Override
	public ActivityTimingId getId() {
		return super.getId();
	}

	/**
	 * @return the course
	 */
	@Nonnull
	@Deprecated
	public ActivityReferenceIds getCourse() {
		return new ActivityReferenceIds.Builder().setActivityId(courseId()).build();
	}

	@Nonnull
	@Deprecated
	public ActivityReferenceIds course() {
		return getCourse();
	}

	/**
	 * @return the attempt
	 */
	@Nonnull
	@Deprecated
	public ActivityReferenceIds getAttempt() {
		return new ActivityReferenceIds.Builder().setActivityId(attemptId()).build();
	}

	@Nonnull
	@Deprecated
	public ActivityReferenceIds attempt() {
		return getAttempt();
	}

	/**
	 * @return the completions
	 */
	public List<Completion> getCompletions() {
		// required when completions are loaded from datastore since timing is not set
		for (Completion completion : completions) {
			completion.timing = this;
		}
		return this.completions;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public List<Completion> get() {
		return getCompletions();
	}

	public List<Completion> completions() {
		return getCompletions();
	}

	/**
	 * @return the failures
	 */
	public Set<TimingCompletionCategory> getFailures() {
		return this.failures;
	}

	public Set<TimingCompletionCategory> failures() {
		return getFailures();
	}

	/**
	 * @return the courseCompleted
	 */
	@XmlAttribute(name = "courseCompleted")
	public Boolean isCourseCompleted() {
		return !this.completions.isEmpty();
	}

	/**
	 * indicates if the course was completed or not. The index maps to attemptId != null.
	 * 
	 * @see #attemptId
	 * @see IfCourseCompleted
	 * @return
	 */
	@Nonnull
	@Field(indexedFor = "TN-591")
	@ApiModelProperty("True if the Attempt completes the Course.")
	public Boolean courseCompleted() {
		return isCourseCompleted();
	}

	/**
	 * @see #reciprocal
	 * 
	 * @see BooleanUtils#isTrue(Boolean)
	 * 
	 */
	@Nullable
	public Boolean reciprocal() {
		return this.reciprocal;
	}

	public Boolean hasReciprocal() {
		return this.reciprocal != null;
	}

	@Override
	protected void postLoad() {
		super.postLoad();
		this.attemptId = id().attemptId();
	}

	/**
	 * A single activity may have multiple Attempts.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.FIELD)
	@Dictionary(domain = ActivityTiming.Completion.FIELD.DOMAIN)
	public static class Completion
			implements Serializable {

		private static final long serialVersionUID = 1927628446799432206L;

		/**
		 * This is a composition of {@link ActivityTiming}. Use {@link Builder} to create.
		 * 
		 */
		private Completion(ActivityTiming relation) {
			this.timing = relation;
		}

		private Completion() {

		}

		@XmlElement(name = Activity.FIELD.DOMAIN)
		@XmlJavaTypeAdapter(ActivityTimingRelationAttemptXmlAdapter.class)
		@Ignore
		private ActivityTiming timing;

		@XmlElement
		@Nonnull
		@Field(key = FIELD.START, dataType = FieldDataType.DATE)
		private SimpleTrackpoint start;

		@XmlElement
		@Nonnull
		@Field(key = FIELD.FINISH, dataType = FieldDataType.DATE)
		private SimpleTrackpoint finish;

		/**
		 * @return the start
		 */
		public Trackpoint getStart() {
			return this.start;
		}

		public Trackpoint start() {
			return getStart();
		}

		/**
		 * @return the finish
		 */
		public Trackpoint getFinish() {
			return this.finish;
		}

		public Trackpoint finish() {
			return this.getFinish();
		}

		/**
		 * @return the timing
		 */
		public ActivityTiming getTiming() {
			return this.timing;
		}

		public ActivityTiming timing() {
			return getTiming();
		}

		/**
		 * time from {@link #start} to {@link #finish}.
		 * 
		 * @return
		 */
		public Duration getDuration() {
			return new Duration(start.getTimestamp(), finish.getTimestamp());
		}

		public Duration duration() {
			return getDuration();
		}

		public Interval interval() {
			return new Interval(start.getTimestamp(), finish.getTimestamp());
		}

		public static class FIELD
				extends BASE_FIELD {
			public static final String COMPLETION = "completion";
			public static final String DOMAIN = COMPLETION;
			public static final String DURATION = "duration";
			public static final String START = "start";
			public static final String FINISH = "finish";

			public static final class KEY {
				public static final FieldKey DURATION = new FieldKey(FIELD.DOMAIN, FIELD.DURATION);
			}
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.finish == null) ? 0 : this.finish.hashCode());
			result = prime * result + ((this.start == null) ? 0 : this.start.hashCode());
			return result;
		}

		/**
		 * THis is required for comparisons of timings.
		 * 
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			Completion other = (Completion) obj;
			return ActivityTimingRelationUtil.completionTimesEqual(this, other);
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return new Interval(start.getTimestamp(), finish.getTimestamp()).toString();

		}
	}

	@XmlRootElement(name = FIELD.FIELDS_NAME)
	@XmlType(namespace = XmlNamespace.API_VALUE, name = FIELD.FIELDS_NAME)
	public static final class FIELD
			extends BaseEntity.BASE_FIELD {
		public static final String DOMAIN = "activityTiming";
		public static final String DOMAIN_PLURAL = DOMAIN + "s";
		static final String FIELDS_NAME = DOMAIN + FIELDS;
		public static final String ACTIVITY_TIMING_RELATION_ID = ActivityTimingWebApiUri.TIMINGS_ID;
		public static final String COURSE = ActivityTimingWebApiUri.COURSE;
		public static final String ATTEMPT = ActivityTimingWebApiUri.ATTEMPT;
		public static final String COMPLETIONS = Completion.FIELD.COMPLETION + "s";

		public static final class KEY {
			public static final FieldKey COURSE = new FieldKey(FIELD.DOMAIN, FIELD.COURSE);
			public static final FieldKey ATTEMPT = new FieldKey(FIELD.DOMAIN, FIELD.ATTEMPT);
			public static final FieldKey COURSE_ACTIVITY_ID = new FieldKey(COURSE,
					ActivityReferenceIds.FIELD.KEY.ACTIVITY_ID);
			public static final FieldKey COURSE_APPLICATION_KEY = new FieldKey(COURSE,
					ActivityReferenceIds.FIELD.KEY.APPLICATION_KEY);
			public static final FieldKey ATTEMPT_ACTIVITY_ID = new FieldKey(ATTEMPT,
					ActivityReferenceIds.FIELD.KEY.ACTIVITY_ID);
			public static final FieldKey ATTEMPT_APPLICATION_KEY = new FieldKey(ATTEMPT,
					ActivityReferenceIds.FIELD.KEY.APPLICATION_KEY);

			public static final FieldKey COMPLETIONS = new FieldKey(FIELD.DOMAIN, FIELD.COMPLETIONS);
			public static final FieldKey DURATION = new FieldKey(COMPLETIONS, Completion.FIELD.KEY.DURATION);
			public final static FieldKey DATE_CREATED = new FieldKey(DOMAIN, FIELD.DATE_CREATED);
		}

		public static class OPTIONS {
			public static final String ACCOUNT = new FieldKey(FIELD.DOMAIN, Account.FIELD.DOMAIN).getAbsoluteKey();
			public static final String ALL = new FieldKey(FIELD.DOMAIN, Account.FIELD.DOMAIN).getAbsoluteKey();

		}
	}

	private static class IfCourseNotCompleted
			extends PojoIf<ActivityTiming> {

		@Override
		public boolean matchesPojo(ActivityTiming timing) {
			return (timing != null && !timing.courseCompleted());
		}

	}

	private static class IfCourseCompleted
			extends PojoIf<ActivityTiming> {

		@Override
		public boolean matchesPojo(ActivityTiming timing) {
			return (timing != null && timing.courseCompleted());
		}

	}

}
