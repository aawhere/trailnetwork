/**
 * 
 */
package com.aawhere.activity.timing;

import com.aawhere.persist.EntityMessage;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Internationalized description for {@link ActivityTiming}.
 * 
 * @author aroller
 * 
 */
public enum ActivityTimingRelationMessage implements Message {

	/** Explain_Entry_Here */
	COURSE_NAME("Course"),
	COURSE_DESCRIPTION("The activity which is a common route of another activity."),
	ACTIVITY_TIMING_RELATION_NAME("Timing Ranking"),
	ACTIVITY_TIMING_RELATION_DESCRIPTION(
			"The 'race' results of comparing an activity to another activity that it completed from start to finish. "),
	ID_NAME("Internal Identifier"),
	ID_DESCRIPTION("Used to reference a timing results.  It is a composite of the Course-Attempt"),
	ATTEMPT_NAME("Attempt"),
	ATTEMPT_DESCRIPTION("The activity that is ranked amongst all who have completed the course from start to finish"),
	START_NAME("Start"),
	START_DESCRIPTION(
			"The beginning of the course (starting line).  This is the first point of the course and not typically the start for the attempt."),
	FINISH_NAME("Finish"),
	FINISH_DESCRIPTION(
			"The end of the course (finish line). This is the last point of the course and not typically the last for the attempt."),

	PROCESSOR_VERSION_NAME("Processor Version"),
	PROCESSOR_VERSION_DESCRIPTION("Indicates which set of rules were applied during the creation of this relationship."),
	/** Message after notification has completed. */
	APPLICATION_NOTIFIED_OF_COURSE_COMPLETION(
			"{APPLICATION} notified that activity {ATTEMPT} completed the course {COURSE}.",
			Param.APPLICATION,
			Param.COURSE,
			Param.ATTEMPT),
	APPLICATION_NOTIFIED_OF_COURSE_COMPLETION_NOT_DESIRED(
			"{APPLICATION} isn't interested in notifications for {ATTEMPT} completing the course {COURSE}.",
			Param.APPLICATION,
			Param.COURSE,
			Param.ATTEMPT),
	TIMING_NAME("Timing"),
	TIMING_DESCRIPTION(""),
	COURSE_COMPLETED_NAME("Course Completed"),
	COURSE_COMPLETED_DESCRIPTION("True if the course was completed by the attempt."),
	DATE_CREATED_NAME(EntityMessage.DATE_CREATED_NAME.getValue()), /* */
	DATE_CREATED_DESCRIPTION(EntityMessage.DATE_CREATED_DESCRIPTION.getValue()), /* */
	DATE_UPDATED_NAME(EntityMessage.DATE_UPDATED_NAME.getValue()), /* */
	DATE_UPDATED_DESCRIPTION(EntityMessage.DATE_UPDATED_DESCRIPTION.getValue()), /* */
	DURATION_NAME("Duration"),
	DURATION_DESCRIPTION("The time it took the attempt to complete the course (milliseconds)."),
	/** Used when a new version is being processed. */
	QUEUED_FOR_UPDATE("{COUNT} Timing Relations queued for updating to version {VERSION}.", Param.COUNT, Param.VERSION),
	/** Indicates the latest version has already been queued for updating */
	VERSION_ALREADY_QUEUED(
			"Timings previously queued to process on {WHEN} for version {VERSION} .",
			Param.VERSION,
			Param.WHEN);

	private ParamKey[] parameterKeys;
	private String message;

	private ActivityTimingRelationMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		APPLICATION, COURSE, ATTEMPT, COUNT,
		/**
		 * {@link ActivityTimingRelationProcessorVersion}
		 * 
		 */
		VERSION,
		/**
		 * Anything that represents time that can be localized. Probably date.
		 */
		WHEN;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
