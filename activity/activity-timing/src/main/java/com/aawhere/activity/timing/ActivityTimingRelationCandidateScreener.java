/**
 * 
 */
package com.aawhere.activity.timing;

import static com.aawhere.activity.timing.ActivityTimingRelationUserStory.*;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.spatial.ActivitySpatialRelationEndpointVerifier;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCellComparison;
import com.aawhere.track.TrackSummaryUtil;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * An object specializing in screening two activities to see if they could make a compatible
 * {@link ActivityTiming}.
 * 
 * This works only with information provided from Activity. TN-338
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationCandidateScreener {

	/**
	 * Provides the default fudge factor allowing geocells a little bit of tolerance to differences
	 * because they are geocells after all. TN-465 increased this value from 2 to 4. TN-623 Turned
	 * this value into a ratio to avoid passing short courses with only a few more cells than 4.x
	 * 
	 */
	private static final Integer GEO_CELL_COMPARISON_MIN_TOLERANCE = 1;
	private static final Ratio GEO_CELL_COMPARISON_TOLERANCE = MeasurementUtil.ratio(3, 100);
	private Activity course;
	private Activity attempt;
	private TimingCompletionCategory reason;
	/**
	 * Indicates that the geo cells are similar enough to indicate they may be reciprocals of each
	 * other. This is useful for course-focused screening.
	 * 
	 */
	public Boolean reciprocalCandidate;;

	/**
	 * Used to construct all instances of ActivityTimingRelationCandidateScreener.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<ActivityTimingRelationCandidateScreener> {

		/**
		 * The extra cushion to put around the track summary bounding box of the attempt to see if
		 * it contains the course. We're using a factor larger since geocell bounding boxes are
		 * still heavily used and will create large jumps. The bounding box is being stored since
		 * version 24 so those original activities are no longer being processed this can be
		 * reduced.
		 */
		private static final Length OUT_OF_BOUNDS_BUFFER = QuantityMath
				.create(ActivityTimingRelationUserStory.DEFAULT_BUFFER_WIDTH).times(4).getQuantity();
		private MultiLineString attemptLine;
		private AttemptLineProvider attemptLineProvider;

		public Builder() {
			super(new ActivityTimingRelationCandidateScreener());
		}

		public Builder course(Activity course) {
			building.course = course;
			return this;
		}

		public Builder attempt(Activity attempt) {
			building.attempt = attempt;
			return this;
		}

		public Builder attemptLineProvider(AttemptLineProvider provider) {
			this.attemptLineProvider = provider;
			return this;
		}

		public Builder attemptLine(MultiLineString attemptLine) {
			this.attemptLine = attemptLine;
			return this;
		}

		@Override
		public void validate() {
			super.validate();
			Assertion.exceptions().notNull("course", building.course);
			Assertion.exceptions().notNull("attempt", building.attempt);
		}

		@Override
		public ActivityTimingRelationCandidateScreener build() {
			ActivityTimingRelationCandidateScreener built = super.build();

			GeoCellComparison geoCellComparison;

			// Run some pre-checks to avoid unnecessary processing and noisy results
			// TN-277 Identical Activities shouldn't produce Timing completions
			if (TrackSummaryUtil.isLikelyFromTheSameTrack(	built.course.getTrackSummary(),
															built.attempt.getTrackSummary())) {
				// TODO:Handle identical tracks. remove? associate accounts?
				// built.resultBuilder = new ActivityTimingRelation.Builder();
				built.reason = TimingCompletionCategory.IDENTICAL_TRACKS;
			}

			// TN-278 Short Activities should not qualify as a course for timings
			else if (!isLongEnough(built.course)) {
				built.reason = TimingCompletionCategory.COURSE_TOO_SHORT;
			}
			// TN-476
			else if (!hasBigEnoughFootprint(built.course)) {
				built.reason = TimingCompletionCategory.COURSE_FOOTPRINT_TOO_SMALL;

			} else if (!hasBigEnoughFootprint(built.attempt)) {
				built.reason = TimingCompletionCategory.ATTEMPT_FOOTPRINT_TOO_SMALL;
			}
			// TN-310
			else if (!isLongEnough(built.course, built.attempt)) {
				built.reason = TimingCompletionCategory.ATTEMPT_TOO_SHORT;
			}
			// TN-623 track summary bounds validation is efficient and more accurate than geocells
			else if (!BoundingBoxUtil.expand(built.attempt.boundingBox(), OUT_OF_BOUNDS_BUFFER)
					.contains(built.course.boundingBox())) {
				built.reason = TimingCompletionCategory.OUT_OF_BOUNDS;
			}
			// TN-305 - Don't bother comparing spatially non-similar activities.
			else if (!(geoCellComparison = GeoCellComparison.left(built.attempt.getTrackSummary().getSpatialBuffer())
					.right(built.course.getTrackSummary().getSpatialBuffer()).tolerance(GEO_CELL_COMPARISON_TOLERANCE)
					.tolerance(GEO_CELL_COMPARISON_MIN_TOLERANCE).build()).contains()) {
				built.reason = TimingCompletionCategory.NOT_SIMILAR_ENOUGH;
			} else {
				// TN-813 Reciprocals create courses and routes
				built.reciprocalCandidate = geoCellComparison.equalsWithinTolerance();

				endpointsReached(built);
			}
			// else
			return built;
		}

		/**
		 * Does an efficient check to see if the start/finish has been reached.
		 * 
		 * This will assign the failure reason directly.
		 * 
		 * @param built
		 */
		private Boolean endpointsReached(final ActivityTimingRelationCandidateScreener built) {
			if (this.attemptLine == null && this.attemptLineProvider != null) {
				this.attemptLine = this.attemptLineProvider.attemptLine(built.attempt);
			}
			Boolean passed;
			if (this.attemptLine != null) {
				// now, run spatial check against the track to see if the start/end is reached
				ActivitySpatialRelationEndpointVerifier verifier = ActivitySpatialRelationEndpointVerifier.create()
						.activity(built.course).multiLineString(attemptLine).setMaxLengthAway(GOAL_LINE_LENGTH).build();
				if (!verifier.passed()) {
					TimingCompletionCategory failure;
					if (verifier.startInRange()) {
						failure = TimingCompletionCategory.DID_NOT_FINISH;
					} else {
						failure = TimingCompletionCategory.DID_NOT_START;
					}
					built.reason = failure;
				}
				passed = verifier.passed();
			} else {
				// we checked all that could be checked...
				passed = true;
			}
			return passed;

		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTimingRelationCandidateScreener */
	private ActivityTimingRelationCandidateScreener() {
	}

	/**
	 * True indicates the candidate passed screening.
	 * 
	 * @return
	 */
	public Boolean passed() {
		return reason == null;
	}

	public Boolean failed() {
		return !passed();
	}

	/**
	 * @return the reciprocalCandidate
	 */
	public Boolean reciprocalCandidate() {
		return this.reciprocalCandidate;
	}

	/**
	 * @return the reason
	 */
	public TimingCompletionCategory reason() {
		return this.reason;
	}

	/**
	 * @return the course
	 */
	public Activity course() {
		return this.course;
	}

	/**
	 * @return the attempt
	 */
	public Activity attempt() {
		return this.attempt;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return timingFailed().toString() + ":" + passed() + ":" + reason;
	}

	public static interface AttemptLineProvider {
		/**
		 * Given the attempt activity this will return the MultiLineString. Allows a way to reduce
		 * retrieving the line string if other calls have already failed.
		 * 
		 * @param attempt
		 * @return
		 */
		public MultiLineString attemptLine(Activity attempt);
	}

	/**
	 * Only to be used when {@link #failed()} since {@link #passed()} indicates a need for deep
	 * processing.
	 * 
	 * @return
	 */
	public ActivityTiming timingFailed() {
		return ActivityTiming.create().courseId(course.id()).attemptId(attempt.id()).addFailure(reason).build();
	}

	/**
	 * Identifies what is being screened.
	 * 
	 * @return
	 */
	public ActivityTimingId candidateId() {
		return new ActivityTimingId(course.id(), attempt.id());
	}

	/** Communicates the passing and failing of timing candidates for screening. */
	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE, name = "timingScreeningResults")
	@XmlAccessorType(XmlAccessType.NONE)
	public static class ScreeningResults
			implements Serializable {

		private static final long serialVersionUID = -8300718722720008771L;
		private HashMap<ActivityTimingId, ActivityTimingRelationCandidateScreener> passers = new HashMap<>();
		@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
		private HashMultimap<TimingCompletionCategory, ActivityTimingId> failers = HashMultimap.create();

		/**
		 * Reports those items not screened again because they already exist. These are just ids and
		 * do not indicate failing nor passing.
		 * 
		 */
		@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
		private Set<ActivityTimingId> existing;

		/**
		 * Reports which timings were not screened because they already exist.
		 * 
		 * @see ActivityTimingRelationCandidateScreener#report(Iterable)
		 * */
		private ScreeningResults existingResults;

		@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
		private List<ActivityTimingId> reciprocalCandidates = Lists.newArrayList();

		/**
		 * for jaxb
		 */
		ScreeningResults() {
		}

		private void add(ActivityTimingRelationCandidateScreener screener) {
			if (screener.passed()) {
				passers.put(screener.candidateId(), screener);
				// TN-813 targeting reciprocals creates efficiency
				if (screener.reciprocalCandidate()) {
					reciprocalCandidates.add(screener.candidateId());
				}
			} else {
				failers.put(screener.reason(), screener.candidateId());
			}
		}

		/**
		 * @return the failers
		 */
		public Multimap<TimingCompletionCategory, ActivityTimingId> failers() {
			// not immutable because nulls happen
			return this.failers;
		}

		/**
		 * @return the passers
		 */
		public Map<ActivityTimingId, ActivityTimingRelationCandidateScreener> passers() {
			// not immutable because nulls happen
			return this.passers;
		}

		@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
		public Collection<ActivityTimingRelationCandidateScreener> getPassingScreeners() {
			return passingScreeners();
		}

		public Collection<ActivityTimingRelationCandidateScreener> passingScreeners() {
			return this.passers.values();
		}

		/**
		 * @return the existing
		 */
		public Set<ActivityTimingId> existing() {
			return this.existing;
		}

		public List<ActivityTimingId> reciprocalCandidates() {
			return this.reciprocalCandidates;
		}

		public ScreeningResults existingResults() {
			return this.existingResults;

		}
	}

	/** Combines the {@link ScreeningResults} with results from running the processors directly. */
	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.NONE)
	public static class VerifiedTimingResults {
		@XmlElement
		private Set<ActivityTiming> completed;
		@XmlElement
		private Set<ActivityTiming> failed;
		@XmlElement
		private ScreeningResults screeningResults;

		/**
		 * for jaxb
		 */
		VerifiedTimingResults() {
		}

		public VerifiedTimingResults(ScreeningResults screeningResults, Set<ActivityTiming> completed,
				Set<ActivityTiming> failed) {
			super();
			this.screeningResults = screeningResults;
			this.completed = completed;
			this.failed = failed;
		}

		public Set<ActivityTiming> completed() {
			return this.completed;
		}

		public Set<ActivityTiming> failed() {
			return this.failed;
		}

		public ScreeningResults screeningResults() {
			return this.screeningResults;
		}

	}

	/**
	 * Iterates through the courses given comparing it to the single attempt providing a
	 * {@link ScreeningResults} to quantify the results.
	 * 
	 * @param attempt
	 * @param courseIterator
	 * @param attemptLine
	 * @return the quantified results...hopefully mostly passers
	 */
	public static ScreeningResults coursesScreened(Activity attempt, final Iterable<Activity> courseIterator,
			MultiLineString attemptLine, Set<ActivityTimingId> existingIds) {
		ScreeningResults results = new ScreeningResults();
		results.existing = existingIds;

		// used by the story to verify endpoints, getting once is good!
		final ActivityId attemptId = attempt.getId();
		for (Activity course : courseIterator) {
			ActivityId courseId = course.id();
			if (!attemptId.equals(courseId)) {
				ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
						.course(course).attempt(attempt).attemptLine(attemptLine).build();
				results.add(screener);
			}
		}

		return results;
	}

	/**
	 * Bulk screens the activiites given and reports in the {@link ScreeningResults}. Also given
	 * existing timings this will summarize the results in the {@link ScreeningResults#existing()}
	 * which can be used as validation.
	 * 
	 * {@link TimingCompletionCategory#DID_NOT_START} and
	 * {@link TimingCompletionCategory#DID_NOT_FINISH} are not screened since that requires a
	 * spatial buffer which can be costly since each attempt spatial buffer would be loaded. It can
	 * be added as an option if needed.
	 * 
	 * @param course
	 * @param activities
	 * @param values
	 * @return
	 */
	public static ScreeningResults attemptsScreened(Activity course, Iterable<Activity> activities,
			Iterable<ActivityTiming> existingTimings) {

		ScreeningResults results = new ScreeningResults();
		results.existingResults = report(existingTimings);
		for (Activity attempt : activities) {
			ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
					.course(course).attempt(attempt).build();
			results.add(screener);
		}
		return results;
	}

	/**
	 * Reports existing timings in {@link ScreeningResults}. {@link ScreeningResults#passers()} will
	 * contain only keys, not values since screening did not take place now.
	 * {@link ScreeningResults#failers()} will report what was previously known.
	 * 
	 * @param timings
	 * @return
	 */
	@Nullable
	public static ScreeningResults report(Iterable<ActivityTiming> timings) {
		ScreeningResults results = null;
		for (ActivityTiming timing : timings) {
			if (results == null) {
				results = new ScreeningResults();
			}
			ActivityTimingId timingId = timing.id();
			if (timing.isCourseCompleted()) {
				results.passers.put(timingId, null);
				if (BooleanUtils.isTrue(timing.reciprocal())) {
					results.reciprocalCandidates.add(timingId);
				}
			} else {

				results.failers.put(TimingCompletionCategory.EXISTING, timingId);
			}
		}
		return results;
	}

	public static Function<ActivityTimingRelationCandidateScreener, ActivityTimingId> candidateIdFunction() {
		return new Function<ActivityTimingRelationCandidateScreener, ActivityTimingId>() {

			@Override
			public ActivityTimingId apply(ActivityTimingRelationCandidateScreener input) {
				return (input == null) ? null : input.candidateId();
			}
		};
	}

}
