/**
 * 
 */
package com.aawhere.activity.timing;

import javax.xml.bind.annotation.XmlTransient;

import com.googlecode.objectify.condition.PojoIf;

/**
 * @author aroller
 *
 */
/**
 * Used to slim down timing relations that are stored for archive purposes allowing a timing
 * relation to be persisted indicated a course is not completed. usage
 * 
 * @Ignore(IfCourseNotCompleted.class)
 * 
 * @author aroller
 * 
 */
@XmlTransient
public class IfCourseNotCompleted
		extends PojoIf<ActivityTiming> {

	/*
	 * (non-Javadoc)
	 * @see com.googlecode.objectify.condition.If#matchesPojo(java.lang.Object)
	 */
	@Override
	public boolean matchesPojo(ActivityTiming pojo) {
		boolean courseCompleted;
		if (pojo != null) {
			courseCompleted = pojo.courseCompleted();
		} else {
			courseCompleted = false;
		}
		return !courseCompleted;
	}

}
