/**
 * 
 */
package com.aawhere.activity.timing;

import java.util.Comparator;
import java.util.List;

import org.joda.time.Duration;

import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.lang.ComparatorResult;
import com.aawhere.lang.ObjectBuilder;

/**
 * Provides the default ordering of timings by shortest duration first.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationDurationComparator
		implements Comparator<ActivityTiming> {

	/**
	 * Used to construct all instances of ActivityTimingRelationDurationComparator.
	 */
	public static class Builder
			extends ObjectBuilder<ActivityTimingRelationDurationComparator> {

		public Builder() {
			super(new ActivityTimingRelationDurationComparator());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTimingRelationDurationComparator */
	private ActivityTimingRelationDurationComparator() {
	}

	/**
	 *
	 * 
	 */
	@Override
	public int compare(ActivityTiming dis, ActivityTiming dat) {

		int disIsBetter = ComparatorResult.LESS_THAN.getValue();
		int datIsBetter = ComparatorResult.GREATER_THAN.getValue();
		int equal = ComparatorResult.EQUAL.getValue();
		if (dis == null && dat == null) {
			return equal;
		}
		if (dat == null) {
			return disIsBetter;
		}
		if (dis == null) {
			return datIsBetter;
		}
		List<Completion> disCompletions = dis.getCompletions();
		List<Completion> datCompletions = dat.getCompletions();
		if (disCompletions.isEmpty() && datCompletions.isEmpty()) {
			return equal;
		}

		Completion disBest = ActivityTimingRelationUtil.quickest(dis);
		Completion datBest = ActivityTimingRelationUtil.quickest(dat);
		if (disBest == null) {
			return datIsBetter;
		} else if (datBest == null) {
			return disIsBetter;
		}

		Duration disDuration = disBest.getDuration();
		Duration datDuration = datBest.getDuration();
		// let's do our best to avoid equality so we can provide a consistent order
		return disDuration.compareTo(datDuration);
	}

}
