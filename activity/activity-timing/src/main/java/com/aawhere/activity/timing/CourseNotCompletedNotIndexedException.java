package com.aawhere.activity.timing;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

@StatusCode(value = HttpStatusCode.BAD_REQUEST, message = "{FIELD_KEY} only supports querying for true.")
public class CourseNotCompletedNotIndexedException
		extends BaseRuntimeException {

	private static final long serialVersionUID = -1928527649685002713L;

	public CourseNotCompletedNotIndexedException() {
		super(CourseNotCompletedNotIndexedExceptionMessage.message(ActivityTimingField.Key.COURSE_COMPLETED));
	}
}
