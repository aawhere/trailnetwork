/**
 * 
 */
package com.aawhere.activity.timing;

import javax.annotation.Nullable;

import com.aawhere.activity.ActivityId;

import com.google.common.base.Function;

/**
 * For a repeat course this will produce the ids for a timing given the attempts via
 * {@link #apply(ActivityId)}.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationIdFromAttemptIdFunction
		implements Function<ActivityId, ActivityTimingId> {

	private ActivityId courseId;

	/**
	 * use {@link #build(ActivityId)}
	 */
	private ActivityTimingRelationIdFromAttemptIdFunction() {
	}

	public static ActivityTimingRelationIdFromAttemptIdFunction build(ActivityId courseId) {
		ActivityTimingRelationIdFromAttemptIdFunction function = new ActivityTimingRelationIdFromAttemptIdFunction();
		function.courseId = courseId;
		return function;
	}

	/**
	 * @return the {@link ActivityTimingId} of {@link #courseId} with the given attemptId or null if
	 *         they are equal since timing relations against itself are not valid.
	 * 
	 */
	@Override
	@Nullable
	public ActivityTimingId apply(ActivityId attemptId) {

		if (attemptId == null || courseId.equals(attemptId)) {
			return null;
		}
		return new ActivityTimingId(this.courseId, attemptId);
	}
}
