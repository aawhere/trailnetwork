/**
 * 
 */
package com.aawhere.activity.timing;

import javax.annotation.Nullable;

import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class ActivityTimingRelationFromCompletionFunction
		implements Function<Completion, ActivityTiming> {

	/**
	 * Used to construct all instances of ActivityTimingRelationFromCompletionFunction.
	 */
	public static class Builder
			extends ObjectBuilder<ActivityTimingRelationFromCompletionFunction> {

		public Builder() {
			super(new ActivityTimingRelationFromCompletionFunction());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTimingRelationFromCompletionFunction */
	private ActivityTimingRelationFromCompletionFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public ActivityTiming apply(@Nullable Completion input) {
		if (input == null) {
			return null;
		}
		return input.getTiming();
	}

}
