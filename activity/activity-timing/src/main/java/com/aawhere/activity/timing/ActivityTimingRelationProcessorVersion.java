/**
 * 
 */
package com.aawhere.activity.timing;

import com.aawhere.activity.ProcessorVersion;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Provides the history of changes made to relations processing allowing for upgrades to existing
 * relations in the case the changes may change the results. Each version provides an explanation of
 * what happened.
 * 
 * 
 * @author aroller
 * 
 */
public enum ActivityTimingRelationProcessorVersion implements Message, ProcessorVersion {
	/** */
	A("Launch of the TrailNetwork. TN-236"),

	B("Support for multiple visits. TN-248; Detailed Course Validation. TN-233"),

	C("improved finish area detection to avoid false starts. TN-283 TN-284"),

	D("Lengthened the allowable deviation length from 200m to 400m. TN-282");

	private static ParamKey[] EMPTY_KEYS = {};
	private String message;

	private ActivityTimingRelationProcessorVersion(String message, ParamKey... parameters) {
		this.message = message;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return EMPTY_KEYS;
	}

}
