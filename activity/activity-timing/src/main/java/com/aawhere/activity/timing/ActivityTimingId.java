/**
 * 
 */
package com.aawhere.activity.timing;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.activity.ActivityId;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierWithParent;
import com.aawhere.id.StringIdentifierWithParent;
import com.aawhere.lang.string.FromString;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.base.Functions;

/**
 * The {@link Identifier} for {@link ActivityTiming}. It is a composite Identifier of the course and
 * attempt activities that this timing represents.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE, name = ActivityTiming.FIELD.ACTIVITY_TIMING_RELATION_ID)
public class ActivityTimingId
		extends StringIdentifierWithParent<ActivityTiming, ActivityId> {

	private static final long serialVersionUID = -4148362417659586219L;
	private static final Function<String, String> VALUE_FUNCTION = Functions
			.compose(Functions.toStringFunction(), Functions.compose(ActivityTimingRelationUtil
					.attemptIdFromTimingIdFunction(), ActivityTimingRelationUtil.timingIdFromStringFunction()));
	private static final Function<String, ActivityId> PARENT_FUNCTION = Functions.compose(ActivityTimingRelationUtil
			.courseIdFunction(), ActivityTimingRelationUtil.timingIdFromStringFunction());
	static final Class<ActivityId> PARENT_ID_TYPE = ActivityId.class;
	static final Class<ActivityTiming> KIND = ActivityTiming.class;

	/**
	 * A freind to frameworks.
	 * 
	 */
	ActivityTimingId() {
		super(KIND);
	}

	/**
	 * @param value
	 * @param KIND
	 */
	public ActivityTimingId(String value, ActivityId courseId) {
		super(value, courseId, KIND);
	}

	/**
	 * supports {@link FromString} principles.
	 * 
	 * @param value
	 */
	public ActivityTimingId(String value) {
		super(IdentifierWithParent.<String, ActivityId> create().compositeValue(value).parentType(PARENT_ID_TYPE)
				.kind(KIND).valueFunction(VALUE_FUNCTION).parentFunction(PARENT_FUNCTION).build());
	}

	/**
	 * This is backwards compatible with the previous non-parent approach of course-attempt.
	 * 
	 * @param courseId
	 * @param attemptId
	 */
	public ActivityTimingId(ActivityId courseId, ActivityId attemptId) {
		this(attemptId.toString(), courseId);
	}

	/**
	 * Creates the composite id from a given entity which has {@link ActivityTiming#getCourse()} and
	 * {@link ActivityTiming#getAttempt()} populated.
	 * 
	 * @param existing
	 */
	public ActivityTimingId(ActivityTiming existing) {
		this(existing.courseId(), existing.attemptId());
	}

	public ActivityId courseId() {
		return getParent();
	}

	public ActivityId attemptId() {
		return new ActivityId(getValue());
	}

	@XmlTransient
	@Override
	public ActivityId getParent() {
		return super.getParent();
	}

	/**
	 * @return
	 */
	public ActivityId getCourseId() {
		return courseId();
	}

	public ActivityId getAttemptId() {
		return attemptId();
	}
}
