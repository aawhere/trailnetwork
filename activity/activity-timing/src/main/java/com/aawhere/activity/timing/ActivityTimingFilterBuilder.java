/**
 * 
 */
package com.aawhere.activity.timing;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.activity.ActivityId;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.field.FieldKey;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseFilterBuilder;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;

/**
 * Builds the filters to produce {@link ActivityTimingRelations}.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingFilterBuilder
		extends BaseFilterBuilder<ActivityTimingFilterBuilder> {

	/**
	 * @see #create()
	 */
	private ActivityTimingFilterBuilder() {
		super();
	}

	/**
	 * @param mutant
	 */
	private ActivityTimingFilterBuilder(Filter mutant) {
		super(mutant);
	}

	/** Provides the basic filter ordering by duration */
	public static ActivityTimingFilterBuilder create() {
		return new ActivityTimingFilterBuilder();
	}

	public static ActivityTimingFilterBuilder clone(Filter original) {
		return new ActivityTimingFilterBuilder(Filter.cloneForBuilder(original));
	}

	/**
	 * Only adds sorting if no sorting is yet present. This is automatically called during
	 * {@link #build()}.
	 * 
	 * If conditions are provided then a sort must be provided that is suitable for those
	 * conditions.
	 * 
	 * If no conditions are provided this will sort by {@link #orderByRecentlyCreated()} to provide
	 * the most likely view and a supported index.
	 * 
	 * 
	 * 
	 * @return
	 */
	public ActivityTimingFilterBuilder addDefaultSortIfNoneAlready() {
		if (!building.hasSortOrder()) {
			if (building.getConditions().isEmpty()) {
				orderByRecentlyCreated();
			}
		}
		return this;
	}

	/**
	 * The default sort order by duration ascending so the shortest time first shows ranking from
	 * best to worst.
	 * 
	 * @deprecated this no longer has an index
	 */
	@Deprecated
	public ActivityTimingFilterBuilder orderByShortestDuration() {

		throw new UnsupportedOperationException();
	}

	@Deprecated
	public ActivityTimingFilterBuilder orderByRecentlyCreated() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Provides the filter for the course provided any {@link ActivityId}. It will create a query
	 * against {@link ActivityId} if it is our id or leave it as an {@link ActivityId} otherwise.
	 * 
	 * This is only for completed timings. To find non-completed timings you may use
	 * {@link #setParent(com.aawhere.id.Identifier)}.
	 * 
	 * @param courseId
	 * @return
	 */
	public ActivityTimingFilterBuilder courseCompleted(ActivityId courseId) {
		setParent(courseId);
		courseCompleted();
		return this;
	}

	/**
	 * Filters for the course (parent) resulting in all children unless other filters are invoked.
	 * 
	 * @see #courseCompleted(ActivityId)
	 * @param courseId
	 * @return
	 */
	public ActivityTimingFilterBuilder course(ActivityId courseId) {
		setParent(courseId);
		return this;
	}

	/**
	 * Simply applies a condition that will ensure timings found will have confirmed completions.
	 * 
	 * @return
	 */
	public ActivityTimingFilterBuilder courseCompleted() {
		// this may not be the best way to ensure...
		// consider sort by attemptId
		// consider attemptId > "0"
		return addCondition(FilterCondition.create().field(ActivityTimingField.Key.ATTEMPT_ID)
				.operator(FilterOperator.NOT_EQUALS).value(null).build());
	}

	/**
	 * provides the filter for timings of an activity when the activity acted as the attempt.
	 * 
	 * @param attemptId
	 * @return
	 */
	public ActivityTimingFilterBuilder attempt(ActivityId attemptId) {
		addCondition(FilterCondition.create().field(ActivityTimingField.Key.ATTEMPT_ID).operator(FilterOperator.EQUALS)
				.value(attemptId).build());
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.ObjectBuilder#build()
	 */
	@Override
	public Filter build() {
		courseIdIsParent();
		courseCompletedFieldExtracted();
		Filter built = super.build();

		return built;
	}

	@SuppressWarnings("rawtypes")
	private void courseCompletedFieldExtracted() {
		Collection<FilterCondition> removed = removed(ActivityTimingField.Key.COURSE_COMPLETED);
		if (CollectionUtils.isNotEmpty(removed)) {
			FilterCondition filterCondition = removed.iterator().next();
			Boolean courseCompleted = (Boolean) filterCondition.getValue();
			if (BooleanUtils.isTrue(courseCompleted)) {
				courseCompleted();
			} else {
				throw new CourseNotCompletedNotIndexedException();
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private void courseIdIsParent() {
		// super.build() deals with parent...so extract it first if necessary
		Collection<FilterCondition> removed = removed(ActivityTimingField.Key.COURSE_ID);
		if (CollectionUtils.isNotEmpty(removed)) {
			FilterCondition filterCondition = removed.iterator().next();
			ActivityId parentId = (ActivityId) filterCondition.getValue();
			setParent(parentId);
		}
	}

	/**
	 * Restricts only to the given {@link Application}s.
	 * 
	 * @param applicationKey
	 * @return
	 */
	public ActivityTimingFilterBuilder courseApplication(ApplicationKey... applicationKeys) {
		FieldKey key = ActivityTiming.FIELD.KEY.COURSE_APPLICATION_KEY;
		application(key, applicationKeys);
		return this;
	}

	/**
	 * @param key
	 * @param applicationKeys
	 */
	private void application(FieldKey key, ApplicationKey... applicationKeys) {
		addCondition(new FilterCondition.Builder<ApplicationKey[]>().field(key).operator(FilterOperator.IN)
				.value(applicationKeys).build());
	}

	/**
	 * Includes only those timings where the attempt is one of the given applicationKeys.
	 * 
	 * @param applicationKeys
	 * @return
	 */
	public ActivityTimingFilterBuilder attemptApplication(ApplicationKey... applicationKeys) {
		application(ActivityTiming.FIELD.KEY.ATTEMPT_APPLICATION_KEY, applicationKeys);
		return this;
	}

	/**
	 * Ensures both attempt and course have matches within the given application keys.
	 * 
	 * @param applicationKeys
	 * @return
	 */
	public ActivityTimingFilterBuilder application(ApplicationKey... applicationKeys) {
		courseApplication(applicationKeys);
		attemptApplication(applicationKeys);
		return this;
	}

}
