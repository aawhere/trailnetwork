/**
 * 
 */
package com.aawhere.activity.timing;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityReferenceIds;

/**
 * A simple adapter that produces the {@link ActivityId} from the entire timing.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationAttemptXmlAdapter
		extends XmlAdapter<ActivityReferenceIds, ActivityTiming> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public ActivityTiming unmarshal(ActivityReferenceIds v) throws Exception {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public ActivityReferenceIds marshal(ActivityTiming timing) throws Exception {
		if (timing == null) {
			return null;
		}
		return timing.attempt();
	}

}
