/**
 * 
 */
package com.aawhere.activity.spatial;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.spatial.ActivitySpatialRelationEndpointVerifier.Builder;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.Track;

import com.vividsolutions.jts.geom.MultiLineString;

/**
 * @author aroller
 * 
 */
public class ActivitySpatialRelationEndpointVerifierUnitTest {

	/**
	 * The track that is used to create the line string.
	 * 
	 */
	protected static final Track ATTEMPT_TRACK = LINE;
	private MultiLineString multiLineString;
	private Builder verifierBuilder;

	@Before
	public void setUp() {
		this.multiLineString = JtsTrackGeomUtil.trackToMultiLineString(ATTEMPT_TRACK, true);
		this.verifierBuilder = ActivitySpatialRelationEndpointVerifier.create().multiLineString(multiLineString)
				.setMaxLengthAway(MeasurementUtil.createLengthInMeters(50));
	}

	/**
	 * @return the multiLineString
	 */
	public MultiLineString getMultiLineString() {
		return this.multiLineString;
	}

	@Test
	public void sameTrack() {
		Activity activity = ActivityTestUtil.createActivity(LINE_TIME_SHIFT);
		Boolean startExpected = true;
		Boolean endExpected = true;
		assertEndpoints(activity, startExpected, endExpected);
	}

	@Test
	public void didNotStart() {
		Activity activity = ActivityTestUtil.createActivity(LINE_BEFORE);
		Boolean startExpected = false;
		Boolean endExpected = null;
		assertEndpoints(activity, startExpected, endExpected);
	}

	@Test
	public void didNotFinish() {
		assertEndpoints(ActivityTestUtil.createActivity(LINE_LONGER), true, false);
	}

	/**
	 * @param activity
	 * @param startExpected
	 * @param endExpected
	 * @return
	 */
	public ActivitySpatialRelationEndpointVerifier assertEndpoints(Activity activity, Boolean startExpected,
			Boolean endExpected) {
		ActivitySpatialRelationEndpointVerifier verifier = verifierBuilder.activity(activity).build();
		assertEquals("start", startExpected, verifier.startInRange());
		assertEquals("end", endExpected, verifier.endInRange());
		return verifier;
	}
}
