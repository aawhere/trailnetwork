/**
 * 
 */
package com.aawhere.activity.timing;

import static com.aawhere.track.ExampleTracks.*;

import com.aawhere.track.SimpleTrack;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;

/**
 * @author aroller
 * 
 */
public class ActivityTimingRelationCourseTooShortUserStoryUnitTest
		extends ActivityTimingRelationUserStoryUnitTest {

	/**
	 * 
	 */
	public ActivityTimingRelationCourseTooShortUserStoryUnitTest() {
		super(new SimpleTrack.TrackpointCollectionBuilder().add(P1_MICRO).add(P1_T2).getTrack(), LINE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.relation.ActivityTimingRelationUserStoryUnitTest#assertCreated()
	 */
	@Override
	protected void assertCreated() {
		final TimingCompletionCategory expectedFailure = TimingCompletionCategory.COURSE_TOO_SHORT;
		assertFailure(expectedFailure, true);

	}

}
