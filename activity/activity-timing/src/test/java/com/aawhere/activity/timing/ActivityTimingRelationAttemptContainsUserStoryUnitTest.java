/**
 * 
 */
package com.aawhere.activity.timing;

import com.aawhere.track.ExampleTracks;

/**
 * A positive result where the course fits within the attempt making it a good candidate.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationAttemptContainsUserStoryUnitTest
		extends ActivityTimingRelationUserStoryUnitTest {

	/**
	 * 
	 */
	public ActivityTimingRelationAttemptContainsUserStoryUnitTest() {
		super(ExampleTracks.LINE, ExampleTracks.LINE_BEFORE_AFTER);
	}

}
