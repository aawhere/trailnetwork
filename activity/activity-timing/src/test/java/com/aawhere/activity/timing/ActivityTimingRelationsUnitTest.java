/**
 * 
 */
package com.aawhere.activity.timing;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.activity.timing.ActivityTiming;
import com.aawhere.activity.timing.ActivityTimingRelations;
import com.aawhere.persist.BaseEntitiesBaseUnitTest;
import com.aawhere.personalize.xml.XmlAdapterTestUtils;

/**
 * @author aroller
 * 
 */
public class ActivityTimingRelationsUnitTest
		extends BaseEntitiesBaseUnitTest<ActivityTiming, ActivityTimingRelations, ActivityTimingRelations.Builder> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#createEntitySampleWithId()
	 */
	@Override
	public ActivityTiming createEntitySampleWithId() {

		return ActivityTimingRelationUnitTest.getInstance().getEntitySampleWithId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return ActivityTimingTestUtil.adapters();
	}

}
