/**
 * 
 */
package com.aawhere.activity.timing;

import static com.aawhere.activity.timing.ActivityTimingTestUtil.*;
import static com.aawhere.activity.timing.ActivityTimingRelationUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.timing.ActivityTiming.Builder;
import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.app.KnownApplication;
import com.aawhere.collections.Index;
import com.aawhere.lang.ComparatorResult;
import com.aawhere.test.TestUtil;
import com.aawhere.track.SimpleTrackpoint;
import com.aawhere.track.TrackpointTestUtil;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class ActivityTimingRelationUtilUnitTest {

	@Test
	public void testDurationComparable() {

		ActivityTiming quickest = createWithOneCompletion();
		final Completion quickestCompletion = quickest.getCompletions().get(0);
		SimpleTrackpoint longerFinish = SimpleTrackpoint.copy(quickestCompletion.getFinish())
				.setTimestamp(quickestCompletion.getFinish().getTimestamp().plus(9824982)).build();
		ActivityTiming longer = createWithOneCompletion(quickestCompletion.getStart(), longerFinish);

		ActivityTimingRelationUnitTest noCompletionTest = ActivityTimingRelationUnitTest.getInstance();
		ActivityTiming noCompletion = noCompletionTest.getEntitySample();
		noCompletion = ActivityTiming.create().setCourse(noCompletion.getCourse())
				.setAttempt(noCompletion.getAttempt()).addFailure(TimingCompletionCategory.DID_NOT_FINISH).build();
		ActivityTimingRelationDurationComparator comparator = ActivityTimingRelationDurationComparator.create().build();

		assertEquals(	"no duration to the end",
						ComparatorResult.LESS_THAN.getValue(),
						comparator.compare(quickest, noCompletion));
		assertEquals(	"no duration to the end",
						ComparatorResult.GREATER_THAN.getValue(),
						comparator.compare(noCompletion, quickest));
		assertEquals("should be the same", ComparatorResult.EQUAL.getValue(), comparator.compare(quickest, quickest));
		assertEquals(	"should be the same",
						ComparatorResult.EQUAL.getValue(),
						comparator.compare(noCompletion, noCompletion));

		// put them in the list in the wrong order
		ArrayList<ActivityTiming> list = Lists.newArrayList(longer, noCompletion, quickest);
		// sorting should go from shortest to longest and nulls after
		Collections.sort(list, comparator);
		assertSame(quickest, list.get(Index.FIRST.index));
		assertSame(longer, list.get(Index.SECOND.index));
		assertSame(noCompletion, list.get(Index.THIRD.index));

	}

	@Test
	public void testEqualsDeeply() {
		ActivityTiming first = createWithOneCompletion();
		assertTrue("these are the same", ActivityTimingRelationUtil.equalsDeeply(first, first));
		ActivityTiming second = createWithOneCompletion();
		assertNotSame("separate creations", first, second);
		TestUtil.assertNotEquals("should be different ids", first, second);
		assertFalse("these most definitely are not the same " + first + second,
					ActivityTimingRelationUtil.equalsDeeply(first, second));

		{
			final Builder builder = cloneTiming(first);
			ActivityTiming wrongAttempt = builder.setAttempt(second.getAttempt()).build();
			assertFalse("attempt is wrong", equalsDeeply(first, wrongAttempt));
		}
		{
			ActivityTiming wrongCourse = cloneTiming(first).setCourse(second.getCourse()).build();
			assertFalse("course is wrong", equalsDeeply(first, wrongCourse));
		}
		{
			ActivityTiming wrongCompletions = cloneTiming(first).setCompletions(second.getCompletions()).build();
			assertFalse("completions is wrong", equalsDeeply(first, wrongCompletions));
		}

		second = ActivityTiming.mutate(second).setCompletions(first.getCompletions()).setFailures(first.getFailures())
				.build();
		assertFalse("ids cannot be changed so this must not be equal even if everything else is",
					ActivityTimingRelationUtil.equalsDeeply(first, second));

	}

	@Test
	public void testEqualsDeeplyFailuresConsidered() {
		ActivityTiming first = ActivityTimingTestUtil.builder()
				.setFailures(Sets.newHashSet(TimingCompletionCategory.ATTEMPT_FOOTPRINT_TOO_SMALL)).build();
		// Failures is empty for created version
		HashSet<TimingCompletionCategory> invalidFailures = Sets.newHashSet(TimingCompletionCategory.COURSE_TOO_SHORT,
																			TimingCompletionCategory.DID_NOT_FINISH);
		ActivityTiming wrongFailures = ActivityTiming.create().courseId(first.courseId()).attemptId(first.attemptId())
				.setFailures(invalidFailures).build();
		assertFalse("failures is wrong", equalsDeeply(first, wrongFailures));
		assertTrue("failures are wrong, but we aren't checking", equalsDeeplyIgnoreFailures(first, wrongFailures));
		assertTrue(	"these are the same, but worth checking since set is not empty as before",
					equalsDeeply(wrongFailures, wrongFailures));
	}

	@Test
	public void testSort() {
		ActivityTiming expected = fastest();
		List<ActivityTiming> list = Lists.newArrayList(slowest(), expected, middle());
		Collections.shuffle(list);
		ActivityTiming quickest = ActivityTimingRelationUtil.quickest(list);
		assertEquals(expected, quickest);
	}

	@Test
	public void testBetter() {

		ActivityTiming fastest = fastest();
		ActivityTiming fastestWithTrueReciprocal = ActivityTiming.mutate(fastest()).reciprocal(true).build();
		ActivityTiming fastestWithFalseReciprocal = ActivityTiming.mutate(fastest()).reciprocal(false).build();
		ActivityTiming middle = middle();
		ActivityTiming slowestWithTrueReciprocal = ActivityTiming.mutate(slowest()).reciprocal(true).build();
		assertBetter("both null, speed matters", fastest, middle);
		assertBetter("both true, speed matters", fastestWithTrueReciprocal, slowestWithTrueReciprocal);
		assertBetter("slowest has reciprocal against false", slowestWithTrueReciprocal, fastestWithFalseReciprocal);
		assertBetter("slowest has reciprocal against null ", slowestWithTrueReciprocal, middle);

	}

	/**
	 * 
	 */
	@Test
	public void testReciprocalsUpdated() {

		ActivityTiming completed = ActivityTimingTestUtil.createWithOneCompletion();
		ActivityTiming reciprocalCompleted = ActivityTiming
				.create()
				.courseId(completed.attemptId())
				.attemptId(completed.courseId())
				.reciprocal(null)
				.addCompletion(	TrackpointTestUtil.generateRandomTrackpoint(),
								TrackpointTestUtil.generateRandomTrackpoint()).build();
		ActivityTiming notCompleted = ActivityTiming.create().courseId(completed.courseId())
				.attemptId(completed.attemptId()).addFailure(TimingCompletionCategory.DID_NOT_FINISH).build();

		assertReciprocalsUpdated("right isn't known so reciprocal isn't updated", completed, null, null, null);
		assertReciprocalsUpdated("both completed..reciprocals!", completed, reciprocalCompleted, true, true);
		assertReciprocalsUpdated(	"left failed so it should be updated even if right isn't known",
									notCompleted,
									null,
									null,// null because reciprocals are null if timing failed
									null);
		assertReciprocalsUpdated(	"left completed, right didn't so only left is updated",
									reciprocalCompleted,
									notCompleted,
									false,
									null);
		assertReciprocalsUpdated(	"right completed, left didn't so only right is updated",
									notCompleted,
									reciprocalCompleted,
									null,
									false);

	}

	/**
	 * Asserts that the timings are updated to the expected result given their condition of
	 * completed, if they complete each other and ignoring nulls. The expected result could be true,
	 * false or null.
	 * 
	 * @param message
	 * @param left
	 * @param right
	 * @param leftReciprocalExpected
	 *            null indicates no update, true or false is the expected reciprocal value
	 */
	private void assertReciprocalsUpdated(String message, ActivityTiming left, ActivityTiming right,
			@Nullable Boolean leftReciprocalExpected, Boolean rightReciprocalExpected) {

		Pair<ActivityTiming, ActivityTiming> reciprocalsUpdated = ActivityTimingRelationUtil.reciprocalsUpdated(left,
																												right);
		if (leftReciprocalExpected != null) {
			ActivityTiming leftResult = reciprocalsUpdated.getLeft();
			assertNotNull(message, leftResult);
			assertEquals(message, leftReciprocalExpected, leftResult.reciprocal());
		} else {
			assertNull(reciprocalsUpdated.getLeft());
		}
		if (rightReciprocalExpected != null) {
			assertNotNull(message, reciprocalsUpdated.getRight());
			assertEquals(message, rightReciprocalExpected, reciprocalsUpdated.getRight().reciprocal());
		} else {
			assertNull(reciprocalsUpdated.getRight());
		}
	}

	public static void assertBetter(String message, ActivityTiming expected, ActivityTiming other) {
		assertEquals(message, expected, better(expected, other));
		assertEquals(message, expected, better(other, expected));
	}

	@Test
	public void testTimingGroups() {
		ActivityId group1Activity1 = new ActivityId(KnownApplication.UNKNOWN.key, "1+1");
		ActivityId group1Activity2 = new ActivityId(KnownApplication.UNKNOWN.key, "1+2");
		ImmutableList<ActivityId> group1 = ImmutableList.of(group1Activity1, group1Activity2);
		// group2
		ActivityId group2Activity1 = new ActivityId(KnownApplication.UNKNOWN.key, "2+1");
		ActivityId group2Activity2 = new ActivityId(KnownApplication.UNKNOWN.key, "2+2");
		ActivityId group2Activity3 = new ActivityId(KnownApplication.UNKNOWN.key, "2+3");
		ImmutableList<ActivityId> group2 = ImmutableList.of(group2Activity1, group2Activity2, group2Activity3);
		// group 3
		ActivityId group3Activity1 = new ActivityId(KnownApplication.UNKNOWN.key, "3+1");
		// notice group1activity1 is also added to group 3 since this must handle activity ids in
		// multiple groups
		ImmutableList<ActivityId> group3 = ImmutableList.of(group3Activity1, group1Activity1);
		ImmutableList<ImmutableList<ActivityId>> allGroups = ImmutableList.of(group1, group2, group3);
		ImmutableSet<ActivityTimingId> expected = ImmutableSet.of//
				(// group1 activity 1
				/**/timingId(group1Activity1, group2Activity1), //
					timingId(group1Activity1, group2Activity2), //
					timingId(group1Activity1, group2Activity3), //
					timingId(group1Activity1, group3Activity1),//
					// group 1 activity 2
					timingId(group1Activity2, group2Activity1), //
					timingId(group1Activity2, group2Activity2), //
					timingId(group1Activity2, group2Activity3), //
					timingId(group1Activity2, group3Activity1),//
					// group 2 activity 1
					timingId(group2Activity1, group1Activity1), //
					timingId(group2Activity1, group1Activity2), //
					timingId(group2Activity1, group3Activity1),//
					// group 2 activity 2
					timingId(group2Activity2, group1Activity1), //
					timingId(group2Activity2, group1Activity2), //
					timingId(group2Activity2, group3Activity1),//
					// group 2 activity 3
					timingId(group2Activity3, group1Activity1), //
					timingId(group2Activity3, group1Activity2), //
					timingId(group2Activity3, group3Activity1),//
					// group 3 activity 1
					timingId(group3Activity1, group1Activity1), //
					timingId(group3Activity1, group1Activity2), //
					timingId(group3Activity1, group2Activity1), //
					timingId(group3Activity1, group2Activity2), //
					timingId(group3Activity1, group2Activity3), //
					// group 3, group1 activity 1 again from different group
					// these aren't that useful, but they prove the algorithm doesn't blow up
					timingId(group1Activity1, group1Activity2), //
					timingId(group1Activity2, group1Activity1) //
				);

		ImmutableSet<ActivityTimingId> actual = ActivityTimingRelationUtil.timingsIdsAcrossGroups(allGroups);
		TestUtil.assertCollectionEquals("wrong timings produced", expected, actual);
	}
}
