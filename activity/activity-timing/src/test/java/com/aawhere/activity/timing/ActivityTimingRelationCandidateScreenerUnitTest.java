/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import java.util.List;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.AttemptLineProvider;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.ScreeningResults;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleRealisticTracks;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.TrackSummary.Builder;
import com.aawhere.track.TrackSummaryTestUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.match.TrackTimingRelationCalculator;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.google.common.collect.ImmutableList;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Tests the quick screening of Activities using {@link ActivityTimingRelationCandidateScreener}.
 * 
 * many of the tests are better handled by the user story.
 * 
 * @see ActivityTimingRelationUserStoryUnitTest
 * @author aroller
 * 
 */
public class ActivityTimingRelationCandidateScreenerUnitTest {

	@Test
	public void testIdentical() {
		Activity activity = ActivityTestUtil.createActivity();
		ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
				.attempt(activity).course(activity).build();

		assertEquals(TimingCompletionCategory.IDENTICAL_TRACKS, screener.reason());
		assertFalse("should have failed", screener.passed());
	}

	/**
	 * Tests that a multi line string will be provided and verify it will fail the test due to
	 * start/end points.
	 */
	@Test
	public void testLineProvider() {
		Activity course = ActivityTestUtil.createActivity(ExampleTracks.LINE_BEFORE);
		Activity attempt = ActivityTestUtil.createActivity(ExampleTracks.LINE_BEFORE_AFTER);

		// anonymous class provider
		AttemptLineProvider provider = new ActivityTimingRelationCandidateScreener.AttemptLineProvider() {
			@Override
			public MultiLineString attemptLine(Activity attempt) {
				// purposely providing the wrong line to make sure start is not reached.
				return JtsTrackGeomUtil.trackToMultiLineString(ExampleTracks.LINE);
			}
		};
		ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
				.course(course).attempt(attempt).attemptLineProvider(provider).build();

		assertEquals(	"the line stirng provided misses the start",
						TrackTimingRelationCalculator.TimingCompletionCategory.DID_NOT_START,
						screener.reason());
		assertFalse("should have failed due to start", screener.passed());
	}

	@Test
	public void testWithoutLineProvider() {
		Activity course = ActivityTestUtil.createActivity(ExampleTracks.LINE_BEFORE);
		Activity attempt = ActivityTestUtil.createActivity(ExampleTracks.LINE_BEFORE_AFTER);
		ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
				.course(course).attempt(attempt).build();
		assertTrue("without screener it should pass", screener.passed());
	}

	@Test
	public void testCourseTooShort() {
		Activity course = ActivityTestUtil.createActivity(ExampleTracks.MICRO);
		Activity attempt = ActivityTestUtil.createActivity(ExampleTracks.LINE);
		ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
				.course(course).attempt(attempt).build();
		assertEquals("course is micro?", TimingCompletionCategory.COURSE_TOO_SHORT, screener.reason());
		assertFalse(screener.passed());
	}

	@Test
	public void testCourseFootprintTooSmall() {
		Length half = QuantityMath.create(ActivityTimingRelationUserStory.BOUNDS_LENGTH_MIN_DEFAULT).dividedBy(2)
				.getQuantity();
		// this creates a track that repeats out and back exceeding the length of minimum course
		// length, but reversing to stay within the footprint.
		List<Trackpoint> track = ExampleRealisticTracks.create().start().atLeast(half).repeat().reverse().repeatAll(10)
				.track();
		Activity smallFootprint = ActivityTestUtil.createActivity(track);
		Activity bigEnough = ActivityTestUtil.createActivity(ExampleTracks.LINE);
		assertEquals(TimingCompletionCategory.ATTEMPT_FOOTPRINT_TOO_SMALL, ActivityTimingRelationCandidateScreener
				.create().course(bigEnough).attempt(smallFootprint).build().reason());
		assertEquals(TimingCompletionCategory.COURSE_FOOTPRINT_TOO_SMALL, ActivityTimingRelationCandidateScreener
				.create().course(smallFootprint).attempt(bigEnough).build().reason());

	}

	@Test
	public void testAttemptTooShort() {
		Activity course = ActivityTestUtil.createActivity(ExampleTracks.LINE_BEFORE);
		Activity attempt = ActivityTestUtil.createActivity(ExampleTracks.LINE);
		ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
				.course(course).attempt(attempt).build();
		assertEquals("attempt is shorter than course", TimingCompletionCategory.ATTEMPT_TOO_SHORT, screener.reason());
		assertFalse(screener.passed());
	}

	/** faking a bounding box that will pass, the different geocells should catch the problem. */
	@Test
	public void testNotSimilarEnough() {
		Activity course = ActivityTestUtil.createActivity(ExampleTracks.LINE_BEFORE);
		Activity attempt = ActivityTestUtil.createActivity(ExampleTracks.LINE_DEVIATION);
		// provide a fake bounding box to get past the bounding box check and force geocell check
		Builder mutant = TrackSummaryTestUtil.mutate(attempt.getTrackSummary());
		mutant.setBoundingBox(course.boundingBox());
		ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
				.course(course).attempt(attempt).build();
		assertEquals("attempt is shorter than course", TimingCompletionCategory.NOT_SIMILAR_ENOUGH, screener.reason());
		assertFalse(screener.passed());
	}

	/** the bounding box will expose that these are not similar enough. */
	@Test
	public void testOutOfBounds() {
		Activity course = ActivityTestUtil.createActivity(ExampleTracks.LINE_BEFORE);
		Activity attempt = ActivityTestUtil.createActivity(ExampleTracks.LINE_DEVIATION);
		ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
				.course(course).attempt(attempt).build();
		assertEquals("attempt is shorter than course", TimingCompletionCategory.OUT_OF_BOUNDS, screener.reason());
		assertFalse(screener.passed());
	}

	/**
	 * Tests each component of the
	 * {@link ActivityTimingRelationCandidateScreener#attemptsScreened(Activity, Iterable, Iterable)}
	 * including passing, reciprocal and failers for both new activities that are actually screened
	 * and existing that previously were screened.
	 * 
	 */
	@Test
	public void testAttemptsScreened() {
		Activity course = ActivityTestUtil.createActivity(ExampleTracks.LINE);
		Activity failer = ActivityTestUtil.createActivity(ExampleTracks.DEVIATE_AROUND_LINE);
		Activity passer = ActivityTestUtil.createActivity(ExampleTracks.LINE_LONGER);
		Activity reciprocal = ActivityTestUtil.createActivity(ExampleTracks.LINE_TIME_SHIFT);
		ActivityTiming expectedReciprocal = ActivityTimingTestUtil.builderWithOneCompletion()
				.setCourse(ActivityReferenceIds.create().setActivity(course).build())
				.setAttempt(ActivityReferenceIds.create().setActivity(reciprocal).build()).build();
		ImmutableList<Activity> attempts = ImmutableList.of(passer, reciprocal, failer);
		ActivityTiming existingPasser = ActivityTimingTestUtil.createWithOneCompletion();
		ActivityTiming existingFailer = ActivityTimingTestUtil.failure();
		ActivityTiming existingReciprocal = ActivityTimingTestUtil.reciprocal(existingPasser);
		ImmutableList<ActivityTiming> existing = ImmutableList.of(existingPasser, existingFailer, existingReciprocal);
		ScreeningResults screeningResults = ActivityTimingRelationCandidateScreener.attemptsScreened(	course,
																										attempts,
																										existing);
		TestUtil.assertContains(new ActivityTimingId(course.id(), passer.id()), screeningResults.passers().keySet());
		TestUtil.assertContains(expectedReciprocal.id(), screeningResults.passers().keySet());
		TestUtil.assertContains(expectedReciprocal.id(), screeningResults.reciprocalCandidates());
		TestUtil.assertContainsOnly(new ActivityTimingId(course.id(), failer.id()), screeningResults.failers().values());

		ScreeningResults existingResults = screeningResults.existingResults();
		TestUtil.assertContains(existingPasser.id(), existingResults.passers().keySet());
		TestUtil.assertContains(existingReciprocal.id(), existingResults.passers().keySet());
		TestUtil.assertContainsOnly(existingReciprocal.id(), existingResults.reciprocalCandidates());
		TestUtil.assertContainsOnly(existingFailer.id(), existingResults.failers().values());
	}
}
