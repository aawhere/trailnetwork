package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;

public class ActivityTimingFilterBuilderUnitTest {

	@Test
	public void testCourseCompletedField() {
		boolean courseCompleted = true;
		Filter filter = courseCompletedFilter(courseCompleted);
		assertTrue(	"course completed must ensure attempt id is not null " + filter,
					filter.containsCondition(ActivityTimingField.Key.ATTEMPT_ID));
	}

	public Filter courseCompletedFilter(boolean courseCompleted) {
		Filter filter = ActivityTimingFilterBuilder
				.create()
				.addCondition(FilterCondition.create().field(ActivityTimingField.Key.COURSE_COMPLETED)
						.value(courseCompleted).build()).build();
		return filter;
	}

	@Test(expected = CourseNotCompletedNotIndexedException.class)
	public void testCourseNotCompleted() {
		courseCompletedFilter(false);
	}

	@Test
	public void testCourseIdIsParent() {
		ActivityId expectedParent = ActivityTestUtil.activityId();
		Filter filter = ActivityTimingFilterBuilder
				.create()
				.addCondition(FilterCondition.create().field(ActivityTimingField.Key.COURSE_ID).value(expectedParent)
						.build()).build();
		assertEquals("course Id must move to parent", expectedParent, filter.getParent());
		assertFalse("course id must be removed", filter.containsCondition(ActivityTimingField.Key.COURSE_ID));
	}
}
