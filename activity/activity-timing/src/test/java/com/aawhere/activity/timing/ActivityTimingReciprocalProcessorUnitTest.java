package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.timing.ActivityTimingRelations.Builder;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Trackpoint;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Challenges the {@link ActivityTimingReciprocalProcessor} by providing known timings, asserting
 * updates are taking place as necessary and that two timings will be produced in all situations.
 * 
 * All the tests do the setup and {@link #test()} does the work, which is invoked after the test is
 * called.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingReciprocalProcessorUnitTest {

	private ActivityTiming timingExpected;
	private ActivityTiming reciprocalExpected;
	private Iterable<Trackpoint> track1;
	private Iterable<Trackpoint> track2;
	private Boolean reciprocalRelationshipExpected = true;
	private Boolean timingKnown = true;
	private Boolean timingCompleted = true;
	private Boolean reciprocalKnown = true;
	private Boolean timingUpdateExpected = true;
	private Boolean reciprocalCompleted = true;
	private Boolean reciprocalUpdateExpected = true;

	Builder timingsBuilder = ActivityTimingRelations.create();

	@Before
	public void setUp() {
		this.track1 = ExampleTracks.LINE;
		this.track2 = ExampleTracks.LINE_TIME_SHIFT;
	}

	@After
	public void test() {

		Activity activity1 = ActivityTestUtil.createActivity(track1);
		Activity activity2 = ActivityTestUtil.createActivity(track2);
		Function<ActivityId, Activity> activityProvider = Functions.forMap(IdentifierUtils.map(ImmutableList
				.of(activity1, activity2)));

		ImmutableMap<Activity, Iterable<Trackpoint>> trackMap = ImmutableMap.<Activity, Iterable<Trackpoint>> builder()
				.put(activity1, track1).put(activity2, track2).build();

		Function<Activity, Iterable<Trackpoint>> trackProvider = Functions.forMap(trackMap);

		// build timings
		ActivityTiming.Builder timingBuilder = ActivityTiming.create();
		if (timingCompleted) {
			timingBuilder
					.addCompletion(activity2.getTrackSummary().getStart(), activity2.getTrackSummary().getFinish());
			// if the reciprocal is not known then the reciprocal relationship can't yet be known
			if (reciprocalKnown) {
				timingBuilder.reciprocal(reciprocalRelationshipExpected);
			}
		}
		this.timingExpected = timingBuilder.courseId(activity1).attemptId(activity2).build();

		if (timingKnown) {
			// order matters
			timingsBuilder.add(timingExpected);
		}

		com.aawhere.activity.timing.ActivityTiming.Builder reciprocalBuilder = ActivityTiming.create();

		if (this.reciprocalCompleted) {
			reciprocalBuilder.addCompletion(activity1.getTrackSummary().getStart(), activity1.getTrackSummary()
					.getFinish());
		}
		this.reciprocalExpected = reciprocalBuilder.courseId(activity2).attemptId(activity1)

		.reciprocal(reciprocalRelationshipExpected).build();
		if (reciprocalKnown) {
			timingsBuilder.add(reciprocalExpected);

			// the order of the timings determines which is the timing and the reciprocal
			if (!this.timingKnown) {
				ActivityTiming swapping = this.timingExpected;
				this.timingExpected = this.reciprocalExpected;
				this.reciprocalExpected = swapping;
			}
		}

		ActivityTimingReciprocalProcessor processor = ActivityTimingReciprocalProcessor
				.create(this.timingExpected.id()).activityProvider(activityProvider).timings(timingsBuilder.build())
				.attempLineProvider(null).trackProvider(trackProvider).build();
		ActivityTiming actualTiming = processor.timing();
		ActivityTiming actualReciprocal = processor.reciprocal();
		if (actualTiming != null) {
			if (actualTiming.courseCompleted()) {
				assertNotNull("reciprocal must be processed if timing passed", actualReciprocal);
				assertEquals("reciprocal", reciprocalExpected, processor.reciprocal());
				assertEquals("reicprocal  completed", this.reciprocalCompleted, processor.reciprocal()
						.courseCompleted());
				assertEquals("timing is reciprocal", reciprocalRelationshipExpected, processor.timing().reciprocal());
				if (reciprocalCompleted) {
					assertEquals("reicprocal is reciprocal", reciprocalRelationshipExpected, processor.reciprocal()
							.reciprocal());
				} else {
					assertNull("failing timings should report null for reciprocal", processor.reciprocal().reciprocal());
				}
			} else {
				assertNull("reciprocal shouldn't be processed if timing failed", actualReciprocal);
			}
		} else {
			assertFalse("reciprocal must have failed screening if timing is null", actualReciprocal.courseCompleted());
		}
		assertEquals("timing updated", this.timingUpdateExpected, processor.timingUpdated());
		assertEquals("reicprocal updated", this.reciprocalUpdateExpected, processor.reciprocalUpdated());

		if (false) {
			// timing testing
			{
				assertEquals("timing", timingExpected, processor.timing());
				if (processor.timing().courseCompleted()) {
					assertEquals("timing is reciprocal", reciprocalRelationshipExpected, processor.timing()
							.reciprocal());
				}
				assertEquals("timing completed", timingExpected.courseCompleted(), processor.timing().courseCompleted());

			}
			// reciprocal testing
			{
				if (timingExpected.courseCompleted()) {
					if (reciprocalKnown) {

						if (processor.reciprocal().courseCompleted()) {

						}
					} else {
						assertNull("timing failed so reciprocal shouldn't be processed", processor.reciprocal());
					}
				}

			}
		}

	}

	@Test
	public void testNoTimingsKnownFirstFailsScreening() {
		this.track1 = ExampleTracks.LINE_LONGER;
		this.timingCompleted = false;
		this.reciprocalRelationshipExpected = false;
		this.timingKnown = false;
		this.reciprocalKnown = false;
		// reciprocal shouldn't even be processed
		this.reciprocalUpdateExpected = false;
	}

	@Test
	public void testNoTimingsKnownSecondFailsScreening() {
		this.track2 = ExampleTracks.LINE_LONGER;
		this.reciprocalCompleted = false;
		this.reciprocalRelationshipExpected = false;
		this.timingKnown = false;
		this.reciprocalKnown = false;

		// Since the timing passed, but the reciprocal failed the timing shouldn't be deep
		// processed, hence nothing to store
		this.timingUpdateExpected = false;

	}

	/**
	 * TODO Figure out how to pass screening, but fail deeply.
	 */
	public void testNoTimingsKnownFirstFailsDeep() {

	}

	/**
	 * TODO Figure out how to pass screening, but fail deeply.
	 */
	public void testNoTimingsKnownSecondFailsDeep() {

	}

	@Test
	public void testOneTimingKnownCameFailedReciprocalCompletes() {
		// the known is a failure so activity1 doesn't complete activity2
		this.track1 = ExampleTracks.LINE_LONGER;
		this.timingCompleted = false;
		// update not expected because it is not completed so no need to mark non-reciprocal
		this.timingUpdateExpected = false;
		this.reciprocalRelationshipExpected = false;
		this.reciprocalKnown = false;
		// the timing didn't complete so the reciprocal shouldn't have been deep processed and
		// nothing to store
		this.reciprocalUpdateExpected = false;
	}

	@Test
	@Ignore("find a track that will pass screening, but fail deep")
	public void testOneTimingKnownAttempFailsDeep() {

		this.reciprocalKnown = false;
		this.reciprocalRelationshipExpected = false;
		this.reciprocalCompleted = false;
	}

	@Test
	public void testOneCompletedTimingKnownAttemptFailsScreening() {
		this.track2 = ExampleTracks.LINE_DEVIATION;
		this.reciprocalKnown = false;
		this.reciprocalRelationshipExpected = false;
		this.reciprocalCompleted = false;
	}

	@Test
	public void testReciprocalTimingsNeitherKnown() {
		this.timingKnown = false;
		this.reciprocalKnown = false;
	}

	/**
	 * When the timingId provided is different than the only timing known...the results are swapped
	 * 
	 */
	@Test
	public void testReciprocalTimingsReciprocalKnown() {
		// the reciprocal is swapped and put in place of the timing.
		this.timingKnown = false;
		this.timingUpdateExpected = false;
	}

	/**
	 * same as {@link #testTwoReciprocalTimingsKnown()}, but one is not known
	 */
	@Test
	public void testReciprocalTimingsTimingKnown() {
		this.reciprocalKnown = false;
	}

	@Test
	public void testTwoReciprocalTimingsKnown() {
		// both are known so nothing should change.
		this.timingUpdateExpected = false;
		this.reciprocalUpdateExpected = false;
	}
}
