/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.timing.ActivityTiming.Builder;
import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.lang.string.FromStringUnitTest;
import com.aawhere.lang.string.StringFormatException;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.track.SimpleTrackpoint;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.TrackpointTestUtil;

/**
 * Test for {@link ActivityTiming}.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationUnitTest
		extends
		BaseEntityJaxbBaseUnitTest<ActivityTiming, ActivityTiming.Builder, ActivityTimingId, ActivityTimingRelationRepository> {

	private ActivityReferenceIds course;
	private ActivityReferenceIds attempt;
	private Trackpoint start;
	private Trackpoint finish;
	private Trackpoint start2;
	private Trackpoint finish2;
	private Boolean reciprocal;

	private ActivityTimingId id;

	/**
	 * 
	 */
	public ActivityTimingRelationUnitTest() {
		super(NON_MUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		this.course = ActivityTestUtil.createActivityReferenceIds();
		this.attempt = ActivityTestUtil.createActivityReferenceIds();
		this.id = new ActivityTimingId(this.course.getActivityId(), this.attempt.getActivityId());
		List<Trackpoint> trackpoints = TrackpointTestUtil.generateOrderedTrackpoints();
		this.start = trackpoints.get(0);
		this.finish = trackpoints.get(1);
		List<Trackpoint> trackpoints2 = TrackpointTestUtil.generateOrderedTrackpoints();
		this.start2 = trackpoints2.get(0);
		this.finish2 = trackpoints2.get(1);
		this.reciprocal = true;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.setCourse(course);
		builder.setAttempt(attempt);
		builder.addCompletion(start, finish);
		builder.addCompletion(start2, finish2);
		builder.reciprocal(reciprocal);
		// no need to populate id...self populating.
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertCreation(ActivityTiming sample) {
		super.assertCreation(sample);
		assertEquals(this.course, sample.getCourse());
		assertEquals(this.attempt, sample.getAttempt());
		List<Completion> completions = sample.getCompletions();
		assertEquals("completions size", 2, completions.size());
		Completion completion = completions.get(0);
		TrackpointTestUtil.assertTrackpointEquals(this.start, completion.getStart());
		TrackpointTestUtil.assertTrackpointEquals(this.finish, completion.getFinish());
		Completion completion2 = completions.get(1);
		TrackpointTestUtil.assertTrackpointEquals(this.start2, completion2.getStart());
		TrackpointTestUtil.assertTrackpointEquals(this.finish2, completion2.getFinish());
		assertNotNull(completion.getDuration());
		assertTrue("duration isn't positive", completion.getDuration().getMillis() > 0);
		assertEquals(this.id, sample.getId());
		assertTrue("course completed", sample.isCourseCompleted());
		assertEquals("reciprocal", this.reciprocal, sample.reciprocal());
	}

	/**
	 * @return
	 * 
	 */
	public static ActivityTimingRelationUnitTest getInstance() {
		ActivityTimingRelationUnitTest activityTimingRelationUnitTest = new ActivityTimingRelationUnitTest();
		activityTimingRelationUnitTest.setUp();
		activityTimingRelationUnitTest.baseSetUp();
		return activityTimingRelationUnitTest;

	}

	/**
	 * @param build
	 */
	public void setFinish(SimpleTrackpoint finish) {
		this.finish = finish;
	}

	/**
	 * @param start3
	 */
	public void setStart(Trackpoint start) {
		this.start = start;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return ActivityTimingTestUtil.adapters();
	}

	/**
	 * /* (non-Javadoc)
	 * 
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertEntityEquals(com.aawhere.persist.BaseEntity,
	 *      com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertEntityEquals(ActivityTiming expected, ActivityTiming actual) {
		super.assertEntityEquals(expected, actual);
		assertEquals(expected.getCompletions(), actual.getCompletions());
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected ActivityTimingRelationRepository createRepository() {
		return new ActivityTimingRelationMockRepository();
	}

	public static class ActivityTimingRelationMockRepository
			extends MockFilterRepository<ActivityTimingRelations, ActivityTiming, ActivityTimingId>
			implements ActivityTimingRelationRepository {

	}

	@Test
	public void testParseCompositeIdValue() throws StringFormatException, NoSuchMethodException {
		ActivityTimingId timingId = ActivityTimingTestUtil.timingId();
		ActivityTimingId rebuilt = FromStringUnitTest.assertFromString(timingId);
		assertEquals(timingId.attemptId(), rebuilt.attemptId());
		assertEquals(timingId.courseId(), rebuilt.courseId());

	}
}
