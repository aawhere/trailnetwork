/**
 * 
 */
package com.aawhere.activity.timing;

import static com.aawhere.track.ExampleTracks.*;

import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.timing.ActivityTiming.Builder;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.TrackpointTestUtil;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.google.appengine.repackaged.com.google.common.collect.ImmutableSet;

/**
 * Useful functions for producing testing data related to {@link ActivityTiming}.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingTestUtil {

	static public ActivityTiming createWithOneCompletion(Trackpoint start, Trackpoint finish) {
		Builder builder = builder(start, finish);

		return builder.build();
	}

	public static Builder builder(Trackpoint start, Trackpoint finish) {
		Builder builder = builder();
		builder.addCompletion(start, finish);
		return builder;
	}

	/**
	 * A good start this provides course and attempt so you don't have to.
	 * 
	 * @return
	 */
	public static Builder builder() {
		ActivityReferenceIds course = ActivityTestUtil.createActivityReferenceIds();
		ActivityReferenceIds attempt = ActivityTestUtil.createActivityReferenceIds();
		Builder builder = ActivityTiming.create();
		builder.setCourse(course).setAttempt(attempt);
		return builder;
	}

	static public ActivityTiming createWithOneCompletion() {

		return builderWithOneCompletion().build();
	}

	/**
	 * Provides a random failure with a completion of random course/attempt
	 * 
	 * @return
	 */
	public static ActivityTiming failure() {
		return builder().addFailure(TestUtil.generateRandomEnum(TimingCompletionCategory.class)).build();
	}

	/**
	 * Focused on time this will provide the fastest timing completion when compared to
	 * {@link #middle()} and {@link #slowest()}.
	 * 
	 * @return
	 */
	public static ActivityTiming fastest() {
		return createWithOneCompletion(P1, P2);
	}

	public static ActivityTiming middle() {
		return createWithOneCompletion(P1, P3);
	}

	public static ActivityTiming slowest() {
		return createWithOneCompletion(P1, P4);
	}

	public static Builder builderWithOneCompletion() {

		List<Trackpoint> trackpoints = TrackpointTestUtil.generateOrderedTrackpoints();
		Trackpoint startPoint = trackpoints.get(0);
		Trackpoint finishPoint = trackpoints.get(1);
		return builder(startPoint, finishPoint);
	}

	/**
	 * Copies properties from the given to the builder returned. This could go into main code if
	 * cloning is needed outside of testing.
	 * 
	 * @param first
	 * @return
	 */
	public static Builder cloneTiming(ActivityTiming first) {
		return ActivityTiming.create().setCourse(first.getCourse()).setAttempt(first.getAttempt()).setOther(first);
	}

	public static Set<XmlAdapter<?, ?>> adapters() {

		final Set<XmlAdapter<?, ?>> adapters = PersonTestUtil.adapters();
		adapters.addAll(ActivityTestUtil.adapters());
		return adapters;
	}

	/**
	 * Produces a reciprocal of the timing given copying most everything, swapping the attempt and
	 * course and ensuring reciprocal is true for the result.
	 * 
	 * @param unrelatedAtFirst
	 * @return
	 */
	public static ActivityTiming reciprocal(ActivityTiming given) {
		return ActivityTiming.create().setAttempt(given.course()).setCourse(given.attempt())
				.setCompletions(given.completions()).reciprocal(true).build();
	}

	/**
	 * If you just want persistence and don't care what repository it uses.
	 * 
	 * @param timing
	 * @return
	 */
	public static ActivityTiming persisted(ActivityTiming timing) {
		return persisted(timing, new ActivityTimingRelationUnitTest.ActivityTimingRelationMockRepository());
	}

	/**
	 * It will persist it into the repository given.
	 * 
	 * @param timing
	 * @param repository
	 * @return
	 */
	public static ActivityTiming persisted(ActivityTiming timing, ActivityTimingRelationRepository repository) {
		return repository.store(timing);

	}

	/**
	 * Provides a timing with the reciprocal ID, but the timing is a failure.
	 * 
	 * @param failure
	 * @return
	 */
	public static ActivityTiming reciprocalFailure(ActivityTiming given) {
		return ActivityTiming.create().setAttempt(given.course()).setCourse(given.attempt())
				.setFailures(ImmutableSet.of(TimingCompletionCategory.DID_NOT_FINISH)).build();
	}

	/**
	 * @return
	 */
	public static ActivityTimingId timingId() {
		return new ActivityTimingId(ActivityTestUtil.activityId(), ActivityTestUtil.activityId());

	}
}
