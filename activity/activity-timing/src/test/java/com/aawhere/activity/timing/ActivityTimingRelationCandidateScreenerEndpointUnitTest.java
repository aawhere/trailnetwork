/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import org.junit.Before;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.spatial.ActivitySpatialRelationEndpointVerifier;
import com.aawhere.activity.spatial.ActivitySpatialRelationEndpointVerifierUnitTest;

/**
 * @author aroller
 * 
 */
public class ActivityTimingRelationCandidateScreenerEndpointUnitTest
		extends ActivitySpatialRelationEndpointVerifierUnitTest {

	private Activity attempt;

	@Before
	public void setUp() {
		super.setUp();
		this.attempt = ActivityTestUtil.createActivity(ATTEMPT_TRACK);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.activity.spatial.ActivitySpatialRelationEndpointVerifierUnitTest#assertEndpoints
	 * (com.aawhere.activity.Activity, java.lang.Boolean, java.lang.Boolean)
	 */
	@Override
	public ActivitySpatialRelationEndpointVerifier assertEndpoints(Activity activity, Boolean startExpected,
			Boolean endExpected) {
		ActivitySpatialRelationEndpointVerifier verifier = super.assertEndpoints(activity, startExpected, endExpected);
		ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
				.course(activity).attemptLine(getMultiLineString()).attempt(attempt).build();
		assertEquals(String.valueOf(screener.reason()), verifier.passed(), screener.passed());
		return verifier;
	}
}
