/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.Track;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackpointTestUtil;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.aawhere.track.match.TrackTimingRelationCalculatorUnitTest;
import com.aawhere.track.story.BaseTrackUserStoryUnitTest;
import com.aawhere.track.story.TrackUserStories.Builder;

/**
 * This is fairly simple and just makes sure the story works. The
 * {@link TrackTimingRelationCalculatorUnitTest} verifies most situations. Extensions of this tests
 * make sure pre-checks are working properly.
 * 
 * @see ActivityTimingRelationAttemptContainsUserStoryUnitTest
 * @see ActivityTimingRelationAttemptWithinUserStoryUnitTest
 * @see ActivityTimingRelationCourseTooShortUserStoryUnitTest
 * @see ActivityTimingRelationIdenticalActivitiesUserStoryUnitTest
 * 
 * 
 * @see TrackTimingRelationCalculatorUnitTest
 * @author aroller
 * 
 */
abstract public class ActivityTimingRelationUserStoryUnitTest
		extends BaseTrackUserStoryUnitTest<ActivityTimingRelationUserStory, ActivityTiming> {

	private ActivityReferenceIds courseIds;
	private ActivityReferenceIds attemptIds;
	private Track courseTrack;
	protected ActivityTimingRelationUserStory story;
	private Activity course;
	private Activity attempt;
	private ActivityTimingRelationCandidateScreener screener;

	/**
	 * 
	 */
	public ActivityTimingRelationUserStoryUnitTest() {
		this(ExampleTracks.LINE, ExampleTracks.LINE_LONGER);
	}

	/** Rare case when an attempt is same as course. */
	protected ActivityTimingRelationUserStoryUnitTest(Track courseTrack) {
		this(courseTrack, courseTrack);
	}

	protected ActivityTimingRelationUserStoryUnitTest(Track courseTrack, Track attemptTrack) {
		super(courseTrack);
		this.courseTrack = courseTrack;
		this.course = ActivityTestUtil.createActivity(courseTrack);
		this.courseIds = new ActivityReferenceIds.Builder().setActivity(course).build();
		this.attempt = ActivityTestUtil.createActivity(attemptTrack);
		this.attemptIds = new ActivityReferenceIds.Builder().setActivity(attempt).build();
		this.screener = ActivityTimingRelationCandidateScreener.create().attempt(attempt).course(course)
				.attemptLine(JtsTrackGeomUtil.trackToMultiLineString(attemptTrack)).build();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.story.BaseTrackUserStoryUnitTest#addUserStories(com.aawhere.track.story
	 * .TrackUserStories.Builder)
	 */
	@Override
	protected void addUserStories(Builder builder) {
		ActivityTimingRelationUserStory.Builder storyBuilder = createUserStoryBuilder()
				.setCourseTrack(this.courseTrack).screener(screener);
		this.story = builder.setUserStory(storyBuilder);
	}

	/**
	 * @return
	 */
	protected com.aawhere.activity.timing.ActivityTimingRelationUserStory.Builder createUserStoryBuilder() {
		return ActivityTimingRelationUserStory.create();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.story.BaseTrackUserStoryUnitTest#assertCreated()
	 */
	@Override
	protected void assertCreated() {
		// override if you don't want the completion
		assertCompletion();
	}

	/**
	 * 
	 */
	private void assertCompletion() {
		ActivityTiming timingRelation = this.story.getResult();
		assertEquals(this.courseIds, timingRelation.getCourse());
		assertEquals(this.attemptIds, timingRelation.getAttempt());
		assertTrue(timingRelation.getFailures().toString(), timingRelation.isCourseCompleted());
		TestUtil.assertSize(1, timingRelation.getCompletions());
		Completion completion = timingRelation.getCompletions().get(0);
		TrackSummary summary = course.getTrackSummary();
		TrackpointTestUtil.assertTrackpointEquals(summary.getStart(), completion.getStart());
		TrackpointTestUtil.assertTrackpointEquals(summary.getFinish(), completion.getFinish());
		assertTrue("all completions should have passed screening", story.screenSucceeded());
	}

	protected void assertFailure(final TimingCompletionCategory expectedFailure) {
		assertFailure(expectedFailure, false);
	}

	/**
	 * @param expectedFailure
	 */
	protected void assertFailure(final TimingCompletionCategory expectedFailure, Boolean caughtByScreening) {
		assertTrue("all have result", this.story.hasResult());
		ActivityTiming result = this.story.getResult();
		assertEquals("screening caught it", !caughtByScreening, this.story.screenSucceeded());
		TestUtil.assertEmpty(result.getCompletions());
		TestUtil.assertNotEmpty(result.getFailures());
		TestUtil.assertContains("failure expected", expectedFailure, result.getFailures());
	}
}
