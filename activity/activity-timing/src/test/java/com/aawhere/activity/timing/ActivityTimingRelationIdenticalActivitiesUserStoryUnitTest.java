/**
 * 
 */
package com.aawhere.activity.timing;

import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;

/**
 * TN-277 Identical Activities shouldn't produce Timing completions
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationIdenticalActivitiesUserStoryUnitTest
		extends ActivityTimingRelationUserStoryUnitTest {

	/**
	 * 
	 */
	public ActivityTimingRelationIdenticalActivitiesUserStoryUnitTest() {
		super(ExampleTracks.LINE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.relation.ActivityTimingRelationUserStoryUnitTest#assertCreated()
	 */
	@Override
	protected void assertCreated() {
		TestUtil.assertContains("identical activities shouldn't race", TimingCompletionCategory.IDENTICAL_TRACKS, story
				.getResult().getFailures());
		TestUtil.assertEmpty(story.getResult().getCompletions());
	}

}
