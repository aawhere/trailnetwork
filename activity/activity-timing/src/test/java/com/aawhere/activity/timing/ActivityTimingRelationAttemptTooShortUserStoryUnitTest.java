/**
 * 
 */
package com.aawhere.activity.timing;

import static com.aawhere.track.ExampleTracks.*;

import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;

/**
 * TN-310 says attempts must be longer than the course.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationAttemptTooShortUserStoryUnitTest
		extends ActivityTimingRelationUserStoryUnitTest {

	/**
	 * 
	 */
	public ActivityTimingRelationAttemptTooShortUserStoryUnitTest() {
		super(LINE_LONGER, LINE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.relation.ActivityTimingRelationUserStoryUnitTest#assertCreated()
	 */
	@Override
	protected void assertCreated() {
		final TimingCompletionCategory expectedFailure = TimingCompletionCategory.ATTEMPT_TOO_SHORT;
		assertFailure(expectedFailure, true);
	}

}
