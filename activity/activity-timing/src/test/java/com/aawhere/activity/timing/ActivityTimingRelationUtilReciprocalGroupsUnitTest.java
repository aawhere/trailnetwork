/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.test.TestUtil;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSetMultimap;

/**
 * @see ActivityTimingRelationUtil#reciprocalGroups(Iterable)
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationUtilReciprocalGroupsUnitTest {

	private ActivityTiming course1Timing1;
	private ActivityTiming course1Reciprocal1;
	private ReciprocalGroupId group1Id;
	private ImmutableList.Builder<ActivityTiming> timingsBuilder = ImmutableList.builder();
	private ImmutableSetMultimap.Builder<ReciprocalGroupId, ActivityTiming> expectedGroupsBuilder = ImmutableSetMultimap
			.builder();
	private ActivityTiming course1Timing2;
	private ActivityTiming course1Reciprocal2;

	@Before
	public void setUp() {
		this.course1Timing1 = ActivityTimingTestUtil.builderWithOneCompletion().reciprocal(true).build();
		this.course1Reciprocal1 = ActivityTiming.create().setCourse(course1Timing1.attempt())
				.setAttempt(course1Timing1.course()).setCompletions(course1Timing1.completions()).reciprocal(true)
				.build();

		this.course1Timing2 = ActivityTiming.create().setCourse(course1Timing1.course())
				.setAttempt(ActivityTestUtil.createActivityReferenceIds()).reciprocal(true)
				.setCompletions(course1Timing1.completions()).build();
		this.course1Reciprocal2 = ActivityTiming.create().setCourse(course1Timing2.attempt())
				.setAttempt(course1Timing2.course()).setCompletions(course1Timing1.completions()).reciprocal(true)
				.build();
		this.group1Id = new ReciprocalGroupId(course1Timing1.id().getCourseId());
	}

	@After
	public void test() {
		final ImmutableList<ActivityTiming> timings = timingsBuilder.build();
		ImmutableSetMultimap<ReciprocalGroupId, ActivityTiming> reciprocalGroups = ActivityTimingRelationUtil
				.reciprocalGroups(timings);
		TestUtil.assertSize(timings.size(), reciprocalGroups.entries());
		final ImmutableSetMultimap<ReciprocalGroupId, ActivityTiming> expectedGroups = expectedGroupsBuilder.build();
		assertEquals(	expectedGroups.keySet().size() + " groups expected, but found "
								+ reciprocalGroups.keySet().size(),
						expectedGroups,
						reciprocalGroups);

	}

	private void add(ReciprocalGroupId group, ActivityTiming timing) {
		this.timingsBuilder.add(timing);
		this.expectedGroupsBuilder.put(group, timing);
	}

	@Test
	public void testNothing() {
		// the default is setup to expect nothing
	}

	@Test
	public void testSingleGroup() {
		group1();
	}

	/**
	 * 
	 */
	public void group1() {
		add(this.group1Id, course1Timing1);
		add(this.group1Id, course1Reciprocal1);
	}

	@Test
	public void testSingleGroupAnotherPair() {
		group1();
		add(this.group1Id, course1Timing2);
		add(this.group1Id, course1Reciprocal2);
	}

	@Test
	public void testTwoGroups() {
		group1();
		final ActivityTiming timing = ActivityTimingTestUtil.builderWithOneCompletion().reciprocal(true).build();
		add(new ReciprocalGroupId(timing.id().getCourseId()), timing);
	}

	/**
	 * a-b is assigned to group1 c-d is assigned to group2 c-a arrives without a-c and we find a
	 * situation where two groups can be assigned
	 */
	@Test
	public void testReciprocalsAssignedToTwoGroups() {
		group1();
		final ActivityTiming unrelatedAtFirst = ActivityTimingTestUtil.builderWithOneCompletion().reciprocal(true)
				.build();
		ActivityTiming unrelatedAtFirstReciprocal = ActivityTimingTestUtil.reciprocal(unrelatedAtFirst);
		add(group1Id, unrelatedAtFirst);
		add(group1Id, unrelatedAtFirstReciprocal);

		// now add the recirpocals that will tie the two groups together

		final ActivityReferenceIds a = course1Reciprocal1.course();
		final ActivityReferenceIds c = unrelatedAtFirst.course();
		ActivityTiming relator = ActivityTimingTestUtil.builderWithOneCompletion().setCourse(a).setAttempt(c)
				.reciprocal(true).build();
		ActivityTiming relatorReciprocal = ActivityTimingTestUtil.builderWithOneCompletion().setCourse(c).setAttempt(a)
				.reciprocal(true).build();
		add(group1Id, relatorReciprocal);
		// incomplete data can happen causing
		// add(group1Id, relator);

	}

	/**
	 * When reciprocals of a common timing become reciprocals A <-> B & B <-> C then A <~> C
	 */
	@Test
	public void testTransitive() {
		group1();
		ActivityTiming relatedToSecond = ActivityTimingTestUtil.builderWithOneCompletion()
				.setCourse(course1Timing1.attempt()).setAttempt(ActivityTestUtil.createActivityReferenceIds())
				.reciprocal(true).build();
		add(this.group1Id, relatedToSecond);
	}
}
