/**
 * 
 */
package com.aawhere.activity.timing;

import com.aawhere.track.ExampleTracks;
import com.aawhere.track.SimpleTrack;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;

/**
 * This tests that spatial buffers must be similar or it will be rejected.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationAttemptWithinUserStoryUnitTest
		extends ActivityTimingRelationUserStoryUnitTest {

	/**
	 * 
	 */
	public ActivityTimingRelationAttemptWithinUserStoryUnitTest() {
		// line can't start at the same place, must be shorter than deviant.
		super(new SimpleTrack.TrackpointCollectionBuilder().add(ExampleTracks.P_MINUS_1).add(ExampleTracks.P1)
				.add(ExampleTracks.P2).getTrack(), ExampleTracks.LINE_DEVIATION);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.timing.ActivityTimingRelationUserStoryUnitTest#assertCreated()
	 */
	@Override
	protected void assertCreated() {
		assertFailure(TimingCompletionCategory.OUT_OF_BOUNDS, true);
	}

}
