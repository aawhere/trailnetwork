/**
 * 
 */
package com.aawhere.activity;

/**
 * Any entity able to be identified by the {@link ActivityId}.
 * 
 * @author brian
 * 
 */
public interface ActivityIdentifiable {

	ActivityId getActivityId();
}
