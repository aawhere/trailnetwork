/**
 * 
 */
package com.aawhere.activity.relation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.Activity;
import com.aawhere.collections.ArrayUtilsExtended;
import com.aawhere.xml.XmlNamespace;

/**
 * Keeps track of the relationships that have been created for the {@link Activity}.
 * 
 * @deprecated use ActivityScopedMilestone and accompanied version
 * 
 * @author aroller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public enum RelationProcessorVersion {
	/**
	 * the cryptic underscore represents unprocessed. Single characters will save tons of room
	 * during datastore.
	 */
	_,
	/** The first version of the processing. */
	A;

	/**
	 * Indicates if this Version is not the {@link #latest()}, hence consider to be stale.
	 * 
	 * @see #latest()
	 * 
	 * @return
	 */
	public Boolean isStale() {

		return !this.equals(RelationProcessorVersion.latest());
	}

	public static RelationProcessorVersion latest() {
		return ArrayUtilsExtended.last(RelationProcessorVersion.values());
	}

	/**
	 * @return
	 */
	public Boolean isUnprocessed() {

		return this.equals(_);
	}

	public static RelationProcessorVersion unprocessed() {
		return _;
	}
}
