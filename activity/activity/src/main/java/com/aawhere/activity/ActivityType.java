/**
 *
 */
package com.aawhere.activity;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.StringUtils;

import com.aawhere.lang.If;
import com.aawhere.lang.enums.EnumWithDefault;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.Messages;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.util.rb.StatusMessage;
import com.aawhere.util.rb.StatusMessages;
import com.aawhere.util.rb.VerbTense;
import com.aawhere.xml.XmlNamespace;
import com.wordnik.swagger.annotations.ApiModel;

/**
 * Activity types as represented by TrailNetwork.
 * 
 * Currently does not support nested activity types (e.g. Mountain Biking as a subset of Cycling).
 * 
 * The names are purposely small to reduce storage space and distribution for these values that
 * appear in every activity. The display names will satisfy anyone that is not a developer.
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@ApiModel(description = ActivityMessage.Type.Doc.FULL_DESCRIPTION)
public enum ActivityType implements Message, EnumWithDefault<ActivityType> {

	/* NOTE: Sharing as strings does not handle underscores so keep these short. see #fromString */
	/* */
	BIKE("Cycling", TransportMode.BIKE, NounPhrase.BIKE, Verb.BIKE),
	/* */
	ROAD("Road Biking", TransportMode.BIKE, NounPhrase.ROAD, Verb.ROAD),
	/* */
	MTB("Mountain Biking", TransportMode.BIKE, NounPhrase.MTB, Verb.MTB),
	/* */
	RUN("Running", TransportMode.FOOT, NounPhrase.RUN, Verb.RUN),
	/** Keep it isolated to actual Hiking. Walking is separate. */
	HIKE("Hiking", TransportMode.FOOT, NounPhrase.HIKE, Verb.HIKE),
	/** All foot transportation that isn't hiking. */
	WALK("Walking", TransportMode.FOOT, NounPhrase.WALK, Verb.WALK),
	/** When the type can't be known. */
	U("Unknown", null, NounPhrase.U, Verb.U),
	/** When none of the others fit. */
	OTHER("Other", null, NounPhrase.OTHER, Verb.OTHER),
	/** Any of they various types of skiing is fine. */
	SKI("Skiing", null, NounPhrase.SKI, Verb.SKI),
	/** Inline or ice is fine. */
	SKATE("Skating", null, NounPhrase.SKATE, Verb.SKATE),
	/** Pool, lake, ocean, etc. */
	SWIM("Swimming", null, NounPhrase.SWIM, Verb.SWIM);

	public final String message;
	@Nullable
	public final TransportMode transportMode;
	public final NounPhrase nounAdjectivePhrase;
	public final Verb verb;

	private ActivityType(String message, @Nullable TransportMode transportMode, NounPhrase adjectivePhrase, Verb verb) {
		this.message = message;
		this.transportMode = transportMode;
		this.nounAdjectivePhrase = adjectivePhrase;
		this.verb = verb;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return EMPTY_PARAM_KEYS;
	}

	/**
	 * Provides a more forgiving match for using the name as a lookup.
	 * 
	 * @see #valueOf(String)
	 * @param value
	 * @return
	 */
	public static ActivityType fromString(String value) {
		try {
			return valueOf(StringUtils.upperCase(value));
		} catch (Exception e) {
			return U;
		}
	}

	/**
	 * the reciprocal of {@link #fromString(String)} ability to use lower case.
	 * 
	 * @return
	 */
	public String toLowerCase() {
		return name().toLowerCase();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.enums.EnumWithDefault#getDefault()
	 */
	@Override
	public ActivityType getDefault() {
		return U;
	}

	/**
	 * The action of the activity type in it's {@link VerbTense}. These are simple verbs.
	 * 
	 * http://www.perfect-english-grammar.com/past-simple.html
	 * 
	 * 
	 * @author aroller
	 * 
	 */
	private static enum Verb implements Message {
		/** Bike has all tenses for example even though only past (other) is used currently. */
		BIKE("{TENSE_KEY,select, "//
				+ "present{is biking}"// not used, but presents a possibility
				+ "future{will bike}" //
				+ "other{biked} " // past ...most direct form although turse
				+ "}"), //
		ROAD("{TENSE_KEY,select, "//
				+ "other{cycled} " + "}"), //
		MTB("{TENSE_KEY,select, "//
				+ "other{mountain biked} " + "}"), //
		RUN("{TENSE_KEY,select, "//
				+ "other{ran} " + "}"), //
		HIKE("{TENSE_KEY,select, "//
				+ "other{hiked} " + "}"), //
		WALK("{TENSE_KEY,select, "//
				+ "other{walked} " + "}"), //
		U("{TENSE_KEY,select, "//
				+ "other{completed} " + "}"), //
		OTHER("{TENSE_KEY,select, "//
				+ "other{completed} " + "}"), //
		SKI("{TENSE_KEY,select, "//
				+ "other{skiied} " + "}"), //
		SKATE("{TENSE_KEY,select, "//
				+ "other{skated} " + "}"), //
		SWIM("{TENSE_KEY,select, "//
				+ "other{swam} " + "}"), //
		;

		public final String value;

		private Verb(String value) {
			this.value = value;
		}

		@Override
		public String getValue() {
			return this.value;
		}

		@Override
		public ParamKey[] getParameterKeys() {
			return VerbTense.paramKeys();
		}

	}

	/**
	 * {@link ActivityType} provides the name for each type as a titlized verb, capitalized. When
	 * using the {@link ActivityType} in a sentence, activity type is used as a noun directly or as
	 * an adjective describing a generalized noun (mountain bike ride) where mountain bike is the
	 * adjective describing the noun ride.
	 * 
	 * 
	 * This is intended to be used in the middle of a sentence so all are lowercase (for nouns in
	 * english).
	 * 
	 * http://www.chompchomp.com/terms/nounphrase.htm
	 * 
	 * @author brian
	 * 
	 */
	private static enum NounPhrase implements Message {
		BIKE("{COUNT_VALUE,plural, one{bike ride} other{bike rides}}"),
		/* */
		ROAD(BIKE.getValue()),
		/* */
		MTB("{COUNT_VALUE,plural, one{mountain bike ride} other{mountain bike rides}}"),
		/* */
		RUN("{COUNT_VALUE,plural, one{run} other{runs}}"),
		/** */
		HIKE("{COUNT_VALUE,plural, one{hike} other{hikes}}"),
		/** */
		WALK("{COUNT_VALUE,plural, one{walk} other{walks}}"),
		/** */
		U("{COUNT_VALUE,plural, one{activity} other{activities}}"),
		/***/
		OTHER(U.getValue()),
		/** */
		SKI("{COUNT_VALUE,plural, one{ski session} other{ski sessions}}"),
		/** */
		SKATE("{COUNT_VALUE,plural, one{skate} other{skates}}"),
		/** */
		SWIM("{COUNT_VALUE,plural, one{swim} other{swims}}");

		private final String message;
		private static ParamKey[] paramKeys = { ActivityType.Param.COUNT };

		private NounPhrase(String message) {
			this.message = message;
		}

		@Override
		public String getValue() {
			return message;
		}

		@Override
		public ParamKey[] getParameterKeys() {
			return paramKeys;
		}
	}

	public enum Param implements ParamKey {
		/** A number indicating how many */
		COUNT,
		/**
		 * Describes the {@link ActivityType} in a sentence. It may be used with
		 * {@link ActivityType#nounAdjectivePhrase} or {@link ActivityType#verb} all the same which
		 * are unlikely to show up in the same sentence together.
		 */
		ACTIVITY;

		@Override
		public CustomFormat<?> getFormat() {
			return null;
		}

	}

	public static Messages messages() {
		List<ActivityType> asList = Arrays.asList(ActivityType.values());
		return new Messages(asList);
	}

	public static StatusMessages verbStatusMessages(VerbTense verbTense) {
		com.aawhere.util.rb.StatusMessages.Builder builder = StatusMessages.create();

		ActivityType[] values = values();
		for (ActivityType activityType : values) {
			builder.addMessage(verbStatusMessage(activityType, verbTense));
		}

		return builder.build();
	}

	public static StatusMessage verbStatusMessage(ActivityType activityType, VerbTense verbTense) {
		return new StatusMessage(verbCompleteMessage(activityType, verbTense));
	}

	public static CompleteMessage verbCompleteMessage(ActivityType activityType, VerbTense verbTense) {
		return VerbTense.completeMessage(activityType.verb, verbTense);
	}

	/**
	 * 
	 * @param activityType
	 * @param countOfParticipants
	 *            null is singlular
	 * @return
	 */
	public static CompleteMessage nounPhraseCompleteMessage(ActivityType activityType,
			@Nullable Integer countOfParticipants) {

		return CompleteMessage.create(activityType.nounAdjectivePhrase)
				.param(Param.COUNT, If.nil(countOfParticipants).use(1)).build();
	}

	public static StatusMessage
			nounPhraseStatusMessage(ActivityType activityType, @Nullable Integer countOfParticipants) {

		return new StatusMessage(nounPhraseCompleteMessage(activityType, countOfParticipants));
	}

}
