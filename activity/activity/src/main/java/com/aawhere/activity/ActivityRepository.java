/**
 *
 */
package com.aawhere.activity;

import com.aawhere.app.Installation;
import com.aawhere.field.FieldDictionary;
import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Repository;

/**
 * Storage {@link Repository} for {@link Activity}.
 * 
 * @author roller
 * 
 */
public interface ActivityRepository
		extends CompleteRepository<Activities, Activity, ActivityId> {

	/**
	 * Appends the given {@link Installation} to the {@link Activity#getHistory()}.
	 * 
	 * @param installation
	 * @throws EntityNotFoundException
	 */
	public Activity appendToHistory(ActivityId id, Installation installation) throws EntityNotFoundException;

	/**
	 * Appends the given {@link Installation} to the {@link Activity#getHistory()}.
	 * 
	 * @param installation
	 * @throws EntityNotFoundException
	 */
	public Activity appendToHistory(Activity activity, Installation installation);

	public FieldDictionary getDictionary();

	/**
	 * Marks the activity as gone (deleted from external system.
	 * 
	 * @see Activity#isGone()
	 * */
	public Activity gone(ActivityId activityId) throws EntityNotFoundException;

}
