/**
 * 
 */
package com.aawhere.activity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.activity.relation.ActivityRelationWebApiUri;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationXmlAdapter;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountFromIdXmlAdapter;
import com.aawhere.app.account.AccountId;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;
import com.googlecode.objectify.annotation.Index;

/**
 * Simple object to help organize import ids related to an Activity.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE, name = ActivityReferenceIds.FIELD.DOMAIN)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = ActivityReferenceIds.FIELD.DOMAIN)
public class ActivityReferenceIds
		implements Serializable {

	private static final long serialVersionUID = -3671003739734565332L;

	@Index
	@Field(key = FIELD.ACTIVITY_ID, dataType = FieldDataType.ATOM, indexedFor = { "TN-365", "TN-364" })
	@XmlElement(name = FIELD.ACTIVITY_ID)
	private ActivityId activityId;

	@Field(key = FIELD.APPLICATION_KEY, dataType = FieldDataType.ATOM)
	@XmlElement(name = FIELD.APPLICATION_KEY)
	private ApplicationKey applicationKey;

	@Field(key = FIELD.ACCOUNT_ID, dataType = FieldDataType.ATOM)
	@XmlElement(name = Account.FIELD.ACCOUNT_ID)
	private AccountId accountId;

	/**
	 * Used to construct all instances of ActivityIds.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<ActivityReferenceIds> {

		public Builder() {
			super(new ActivityReferenceIds());
		}

		public Builder setAccountId(AccountId accountId) {
			building.accountId = accountId;
			return this;
		}

		public Builder setActivityId(ActivityId activityId) {
			building.activityId = activityId;
			building.applicationKey = activityId.getApplicationKey();
			return this;
		}

		public Builder setActivity(Activity activity) {
			setActivityId(activity.getId());
			setAccountId(activity.getAccountId());
			return this;
		}

		public Builder setActivityIds(ActivityReferenceIds activityIds) {
			setActivityId(activityIds.activityId);
			setAccountId(activityIds.accountId);
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityIds */
	private ActivityReferenceIds() {
	}

	/**
	 * @return the activityId
	 */
	public ActivityId getActivityId() {
		return this.activityId;
	}

	/**
	 * @return the accountId
	 */
	public AccountId getAccountId() {
		return this.accountId;
	}

	/**
	 * Provides the full account.
	 * 
	 * @return
	 */
	@XmlJavaTypeAdapter(AccountFromIdXmlAdapter.class)
	public AccountId getAccount() {
		return this.accountId;
	}

	/**
	 * @return the applicationKey
	 */
	public ApplicationKey getApplicationKey() {
		return this.applicationKey;
	}

	@XmlJavaTypeAdapter(ApplicationXmlAdapter.class)
	@XmlElement(name = Application.FIELD.DOMAIN)
	public ApplicationKey getApplication() {
		return this.applicationKey;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.activityId == null) ? 0 : this.activityId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// only activity id is necessary since these values don't change for an activity.
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ActivityReferenceIds other = (ActivityReferenceIds) obj;
		if (this.activityId == null) {
			if (other.activityId != null) {
				return false;
			}
		} else if (!this.activityId.equals(other.activityId)) {
			return false;
		}
		return true;
	}

	@XmlTransient
	public static final class FIELD {
		public static final String APPLICATION_KEY = ActivityRelationWebApiUri.APPLICATION_KEY;
		public static final String ACTIVITY_ID = ActivityRelationWebApiUri.ACTIVITY_ID;
		public static final String ACCOUNT_ID = "accountId";
		public static final String DOMAIN = "activityRefIds";

		public static final class KEY {
			public static final FieldKey ACTIVITY_ID = new FieldKey(DOMAIN, FIELD.ACTIVITY_ID);
			public static final FieldKey APPLICATION_KEY = new FieldKey(DOMAIN, FIELD.APPLICATION_KEY);
		}
	}
}
