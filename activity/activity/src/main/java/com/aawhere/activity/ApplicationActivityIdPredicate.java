/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.app.Application;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Predicate;

/**
 * Used to filter a list searching for an {@link Activity} with the given
 * {@link ApplicationActivityId} for a specific {@link Application}.
 * 
 * @author Aaron Roller
 * 
 */
public class ApplicationActivityIdPredicate
		implements Predicate<Activity> {

	private ApplicationActivityId id;

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(Activity input) {
		return id.equals(input.getId());
	}

	/**
	 * Used to construct all instances of ApplicationActivityIdPredicate.
	 */
	public static class Builder
			extends ObjectBuilder<ApplicationActivityIdPredicate> {

		public Builder() {
			super(new ApplicationActivityIdPredicate());
		}

		public Builder setId(ApplicationActivityId id) {
			building.id = id;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.id);
		}

	}// end Builder

	/** Use {@link Builder} to construct ApplicationActivityIdPredicate */
	private ApplicationActivityIdPredicate() {
	}

}
