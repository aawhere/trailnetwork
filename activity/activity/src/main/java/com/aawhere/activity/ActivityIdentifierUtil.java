/**
 * 
 */
package com.aawhere.activity;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationStats;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

/**
 * Useful for creating {@link ApplicationActivityId}s and {@link ActivityId} from
 * {@link ActivityIdentifier}.
 * 
 * @author aroller
 * 
 */
public class ActivityIdentifierUtil {

	/**
	 * Just a nice method to create ids given String values and the application that they represent.
	 */

	public static ActivityId[] activityIds(ApplicationKey applicationKey, String... idValues) {
		ActivityId[] ids = new ActivityId[idValues.length];
		for (int whichId = 0; whichId < idValues.length; whichId++) {
			String currentId = idValues[whichId];
			ids[whichId] = new ActivityId(applicationKey, currentId);
		}
		return ids;
	}

	/**
	 * Given a string containing qualified applicationIds separated by commas this will parse and
	 * return the list.
	 * 
	 * @param idValuesSeparatedByCommas
	 * @return
	 */
	public static List<ActivityId> idValuesToList(String idValuesSeparatedByCommas) {
		ArrayList<ActivityId> applicationIds = new ArrayList<ActivityId>();
		if (!StringUtils.isEmpty(idValuesSeparatedByCommas)) {
			String[] idValues = StringUtils.split(idValuesSeparatedByCommas, ',');
			for (int i = 0; i < idValues.length; i++) {
				String string = idValues[i];
				applicationIds.add(new ActivityId(string));
			}
		}
		return applicationIds;
	}

	/**
	 * Indicates that the ApplicationActivityId is actually a reference to our System and not an
	 * external.
	 * 
	 * @param id
	 * @return true if the id.getKey == our system or false otherwise
	 */
	public static Boolean appIsUs(ApplicationActivityId id) {
		if (id == null) {
			return false;
		}
		return KnownApplication.TRAIL_NETWORK.key.equals(id.getApplicationKey());
	}

	/**
	 * Indicates if this can be converted to {@link ActivityId}.
	 * 
	 * @see #appIsUs(ApplicationActivityId)
	 * @see #activityIdFrom(ApplicationActivityId)
	 * 
	 * @param id
	 * @return
	 */
	public static Boolean appIsUs(ActivityIdentifier id) {
		Boolean result;
		if (id == null) {
			result = false;
		} else if (id instanceof ApplicationActivityId) {
			result = appIsUs((ApplicationActivityId) id);
		} else if (id instanceof ActivityId) {
			result = true;
		} else {
			throw new IllegalArgumentException(id.getClass() + " is not yet known to this method");
		}
		return result;
	}

	/**
	 * Given the {@link ActivityIdentifier} this will return the {@link ActivityId} from it only if
	 * it is ours or throw an exception otherwise.
	 * 
	 * @param id
	 * @return
	 * @throws IllegalArgumentException
	 *             when it can't be converted to ActivityId since calling
	 *             {@link #appIsUs(ActivityIdentifier)} would avoid this exception
	 */
	public static ActivityId activityIdFrom(ActivityIdentifier id) {
		ActivityId result;

		// this is the quickest and easiest and most common
		if (id == null) {
			result = null;
		} else if (appIsUs(id)) {
			if (id instanceof ApplicationActivityId) {
				ApplicationActivityId aid = (ApplicationActivityId) id;
				result = new ActivityId(aid);
			} else {

				return new ActivityId(id.toString());
			}

		} else {
			result = new ActivityId(id.toString());
		}
		return result;
	}

	/**
	 * Given an {@link ActivityIdentifier} this will convert it to an {@link ApplicationActivityId}
	 * if and only if it is already an {@link ApplicationActivityId} and it is not one of ours. If
	 * it is one of ours then you should be calling {@link #activityIdFrom(ActivityIdentifier)}.
	 * 
	 * @param id
	 * @return
	 * @throws IllegalArgumentException
	 *             when it is one of ours since you should call {@link #appIsUs(ActivityIdentifier)}
	 *             then {@link #activityIdFrom(ActivityIdentifier)} to get the {@link ActivityId}
	 */
	public static ActivityId applicationActivityIdFrom(ActivityId id) {
		ActivityId result;
		if (id == null) {
			result = null;
		} else if (appIsUs(id)) {
			throw new IllegalArgumentException("If one of ours you should want ActivityId.  call #appIsUs first.");
		} else {
			// not ours nor null so must be an app id already
			result = id;
		}
		return result;
	}

	/**
	 * Does a simple check to see if the key provides is a valid format. It provides no guarantee
	 * that the values the key represents are valid (such as Application or that an entity matches
	 * an ID).
	 * 
	 * @param key
	 * @return
	 */
	public static Boolean isValidKeyFormat(String key) {
		return IdentifierUtils.isCompositeKeyValue(key);
	}

	/**
	 * Inspects the value for proper format conforming to the standards for consistency. Throws
	 * exceptions providing feedback.
	 * 
	 * @param id
	 * @throws UnknownApplicationException
	 * @throws InvalidApplicationActivityIdException
	 */
	public static ActivityId validateFormat(ActivityId id) throws UnknownApplicationException,
			InvalidApplicationActivityIdException {
		Assertion.assertNotNull(Activity.FIELD.ACTIVITY_ID, id);
		String value = id.getValue();
		if (!IdentifierUtils.isCompositeKeyValue(value)) {
			throw new InvalidApplicationActivityIdException(value, IdentifierUtils.getCompositeSeparator());
		}
		KnownApplication.byKey(id.getApplicationKey());
		return new ActivityId(id.getApplicationKey(), id.getRemoteId());
	}

	/**
	 * Builds an {@link ApplicationActivityId} from the given String. It may be the absolute
	 * activity id with the application and remoteId. If it is just a number without the hyphen it
	 * will assumed to be an id for this system and will be returned as such.
	 * 
	 * @param idValue
	 * @return
	 */
	public static ActivityId idFrom(String idValue) {
		if (isValidKeyFormat(idValue)) {
			return new ActivityId(idValue);
		} else {
			return new ActivityId(KnownApplication.TRAIL_NETWORK.key, idValue);
		}
	}

	public static Boolean equals(ActivityId id, ActivityReferenceIds activityReferenceIds) {
		Boolean result;
		if (appIsUs(id)) {
			ActivityId activityId = activityIdFrom(id);
			result = activityId.equals(activityReferenceIds.getActivityId());
		} else {
			ActivityId appId = applicationActivityIdFrom(id);
			result = appId.equals(activityReferenceIds.getActivityId());
		}
		return result;
	}

	/**
	 * Simple supplier of {@link Application} from the given {@link ActivityId}
	 * 
	 * @return
	 */
	public static Function<ActivityId, Application> applicationsFromActivityIdFunction() {
		return new Function<ActivityId, Application>() {

			@Override
			public Application apply(ActivityId input) {
				return KnownApplication.byKey(input.getApplicationKey());
			}
		};
	}

	/**
	 * application counter used to provide {@link ApplicationStats} for the given ids
	 * 
	 * @param activityIds
	 * @return
	 */
	public static CategoricalStatsCounter<ApplicationStats, Application> applicationCounter(
			Iterable<ActivityId> activityIds) {
		Function<ActivityId, Application> applicationFunction = ActivityIdentifierUtil
				.applicationsFromActivityIdFunction();
		Iterable<Application> applications = Iterables.transform(activityIds, applicationFunction);
		CategoricalStatsCounter<ApplicationStats, Application> counter = ApplicationStats.counter(applications);
		return counter;
	}

	/**
	 * Provides the key from the activity id if it is not null.
	 * 
	 * @return
	 * 
	 */
	public static Function<ActivityId, ApplicationKey> applicationKeyFunction() {
		return new Function<ActivityId, ApplicationKey>() {

			@Override
			public ApplicationKey apply(ActivityId input) {
				return (input == null) ? null : input.getApplicationKey();
			}
		};
	}

	/**
	 * Upgrades the string value for the activity ids to {@link ActivityId} objects. The id values
	 * may be a remote id in which case the application must be provided. If the applicationKey is
	 * provided then it is assumed the values are remote ids and all remoteIds must come from that
	 * same application.
	 * 
	 * @param idValuesWhitespaceSeparated
	 * @param applicationKey
	 * @return
	 * @throws UnknownApplicationException
	 *             when the given key, if given, is not a known application
	 */
	public static Iterable<ActivityId> activityIdsFromWhitespaceSeparated(@Nonnull String idValuesWhitespaceSeparated,
			@Nullable ApplicationKey applicationKey) throws UnknownApplicationException {
		String[] idValues = StringUtils.split(idValuesWhitespaceSeparated);
		Iterable<ActivityId> activityIds;

		if (applicationKey != null) {
			// validates known application
			KnownApplication.byKey(applicationKey);
			ActivityId[] activityIdArray = ActivityIdentifierUtil.activityIds(applicationKey, idValues);
			activityIds = ImmutableList.copyOf(activityIdArray);
		} else {
			activityIds = activityIdsFromStrings(ImmutableList.copyOf(idValues));
		}
		return activityIds;
	}

	/**
	 * @see #activityIdFromStringFunction()
	 * @param idValues
	 * @return
	 */
	public static Iterable<ActivityId> activityIdsFromStrings(Iterable<String> idValues) {
		return Iterables.transform(idValues, activityIdFromStringFunction());
	}

	public static Function<String, ActivityId> activityIdFromStringFunction() {
		return new Function<String, ActivityId>() {

			@Override
			public ActivityId apply(String input) {
				return (input != null) ? new ActivityId(input) : null;
			}
		};
	}
}
