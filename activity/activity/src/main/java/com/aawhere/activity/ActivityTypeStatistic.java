/**
 * 
 */
package com.aawhere.activity;

import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.googlecode.objectify.annotation.Index;

/**
 * A simply map of {@link #count} a of occurrences an {@link ActivityType} appears in some grouping
 * of {@link Activities}.
 * 
 * 
 * This is {@link Comparable} ordering the highest count first, and by Enum order second.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = ActivityTypeStatistic.FIELD.DOMAIN)
public class ActivityTypeStatistic
		extends CategoricalStatistic<ActivityType>
		implements Cloneable {

	private static final long serialVersionUID = -6891240228386481896L;

	/**
	 * Used to construct all instances of ActivityTypeStats.
	 */
	@XmlTransient
	public static class Builder
			extends CategoricalStatistic.Builder<ActivityTypeStatistic, ActivityType, Builder> {

		public Builder() {
			super(new ActivityTypeStatistic());
		}

		private Builder(ActivityTypeStatistic mutant) {
			super(mutant);
		}

		public Builder type(ActivityType type) {
			building.type = type;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("type", building.type);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.calc.CategoricalStatistic.Builder#build()
		 */
		@Override
		public ActivityTypeStatistic build() {

			return super.build();

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.calc.CategoricalStatistic.Builder#category(java.lang.Object)
		 */
		@Override
		public Builder category(ActivityType category) {
			return type(category);
		}

	}// end Builder

	/** Use {@link Builder} to construct ActivityTypeStats */
	private ActivityTypeStatistic() {
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(ActivityTypeStatistic count) {
		return new Builder(count);
	}

	public static Builder clone(ActivityTypeStatistic statistic) {
		try {
			return new Builder((ActivityTypeStatistic) statistic.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	@XmlElement(name = FIELD.ACTIVITY_TYPE)
	@Field(key = FIELD.ACTIVITY_TYPE, indexedFor = "TN-576", dataType = FieldDataType.ATOM)
	@Index
	private ActivityType type;

	/**
	 * familar synonym for {@link #category()}
	 * 
	 * @return the type
	 */
	public ActivityType type() {
		return this.type;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.CategoricalStats#category()
	 */
	@Override
	public ActivityType category() {
		return type;
	}

	public static Function<ActivityTypeStatistic, ActivityType> typeFunction() {
		return CategoricalStatistic.categoryFunction();
	}

	@SuppressWarnings("unchecked")
	public static Function<ActivityTypeStatistic, Integer> countFunction() {
		return CategoricalStatistic.countFunction();
	}

	/** Simply provides the activity types from the counts given. */
	public static Set<ActivityType> activityTypes(Iterable<ActivityTypeStatistic> counts) {
		return Sets.newHashSet(Iterables.transform(counts, typeFunction()));
	}

	/**
	 * Provides the activity types in the order provided by the stats.
	 * 
	 * @param stats
	 * @return
	 */
	public static List<ActivityType> activityTypesList(Iterable<ActivityTypeStatistic> stats) {
		return ImmutableList.<ActivityType> builder().addAll(Iterables.transform(stats, typeFunction())).build();
	}

	public static class FIELD {
		public static final String ACTIVITY_TYPE = Activity.FIELD.ACTIVITY_TYPE;
		public static final String DOMAIN = "activityTypeStats";

		public static class KEY {
			public static final FieldKey ACTIVITY_TYPE = new FieldKey(FIELD.DOMAIN, FIELD.ACTIVITY_TYPE);
			public static final FieldKey PERCENT = new FieldKey(FIELD.DOMAIN, CategoricalStatistic.FIELD.PERCENT);
		}
	}

	public static Supplier<Builder> builderSupplier() {
		return new Supplier<ActivityTypeStatistic.Builder>() {

			@Override
			public Builder get() {
				return create();
			}
		};
	}

	/**
	 * Provides a counter to be used and configured by the client. This just makes it cleaner and
	 * assigns the given categories as well as the {@link #builderSupplier()}.
	 * 
	 * @param categories
	 * @return
	 */
	public static CategoricalStatsCounter.Builder<ActivityTypeStatistic, ActivityType, Builder> counter(
			Iterable<ActivityType> categories) {
		CategoricalStatsCounter.Builder<ActivityTypeStatistic, ActivityType, ActivityTypeStatistic.Builder> builder = CategoricalStatsCounter
				.create();
		builder.categories(categories);
		builder.statsBuilder(builderSupplier());

		return builder;
	}

}
