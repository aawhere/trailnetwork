package com.aawhere.activity;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang.StringUtils;

import com.aawhere.activity.RelativeStatsUtil.PersonIdXmlAdapter;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.xml.CountFromIntegerXmlAdapter;
import com.aawhere.person.PersonId;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.ImmutableList;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Reports the number of times a Route has been completed the context:
 * 
 * <ul>
 * <li>Person - only the person identified</li>
 * <li>ActivityType - the sport being performed for all</li>
 * <li>Person + Activity Type - the Person performing a specific ActivityType</li>
 * </ul>
 * 
 * The omission of any filter indicates the count is for all completions at the TrailNetwork.
 * 
 */
// FIXME: this should extend Categorical Statstic with Total being optional
@ApiModel(description = "Number of RouteCompletions for those filter specified")
@Dictionary(domain = "relativeStats")
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class RelativeStatistic
		implements Serializable, Cloneable {

	private static final long serialVersionUID = 5058037268699493031L;

	static final char SEPARATOR = '|';

	@ApiModel(value = "Allows for filtering based on related filters")
	public enum Layer {
		/** */
		@ApiModelProperty("All in the TrailNetwork")
		ALL(false, false, false),

		/** */
		@ApiModelProperty("Grouped by similar sports")
		ACTIVITY_TYPE(true, false, false),

		/** */
		@ApiModelProperty("Grouped by similar sports and the length of the Activity")
		ACTIVITY_TYPE_DISTANCE(true, false, true),

		/** */
		@ApiModelProperty("Grouped by Person")
		PERSON(false, true, false),

		/** */
		@ApiModelProperty("Grouped by Person and sport")
		PERSON_ACTIVITY_TYPE(true, true, false),
		/** */
		@ApiModelProperty("Person, sport and the length of the Activity")
		PERSON_ACTIVITY_TYPE_DISTANCE(true, true, true), ;

		final public Boolean activityTyped;
		final public Boolean personal;
		final public Boolean distanceMeasured;

		private Layer(Boolean activityType, Boolean person, Boolean distanceMeasured) {
			this.activityTyped = activityType;
			this.personal = person;
			this.distanceMeasured = distanceMeasured;
		}

	}

	@ApiModelProperty(value = "Uniquely identifies this count from all others",
			notes = "The id is not used to retrieve this from anywhere.", required = true)
	@XmlAttribute
	@Field
	@Nonnull
	private String id;

	/**
	 * token allowing for quick separation between statistics. Although this is embedded in the id,
	 * it is better to provide it directly so a client may take advantage of the classification
	 * effort.
	 * 
	 */
	@Field(searchable = true)
	@Nonnull
	@XmlElement
	private Layer layer;

	@ApiModelProperty(value = "The number of times a route has been completed", required = true)
	@XmlElement
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Field(searchable = true)
	@Nonnull
	private Integer count;

	@ApiModelProperty(value = "The Person that completed the route")
	@XmlElement
	@Field(/* xmlAdapter = PersonIdXmlAdapter.class this isn't working, */searchable = true)
	@Nullable
	private PersonId personId;

	@ApiModelProperty(value = "The ActivityType that completed the route",
			notes = "Represents only this count. The Route may be completed by other Activity Types.")
	@XmlElement
	@Field(searchable = true)
	@Nullable
	private ActivityType activityType;

	@XmlElement
	@Field(searchable = true)
	@Nullable
	@ApiModelProperty(value = "Indicates the distance range specific for the Activity Type.")
	private ActivityTypeDistanceCategory distance;

	/**
	 * Used to construct all instances of RouteCompletionCount.
	 */
	public static class Builder
			extends ObjectBuilder<RelativeStatistic> {

		private Identifier<?, ?> featureId;

		public Builder() {
			super(new RelativeStatistic());
		}

		private Builder(RelativeStatistic mutant) {
			super(mutant);
		}

		/** @see #activityType */
		public Builder activityType(ActivityType activityType) {
			building.activityType = activityType;
			return this;
		}

		/** @see #distance */
		public Builder distance(ActivityTypeDistanceCategory distance) {
			if (distance != null) {
				building.distance = distance;
				activityType(distance.activityType);
			}
			return this;
		}

		/** @see #personId */
		public Builder personId(PersonId personId) {
			building.personId = personId;
			return this;
		}

		/** @see #count */
		public Builder count(Integer count) {
			building.count = count;
			return this;
		}

		/** @see #layer */
		public Builder layer(Layer layer) {
			building.layer = layer;
			return this;
		}

		@Override
		public RelativeStatistic build() {
			RelativeStatistic built = super.build();
			if (built.layer == null) {
				boolean hasPersonId = built.hasPersonId();
				boolean hasActivityType = built.hasActivityType();
				boolean hasDistance = built.hasDistance();
				Layer autoLayer = RelativeStatsUtil.layer(hasPersonId, hasActivityType, hasDistance);
				built.layer = autoLayer;
			}
			if (built.id == null) {
				Assertion.exceptions().notNull("featureId", this.featureId);
				ImmutableList.Builder<String> idParts = ImmutableList.builder();
				// #featureId depends upon this order
				idParts.add(IdentifierUtils.domain(featureId));
				idParts.add(featureId.toString());
				idParts.add(built.layer.name());
				if (built.hasPersonId()) {
					idParts.add(built.personId.toString());
				}
				if (built.hasActivityType()) {
					idParts.add(built.activityType.toString());
					if (built.hasDistance()) {
						idParts.add(built.distance.toString());
					}
				}
				built.id = StringUtils.join(idParts.build(), SEPARATOR);
			}
			return built;
		}

		@Override
		protected void validate() {

			// count isn't always available when putting this to use as an identifier.
			// use clone to later add count
			// Assertion.exceptions().notNull("count", building.count);

			super.validate();
		}

		/** @see #id */
		public Builder id(String id) {
			building.id = id;
			return this;
		}

		public Builder featureId(Identifier<?, ?> featureId) {
			this.featureId = featureId;
			return this;
		}

		/**
		 * Clears out {@link #id(String)}, especially useful when {@link #clone()} is called to
		 * change the {@link #featureId(Identifier)}.
		 * 
		 * @return
		 */
		public Builder resetId() {
			building.id = null;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct RouteCompletionCount */
	private RelativeStatistic() {
	}

	/** @see #count */
	public Integer count() {
		return this.count;
	}

	/** @see #activityType */
	public ActivityType activityType() {
		return this.activityType;
	}

	/** @see #personId */
	public PersonId personId() {
		return this.personId;
	}

	@XmlJavaTypeAdapter(PersonIdXmlAdapter.class)
	private PersonId getPerson() {
		return personId();
	}

	/** @see #id */
	public String id() {
		return this.id;
	}

	/** @see #layer */
	public Layer layer() {
		return this.layer;
	}

	/** @see #distance */
	public ActivityTypeDistanceCategory distance() {
		return this.distance;
	}

	public boolean hasDistance() {
		return this.distance != null;
	}

	public boolean hasActivityType() {
		return this.activityType != null;
	}

	public boolean hasPersonId() {
		return this.personId != null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelativeStatistic other = (RelativeStatistic) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.id;
	}

	public static Builder clone(RelativeStatistic incoming) {
		try {
			return new Builder((RelativeStatistic) incoming.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}

	}
}
