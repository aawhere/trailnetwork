/**
 *
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.persist.EntityMessage;
import com.aawhere.swagger.DocSupport.Allowable;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Provides personalized messages for {@link Activity}.
 * 
 * @author Aaron Roller
 * 
 */
@XmlTransient
public enum ActivityMessage implements Message {

	/** Used when the name is not provided. */
	UNKNOWN(ActivityMessage.UNKNOWN_VALUE),
	/** Message explaining the correct format when detected otherwise */
	INVALID_APPLICATION_ACTIVITY_ID(
			"The id '{ID}' is not in the correct format: [Application Key]{DILIMETER}[ID].",
			Param.ID,
			Param.DILIMETER),
	ID_NAME(EntityMessage.ID_NAME.getValue()),
	ID_DESCRIPTION(EntityMessage.ID_DESCRIPTION.getValue()),
	ACTIVITY_NAME("Activity"),
	ACTIVITY_DESCRIPTION("One person's recording of a recreational, fitness or other movement."),
	NAME_NAME("Activity Name"),
	NAME_DESCRIPTION("A title the person provided to describe their activity."),
	DESCRIPTION_NAME("Activity Description"),
	DESCRIPTION_DESCRIPTION(
			"Details the person performing the activity provided to understand what happened during the activity."),
	ACTIVITY_TYPE_NAME("Activity Type"),
	ACTIVITY_TYPE_DESCRIPTION("The type of activity performed. Examples: " + ActivityType.HIKE.getValue() + ", "
			+ ActivityType.BIKE.getValue() + ", " + ActivityType.RUN.getValue()),
	DATE_CREATED_NAME(EntityMessage.DATE_CREATED_NAME.getValue()), /* */
	DATE_CREATED_DESCRIPTION(EntityMessage.DATE_CREATED_DESCRIPTION.getValue()), /* */
	DATE_UPDATED_NAME(EntityMessage.DATE_UPDATED_NAME.getValue()), /* */
	DATE_UPDATED_DESCRIPTION(EntityMessage.DATE_UPDATED_DESCRIPTION.getValue()), /* */
	TRACK_SUMMARY_NAME("Track Summary"),
	TRACK_SUMMARY_DESCRIPTION("The TrackSummary associated with this activity."),
	APPLICATION_KEY_NAME("Application"),
	APPLICATION_KEY_DESCRIPTION("Application this activity came from.");

	public static final String UNKNOWN_VALUE = "Unknown";
	private ParamKey[] parameterKeys;
	private String message;

	private ActivityMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		/** The value of the ID */
		ID,
		/** The separator used for id formats. */
		DILIMETER,
		/**
		 * The {@link ApplicationKey} of an {@link Application} or the Application itself.
		 */
		APPLICATION;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	/** For {@link ActivityType} */
	public static final class Type {
		public static final class Doc {
			public static final String DESCRIPTION = "The sport being performed.";
			public static final String FULL_DESCRIPTION = "Activity Types describe the sport being performed when recording an Activity. "
					+ "A smaller set than provided by most Applications the choice of ActivityTypes provided is meant to generalize to easier to understand and less personal, but a benfit of the community. "
					+ "The Activity Type provided with the Activity by the Account holder at the Application is mapped from the Application's Activity Types to our own. "
					+ "Referenced by their key in upper or lower case, the Activity Types also provide descriptions intended to be displayed to the end user. ";
		}
	}

	public static final class Doc {
		public static final String FULL_DESCRIPTION = "A single 'session' of a workout or recreational Activity, typically recorded by a GPS Track log. "
				+ "Recorded by an Account at an Application and Activity is imported and processed from the details of the Track producing the TrackSummary. ";
		public static final String ACTIVITY_TYPE_DATA_TYPE = "ActivityType";
		/**
		 * A limited set of activity types commonly used. FIXME:This should be generated.
		 */
		public static final String ACTIVITY_TYPE_ALLOWABLE_OPTIONS = "BIKE" + Allowable.NEXT + "RUN" + Allowable.NEXT
				+ "MTB" + Allowable.NEXT + "WALK";

	}
}
