/**
 * 
 */
package com.aawhere.activity;

import java.io.Serializable;
import java.util.SortedSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.collections4.CollectionUtils;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationKeyStats;
import com.aawhere.app.ApplicationStats;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;

/**
 * Statistic provided for some group of activities providing informaiton like
 * {@link ApplicationStats} and {@link ActivityTypeStatistic} and potentially other
 * {@link CategoricalStatistic}.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE, name = ActivityStats.FIELD.DOMAIN)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = ActivityStats.FIELD.DOMAIN)
public class ActivityStats
		implements Serializable, Cloneable {

	@Nonnull
	@XmlAttribute
	private Integer total;

	/**
	 * Maps the number of activities performed for each activity type.
	 * 
	 * */
	@Nullable
	@XmlElement(name = FIELD.ACTIVITY_TYPE_STATS)
	@Field(key = FIELD.ACTIVITY_TYPE_STATS, parameterType = ActivityTypeStatistic.class)
	private SortedSet<ActivityTypeStatistic> activityTypeStats;

	@Nullable
	@XmlElement(name = FIELD.APPLICATION_STATS)
	@Field(key = FIELD.APPLICATION_STATS, parameterType = ApplicationStats.class)
	private SortedSet<ApplicationStats> applicationStats;

	/** Indicates the number of common devices based on the keys provided from the device. */
	@Nullable
	@XmlElement(name = FIELD.DEVICE_TYPE_STATS)
	@Field(parameterType = ApplicationKeyStats.class)
	private SortedSet<ApplicationKeyStats> deviceTypeStats;

	@Nullable
	@Field(parameterType = ActivityTypeDistanceCategoryStatistic.class)
	@XmlElement
	private SortedSet<ActivityTypeDistanceCategoryStatistic> distanceStats;

	/**
	 * @return the activityTypeStats
	 */
	public SortedSet<ActivityTypeStatistic> activityTypeStats() {
		return this.activityTypeStats;
	}

	/**
	 * @return the applicationStats
	 */
	public SortedSet<ApplicationStats> applicationStats() {
		return this.applicationStats;
	}

	public SortedSet<ActivityTypeDistanceCategoryStatistic> distanceStats() {
		return this.distanceStats;

	}

	public boolean hasDistanceStats() {
		return CollectionUtils.isNotEmpty(this.distanceStats);
	}

	public boolean hasActivityTypeStats() {
		return CollectionUtils.isNotEmpty(this.activityTypeStats);
	}

	public boolean hasDeviceTypeStats() {
		return CollectionUtils.isNotEmpty(this.deviceTypeStats);
	}

	public boolean hasApplicationStats() {
		return CollectionUtils.isNotEmpty(this.applicationStats);
	}

	/**
	 * @return the total
	 */
	public Integer total() {
		return this.total;
	}

	/**
	 * Used to construct all instances of ActivityStats.
	 */
	@XmlTransient
	public static class Builder
			extends AbstractBuilder<ActivityStats, Builder> {
		public Builder() {
			super(new ActivityStats());
		}

		/**
		 * @param clone
		 */
		public Builder(ActivityStats mutant) {
			super(mutant);
		}

	}

	public static class AbstractBuilder<S extends ActivityStats, B extends AbstractBuilder<S, B>>
			extends AbstractObjectBuilder<S, B> {

		/** Used when counting is being done by the builder, not assigned. */
		private Integer countingTotal;

		protected AbstractBuilder(S other) {
			super(other);
		}

		private B total(CategoricalStatsCounter<?, ?> counter) {
			Integer counterTotal = counter.total();
			totalCounted(counterTotal);
			return dis;
		}

		/**
		 * When methods are counting upon assignment they use this method which picks the best
		 * counts provided.
		 * 
		 * {@link #total(Integer)} will over-write everything.
		 * 
		 * @param counterTotal
		 */
		private void totalCounted(Integer counterTotal) {
			if (this.countingTotal != null) {
				// some stats are filtered so respect the larger total given from any source
				// this total affects the percentages
				this.countingTotal = Math.max(this.countingTotal, counterTotal);
			} else {
				this.countingTotal = counterTotal;
			}
		}

		private ActivityStats building() {
			return building;
		}

		/**
		 * When the client wishes to communicate a total different than that which is autatomically
		 * calculated.
		 * 
		 * @param counterTotal
		 */
		public B total(Integer counterTotal) {
			building().total = counterTotal;
			return dis;
		}

		/**
		 * Pulls all import attributes from the given and assigns it to {@link #building()}.
		 * Purposely general to allow a subclass to absorb the properties of the parent.
		 */
		public B copy(ActivityStats from) {
			total(from.total());
			applicationStats(from.applicationStats());
			deviceTypeStats(from.deviceTypeStats());
			activityTypeStats(from.activityTypeStats());
			distanceStats(from.distanceStats);
			return dis;
		}

		public B applicationStats(SortedSet<ApplicationStats> applicationStats) {
			if (applicationStats != null) {
				building().applicationStats = applicationStats;
				total(applicationStats);
			}
			return dis;
		}

		public B deviceTypeStats(SortedSet<ApplicationKeyStats> deviceTypeStats) {
			if (deviceTypeStats != null) {
				building().deviceTypeStats = deviceTypeStats;
				total(deviceTypeStats);
			}
			return dis;
		}

		public B activityTypeStats(SortedSet<ActivityTypeStatistic> activityTypeStats) {
			if (activityTypeStats != null) {
				building().activityTypeStats = activityTypeStats;
				total(activityTypeStats);
			}
			return dis;
		}

		public B distanceStats(SortedSet<ActivityTypeDistanceCategoryStatistic> distanceStats) {
			if (distanceStats != null) {
				building().distanceStats = distanceStats;
				total(distanceStats);
			}
			return dis;
		}

		/**
		 * @param applicationStats
		 * @return
		 */
		private <T extends CategoricalStatistic<C>, C> B total(SortedSet<T> stats) {
			totalCounted(CategoricalStatsCounter.sum(stats));
			return dis;
		}

		public B applicationCounter(CategoricalStatsCounter<ApplicationStats, Application> applicationStatsCounter) {
			building().applicationStats = applicationStatsCounter.stats();
			total(applicationStatsCounter);
			return dis;
		}

		public B deviceTypeCounter(CategoricalStatsCounter<ApplicationKeyStats, ApplicationKey> deviceTypeCounter) {
			building().deviceTypeStats = deviceTypeCounter.stats();
			total(deviceTypeCounter);
			return dis;
		}

		public B activityTypeCounter(CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> activityTypeCounter) {
			building().activityTypeStats = activityTypeCounter.stats();
			total(activityTypeCounter);
			return dis;
		}

		public
				B
				distanceCounter(
						CategoricalStatsCounter<ActivityTypeDistanceCategoryStatistic, ActivityTypeDistanceCategory> distanceCounter) {
			building().distanceStats = distanceCounter.stats();
			total(distanceCounter);
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			// either sets total...if not set then what's the point of this?
			Assertion.exceptions().notNull("total", building().total);

		}

		@Override
		public S build() {
			if (building().total == null) {
				building().total = this.countingTotal;
			}
			S built = super.build();

			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct ActivityStats */
	protected ActivityStats() {
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder clone(ActivityStats original) {
		try {
			return new Builder((ActivityStats) original.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	public static class FIELD {

		public static final String DEVICE_TYPE_STATS = "deviceTypeStats";
		public static final String DOMAIN = "activityStats";
		public static final String ACTIVITY_TYPE_STATS = ActivityTypeStatistic.FIELD.DOMAIN;
		public static final String APPLICATION_STATS = ApplicationStats.FIELD.DOMAIN;

		public static final class KEY {
			public static final FieldKey ACTIVITY_TYPES = new FieldKey(new FieldKey(DOMAIN, ACTIVITY_TYPE_STATS),
					ActivityTypeStatistic.FIELD.ACTIVITY_TYPE);
			public static final FieldKey APPLICATION = new FieldKey(new FieldKey(DOMAIN, APPLICATION_STATS),
					ApplicationStats.FIELD.APPLICATION);
		}
	}

	private static final long serialVersionUID = 1918478069385422012L;

	/**
	 * Technically mutation, but self mutation to make sure state of stats are updated with total
	 * which they do not store with each entry.
	 * 
	 */
	public void postLoad() {
		CategoricalStatistic.postLoad(this.activityTypeStats, total);
		CategoricalStatistic.postLoad(this.applicationStats, total);
		CategoricalStatistic.postLoad(this.deviceTypeStats, total);
		CategoricalStatistic.postLoad(this.distanceStats, total);
	}

	public static ActivityStats activityTypesOnly(Iterable<ActivityType> activityTypes) {
		return ActivityStats.create().activityTypeCounter(ActivityTypeStatistic.counter(activityTypes).build()).build();
	}

	/**
	 * Accommodates calculating device type stats at a later time. This is a mutator, but
	 * specifically a completing mutator that only assigns if not already assigned. The total is
	 * completed ignored since the total has already been decided and future loading will receive
	 * the total across all stats managed by this.
	 * 
	 * @param deviceTypeStats
	 * @return
	 */
	public ActivityStats completeDeviceTypes(
			CategoricalStatsCounter<ApplicationKeyStats, ApplicationKey> deviceTypeStatsCounter) {
		Assertion.e().assertNull("deviceTypeStats", this.deviceTypeStats);
		this.deviceTypeStats = deviceTypeStatsCounter.stats();

		return this;
	}

	/**
	 * @return
	 * @return
	 */
	public SortedSet<ApplicationKeyStats> deviceTypeStats() {
		return this.deviceTypeStats;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("(");
		stringBuilder.append(total());
		stringBuilder.append(")");
		if (this.activityTypeStats != null) {
			stringBuilder.append(this.activityTypeStats);
		}
		if (this.distanceStats != null) {
			stringBuilder.append(this.distanceStats);
		}
		if (this.applicationStats != null) {
			stringBuilder.append(this.applicationStats);
		}
		if (this.deviceTypeStats != null) {
			stringBuilder.append(this.deviceTypeStats);
		}
		return stringBuilder.toString();
	}

	private enum TotalFunction implements Function<ActivityStats, Integer> {
		INSTANCE;
		@Override
		@Nullable
		public Integer apply(@Nullable ActivityStats activityStats) {
			return (activityStats != null) ? activityStats.total() : null;
		}

		@Override
		public String toString() {
			return "TotalFunction";
		}
	}

	public static Function<ActivityStats, Integer> totalFunction() {
		return TotalFunction.INSTANCE;
	}
}
