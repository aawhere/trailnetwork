/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Communicates the format is incorrect for {@link ActivityId}.
 * 
 * @see ActivityMessage#INVALID_APPLICATION_ACTIVITY_ID
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class InvalidApplicationActivityIdException
		extends BaseRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5206007148324983805L;

	/**
	 * @param dynamicResourceBundleMessage
	 */
	public InvalidApplicationActivityIdException(String invalidId, String dilimeter) {
		super(new CompleteMessage.Builder(ActivityMessage.INVALID_APPLICATION_ACTIVITY_ID)
				.addParameter(ActivityMessage.Param.ID, invalidId)
				.addParameter(ActivityMessage.Param.DILIMETER, dilimeter).build());
	}

}
