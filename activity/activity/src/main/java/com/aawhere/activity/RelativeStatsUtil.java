package com.aawhere.activity;

import java.io.Serializable;
import java.util.SortedSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.field.DomainWrongException;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.identity.IdentityManager;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonProvider;
import com.aawhere.person.group.SocialGroupMembership.PersonXmlAdapter;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;

public class RelativeStatsUtil {

	private enum FeatureIdFunction implements Function<RelativeStatistic, String> {
		INSTANCE;
		@Override
		public String apply(RelativeStatistic input) {
			return (input != null) ? featureIdValue(input) : null;
		}
	}

	public static Function<RelativeStatistic, String> featureIdFunction() {
		return FeatureIdFunction.INSTANCE;
	}

	public static Iterable<String> featureIds(Iterable<RelativeStatistic> stats) {
		return Iterables.transform(stats, featureIdFunction());
	}

	public static Iterable<String> ids(Iterable<RelativeStatistic> stats) {
		return Iterables.transform(stats, idFunction());
	}

	private enum IdFunction implements Function<RelativeStatistic, String>, Serializable {
		INSTANCE;
		@Override
		public String apply(RelativeStatistic input) {
			return (input != null) ? input.id() : null;
		}
	}

	public static Function<RelativeStatistic, String> idFunction() {
		return IdFunction.INSTANCE;
	}

	private enum PersonalLayerFunction implements Function<Layer, Boolean> {
		INSTANCE;
		@Override
		public Boolean apply(Layer input) {
			return (input != null) ? input.personal : null;
		}
	}

	public static Function<Layer, Boolean> personalLayerFunction() {
		return PersonalLayerFunction.INSTANCE;
	}

	/**
	 * @see #personalExcludedPredicate()
	 * @param unfiltered
	 * @return
	 */
	@Nonnull
	public static Iterable<RelativeStatistic> personalExcluded(@Nonnull Iterable<RelativeStatistic> unfiltered) {
		return Iterables.filter(unfiltered, personalExcludedPredicate());
	}

	/**
	 * @see #personalLayerPredicate(boolean)
	 * @return
	 */
	public static Predicate<RelativeStatistic> personalExcludedPredicate() {
		return personalLayerPredicate(false);
	}

	/**
	 * * Filters out anything that either declares itself as {@link Layer#personal} or is not
	 * otherwise able to declare one way or the other (null values for example).
	 * 
	 * 
	 * @return
	 */
	public static Predicate<RelativeStatistic> personalLayerPredicate(boolean includePersonal) {
		return Predicates.and(	Predicates.notNull(),
								Predicates.compose(	Predicates.equalTo(includePersonal),
													Functions.compose(personalLayerFunction(), layerFunction())));
	}

	/**
	 * @see #personalLayerPredicate(boolean)
	 * @return
	 */
	public static Predicate<RelativeStatistic> personalOnlyPredicate() {
		return personalLayerPredicate(true);
	}

	private enum ActivityTypeFunction implements Function<RelativeStatistic, ActivityType> {
		INSTANCE;
		@Override
		@Nullable
		public ActivityType apply(@Nullable RelativeStatistic stats) {
			return (stats != null) ? stats.activityType() : null;
		}

		@Override
		public String toString() {
			return "ActivityTypeFunction";
		}
	}

	public static Function<RelativeStatistic, ActivityType> activityTypeFunction() {
		return ActivityTypeFunction.INSTANCE;
	}

	private enum DistanceFunction implements Function<RelativeStatistic, ActivityTypeDistanceCategory> {
		INSTANCE;
		@Override
		@Nullable
		public ActivityTypeDistanceCategory apply(@Nullable RelativeStatistic relativeStat) {
			return (relativeStat != null) ? relativeStat.distance() : null;
		}

		@Override
		public String toString() {
			return "DistanceFunction";
		}
	}

	public static Function<RelativeStatistic, ActivityTypeDistanceCategory> distanceFunction() {
		return DistanceFunction.INSTANCE;
	}

	/**
	 * Maps the relative to the categorical statistic given the total.
	 * 
	 * @param relativeStatistic
	 * @param total
	 * @return
	 */
	@Nullable
	public static ActivityTypeStatistic activityTypeStatistic(@Nullable RelativeStatistic relativeStatistic,
			Integer total) {
		if (relativeStatistic == null) {
			return null;
		}
		return ActivityTypeStatistic.create().category(relativeStatistic.activityType())
				.count(relativeStatistic.count()).total(total).build();
	}

	public static ActivityTypeDistanceCategoryStatistic distanceStatistic(RelativeStatistic relativeStatistic,
			Integer total) {
		if (relativeStatistic == null || !relativeStatistic.hasDistance()) {
			return null;
		}
		return ActivityTypeDistanceCategoryStatistic.create().category(relativeStatistic.distance())
				.count(relativeStatistic.count()).total(total).build();
	}

	private static class DistanceStatisticFunction
			implements Function<RelativeStatistic, ActivityTypeDistanceCategoryStatistic>, Serializable {

		private static final long serialVersionUID = 8266442625098783201L;
		final private Integer total;

		public DistanceStatisticFunction(Integer total) {
			this.total = total;
		}

		@Override
		@Nullable
		public ActivityTypeDistanceCategoryStatistic apply(@Nullable RelativeStatistic stat) {
			return distanceStatistic(stat, total);
		}

		@Override
		public String toString() {
			return "DistanceStatisticFunction";
		}
	}

	public static Function<RelativeStatistic, ActivityTypeDistanceCategoryStatistic> distanceStatisticFunction(
			Integer total) {
		return new DistanceStatisticFunction(total);
	}

	private static class ActivityTypeStatisticFunction
			implements Function<RelativeStatistic, ActivityTypeStatistic>, Serializable {

		private static final long serialVersionUID = 1796686142100647346L;
		private Integer total;

		private ActivityTypeStatisticFunction(Integer total) {
			this.total = total;
		}

		@Override
		@Nullable
		public ActivityTypeStatistic apply(@Nullable RelativeStatistic relativeStats) {
			return activityTypeStatistic(relativeStats, total);
		}

		@Override
		public String toString() {
			return "ActivityTypeStatisticFunction";
		}
	}

	public static Function<RelativeStatistic, ActivityTypeStatistic> activityTypeStatisticFunction(final Integer total) {
		return new ActivityTypeStatisticFunction(total);
	}

	public static SortedSet<ActivityTypeStatistic> activityTypeStats(Iterable<RelativeStatistic> relativeStats,
			Integer total) {
		Iterable<ActivityTypeStatistic> activityTypeStats = Iterables.transform(relativeStats,
																				activityTypeStatisticFunction(total));
		Iterable<ActivityTypeStatistic> notNullActivityTypeStats = Iterables.filter(activityTypeStats,
																					Predicates.notNull());
		ImmutableSortedSet<ActivityTypeStatistic> sortedActivityTypeStats = ImmutableSortedSet
				.<ActivityTypeStatistic> naturalOrder().addAll(notNullActivityTypeStats).build();
		return ActivityUtil.bikingCombined(sortedActivityTypeStats, ActivityUtil.MIN_ACTIVITY_TYPE_PERCENT);
	}

	public static SortedSet<ActivityTypeDistanceCategoryStatistic>
			activityTypeDistanceStats(Iterable<RelativeStatistic> relativeStats,
					SortedSet<ActivityTypeStatistic> activityTypeStats, Integer total) {
		// FIXME:This needs to generalize biking to match the activity types chosen by merging the
		// categories.
		Iterable<ActivityTypeDistanceCategoryStatistic> distanceStats = Iterables
				.transform(relativeStats, distanceStatisticFunction(total));
		Iterable<ActivityTypeDistanceCategoryStatistic> notNullDistanceStats = Iterables.filter(distanceStats,
																								Predicates.notNull());
		return ImmutableSortedSet.<ActivityTypeDistanceCategoryStatistic> naturalOrder().addAll(notNullDistanceStats)
				.build();
	}

	/**
	 * Given the portion of the relative count compared to the total this will compute a relative
	 * score against the given.
	 * 
	 * So if the score is 100 and a relative stats has 1 completion of a total 10 yields 10% then
	 * the resulting score would be 10. That way when someone that completes 5 will get 50% and a
	 * score of 50. If all are called the the total of all scores would be the score itself.
	 * 
	 * 
	 * 
	 * @param score
	 * @param ratio
	 * @return
	 */
	static public Integer score(Integer score, Ratio ratio) {
		return (int) (score * ratio.doubleValue(ExtraUnits.RATIO));
	}

	// request scope
	public static class PersonIdXmlAdapter
			extends PersonXmlAdapter {
		public PersonIdXmlAdapter() {
		}

		public PersonIdXmlAdapter(Function<PersonId, Person> provider, IdentityManager identityManager) {
			super(provider, identityManager);
		}

		@Inject
		public PersonIdXmlAdapter(PersonProvider provider, IdentityManager identityManager) {
			super(provider, identityManager);
		}

	}

	/**
	 * Partially deciphers the id into it's domain and feature id to be translated into an
	 * Identifier by a knowledgable client.
	 * 
	 * @param id
	 * @return the domain in the left and the id in the right.
	 */
	public static Pair<String, String> featureIdWithDomain(String id) {
		String[] parts = StringUtils.split(id, RelativeStatistic.SEPARATOR);
		if (parts.length < 2) {
			throw InvalidArgumentException.notUnderstood("relative stats id", id);
		}
		return Pair.of(parts[0], parts[1]);
	}

	/**
	 * Provides the featureId from the stats (embedded in the {@link #id()}) regardless of the
	 * domain in which it is unique.
	 * 
	 * @param stats
	 * @return
	 */
	public static String featureIdValue(RelativeStatistic stats) {
		return featureIdWithDomain(stats.id()).getRight();
	}

	public static <I extends Identifier<?, ?>> I featureId(RelativeStatistic stats, Class<I> idType) {
		return IdentifierUtils.idFrom(featureIdValue(stats), idType);
	}

	/**
	 * Further assists extracting the feature id and validates the domain is as expected.
	 * 
	 * @param relativeStatsId
	 * @param domainExpected
	 * @return
	 * @throws DomainWrongException
	 */
	public static String featureIdForDomain(String relativeStatsId, String domainExpected) throws DomainWrongException {
		if (relativeStatsId == null) {
			return null;
		}
		Pair<String, String> featureId = featureIdWithDomain(relativeStatsId);
		String domain = featureId.getLeft();
		if (domainExpected.equals(domain)) {
			String idValue = featureId.getRight();
			return idValue;
		} else {
			throw new DomainWrongException(domainExpected, domain);
		}
	}

	private enum LayerFunction implements Function<RelativeStatistic, Layer> {
		INSTANCE;
		@Override
		@Nullable
		public Layer apply(@Nullable RelativeStatistic stats) {
			return (stats != null) ? stats.layer() : null;
		}

		@Override
		public String toString() {
			return "LayerFunction";
		}
	}

	public static Function<RelativeStatistic, Layer> layerFunction() {
		return LayerFunction.INSTANCE;
	}

	/**
	 * Given the information this will create all possible relative stats and how they match each
	 * layer. When provided:
	 * <ul>
	 * <li>Only featureId then {@link Layer#ALL} is provided</li>
	 * <li>ActivityType then {@link Layer#ALL} and {@link Layer#ACTIVITY_TYPE}</li>
	 * <li>Person then {@link Layer#ALL} and {@link Layer#PERSON}</li>
	 * <li>Person and ActivityType then {@link Layer#ALL},{@link Layer#ACTIVITY_TYPE},
	 * {@link Layer#PERSON} and {@link Layer#PERSON_ACTIVITY_TYPE}.</li>
	 * </ul>
	 * 
	 * @param featureId
	 *            uniquely identifying the entity within it's domain
	 * @param personId
	 *            the person related to the feature
	 * @param activityType
	 *            the type of activity related to the feature
	 * @param distance
	 *            the length the feature is identifying effort
	 * @return the list containing all of the {@link RelativeStatistic} for the layers. It is empty
	 *         if featureId is null.
	 */
	@Nonnull
	public static ImmutableList<RelativeStatistic> relativeStats(@Nullable Identifier<?, ?> featureId,
			@Nullable PersonId personId, @Nullable ActivityType activityType, @Nullable Length distance) {
		ImmutableList.Builder<RelativeStatistic> all = ImmutableList.builder();
		if (featureId != null) {
			all.add(RelativeStatistic.create().featureId(featureId).layer(Layer.ALL).build());
			if (personId != null) {
				all.add(RelativeStatistic.create().featureId(featureId).layer(Layer.PERSON).personId(personId).build());
				if (activityType != null) {
					all.add(RelativeStatistic.create().featureId(featureId).layer(Layer.PERSON_ACTIVITY_TYPE)
							.personId(personId).activityType(activityType).build());

					if (distance != null) {
						all.add(RelativeStatistic.create().featureId(featureId)
								.layer(Layer.PERSON_ACTIVITY_TYPE_DISTANCE).personId(personId)
								.activityType(activityType)
								.distance(ActivityTypeDistanceCategory.valueOfWithDefault(activityType, distance))
								.build());
					}

				}
			}
			if (activityType != null) {
				all.add(RelativeStatistic.create().featureId(featureId).layer(Layer.ACTIVITY_TYPE)
						.activityType(activityType).build());
				if (distance != null) {
					all.add(RelativeStatistic.create().featureId(featureId).layer(Layer.ACTIVITY_TYPE_DISTANCE)
							.activityType(activityType)
							.distance(ActivityTypeDistanceCategory.valueOf(activityType, distance)).build());
				}

			}
		}
		return all.build();

	}

	/**
	 * 
	 * @param hasPersonId
	 * @param hasActivityType
	 * @param hasDistance
	 * @return
	 */
	public static Layer layer(Boolean hasPersonId, Boolean hasActivityType, Boolean hasDistance) {
		hasPersonId = BooleanUtils.isTrue(hasPersonId);
		hasDistance = BooleanUtils.isTrue(hasDistance);
		// activity type is implied if distance is given
		hasActivityType = BooleanUtils.isTrue(hasActivityType) || hasDistance;

		Layer autoLayer;
		if (hasActivityType) {
			if (hasPersonId) {

				if (hasDistance) {
					autoLayer = Layer.PERSON_ACTIVITY_TYPE_DISTANCE;
				} else {
					autoLayer = Layer.PERSON_ACTIVITY_TYPE;
				}
			} else {
				if (hasDistance) {
					autoLayer = Layer.ACTIVITY_TYPE_DISTANCE;
				} else {
					autoLayer = Layer.ACTIVITY_TYPE;
				}
			}
		} else {
			if (hasPersonId) {
				autoLayer = Layer.PERSON;
			} else {
				autoLayer = Layer.ALL;
			}
		}
		return autoLayer;
	}

}
