package com.aawhere.activity;

import java.io.Serializable;
import java.util.Iterator;
import java.util.SortedSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections4.CollectionUtils;

import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.ObjectBuilder;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Range;

/**
 * Provides a transportable result of {@link RelativeStatistic} objects grouped into
 * {@link #activityStats} and {@link #personStats}.
 * 
 * @author aroller
 * 
 * @param <I>
 */
public class RelativeStatsActivityStatsAggregator<I extends Identifier<?, ?>>
		implements Serializable {

	private static final long serialVersionUID = -7565807504281968385L;

	@Nonnull
	private I featureId;

	/**
	 * The non-personal {@link ActivityStats}.
	 * 
	 */
	@Nullable
	private ActivityStats activityStats;

	/**
	 * The {@link ActivityStats} whos numbers represent the number of unique persons that created
	 * the {@link #activityStats()}.
	 * 
	 */
	@Nullable
	private ActivityStats personStats;

	/**
	 * Used to construct all instances of RelativeStatsActivityStatsAggregator.
	 */
	public static class Builder<I extends Identifier<?, ?>>
			extends ObjectBuilder<RelativeStatsActivityStatsAggregator<I>> {

		private Iterator<RelativeStatistic> statsIterator;

		public Builder() {
			super(new RelativeStatsActivityStatsAggregator<I>());
		}

		/**
		 * Extracts all that is possible from the stream of {@link RelativeStatistic} that has an
		 * appropriate spot in {@link ActivityStats}. The left being the activity stats that are
		 * related, the right being the number of people involved. Person stats are optional so if
		 * they aren't provided they will be null
		 * 
		 * @param stats
		 * @return
		 */
		private void activityStats(RelativeStatsActivityStatsAggregator<I> aggregator) {
			ImmutableListMultimap<Layer, RelativeStatistic> statsByLayer = Multimaps.index(	this.statsIterator,
																							RelativeStatsUtil
																									.layerFunction());
			publicStats(aggregator, statsByLayer);
			activityStatsForPersons(statsByLayer, aggregator);
		}

		private void publicStats(RelativeStatsActivityStatsAggregator<I> aggregator,
				ImmutableListMultimap<Layer, RelativeStatistic> statsByLayer) {
			ImmutableList<RelativeStatistic> allLayerStats = statsByLayer.get(Layer.ALL);
			if (!allLayerStats.isEmpty()) {
				InvalidArgumentException.range(Range.singleton(1), allLayerStats.size());

				ActivityStats.Builder builder = ActivityStats.create();
				// assign the total first since it is used in the others
				Integer total = allLayerStats.iterator().next().count();
				builder.total(total);
				SortedSet<ActivityTypeStatistic> activityTypeStats = RelativeStatsUtil.activityTypeStats(statsByLayer
						.get(Layer.ACTIVITY_TYPE), total);
				if (CollectionUtils.isNotEmpty(activityTypeStats)) {
					builder.activityTypeStats(activityTypeStats);
					SortedSet<ActivityTypeDistanceCategoryStatistic> activityTypeDistanceStats = RelativeStatsUtil
							.activityTypeDistanceStats(	statsByLayer.get(Layer.ACTIVITY_TYPE_DISTANCE),
														activityTypeStats,
														total);
					if (CollectionUtils.isNotEmpty(activityTypeDistanceStats)) {
						builder.distanceStats(activityTypeDistanceStats);
					}
				}
				aggregator.activityStats = builder.build();
			}
		}

		/**
		 * Inspects the given map of relative statistics to gather the total number of people as
		 * indicated the the {@link Layer#PERSON} and {@link Layer#PERSON_ACTIVITY_TYPE}.
		 * 
		 * @param statsByLayer
		 * @return
		 */
		@Nullable
		public void activityStatsForPersons(ImmutableListMultimap<Layer, RelativeStatistic> statsByLayer,
				RelativeStatsActivityStatsAggregator<I> aggregator) {
			ImmutableList<RelativeStatistic> personStats = statsByLayer.get(Layer.PERSON);
			if (CollectionUtils.isNotEmpty(personStats)) {
				ActivityStats.Builder activityStats = ActivityStats.create();
				// each relative stat represents the count for a single person
				// counting each represents the total number of people
				int numberOfPersons = personStats.size();
				activityStats.total(numberOfPersons);

				ImmutableList<RelativeStatistic> personActivityTypeStats = statsByLayer.get(Layer.PERSON_ACTIVITY_TYPE);
				if (CollectionUtils.isNotEmpty(personActivityTypeStats)) {
					ImmutableListMultimap<ActivityType, RelativeStatistic> personsForActivityType = Multimaps
							.index(personActivityTypeStats, RelativeStatsUtil.activityTypeFunction());
					ImmutableSet<ActivityType> activityTypes = personsForActivityType.keySet();
					// for each activity type
					ImmutableSortedSet.Builder<ActivityTypeStatistic> activityTypeStats = ImmutableSortedSet
							.naturalOrder();
					for (ActivityType activityType : activityTypes) {
						ImmutableList<RelativeStatistic> persons = personsForActivityType.get(activityType);
						int numberOfPersonsForActivityType = persons.size();
						ActivityTypeStatistic activityTypeStatistic = ActivityTypeStatistic.create()
								.category(activityType).count(numberOfPersonsForActivityType).total(numberOfPersons)
								.build();
						activityTypeStats.add(activityTypeStatistic);
					}
					activityStats
							.activityTypeStats(ActivityUtil.bikingCombined(	activityTypeStats.build(),
																			ActivityUtil.MIN_ACTIVITY_TYPE_PERCENT));
				}
				aggregator.personStats = activityStats.build();
			}
		}

		/** @see #featureId */
		public Builder<I> featureId(I featureId) {
			building.featureId = featureId;
			return this;
		}

		/** @see #statsIterator */
		public Builder<I> statsIterator(Iterator<RelativeStatistic> statsIterator) {
			this.statsIterator = statsIterator;
			return this;
		}

		@Override
		protected void validate() {
			Assertion.exceptions().notNull("statsIterator", this.statsIterator);

			super.validate();
		}

		@Override
		public RelativeStatsActivityStatsAggregator<I> build() {
			RelativeStatsActivityStatsAggregator<I> built = super.build();
			activityStats(built);
			return built;
		}

	}// end Builder

	public static <I extends Identifier<?, ?>> Builder<I> create(I featureId) {
		return new Builder<I>().featureId(featureId);
	}

	/** Use {@link Builder} to construct RelativeStatsActivityStatsAggregator */
	private RelativeStatsActivityStatsAggregator() {
	}

	public ActivityStats activityStats() {
		return activityStats;
	}

	public ActivityStats personStats() {
		return personStats;
	}

	/** @see #featureId */
	public I featureId() {
		return this.featureId;
	}

	@Override
	public String toString() {
		return this.featureId.toString();
	}

	public static <I extends Identifier<?, ?>> Function<RelativeStatsActivityStatsAggregator<I>, I> idFunction() {
		return new Function<RelativeStatsActivityStatsAggregator<I>, I>() {
			@Override
			@Nullable
			public I apply(@Nullable RelativeStatsActivityStatsAggregator<I> aggregator) {
				return (aggregator != null) ? aggregator.featureId() : null;
			}

			@Override
			public String toString() {
				return "IdFunction";
			}
		};
	}
}
