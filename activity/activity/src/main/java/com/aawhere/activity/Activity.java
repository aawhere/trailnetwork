/**
 *
 */
package com.aawhere.activity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationWebApiUri;
import com.aawhere.app.Device;
import com.aawhere.app.IdentityHistory;
import com.aawhere.app.Installation;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountMessage;
import com.aawhere.app.account.AccountProvider;
import com.aawhere.collections.ListUtilsExtended;
import com.aawhere.field.DictionaryIndex;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.AnnotatedDictionary;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.EmbeddedDictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.IdentifierToIdentifiableXmlAdapter;
import com.aawhere.identity.IdentityManager;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxProvider;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.person.Person;
import com.aawhere.person.PersonFromAccountIdXmlAdapter;
import com.aawhere.person.PersonMessage;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackSummaryMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Function;
import com.google.inject.Inject;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;
import com.googlecode.objectify.condition.IfNull;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * An single logical event that has a start and finish (hence duration) and is typically a single
 * activity type.
 * 
 * @author Aaron Roller
 * 
 */
@Cache
@Unindex
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Dictionary(domain = Activity.FIELD.DOMAIN, messages = ActivityMessage.class, indexes = {
		Activity.SpatialBufferIndex.class, Activity.DistanceSpatialBufferIndex.class,
		Activity.AccountStartTimeIndex.class, Activity.StartTimeIndex.class })
@ApiModel(description = ActivityMessage.Doc.FULL_DESCRIPTION)
public class Activity
		extends StringBaseEntity<Activity, ActivityId>
		implements AnnotatedDictionary, SelfIdentifyingEntity, BoundingBoxProvider {

	private static final long serialVersionUID = 6008701992725945079L;

	/**
	 * Redundant declaration from {@link #applicationActivityId}, but necessary for filtering
	 * activities by provider without having to navigate into history and matching the standard set
	 * by {@link ActivityReferenceIds}.
	 * 
	 */
	@Field(key = FIELD.APPLICATION_KEY)
	@XmlElement(name = FIELD.APPLICATION_KEY)
	@ApiModelProperty(required = true, allowableValues = KnownApplication.Doc.ALLOWABLE_KEYS)
	private ApplicationKey applicationKey;

	/**
	 * Free-form notes about this Activity. This does not represent comments related to the
	 * activity, but rather a description from the {@link #user}. An empty description is provided
	 * if no description is provided from the user.
	 */
	@XmlElement
	@com.aawhere.field.annotation.Field
	@ApiModelProperty(required = false, value = "Comments or description provided by the Account at the Application.")
	private String description;

	/**
	 * Provides the history of how this Activity was created and where it has visited along the way
	 * to get to this system.
	 * 
	 * This will contain the visit to this application, the previous place where this application
	 * retrieved it from following it all the way back to creation, typically by a device like a GPS
	 * or phone.
	 * 
	 * TN-344 removed this from the datastore.
	 */
	@XmlElement
	@IgnoreSave
	@ApiModelProperty(hidden = true)
	private IdentityHistory history;

	/**
	 * TN-344 moved this out of history and into it's own spot for safe keeping.
	 * 
	 */
	@Nullable
	@XmlElement
	@ApiModelProperty(required = false, value = "Describes the type of device that recorded this activity.")
	private Device device;

	/**
	 * The displayable name of the activity as perceived by the owner. This is a message, rather
	 * than a string to support a default, localized, name.
	 */
	@XmlElement
	@Field(key = FIELD.NAME)
	@ApiModelProperty(required = false, value = "The brief title given by the Account at the Application.")
	private Message name;

	/**
	 * Identifies the type of activity represented by this activity.
	 */
	@XmlElement(name = FIELD.ACTIVITY_TYPE)
	@Field(key = FIELD.ACTIVITY_TYPE, dataType = FieldDataType.ATOM)
	@ApiModelProperty(required = true, value = ActivityMessage.Type.Doc.DESCRIPTION)
	private ActivityType activityType;

	/**
	 * Totals and averages describing this Activity. At minimum, this will contain the
	 * start/stop/duration which all activities require.
	 * 
	 * 
	 * @since 9
	 */
	@EmbeddedDictionary(flatten = false)
	@XmlElement(name = FIELD.TRACK_SUMMARY)
	@Nonnull
	@Field(key = FIELD.TRACK_SUMMARY)
	@ApiModelProperty(required = true, value = TrackSummaryMessage.Doc.DESCRIPTION)
	private TrackSummary trackSummary;

	/**
	 * The actual user account that created this activity. This differs from person in that this
	 * will never change. Person is derived from many accounts and Person may be updated.
	 */
	@Nonnull
	@Index
	@XmlElement(name = FIELD.ACCOUNT_ID)
	@Field(key = FIELD.ACCOUNT_ID, indexedFor = "TN-545", xmlAdapter = Activity.AccountIdXmlAdapter.class)
	@ApiModelProperty(required = true, value = "The user owner of this Activity at an Application.")
	private AccountId accountId;

	@IgnoreSave
	private AccountId account;

	/**
	 * If an activity is gone that means it has been deleted or no longer active at a remote
	 * resource. We should remove this from participating in the system.
	 */
	@Nullable
	@XmlAttribute
	@IgnoreSave(IfNull.class)
	@ApiModelProperty(required = false, value = "Indicates if the Activity is no longer available.")
	private Boolean gone;

	/** Use {@link Builder} to construct Activity */
	private Activity() {
	}

	/**
	 * @return the description
	 */
	@Nonnull
	public String getDescription() {
		return StringUtils.defaultString(description);
	}

	/**
	 * 
	 * @return the activity type
	 */
	public ActivityType getActivityType() {
		return activityType;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM, indexedFor = "Repository.load()")
	@Override
	public ActivityId getId() {
		return super.getId();
	}

	/**
	 * @return the device
	 */
	@Nullable
	public Installation getOrigin() {
		return ListUtilsExtended.firstOrNull(this.history.getInstallations());
	}

	/**
	 * @return the history
	 */
	public IdentityHistory getHistory() {
		return this.history;
	}

	/**
	 * @return the name
	 */
	@Nonnull
	public Message getName() {
		return this.name;
	}

	/**
	 * @return the gone
	 */
	public Boolean isGone() {
		return BooleanUtils.isTrue(this.gone);
	}

	@XmlElement(name = FIELD.APPLICATION_REFERENCE)
	/** Specifically provided to supply the url with the xml transformation, but that is all */
	@XmlJavaTypeAdapter(ApplicationActivityReferenceXmlAdapter.class)
	public ActivityId getApplicationReference() {
		return id();
	}

	@XmlJavaTypeAdapter(ActivityApplicationXmlAdapter.class)
	@XmlElement(name = Application.FIELD.DOMAIN)
	public ApplicationKey getApplication() {
		return this.applicationKey;
	}

	/**
	 * @return the trackSummary
	 */
	@Nonnull
	public TrackSummary getTrackSummary() {
		return this.trackSummary;
	}

	/**
	 * @return the device
	 */
	public Device getDevice() {
		return this.device;
	}

	/**
	 * @return the account
	 */
	public AccountId getAccountId() {
		return this.accountId;
	}

	/**
	 * Special XmlAdapter method for loading the account
	 */
	@XmlElement(name = FIELD.ACCOUNT)
	@XmlJavaTypeAdapter(Activity.AccountIdXmlAdapter.class)
	@ApiModelProperty(value = "The full Account represented by the AccountId.", dataType = AccountMessage.Doc.DATA_TYPE)
	private
			AccountId getAccount() {
		return this.getAccountId();
	}

	/** provided for client easy of knowing which person completed an activity. */
	@Field(key = FIELD.PERSON, xmlAdapter = PersonFromAccountIdXmlAdapter.class)
	@XmlElement(name = FIELD.PERSON)
	@XmlJavaTypeAdapter(PersonFromAccountIdXmlAdapter.class)
	@ApiModelProperty(required = false, value = "", dataType = PersonMessage.Doc.DATA_TYPE)
	public AccountId getPerson() {
		return this.accountId;
	}

	@Override
	protected void prePersist() {
		super.prePersist();
		// don't persist the default message.
		this.name = If.equal(this.name, ActivityMessage.UNKNOWN).use(null);
		this.trackSummary.prePersist();
	}

	@Override
	protected void postLoad() {
		super.postLoad();
		// set the default if null
		this.name = If.nil(this.name).use(ActivityMessage.UNKNOWN);
		// TN-904, may remove after all activities are touched.
		if (this.accountId == null && this.account != null) {
			this.accountId = this.account;
		}
	}

	/**
	 * Used to construct all instances of Activity.
	 */
	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<Activity, Builder, ActivityId> {

		public Builder() {
			super(new Activity());
		}

		Builder(Activity mutatee) {
			super(mutatee);
		}

		public Builder setDescription(String description) {
			building.description = description;
			return this;
		}

		public Builder setName(Message name) {
			building.name = name;
			return this;

		}

		public Builder setGone(Boolean gone) {
			building.gone = gone;
			return this;
		}

		/**
		 * Provides validation against the {@link ActivityId} before assigning.
		 * 
		 */
		@Override
		public Builder setId(ActivityId activityId) {
			activityId = ActivityIdentifierUtil.validateFormat(activityId);
			super.setId(activityId);
			building.applicationKey = activityId.getApplicationKey();

			return this;
		}

		@Deprecated
		public Builder setApplicationActivityId(ApplicationActivityId appId) {
			this.setId(new ActivityId(appId));
			return this;
		}

		public Builder setActivityType(ActivityType activityType) {
			building.activityType = activityType;
			return this;
		}

		public Builder setHistory(IdentityHistory history) {
			building.history = history;
			return this;
		}

		/**
		 * @param device
		 */
		public Builder setDevice(Device device) {
			building.device = device;

			return this;
		}

		Builder appendToHistory(Installation installation) {
			if (building.history != null) {
				building.history = new IdentityHistory.Builder(building.history).add(installation).build();
			}
			return this;
		}

		public Builder setTrackSummary(TrackSummary trackSummary) {
			building.trackSummary = trackSummary;
			return this;
		}

		public Builder setAccount(Account account) {
			return setAccountId(account.getId());
		}

		public Builder setAccountId(AccountId accountId) {
			building.accountId = accountId;
			return this;
		}

		@Override
		public void validate() {
			Assertion.assertNotNull("summary", building.trackSummary);
			Assertion.exceptions().notNull("accountId", building.accountId);

			// TODO:make person nonnull TN-134 after removing GC specific code
			// Assertion.assertNotNull("person", building.person);
			// every activity in our system came from somewhere.

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.BaseEntity.Builder#build()
		 */
		@Override
		public Activity build() {
			building.name = If.nil(building.name).use(ActivityMessage.UNKNOWN);
			if (building.history != null && building.device == null) {
				setDevice(building.history.getDevice());
			}
			return super.build();
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/* package */static Builder mutate(Activity activity) {
		return new Builder(activity);
	}

	@XmlRootElement(name = FIELD.FIELDS_NAME)
	@XmlType(namespace = XmlNamespace.API_VALUE, name = FIELD.FIELDS_NAME)
	@XmlTransient
	public static final class FIELD
			extends BaseEntity.BASE_FIELD {
		public static final String ACTIVITY_TYPE = "activityType";
		static final String FIELDS_NAME = "activityFields";
		@XmlAttribute(name = FIELD.DOMAIN)
		public final static String DOMAIN = "activity";
		public static final String ACTIVITY_ID = ID;
		@XmlAttribute(name = FIELD.NAME)
		public final static String NAME = "name";
		@XmlAttribute(name = FIELD.TRACK_SUMMARY)
		public static final String TRACK_SUMMARY = TrackSummary.FIELD.DOMAIN;
		public static final String ACCOUNT = Account.FIELD.DOMAIN;
		public static final String PERSON = Person.FIELD.DOMAIN;
		public static final String ACCOUNT_ID = Account.FIELD.ACCOUNT_ID;
		public static final String APPLICATION_REFERENCE = ApplicationWebApiUri.APPLICATION_REFERENCE;

		@Deprecated
		public final static String APPLICATION_ACTIVITY_ID = ACTIVITY_ID;
		@XmlAttribute(name = FIELD.APPLICATION_KEY)
		public static final String APPLICATION_KEY = ActivityWebApiUri.APPLICATION_KEY;

		public static final class KEY {
			public static final FieldKey ACTIVITY_ID = new FieldKey(DOMAIN, FIELD.ACTIVITY_ID);
			public final static FieldKey NAME = new FieldKey(DOMAIN, FIELD.NAME);
			public final static FieldKey TRACK_SUMMARY = new FieldKey(DOMAIN, FIELD.TRACK_SUMMARY);
			@Deprecated
			public final static FieldKey APPLICATION_ACTIVITY_ID = new FieldKey(DOMAIN, FIELD.APPLICATION_ACTIVITY_ID);
			public static final FieldKey APPLICATION_KEY = new FieldKey(DOMAIN, FIELD.APPLICATION_KEY);
			public final static FieldKey DATE_CREATED = new FieldKey(DOMAIN, FIELD.DATE_CREATED);

			public final static FieldKey TRACK_SUMMARY_START_TIME = new FieldKey(DOMAIN, FIELD.TRACK_SUMMARY,
					TrackSummary.FIELD.KEY.START_TIME);
			public final static FieldKey TRACK_SUMMARY_SPATIAL_BUFFER_CELLS = new FieldKey(DOMAIN,
					TrackSummary.FIELD.KEY.TRACK_SUMMARY_SPATIAL_BUFFER_CELLS.toString());
			public final static FieldKey TRACK_SUMMARY_SPATIAL_BUFFER_MAX_RESOLUTION = new FieldKey(DOMAIN,
					TrackSummary.FIELD.KEY.TRACK_SUMMARY_SPATIAL_BUFFER_MAX_RESOLUTION.toString());
			public final static FieldKey DISTANCE = new FieldKey(DOMAIN,
					TrackSummary.FIELD.KEY.DISTANCE.getAbsoluteKey());
			public final static FieldKey ACCOUNT_ID = new FieldKey(DOMAIN, FIELD.ACCOUNT_ID);
		}

		public static final class OPTIONS {
			public static final String ACCOUNT = new FieldKey(DOMAIN, FIELD.ACCOUNT).getAbsoluteKey();
			public static final String APPLICATION_REFERENCE = new FieldKey(DOMAIN, FIELD.APPLICATION_REFERENCE)
					.getAbsoluteKey();
			public static final String PERSON = new FieldKey(DOMAIN, FIELD.PERSON).getAbsoluteKey();
			public static final String APPLICATION = new FieldKey(FIELD.DOMAIN, Application.FIELD.DOMAIN)
					.getAbsoluteKey();
		}
	}

	public static class SpatialBufferIndex
			extends DictionaryIndex {
		public SpatialBufferIndex() {
			super(FIELD.KEY.TRACK_SUMMARY_SPATIAL_BUFFER_MAX_RESOLUTION, FIELD.KEY.TRACK_SUMMARY_SPATIAL_BUFFER_CELLS);
		}
	}

	/** TN-679 show the most recent activities for a person or account. */
	public static class AccountStartTimeIndex
			extends DictionaryIndex {
		public AccountStartTimeIndex() {
			super(FIELD.KEY.TRACK_SUMMARY_START_TIME, FIELD.KEY.ACCOUNT_ID);
		}
	}

	/**
	 * TN-679 individual index required for complex
	 * 
	 * @see AccountStartTimeIndex
	 */
	public static class StartTimeIndex
			extends DictionaryIndex {
		public StartTimeIndex() {
			super(FIELD.KEY.TRACK_SUMMARY_START_TIME);
		}
	}

	/**
	 * @see TN-310
	 */
	public static class DistanceSpatialBufferIndex
			extends DictionaryIndex {
		public DistanceSpatialBufferIndex() {
			super(FIELD.KEY.DISTANCE, FIELD.KEY.TRACK_SUMMARY_SPATIAL_BUFFER_CELLS,
					FIELD.KEY.TRACK_SUMMARY_SPATIAL_BUFFER_MAX_RESOLUTION);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.BoundingBoxProvider#bounds()
	 */
	@Override
	public BoundingBox boundingBox() {
		return getTrackSummary().boundingBox();
	}

	public static class AccountIdXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<Account, AccountId> {
		final private IdentityManager identityManager;

		public AccountIdXmlAdapter() {
			this.identityManager = null;
		}

		public AccountIdXmlAdapter(Function<AccountId, Account> provider, IdentityManager identityManager) {
			super(provider);
			this.identityManager = identityManager;
		}

		@Inject
		public AccountIdXmlAdapter(AccountProvider provider, IdentityManager identityManager) {
			super(provider.idFunction());
			this.identityManager = identityManager;
		}

		@Override
		public Account marshal(AccountId id) throws Exception {
			if (identityManager != null) {
				return identityManager.enforceAccess(super.marshal(id));
			} else {
				return super.marshal(id);
			}
		}
	}

}
