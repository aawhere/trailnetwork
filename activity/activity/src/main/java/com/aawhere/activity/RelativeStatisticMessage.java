package com.aawhere.activity;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Provides common personalization for relative stats. This doesn't provide the entire message since
 * that is relative to the {@link RelativeStatsUtil#featureIdValue(RelativeStatistic)}.
 * 
 * @author aroller
 * 
 */
public enum RelativeStatisticMessage implements Message {

	;

	private ParamKey[] parameterKeys;
	private String message;

	private RelativeStatisticMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		PERSON;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
