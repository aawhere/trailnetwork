/**
 * 
 */
package com.aawhere.activity;

/**
 * Uniquely identifies an {@link Activity} in the system. This may be an {@link ActivityId} or may
 * be something that can be converted into an {@link ActivityId}.
 * 
 * @author aroller
 * 
 */
public interface ActivityIdentifier {

	/**
	 * This will return the raw identifier in it's string form that may be used to identify the
	 * underlying id.
	 * 
	 * @return the string version of the underlying #getValue() method.
	 */
	@Override
	public String toString();
}
