/**
 *
 */
package com.aawhere.activity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxProvider;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.Iterables;

/**
 * Represents an ordered collection of Activities
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class Activities
		extends BaseFilterEntities<Activity>
		implements BoundingBoxProvider {

	@XmlElement(name = BoundingBox.FIELD.NAME)
	private BoundingBox bounds;

	/**
	 * Use Builder to construct Activities
	 */
	private Activities() {
		super();
	}

	@XmlElement(name = "activity")
	@Override
	public List<Activity> getCollection() {
		return super.getCollection();
	}

	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, Activity, Activities> {

		public Builder() {
			super(new Activities());
		}

		Builder(Activities activities) {
			super(activities);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public Activities build() {
			Activities built = super.build();
			built.bounds = BoundingBoxUtil
					.combined(Iterables.transform(built.all(), ActivityUtil.boundingBoxFunction()));
			return built;
		}

	}

	public static Builder create() {
		return new Builder();
	}

	static Builder mutate(Activities activities) {
		return new Builder(activities);
	}

	/**
	 * @return the bounds
	 */
	public BoundingBox boundingBox() {
		return this.bounds;
	}

	@XmlTransient
	public static final class FIELD {
		public static final String ACTIVITIES = ActivityWebApiUri.ACTIVITIES;
		public static final String BOUNDS = GeoWebApiUri.BOUNDS;
	}
}
