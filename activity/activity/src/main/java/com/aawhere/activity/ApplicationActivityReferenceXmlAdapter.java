/**
 * 
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.xml.UrlDisplay;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Transforms an {@link ActivityId} into it's destination url. This is optional so it handles null
 * checks.
 * 
 * 
 * @author aroller
 * 
 */
@Singleton
public class ApplicationActivityReferenceXmlAdapter
		extends XmlAdapter<UrlDisplay, ActivityId> {

	public ApplicationActivityUrlProvider provider;

	/**
	 * 
	 */
	ApplicationActivityReferenceXmlAdapter() {
	}

	/**
	 * @param provider
	 */
	@Inject
	public ApplicationActivityReferenceXmlAdapter(ApplicationActivityUrlProvider provider) {
		super();
		this.provider = provider;
	}

	@Override
	public ActivityId unmarshal(UrlDisplay v) throws Exception {
		return new ActivityId();
	}

	@Override
	public UrlDisplay marshal(ActivityId v) throws Exception {
		if (provider == null || v == null) {
			return null;
		}
		UrlDisplay xml = new UrlDisplay(provider.applicationUrl(v));
		return xml;
	}

}
