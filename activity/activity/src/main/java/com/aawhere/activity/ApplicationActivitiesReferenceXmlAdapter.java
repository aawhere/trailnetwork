/**
 * 
 */
package com.aawhere.activity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.xml.UrlDisplay;
import com.aawhere.xml.XmlNamespace;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Displays multiple application references including the activity id to reference which activity
 * the reference belongs to.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ApplicationActivitiesReferenceXmlAdapter
		extends
		XmlAdapter<ApplicationActivitiesReferenceXmlAdapter.ActivityApplicationReferences, Iterable<ActivityId>> {

	final private ApplicationActivityReferenceXmlAdapter adapter;

	/**
	 * 
	 */
	public ApplicationActivitiesReferenceXmlAdapter() {
		this.adapter = null;
	}

	@Inject
	public ApplicationActivitiesReferenceXmlAdapter(ApplicationActivityReferenceXmlAdapter adapter) {
		this.adapter = adapter;
	}

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.NONE)
	public static class ActivityApplicationReferences {
		@XmlElement(name = Activity.FIELD.APPLICATION_REFERENCE)
		List<ActivityUrlXml> applicationReferences = new ArrayList<>();
	}

	/**
	 * provides the {@link UrlDisplay} with the embedded activity id to identify which url it
	 * belongs to.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE, name = Activity.FIELD.APPLICATION_REFERENCE)
	@XmlAccessorType(XmlAccessType.NONE)
	public static class ActivityUrlXml
			extends UrlDisplay {

		/**
		 * 
		 */
		public ActivityUrlXml() {
			this.activityId = null;
		}

		public ActivityUrlXml(UrlDisplay urlXml, ActivityId id) {
			super(urlXml);
			this.activityId = id;
		}

		@XmlElement(name = ActivityWebApiUri.ACTIVITY_ID)
		final ActivityId activityId;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Iterable<ActivityId> unmarshal(ActivityApplicationReferences references) throws Exception {
		if (references == null) {
			return null;
		}

		List<ActivityId> ids = new ArrayList<ActivityId>();
		for (ActivityUrlXml ref : references.applicationReferences) {
			ids.add(ref.activityId);
		}
		return ids;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public ActivityApplicationReferences marshal(Iterable<ActivityId> ids) throws Exception {
		if (ids == null || this.adapter == null) {
			return null;
		}
		ActivityApplicationReferences references = new ActivityApplicationReferences();
		for (ActivityId activityId : ids) {
			ActivityUrlXml reference = new ActivityUrlXml(this.adapter.marshal(activityId), activityId);
			references.applicationReferences.add(reference);
		}
		return references;
	}
}
