/**
 *
 */
package com.aawhere.activity;

import com.aawhere.app.account.AccountId;
import com.aawhere.field.FieldDictionary;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.track.TrackSummaryFilterBuilder;

import com.google.common.collect.Lists;

/**
 * Provides convenient methods for building a filter for Activities.
 * 
 * @author Brian Chapman
 * 
 */
public class ActivityFilterBuilder
		extends TrackSummaryFilterBuilder<ActivityFilterBuilder> {

	public static ActivityFilterBuilder create(FieldDictionary dictionary) {
		return new ActivityFilterBuilder(dictionary);
	}

	/** Use {@link Builder} to construct ActivityFilterBuilder */
	public ActivityFilterBuilder(FieldDictionary dictionary) {
		super(dictionary.getDefinition(Activity.FIELD.TRACK_SUMMARY).getNested());
	}

	protected ActivityFilterBuilder(Filter mutatee, FieldDictionary dictionary) {
		super(mutatee, dictionary.getDefinition(Activity.FIELD.TRACK_SUMMARY).getNested());
	}

	public ActivityFilterBuilder activityId(ActivityId activityId) {
		addCondition(new FilterCondition.Builder<ActivityId>().field(Activity.FIELD.KEY.ACTIVITY_ID)
				.operator(FilterOperator.EQUALS).value(activityId).build());
		return this;
	}

	/**
	 * @deprecated use {@link #clone(Filter, FieldDictionary)}
	 * @param filter
	 * @param dictionary
	 * @return
	 */
	protected static ActivityFilterBuilder mutate(Filter filter, FieldDictionary dictionary) {
		return new ActivityFilterBuilder(filter, dictionary);
	}

	protected static ActivityFilterBuilder clone(Filter filter, FieldDictionary dictionary) {
		return new ActivityFilterBuilder(Filter.cloneForBuilder(filter), dictionary);
	}

	/**
	 * Filters activities that match an account id.
	 * 
	 * @param accountId
	 */
	public ActivityFilterBuilder accountId(AccountId... accountIds) {
		return accountId(Lists.newArrayList(accountIds));
	}

	public ActivityFilterBuilder accountId(Iterable<AccountId> accountIds) {
		return addCondition(new FilterCondition.Builder<Iterable<AccountId>>().field(Activity.FIELD.KEY.ACCOUNT_ID)
				.operator(FilterOperator.IN).value(accountIds).build());
	}

	public ActivityFilterBuilder orderByMostRecentStartTime() {
		return orderBy(Activity.FIELD.KEY.TRACK_SUMMARY_START_TIME, Direction.DESCENDING);
	}
}
