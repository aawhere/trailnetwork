/**
 * 
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.xml.XmlNamespace;

/**
 * A super category of {@link ActivityType} grouping similar activities together that use the same
 * vehicle of performing the activity.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public enum TransportMode implements Message {

	/** Any activity that uses the feet to get around regardless of speed. */
	FOOT("Foot"),
	/**
	 * Any type of bike on any terrain.
	 * */
	BIKE("Bike"),
	/** Any motorized vehicle that is on land. */
	AUTO("Auto"),
	/** Any motorized or human powered vehicle on water. */
	BOAT("Boat"),
	/** Anything that flies. */
	PLANE("Plane");

	private ParamKey[] parameterKeys;
	@XmlElement(name = "name")
	private String message;

	private TransportMode(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** EXPLAIN_PARAM_HERE */
		FIRST_PARAM;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
