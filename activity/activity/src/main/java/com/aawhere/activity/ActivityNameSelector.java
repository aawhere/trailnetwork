package com.aawhere.activity;

import static com.aawhere.lang.string.StringUtilsExtended.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.EndpointSentenceParser;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessageUtil;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.collect.Ordering;

/**
 * Determines the best name for from the activities contained in the supplied Leaderboards.
 * 
 * 
 * @author Brian Chapman
 * @author Aaron Roller
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class ActivityNameSelector {

	/**
	 * Used to construct all instances of enclosing_type.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<ActivityNameSelector> {

		private Iterator<String> namesGiven;

		public Builder() {
			super(new ActivityNameSelector());
		}

		/** The best way to provide the names directly. */
		public Builder names(Iterable<String> names) {
			this.namesGiven = names.iterator();
			return this;
		}

		public Builder names(Iterator<String> names) {
			this.namesGiven = names;
			return this;
		}

		public Builder activities(Iterable<Activity> activities) {
			Iterable<Message> messages = Iterables.transform(activities, ActivityUtil.nameFunction());
			this.namesGiven = Iterables.transform(messages, Functions.toStringFunction()).iterator();
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("namesGiven", this.namesGiven);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ActivityNameSelector build() {
			ActivityNameSelector built = super.build();

			// TN-917 moved the filtering of unknown outside of this...bad activity names should be
			// filtered before giving here
			// TN-482 this is a bit aggressive since other important words may follow
			Iterator<String> cleansed = Iterators.transform(namesGiven, suffixDropper("with"));
			cleansed = Iterators.transform(cleansed, suffixDropper("w/"));
			cleansed = Iterators.transform(cleansed, replaceFunction("-", SPACE));
			cleansed = Iterators.transform(cleansed, replaceFunction(",", SPACE));
			cleansed = Iterators.transform(cleansed, replaceFunction(".", SPACE));
			// every other character besides letters,numbers and spaces gets removed
			cleansed = Iterators.transform(cleansed, replaceAllFunction("[^A-Za-z0-9\\s]", StringUtils.EMPTY));
			cleansed = Iterators.transform(	Iterators.transform(cleansed, splitByCharacterTypeCamcelCaseFunction()),
											joinFunction(SPACE));
			cleansed = Iterators.transform(cleansed, capitalizeFullyFunction());
			cleansed = Iterators.transform(cleansed, sentenceSpacingFunction());
			LinkedList<String> activityNameCandidates = new LinkedList<>();
			LinkedList<String> startCandidates = new LinkedList<>();
			LinkedList<String> finishCandidates = new LinkedList<>();

			List<EndpointSentenceParser> parsers = Lists.newArrayList();
			while (cleansed.hasNext()) {
				String phrase = cleansed.next();
				EndpointSentenceParser parser = EndpointSentenceParser.create(phrase).build();
				parsers.add(parser);
				if (parser.hasRemainder()) {
					activityNameCandidates.add(parser.remainder());
				} else {
					// if fully stripped by to/from then post the whole thing
					// think commute "Home to Work"
					activityNameCandidates.add(phrase);
				}
				if (parser.hasFrom()) {
					startCandidates.add(parser.from());
				}
				if (parser.hasTo()) {
					finishCandidates.add(parser.to());
				}
			}
			built.endpointSentenceParsers = ImmutableList.copyOf(parsers);
			built.ranker = StringSimilarityRanker.create().addStrings(activityNameCandidates).build();
			built.bestNames = ImmutableList.copyOf(Lists.transform(	built.ranker.topPhrases(),
																	MessageUtil.stringToMessageFunction()));

			built.startRanker = StringSimilarityRanker.create().addStrings(startCandidates).build();
			built.bestStartNames = ImmutableList.copyOf(Lists.transform(built.startRanker.topPhrases(),
																		MessageUtil.stringToMessageFunction()));
			built.finishRanker = StringSimilarityRanker.create().addStrings(finishCandidates).build();
			built.bestFinishNames = ImmutableList.copyOf(Lists.transform(	built.finishRanker.topPhrases(),
																			MessageUtil.stringToMessageFunction()));
			return built;
		}
	}// end Builder

	/** Use {@link Builder} to construct enclosing_type */
	private ActivityNameSelector() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @see #activityNames()
	 */
	@XmlElement
	private List<? extends Message> bestNames;
	@XmlElement
	private StringSimilarityRanker ranker;
	@XmlElement
	private StringSimilarityRanker startRanker;
	@XmlElement
	private StringSimilarityRanker finishRanker;
	@XmlElement
	private List<? extends Message> bestStartNames;
	@XmlElement
	private List<? extends Message> bestFinishNames;
	@XmlElement
	private List<EndpointSentenceParser> endpointSentenceParsers;

	/** Provides the best name of null if no best name is determined. */
	public Message bestName() {
		return Iterables.getFirst(bestNames, defaultBest(ranker));
	}

	public Boolean hasActivityName() {
		return !this.bestNames.isEmpty();
	}

	public Message bestStartName() {
		return Iterables.getFirst(this.bestStartNames, defaultBest(startRanker));
	}

	public Boolean hasStartName() {
		return !this.bestStartNames.isEmpty();
	}

	public Message bestFinishName() {
		return Iterables.getFirst(this.bestFinishNames, defaultBest(finishRanker));
	}

	public Boolean hasFinishName() {
		return !this.bestFinishNames.isEmpty();
	}

	/**
	 * @return the bestStartNames
	 */
	public List<? extends Message> startNames() {
		return this.bestStartNames;
	}

	/**
	 * @return the bestFinishNames
	 */
	public List<? extends Message> finishNames() {
		return this.bestFinishNames;
	}

	/**
	 * List of best names sorted in descending order (best match to least best match).
	 * 
	 * @return
	 */
	public List<? extends Message> activityNames() {
		return bestNames;
	}

	/**
	 * @return the startRanker
	 */
	public StringSimilarityRanker startRanker() {
		return this.startRanker;
	}

	/**
	 * @return the finishRanker
	 */
	public StringSimilarityRanker finishRanker() {
		return this.finishRanker;
	}

	/**
	 * @return the ranker
	 */
	public StringSimilarityRanker ranker() {
		return this.ranker;
	}

	/**
	 * @return
	 */
	private static Message defaultBest(StringSimilarityRanker ranker) {
		return MessageUtil.message(Iterables.getFirst(ranker.phrases, null), ActivityMessage.UNKNOWN);
	}

	/**
	 * Givin a list of documents, this will split it into phrases and determine the most common
	 * (lowest score) phrase. The most unique phrase has the highest score.
	 * 
	 * TODO: move into commons.
	 * 
	 * @author Brian Chapman
	 * 
	 */
	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.NONE)
	public static class StringSimilarityRanker {

		/**
		 * This is the maximum number of words that a name will be broken down into (using
		 * http://en.wikipedia.org/wiki/N-gram).
		 */
		private static final Integer MAX_PHRASES = 10;

		/**
		 * Used to construct all instances of TfIdfUnitTest.StringSimilarityRanker.
		 */
		public static class Builder
				extends ObjectBuilder<StringSimilarityRanker> {

			public Builder() {
				super(new StringSimilarityRanker());
			}

			/**
			 * Add phrases to the ranker. Duplicate phrases are maintained (not removed).
			 * 
			 * @param input
			 * @return
			 */
			public Builder addStrings(Iterable<String> input) {
				Iterables.addAll(building.phrases, input);
				return this;
			}

			/*
			 * (non-Javadoc)
			 * @see com.aawhere.lang.ObjectBuilder#build()
			 */
			@Override
			public StringSimilarityRanker build() {
				StringSimilarityRanker built = super.build();
				built.scores = new TreeMap<>();

				// count the number of occurrences of each phrase
				Multiset<String> countingSet = HashMultiset.create();
				for (String string : built.phrases) {
					// make a phrase for every word found
					int numberOfWords = numberOfWords(StringUtils.trim(string));
					for (int numberOfWordsPerPhrase = 1; numberOfWordsPerPhrase <= numberOfWords
							&& numberOfWordsPerPhrase < MAX_PHRASES; numberOfWordsPerPhrase++) {
						List<String> ngrams = NaturalLanguageUtils.ngrams(numberOfWordsPerPhrase, string);
						countingSet.addAll(ngrams);
					}
				}

				/**
				 * Indicates the number of matches for those original phrases given. Full phrases
				 * given should have higher value than partial.
				 */
				Multiset<String> exactMatches = HashMultiset.create(built.phrases);

				// map the weighted counts
				for (Entry<String> entry : countingSet.entrySet()) {
					PhraseScore score = new PhraseScore();
					String phrase = entry.getElement();
					int count = entry.getCount();
					int weightedCount = count;
					final int numberOfExactMatches = exactMatches.count(phrase);
					// phrase must show up more than once, otherwise it isn't worth much so drop it
					if (count > 1) {
						// exact matches are preferred if given multiple times
						weightedCount += numberOfExactMatches;
						weightedCount *= phraseValue(phrase);
						score.phrase = phrase;
						score.score = weightedCount;
						// this may return zero if it's an ngram
						score.exactMatchCount = numberOfExactMatches;
						score.matchCount = count;
						built.scores.put(phrase, score);
					}
				}

				// second pass increases left phrases to give a fair playing ground with
				// right phrases that it helped create
				// So if the right key is contained within the left key then give the left key
				// the count that the right key got from the left key
				for (Entry<String> small : countingSet.entrySet()) {
					for (java.util.Map.Entry<String, PhraseScore> large : built.scores.entrySet()) {
						String largePhrase = large.getKey();
						if (largePhrase.contains(small.getElement())) {
							// large phrase may have helped small phrase
							// only help large phrase if it was sent more than once
							int countOfLargePhrase = countingSet.count(largePhrase);
							if (countOfLargePhrase > 1) {
								// muate the original
								final PhraseScore largePhraseScore = large.getValue();
								largePhraseScore.score += countOfLargePhrase;
							}
						}
					}
				}
				// order by highest weighted score
				Ordering<String> orderByScore = Ordering.natural().reverse().onResultOf(Functions.forMap(built.scores));
				built.scores = ImmutableSortedMap.copyOf(built.scores, orderByScore);

				return built;
			}
		}// end Builder

		/** Use {@link Builder} to construct TfIdfUnitTest.StringSimilarityRanker */
		private StringSimilarityRanker() {
		}

		public static Builder create() {
			return new Builder();
		}

		@XmlElement(name = "phrase")
		private List<String> phrases = new ArrayList<>();

		@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
		private SortedMap<String, PhraseScore> scores;

		/**
		 * 
		 * @return
		 */
		public SortedMap<String, PhraseScore> scores() {
			return scores;
		}

		/**
		 * Provides the score associated to the given phrase or null if not part of this ranker.
		 * 
		 * @param phrase
		 * @return
		 */
		public PhraseScore score(String phrase) {
			return scores.get(phrase);
		}

		/**
		 * Returns a list of top phrases ordered from best to least best similarity.
		 * 
		 * @return
		 */
		public List<String> topPhrases() {
			return Lists.newArrayList(scores.keySet());
		}

	}

	/**
	 * Simple class that keeps track of the score and values that helped it create the score. The
	 * phrase is provided for identity and used for equality checks to assist with sorting.
	 * 
	 * The equals, hashscode and compareto use phrase and score for each to ensure consistency.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.NONE)
	public static class PhraseScore
			implements Serializable, Comparable<PhraseScore> {

		private static final long serialVersionUID = -6073352462971047206L;
		@XmlAttribute
		private String phrase;
		@XmlAttribute
		private Integer score;
		/** the total number of times the phrase matches ngrams */
		@XmlAttribute
		private Integer matchCount;
		/** The number of phrases that match each other exactly as given. */
		@XmlAttribute
		private Integer exactMatchCount;

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.phrase == null) ? 0 : this.phrase.hashCode());
			result = prime * result + ((this.score == null) ? 0 : this.score.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PhraseScore other = (PhraseScore) obj;
			if (this.phrase == null) {
				if (other.phrase != null)
					return false;
			} else if (!this.phrase.equals(other.phrase))
				return false;
			if (this.score == null) {
				if (other.score != null)
					return false;
			} else if (!this.score.equals(other.score))
				return false;
			return true;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(PhraseScore that) {
			// phrase comparison is important for consistency with equals
			// if counts are equal, provide sorting by phrase
			if (this.score.equals(that.score)) {
				return this.phrase.compareTo(that.phrase);
			} else {
				return this.score.compareTo(that.score);
			}
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "PhraseScore [phrase=" + this.phrase + ", score=" + this.score + ", matchCount=" + this.matchCount
					+ ", exactMatchCount=" + this.exactMatchCount + "]";
		}

		/**
		 * @return the phrase
		 */
		public String phrase() {
			return this.phrase;
		}

		/**
		 * @return the score
		 */
		public Integer score() {
			return this.score;
		}

		/**
		 * @return the matchCount
		 */
		public Integer matchCount() {
			return this.matchCount;
		}

		/**
		 * @return the exactMatchCount
		 */
		public Integer exactMatchCount() {
			return this.exactMatchCount;
		}

	}

	private static final String WORD_DELIMINATOR = " ";

	public static class NaturalLanguageUtils {

		private static final Integer MIN_PHRASE_LENGTH = 4;
		private static final String WHITESPACE_REGEX = "\\s+";

		public static List<String> ngrams(int n, String str) {

			List<String> ngrams = new ArrayList<String>();
			// Split on white space
			String[] words = str.split(WHITESPACE_REGEX);
			for (int i = 0; i < words.length - n + 1; i++) {
				// Add words together into phrases.
				String gram = concat(words, i, i + n);
				if (!gram.isEmpty() && gram.length() >= MIN_PHRASE_LENGTH) {
					ngrams.add(gram);
				}
			}
			return ngrams;
		}

		public static String concat(String[] words, int start, int end) {
			StringBuilder sb = new StringBuilder();
			for (int i = start; i < end; i++) {
				String word = words[i];
				if (i > start) {
					sb.append(WORD_DELIMINATOR);
				}
				sb.append(word);
			}
			return sb.toString();
		}
	}

	/** Given the dilimeter this will split the remainder of the string folliwng the dilimeter. */
	public static Function<String, String> suffixDropper(final String dilimeter) {
		return new Function<String, String>() {

			@Override
			public String apply(String input) {
				if (input == null) {
					return null;
				}
				String[] strings = input.split(dilimeter);
				return strings[0];
			}
		};

	}

	/**
	 * Provides an estimate of the value of the phrase based on the number of words and the
	 * importance of each word.
	 * 
	 * @param phrase
	 * @return
	 */
	private static int phraseValue(String phrase) {
		// TODO: consider the number of valuable words?
		int value = 0;
		String[] words = words(phrase);
		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			int wordLength = word.length();
			// two letter words don't count
			if (wordLength > 2) {
				// three letter words barely count
				value += 1;
				// four or more and weight it strongly
				// the longer the better
				if (wordLength > 3) {
					value += wordLength;
				}
			}
		}
		return value;
	}

	/**
	 * @param phrase
	 * @return
	 */
	private static int numberOfWords(String phrase) {
		int numberOfWords = words(phrase).length;
		return numberOfWords;
	}

	/**
	 * @param str
	 * @return
	 */
	private static String[] words(String str) {

		return StringUtils.split(str, WORD_DELIMINATOR);
	}

}
