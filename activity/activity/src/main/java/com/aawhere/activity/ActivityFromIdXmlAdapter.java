/**
 * 
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Transforms an id into the Activity the id represents. This must do so with the bound
 * {@link ActivityProvider} which is typically a service.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityFromIdXmlAdapter
		extends XmlAdapter<Activity, ActivityId> {

	private ActivityProvider activityProvider;

	/**
	 * @param activityTestUtil
	 */
	@Inject
	public ActivityFromIdXmlAdapter(ActivityProvider activityProvider) {
		this.activityProvider = activityProvider;
	}

	/**
	 * Default to be used when no adaptation is desired.
	 */
	public ActivityFromIdXmlAdapter() {

	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Activity marshal(ActivityId id) throws Exception {
		if (id == null || activityProvider == null) {
			return null;
		}
		return activityProvider.getActivity(id);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public ActivityId unmarshal(Activity activity) throws Exception {
		if (activity == null || activityProvider == null) {
			return null;
		}
		return activity.id();
	}

}
