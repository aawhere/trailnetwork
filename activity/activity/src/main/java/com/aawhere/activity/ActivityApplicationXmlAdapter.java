/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.app.ApplicationXmlAdapter;

import com.google.inject.Singleton;

/**
 * Allows specific targeting of adaptation for Activity.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityApplicationXmlAdapter
		extends ApplicationXmlAdapter {

	/**
	 * 
	 */
	public ActivityApplicationXmlAdapter() {
	}

	/**
	 * @param show
	 */
	public ActivityApplicationXmlAdapter(Boolean show) {
		super(show);
	}

}
