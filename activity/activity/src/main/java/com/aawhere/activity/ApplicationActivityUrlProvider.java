/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.app.Application;

/**
 * Provides the url for the activity at it's orginal {@link Application}
 * 
 * @author aroller
 * 
 */
public interface ApplicationActivityUrlProvider {
	String applicationUrl(ActivityId activityId);
}
