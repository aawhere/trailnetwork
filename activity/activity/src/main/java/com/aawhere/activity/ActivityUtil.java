/**
 * 
 */
package com.aawhere.activity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.collections4.list.SetUniqueList;
import org.joda.time.DateTime;

import com.aawhere.activity.ActivityStats.Builder;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationKeyStats;
import com.aawhere.app.ApplicationStats;
import com.aawhere.app.Device;
import com.aawhere.app.DeviceCapabilities;
import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.app.DeviceUtil;
import com.aawhere.app.account.AccountId;
import com.aawhere.collections.Index;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.doc.archive.ArchivedDocumentUtil;
import com.aawhere.id.IdentifierToIdentifiableFunction;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.If;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.EntityUtil;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackSummaryUtil;
import com.aawhere.util.rb.Message;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;

/**
 * Helper methods to keep the noise out of the model object.
 * 
 * @author aroller
 * 
 */
public class ActivityUtil {

	/**
	 * Keeps activity type display and filtering simple by keeping only those significant
	 * 
	 */
	public static final Ratio MIN_ACTIVITY_TYPE_PERCENT = MeasurementUtil.ratio(3, 100);

	/**
	 * @see EntityUtil#modifiedSince(com.aawhere.persist.BaseEntity, DateTime)
	 * 
	 * @param activity
	 * @param timestamp
	 * @return
	 */
	public static Boolean modifiedSince(Activity activity, DateTime timestamp) {
		return EntityUtil.modifiedSince(activity, timestamp);
	}

	/**
	 * Returns the name if not null or a Default name if null.
	 * 
	 * @param name
	 * @return
	 */
	public static Message nameOrDefault(Message name) {
		return If.nil(name).use(ActivityMessage.UNKNOWN);
	}

	/**
	 * Returns the first element in the list or a default message if empty.
	 * 
	 * @param name
	 * @return
	 */
	public static Message firstOrDefault(List<Message> names) {
		if (names.isEmpty()) {
			return ActivityMessage.UNKNOWN;
		} else {
			return names.get(Index.FIRST.index);
		}
	}

	public static Function<Activity, ActivityType> activityTypeFunction() {
		return new Function<Activity, ActivityType>() {

			@Override
			public ActivityType apply(Activity input) {
				return (input == null) ? null : input.getActivityType();
			}
		};
	}

	/** Pieces together the activities from their ids to provide the activity type counter. */
	public static CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> activityTypeCount(
			Iterable<ActivityId> ids, IdentifierToIdentifiableFunction<ActivityId, Activity> activityProvider) {
		Iterable<ActivityType> activityTypes = activityTypeIterable(ids, activityProvider);
		return ActivityTypeStatistic.counter(activityTypes).build();
	}

	/**
	 * These are essentially the same thing. If provided the activities themselves may be valuable
	 * to display the difference, but in aggregation and filtering its easier to show/look for just
	 * one.
	 * 
	 * @return
	 */
	public static Function<ActivityType, ActivityType> hikeIsWalkFunction() {
		final ActivityType from = ActivityType.HIKE;
		final ActivityType to = ActivityType.WALK;
		return activityTypeTransformer(from, to);
	}

	public static Function<ActivityType, ActivityType> roadIsBikeFunction() {
		return activityTypeTransformer(ActivityType.ROAD, ActivityType.BIKE);
	}

	/**
	 * A system-determined grouping of activity types when reporting high level stats. This seems a
	 * lot like specific business logic so perhaps it should live in something like a story instead
	 * of this util.
	 * 
	 * @return
	 */
	public static Function<ActivityType, ActivityType> activityTypeStatsTransformers() {
		// a bit wasteful since each function will be called when we know if one finds a match the
		// other doesn't need to be called.
		// additional types can be added
		return hikeIsWalkFunction();
	}

	/**
	 * If the function receives an activity type that equals to "from" then it will return the
	 * activity type "to" in it's place, otherwise unchanged.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	private static Function<ActivityType, ActivityType> activityTypeTransformer(final ActivityType from,
			final ActivityType to) {
		return new ActivityTypeTransformer(from, to);
	}

	private static class ActivityTypeTransformer
			implements Function<ActivityType, ActivityType>, Serializable {

		private static final long serialVersionUID = 9090581485142944842L;
		private ActivityType to;
		private ActivityType from;

		private ActivityTypeTransformer(ActivityType from, ActivityType to) {
			this.from = from;
			this.to = to;
		}

		@Override
		public ActivityType apply(ActivityType input) {
			return (input == null) ? null : (input.equals(from)) ? to : input;
		}

	}

	public static Function<ActivityStats, SortedSet<ActivityTypeStatistic>> activityTypeStatsFunction() {
		return new Function<ActivityStats, SortedSet<ActivityTypeStatistic>>() {

			@Override
			public SortedSet<ActivityTypeStatistic> apply(ActivityStats input) {
				return (input == null) ? null : input.activityTypeStats();
			}
		};
	}

	public static Function<ActivityStats, SortedSet<ActivityTypeDistanceCategoryStatistic>> distanceStatsFunction() {
		return new Function<ActivityStats, SortedSet<ActivityTypeDistanceCategoryStatistic>>() {

			@Override
			public SortedSet<ActivityTypeDistanceCategoryStatistic> apply(ActivityStats input) {
				return (input == null) ? null : input.distanceStats();
			}
		};
	}

	public static Function<ActivityStats, SortedSet<ApplicationStats>> applicationStatsFunction() {
		return new Function<ActivityStats, SortedSet<ApplicationStats>>() {

			@Override
			public SortedSet<ApplicationStats> apply(ActivityStats input) {
				return (input == null) ? null : input.applicationStats();
			}
		};
	}

	public static Function<ActivityStats, SortedSet<ApplicationKeyStats>> deviceTypeStatsFunction() {
		return new Function<ActivityStats, SortedSet<ApplicationKeyStats>>() {

			@Override
			public SortedSet<ApplicationKeyStats> apply(ActivityStats input) {
				return (input == null) ? null : input.deviceTypeStats();
			}
		};
	}

	/**
	 * When stats are provided that includes both hikes and walks this will combine the results into
	 * just walking for simplicity.
	 * 
	 * @param verbose
	 * @return
	 */
	public static SortedSet<ActivityTypeStatistic> walkingCombined(final SortedSet<ActivityTypeStatistic> verbose) {
		final ActivityTypeStatistic hike = CategoricalStatistic.statsForCategory(verbose, ActivityType.HIKE);
		final ActivityTypeStatistic walk = CategoricalStatistic.statsForCategory(verbose, ActivityType.WALK);
		SortedSet<ActivityTypeStatistic> results;
		if (hike != null && walk != null) {
			ActivityTypeStatistic merged = CategoricalStatistic.merged(	hike,
																		walk,
																		ActivityTypeStatistic.builderSupplier());
			Iterable<ActivityTypeStatistic> verboseNoHikes = Iterables.filter(verbose, Predicates.not(Predicates
					.equalTo(hike)));
			Iterable<ActivityTypeStatistic> verboseNoHikesAndWalks = Iterables.filter(verboseNoHikes, Predicates
					.not(Predicates.equalTo(walk)));
			results = ImmutableSortedSet.<ActivityTypeStatistic> naturalOrder().addAll(verboseNoHikesAndWalks)
					.add(merged).build();
		} else {
			// nothing to merge
			results = verbose;
		}
		return results;
	}

	/**
	 * For groups of activities that follow the same route do not need to report Biking and Mountain
	 * Biking since they typically would be one or the other. This will inspect the given stats and
	 * return an updated stats reflecting the most likely biking activity type to represent.
	 * 
	 * Notice: this could be expanded to {@link ActivityType#ROAD}, but for now
	 * {@link #roadIsBikeFunction()} merges before stats arise.
	 * 
	 * @return
	 */
	public static SortedSet<ActivityTypeStatistic> bikingCombined(SortedSet<ActivityTypeStatistic> verbose,
			Ratio minForMtbToAbsorb) {
		SortedSet<ActivityTypeStatistic> response = verbose;

		final ActivityTypeStatistic bikeStats = CategoricalStatistic.statsForCategory(verbose, ActivityType.BIKE);
		final ActivityTypeStatistic mtbStats = CategoricalStatistic.statsForCategory(verbose, ActivityType.MTB);
		final ActivityTypeStatistic roadStats = CategoricalStatistic.statsForCategory(verbose, ActivityType.ROAD);

		// at least one has to be found, otherwise just return
		if (bikeStats != null || mtbStats != null || roadStats != null) {
			ActivityType activityTypeChosen = bikeGeneralized(bikeStats, mtbStats, roadStats, minForMtbToAbsorb);
			ActivityTypeStatistic into;
			final ActivityTypeStatistic from1;
			final ActivityTypeStatistic from2;

			switch (activityTypeChosen) {
				case MTB:
					into = mtbStats;
					from1 = roadStats;
					from2 = bikeStats;
					break;
				case ROAD:
					into = roadStats;
					from1 = mtbStats;
					from2 = bikeStats;
					break;
				case BIKE:
					into = bikeStats;
					from1 = mtbStats;
					from2 = roadStats;
					break;
				default:
					throw new InvalidChoiceException(activityTypeChosen);
			}

			if (from1 != null) {
				into = CategoricalStatistic.merged(from1, into, ActivityTypeStatistic.builderSupplier());
			}
			if (from2 != null) {
				into = CategoricalStatistic.merged(from2, into, ActivityTypeStatistic.builderSupplier());
			}
			// now replace the list with the bike results
			response = Sets.newTreeSet(verbose);
			if (bikeStats != null) {
				response.remove(bikeStats);
			}
			if (mtbStats != null) {
				response.remove(mtbStats);
			}
			if (roadStats != null) {
				response.remove(roadStats);
			}
			if (into != null) {
				response.add(into);
			}
		}
		return response;
	}

	/**
	 * Provided the bike stats and mountain bike stats this picks one or the either based on the
	 * given ratio.
	 * 
	 * @param bikeStats
	 * @param mtbStats
	 * @param minForMtbToAbsorb
	 * @return
	 */
	public static Boolean isMountainBike(ActivityTypeStatistic bikeStats, ActivityTypeStatistic mtbStats,
			final Ratio minForMtbToAbsorb) {
		return bikeGeneralized(bikeStats, mtbStats, null, minForMtbToAbsorb).equals(ActivityType.MTB);
	}

	/**
	 * given the stats for road, mountain and general biking this will report either
	 * {@link ActivityType#BIKE} or {@link ActivityType#MTB}.
	 * 
	 * TODO:choose road biking too
	 * 
	 * @param bikeStats
	 * @param mtbStats
	 * @param minForMtbToAbsorb
	 * @return
	 */
	public static ActivityType bikeGeneralized(ActivityTypeStatistic bikeStats, ActivityTypeStatistic mtbStats,
			ActivityTypeStatistic roadBikeStats, final Ratio minForMtbToAbsorb) {
		ActivityType selected = null;
		int roadBikeCount = 0;
		int mtbCount = 0;
		int bikeCount = 0;

		if (roadBikeStats != null) {
			roadBikeCount = roadBikeStats.count();
		}
		if (mtbStats != null) {
			mtbCount = mtbStats.count();
		}
		if (bikeStats != null) {
			bikeCount = bikeStats.count();
		}

		if (roadBikeCount > mtbCount + bikeCount) {
			selected = ActivityType.ROAD;
		} else if (mtbCount >= roadBikeCount + bikeCount) {
			selected = ActivityType.MTB;
		} else if (mtbStats != null && QuantityMath.create(mtbStats.getPercent()).greaterThanEqualTo(minForMtbToAbsorb)) {
			selected = ActivityType.MTB;
		}
		// if MTB couldn't be determined then assume it is BIKE
		if (selected == null) {
			selected = ActivityType.BIKE;
		}
		return selected;
	}

	/**
	 * Given all the the stats, presumably form multiple entities that are keeping these stats, this
	 * will cumulate the counts based on the categories. The results is a single ActivityStats that
	 * represents the sum of all that was given.
	 * 
	 * @param allStats
	 * @return
	 */
	public static ActivityStats cumulation(Iterable<ActivityStats> allStats) {
		final Builder builder = ActivityStats.create();
		// activity type
		{
			final Iterable<ActivityTypeStatistic> activityTypeStats = Iterables.concat(Iterables.filter(Iterables
					.transform(allStats, activityTypeStatsFunction()), Predicates.notNull()));
			SortedSet<ActivityTypeStatistic> activityTypesCumulationStats = CategoricalStatsCounter
					.cumulation(activityTypeStats, ActivityTypeStatistic.builderSupplier());
			builder.activityTypeStats(activityTypesCumulationStats);
		}
		// distance
		{
			final Iterable<ActivityTypeDistanceCategoryStatistic> distanceStats = Iterables.concat(Iterables
					.filter(Iterables.transform(allStats, distanceStatsFunction()), Predicates.notNull()));
			SortedSet<ActivityTypeDistanceCategoryStatistic> distanceCumulationStats = CategoricalStatsCounter
					.cumulation(distanceStats, ActivityTypeDistanceCategoryStatistic.builderSupplier());
			builder.distanceStats(distanceCumulationStats);
		}
		// application
		{
			Iterable<ApplicationStats> applicationStats = Iterables.concat(Iterables.filter(Iterables
					.transform(allStats, applicationStatsFunction()), Predicates.notNull()));
			SortedSet<ApplicationStats> applicationCumulationStats = CategoricalStatsCounter
					.cumulation(applicationStats, ApplicationStats.builderSupplier());
			builder.applicationStats(applicationCumulationStats);
		}
		// device types
		{
			Iterable<ApplicationKeyStats> deviceTypeStats = Iterables.concat(Iterables.filter(Iterables
					.transform(allStats, deviceTypeStatsFunction()), Predicates.notNull()));
			SortedSet<ApplicationKeyStats> deviceTypeCumulationStats = CategoricalStatsCounter
					.cumulation(deviceTypeStats, ApplicationKeyStats.builderSupplier());
			builder.deviceTypeStats(deviceTypeCumulationStats);
		}
		return builder.build();
	}

	/**
	 * @param ids
	 * @param activityProvider
	 * @return
	 */
	public static Iterable<ActivityType> activityTypeIterable(Iterable<ActivityId> ids,
			Function<ActivityId, Activity> activityProvider) {
		Iterable<Activity> activityIterable = Iterables.transform(ids, activityProvider);
		Iterable<ActivityType> activityTypes = Iterables.transform(activityIterable, activityTypeFunction());
		return activityTypes;
	}

	public static Function<Activity, TrackSummary> trackSummaryFunction() {
		return new Function<Activity, TrackSummary>() {

			@Override
			public TrackSummary apply(Activity input) {
				return (input == null) ? null : input.getTrackSummary();
			}
		};
	}

	public static Function<Activity, BoundingBox> boundingBoxFunction() {
		return Functions.compose(TrackSummaryUtil.boundingBoxFunction(), trackSummaryFunction());
	}

	/**
	 * simply provides the application key from teh activity id.
	 * 
	 * @return
	 */
	public static Function<ActivityId, ApplicationKey> applicationKeyFunction() {
		return ActivityIdentifierUtil.applicationKeyFunction();
	}

	public static Function<Activity, ArchivedDocumentId> archivedDocumentIdFunction() {
		return Functions.compose(TrackSummaryUtil.archivedDocumentIdFunction(), trackSummaryFunction());
	}

	public static Function<ArchivedDocument, ActivityId> archivedDocumentApplicationDataIdFunction() {
		return Functions.compose(	ActivityIdentifierUtil.activityIdFromStringFunction(),
									ArchivedDocumentUtil.applicationDataIdFunction());
	}

	/**
	 * Since Activities have an archived document id and each activity has it's own then a bimap is
	 * returned of the archived document id mapped to it's activity of origin.
	 * 
	 * @param activities
	 * @return
	 */
	public static BiMap<ArchivedDocumentId, Activity> archivedDocumentIds(Iterable<Activity> activities) {
		ImmutableMap<ArchivedDocumentId, Activity> uniqueIndex = Maps.uniqueIndex(	activities,
																					archivedDocumentIdFunction());
		return HashBiMap.create(uniqueIndex);
	}

	public static Function<ActivityType, TransportMode> transportModeFunction() {
		return new Function<ActivityType, TransportMode>() {

			@Override
			public TransportMode apply(ActivityType input) {
				return (input == null) ? null : input.transportMode;
			}
		};
	}

	/**
	 * Given the activity types and the distance this will return all categories that match the
	 * activity type and distance pair. If an activity type is not found then it is ignored and no
	 * result will be included to represent that type.
	 * 
	 * The list provides the categories in the order the activity types were received...to preserve
	 * order assuming they were provided with relevance. It also ensures uniqueness so if an
	 * Activity type is provided twice only the first will be respected.
	 * 
	 * @param activityTypes
	 * @param distance
	 */
	@Nonnull
	public static List<ActivityTypeDistanceCategory> distanceCategories(Iterable<ActivityType> activityTypes,
			Length distance) {
		final Iterable<ActivityTypeDistanceCategory> categories = Iterables
				.transform(activityTypes, distanceCategoryFunction(distance));
		final Iterable<ActivityTypeDistanceCategory> withoutNulls = Iterables.filter(categories, Predicates.notNull());
		// this enforces uniqueness, but keeps order
		return SetUniqueList.<ActivityTypeDistanceCategory> setUniqueList(Lists.newArrayList(withoutNulls));
	}

	/**
	 * @see ActivityTypeDistanceCategory#valueOf(ActivityType, Comparable)
	 * 
	 * @param distance
	 * @return
	 */
	public static Function<ActivityType, ActivityTypeDistanceCategory> distanceCategoryFunction(final Length distance) {
		return new Function<ActivityType, ActivityTypeDistanceCategory>() {

			@Override
			public ActivityTypeDistanceCategory apply(ActivityType input) {
				return ActivityTypeDistanceCategory.valueOf(input, distance);
			}
		};
	}

	/**
	 * Keeps those activities with track summaries that have not yet been updated with it's own
	 * bounds. TN-666
	 * 
	 * */
	public static Predicate<Activity> nullTrackSummaryBoundsPredicate() {
		return Predicates.compose(TrackSummary.nullBoundingBoxPredicate(), trackSummaryFunction());
	}

	public static Function<Activity, Device> deviceFunction() {
		return new Function<Activity, Device>() {

			@Override
			public Device apply(Activity input) {
				return (input == null) ? null : input.getDevice();
			}
		};
	}

	public static Function<Activity, SignalQuality> signalFunction() {
		return new Function<Activity, SignalQuality>() {

			@Override
			public SignalQuality apply(Activity input) {
				return (input == null) ? null : input.getTrackSummary().getSignalQuality();
			}
		};
	}

	/**
	 * Given the activities this will return the counter of the device keys for those activities
	 * that provide devices.
	 * 
	 * @param activities
	 * @return
	 */
	public static CategoricalStatsCounter<ApplicationKeyStats, ApplicationKey> deviceTypeStatsCounter(
			Iterable<Activity> activities) {
		return ApplicationKeyStats.counter(deviceKeys(deviceRequired(activities)));
	}

	public static Function<Activity, ApplicationKey> deviceKeyFunction() {
		return Functions.compose(DeviceUtil.keyFunction(), deviceFunction());
	}

	public static Iterable<ApplicationKey> deviceKeys(Iterable<Activity> activities) {
		return Iterables.transform(activities, deviceKeyFunction());
	}

	public static Iterable<SignalQuality> signalQualities(Iterable<Activity> activities) {
		return Iterables.transform(activities, signalFunction());
	}

	public static Function<Activity, DeviceCapabilities> deviceCapabilitiesFunction(
			DeviceCapabilitiesRepository capabilityRepository) {
		return Functions.compose(capabilityRepository.capabilitiesFunction(), deviceKeyFunction());
	}

	/**
	 * Provides a filter on the activities to ensure those returned have a device to give by using a
	 * {@link Predicates#notNull()}.
	 * 
	 * @param activities
	 * @return
	 */
	public static Iterable<Activity> deviceRequired(Iterable<Activity> activities) {
		return Iterables.filter(activities, Predicates.compose(Predicates.notNull(), deviceFunction()));
	}

	/**
	 * Provides a filter on the activities to ensure those returned have a signal to give by using a
	 * {@link Predicates#notNull()}.
	 * 
	 * @param activities
	 * @return
	 */
	public static Iterable<Activity> signalRequired(Iterable<Activity> activities) {
		return Iterables.filter(activities, Predicates.compose(Predicates.notNull(), signalFunction()));
	}

	/**
	 * @param stats
	 * @param minRatioToChooseMtb
	 * @return
	 */
	public static Boolean isMountainBike(CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> stats,
			Ratio minRatioToChooseMtb) {
		return isMountainBike(stats.stats(ActivityType.BIKE), stats.stats(ActivityType.MTB), minRatioToChooseMtb);
	}

	public static ActivityProvider activityProvider(final Iterable<Activity> activities) {
		return new ActivityProvider.Default(activities);
	}

	/**
	 * Provides the names available in the given activities. Nulls and Unknown names are
	 * automatically filtered out.
	 * 
	 * @param courses
	 * @return
	 */
	public static Iterable<Message> names(Iterable<Activity> activities) {
		return Iterables
				.filter(Iterables.filter(Iterables.transform(activities, nameFunction()), Predicates.notNull()),
						Predicates.not(unknownPredicate()));
	}

	/**
	 * Provided activities, this returns a map of the activity id to it's valuable name. Nulls and
	 * Unknowns are filtered out.
	 * 
	 * @param activities
	 * @return
	 */
	public static Map<ActivityId, Message> nameMap(Iterable<Activity> activities) {
		BiMap<Activity, ActivityId> idMap = IdentifierUtils.biMap(activities);
		Map<ActivityId, Message> map = Maps.transformValues(idMap.inverse(), nameFunction());
		return Maps.filterValues(Maps.filterValues(map, Predicates.notNull()), Predicates.not(unknownPredicate()));
	}

	public static Function<Activity, Message> nameFunction() {
		return new Function<Activity, Message>() {

			@Override
			public Message apply(Activity input) {
				return (input != null) ? input.getName() : null;
			}
		};
	}

	public static Predicate<Message> unknownPredicate() {
		return new Predicate<Message>() {

			@Override
			public boolean apply(Message input) {
				return ActivityMessage.UNKNOWN.equals(input);
			}
		};
	}

	public static Function<Activity, GeoCells> spatialBufferFunction() {
		return Functions.compose(TrackSummaryUtil.spatialBufferFunction(), trackSummaryFunction());
	}

	/**
	 * Of all the activities given this will group each activity into groups where geocells are
	 * {@link GeoCells#equals(Object)}.
	 * 
	 * @param activities
	 * @return
	 */
	public static ImmutableListMultimap<GeoCells, Activity> spatialBufferGroups(Iterable<Activity> activities) {
		return Multimaps.index(activities, spatialBufferFunction());
	}

	/**
	 * Standard way to describe the start area for an Activity. Note that a single geocell can be
	 * quite misleading about poximity of two starts to each other since they could be 1 mm on
	 * either side of the same geocell boundary.
	 * 
	 * @param activity
	 * @param resolution
	 * @return
	 */
	public static GeoCell startGeoCell(Activity activity, Resolution resolution) {
		return GeoCellUtil.geoCell(activity.getTrackSummary().getStart().getLocation(), resolution);
	}

	public static Function<Activity, AccountId> accountIdFunction() {
		return new Function<Activity, AccountId>() {

			@Override
			public AccountId apply(Activity input) {
				return (input != null) ? input.getAccountId() : null;
			}
		};
	}

	public static Iterable<AccountId> accountIds(Iterable<Activity> activities) {
		return Iterables.transform(activities, accountIdFunction());
	}

	/**
	 * Indicates true only if the name is not null and it is not {@link #unknownPredicate()}
	 * 
	 * @return
	 */
	public static Predicate<Activity> nameKnownPredicate() {
		return Predicates.and(	Predicates.notNull(),
								Predicates.compose(Predicates.not(unknownPredicate()), nameFunction()));
	}

	public static Predicate<String> nameValueNotUnknownPredicate() {
		return new Predicate<String>() {

			@Override
			public boolean apply(String input) {
				return nameKnown(input);
			}

		};
	}

	/**
	 * An indicator that the name is not {@link ActivityMessage#UNKNOWN} or null.
	 * 
	 * @param input
	 * @return
	 */
	public static boolean nameKnown(@Nullable String input) {
		return input != null && !ActivityMessage.UNKNOWN_VALUE.equalsIgnoreCase(input);
	}

	public static com.aawhere.activity.Activity.Builder mutate(Activity activity) {
		return new Activity.Builder(activity);
	}
}
