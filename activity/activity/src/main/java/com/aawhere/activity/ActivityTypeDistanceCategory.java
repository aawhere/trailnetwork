/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.activity.ActivityType.*;
import static com.aawhere.measure.MeasurementUtil.*;

import java.util.Arrays;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.Unit;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.enums.EnumWithDefault;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.Messages;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.collect.Range;

/**
 * Common categories for running to be used for understanding and filtering.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public enum ActivityTypeDistanceCategory implements Message, EnumWithDefault<ActivityTypeDistanceCategory> {

	/** Order matters since an Enum implements comparable on the ordinal */
	/**  */
	FIVE_K_RUN(RUN, "5K", 0, 6, AdjectivePhrase.FIVE_K_RUN),
	/** */
	TEN_K_RUN(RUN, "10K", 6, 12, AdjectivePhrase.TEN_K_RUN),
	/** */
	HALF_MARATHON(RUN, "Half Marathon", 12, 25, AdjectivePhrase.HALF_MARATHON),
	/** */
	MARATHON(RUN, "Marathon", 25, 45, AdjectivePhrase.MARATHON),
	/** */
	ULTRA_RUN(RUN, "Ultra", 45, null, AdjectivePhrase.ULTRA_RUN),
	/** */
	SHORT_BIKE(BIKE, "Short", 0, 16, AdjectivePhrase.SHORT_BIKE),
	/** */
	MEDIUM_BIKE(BIKE, "Medium", 16, 40, AdjectivePhrase.MEDIUM_BIKE),
	/** */
	LONG_BIKE(BIKE, "Long", 40, 100, AdjectivePhrase.LONG_BIKE),
	/** Century has to remain biking until Road is handled on the web */
	CENTURY(BIKE, "Century", 100, null, AdjectivePhrase.CENTURY),
	/**
	 * century information from
	 * http://www.granfondoguide.com/Contents/Index/931/what-is-a-century-bike-ride Century rides
	 * are the backbone and life blood of road cycling. Supported and sponsored by local cycling
	 * clubs across the country. A century ride is 100 miles in distance, but most events support
	 * different distances, for all cycling abilities, typically:
	 * 
	 * <pre>
	 * 		25 miles - quarter century
	 * 		50 miles - half century
	 * 		62 miles - metric century (100km)
	 * 		100 miles - imperial century
	 * 		120 miles - double metric century (200km)
	 * 		200 miles - double century, also known as a "double"
	 * </pre>
	 */
	SHORT_ROAD(ROAD, "Short", 0, 20, AdjectivePhrase.SHORT_ROAD),
	/** */
	MEDIUM_ROAD(ROAD, "Medium", 20, 95, AdjectivePhrase.MEDIUM_ROAD),
	/** */
	METRIC_CENTURY(ROAD, "Metric Century", 95, 155, AdjectivePhrase.METRIC_CENTURY),
	/** named road century to separate from general BIKE */
	ROAD_CENTURY(ROAD, "Century", 155, 310, AdjectivePhrase.CENTURY),
	/** */
	DOUBLE_CENTURY(ROAD, "Double Century", 310, null, AdjectivePhrase.DOUBLE_CENTURY),
	/** */
	SHORT_MTB(MTB, "Short", 0, 10, AdjectivePhrase.SHORT_MTB),
	/** */
	MEDIUM_MTB(MTB, "Medium", 10, 25, AdjectivePhrase.MEDIUM_MTB),
	/** */
	LONG_MTB(MTB, "Long", 25, 50, AdjectivePhrase.LONG_MTB),
	/** */
	EPIC(MTB, "Epic", 50, null, AdjectivePhrase.EPIC),

	/**  */
	SHORT_WALK(WALK, "Short", 0, 3, AdjectivePhrase.SHORT_WALK),
	/** */
	MEDIUM_WALK(WALK, "Medium", 3, 10, AdjectivePhrase.MEDIUM_WALK),
	/** */
	LONG_WALK(WALK, "Long", 10, null, AdjectivePhrase.LONG_WALK),

	/**  */
	SHORT(U, "Short", 0, 5, AdjectivePhrase.SHORT),
	/** */
	MEDIUM(U, "Medium", 5, 20, AdjectivePhrase.MEDIUM),
	/** */
	LONG(U, "Long", 20, null, AdjectivePhrase.LONG),

	/** Default value to support future versions if categories are added. */
	FUTURE(U, "Coming soon...", Integer.MIN_VALUE, Integer.MAX_VALUE, AdjectivePhrase.LONG);

	/**
	 * 
	 */
	private static final ActivityType DEFAULT = U;
	private ParamKey[] parameterKeys = {};
	private String message;
	public final Range<Comparable<Length>> range;
	@XmlElement
	public final ActivityType activityType;
	public final AdjectivePhrase asAdjective;

	private ActivityTypeDistanceCategory(ActivityType activityType, String message, Integer minInclusiveKm,
			Integer maxExclusiveKm, AdjectivePhrase adjectivePhrase) {
		this.message = message;
		this.activityType = activityType;
		this.asAdjective = adjectivePhrase;
		QuantityFactory<Length> lengthFactory = MeasurementUtil.factory(Length.class);
		Unit<Length> km = MetricSystem.KILO(MetricSystem.METRE);
		final Comparable<Length> min = comparable(lengthFactory.create(minInclusiveKm, km));
		if (maxExclusiveKm != null) {
			final Comparable<Length> max = comparable(lengthFactory.create(maxExclusiveKm, km));
			this.range = Range.closedOpen(min, max);
		} else {
			this.range = Range.atLeast(min);
		}
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.enums.EnumWithDefault#getDefault()
	 */
	@Override
	public ActivityTypeDistanceCategory getDefault() {
		return FUTURE;
	}

	/**
	 * Provides teh {@link ActivityTypeDistanceCategory} that matches the given {@link ActivityType}
	 * and has a {@link #range} that {@link Range#contains(Comparable)} the given distance.
	 * 
	 * @param activityType
	 * @param distance
	 * @return the
	 */
	@Nullable
	public static ActivityTypeDistanceCategory valueOf(@Nonnull ActivityType activityType, @Nonnull Length distance) {
		return valueOf(activityType, comparable(distance));
	}

	/**
	 * When you need a guarantee that a distance will be returned you may provide a default Activity
	 * that is known to have distance, like {@link #DEFAULT}.
	 * 
	 * @param activityType
	 * @param distance
	 * @param defaultCategories
	 * @return
	 */
	@Nonnull
	public static ActivityTypeDistanceCategory valueOf(@Nonnull ActivityType activityType, @Nonnull Length distance,
			@Nonnull ActivityType defaultCategories) {
		ActivityTypeDistanceCategory category = valueOf(activityType, distance);
		if (category == null) {
			category = valueOf(defaultCategories, distance);
			Assertion.exceptions().notNull("category", category);
		}
		return category;
	}

	/**
	 * calls {@link #valueOf(ActivityType, Length, ActivityType)} with the {@link #DEFAULT}.
	 * 
	 * @param activityType
	 * @param distance
	 * @return
	 */
	@Nonnull
	public static ActivityTypeDistanceCategory valueOfWithDefault(@Nonnull ActivityType activityType,
			@Nonnull Length distance) {
		return valueOf(activityType, distance, DEFAULT);
	}

	public Boolean isDefault() {
		return this.activityType.equals(DEFAULT);
	}

	/**
	 * Provides the distance category corresponding the to ActivityType and Length given or null if
	 * no category relates to the distance category.
	 * 
	 * @param activityType
	 * @param distance
	 * @return the category or null
	 */
	@Nullable
	public static ActivityTypeDistanceCategory valueOf(ActivityType activityType, Comparable<Length> distance) {
		ActivityTypeDistanceCategory[] values = values();
		// this could be made more efficient by pre-sorting into sets of activity types.
		for (ActivityTypeDistanceCategory activityTypeDistanceCategory : values) {
			if (activityTypeDistanceCategory.activityType.equals(activityType)) {
				if (activityTypeDistanceCategory.range.contains(distance)) {
					return activityTypeDistanceCategory;
				}
			}
		}
		return null;
	}

	public static Function<ActivityType, ActivityTypeDistanceCategory> valueOfFunction(@Nonnull final Length distance) {
		return new Function<ActivityType, ActivityTypeDistanceCategory>() {

			@Override
			public ActivityTypeDistanceCategory apply(ActivityType input) {
				return (input != null) ? valueOf(input, distance) : null;
			}
		};
	}

	private static enum AdjectivePhrase implements Message {
		FIVE_K_RUN("{COUNT_VALUE,plural, one{5K run} other{5K runs}}"),
		/** */
		TEN_K_RUN("{COUNT_VALUE,plural, one{10K run} other{10K runs}}"),
		/** */
		HALF_MARATHON("{COUNT_VALUE,plural, one{half marathon} other{half marathons}}"),
		/** */
		MARATHON("{COUNT_VALUE,plural, one{marathon} other{marathons}}"),
		/** */
		ULTRA_RUN("{COUNT_VALUE,plural, one{ultra run} other{ultra runs}}"), SHORT_MTB(
				"{COUNT_VALUE,plural, one{short mountain bike ride} other{short mountain bike rides}}"),
		/** */
		MEDIUM_MTB("{COUNT_VALUE,plural, one{mountain bike ride} other{mountain bike rides}}"),
		/** */
		LONG_MTB("{COUNT_VALUE,plural, one{long mountain bike ride} other{long mountain bike rides}}"),
		/** */
		EPIC("{COUNT_VALUE,plural, one{epic mountain bike ride} other{epic mountain bike rides}}"), SHORT_BIKE(
				"{COUNT_VALUE,plural, one{short bike ride} other{short bike rides}}"),
		/** */
		MEDIUM_BIKE("{COUNT_VALUE,plural, one{medium length bike ride} other{medium length bike rides}}"),
		/** */
		LONG_BIKE("{COUNT_VALUE,plural, one{long bike ride} other{long bike rides}}"),
		/** */
		SHORT_ROAD("{COUNT_VALUE,plural, one{short road ride} other{short road rides}}"),
		/** purposely removing length since this would be considered "normal" length */
		MEDIUM_ROAD("{COUNT_VALUE,plural, one{road ride} other{road rides}}"),
		/** */
		METRIC_CENTURY("{COUNT_VALUE,plural, one{metric century} other{metric centuries}}"),
		/** */
		CENTURY("{COUNT_VALUE,plural, one{century} other{centuries}}"),
		/** */
		DOUBLE_CENTURY("{COUNT_VALUE,plural, one{double century} other{double centuries}}"),

		/** */
		SHORT_WALK("{COUNT_VALUE,plural, one{stroll} other{strolls}}"),
		/** */
		MEDIUM_WALK("{COUNT_VALUE,plural, one{walk} other{walks}}"),

		/** */
		LONG_WALK("{COUNT_VALUE,plural, one{long walk} other{long walks}}"),

		/** */
		SHORT("{COUNT_VALUE,plural, one{short activity} other{short activities}}"),
		/** */
		MEDIUM("{COUNT_VALUE,plural, one{activity} other{activities}}"),

		/** */
		LONG("{COUNT_VALUE,plural, one{long activity} other{long activities}}"),

		;

		private final String message;
		private static ParamKey[] paramKeys = { ActivityType.Param.COUNT };

		private AdjectivePhrase(String message) {
			this.message = message;
		}

		@Override
		public String getValue() {
			return message;
		}

		@Override
		public ParamKey[] getParameterKeys() {
			return paramKeys;
		}
	}

	public static Messages messages() {
		return new Messages(Arrays.asList(values()));
	}

}
