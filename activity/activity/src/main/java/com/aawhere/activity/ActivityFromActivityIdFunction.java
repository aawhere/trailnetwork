/**
 * 
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.id.IdentifierToIdentifiableFunction;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.EntityNotFoundException;

import com.google.common.base.Function;

/**
 * Retreives an activity using {@link ActivityProvider} from the givin {@link ActivityId}
 * 
 * @author Brian Chapman
 * @deprecated use {@link IdentifierToIdentifiableFunction}
 * @see ActivityService#idFunction()
 */
@Deprecated
public class ActivityFromActivityIdFunction
		implements Function<ActivityId, Activity> {

	/**
	 * Used to construct all instances of ActivityFromActivityIdFunction.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<ActivityFromActivityIdFunction> {

		public Builder() {
			super(new ActivityFromActivityIdFunction());
		}

		public Builder activityProvider(ActivityProvider activityProvider) {
			building.activityProvider = activityProvider;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct ActivityFromActivityIdFunction */
	private ActivityFromActivityIdFunction() {
	}

	public static Builder create() {
		return new Builder();
	}

	public ActivityProvider activityProvider;

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Activity apply(ActivityId activityId) {
		try {
			return activityProvider.getActivity(activityId);
		} catch (EntityNotFoundException e) {
			// unfortunate but a runtime exception won't do here.
			return null;
		}
	}

}
