/**
 * 
 */
package com.aawhere.activity;

import java.util.List;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationKeyStats;
import com.aawhere.app.DeviceCapabilities;
import com.aawhere.app.SignalQualityStats;
import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.app.DeviceUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

/**
 * The courses each have contributing factors that can make their signal quality better than others.
 * This looks at a set of Activities and recommends courses based on various attributes like track
 * log summary stats, device, etc.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class DeviceSignalActivitySelector {

	private static final Ordering<SignalQuality> SIGNAL_QUALITY_ORDERING = signalQualityOrdering();

	/**
	 * Used to construct all instances of DeviceSignalActivitySelector.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<DeviceSignalActivitySelector> {
		private Iterable<Activity> courses;
		private DeviceCapabilitiesRepository capabilitiesRepository;

		public Builder() {
			super(new DeviceSignalActivitySelector());
		}

		public Builder activities(Iterable<Activity> courses) {
			this.courses = courses;
			return this;
		}

		public Builder capabilitiesRepository(DeviceCapabilitiesRepository capabilitiesRepository) {
			this.capabilitiesRepository = capabilitiesRepository;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("capabilitiesRepository", this.capabilitiesRepository);
		}

		@Override
		public DeviceSignalActivitySelector build() {
			/**
			 * 1. sort by signal quality. 2. Filter out all but highest category. 3. Subsort by best
			 * overall device. 4. choose best acivity.
			 * 
			 * store sorted signal list, sorted device list (of best signals) best activity.
			 */

			DeviceSignalActivitySelector built = super.build();

			Iterable<Activity> activitiesWithSignal = ActivityUtil.signalRequired(this.courses);
			Iterable<Activity> activitiesWithDevices = ActivityUtil.deviceRequired(this.courses);
			Function<Activity, DeviceCapabilities> deviceCapabilitiesFunction = ActivityUtil
					.deviceCapabilitiesFunction(capabilitiesRepository);
			// FIXME: after updating all activities with a signal, this check will be unncessary.
			if (!activitiesWithSignal.iterator().hasNext()) {
				// Don't have signals, sort by devices.
				built.bestFirst = bestDeviceFirst(deviceCapabilitiesFunction).sortedCopy(activitiesWithDevices);
			} else {
				// Have signals, use them first, then sort by devices, if any.
				built.bestFirst = bestSignalFirstOrdering(ActivityUtil.signalFunction())
						.compound(bestDeviceFirst(deviceCapabilitiesFunction)).sortedCopy(activitiesWithSignal);
			}

			Activity defaultCourse = null;
			built.best = Iterables.getFirst(built.bestFirst, Iterables.getFirst(this.courses, defaultCourse));
			if (built.best == defaultCourse) {
				throw new IllegalArgumentException("no courses were given");
			}
			built.deviceTypeStats = ApplicationKeyStats.counter(ActivityUtil.deviceKeys(activitiesWithDevices));
			built.signalQualityStats = SignalQualityStats.counter(ActivityUtil.signalQualities(activitiesWithSignal));
			return built;
		}

	}// end Builder

	@XmlElement
	private Activity best;
	@XmlElement
	private List<Activity> bestFirst;
	@XmlElement
	private CategoricalStatsCounter<ApplicationKeyStats, ApplicationKey> deviceTypeStats;
	@XmlElement
	private CategoricalStatsCounter<SignalQualityStats, SignalQuality> signalQualityStats;

	/** Use {@link Builder} to construct DeviceSignalActivitySelector */
	private DeviceSignalActivitySelector() {
	}

	public static Builder create() {
		return new Builder();
	}

	public static Ordering<SignalQuality> signalQualityOrdering() {
		return Ordering.natural().reverse();
	}

	public static Ordering<Activity> bestSignalFirstOrdering(Function<Activity, SignalQuality> signalFunction) {
		return SIGNAL_QUALITY_ORDERING.onResultOf(signalFunction);
	}

	public static Ordering<Activity> bestDeviceFirst(Function<Activity, DeviceCapabilities> capabilitiesFunction) {
		return DeviceUtil.DEVICE_SIGNAL_QUALITY_ORDERING.onResultOf(capabilitiesFunction);
	}

	/**
	 * @return
	 */
	@Nonnull
	public Activity best() {
		return this.best;
	}

	/**
	 * @return the bestFirst
	 */
	public List<Activity> bestFirst() {
		return this.bestFirst;
	}

	/**
	 * @return the deviceTypeStats
	 */
	public CategoricalStatsCounter<ApplicationKeyStats, ApplicationKey> deviceTypeStats() {
		return this.deviceTypeStats;
	}
}
