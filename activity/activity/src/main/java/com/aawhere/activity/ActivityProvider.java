/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceStandard;
import com.aawhere.persist.ServiceStandardBase;

import com.google.common.base.Function;

/**
 * Indicates that this class can provide activities.
 * 
 * @author Brian Chapman
 * 
 */
public interface ActivityProvider
		extends ServiceStandard<Activities, Activity, ActivityId> {

	public Activity getActivity(ActivityId activityId) throws EntityNotFoundException;

	public Activities getActivities(Iterable<ActivityId> ids);

	public static class Default
			extends ServiceStandardBase<Activities, Activity, ActivityId>
			implements ActivityProvider {

		public Default() {
			super();
		}

		public Default(Function<ActivityId, Activity> idFunction) {
			super(idFunction);
		}

		public Default(Iterable<Activity> entities) {
			super(entities);
		}

		@Override
		public Activity getActivity(ActivityId activityId) throws EntityNotFoundException {
			return entity(activityId);
		}

		@Override
		public Activities getActivities(Iterable<ActivityId> ids) {
			return Activities.create().addAll(all(ids).values()).build();
		}

	}
}
