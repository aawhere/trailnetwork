/**
 *
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.StringIdentifier;
import com.aawhere.persist.ServicedBy;
import com.aawhere.xml.XmlNamespace;

/**
 * The {@link Identifier} for {@link Activity}.
 * 
 * @see Activity#getId()
 * 
 * @author Aaron Roller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@ServicedBy(ActivityProvider.class)
public class ActivityId
		extends StringIdentifier<Activity>
		implements ActivityIdentifier, ApplicationDataId, ApplicationActivityId {
	private static final long serialVersionUID = 6983089570546384171L;

	/**
	 * @param kind
	 */
	ActivityId() {
		super(Activity.class);
	}

	/**
	 * @deprecated use {@link #ActivityId(String)}
	 * 
	 * @param ourId
	 */
	public ActivityId(Long ourId) {
		this(KnownApplication.TRAIL_NETWORK.key, String.valueOf(ourId));
	}

	/**
	 * Provided to any client that may have a Long formatted as a String.
	 * 
	 * @param value
	 * @param kind
	 * @throws UnknownApplicationException
	 *             a runtime stopping invalid applications immediately since they are all
	 *             {@link KnownApplication}.
	 */
	public ActivityId(String value) throws UnknownApplicationException {
		super(value, Activity.class);

	}

	public ActivityId(ApplicationKey applicationKey, String remoteId) {
		this(IdentifierUtils.createCompositeIdValue(applicationKey, remoteId));
	}

	/**
	 * @deprecated stop using appId
	 * @param appId
	 */
	public ActivityId(ApplicationActivityId appId) {
		this(appId.toString());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.ApplicationDataId#getApplicationKey()
	 */
	@Override
	public ApplicationKey getApplicationKey() {
		return new ApplicationKey(IdentifierUtils.splitCompositeId(this).getLeft());
	}

	@XmlAttribute
	public String getApp() {
		return getApplicationKey().getValue();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.ApplicationDataId#getRemoteId()
	 */
	@Override
	public String getRemoteId() {
		return IdentifierUtils.splitCompositeId(this).getRight();
	}

	@XmlAttribute
	public String getAppId() {
		return getRemoteId();
	}
}
