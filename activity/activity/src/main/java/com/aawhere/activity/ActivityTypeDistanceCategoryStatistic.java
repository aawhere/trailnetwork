package com.aawhere.activity;

import java.util.SortedSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;

@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = "activityTypeDistanceCategoryStatistic")
public class ActivityTypeDistanceCategoryStatistic
		extends CategoricalStatistic<ActivityTypeDistanceCategory> {

	@XmlElement
	@Field
	private ActivityTypeDistanceCategory distanceCategory;

	/**
	 * Used to construct all instances of ActivityTypeDistanceCategoryStatistic.
	 */
	public static class Builder
			extends
			CategoricalStatistic.Builder<ActivityTypeDistanceCategoryStatistic, ActivityTypeDistanceCategory, Builder> {

		public Builder() {
			super(new ActivityTypeDistanceCategoryStatistic());
		}

		@Override
		protected void validate() {
			Assertion.exceptions().notNull("distanceCategory", building.distanceCategory);

			super.validate();
		}

		@Override
		public ActivityTypeDistanceCategoryStatistic build() {
			ActivityTypeDistanceCategoryStatistic built = super.build();

			return built;
		}

		@Override
		public Builder category(ActivityTypeDistanceCategory category) {
			building.distanceCategory = category;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTypeDistanceCategoryStatistic */
	private ActivityTypeDistanceCategoryStatistic() {
	}

	@Override
	public ActivityTypeDistanceCategory category() {
		return distanceCategory();
	}

	/** @see #distanceCategory */
	public ActivityTypeDistanceCategory distanceCategory() {
		return this.distanceCategory;
	}

	private static final long serialVersionUID = -349737370014640280L;

	public static
			CategoricalStatsCounter.Builder<ActivityTypeDistanceCategoryStatistic, ActivityTypeDistanceCategory, Builder>
			counter(Iterable<ActivityTypeDistanceCategory> categories) {
		CategoricalStatsCounter.Builder<ActivityTypeDistanceCategoryStatistic, ActivityTypeDistanceCategory, ActivityTypeDistanceCategoryStatistic.Builder> builder = CategoricalStatsCounter
				.create();
		builder.categories(categories);
		builder.statsBuilder(builderSupplier());

		return builder;
	}

	public static Supplier<Builder> builderSupplier() {
		return new Supplier<ActivityTypeDistanceCategoryStatistic.Builder>() {

			@Override
			public Builder get() {
				return create();
			}
		};
	}

	/**
	 * Copies the {@link ActivityType}, {@link ActivityTypeStatistic#count()} and
	 * {@link ActivityTypeStatistic#total()} from the {@link ActivityTypeDistanceCategoryStatistic}
	 * and converts it to an {@link ActivityTypeDistanceCategoryStatistic} corresponding to the
	 * given. This is useful for reporting of fixed-length data structures that already have the
	 * activity type statistic calculated.
	 * 
	 * @param distance
	 * @param stats
	 * @return
	 */
	public static SortedSet<ActivityTypeDistanceCategoryStatistic> distanceCategoriesFromActivityTypes(Length distance,
			SortedSet<ActivityTypeStatistic> stats) {
		Iterable<ActivityTypeDistanceCategoryStatistic> transformed = Iterables
				.transform(stats, distanceStatFromActivityTypeStatFunction(distance));
		return ImmutableSortedSet.<ActivityTypeDistanceCategoryStatistic> naturalOrder().addAll(transformed).build();
	}

	public static Function<ActivityTypeStatistic, ActivityTypeDistanceCategoryStatistic>
			distanceStatFromActivityTypeStatFunction(@Nonnull final Length distance) {
		return new Function<ActivityTypeStatistic, ActivityTypeDistanceCategoryStatistic>() {

			@Override
			public ActivityTypeDistanceCategoryStatistic apply(ActivityTypeStatistic activityTypeStat) {
				return distanceStatistic(activityTypeStat, distance);
			}
		};
	}

	/**
	 * Copies all information from the given into the the corresponding
	 * {@link ActivityTypeDistanceCategory} complete with the correct
	 * {@link ActivityTypeDistanceCategory#valueOf(ActivityType, Length)}.
	 * 
	 * The distance category will provide a default distance category if one is not provided for the
	 * specific activity type. If you wish to have null then passing an option to return null in
	 * such a case would work fine.
	 * 
	 * @param activityTypeStat
	 * @param distance
	 * @return
	 */
	@Nullable
	public static ActivityTypeDistanceCategoryStatistic distanceStatistic(
			@Nullable ActivityTypeStatistic activityTypeStat, @Nonnull final Length distance) {
		if (activityTypeStat == null) {
			return null;
		}
		ActivityTypeDistanceCategory distanceCategory = ActivityTypeDistanceCategory
				.valueOfWithDefault(activityTypeStat.category(), distance);
		return ActivityTypeDistanceCategoryStatistic.create().copyDifferent(activityTypeStat)
				.category(distanceCategory).build();
	}
}
