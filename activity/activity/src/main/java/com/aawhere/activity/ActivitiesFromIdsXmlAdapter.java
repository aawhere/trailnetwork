/**
 * 
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.id.IdentifierUtils;

import com.google.common.base.Function;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class ActivitiesFromIdsXmlAdapter
		extends XmlAdapter<Activities, Iterable<ActivityId>> {

	final private ActivityProvider provider;

	/**
	 * 
	 */
	public ActivitiesFromIdsXmlAdapter() {
		this.provider = null;
	}

	@Inject
	public ActivitiesFromIdsXmlAdapter(ActivityProvider provider) {
		this.provider = provider;
	}

	public ActivitiesFromIdsXmlAdapter(Function<ActivityId, Activity> provider) {
		this.provider = new ActivityProvider.Default(provider);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Iterable<ActivityId> unmarshal(Activities activities) throws Exception {
		return (activities != null) ? IdentifierUtils.ids(activities) : null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Activities marshal(Iterable<ActivityId> ids) throws Exception {
		return (ids != null && this.provider != null) ? provider.getActivities(ids) : null;
	}

}
