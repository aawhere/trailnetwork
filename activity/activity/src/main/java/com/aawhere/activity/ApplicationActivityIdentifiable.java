/**
 * 
 */
package com.aawhere.activity;

/**
 * Any entity able to be identified by the {@link ApplicationActivityId}.
 * 
 * @author aroller
 * @deprecated "use
 * 
 */
@Deprecated
public interface ApplicationActivityIdentifiable {

	ApplicationActivityId getApplicationActivityId();
}
