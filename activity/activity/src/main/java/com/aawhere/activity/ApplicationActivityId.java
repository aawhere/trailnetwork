/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.ApplicationKey;

/**
 * The identifier that an {@link Application} uses to uniquely identify an activity within their own
 * system. This, when combined with an Application becomes a complete which is a autonomous, global
 * identifier.
 * 
 * The {@link #getValue()} is a combination of the {@link ApplicationKey} and the idValue that the
 * {@link Application} uses to uniquely identify an {@link Activity}. The {@link ApplicationKey} is
 * unique within our system so the combination of the two universally identifies an Activity
 * uniquely.
 * 
 * @deprecated {@link ActivityId} is now the Application Activity ID.
 * 
 * @author Aaron Roller
 * 
 */
public interface ApplicationActivityId
		extends ApplicationDataId, ActivityIdentifier {

	/**
	 * @return the applicationKey
	 */
	public ApplicationKey getApplicationKey();

	/**
	 * The ID that the application, identifieid by {@link #applicationKey}, uses to reference the
	 * activity.
	 * 
	 * @return
	 */
	public String getRemoteId();

}
