package com.aawhere.activity;

import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.lang.If;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.Person;
import com.aawhere.person.PersonDescriptionMessage;
import com.aawhere.person.PersonProvider;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.VerbTense;

public class RelativeStatisticDescriptionPersonalizer {

	/**
	 * Utility method that provides standard {@link RelativeStatisticMessage.Param} values with
	 * corresponding values into messages. The actual message key must be provided to the resulting
	 * message builder based on the context, typically of the {@link RelativeStatistic} feature.
	 * 
	 * This may be too optimistic thinking a utility method can handle all situations required to
	 * describe relative statistics so expand it into an instance helper if customizations are
	 * needed (rather than created 10 methods with varying parameters).
	 * 
	 * Standard parameters will be provided so you should add them to the sentence in the
	 * appropriate place.
	 * 
	 * <ul>
	 * <li>PersonDescriptionMessage.Param.PERSON - the name of the person or a general description
	 * of a person (beginning a sentence). ex: Aaron Roller or One person or Multiple people, etc</li>
	 * <li>ActivityType.Param.ACTIVITY - the past tense verb of the activity type. ex: mountain
	 * biked, ran, completed</li>
	 * <li>ActivityType.Param.COUNT - provided to ActivityType to indicate the number of Activities
	 * related</li>
	 * <li></li>
	 * </ul>
	 * 
	 * If the layer is all then there is no relative message, although if it makes sense to have a
	 * message for all then it can be added.
	 * 
	 * @param relativeStatistic
	 * @param personProvider
	 * @return the CompleteMessage.Builder with the params assigned (or null if layer is ALL).
	 */
	public static CompleteMessage.Builder params(RelativeStatistic relativeStatistic, PersonProvider personProvider) {
		Layer layer = relativeStatistic.layer();
		// nothing relative to talk about for the ALL filter.
		if (!Layer.ALL.equals(layer)) {
			// the messsages are custom, but share some parameters so we work from top to
			// bottom
			CompleteMessage.Builder relativeStatisticMessage = new CompleteMessage.Builder();
			Integer count = relativeStatistic.count();
			if (layer.personal) {

				Person person;
				try {
					person = personProvider.person(relativeStatistic.personId());
				} catch (EntityNotFoundException e) {
					// this is unexpected. just treat the person as identity not shared
					person = null;
					LoggerFactory.getLogger(RelativeStatisticDescriptionPersonalizer.class)
							.warning(relativeStatistic.personId() + " is not found for " + relativeStatistic.id());
				}
				CompleteMessage personMessge = PersonDescriptionMessage.personStartingASentence(person);
				relativeStatisticMessage.addParameter(PersonDescriptionMessage.Param.PERSON, personMessge);
			}
			// activity type is added always the default giving a general action
			ActivityType activityType = If.nil(relativeStatistic.activityType()).use(ActivityType.U);
			CompleteMessage activityMessage = CompleteMessage.create(activityType.verb)
					.param(VerbTense.Param.TENSE, VerbTense.PAST).build();
			relativeStatisticMessage.param(ActivityType.Param.ACTIVITY, activityMessage);
			return relativeStatisticMessage;
		} else {
			return null;
		}
	}
}
