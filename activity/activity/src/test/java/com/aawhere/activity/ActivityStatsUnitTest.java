/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationStats;
import com.aawhere.app.ApplicationTestUtil;
import com.aawhere.measure.calc.CategoricalStatsCounter;

import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class ActivityStatsUnitTest {

	/**
	 * Typically the count of activity types and applications must be equal, but in the cases of
	 * filtering the totals disagree. Always assume the larger is more correct.
	 * 
	 */
	@Test
	public void testDifferentTotal() {
		Integer numberOfActivityTypes = 5;
		Iterable<ActivityType> activityTypes = ActivityTestUtil.activityTypes(Range.singleton(numberOfActivityTypes));
		CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> activityTypesCounter = ActivityTypeStatistic
				.counter(activityTypes).build();
		assertEquals(numberOfActivityTypes, activityTypesCounter.total());

		Integer numberOfApplications = numberOfActivityTypes + 1;
		Iterable<Application> applications = ApplicationTestUtil.applications(numberOfApplications);
		CategoricalStatsCounter<ApplicationStats, Application> applicationCounter = ApplicationStats
				.counter(applications);
		assertEquals(numberOfApplications, applicationCounter.total());
		ActivityStats activityStats = ActivityStats.create().activityTypeCounter(activityTypesCounter)
				.applicationCounter(applicationCounter).build();
		assertEquals(numberOfApplications, activityStats.total());
		// assign again in activity types second
		ActivityStats activityStats2 = ActivityStats.create().applicationCounter(applicationCounter)
				.activityTypeCounter(activityTypesCounter).build();
		assertEquals(numberOfApplications, activityStats2.total());
	}
}
