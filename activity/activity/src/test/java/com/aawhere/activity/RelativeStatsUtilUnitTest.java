package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Set;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.test.TestUtil;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class RelativeStatsUtilUnitTest {

	@Test
	public void testPersonalFiltered() {
		RelativeStatistic all = RelativeStatsTestUtil.allBuilder().build();
		// notice the null is added in there. as long as we are filtering...
		ArrayList<RelativeStatistic> stats = Lists.newArrayList(all, RelativeStatsTestUtil.personActivityType(), null);
		Iterable<RelativeStatistic> nonPersonal = RelativeStatsUtil.personalExcluded(stats);
		TestUtil.assertContainsOnly(all, nonPersonal);

	}

	@Test
	public void testAllStats() {
		PersonId personId = null;
		ActivityType activityType = null;
		Length distance = null;
		assertStats(personId, activityType, distance, Layer.ALL);
	}

	@Test
	public void testPersonStats() {
		PersonId personId = PersonTestUtil.personId();
		ActivityType activityType = null;
		Length distance = null;
		assertStats(personId, activityType, distance, Layer.ALL, Layer.PERSON);
	}

	@Test
	public void testPersonActivityTypeStats() {
		PersonId personId = PersonTestUtil.personId();
		ActivityType activityType = ActivityTestUtil.activityType();
		Length distance = null;
		assertStats(personId,
					activityType,
					distance,
					Layer.ALL,
					Layer.PERSON,
					Layer.ACTIVITY_TYPE,
					Layer.PERSON_ACTIVITY_TYPE);
	}

	@Test
	public void testPersonActivityTypeDistanceStats() {
		PersonId personId = PersonTestUtil.personId();
		ActivityType activityType = ActivityType.BIKE;
		Length distance = MeasureTestUtil.createRandomLength();
		assertStats(personId, activityType, distance, Layer.values());
	}

	@Test
	public void testActivityTypeStats() {
		PersonId personId = null;
		ActivityType activityType = ActivityTestUtil.activityType();
		Length distance = null;
		assertStats(personId, activityType, distance, Layer.ALL, Layer.ACTIVITY_TYPE);
	}

	@Test
	public void testActivityTypeDistanceStats() {
		PersonId personId = null;
		ActivityType activityType = ActivityTestUtil.activityType();
		Length distance = MeasureTestUtil.createRandomLength();
		assertStats(personId, activityType, distance, Layer.ALL, Layer.ACTIVITY_TYPE, Layer.ACTIVITY_TYPE_DISTANCE);
	}

	/** For those distance categories not defined...the default must be used. */
	@Test
	public void testActivityTypeDistanceStatsDefaultValue() {
		PersonId personId = null;
		ActivityType activityType = ActivityType.SWIM;
		Length distance = MeasureTestUtil.createRandomLength();
		assertStats(personId, activityType, distance, Layer.ALL, Layer.ACTIVITY_TYPE, Layer.ACTIVITY_TYPE_DISTANCE);
	}

	private void assertStats(PersonId personId, ActivityType activityType, Length distance, Layer... expectedLayer) {
		Set<Layer> expectedLayers = ImmutableSet.<Layer> builder().add(expectedLayer).build();
		ImmutableList<RelativeStatistic> relativeStats = RelativeStatsUtil.relativeStats(	ActivityTestUtil
																									.activityId(),
																							personId,
																							activityType,
																							distance);
		ImmutableMap<Layer, RelativeStatistic> layerMap = Maps.uniqueIndex(	relativeStats,
																			RelativeStatsUtil.layerFunction());
		assertEquals(expectedLayers, layerMap.keySet());

		for (RelativeStatistic relativeStatistic : relativeStats) {

			Layer layer = relativeStatistic.layer();
			switch (layer) {
				case PERSON_ACTIVITY_TYPE_DISTANCE:
					if (relativeStatistic.distance() == null) {
						throw new IllegalStateException(relativeStatistic + " has a null distance");
					}
					MeasureTestUtil.assertRange(relativeStatistic.distance().range, distance);
					// intentional not breaking to allow common testing
				case PERSON_ACTIVITY_TYPE:
					assertEquals(activityType, relativeStatistic.activityType());
					// intentional pass on
				case PERSON:
					assertEquals(personId, relativeStatistic.personId());
					break;
				case ACTIVITY_TYPE:
					assertEquals(activityType, relativeStatistic.activityType());
				case ACTIVITY_TYPE_DISTANCE:
					assertEquals(activityType, relativeStatistic.activityType());
					break;
				case ALL:
					break;
				default:
					fail(layer + " unexpected from the documents allowing only personal ");
					break;
			}
		}
	}

	@Test
	public void testLayerAuto() {
		assertEquals(Layer.ALL, RelativeStatsUtil.layer(false, false, false));
		assertEquals(Layer.ACTIVITY_TYPE, RelativeStatsUtil.layer(false, true, false));
		assertEquals(Layer.ACTIVITY_TYPE_DISTANCE, RelativeStatsUtil.layer(false, true, true));
		assertEquals(Layer.PERSON_ACTIVITY_TYPE, RelativeStatsUtil.layer(true, true, false));
		assertEquals(Layer.PERSON_ACTIVITY_TYPE_DISTANCE, RelativeStatsUtil.layer(true, true, true));
		assertEquals(	"ensures that distance implies activity type",
						Layer.ACTIVITY_TYPE_DISTANCE,
						RelativeStatsUtil.layer(false, false, true));
		assertEquals(Layer.PERSON_ACTIVITY_TYPE_DISTANCE, RelativeStatsUtil.layer(true, false, true));
	}

	@Test
	public void testPersonalLayerFunctionPredicate() {
		assertPersonalPredicate(true, Layer.PERSON);
		assertPersonalPredicate(true, Layer.PERSON_ACTIVITY_TYPE);
		assertPersonalPredicate(true, Layer.PERSON_ACTIVITY_TYPE_DISTANCE);
		assertPersonalPredicate(false, Layer.ALL);
		assertPersonalPredicate(false, Layer.ACTIVITY_TYPE);
		assertPersonalPredicate(false, Layer.ACTIVITY_TYPE_DISTANCE);
	}

	private Predicate<RelativeStatistic> assertPersonalPredicate(Boolean expectedTrue, Layer layer) {
		Predicate<RelativeStatistic> personalOnlyPredicate = RelativeStatsUtil.personalOnlyPredicate();
		assertEquals(	layer.toString(),
						expectedTrue,
						personalOnlyPredicate.apply(RelativeStatsTestUtil.allBuilder().layer(layer).build()));
		return personalOnlyPredicate;
	}
}
