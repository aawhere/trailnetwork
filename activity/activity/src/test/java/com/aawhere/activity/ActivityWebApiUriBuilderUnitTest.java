/**
 * 
 */
package com.aawhere.activity;

import org.junit.Test;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class ActivityWebApiUriBuilderUnitTest {

	@Test
	public void testGetByApplicationId() {
		ApplicationKey expectedApplication = KnownApplication.TRAIL_NETWORK.key;
		ActivityId expectedAId = new ActivityId(expectedApplication, "1234567");
		String actual = ActivityWebApiUri.getActivityByApplicationId(expectedAId).toString();
		TestUtil.assertContains(actual, expectedApplication.getValue());
		TestUtil.assertEndsWith(actual, expectedAId.getRemoteId());
		TestUtil.assertStartsWith(actual, "/");
		TestUtil.assertContains(actual, expectedAId.getValue());
	}

}
