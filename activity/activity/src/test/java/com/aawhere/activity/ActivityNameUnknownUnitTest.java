/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

/**
 * Tests the persistence of an activity when Activity name is not provided.
 * 
 * @author Aaron Roller
 * 
 */
public class ActivityNameUnknownUnitTest
		extends ActivityUnitTest {

	@Override
	protected void creationPopulation(com.aawhere.activity.Activity.Builder builder) {
		super.creationPopulation(builder);
		//
		builder.setName(null);
	};

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityUnitTest#assertCreation(com.aawhere.activity.Activity)
	 */
	@Override
	public void assertCreation(Activity sample) {
		// do not call parent assert...it will fail
		assertEquals(ActivityMessage.UNKNOWN, sample.getName());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertStored(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertStored(Activity sample) {
		// Unknown values shouldn't be stored...null is fine.
		assertNull(sample.getName());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertLoadAfterStore(com.aawhere.persist.BaseEntity
	 * )
	 */
	@Override
	protected void assertLoadAfterStore(Activity found) {
		// not calling super on purpose
		assertCreation(found);
	}
}
