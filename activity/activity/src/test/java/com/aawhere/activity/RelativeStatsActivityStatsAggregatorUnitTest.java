package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.SortedSet;

import org.junit.Test;

import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonTestUtil;
import com.google.common.collect.ImmutableList;

public class RelativeStatsActivityStatsAggregatorUnitTest {

	@Test
	public void testActivityStats() {

		RelativeStatistic all = RelativeStatsTestUtil.allBuilder().count(10).build();

		// activity types
		RelativeStatistic bike = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.BIKE).count(4).build();
		RelativeStatistic run = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.RUN).count(3).build();
		RelativeStatistic walk = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.WALK).count(3).build();

		// distances
		RelativeStatistic longBike = RelativeStatsTestUtil.distance(ActivityTypeDistanceCategory.LONG_BIKE).count(2)
				.build();
		RelativeStatistic shortBike = RelativeStatsTestUtil.distance(ActivityTypeDistanceCategory.SHORT_BIKE).count(2)
				.build();
		// notice this has the most
		RelativeStatistic shortRun = RelativeStatsTestUtil.distance(ActivityTypeDistanceCategory.FIVE_K_RUN).count(3)
				.build();
		RelativeStatistic shortWalk = RelativeStatsTestUtil.distance(ActivityTypeDistanceCategory.SHORT_WALK).count(2)
				.build();
		RelativeStatistic longWalk = RelativeStatsTestUtil.distance(ActivityTypeDistanceCategory.LONG_WALK).count(1)
				.build();

		ImmutableList<RelativeStatistic> list = ImmutableList.of(	walk,
																	run,
																	bike,
																	longBike,
																	shortBike,
																	shortRun,
																	shortWalk,
																	longWalk,
																	all);
		RelativeStatsActivityStatsAggregator<ActivityId> aggregator = RelativeStatsActivityStatsAggregator
				.create(ActivityTestUtil.activityId()).statsIterator(list.iterator()).build();
		ActivityStats activityStats = aggregator.activityStats();
		assertEquals(all.count(), activityStats.total());

		SortedSet<ActivityTypeStatistic> activityTypeStats = activityStats.activityTypeStats();
		assertEquals("wrong activity type count", 3, activityTypeStats.size());
		ActivityTypeStatistic first = activityTypeStats.first();
		assertEquals(bike.activityType(), first.category());
		assertEquals(bike.count(), first.count());
		assertNull("no person layer was provided", aggregator.personStats());

		// distance
		SortedSet<ActivityTypeDistanceCategoryStatistic> distanceStats = activityStats.distanceStats();
		assertNotNull(distanceStats);
		ActivityTypeDistanceCategoryStatistic firstDistanceStat = distanceStats.first();
		assertEquals("long run has the most completions", shortRun.distance(), firstDistanceStat.category());
		assertEquals("long walk only has 1", longWalk.distance(), distanceStats.last().category());
	}

	@Test
	public void testActivityStatsAndPersons() {
		// these totals add up. it all matters
		RelativeStatistic all = RelativeStatsTestUtil.allBuilder().count(10).build();
		RelativeStatistic bike = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.BIKE).count(7).build();
		RelativeStatistic run = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.RUN).count(3).build();

		PersonId person1 = PersonTestUtil.personId();
		PersonId person2 = PersonTestUtil.personId();

		RelativeStatistic person1Run = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.RUN).personId(person1)
				.layer(Layer.PERSON_ACTIVITY_TYPE).count(3).build();
		RelativeStatistic person1Bike = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.BIKE).personId(person1)
				.layer(Layer.PERSON_ACTIVITY_TYPE).count(3).build();
		RelativeStatistic person2Bike = RelativeStatsTestUtil.activityTypeBuilder(ActivityType.BIKE).personId(person2)
				.layer(Layer.PERSON_ACTIVITY_TYPE).count(4).build();

		RelativeStatistic person1All = RelativeStatsTestUtil.allBuilder().personId(person1).layer(Layer.PERSON)
				.count(person1Bike.count() + person1Run.count()).build();
		RelativeStatistic person2All = RelativeStatsTestUtil.allBuilder().personId(person1).layer(Layer.PERSON)
				.count(person1Bike.count() + person2Bike.count()).build();

		ImmutableList<RelativeStatistic> relativeStatsList = ImmutableList.of(	person1Run,
																				person1Bike,
																				person2Bike,
																				person1All,
																				person2All,
																				all,
																				bike,
																				run);

		RelativeStatsActivityStatsAggregator<ActivityId> aggregator = RelativeStatsActivityStatsAggregator
				.create(ActivityTestUtil.activityId()).statsIterator(relativeStatsList.iterator()).build();
		ActivityStats activityStats = aggregator.activityStats();
		assertEquals("total is wrong", all.count(), activityStats.total());
		assertEquals(bike.activityType(), activityStats.activityTypeStats().first().category());
		assertEquals(run.activityType(), activityStats.activityTypeStats().last().category());
		ActivityStats personStats = aggregator.personStats();
		assertNotNull("person is missing", personStats);
		assertEquals("number of people", 2, personStats.total().intValue());
		ActivityTypeStatistic first = personStats.activityTypeStats().first();
		assertEquals("biking is more popular", ActivityType.BIKE, first.category());
		assertEquals("wrong number of people for biking", 2, first.count().intValue());
		ActivityTypeStatistic last = personStats.activityTypeStats().last();
		assertEquals("biking is more popular", ActivityType.RUN, last.category());
		assertEquals("wrong number of people for run", 1, last.count().intValue());
	}
}
