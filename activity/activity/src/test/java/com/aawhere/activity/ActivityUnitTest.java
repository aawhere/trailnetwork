/**
 *
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity.Builder;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.Device;
import com.aawhere.app.IdentityHistory;
import com.aawhere.app.IdentityHistoryUnitTest;
import com.aawhere.app.Installation;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.person.Person;
import com.aawhere.person.PersonUnitTest;
import com.aawhere.test.TestUtil;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackSummaryTestUtil;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessageTestUtil;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Simple tests against {@link Activity}.
 * 
 * @author roller
 * 
 */
public class ActivityUnitTest
		extends BaseEntityJaxbBaseUnitTest<Activity, Activity.Builder, ActivityId, ActivityRepository> {

	private String nameValue = TestUtil.generateRandomString();
	private Message name = new StringMessage(nameValue);
	private ActivityType activityType = ActivityTestUtil.activityType();
	private IdentityHistoryUnitTest historyUnitTest;
	private Account account;
	private ActivityId applicationActivityId;
	private IdentityHistory history;
	private TrackSummary trackSummary;
	private SignalQuality signalQuality;
	private Device device;

	/**
	 *
	 */
	public ActivityUnitTest() {
		super(NON_MUTABLE);
	}

	@Override
	@Before
	public void setUp() {
		this.historyUnitTest = IdentityHistoryUnitTest.getInstance();
		this.history = historyUnitTest.getEntitySample();
		this.device = history.getDevice();
		Installation installation = history.getInstallations().get(0);
		this.applicationActivityId = new ActivityId(installation.getApplication().getKey(),
				installation.getExternalId());
		this.account = AccountTestUtil.account();
		this.trackSummary = TrackSummaryTestUtil.getSampleTrackSummary();
		this.signalQuality = SignalQuality.GOOD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#generateId(com.aawhere.persist.BaseEntity.Builder)
	 */
	@Override
	protected ActivityId getId() {
		return applicationActivityId;
	}

	/**
	 * Useful for children tests to override and provide an appropriate person to match their
	 * requirements (such as persisted).
	 * 
	 * @return
	 */
	protected Person createPerson() {
		return PersonUnitTest.instance().getEntitySampleWithId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere
	 * .persist.BaseEntity.Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.setName(name);
		builder.setDescription(nameValue);
		builder.setActivityType(activityType);
		builder.setTrackSummary(trackSummary);
		builder.setHistory(history);
		builder.setDevice(device);
		// create person is called here instead of setUp to avoid causing problems for Objectify
		// helper setup.
		builder.setAccount(account);
		builder.setId(applicationActivityId);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulationWithId(com .aawhere
	 * .persist.BaseEntity.Builder)
	 */
	@Override
	protected void creationPopulationWithId(Builder builder) {
		// Calls #createionPopulation()
		super.creationPopulationWithId(builder);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	public void assertCreation(Activity sample) {
		super.assertCreation(sample);
		assertEquals(name, sample.getName());
		super.assertCreation(sample);
		historyUnitTest.assertHistory(sample.getHistory());
		assertEquals(activityType, sample.getActivityType());
		assertEquals(this.applicationActivityId.getValue(), sample.getId().getValue());
		assertEquals(account.getId(), sample.getAccountId());
		assertNotNull("summary", sample.getTrackSummary());
		assertEquals(device, sample.getDevice());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityJaxbBaseUnitTest#assertJaxb(com.aawhere.xml.bind.JAXBTestUtil)
	 */
	@Override
	protected void assertJaxb(JAXBTestUtil<Activity> util) {
		super.assertJaxb(util);
		util.assertContains(Activity.FIELD.PERSON);
		util.assertContains(Activity.FIELD.ACCOUNT);
		util.assertContains(Activity.FIELD.APPLICATION_REFERENCE);
	}

	/**
	 * Makes sure the finder for an application's id works for the repository being tested.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testFindByExternalId() throws EntityNotFoundException {
		Activity expected = getEntitySample();
		final ActivityRepository repo = getRepository();
		repo.store(expected);
		Installation installation = this.historyUnitTest.getFirstInstallation().getInstallation();
		final String externalId = installation.getExternalId();
		assertNotNull(externalId);
		ApplicationKey applicationKey = installation.getApplication().getKey();
		assertEquals(	"makes sure the external Id is retrievable",
						externalId,
						expected.getHistory().getByExternalId(applicationKey, externalId).getExternalId());
		ActivityId applicationActivityId = new ActivityId(applicationKey, externalId);

		Activity actual = repo.load(applicationActivityId);
		assertEquals("problem searching for " + applicationActivityId.toString(), expected, actual);

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.l.Id)
	 */
	@Override
	protected Activity mutate(ActivityId id) throws EntityNotFoundException {

		Activity sample = super.mutate(id);
		return sample;
		/*
		 * TN-344 killed history Installation installation =
		 * InstallationUnitTest.getInstance().getInstallation(); this.historySizeBeforeMutation =
		 * sample.getHistory().getInstallations().size(); return
		 * getRepository().appendToHistory(sample.getId(), installation);
		 */
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(Activity mutated) {

		super.assertMutation(mutated);
		// assertEquals(this.historySizeBeforeMutation + 1,
		// mutated.getHistory().getInstallations().size());
	}

	@Test(expected = EntityNotFoundException.class)
	public void testFindByBadExternalId() throws EntityNotFoundException {
		ApplicationKey applicationKey = KnownApplication.GARMIN_CONNECT.key;
		final ActivityId badId = new ActivityId(applicationKey, "-1");
		getRepository().load(badId);

		fail(badId.toString() + " shouldn't be found");

	}

	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return ActivityTestUtil.adapters();
	}

	/** mutates this test to use the given activity id instead of the auto-generated. */
	public void setActivityId(ActivityId activityId) {
		// TODO:replace with the activityId when ApplicaitonActivityId is replaced
		this.applicationActivityId = new ActivityId(activityId.toString());
	}

	public static ActivityUnitTest getInstance() {
		ActivityUnitTest instance = new ActivityUnitTest();
		instance.baseSetUp();
		instance.setUp();
		return instance;
	}

	public static ActivityUnitTest create(TrackSummary trackSummary) {
		final ActivityUnitTest instance = getInstance();
		instance.trackSummary = trackSummary;
		return instance;
	}

	@Test
	public void testApplicationReference() {
		JAXBTestUtil<Activity> testUtil = JAXBTestUtil.create(getEntitySample()).adapter(getAdapters()).build();
		testUtil.assertContains(ActivityTestUtil.FAKE_URL_BASE);
		testUtil.assertContains("?");
	}

	@Test
	public void testMessage() {
		MessageTestUtil.assertMessages(ActivityMessage.class);
	}

	public static class ActivityMockRepository
			extends MockFilterRepository<Activities, Activity, ActivityId>
			implements ActivityRepository {

		/**
		 *
		 */
		public ActivityMockRepository() {
			super();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityRepository#appendToHistory(com.aawhere
		 * .activity.ActivityId, com.aawhere.app.Installation)
		 */
		@Override
		public Activity appendToHistory(ActivityId id, Installation installation) throws EntityNotFoundException {
			return appendToHistory(load(id), installation);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityRepository#appendToHistory(com.aawhere
		 * .activity.Activity, com.aawhere.app.Installation)
		 */
		@Override
		public Activity appendToHistory(Activity activity, Installation installation) {
			return update(new Activity.Builder(activity).appendToHistory(installation).build());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityRepository#getDictionary()
		 */
		@Override
		public FieldDictionary getDictionary() {
			return FieldAnnotationTestUtil.getAnnoationFactory().getDictionary(Activity.class);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityRepository#gone(com.aawhere.activity.ActivityId)
		 */
		@Override
		public Activity gone(ActivityId activityId) throws EntityNotFoundException {
			return update(Activity.mutate(load(activityId)).setGone(true).build());
		}

	}

	@Test
	public void testActivityReferenceIds() {
		ActivityReferenceIds referenceIds = ActivityTestUtil.createActivityReferenceIds();
		assertNotNull(referenceIds.getActivityId());
		assertNotNull(referenceIds.getAccountId());
	}

	/**
	 * TN-623 reintroduced bounding box from the calculator for storage. Make sure it is being
	 * stored.
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	public void testBoundingBoxPreserved() throws EntityNotFoundException {
		BoundingBox fake = BoundingBoxTestUtils.createRandomBoundingBox();
		this.trackSummary = TrackSummary.mutate(trackSummary).setBoundingBox(fake).build();
		Activity sample = getEntitySample();
		BoundingBoxTestUtils.assertEquals(fake, sample.boundingBox());
		Activity stored = getRepository().store(sample);
		BoundingBoxTestUtils.assertEquals(fake, stored.boundingBox());
		Activity found = getRepository().load(stored.id());
		BoundingBoxTestUtils.assertEquals(fake, found.boundingBox());
	}

	/**
	 * @param device
	 */
	public ActivityUnitTest setDevice(Device device) {
		this.history = historyUnitTest.getEntitySampleWithDevice(device);
		this.device = device;
		return this;
	}

	public ActivityUnitTest setSignalQuality(SignalQuality quality) {
		TrackSummary ts = TrackSummary.mutate(this.trackSummary).setSignalQuality(quality).build();
		this.trackSummary = ts;
		return this;
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected ActivityRepository createRepository() {
		return new ActivityMockRepository();
	}
}
