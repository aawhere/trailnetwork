/**
 *
 */
package com.aawhere.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.activity.Activities.Builder;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationStats;
import com.aawhere.app.ApplicationTestUtil;
import com.aawhere.app.Device;
import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.app.DeviceTestUtil;
import com.aawhere.app.KnownApplication;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceTestUtilBase;
import com.aawhere.person.Person;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackSummaryTestUtil;
import com.aawhere.track.TrackSummaryUnitTest;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.story.TrackSummaryUserStoriesTestUtil;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageFormatter;
import com.aawhere.util.rb.MessagePersonalizerUnitTest;
import com.aawhere.util.rb.VerbTense;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import com.google.inject.Singleton;

/**
 * @author Brian Chapman
 * 
 */
@Singleton
public class ActivityTestUtil
		extends ServiceTestUtilBase<Activities, Activity, ActivityId>
		implements ActivityProvider {

	/**
	 * 
	 */
	public static final String FAKE_URL_BASE = "http://activity.test.util/";

	public ActivityTestUtil() {
	}

	public static ActivityId createRandomActivityId() {
		return new ActivityId(KnownApplication.UNKNOWN.key, TestUtil.generateRandomAlphaNumeric());
	}

	public static Activity createActivity() {
		return ActivityUnitTest.getInstance().getEntitySample();
	}

	public static Activity createActivity(ActivityId id) {
		final ActivityUnitTest instance = ActivityUnitTest.getInstance();
		instance.setActivityId(id);
		return instance.getEntitySample();
	}

	public static Activity createActivity(Device device) {
		final ActivityUnitTest instance = ActivityUnitTest.getInstance();
		instance.setDevice(device);
		return instance.getEntitySample();
	}

	public static Activity createActivityWithDevice(ApplicationKey deviceKey) {
		return createActivity(DeviceTestUtil.device(deviceKey));
	}

	public static Activity createActivityWithSignalAndDevice(SignalQuality signal, ApplicationKey deviceKey) {
		final ActivityUnitTest instance = ActivityUnitTest.getInstance();
		instance.setDevice(DeviceTestUtil.device(deviceKey));
		instance.setSignalQuality(signal);
		return instance.getEntitySample();
	}

	public static Activity createActivityWithId() {
		return ActivityUnitTest.create(TrackSummaryTestUtil.getSampleTrackSummary()).getEntitySampleWithId();
	}

	public static Activity.Builder mutateActivity(Activity activity) {
		return Activity.mutate(activity);
	}

	/**
	 * Creates a realistic activity given a track it will produce a representative track summary and
	 * assign it to the Activity.
	 * 
	 * @param track
	 * @return
	 */
	public static Activity createActivity(Iterable<Trackpoint> track) {
		TrackSummary trackSummary = TrackSummaryUserStoriesTestUtil.create(track);
		return createActivity(trackSummary);
	}

	/**
	 * @param trackSummary
	 * @return
	 */
	public static Activity createActivity(TrackSummary trackSummary) {
		return ActivityUnitTest.create(trackSummary).getEntitySampleWithId();
	}

	/**
	 * same as {@link #createApplicationActivityId()}
	 * 
	 * @return
	 */
	public static ActivityId activityId() {
		return activityId(KnownApplication.UNKNOWN.key);
	}

	/**
	 * @deprecated use {@link #activityId()}
	 * @return
	 */
	@Deprecated
	public static ActivityId createApplicationActivityId() {

		return activityId();
	}

	public static ActivityReferenceIds createActivityReferenceIds() {
		return new ActivityReferenceIds.Builder().setActivity(createActivityWithId()).build();
	}

	/**
	 * creates a random count of applicationActivityIds from zero to 10
	 * 
	 * @see #createApplicationActivityIds(int)
	 * */
	public static List<ActivityId> createApplicationActivityIds() {
		return createApplicationActivityIds(TestUtil.generateRandomInt(0, 10));
	}

	public static Set<ActivityId> createActivityIds() {
		return Sets.newHashSet(createApplicationActivityIds());
	}

	/**
	 * @see #createApplicationActivityId()
	 * @param count
	 * @return
	 */
	public static List<ActivityId> createApplicationActivityIds(int count) {
		ArrayList<ActivityId> ids = new ArrayList<ActivityId>();
		for (int i = 0; i < count; i++) {
			ids.add(createApplicationActivityId());
		}
		return ids;
	}

	/**
	 * Generates random activities ranging from 1 to 10.
	 * 
	 * @see #createActivity()
	 * @return
	 */
	public static Activities createActivities() {
		Builder builder = Activities.create();
		int count = TestUtil.generateRandomInt(1, 10);
		for (int i = 0; i < count; i++) {
			builder.add(createActivity());

		}
		return builder.build();
	}

	/**
	 * A generic implementation of {@link ActivityProvider} that returns random activities with the
	 * Id provided.
	 * 
	 * @return
	 */
	public static ActivityProvider createActivityProvider() {
		return new ActivityTestUtil();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityProvider#getActivity(com.aawhere.activity.ActivityId)
	 */
	@Override
	public Activity getActivity(ActivityId activityId) throws EntityNotFoundException {
		return createActivity(activityId);
	}

	/**
	 * Provides the standard idAdapter that will create activities matching the given id using
	 * {@link #createActivity(ActivityId)}.
	 * 
	 * @return
	 */
	public static ActivityFromIdXmlAdapter idAdapter() {
		return new ActivityFromIdXmlAdapter(new ActivityTestUtil());
	}

	public static Set<XmlAdapter<?, ?>> adapters() {
		Set<XmlAdapter<?, ?>> adapters = PersonTestUtil.adapters();
		adapters.addAll(TrackSummaryUnitTest.getInstance().getAdapters());
		adapters.add(MessagePersonalizerUnitTest.getInstance().getAdapter());
		adapters.add(idAdapter());
		adapters.add(new ActivitiesFromIdsXmlAdapter(new ActivityTestUtil()));
		final ApplicationActivityReferenceXmlAdapter referenceAdapter = applicationActivityReferenceAdapter();
		adapters.add(referenceAdapter);
		adapters.add(new ApplicationActivitiesReferenceXmlAdapter(referenceAdapter));
		adapters.addAll(PersonTestUtil.adapters());
		return adapters;
	}

	/**
	 * @return
	 */
	private static ApplicationActivityReferenceXmlAdapter applicationActivityReferenceAdapter() {
		return new ApplicationActivityReferenceXmlAdapter(new ApplicationActivityUrlProvider() {

			@Override
			public String applicationUrl(ActivityId activityId) {
				if (activityId == null) {
					return null;
				}
				return FAKE_URL_BASE + activityId.getRemoteId() + "?something=somethingElse&thisToo";
			}
		});
	}

	/**
	 * Generates a randomly sized list with unique activity types and a random count assigned to
	 * each.
	 * 
	 * @return
	 */
	public static SortedSet<ActivityTypeStatistic> activityTypeStats() {
		final int min = 1;
		return activityTypeStats(min);
	}

	/**
	 * @param min
	 * @return
	 */
	static SortedSet<ActivityTypeStatistic> activityTypeStats(final int min) {
		TreeSet<ActivityTypeStatistic> counts = new TreeSet<>();
		final ActivityType[] allTypes = ActivityType.values();
		int numberToGenerate = TestUtil.generateRandomInt(min, allTypes.length);
		Integer total = TestUtil.generateRandomInt(1, 1000);

		for (int i = 0; i < numberToGenerate; i++) {
			final Integer count = total - (total / numberToGenerate);
			counts.add(ActivityTypeStatistic.create().count(count).total(total).type(allTypes[i]).build());
		}
		return counts;
	}

	/** Some random activity type. */
	public static ActivityType activityType() {
		return TestUtil.generateRandomEnum(ActivityType.class);
	}

	/**
	 * @param key
	 * @return
	 */
	public static ActivityId createApplicationActivityId(ApplicationKey key) {
		return new ActivityId(key, TestUtil.generateRandomAlphaNumeric());
	}

	/**
	 * Generates a random completion summary providing both application and activty types, but the
	 * same size as required.
	 * 
	 * @return
	 */
	public static ActivityStats activityStats() {
		// the totals must be equal
		final int total = TestUtil.generateRandomInt(1, 100);
		final Range<Integer> totalRange = Range.singleton(total);
		Iterable<ActivityType> activityTypes = ActivityTestUtil.activityTypes(totalRange);
		Iterable<Application> applications = ApplicationTestUtil.applications(total);
		List<ActivityTypeDistanceCategory> distanceCategories = ActivityTestUtil.distanceCategories(totalRange);
		CategoricalStatsCounter<ActivityTypeDistanceCategoryStatistic, ActivityTypeDistanceCategory> distanceCounter = ActivityTypeDistanceCategoryStatistic
				.counter(distanceCategories).build();
		return ActivityStats.create().activityTypeCounter(ActivityTypeStatistic.counter(activityTypes).build())
				.distanceCounter(distanceCounter).applicationCounter(ApplicationStats.counter(applications)).build();
	}

	/**
	 * @see TestUtil#generateRandomEnums(Class, Range)
	 * @param range
	 * @return
	 */
	public static Iterable<ActivityType> activityTypes(Range<Integer> range) {
		return TestUtil.generateRandomEnums(ActivityType.class, range);
	}

	/**
	 * @param activity
	 *            is the mutant.
	 * @param deviceKey
	 *            is the identifier of the targeted device from {@link DeviceTestUtil} constants.
	 * @return
	 */
	public static Activity setDevice(Activity activity, ApplicationKey deviceKey) {
		return mutateActivity(activity).setDevice(DeviceTestUtil.device(deviceKey)).build();
	}

	/**
	 * A test function that provides no consistency between the activity given and the person
	 * provided.
	 * 
	 * @return
	 */
	public static Function<ActivityId, Person> personProvider() {
		return personProvider(new HashMap<ActivityId, Person>());
	}

	public static Function<ActivityId, Person> personProvider(Map<ActivityId, Person> provided) {
		final HashMap<ActivityId, Person> persons = Maps.newHashMap(provided);
		return new Function<ActivityId, Person>() {

			@Override
			public Person apply(ActivityId input) {
				Person result = persons.get(input);
				if (result == null) {
					result = PersonTestUtil.personWithId();
					persons.put(input, result);
				}
				return result;
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityProvider#getActivities(java.lang.Iterable)
	 */
	@Override
	public Activities getActivities(Iterable<ActivityId> ids) {
		Builder builder = Activities.create();
		for (ActivityId activityId : ids) {
			try {
				builder.add(getActivity(activityId));
			} catch (EntityNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
		return builder.build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceTestUtilBase#entity(com.aawhere.id.Identifier)
	 */
	@Override
	public Activity entity(ActivityId id) {
		return createActivity(id);
	}

	/**
	 * Provides random distance categories, at least one and only a few. No logic applied ensuring
	 * activity types aren't repeated across categories.
	 * 
	 * @return
	 */
	public static List<ActivityTypeDistanceCategory> distanceCategories() {
		Range<Integer> range = Range.closed(1, 5);
		return distanceCategories(range);
	}

	private static List<ActivityTypeDistanceCategory> distanceCategories(Range<Integer> range) {
		return TestUtil.generateRandomEnums(ActivityTypeDistanceCategory.class, range);
	}

	/**
	 * Provides the category matching to the activity type or the default if there are no categories
	 * for the type given.
	 * 
	 * @param activityType
	 * @return
	 */
	@Nonnull
	public static ActivityTypeDistanceCategory distanceCategory(ActivityType activityType) {

		return ActivityTypeDistanceCategory.valueOfWithDefault(activityType, MeasurementUtil
				.createLengthInMeters(TestUtil.generateRandomInt(2000, 30000)));
	}

	public static String activityTypeVerb(ActivityType activityType, VerbTense verbTense) {
		return MessageFormatter.create()
				.message(CompleteMessage.create(activityType.verb).param(VerbTense.Param.TENSE, verbTense).build())
				.locale(Locale.US).build().getFormattedMessage();
	}

	public static String activityTypeDistanceCategoryAdjective(ActivityTypeDistanceCategory distanceCategory,
			Integer count) {
		return MessageFormatter
				.create()
				.message(CompleteMessage.create(distanceCategory.asAdjective).param(ActivityType.Param.COUNT, count)
						.build()).locale(Locale.US).build().getFormattedMessage();
	}

	public static ActivityId activityId(ApplicationKey key) {
		return new ActivityId(key, TestUtil.generateRandomAlphaNumeric());
	}

	public static Activity activity(ApplicationKey key) {
		return createActivity(activityId(key));
	}

}
