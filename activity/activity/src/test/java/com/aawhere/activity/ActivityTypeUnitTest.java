/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.activity.ActivityType.*;
import static org.junit.Assert.*;

import java.util.Locale;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.i18n.LocaleUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageFormatterUnitTest;
import com.aawhere.util.rb.MessageResourceBundleUnitTest;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author aroller
 * 
 */
public class ActivityTypeUnitTest {

	private static final ActivityType EXPECTED = WALK;

	@Test
	@Ignore("as a root activity type is being treated as an enum")
	public void testJaxbAsRoot() {

		final ActivityType expected = EXPECTED;
		assertJaxb(expected);
	}

	/**
	 * @param expected
	 */
	private void assertJaxb(final Object expected) {
		JAXBTestUtil<Object> util = JAXBTestUtil.create(expected).build();
		util.assertContains(EXPECTED.name());
		util.assertContains(EXPECTED.message);
		util.assertContains(EXPECTED.transportMode.name());
	}

	@Test
	@Ignore("as contained the message is being handled by MessagePersonalizer.  We must extend that framework to display all attributes")
	public
			void testJaxbElementContained() {

		assertJaxb(new ElementContainter());
	}

	@Test
	@Ignore("attributes aren't going to happen.  The goal is to display more, not less")
	public void testJaxbAttributeContained() {

		assertJaxb(new AttributeContainter());
	}

	@XmlRootElement
	public static class ElementContainter {
		@XmlElement
		ActivityType element = EXPECTED;

	}

	@XmlRootElement
	public static class AttributeContainter {
		@XmlAttribute
		ActivityType element = EXPECTED;

	}

	@Test
	public void testDistanceCategoryActivityTypeNotFound() {
		assertNull(	"other should never have a range",
					ActivityTypeDistanceCategory.valueOf(ActivityType.OTHER, MeasurementUtil.ZERO_LENGTH));
	}

	@Test
	public void testDistanceCategoryWithWalkDefault() {
		assertEquals(	"default should be used",
						ActivityTypeDistanceCategory.SHORT_WALK,
						ActivityTypeDistanceCategory.valueOf(	ActivityType.OTHER,
																MeasurementUtil.ZERO_LENGTH,
																ActivityType.WALK));
	}

	@Test
	public void testAdjectivePlurals() {
		// U has activity and activities which is easy to detect the very different forms
		assertAdjectivePlural(1, U, "activity");
		// zero is plural..wierd, huh?
		assertAdjectivePlural(0, U, "activities");
		assertAdjectivePlural(3, U, "activities");
	}

	private void assertAdjectivePlural(int count, ActivityType activityType, String expectedWord) {
		CompleteMessage singular = CompleteMessage.create(activityType.nounAdjectivePhrase)
				.param(ActivityType.Param.COUNT, count).build();
		MessageResourceBundleUnitTest.assertMessage(expectedWord, singular);
	}

	@Test
	public void testDistanceCategoryWithDefault() {
		assertEquals(	"default should be used",
						ActivityTypeDistanceCategory.SHORT,
						ActivityTypeDistanceCategory
								.valueOfWithDefault(ActivityType.OTHER, MeasurementUtil.ZERO_LENGTH));
	}

	@Test
	@Ignore("the test is being very flaky and mysterious not finding properties files and looking for NounPhrase https://aawhere.jira.com/builds/browse/AAWHERE-ALL51-109#new-failed-tests ")
	public
			void testLocalization() {
		MessageFormatterUnitTest.assertMessageFormatter(ActivityType.BIKE, Locale.US, ActivityType.BIKE.message);
		MessageFormatterUnitTest.assertMessageFormatter(BIKE, Locale.ENGLISH, ActivityType.BIKE.message);
		MessageFormatterUnitTest.assertMessageFormatter(BIKE,
														MessageResourceBundleUnitTest.TEST_LOCALE,
														ActivityType.BIKE.message);
		MessageFormatterUnitTest.assertMessageFormatter(BIKE, Locale.US, ActivityType.BIKE.message);
		MessageFormatterUnitTest.assertMessageFormatter(BIKE, Locale.GERMAN, "Radfahren");
		MessageFormatterUnitTest.assertMessageFormatter(BIKE,
														LocaleUtil.fromWebString(LocaleUtil.DEUTSCH_CH),
														"Velofahren");
	}
}
