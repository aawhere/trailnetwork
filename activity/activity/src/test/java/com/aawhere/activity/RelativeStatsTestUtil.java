package com.aawhere.activity;

import java.util.List;

import com.aawhere.activity.RelativeStatistic.Builder;
import com.aawhere.test.TestUtil;
import com.google.common.base.Supplier;

public class RelativeStatsTestUtil {

	public static RelativeStatistic.Builder allBuilder() {
		return test().all();
	}

	public static RelativeStatistic.Builder personBuilder() {
		return test().person();
	}

	public static RelativeStatistic.Builder personActivityTypeBuilder() {
		return test().activityTypePerson();
	}

	private static RelativeStatsUnitTest test() {
		RelativeStatsUnitTest test = new RelativeStatsUnitTest();
		test.setUp();
		return test;
	}

	public static RelativeStatistic personActivityType() {
		return personActivityTypeBuilder().build();
	}

	public static List<RelativeStatistic> personActivityTypes() {
		return TestUtil.supply(personActivityTypeSupplier(), TestUtil.generateRandomInt(0, 10));
	}

	public static Supplier<RelativeStatistic> personActivityTypeSupplier() {
		return new Supplier<RelativeStatistic>() {

			@Override
			public RelativeStatistic get() {
				return personActivityType();
			}
		};
	}

	public static Builder activityTypeBuilder(ActivityType activityType) {
		return test().activityType(activityType);

	}

	public static Builder distance(ActivityTypeDistanceCategory distance) {
		return test().distance(distance);

	}
}
