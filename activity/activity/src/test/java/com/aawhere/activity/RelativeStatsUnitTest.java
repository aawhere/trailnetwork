package com.aawhere.activity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.RelativeStatistic.Builder;
import com.aawhere.activity.RelativeStatistic.Layer;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.test.TestUtil;

public class RelativeStatsUnitTest {

	private ActivityId id;
	private Integer count;
	private Builder statsBuilder;
	private Layer layer;

	@Before
	public void setUp() {
		this.id = ActivityTestUtil.activityId();
		this.count = TestUtil.generateRandomInt();
		this.layer = Layer.ALL;
		this.statsBuilder = RelativeStatistic.create().featureId(id).count(count).layer(this.layer);

	}

	@Test
	public void testMinimal() {
		assertStats(all().build());
	}

	Builder all() {
		return this.statsBuilder;
	}

	private void assertStats(RelativeStatistic stats) {
		TestUtil.assertContains(stats.id(), id.toString());
		assertEquals(this.count, stats.count());
		assertEquals(this.layer, stats.layer());
	}

	@Test
	public void testActivityType() {
		ActivityType expected = ActivityTestUtil.activityType();
		RelativeStatistic stats = activityType(expected).build();
		TestUtil.assertContains(stats.id(), expected.toString());
	}

	Builder activityType(ActivityType expected) {
		return all().activityType(expected).layer(Layer.ACTIVITY_TYPE);
	}

	Builder distance(ActivityTypeDistanceCategory expected) {
		return all().distance(expected).layer(Layer.ACTIVITY_TYPE_DISTANCE);
	}

	@Test
	public void testAutoLayerActivityType() {
		assertEquals(Layer.ACTIVITY_TYPE, all().layer(null).activityType(ActivityTestUtil.activityType()).build()
				.layer());
	}

	@Test
	public void testAutoLayerActivityTypePerson() {
		assertEquals(Layer.PERSON_ACTIVITY_TYPE, activityTypePerson().build().layer());
	}

	@Test
	public void testDistance() {
		// must choose an activity type that has distance category
		ActivityType activityType = ActivityType.BIKE;
		ActivityTypeDistanceCategory distance = ActivityTestUtil.distanceCategory(activityType);
		RelativeStatistic actual = distance(distance).build();

		assertEquals(distance, actual.distance());
		assertEquals(activityType, actual.activityType());
		assertEquals(Layer.ACTIVITY_TYPE_DISTANCE, actual.layer());
	}

	Builder activityTypePerson() {
		return all().layer(null).activityType(ActivityTestUtil.activityType()).personId(PersonTestUtil.personId());
	}

	@Test
	public void testAutoLayerPerson() {
		assertEquals(Layer.PERSON, person().build().layer());
	}

	Builder person() {
		return all().layer(null).personId(PersonTestUtil.personId());
	}

	@Test
	public void testAutoLayerAll() {
		assertEquals(Layer.ALL, all().layer(null).build().layer());
	}

}
