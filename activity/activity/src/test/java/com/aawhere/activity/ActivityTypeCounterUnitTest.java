/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.test.TestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.test.TestUtil;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class ActivityTypeCounterUnitTest {

	private ActivityTypeStatistic most;
	private ActivityTypeStatistic more;
	private ActivityTypeStatistic less;
	private ActivityTypeStatistic least;
	private Ratio mostRatio;
	private Ratio moreRatio;
	private Ratio lessRatio;
	private Ratio leastRatio;

	private List<ActivityTypeStatistic> expectedOrder;
	private Set<ActivityType> activityTypesRepresented;
	private List<ActivityType> activityTypes;
	private Integer total;
	private CategoricalStatsCounter.Builder<ActivityTypeStatistic, ActivityType, ActivityTypeStatistic.Builder> builder;
	private CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> counter;

	@Before
	public void setUp() {
		this.builder = new CategoricalStatsCounter.Builder<ActivityTypeStatistic, ActivityType, ActivityTypeStatistic.Builder>()
				.statsBuilder(ActivityTypeStatistic.builderSupplier());
		final int high = 4;
		final int mediumHigh = 3;
		final int mediumLow = 2;
		final int low = 1;
		this.total = high + mediumHigh + mediumLow + low;
		mostRatio = MeasurementUtil.ratio(high, total);
		moreRatio = MeasurementUtil.ratio(mediumHigh, total);
		lessRatio = MeasurementUtil.ratio(mediumLow, total);
		leastRatio = MeasurementUtil.ratio(low, total);
		this.most = ActivityTypeStatistic.create().count(high).type(ActivityType.OTHER).total(total).build();
		this.more = ActivityTypeStatistic.create().count(mediumHigh).type(ActivityType.HIKE).total(total).build();
		this.less = ActivityTypeStatistic.create().count(mediumLow).type(ActivityType.SKATE).total(total).build();
		this.least = ActivityTypeStatistic.create().count(low).type(ActivityType.RUN).total(total).build();
		this.expectedOrder = Lists.newArrayList(this.most, this.more, this.less, this.least);
		this.activityTypesRepresented = Sets.newHashSet(Collections2.transform(	expectedOrder,
																				ActivityTypeStatistic.typeFunction()));

	}

	private List<ActivityType> activityTypesAdded() {
		activityTypesAdded(least);
		activityTypesAdded(less);
		activityTypesAdded(more);
		activityTypesAdded(most);
		Collections.shuffle(this.activityTypes);
		return this.activityTypes;
	}

	private Integer totalFromActivityTypes() {
		return this.activityTypes.size();
	}

	private void activityTypesAdded(ActivityTypeStatistic stats) {
		if (this.activityTypes == null) {
			this.activityTypes = new ArrayList<ActivityType>();
		}
		List<ActivityType> nCopies = Collections.nCopies(stats.count(), stats.category());
		this.activityTypes.addAll(nCopies);
	}

	@Test
	public void testSort() {
		ArrayList<ActivityTypeStatistic> actual = new ArrayList<>(this.expectedOrder);
		Collections.shuffle(actual);
		Collections.sort(actual);
		assertEquals(expectedOrder, actual);
	}

	private CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> build(Iterable<ActivityType> categories) {
		this.counter = builder.categories(categories).build();
		builder = null;
		return this.counter;
	}

	@Test
	public void testNone() {

		final ArrayList<ActivityType> none = Lists.newArrayList();

		CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> counter = builder.categories(none).build();
		TestUtil.assertEmpty(counter.stats());
		assertEquals(0, counter.total().intValue());
	}

	@Test
	public void testOneOfEach() {
		build(activityTypesRepresented);
		Iterable<Integer> counts = Iterables.transform(counter.stats(), ActivityTypeStatistic.countFunction());
		Iterable<Integer> filtered = Iterables.filter(counts, Predicates.equalTo(1));
		TestUtil.assertIterablesEquals("all counts are 1", counts, filtered);
	}

	@Test
	public void testExpected() {
		activityTypesAdded();
		build(this.activityTypes);
		final SortedSet<ActivityTypeStatistic> counts = this.counter.stats();
		TestUtil.assertIterablesEquals("values are supposed to be ordered", this.expectedOrder, counts);
		Object expectedTotal = this.activityTypes.size();
		assertEquals("total", expectedTotal, this.counter.total());
		// now confirm get by key works.
		for (ActivityTypeStatistic expected : this.expectedOrder) {
			ActivityTypeStatistic actual = this.counter.stats(expected.type());
			assertEquals("returned wrong type", expected, actual);
		}
		Integer sum = CategoricalStatsCounter.sum(counts);
		assertEquals("sum of all", expectedTotal, sum);
	}

	@Test
	public void testCumulativeOneOfEach() {
		final TreeSet<ActivityTypeStatistic> expected = new TreeSet<>(this.expectedOrder);
		SortedSet<ActivityTypeStatistic> cumulation = CategoricalStatsCounter.cumulation(	expected,
																							ActivityTypeStatistic
																									.builderSupplier());

		assertEquals(expected, cumulation);
	}

	/** Proves that filtering will retain the counter total. */
	@Test
	public void testFilterByPercentMostOnlyInCounter() {
		final ActivityTypeStatistic only = this.most;
		final ArrayList<ActivityTypeStatistic> expected = Lists.newArrayList(only);
		final Ratio ratio = this.mostRatio;
		Predicate<ActivityTypeStatistic> percentPredicate = CategoricalStatistic.percentPredicate(Range.atLeast(ratio));
		this.builder.predicate(percentPredicate);
		CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> counter = this.build(activityTypesAdded());
		TestUtil.assertIterablesEquals("failed filtering with ratio " + ratio, expected, counter.stats());
		assertEquals("total is incorrect", totalFromActivityTypes(), counter.total());

	}

	/**
	 * like {@link #testFilterByPercentMostOnlyInCounter()} the stats will produce a total different
	 * than that which can be cumulated from the count.
	 */
	@Test
	public void testCumulationOfFiltered() {

		Predicate<ActivityTypeStatistic> percentPredicate = CategoricalStatistic.percentPredicate(Range
				.atLeast(this.mostRatio));
		this.builder.predicate(percentPredicate);
		CategoricalStatsCounter<ActivityTypeStatistic, ActivityType> filteredCounter = this.build(activityTypesAdded());
		assertEquals("total is wrong", total, filteredCounter.total());

	}

	@Test
	public void testFilterByPercentMostOnly() {
		final ArrayList<ActivityTypeStatistic> expected = Lists.newArrayList(this.most);
		final Ratio minRatio = this.mostRatio;
		assertPercentFilter(minRatio, expected);
	}

	@Test
	public void testFilterByPercentMore() {
		assertPercentFilter(moreRatio, Lists.newArrayList(this.most, this.more));
	}

	@Test
	public void testFilterByPercentLess() {
		assertPercentFilter(lessRatio, Lists.newArrayList(this.most, this.more, this.less));
	}

	@Test
	public void testFilterByPercentLeast() {
		assertPercentFilter(leastRatio, this.expectedOrder);
	}

	@Test
	public void testFilterByPercentAll() {
		assertPercentFilter(MeasurementUtil.RATIO_ZERO, this.expectedOrder);
	}

	@Test
	public void testFilterByPercentOneHundredPercent() {
		ActivityTypeStatistic solo = ActivityTypeStatistic.create().count(total).type(ActivityType.OTHER).total(total)
				.build();
		final ArrayList<ActivityTypeStatistic> expected = Lists.newArrayList(solo);
		Iterable<ActivityTypeStatistic> actual = CategoricalStatistic.filterByPercent(expected, Range
				.atLeast(MeasurementUtil.RATIO_ONE));
		assertIterablesEquals("100% didn't match", expected, actual);
	}

	@Test
	public void testFilterByPercentNone() {
		final List<ActivityTypeStatistic> empty = Collections.emptyList();
		assertPercentFilter(MeasurementUtil.RATIO_ONE, empty);
	}

	/**
	 * Filtering on all known possibilities this ensures that only those remain that match the range
	 * of at least that ratio which is given.
	 * 
	 * @param minRatio
	 * @param expected
	 */
	private void assertPercentFilter(final Ratio minRatio, final List<ActivityTypeStatistic> expected) {
		Iterable<ActivityTypeStatistic> actual = CategoricalStatistic.filterByPercent(	expectedOrder,
																						Range.atLeast(minRatio));
		TestUtil.assertIterablesEquals("incorrectly filtered", expected, actual);
	}

	@Test
	public void testCumulative3x() {
		ArrayList<ActivityTypeStatistic> input = new ArrayList<>();
		input.addAll(this.expectedOrder);
		input.addAll(this.expectedOrder);
		input.addAll(this.expectedOrder);
		SortedSet<ActivityTypeStatistic> cumulation = CategoricalStatsCounter.cumulation(input, ActivityTypeStatistic
				.builderSupplier());
		int multiplier = 3;
		TreeSet<ActivityTypeStatistic> expected = new TreeSet<>();
		expected.add(ActivityTypeStatistic.create().category(this.most.category())
				.count(this.most.count() * multiplier).total(this.most.total()).build());
		expected.add(ActivityTypeStatistic.create().category(this.more.category())
				.count(this.more.count() * multiplier).total(this.more.total()).build());
		expected.add(ActivityTypeStatistic.create().category(this.less.category())
				.count(this.less.count() * multiplier).total(this.less.total()).build());
		expected.add(ActivityTypeStatistic.create().category(this.least.category())
				.count(this.least.count() * multiplier).total(this.least.total()).build());
		assertEquals(expected, cumulation);
	}
}
