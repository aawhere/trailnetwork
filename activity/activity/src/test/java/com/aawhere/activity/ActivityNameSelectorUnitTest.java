/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.text.WordUtils;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityNameSelector.PhraseScore;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessageUtil;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * Unit test for {@link ActivityNameSelector}
 * 
 * @author Brian Chapman
 * 
 */
public class ActivityNameSelectorUnitTest {

	private static final String UNKNOWN = "Unknown";
	/**
	 * 
	 */
	private static final String TROOP_80 = "Troop 80";
	/**
	 * 
	 */
	private static final String MATT_DAVIS = "Matt Davis";
	/**
	 * 
	 */
	private static final String HOME_TO_WORK = "Home to Work";
	/**
	 * 
	 */
	private static final String SANTA_CRUZ = "Santa Cruz";
	private String demo;
	private String shooters;

	@Before
	public void setUp() {
		demo = "Demonstration Forest";
		shooters = "Shooters";
	}

	/**
	 * Often people describe their associates in the name. This demonstrates common naming styles
	 * and looks for name suggestions excluding the participants. TN-482
	 */
	@Test
	public void testNamesDescribingParticipants() {
		assertBestName(demo, Lists.newArrayList(demo, //
												demo + " Backside",//
												demo + " with Aaron",//
												demo + " with Brian",//
												demo + " with Aaron",//
												demo + " with Aaron",//
												"Favorite Loop for Aaron",//
												"Aaron & Brian"//
		));
	}

	/**
	 * Given some naming standards people use hyphens instead of spaces to indicate trails along the
	 * way. Although naming of the trails would be nice, for now just name the route with common
	 * words by replacing hyphens with spaces.
	 * 
	 * Pelican Inn being the target example.
	 * <p>
	 * <phrase>RedwoodCreek-HeatherCutoff-Owl</phrase>
	 * <phrase>RedwoodCreek-HeatherCutoff-Coastal</phrase>
	 * <phrase>RedwoodCreek-HeatherCutoff-Owl</phrase>
	 * <phrase>RedwoodCreek-HeatherCutoff-Owl</phrase> <phrase>Pelican-Heather Cutoff-Return
	 * US1/Community Ctr</phrase>
	 * </p>
	 */
	@Test
	public void testHyphenNaming() {
		List<String> phrases = Lists.newArrayList("RedwoodCreek-HeatherCutoff-Owl",//
													"RedwoodCreek-HeatherCutoff-Coastal",//
													"RedwoodCreek-HeatherCutoff-Owl",//
													"RedwoodCreek-HeatherCutoff-Owl",//
													"Pelican-Heather Cutoff-Return US1/Community Ctr"//
		);
		String expected = "Redwood Creek Heather Cutoff Owl";
		assertBestName(expected, null, null, phrases);

	}

	/**
	 * Actual names detected "from" Mike's Bike's.
	 * 
	 */
	@Test
	public void testMikesBikesTrailhead() {
		// Before second pass long contributing phrase increase
		// {Mikes=114, Sausalito=100, Mikes Bikes=96, Bikes=48, Mikes Bike=22, Bike=10, ride=1}
		// after second pass long contributed phrase increase
		// {Mikes Bikes=136, Mikes=133, Sausalito=110, Bikes=64, Mikes Bike=28, Bike=12, ride=1}
		ArrayList<String> phrases;
		String mikesBikes = "Mikes Bikes";
		phrases = Lists.newArrayList("SB",//
										"Sausalito",//
										"Mike's",// Mike's+1
										"Mike's Bikes",// Mike's+2 Bikes+1
										"Sausalito",// Sausalito+1
										mikesBikes,// Mikes Bikes+1,Mikes+1,Bikes+2
										mikesBikes,// Mikes Bikes+2,Mikes+2,Bikes+3
										"Mike's",// Mike's+3
										"Mikes",// Mikes+3
										"Sausalito",// Sausalito+2
										"Mikes Bike.  Lost connection at Tiburon",// Mikes+4,Mikes
																					// Bikes+3
										"Sausalito",// Sausalito+3
										mikesBikes,// Mikes Bikes+3,Mikes+4,Bikes+4
										"Mike's Bikes",// Mikes Bikes+4,Mikes+5,Bikes+5
										"Mikes",// Mikes+6
										"Sausalito -",// Sausalito+4
										"Kappas Marina",//
										"Sausalito Mike's",// Sausalito+5 Mike's+4
										"MB",//
										"Mike's",// Mike's+5
										"Sausalito",// Sausalito+5
										"Mike's",// Mike's+6
										"Sausalito",// Sausalito+6
										"Mill Valley (recovery ride)",//
										"Mike's",// Mike's+6
										mikesBikes,// Mikes Bikes+5,Mikes+6,Bikes+5
										"MB",//
										mikesBikes,// Mikes Bikes+6,Mikes+7,Bikes+6
										mikesBikes,// Mikes Bikes+7,Mikes+8,Bikes+7
										"Mikes Bike",// Mikes Bikes+8,Mikes+9,Bikes+8
										"Sausalito MB",//
										"Mike's",// Mike's+7
										"Sausalito w/ JD"// Sausalito+8
		);
		ActivityNameSelector selector = assertBestName(mikesBikes, null, null, phrases);
		// this one shouldn't make the short list
		TestUtil.assertDoesntContain(selector.ranker().scores().toString(), "connection");
		final PhraseScore mikesBikesScore = selector.ranker().score(mikesBikes);
		assertEquals(8, mikesBikesScore.exactMatchCount().intValue());
		assertEquals(8, mikesBikesScore.matchCount().intValue());
	}

	@Test
	public void testBestNameIsSuffix() {
		assertBestName(demo, Lists.newArrayList(demo, //
												"Long day at " + demo,//
												"Aaron and Brian at " + demo,//
												"Big Loop of " + demo,//
												"Personal Best for " + demo,//
												"Forest Loop"// ensures 2 words are better than 1
		));
	}

	@Test
	public void testFromTrailheadName() {
		assertBestName(demo, SANTA_CRUZ, null, Lists.newArrayList(demo + " from Nicene", //
																	demo + " from Santa Cruz",//
																	demo + " from Nicene Marks",//
																	demo + " from Santa Cruz",//
																	demo //

				));
	}

	@Test
	public void testHomeToWork() {
		assertBestName(WordUtils.capitalize(HOME_TO_WORK), "Home", "Work", Lists.newArrayList(HOME_TO_WORK, //
																								"Rainy day",//
																								HOME_TO_WORK,//
																								HOME_TO_WORK,//
																								"Commute"//

		));
	}

	@Test
	public void testTennesseeValleyMuirBeach() {
		assertBestName(	"Coastal",
						"Tennessee Valley",
						"Muir Beach",
						Lists.newArrayList("Coastal to Muir", //
											"Coastal from Tennessee Valley",//
											"Coastal to Muir Beach",//
											"Headlands",//
											"Headlands from Tennessee Valley to Muir Beach",//
											"Tennessee to Muir",//
											"Ride",//
											"Pirates Cove",//
											"To Muir Beach from Tennessee Valley",//
											"Coastal",//
											"Coastal"//

						));
	}

	@Test
	public void testManyTrailingWords() {
		assertBestName(demo, Lists.newArrayList(demo + " in a daze", //
												demo + " with a headache",//
												demo + " upside down and turned around",//
												demo + " ran over a squirrel",//
												demo + " just another stupid title"));
	}

	@Test
	public void testSelectBestName() throws EntityNotFoundException {
		assertBestName(demo, Lists.newArrayList(demo, shooters, demo, shooters, demo, randomName(), randomName()));
	}

	@Test
	public void testSelectBestNameTennesseeValley() {
		// Miwok Loop shows up 4 times, Tennessee Valley shows up 5
		assertBestName("Tennessee Valley", Lists.newArrayList(	"DA Ride",
																"East Loop @ Tennessee Valley",
																"Marin Headlands Loop",
																"Miwok Loop (Headlands)",
																"Miwok Loop from Tennessee Valley",
																"Miwok Loop: Tennessee Valley",
																"Tennessee Valley",
																"Tennessy with Mark",
																"Wednesday Marin Night Ride",
																"bike victor sami"));
	}

	@Test
	public void testLotsOfStopWords() {
		assertBestName(demo, Lists.newArrayList("Road with Mike to the Store and back",
												"Did a ride where I road with Mike",
												"Road with Mike then hung out at Starbucks and drank coffee",
												"This and that and more and more and and and",
												"I a and this to from he she the the the",
												demo,
												demo));
	}

	@Test
	public void testRepeatWordsAreCounted() {
		assertBestName(shooters, Lists.newArrayList(demo,
													demo,
													shooters,
													shooters,
													shooters,
													shooters,
													shooters,
													shooters,
													shooters,
													shooters,
													shooters,
													shooters,
													shooters));
	}

	/**
	 * A real set of results when crawling CC at Mountain Home Inn and targeting the trailhead.
	 * 
	 * The finish names is fed:
	 * 
	 */
	@Test
	public void testMountainHomeInn() {
		ArrayList<String> phrases = Lists
				.newArrayList("Mtn Home, thtr, Matt davis",//
								"Mountain Home Inn to Bootjack",//
								"mtn home, wpi, thtr, troop 80",//
								"Run",//
								"Matt Davis ",//
								"mtn home, thtr, tcc",//
								"Mt. Home, Matt Davis w/ Jerry",//
								"GGTC trail running \"clinic\"",//
								"mtn home, thtr, troop 80",//
								"mtn home, thtr, matt davis",//
								"matt davis, thtr, troop 80....calf tender at end",//
								"Tam Run With Bootjack Bailout",//
								"Coastal-MattDavis",//
								"Mtn Home Inn to Bootjack Parking Lot via Matt Davis",//
								"Pamakids running camp trail run",//
								"Mt.home to west point inn and back",//
								"Saturday Morning Run",//
								"Mt Home & around Mt. Tam",//
								MATT_DAVIS,//
								"Eastwood-Sierra-Fern-BenJohnson-OldMine-RockSpring-Nora-MatttDavis",//
								"mtn home, thtr, tcc",//
								"matt davis, thtr, tcc, troop 80",//
								"MattDavis-Pantoll-Cardiac-TCC-MtHome",//
								"matt davis, thtr, troop 80",//
								"Mt. Tam Railroad Grade to Cabins",//
								"RR - Matt Davis",//
								"Mt Home Inn to Bootjack",//
								"Mt Tam",//
								"mtn home, thtr, tcc, troop 80",//
								"RR - Hogsback",//
								"Matt Davis Troop 80",//
								"Mountain Home Inn to Bootjack",//
								"RR Grade to Matt Davis",//
								"Tam Recovery Run",//
								"Maitenance Loop Run",//
								"Mtn Home West Point Inn",//
								"mtn home, thtr, troop 80",//
								"Matt Davis Trail",//
								"mtn home, thtr, tcc",//
								"Matt Davis to West Pt Inn",//
								"Matt Davis to Troop 88 Loop",//
								"Mt. Tam ",//
								"10/13/2013 Trail Run w GGTC",//
								"matt davis, thtr, troop 80",//
								"Mt. Home Inn to Bootjack",//
								"RR to Matt Davis",//
								"matt davis, thtr, matt davis",//
								"RR Grade",//
								"mtn home, thtr, troop 80",//
								"mtn home, thtr, troop 80",//
								"Mt. Tam ",//
								"mtn home, thtr, troop 80",//
								"Mountain Home Inn to Bootjack",//
								"Mt Home Inn to Bootjack",//
								"Mt. Tam Trail run",//
								"mtn home, thtr, troop 80",//
								"Matt Davis out and back",//
								"Quick afterwork Mt. Tam Run w/ Vic",//
								"mtn home, thtr, troop 80",//
								"Mt. Home Inn to Bootjack",//
								"Mt. Home Inn to Bootjack",//
								"Mt Home Inn to Bootjack",//
								"Matt Davis Trail Run",//
								"Thanksgiving 5 miler",//
								"Mt. Tam Hike",//
								"BARFie run in Mt. Tam",//
								"Eldridge-Northside-MountainTheater-WestpointInn-Nora-MountainInn",//
								"RR to Matt Davis");

		final String mountainHomeInn = "Mountain Home Inn";
		ActivityNameSelector selector = assertBestName(MATT_DAVIS, mountainHomeInn, "Bootjack", phrases);
		assertEquals(3, selector.startRanker().score(mountainHomeInn).matchCount().intValue());

	}

	@Test
	public void testNumbersIncluded() {
		assertBestName(TROOP_80, Lists.newArrayList(TROOP_80, TROOP_80, TROOP_80, TROOP_80));
	}

	/**
	 * Uses the name from the course if the leaderboards don't have rankings (which they should
	 * anyways).
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testEmptyInput() throws EntityNotFoundException {
		Message best = best(new ArrayList<String>());
		assertNotNull(best);
		TestUtil.assertNotEquals(best, ActivityMessage.UNKNOWN);
	}

	/**
	 * 
	 * @param bestActivityName
	 * @param bestStartName
	 * @param bestEndName
	 * @param canidates
	 * @return
	 */
	private ActivityNameSelector assertBestName(String bestActivityName, String bestStartName, String bestEndName,
			List<String> canidates) {
		ActivityNameSelector selector = selector(canidates);
		assertEquals(bestActivityName, selector.bestName().getValue());
		if (bestEndName != null) {
			assertEquals("finish", bestEndName, MessageUtil.valueOf(selector.bestFinishName()));
		}
		if (bestStartName != null) {
			assertEquals("start", bestStartName, MessageUtil.valueOf(selector.bestStartName()));

		}
		return selector;

	}

	private void assertBestName(String best, List<String> canidates) {
		Message bestName = best(canidates);
		assertEquals(best, bestName.getValue());
	}

	private Message best(List<String> canidates) {
		ActivityNameSelector selector = selector(canidates);
		Message bestName = selector.bestName();
		return bestName;
	}

	/**
	 * @param canidates
	 * @return
	 */
	private ActivityNameSelector selector(List<String> canidates) {

		NameSelectorActivityProvider activityProvider = NameSelectorActivityProvider.create().addNames(canidates)
				.build();
		ActivityNameSelector selector = ActivityNameSelector.create().activities(activityProvider).build();
		return selector;
	}

	private String randomName() {
		return TestUtil.generateRandomAlphaNumeric();
	}

	/**
	 * Activity provider that can be customized to use lists of activity names. It will cycle
	 * through the names, creating random activities containing the names.
	 * 
	 * @deprecated this will be removed and replaced with just a string provider when activities is
	 *             no longer needed.
	 * @author brian
	 * 
	 */
	@Deprecated
	public static class NameSelectorActivityProvider
			implements ActivityProvider, Iterable<Activity> {

		/**
		 * Used to construct all instances of
		 * ActivityNameSelectorUnitTest.NameSelectorActivityProvider.
		 */
		public static class Builder
				extends ObjectBuilder<NameSelectorActivityProvider> {

			public Builder() {
				super(new NameSelectorActivityProvider());
			}

			public Builder addNames(List<String> names) {
				building.names.addAll(names);
				return this;
			}

			public Builder addNames(String... names) {
				building.names.addAll(Lists.newArrayList(names));
				return this;
			}

			/*
			 * (non-Javadoc)
			 * @see com.aawhere.lang.ObjectBuilder#build()
			 */
			@Override
			public NameSelectorActivityProvider build() {
				if (building.names.isEmpty()) {
					// need at least one name
					building.names.add(TestUtil.generateRandomAlphaNumeric());
				}
				building.nameIt = building.names.iterator();
				return super.build();
			}

		}// end Builder

		private List<String> names = new ArrayList<>();
		private Iterator<String> nameIt;

		/**
		 * Use {@link Builder} to construct
		 * ActivityNameSelectorUnitTest.NameSelectorActivityProvider
		 */
		private NameSelectorActivityProvider() {
		}

		public static Builder create() {
			return new Builder();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityProvider#getActivity(com.aawhere.activity.ActivityId)
		 */
		@Override
		public Activity getActivity(ActivityId activityId) throws EntityNotFoundException {
			if (!nameIt.hasNext()) {
				nameIt = names.iterator();
			}
			Message nextName = new StringMessage(nameIt.next());
			return ActivityTestUtil.mutateActivity(ActivityTestUtil.createActivity(activityId)).setName(nextName)
					.build();
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Iterable#iterator()
		 */
		@Override
		public Iterator<Activity> iterator() {
			return new Iterator<Activity>() {

				@Override
				public void remove() {
					throw new RuntimeException("TODO:Implement this");
				}

				@Override
				public Activity next() {
					try {
						return getActivity(ActivityTestUtil.createApplicationActivityId());
					} catch (EntityNotFoundException e) {
						throw new RuntimeException(e);
					}
				}

				@Override
				public boolean hasNext() {
					return nameIt.hasNext();
				}
			};
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityProvider#getActivities(java.lang.Iterable)
		 */
		@Override
		public Activities getActivities(Iterable<ActivityId> ids) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.ServiceStandard#all(java.lang.Iterable)
		 */
		@Override
		public Map<ActivityId, Activity> all(Iterable<ActivityId> ids) {
			return null;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.ServiceStandard#idFunction()
		 */
		@Override
		public Function<ActivityId, Activity> idFunction() {
			return null;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.ServiceStandard#provides()
		 */
		@Override
		public Class<Activity> provides() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.persist.ServiceStandard#entity(com.aawhere.id.Identifier)
		 */
		@Override
		public Activity entity(ActivityId id) throws EntityNotFoundException {
			throw new RuntimeException("TODO:Implement this");
		}
	}

	@Test
	public void testJaxb() {
		JAXBTestUtil<ActivityNameSelector> util = JAXBTestUtil.create(selector(Lists.newArrayList(demo, shooters)))
				.build();
		util.assertContains(demo);
		util.assertContains(shooters);
	}
}
