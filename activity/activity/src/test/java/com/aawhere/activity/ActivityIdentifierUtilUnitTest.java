/**
 *
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.test.TestUtil;
import com.google.common.collect.ImmutableList;

/**
 * Tests {@link ActivityIdentifier}, {@link ActivityId} and {@link ApplicationActivityId} using
 * {@link ActivityIdentifierUtil}.
 * 
 * 
 * @author aroller
 * 
 */
public class ActivityIdentifierUtilUnitTest {

	/**
	 *
	 */
	/** @see #testToFromString() if you change this */
	public static final String idValue = "123456789";
	public static final ApplicationKey key = KnownApplication.GARMIN_CONNECT.key;
	// hard-coded on purpose for testing. This will break if GarminConnect key changes
	public static final String COMBINE_KEY_VALUE = "gc-123456789";
	public static final ActivityId remoteId = new ActivityId(key, idValue);
	public static final ApplicationKey ourKey = KnownApplication.TRAIL_NETWORK.key;
	public static final ActivityId ourId = new ActivityId(ourKey, idValue);
	public static final ActivityId ourApplicationActivityIdHidden = ourId;
	public static final ActivityId ourActivityId = new ActivityId(ourKey, idValue);
	public static final ActivityId ourActivityIdHidden = ourActivityId;
	public static final ActivityId remoteIdHidden = remoteId;
	public static final ActivityReferenceIds ourIds = ActivityReferenceIds.create().setActivityId(ourActivityId)
			.build();

	/**
	 * Test method for {@link com.aawhere.activity.ApplicationActivityId#equals(java.lang.Object)} .
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(remoteId.equals(remoteId));
		ActivityId redo = new ActivityId(remoteId.getApplicationKey(), remoteId.getRemoteId());
		assertTrue(remoteId.equals(redo));
		ActivityId otherKey = new ActivityId(ourKey, idValue);
		assertFalse(remoteId.equals(otherKey));
		ActivityId otherValue = new ActivityId(key, idValue + "BS!");
		assertFalse(remoteId.equals(otherValue));
	}

	/**
	 * Test method for
	 * {@link com.aawhere.activity.ApplicationActivityId#ApplicationActivityId(com.aawhere.app.ApplicationKey, java.lang.String)}
	 * .
	 */
	@Test
	public void testApplicationActivityIdApplicationKeyString() {
		assertEquals(key, remoteId.getApplicationKey());
		assertEquals(idValue, remoteId.getRemoteId());
		assertEquals(COMBINE_KEY_VALUE, remoteId.getValue());
	}

	/**
	 * Sometimes a client provides the key in upper case. This must protect us internally to always
	 * format the key to our liking...always lower case. We also must not mess with the
	 * capitalization of their key so test that the key remains unchanged.
	 * 
	 * @throws UnknownApplicationException
	 * @throws InvalidApplicationActivityIdException
	 */
	@Test
	public void testUpperCaseKey() throws InvalidApplicationActivityIdException, UnknownApplicationException {
		// purposely not referencing constants for clarity and control
		String expected = "gc-A13bC";
		String upper = "GC-A13bC";
		ActivityId upperId = new ActivityId(upper);
		ActivityId lowerId = ActivityIdentifierUtil.validateFormat(upperId);
		assertEquals("case isn't being applied correctly", expected, lowerId.getValue());
	}

	@Test
	public void testIsValidFormat() {
		assertTrue(COMBINE_KEY_VALUE, ActivityIdentifierUtil.isValidKeyFormat(COMBINE_KEY_VALUE));
		assertFalse(idValue, ActivityIdentifierUtil.isValidKeyFormat(idValue));
	}

	@Test
	public void testToFromString() {
		String idString = remoteId.getValue();

		// string hard coded rather than programatically built here for clarity
		assertEquals(COMBINE_KEY_VALUE, idString);
		ActivityId actual = new ActivityId(COMBINE_KEY_VALUE);
		assertEquals(remoteId, actual);
		assertEquals(remoteId.getApplicationKey(), actual.getApplicationKey());
		assertEquals(remoteId.getRemoteId(), actual.getRemoteId());
	}

	@Test
	public void testConstructIds() {
		String a = "a";
		String b = "b";
		ActivityId[] ids = ActivityIdentifierUtil.activityIds(key, a, b);
		assertEquals(2, ids.length);
		assertEquals(a, ids[0].getRemoteId());
		assertEquals(key, ids[0].getApplicationKey());
		assertEquals(b, ids[1].getRemoteId());
	}

	@Test(expected = InvalidApplicationActivityIdException.class)
	public void testInvalidKeyFormat() {
		// throws UnknownApplicationException
		ActivityIdentifierUtil.validateFormat(new ActivityId("no dash"));
	}

	/**
	 * The {@link ApplicationActivityId} accepts a string which should be the qualified string, but
	 * if it's just a number then it is assumed to be the trailnetwork. This allows external users
	 * and internal users to use the same endpoints.
	 * 
	 */
	@Test(expected = InvalidApplicationActivityIdException.class)
	public void testUnqualifiedString() {
		ActivityIdentifierUtil.validateFormat(new ActivityId(idValue));
	}

	@Test
	public void testApplicationIdCommaStringToList() {
		String first = "unknown-1234";
		String second = "tn-234";
		String value = StringUtils.join(Arrays.asList(first, second), ",");
		List<ActivityId> idValuesToList = ActivityIdentifierUtil.idValuesToList(value);
		assertEquals(first, idValuesToList.get(0).getValue());
		assertEquals(second, idValuesToList.get(1).getValue());
	}

	@Test
	public void testAppIsUs() {
		assertFalse(remoteId + " isn't from us", ActivityIdentifierUtil.appIsUs(remoteId));

		assertTrue(remoteId + " is from us", ActivityIdentifierUtil.appIsUs(ourId));
	}

	@Test
	public void testActivityIdFromUs() {
		assertEquals(ourActivityId, ActivityIdentifierUtil.activityIdFrom(ourActivityId));
		assertEquals(ourActivityId, ActivityIdentifierUtil.activityIdFrom(ourActivityIdHidden));
		assertEquals(ourActivityId, ActivityIdentifierUtil.activityIdFrom(ourApplicationActivityIdHidden));
	}

	@Test
	public void testApplicationIdHidden() {
		assertSame(remoteId, ActivityIdentifierUtil.applicationActivityIdFrom(remoteIdHidden));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testApplicationActivityIdFromOurActivityIdHidden() {
		ActivityIdentifierUtil.applicationActivityIdFrom(ourActivityIdHidden);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testApplicationActivityIdFromOurApplicaitonActivityIdHidden() {
		ActivityIdentifierUtil.applicationActivityIdFrom(ourApplicationActivityIdHidden);
	}

	@Test
	public void testActivityIdFromTheirApplicaitonActivityIdHidden() {
		assertEquals(remoteIdHidden.toString(), ActivityIdentifierUtil.activityIdFrom(remoteIdHidden).getValue());
	}

	@Test
	public void testConvert() {
		ActivityId activityId = ActivityIdentifierUtil.activityIdFrom(ourId);
		assertEquals(ourApplicationActivityIdHidden, activityId);
	}

	@Test
	public void testActivityReferenceIdsEquals() {
		assertTrue(ActivityIdentifierUtil.equals(ourActivityIdHidden, ourIds));
		assertTrue(ActivityIdentifierUtil.equals(ourApplicationActivityIdHidden, ourIds));
		assertFalse(ActivityIdentifierUtil.equals(remoteIdHidden, ourIds));
		assertFalse(ActivityIdentifierUtil.equals(remoteId, ourIds));
	}

	@Test
	public void testWhitespaceSeparatedNoApplicationKey() {
		ActivityId first = ActivityTestUtil.activityId();
		ActivityId second = ActivityTestUtil.activityId();
		String joined = StringUtils.join(ImmutableList.of(first.toString(), second.toString()), System.lineSeparator());
		Iterable<ActivityId> activityIds = ActivityIdentifierUtil.activityIdsFromWhitespaceSeparated(joined, null);
		TestUtil.assertSize(2, activityIds);
		Iterator<ActivityId> iterator = activityIds.iterator();
		assertEquals(first, iterator.next());
		assertEquals(second, iterator.next());
	}

	@Test
	public void testWhitespaceSeparatedApplicationKey() {
		ActivityId first = ActivityTestUtil.activityId();
		ActivityId second = ActivityTestUtil.activityId();
		String joined = StringUtils.join(ImmutableList.of(first.getRemoteId().toString(), second.getRemoteId()
				.toString()), System.lineSeparator());
		Iterable<ActivityId> activityIds = ActivityIdentifierUtil.activityIdsFromWhitespaceSeparated(joined, first
				.getApplicationKey());
		TestUtil.assertSize(2, activityIds);
		Iterator<ActivityId> iterator = activityIds.iterator();
		assertEquals(first, iterator.next());
		assertEquals(second, iterator.next());
	}

	@Test(expected = UnknownApplicationException.class)
	public void testWhitespaceSeparatedUnknownApplicationKey() {
		ActivityIdentifierUtil.activityIdsFromWhitespaceSeparated(	idValue,
																	new ApplicationKey(TestUtil
																			.generateRandomAlphaNumeric()));
	}
}
