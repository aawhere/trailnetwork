/**
 *
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Test;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.persist.BaseEntitiesBaseUnitTest;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.TrackUtil;

/**
 * Simple tests against {@link Activity}.
 * 
 * @author roller
 * 
 */
public class ActivitiesUnitTest
		extends BaseEntitiesBaseUnitTest<Activity, Activities, Activities.Builder> {

	public static ActivitiesUnitTest newInstance() {
		ActivitiesUnitTest instance = new ActivitiesUnitTest();
		instance.baseSetUp();
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#createEntitySampleWithId()
	 */
	@Override
	public Activity createEntitySampleWithId() {
		return ActivityUnitTest.getInstance().getEntitySampleWithId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return ActivityUnitTest.getInstance().getAdapters();
	}

	@Override
	protected Class<?>[] getClassesToBeBound() {
		Class<?>[] classes = { Activities.class, Activity.class };
		return classes;
	}

	@Test
	public void testBoundsEmpty() {
		Activities activities = Activities.create().build();
		assertNull("no activities, no bounds", activities.boundingBox());
	}

	@Test
	public void testBoundsSingleActivity() {
		Activity activity = createEntitySampleWithId();
		Activities activities = Activities.create().add(activity).build();
		assertNotNull("one activity is enough for a bounds", activities.boundingBox());
		// "activities should inherit the given bounds from the activity",
		BoundingBoxTestUtils.assertEquals(activity.getTrackSummary().getBoundingBox(), activities.boundingBox());
	}

	@Test
	public void testBoundsMultipleActivities() {
		Activity before = ActivityTestUtil.createActivity(ExampleTracks.LINE_BEFORE);
		Activity after = ActivityTestUtil.createActivity(ExampleTracks.LINE_LONGER);
		Activities activities = Activities.create().add(before).add(after).build();
		Iterable<GeoCoordinate> coordinates = TrackUtil.coordinates(ExampleTracks.LINE_BEFORE_AFTER);
		BoundingBox expected = BoundingBoxUtil.boundingBox(coordinates);
		BoundingBoxTestUtils.assertEquals(expected, activities.boundingBox());

	}
}
