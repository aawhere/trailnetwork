/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.app.DeviceTestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.lang.ObjectBuilder;
import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class DeviceSignalActivitySelectorUnitTest {

	@Test
	public void testOnlyOneBestDevice() {
		assertSelector(SignalDeviceTestCase.create().expected(SignalQuality.GREAT, GREAT_DEVICE)
				.addActivity(SignalQuality.POOR, POOR_DEVICE).build());
	}

	@Test
	public void testBreakTieWithDevice() {
		assertSelector(SignalDeviceTestCase.create().expected(SignalQuality.GOOD, GREAT_DEVICE)
				.addActivity(SignalQuality.GOOD, GOOD_DEVICE).build());
	}

	@Test
	public void testSignalQualityIsCheckedFirst() {
		assertSelector(SignalDeviceTestCase.create().expected(SignalQuality.GOOD, POOR_DEVICE)
				.addActivity(SignalQuality.FAIR, GREAT_DEVICE).build());
	}

	@Test
	public void testNullDevice() {
		assertSelector(SignalDeviceTestCase.create().expected(SignalQuality.GREAT, GOOD_DEVICE)
				.addActivity(SignalQuality.GOOD, null).build());
	}

	@Test
	public void testNullSignalAndDevice() {
		assertSelector(SignalDeviceTestCase.create().expected(SignalQuality.FAIR, FAIR_DEVICE).addActivity(null, null)
				.build());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoCoursesGiven() {
		List<Activity> none = Collections.emptyList();
		DeviceSignalActivitySelector.create().activities(none).capabilitiesRepository(REPOSITORY).build();
	}

	/**
	 * @param poorCompletion
	 * @param greatCompletion
	 */
	private void assertSelector(SignalDeviceTestCase testCase) {

		ArrayList<Activity> input = Lists.newArrayList();
		input.addAll(testCase.others);
		input.add(testCase.expected);
		DeviceSignalActivitySelector selector = DeviceSignalActivitySelector.create().activities(input)
				.capabilitiesRepository(REPOSITORY).build();

		assertEquals(testCase.expected, selector.best());
	}

	private static class SignalDeviceTestCase {

		/**
		 * Used to construct all instances of
		 * DeviceSignalActivitySelectorUnitTest.SignalDeviceTestCase.
		 */
		public static class Builder
				extends ObjectBuilder<SignalDeviceTestCase> {

			public Builder() {
				super(new SignalDeviceTestCase());
			}

			public Builder expected(SignalQuality signal, ApplicationKey device) {
				building.expected = ActivityTestUtil.createActivityWithSignalAndDevice(signal, device);
				return this;
			}

			public Builder addActivity(SignalQuality signal, ApplicationKey device) {
				building.others.add(ActivityTestUtil.createActivityWithSignalAndDevice(signal, device));
				return this;
			}

			public SignalDeviceTestCase build() {
				SignalDeviceTestCase built = super.build();
				return built;
			}

		}// end Builder

		protected Activity expected;
		protected List<Activity> others = Lists.newArrayList();

		public static Builder create() {
			return new Builder();
		}

		/**
		 * Use {@link Builder} to construct
		 * DeviceSignalActivitySelectorUnitTest.SignalDeviceTestCase
		 */
		private SignalDeviceTestCase() {
		}

	}

}
