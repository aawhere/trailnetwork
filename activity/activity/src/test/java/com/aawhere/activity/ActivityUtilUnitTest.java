/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.Message;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class ActivityUtilUnitTest {

	@Test
	public void testCumulativeStats() {

		ActivityStats stats1 = ActivityTestUtil.activityStats();
		ActivityStats stats2 = ActivityTestUtil.activityStats();
		final HashSet<ActivityStats> all = Sets.newHashSet(stats1, stats2);
		ActivityStats cumulation = ActivityUtil.cumulation(all);
		Integer expectedTotal = stats1.total() + stats2.total();
		assertEquals(" total", expectedTotal, cumulation.total());
		TestUtil.assertNotEmpty(cumulation.activityTypeStats());
		TestUtil.assertNotEmpty(cumulation.distanceStats());
	}

	@Test
	public void testHikeIsWalk() {
		ActivityType input = ActivityType.HIKE;
		assertEquals(ActivityType.WALK, ActivityUtil.hikeIsWalkFunction().apply(input));
		assertEquals(ActivityType.WALK, ActivityUtil.hikeIsWalkFunction().apply(ActivityType.WALK));
		assertNull(ActivityUtil.hikeIsWalkFunction().apply(null));
	}

	@Test
	public void testStatsForCategory() {
		ActivityStats stats = ActivityTestUtil.activityStats();
		SortedSet<ActivityTypeStatistic> all = stats.activityTypeStats();
		for (ActivityTypeStatistic activityTypeStats : all) {
			ActivityTypeStatistic statsForCategory = CategoricalStatistic.statsForCategory(all, activityTypeStats
					.category());
			assertEquals(all + " provided the wrong stats ", activityTypeStats, statsForCategory);
		}
	}

	@Test
	public void testBikeGeneralizedAllNull() {
		ActivityUtil.bikeGeneralized(null, null, null, MeasurementUtil.RATIO_ONE);
	}

	@Test
	public void testBikeGeneralizedAllEqual() {
		final int bikeCount = 10;
		final int mtbCount = 10;
		final int roadCount = 10;
		ActivityType expectedWinner = ActivityType.BIKE;
		assertBikeGeneralized(bikeCount, mtbCount, roadCount, expectedWinner);
	}

	@Test
	public void testBikeGeneralizedMoreRoad() {
		assertBikeGeneralized(0, 1, 2, ActivityType.ROAD);
	}

	@Test
	public void testBikeGeneralizedMoreMtbThanRoadNoBike() {
		assertBikeGeneralized(0, 2, 1, ActivityType.MTB);
	}

	@Test
	public void testBikeGeneralizedMoreMtbThanBikeNoRoad() {
		assertBikeGeneralized(1, 2, 0, ActivityType.MTB);
	}

	@Test
	public void testBikeGeneralizedMtbBikeEqualNoRoad() {
		assertBikeGeneralized(1, 1, 0, ActivityType.MTB);
	}

	@Test
	public void testBikeGeneralizedMoreBikeThanMtbNoRoad() {
		assertBikeGeneralized(2, 1, 0, ActivityType.BIKE);
	}

	@Test
	public void testBikeGeneralizedMtbOnly() {
		assertBikeGeneralized(0, 1, 0, ActivityType.MTB);
	}

	@Test
	public void testBikingCombinedAllNull() {
		SortedSet<ActivityTypeStatistic> activityTypeStats = Sets.newTreeSet();
		ActivityTypeStatistic expectedFirst = null;
		assertBikingCombined(activityTypeStats, expectedFirst);
	}

	@Test
	public void testBikingCombinedBikingOnly() {
		int total = 2;
		ActivityTypeStatistic expectedFirst = ActivityTypeStatistic.create().count(total).total(total)
				.category(ActivityType.BIKE).build();
		assertBikingCombined(ImmutableSortedSet.of(expectedFirst), expectedFirst);
	}

	@Test
	public void testBikingCombinedBikingTwoBattle() {
		assertBikingCombinedTwoTypes(ActivityType.MTB, ActivityType.BIKE);
		assertBikingCombinedTwoTypes(ActivityType.MTB, ActivityType.ROAD);
		assertBikingCombinedTwoTypes(ActivityType.BIKE, ActivityType.MTB);
		assertBikingCombinedTwoTypes(ActivityType.ROAD, ActivityType.BIKE);
	}

	@Test
	public void testBikeBeatsRoad() {
		assertBikingCombinedTwoTypes(ActivityType.BIKE, ActivityType.ROAD);
	}

	@Test
	public void testRoadBeatsMtb() {
		assertBikingCombinedTwoTypes(ActivityType.ROAD, ActivityType.MTB);
	}

	private void assertBikingCombinedTwoTypes(ActivityType winningType, ActivityType losingType) {
		int total = 100;
		int winningCount = 98;
		ActivityTypeStatistic expectedFirst = ActivityTypeStatistic.create().count(winningCount).total(total)
				.category(winningType).build();
		ActivityTypeStatistic second = ActivityTypeStatistic.create().count(total - winningCount).total(total)
				.category(losingType).build();

		assertBikingCombined(ImmutableSortedSet.of(expectedFirst, second), expectedFirst);
	}

	/**
	 * Specialty tester for various combinations of combining biking categories. This only works
	 * with {@link ActivityType#ROAD}, {@link ActivityType#MTB} and {@link ActivityType#BIKE} in the
	 * list so it always expectes a single result unless expected first is null then zero is
	 * expected.
	 * 
	 * @param activityTypeStats
	 * @param expectedSize
	 * @param expectedFirst
	 */
	private void assertBikingCombined(SortedSet<ActivityTypeStatistic> activityTypeStats,
			ActivityTypeStatistic expectedFirst) {
		SortedSet<ActivityTypeStatistic> bikingCombined = ActivityUtil
				.bikingCombined(activityTypeStats, ActivityUtil.MIN_ACTIVITY_TYPE_PERCENT);
		int expectedSize = (expectedFirst == null) ? 0 : 1;
		TestUtil.assertSize(expectedSize, bikingCombined);
		if (expectedFirst != null) {
			// expected first should inherit all the counts
			assertEquals(	ActivityTypeStatistic.clone(expectedFirst).count(expectedFirst.total()).build(),
							bikingCombined.first());
		}
	}

	/**
	 * @param bikeCount
	 * @param mtbCount
	 * @param roadCount
	 * @param expectedWinner
	 */
	private void assertBikeGeneralized(final int bikeCount, final int mtbCount, final int roadCount,
			ActivityType expectedWinner) {
		final int total = bikeCount + mtbCount + roadCount;
		ActivityType bikeGeneralized = ActivityUtil.bikeGeneralized(ActivityTypeStatistic.create()
																			.category(ActivityType.BIKE).total(total)
																			.count(bikeCount).build(),
																	ActivityTypeStatistic.create()
																			.category(ActivityType.MTB).count(mtbCount)
																			.total(total).build(),
																	ActivityTypeStatistic.create()
																			.category(ActivityType.ROAD).total(total)
																			.count(roadCount).build(),
																	MeasurementUtil.RATIO_ONE);
		assertEquals(expectedWinner, bikeGeneralized);
	}

	@Test
	public void testMerge() {
		SortedSet<ActivityTypeStatistic> activityTypeStats = ActivityTestUtil.activityTypeStats(2);
		Iterator<ActivityTypeStatistic> iterator = activityTypeStats.iterator();
		ActivityTypeStatistic from = iterator.next();
		ActivityTypeStatistic into = iterator.next();
		ActivityTypeStatistic merged = CategoricalStatistic.merged(from, into, ActivityTypeStatistic.builderSupplier());
		assertEquals("count", from.count() + into.count(), merged.count().intValue());
		assertEquals("total", from.total(), merged.total());
		assertEquals("category", into.category(), merged.category());
	}

	@Test
	public void testBikeMergeNoMin() {
		final Ratio minRatioForMtbToAbsort = MeasurementUtil.RATIO_ONE;
		final int bikeCount = 10;
		final int mtbCount = 5;
		final ActivityType expectedKeeper = ActivityType.BIKE;
		assertBikeMerge(bikeCount, mtbCount, expectedKeeper, minRatioForMtbToAbsort);
	}

	@Test
	public void testBikeMergeMinMet() {
		final int bikeCount = 10;
		final int mtbCount = 5;
		final Ratio minRatioForMtbToAbsort = MeasurementUtil.ratio(mtbCount, bikeCount + mtbCount);
		final ActivityType expectedKeeper = ActivityType.MTB;
		assertBikeMerge(bikeCount, mtbCount, expectedKeeper, minRatioForMtbToAbsort);
	}

	@Test
	public void testBikeMergeMoreMtb() {
		final int bikeCount = 7;
		final int mtbCount = 8;
		final Ratio minRatioForMtbToAbsort = MeasurementUtil.RATIO_ONE;
		final ActivityType expectedKeeper = ActivityType.MTB;
		assertBikeMerge(bikeCount, mtbCount, expectedKeeper, minRatioForMtbToAbsort);
	}

	/**
	 * @param bikeCount
	 * @param mtbCount
	 * @param expectedKeeper
	 * @param minRatioForMtbToAbsort
	 */
	private void assertBikeMerge(final int bikeCount, final int mtbCount, final ActivityType expectedKeeper,
			final Ratio minRatioForMtbToAbsort) {
		Integer total = mtbCount + bikeCount;
		SortedSet<ActivityTypeStatistic> all = new TreeSet<>();
		ActivityTypeStatistic bikeStats = ActivityTypeStatistic.create().category(ActivityType.BIKE).count(bikeCount)
				.total(total).build();
		ActivityTypeStatistic mtb = ActivityTypeStatistic.create().category(ActivityType.MTB).count(mtbCount)
				.total(total).build();
		all.add(bikeStats);
		all.add(mtb);
		SortedSet<ActivityTypeStatistic> bikingCombined = ActivityUtil.bikingCombined(all, minRatioForMtbToAbsort);
		ActivityTypeStatistic kept = CategoricalStatistic.statsForCategory(bikingCombined, expectedKeeper);
		final ActivityType expectedDiscard = (expectedKeeper == ActivityType.BIKE) ? ActivityType.MTB
				: ActivityType.BIKE;
		ActivityTypeStatistic discarded = CategoricalStatistic.statsForCategory(bikingCombined, expectedDiscard);
		assertNotNull(kept);
		assertNull(discarded);
		assertEquals(total, kept.total());
		assertEquals(MeasurementUtil.RATIO_ONE, kept.getPercent());
	}

	@Test
	public void testRunDistanceCategoryWithinRange() {
		ActivityTypeDistanceCategory expected = ActivityTypeDistanceCategory.HALF_MARATHON;
		Comparable<Length> expectedDistance = expected.range.lowerEndpoint();
		final ActivityType expectedType = ActivityType.RUN;
		assertActivityTypeDistanceCategory(expected, expectedDistance, expectedType);
	}

	/**
	 * Confirms the distance category can be found.
	 * 
	 * @param expected
	 * @param expectedDistance
	 * @param expectedType
	 */
	private void assertActivityTypeDistanceCategory(ActivityTypeDistanceCategory expected,
			Comparable<Length> expectedDistance, final ActivityType expectedType) {
		ActivityTypeDistanceCategory actual = ActivityTypeDistanceCategory.valueOf(expectedType, expectedDistance);
		assertEquals(expectedDistance.toString(), expected, actual);
	}

	@Test
	public void testDistanceCategoryDistanceNotFound() {
		assertActivityTypeDistanceCategory(	null,
											MeasurementUtil.comparable(MeasurementUtil.createLengthInMeters(-1)),
											ActivityType.RUN);
	}

	@Test
	public void testDistanceCategoryUpperBound() {
		assertActivityTypeDistanceCategory(	ActivityTypeDistanceCategory.ULTRA_RUN,
											MeasurementUtil.comparable(MeasurementUtil
													.createLengthInMeters(Integer.MAX_VALUE)),
											ActivityType.RUN);
	}

	@Test
	public void testDistanceCategories() {
		Length fiveK = MeasurementUtil.createLengthInMeters(5000);
		HashSet<ActivityType> activityTypes = Sets.newHashSet(ActivityType.RUN, ActivityType.OTHER, ActivityType.WALK);
		List<ActivityTypeDistanceCategory> distanceCategories = ActivityUtil.distanceCategories(activityTypes, fiveK);
		TestUtil.assertSize(2, distanceCategories);
		TestUtil.assertContains(ActivityTypeDistanceCategory.FIVE_K_RUN, distanceCategories);
		TestUtil.assertContains(ActivityTypeDistanceCategory.MEDIUM_WALK, distanceCategories);
	}

	@Test
	public void testNames() {
		Activities activities = ActivityTestUtil.createActivities();
		List<Message> names = Lists.newArrayList(ActivityUtil.names(activities));
		TestUtil.assertSize(activities.size(), names);
		Iterator<Message> namesIterator = names.iterator();
		for (Activity activity : activities) {
			assertEquals(activity.getName(), namesIterator.next());
		}
	}

	@Test
	public void testNamesMap() {
		Activities activities = ActivityTestUtil.createActivities();
		Map<ActivityId, Message> nameMap = ActivityUtil.nameMap(activities);
		for (Activity activity : activities) {
			final ActivityId id = activity.id();
			assertEquals(nameMap.toString(), activity.getName(), nameMap.get(id));
		}

	}
}
