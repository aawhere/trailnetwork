/**
 * 
 */
package com.aawhere.activity.export.kml;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.DocumentType;
import net.opengis.kml.v_2_2_0.MultiGeometryType;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ActivityUnitTest;
import com.aawhere.jts.geom.match.JtsMatchUtil;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.Track;
import com.aawhere.track.kml.TrackKmlProcessor;
import com.aawhere.track.kml.TrackKmlTestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;

/**
 * @author aroller
 * 
 */
public class ActvityKmlUnitTest {

	private Track track;
	private Geometry buffer;
	private LineString comparisonLineString;
	private MultiGeometryType line;
	private Activity source;
	private Activity target;

	@Before
	public void setUp() {
		this.track = ExampleTracks.LINE;
		TrackKmlProcessor processor = TrackKmlProcessor.create().setTrack(track).build();
		this.comparisonLineString = JtsTrackGeomUtil.trackToLineStringWithoutDuplicateTrackpoints(track);
		this.buffer = JtsMatchUtil.buffer(comparisonLineString, 1);
		this.line = TrackKmlTestUtil.line();
		this.source = ActivityUnitTest.getInstance().getEntitySampleWithId();
		this.target = ActivityUnitTest.getInstance().getEntitySampleWithId();

	}

	@Test
	public void testLine() {
		ActivityKml kml = ActivityKml.create().setActualTrack(line).build();
		JAXBTestUtil<?> util = JAXBTestUtil.create(kml.getElement()).build();
		// System.out.println(util.getXml());
	}

	@Test
	public void testActivities() {
		Activities activities = ActivityTestUtil.createActivities();
		ActivitiesKml kml = ActivitiesKml.create().setActivities(activities).build();
		JAXBTestUtil<JAXBElement<DocumentType>> util = JAXBTestUtil.create(kml.getElement()).build();
		util.assertContains("Network");
	}

}
