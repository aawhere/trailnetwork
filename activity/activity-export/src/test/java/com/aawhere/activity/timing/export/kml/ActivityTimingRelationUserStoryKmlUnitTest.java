/**
 * 
 */
package com.aawhere.activity.timing.export.kml;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;

import com.aawhere.activity.timing.ActivityTimingRelationUserStoryUnitTest;
import com.aawhere.activity.timing.ActivityTimingRelationUserStory.Builder;
import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.doc.kml.KmlTestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.track.match.CourseAttemptDeviationKmlView;

/**
 * Runs any test in {@link ActivityTimingRelationUserStoryUnitTest} and writes the results to files.
 * 
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class ActivityTimingRelationUserStoryKmlUnitTest
		extends ActivityTimingRelationUserStoryUnitTest {

	@Rule
	public TestName name = new TestName();

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.activity.relation.ActivityTimingRelationUserStoryUnitTest#createUserStoryBuilder
	 * ()
	 */
	@Override
	protected Builder createUserStoryBuilder() {
		return ActivityTimingRelationUserStoryKml.create();
	}

	@After
	public void writeToFile() throws IOException {
		ActivityTimingRelationUserStoryKml kmlStory = (ActivityTimingRelationUserStoryKml) super.story;
		if (kmlStory != null) {
			List<CourseAttemptDeviationKmlView> views = kmlStory.getViews();
			int counter = 1;
			for (CourseAttemptDeviationKmlView view : views) {
				KmlDocument doc = KmlDocument.create().setMain(view.getElement()).build();

				String filename = getClass().getSimpleName() + "." + name.getMethodName() + counter++;
				KmlTestUtil.writeToFile(doc, filename);
			}
		}
	}

}
