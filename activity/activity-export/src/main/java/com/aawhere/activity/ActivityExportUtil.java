/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.text.TextDocumentProducer;

/**
 * @author aroller
 * 
 */
public class ActivityExportUtil {

	public static TextDocumentProducer gpolylineDocumentProducer() {
		return new TextDocumentProducer.Builder().setFormats(DocumentFormatKey.GPOLYLINE).build();
	}
}
