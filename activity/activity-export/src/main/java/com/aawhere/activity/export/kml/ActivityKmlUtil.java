/**
 * 
 */
package com.aawhere.activity.export.kml;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.export.kml.ActivityKml.Builder;
import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.lang.If;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.kml.TrackKmlProcessor;

/**
 * @author aroller
 * 
 */
public class ActivityKmlUtil {

	/**
	 * Given an activity this will return the best name for the activity.
	 * 
	 * @param activity
	 * @return
	 */
	public static String name(Activity activity) {
		return If.nil(activity.getName().toString()).use(activity.getId().toString());
	}

	/**
	 * Converts activities into an entire kml document.
	 * 
	 * @see ActivitiesKml
	 * @param activities
	 * @return
	 */
	public static KmlDocument activitiesKml(Activities activities) {
		ActivitiesKml kml = ActivitiesKml.create().setActivities(activities).build();
		return KmlDocument.create().setMain(kml.getElement()).build();
	}

	/**
	 * Given an activity and track this will return a fine representation of the KML.
	 * 
	 * This produces actual KML, not a network link.
	 * 
	 * @param activity
	 * @param track
	 * @return
	 */
	public static ActivityKml activityTrack(Activity activity, Iterable<Trackpoint> track) {
		final Builder builder = activityTrackBuilder(activity, track);
		ActivityKml activityKml = builder.build();
		return activityKml;
	}

	/**
	 * @see #activityTrack(Activity, Iterable)
	 * @param activity
	 * @param track
	 * @return
	 */
	public static Builder activityTrackBuilder(Activity activity, Iterable<Trackpoint> track) {
		// TODO:move this to a user story
		TrackKmlProcessor processor = new TrackKmlProcessor.Builder().setTrack(track).build();
		final Builder builder = ActivityKml.create(activity).setActualTrack(processor.getTrackLineStrings());
		return builder;
	}
}
