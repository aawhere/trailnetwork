/**
 * 
 */
package com.aawhere.activity.export.kml;

import java.util.ArrayList;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractFeatureType;
import net.opengis.kml.v_2_2_0.ColorModeEnumType;
import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.LineStyleType;
import net.opengis.kml.v_2_2_0.MultiGeometryType;
import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PlacemarkType;
import net.opengis.kml.v_2_2_0.StyleType;

import com.aawhere.activity.Activity;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.style.Color;

/**
 * A single {@link PlacemarkType} that represents a single {@link Activity} containing many
 * components appropriate for the activity.
 * 
 * FIXME: This whole class implemention is shiiiite! 2013-11-11 AR
 * 
 * @author aroller
 * 
 */
public class ActivityKml {
	private static final String FOLDER = "folder";
	private static final String STYLE = "style";
	private static final String TRACK = "track";
	/**
	 * This is just temporary until we figure out how to provide Localized Messages.
	 * 
	 */
	public static final String ACTUAL_TRACK_NAME = "Track";
	private PlacemarkType track;
	private LineStyleType lineStyle;

	/**
	 * Used to construct all instances of ActivityKml.
	 */
	public static class Builder
			extends ObjectBuilder<ActivityKml> {

		private ObjectFactory factory = new ObjectFactory();
		private Activity activity;
		private Boolean visible = Boolean.TRUE;
		private Color lineColor;
		private Double lineWidth = 3.0;
		private MultiGeometryType trackLineStrings;

		public Builder() {
			super(new ActivityKml());
			building.folder = factory.createFolderType();
		}

		public Builder setActivity(Activity activity) {
			this.activity = activity;
			return this;
		}

		public Builder setVisible(Boolean visible) {
			this.visible = visible;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ActivityKml build() {
			ActivityKml built = super.build();
			String activityName;
			if (activity != null) {
				activityName = activity.getId().toString();
			} else {
				activityName = "Activity";
			}
			buildTrack(built);
			built.folder.setName(activityName);
			built.folder.setVisibility(visible);
			if (this.activity != null) {
				built.folder.setId(id(FOLDER));
				built.track.setId(id(TRACK));

			}
			return built;
		}

		private String id(String base) {
			if (this.activity == null) {
				return null;
			}
			return base + "-activity-" + this.activity.id().toString();
		}

		/**
		 * Displays the actual track that was recoarding including showing missing segments.
		 * 
		 * @param line
		 * @return
		 */
		public Builder setActualTrack(MultiGeometryType trackLineStrings) {
			this.trackLineStrings = trackLineStrings;

			return this;
		}

		/**
		 * @param trackLineStrings
		 */
		private void buildTrack(ActivityKml building) {
			building.track = factory.createPlacemarkType();
			building.track.setVisibility(visible);

			building.track.setName(ACTUAL_TRACK_NAME);
			building.track.setAbstractGeometryGroup(factory.createMultiGeometry(trackLineStrings));
			building.folder.getAbstractFeatureGroup().add(factory.createPlacemark(building.track));
			setLineStyle(building, building.track);
			building.track.setId(id(TRACK));
		}

		/**
		 * @param placemark
		 */
		private void setLineStyle(ActivityKml building, PlacemarkType placemark) {
			building.lineStyle = factory.createLineStyleType();
			building.lineStyle.setId(id(STYLE));
			building.lineStyle.setWidth(lineWidth);
			if (this.lineColor == null) {
				building.lineStyle.setColorMode(ColorModeEnumType.RANDOM);
				building.lineStyle.setColor(KmlUtil.color(Color.WHITE));
			} else {
				building.lineStyle.setColor(KmlUtil.color(lineColor));
			}
			StyleType style = factory.createStyleType();
			style.setLineStyle(building.lineStyle);

			placemark.getAbstractStyleSelectorGroup().add(factory.createStyle(style));

		}

		/**
		 * @param lineColor
		 * @return
		 */
		public Builder setLineColor(Color lineColor) {
			this.lineColor = lineColor;
			return this;
		}

		public Builder setLineWidth(Double width) {
			this.lineWidth = width;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	public static Builder create(Activity activity) {
		return create().setActivity(activity);
	}

	/** Use {@link Builder} to construct ActivityKml */
	private ActivityKml() {

	}

	private FolderType folder;
	private ArrayList<JAXBElement<StyleType>> styles = new ArrayList<JAXBElement<StyleType>>();

	/**
	 * @return the folder
	 */
	public FolderType getFolder() {
		return this.folder;
	}

	/**
	 * @return
	 */
	public JAXBElement<? extends AbstractFeatureType> getElement() {
		return new ObjectFactory().createFolder(folder);
	}

	public Iterable<JAXBElement<StyleType>> getStyles() {
		return this.styles;
	}

}
