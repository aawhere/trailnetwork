/**
 * 
 */
package com.aawhere.activity.export.kml;

import java.net.URI;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.DocumentType;
import net.opengis.kml.v_2_2_0.LinkType;
import net.opengis.kml.v_2_2_0.NetworkLinkType;
import net.opengis.kml.v_2_2_0.ObjectFactory;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.ws.rs.SystemWebApiUri;

import com.google.common.net.MediaType;

/**
 * Provided an {@link Activities} this will produce {@link NetworkLinkType}s that will retrieve each
 * individual activity upon request.
 * 
 * @author aroller
 * 
 */
public class ActivitiesKml {

	private DocumentType document;
	private JAXBElement<DocumentType> element;

	/**
	 * Used to construct all instances of ActivitiesKml.
	 */
	public static class Builder
			extends ObjectBuilder<ActivitiesKml> {

		public Builder() {
			super(new ActivitiesKml());
		}

		public Builder setActivities(Activities activities) {
			this.activities = activities;
			return this;
		}

		private Activities activities;

		@Override
		public ActivitiesKml build() {
			ActivitiesKml built = super.build();
			ObjectFactory factory = new ObjectFactory();
			DocumentType doc = factory.createDocumentType();
			for (Activity activity : this.activities) {
				NetworkLinkType link = factory.createNetworkLinkType();
				link.setName(ActivityKmlUtil.name(activity));
				link.setDescription(activity.getDescription());
				final ActivityId activityId = activity.getId();
				URI wsPath = ActivityWebApiUri.getActivityByApplicationId(activityId, MediaType.KML);
				URI absolute = SystemWebApiUri.endpoint(wsPath.toString()).build();
				LinkType linkType = factory.createLinkType();
				linkType.setHref(absolute.toString());
				link.setUrl(linkType);
				doc.getAbstractFeatureGroup().add(factory.createNetworkLink(link));
			}
			built.document = doc;
			built.element = factory.createDocument(doc);
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivitiesKml */
	private ActivitiesKml() {
	}

	/**
	 * @return the document
	 */
	public DocumentType getDocument() {
		return this.document;
	}

	public JAXBElement<DocumentType> getElement() {
		return this.element;
	}
}
