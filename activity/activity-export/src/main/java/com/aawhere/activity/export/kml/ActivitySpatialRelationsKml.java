/**
 * 
 */
package com.aawhere.activity.export.kml;

import com.aawhere.lang.ObjectBuilder;

/**
 * @author aroller
 * 
 */
public class ActivitySpatialRelationsKml {

	/**
	 * Used to construct all instances of ActivitySpatialRelationsKml.
	 */
	public static class Builder
			extends ObjectBuilder<ActivitySpatialRelationsKml> {

		public Builder() {
			super(new ActivitySpatialRelationsKml());
		}

		public ActivitySpatialRelationsKml build() {
			ActivitySpatialRelationsKml built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivitySpatialRelationsKml */
	private ActivitySpatialRelationsKml() {
	}

}
