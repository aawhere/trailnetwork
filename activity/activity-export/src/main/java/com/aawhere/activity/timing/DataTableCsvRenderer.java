/**
 * 
 */
package com.aawhere.activity.timing;

import java.util.List;

import com.aawhere.lang.ObjectBuilder;

import com.google.visualization.datasource.datatable.ColumnDescription;
import com.google.visualization.datasource.datatable.DataTable;
import com.google.visualization.datasource.datatable.TableCell;
import com.google.visualization.datasource.datatable.TableRow;
import com.google.visualization.datasource.datatable.value.ValueType;
import com.google.visualization.datasource.render.CsvRenderer;

/**
 * Replacement of {@link CsvRenderer} due to <a href="
 * http://stackoverflow.com/questions/19653742/google-app-engine
 * -does-not-support-subclasses-of-java-util-logging-logger-com-i">Logging Violation on App
 * Engine</a>.
 * 
 * This is not robust in the hopes it can be removed. TN-421
 * 
 * @author aroller
 * 
 */
public class DataTableCsvRenderer {

	public String csv;

	/**
	 * Used to construct all instances of DataTableCsvRenderer.
	 */
	public static class Builder
			extends ObjectBuilder<DataTableCsvRenderer> {

		private DataTable datable;
		private String separator = ",";

		public Builder() {
			super(new DataTableCsvRenderer());
		}

		public DataTableCsvRenderer build() {
			DataTableCsvRenderer built = super.build();
			StringBuilder result = new StringBuilder();

			List<ColumnDescription> columnDescriptions = this.datable.getColumnDescriptions();
			for (ColumnDescription columnDescription : columnDescriptions) {
				appendQuote(result);
				result.append(columnDescription.getLabel());
				appendQuote(result);
				appendSeparator(result);
			}

			appendNewline(result);

			List<TableRow> rows = this.datable.getRows();
			for (TableRow tableRow : rows) {
				List<TableCell> cells = tableRow.getCells();
				for (TableCell tableCell : cells) {
					if (tableCell.getType() == ValueType.TEXT) {
						appendQuote(result);
					}
					result.append(tableCell.getValue());
					if (tableCell.getType() == ValueType.TEXT) {
						appendQuote(result);
					}
					appendSeparator(result);
				}
				appendNewline(result);
			}
			built.csv = result.toString();
			return built;
		}

		/**
		 * @param result
		 */
		private void appendNewline(StringBuilder result) {
			result.append("\n");
		}

		/**
		 * @param result
		 */
		private void appendSeparator(StringBuilder result) {
			result.append(this.separator);
		}

		/**
		 * @param result
		 */
		private void appendQuote(StringBuilder result) {
			result.append("\"");
		}

		/**
		 * @param separator2
		 * @return
		 */
		public Builder separator(String separator) {
			this.separator = separator;
			return this;
		}

	}// end Builder

	public static Builder create(DataTable dataTable) {
		final Builder builder = new Builder();
		builder.datable = dataTable;
		return builder;
	}

	/** Use {@link Builder} to construct DataTableCsvRenderer */
	private DataTableCsvRenderer() {
	}

	/**
	 * @return the csv
	 */
	public String getCsv() {
		return this.csv;
	}
}
