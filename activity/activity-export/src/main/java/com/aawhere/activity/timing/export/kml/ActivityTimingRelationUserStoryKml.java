/**
 * 
 */
package com.aawhere.activity.timing.export.kml;

import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractFeatureType;
import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.ObjectFactory;

import com.aawhere.activity.timing.ActivityTimingRelationUserStory;
import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.track.match.CourseAttemptDeviationKmlView;
import com.aawhere.track.match.TrackTimingRelationCalculatorKml;

/**
 * Provides the KML view of the {@link ActivityTimingRelationUserStory} for viewing in Google Earth.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationUserStoryKml
		extends ActivityTimingRelationUserStory {

	/**
	 * Used to construct all instances of ActivityTimingRelationKmlUserStory.
	 */
	public static class Builder
			extends ActivityTimingRelationUserStory.Builder {

		public Builder() {
			super(new ActivityTimingRelationUserStoryKml());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.timing.ActivityTimingRelationUserStory.Builder#build()
		 */
		@Override
		public ActivityTimingRelationUserStory build() {
			final ActivityTimingRelationUserStory built = super.build();
			super.buildCalculator(built);
			return built;
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.activity.relation.ActivityTimingRelationUserStory.Builder#createCalculatorBuilder
		 * ()
		 */
		@Override
		protected com.aawhere.track.match.TrackTimingRelationCalculator.Builder createCalculatorBuilder() {
			return TrackTimingRelationCalculatorKml.create();
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTimingRelationKmlUserStory */
	private ActivityTimingRelationUserStoryKml() {
	}

	public List<CourseAttemptDeviationKmlView> getViews() {
		TrackTimingRelationCalculatorKml calc = (TrackTimingRelationCalculatorKml) getCalculator();
		return calc.getViews();
	}

	/**
	 * Provides the views as the root of a ready-to-export KmlDOcument.
	 * 
	 * @return
	 */
	public KmlDocument getKmlDocument() {
		List<CourseAttemptDeviationKmlView> views = getViews();
		ObjectFactory factory = KmlUtil.FACTORY;
		FolderType folder = factory.createFolderType();
		List<JAXBElement<? extends AbstractFeatureType>> group = folder.getAbstractFeatureGroup();
		for (CourseAttemptDeviationKmlView view : views) {
			group.add(view.getElement());
		}

		KmlDocument kmlDocument = new KmlDocument.Builder().setMain(factory.createFolder(folder)).build();
		return kmlDocument;
	}
}
