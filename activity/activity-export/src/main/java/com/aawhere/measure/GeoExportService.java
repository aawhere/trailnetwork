/**
 * 
 */
package com.aawhere.measure;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxes;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.persist.FilteredResponse;

/**
 * Stateless service used to translated {@link GeoCell}, {@link BoundingBox} and other Geo related
 * model objects into web-capable response objects.
 * 
 * @author aroller
 * 
 */
public class GeoExportService {

	/**
	 * Used to construct all instances of GeoExportService.
	 */
	public static class Builder
			extends ObjectBuilder<GeoExportService> {

		public Builder() {
			super(new GeoExportService());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct GeoExportService */
	private GeoExportService() {
	}

	/**
	 * Given a bounding box this will respond with a {@link FilteredResponse} containing a
	 * collection of single-celled {@link GeoCellBoundingBox}es that represent the area at a
	 * reasonable resolution understandable by a viewer.
	 * 
	 * @param boundary
	 * @return
	 */
	public BoundingBoxes geoCellBoundingBoxes(BoundingBox boundary) {
		return GeoCellUtil.boundingBoxes(boundary);
	}

}
