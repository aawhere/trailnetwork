/**
 * 
 */
package com.aawhere.measure;

import net.opengis.kml.v_2_2_0.KmlType;

import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCellUtil;

/**
 * Working with {@link GeoExportService} this exports {@link KmlType} documents.
 * 
 * @author aroller
 * 
 */
public class GeoKmlExportService {

	/**
	 * Used to construct all instances of GeoKmlExportService.
	 */
	public static class Builder
			extends ObjectBuilder<GeoKmlExportService> {

		public Builder() {
			super(new GeoKmlExportService());
		}

		public GeoKmlExportService build() {
			GeoKmlExportService built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct GeoKmlExportService */
	private GeoKmlExportService() {
	}

	public KmlDocument geoCellBoundingBoxes(BoundingBox boundingBox) {

		return KmlMeasureUtil.document(GeoCellUtil.boundingBoxes(boundingBox));
	}

	/**
	 * If bbox is given it will take precedence, otherwise bounding box provided will take
	 * precedence.
	 * 
	 * This allows Google Earth/WMS parameter override if provided, but if not then standard
	 * bounding box can be used.
	 * 
	 * @param boundingBox
	 * @param bbox
	 * @return
	 */
	public KmlDocument geoCellBoundingBoxes(GeoCellBoundingBox boundingBox, String bbox) {
		BoundingBox box;
		if (bbox == null) {
			if (boundingBox == null) {
				box = BoundingBoxUtil.WORLD;
			} else {
				box = boundingBox;
			}
		} else {
			box = WmsBoundingBoxFormat.create().setString(bbox).build().getBoundingBox();
		}
		return geoCellBoundingBoxes(box);
	}
}
