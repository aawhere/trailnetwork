/**
 *
 */
package com.aawhere.activity.spatial;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

/**
 * A collection of {@link ActivitySpatialRelation}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(name = ActivitySpatialRelation.FIELD.DOMAIN_PLURAL, namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class ActivitySpatialRelations
		extends BaseFilterEntities<ActivitySpatialRelation> {

	/**
	 * Use Builder to construct {@link ActivitySpatialRelation}
	 */
	private ActivitySpatialRelations() {
		super();
	}

	@XmlElement(name = ActivitySpatialRelation.FIELD.DOMAIN)
	@Override
	public List<ActivitySpatialRelation> getCollection() {
		return super.getCollection();
	}

	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, ActivitySpatialRelation, ActivitySpatialRelations> {

		public Builder() {
			super(new ActivitySpatialRelations());
		}

	}

	public static Builder create() {
		return new ActivitySpatialRelations.Builder();
	}

}
