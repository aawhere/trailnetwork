package com.aawhere.activity.spatial;

import com.aawhere.persist.FilterRepository;
import com.aawhere.persist.Repository;

/**
 * Storage Repository for {@link ActivitySpatialRelation} objects. Place
 * TrackSimilarityRelation-specific finders here.
 * 
 * @author Brian Chapman
 */
public interface ActivitySpatialRelationRepository
		extends Repository<ActivitySpatialRelation, ActivitySpatialRelationId>,
		FilterRepository<ActivitySpatialRelations, ActivitySpatialRelation, ActivitySpatialRelationId> {

}
