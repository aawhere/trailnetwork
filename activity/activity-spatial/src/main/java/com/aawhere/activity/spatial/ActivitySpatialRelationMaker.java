/**
 *
 */
package com.aawhere.activity.spatial;

import com.aawhere.activity.Activity;
import com.aawhere.jts.geom.match.JtsMatchUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.geocell.GeoCellComparison;
import com.aawhere.track.match.BufferedLineStringSimilarityCalculator;
import com.aawhere.track.match.detector.SameLocationDetector;

import com.vividsolutions.jts.geom.Geometry;

/**
 * Given two tracks this will produce a {@link ActivitySpatialRelation} for the source relationship
 * to the target in both directions as {@link #relation()} and {@link #inverseRelation()}. More
 * specifically it will inspect the spatial overlap between the two tracks.
 * 
 * This produces both relationships for performance considerations since some functions are the same
 * regardless of direction (i.e. identical).
 * 
 * The buffers are passed in to this (rather than calculated) to encourage caching of buffers at the
 * service tier which can be costly.
 * 
 * This assumes there is some sort of spatial overlap done and does not perform simple checks to
 * avoid deep processing. For example using the {@link GeoCellComparison} which is less accurate,
 * but much faster than this. It is encouraged to do that before calculating or retrieving the
 * associated JTS geometries.
 * 
 * @author roller
 * 
 * 
 * 
 */
public class ActivitySpatialRelationMaker {

	private Activity course;
	private Activity attempt;

	private ActivitySpatialRelation relation;
	private ActivitySpatialRelation inverseRelation;

	private Geometry courseLine;
	private Geometry attemptLine;

	private Geometry courseSpatialBuffer;
	private Geometry attemptSpatialBuffer;

	private ActivitySpatialRelationMaker() {
	};

	private void execute() {

		// common attributes
		Boolean sameStartLocation = isSameStartLocation();
		Boolean sameFinishLocation = isSameFinishLocation();
		Boolean identical = JtsMatchUtil.identical(this.courseLine, this.attemptLine);

		// source
		this.relation = createRelation(	this.course,
										this.attempt,
										this.courseLine,
										this.attemptLine,
										this.courseSpatialBuffer,
										this.attemptSpatialBuffer,
										sameStartLocation,
										sameFinishLocation,
										identical);
		// target
		this.inverseRelation = createRelation(	this.attempt,
												this.course,
												this.attemptLine,
												this.courseLine,
												this.attemptSpatialBuffer,
												this.courseSpatialBuffer,
												sameStartLocation,
												sameFinishLocation,
												identical);

	}

	/**
	 * Does the work from the source to the target and creates the relation describing the two
	 * tracks in that direction.
	 * 
	 * @param source
	 * @param target
	 * @param sourceGeometry
	 * @param targetGeometry
	 * @param sameStartLocation
	 * @param sameFinishLocation
	 * @param simultaneous
	 * @param identical
	 * @return
	 */
	private ActivitySpatialRelation createRelation(Activity source, Activity target, Geometry sourceGeometry,
			Geometry targetGeometry, Geometry bufferedSource, Geometry bufferedTarget, Boolean sameStartLocation,
			Boolean sameFinishLocation, Boolean identical) {
		ActivitySpatialRelation.Builder sourceRelationBuilder = new ActivitySpatialRelation.Builder();
		sourceRelationBuilder.course(source);
		sourceRelationBuilder.attempt(target);

		BufferedLineStringSimilarityCalculator calculator = BufferedLineStringSimilarityCalculator.create()
				.setSourceGeometry(sourceGeometry).setTargetGeometry(targetGeometry)
				.setSourceBufferedGeometry(bufferedSource).setTargetBufferedGeometry(bufferedTarget).build();

		sourceRelationBuilder.overlap(calculator.getSourceRatio());

		sourceRelationBuilder.sameStart(sameStartLocation);
		sourceRelationBuilder.sameFinish(sameFinishLocation);
		sourceRelationBuilder.identical(identical);

		ActivitySpatialRelation sourceRelation = sourceRelationBuilder.build();
		return sourceRelation;
	}

	private Boolean isSameStartLocation() {
		return getDetector().hasSameStart();
	}

	private Boolean isSameFinishLocation() {
		return getDetector().hasSameFinish();
	}

	private SameLocationDetector getDetector() {
		return SameLocationDetector.create().setSourceTrackSummary(course.getTrackSummary())
				.setTargetTrackSummary(attempt.getTrackSummary()).build();
	}

	/**
	 * @return the relation from {@link #course} to {@link #attempt}.
	 */
	public ActivitySpatialRelation relation() {
		return relation;
	}

	/**
	 * @return the relation from {@link #attempt} to {@link #course}.
	 */
	public ActivitySpatialRelation inverseRelation() {
		return inverseRelation;
	}

	public static class Builder
			extends ObjectBuilder<ActivitySpatialRelationMaker> {

		public Builder() {
			super(new ActivitySpatialRelationMaker());
		}

		public Builder courseLine(Geometry sourceTrackGeometry) {
			building.courseLine = sourceTrackGeometry;
			return this;
		}

		public Builder attemptLine(Geometry targetTrackGeometry) {
			building.attemptLine = targetTrackGeometry;
			return this;
		}

		/**
		 * @param source
		 *            the source to set
		 */
		public Builder course(Activity activity) {
			building.course = activity;
			return this;
		}

		/**
		 * @param target
		 *            the target to set
		 */
		public Builder attempt(Activity activity) {
			building.attempt = activity;
			return this;
		}

		public Builder attemptSpatialBuffer(Geometry targetBufferedTrackGeometry) {
			building.attemptSpatialBuffer = targetBufferedTrackGeometry;
			return this;
		}

		public Builder courseSpatialBuffer(Geometry sourceBufferedTrackGeometry) {
			building.courseSpatialBuffer = sourceBufferedTrackGeometry;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ActivitySpatialRelationMaker build() {
			ActivitySpatialRelationMaker maker = super.build();
			maker.execute();
			return maker;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull("sourceActivity", building.course);
			Assertion.assertNotNull("targetActivity", building.attempt);
			Assertion.assertNotNull("sourceGeometry", building.courseLine);
			Assertion.assertNotNull("targetGeometry", building.attemptLine);
			Assertion.exceptions().notNull("courseSpatialBuffer", building.courseSpatialBuffer);
			Assertion.exceptions().notNull("attemptSpatialBuffer", building.attemptSpatialBuffer);

			super.validate();
		}
	}

	/**
	 * 
	 */
	public static Builder create() {
		return new Builder();
	}

}
