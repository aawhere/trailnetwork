/**
 *
 */
package com.aawhere.activity.spatial;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.ActivityId;
import com.aawhere.measure.Ratio;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.Sets;

/**
 * A filter used to request and describe a subset of {@link ActivitySpatialRelation} in the system.
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(name = "ActivityRelationsFilter", namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class ActivitySpatialRelationsFilterBuilder
		extends Filter.BaseFilterBuilder<ActivitySpatialRelationsFilterBuilder> {

	private Ratio similarity;
	private FilterOperator similarityOperator;

	private ActivityId sourceId;

	private FilterOperator sourceIdOperator;

	private ActivityId targetId;

	private FilterOperator targetIdOperator;

	public static ActivitySpatialRelationsFilterBuilder create() {
		return new ActivitySpatialRelationsFilterBuilder();
	}

	/**
	 * @param filter
	 */
	private ActivitySpatialRelationsFilterBuilder() {
		super();
		// default sort order
		orderBySimilarity();
	}

	/**
	 * Provides mutation access to {@link #mutate(Filter)}.
	 * 
	 * @param filter
	 */
	private ActivitySpatialRelationsFilterBuilder(Filter filter) {
		super(filter);
	}

	/** Allows trusted friends to mutate the filter. */
	static ActivitySpatialRelationsFilterBuilder mutate(Filter filter) {
		return new ActivitySpatialRelationsFilterBuilder(filter);
	}

	public ActivitySpatialRelationsFilterBuilder setSimilarity(Ratio similarity, FilterOperator operator) {
		this.similarity = similarity;
		this.similarityOperator = operator;
		return this;
	}

	/**
	 * Provides a flexible way to retrieve a relationship when the source if of interest.
	 * 
	 * @param sourceId
	 * @param operator
	 * @return
	 */
	public ActivitySpatialRelationsFilterBuilder setSourceId(ActivityId sourceId, FilterOperator operator) {
		this.sourceId = sourceId;
		this.sourceIdOperator = operator;
		return this;
	}

	/**
	 * One or more ids that are the source where the operator is {@link FilterOperator#IN}.
	 * 
	 * @see #setSourceId(Set)
	 * @param sourceIds
	 * @return
	 */
	public ActivitySpatialRelationsFilterBuilder setSourceId(ActivityId... sourceIds) {
		setSourceId(Sets.newHashSet(Arrays.asList(sourceIds)));
		return this;
	}

	/**
	 * One or more ids that are the source where the operator is {@link FilterOperator#IN}.
	 * 
	 * @param sourceIds
	 * @return
	 */
	public ActivitySpatialRelationsFilterBuilder setSourceId(Set<ActivityId> sourceIds) {
		addCondition(new FilterCondition.Builder<Set<ActivityId>>().field(ActivitySpatialRelation.FIELD.KEY.COURSE_ID)
				.operator(FilterOperator.IN).value(sourceIds).build());

		return this;
	}

	public ActivitySpatialRelationsFilterBuilder setTargetId(ActivityId targetId, FilterOperator operator) {
		this.targetId = targetId;
		this.targetIdOperator = operator;

		return this;
	}

	public ActivitySpatialRelationsFilterBuilder setTargetId(ActivityId... activityIds) {
		setTargetId(new TreeSet<ActivityId>(Arrays.asList(activityIds)));
		return this;
	}

	public ActivitySpatialRelationsFilterBuilder setTargetId(Set<ActivityId> targetIds) {
		FilterCondition<Set<ActivityId>> condition = new FilterCondition.Builder<Set<ActivityId>>()
				.field(ActivitySpatialRelation.FIELD.KEY.ATTEMPT_ID).operator(FilterOperator.IN).value(targetIds)
				.build();
		addCondition(condition);
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.ObjectBuilder#build()
	 */
	@Override
	public Filter build() {

		if (this.sourceId != null) {
			addCondition(new FilterCondition.Builder<ActivityId>().field(ActivitySpatialRelation.FIELD.KEY.COURSE_ID)
					.operator(sourceIdOperator).value(sourceId).build());
		}
		if (this.targetId != null) {
			addCondition(new FilterCondition.Builder<ActivityId>().field(ActivitySpatialRelation.FIELD.KEY.ATTEMPT_ID)
					.operator(targetIdOperator).value(targetId).build());
		}
		if (this.similarity != null) {

			addCondition(new FilterCondition.Builder<Ratio>().field(ActivitySpatialRelation.FIELD.KEY.OVERLAP)
					.operator(this.similarityOperator).value(this.similarity).build());
		}
		return super.build();
	}

	/**
	 * Defines and makes available the default sort for relations which is the highest similarity
	 * first.
	 * 
	 * @return
	 * 
	 */
	public ActivitySpatialRelationsFilterBuilder orderBySimilarity() {
		// default sort order should always be most similar first
		orderBy(ActivitySpatialRelation.FIELD.KEY.OVERLAP, Direction.DESCENDING);
		return this;
	}

}
