/**
 *
 */
package com.aawhere.activity.spatial;

import com.aawhere.activity.ActivityId;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.StringIdentifier;

/**
 * Identifier for {@link ActivitySpatialRelation}
 * 
 * @author Brian Chapman
 * 
 */
public class ActivitySpatialRelationId
		extends StringIdentifier<ActivitySpatialRelation> {

	private static final long serialVersionUID = 6000644104031917090L;

	/**
	 * @param kind
	 */
	ActivitySpatialRelationId() {
		super(ActivitySpatialRelation.class);
	}

	/**
	 * Provided to any client that may have a Long formatted as a String.
	 * 
	 * @param value
	 * @param kind
	 */
	public ActivitySpatialRelationId(String value) {
		super(value, ActivitySpatialRelation.class);
	}

	/**
	 * Takes a {@link ActivitySpatialRelation} and creates a composite Id using the target and
	 * source ids.
	 * 
	 * @param entity
	 */
	public ActivitySpatialRelationId(ActivitySpatialRelation entity) {
		super(
				IdentifierUtils.createCompositeIdValue(entity.course().getActivityId(), entity.attempt()
						.getActivityId()), ActivitySpatialRelation.class);
	}

	/**
	 * Takes a {@link ActivitySpatialRelation} and creates a composite Id using the target and
	 * source ids.
	 * 
	 * @param entity
	 */
	public ActivitySpatialRelationId(ActivityId sourceId, ActivityId targetId) {
		super(IdentifierUtils.createCompositeIdValue(sourceId, targetId), ActivitySpatialRelation.class);
	}

}
