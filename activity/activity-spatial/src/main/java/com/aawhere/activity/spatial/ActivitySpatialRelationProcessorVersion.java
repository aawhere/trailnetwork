/**
 * 
 */
package com.aawhere.activity.spatial;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.ProcessorVersion;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlEnum
@XmlType(namespace = XmlNamespace.API_VALUE)
public enum ActivitySpatialRelationProcessorVersion implements Message, ProcessorVersion {

	/** Launch */
	A("Launch Version.");

	private ParamKey[] parameterKeys;
	private String message;

	private ActivitySpatialRelationProcessorVersion(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** EXPLAIN_PARAM_HERE */
		FIRST_PARAM;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
