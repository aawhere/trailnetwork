/**
 *
 */
package com.aawhere.activity.spatial;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.EntityMessage;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Personalized messages for {@link ActivitySpatialRelation}
 * 
 * @author Brian Chapman
 * 
 */
@XmlTransient
public enum ActivitySpatialRelationMessage implements Message {

	ID_NAME(EntityMessage.ID_NAME.getValue()),
	ID_DESCRIPTION(EntityMessage.ID_DESCRIPTION.getValue()),
	ACTIVITY_RELATION_NAME("Activity Relation"),
	ACTIVITY_RELATION_DESCRIPTION("Describes related attributes between two activities"),
	SOURCE_NAME("Source Identifier"),
	SOURCE_DESCRIPTION("Source activity identifier for the relationship"),
	TARGET_NAME("Target Identifier"),
	TARGET_DESCRIPTION("Target activity identifier for the relationship"),
	SIMILARITY_NAME("Similarity"),
	SIMILARITY_DESCRIPTION("Describes how similar the two activities tracks are to each other"),
	/** Message for explaining why a relation is not persisted. */
	INSIGNIFICANT_RELATION(
			"{SOURCE} had only {SIMILARITY} similarity compared to {TARGET}. Greater than {MIN_SIMILARITY} is required for persistence.",
			Param.SOURCE,
			Param.TARGET,
			Param.SIMILARITY,
			Param.MIN_SIMILARITY),
	DATE_CREATED_NAME(EntityMessage.DATE_CREATED_NAME.getValue()), /* */
	DATE_CREATED_DESCRIPTION(EntityMessage.DATE_CREATED_DESCRIPTION.getValue()), /* */
	DATE_UPDATED_NAME(EntityMessage.DATE_UPDATED_NAME.getValue()), /* */
	DATE_UPDATED_DESCRIPTION(EntityMessage.DATE_UPDATED_DESCRIPTION.getValue()), /* */
	ACTIVITY_SPATIAL_RELATION_NAME("Activity Spacial Relation"),
	ACTIVITY_SPATIAL_RELATION_DESCRIPTION(
			"Summarizes similarities between two tracks. The source is the baseline input and the target is the activity that is compared against the source. Each relation also has an inverse relation with the source and target swapped.");

	private ParamKey[] parameterKeys;
	private String message;

	private ActivitySpatialRelationMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** The id for the source. */
		SOURCE,
		/** The id for the target */
		TARGET,
		/** The minimum required similarity for a significant relationship. */
		MIN_SIMILARITY,
		/**
		 * The similarity as defined in {@link ActivitySpatialRelation#getSimilarity()}
		 */
		SIMILARITY;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}
}
