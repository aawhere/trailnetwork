/**
 *
 */
package com.aawhere.activity.spatial;

import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.geocell.GeoCellComparison;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.util.rb.MessageStatus;
import com.aawhere.util.rb.StatusMessages;

import com.google.common.base.Function;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Provides the implementation to satisfy the user stories related to creating a
 * {@link ActivitySpatialRelation}
 * 
 * @author Brian Chapman
 * @author aroller
 * @since 15
 */
public class ActivitySpatialRelationUserStory
		implements Function<ActivitySpatialRelationId, Pair<ActivitySpatialRelation, ActivitySpatialRelation>> {

	public static final Ratio DEFAULT_SIMILARITY_CUTOFF = MeasurementUtil.createRatio(0.5d);
	public static final Length DEFAULT_BUFFER_DISTANCE = MeasurementUtil.createLengthInMeters(20);
	public static final Length SAME_LOCATION_DISTANCE = DEFAULT_BUFFER_DISTANCE;

	static final ActivitySpatialRelationProcessorVersion VERSION = ActivitySpatialRelationProcessorVersion.A;

	private Function<ActivityId, Activity> activityProvider;
	private Function<ActivityId, MultiLineString> lineProvider;
	private Function<ActivityId, Geometry> bufferProvider;

	private StatusMessages.Builder messages = new StatusMessages.Builder();

	/** Use {@link Builder} to construct TrackSummaryUserStories */
	private ActivitySpatialRelationUserStory() {
	}

	/**
	 * Given the two ActivityIds, this will relate the two and provide a bi-directional spatial
	 * relation by attempting the most efficient comparisons first, then calling for some of the
	 * heavier data to do some deep analysis.
	 * 
	 * @param courseId
	 * @param attemptId
	 * @return
	 */
	public Pair<ActivitySpatialRelation, ActivitySpatialRelation> relate(ActivityId courseId, ActivityId attemptId) {
		if (courseId == null || attemptId == null) {
			return null;
		}
		return apply(new ActivitySpatialRelationId(courseId, attemptId));
	}

	@Override
	@Nullable
	public Pair<ActivitySpatialRelation, ActivitySpatialRelation> apply(@Nullable ActivitySpatialRelationId input) {

		Pair<ActivitySpatialRelation, ActivitySpatialRelation> result = null;

		if (input != null) {

			Pair<ActivityId, ActivityId> ids = ActivitySpatialRelationsUtil.split(input);

			ActivityId courseId = ids.getLeft();
			ActivityId attemptId = ids.getRight();
			Activity course = activityProvider.apply(courseId);
			if (course != null) {
				Activity attempt = activityProvider.apply(attemptId);
				if (attempt != null) {
					Boolean intersects = GeoCellComparison.left(course.getTrackSummary().getSpatialBuffer())
							.right(attempt.getTrackSummary().getSpatialBuffer()).build().intersects();
					if (intersects) {
						ActivitySpatialRelationMaker maker = ActivitySpatialRelationMaker.create().course(course)
								.attempt(attempt).courseLine(lineProvider.apply(courseId))
								.attemptLine(lineProvider.apply(attemptId))
								.courseSpatialBuffer(bufferProvider.apply(courseId))
								.attemptSpatialBuffer(bufferProvider.apply(attemptId)).build();
						result = Pair.of(maker.relation(), maker.inverseRelation());
					} else {
						result = ActivitySpatialRelationsUtil.noBiRelation(course, attempt);
					}
				} else {
					messages.addMessage(MessageStatus.FAILURE, EntityNotFoundException.message(attemptId));
				}
			} else {
				messages.addMessage(MessageStatus.FAILURE, EntityNotFoundException.message(courseId));
			}
		} else {
			messages.addMessage(MessageStatus.FAILURE, EntityNotFoundException.message(input));
		}
		return result;
	}

	public StatusMessages messages() {
		return this.messages.build();
	}

	/**
	 * Used to construct all instances of TrackSimilarityRelationUserStory.
	 * 
	 * 
	 */
	public static class Builder
			extends ObjectBuilder<ActivitySpatialRelationUserStory> {

		public Builder() {
			super(new ActivitySpatialRelationUserStory());
		}

		public Builder activityProvider(Function<ActivityId, Activity> activityProvider) {
			building.activityProvider = activityProvider;
			return this;
		}

		public Builder lineProvider(Function<ActivityId, MultiLineString> lineProvider) {
			building.lineProvider = lineProvider;
			return this;
		}

		public Builder bufferProvider(Function<ActivityId, Geometry> bufferProvider) {
			building.bufferProvider = bufferProvider;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("lineProvider", building.lineProvider);
			Assertion.exceptions().notNull("activityProvider", building.activityProvider);
			Assertion.exceptions().notNull("bufferProvider", building.bufferProvider);
		}

		@Override
		public ActivitySpatialRelationUserStory build() {
			ActivitySpatialRelationUserStory story = super.build();
			return story;
		}

	}// end Builder

	public static Builder create() {
		return new ActivitySpatialRelationUserStory.Builder();
	}

	/**
	 * Simple indicator if the given version is not equal to the latest {@link #VERSION}.
	 * 
	 * @param mostRecentVersion
	 * @return
	 */
	public static Boolean isStale(@Nullable ActivitySpatialRelationProcessorVersion mostRecentVersion) {
		// returns true if null
		return !VERSION.equals(mostRecentVersion);
	}

}
