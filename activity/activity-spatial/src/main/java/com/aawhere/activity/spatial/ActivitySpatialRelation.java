/**
 *
 */
package com.aawhere.activity.spatial;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.Ratio;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.xml.XmlNamespace;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;

/**
 * Summarizes similarities between two tracks from a geospatial perspective...the source being the
 * baseline input and the target being the one attempting relation. For this reason there should
 * always be a {@link ActivitySpatialRelation} of the inverse.
 * 
 * @author roller
 * 
 */
@XmlRootElement
@XmlType(name = ActivitySpatialRelation.FIELD.DOMAIN, namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Unindex
@Entity
@Cache
@Dictionary(domain = ActivitySpatialRelation.FIELD.DOMAIN, messages = ActivitySpatialRelationMessage.class)
public final class ActivitySpatialRelation
		extends StringBaseEntity<ActivitySpatialRelation, ActivitySpatialRelationId>
		implements SelfIdentifyingEntity {

	private static final long serialVersionUID = -3213695592780779144L;

	@Index
	@XmlElement(name = FIELD.COURSE)
	@Field(key = FIELD.COURSE)
	private ActivityReferenceIds course;

	@Index
	@XmlElement(name = FIELD.ATTEMPT)
	@Field(key = FIELD.ATTEMPT)
	private ActivityReferenceIds attempt;

	@XmlAttribute
	private Boolean identical;

	@Index
	@XmlElement
	@com.aawhere.field.annotation.Field(key = ActivitySpatialRelation.FIELD.OVERLAP, dataType = FieldDataType.NUMBER)
	private Ratio overlap;

	@XmlAttribute
	private Boolean sameStart;

	@XmlAttribute
	private Boolean sameFinish;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@com.aawhere.field.annotation.Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM)
	@Override
	public ActivitySpatialRelationId getId() {
		return super.getId();
	}

	/**
	 * indicates that the source is identical to the target. Useful when determining if two tracks
	 * are the same.
	 * 
	 * @return True if identical
	 */
	public Boolean isIdentical() {
		if (identical == null) {
			return false;
		}
		return identical;
	}

	/**
	 * The ratio of the points along the {@link #course} visited by the points along the
	 * {@link #attempt}. If the attempt never crossed the course the ratio is 0. If the attempt
	 * visited every point of the course the ratio is 1. The amount the attempt travels beyond the
	 * course does not affect this ratio, however, the inverse relationship would provide such
	 * insight.
	 * 
	 * @return similarityRatio
	 */
	public Ratio overlap() {
		return overlap;
	}

	/**
	 * Both the source and the target track started in the same location. This comparison is done
	 * given a radius in which two points are considered the same location. Having the same start
	 * (and end) points can identify a Trailhead or it may indicate a "sub-loop" within a larger
	 * network of trails.
	 * 
	 * @return True if start locations are equal
	 */
	public Boolean sameStart() {
		return sameStart;
	}

	/**
	 * Both the source and the target track finished at the same location. This comparison is done
	 * given a radius in which two points are considered the same location. Having the same finish
	 * (and start) points can identify a trailhead or it may indicate a "sub-loop" within a larger
	 * network of trails.
	 * 
	 * @return true if finish locations are equal
	 */
	public Boolean sameFinish() {
		return sameFinish;
	}

	/**
	 * @return the target
	 */
	public ActivityReferenceIds attempt() {
		return this.attempt;
	}

	/**
	 * @return the source
	 */
	public ActivityReferenceIds course() {
		return this.course;
	}

	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<ActivitySpatialRelation, Builder, ActivitySpatialRelationId> {

		public Builder() {
			super(new ActivitySpatialRelation());
		}

		protected Builder(ActivitySpatialRelation relation) {
			super(relation);
		}

		public Builder course(Activity source) {
			course(new ActivityReferenceIds.Builder().setActivity(source).build());
			return this;
		}

		/**
		 * @param source
		 * @return
		 */
		public Builder course(ActivityReferenceIds source) {
			building.course = source;
			return this;
		}

		public Builder attempt(Activity target) {
			attempt(new ActivityReferenceIds.Builder().setActivity(target).build());
			return this;
		}

		public Builder attempt(ActivityReferenceIds target) {
			building.attempt = target;
			return this;
		}

		public Builder identical(Boolean identical) {
			building.identical = identical;
			return this;
		}

		public Builder overlap(Ratio similiarity) {
			building.overlap = similiarity;
			return this;
		}

		public Builder sameStart(Boolean sameStartLocation) {
			building.sameStart = sameStartLocation;
			return this;
		}

		public Builder sameFinish(Boolean sameFinishLocation) {
			building.sameFinish = sameFinishLocation;
			return this;
		}

		@Override
		public void validate() {
			Assertion.assertNotNull("target", building.attempt);
			Assertion.assertNotNull("source", building.course);
		}

		@Override
		public ActivitySpatialRelation build() {
			setId(new ActivitySpatialRelationId(building.course.getActivityId(), building.attempt().getActivityId()));

			ActivitySpatialRelation built = super.build();

			return built;
		}

	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(ActivitySpatialRelation relation) {
		return new Builder(relation);
	}

	public static class FIELD {
		public static final String DOMAIN = "activitySpatial";
		public static final String DOMAIN_PLURAL = DOMAIN + "s";
		public static final String COURSE = "course";
		public static final String ATTEMPT = "attempt";
		public static final String OVERLAP = "overlap";

		public static final class KEY {
			public static final FieldKey OVERLAP = new FieldKey(DOMAIN, FIELD.OVERLAP);
			public static final FieldKey COURSE = new FieldKey(DOMAIN, FIELD.COURSE);
			public static final FieldKey ATTEMPT = new FieldKey(DOMAIN, FIELD.ATTEMPT);
			public static final FieldKey ATTEMPT_ID = new FieldKey(ATTEMPT, ActivityReferenceIds.FIELD.KEY.ACTIVITY_ID);
			public static final FieldKey COURSE_ID = new FieldKey(COURSE, ActivityReferenceIds.FIELD.KEY.ACTIVITY_ID);

		}
	}
}
