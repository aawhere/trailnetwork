/**
 *
 */
package com.aawhere.activity.spatial;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.measure.MeasurementUtil;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

/**
 * @author Brian Chapman
 * 
 */
public class ActivitySpatialRelationsUtil {

	public static Set<ActivityId> getSourceActivityIds(ActivitySpatialRelations relations) {
		Set<ActivityId> ids = new HashSet<ActivityId>();
		for (ActivitySpatialRelation relation : relations) {
			ids.add(relation.course().getActivityId());
		}
		return ids;
	}

	public static Set<ActivityId> getTargetActivityIds(Iterable<ActivitySpatialRelation> relations) {
		Set<ActivityId> ids = new HashSet<ActivityId>();
		for (ActivitySpatialRelation relation : relations) {
			ids.add(relation.attempt().getActivityId());
		}
		return ids;
	}

	/**
	 * Produces a relation indicating no similarity exists.
	 * 
	 * @param course
	 * @param attempt
	 */
	public static ActivitySpatialRelation noRelation(Activity course, Activity attempt) {
		return ActivitySpatialRelation.create().course(attempt).attempt(attempt).overlap(MeasurementUtil.RATIO_ZERO)
				.build();
	}

	/**
	 * The common situation since if there is no relation in one direction then the inverse applies.
	 * 
	 * @param course
	 * @param attempt
	 * @return pair with left as the course-attempt relation and right as the inverse.
	 */
	public static Pair<ActivitySpatialRelation, ActivitySpatialRelation>
			noBiRelation(Activity course, Activity attempt) {
		return Pair.of(noRelation(course, attempt), noRelation(attempt, course));
	}

	/**
	 * Provided a timing id this will split into course (left) and attempt (right) ids.
	 * 
	 * @param id
	 * @return
	 */
	public static Pair<ActivityId, ActivityId> split(ActivitySpatialRelationId id) {
		return IdentifierUtils.splitCompositeId(id, ActivityId.class);
	}

	/**
	 * Swaps the course and attempt to represent the inverse relationship of what is given.
	 * 
	 * @param spatialId
	 */
	public static ActivitySpatialRelationId inverseId(ActivitySpatialRelationId spatialId) {
		Pair<ActivityId, ActivityId> split = split(spatialId);
		return new ActivitySpatialRelationId(split.getRight(), split.getLeft());
	}

	/**
	 * provides an {@link ActivitySpatialRelationId} for each activity id given ensure that each id
	 * is paired up once and only once, regardless who's first. It's intended to be use with a
	 * bi-relational request.
	 * 
	 * @param courseIds
	 * @return
	 */
	public static ImmutableSet<ActivitySpatialRelationId> idCombinations(Set<ActivityId> activityIds) {
		LinkedList<ActivityId> queue = Lists.newLinkedList(activityIds);
		ImmutableSet.Builder<ActivitySpatialRelationId> ids = ImmutableSet.builder();
		while (queue.size() > 1) {
			ActivityId left = queue.removeFirst();
			for (int i = 0; i < queue.size(); i++) {
				ActivityId right = queue.get(i);
				ids.add(new ActivitySpatialRelationId(left, right));
			}
		}
		return ids.build();

	}

}
