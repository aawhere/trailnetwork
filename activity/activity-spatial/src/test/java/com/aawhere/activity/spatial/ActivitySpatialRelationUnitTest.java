/**
 *
 */
package com.aawhere.activity.spatial;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityUnitTest;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.quantity.RatioUnitTest;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.MockFilterRepository;

/**
 * @author Brian Chapman
 * 
 */
public class ActivitySpatialRelationUnitTest
		extends
		BaseEntityJaxbBaseUnitTest<ActivitySpatialRelation, ActivitySpatialRelation.Builder, ActivitySpatialRelationId, ActivitySpatialRelationRepository> {

	public ActivitySpatialRelationUnitTest() {
		super(NON_MUTABLE);
	}

	private Activity source;
	private Activity target;
	private final Ratio similarity = RatioUnitTest.generateRandomRatio();
	private ActivitySpatialRelationProcessorVersion version;

	@Override
	@Before
	public void setUp() {

		this.source = ActivityUnitTest.getInstance().getEntitySampleWithId();
		this.target = ActivityUnitTest.getInstance().getEntitySampleWithId();
		this.version = ActivitySpatialRelationProcessorVersion.A;
	}

	/** Ensures the same person assignments are working properly */
	@Test
	public void testSamePerson() {
		ActivitySpatialRelation.Builder builder = ActivitySpatialRelation.create();
		creationPopulation(builder);
		// assign the source to the target making the person the same.
		builder.attempt(source);

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere
	 * .persist.BaseEntity.Builder)
	 */
	@Override
	protected void creationPopulation(ActivitySpatialRelation.Builder builder) {
		builder.course(source);
		builder.attempt(target);
		builder.overlap(similarity);
		super.creationPopulation(builder);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	public void assertCreation(ActivitySpatialRelation sample) {
		super.assertCreation(sample);
		assertEquals(source.getId(), sample.course().getActivityId());
		assertEquals(target.getId(), sample.attempt().getActivityId());
	}

	public static ActivitySpatialRelationUnitTest getInstance() {
		ActivitySpatialRelationUnitTest instance = new ActivitySpatialRelationUnitTest();
		instance.baseSetUp();
		instance.setUp();
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return ActivitySpatialRelationTestUtil.adapters();
	}

	public static class ActivitySpatialRelationMockRepository
			extends MockFilterRepository<ActivitySpatialRelations, ActivitySpatialRelation, ActivitySpatialRelationId>
			implements ActivitySpatialRelationRepository {
		@Override
		protected ActivitySpatialRelationId generateRandomId(ActivitySpatialRelation entity) {
			return new ActivitySpatialRelationId(entity);
		}
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected ActivitySpatialRelationRepository createRepository() {
		return new ActivitySpatialRelationMockRepository();
	}
}
