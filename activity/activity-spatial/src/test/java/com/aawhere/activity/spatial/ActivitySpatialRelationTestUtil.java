/**
 * 
 */
package com.aawhere.activity.spatial;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.ExtraUnits;

/**
 * @author aroller
 * 
 */
public class ActivitySpatialRelationTestUtil {

	public void printRelation(ActivitySpatialRelation relation, String sourceFileName, String targetFileName) {
		final String newLine = System.getProperty("line.separator"); // Platform new line
		StringBuffer sb = new StringBuffer();
		sb.append("**************************").append(newLine);
		sb.append("Relationship between ").append(sourceFileName).append(" and ").append(targetFileName)
				.append(newLine);

		sb.append("Similarity Percentate: ").append(relation.overlap().doubleValue(ExtraUnits.PERCENT)).append(newLine);
		sb.append("Identical: ").append(relation.isIdentical()).append(newLine);
		sb.append("SameFinishLocation: ").append(relation.sameFinish()).append(newLine);
		sb.append("SameStartLocation: ").append(relation.sameStart()).append(newLine);

		sb.append(newLine).append(newLine);
		System.out.print(sb.toString());
	}

	public static void assertRelation(ActivitySpatialRelation relation, Boolean identical, Ratio expectedOverlap) {

		assertNotNull(relation.course().getActivityId());
		assertNotNull(relation.attempt().getActivityId());
		assertNotNull(relation.isIdentical());
		assertEquals("identical", identical, relation.isIdentical());
		assertNotNull(relation.sameStart());
		assertNotNull(relation.sameFinish());
		if (identical) {
			assertTrue("start", relation.sameStart());
			assertTrue("finish", relation.sameFinish());
		}
		// Due to the buffer around the JTS Geometry, you will not get back the exact expected
		// overlap, instead it will be slightly off by the buffer amount.
		assertEquals(	expectedOverlap.doubleValue(ExtraUnits.RATIO),
						relation.overlap().doubleValue(ExtraUnits.RATIO),
						0.001);
	}

	public static Set<XmlAdapter<?, ?>> adapters() {
		return ActivityTestUtil.adapters();
	}
}
