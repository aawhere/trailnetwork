package com.aawhere.activity.spatial;

import static com.aawhere.activity.spatial.ActivitySpatialRelationsUtil.*;

import java.util.HashSet;

import org.junit.Test;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.test.TestUtil;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class ActivitySpatialRelationUtilUnitTest {

	@Test
	public void testIdsCombinationSize1() {
		TestUtil.assertEmpty(idCombinations(ImmutableSet.of(ActivityTestUtil.activityId())));
	}

	@Test
	public void testIdCombinationsSize2() {
		ActivityId first = ActivityTestUtil.activityId();
		ActivityId second = ActivityTestUtil.activityId();
		String message = "two in = 1 out";
		ImmutableSet<ActivitySpatialRelationId> expected = ImmutableSet
				.of(new ActivitySpatialRelationId(first, second));
		ImmutableList<ActivityId> input = ImmutableList.of(first, second);
		assertIdCombinations(message, input, expected);

	}

	@Test
	public void testIdCombinationsSize3() {
		ActivityId first = ActivityTestUtil.activityId();
		ActivityId second = ActivityTestUtil.activityId();
		ActivityId third = ActivityTestUtil.activityId();
		String message = "three in = 3 out";
		ImmutableSet<ActivitySpatialRelationId> expected = ImmutableSet
				.of(new ActivitySpatialRelationId(first, second),
					new ActivitySpatialRelationId(first, third),
					new ActivitySpatialRelationId(second, third));
		ImmutableList<ActivityId> input = ImmutableList.of(first, second, third);
		assertIdCombinations(message, input, expected);
	}

	public void assertIdCombinations(String message, ImmutableList<ActivityId> input,
			ImmutableSet<ActivitySpatialRelationId> expected) {
		ImmutableSet<ActivitySpatialRelationId> idCombinations = idCombinations(new HashSet<>(input));
		// we can't know the order the method will choose so compare size only
		TestUtil.assertSize(input.toString(), expected.size(), idCombinations);
	}
}
