/**
 *
 */
package com.aawhere.activity.spatial;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author brian
 * 
 */
public class ActivitySpatialRelationIdUnitTest {

	private final String TEST_ID = "test";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() {
		ActivitySpatialRelationId id = new ActivitySpatialRelationId(TEST_ID);
		assertCreated(id, TEST_ID);

		ActivitySpatialRelationId id2 = new ActivitySpatialRelationId(TEST_ID);
		assertCreated(id2, TEST_ID);
	}

	@Test
	public void testCreateIdentifierFromEntity() {
		ActivitySpatialRelation entity = ActivitySpatialRelationUnitTest.getInstance().getEntitySample();
		ActivitySpatialRelationId id = new ActivitySpatialRelationId(entity);
		assertCreated(id, entity.course().getActivityId().getValue().toString() + "-"
				+ entity.attempt().getActivityId().getValue().toString());
	}

	@Test
	public void testCreateIdentifierFromActivityIds() {
		ActivitySpatialRelation entity = ActivitySpatialRelationUnitTest.getInstance().getEntitySample();
		ActivitySpatialRelationId id = new ActivitySpatialRelationId(entity.course().getActivityId(), entity.attempt()
				.getActivityId());
		assertCreated(id, entity.course().getActivityId().getValue().toString() + "-"
				+ entity.attempt().getActivityId().getValue().toString());
	}

	private void assertCreated(ActivitySpatialRelationId id, String idValue) {
		assertNotNull(id);
		assertNotNull(idValue);
		assertEquals(idValue, id.getValue());
	}

}
