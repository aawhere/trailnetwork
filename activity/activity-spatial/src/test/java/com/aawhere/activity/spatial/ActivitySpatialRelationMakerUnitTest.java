/**
 *
 */
package com.aawhere.activity.spatial;

import static com.aawhere.activity.spatial.ActivitySpatialRelationTestUtil.*;

import java.io.IOException;

import org.junit.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.doc.track.gpx._1._1.GpxTestUtilFactory;
import com.aawhere.doc.track.gpx._1._1.GpxTrack;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxTestUtil.GpxTestDocument;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.Track;
import com.aawhere.track.match.BufferedLineStringSimilarityCalculatorUnitTest;
import com.aawhere.xml.bind.JAXBReadException;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Mostly enforces the population and function of {@link ActivitySpatialRelation} from
 * {@link ActivitySpatialRelationMaker}'s.
 * 
 * Actual similarity testing is done at the more granular
 * {@link BufferedLineStringSimilarityCalculatorUnitTest}.
 * 
 * @author roller
 * @see ActivitySpatialRelationMaker
 */
public class ActivitySpatialRelationMakerUnitTest {

	Activity activity1 = ActivityTestUtil.createActivityWithId();
	Activity activity2 = ActivityTestUtil.createActivityWithId();
	private boolean NOT_IDENTICAL = false;
	Track track1 = ExampleTracks.LINE;
	Track track2 = ExampleTracks.LINE_LONGER;
	MultiLineString lineString1 = JtsTrackGeomUtil.trackToMultiLineString(track1);
	MultiLineString lineString2 = JtsTrackGeomUtil.trackToMultiLineString(track2);
	Geometry buffer1 = lineString1.buffer(0.0001);
	Geometry buffer2 = lineString2.buffer(0.0001);

	public static ActivitySpatialRelationMakerUnitTest create() {
		return new ActivitySpatialRelationMakerUnitTest();
	}

	@Test
	public void testCreateRelation() {
		Ratio sourceOverlap = MeasurementUtil.RATIO_ONE;
		Ratio targetOverlap = MeasurementUtil.createRatio(2d / 3d);

		ActivitySpatialRelationMaker matchMaker = new ActivitySpatialRelationMaker.Builder().courseLine(lineString1)
				.course(activity1).attemptLine(lineString2).attempt(activity2).courseSpatialBuffer(buffer1)
				.attemptSpatialBuffer(buffer2).build();
		ActivitySpatialRelation sourceRelation = matchMaker.relation();
		ActivitySpatialRelation targetRelation = matchMaker.inverseRelation();

		assertRelation(sourceRelation, NOT_IDENTICAL, sourceOverlap);
		assertRelation(targetRelation, NOT_IDENTICAL, targetOverlap);
	}

	/**
	 * Takes any gpx files dropped in the resource folder and runs all combinations between them,
	 * then spits out the result to the console. Does not do any assertions, this test is provided
	 * for manual testing and tinkering.
	 * 
	 * Please don't check in the gpx files as it slows down the unit testing and doesn't provide any
	 * additional value over what can be done shorter/quicker tracks.
	 * 
	 * @throws JAXBReadException
	 */
	@Test
	public void testManual() throws GpxReadException, IOException, GpxReadException {
		final String GPX_DOC_LOCATION = "**/*.gpx";

		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		Resource[] resources = resolver.getResources(GPX_DOC_LOCATION);

		for (int i = 0; i < resources.length; i++) {
			for (int j = (i + 1); j < resources.length; j++) {

				String sourceFileName = resources[i].getFilename();
				String targetFileName = resources[j].getFilename();

				testAndPrintTracks(sourceFileName, targetFileName);
			}
		}
	}

	private void testAndPrintTracks(String sourceFileName, String targetFileName) throws GpxReadException {

		GpxTestDocument source = new GpxTestDocument(sourceFileName, 1, 1, 2);
		GpxTestDocument target = new GpxTestDocument(targetFileName, 1, 1, 2);

		GpxTrack sourceTrack = new GpxTrack(GpxTestUtilFactory.getUtil().getTestGpx(source));
		GpxTrack targetTrack = new GpxTrack(GpxTestUtilFactory.getUtil().getTestGpx(target));

		ActivitySpatialRelationMaker.Builder builder = new ActivitySpatialRelationMaker.Builder();
		ActivitySpatialRelationMaker matchMaker = builder
				.courseLine(JtsTrackGeomUtil.trackToMultiLineString(sourceTrack))
				.attemptLine(JtsTrackGeomUtil.trackToMultiLineString(targetTrack)).course(activity1).attempt(activity2)
				.build();

		ActivitySpatialRelation sourceRelation = matchMaker.relation();
		ActivitySpatialRelation targetRelation = matchMaker.inverseRelation();

		assertRelation(sourceRelation, false, MeasurementUtil.createRatio(0.86));
		assertRelation(targetRelation, false, MeasurementUtil.createRatio(0.86));

		// printRelation(sourceRelation, sourceFileName, targetFileName);
		// printRelation(targetRelation, targetFileName, sourceFileName);
	}

}
