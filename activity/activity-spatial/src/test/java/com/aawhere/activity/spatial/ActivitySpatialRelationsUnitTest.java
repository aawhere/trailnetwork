/**
 * 
 */
package com.aawhere.activity.spatial;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.persist.BaseEntitiesBaseUnitTest;
import com.aawhere.personalize.xml.XmlAdapterTestUtils;

/**
 * @author aroller
 * 
 */
public class ActivitySpatialRelationsUnitTest
		extends
		BaseEntitiesBaseUnitTest<ActivitySpatialRelation, ActivitySpatialRelations, ActivitySpatialRelations.Builder> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#createEntitySampleWithId()
	 */
	@Override
	public ActivitySpatialRelation createEntitySampleWithId() {

		return ActivitySpatialRelationUnitTest.getInstance().getEntitySampleWithId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {

		return ActivitySpatialRelationTestUtil.adapters();
	}

}
