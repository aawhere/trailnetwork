package com.aawhere.activity.spatial;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.id.IdentifierToIdentifiableMapFunction;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.Track;
import com.aawhere.util.rb.StatusMessages;

import com.google.common.base.Functions;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Tests the function of the story, but not the spatial relations which is left up to those
 * utilities that provide the details of the spatial processing results.
 * 
 * 
 */
public class ActivitySpatialRelationUserStoryUnitTest {

	private ActivitySpatialRelationUserStory story;
	private Activity line;
	private Activity longer;

	@Before
	public void setUp() {

		HashMap<ActivityId, Track> tracks = new HashMap<>();
		line = createActivity(tracks, ExampleTracks.LINE);
		longer = createActivity(tracks, ExampleTracks.LINE_LONGER);
		IdentifierToIdentifiableMapFunction<ActivityId, Activity> activityProvider;
		activityProvider = new IdentifierToIdentifiableMapFunction.Builder<ActivityId, Activity>().add(line)
				.add(longer).build();
		Collection<Activity> activities = activityProvider.all().values();
		HashMap<ActivityId, Geometry> bufferProfider = new HashMap<>();
		HashMap<ActivityId, MultiLineString> lineProvider = new HashMap<>();
		for (Activity activity : activities) {

			// FIXME:This could be useful in to others...perhaps even in main code
			Pair<? extends Geometry, MultiLineString> geometries = JtsTrackGeomUtil
					.buffer(tracks.get(activity.getId()), ActivitySpatialRelationUserStory.DEFAULT_BUFFER_DISTANCE);
			bufferProfider.put(activity.getId(), geometries.getLeft());
			lineProvider.put(activity.id(), geometries.getRight());
		}
		story = ActivitySpatialRelationUserStory.create().activityProvider(activityProvider)
				.bufferProvider(Functions.forMap(bufferProfider)).lineProvider(Functions.forMap(lineProvider)).build();

	}

	/**
	 * @param tracks
	 * @param track
	 */
	private static Activity createActivity(HashMap<ActivityId, Track> tracks, final Track track) {
		Activity activity = ActivityTestUtil.createActivity(track);
		tracks.put(activity.getId(), track);
		return activity;
	}

	@Test
	public void testIdentical() {
		Pair<ActivitySpatialRelation, ActivitySpatialRelation> relations = story.relate(line.id(), line.id());
		ActivitySpatialRelationTestUtil.assertRelation(relations.getLeft(), true, MeasurementUtil.RATIO_ONE);
		ActivitySpatialRelationTestUtil.assertRelation(relations.getRight(), true, MeasurementUtil.RATIO_ONE);
	}

	@Test
	public void testNotIdentical() {
		Pair<ActivitySpatialRelation, ActivitySpatialRelation> relations = story.relate(line.getId(), longer.getId());
		StatusMessages messages = story.messages();
		assertFalse(messages.toString(), messages.hasError());
		assertNotNull("relations not provided", relations);
		ActivitySpatialRelationTestUtil.assertRelation(relations.getLeft(), false, MeasurementUtil.RATIO_ONE);
		ActivitySpatialRelationTestUtil.assertRelation(	relations.getRight(),
														false,
														QuantityMath.create(ExampleTracks.LINE_LENGTH)
																.over(ExampleTracks.LINE_LONGER_LENGTH));
	}
}
