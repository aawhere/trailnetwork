/**
 *
 */
package com.aawhere.di;

import com.aawhere.activity.ActivityApplicationXmlAdapter;
import com.aawhere.activity.ActivityObjectifyRepository;
import com.aawhere.activity.ActivityProvider;
import com.aawhere.activity.ActivityRepository;
import com.aawhere.activity.ActivityService;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

/**
 * @author brian
 * 
 */
public class ActivityModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(ActivityRepository.class).to(ActivityObjectifyRepository.class);
		bind(ActivityProvider.class).to(ActivityService.class);
	}

	@Provides
	protected ActivityApplicationXmlAdapter applicationXmlAdapter() {
		return new ActivityApplicationXmlAdapter(true);
	}
}
