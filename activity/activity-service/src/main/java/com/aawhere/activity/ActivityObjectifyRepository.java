/**
 *
 */
package com.aawhere.activity;

import com.aawhere.app.IdentityTranslators;
import com.aawhere.app.Installation;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.ObjectifyFilterQueryWriter;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.persist.ObjectifyFilterRepositoryDelegate;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.aawhere.track.TrackTranslators;
import com.google.appengine.api.datastore.Query;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * The GAE way to persist {@link Activity} using {@link ObjectifyRepository}.
 * 
 * @author Aaron Roller
 * 
 */
@Singleton
public class ActivityObjectifyRepository
		extends ObjectifyFilterRepository<Activity, ActivityId, Activities, Activities.Builder>
		implements ActivityRepository, CompleteRepository<Activities, Activity, ActivityId> {

	static {
		// required before registry for TrackSummary#Trackpoint
		TrackTranslators.add(ObjectifyService.factory());
		IdentityTranslators.add(ObjectifyService.factory());
		ObjectifyService.register(Activity.class);
	}

	@Inject
	public ActivityObjectifyRepository(FieldDictionaryFactory dictionaryFactory) {
		this(dictionaryFactory, SYNCHRONIZATION_DEFAULT);
	}

	ActivityObjectifyRepository(FieldDictionaryFactory dictionaryFactory,
			ObjectifyRepository.Synchronization synchronous) {
		super(new Delegate(dictionaryFactory), synchronous);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityRepository#appendToHistory(com.aawhere.app .Installation)
	 */
	@Override
	public Activity appendToHistory(ActivityId id, Installation installation) throws EntityNotFoundException {
		return appendToHistory(load(id), installation);

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityRepository#getDictionary()
	 */
	@Override
	public FieldDictionary getDictionary() {
		return getFieldDictionaryFactory().getDictionary(Activity.FIELD.DOMAIN);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityRepository#appendToHistory(com.aawhere.app .Installation)
	 */
	@Override
	public Activity appendToHistory(Activity activity, Installation installation) {
		return update(new Activity.Builder(activity).appendToHistory(installation).build());
	}

	static class Delegate
			extends ObjectifyFilterRepositoryDelegate<Activities, Activities.Builder, Activity, ActivityId> {

		/**
		 * @param fieldDictionaryFactory
		 */
		public Delegate(FieldDictionaryFactory fieldDictionaryFactory) {
			super(fieldDictionaryFactory);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityRepository#gone(com.aawhere.activity.ActivityId)
	 */
	@Override
	public Activity gone(ActivityId activityId) throws EntityNotFoundException {
		return this.update(Activity.mutate(load(activityId)).setGone(true).build());
	}

	/**
	 * For those systems requiring a raw datastore query this will provide the query using the
	 * standard processing of the {@link ActivityFilterBuilder} with the
	 * {@link ObjectifyFilterQueryWriter} so the query is prepared the same way it would be through
	 * standard processing.
	 * 
	 * @param activityFilter
	 * @return
	 */
	public Query datastoreQuery(Filter activityFilter) {
		// the filter builder provides any standard processing for activity filters
		Filter cloned = ActivityFilterBuilder.clone(activityFilter, getDictionary()).build();
		return writer(cloned).getRawQuery();
	}

}
