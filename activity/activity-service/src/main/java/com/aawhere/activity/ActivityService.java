/**
 *
 */
package com.aawhere.activity;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.Nullable;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.Installation;
import com.aawhere.app.InstanceKey;
import com.aawhere.app.KnownInstallation;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountService;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.geocell.GeoCellFilterBuilder;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonService;
import com.aawhere.search.SearchService;
import com.aawhere.track.Track;
import com.aawhere.track.TrackIncapableDocumentException;
import com.aawhere.track.TrackInfo;
import com.aawhere.track.TrackService;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackSummaryFilterBuilder;
import com.aawhere.track.Trackpoint;
import com.google.common.base.Function;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;
import com.google.inject.Inject;

/**
 * Provides access to {@link Activity} and its persisted data.
 * 
 * @author roller
 * 
 */
public class ActivityService
		extends EntityService<Activities, Activity, ActivityId>
		implements ActivityProvider {

	private static Logger LOG = LoggerFactory.getLogger(ActivityService.class);
	private ActivityObjectifyRepository activityRepository;
	private TrackService trackService;
	private SearchService searchService;
	private AccountService accountService;
	private PersonService personService;

	@Inject
	public ActivityService(ActivityRepository repository, TrackService trackService, PersonService personService,
			SearchService searchService) {
		super(repository);
		this.activityRepository = (ActivityObjectifyRepository) repository;
		this.trackService = trackService;
		this.searchService = searchService;
		this.accountService = personService.accountService();
		this.personService = personService;
	}

	/**
	 * Retrieve an {@link Activity} from the {@link ActivityRepository}
	 * 
	 * @param activityId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Override
	public Activity getActivity(ActivityId activityId) throws EntityNotFoundException {

		// throws UnknownApplicationException
		activityId = ActivityIdentifierUtil.validateFormat(activityId);
		final Activity loaded = activityRepository.load(activityId);
		return loaded;
	}

	@Override
	public Boolean exists(ActivityId id) {
		Filter filter = getActivityFilterBuilder().activityId(id).build();
		Iterable<ActivityId> ids = this.activityRepository.idPaginator(filter);
		return ids.iterator().hasNext();
	}

	public Integer activitiesTouched(Filter filter) {
		return this.activityRepository.touched(filter);
	}

	/**
	 * The most efficient way to access a bunch of activities by id. The resulting
	 * {@link Activities#getStatusMessages()} will report any ids not found.
	 * 
	 * @param ids
	 * @return
	 */
	@Override
	public Activities getActivities(Iterable<ActivityId> ids) {
		return this.activityRepository.entities(ids);
	}

	/**
	 * A helper method to filter those activities related to the activity and any other conditions
	 * set forth by the given filter.
	 * 
	 * @throws EntityNotFoundException
	 */
	public Activities activities(AccountId accountId, Filter filter) throws EntityNotFoundException {
		// this serves as a validation that the account exists, but also finds by alias if that is
		// all that is provided.
		accountId = accountService.account(accountId).id();
		return getActivities(getActivityFilterBuilder(filter).accountId(accountId).orderByMostRecentStartTime().build());
	}

	/**
	 * Provides all activities for a given person in order of most recent first.
	 * 
	 * @see #activities(AccountId, Filter)
	 * @param personId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Activities activities(PersonId personId, Filter filter) throws EntityNotFoundException {
		Person person = this.personService.person(personId);
		return getActivities(getActivityFilterBuilder(filter).accountId(person.accountIds())
				.orderByMostRecentStartTime().build());
	}

	/**
	 * Guarantees uniqueness, then calls {@link #getActivities(Iterable)}.
	 * 
	 * @param ids
	 * @return
	 */
	public Activities getActivities(Collection<ActivityId> ids) {
		return getActivities((Iterable<ActivityId>) SetUtilsExtended.asSet(ids));
	}

	/**
	 * Provided the ActivityId this will return the contents of the document in which it originated
	 * in it's encoded format.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	public byte[] getDocumentContents(ActivityId id) throws EntityNotFoundException {
		Activity activity = getActivity(id);
		return this.trackService.getDocumentService().getEncodedData(activity.getTrackSummary().getDocumentId());
	}

	/**
	 * Retrieve Activities via {@link ActivitiesFilter}.
	 * 
	 * @param filter
	 * @return
	 */
	public Activities getActivities(Filter filter) {
		Activities activities = activityRepository.filter(processFilter(filter));
		if (filter.getOptions().contains("TN-579")) {
			activities = Activities.mutate(activities).transformer(tn579UntitledActivitiesFunction()).build();
		}
		return activities;
	}

	public Iterable<Activity> getActivitiesAsIterable(Filter filter) {
		return activityRepository.filterAsIterable(processFilter(filter));
	}

	/**
	 * Given {@link GeoCells} this will return those activities
	 * {@link TrackSummaryFilterBuilder#intersects(com.aawhere.track.TrackSummary)} the cells given.
	 * A synonym for {@link #getActivities(Filter)} using
	 * {@link GeoCellFilterBuilder#intersection(GeoCells)}.
	 * 
	 * Filter is provided because a paginator must be used if you wanted all the results.
	 * 
	 * @param geoCells
	 * @return
	 */
	public Activities getActivities(GeoCells geoCells, Filter filter) {
		Filter intersectionFilter = getActivityFilterBuilder(filter).spatialBuffer().intersection(geoCells).build();
		return getActivities(intersectionFilter);
	}

	/**
	 * When only the {@link ActivityId} is needed as the result of any {@link Filter}. This will
	 * return an iterator that will make subsequent calls to the database and should be used for
	 * extraordinarily large results.
	 * 
	 * @param filter
	 * @return
	 */
	public Iterable<ActivityId> getActivityIds(Filter filter) {
		return activityRepository.idPaginator(filter);
	}

	private String getDomain() {
		return AnnotatedDictionaryUtils.domainFrom(Activity.class);
	}

	/**
	 * Given the Activity, this will return a {@link Track} if it is capable.
	 * 
	 * @param activityId
	 * @return
	 * @throws EntityNotFoundException
	 *             if the activityId is not found in the system.
	 * @throws TrackIncapableDocumentException
	 *             if the activity does not allow a track.
	 */
	public Track getTrack(ActivityId activityId) throws EntityNotFoundException, TrackIncapableDocumentException {
		Activity activity = getActivity(activityId);
		return trackService.getTrack(activity.getTrackSummary().getDocumentId());
	}

	public Function<ActivityId, Iterable<Trackpoint>> trackProvider() {
		return new Function<ActivityId, Iterable<Trackpoint>>() {

			@Override
			@Nullable
			public Track apply(@Nullable ActivityId input) {
				try {
					return getTrack(input);
				} catch (EntityNotFoundException e) {
					throw ToRuntimeException.wrapAndThrow(e);
				}
			}
		};
	}

	/**
	 * provides the {@link TrackSummary} and {@link Track} when given the corresponding
	 * {@link ActivityId}.
	 */
	public Function<ActivityId, TrackInfo> trackInfoFunction() {
		return new Function<ActivityId, TrackInfo>() {

			@Override
			public TrackInfo apply(ActivityId input) {
				try {
					final Activity activity = activity(input);
					return TrackInfo.build(activity.getTrackSummary(), getTrack(activity));
				} catch (TrackIncapableDocumentException | EntityNotFoundException e) {
					LOG.warning(input + " not able to be produced " + e.getMessage());
					return null;
				}
			}
		};
	}

	/**
	 * Provides all the {@link Activity}s for a particular {@link Application} installation that is
	 * identified by a {@link InstanceKey}.
	 * 
	 * 
	 * @param applicationKey
	 * @param instanceKey
	 * @return
	 */
	public Set<Activity> getActivitiesForDevice(ApplicationKey applicationKey, InstanceKey instanceKey) {
		throw new NotImplementedException();
	}

	/**
	 * Store an activity in the repository.This will also update and activity so it is dangerous to
	 * open publicly since the activity passed in does not represent what is in the database.
	 * 
	 * @param activity
	 * @deprecated use create
	 */
	@Deprecated
	public void store(Activity activity) {
		create(activity);
	}

	/**
	 * Creates the activity in the datastore returning it with the database filled items such as ID,
	 * create date, etc.
	 * 
	 * Additionally the identity history appended with the visit to this Application.
	 * 
	 * @param activity
	 */
	/* package to isolate to importers */Activity create(Activity activity) {
		activityRepository.store(activity);

		appendToHistory(activity, KnownInstallation.thisSystem(activity.getId()));
		return activity;
	}

	public void appendToHistory(ActivityId id, Installation installation) throws EntityNotFoundException {
		activityRepository.appendToHistory(id, installation);
	}

	public void appendToHistory(Activity activity, Installation installation) {
		activityRepository.appendToHistory(activity, installation);
	}

	private Filter processFilter(Filter filter) {
		return filter;
	}

	/**
	 * @return the trackService
	 */
	public TrackService getTrackService() {
		return this.trackService;
	}

	/**
	 * Just a convenience to get an Archived document by the ActivityId.
	 * 
	 * @param activityId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ArchivedDocument getArchivedDocument(ActivityId activityId) throws EntityNotFoundException {
		Activity activity = getActivity(activityId);
		return this.trackService.getDocumentService().getArchivedDocument(activity.getTrackSummary().getDocumentId());
	}

	/**
	 * Given a date this will return true if the activity has been modified more recent than the
	 * given date.
	 * 
	 * @param activityId
	 * @param lastModified
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Boolean modifiedSince(ActivityId activityId, DateTime lastModified) throws EntityNotFoundException {
		return ActivityUtil.modifiedSince(getActivity(activityId), lastModified);
	}

	/**
	 * Provides a paginator of ids using the of the results based on the filter.
	 * 
	 * There is no limit to the number of ids produced so be mindful of what you do with the results
	 * 
	 * @param filter
	 */
	public Iterable<ActivityId> idPaginator(Filter filter) {
		return activityRepository.idPaginator(filter);
	}

	public FieldDictionary getDictionary() {
		return this.activityRepository.getDictionary();
	}

	public ActivityFilterBuilder getActivityFilterBuilder() {
		return ActivityFilterBuilder.create(getDictionary());
	}

	ActivityFilterBuilder getActivityFilterBuilder(Filter filter) {
		return ActivityFilterBuilder.clone(filter, getDictionary());
	}

	/**
	 * @param activity
	 * @return
	 * @throws EntityNotFoundException
	 * @throws TrackIncapableDocumentException
	 */
	public Track getTrack(Activity activity) throws TrackIncapableDocumentException, EntityNotFoundException {
		return trackService.getTrack(activity.getTrackSummary().getDocumentId());
	}

	/**
	 * Iterates each page of the filter until no more pages. This is connected to the repository so
	 * subsequent calls will be made.
	 * 
	 * @param filter
	 * @return
	 */
	public Iterable<Activity> activityPaginator(Filter filter) {
		return activityRepository.entityPaginator(filter);
	}

	/**
	 * Activities with low resolution introduced geocells without parents erroneously. This repairs
	 * those items by adding parents. TN-608
	 * 
	 * @param filter
	 * @return
	 */
	public Map<Resolution, Integer> repairLowResolutionGeoCellsMissingParentsTn608(Filter filter) {
		HashMap<Resolution, Integer> updatedCounts = new HashMap<>();
		if (filter.getOptions().contains("TN-608")) {
			Resolution[] resolutions = ArrayUtils.subarray(	Resolution.values(),
															Resolution.FIVE.ordinal(),
															Resolution.EIGHT.ordinal());
			for (int i = 0; i < resolutions.length; i++) {
				Integer numberUpdated = 0;
				Resolution resolution = resolutions[i];

				Filter resolutionFilter = getActivityFilterBuilder(filter).spatialBuffer().equalsResolution(resolution)
						.build();
				Iterable<Activity> activityPaginator = activityPaginator(resolutionFilter);
				for (Activity activity : activityPaginator) {
					GeoCells withoutParents = activity.getTrackSummary().getSpatialBuffer();
					GeoCells withParents = GeoCellUtil.withAncestors(withoutParents);
					// mutating by reference. activity will update the changed summary
					TrackSummary.mutate(activity.getTrackSummary()).setSpatialBuffer(withParents).build();
					activityRepository.store(activity);
					numberUpdated++;
				}
				updatedCounts.put(resolution, numberUpdated);
			}
		}
		return updatedCounts;
	}

	/**
	 * @param activityIds
	 * @return
	 */
	public BiMap<ActivityId, ArchivedDocumentId> trackArchivedDocumentIds(Iterable<ActivityId> activityIds) {
		// we can't do a projection query because archived document is not indexed
		// FIXME: move loadAll for iterable to the repository in general
		ActivityObjectifyRepository aor = this.activityRepository;
		Map<ActivityId, Activity> activitiesMap = aor.loadAll(activityIds);
		return HashBiMap.create(Maps.transformValues(activitiesMap, ActivityUtil.archivedDocumentIdFunction()));
	}

	public Function<Activity, Activity> tn579UntitledActivitiesFunction() {
		return new Function<Activity, Activity>() {

			@Override
			public Activity apply(Activity input) {

				if (input.getName() != null && "Untitled".equals(input.getName().getValue())) {
					input = Activity.mutate(input).setName(null).build();
					activityRepository.store(input);
				}
				return input;
			}
		};
	}

	/**
	 * Marks {@link Activity#isGone()} to true. If the activity is not found in our system...well,
	 * then what do you care?
	 * 
	 * @param activityId
	 * @return the updated activity or null if the activity is not found.
	 * @throws EntityNotFoundException
	 */
	public Activity activityIsGone(ActivityId activityId) {
		try {
			return this.activityRepository.gone(activityId);
		} catch (EntityNotFoundException e) {
			LoggerFactory.getLogger(getClass()).warning(activityId
					+ " is marked as gone, but doesn't exist in our system anyway");
			return null;
		}
	}

	/**
	 * @see #getActivity(ActivityId)
	 * 
	 * @param course
	 * @throws EntityNotFoundException
	 */
	public Activity activity(ActivityId activityId) throws EntityNotFoundException {
		return getActivity(activityId);
	}

	/**
	 * Provides an efficient way to retrieve known activities by their id.
	 * 
	 * @see ObjectifyFilterRepository#entities(Iterable)
	 * @param activityIds
	 * @return
	 */
	public Iterable<Activity> activities(Iterable<ActivityId> activityIds) {
		// I hope the collection returned is "connected" to avoid a potentially huge collection here
		return this.activityRepository.loadAll(activityIds).values();
	}

	public Function<ActivityId, Person> personFunction() {
		return new Function<ActivityId, Person>() {

			@Override
			public Person apply(ActivityId input) {
				try {
					return (input == null) ? null : personService.person(activity(input).getAccountId());
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/**
	 * @param courseId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public TrackInfo trackInfo(ActivityId courseId) throws EntityNotFoundException {
		Activity activity = activity(courseId);
		final TrackSummary summary = activity.getTrackSummary();
		Track track = trackService.getTrack(summary.getDocumentId());
		return TrackInfo.build(summary, track);
	}

	/**
	 * given an activity this provides the track.
	 * 
	 * @see #trackProvider()
	 * 
	 * @return
	 */
	public Function<Activity, Iterable<Trackpoint>> trackFunction() {
		return new Function<Activity, Iterable<Trackpoint>>() {
			@Override
			public Iterable<Trackpoint> apply(Activity input) {
				try {
					return (input != null) ? trackService.getTrack(input.getTrackSummary().getDocumentId()) : null;
				} catch (TrackIncapableDocumentException | EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

}
