package com.aawhere.activity;

import com.aawhere.field.FieldKey;
import com.aawhere.person.PersonId;
import com.aawhere.search.SearchDocument;
import com.aawhere.search.SearchDocument.Field;
import com.aawhere.search.SearchUtils;
import com.google.common.collect.Multimap;

public class ActivitySearchUtil {

	/**
	 * Given the search results this will translate the documents into it's
	 * {@link RelativeStatistic}.
	 * 
	 * @param searchDocument
	 * @return
	 */
	public static RelativeStatistic relativeStats(SearchDocument searchDocument, FieldKey personIdKey,
			FieldKey activityTypeKey, FieldKey distanceCategoryKey, FieldKey countKey) {
		RelativeStatistic.Builder builder = RelativeStatistic.create();
		Multimap<String, Field> fieldMap = SearchUtils.fieldMap(searchDocument.fields());
		builder.id(searchDocument.id());
		builder.personId(SearchUtils.fieldValue(personIdKey.getAbsoluteKey(), PersonId.class, fieldMap));
		builder.activityType(SearchUtils.fieldValue(activityTypeKey.getAbsoluteKey(), ActivityType.class, fieldMap));
		builder.distance(SearchUtils.fieldValue(distanceCategoryKey.getAbsoluteKey(),
												ActivityTypeDistanceCategory.class,
												fieldMap));
		builder.count(SearchUtils.fieldValue(countKey.getAbsoluteKey(), Integer.class, fieldMap));
		return builder.build();
	}
}
