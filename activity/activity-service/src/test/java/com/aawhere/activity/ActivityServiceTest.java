/**
 *
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.Installation;
import com.aawhere.app.InstallationUnitTest;
import com.aawhere.app.KnownApplication;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.DatastoreTestUtil;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityServiceBaseTest;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ServiceTest;
import com.aawhere.track.Track;
import com.aawhere.track.TrackIncapableDocumentException;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.BiMap;
import com.google.common.collect.Sets;

/**
 * @see ActivityService
 * @author Brian Chapman
 * 
 */
@Category(ServiceTest.class)
public class ActivityServiceTest
		extends EntityServiceBaseTest<ActivityService, Activities, Activity, ActivityId> {

	// @Rule
	// public final Timeout timeout = new Timeout(10000);

	private ActivityServiceTestUtil activityTestUtil;
	private ActivityService activityService;
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Override
	@Before
	public void setUpServices() {
		helper.setUp();
		activityTestUtil = ActivityServiceTestUtil.create().build();
		activityService = activityTestUtil.getActivityService();
		GpxDocumentProducer.register();
	}

	@Override
	@After
	public void tearDownServices() {
		helper.tearDown();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link com.aawhere.activity.ActivityService#getActivity(com.aawhere.activity.ActivityId)} .
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testGetActivityActivityId() throws EntityNotFoundException {
		Activity activity = activityTestUtil.createPersistedActivity();
		Activity result = activityService.getActivity(activity.getId());
		assertEquals(activity, result);

		result = activityService.getActivity(activity.getId());
		assertEquals("same id, but hidden by interface", activity, result);

	}

	@Test
	public void testGetActivitiesByAccountId() throws EntityNotFoundException {
		Activity activity = activityTestUtil.createPersistedActivity();
		this.activityTestUtil.getTrackTestUtil().getDocumentTestUtil().getPersonServiceTestUtil()
				.createPersistedAccount(activity.getAccountId());
		// creates an activity to prove all is not being retrieved
		activityTestUtil.createPersistedActivity();
		// account isn't created. Must use import service to test full function
		Activities activities = activityService.activities(activity.getAccountId(), Filter.create().build());
		TestUtil.assertContainsOnly(activity, activities);
	}

	/**
	 * Tests only that the pieces are put together. spatial integrity is left up to those that
	 * specialize in spatial queries.
	 * 
	 * @see ActivityService#getActivities(com.aawhere.measure.geocell.GeoCells, Filter)
	 */
	@Test
	public void testCellsMethod() {
		Filter filter = activityService.getActivityFilterBuilder().build();
		final int expectedSize = 1;
		assertCellsMethod(filter, expectedSize);

	}

	/**
	 * Simple test to ensure the filter is being respected by asking for the second page of a 1 page
	 * result set.
	 */
	@Test
	public void testCellsMethodFilterBeyondResults() {
		Filter filter = activityService.getActivityFilterBuilder().setPage(2).build();
		final int expectedSize = 0;
		assertCellsMethod(filter, expectedSize);

	}

	@Test
	public void testGone() throws EntityNotFoundException {
		Activity activity = this.activityTestUtil.createPersistedActivity();
		assertFalse("newly created of course are not gone", activity.isGone());
		Activity updated = activityService.activityIsGone(activity.id());
		assertTrue("update didn't even make the pass back", updated.isGone());
		assertTrue("finder found activity not gone", activityService.getActivity(activity.id()).isGone());
	}

	/**
	 * @param filter
	 * @param expectedSize
	 */
	private void assertCellsMethod(Filter filter, final int expectedSize) {
		Activity activity = activityTestUtil.createPersistedActivity();
		GeoCells spatialBuffer = activity.getTrackSummary().getSpatialBuffer();
		Activities activities = activityService.getActivities(spatialBuffer, filter);
		final Collection<Activity> all = activities.all();
		TestUtil.assertSize(expectedSize, all);
		if (all.size() > 0) {
			TestUtil.assertContains(activity, all);
		}
	}

	/**
	 * Test method for
	 * {@link com.aawhere.activity.ActivityService#getActivity(com.aawhere.activity.ActivityId)} .
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test(expected = EntityNotFoundException.class)
	public void testGetActivityActivityIdException() throws EntityNotFoundException {
		ActivityId activityId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, ActivityIdFixture.GC_ACTIVITY_ID);
		activityService.getActivity(activityId);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.activity.ActivityService#getTrack(com.aawhere.activity.ActivityId)} .
	 * 
	 * @throws TrackIncapableDocumentException
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testGetTrack() throws EntityNotFoundException, TrackIncapableDocumentException {
		Activity activity = activityTestUtil.createPersistedActivity();
		Track track = activityService.getTrack(activity.getId());
		assertNotNull(track);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.activity.ActivityService#getActivities(com.aawhere.activity.ActivitiesFilter)}
	 * .
	 */
	@Test
	public void testGetActivities() {
		Activity activity = activityTestUtil.createPersistedActivity();
		Filter filter = getActivityFilterBuilder().build();
		Activities result = activityService.getActivities(filter);
		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(activity, result.iterator().next());
	}

	@Test
	public void testExists() {
		assertFalse("activity not yet created shouldn't exist",
					activityService.exists(ActivityTestUtil.createRandomActivityId()));
		Activity activity = activityTestUtil.createPersistedActivity();
		assertTrue("activity created should exist", activityService.exists(activity.getId()));
	}

	/**
	 * @return
	 */
	private ActivityFilterBuilder getActivityFilterBuilder() {
		return new ActivityFilterBuilder(activityService.getDictionary());
	}

	/**
	 * Test method for
	 * {@link com.aawhere.activity.ActivityService#getActivities(com.aawhere.activity.ActivitiesFilter)}
	 * .
	 */
	@Test
	public void testGetActivitiesWithTrackSummaryFilter() {
		activityTestUtil.createPersistedActivity();
		activityTestUtil.createPersistedActivity();
		Filter filter = getActivityFilterBuilder().setLimit(1).build();
		Activities result = activityService.getActivities(filter);
		assertNotNull(result);
		assertEquals(1, result.size());
	}

	/**
	 * Test method for
	 * {@link com.aawhere.activity.ActivityService#getActivity(com.aawhere.app.ApplicationKey, com.aawhere.activity.ApplicationActivityId)}
	 * .
	 * 
	 * @throws EntityNotFoundException
	 * @throws com.google.appengine.api.datastore.EntityNotFoundException
	 */
	@Test
	public void testGetActivityApplicationKeyApplicationActivityId() throws EntityNotFoundException {
		Activity activity = activityTestUtil.createPersistedActivity();
		Installation installation = activity.getHistory().getInstallations().iterator().next();
		String externalId = installation.getExternalId();
		ApplicationKey applicationKey = installation.getApplication().getKey();
		assertEquals(activity.getId().getApplicationKey(), applicationKey);
		ActivityId applicationActivityId = new ActivityId(applicationKey, externalId);
		Activity result = activityService.getActivity(applicationActivityId);
		assertEquals(activity, result);

		result = activityService.getActivity(applicationActivityId);
		assertEquals("same id, hidden by interface should return the same", activity, result);

	}

	/**
	 * Test method for
	 * {@link com.aawhere.activity.ActivityService#store(com.aawhere.activity.Activity)} .
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testStore() throws EntityNotFoundException {
		Activity activity = ActivityUnitTest.getInstance().getEntitySample();
		activityService.create(activity);
		DatastoreTestUtil.assertPersisted(activity);
		// TN-295 - both must be stored temporarily during the transition
		Activity loaded = activityTestUtil.getRepository().load(activity.getId());
		assertNotNull("TN-295 transition not working", loaded.getTrackSummary());
	}

	/**
	 * Test method for
	 * {@link com.aawhere.activity.ActivityService#appendToHistory(com.aawhere.activity.ActivityId, com.aawhere.app.Installation)}
	 * .
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	@Ignore("TN-344 killed history")
	public void testAppendToHistory() throws EntityNotFoundException {
		Activity activity = activityTestUtil.createPersistedActivity();
		Installation installation = InstallationUnitTest.getInstance().getInstallation();
		int installationSize = activity.getHistory().getInstallations().size();
		activityService.appendToHistory(activity.getId(), installation);
		Activity result = activityService.getActivity(activity.getId());
		assertEquals(installationSize + 1, result.getHistory().getInstallations().size());
	}

	@Test
	public void testArchivedDocumentIds() {
		Activity first = activityTestUtil.createPersistedActivity();
		Activity second = activityTestUtil.createPersistedActivity();

		BiMap<ActivityId, ArchivedDocumentId> archivedDocumentIds = this.activityService
				.trackArchivedDocumentIds(IdentifierUtils.ids(Sets.newHashSet(first, second)));
		assertEquals("first", first.getTrackSummary().getDocumentId(), archivedDocumentIds.get(first.id()));
		assertEquals("second", second.getTrackSummary().getDocumentId(), archivedDocumentIds.get(second.id()));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#entity()
	 */
	@Override
	public Activity entity() {
		return ActivityTestUtil.createActivity();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#service()
	 */
	@Override
	public ActivityService service() {
		return this.activityService;
	}

}
