package com.aawhere.activity;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.person.Person;
import com.aawhere.person.PersonServiceTestUtil;
import com.aawhere.test.category.PersistenceTest;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 *
 */

/**
 * Tests the {@link ActivityObjectifyRepository}.
 * 
 * @author Aaron Roller
 * 
 */
@Category(PersistenceTest.class)
public class ActivityObjectifyRepositoryTest
		extends ActivityUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();

	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityUnitTest#createPerson()
	 */
	@Override
	protected Person createPerson() {
		return PersonServiceTestUtil.create().build().persistedPerson();
	}

	@Override
	public Activity getEntitySample() {
		Activity activity = super.getEntitySample();
		return activity;
	}

	public Activity getUniqueEntitySample() {
		Activity activity = ActivityUnitTest.getInstance().getEntitySample();
		return activity;
	}

	public static ActivityObjectifyRepositoryTest getInstance() {
		ActivityObjectifyRepositoryTest instance = new ActivityObjectifyRepositoryTest();
		try {
			instance.setUpObjectify();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		instance.baseSetUp();
		instance.setUp();
		return instance;
	}

	/*
	 * @see com.aawhere.activity.ActivityUnitTest#createRepository()
	 */
	@Override
	protected ActivityRepository createRepository() {
		ActivityServiceTestUtil activityTestUtil = ActivityServiceTestUtil.create().build();
		return activityTestUtil.getRepository();
	}

	@Override
	public void assertDeleted(ActivityId id) throws EntityNotFoundException {
		GaePersistTestUtils.syncCache();
		Activity supposedToBeDeleted = null;
		int attempts = 10;
		int sleepMillis = 100;
		long start = System.currentTimeMillis();
		String message = "";
		try {
			// delete is non blocking...let's loiter until it is gone.
			do {
				// make sure it is gone...this throws NotFound for the expected exit
				supposedToBeDeleted = getRepository().load(id);
				// purposely put this after since exception should skip this if not
				// needed.
				Thread.sleep(sleepMillis);
			} while (supposedToBeDeleted != null && attempts++ < 100);
			message = id.toString() + " didn't delete even after waiting " + (System.currentTimeMillis() - start);
			fail(message);
		} catch (EntityNotFoundException e) {
			// this is expected.
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(message, e);
		}
	}

	@Test
	public void testLoadsDeviceWithNullInstanceKey() {

	}

}
