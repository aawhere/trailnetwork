/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.track.ExampleTracks.*;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.story.TrackSummaryUserStoriesTestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * Tests the track summary storage filters for Track Summary part of Activity.
 * 
 * TODO:Abstract this into a re-usable test if others use Track Summary.
 * 
 * @author aroller
 * 
 */
public class ActivityTrackSummaryObjectifyFilterRepositoryTest {

	private ActivityServiceTestUtil testUtil;

	/**
	 *
	 */
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void baseSetUp() throws Exception {
		helper.setUp();
		this.testUtil = ActivityServiceTestUtil.create().build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void baseTearDown() throws Exception {
		helper.tearDown();
	}

	/**
	 * TODO:this is generic
	 */
	private Activity persist(Iterable<Trackpoint> track) {
		TrackSummary summary = TrackSummaryUserStoriesTestUtil.create(track);
		return persist(summary);
	}

	public Activity line() {
		return persist(LINE);
	}

	public Activity deviant() {
		return persist(DEVIATE_AROUND_LINE);
	}

	public Activity superLong() {
		return persist(LINE_SUPER_LONG);
	}

	/**
	 * this is implementation of abstract method.
	 * 
	 * @param summary
	 * @return
	 */
	private Activity persist(TrackSummary summary) {
		return testUtil.createPersistedActivity(summary);
	}

	/**
	 * All activities should contain themselves.
	 */
	@Test
	public void lineContainsItself() {
		Activity line = line();
		assertContains(line);
	}

	/**
	 * Although line overlaps, it doesn't completely contain super long so it won't qualify.
	 * 
	 */
	@Test
	public void lineDoesntContainSuperLong() {
		line();
		Activity lineSuperLong = superLong();
		assertContains(lineSuperLong);
	}

	/**
	 * Expected should be contained within the container (both already persisted).
	 * 
	 * @param containee
	 * @param container
	 */
	private void assertContains(Activity containee, Activity... expectedContainers) {
		Filter filter = getFilterBuilder().contains(containee.getTrackSummary()).build();
		assertFilter(filter, containee, expectedContainers);
	}

	/**
	 * @param filter
	 * @param target
	 * @param expected
	 */
	private void assertFilter(Filter filter, Activity target, Activity... expected) {
		Activities actualContainers = filter(filter);
		// expected always contains itself.
		expected = ArrayUtils.add(expected, target);
		final int expectedNumberOfContainers = expected.length;
		TestUtil.assertSize(expectedNumberOfContainers, actualContainers.getCollection());
		for (int i = 0; i < expectedNumberOfContainers; i++) {
			Activity container = expected[i];
			TestUtil.assertContains(container, actualContainers.getCollection());
		}
	}

	/**
	 * Super Long Line contains line and itself.
	 * 
	 */
	@Test
	public void superLongContainsLine() {
		final Activity line = line();
		assertContains(line, superLong());
	}

	@Ignore("TN-368")
	@Test
	public void lineLength() {
		line();
		Activity superLongLine = superLong();
		Filter greaterThanFilter = getFilterBuilder().distanceGreaterThan(LINE_LONGER_LENGTH).build();
		Activities results = filter(greaterThanFilter);
		TestUtil.assertSize(1, results.getCollection());
		TestUtil.assertContains(superLongLine, results.getCollection());

	}

	@Test
	public void lineIntersectsItself() {
		Activity target = line();
		assertIntersections(target);
	}

	/**
	 * @param target
	 */
	private void assertIntersections(Activity target, Activity... expectedIntersections) {
		Filter intersectsFilter = getFilterBuilder().intersects(target.getTrackSummary()).build();
		assertFilter(intersectsFilter, target, expectedIntersections);

	}

	@Test
	public void testlineDoesntIntersectDeviant() {
		deviant();
		assertIntersections(line());
	}

	@Test
	public void testDeviantDoesntIntersectLine() {
		line();
		assertIntersections(deviant());
	}

	@Test
	public void testDeviantIntersectionsSuperLong() {
		assertIntersections(deviant(), superLong());
	}

	/**
	 * @param filter
	 * @return
	 */
	private Activities filter(Filter filter) {
		return this.testUtil.getActivityService().getActivities(filter);
	}

	public ActivityFilterBuilder getFilterBuilder() {
		return testUtil.getActivityService().getActivityFilterBuilder();
	}

}
