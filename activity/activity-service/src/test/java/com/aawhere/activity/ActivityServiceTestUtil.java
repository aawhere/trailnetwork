/**
 *
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.DeviceTestUtil;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.search.GaeSearchTestUtils;
import com.aawhere.search.SearchService;
import com.aawhere.track.TrackService;
import com.aawhere.track.TrackServiceTestUtil;
import com.aawhere.track.TrackSummary;
import com.aawhere.util.rb.StringMessage;

/**
 * @author Brian Chapman
 * 
 */
public class ActivityServiceTestUtil {

	private TrackServiceTestUtil trackTestUtil;
	private ActivityService activityService;
	private ActivityObjectifyRepository activityRepository;
	private SearchService searchService;

	public ActivityService getActivityService() {
		return activityService;
	}

	public ActivityObjectifyRepository getRepository() {
		return activityRepository;
	}

	public ActivityObjectifyRepository getFilterRepository() {
		return getRepository();
	}

	/**
	 * @return the trackTestUtil
	 */
	public TrackServiceTestUtil getTrackTestUtil() {
		return this.trackTestUtil;
	}

	public SearchService getSearchService() {
		return searchService;
	}

	public Activity createPersistedActivity() {
		return createPersistedActivity(null, null);
	}

	public Activity createPersistedActivity(TrackSummary ts) {
		return createPersistedActivity(ts, null);
	}

	public Activity createPersistedActivity(String name) {
		Activity activity = createPersistedActivity();
		return getRepository().update(Activity.mutate(activity).setName(new StringMessage(name)).build());

	}

	public Activity createPersistedActivity(TrackSummary ts, ActivityId activityId) {
		if (ts == null) {
			ts = trackTestUtil.createPersistedTrackSummary();
		}
		ActivityUnitTest instance = ActivityUnitTest.getInstance();
		if (activityId != null) {
			instance.setActivityId(activityId);
		}
		Activity activity = instance.getEntitySample();
		getTrackTestUtil().getDocumentTestUtil().getPersonServiceTestUtil()
				.createPersistedAccount(activity.getAccountId());
		Activity activityMutated = Activity.mutate(activity).setTrackSummary(ts).build();
		activityService.create(activityMutated);
		assertNotNull(activityMutated.getId());
		assertNotNull(activityMutated.getTrackSummary());
		assertNotNull(activityMutated.getAccountId());
		return activity;
	}

	public Activity createPersistedActivity(ActivityId activityId) {
		Activity activity = createPersistedActivity(null, activityId);
		getTrackTestUtil().getDocumentTestUtil().getPersonServiceTestUtil()
				.createPersistedAccount(activity.getAccountId());
		return activity;
	}

	/**
	 * Used to construct all instances of ActivityServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<ActivityServiceTestUtil> {

		private com.aawhere.track.TrackServiceTestUtil.Builder trackTestUtilBuilder;

		public Builder() {
			super(new ActivityServiceTestUtil());
			this.trackTestUtilBuilder = new TrackServiceTestUtil.Builder();
		}

		/**
		 * @return the trackTestUtilBuilder
		 */
		public com.aawhere.track.TrackServiceTestUtil.Builder getTrackTestUtilBuilder() {
			return this.trackTestUtilBuilder;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ActivityServiceTestUtil build() {
			FieldAnnotationDictionaryFactory annotatedDictionaryFactory = trackTestUtilBuilder
					.getDocumentServiceTestUtilBuilder().getPersonServiceTestUtilBuilder()
					.getAnnotationDictionaryFactory();
			annotatedDictionaryFactory.getDictionary(Activity.class);

			building.searchService = GaeSearchTestUtils.createGaeSearchService();
			building.trackTestUtil = trackTestUtilBuilder.build();
			building.activityRepository = new ActivityObjectifyRepository(annotatedDictionaryFactory.getFactory());
			TrackService trackService = building.trackTestUtil.getTrackService();
			trackService = building.trackTestUtil.getTrackService();
			building.activityService = new ActivityService(building.activityRepository, trackService,
					building.trackTestUtil.getDocumentTestUtil().getPersonServiceTestUtil().getPersonService(),
					building.searchService);
			return super.build();
		}

	}// end Builder

	/** Use {@link Builder} to construct ActivityServiceTestUtil */
	private ActivityServiceTestUtil() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @param courseId
	 * @param bike
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Activity setActivityType(ActivityId activityId, ActivityType activityType) throws EntityNotFoundException {
		return this.activityRepository.update(Activity.mutate(this.activityRepository.load(activityId))
				.setActivityType(activityType).build());
	}

	/**
	 * Mutatest and persists the device identified by the key to the targeted activity.
	 * 
	 * @param activityId
	 * @param deviceKey
	 *            from {@link DeviceTestUtil} constants.
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Activity setDevice(ActivityId activityId, ApplicationKey deviceKey) throws EntityNotFoundException {
		return this.activityRepository.update(ActivityTestUtil.setDevice(	this.activityRepository.load(activityId),
																			deviceKey));
	}
}
