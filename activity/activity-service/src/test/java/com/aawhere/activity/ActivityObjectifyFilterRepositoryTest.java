/**
 *
 */
package com.aawhere.activity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.persist.BaseObjectifyFilterRepositoryTest;
import com.aawhere.persist.EmptyQueryException;
import com.aawhere.track.TrackService;
import com.aawhere.track.TrackServiceTestUtil;

/**
 * @author Brian Chapman
 * 
 */
public class ActivityObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<ActivityId, Activity, ActivityObjectifyRepository, ActivityRepository, Activities, Activities.Builder> {

	ActivityObjectifyRepository activityRepository;
	TrackService trackService;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ActivityServiceTestUtil activityTestUtil = ActivityServiceTestUtil.create().build();
		TrackServiceTestUtil trackTestUtil = activityTestUtil.getTrackTestUtil();
		activityRepository = activityTestUtil.getRepository();
		this.trackService = trackTestUtil.getTrackService();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#testFilter_Cursors()
	 */
	@Override
	@Test
	@Ignore("temporarily ignoring since Activity cursors are disabled since inherting from TrackSummary")
	public void testFilter_Cursors() {
		// TODO Auto-generated method stub
		super.testFilter_Cursors();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#testFilter_CursorsUsedInQuery()
	 */
	@Override
	@Test
	@Ignore("see #testFilter_Cursors ignore")
	public void testFilter_CursorsUsedInQuery() throws IllegalAccessException, EmptyQueryException {
		// TODO Auto-generated method stub
		super.testFilter_CursorsUsedInQuery();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected Activity createEntity() {
		Activity activity = ActivityTestUtil.createActivity();
		getRepository().store(activity);
		return activity;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected ActivityRepository getRepository() {
		return this.activityRepository;
	}

}
