/**
 * 
 */
package com.aawhere.activity.spatial;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityTrackGeometryService;
import com.aawhere.cache.Cache;
import com.aawhere.persist.EntityNotFoundException;

import com.google.common.base.Function;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Provides {@link ActivitySpatialRelation} and related persistence.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivitySpatialRelationService {

	private ActivityTrackGeometryService trackGeometryService;
	private ActivitySpatialRelationUserStory story;
	private ActivityImportService activityImportService;

	@Inject
	public ActivitySpatialRelationService(ActivityImportService activityImportService,
			ActivityTrackGeometryService trackGeometryService) {
		this.activityImportService = activityImportService;
		this.trackGeometryService = trackGeometryService;
		this.story = new ActivitySpatialRelationUserStory.Builder()
				.bufferProvider(this.trackGeometryService.bufferFunction(ActivitySpatialRelationUserStory.DEFAULT_BUFFER_DISTANCE))
				.lineProvider(this.trackGeometryService.multiLineStringFunction())
				.activityProvider(this.activityImportService.activityProvider()).build();
	}

	public Pair<ActivitySpatialRelation, ActivitySpatialRelation> spatialBiRelation(ActivitySpatialRelationId id)
			throws EntityNotFoundException {
		Pair<ActivitySpatialRelation, ActivitySpatialRelation> pair = story.apply(id);
		if (pair == null) {
			throw new EntityNotFoundException(id);
		} else {
			return pair;
		}
	}

	@Cache
	public ActivitySpatialRelation spatialRelation(ActivitySpatialRelationId id) throws EntityNotFoundException {
		return spatialBiRelation(id).getLeft();
	}

	public Function<ActivitySpatialRelationId, ActivitySpatialRelation> idFunction() {
		return new Function<ActivitySpatialRelationId, ActivitySpatialRelation>() {

			@Override
			@Nullable
			public ActivitySpatialRelation apply(@Nullable ActivitySpatialRelationId input) {
				try {
					return spatialRelation(input);
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}
}
