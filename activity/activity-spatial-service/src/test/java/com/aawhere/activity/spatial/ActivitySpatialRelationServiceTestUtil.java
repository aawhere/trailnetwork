/**
 * 
 */
package com.aawhere.activity.spatial;

import com.aawhere.activity.ActivityImportServiceTestUtil;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.queue.QueueTestUtil;

/**
 * @author aroller
 * 
 */
public class ActivitySpatialRelationServiceTestUtil {

	private ActivityImportServiceTestUtil importServiceTestUtil;
	private ActivitySpatialRelationService spatialService;

	/**
	 * Used to construct all instances of ActivitySpatialRelationServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<ActivitySpatialRelationServiceTestUtil> {

		private ActivityImportServiceTestUtil.Builder importServiceUtilBuilder;

		public Builder() {
			super(new ActivitySpatialRelationServiceTestUtil());
			this.importServiceUtilBuilder = ActivityImportServiceTestUtil.create();
			configureImportService(this.importServiceUtilBuilder);
		}

		/**
		 * @return the importServiceUtilBuilder
		 */
		public ActivityImportServiceTestUtil.Builder getImportServiceUtilBuilder() {
			return this.importServiceUtilBuilder;
		}

		public Builder useExternalImportUtil(ActivityImportServiceTestUtil.Builder importServiceUtilBuilder) {
			// someone else is building the imports.
			this.importServiceUtilBuilder = null;
			return configureImportService(importServiceUtilBuilder);
		}

		private Builder configureImportService(ActivityImportServiceTestUtil.Builder importServiceUtilBuilder) {

			importServiceUtilBuilder.setQueueFactory(QueueTestUtil.getQueueFactory());
			return this;
		}

		/** Must be set if managed by another. */
		public Builder setImportServiceUtil(ActivityImportServiceTestUtil importTestUtil) {
			building.importServiceTestUtil = importTestUtil;
			return this;
		}

		public ActivitySpatialRelationServiceTestUtil build() {

			ActivitySpatialRelationServiceTestUtil built = super.build();
			// either someone set from external or we are using ours.
			if (built.importServiceTestUtil == null) {
				built.importServiceTestUtil = this.importServiceUtilBuilder.build();
			}

			built.spatialService = new ActivitySpatialRelationService(
					built.importServiceTestUtil.getActivityImportService(),
					built.importServiceTestUtil.getTrackGeometryService());
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return the importServiceTestUtil
	 */
	public ActivityImportServiceTestUtil importServiceTestUtil() {
		return this.importServiceTestUtil;
	}

	/**
	 * @return the spatialService
	 */
	public ActivitySpatialRelationService spatialService() {
		return this.spatialService;
	}

	/** Use {@link Builder} to construct ActivitySpatialRelationServiceTestUtil */
	private ActivitySpatialRelationServiceTestUtil() {
	}

}
