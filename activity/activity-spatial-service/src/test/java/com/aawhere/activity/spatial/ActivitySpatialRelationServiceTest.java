/**
 * 
 */
package com.aawhere.activity.spatial;

import static org.junit.Assert.*;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ExampleTracksImportServiceTestUtil;
import com.aawhere.activity.spatial.ActivitySpatialRelationServiceTestUtil.Builder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.persist.GaePersistTestUtils;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * Tests that the service has put together the pieces to satisfy the persistence requirements of
 * {@link ActivitySpatialRelation}. The unit tests should test the logic of each method.
 * 
 * @author aroller
 * 
 */
public class ActivitySpatialRelationServiceTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private ActivitySpatialRelationServiceTestUtil testUtil;
	private ActivitySpatialRelationService service;
	private ExampleTracksImportServiceTestUtil exampleTracks;

	@Before
	public void setUpServices() {
		helper.setUp();
		final Builder utilBuilder = ActivitySpatialRelationServiceTestUtil.create();
		ExampleTracksImportServiceTestUtil.Builder exampleTracksBuilder = ExampleTracksImportServiceTestUtil.create()
				.registerDocuments(utilBuilder.getImportServiceUtilBuilder());
		this.testUtil = utilBuilder.build();
		this.service = testUtil.spatialService();
		this.exampleTracks = exampleTracksBuilder.importUtil(this.testUtil.importServiceTestUtil()).build();
	}

	@After
	public void tearDownService() {
		helper.tearDown();
	}

	@Test
	public void testIdentical() throws BaseException {
		Activity course = this.exampleTracks.line();
		Activity attempt = this.exampleTracks.line();
		final ActivitySpatialRelationId id = new ActivitySpatialRelationId(course.id(), attempt.id());
		Pair<ActivitySpatialRelation, ActivitySpatialRelation> spatialBiRelation = this.service.spatialBiRelation(id);
		ActivitySpatialRelation relation = spatialBiRelation.getLeft();
		ActivitySpatialRelation inverseRelation = spatialBiRelation.getRight();
		ActivitySpatialRelationId inverseId = ActivitySpatialRelationsUtil.inverseId(id);
		assertEquals(id, relation.id());
		assertEquals(inverseId, relation.id());
		assertEquals(MeasurementUtil.RATIO_ONE, relation.overlap());
		assertEquals(MeasurementUtil.RATIO_ONE, inverseRelation.overlap());
	}

}
