/**
 * 
 */
package com.aawhere.app;

import com.aawhere.app.account.Account;
import com.aawhere.doc.Document;

/**
 * External document used to produce properties of an {@link Account} provided from the
 * {@link Application}.
 * 
 * FIXME:This would be better in identity, however, document depends on identity and it should be
 * the inverse AR 2014.05.09
 * 
 * @author aroller
 * 
 */
public interface AccountDocument
		extends Document {

	/** provides the username alias. */
	public String username();

	/** The unique, non changing, identifier for the account at the application. */
	public String userId();

	/**
	 * The name that identifies the person at the {@link Application}. This may be the first and
	 * last name or some other combination of names the person may use, but unlike
	 * {@link #username()} this is not unique.
	 * 
	 * @return
	 */
	public String name();
}
