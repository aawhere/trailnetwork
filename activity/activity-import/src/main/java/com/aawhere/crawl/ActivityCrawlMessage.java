/**
 * 
 */
package com.aawhere.crawl;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Messages related to crawling a remote {@link Application}.
 * 
 * @author Brian Chapman
 * 
 */
public enum ActivityCrawlMessage implements Message {

	/** When a requested query was not valid */
	REMOTE_QUERY_NOT_VALID(
			"The query {REMOTE_URL} was  at {APPLICATION} was not valid",
			Param.REMOTE_URL,
			Param.APPLICATION),
	/** Used as a last resort if you don't have a better explanation */
	GENERAL_CRAWL_ERROR(
			"An error occurred during crawling of {REMOTE_URL} from {APPLICATION} that we aren't properly handling. This problem may be fixed in the future, but subsequent attempts will likely cause the same problem.",
			Param.REMOTE_URL,
			Param.APPLICATION),
	/**
	 * Indicates that a crawl was not attempted because there was a previous crawl attempt that
	 * failed and hasn't been fixed.
	 */
	PREVIOUS_FAILURE(
			"No crawl attempt will be made for {APPLICATION} at url {REMOTE_URL} since previous attempts failed.",
			Param.REMOTE_URL,
			Param.APPLICATION),
	BAD_INPUT_ERROR("The query paramaters ({QUERY_PARAMS}) provided could not be understood.", Param.QUERY_PARAMS);

	private ParamKey[] parameterKeys;
	private String message;

	private ActivityCrawlMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** The url used to query the {@link Application} */
		REMOTE_URL,

		/**
		 * The {@link ApplicationKey} of an {@link Application} or the Application itself.
		 */
		APPLICATION,
		/**
		 * The Query paramaters from the GarminConnectActivitiesFilter.
		 */
		QUERY_PARAMS;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
