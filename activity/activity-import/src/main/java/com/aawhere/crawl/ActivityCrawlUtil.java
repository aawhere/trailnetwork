/**
 * 
 */
package com.aawhere.crawl;

import static com.aawhere.route.process.RouteProcessWebApiUri.APP_CONNECT_CRAWL_GEO_CELL;
import static com.aawhere.ws.rs.UriConstants.SLASH;

import java.net.URI;

import com.aawhere.activity.ActivityWebApiUri;
import com.aawhere.doc.garmin.activity.GarminConnectActivitiesFilter;
import com.aawhere.measure.geocell.GeoCell;

/**
 * 
 * Utility functions for ActivityCrawlers.
 * 
 * @author Brian Chapman
 * 
 */
public class ActivityCrawlUtil {

	public static URI getLocalUrl(GarminConnectActivitiesFilter filter, GeoCell geocell) {
		String uriPath = appConnectCrawlGeoCellPath(geocell);
		URI uri = filter.appendTnFilterParamaters(uriPath);
		return uri;
	}

	public static String appConnectCrawlGeoCellPath(GeoCell geoCell) {
		return ActivityWebApiUri.ADMIN_PROCESS_PATH + SLASH + APP_CONNECT_CRAWL_GEO_CELL + SLASH
				+ geoCell.getCellAsString();
	}
}
