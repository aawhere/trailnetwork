/**
 * 
 */
package com.aawhere.crawl;

import java.net.URI;

import com.aawhere.app.ApplicationKey;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;

/**
 * Indicates a failure to crawl for a variety of reasons indicated by calling the proper builder
 * method.
 * 
 * @see ActivityCrawlException
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(HttpStatusCode.ACCEPTED)
public class ActivityCrawlException
		extends BaseException {

	private static final long serialVersionUID = 2340956898145206303L;

	/**
	 * Used to construct all instances of ActivityCrawlException.
	 * 
	 * Not a typical builder since Exceptions require full content upon construction.
	 */
	public static class Builder {

		private Throwable cause;
		private ApplicationKey applicationKey;
		private CompleteMessage.Builder messageBuilder = new CompleteMessage.Builder();
		private CompleteMessage message;

		public Builder() {

		}

		public Builder setCause(Throwable cause) {
			this.cause = cause;
			return this;
		}

		public ActivityCrawlException build() {
			Assertion.assertNotNull(this.applicationKey);
			if (message == null) {
				message = messageBuilder.build();
			}
			return new ActivityCrawlException(this.applicationKey, message, this.cause);
		}

		/**
		 * @param applicationActivityId2
		 * @return
		 */
		public Builder setApplication(ApplicationKey applicationKey) {
			this.applicationKey = applicationKey;
			messageBuilder.addParameter(ActivityCrawlMessage.Param.APPLICATION, applicationKey);
			return this;
		}

		public Builder setRemoteUri(URI remoteUri) {
			messageBuilder.addParameter(ActivityCrawlMessage.Param.REMOTE_URL, remoteUri);
			return this;
		}

		public Builder setMessage(CompleteMessage message) {
			this.message = message;
			return this;
		}

		public Builder remoteQueryNotValid() {
			validateMessage();
			this.messageBuilder.setMessage(ActivityCrawlMessage.REMOTE_QUERY_NOT_VALID);
			return this;
		}

		/**
		 * @return
		 */
		public Builder generalError() {
			validateMessage();
			this.messageBuilder.setMessage(ActivityCrawlMessage.GENERAL_CRAWL_ERROR);
			return this;
		}

		/**
		 * @return
		 */
		public Builder previousCrawlFailed() {
			validateMessage();
			this.messageBuilder.setMessage(ActivityCrawlMessage.PREVIOUS_FAILURE);
			return this;
		}

		private void validateMessage() {
			if (message != null) {
				throw new IllegalStateException("Already set the message");
			}
		}

	}// end Builder

	private ApplicationKey applicationKey;
	private Enum<? extends com.aawhere.util.rb.Message> code;

	/** Use {@link Builder} to construct ActivityCrawlException */
	@SuppressWarnings("unchecked")
	private ActivityCrawlException(ApplicationKey applicationKey, CompleteMessage message, Throwable cause) {
		super(message, cause);
		this.applicationKey = applicationKey;
		Message messageCode = message.getMessage();
		this.code = (Enum<? extends Message>) messageCode;
	}

	/**
	 * @return the id
	 */
	public ApplicationKey getApplication() {
		return this.applicationKey;
	}

}
