/**
 *
 */
package com.aawhere.doc;

import static com.aawhere.doc.AxmDocumentUtil.*;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;

import org.joda.time.LocalDate;

import com.aawhere.activity.ActivityDocument;
import com.aawhere.activity.UserNameNotProvidedException;
import com.aawhere.app.Device;
import com.aawhere.app.IdentityHistory;
import com.aawhere.app.Installation;
import com.aawhere.app.account.Account;
import com.aawhere.collections.Index;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;

import com.garmin.developer.schemas.axm.ActivityAxm;
import com.garmin.developer.schemas.axm.ActivityTypeAxm;
import com.garmin.developer.schemas.axm.AxmAxm;
import com.garmin.developer.schemas.axm.CreationHistoryAxm;
import com.garmin.developer.schemas.axm.CreationHistoryAxm.EntryAxm;
import com.garmin.developer.schemas.axm.ObjectFactory;

/**
 * @author Aaron Roller
 * 
 */
public class AxmDocument
		extends JaxbDocument<AxmAxm>
		implements Document, ActivityDocument {

	private static final long serialVersionUID = 6291684423933383476L;
	static final String UNKNOWN_NAME = "Untitled";

	/**
	 * Used to construct all instances of AxmDocument.
	 */
	public static class Builder
			extends JaxbDocument.Builder<AxmDocument, AxmAxm, Builder> {

		public Builder() {
			super(new AxmDocument());
		}

	}// end Builder

	/** Use {@link Builder} to construct AxmDocument */
	private AxmDocument() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.AXM_1_0;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.JaxbDocument#getObjectFactoryClass()
	 */
	@Override
	protected Class<?> getObjectFactoryClass() {
		return ObjectFactory.class;
	}

	private ActivityAxm getActivityAxm() {
		return this.getRoot().getActivities().get(Index.FIRST.ordinal());
	}

	private void addToHistory(Installation installation, IdentityHistory.Builder historyBuilder) {
		if (installation != null) {
			historyBuilder.add(installation);
		}
	}

	private IdentityHistory documentHistory(ActivityAxm axm) {
		IdentityHistory.Builder history = IdentityHistory.create();
		List<CreationHistoryAxm> creationHistories = axm.getCreationHistories();

		for (Iterator<CreationHistoryAxm> iterator = creationHistories.iterator(); iterator.hasNext();) {
			CreationHistoryAxm creationHistoryAxm = iterator.next();
			List<EntryAxm> entries = creationHistoryAxm.getEntries();
			// Special case. The first history is assumed to be the device. This is true most of
			// the time, but not always.
			Iterator<EntryAxm> iterator2 = entries.iterator();
			if (iterator2.hasNext()) {
				EntryAxm deviceAxm = iterator2.next();
				Device device = createDevice(deviceAxm);
				Installation deviceInstallation = installation(deviceAxm, device);
				addToHistory(deviceInstallation, history);
			}
			while (iterator2.hasNext()) {
				EntryAxm entryAxm = iterator2.next();
				final Installation installation = installation(entryAxm);
				addToHistory(installation, history);
			}
		}
		// this indicates the export time and version of Garmin Connect.
		history.add(getAuthor(getRoot()));
		return history.build();
	}

	// ********************
	// ActivityDocument Methods
	// *******************

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityName()
	 */
	@Override
	public @Nullable
	String getActivityName() {
		final ActivityAxm activityAxm = getActivityAxm();
		return (activityAxm.isNamed()) ? activityAxm.getName() : null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityId()
	 */
	@Override
	public String getActivityId() {
		return this.getActivityAxm().getId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getUserId()
	 */
	@Override
	public String getUserId() {
		try {
			return findUserName(this).getValue();
		} catch (UserNameNotProvidedException e) {
			// We can't throw the runtime as it isn't part of the interface's sig.
			// TODO: Perhaps it should be?
			throw new ToRuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getAccount()
	 */
	@Override
	public Account getAccount() {
		try {
			return findAccount(this.getRoot());
		} catch (UserNameNotProvidedException e) {
			throw new ToRuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityType()
	 */
	@Override
	public String getActivityType() {
		ActivityTypeAxm currentLevel = (ActivityTypeAxm) getActivityAxm().getActivityType();
		return currentLevel.getId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getHistory()
	 */
	@Override
	public IdentityHistory getHistory() {
		return documentHistory(getActivityAxm());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getDescription()
	 */
	@Override
	public String getDescription() {
		return getActivityAxm().getDescription();
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getDate()
	 */
	@Override
	public LocalDate getDate() {
		LoggerFactory.getLogger(getClass()).warning("not yet implemented, but it could be");
		return null;
	}

}
