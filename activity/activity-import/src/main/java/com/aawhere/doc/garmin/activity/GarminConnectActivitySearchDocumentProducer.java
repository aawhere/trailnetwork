/**
 * 
 */
package com.aawhere.doc.garmin.activity;

import java.io.IOException;
import java.io.InputStream;

import com.aawhere.activity.ActivitySearchDocument;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.doc.garmin.activity.json.ActivitySearch;
import com.aawhere.doc.track.axm_1_0.AxmDocumentProducer;
import com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Singleton;

/**
 * Produces {@link GarminConnectActivitySearchResults} for an {@link ActivitySearchDocument}
 * implementation of {@link Document}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class GarminConnectActivitySearchDocumentProducer
		implements DocumentProducer {

	/**
	 * 
	 */
	private static final DocumentFormatKey[] DOCUMENT_FORMAT_KEYS = new DocumentFormatKey[] { DocumentFormatKey.GC_SEARCH_JSON };
	private ObjectMapper mapper = new ObjectMapper();

	/**
	 * 
	 */
	public GarminConnectActivitySearchDocumentProducer() {

	}

	public static GarminConnectActivitySearchDocumentProducer build() {
		return new GarminConnectActivitySearchDocumentProducer();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	public Document produce(Object dataInput) throws DocumentDecodingException, DocumentContentsNotRecognizedException {
		ActivitySearch search;
		try {
			if (dataInput instanceof byte[]) {
				search = mapper.readValue((byte[]) dataInput, ActivitySearch.class);
			} else if (dataInput instanceof InputStream) {
				search = mapper.readValue((InputStream) dataInput, ActivitySearch.class);
			} else if (dataInput instanceof String) {
				search = mapper.readValue((String) dataInput, ActivitySearch.class);
			} else {
				throw new DocumentContentsNotRecognizedException();
			}
		} catch (IOException e) {
			throw new DocumentDecodingException(e);
		}
		return new GarminConnectActivitySearchResults.Builder().setResults(search.getResults()).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	public DocumentFormatKey[] getDocumentFormatsSupported() {
		return DOCUMENT_FORMAT_KEYS;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {
		return DiscoveryOrder.MEDIUM;
	}

	public static DocumentFactory.Builder registerAll(DocumentFactory.Builder factoryBuilder) {
		return factoryBuilder.register(new AxmDocumentProducer.Builder().build())
				.register(new GpxDocumentProducer.Builder().build())
				.register(GarminConnectActivitySearchDocumentProducer.build());
	}
}
