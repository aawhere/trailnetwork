/**
 *
 */
package com.aawhere.doc.track.gpx._1._1;

import java.io.Serializable;
import java.util.Iterator;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.Trk;
import com.aawhere.gpx._1._1.Trkseg;
import com.aawhere.gpx._1._1.Wpt;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;

/**
 * A GPX Document that exposes the Track contained within. There may be multiple tracks within a GPX
 * document so this handles only a single Track.
 * 
 * @author roller
 * 
 */
public class GpxTrack
		implements Track {

	private static final long serialVersionUID = 8263598691073261112L;

	/* Note that Seralizable was added for caching support. */
	private class TrackpointIterator
			implements Iterator<Trackpoint>, Serializable {

		private static final long serialVersionUID = -7637262851659306359L;
		private Iterator<Trkseg> segmentElementIterator;
		private Iterator<Wpt> trackpointElementIterator;

		/**
		 *
		 */
		public TrackpointIterator() {
			this.segmentElementIterator = trackElement.getSegments().iterator();
			prepareNextTrackpointIterator();
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			prepareForNext();
			boolean hasNext = trackpointElementIterator.hasNext();

			return hasNext;
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#next()
		 */
		@Override
		public GpxTrackpoint next() {
			prepareForNext();
			Wpt trackpointElement = this.trackpointElementIterator.next();
			return new GpxTrackpoint(trackpointElement);
		}

		/**
		 * Fastforwards to setup the next available trackpoint by searching through the segments.
		 * 
		 */
		private void prepareForNext() {
			// keep searching segments until another trackpoint is found
			while (segmentElementIterator.hasNext() && !trackpointElementIterator.hasNext()) {
				prepareNextTrackpointIterator();
			}
		}

		/**
		 * @return
		 */
		private void prepareNextTrackpointIterator() {
			this.trackpointElementIterator = segmentElementIterator.next().getTrackpoints().iterator();
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();

		}

	}

	private Trk trackElement;

	/**
	 * Assumes a single track is present and uses it as the defining track.
	 * 
	 * @param doc
	 */
	public GpxTrack(Gpx doc) {
		this.trackElement = doc.getTracks().get(0);
	}

	/**
	 * @param trkType
	 */
	public GpxTrack(Trk trk) {
		this.trackElement = trk;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Trackpoint> iterator() {

		return new TrackpointIterator();
	}

}
