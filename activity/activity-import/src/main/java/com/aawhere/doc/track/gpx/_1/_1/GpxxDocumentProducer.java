/**
 * 
 */
package com.aawhere.doc.track.gpx._1._1;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.gpx._1._1.GpxReader;
import com.aawhere.gpx._1._1.GpxRelaxedReader;
import com.aawhere.gpx._1._1.JaxbReader;
import com.aawhere.lang.ObjectBuilder;

/**
 * A producer that recognizes GPX files that was not recognized by the stricter parsers. This is a
 * catch all that will attempt to read the GPX file as an xml file and look for the tags it desires
 * without worrying about the fuss of schemas, xml declarations, etc.
 * 
 * http://developer.garmin.com/schemas/gpxx/v3/
 * 
 * Specifically these are GPX files that are generated from devices. It has been observed that the
 * extensions are misplaced and may cause JIBX not to understand the file, however, JAXB apparently
 * does. For this reason we have provided this producer to allow these files to be read
 * successfully.
 * 
 * @author aroller
 * 
 */
public class GpxxDocumentProducer
		extends GpxDocumentProducer {

	private GpxReader reader;

	/**
	 * Used to construct all instances of GpxxDocumentProducer.
	 */
	public static class Builder
			extends ObjectBuilder<GpxxDocumentProducer> {

		public Builder() {
			super(new GpxxDocumentProducer());
		}

		@Override
		public GpxxDocumentProducer build() {
			GpxxDocumentProducer built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct GpxxDocumentProducer */
	private GpxxDocumentProducer() {
		this.reader = new GpxRelaxedReader();
	}

	public static Builder createGpxx() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer#getReader()
	 */
	@Override
	protected GpxReader getReader() {
		return reader;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	public DocumentFormatKey[] getDocumentFormatsSupported() {
		return ArrayUtils.toArray(DocumentFormatKey.GPX);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {
		return DiscoveryOrder.LOW;
	}
}
