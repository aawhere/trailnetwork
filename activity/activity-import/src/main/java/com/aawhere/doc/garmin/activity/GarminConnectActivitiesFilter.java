/**
 *
 */
package com.aawhere.doc.garmin.activity;

import java.io.Serializable;
import java.net.URI;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.StringAdapter;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.GeoQuantity;
import com.aawhere.ws.rs.UriBuilderUtil;
import com.aawhere.xml.XmlNamespace;

/**
 * Simple filter object used to build request parameters for Garmin Connect Activity Search
 * ServiceStandard.
 * 
 * https://aawhere.jira.com/wiki/display/AAWHERE/Garmin+Connect+Provider
 * 
 * NOTE: These filters do not support the concept of operator that is provided by the GC API. All
 * operators are assumed to be equals operator (=). Chosen not to implement due to simplicity so it
 * may be implemented should there be a necessity.
 * 
 * @author aroller
 * 
 */
@XmlRootElement(name = "filter", namespace = XmlNamespace.API_VALUE)
@XmlType(name = "garminConnectActivitiesFilter", namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
public class GarminConnectActivitiesFilter
		implements Serializable, Cloneable {

	private static final long serialVersionUID = 7166929446881523554L;

	/**
	 * Garmin Connect query paramaters
	 */
	static final String BB_MIN_LAT = "bbMinLat";
	static final String BB_MIN_LON = "bbMinLong";
	static final String BB_MAX_LAT = "bbMaxLat";
	static final String BB_MAX_LON = "bbMaxLong";
	static final String PREVIOUS_DAYS = "previousDays";
	static final String OWNER = "owner";
	static final String ACTIVITY_TYPE = "activityType";
	static final String PAGE = "currentPage";
	static final String LIMIT_PER_PAGE = "limit";
	static final String IGNORE_UNTITLED = "ignoreUntitled";
	static final Integer DEFAULT_LIMIT_PER_PAGE = 5;
	static final Integer DEFAULT_PAGE = 1;
	/** Sets a time range to include way back until now. */
	static final Integer PREVIOUS_DAYS_DEFAULT = 20000;
	/**
	 * TrailNetwork query Params
	 */
	static final String QUERY_PARAM_PAGE = "page";
	static final String QUERY_PARAM_USERNAME = "username";
	static final String QUERY_PARAM_BOUNDS = "bounds";
	static final String QUERY_PARAM_LIMIT = "limit";
	static final String QUERY_PARAM_ACTIVITY_TYPE = "activityType";
	static final String QUERY_PARAM_IGNORE_UNTITLED = "ignoreUntitled";

	public enum ActivityType {
		/** Cycling is a top level filter which includes all other biking activities. */
		CYCLING,
		/** A leaf activity type part of cycling. */
		MOUNTAIN_BIKING,
		/** A leaf activity type part of cycling. */
		ROAD_BIKING,
		/** Hiking is a top level filter which does not have any wub types. */
		HIKING,
		/** Running is a top level filter which includes all other running activity types */
		RUNNING,
		/** Cyclocross is a leaf of cycling. */
		CYCLOCROSS,
		/** downhill_biking is a leaf of cycling */
		DOWNHILL_BIKING,
		/** recumbent_cycling is a leaf of cycling */
		RECUMBENT_CYCLING,
		/** track_cycling is a leaf of cycling */
		TRACK_CYCLING,
		/** street_running is a leaf of running */
		STREET_RUNNING,
		/** track_running is a leaf of running */
		TRACK_RUNNING,
		/** trail_running is a leaf of running */
		TRAIL_RUNNING,
		/** independent */
		CROSS_COUNTRY_SKIING,
		/** independent */
		RESORT_SKIING_SNOWBOARDING,
		/** independent */
		BACKCOUNTRY_SKIING_SNOWBOARDING,
		/** */
		SKATE_SKIING,
		/** */
		SWIMMING,
		/** leaf of swimming */
		OPEN_WATER_SWIMMING,
		/** leaf of swimming */
		LAP_SWIMMING,
		/** Parent */
		WALKING,
		/** leaf of walking */
		CASUAL_WALKING,
		/** leaf of walking */
		SPEED_WALKING,
		/** */
		INLINE_SKATING,
		/** */
		SKATING,
		/** */
		PADDLING,
		/** */
		MOUNTAINEERING,
		/** */
		FLYING,
		/** */
		SNOW_SHOE;
	}

	static final NumberFormat bbFormat;
	static {
		bbFormat = DecimalFormat.getNumberInstance();
		bbFormat.setGroupingUsed(false);
		bbFormat.setMaximumFractionDigits(5);
	}

	@QueryParam(QUERY_PARAM_PAGE)
	private Integer page;
	@QueryParam(QUERY_PARAM_BOUNDS)
	private BoundingBox boundingBox;
	@QueryParam(QUERY_PARAM_USERNAME)
	private String username;
	@QueryParam(QUERY_PARAM_LIMIT)
	private Integer limitPerPage = DEFAULT_LIMIT_PER_PAGE;
	@QueryParam(QUERY_PARAM_ACTIVITY_TYPE)
	public ActivityType activityType;
	@QueryParam(QUERY_PARAM_IGNORE_UNTITLED)
	public Boolean ignoreUntitled;

	/**
	 * Used to convert {@link GarminConnectActivitiesFilter} into a url-string that can be used to
	 * make a request to Garmin Connect.
	 * 
	 * @param uriPath
	 * @return
	 */
	public String appendFilterParmaters(String uriPath) {
		UriBuilder uriBuilder = UriBuilder.fromPath(uriPath);
		UriBuilderUtil.appendQueryParamIfNotNull(uriBuilder, OWNER, this.username);
		UriBuilderUtil.appendQueryParamIfNotNull(uriBuilder, PAGE, this.page);
		if (this.boundingBox != null) {

			GeoCoordinate max = boundingBox.getNorthEast();
			GeoCoordinate min = boundingBox.getSouthWest();
			uriBuilder.queryParam(BB_MAX_LAT, formatGeo(max.getLatitude()));
			uriBuilder.queryParam(BB_MAX_LON, formatGeo(max.getLongitude()));
			uriBuilder.queryParam(BB_MIN_LAT, formatGeo(min.getLatitude()));
			uriBuilder.queryParam(BB_MIN_LON, formatGeo(min.getLongitude()));
		}

		if (this.activityType != null) {
			// their api is Case Sensitive requiring all lower case. Our enum should match them
			// perfectly.
			uriBuilder.queryParam(ACTIVITY_TYPE, formatActivityType(activityType));
		}
		UriBuilderUtil.appendQueryParamIfNotNull(uriBuilder, LIMIT_PER_PAGE, limitPerPage);

		UriBuilderUtil.appendQueryParamIfNotNull(uriBuilder, IGNORE_UNTITLED, getIgnoreUntitled());
		uriBuilder.queryParam(PREVIOUS_DAYS, PREVIOUS_DAYS_DEFAULT);
		return uriBuilder.build((String) null).toString();

	}

	/**
	 * @return the ignoreUntitled
	 */
	public Boolean getIgnoreUntitled() {
		// applying the default here because Query Param can null out the default in the field
		if (this.ignoreUntitled == null) {
			this.ignoreUntitled = Boolean.FALSE;
		}
		return this.ignoreUntitled;
	}

	/**
	 * Creates a URI that can be used to queue in the trailnetwork application. It reverses the
	 * {@link StringAdapter}s used to create {@link GarminConnectActivitiesFilter}. Hitting this
	 * endpoint should re-construct {@link GarminConnectActivitiesFilter} again.
	 * 
	 * @param uriPath
	 * @return
	 */
	public URI appendTnFilterParamaters(String uriPath) {
		UriBuilder uriBuilder = UriBuilder.fromPath(uriPath);
		UriBuilderUtil.appendQueryParamIfNotNull(uriBuilder, QUERY_PARAM_USERNAME, this.username);
		UriBuilderUtil.appendQueryParamIfNotNull(uriBuilder, QUERY_PARAM_PAGE, this.page);
		if (this.boundingBox != null) {
			uriBuilder.queryParam(QUERY_PARAM_BOUNDS, this.boundingBox.getBounds());
		}
		UriBuilderUtil.appendQueryParamIfNotNull(uriBuilder, QUERY_PARAM_ACTIVITY_TYPE, this.activityType);
		UriBuilderUtil.appendQueryParamIfNotNull(uriBuilder, QUERY_PARAM_LIMIT, limitPerPage);
		UriBuilderUtil.appendQueryParamIfNotNull(uriBuilder, QUERY_PARAM_IGNORE_UNTITLED, this.getIgnoreUntitled());
		URI uri = uriBuilder.build((String) null);
		return uri;
	}

	public void incrementPage() {
		if (page == null) {
			page = DEFAULT_PAGE;
		}
		this.page = page + 1;
	}

	private static String formatGeo(GeoQuantity<?> quantity) {
		return bbFormat.format(quantity.getInDecimalDegrees());
	}

	public static String formatActivityType(ActivityType activityType) {
		return activityType.name().toLowerCase();
	}

	/**
	 * @return the limitPerPage
	 */
	public Integer getLimitPerPage() {
		if (limitPerPage == null) {
			this.limitPerPage = DEFAULT_LIMIT_PER_PAGE;
		}
		return this.limitPerPage;
	}

	/**
	 * @return the boundingBox
	 */
	public BoundingBox getBoundingBox() {
		return this.boundingBox;
	}

	/**
	 * @return the page
	 */
	public Integer getPage() {
		return this.page;
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(GarminConnectActivitiesFilter filter) {
		return new Builder(filter);
	}

	public static Builder clone(GarminConnectActivitiesFilter filter) {
		try {
			return new Builder((GarminConnectActivitiesFilter) filter.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Used to construct all instances of GarminConnectActivitiesFilter.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<GarminConnectActivitiesFilter> {

		public Builder() {
			super(new GarminConnectActivitiesFilter());
		}

		public Builder(GarminConnectActivitiesFilter filter) {
			super(filter);
		}

		public Builder setBoundingBox(BoundingBox boundingBox) {
			building.boundingBox = boundingBox;
			return this;
		}

		public Builder setOwner(String username) {
			building.username = username;
			return this;
		}

		public Builder setActivityType(ActivityType activityType) {
			building.activityType = activityType;
			return this;
		}

		public Builder setPageNumber(Integer pageNumber) {
			building.page = pageNumber;
			return this;
		}

		public Builder setPerPageLimit(Integer limit) {
			building.limitPerPage = limit;
			return this;
		}

		/**
		 * Don't show results that have no activity name.
		 * 
		 * @return
		 */
		public Builder ignoreUnnamed() {
			building.ignoreUntitled = Boolean.TRUE;
			return this;
		}

		/**
		 * Return results that do not have an activity name?
		 * 
		 * @return
		 */
		public Builder showUnnamed() {
			building.ignoreUntitled = Boolean.FALSE;
			return this;
		}
	}// end Builder

	/** Use {@link Builder} to construct GarminConnectActivitiesFilter */
	public GarminConnectActivitiesFilter() {
	}
}
