/**
 * 
 */
package com.aawhere.doc.garmin.activity;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Messages useful for status regarding Garmin Connect interaction.
 * 
 * @author aroller
 * 
 */
public enum GarminConnectApiMessage implements Message {

	/** Often times a coding error or a combination of bad request parameters. */
	BAD_REQUEST("A bad request was made to Garmin Connect API {RESPONSE}.", Param.RESPONSE), COMMUNICATION_PROBLEM(
			"There was a problem communicating with Garmin Connect API: {RESPONSE}",
			Param.RESPONSE),

	/**
	 * Indicates there was an error while processing the response likely due to unexpected data
	 * found in the standard format.
	 */
	PARSE_ERROR("Garmin Connect API returned information we aren't able to understand: {RESPONSE}", Param.RESPONSE);

	private ParamKey[] parameterKeys;
	private String message;

	private GarminConnectApiMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/**
		 * The message explaining the response of the API. May be the message from the exception.
		 */
		RESPONSE;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
