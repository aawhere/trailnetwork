/**
 *
 */
package com.aawhere.doc.track.gpx._1._1;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxReader;
import com.aawhere.lang.ObjectBuilder;

/**
 * Produces {@link GpxDocument}s.
 * 
 * @author Aaron Roller
 * 
 */
public class GpxDocumentProducer
		implements DocumentProducer {

	/**
	 * Used to construct all instances of GpxDocumentProducer.
	 */
	public static class Builder
			extends ObjectBuilder<GpxDocumentProducer> {

		public Builder() {
			super(new GpxDocumentProducer());
		}

	}// end Builder

	/**
	 * @return
	 */
	public static DocumentProducer create() {
		return new Builder().build();
	}

	/** Registers with the {@link DocumentFactory}. */
	public static void register() {
		DocumentFactory.getInstance().register(create());
	}

	/** Use {@link Builder} to construct GpxDocumentProducer */
	protected GpxDocumentProducer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	public Document produce(Object dataInput) throws DocumentDecodingException, DocumentContentsNotRecognizedException {
		Document result;
		Gpx gpx;
		if (dataInput instanceof byte[]) {
			dataInput = new ByteArrayInputStream((byte[]) dataInput);
		}

		if (dataInput instanceof InputStream) {
			InputStream inputStream = (InputStream) dataInput;
			GpxReader reader = getReader();
			try {
				gpx = reader.read(inputStream);
			} catch (GpxReadException e) {
				throw new DocumentContentsNotRecognizedException();
			}
		} else if (dataInput instanceof Gpx) {
			gpx = (Gpx) dataInput;
		} else {
			throw new DocumentContentsNotRecognizedException();
		}
		result = new GpxDocument.Builder().setGpx(gpx).setFormat(getDocumentFormatsSupported()[0]).build();

		return result;
	}

	/**
	 * @return
	 */
	protected GpxReader getReader() {
		return GpxReaderFactory.getReader();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	public DocumentFormatKey[] getDocumentFormatsSupported() {
		return ArrayUtils.toArray(DocumentFormatKey.GPX_1_1);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {
		return DiscoveryOrder.FIRST;
	}

}
