/**
 * 
 */
package com.aawhere.doc.track.gpx._1._1;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.gpx._1._0.Gpx10JaxbReader;
import com.aawhere.gpx._1._1.GpxReader;

/**
 * A Producer that recognizes GPX 1.0 documents, but reports it in a GPX 1.1 format so it may be
 * used by the system.
 * 
 * @author aroller
 * 
 */
public class Gpx10DocumentProducer
		extends GpxDocumentProducer {

	private Gpx10JaxbReader reader;

	/**
	 * 
	 */
	public Gpx10DocumentProducer() {
		this.reader = new Gpx10JaxbReader();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {
		return DiscoveryOrder.MEDIUM;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	public DocumentFormatKey[] getDocumentFormatsSupported() {
		return new DocumentFormatKey[] { DocumentFormatKey.GPX_1_0 };
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer#getReader()
	 */
	@Override
	protected GpxReader getReader() {
		return reader;
	}
}
