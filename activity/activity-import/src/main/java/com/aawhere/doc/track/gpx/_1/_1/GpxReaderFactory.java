/**
 * 
 */
package com.aawhere.doc.track.gpx._1._1;

import com.aawhere.gpx._1._1.GpxReader;
import com.aawhere.gpx._1._1.JibxReader;

/**
 * I get a valid {@link GpxReader}.
 * 
 * @author Brian Chapman
 * 
 */
public class GpxReaderFactory {

	public static GpxReader getReader() {
		return new JibxReader();
	}

}
