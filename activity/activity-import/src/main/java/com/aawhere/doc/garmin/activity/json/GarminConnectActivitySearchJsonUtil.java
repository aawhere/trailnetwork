/**
 * 
 */
package com.aawhere.doc.garmin.activity.json;

import com.aawhere.doc.garmin.activity.GarminConnectActivitiesFilter;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.MeasurementUtil;

/**
 * Useful methods to help the generated classes resulting related to {@link ActivitySearch}.
 * 
 * @author aroller
 * 
 */
public class GarminConnectActivitySearchJsonUtil {

	public static GarminConnectActivitiesFilter.Builder resultsToFilter(Search search) {
		GarminConnectActivitiesFilter.Builder builder = new GarminConnectActivitiesFilter.Builder();
		builder.setOwner(search.getQuery().getFilters().getOwner());
		builder.setPageNumber(Integer.parseInt(search.getCurrentPage()));
		BoundingBox boundingBox = search.getQuery().getBoundingBox();
		if (boundingBox != null) {
			builder.setBoundingBox(boundingBoxFromQuery(boundingBox));
		}
		return builder;
	}

	public static com.aawhere.measure.BoundingBox boundingBoxFromQuery(BoundingBox box) {
		Longitude east = MeasurementUtil.createLongitude(Double.parseDouble(box.getMaxLongitude()));
		Longitude west = MeasurementUtil.createLongitude(Double.parseDouble(box.getMinLongitude()));
		Latitude north = MeasurementUtil.createLatitude(Double.parseDouble(box.getMaxLatitude()));
		Latitude south = MeasurementUtil.createLatitude(Double.parseDouble(box.getMinLatitude()));
		com.aawhere.measure.BoundingBox result = new com.aawhere.measure.BoundingBox.Builder().setEast(east)
				.setNorth(north).setSouth(south).setWest(west).build();
		return result;
	}
}
