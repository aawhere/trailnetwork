/**
 * 
 */
package com.aawhere.doc.garmin.activity;

import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.joda.time.LocalDate;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivitySearchDocument;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.garmin.activity.json.Activitie;
import com.aawhere.doc.garmin.activity.json.Activity;
import com.aawhere.doc.garmin.activity.json.ActivitySummaryBeginTimestamp;
import com.aawhere.doc.garmin.activity.json.GarminConnectActivitySearchJsonUtil;
import com.aawhere.doc.garmin.activity.json.Query;
import com.aawhere.doc.garmin.activity.json.Results;
import com.aawhere.doc.garmin.activity.json.Search;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

/**
 * Used to request activities from GarminConnect and process the responses.
 * 
 * This is abstracted from any particular format, but uses the response and request to provide
 * useful information used to navigate the activit search API.
 * 
 * @author aroller
 * 
 */
@XmlRootElement(name = GarminConnectActivitySearchResults.ROOT_NAME, namespace = XmlNamespace.API_VALUE)
@XmlType(name = GarminConnectActivitySearchResults.ROOT_NAME, namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class GarminConnectActivitySearchResults
		implements ActivitySearchDocument {

	static final String ROOT_NAME = "results";
	private Multimap<AccountId, ActivityId> ids;
	private Results results;
	private LocalDate mostRecentStartDate;

	/**
	 * Used to construct all instances of GarminConnectActivitySearch.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<GarminConnectActivitySearchResults> {

		public Builder() {
			super(new GarminConnectActivitySearchResults());
		}

		/**
		 * @param page1SearchResults
		 * @return
		 */
		public Builder setResults(Results results) {
			building.results = results;
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public GarminConnectActivitySearchResults build() {
			final GarminConnectActivitySearchResults built = super.build();
			Multimap<AccountId, ActivityId> result = HashMultimap.create();
			for (Activitie activityElement : built.getActivities()) {
				Activity activity = activityElement.getActivity();
				final AccountId accountId = new AccountId(KnownApplication.GARMIN_CONNECT.key, activity.getUserId());
				ActivityId activityId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, activity.getActivityId());
				result.put(accountId, activityId);
				ActivitySummaryBeginTimestamp activitySummaryBeginTimestamp = activity
						.getActivitySummaryBeginTimestamp();
				if (activitySummaryBeginTimestamp != null) {
					String dateValue = activitySummaryBeginTimestamp.getValue();
					built.mostRecentStartDate = JodaTimeUtil.moreRecent(LocalDate.parse(dateValue),
																		built.mostRecentStartDate);
				}

			}
			built.ids = result;
			return built;

		}
	}// end Builder

	/** Use {@link Builder} to construct GarminConnectActivitySearch */
	private GarminConnectActivitySearchResults() {
	}

	/**
	 * If there are any results on this page then it's worth trying the next.
	 * 
	 * previously looking for equality of expected page count vs. actual page count, but that
	 * exactness may cause false stops if GC returns an invalid number for some unknown reason. In
	 * this case asking for one more page that will provide empty results doesn't hurt.
	 * 
	 */
	public Boolean anotherPageMayBeAvailable() {
		// the total page count isn't guaranteed so this logic is required
		return getNumberOfActivitiesOnCurrentPage() > 0;
	}

	@XmlAttribute()
	public Integer getCurrentPage() {
		return Integer.parseInt(results.getSearch().getCurrentPage());
	}

	/**
	 * This is not a reliable call so the scope is reduced to package for internal use Total found
	 * reports 1000 or the actual number if they know it is the last page.
	 * */
	Integer getTotalAvailable() {

		return Integer.parseInt(results.getSearch().getTotalFound());
	}

	@XmlAttribute()
	public Integer getIndexForFirstOnThisPage() {
		return Integer.parseInt(results.getSearch().getQuery().getActivityStart());
	}

	@XmlAttribute()
	public Integer getNumberOfActivitiesOnCurrentPage() {
		return this.results.getActivities().size();
	}

	@XmlAttribute()
	public Integer getNumberOfActivitiesPerPage() {
		return Integer.parseInt(results.getSearch().getQuery().getActivitiesPerPage());
	}

	/**
	 * Uses the {@link Search} and {@link Query} from the {@link #results} to build the filters
	 * required to request the next page.
	 * 
	 * This already assumes that you called {@link #anotherPageMayBeAvailable()} .
	 * 
	 * @return
	 */
	@XmlElement()
	public GarminConnectActivitiesFilter getFilterForNextPage() {
		com.aawhere.doc.garmin.activity.GarminConnectActivitiesFilter.Builder filterBuilder = GarminConnectActivitySearchJsonUtil
				.resultsToFilter(results.getSearch());
		filterBuilder.setPageNumber(getCurrentPage() + 1);
		return filterBuilder.build();
	}

	@XmlElement()
	public GarminConnectActivitiesFilter getFilter() {
		return GarminConnectActivitySearchJsonUtil.resultsToFilter(results.getSearch()).build();
	}

	/** Package local iterator of the raw results. If this goes beyond the package...abstract it! */
	List<Activitie> getActivities() {
		return this.results.getActivities();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.GC_SEARCH_JSON;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivitySearchDocument#activityIds()
	 */
	@Override
	public Set<ActivityId> activityIds() {
		return Sets.newHashSet(ids().values());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivitySearchDocument#finalPage()
	 */
	@Override
	public Boolean finalPage() {
		return !anotherPageMayBeAvailable();
	}

	/*
	 * @see com.aawhere.activity.ActivitySearchDocument#mostRecentStartDate()
	 */
	@Override
	public LocalDate mostRecentStartDate() {
		return this.mostRecentStartDate;
	}

	/*
	 * @see com.aawhere.activity.ActivitySearchDocument#ids()
	 */
	@Override
	public Multimap<AccountId, ActivityId> ids() {

		return this.ids;

	}

}
