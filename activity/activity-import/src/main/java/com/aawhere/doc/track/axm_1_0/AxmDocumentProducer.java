/**
 *
 */
package com.aawhere.doc.track.axm_1_0;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.doc.AxmDocument;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.bind.JAXBReadException;
import com.aawhere.xml.bind.JAXBReader;

import com.garmin.developer.schemas.axm.AxmAxm;
import com.garmin.developer.schemas.axm.ObjectFactory;

/**
 * Produces {@link AxmDocument}s.
 * 
 * @author Brian Chapman
 * 
 */
public class AxmDocumentProducer
		implements DocumentProducer {

	/**
	 * Used to construct all instances of AxmDocumentProducer.
	 */
	public static class Builder
			extends ObjectBuilder<AxmDocumentProducer> {

		public Builder() {
			super(new AxmDocumentProducer());
		}

	}// end Builder

	/**
	 * @return
	 */
	public static DocumentProducer create() {
		return new Builder().build();
	}

	/** Registers with the {@link DocumentFactory}. */
	public static void register() {
		DocumentFactory.getInstance().register(create());
	}

	/** Use {@link Builder} to construct GpxDocumentProducer */
	private AxmDocumentProducer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	public Document produce(Object dataInput) throws DocumentDecodingException, DocumentContentsNotRecognizedException {
		Document result;
		AxmAxm axmType;

		if (dataInput instanceof byte[]) {
			dataInput = new ByteArrayInputStream((byte[]) dataInput);
		}

		if (dataInput instanceof InputStream) {
			InputStream inputStream = (InputStream) dataInput;
			JAXBReader<AxmAxm, ObjectFactory> reader = JAXBReader.getReader(ObjectFactory.class);
			try {
				axmType = reader.read(inputStream);
			} catch (JAXBReadException e) {
				throw new DocumentContentsNotRecognizedException();
			}
		} else if (dataInput instanceof AxmAxm) {
			axmType = (AxmAxm) dataInput;
		} else {
			throw new DocumentContentsNotRecognizedException();
		}
		result = new AxmDocument.Builder().setRoot(axmType).build();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	public DocumentFormatKey[] getDocumentFormatsSupported() {
		return ArrayUtils.toArray(DocumentFormatKey.AXM_1_0);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {
		return DiscoveryOrder.HIGH;
	}

}
