/**
 *
 */
package com.aawhere.doc.track.gpx._1._1;

import java.util.Iterator;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.track.Track;
import com.aawhere.track.TrackContainer;

/**
 * Provides {@link Track}s from the standard GPX format.
 * 
 * @see GpxType
 * 
 * 
 * @author Aaron Roller
 * 
 */
public class GpxDocument
		implements Document, Iterable<Track>, TrackContainer {

	private static final long serialVersionUID = 4881662740303030933L;

	/**
	 * Used to construct all instances of GpxDocument.
	 */
	public static class Builder
			extends ObjectBuilder<GpxDocument> {

		public Builder() {
			super(new GpxDocument());
		}

		public Builder setGpx(Gpx gpxType) {
			building.gpx = gpxType;
			return this;
		}

		public Builder setFormat(DocumentFormatKey format) {
			building.format = format;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("format", building.format);
			Assertion.exceptions().notNull("gpx", building.gpx);

		}
	}// end Builder

	private Gpx gpx;
	private DocumentFormatKey format;

	/** Use {@link Builder} to construct GpxDocument */
	private GpxDocument() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return this.format;
	}

	/**
	 * @return the gpxType
	 */
	public Gpx getGpx() {
		return gpx;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Track> iterator() {
		return new GpxTracks(getGpx()).iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.TrackContainer#getTrack()
	 */
	@Override
	public Track getTrack() {
		final Iterator<Track> iterator = iterator();
		if (iterator.hasNext()) {
			return iterator().next();
		} else {
			// per the contract of TrackContainer
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		return GpxReaderFactory.getReader().write(gpx);
	}
}
