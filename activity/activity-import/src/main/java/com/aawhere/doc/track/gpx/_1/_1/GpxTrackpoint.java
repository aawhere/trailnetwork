/**
 *
 */
package com.aawhere.doc.track.gpx._1._1;

import org.joda.time.DateTime;

import com.aawhere.gpx._1._1.Wpt;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

/**
 * Extracts the points form a GPX Trackpoint Element.
 * 
 * @author roller
 * 
 */
public class GpxTrackpoint
		implements Trackpoint {

	private static final long serialVersionUID = 4173636472964565899L;
	private Wpt trackpointElement;
	private Elevation elevation;
	private Latitude latitude;
	private Longitude longitude;

	protected GpxTrackpoint() {
	}

	/**
	 * @param trackpointElement
	 */
	public GpxTrackpoint(Wpt trackpointElement) {
		this.trackpointElement = trackpointElement;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Trackpoint#getTimestamp()
	 */
	@Override
	public DateTime getTimestamp() {
		return trackpointElement.getDateTime();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Point3d#getElevation()
	 */
	@Override
	public com.aawhere.measure.Elevation getElevation() {
		if (elevation == null) {
			Double elevationValue = this.trackpointElement.getElevation();
			if (elevationValue != null) {
				elevation = MeasurementUtil.createElevationInMetersRounded(elevationValue);
			} else {
				elevation = null;
			}
		}
		return elevation;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Point#getLocation()
	 */
	@Override
	public GeoCoordinate getLocation() {
		return new GeoCoordinate.Builder().setLatitude(getLatitude()).setLongitude(getLongitude()).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Point#getLatitude()
	 */

	private Latitude getLatitude() {
		if (latitude == null) {
			Double latitudeValue = this.trackpointElement.getLatitude();

			if (latitudeValue != null) {
				latitude = MeasurementUtil.createLatitudeRounded(latitudeValue.doubleValue());

			} else {
				latitude = null;
			}
		}
		return latitude;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Point#getLongitude()
	 */
	private Longitude getLongitude() {
		if (longitude == null) {
			Double longitudeValue = this.trackpointElement.getLongitude();
			if (longitudeValue != null) {
				longitude = MeasurementUtil.createLongitudeRounded(longitudeValue.doubleValue());
			} else {
				longitude = null;
			}
		}
		return longitude;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Point#isLocationCapable()
	 */
	@Override
	public Boolean isLocationCapable() {
		// TODO:Move this to a base class or helper?
		return getLatitude() != null && getLongitude() != null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Trackpoint#equals(com.aawhere.track.Trackpoint)
	 */
	@Override
	public boolean equals(Trackpoint that) {
		return TrackUtil.equals(this, that);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return TrackUtil.hashCode(this);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return TrackUtil.equals(this, obj);
	}

	@Override
	public String toString() {

		DateTime timestamp = getTimestamp();
		String result;
		if (timestamp != null) {
			result = timestamp.toString();
		} else {
			result = super.toString();
		}
		return result;

	}
}
