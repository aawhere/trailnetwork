/**
 *
 */
package com.aawhere.doc;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;

import com.aawhere.activity.UserNameNotProvidedException;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.Device;
import com.aawhere.app.Installation;
import com.aawhere.app.Instance;
import com.aawhere.app.InstanceKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.Account;
import com.aawhere.i18n.LocaleUtil;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.util.rb.LocalizedStringMessage;
import com.garmin.developer.schemas.axm.ActivityAxm;
import com.garmin.developer.schemas.axm.ApplicationAxm;
import com.garmin.developer.schemas.axm.ApplicationInstallationAxm;
import com.garmin.developer.schemas.axm.ApplicationInstanceAxm;
import com.garmin.developer.schemas.axm.ApplicationVersionAxm;
import com.garmin.developer.schemas.axm.AxmAxm;
import com.garmin.developer.schemas.axm.CreationHistoryAxm;
import com.garmin.developer.schemas.axm.CreationHistoryAxm.EntryAxm;
import com.garmin.developer.schemas.axm.DefinitionAxm;
import com.garmin.developer.schemas.axm.DisplayAxm;
import com.garmin.developer.schemas.axm.UserAxm;

/**
 * Utilities for retrieving common components from an AxmDocument
 * 
 * @author Brian Chapman
 * 
 */
public class AxmDocumentUtil {

	/**
	 * 
	 */
	private static final Application GC_APPLICATION = KnownApplication.GARMIN_CONNECT.asApplication();
	public static final String GARMIN_CONNECT_UNKNOWN_INSTALLATION_ID_VALUE = "1";
	public static final String GC_ACTIVITY_TYPE_PREFIX = "activityType_";

	/**
	 * @see #findUserName(AxmDocument)
	 * 
	 * @param axm
	 * @return
	 * @throws UserNameNotProvidedException
	 */
	public static InstanceKey findUserName(AxmDocument axm) throws UserNameNotProvidedException {
		return findUserName(axm.getRoot());
	}

	/**
	 * Find the username from an AxmDocument. Return null if none found.
	 * 
	 * @param axm
	 * @return
	 * @throws UserNameNotProvidedException
	 */
	public static InstanceKey findUserName(AxmAxm axm) throws UserNameNotProvidedException {
		return new InstanceKey(findAccount(axm).getRemoteId());
	}

	public static com.aawhere.app.account.Account findAccount(AxmAxm axm) throws UserNameNotProvidedException {
		List<ActivityAxm> activities = axm.getActivities();
		if (activities.size() != 1) {
			throw new IllegalArgumentException("we don't support docs with multiple activities");
		} else {
			UserAxm userAxm = (UserAxm) activities.iterator().next().getUser();
			if (userAxm == null) {
				throw new UserNameNotProvidedException();
			}
			List<DefinitionAxm> definitions = axm.getDictionary().getDefinitions();
			for (DefinitionAxm def : definitions) {
				if (def.getId() == userAxm.getId()) {
					String userName = userAxm.getUserName();
					Account.Builder accountBuilder = new com.aawhere.app.account.Account.Builder()
							.setApplicationKey(GC_APPLICATION.getKey()).setRemoteId(userAxm.getIdValue());
					// TN-553 found a case where the alias was null
					if (userName != null) {
						accountBuilder.addAlias(userAxm.getUserName());
					}

					return accountBuilder.build();
				}
			}
		}
		throw new UserNameNotProvidedException();
	}

	/** Gets the author applicaiton and version from the document root. */
	public static Installation getAuthor(AxmAxm axm) {
		ApplicationInstallationAxm author = (ApplicationInstallationAxm) axm.getAuthor();
		Instance instance = instance((ApplicationInstanceAxm) author.getInstance());
		if (instance == null) {
			// although we could ask for the application, we would rather use our constant
			instance = new Instance.Builder().setApplication(GC_APPLICATION).build();
		}
		String version = version(author.getVersion());
		return new Installation.Builder().setInstance(instance).setVersion(version).setTimestamp(DateTime.now())
				.build();
	}

	public static Instance instance(ApplicationInstanceAxm axm) {
		Instance instance;
		if (axm != null) {
			Application application = createApplication((ApplicationAxm) axm.getApplication());
			InstanceKey key = new InstanceKey(axm.getIdValue());
			instance = new Instance.Builder().setApplication(application).setKey(key).build();
		} else {
			instance = null;
		}
		return instance;
	}

	/**
	 * @param applicationInstallation
	 * @return
	 */
	public static Installation installation(EntryAxm entryAxm, Instance instance) {
		Object applicationInstallation = entryAxm.getApplicationInstallation();

		ApplicationInstallationAxm axm = (ApplicationInstallationAxm) applicationInstallation;
		String key = axm.getIdValue();
		Installation result;
		if (key.equals(AxmDocumentUtil.GARMIN_CONNECT_UNKNOWN_INSTALLATION_ID_VALUE)) {
			result = null;
		} else {
			DateTime timestamp = JodaTimeUtil.createDateTime(entryAxm.getUploadDate());
			result = new Installation.Builder().setInstance(instance).setVersion(version(axm.getVersion()))
					.setTimestamp(timestamp).build();
		}
		return result;
	}

	/**
	 * @param applicationInstallation
	 * @return
	 */
	public static Installation installation(EntryAxm entryAxm) {
		Instance instance = AxmDocumentUtil.createInstance(entryAxm);
		return installation(entryAxm, instance);
	}

	/**
	 * @param version
	 * @return
	 */
	public static String version(Object version) {
		String result = null;
		if (version != null) {
			ApplicationVersionAxm axm = (ApplicationVersionAxm) version;
			List<DisplayAxm> displays = axm.getDisplays();
			if (!CollectionUtils.isEmpty(displays)) {
				DisplayAxm display = displays.get(0);
				result = display.getValue();
			}
		}
		return result;
	}

	/**
	 * Create a {@link Device} from a {@link CreationHistoryAxm}.
	 * 
	 * @param axm
	 * @return
	 */
	public static Device createDevice(EntryAxm entryAxm) {
		return (Device) createInstance(entryAxm, Device.createDevice());
	}

	/**
	 * Create a {@link Instance} from a {@link CreationHistoryAxm}.
	 * 
	 * @param axm
	 * @return
	 */
	public static Instance createInstance(EntryAxm entryAxm) {
		return createInstance(entryAxm, Instance.createInstance());
	}

	private static Instance createInstance(EntryAxm entryAxm, Instance.BaseBuilder<?, ?> builder) {
		ApplicationInstallationAxm axm = (ApplicationInstallationAxm) entryAxm.getApplicationInstallation();
		String key = axm.getIdValue();
		Instance result;
		if (key.equals(GARMIN_CONNECT_UNKNOWN_INSTALLATION_ID_VALUE)) {
			result = null;
		} else {
			final Application application = createApplication((ApplicationAxm) axm.getApplication());
			result = builder.setApplication(application)
					.setKey(createInstanceKey((ApplicationInstanceAxm) axm.getInstance())).build();
		}
		return result;
	}

	/**
	 * @param application
	 * @return
	 */
	public static Application createApplication(ApplicationAxm axm) {
		Application result;

		final DisplayAxm displayAxm = axm.getDisplays().get(0);
		final ApplicationKey key = new ApplicationKey(axm.getIdValue());
		if (key.equals(KnownApplication.UNKNOWN.key)) {
			result = KnownApplication.UNKNOWN.asApplication();
		} else {
			LocalizedStringMessage name = new LocalizedStringMessage(displayAxm.getValue(),
					LocaleUtil.localeFrom(displayAxm.getLang(), displayAxm.getCountry()));
			result = new Application.Builder().setKey(key).setName(name).build();
		}

		return result;
	}

	/**
	 * @param instance
	 * @return
	 */
	public static InstanceKey createInstanceKey(ApplicationInstanceAxm axm) {
		if (axm != null) {
			final Object key = axm.getKey();
			if (key != null) {
				return new InstanceKey(key.toString());
			}
		}
		return null;
	}

}
