/**
 *
 */
package com.aawhere.doc.track.gpx._1._1;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.Trk;
import com.aawhere.track.Track;

/**
 * Combines multiple GPX Tracks into a collection used for iteration.
 * 
 * @see MultipleTracksProcessor.Builder#setTracks(Iterable)
 * 
 * @author Aaron Roller on Apr 6, 2011
 * 
 */
public class GpxTracks
		implements Iterable<Track>, Serializable {

	private static final long serialVersionUID = 8439549264845134999L;

	private List<Track> tracks = new LinkedList<Track>();

	public GpxTracks(Gpx gpxType) {
		for (Trk trk : gpxType.getTracks()) {
			tracks.add(new GpxTrack(trk));
		}
	}

	@Override
	public Iterator<Track> iterator() {

		return tracks.iterator();
	}

}
