/**
 *
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.ActivityImportFailure.ActivityImportFailureId;
import com.aawhere.app.Application;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.StringIdentifier;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.xml.XmlNamespace;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Unindex;

/**
 * Describes an {@link Activity} located externally at a remote {@link Application}.
 * 
 * This is primarily used to keep track of import details, specifically those that are not able to
 * be imported due to any reason. This will prevent hitting the Application multiple times to
 * import.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Unindex
@Dictionary(domain = ActivityImportFailure.FIELD.DOMAIN, messages = ActivityImportMessage.class)
public class ActivityImportFailure
		extends BaseActivityImportMilestone<ActivityImportFailure, ActivityImportFailureId>
		implements SelfIdentifyingEntity {

	private static final long serialVersionUID = 6875194013937639723L;

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static class ActivityImportFailureId
			extends StringIdentifier<ActivityImportFailure> {

		private static final long serialVersionUID = 4311885675195030723L;

		ActivityImportFailureId() {
			super(ActivityImportFailure.class);
		}

		public ActivityImportFailureId(ActivityId activityId) {
			this(activityId.getValue());
		}

		/**
		 * @param value
		 * @param kind
		 */
		public ActivityImportFailureId(String value) {
			super(value, ActivityImportFailure.class);

		}

	}

	/**
	 * Used to construct all instances of ActivityImportMilestone.
	 */
	@XmlTransient
	public static class Builder
			extends BaseActivityImportMilestone.Builder<Builder, ActivityImportFailure, ActivityImportFailureId> {

		public Builder() {
			super(new ActivityImportFailure());
		}

		public Builder setVersion(ActivityImportProcessorVersion version) {
			building.processorVersion = version;
			return dis;
		}

		/*
		 * @see
		 * com.aawhere.activity.BaseActivityImportMilestone.Builder#setActivityId(com.aawhere.activity
		 * .ActivityId)
		 */
		@Override
		public Builder setActivityId(ActivityId id) {
			if (!hasId()) {
				setId(new ActivityImportFailure.ActivityImportFailureId(id));
			}
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ActivityImportFailure build() {

			return super.build();
		}

	}// end Builder

	/**
	 * @return
	 */
	public static ActivityImportFailure.Builder create() {
		return new Builder();
	}

	/** @see MilestoneVersion */
	@XmlElement(name = FIELD.VERSION)
	@Field(key = FIELD.VERSION)
	private ActivityImportProcessorVersion processorVersion;

	/**
	 * @return the processorVersion
	 */
	public ActivityImportProcessorVersion getProcessorVersion() {
		return this.processorVersion;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM, indexedFor = "TN-362")
	@Override
	public ActivityImportFailureId getId() {
		return super.getId();
	}

	/** Use {@link Builder} to construct ActivityImportMilestone */
	private ActivityImportFailure() {
	}

	@XmlTransient
	public static final class FIELD {
		static final String DOMAIN = "activityImportFailure";
		public static final String VERSION = "processorVersion";

		public static class KEY
				extends BaseActivityImportMilestone.FIELD.KEY {
			public static final FieldKey VERSION = new FieldKey(DOMAIN, FIELD.VERSION);
			public static final FieldKey ACTIVITY_ID = new FieldKey(DOMAIN,
					BaseActivityImportMilestone.FIELD.ACTIVITY_ID);
			public static final FieldKey ID = new FieldKey(DOMAIN, BaseEntity.BASE_FIELD.ID);
			public static final FieldKey ERROR_CODE = new FieldKey(DOMAIN, ImportMilestone.FIELD.ERROR_CODE);
			public static final FieldKey STATUS = new FieldKey(DOMAIN, ImportMilestone.FIELD.STATUS);
			public static final FieldKey DATE_CREATED = new FieldKey(DOMAIN, BaseEntity.BASE_FIELD.DATE_CREATED);
		}
	}

}
