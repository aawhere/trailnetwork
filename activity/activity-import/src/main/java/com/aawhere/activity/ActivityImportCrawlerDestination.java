/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.activity.ApplicationActivityImportInstructions.FIELD.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.annotation.Nullable;
import javax.measure.quantity.Length;
import javax.measure.unit.USCustomarySystem;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;
import org.joda.time.Days;

import com.aawhere.activity.ApplicationActivityImportInstructions.Destination;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.account.AccountId;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.Filter;
import com.aawhere.queue.Task;
import com.aawhere.util.rb.KeyedMessageFormat;
import com.aawhere.xml.XmlNamespace;

/**
 * Prepares the URL to search activities when given either account information or cell/geo
 * information information. activities from a remote {@link Application} using the
 * {@link ApplicationActivityImportInstructions}.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class ActivityImportCrawlerDestination {

	/**
	 * The application being crawled. Provided from {@link #accountId} or directly when crawling by
	 * cells.
	 */
	@XmlElement
	private ApplicationKey applicationKey;
	/**
	 * Account ID representing the unique id for the user. In this case sometimes
	 * {@link AccountId#getRemoteId()} may return the alias, rather than the true account id
	 * depending upon the API parameters for the specific application (i.e. MMF uses the account id,
	 * but GC uses the alias).
	 */
	@XmlElement
	@Nullable
	private AccountId accountId;
	/** When crawling by area the geocells are provided. */
	@Nullable
	private GeoCells cells;

	@XmlElement
	private Destination destination;
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private HashMap<String, Object> templateMap = new HashMap<>();
	@XmlElement
	private Filter filter;

	/**
	 * Optionally provided, this provides a common time filter showing the most recent.
	 * 
	 * @see ApplicationActivityImportInstructions.FIELD#SINCE_TIMESTAMP
	 */
	@XmlElement
	@Nullable
	private DateTime sinceTimestamp;
	/**
	 * The final goal of this to build the url that will retrieve the crawling results from the
	 * application.
	 */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private URL url;

	/**
	 * Used to construct all instances of ActivityImportCrawlerDestination.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<ActivityImportCrawlerDestination> {

		private ApplicationActivityImportInstructions instructions;

		public Builder() {
			super(new ActivityImportCrawlerDestination());
		}

		public Builder instructions(ApplicationActivityImportInstructions instructions) {
			this.instructions = instructions;

			return this;
		}

		public Builder accountId(AccountId accountId) {
			building.accountId = accountId;
			building.applicationKey = accountId.getApplicationKey();
			return this;
		}

		public Builder since(DateTime sinceTimestamp) {
			building.sinceTimestamp = sinceTimestamp;
			return this;
		}

		public Builder filter(Filter filter) {
			building.filter = filter;
			return this;
		}

		private void buildAccount(ActivityImportCrawlerDestination built) {
			if (built.isCrawlingByAccount()) {
				built.destination = this.instructions.activitySearchForAccountDestination();
				if (built.destination == null) {
					throw ToRuntimeException.wrapAndThrow(new ApplicationImportInstructionsNotFoundException(
							built.accountId.getApplicationKey()));
				}
				built.templateMap.put(	ApplicationActivityImportInstructions.FIELD.ACCOUNT_REMOTE_ID,
										built.accountId.getRemoteId());
			}

		}

		private void buildCell(ActivityImportCrawlerDestination built) {

			if (built.isCrawlingByCells()) {
				built.destination = this.instructions.searchDestination();
				GeoCellBoundingBox bounds = GeoCellBoundingBox.createGeoCell().setGeoCells(built.cells).build();
				built.templateMap.put(CENTER_LAT, bounds.getCenter().getLatitude().getInDecimalDegrees());
				built.templateMap.put(CENTER_LON, bounds.getCenter().getLongitude().getInDecimalDegrees());
				built.templateMap.put(MIN_LAT, bounds.getMin().getLatitude().getInDecimalDegrees());
				built.templateMap.put(MIN_LON, bounds.getMin().getLongitude().getInDecimalDegrees());
				built.templateMap.put(MAX_LAT, bounds.getMax().getLatitude().getInDecimalDegrees());
				built.templateMap.put(MAX_LON, bounds.getMax().getLongitude().getInDecimalDegrees());
				Length diagonal = bounds.getDiagonal();
				// the diagonal is the longest length of the box. Proximity is circular and will
				// encompass the corner and more so there will be overlap
				Length proximity = QuantityMath.create(diagonal).dividedBy(2).getQuantity();
				built.templateMap.put(PROXIMITY_IN_MILES, proximity.doubleValue(USCustomarySystem.MILE));
				// add other boundary replacements as appropriate
			}
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("filter", building.filter);
			Assertion.exceptions().notNull("instructions", this.instructions);
			Assertion.exceptions().notNull("applicationKey", building.applicationKey);
			Assertion.exceptions().assertMinimumNotNull("cells or accountId", 1, building.cells, building.accountId);

		}

		@Override
		public ActivityImportCrawlerDestination build() {
			ActivityImportCrawlerDestination built = super.build();
			// TN-809 Paging is zero-based for mmf & everytrail,ours is 1-based
			final Integer startZeroBased = built.filter.getStart() - 1;
			built.templateMap.put(FILTER_START, startZeroBased);
			built.templateMap.put(FILTER_PAGE, built.filter.getPage());

			// standard limit for modified since
			if (built.sinceTimestamp != null) {
				built.templateMap.put(SINCE_TIMESTAMP, built.sinceTimestamp);
				int days = Days.daysBetween(built.sinceTimestamp.toLocalDate(), DateTime.now().toLocalDate()).getDays();
				built.templateMap.put(SINCE_DAYS, days);
			}
			buildAccount(built);
			buildCell(built);
			Assertion.exceptions().notNull("destination", built.destination);
			// URI Builder would be more obvious, but it does some aggressive escaping that causes
			// problems.
			String preparedUri = KeyedMessageFormat.create(built.destination.url).forgiveExtraParameters()
					.putAll(built.templateMap).build().getFormattedMessage();
			try {
				built.url = new URL(preparedUri);
			} catch (MalformedURLException e) {
				// this is our url. potentially ugly syntax could be added from id inputs
				throw InvalidArgumentException.notUnderstood(ActivityImportMessage.APPLICATION_URI, preparedUri);
			}
			return built;
		}

		/**
		 * @param cells
		 * @return
		 */
		public Builder cells(GeoCells cells) {
			building.cells = cells;
			return this;
		}

		/**
		 * @param applicationKey
		 * @return
		 */
		public Builder applicationKey(ApplicationKey applicationKey) {
			building.applicationKey = applicationKey;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct ActivityImportCrawlerDestination */
	private ActivityImportCrawlerDestination() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return the destination
	 */
	public Destination destination() {
		return this.destination;
	}

	/**
	 * @return the url
	 */
	public URL url() {
		return this.url;
	}

	public Boolean isCrawlingByAccount() {
		return this.accountId != null;
	}

	public Boolean isCrawlingByCells() {
		return this.cells != null;
	}

	/**
	 * Provides the task that can be posted to the queue to retrieve the next page of the results
	 * matching the filter.
	 * 
	 * @return
	 * 
	 */
	public Task nextPageTask() {
		return nextPageTask(null);
	}

	public Task nextPageTask(@Nullable Boolean deep) {
		Task task = null;
		int nextPage = filter.getPage() + 1;
		if (isCrawlingByCells()) {
			task = ActivityWebApiUri.applicationCellsEndpoint(	applicationKey.getValue(),
																cells.toString(),
																nextPage,
																deep);
		}
		if (isCrawlingByAccount()) {
			task = ActivityWebApiUri.accountEndpoint(accountId, nextPage, deep);
		}
		return task;
	}

}
