/**
 * 
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.id.Identifier;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.xml.XmlNamespace;

/**
 * Provides an {@link ImportMilestone} that loggs the activity associated with the Milestone event.
 * 
 * @author aroller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
abstract public class BaseActivityImportMilestone<MilestoneT extends BaseActivityImportMilestone<MilestoneT, IdentifierT>, IdentifierT extends Identifier<String, MilestoneT>>
		extends ImportMilestone<MilestoneT, IdentifierT>
		implements ActivityIdentifiable, SelfIdentifyingEntity {

	private static final long serialVersionUID = 2468929050433101474L;

	/**
	 * Used to construct all instances of ActivityScopedMilestone.
	 */
	@XmlTransient
	abstract public static class Builder<BuilderT extends BaseActivityImportMilestone.Builder<BuilderT, MilestoneT, IdentifierT>, MilestoneT extends BaseActivityImportMilestone<MilestoneT, IdentifierT>, IdentifierT extends Identifier<String, MilestoneT>>
			extends ImportMilestone.Builder<BuilderT, MilestoneT, IdentifierT> {

		protected Builder(MilestoneT milestone) {
			super(milestone);
		}

		@Override
		protected String getGeneralErrorString() {
			return ActivityImportMessage.GENERAL_IMPORT_ERROR.toString();
		}

		/**
		 * When the activity is known, this will be set.
		 * 
		 * @param id
		 * @return
		 */
		abstract public BuilderT setActivityId(ActivityId id);

	}// end Builder

	/** Use {@link Builder} to construct ActivityScopedMilestone */
	protected BaseActivityImportMilestone() {
	}

	/**
	 * @return the activityId
	 */
	@Override
	public ActivityId getActivityId() {

		final IdentifierT id = getId();
		if (id == null) {
			return null;
		}
		return new ActivityId(id.getValue());
	}

	@XmlTransient
	public static final class FIELD {
		public final static String DOMAIN = ImportMilestone.FIELD.DOMAIN;
		public final static String ACTIVITY_ID = ActivityWebApiUri.ACTIVITY_ID;

		public static class KEY
				extends ImportMilestone.FIELD.KEY {

		}
	}
}
