/**
 * 
 */
package com.aawhere.activity;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.ImportMilestone.Status;
import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;

/**
 * A container for multiple {@link ActivityImportFailure}s all related to the same activity
 * maintaining order by time.
 * 
 * @author brian
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class ImportMilestoneHistory<MilestoneT extends ImportMilestone<MilestoneT, ?>>
		extends BaseFilterEntities<MilestoneT> {

	@Override
	public List<MilestoneT> getCollection() {
		// TODO:this shouldn't be sorted here, but should use a sorted
		// collection
		List<MilestoneT> collection = super.getCollection();
		// This really really should be a sorted collection. Perhaps a PriorityQueue?
		// We don't want to sort and get arbitrary results if we don't have a date to sort on.
		if (!collection.isEmpty() && collection.iterator().next().getDateCreated() != null) {
			Collections.sort(collection);
		}
		return collection;
	}

	/**
	 * The most recent milestone to be added or null if there are none.
	 * 
	 * @return
	 */
	@Nullable
	public MilestoneT getLatestMilestone() {
		return com.aawhere.collections.ListUtilsExtended.lastOrNull(getCollection());
	}

	@Nullable
	public Status getLatestStatus() {
		MilestoneT milestone = getLatestMilestone();
		if (milestone != null) {
			return milestone.getStatus();
		} else {
			return null;
		}
	}

	public Boolean isErroneous() {
		// TODO: make this robust by having it look to see if the last status is
		// erroneous, not just any
		Predicate<Status> predicate = Predicates.equalTo(Status.FAILED);
		Collection<Status> errorStatuses = filterStatus(predicate);
		return !errorStatuses.isEmpty();
	}

	public Boolean isQueuedForImport() {
		Status latestStatus = getLatestStatus();
		return latestStatus != null && latestStatus.equals(Status.QUEUED);
	}

	/**
	 * @param predicate
	 * @return
	 */
	Collection<Status> filterStatus(Predicate<Status> predicate) {
		Collection<Status> statuses = Collections2.transform(getCollection(), new ActivityMilestoneStatusFunction());
		Collection<Status> errorStatuses = Collections2.filter(statuses, predicate);
		return errorStatuses;
	}

}