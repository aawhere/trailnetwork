package com.aawhere.activity.imprt.mmf.type;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class _embedded {
	private List<Activity_types> activity_types;

	public List<Activity_types> getActivity_types() {
		return this.activity_types;
	}

	public void setActivity_types(List<Activity_types> activity_types) {
		this.activity_types = activity_types;
	}
}
