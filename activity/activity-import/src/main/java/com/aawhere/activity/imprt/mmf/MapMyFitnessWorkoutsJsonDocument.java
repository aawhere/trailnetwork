/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.mutable.Mutable;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportId;
import com.aawhere.activity.ActivitySearchDocument;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

/**
 * The activity results when calling MMF for workouts, typically by user id. This also handles
 * {@link RoutesMmfJson} which extends {@link Workouts} to provide account ids and ActivityIds,
 * although those types of "Activities" aren't supported.
 * 
 * @author aroller
 * 
 */
public class MapMyFitnessWorkoutsJsonDocument
		implements ActivitySearchDocument {

	private Multimap<AccountId, ActivityId> activityIds;
	private Boolean finalPage;
	public LocalDate mostRecentDate;

	/**
	 * Used to construct all instances of MapMyFitnessWorkoutsJsonDocument.
	 */
	public static class Builder
			extends ObjectBuilder<MapMyFitnessWorkoutsJsonDocument>
			implements Mutable<EmbeddedCollection<? extends Workouts>> {

		private EmbeddedCollection<? extends Workouts> results;

		public Builder() {
			super(new MapMyFitnessWorkoutsJsonDocument());
		}

		public Builder workouts(EmbeddedCollection<? extends Workouts> results) {
			this.results = results;
			return this;
		}

		@Override
		public MapMyFitnessWorkoutsJsonDocument build() {
			MapMyFitnessWorkoutsJsonDocument built = super.build();
			built.activityIds = HashMultimap.create();
			Workouts embedded = results.get_embedded();
			if (embedded != null) {
				List<? extends Workout> workouts = embedded.getWorkouts();
				for (Workout workout : workouts) {

					AccountId accountId = new AccountId(KnownApplication.MMF.key, workout.get_links().getUser()
							.iterator().next().getId());
					String remoteId = workout.get_links().getSelf().get(0).getId();
					ActivityId id;
					// only bother if time series is present
					if (workout.getHas_time_series()) {
						id = new ActivityId(applicationKey(), remoteId);
					} else {
						id = new ActivityImportId.NotWorthImporting(applicationKey(), remoteId);
					}
					built.activityIds.put(accountId, id);
					DateTime start = MapMyFitnessWorkoutJson.start(workout);
					if (start != null) {
						LocalDate date = start.toLocalDate();
						built.mostRecentDate = JodaTimeUtil.moreRecent(date, built.mostRecentDate);
					}
				}
			}
			// next is only provided if there is a next.
			built.finalPage = CollectionUtils.isEmpty(results.get_links().getNext());
			return built;
		}

		/**
		 * Provides the application key from the results document.
		 * 
		 * @see WorkoutsResults
		 * @see RoutesResultsMmfJson
		 * @return
		 */
		private ApplicationKey applicationKey() {
			return this.results.applicationKey();
		}

		/*
		 * (non-Javadoc)
		 * @see org.apache.commons.lang3.mutable.Mutable#getValue()
		 */
		@Override
		public EmbeddedCollection<? extends Workouts> getValue() {
			return this.results;
		}

		/*
		 * (non-Javadoc)
		 * @see org.apache.commons.lang3.mutable.Mutable#setValue(java.lang.Object)
		 */
		@Override
		public void setValue(EmbeddedCollection<? extends Workouts> value) {
			this.results = value;
		}

	}// end Builder

	/** Use {@link Builder} to construct MapMyFitnessWorkoutsJsonDocument */
	private MapMyFitnessWorkoutsJsonDocument() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivitySearchDocument#activityIds()
	 */
	@Override
	public Set<ActivityId> activityIds() {
		return Sets.newHashSet(this.activityIds.values());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.MMFUW7;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivitySearchDocument#finalPage()
	 */
	@Override
	public Boolean finalPage() {
		return finalPage;
	}

	/*
	 * @see com.aawhere.activity.ActivitySearchDocument#ids()
	 */
	@Override
	public Multimap<AccountId, ActivityId> ids() {
		return activityIds;
	}

	/**
	 * @return the mostRecentDate
	 */
	@Override
	public LocalDate mostRecentStartDate() {
		return this.mostRecentDate;
	}

}
