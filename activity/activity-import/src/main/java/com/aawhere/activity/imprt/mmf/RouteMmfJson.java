/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Used for browsing MMF by location looking only for
 * 
 * @author aroller
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteMmfJson
		extends Workout {

	private _links _links;

	/** Provides access to the route id and user for search document. */
	@Override
	public _links get_links() {
		return this._links;
	}

	@Override
	public void set_links(_links _links) {
		this._links = _links;
	}
}
