/**
 *
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.field.FieldKey;
import com.aawhere.xml.XmlNamespace;

/**
 * Builds upon {@link ImportHistoryFilterBuilder}, adding activityId.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class BaseActivityImportHistoryFilterBuilder<FilterBuilderT extends ImportHistoryFilterBuilder<FilterBuilderT>>
		extends ImportHistoryFilterBuilder<FilterBuilderT> {

	public BaseActivityImportHistoryFilterBuilder() {
		super();
	}

	/** Limits to only the given activity id. */
	public FilterBuilderT equalsActivityId(ActivityId activityId) {
		return idEqualTo(new ActivityImportFailure.ActivityImportFailureId(activityId));
	}

	protected abstract FieldKey getActivityKey();

}
