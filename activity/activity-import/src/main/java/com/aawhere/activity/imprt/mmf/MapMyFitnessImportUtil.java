/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.util.regex.Pattern;

/**
 * @author aroller
 * 
 */
public class MapMyFitnessImportUtil {

	/**
	 * Looks for a phrase with something leading the name, but ends with a date.
	 * 
	 * Walked 1.00 mi on 4/16/13
	 * 
	 * File Import - Bike Ride (12/18/2011)
	 * 
	 * http://regex101.com/
	 * 
	 */
	private static final Pattern DATE_REGEX = Pattern.compile("^.+?\\d{1,2}\\/\\d{1,2}\\/\\d{2,4}\\)?$");
	/**
	 * 10.19 mi Run on Sep 6, 2012 9:29 PM
	 * 
	 * ^.+?\d{1,2}:\d{2}\s[[a|A]|[p|P]][M|m]$
	 * */
	private static final Pattern TIME_REGEX = Pattern.compile("^.+?\\d{1,2}:\\d{2}\\s[[a|A]|[p|P]][M|m]$");

	/**
	 * Road Cycling on Aug. 18, 2013
	 * 
	 * Walk on Oct. 5, 2014
	 * 
	 * ^.+?\.\s\d{1,2},\s\d{2,4}?$
	 */
	private static final Pattern WORD_DATE_REGEX = Pattern.compile("^.+?\\.\\s\\d{1,2},\\s\\d{2,4}?$");

	/**
	 * mm-312498573
	 * 
	 * Bike Ride on 2013-06-16
	 */
	private static final Pattern DATE_DASHED_REGEX = Pattern.compile("^.+?\\d{4}-\\d{2}-\\d{2}$");

	private static final Pattern[] NAME_DEFAULT_PATTERNS = { DATE_REGEX, TIME_REGEX, WORD_DATE_REGEX, DATE_DASHED_REGEX };

	public static Boolean isDefaultName(String name) {

		boolean matches = false;
		for (int i = 0; i < NAME_DEFAULT_PATTERNS.length && !matches; i++) {
			Pattern pattern = NAME_DEFAULT_PATTERNS[i];
			matches = pattern.matcher(name).matches();
		}
		return matches;
	}

	/**
	 * Given the name this will ensure it's worth keep or send a null back if not.
	 * 
	 * @see #isDefaultName(String)
	 * 
	 * @param name
	 * @return
	 */
	public static String filter(String name) {
		if (isDefaultName(name)) {
			name = null;
		}
		return name;
	}

}
