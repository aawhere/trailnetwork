/**
 * 
 */
package com.aawhere.activity.imprt.rwgps;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.LocalDate;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportCrawler;
import com.aawhere.activity.ActivitySearchDocument;
import com.aawhere.app.account.AccountId;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.joda.time.JodaTimeUtil;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RWGpsUserActivitiesJson
		implements ActivitySearchDocument {

	/** The document doesn't provide the account since all are the same. */
	static final AccountId UNKNOWN = ActivityImportCrawler.ACCOUNT_ID_UNAVAILABLE;
	private List<RWGpsUserActivityJson> results;

	/*
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		throw new UnsupportedOperationException();
	}

	/*
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.RW_USER_ACTIVITIES_JSON;
	}

	/*
	 * @see com.aawhere.activity.ActivitySearchDocument#activityIds()
	 */
	@Override
	public Set<ActivityId> activityIds() {
		return Sets.newHashSet(Iterables.filter(Iterables.transform(this.results, activityIdFunction()),
												Predicates.notNull()));
	}

	/*
	 * @see com.aawhere.activity.ActivitySearchDocument#ids()
	 */
	@Override
	public Multimap<AccountId, ActivityId> ids() {
		return Multimaps.index(activityIds(), Functions.constant(UNKNOWN));
	}

	/*
	 * @see com.aawhere.activity.ActivitySearchDocument#finalPage()
	 */
	@Override
	public Boolean finalPage() {
		return CollectionUtils.isEmpty(results);
	}

	/*
	 * @see com.aawhere.activity.ActivitySearchDocument#mostRecentStartDate()
	 */
	@Override
	public LocalDate mostRecentStartDate() {

		return (!finalPage()) ? JodaTimeUtil.mostRecentPartial(Iterables.transform(this.results, dateFunction()))
				: null;
	}

	public static Function<RWGpsUserActivityJson, LocalDate> dateFunction() {
		return new Function<RWGpsUserActivityJson, LocalDate>() {

			@Override
			public LocalDate apply(RWGpsUserActivityJson input) {
				return (input != null) ? input.date() : null;
			}
		};
	}

	public static Function<RWGpsUserActivityJson, ActivityId> activityIdFunction() {
		return new Function<RWGpsUserActivityJson, ActivityId>() {

			@Override
			public ActivityId apply(RWGpsUserActivityJson input) {
				return (input != null) ? input.activtiyId() : null;
			}
		};
	}
}
