package com.aawhere.activity.imprt.mmf.type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Icon_url {
	private String href;

	public String getHref() {
		return this.href;
	}

	public void setHref(String href) {
		this.href = href;
	}
}
