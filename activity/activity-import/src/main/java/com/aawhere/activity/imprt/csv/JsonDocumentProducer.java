/**
 * 
 */
package com.aawhere.activity.imprt.csv;

import java.io.IOException;
import java.io.InputStream;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author aroller
 * 
 */
public class JsonDocumentProducer<D extends Document>
		implements DocumentProducer {

	private DocumentFormatKey[] formats;
	private DiscoveryOrder discoveryOrder = DiscoveryOrder.MEDIUM;
	private Class<D> documentType;

	/**
	 * Used to construct all instances of JsonDocumentProducer.
	 */
	public static class Builder<D extends Document>
			extends ObjectBuilder<JsonDocumentProducer<D>> {

		public Builder() {
			super(new JsonDocumentProducer<D>());
		}

		public Builder<D> format(DocumentFormatKey... format) {
			building.formats = format;
			return this;
		}

		public Builder<D> documentType(Class<D> documentType) {
			building.documentType = documentType;
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.exceptions().notNull("documentType", building.documentType);
			Assertion.exceptions().notNull("formats", building.formats);

		}

		@Override
		public JsonDocumentProducer<D> build() {
			JsonDocumentProducer<D> built = super.build();
			return built;
		}

	}// end Builder

	public static <D extends Document> Builder<D> create() {
		return new Builder<D>();
	}

	/** Use {@link Builder} to construct JsonDocumentProducer */
	private JsonDocumentProducer() {
	}

	/*
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	public D produce(Object dataInput) throws DocumentDecodingException, DocumentContentsNotRecognizedException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibilityChecker(mapper.getVisibilityChecker().withFieldVisibility(Visibility.ANY));
		D document;

		try {
			if (dataInput instanceof String) {
				document = mapper.readValue((String) dataInput, documentType);
			} else if (dataInput instanceof byte[]) {
				document = mapper.readValue((byte[]) dataInput, documentType);
			} else if (dataInput instanceof InputStream) {
				document = mapper.readValue((InputStream) dataInput, documentType);
			} else {
				throw new DocumentContentsNotRecognizedException();
			}
		} catch (JsonParseException | JsonMappingException e) {
			throw new DocumentDecodingException(e);
		} catch (IOException e) {
			throw new DocumentContentsNotRecognizedException(e);
		}

		return document;
	}

	/*
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	public DocumentFormatKey[] getDocumentFormatsSupported() {
		return formats;
	}

	/*
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {
		return discoveryOrder;
	}

}
