/**
 * 
 */
package com.aawhere.activity.imprt.rwgps;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aawhere.activity.ActivityDocument;
import com.aawhere.activity.imprt.mmf.MapMyFitnessImportUtil;
import com.aawhere.app.IdentityHistory;
import com.aawhere.app.account.Account;
import com.aawhere.collections.IterablesExtended;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.SimpleTrack;
import com.aawhere.track.Track;
import com.aawhere.track.TrackContainer;
import com.aawhere.track.TrackUtil;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author aroller
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RWGpsTripDetailsJsonActivityDocument
		implements ActivityDocument, TrackContainer {

	public final Trip trip = null;

	/**
	 * 
	 */
	public RWGpsTripDetailsJsonActivityDocument() {

	}

	/*
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		throw new UnsupportedOperationException();
	}

	/*
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.RW_TRIP_JSON;
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getActivityName()
	 */
	@Override
	public String getActivityName() {
		// OK, maybe this is cheating, but it works?
		return MapMyFitnessImportUtil.filter(trip.name);
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getActivityId()
	 */
	@Override
	public String getActivityId() {
		return trip.id;
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getUserId()
	 */
	@Override
	public String getUserId() {
		return trip.user.id;
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getAccount()
	 */
	@Override
	public Account getAccount() {
		return trip.user.account();
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getActivityType()
	 */
	@Override
	public String getActivityType() {
		return null;
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getHistory()
	 */
	@Override
	public IdentityHistory getHistory() {
		return null;
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getDescription()
	 */
	@Override
	public String getDescription() {
		return trip.description;
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getDate()
	 */
	@Override
	public LocalDate getDate() {
		return JodaTimeUtil.dateTimeFromUnixEpoch(trip.metrics.firstTime).toLocalDate();
	}

	/*
	 * @see com.aawhere.track.TrackContainer#getTrack()
	 */
	@Override
	public Track getTrack() {
		Iterable<com.aawhere.track.Trackpoint> trackpoints = IterablesExtended.cast(trip.track_points);
		return new SimpleTrack.Builder(trackpoints).build();
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Metrics {
		public final Long firstTime = null;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Trip {
		public final String id = null;
		public final String name = null;
		public final String privacy_code = null;
		public final RWGpsUserJsonAccountDocument user = null;
		public final String description = null;
		public final Metrics metrics = null;
		public final List<Trackpoint> track_points = null;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Trackpoint
			implements com.aawhere.track.Trackpoint {

		private static final long serialVersionUID = -162001067467567457L;

		public final Double x = null;
		public final Double y = null;
		public final Long t = null;
		public final Double e = null;

		/*
		 * @see com.aawhere.track.Point3d#getElevation()
		 */
		@Override
		public Elevation getElevation() {
			return (e != null) ? MeasurementUtil.createElevationInMeters(e) : null;
		}

		/*
		 * @see com.aawhere.track.Point#getLocation()
		 */
		@Override
		public GeoCoordinate getLocation() {
			return (isLocationCapable()) ? MeasurementUtil.createGeoCoordinate(y, x) : null;
		}

		/*
		 * @see com.aawhere.track.Point#isLocationCapable()
		 */
		@Override
		public Boolean isLocationCapable() {
			return x != null && y != null;
		}

		/*
		 * @see com.aawhere.track.Trackpoint#getTimestamp()
		 */
		@Override
		public DateTime getTimestamp() {
			return JodaTimeUtil.dateTimeFromUnixEpoch(t);
		}

		/*
		 * @see com.aawhere.track.Trackpoint#equals(com.aawhere.track.Trackpoint)
		 */
		@Override
		public boolean equals(com.aawhere.track.Trackpoint that) {
			return TrackUtil.equals(this, that);
		}

	}

}
