/**
 * 
 */
package com.aawhere.activity;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nullable;

import com.aawhere.activity.ActivityImportFailure.ActivityImportFailureId;
import com.aawhere.activity.imprt.mmf.MapMyFitnessImportUtil;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountUtil;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.persist.Filter;
import com.aawhere.queue.BatchQueues;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.Task;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.util.rb.Message;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class ActivityImportUtil {

	public static Set<ActivityId> idsFromImports(Iterable<ActivityImportFailure> milestones) {
		// TODO:I should have used Google Collections
		HashSet<ActivityId> ids = new HashSet<ActivityId>();
		for (ActivityImportFailure milestone : milestones) {
			ids.add(milestone.getActivityId());
		}

		return ids;
	}

	/**
	 * Extracts the deep parameter from filter.
	 * 
	 * @param filter
	 * @return
	 */
	public static Boolean deep(Filter filter) {
		return filter != null && filter.containsOption(SystemWebApiUri.DEEP);
	}

	/**
	 * A standard place to provide the web connected document retriever.
	 * 
	 * @see ActivityImportCrawler
	 * 
	 * @return
	 */
	public static Function<ActivityImportCrawlerDestination, DocumentRetriever> webDocumentRetrieverFunction(
			final DocumentFactory documentFactory) {
		return new Function<ActivityImportCrawlerDestination, DocumentRetriever>() {

			@Override
			public DocumentRetriever apply(ActivityImportCrawlerDestination input) {
				return ActivityImportCrawler.createWebDocumentRetriever(input, documentFactory);
			}
		};
	}

	/**
	 * Returns only those activity ids that indicate they are worth importing.
	 * 
	 * @see #idWorthImportingPredicate()
	 * @param unfiltered
	 * @return
	 */
	public static Set<ActivityId> idsWorthImporting(Set<ActivityId> unfiltered) {
		return Sets.filter(unfiltered, idWorthImportingPredicate());
	}

	/**
	 * Filters out those ActivityId that implement {@link ActivityImportId} and indicate false or if
	 * the given input is null. Otherwise everything else is true. Nulls are supported.
	 * 
	 * @return
	 */
	public static Predicate<ActivityId> idWorthImportingPredicate() {
		return new Predicate<ActivityId>() {

			@Override
			public boolean apply(@Nullable ActivityId input) {
				if (input == null) {
					return false;
				}
				return (input instanceof ActivityImportId) ? ((ActivityImportId) input).isWorthImporting() : true;
			}
		};
	}

	public static Function<ActivityImportFailureId, ActivityId> activityIdFunction() {
		return new Function<ActivityImportFailure.ActivityImportFailureId, ActivityId>() {

			@Override
			public ActivityId apply(ActivityImportFailureId input) {
				return activityId(input);
			}
		};
	}

	/**
	 * Provides the activity id from the {@link ActivityImportFailureId} or null if null is given.
	 * 
	 * @param input
	 * @return
	 */
	public static ActivityId activityId(ActivityImportFailureId input) {
		return (input != null) ? new ActivityId(input.getValue()) : null;
	}

	/**
	 * Posts a task for every account found in the resulting filter.
	 * 
	 * @param accountIds
	 * @param filter
	 * @return
	 */
	public static Integer
			crawlTasksForAccounts(Iterable<AccountId> accountIds, Filter filter, QueueFactory queueFactory) {

		BatchQueues<ApplicationKey> queue = new BatchQueues.Builder<ApplicationKey>()
				.queueProvider(AccountUtil.queueProvider(queueFactory)).build();

		for (AccountId accountId : accountIds) {
			if (accountId != null) {
				Task task = ActivityWebApiUri.accountEndpoint(	accountId,
																filter.getPage(),
																ActivityImportUtil.deep(filter));
				queue.add(task, accountId.getApplicationKey());
			}
		}
		return queue.finishAll();
	}

	/**
	 * @param nonExistingActivities
	 * @return
	 */
	public static Iterable<ActivityImportFailureId> activityImportFailureIds(Iterable<ActivityId> activityIds) {
		return Iterables.transform(activityIds, activityImportFailureIdFunction());
	}

	public static Function<ActivityId, ActivityImportFailureId> activityImportFailureIdFunction() {
		return new Function<ActivityId, ActivityImportFailure.ActivityImportFailureId>() {

			@Override
			public ActivityImportFailureId apply(ActivityId input) {
				return (input != null) ? new ActivityImportFailureId(input) : null;
			}
		};
	}

	/**
	 * @param nonExistingFailureIds
	 * @return
	 */
	public static Iterable<ActivityId> activityIds(Iterable<ActivityImportFailureId> failureIds) {
		return Iterables.transform(failureIds, activityIdFunction());
	}

	public static Predicate<Activity> nameDefaultActivityRemovalPredicate() {
		return new Predicate<Activity>() {

			@Override
			public boolean apply(Activity input) {
				// keep(true) if it's not the default
				return !namedByApplication(input.id().getApplicationKey(), String.valueOf(input.getName()));
			}
		};
	}

	/**
	 * Allows filtering out default names when identified by the {@link Application} from the
	 * {@link ActivityId}.
	 * 
	 * @return
	 */
	public static Predicate<Entry<ActivityId, String>> nameDefaultRemovalPredicate() {
		return new Predicate<Entry<ActivityId, String>>() {

			@Override
			public boolean apply(Entry<ActivityId, String> input) {
				return !namedByApplication(input.getKey().getApplicationKey(), input.getValue());
			}

		};
	}

	/** Indicates if this is a known default name for the given application */
	public static Boolean namedByApplication(final ApplicationKey applicationKey, @Nullable String name) {
		// TODO:use a plug and play filter provider
		switch (applicationKey.getValue()) {
			case KnownApplication.MMF_KEY:
				return MapMyFitnessImportUtil.isDefaultName(name);
			default:
				break;
		}
		return false;
	}

	/**
	 * An indication that the name does not match an indicator of an unknown or
	 * {@link #namedByApplication(ApplicationKey, String)}.
	 * 
	 * @param application
	 * @param nameString
	 * @return
	 */
	public static Boolean namedByPerson(ApplicationKey application, String nameString) {
		return ActivityUtil.nameKnown(nameString) && !namedByApplication(application, nameString);
	}

	/**
	 * extracts the information from {@link Activity} to answer
	 * {@link #namedByPerson(ApplicationKey, String)}.
	 * 
	 * @param activity
	 * @return
	 */
	public static Boolean namedByPerson(Activity activity) {
		Message name = activity.getName();
		return name != null && namedByPerson(activity.id().getApplicationKey(), name.toString());
	}

}
