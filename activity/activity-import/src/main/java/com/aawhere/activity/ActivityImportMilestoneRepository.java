/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.activity.ActivityImportFailure.ActivityImportFailureId;
import com.aawhere.persist.CompleteRepository;

/**
 * Repository for {@link ActivityImportFailure}
 * 
 * TODO:Determine we really need two different repositories with
 * {@link ActivityImportHistoryRepository}.
 * 
 * @author aroller
 * 
 */
public interface ActivityImportMilestoneRepository
		extends CompleteRepository<ActivityImportFailures, ActivityImportFailure, ActivityImportFailureId> {

}