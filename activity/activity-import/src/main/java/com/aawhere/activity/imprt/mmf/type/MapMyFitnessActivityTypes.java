/**
 * 
 */
package com.aawhere.activity.imprt.mmf.type;

import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aawhere.activity.ActivityType;
import com.aawhere.activity.imprt.mmf.MmfJsonReader;
import com.aawhere.activity.imprt.mmf.MmfJsonReader.Builder;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.io.FileNotFoundException;
import com.aawhere.lang.exception.ToRuntimeException;

import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class MapMyFitnessActivityTypes {

	private final Map<String, ActivityType> knownActivityTypes;

	/**
	 * 
	 */
	public MapMyFitnessActivityTypes() {
		try {
			HashMap<String, ActivityType> mapped = new HashMap<>();
			// these are the roots or parents that we wish to map and be recognized
			mapped.put("9", ActivityType.WALK);
			mapped.put("24", ActivityType.HIKE);
			mapped.put("11", ActivityType.BIKE);
			mapped.put("41", ActivityType.MTB);
			mapped.put("36", ActivityType.ROAD);
			mapped.put("16", ActivityType.RUN);
			mapped.put("25", ActivityType.RUN);// indoor run is a root
			mapped.put("15", ActivityType.SWIM);
			mapped.put("20", ActivityType.SWIM);
			mapped.put("63", ActivityType.SKI);
			mapped.put("74", ActivityType.SKI);
			ActivityTypes all = readAll();
			List<Activity_types> activity_types = all.get_embedded().getActivity_types();
			for (Activity_types activity_type : activity_types) {
				final _links links = activity_type.get_links();
				boolean matchFound = false;
				final List<Self> selfs = links.getSelf();
				String selfId = null;
				for (Self self : selfs) {
					String id = self.getId();
					matchFound |= mapped.containsKey(id);
					selfId = id;
				}
				// Bike is a parent of MTB, or MTB is a parent of MTB Hills.
				if (!matchFound) {
					List<Parent> parents = links.getParent();
					if (parents != null) {
						for (Parent parent : parents) {
							String parentId = parent.getId();
							ActivityType known = mapped.get(parentId);
							if (known != null) {
								mapped.put(selfId, known);
								matchFound = true;
							}
						}
					}
				}
				// BIKE is the root of MTB Hills, MTB and BIKE itself.
				if (!matchFound) {
					List<Root> roots = links.getRoot();
					if (roots != null) {
						for (Root root : roots) {
							String rootId = root.getId();
							ActivityType known = mapped.get(rootId);
							if (known != null) {
								mapped.put(selfId, known);
								matchFound = true;
							}
						}
					}
				}
			}

			knownActivityTypes = Collections.unmodifiableMap(mapped);
		} catch (DocumentDecodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static ActivityTypes read(InputStream inputStream) throws DocumentContentsNotRecognizedException,
			DocumentDecodingException {
		final Builder<ActivityTypes> builder = MmfJsonReader.create();
		MmfJsonReader<ActivityTypes> reader = builder.type(ActivityTypes.class).build();
		return reader.read(inputStream);
	}

	public ActivityTypes readAll() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		final String fileName = "ActivityTypes.json";

		InputStream is = this.getClass().getResourceAsStream(fileName);
		if (is == null) {
			throw ToRuntimeException.wrapAndThrow(new FileNotFoundException(fileName));
		}
		return read(is);
	}

	/**
	 * Returns the {@link ActivityType} matching the key or null if it is not known.
	 * 
	 * @param mmfActivityTypeId
	 * @return
	 */
	public ActivityType type(String mmfActivityTypeId) {
		return knownActivityTypes.get(mmfActivityTypeId);
	}

	/**
	 * Provides the mapping for anyone to lookup, but not modify.
	 * 
	 * @return
	 */
	public Map<String, ActivityType> knownActivityTypes() {
		return knownActivityTypes;
	}
}
