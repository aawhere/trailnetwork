package com.aawhere.activity.imprt.mmf.type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivityTypes {
	private _embedded _embedded;
	private _links _links;
	private Number total_count;

	public _embedded get_embedded() {
		return this._embedded;
	}

	public void set_embedded(_embedded _embedded) {
		this._embedded = _embedded;
	}

	public _links get_links() {
		return this._links;
	}

	public void set_links(_links _links) {
		this._links = _links;
	}

	public Number getTotal_count() {
		return this.total_count;
	}

	public void setTotal_count(Number total_count) {
		this.total_count = total_count;
	}
}
