/**
 * 
 */
package com.aawhere.activity.imprt.rwgps;

import com.aawhere.activity.imprt.csv.JsonDocumentProducer;
import com.aawhere.doc.DocumentFactory.Builder;
import com.aawhere.doc.DocumentFormatKey;

/**
 * @author aroller
 * 
 */
public class RWGpsDocumentProducers {

	/**
	 * https://ridewithgps.com/users/2/trips.json
	 * 
	 * @return
	 */
	public static JsonDocumentProducer<RWGpsUserActivitiesJson> userActivitiesProducer() {
		return JsonDocumentProducer.<RWGpsUserActivitiesJson> create().documentType(RWGpsUserActivitiesJson.class)
				.format(DocumentFormatKey.RW_USER_ACTIVITIES_JSON).build();
	}

	/**
	 * https://ridewithgps.com/trips/94.json
	 * 
	 * @return
	 */
	public static JsonDocumentProducer<RWGpsTripDetailsJsonActivityDocument> activityProducer() {
		return JsonDocumentProducer.<RWGpsTripDetailsJsonActivityDocument> create()
				.documentType(RWGpsTripDetailsJsonActivityDocument.class).format(DocumentFormatKey.RW_TRIP_JSON)
				.build();
	}

	/**
	 * @param factoryBuilder
	 */
	public static void registerAll(Builder factoryBuilder) {
		factoryBuilder.register(activityProducer());
		factoryBuilder.register(userActivitiesProducer());
	}

}
