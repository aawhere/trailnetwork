/**
 * 
 */
package com.aawhere.activity.imprt.rwgps;

import org.apache.commons.lang3.BooleanUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportId;
import com.aawhere.app.KnownApplication;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The Activity results that makes up {@link RWGpsUserActivitiesJson}.
 * 
 * @see RWGpsTripDetailsJsonActivityDocument
 * 
 * @author aroller
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RWGpsUserActivityJson {

	public final String id = null;
	public final Boolean is_gps = null;
	public final String departed_at = null;

	/**
	 * 
	 */
	public RWGpsUserActivityJson() {
	}

	public final LocalDate date() {
		return DateTime.parse(departed_at).toLocalDate();
	}

	/**
	 * @return
	 */
	public ActivityId activtiyId() {
		return (BooleanUtils.isTrue(is_gps)) ? new ActivityId(KnownApplication.RWGPS.key, id)
				: new ActivityImportId.NotWorthImporting(KnownApplication.RWGPS.key, id);
	}

}
