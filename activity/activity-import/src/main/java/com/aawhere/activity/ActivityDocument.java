/**
 * 
 */
package com.aawhere.activity;

import javax.annotation.Nullable;

import org.joda.time.LocalDate;

import com.aawhere.app.IdentityHistory;
import com.aawhere.app.account.Account;
import com.aawhere.doc.Document;
import com.aawhere.track.Track;

/**
 * Any document that can provide all data required for an {@link Activity} and it's {@link Track}.
 * 
 * @author aroller
 * 
 */
public interface ActivityDocument
		extends Document {

	/**
	 * Provides the name given the activity by the user at the Application.
	 * 
	 * @return the name or null if not known.
	 */
	public @Nullable
	String getActivityName();

	/**
	 * @return the activityId
	 */
	public String getActivityId();

	/**
	 * @return the userId
	 */
	public String getUserId();

	public Account getAccount();

	public String getActivityType();

	/**
	 * The history of where this activity has been. Typically starting with the device and ending at
	 * the application serving up this activity. There must be at least one entry representing the
	 * Application.
	 * 
	 * @return
	 */
	public IdentityHistory getHistory();

	/**
	 * Comments about the activity more lengthy than the name.
	 * 
	 * @return
	 */
	public String getDescription();

	/**
	 * @return
	 */
	LocalDate getDate();

}
