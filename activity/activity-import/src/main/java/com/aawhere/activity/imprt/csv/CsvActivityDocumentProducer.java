/**
 * 
 */
package com.aawhere.activity.imprt.csv;

import java.io.InputStream;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.app.UnknownApplicationException;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.lang.ObjectBuilder;

/**
 * Produces the {@link CsvActivityDocument} from String and InputStream.
 * 
 * @author aroller
 * 
 */
public class CsvActivityDocumentProducer
		implements DocumentProducer {

	/**
	 * Used to construct all instances of CsvActivityDocumentProducer.
	 */
	public static class Builder
			extends ObjectBuilder<CsvActivityDocumentProducer> {

		public Builder() {
			super(new CsvActivityDocumentProducer());
		}

	}// end Builder

	/** Use {@link Builder} to construct CsvActivityDocumentProducer */
	private CsvActivityDocumentProducer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	@Nonnull
	public Document produce(@Nonnull Object dataInput) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException {

		CsvActivityDocument.Builder builder;
		if (dataInput instanceof String) {
			try {
				builder = CsvActivityDocument.create((String) dataInput);
			} catch (UnknownApplicationException e) {
				throw new DocumentDecodingException(e);
			}
		} else if (dataInput instanceof byte[]) {
			try {
				builder = CsvActivityDocument.create((byte[]) dataInput);
			} catch (UnknownApplicationException e) {
				throw new DocumentDecodingException(e);
			}
		} else if (dataInput instanceof InputStream) {
			try {
				builder = CsvActivityDocument.create((InputStream) dataInput);
			} catch (UnknownApplicationException e) {
				throw new DocumentDecodingException(e);
			}
		} else {
			throw new DocumentContentsNotRecognizedException();
		}
		return builder.build();

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	@Nonnull
	public DocumentFormatKey[] getDocumentFormatsSupported() {
		return ArrayUtils.toArray(DocumentFormatKey.ACTIVITY_CSV);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {

		return DiscoveryOrder.LOW;
	}

}
