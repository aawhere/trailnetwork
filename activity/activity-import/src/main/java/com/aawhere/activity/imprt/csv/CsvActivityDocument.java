/**
 * 
 */
package com.aawhere.activity.imprt.csv;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.annotation.Nullable;
import javax.measure.quantity.DataAmount;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.USCustomarySystem;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.ISODateTimeFormat;

import com.aawhere.activity.ActivityDocument;
import com.aawhere.activity.ActivityDocumentDecodingException;
import com.aawhere.activity.UserNameNotProvidedException;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.IdentityHistory;
import com.aawhere.app.Installation;
import com.aawhere.app.Instance;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.app.account.Account;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;
import com.aawhere.track.Track;
import com.aawhere.track.TrackContainer;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

/**
 * A simple Comma Separated Values file for general purposes.
 * 
 * http://www.rfc-editor.org/rfc/rfc4180.txt
 * 
 * This requires an offset duration for timestamp provided in the first line of the document.
 * 
 * WARNING:getbytes does not behave consistently depending upon how this was constructed. An
 * inputstream does not produce bytes. It could so if there is a problem we should fix it and make
 * it work.
 * 
 * TODO: Split this out into different Trackpoint readers if other formats need to be supported. It
 * is a simple implementation right now.
 * 
 * 
 * FIXME:This is not thread safe. Only one iteration of track is supported at a time.
 * 
 * WARNING: This is limited to data with 10 MB when calling iteration multiple times
 * 
 * @author aroller
 * 
 */
public class CsvActivityDocument
		implements Document, TrackContainer, ActivityDocument, Serializable {

	/**
	 * 
	 */
	private static final String VERSION = "v1.0";
	/**
	 * 
	 */
	private static final char SEPARATOR_CHAR = ',';
	private static final long serialVersionUID = -1035011587629791437L;
	static final int DURATION_OFFSET = 0;
	static final int LATITUDE = 1;
	static final int LONGITUDE = 2;
	static final int ELEVATION = 3;
	static final int REQUIRED_FIELD_COUNT = 4;
	private transient LineNumberReader reader;
	private DateTime startTime;
	private String activityId;
	private String userId;
	private String activityType;
	private String version;
	private String applicationKey;
	private Application application;
	private String device;
	private String deviceManufacturer;
	/** The time the application received the activity */
	private DateTime uploadTime;
	private byte[] bytes;
	/** used to enforce concurrency control. */
	private Iterator<Trackpoint> currentlySupportedIterator;

	/**
	 * Use {@link CsvActivityDocument#create(byte[])} methods.
	 */
	public static class Builder
			extends ObjectBuilder<CsvActivityDocument> {

		private Builder() {
			super(new CsvActivityDocument());
		}

		private Builder setContents(String contents) throws DocumentDecodingException,
				DocumentContentsNotRecognizedException, UnknownApplicationException {
			building.reader = new LineNumberReader(new StringReader(contents));
			building.readHeader();
			return this;
		}

		private Builder setInputStream(InputStream is) throws DocumentDecodingException,
				DocumentContentsNotRecognizedException, UnknownApplicationException {
			building.reader = new LineNumberReader(new InputStreamReader(is));
			building.readHeader();
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public CsvActivityDocument build() {
			Assertion.assertNotNull(building.reader);

			return super.build();
		}

		/**
		 * @param dataInput
		 * @throws DocumentDecodingException
		 * @throws DocumentContentsNotRecognizedException
		 */
		private Builder setBytes(byte[] dataInput) {
			building.bytes = dataInput;
			return this;

		}
	}// end Builder

	public static Builder create(String contents) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, UnknownApplicationException {
		return new Builder().setContents(contents);
	}

	public static Builder create(InputStream is) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, UnknownApplicationException {
		return new Builder().setInputStream(is);
	}

	public static Builder create(byte[] bytes) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, UnknownApplicationException {
		Builder builder = new Builder();
		builder.setBytes(bytes);
		return builder.setInputStream(new ByteArrayInputStream(bytes));
	}

	/** Use {@link Builder} to construct CsvDocument */
	private CsvActivityDocument() {
	}

	/**
	 * @param contents
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws UnknownApplicationException
	 */
	private void readHeader() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {

		try {
			this.version = readHeaderLine(new DocumentContentsNotRecognizedException());
		} catch (DocumentDecodingException e1) {
			// can't get the first line...this must not be a csv doc
			throw new DocumentContentsNotRecognizedException();
		}
		If.notEqual(this.version, VERSION).raise(DocumentDecodingException.unexpectedValue(VERSION, this.version));

		this.applicationKey = readHeaderLine(new DocumentContentsNotRecognizedException());
		this.application = KnownApplication.byKey(new ApplicationKey(applicationKey));
		this.activityId = readHeaderLine(ActivityDocumentDecodingException.missingActivityId());
		this.userId = readHeaderLine(new UserNameNotProvidedException());
		this.activityType = readHeaderLine(ActivityDocumentDecodingException.missingActivityType());
		this.uploadTime = readTimestampLine();
		this.deviceManufacturer = readHeaderLine(new DocumentContentsNotRecognizedException());
		this.device = readHeaderLine(new DocumentContentsNotRecognizedException());
		this.startTime = readTimestampLine();

		// mark the end of the header to support multiple reads.
		if (reader.markSupported()) {
			try {
				// FIXME:This should use
				DataAmount max = QuantityFactory.getInstance(DataAmount.class)
						.create(10, MetricSystem.MEGA(USCustomarySystem.BYTE));
				reader.mark((int) max.doubleValue(USCustomarySystem.BYTE));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			throw new UnsupportedOperationException("mark is not supported for " + this.reader);
		}
	}

	/**
	 * @param start
	 * @return
	 * @throws DocumentDecodingException
	 * @throws UserNameNotProvidedException
	 */
	private DateTime readTimestampLine() throws DocumentDecodingException, UserNameNotProvidedException {
		// time
		String startTimeString = readHeaderLine(ActivityDocumentDecodingException.missingTimestamp());
		If.nil(startTimeString).raise(new UserNameNotProvidedException());

		try {
			return ISODateTimeFormat.dateTimeParser().parseDateTime(startTimeString);
		} catch (IllegalArgumentException e) {
			// normally don't catch runtimes, but they provide no alternative.
			throw ActivityDocumentDecodingException.invalidTimestamp(startTimeString);
		}
	}

	/**
	 * @param notRecognized
	 * @return
	 * @throws IOException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 */
	private String readHeaderLine(DocumentDecodingException notRecognized) throws DocumentDecodingException {
		String line;
		try {
			line = readLine();
			If.tru(StringUtils.isEmpty(line)).raise(notRecognized);
		} catch (IOException e) {
			throw notRecognized;
		}

		String trimHeaderLine = trimHeaderLine(line);
		If.tru(StringUtils.isEmpty(trimHeaderLine)).raise(notRecognized);
		return trimHeaderLine;
	}

	/**
	 * The only way the reader should be read since this may build the bytes.
	 * 
	 * @return
	 * @throws IOException
	 */
	private String readLine() throws IOException {
		String line = reader.readLine();

		return line;
	}

	/**
	 * @param line
	 * @return
	 * @throws DocumentDecodingException
	 */
	private String trimHeaderLine(String line) throws DocumentDecodingException {

		String[] split = StringUtils.splitPreserveAllTokens(line, SEPARATOR_CHAR);
		int actualColumns = split.length;

		If.notEqual(REQUIRED_FIELD_COUNT, actualColumns)
				.raise(ActivityDocumentDecodingException.invalidFieldCount(line, REQUIRED_FIELD_COUNT, actualColumns));
		return split[0];
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.ACTIVITY_CSV;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.TrackContainer#getTrack()
	 */
	@Override
	public Track getTrack() {

		// else iterate through the points and create the track.
		return new Track() {
			private static final long serialVersionUID = 2854499736072778720L;

			/**
			 * Iterates through each of hte lines of the document. This does not support concurrent
			 * access so only one iterator at a time!
			 * 
			 */
			@Override
			public Iterator<Trackpoint> iterator() throws ConcurrentModificationException {

				try {
					// rewind back to the header so we can read the track again
					reader.reset();
				} catch (IOException e1) {
					throw new RuntimeException(e1);
				}

				// only one iterator at a time!
				currentlySupportedIterator = new Iterator<Trackpoint>() {
					private Trackpoint next;
					private boolean moreRemaining = true;

					@Override
					public void remove() {
						throw new UnsupportedOperationException("we don't go back in time here");

					}

					@Override
					public Trackpoint next() {

						// in case they call next without hasNext.
						if (next == null) {
							if (moreRemaining) {
								readNext();
							}
							// if next is still null at this point you shouldn't be calling it
							// without hasNext first.
							if (next == null) {
								throw new NoSuchElementException("You should call hasNext before calling this.");
							}
						}
						Trackpoint result = next;
						// must null out to indicate the need to read another
						next = null;
						return result;

					}

					@Override
					public boolean hasNext() {

						return next != null || readNext() != null;

					}

					/**
					 * reads the next line ignoring empty lines until a line is found or null.
					 * 
					 * @return
					 * @throws IOException
					 * 
					 */
					private Trackpoint readNext() throws ConcurrentModificationException {
						if (currentlySupportedIterator != this) {
							throw new ConcurrentModificationException(
									"this class supports only a single iteration at a time, currently suppporting "
											+ currentlySupportedIterator);
						}
						try {
							while (next == null && moreRemaining) {
								String line = readLine();
								if (line != null) {
									String[] parts = StringUtils.splitPreserveAllTokens(line, SEPARATOR_CHAR);

									// validate parts size here

									try {
										next = TrackUtil.createPointAllowNulls(	startTime,
																				parts[DURATION_OFFSET],
																				parts[LATITUDE],
																				parts[LONGITUDE],
																				parts[ELEVATION]);
										// next may be null which means we have nothing useful
									} catch (IndexOutOfBoundsException e) {
										// catching a runtime here is more efficient than count
										// check?
										throw ActivityDocumentDecodingException
												.invalidFieldCountRuntime(line, REQUIRED_FIELD_COUNT, parts.length);

									}
								} else {
									moreRemaining = false;
								}
							}
							return next;
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					}
				};
				return currentlySupportedIterator;

			}
		};
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {

		return bytes;
	}

	/**
	 * @return the activityId
	 */
	@Override
	public String getActivityId() {
		return this.activityId;
	}

	/**
	 * @return the userId
	 */
	@Override
	public String getUserId() {
		return this.userId;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getAccount()
	 */
	@Override
	public Account getAccount() {
		// no aliases to add.
		return new Account.Builder().setApplicationKey(getApplication().getKey()).setRemoteId(getUserId()).build();
	}

	@Override
	public String getActivityType() {
		return this.activityType;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return this.version;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityName()
	 */
	@Override
	public @Nullable
	String getActivityName() {
		// activity name is not yet provided in this document
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getHistory()
	 */
	@Override
	public IdentityHistory getHistory() {

		Instance instance = Instance.createInstance().setApplication(application).build();
		Installation installation = Installation.create().setExternalId(getActivityId()).setInstance(instance).build();
		IdentityHistory history = IdentityHistory.create().add(installation).build();
		return history;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getDescription()
	 */
	@Override
	public String getDescription() {

		return null;
	}

	/**
	 * @return the application
	 */
	public Application getApplication() {
		return this.application;
	}

	/**
	 * @return the device
	 */
	public String getDevice() {
		return this.device;
	}

	/**
	 * @return the deviceManufacturer
	 */
	public String getDeviceManufacturer() {
		return this.deviceManufacturer;
	}

	/**
	 * @return the startTime
	 */
	public DateTime getStartTime() {
		return this.startTime;
	}

	/**
	 * @return the uploadTime
	 */
	public DateTime getUploadTime() {
		return this.uploadTime;
	}

	/**
	 * @see Serializable
	 * @param in
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		if (this.bytes != null) {
			this.reader = new LineNumberReader(new InputStreamReader(new ByteArrayInputStream(this.bytes)));
			try {
				this.readHeader();
			} catch (DocumentContentsNotRecognizedException e) {
				throw new RuntimeException(e);
			} catch (DocumentDecodingException e) {
				throw new RuntimeException(e);
			} catch (UnknownApplicationException e) {
				throw new RuntimeException(e);
			}

		} else {
			throw new IllegalStateException("unable to create reader since bytes are null");
		}
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getDate()
	 */
	@Override
	public LocalDate getDate() {
		LoggerFactory.getLogger(getClass()).warning("date not implemented, but it could be");
		return null;
	}
}
