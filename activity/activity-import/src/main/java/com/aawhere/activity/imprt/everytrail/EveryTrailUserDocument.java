/**
 * 
 */
package com.aawhere.activity.imprt.everytrail;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.aawhere.app.AccountDocument;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.lang.If;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.collect.Lists;

/**
 * A Custom XML Reader specifically for EveryTrail.
 * 
 * @author aroller
 * 
 */
public class EveryTrailUserDocument
		implements Document, AccountDocument {

	private static final long serialVersionUID = 9164173803777621470L;
	private byte[] bytes;
	private String username;
	private String userId;
	private String name;

	/**
	 * Used to construct all instances of EveryTrailActivityDocument.
	 */
	public static class Builder
			extends ObjectBuilder<EveryTrailUserDocument> {

		final String ID_ELEMENT_NAME = "id";
		private org.w3c.dom.Document document;

		public Builder() {
			super(new EveryTrailUserDocument());
		}

		public Builder contents(byte[] bytes) throws SAXException, IOException, ParserConfigurationException {
			building.bytes = bytes;
			ByteArrayInputStream contents = new ByteArrayInputStream(bytes);
			this.document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(contents);
			Element response = this.document.getDocumentElement();
			Element userElement = (Element) response.getElementsByTagName("user").item(0);
			building.userId = userElement.getAttribute("id");
			Element usernameElement = (Element) userElement.getElementsByTagName("username").item(0);
			building.username = usernameElement.getTextContent();

			Element firstNameElement = (Element) userElement.getElementsByTagName("firstName").item(0);
			String firstName = If.nil(firstNameElement.getTextContent()).use(null);
			Element lastNameElement = (Element) userElement.getElementsByTagName("lastName").item(0);
			String lastName = If.nil(lastNameElement.getTextContent()).use(null);
			if (StringUtils.isNotEmpty(firstName) && StringUtils.isNotEmpty(lastName)) {
				building.name = StringUtils.join(Lists.newArrayList(firstName, lastName), " ");
			} else if (StringUtils.isNotEmpty(firstName)) {
				building.name = firstName;
			} else if (StringUtils.isNotEmpty(lastName)) {
				building.name = lastName;
			}// else leave as null

			return this;
		}

		@Override
		public EveryTrailUserDocument build() {
			EveryTrailUserDocument built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct EveryTrailActivityDocument */
	private EveryTrailUserDocument() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.AccountDocument#username()
	 */
	@Override
	public String username() {
		return this.username;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.AccountDocument#name()
	 */
	@Override
	public String name() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.AccountDocument#userId()
	 */
	@Override
	public String userId() {
		return this.userId;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		return this.bytes;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.ETUX;
	}

	public static class Producer
			implements DocumentProducer {

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
		 */
		@Override
		public Document produce(Object dataInput) throws DocumentDecodingException,
				DocumentContentsNotRecognizedException {
			if (dataInput instanceof byte[]) {
				byte[] bytes = (byte[]) dataInput;
				try {
					return EveryTrailUserDocument.create().contents(bytes).build();
				} catch (SAXException | IOException | ParserConfigurationException e) {

					throw new DocumentContentsNotRecognizedException();
				}
			} else {
				throw new DocumentContentsNotRecognizedException();
			}
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
		 */
		@Override
		public DocumentFormatKey[] getDocumentFormatsSupported() {
			return new DocumentFormatKey[] { DocumentFormatKey.ETUX };
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
		 */
		@Override
		public DiscoveryOrder getDiscoveryOrder() {
			return DiscoveryOrder.LAST;
		}
	}

	public static Producer producer() {
		return new Producer();
	}
}
