package com.aawhere.activity.imprt.mmf;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/** Reference in a workout to a route. */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteLink {
	private String href;
	private String id;

	public String getHref() {
		return this.href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
