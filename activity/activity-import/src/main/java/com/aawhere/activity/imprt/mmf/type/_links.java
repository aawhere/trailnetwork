package com.aawhere.activity.imprt.mmf.type;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class _links {
	private List<Icon_url> icon_url;
	private List<Root> root;
	private List<Self> self;
	private List<Parent> parent;

	public List<Parent> getParent() {
		return this.parent;
	}

	public void setParent(List<Parent> parent) {
		this.parent = parent;
	}

	public List<Icon_url> getIcon_url() {
		return this.icon_url;
	}

	public void setIcon_url(List<Icon_url> icon_url) {
		this.icon_url = icon_url;
	}

	public List<Root> getRoot() {
		return this.root;
	}

	public void setRoot(List<Root> root) {
		this.root = root;
	}

	public List<Self> getSelf() {
		return this.self;
	}

	public void setSelf(List<Self> self) {
		this.self = self;
	}
}
