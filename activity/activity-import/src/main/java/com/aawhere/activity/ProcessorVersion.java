/**
 * 
 */
package com.aawhere.activity;

/**
 * Indicates that this represents a {@link ProcessorVersion}.
 * 
 * @author Brian Chapman
 * 
 */
public interface ProcessorVersion {

}
