package com.aawhere.activity.imprt.mmf.type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Activity_types {
	private _links _links;
	private boolean has_children;
	private Number mets;
	private String name;
	private String short_name;

	public _links get_links() {
		return this._links;
	}

	public void set_links(_links _links) {
		this._links = _links;
	}

	public boolean getHas_children() {
		return this.has_children;
	}

	public void setHas_children(boolean has_children) {
		this.has_children = has_children;
	}

	public Number getMets() {
		return this.mets;
	}

	public void setMets(Number mets) {
		this.mets = mets;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShort_name() {
		return this.short_name;
	}

	public void setShort_name(String short_name) {
		this.short_name = short_name;
	}
}
