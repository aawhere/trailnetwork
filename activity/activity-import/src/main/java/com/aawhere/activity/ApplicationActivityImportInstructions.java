/**
 * 
 */
package com.aawhere.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.app.Application;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountField;
import com.aawhere.app.account.AccountWebApiUri;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.retrieve.ModifiableDocumentRetriever;
import com.aawhere.retrieve.WebDocumentRetriever;
import com.aawhere.xml.XmlNamespace;
import com.google.api.client.http.HttpHeaders;

/**
 * Used by ActivityImportService to retrieve activities from the remote application using the
 * instructions provided.
 * 
 * TODO: this will become an Entity without a doubt.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class ApplicationActivityImportInstructions {

	/**
	 * If provided, this will override the default activity type for the Application. Handy when a
	 * site has only one type of activity.
	 * 
	 */
	public static final String ACTIVITY_TYPE_DEFAULT_KEY = "*";
	static final ActivityType ACTIVITY_TYPE_DEFAULT = ActivityType.OTHER;

	/**
	 * Used to construct all instances of ApplicationActivityImportInstructions.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<ApplicationActivityImportInstructions> {

		public Builder() {
			super(new ApplicationActivityImportInstructions());
		}

		public Builder addActivityUrl(String url, DocumentFormatKey documentFormat) {
			building.activityUrls.add(new Destination(url, documentFormat));
			return this;
		}

		public Builder addActivityUrl(String url, DocumentFormatKey documentFormat, HttpHeaders headers) {
			building.activityUrls.add(new Destination(url, documentFormat, headers));
			return this;
		}

		public Builder setApplication(Application application) {
			building.application = application;
			return this;
		}

		public Builder addActivityTypeMapping(String remoteKey, ActivityType ours) {
			building.activityTypes.put(remoteKey, ours);
			return this;
		}

		public Builder setCourseCompletedCallbackUrl(String courseCompletedCallbackUrl) {
			building.courseCompletedCallbackUrl = courseCompletedCallbackUrl;
			return this;
		}

		public Builder setModifiableDocuments(Boolean modifiable) {
			building.modifiableDocuments = modifiable;
			return this;
		}

		public Builder profilePage(String profilePage) {
			building.profilePage = profilePage;
			return this;
		}

		public Builder webUrl(String webUrl) {
			building.webUrl = webUrl;
			return this;
		}

		public Builder setConnectionTimeoutInMillis(Integer millis) {
			building.connectionTimeoutInMillis = millis;
			return this;
		}

		public Builder setAccountDestination(Destination accountDestination) {
			building.accountDestination = accountDestination;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.exceptions().notNull("application", building.application);

			super.validate();
		}

		public Builder setSearchDestination(Destination destination) {
			building.searchDestination = destination;
			return this;
		}

		public Builder setActivitySearchForAccountDestination(Destination destination) {
			building.activitySearchForAccountDestination = destination;
			return this;
		}

		/**
		 * @param knownActivityTypes
		 */
		public void setActivityTypeMapping(Map<String, ActivityType> knownActivityTypes) {
			building.activityTypes = knownActivityTypes;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ApplicationActivityImportInstructions build() {
			final ApplicationActivityImportInstructions built = super.build();
			// use the given default if given, otherwise use other
			built.activityTypeDefault = If.nil(built.activityTypes.get(ACTIVITY_TYPE_DEFAULT_KEY))
					.use(ACTIVITY_TYPE_DEFAULT);
			return built;

		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Set of complete activity urls that have a single variable named activityId enclosed in
	 * braces. The {activityId} variable will be replaced with the the value of the activity being
	 * requested for each url.
	 * 
	 * {@link DocumentFormatKey} is the format of the document at the url.
	 */
	@XmlElement
	private List<Destination> activityUrls = new ArrayList<Destination>();

	/** The application for which these instructions describe. */
	@XmlElement
	private Application application;

	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Map<String, ActivityType> activityTypes = new HashMap<String, ActivityType>();

	/**
	 * The default activity type if the given is not recognized. Use
	 * {@link #ACTIVITY_TYPE_DEFAULT_KEY} to override the default of other.
	 * 
	 */
	private ActivityType activityTypeDefault;
	/**
	 * The destination on the web for humans to view the activity.
	 */
	@XmlElement
	private String webUrl;

	/**
	 * A URL that is the destination at the {@link Application} that describes and {@link Account}.
	 * Some providers use the username, others use the account id. TN-675
	 */
	@XmlElement
	private String profilePage;

	/**
	 * Provides the destination to ping with the results of discovering a timing match and course
	 * completed.
	 */
	@XmlElement
	private String courseCompletedCallbackUrl;

	/**
	 * Indicates the documents at the remote destination have the ability to be updated. The
	 * retriever must implement {@link ModifiableDocumentRetriever} compatible with the remote
	 * server's protocol.
	 */
	@XmlAttribute
	private Boolean modifiableDocuments = Boolean.FALSE;

	/** Provides the ability to customize a connection timeout for a particular application. */
	@XmlAttribute
	private Integer connectionTimeoutInMillis = WebDocumentRetriever.DEFAULT_CONNECTION_TIMEOUT;

	/**
	 * Information about searching an Application for activities.
	 * 
	 */
	@XmlElement
	private Destination searchDestination;

	/**
	 * Used to paginate activities for an account similar to that of {@link #searchDestination()}.
	 */
	@XmlElement
	private Destination activitySearchForAccountDestination;

	/** Information about the user. */
	@XmlElement
	private Destination accountDestination;

	/** Use {@link Builder} to construct ApplicationActivityImportInstructions */
	private ApplicationActivityImportInstructions() {
	}

	/**
	 * @return the activityUrl
	 */
	public List<Destination> getActivityUrls() {
		return this.activityUrls;
	}

	/**
	 * @return the profilePage
	 */
	public String getProfilePage() {
		return this.profilePage;
	}

	/**
	 * @return the webUrl
	 */
	public String getWebUrl() {
		return this.webUrl;
	}

	/**
	 * @return the application
	 */
	public Application getApplication() {
		return this.application;
	}

	/**
	 * Given a general string representing an activity type this will return the system type that is
	 * the best match.
	 * 
	 * @param applicationActivityType
	 *            their representation
	 * @return our representation or
	 */
	public ActivityType activityType(String applicationActivityType) {
		return If.nil(this.activityTypes.get(applicationActivityType)).use(activityTypeDefault);
	}

	/**
	 * @return the courseCompletedCallbackUrl
	 */
	@Nullable
	public String getCourseCompletedCallbackUrl() {
		return this.courseCompletedCallbackUrl;
	}

	/**
	 * Simple null check to see if APPLICATION_REFERENCE is provided indicated desire to be
	 * notified.
	 * 
	 * @return
	 */
	public Boolean courseCompletedCallbackDesired() {
		return this.courseCompletedCallbackUrl != null;
	}

	/**
	 * @return the webUrl
	 */
	public String webUrl() {
		return this.webUrl;
	}

	/**
	 * @return the connectionTimeoutInMillis
	 */
	public Integer getConnectionTimeoutInMillis() {
		return this.connectionTimeoutInMillis;
	}

	/**
	 * @see #modifiableDocuments
	 * @return the modifiableDocuments
	 */
	public Boolean hasModifiableDocuments() {
		return this.modifiableDocuments;
	}

	/**
	 * Provides information about how to find activities related to the provided input.
	 * 
	 * @return the searchDestination
	 */
	public Destination searchDestination() {
		return this.searchDestination;
	}

	/**
	 * @return the activitySearchForAccountDestination
	 */
	public Destination activitySearchForAccountDestination() {
		return this.activitySearchForAccountDestination;
	}

	/**
	 * @return the accountDestination
	 */
	public Destination accountDestination() {
		return this.accountDestination;
	}

	/**
	 * A destination where an Activity or part of an activity is stored. This provides the
	 * information necessary to retrieve the document from the destination.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	public static class Destination {
		/**
		 * required for xml
		 */
		Destination() {
			this(null, null);
		}

		/**
		 * 
		 */
		public Destination(String url, DocumentFormatKey format) {
			this(url, format, new HttpHeaders());
		}

		/**
		 * 
		 */
		public Destination(String url, DocumentFormatKey format, HttpHeaders headers) {
			this.url = url;
			this.format = format;
			this.headers = headers;
		}

		public final String url;
		public final DocumentFormatKey format;
		public final HttpHeaders headers;

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return format + url;
		}
	}

	public static class FIELD {
		public static final String ACTIVITY_ID = ActivityWebApiUri.ACTIVITY_ID;
		public static final String CENTER_LAT = "centerLat";
		public static final String CENTER_LON = "centerLon";
		public static final String PROXIMITY_IN_MILES = "proximityInMiles";
		public static final String FILTER_START = "start";
		public static final String FILTER_PAGE = "page";
		public static final String MIN_LON = "minLon";
		public static final String MIN_LAT = "minLat";
		public static final String MAX_LON = "maxLon";
		public static final String MAX_LAT = "maxLat";
		public static final String ACCOUNT_REMOTE_ID = "accountRemoteId";
		public static final String ACCOUNT_ID = AccountField.Absolute.ID;
		public static final String ACCOUNT_ALIAS = AccountWebApiUri.ACCOUNT_ALIAS;

		/**
		 * Used to filter happening after the given timestamp. Typically means start time, but could
		 * be other related information like upload time if available and preferred for provider.
		 * 
		 */
		public static final String SINCE_TIMESTAMP = "sinceTimestamp";
		/**
		 * Equal to {@link #SINCE_TIMESTAMP},but the unit is in days since now.
		 */
		public static final String SINCE_DAYS = "sinceDays";

		public static class PARAM {
			private static final String O = "{";
			private static final String C = "}";

			/**
			 * A template for where the activity Id should appear in urls, etc.
			 */
			public static final String ACTIVITY_ID = ActivityWebApiUri.ACTIVITY_ID_PARAM;
			public static final String PROXIMITY_IN_MILES = O + FIELD.PROXIMITY_IN_MILES + C;
			public static final String FILTER_START = O + FIELD.FILTER_START + C;
			public static final String FILTER_PAGE = O + FIELD.FILTER_PAGE + C;
			public static final String CENTER_LAT = O + FIELD.CENTER_LAT + C;
			public static final String CENTER_LON = O + FIELD.CENTER_LON + C;
			public static final String MIN_LON = O + FIELD.MIN_LON + C;
			public static final String MIN_LAT = O + FIELD.MIN_LAT + C;
			public static final String MAX_LON = O + FIELD.MAX_LON + C;
			public static final String MAX_LAT = O + FIELD.MAX_LAT + C;
			public static final String ACCOUNT_REMOTE_ID = O + FIELD.ACCOUNT_REMOTE_ID + C;
			public static final String ACCOUNT_ALIAS = AccountWebApiUri.ACCOUNT_ALIAS_PARAM;
			public static final String SINCE_TIMESTAMP = O + FIELD.SINCE_TIMESTAMP + C;
			public static final String SINCE_DAYS_AGO = O + FIELD.SINCE_DAYS + C;
			public static final String ACCOUNT_ID = O + FIELD.ACCOUNT_ID + C;

		}

	}
}
