/**
 * 
 */
package com.aawhere.activity;

import java.util.Set;

import org.joda.time.LocalDate;

import com.aawhere.app.Application;
import com.aawhere.app.account.AccountId;
import com.aawhere.doc.Document;

import com.google.common.collect.Multimap;

/**
 * Used to Query external {@link Application}s expecting a list of activity ids and account ids.
 * Some applications provide activities that may not be suitable for the TrailNetwork, however, the
 * account ids may still be worth knowing. In such a
 * 
 * @author aroller
 * 
 */
public interface ActivitySearchDocument
		extends Document {

	/**
	 * The resulting ids from the search request to an external application.
	 * 
	 * @return
	 */
	public Set<ActivityId> activityIds();

	/**
	 * Provides all the accounts and any activity ids associated to each. Fulfillment of
	 * {@link #activityIds()} is new HashMap({@link Multimap#values()}).
	 * 
	 * @return
	 */
	public Multimap<AccountId, ActivityId> ids();

	/**
	 * 
	 * @return true when it is certain no more will be discovered.
	 */
	public Boolean finalPage();

	/**
	 * {@link LocalDate} of the most recent found, or null if none are found.
	 * 
	 * @return the most recent or null if none found.
	 */
	public LocalDate mostRecentStartDate();
}
