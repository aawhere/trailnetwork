/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author aroller
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Trackpoint
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6757187435333778630L;
	public final Double ele;
	public final Double lon;
	public final Double lat;
	public final long offset;

	/**
	 * 
	 */
	@JsonCreator
	public Trackpoint(List<Object> trackpointParts) {
		Double timeOffset = (Double) trackpointParts.get(0);
		this.offset = (long) (timeOffset * 1000);
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (Map<String, Double>) trackpointParts.get(1);
		this.lat = map.get("lat");
		this.lon = map.get("lng");
		this.ele = map.get("elevation");
	}
}
