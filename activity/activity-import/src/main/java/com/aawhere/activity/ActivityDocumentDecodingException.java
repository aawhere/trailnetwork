/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;

/**
 * @author aroller
 * 
 */
public class ActivityDocumentDecodingException
		extends DocumentDecodingException {

	private static final long serialVersionUID = -2772762055563093889L;

	/**
	 * 
	 */
	public ActivityDocumentDecodingException() {
		super();
	}

	/**
	 * @param message
	 */
	public ActivityDocumentDecodingException(Message message) {
		super(message);
	}

	public ActivityDocumentDecodingException(CompleteMessage message) {
		super(message);
	}

	public static DocumentDecodingException missingTimestamp() {
		return new ActivityDocumentDecodingException(ActivityImportMessage.MISSING_TIMESTAMP_FIELD);
	}

	public static DocumentDecodingException invalidTimestamp(String timestamp) {
		return new ActivityDocumentDecodingException(new CompleteMessage.Builder(
				ActivityImportMessage.INVALID_TIMESTAMP_VALUE).addParameter(ActivityImportMessage.Param.VALUE,
																			timestamp).build());
	}

	public static DocumentDecodingException missingActivityId() {
		return new ActivityDocumentDecodingException(ActivityImportMessage.MISSING_ACTIVITY_ID_FIELD);

	}

	public static DocumentDecodingException missingActivityType() {
		return new ActivityDocumentDecodingException(ActivityImportMessage.MISSING_ACTIVITY_TYPE_FIELD);
	}

	public static DocumentDecodingException missingUserId() {
		return new UserNameNotProvidedException();

	}

	public static DocumentDecodingException invalidFieldCount(Object value, Integer expected, Integer actual) {
		return new ActivityDocumentDecodingException(new CompleteMessage.Builder(
				ActivityImportMessage.INVALID_FIELD_COUNT).addParameter(ActivityImportMessage.Param.VALUE, value)
				.addParameter(ActivityImportMessage.Param.EXPECTED, expected)
				.addParameter(ActivityImportMessage.Param.UNEXPECTED, actual).build());
	}

	public static BaseRuntimeException invalidFieldCountRuntime(Object value, Integer expected, Integer actual) {
		return new InvalidArgumentException(invalidFieldCount(value, expected, actual));
	}
}
