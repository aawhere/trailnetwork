/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.io.InputStream;

import com.aawhere.activity.ActivityDocument;
import com.aawhere.app.AccountDocument;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * Produces Workout {@link ActivityDocument}s when given the {@link MapMyFitnessWorkoutJson}.
 * 
 * @see #workout()
 * @see #workouts()
 * 
 * @author aroller
 * 
 */
public abstract class MapMyFitnessDocumentProducer<J>
		implements DocumentProducer {
	private MmfJsonReader<J> reader;

	/**
	 * Used to construct all instances of MapMyFitnessDocumentProducer.
	 */
	public static class Builder<J>
			extends ObjectBuilder<MapMyFitnessDocumentProducer<J>> {

		public Builder(MapMyFitnessDocumentProducer<J> producer) {
			super(producer);
		}

		public Builder<J> formats(DocumentFormatKey... documentFormatKeys) {
			building.formats = documentFormatKeys;
			return this;
		}

		public Builder<J> jsonType(Class<J> jsonType) {
			building.jsonType = jsonType;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("formats", building.formats);
			Assertion.exceptions().notNull("jsonType", building.jsonType);

		}

		@Override
		public MapMyFitnessDocumentProducer<J> build() {
			MapMyFitnessDocumentProducer<J> built = super.build();
			com.aawhere.activity.imprt.mmf.MmfJsonReader.Builder<J> readerBuilder = MmfJsonReader.create();
			built.reader = readerBuilder.type(built.jsonType).build();
			return built;
		}

	}// end Builder

	private Class<J> jsonType;
	private DocumentFormatKey[] formats;

	/** Use {@link Builder} to construct MapMyFitnessDocumentProducer */
	private MapMyFitnessDocumentProducer() {

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	public Document produce(Object dataInput) throws DocumentDecodingException, DocumentContentsNotRecognizedException {
		J workout;
		if (dataInput instanceof byte[]) {
			workout = reader.read((byte[]) dataInput);
		} else if (dataInput instanceof InputStream) {
			workout = reader.read((InputStream) dataInput);
		} else if (dataInput instanceof String) {
			workout = reader.read((String) dataInput);
		} else {
			throw new DocumentContentsNotRecognizedException();
		}
		return document(workout);

	}

	/**
	 * @param workout
	 * @return
	 */
	abstract Document document(J item);

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	public DocumentFormatKey[] getDocumentFormatsSupported() {
		return this.formats;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {
		return DiscoveryOrder.MEDIUM;
	}

	public static MapMyFitnessDocumentProducer<Workout> workout(
			final DeviceCapabilitiesRepository deviceCapabilitiesRepository) {
		MapMyFitnessDocumentProducer<Workout> producer = new MapMyFitnessDocumentProducer<Workout>() {

			@Override
			Document document(Workout item) {
				return MapMyFitnessWorkoutJson.create().workout(item)
						.deviceCapabilitiesRepository(deviceCapabilitiesRepository).build();
			}

		};
		Builder<Workout> builder = new Builder<>(producer);
		builder.formats(DocumentFormatKey.MMFW7);
		builder.jsonType(Workout.class);
		return builder.build();
	}

	public static MapMyFitnessDocumentProducer<WorkoutsResults> workouts() {
		MapMyFitnessDocumentProducer<WorkoutsResults> producer = new MapMyFitnessDocumentProducer<WorkoutsResults>() {

			@Override
			Document document(WorkoutsResults item) {
				return MapMyFitnessWorkoutsJsonDocument.create().workouts(item).build();
			}

		};
		Builder<WorkoutsResults> builder = new Builder<>(producer);
		builder.formats(DocumentFormatKey.MMFUW7);
		builder.jsonType(WorkoutsResults.class);
		return builder.build();
	}

	public static MapMyFitnessDocumentProducer<RoutesResultsMmfJson> routes() {
		MapMyFitnessDocumentProducer<RoutesResultsMmfJson> producer = new MapMyFitnessDocumentProducer<RoutesResultsMmfJson>() {

			@Override
			Document document(RoutesResultsMmfJson item) {
				return MapMyFitnessWorkoutsJsonDocument.create().workouts(item).build();
			}

		};
		Builder<RoutesResultsMmfJson> builder = new Builder<>(producer);
		builder.formats(DocumentFormatKey.MMFR7);
		builder.jsonType(RoutesResultsMmfJson.class);
		return builder.build();
	}

	public static MapMyFitnessDocumentProducer<User> user() {
		MapMyFitnessDocumentProducer<User> producer = new MapMyFitnessDocumentProducer<User>() {

			@Override
			Document document(final User item) {
				return new AccountDocument() {

					@Override
					public byte[] toByteArray() {
						throw new UnsupportedOperationException();
					}

					@Override
					public DocumentFormatKey getDocumentFormat() {
						return DocumentFormatKey.MMFU7;
					}

					@Override
					public String username() {
						return item.getUsername();
					}

					@Override
					public String userId() {
						return item.getId();
					}

					@Override
					public String name() {
						return item.getDisplayName();
					}
				};

			}

		};

		Builder<User> builder = new Builder<>(producer);
		builder.formats(DocumentFormatKey.MMFU7);
		builder.jsonType(User.class);
		return builder.build();
	}

	public static DocumentFactory.Builder registerAll(DocumentFactory.Builder factoryBuilder,
			DeviceCapabilitiesRepository deviceCapabilitiesRepository) {
		return factoryBuilder.register(user()).register(workout(deviceCapabilitiesRepository)).register(workouts())
				.register(routes());
	}
}
