package com.aawhere.activity.imprt.mmf;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Aggregates {
	private Number active_time_total;
	private Number distance_total;
	private Number elapsed_time_total;
	private Number metabolic_energy_total;
	private Number speed_avg;
	private Number speed_max;
	private Number steps_total;

	public Number getActive_time_total() {
		return this.active_time_total;
	}

	public void setActive_time_total(Number active_time_total) {
		this.active_time_total = active_time_total;
	}

	public Number getDistance_total() {
		return this.distance_total;
	}

	public void setDistance_total(Number distance_total) {
		this.distance_total = distance_total;
	}

	public Number getElapsed_time_total() {
		return this.elapsed_time_total;
	}

	public void setElapsed_time_total(Number elapsed_time_total) {
		this.elapsed_time_total = elapsed_time_total;
	}

	public Number getMetabolic_energy_total() {
		return this.metabolic_energy_total;
	}

	public void setMetabolic_energy_total(Number metabolic_energy_total) {
		this.metabolic_energy_total = metabolic_energy_total;
	}

	public Number getSpeed_avg() {
		return this.speed_avg;
	}

	public void setSpeed_avg(Number speed_avg) {
		this.speed_avg = speed_avg;
	}

	public Number getSpeed_max() {
		return this.speed_max;
	}

	public void setSpeed_max(Number speed_max) {
		this.speed_max = speed_max;
	}

	public Number getSteps_total() {
		return this.steps_total;
	}

	public void setSteps_total(Number steps_total) {
		this.steps_total = steps_total;
	}
}
