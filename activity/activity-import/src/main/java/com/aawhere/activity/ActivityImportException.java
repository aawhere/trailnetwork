/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.exception.MessageException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates a failure to import for a variety of reasons indicated by calling the proper builder
 * method.
 * 
 * The status code associated with this is {@link HttpStatusCode#ACCEPTED} which is not intuitive,
 * but persistent users (like a Queue) need to be told that we've done all we can to import this
 * activity so subsequent requests won't satisfy.
 * 
 * The status code of Accepted is important since it is 202 which is required for queues to stop
 * attempting to re-process.
 * 
 * 
 * Extension of {@link EntityNotFoundException} indicates essentially that...we can't find it since
 * we can't import it. Since most callers of activity can't handle {@link ActivityImportException}
 * this allows them to handle them together, but also allows them to handle it directly if they do
 * care.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.ACCEPTED)
public class ActivityImportException
		extends EntityNotFoundException {

	private static final long serialVersionUID = -4283301341906292403L;

	/**
	 * Used to construct all instances of ActivityImportException.
	 * 
	 * Not a typical builder since Exceptions require full content upon construction.
	 */
	public static class Builder {

		private Throwable cause;
		private ActivityId id;
		private CompleteMessage.Builder messageBuilder = new CompleteMessage.Builder();
		private CompleteMessage message;

		public Builder() {

		}

		public Builder setCause(Throwable cause) {
			if (cause instanceof MessageException) {
				this.message = ((MessageException) cause).getCompleteMessage();
			}
			this.cause = cause;
			return this;
		}

		public ActivityImportException build() {
			Assertion.assertNotNull(this.id);
			// some causes provide a message. Use that first, otherwise look to general.
			if (this.message == null) {
				this.message = this.messageBuilder.build();
			}
			return new ActivityImportException(this.id, this.message, this.cause);
		}

		/**
		 * @param applicationActivityId2
		 * @return
		 */
		public Builder setActivityId(ActivityId id) {
			this.id = id;
			messageBuilder.addParameter(ActivityImportMessage.Param.APPLICATION, id.getApplicationKey());
			messageBuilder.addParameter(ActivityImportMessage.Param.ID, id.getRemoteId());
			return this;
		}

		public Builder remoteActivityNotFound() {
			this.messageBuilder.setMessage(ActivityImportMessage.REMOTE_ACTIVITY_NOT_FOUND);
			return this;
		}

		/**
		 * @return
		 */
		public Builder generalError() {
			this.messageBuilder.setMessage(ActivityImportMessage.GENERAL_IMPORT_ERROR);
			return this;
		}

		/**
		 * @return
		 */
		public Builder previousImportFailed() {
			this.messageBuilder.setMessage(ActivityImportMessage.PREVIOUS_FAILURE);
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private ActivityId id;

	/** Use {@link Builder} to construct ActivityImportException */
	private ActivityImportException(ActivityId id, CompleteMessage message, Throwable cause) {
		super(message, cause);
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public ActivityId getId() {
		return this.id;
	}

}
