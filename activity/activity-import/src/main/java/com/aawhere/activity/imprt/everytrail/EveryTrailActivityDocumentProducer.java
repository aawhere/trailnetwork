/**
 * 
 */
package com.aawhere.activity.imprt.everytrail;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.log.LoggerFactory;

/**
 * @author aroller
 * 
 */
public class EveryTrailActivityDocumentProducer
		implements DocumentProducer {

	/**
	 * 
	 */
	private static final DocumentFormatKey[] DOCUMENT_FORMAT_KEYS = new DocumentFormatKey[] { DocumentFormatKey.ETX };

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	public Document produce(Object dataInput) throws DocumentDecodingException, DocumentContentsNotRecognizedException {
		if (dataInput instanceof byte[]) {
			byte[] bytes = (byte[]) dataInput;
			try {
				return EveryTrailActivityDocument.create().contents(bytes).build();
			} catch (SAXException | IOException | ParserConfigurationException e) {
				LoggerFactory.getLogger(getClass()).warning(new String(bytes));
				throw new DocumentContentsNotRecognizedException();
			}
		} else {
			throw new DocumentContentsNotRecognizedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	public DocumentFormatKey[] getDocumentFormatsSupported() {
		return DOCUMENT_FORMAT_KEYS;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {
		return DiscoveryOrder.LOW;
	}

}
