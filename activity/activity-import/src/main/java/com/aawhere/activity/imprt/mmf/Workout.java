package com.aawhere.activity.imprt.mmf;

import java.util.List;

import com.aawhere.activity.ActivityDocument;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/** http://jsongen.byingtondesign.com/ */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Workout {
	private _links _links;
	private Aggregates aggregates;
	private String created_datetime;
	private boolean has_time_series;
	private String name;
	private String reference_key;
	private String source;
	private String start_datetime;
	private String start_locale_timezone;
	private Time_series time_series;
	private String updated_datetime;

	public _links get_links() {
		return this._links;
	}

	public void set_links(_links _links) {
		this._links = _links;
	}

	public Aggregates getAggregates() {
		return this.aggregates;
	}

	public void setAggregates(Aggregates aggregates) {
		this.aggregates = aggregates;
	}

	public String getCreated_datetime() {
		return this.created_datetime;
	}

	public void setCreated_datetime(String created_datetime) {
		this.created_datetime = created_datetime;
	}

	public boolean getHas_time_series() {
		return this.has_time_series;
	}

	public void setHas_time_series(boolean has_time_series) {
		this.has_time_series = has_time_series;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReference_key() {
		return this.reference_key;
	}

	public void setReference_key(String reference_key) {
		this.reference_key = reference_key;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStart_datetime() {
		return this.start_datetime;
	}

	public void setStart_datetime(String start_datetime) {
		this.start_datetime = start_datetime;
	}

	public String getStart_locale_timezone() {
		return this.start_locale_timezone;
	}

	public void setStart_locale_timezone(String start_locale_timezone) {
		this.start_locale_timezone = start_locale_timezone;
	}

	public Time_series getTime_series() {
		return this.time_series;
	}

	public void setTime_series(Time_series time_series) {
		this.time_series = time_series;
	}

	public String getUpdated_datetime() {
		return this.updated_datetime;
	}

	public void setUpdated_datetime(String updated_datetime) {
		this.updated_datetime = updated_datetime;
	}
}
