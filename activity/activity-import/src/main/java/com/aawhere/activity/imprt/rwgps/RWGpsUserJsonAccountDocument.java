/**
 * 
 */
package com.aawhere.activity.imprt.rwgps;

import com.aawhere.app.AccountDocument;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.Account;
import com.aawhere.doc.DocumentFormatKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author aroller
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RWGpsUserJsonAccountDocument
		implements AccountDocument {

	public final String id = null;
	public final String name = null;

	/**
	 * 
	 */
	public RWGpsUserJsonAccountDocument() {
	}

	/*
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		throw new UnsupportedOperationException();
	}

	/*
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.RW_USER_JSON;
	}

	/*
	 * @see com.aawhere.app.AccountDocument#username()
	 */
	@Override
	public String username() {
		return null;
	}

	/*
	 * @see com.aawhere.app.AccountDocument#userId()
	 */
	@Override
	public String userId() {
		return id;
	}

	/*
	 * @see com.aawhere.app.AccountDocument#name()
	 */
	@Override
	public String name() {
		return name;
	}

	/**
	 * @return
	 */
	public Account account() {
		return Account.create().setName(name).setRemoteId(id).setApplicationKey(KnownApplication.RWGPS.key).build();
	}

}
