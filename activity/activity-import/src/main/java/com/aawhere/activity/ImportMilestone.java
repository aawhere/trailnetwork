/**
 * 
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.aawhere.app.Application;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ComparatorResult;
import com.aawhere.lang.exception.MessageException;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.persist.LongBaseEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StatusMessage;
import com.aawhere.xml.XmlNamespace;

/**
 * Describes a milestone event while communicating with an external {@link Application}.
 * 
 * This is primarily used to keep track of import details, specifically those that are not able to
 * be processed due to any reason. This will prevent hitting the Application multiple times to
 * process.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public abstract class ImportMilestone<MilestoneT extends ImportMilestone<MilestoneT, IdentifierT>, IdentifierT extends Identifier<String, MilestoneT>>
		extends StringBaseEntity<MilestoneT, IdentifierT>
		implements Comparable<MilestoneT> {

	private static final long serialVersionUID = -6353421612076842084L;

	/**
	 * Indicates a result of a milestone achievement.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlEnum
	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static enum Status {
		/**
		 * The milestone has been achieved without significant issue. TN-240
		 * 
		 * @since 5
		 * */
		SUCCEEDED,
		/**
		 * SThis indicates the milestone event has been queued for processing.
		 */
		QUEUED,
		/**
		 * This milestone can not be achieved.
		 */
		FAILED,
		/** Indicates an interim step that provides useful information about an activity. */
		PROCESSED,

		/**
		 * Indicates that the milestone was similar to {@link #SUCCEEDED}, but it was updating
		 * values rather than creating. A {@link #SUCCEEDED} milestone should previously exist.
		 */
		UPDATED,

		/** FAILED is to SUCCEEDED as UPDATE_FAILED is to UPDATED. */
		UPDATE_FAILED

	}

	/**
	 * Indicates the success, failure or other for this {@link ImportMilestone} attempt.
	 * 
	 */
	@com.aawhere.field.annotation.Field(key = FIELD.STATUS)
	@XmlElement(name = FIELD.STATUS)
	private Status status;

	/**
	 * The error code that caused a failure (if known). This is often the name of the message enum,
	 * but most importantly it needs to be something that can be queried against to understand how
	 * often a certain type of failure is occurring.
	 */
	@XmlElement(name = FIELD.ERROR_CODE)
	@com.aawhere.field.annotation.Field(key = ImportMilestone.FIELD.ERROR_CODE)
	private String code;

	/**
	 * All the details that can be provided as to why this status occurred. Most commonly used for
	 * exceptions to report a stack trace, but it could contain other messages and warnings that may
	 * be useful for someone debugging why this milestone event is misbehaving.
	 * 
	 */
	@XmlElement
	private String details;

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return this.status;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return this.details;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@Override
	public IdentifierT getId() {
		return super.getId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#toString()
	 */
	@Override
	public String toString() {

		return getId() + " has status " + status + " and code " + code;
	}

	/**
	 * Orders by the create date if available, otherwise by id, else don't order.
	 */
	@Override
	public int compareTo(MilestoneT o) {
		int result;
		if (this.getDateCreated() != null && o.getDateCreated() != null) {
			result = this.getDateCreated().compareTo(o.getDateCreated());
		} else {
			// give up and don't indicate any order change.
			result = ComparatorResult.EQUAL.getValue();
		}
		return result;
	}

	/**
	 * Used to construct all instances of ActivityMilestone.
	 */
	@XmlTransient
	public abstract static class Builder<BuilderT extends Builder<BuilderT, MilestoneT, IdentifierT>, MilestoneT extends ImportMilestone<MilestoneT, IdentifierT>, IdentifierT extends Identifier<String, MilestoneT>>
			extends StringBaseEntity.Builder<MilestoneT, BuilderT, IdentifierT> {

		/**
		 * @deprecated since {@link LongBaseEntity} constructor is deprecated
		 * @param milestone
		 * @param identiferType
		 */
		@Deprecated
		protected Builder(MilestoneT milestone, Class<IdentifierT> identiferType) {
			super(milestone, identiferType);
		}

		protected Builder(MilestoneT milestone) {
			super(milestone);
		}

		public BuilderT setStatus(Status status) {
			building().status = status;
			return dis;
		}

		/**
		 * Provides additional information in the details section. This and cause do not go well
		 * together since they share the same {@link ImportMilestone#details} field.
		 * 
		 * @param message
		 * @return
		 */
		public BuilderT setMessage(CompleteMessage message) {
			assertDetailsNull();
			building().details = message.toString();
			setCode(message.getMessage());
			return dis;
		}

		private void assertDetailsNull() {
			Assertion.exceptions().assertNull("details already set", building().details);
		}

		public BuilderT setStatusMessage(StatusMessage statusMessage) {
			assertDetailsNull();
			building().details = statusMessage.toString();
			setCode(statusMessage.message.getMessage());
			return dis;
		}

		public BuilderT setCode(Message message) {
			building().code = message.name();
			return dis;
		}

		/**
		 * Tell us why this happened by giving the Throwable that made it fail, but from a place
		 * that isn't being handled well. This will output more details from the stack trace for
		 * debuggin.
		 * 
		 * ActivitySpatialRelationMilestone.java
		 * 
		 * @param cause
		 * @return
		 */
		public BuilderT setCause(Throwable cause) {

			if (cause != null) {
				assertDetailsNull();
				boolean fullDetail = false;
				if (cause instanceof MessageException) {
					// the code will be useful for quantifying, searching, etc.
					MessageException causeWithAMessage = (MessageException) cause;
					building().code = causeWithAMessage.getErrorCode();

					// These are specifically thrown for a pretty message when not
					// being handled so give full explanation
					if (building().code != null && building().code.equals(getGeneralErrorString())) {
						fullDetail = true;
					}

					// if it can't give a better error code then let's find out what happened.
					HttpStatusCode statusCode = causeWithAMessage.getStatusCode();
					if (statusCode == null
							|| ComparatorResult
									.isGreaterThanOrEqualTo(statusCode.compareTo(HttpStatusCode.BAD_REQUEST))) {
						fullDetail = true;
					}

				} else {
					// anything that doesn't come as one of ours must not be handled
					fullDetail = true;
				}
				if (fullDetail) {
					building().details = ExceptionUtils.getStackTrace(cause);
				} else {
					building().details = cause.getMessage();
				}
			}
			return dis;
		}

		/**
		 * @return
		 */
		private ImportMilestone<MilestoneT, IdentifierT> building() {
			return building;
		}

		protected abstract String getGeneralErrorString();

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull(building().status);
			super.validate();
		}

	}

	public static class FIELD {
		static final String DOMAIN = ActivityWebApiUri.IMPORT;
		public static final String ERROR_CODE = "code";
		public static final String STATUS = "status";

		public static class KEY {
		}
	}

}