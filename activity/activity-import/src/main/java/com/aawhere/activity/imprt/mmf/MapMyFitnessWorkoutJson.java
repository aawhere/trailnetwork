/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.lang3.mutable.Mutable;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aawhere.activity.ActivityDocument;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.Device;
import com.aawhere.app.DeviceCapabilities;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.app.IdentityHistory;
import com.aawhere.app.Installation;
import com.aawhere.app.Instance;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.Account;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.track.Track;
import com.aawhere.track.TrackContainer;
import com.aawhere.util.rb.StringMessage;

/**
 * @author aroller
 * 
 */
public class MapMyFitnessWorkoutJson
		implements ActivityDocument, TrackContainer {

	private static final long serialVersionUID = 3618323752407022075L;
	public DateTime startTime;

	/**
	 * Used to construct all instances of MapMyFitnessWorkoutJson.
	 */
	public static class Builder
			extends ObjectBuilder<MapMyFitnessWorkoutJson>
			implements Mutable<Workout> {

		public Builder() {
			super(new MapMyFitnessWorkoutJson());
		}

		public Builder workout(Workout workout) {
			building.workout = workout;
			return this;
		}

		public Builder deviceCapabilitiesRepository(DeviceCapabilitiesRepository repo) {
			building.deviceCapabilitiesRepository = repo;
			return this;
		}

		@Override
		public MapMyFitnessWorkoutJson build() {
			MapMyFitnessWorkoutJson built = super.build();

			built.startTime = start(built.workout);

			if (built.startTime != null) {
				final Time_series time_series = built.workout.getTime_series();
				if (time_series != null) {
					built.track = TimeSeriesTrack.create().timeSeries(time_series).start(built.startTime).build();
				}
			}
			return built;
		}

		/*
		 * (non-Javadoc)
		 * @see org.apache.commons.lang3.mutable.Mutable#getValue()
		 */
		@Override
		public Workout getValue() {
			return building.workout;
		}

		/*
		 * (non-Javadoc)
		 * @see org.apache.commons.lang3.mutable.Mutable#setValue(java.lang.Object)
		 */
		@Override
		public void setValue(Workout value) {
			workout(value);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("DeviceCapabilitiesRepository", building.deviceCapabilitiesRepository);
		}

	}// end Builder

	/** Use {@link Builder} to construct MapMyFitnessWorkoutJson */
	private MapMyFitnessWorkoutJson() {
	}

	public static Builder create() {
		return new Builder();
	}

	private DeviceCapabilitiesRepository deviceCapabilitiesRepository;
	private Workout workout;
	private Track track;

	/**
	 * @return the workout
	 */
	Workout getWorkout() {
		return this.workout;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.MMFW7;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityName()
	 */
	@Override
	public String getActivityName() {
		// TN-900 moved this to be stripped during name calculation
		return workout.getName();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityId()
	 */
	@Override
	public String getActivityId() {
		return workout.get_links().getSelf().get(0).getId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getUserId()
	 */
	@Override
	public String getUserId() {
		return workout.get_links().getUser().get(0).getId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getAccount()
	 */
	@Override
	public Account getAccount() {
		return Account.create().setApplicationKey(KnownApplication.MMF.key).setRemoteId(getUserId()).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityType()
	 */
	@Override
	public String getActivityType() {
		final List<Activity_type> activity_type = workout.get_links().getActivity_type();
		final String activityTypeId = activity_type.get(0).getId();
		return activityTypeId;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getHistory()
	 */
	@Override
	public IdentityHistory getHistory() {
		if (workout.getSource() != null) {
			String source = workout.getSource();
			// If we have an alias, convert to one we prefer, else store it as is since we may
			// create an alias in the future.
			ApplicationKey deviceApplicationKey = deviceCapabilitiesRepository.capabilities(source).deviceKey();
			if (deviceApplicationKey == null) {
				deviceApplicationKey = new ApplicationKey(source);
			}
			Application deviceApplication = Application.create().setKey(deviceApplicationKey)
					.setName(new StringMessage(source)).build();
			Device device = Device.createDevice().setApplication(deviceApplication).build();
			Installation deviceInstallation = Installation.create().setInstance(device).build();
			Instance mmfInstance = Instance.createInstance().setApplication(KnownApplication.MMF.asApplication())
					.build();
			Installation mmfInstallation = Installation.create().setExternalId(getActivityId())
					.setInstance(mmfInstance).build();
			IdentityHistory history = IdentityHistory.create().add(deviceInstallation).add(mmfInstallation).build();
			return history;
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getDescription()
	 */
	@Override
	public String getDescription() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.TrackContainer#getTrack()
	 */
	@Override
	public Track getTrack() {
		return track;

	}

	/**
	 * Converts the workout date/time stirng into DateTime
	 * 
	 * @param workout
	 * @return the time or null if not found, which it should almost always be found
	 */
	@Nullable
	public static DateTime start(Workout workout) {
		String startString = workout.getStart_datetime();
		DateTime start;
		if (startString != null) {
			start = DateTime.parse(startString);
		} else {
			start = null;
		}
		return start;
	}

	/*
	 * @see com.aawhere.activity.ActivityDocument#getDate()
	 */
	@Override
	public LocalDate getDate() {
		return (this.startTime != null) ? this.startTime.toLocalDate() : null;
	}
}
