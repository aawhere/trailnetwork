/**
 * 
 */
package com.aawhere.activity.imprt.everytrail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Set;

import javax.annotation.Nullable;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.joda.time.LocalDate;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.aawhere.activity.ActivityDocument;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivitySearchDocument;
import com.aawhere.app.Application;
import com.aawhere.app.IdentityHistory;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.Documents;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

/**
 * A Custom XML Reader specifically for EveryTrail.
 * 
 * @author aroller
 * 
 */
public class EveryTrailActivityDocument
		implements Document, ActivityDocument {

	private String tripId;
	private String name;
	private Account account;
	private Application app = KnownApplication.EVERY_TRAIL.asApplication();
	private String activityType;
	private String description;
	private byte[] bytes;
	/** When multiple ids are found...they are stored here. */
	private Multimap<AccountId, ActivityId> activityIds = HashMultimap.create();
	/**
	 * For the search document this is the date that is the most recent compared to others. null if
	 * none found.
	 */
	@Nullable
	private LocalDate mostRecentDate;
	public LocalDate date;

	/**
	 * Used to construct all instances of EveryTrailActivityDocument.
	 */
	public static class Builder
			extends ObjectBuilder<EveryTrailActivityDocument> {

		final String ID_ELEMENT_NAME = "id";
		private org.w3c.dom.Document document;

		public Builder() {
			super(new EveryTrailActivityDocument());
		}

		public Builder contents(byte[] bytes) throws SAXException, IOException, ParserConfigurationException {
			building.bytes = bytes;
			ByteArrayInputStream contents = new ByteArrayInputStream(bytes);
			this.document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(contents);
			Element response = this.document.getDocumentElement();
			NodeList tripElements = response.getElementsByTagName("trip");
			if (tripElements.getLength() == 0) {
				LoggerFactory.getLogger(getClass()).warning(tripElements.getLength() + " trips found");
			}
			// handles search results for more than 1 activity.
			else if (tripElements.getLength() > 1) {
				for (int i = 0; i < tripElements.getLength(); i++) {
					Element tripElement = (Element) tripElements.item(i);
					String tripIdValue = tripId(tripElement);
					ActivityId activityId = new ActivityId(building.app.getKey(), tripIdValue);
					building.activityIds.put(accountId(tripElement), activityId);
					LocalDate date = date(tripElement);
					building.mostRecentDate = JodaTimeUtil.moreRecent(date, building.mostRecentDate);
				}
			} else {
				Element trip = (Element) tripElements.item(0);
				building.tripId = tripId(trip);
				AccountId accountId = accountId(trip);
				building.activityIds.put(accountId, new ActivityId(building.app.getKey(), building.tripId));
				building.name = element(trip, "name").getTextContent();
				building.account = Account.create().setApplicationKey(accountId.getApplicationKey())
						.setRemoteId(accountId.getRemoteId()).build();
				Element activityTypeElement = element(trip, "activity");
				building.activityType = tripId(activityTypeElement);
				building.description = element(trip, "description").getTextContent();
				building.date = date(trip);
			}
			return this;
		}

		/**
		 * @param trip
		 * @return
		 */
		private LocalDate date(Element trip) {
			return LocalDate.parse(element(trip, "date").getTextContent());
		}

		/**
		 * Working on the trip element this will provide the associated account id.
		 * 
		 * @param trip
		 * @return
		 */
		private AccountId accountId(Element trip) {
			Element userElement = element(trip, "user");
			AccountId accountId = new AccountId(building.app.getKey(), tripId(userElement));
			return accountId;
		}

		/**
		 * @param trip
		 * @return
		 */
		private String tripId(Element trip) {
			return trip.getAttribute(ID_ELEMENT_NAME);
		}

		/**
		 * @param trip
		 * @param nameElementName
		 * @return
		 */
		private Element element(Element trip, String nameElementName) {
			return (Element) trip.getElementsByTagName(nameElementName).item(0);
		}

		@Override
		public EveryTrailActivityDocument build() {
			EveryTrailActivityDocument built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct EveryTrailActivityDocument */
	private EveryTrailActivityDocument() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityName()
	 */
	@Override
	public String getActivityName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityId()
	 */
	@Override
	public String getActivityId() {
		return this.tripId;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getUserId()
	 */
	@Override
	public String getUserId() {
		return this.account.getRemoteId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getAccount()
	 */
	@Override
	public Account getAccount() {
		return account;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getActivityType()
	 */
	@Override
	public String getActivityType() {
		return this.activityType;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getHistory()
	 */
	@Override
	public IdentityHistory getHistory() {
		// TODO:record upload date.
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ActivityDocument#getDescription()
	 */
	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public LocalDate getDate() {
		return this.date;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		return this.bytes;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return DocumentFormatKey.ETX;
	}

	/**
	 * Provides complete re-use of {@link EveryTrailActivityDocument} parsing of a similar document,
	 * but conforms to the requirements of {@link Documents} to avoid implementing multiple
	 * interfaces.
	 * 
	 * @author aroller
	 * 
	 */
	public static class EveryTrailSearchDocument
			implements Document, ActivitySearchDocument {

		private EveryTrailActivityDocument document;

		/**
		 * 
		 */
		public EveryTrailSearchDocument(EveryTrailActivityDocument document) {
			this.document = document;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.Document#toByteArray()
		 */
		@Override
		public byte[] toByteArray() {
			return this.document.toByteArray();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.Document#getDocumentFormat()
		 */
		@Override
		public DocumentFormatKey getDocumentFormat() {

			DocumentFormatKey documentFormat = this.document.getDocumentFormat();
			// document format could be user format, but if activity then indicate search result
			// instead
			if (documentFormat == DocumentFormatKey.ETX) {
				documentFormat = DocumentFormatKey.ETSX;
			}
			return documentFormat;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivitySearchDocument#activityIds()
		 */
		@Override
		public Set<ActivityId> activityIds() {
			return this.document.activityIds();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivitySearchDocument#finalPage()
		 */
		@Override
		public Boolean finalPage() {
			return this.document.finalPage();
		}

		/*
		 * @see com.aawhere.activity.ActivitySearchDocument#ids()
		 */
		@Override
		public Multimap<AccountId, ActivityId> ids() {
			return this.document.activityIds;
		}

		/*
		 * @see com.aawhere.activity.ActivitySearchDocument#getMostRecentDate()
		 */
		@Override
		public LocalDate mostRecentStartDate() {
			return this.document.mostRecentDate;
		}

	}

	/**
	 * @see EveryTrailSearchDocument
	 * @return
	 */
	public Set<ActivityId> activityIds() {
		return Sets.newHashSet(this.activityIds.values());
	}

	/**
	 * @see EveryTrailSearchDocument
	 * @return
	 */
	public Boolean finalPage() {
		// FIXME:There should be a better way to check for a next page, but this will do.
		return activityIds.isEmpty();
	}

}
