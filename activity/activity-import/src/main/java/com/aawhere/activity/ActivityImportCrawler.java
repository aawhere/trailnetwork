/**
 * 
 */
package com.aawhere.activity;

import java.util.Collection;
import java.util.Set;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.LocalDate;

import com.aawhere.activity.ApplicationActivityImportInstructions.Destination;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationDataIdImpl;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.id.StringIdentifier;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.queue.Queue;
import com.aawhere.queue.Queue.Result;
import com.aawhere.queue.Task;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.retrieve.WebDocumentRetriever;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Using the {@link ActivityImportCrawlerDestination} this will crawl the remote {@link Application}
 * producing {@link ActivityId}s that are to be imported and posts them to the given queue for
 * processing. If crawling by area this posts the discovered accounts to the queue. If crawling by
 * account this posts the activities to the queue for the account.
 * 
 * The crawler will continue until no new activities are found for the target. This means already
 * successfully imported, previously failed to import or a task has already been posted within the
 * time limit enforced by tasks.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class ActivityImportCrawler {

	/**
	 * Used for import documents that don't have the account id available. The use of this is
	 * limited to when all activities are related to a single account, but that account is not known
	 * at the moment. The id must be exposed during Activity import.
	 * 
	 */
	public static final AccountId ACCOUNT_ID_UNAVAILABLE = new AccountId(KnownApplication.UNKNOWN.key,
			KnownApplication.Name.UNKNOWN.getValue());
	@XmlElement
	private ActivityImportCrawlerDestination crawlerDestination;

	@XmlAttribute
	private Boolean deep = false;
	@XmlAttribute
	private Boolean finalPage;
	@XmlAttribute
	private Boolean stopBecauseNothingNew = false;
	@XmlElement
	private Task nextPageTask;
	@ApiModelProperty("Activity candidates found in this page")
	@XmlElement
	private Set<ActivityId> activityIdsAvailable;
	@ApiModelProperty(value = "Activities queued to be imported")
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Multimap<Queue.Result, StringIdentifier<?>> idsQueued;
	@ApiModelProperty(value = "Activity has already been imported")
	@XmlElement
	private Set<ActivityId> activityIdsExisting;

	@ApiModelProperty(value = "Some Activities don't have tracks")
	@XmlElement
	private Set<ActivityId> activityIdsNotWorthImporting;

	/** The account ids found in the search results. */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Multimap<AccountId, ActivityId> idsDiscovered;
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private LocalDate mostRecentStartDate;

	/**
	 * Used to construct all instances of ActivityImportCrawler.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<ActivityImportCrawler> {
		private Queue crawlQueue;
		@Nullable
		private Queue activityImportQueue;
		private DocumentRetriever retriever;
		@Nullable
		private Function<Set<ActivityId>, Iterable<ActivityId>> nonExistingIdsFunction;

		public Builder() {
			super(new ActivityImportCrawler());
		}

		public Builder crawlerDestination(ActivityImportCrawlerDestination crawlerDestination) {
			building.crawlerDestination = crawlerDestination;
			return this;
		}

		public Builder activityImportQueue(Queue activityImportQueue) {
			this.activityImportQueue = activityImportQueue;
			return this;
		}

		public Builder crawlQueue(Queue crawlQueue) {
			this.crawlQueue = crawlQueue;
			return this;
		}

		public Builder deep(Boolean deep) {
			building.deep = deep;
			return this;
		}

		@Override
		public ActivityImportCrawler build() {
			try {

				return crawl();
			} catch (BaseException e) {
				throw ToRuntimeException.wrapAndThrow(e);
			}
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("crawlQueue", this.crawlQueue);
			Assertion.exceptions().notNull("crawlerDestination", building.crawlerDestination);
			Assertion.exceptions().notNull("retriever", this.retriever);
		}

		/**
		 * Synonym for {@link #build()}, but provides the exception.
		 * 
		 * @return
		 * @throws BaseException
		 */
		public ActivityImportCrawler crawl() throws BaseException {
			ActivityImportCrawler built = super.build();
			// the end goal really, activities or accounts post their success/failures here.
			built.idsQueued = HashMultimap.create();

			Destination destination = built.crawlerDestination.destination();
			retriever.retrieve(destination.format);
			ActivitySearchDocument document = (ActivitySearchDocument) retriever.getDocument();
			built.activityIdsAvailable = document.activityIds();
			built.idsDiscovered = document.ids();
			built.mostRecentStartDate = document.mostRecentStartDate();
			queueActivities(built);
			queueAccounts(built);

			// if there were any returned let's try the next page to look for more.
			final Collection<StringIdentifier<?>> idsSuccessfullyQueued = built.idsQueued.get(Result.ADDED);
			built.stopBecauseNothingNew = !built.deep
					&& (built.idsQueued == null || CollectionUtils.isEmpty(idsSuccessfullyQueued));

			built.finalPage = document.finalPage();

			if (!built.finalPage && !built.stopBecauseNothingNew) {
				built.nextPageTask = built.crawlerDestination.nextPageTask(built.deep);
				this.crawlQueue.add(built.nextPageTask);
			}

			return built;
		}

		/**
		 * When crawling by area the accounts that are found are to be posted so they can return and
		 * crawl the account as needed. Duplication avoidance is managed by the queue.
		 * 
		 * @param built
		 */
		private void queueAccounts(ActivityImportCrawler built) {
			if (built.crawlerDestination.isCrawlingByCells()) {

				for (AccountId accountId : built.accountIdsAvailable()) {
					Result result = this.crawlQueue.add(ActivityWebApiUri.accountEndpoint(accountId));
					built.idsQueued.put(result, accountId);
				}
			}
		}

		/**
		 * Posts the activities to the queue for the account if we are crawling by account.
		 * 
		 * @param built
		 */
		private void queueActivities(ActivityImportCrawler built) {
			// crawling by account is when we post activities.
			if (built.crawlerDestination.isCrawlingByAccount()) {

				// filter out those that can't be imported. we should still report them as found
				// though
				Set<ActivityId> idsWorthImporting = ActivityImportUtil.idsWorthImporting(built.activityIdsAvailable);

				built.activityIdsNotWorthImporting = Sets.difference(built.activityIdsAvailable, idsWorthImporting);
				Set<ActivityId> activityIdsToQueue;
				// determine if any already exist
				if (this.nonExistingIdsFunction != null) {
					Iterable<ActivityId> nonExistingIds = nonExistingIdsFunction.apply(idsWorthImporting);

					activityIdsToQueue = Sets.newHashSet(nonExistingIds);
					built.activityIdsExisting = Sets.difference(idsWorthImporting, activityIdsToQueue);
				} else {
					activityIdsToQueue = idsWorthImporting;
				}

				if (!activityIdsToQueue.isEmpty()) {

					for (ActivityId activityId : activityIdsToQueue) {

						Result result = this.activityImportQueue.add(ActivityWebApiUri
								.activityImportTask(activityId, ActivityWebApiUri.ACTIVITY_IMPORT_METHOD_FOR_BLOCKING));
						built.idsQueued.put(result, activityId);
					}

				}
			}
		}

		/**
		 * @param retriever2
		 * @return
		 */
		public Builder retriever(DocumentRetriever retriever) {
			this.retriever = retriever;
			return this;
		}

		/**
		 * @param nonExistingIdsFunction2
		 * @return
		 */
		public Builder nonExistingIdsFunction(Function<Set<ActivityId>, Iterable<ActivityId>> nonExistingIdsFunction) {
			this.nonExistingIdsFunction = nonExistingIdsFunction;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct ActivityImportCrawler */
	private ActivityImportCrawler() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Common setup of the {@link ActivityImportCrawler} using the {@link WebDocumentRetriever}.
	 * 
	 * @return
	 */
	public static ActivityImportCrawler.Builder createWebRetrieverCrawler(
			ActivityImportCrawlerDestination crawlerDestination, DocumentFactory documentFactory) {
		WebDocumentRetriever retriever = createWebDocumentRetriever(crawlerDestination, documentFactory);
		return ActivityImportCrawler.create().crawlerDestination(crawlerDestination).retriever(retriever);
	}

	/**
	 * @param crawlerDestination
	 * @param documentFactory
	 * @return
	 */
	public static WebDocumentRetriever createWebDocumentRetriever(ActivityImportCrawlerDestination crawlerDestination,
			DocumentFactory documentFactory) {
		Destination destination = crawlerDestination.destination();
		ApplicationDataIdImpl dataId = new ApplicationDataIdImpl(destination.url);
		WebDocumentRetriever retriever = new WebDocumentRetriever.Builder().setUrl(crawlerDestination.url())
				.setHeaders(destination.headers).setApplicationDataId(dataId).setDocumentFactory(documentFactory)
				.build();
		return retriever;
	}

	/**
	 * @return the activityIdsAvailable
	 */
	public Set<ActivityId> activityIdsAvailable() {
		return this.activityIdsAvailable;
	}

	/**
	 * @return the activityIdsQueued
	 */
	public Multimap<Queue.Result, StringIdentifier<?>> idsQueued() {
		return this.idsQueued;
	}

	/**
	 * @return
	 */
	public Boolean finalPage() {
		return this.finalPage;
	}

	/**
	 * @return the nextPageTask
	 */
	public Task nextPageTask() {
		return this.nextPageTask;
	}

	/**
	 * @return the stopBecauseNoNewActivities
	 */
	public Boolean stoppedBecauseNoNewActivities() {
		return this.stopBecauseNothingNew;
	}

	/**
	 * @return
	 */
	public Boolean deep() {
		return deep;
	}

	public Multimap<AccountId, ActivityId> idsDiscovered() {
		return idsDiscovered;
	}

	/**
	 * @return
	 */
	public Set<AccountId> accountIdsAvailable() {
		return this.idsDiscovered.keySet();
	}

	@XmlElement
	public Set<AccountId> getAccountIdsAvailable() {
		return accountIdsAvailable();
	}

	/**
	 * @return the mostRecentStartDate
	 */
	public LocalDate mostRecentStartDate() {
		return this.mostRecentStartDate;
	}
}
