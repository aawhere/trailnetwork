/**
 * 
 */
package com.aawhere.activity;

import javax.annotation.Nullable;

import com.aawhere.activity.ImportMilestone.Status;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class ActivityMilestoneStatusFunction
		implements Function<ImportMilestone<?, ?>, Status> {

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public Status apply(@Nullable ImportMilestone<?, ?> input) {
		if (input == null) {
			return null;
		} else {
			return input.getStatus();
		}
	}
}
