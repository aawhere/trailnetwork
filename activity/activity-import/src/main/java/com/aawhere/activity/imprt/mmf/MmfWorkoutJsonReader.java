/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.io.IOException;
import java.io.InputStream;

import com.aawhere.activity.imprt.mmf.MapMyFitnessWorkoutJson.Builder;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.io.IoException;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author aroller
 * 
 */
public class MmfWorkoutJsonReader {

	/**
	 * Used to construct all instances of MmfWorkoutJsonReader.
	 */
	public static class Builder
			extends ObjectBuilder<MmfWorkoutJsonReader> {

		public Builder() {
			super(new MmfWorkoutJsonReader());
		}

		public Builder deviceCapabilitiesRepository(DeviceCapabilitiesRepository repo) {
			building.deviceCapabilitiesRepository = repo;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("deviceCapabilitiesRepository", building.deviceCapabilitiesRepository);
		}

		@Override
		public MmfWorkoutJsonReader build() {
			MmfWorkoutJsonReader built = super.build();
			built.mapper = new ObjectMapper();
			built.mapper.setVisibilityChecker(built.mapper.getVisibilityChecker().withFieldVisibility(Visibility.ANY));
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct MmfWorkoutJsonReader */
	private MmfWorkoutJsonReader() {
	}

	public static Builder create() {
		return new Builder();
	}

	private DeviceCapabilitiesRepository deviceCapabilitiesRepository;
	private ObjectMapper mapper;

	public MapMyFitnessWorkoutJson read(String json) throws DocumentDecodingException {
		try {
			Workout workout = mapper.readValue(json.getBytes(), Workout.class);
			return workout(workout);
		} catch (IOException e) {
			throw handle(e);
		}
	}

	/**
	 * @param e
	 * @throws DocumentDecodingException
	 */
	private DocumentContentsNotRecognizedException handle(IOException e) throws DocumentDecodingException {
		if (e instanceof JsonMappingException) {
			throw new DocumentDecodingException(e);
		} else {
			throw new DocumentContentsNotRecognizedException();
		}
	}

	/**
	 * @param workout
	 * @return
	 */
	private MapMyFitnessWorkoutJson workout(Workout workout) {
		return MapMyFitnessWorkoutJson.create().workout(workout)
				.deviceCapabilitiesRepository(deviceCapabilitiesRepository).build();
	}

	/**
	 * @param is
	 * @return
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws IoException
	 */
	public MapMyFitnessWorkoutJson read(InputStream is) throws DocumentContentsNotRecognizedException,
			DocumentDecodingException {
		try {
			return workout(mapper.readValue(is, Workout.class));
		} catch (IOException e) {
			throw handle(e);
		}
	}

	/**
	 * @param is
	 * @return
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws IoException
	 */
	public MapMyFitnessWorkoutJson read(byte[] bytes) throws DocumentContentsNotRecognizedException,
			DocumentDecodingException {
		try {
			return workout(mapper.readValue(bytes, Workout.class));
		} catch (IOException e) {
			throw handle(e);
		}
	}
}
