/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.activity.ApplicationActivityImportInstructions.FIELD.PARAM.*;
import static com.aawhere.doc.AxmDocumentUtil.*;
import static com.aawhere.doc.garmin.activity.GarminConnectActivitiesFilter.ActivityType.*;
import static com.aawhere.ws.rs.UriConstants.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.aawhere.activity.ApplicationActivityImportInstructions.Destination;
import com.aawhere.activity.imprt.mmf.type.MapMyFitnessActivityTypes;
import com.aawhere.activity.timing.ActivityTimingWebApiUri;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.KnownInstance;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountProvider;
import com.aawhere.collections.MapBuilder;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.garmin.activity.GarminConnectActivitiesFilter;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.retrieve.WebDocumentRetriever;
import com.aawhere.util.rb.KeyedMessageFormat;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.aawhere.ws.rs.UriBuilderUtil;
import com.google.api.client.http.HttpHeaders;
import com.google.inject.Singleton;

/**
 * Provides the {@link ApplicationActivityImportInstructions} for {@link KnownApplication}s.
 * 
 * TODO: These known instructions will be persisted.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ApplicationActivityImportInstructionsRepository
		implements ApplicationActivityUrlProvider, AccountProvider.ProfilePage {

	/**
	 * //TN-795 15 seconds seems to be too quick for GC. EveryTrail returns 504 too.
	 * 
	 * Increasing this can cause drag, but it keeps us from making another bad request. We don't
	 * really care if our asynch servers wait that long as long as they eventually give up.
	 */
	private static final int LONGER_TIMEOUT = 30 * 1000;
	/**
	 * The base reference path for GC to view an activity. Just append the remote activity id.
	 * TN-761 updated to modern.
	 * 
	 */
	private static final String GARMIN_CONNECT_ACTIVITY_BASE_PATH = "http://connect.garmin.com/modern/activity/";

	/**
	 * Used to construct all instances of KnownApplicationActivityImportInstructions.
	 */
	public static class Builder
			extends ObjectBuilder<ApplicationActivityImportInstructionsRepository> {

		public Builder() {
			super(new ApplicationActivityImportInstructionsRepository());
		}

		public Builder(ApplicationActivityImportInstructionsRepository child) {
			super(child);
		}

		public Builder addInstructions(ApplicationActivityImportInstructions instructions) {
			building.instructions.put(instructions.getApplication().getKey(), instructions);
			return this;
		}
	}// end Builder

	protected static final String GARMIN_CONNECT_BASE_URI = "http://connectapi.garmin.com/activity-service-1.1/";
	private static final String GARMIN_CONNECT_GPX_PATH = "gpx/activity";
	private static final String GARMIN_CONNECT_AXM_PATH = "axm/activity";

	private HashMap<ApplicationKey, ApplicationActivityImportInstructions> instructions;

	/** Use {@link Builder} to construct KnownApplicationActivityImportInstructions */
	protected ApplicationActivityImportInstructionsRepository() {
		instructions = new HashMap<ApplicationKey, ApplicationActivityImportInstructions>();
	}

	public ApplicationActivityImportInstructions instructionsFor(ApplicationDataId applicationDataId)
			throws ApplicationImportInstructionsNotFoundException {
		return instructionsFor(applicationDataId.getApplicationKey());
	}

	public ApplicationActivityImportInstructions instructionsFor(ApplicationKey applicationKey)
			throws ApplicationImportInstructionsNotFoundException {
		ApplicationActivityImportInstructions instructions = this.instructions.get(applicationKey);
		if (instructions == null) {
			// this validates
			KnownApplication.byKey(applicationKey);
			throw new ApplicationImportInstructionsNotFoundException(applicationKey);
		}
		return instructions;
	}

	public ApplicationActivityImportInstructions instructionsFor(Application application)
			throws ApplicationImportInstructionsNotFoundException {
		return instructionsFor(application.getKey());
	}

	/**
	 * Given common retriever components this will return the retriever in a state ready to
	 * retrieve.
	 * 
	 * Retrievers may need a lot of setup such as security which should be configured prior to
	 * returning.
	 * 
	 * TODO: Assumes a {@link WebDocumentRetriever}. This method will most definitely change as we
	 * expand our calls to other systems.
	 * 
	 * @param applicationDataId
	 * @param documentFactory
	 * @return
	 */
	protected DocumentRetriever createActivityRetriever(Destination destination, ApplicationDataId applicationDataId,
			DocumentFactory documentFactory, ApplicationActivityImportInstructions instructions) {
		final String idParam = ApplicationActivityImportInstructions.FIELD.ACTIVITY_ID;

		return createRetriever(destination, applicationDataId, documentFactory, instructions, idParam);
	}

	protected DocumentRetriever createAccountRetriever(Destination destination, ApplicationDataId applicationDataId,
			DocumentFactory documentFactory, ApplicationActivityImportInstructions instructions) {
		return createRetriever(	destination,
								applicationDataId,
								documentFactory,
								instructions,
								ApplicationActivityImportInstructions.FIELD.ACCOUNT_REMOTE_ID);
	}

	/**
	 * @param destination
	 * @param applicationDataId
	 * @param documentFactory
	 * @param instructions
	 * @param idParam
	 * @return
	 */
	private DocumentRetriever createRetriever(Destination destination, ApplicationDataId applicationDataId,
			DocumentFactory documentFactory, ApplicationActivityImportInstructions instructions, final String idParam) {
		// TODO: move all of this out of here to...subclass or delegate?
		String activityUrlPath = destination.url;

		// this url building is not general enough. it belongs...?
		// TODO: is the repoteId scrubbed when it is accepted at the web-api layer? If not, I see a
		// potential security issue here.
		KeyedMessageFormat format = new KeyedMessageFormat.Builder().put(idParam, applicationDataId.getRemoteId())
				.message(activityUrlPath).build();
		activityUrlPath = format.getFormattedMessage();
		URL url;
		try {
			url = new URL(activityUrlPath);
		} catch (MalformedURLException e) {
			// I think this is our error, not the user's
			throw new RuntimeException(activityUrlPath, e);
		}

		// TODO:Provide a custom retriever
		com.aawhere.retrieve.WebDocumentRetriever.Builder retrieverBuilder = new WebDocumentRetriever.Builder()
				.setApplicationDataId(applicationDataId).setUrl(url).setDocumentFactory(documentFactory)
				.connectionTimeoutMillis(instructions.getConnectionTimeoutInMillis()).setHeaders(destination.headers);

		WebDocumentRetriever retriever = retrieverBuilder.build();

		return retriever;
	}

	/**
	 * Create all document retrievers for the {@link ApplicationActivityImportInstructions}.
	 * 
	 * @param instructions
	 * @param applicaitonActivityId
	 * @param documentFactory
	 * @return
	 */
	protected Set<DocumentRetriever> createRetrievers(ApplicationActivityImportInstructions instructions,
			ActivityId applicaitonActivityId, DocumentFactory documentFactory) {

		Set<DocumentRetriever> retrievers = new HashSet<DocumentRetriever>();
		List<Destination> pairs = instructions.getActivityUrls();
		for (Destination destination : pairs) {
			retrievers.add(createActivityRetriever(destination, applicaitonActivityId, documentFactory, instructions));
		}
		return retrievers;
	}

	/**
	 * Activities are full length GPS recorded workouts associated to a user's effort.
	 * 
	 * @return
	 */
	public static ApplicationActivityImportInstructions twoPeakActivityInstructions() {

		ApplicationActivityImportInstructions.Builder builder = twoPeakInstructionsBuilder(KnownApplication.TWO_PEAK_ACTIVITIES)
				.webUrl("http://2peak.com/training/pulse_data.php?docuId=" + ACTIVITY_ID);
		return builder.build();
	}

	/**
	 * Sections are user-creation portions of an Activity that are of special interest. They use
	 * them mostly to highlight hill climbs.
	 * 
	 * @return
	 */
	public static ApplicationActivityImportInstructions twoPeakSectionInstructions() {
		return twoPeakInstructionsBuilder(KnownApplication.TWO_PEAK_SECTIONS).setModifiableDocuments(true)
				.webUrl("http://2peak.com/section/leaderboard.php?id=" + ACTIVITY_ID).build();
	}

	/**
	 * @return
	 */
	private static ApplicationActivityImportInstructions.Builder twoPeakInstructionsBuilder(KnownApplication app) {
		ApplicationActivityImportInstructions.Builder builder = new ApplicationActivityImportInstructions.Builder();

		builder.setApplication(app.asApplication());

		String subdomain;
		if (KnownInstance.TRAIL_NETWORK.isThisSystem()) {
			subdomain = "www";
		} else {
			subdomain = "dev";
		}
		String apiBase = new StringBuilder().append("http://").append(subdomain).append(".2peak.com/api/v1/")
				.toString();

		// section or activity
		String type;
		if (app.equals(KnownApplication.TWO_PEAK_ACTIVITIES)) {
			type = "activity";
			// TN-288 2peak activities do not need a callback. only sections.
		} else if (app.equals(KnownApplication.TWO_PEAK_SECTIONS)) {
			type = "section";
			StringBuilder callbackUrl = new StringBuilder().append(apiBase).append("callback/").append(type)
					.append(SLASH).append(ActivityTimingWebApiUri.COURSE_PARAM).append("/?key=peak&found=")
					.append(ActivityTimingWebApiUri.ATTEMPT_PARAM);
			builder.setCourseCompletedCallbackUrl(callbackUrl.toString());

		} else {
			throw new InvalidChoiceException(app, KnownApplication.TWO_PEAK_ACTIVITIES,
					KnownApplication.TWO_PEAK_SECTIONS);
		}

		StringBuilder activityUrl = new StringBuilder().append(apiBase).append(type).append(SLASH).append(ACTIVITY_ID)
				.append("/csv?key=peak");

		builder.addActivityUrl(activityUrl.toString(), DocumentFormatKey.ACTIVITY_CSV)
				.addActivityTypeMapping("running", ActivityType.RUN)
				.addActivityTypeMapping("cycling", ActivityType.BIKE)
				.addActivityTypeMapping("nordicwalking", ActivityType.WALK)
				.addActivityTypeMapping("xcskiing", ActivityType.SKI)
				.addActivityTypeMapping("spinning", ActivityType.BIKE)
				.addActivityTypeMapping("athletics", ActivityType.RUN)
				.addActivityTypeMapping("swimming", ActivityType.SWIM)
				.addActivityTypeMapping("inlineskating", ActivityType.SKATE);
		return builder;
	}

	public static ApplicationActivityImportInstructions everyTrailInstructions() {
		ApplicationActivityImportInstructions.Builder builder = ApplicationActivityImportInstructions.create();
		HttpHeaders headers = new HttpHeaders();
		headers.setBasicAuthentication("02453fb4b89c3fb666026a1a778140a6", "8473df0cff6e398f");
		builder.addActivityUrl("http://www.everytrail.com/api/trip/tracks?trip_id=" + ACTIVITY_ID,
		// everytrail trip track response
								DocumentFormatKey.GPX,
								headers);
		builder.addActivityUrl(	"http://www.everytrail.com/api/trip?trip_id=" + ACTIVITY_ID,
								DocumentFormatKey.ETX,
								headers);
		builder.webUrl("http://www.everytrail.com/view_trip.php?trip_id=" + ACTIVITY_ID);
		builder.profilePage("http://www.everytrail.com/profile.php?user_id=" + ACCOUNT_REMOTE_ID);
		String andStartParam = "&start=" + FILTER_START;
		builder.setAccountDestination(new Destination(
				"http://www.everytrail.com/api/user?user_id=" + ACCOUNT_REMOTE_ID, DocumentFormatKey.ETUX, headers));
		final String andSortByCreationDate = "&sort=creation_date&order=DESC";
		builder.setSearchDestination(new Destination("http://www.everytrail.com/api/trip/search?lat=" //
				+ CENTER_LAT + "&lon="//
				+ CENTER_LON + "&min_length=1000&min_duration=60&proximity=" //
				+ PROXIMITY_IN_MILES + andStartParam + andSortByCreationDate, DocumentFormatKey.ETSX, headers));

		// TN-807 results are flaking when filtering on date..include all
		// final String andModifiedAfter = "&modified_after=" + SINCE_TIMESTAMP;
		// activity search for account

		final String activitySearchAccountUrl = "http://www.everytrail.com/api/user/trips?user_id=" + ACCOUNT_REMOTE_ID
				+ andSortByCreationDate + andStartParam;
		builder.setActivitySearchForAccountDestination(new Destination(activitySearchAccountUrl,
				DocumentFormatKey.ETSX, headers));
		builder.setApplication(KnownApplication.EVERY_TRAIL.asApplication());
		builder.addActivityTypeMapping("5", ActivityType.HIKE);
		builder.addActivityTypeMapping("28", ActivityType.HIKE);
		builder.addActivityTypeMapping("13", ActivityType.WALK);
		builder.addActivityTypeMapping("6", ActivityType.MTB);
		builder.addActivityTypeMapping("8", ActivityType.BIKE);
		builder.addActivityTypeMapping("9", ActivityType.RUN);
		builder.addActivityTypeMapping("29", ActivityType.SKATE);
		builder.addActivityTypeMapping("30", ActivityType.SKATE);
		builder.addActivityTypeMapping("26", ActivityType.SKATE);
		builder.addActivityTypeMapping("1", ActivityType.SKI);
		builder.addActivityTypeMapping("2", ActivityType.SKI);
		builder.addActivityTypeMapping("14", ActivityType.OTHER);

		builder.setConnectionTimeoutInMillis(LONGER_TIMEOUT);
		return builder.build();
	}

	public static ApplicationActivityImportInstructions rideWithGpsInstructions() {
		final String CREDENTIALS_QUERY_PARAMS = "?apikey=b87ff2a2&version=2&auth_token=6e3a6c5b6e956e5230bbd314da00f250";
		final String PAGING_QUERY_PARAMS = "&offset=" + FILTER_START + "&limit=20";
		final String ENDPOINT_SUFFIX = ".json";
		final String BASE_URI = "http://ridewithgps.com/";
		final String users = "users";
		final String trips = "trips";
		ApplicationActivityImportInstructions.Builder builder = ApplicationActivityImportInstructions.create();
		builder.addActivityTypeMapping(	ApplicationActivityImportInstructions.ACTIVITY_TYPE_DEFAULT_KEY,
										ActivityType.BIKE);
		final String activityWebUrl = BASE_URI + trips + SLASH + ACTIVITY_ID;
		builder.webUrl(activityWebUrl);
		builder.addActivityUrl(	activityWebUrl + ENDPOINT_SUFFIX + CREDENTIALS_QUERY_PARAMS,
								DocumentFormatKey.RW_TRIP_JSON);
		final String userWebUrl = BASE_URI + users + SLASH + ACCOUNT_REMOTE_ID;

		builder.profilePage(userWebUrl);

		// builder.setAccountDestination(new Destination(userWebUrl,
		// DocumentFormatKey.RW_USER_JSON));
		builder.setActivitySearchForAccountDestination(new Destination(userWebUrl + SLASH + trips + ENDPOINT_SUFFIX
				+ CREDENTIALS_QUERY_PARAMS + PAGING_QUERY_PARAMS, DocumentFormatKey.RW_USER_ACTIVITIES_JSON));
		// builder.setSearchDestination(null) //search destination is not obvious.
		builder.setApplication(KnownApplication.RWGPS.asApplication());

		return builder.build();
	}

	public static ApplicationActivityImportInstructions mmfInstructions() {
		ApplicationActivityImportInstructions.Builder builder = ApplicationActivityImportInstructions.create();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Api-Key", "rym8x3bjartujrjgua5zt64wugeex4vq");
		// TN-568 this comes from https://www.mapmyapi.com/io-docs Access Token
		headers.setAuthorization("Bearer 9d8a33dcfae3167843f24872a170a9ac4c1c4e0e");
		builder.addActivityUrl("https://oauth2-api.mapmyapi.com/v7.0/workout/" + ACTIVITY_ID
				+ "/?field_set=time_series", DocumentFormatKey.MMFW7, headers);
		builder.webUrl("http://www.mapmyfitness.com/workout/" + ACTIVITY_ID);
		builder.profilePage("http://www.mapmyfitness.com/profile/" + ACCOUNT_REMOTE_ID);
		builder.setSearchDestination(new Destination("https://oauth2-api.mapmyapi.com/v7.0/route/?close_to_location="
				+ CENTER_LAT + "," + CENTER_LON + "&maximum_distance=300000&minimum_distance=1000",
				DocumentFormatKey.MMFR7, headers));
		builder.setActivitySearchForAccountDestination(new Destination(
				"https://oauth2-api.mapmyapi.com/v7.0/workout/?user=" + ACCOUNT_REMOTE_ID + "&offset=" //
						+ FILTER_START + "&started_after=" + SINCE_TIMESTAMP,//
				DocumentFormatKey.MMFUW7, headers));
		builder.setAccountDestination(new Destination("https://oauth2-api.mapmyapi.com/v7.0/user/" + ACCOUNT_REMOTE_ID,
				DocumentFormatKey.MMFU7, headers));
		builder.setApplication(KnownApplication.MMF.asApplication());

		MapMyFitnessActivityTypes activityTypes = new MapMyFitnessActivityTypes();

		builder.setActivityTypeMapping(activityTypes.knownActivityTypes());
		builder.setConnectionTimeoutInMillis(45000);
		return builder.build();
	}

	/**
	 * Provides login capabilities at the web app.
	 * 
	 * https://developers.google.com/identity-toolkit/
	 * 
	 * @see KnownApplication#GOOGLE_IDENTITY_TOOLKIT
	 * @return
	 */
	public static ApplicationActivityImportInstructions googleIdentityToolkitInstructions() {
		// TODO: this system should be able to modify/remove accounts by storing the access tokens
		String profileUrl = "http://www.trailnetwork.com/account/"
				+ ApplicationActivityImportInstructions.FIELD.PARAM.ACCOUNT_ID;
		return ApplicationActivityImportInstructions.create()
				.setApplication(KnownApplication.GOOGLE_IDENTITY_TOOLKIT.asApplication()).profilePage(profileUrl)
				.build();
	}

	/**
	 * Garmin Connect Activities that are full length GPS recorded workouts associated to a user's
	 * effort.
	 * 
	 * @return
	 */
	public static ApplicationActivityImportInstructions garminConnectInstructions() {

		// UrlBuilder didn't work here due to escaping of {activityId} by the lib.
		String baseActivityUrl = GARMIN_CONNECT_BASE_URI;
		String axmActivityUrl = baseActivityUrl + GARMIN_CONNECT_AXM_PATH + SystemWebApiUri.SLASH + ACTIVITY_ID;
		String gpxActivityUrl = baseActivityUrl + GARMIN_CONNECT_GPX_PATH + SystemWebApiUri.SLASH + ACTIVITY_ID
				+ "?full=true";
		String htmlUrl = GARMIN_CONNECT_ACTIVITY_BASE_PATH + ACTIVITY_ID;
		final String exploreEndpoint = "http://connectapi.garmin.com/activity-search-service-1.1/json/explore";
		final String andIgnoreUntitledPrefix = "&ignoreUntitled=";
		final String andIgnoreUntitled = andIgnoreUntitledPrefix + Boolean.TRUE;
		final String andDontIgnoreUntitled = andIgnoreUntitledPrefix + Boolean.FALSE;
		final String andPreviousDaysPrefix = "&previousDays=";
		final String andPreviousDaysAgo = andPreviousDaysPrefix + SINCE_DAYS_AGO;
		final String andLimit = "&limit=20";
		final String andCurrentPage = "&currentPage=" + FILTER_PAGE;
		return new ApplicationActivityImportInstructions.Builder()
				.addActivityUrl(gpxActivityUrl, DocumentFormatKey.GPX_1_1)
				.addActivityUrl(axmActivityUrl, DocumentFormatKey.AXM_1_0)
				.setApplication(KnownApplication.GARMIN_CONNECT.asApplication())
				.setSearchDestination(new Destination(exploreEndpoint + "?bbMaxLat=" //
						+ MAX_LAT + "&bbMaxLong="//
						+ MAX_LON + "&bbMinLat=" //
						+ MIN_LAT + "&bbMinLong="//
						+ MIN_LON + andCurrentPage + andLimit + andIgnoreUntitled + andPreviousDaysPrefix + "10000",
						DocumentFormatKey.GC_SEARCH_JSON))
				.setActivitySearchForAccountDestination(new Destination(exploreEndpoint + "?userId=" //
						+ ACCOUNT_REMOTE_ID + andCurrentPage + andLimit + andDontIgnoreUntitled + andPreviousDaysAgo,
						DocumentFormatKey.GC_SEARCH_JSON))
				/* human destination. */
				.webUrl(htmlUrl).profilePage("http://connect.garmin.com/modern/profile/" + ACCOUNT_ALIAS)
				/** HIKE */
				.addActivityTypeMapping(gcActivityType(HIKING), ActivityType.HIKE)
				.addActivityTypeMapping(gcActivityType(MOUNTAINEERING), ActivityType.HIKE)
				.addActivityTypeMapping(gcActivityType(SNOW_SHOE), ActivityType.HIKE)
				/** BIKE */
				.addActivityTypeMapping(gcActivityType(CYCLOCROSS), ActivityType.BIKE)
				.addActivityTypeMapping(gcActivityType(ROAD_BIKING), ActivityType.ROAD)
				.addActivityTypeMapping(gcActivityType(MOUNTAIN_BIKING), ActivityType.MTB)
				.addActivityTypeMapping(gcActivityType(CYCLING), ActivityType.BIKE)
				.addActivityTypeMapping(gcActivityType(DOWNHILL_BIKING), ActivityType.MTB)
				.addActivityTypeMapping(gcActivityType(RECUMBENT_CYCLING), ActivityType.BIKE)
				.addActivityTypeMapping(gcActivityType(TRACK_CYCLING), ActivityType.BIKE)
				/** RUN */
				.addActivityTypeMapping(gcActivityType(RUNNING), ActivityType.RUN)
				.addActivityTypeMapping(gcActivityType(STREET_RUNNING), ActivityType.RUN)
				.addActivityTypeMapping(gcActivityType(TRACK_RUNNING), ActivityType.RUN)
				.addActivityTypeMapping(gcActivityType(TRAIL_RUNNING), ActivityType.RUN)
				.addActivityTypeMapping(gcActivityType(TRAIL_RUNNING), ActivityType.RUN)
				/** WALK */
				.addActivityTypeMapping(gcActivityType(WALKING), ActivityType.WALK)
				.addActivityTypeMapping(gcActivityType(SPEED_WALKING), ActivityType.WALK)
				.addActivityTypeMapping(gcActivityType(CASUAL_WALKING), ActivityType.WALK)
				/** SKI */
				.addActivityTypeMapping(gcActivityType(CROSS_COUNTRY_SKIING), ActivityType.SKI)
				.addActivityTypeMapping(gcActivityType(RESORT_SKIING_SNOWBOARDING), ActivityType.SKI)
				.addActivityTypeMapping(gcActivityType(BACKCOUNTRY_SKIING_SNOWBOARDING), ActivityType.SKI)
				.addActivityTypeMapping(gcActivityType(SKATE_SKIING), ActivityType.SKI)
				/** SKATE */
				.addActivityTypeMapping(gcActivityType(INLINE_SKATING), ActivityType.SKATE)
				.addActivityTypeMapping(gcActivityType(SKATING), ActivityType.SKATE)
				/** SWIM */
				.addActivityTypeMapping(gcActivityType(SWIMMING), ActivityType.SWIM)
				.addActivityTypeMapping(gcActivityType(OPEN_WATER_SWIMMING), ActivityType.SWIM)
				.addActivityTypeMapping(gcActivityType(LAP_SWIMMING), ActivityType.SWIM)
				.addActivityTypeMapping(gcActivityType(SWIMMING), ActivityType.SWIM)

				.setConnectionTimeoutInMillis(LONGER_TIMEOUT).build();
	}

	private static String gcActivityType(GarminConnectActivitiesFilter.ActivityType activityType) {
		return GC_ACTIVITY_TYPE_PREFIX + GarminConnectActivitiesFilter.formatActivityType(activityType);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ApplicationActivityUrlProvider#applicationUrl(com.aawhere.activity.
	 * ActivityId)
	 */
	@Override
	public String applicationUrl(ActivityId activityId) {
		String result;
		try {
			final ApplicationKey applicationKey = activityId.getApplicationKey();
			ApplicationActivityImportInstructions instructionsFor = this.instructionsFor(applicationKey);
			final String webUrl = instructionsFor.webUrl();
			if (webUrl == null) {
				result = null;
			} else {
				result = UriBuilderUtil
						.build(	webUrl,
								new MapBuilder<String, String>()
										.put(	ApplicationActivityImportInstructions.FIELD.ACTIVITY_ID,
												activityId.getRemoteId()).build());
			}
		} catch (ApplicationImportInstructionsNotFoundException e) {
			result = null;
		}
		return result;

	}

	/**
	 * Given an account this will find the associated application for the account and provide the
	 * profile destination for the account holder at the {@link Application}.
	 * 
	 * @param account
	 * @return
	 * @throws ApplicationImportInstructionsNotFoundException
	 */
	@Override
	public String profilePageUrl(Account account) throws ApplicationImportInstructionsNotFoundException {
		AccountId id = account.id();
		ApplicationKey applicationKey = id.getApplicationKey();
		ApplicationActivityImportInstructions instructions = this.instructionsFor(applicationKey);
		String profilePage = instructions.getProfilePage();
		if (profilePage == null) {
			throw new ApplicationImportInstructionsNotFoundException(applicationKey);
		}
		return KeyedMessageFormat.create(profilePage).forgiveExtraParameters()
				.put(ApplicationActivityImportInstructions.FIELD.ACCOUNT_REMOTE_ID, account.remoteId())
				.put(ApplicationActivityImportInstructions.FIELD.ACCOUNT_ALIAS, account.alias())
				.put(ApplicationActivityImportInstructions.FIELD.ACCOUNT_ID, account.id()).build()
				.getFormattedMessage();
	}

	public Boolean accountUpdatable(AccountId accountId) {
		try {
			return instructionsFor(accountId.getApplicationKey()).accountDestination() != null;
		} catch (ApplicationImportInstructionsNotFoundException e) {
			return false;
		}
	}

	/**
	 * 
	 */
	public static Builder create() {
		return new Builder();
	}

}
