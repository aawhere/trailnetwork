/**
 *
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.field.FieldKey;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.xml.XmlNamespace;

/**
 * Used to query for {@link ActivityImportFailure}s from the {@link ActivityImportFailures}.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivityImportFailureFilterBuilder
		extends BaseActivityImportHistoryFilterBuilder<ActivityImportFailureFilterBuilder> {

	public ActivityImportFailureFilterBuilder() {
		super();
	}

	public static ActivityImportFailureFilterBuilder create() {
		return new ActivityImportFailureFilterBuilder();
	}

	public ActivityImportFailureFilterBuilder equalsProcessorVersion(ActivityImportProcessorVersion processorVersion) {
		return addCondition(new FilterCondition.Builder<ActivityImportProcessorVersion>()
				.field(ActivityImportFailure.FIELD.KEY.VERSION).operator(FilterOperator.EQUALS).value(processorVersion)
				.build());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.BaseActivityImportHistoryFilterBuilder#getActivityKey()
	 */
	@Override
	protected FieldKey getActivityKey() {
		return ActivityImportFailure.FIELD.KEY.ACTIVITY_ID;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ImportHistoryFilterBuilder#getStatusKey()
	 */
	@Override
	protected FieldKey getStatusKey() {
		return ActivityImportFailure.FIELD.KEY.STATUS;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.ImportHistoryFilterBuilder#getErrorCodeKey()
	 */
	@Override
	protected FieldKey getErrorCodeKey() {
		return ActivityImportFailure.FIELD.KEY.ERROR_CODE;
	}

}
