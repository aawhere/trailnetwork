/**
 * 
 */
package com.aawhere.activity.imprt.everytrail;

import com.aawhere.doc.DocumentFactory;

/**
 * @author aroller
 * 
 */
public class EveryTrailDocumentUtil {

	/**
	 * Registers every document necessary except the GPXX document producer not yet available int
	 * his project.
	 * 
	 * @param documentFactoryBuilder
	 */
	public static void registerAll(DocumentFactory.Builder documentFactoryBuilder) {
		documentFactoryBuilder.register(new EveryTrailActivityDocumentProducer());
		documentFactoryBuilder.register(new EveryTrailSearchDocumentProducer());
		documentFactoryBuilder.register(EveryTrailUserDocument.producer());
	}
}
