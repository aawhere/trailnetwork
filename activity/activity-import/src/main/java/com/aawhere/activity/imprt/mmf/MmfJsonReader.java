/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.io.IOException;
import java.io.InputStream;

import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.io.IoException;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author aroller
 * @param <T>
 *            the type of object this reader reads.
 * 
 */
public class MmfJsonReader<T> {

	/**
	 * Used to construct all instances of MmfWorkoutJsonReader.
	 */
	public static class Builder<T>
			extends ObjectBuilder<MmfJsonReader<T>> {

		public Builder(MmfJsonReader<T> reader) {
			super(reader);
		}

		public Builder<T> type(Class<? extends T> type) {
			building.type = type;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("type", building.type);

		}

		@Override
		public MmfJsonReader<T> build() {
			MmfJsonReader<T> built = super.build();
			built.mapper = new ObjectMapper();
			built.mapper.setVisibilityChecker(built.mapper.getVisibilityChecker().withFieldVisibility(Visibility.ANY));
			return built;
		}

	}// end Builder

	public static <T> Builder<T> create() {
		return new Builder<T>(new MmfJsonReader<T>());
	}

	/** Use {@link Builder} to construct MmfWorkoutJsonReader */
	private MmfJsonReader() {
	}

	private Class<? extends T> type;
	private ObjectMapper mapper;

	public T read(String json) throws DocumentDecodingException {
		try {
			T t = mapper.readValue(json.getBytes(), type);
			return t;
		} catch (IOException e) {
			throw handle(e);
		}
	}

	/**
	 * @param e
	 * @throws DocumentDecodingException
	 */
	private DocumentContentsNotRecognizedException handle(IOException e) throws DocumentDecodingException {
		if (e instanceof JsonMappingException) {
			throw new DocumentDecodingException(e);
		} else {
			throw new DocumentContentsNotRecognizedException();
		}
	}

	/**
	 * @param is
	 * @return
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws IoException
	 */
	public T read(InputStream is) throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		try {
			return mapper.readValue(is, type);
		} catch (IOException e) {
			throw handle(e);
		}
	}

	/**
	 * @param is
	 * @return
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws IoException
	 */
	public T read(byte[] bytes) throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		try {
			return mapper.readValue(bytes, type);
		} catch (IOException e) {
			throw handle(e);
		}
	}
}
