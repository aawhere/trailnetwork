/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;

/**
 * @author aroller
 * 
 */
public class MapMyFitnessWorkoutFilterBuilder
		extends Filter.BaseFilterBuilder<MapMyFitnessWorkoutFilterBuilder> {

	public static MapMyFitnessWorkoutFilterBuilder create() {
		return new MapMyFitnessWorkoutFilterBuilder();
	}

	/** Used to query MMF for workouts for a single user. */
	public MapMyFitnessWorkoutFilterBuilder accountId(AccountId accountId) {
		return addCondition(FilterCondition.create().field(Account.FIELD.KEY.ACCOUNT_ID)
				.operator(FilterOperator.EQUALS).value(accountId).build());
	}
}
