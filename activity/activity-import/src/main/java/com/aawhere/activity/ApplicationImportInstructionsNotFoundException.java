/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.activity.ActivityImportMessage.Param;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates the {@link Application} is known, however, the instructions aren't found.
 * 
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class ApplicationImportInstructionsNotFoundException
		extends BaseException {

	private static final long serialVersionUID = -6987184379856426351L;

	/**
	 * 
	 */
	public ApplicationImportInstructionsNotFoundException(ApplicationKey applicationKey) {
		super(new CompleteMessage.Builder(ActivityImportMessage.IMPORT_INSTRUCTIONS_MISSING)
				.addParameter(Param.APPLICATION, applicationKey).build());
	}

	public static ApplicationImportInstructionsNotFoundException forQueue(ApplicationKey applicationKey) {
		ApplicationImportInstructionsNotFoundException e = new ApplicationImportInstructionsNotFoundException(
				applicationKey);
		e.setStatusCode(HttpStatusCode.ACCEPTED);
		return e;
	}
}
