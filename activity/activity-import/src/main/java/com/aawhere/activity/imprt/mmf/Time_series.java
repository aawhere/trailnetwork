package com.aawhere.activity.imprt.mmf;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Time_series
		implements Serializable {

	private static final long serialVersionUID = -8204655823841342239L;
	private List<Trackpoint> position;

	/**
	 * @return the position
	 */
	public List<Trackpoint> getPosition() {
		return this.position;
	}

}
