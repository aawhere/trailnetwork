/**
 *
 */
package com.aawhere.activity;

/**
 * Was not able to find a valid username from the Document.
 * 
 * 
 * @author Brian Chapman
 * 
 */
public class UserNameNotProvidedException
		extends ActivityDocumentDecodingException {

	/**
	 *
	 */
	private static final long serialVersionUID = 5054075975793211734L;

	/**
	 * @param message
	 */
	public UserNameNotProvidedException() {
		super(ActivityImportMessage.USERNAME_NOT_FOUND);
	}

}
