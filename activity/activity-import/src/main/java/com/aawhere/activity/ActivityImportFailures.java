/**
 *
 */
package com.aawhere.activity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

/**
 * A collection of {@link ActivityImportFailure}s providing the significant events that take place
 * during the attempts to import and process and Activity into the system.
 * 
 * @author aroller
 * 
 */
@XmlRootElement(name = ActivityImportFailures.FIELD.DOMAIN)
@XmlType(namespace = XmlNamespace.API_VALUE, name = ActivityImportFailures.FIELD.DOMAIN)
@XmlAccessorType(XmlAccessType.NONE)
public class ActivityImportFailures
		extends ImportMilestoneHistory<ActivityImportFailure> {

	@XmlElement(name = ActivityImportFailure.FIELD.DOMAIN)
	@Override
	public List<ActivityImportFailure> getCollection() {
		return super.getCollection();
	}

	/**
	 * Used to construct all instances of ActivityImports.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, ActivityImportFailure, ActivityImportFailures> {

		public Builder() {
			super(new ActivityImportFailures());
		}
	}// end Builder

	/** Use {@link Builder} to construct ActivityImports */
	private ActivityImportFailures() {
	}

	@XmlTransient
	public static final class FIELD {
		public static final String DOMAIN = "activityImportHistory";
	}
}
