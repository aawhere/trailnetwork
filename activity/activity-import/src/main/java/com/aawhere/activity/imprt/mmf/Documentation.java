package com.aawhere.activity.imprt.mmf;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Documentation {
	private String href;

	public String getHref() {
		return this.href;
	}

	public void setHref(String href) {
		this.href = href;
	}
}
