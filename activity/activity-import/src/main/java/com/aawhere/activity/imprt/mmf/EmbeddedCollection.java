/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;

/**
 * @author aroller
 * 
 * @param <I>
 *            the items in the collection
 */
public class EmbeddedCollection<I> {

	private _links _links;
	private Integer total_count;
	private I _embedded;

	/**
	 * @return the _links
	 */
	public _links get_links() {
		return this._links;
	}

	/**
	 * @param _links
	 *            the _links to set
	 */
	public void set_links(_links _links) {
		this._links = _links;
	}

	/**
	 * @return the total_count
	 */
	public Integer getTotal_count() {
		return this.total_count;
	}

	/**
	 * @param total_count
	 *            the total_count to set
	 */
	public void setTotal_count(Integer total_count) {
		this.total_count = total_count;
	}

	/**
	 * @return the _embedded
	 */
	public I get_embedded() {
		return this._embedded;
	}

	/**
	 * @param _embedded
	 *            the _embedded to set
	 */
	public void set_embedded(I _embedded) {
		this._embedded = _embedded;
	}

	public ApplicationKey applicationKey() {
		return KnownApplication.MMF.key;
	}

}
