/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.app.ApplicationKey;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Messages related to importing {@link Activities} into the system.
 * 
 * @author aroller
 * 
 */
public enum ActivityImportMessage implements Message {

	/** When a requested activity is not found at the originating application */
	REMOTE_ACTIVITY_NOT_FOUND("The activity {ID} was not found at {APPLICATION}", Param.ID, Param.APPLICATION),
	/** Used as a last resort if you don't have a better explanation */
	GENERAL_IMPORT_ERROR(
			"An error occurred during import of {ID} from {APPLICATION} that we aren't properly handling. This problem may be fixed in the future, but subsequent attempts will likely cause the same problem.",
			Param.ID,
			Param.APPLICATION),

	/** */
	USERNAME_NOT_FOUND("The username for the requested document was not found"),
	/** */
	MISSING_ACTIVITY_TYPE_FIELD("Activity Type was not provided and is required."),
	/** */
	MISSING_ACTIVITY_ID_FIELD("Activity ID was not provided and is required."),
	/** */
	MISSING_TIMESTAMP_FIELD("Timestamp was not provided and is required."),
	/** */
	INVALID_TIMESTAMP_VALUE("The timestamp '{VALUE}' is not in a recognizable format.", Param.VALUE),
	/** Used to indicate that expected format is known, but unable to decode. */

	/** Indicates a certain number of fields were expected, but another number was provided. */
	INVALID_FIELD_COUNT(
			"The document expects {EXPECTED} fields, but was {UNEXPECTED} for {VALUE}.",
			Param.VALUE,
			Param.EXPECTED,
			Param.UNEXPECTED),
	/**
	 * Indicates that an import was not attempted because there was a previous import attempt that
	 * failed and hasn't been fixed.
	 */
	PREVIOUS_FAILURE(
			"No import attempt will be made for {APPLICATION} activity {ID} since previous attempts failed.",
			Param.ID,
			Param.APPLICATION),
	/** The application requested does not have import instructions. */
	IMPORT_INSTRUCTIONS_MISSING(
			"The application {APPLICATION} is known, but it doesn't have any import instructions.",
			Param.APPLICATION),
	/**
	 * Identifier for a message translating
	 * 
	 */
	APPLICATION_URI("remote application URI");

	private ParamKey[] parameterKeys;
	private String message;

	private ActivityImportMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** The value of the ID */
		ID,
		/** TO present the value that was provided */
		VALUE,
		/** explains what actual was provided */
		UNEXPECTED,
		/** explains what was wanted. */
		EXPECTED,
		/**
		 * The {@link ApplicationKey} of an {@link Application} or the Application itself.
		 */
		APPLICATION;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
