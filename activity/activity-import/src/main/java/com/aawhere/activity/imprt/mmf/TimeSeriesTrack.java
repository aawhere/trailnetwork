/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.util.Iterator;

import org.joda.time.DateTime;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.track.SimpleTrackpoint;
import com.aawhere.track.Track;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author aroller
 * 
 */
public class TimeSeriesTrack
		implements Track {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7442215925782333031L;
	public DateTime start;
	public Time_series series;

	/**
	 * Used to construct all instances of TimeSeriesTrack.
	 */
	public static class Builder
			extends ObjectBuilder<TimeSeriesTrack> {

		public Builder() {
			super(new TimeSeriesTrack());
		}

		public Builder timeSeries(Time_series series) {
			building.series = series;
			return this;
		}

		public Builder start(DateTime start) {
			building.start = start;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("start", building.start);
			Assertion.exceptions().notNull("series", building.series);

		}

		@Override
		public TimeSeriesTrack build() {
			TimeSeriesTrack built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct TimeSeriesTrack */
	private TimeSeriesTrack() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Trackpoint> iterator() {
		return new Iterator<Trackpoint>() {
			Iterator<com.aawhere.activity.imprt.mmf.Trackpoint> iterator = series.getPosition().iterator();
			long startTimeInMillis = start.getMillis();

			@Override
			public void remove() {
				iterator.remove();
			}

			@Override
			public Trackpoint next() {
				com.aawhere.activity.imprt.mmf.Trackpoint next = iterator.next();
				Long absoluteTime = startTimeInMillis + next.offset;
				return TrackUtil.createPoint(absoluteTime, next.lat, next.lon, next.ele);
			}

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}
		};
	}

}
