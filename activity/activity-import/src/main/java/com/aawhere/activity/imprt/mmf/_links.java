package com.aawhere.activity.imprt.mmf;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class _links {
	private List<Activity_type> activity_type;
	private List<Documentation> documentation;
	private List<Privacy> privacy;
	private List<RouteLink> route;
	private List<Self> self;
	private List<User> user;
	private List<Self> next;

	public List<Activity_type> getActivity_type() {
		return this.activity_type;
	}

	public void setActivity_type(List<Activity_type> activity_type) {
		this.activity_type = activity_type;
	}

	public List<Documentation> getDocumentation() {
		return this.documentation;
	}

	public void setDocumentation(List<Documentation> documentation) {
		this.documentation = documentation;
	}

	public List<Privacy> getPrivacy() {
		return this.privacy;
	}

	public void setPrivacy(List<Privacy> privacy) {
		this.privacy = privacy;
	}

	public List<RouteLink> getRoute() {
		return this.route;
	}

	public void setRoute(List<RouteLink> route) {
		this.route = route;
	}

	public List<Self> getSelf() {
		return this.self;
	}

	public void setSelf(List<Self> self) {
		this.self = self;
	}

	public List<User> getUser() {
		return this.user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}

	/**
	 * @return the next
	 */
	public List<Self> getNext() {
		return this.next;
	}

	/**
	 * @param next
	 *            the next to set
	 */
	public void setNext(List<Self> next) {
		this.next = next;
	}
}
