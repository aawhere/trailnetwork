/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import com.aawhere.activity.ActivityImportId;
import com.aawhere.app.ApplicationKey;

/**
 * @author aroller
 * 
 */
public class RoutesResultsMmfJson
		extends EmbeddedCollection<RoutesMmfJson> {

	/**
	 * This is provided to avoid collisions with the main MMF activities. These routes aren't first
	 * class so the rest of the system should need to reference this.
	 * 
	 * @see ActivityImportId.NotWorthImporting
	 */
	static final ApplicationKey KEY = new ApplicationKey("mmr");

	/*
	 * @see com.aawhere.activity.imprt.mmf.EmbeddedCollection#applicationKey()
	 */
	@Override
	public ApplicationKey applicationKey() {
		return KEY;

	}

}
