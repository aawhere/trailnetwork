/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.util.List;

/**
 * @author aroller
 * 
 */
public class Workouts {

	private List<Workout> workouts;

	/**
	 * @return the workouts
	 */
	public List<? extends Workout> getWorkouts() {
		return this.workouts;
	}

	/**
	 * @param workouts
	 *            the workouts to set
	 */
	public void setWorkouts(List<Workout> workouts) {
		this.workouts = workouts;
	}

}
