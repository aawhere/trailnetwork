/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import java.util.List;

/**
 * @author aroller
 * 
 */
public class RoutesMmfJson
		extends Workouts {

	private List<RouteMmfJson> routes;

	public List<RouteMmfJson> getRoutes() {
		return this.routes;
	}

	public void setRoutes(List<RouteMmfJson> routes) {
		this.routes = routes;
	}

	/*
	 * @see com.aawhere.activity.imprt.mmf.Workouts#getWorkouts()
	 */
	@Override
	public List<? extends Workout> getWorkouts() {
		return getRoutes();
	}

}
