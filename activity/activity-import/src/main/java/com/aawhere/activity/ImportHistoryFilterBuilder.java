/**
 *
 */
package com.aawhere.activity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.activity.ImportMilestone.Status;
import com.aawhere.field.FieldKey;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.util.rb.Message;
import com.aawhere.xml.XmlNamespace;

/**
 * Used to query for {@link ImportMilestone}s from the {@link ImportMilestoneHistory}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class ImportHistoryFilterBuilder<FilterBuilderT extends ImportHistoryFilterBuilder<FilterBuilderT>>
		extends Filter.BaseFilterBuilder<FilterBuilderT> {

	public ImportHistoryFilterBuilder() {
		super();
	}

	public FilterBuilderT equalsStatus(Status status) {
		return addCondition(new FilterCondition.Builder<Status>().field(getStatusKey()).operator(FilterOperator.EQUALS)
				.value(status).build());
	}

	/**
	 * Messages are used for queryable codes. This will filter only those that match the
	 * {@link Enum#name()}
	 * 
	 * @param message
	 */
	public FilterBuilderT equalsCode(Enum<? extends Message> message) {
		return addCondition(FilterCondition.create().field(getErrorCodeKey()).operator(FilterOperator.EQUALS)
				.value(message.name()).build());
	}

	protected abstract FieldKey getStatusKey();

	protected abstract FieldKey getErrorCodeKey();

}
