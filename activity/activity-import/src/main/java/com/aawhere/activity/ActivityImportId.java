/**
 * 
 */
package com.aawhere.activity;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.UnknownApplicationException;

/**
 * A flag on an {@link ActivityId} that indicates if it's worth importing. Allows for reporting of
 * finding an ActivityId, but allows it to be filtered out.
 * 
 * @author aroller
 * 
 */
public interface ActivityImportId {

	/**
	 * 
	 * @return true if pre-checks passed indicating this activity might import.
	 */
	Boolean isWorthImporting();

	/**
	 * Provides the common case when the ActivityId provided is not worth importing.
	 * 
	 * @author aroller
	 * 
	 */
	public static class NotWorthImporting
			extends ActivityId
			implements ActivityImportId {

		private static final long serialVersionUID = -9032112937154976768L;

		public NotWorthImporting() {
			super();
		}

		public NotWorthImporting(ApplicationKey applicationKey, String remoteId) {
			super(applicationKey, remoteId);
		}

		public NotWorthImporting(String value) throws UnknownApplicationException {
			super(value);
		}

		@Override
		public Boolean isWorthImporting() {
			return false;
		}

	}
}
