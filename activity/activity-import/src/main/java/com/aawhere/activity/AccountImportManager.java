/**
 * 
 */
package com.aawhere.activity;

import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountWebApiUri;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.persist.Filter;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.RepeatFrequency;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Function;
import com.google.common.base.Supplier;

/**
 * Accounts must stay updated retrieving activities, updating usernames, privacy settings, friends,
 * etc. This manages the scheduling of tasks related to account specific settings using the import
 * services to keep everything updated.
 * 
 * The first page of crawling is treated differently than the rest. The most recent activities is
 * retrieved from the datastore to see if we know about this user already and if so how recent is
 * the known activity. That is used to limit the request to only new activities to save resources
 * remotely and locally.
 * 
 * The account will continued to be crawled by the crawler until the crawler hits a page with no new
 * activities. See the crawler for more details.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class AccountImportManager {

	/** How often the account should be inspected for updates. */
	public static enum RefreshRate {
		OFTEN(RepeatFrequency.DAILY),
		OCCASIONALLY(RepeatFrequency.WEEKLY),
		RARELY(RepeatFrequency.MONTHLY),
		NEVER(null);
		public final RepeatFrequency repeatFrequency;

		private RefreshRate(RepeatFrequency repeatFrequency) {
			this.repeatFrequency = repeatFrequency;
		}
	}

	@Nullable
	@XmlElement
	private Activities mostRecentActivities;

	@Nonnull
	@XmlElement
	private AccountId accountId;

	@Nullable
	@XmlElement
	private DateTime mostRecentStartTime;

	@XmlElement
	@Nullable
	private RefreshRate refreshRate;

	/** The task that may be scheduled for a re-visit in the future for refresh crawl. */
	@XmlElement
	private Task refreshTask;

	@XmlElement
	private Filter filter;
	@XmlElement
	@Nonnull
	private ActivityImportCrawler crawler;

	@XmlAttribute
	private Boolean deepRequested;

	@XmlAttribute
	private Boolean deepChosenByManager;

	/**
	 * Used to construct all instances of AccountImportScheduler.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<AccountImportManager> {

		/**
		 * 
		 */
		private static final String SCHEDULED_QUEUE_NAME_SUFFIX = "delay";
		@Nonnull
		private QueueFactory queueFactory;
		@Nonnull
		private Function<Set<ActivityId>, Iterable<ActivityId>> nonExistingIdsFunction;
		@Nonnull
		private ApplicationActivityImportInstructionsRepository instructionsRepository;
		@Nonnull
		private Supplier<Activities> mostRecentActivitiesSupplier;

		@Nonnull
		private Function<ActivityImportCrawlerDestination, DocumentRetriever> retrieverFunction;

		public Builder() {
			super(new AccountImportManager());
		}

		public Builder queueFactory(QueueFactory queueFactory) {
			this.queueFactory = queueFactory;
			return this;
		}

		public Builder retrieverFunction(Function<ActivityImportCrawlerDestination, DocumentRetriever> function) {
			this.retrieverFunction = function;
			return this;
		}

		public Builder mostRecentActivities(Supplier<Activities> mostRecentActivitiesSupplier) {
			this.mostRecentActivitiesSupplier = mostRecentActivitiesSupplier;
			return this;
		}

		public Builder nonExistingIdsFunction(Function<Set<ActivityId>, Iterable<ActivityId>> nonExistingIdsFunction) {
			this.nonExistingIdsFunction = nonExistingIdsFunction;
			return this;
		}

		public Builder accountId(AccountId accountId) {
			building.accountId = accountId;
			return this;
		}

		public Builder instructionsRepository(ApplicationActivityImportInstructionsRepository instructionsRepository) {
			this.instructionsRepository = instructionsRepository;
			return this;
		}

		Queue getActivityImportQueue() {
			return queueFactory.getQueue(ActivityWebApiUri.ACTIVITY_IMPORT_QUEUE_NAME);
		}

		@Override
		public AccountImportManager build() {
			try {
				return manage();
			} catch (BaseException e) {
				throw ToRuntimeException.wrapAndThrow(e);
			}
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("instructionsRepository", this.instructionsRepository);
			Assertion.exceptions().notNull("accountId", building.accountId);
			Assertion.exceptions().notNull("nonExistingIdsFunction", this.nonExistingIdsFunction);
			Assertion.exceptions().notNull("queueFactory", this.queueFactory);
			Assertion.exceptions().notNull("mostRecentActivitiesSupplier", this.mostRecentActivitiesSupplier);
			Assertion.exceptions().notNull("filter", building.filter);

		}

		/**
		 * just like {@link #build()}, but reports expections
		 * 
		 * @return
		 * @throws ApplicationImportInstructionsNotFoundException
		 * @throws BaseException
		 */
		private AccountImportManager manage() throws ApplicationImportInstructionsNotFoundException, BaseException {
			AccountImportManager built = super.build();

			boolean deep = ActivityImportUtil.deep(built.filter);
			built.deepRequested = deep;

			// the first page is when some decisions to go deep are made
			// if we don't yet know this account we should go deep
			if (built.filter.isFirstPage()) {

				built.mostRecentActivities = mostRecentActivitiesSupplier.get();
				if (CollectionUtils.isNotEmpty(built.mostRecentActivities)) {
					Activity mostRecent = built.mostRecentActivities.iterator().next();
					DateTime startTime = mostRecent.getTrackSummary().getStartTime();
					built.mostRecentStartTime = startTime;
				} else {
					// if this is a new account then go deep
					deep = true;
					built.deepChosenByManager = true;
				}
			}

			// use a since date of the most recent known modification
			// if its deep then go back before GPS
			// notice if this is page two of activities most
			DateTime since = (deep || built.mostRecentStartTime == null) ? new DateTime(0) : built.mostRecentStartTime;
			// setup the destination for account retrieval
			ActivityImportCrawlerDestination destination = ActivityImportCrawlerDestination.create().since(since)
					.accountId(built.accountId).filter(built.filter)
					.instructions(this.instructionsRepository.instructionsFor(built.accountId)).build();

			Queue crawlQueue = crawlQueue(built);
			built.crawler = ActivityImportCrawler.create().retriever(retrieverFunction.apply(destination)).deep(deep)
					.activityImportQueue(getActivityImportQueue()).crawlerDestination(destination)
					.nonExistingIdsFunction(this.nonExistingIdsFunction).crawlQueue(crawlQueue).crawl();

			// the crawler has finished, update the most recent start time for the better of the
			// two:
			LocalDate mostRecentStartDate = built.crawler.mostRecentStartDate();

			// most recent may come from the activities we already know about OR the ones just found
			if (mostRecentStartDate != null) {
				built.mostRecentStartTime = JodaTimeUtil.moreRecent(mostRecentStartDate.toDateTimeAtStartOfDay(),
																	built.mostRecentStartTime);
			}

			// determine the refresh rate only if it's the first page
			if (built.filter.isFirstPage()) {
				// this check uses the results of the crawler
				chooseRefreshRate(built);
				addRefreshTask(built);
			}

			return built;
		}

		/**
		 * Based on the refresh frequency this will post a task to revisit this manager when we feel
		 * its worthy of looking.
		 * 
		 * @param built
		 */
		private void addRefreshTask(AccountImportManager built) {
			if (built.refreshRate != null && !built.refreshRate.equals(RefreshRate.NEVER)) {

				built.refreshTask = Task.clone(ActivityWebApiUri.accountEndpoint(built.accountId))
						.setCountdown(built.refreshRate.repeatFrequency)
						// exclude the version so the delay will always hit the upgraded server
						.setVersionExcluded().build();
				crawlQueue(built, SCHEDULED_QUEUE_NAME_SUFFIX).add(built.refreshTask);
			}
		}

		/**
		 * @param built
		 * @return
		 */
		private Queue crawlQueue(AccountImportManager built, String... suffixes) {
			return AccountWebApiUri.getCrawlQueue(this.queueFactory, built.accountId.getApplicationKey(), suffixes);
		}

		/**
		 * Based on a few factors related to the {@link AccountImportManager#mostRecentStartTime}
		 * and the results of the {@link AccountImportManager#crawler} this will choose a refresh
		 * rate that will determine when the task queue should check again.
		 * 
		 * @param built
		 */
		private void chooseRefreshRate(AccountImportManager built) {

			DateTime mostRecentStartDate = built.mostRecentStartTime;

			// most recent start date indicates something was found either existing or incoming
			if (mostRecentStartDate != null) {

				// if the manager choose to go deep then we should check back soon after imports
				if (BooleanUtils.isTrue(built.deepChosenByManager)) {
					built.refreshRate = RefreshRate.OFTEN;
				} else {
					DateTime now = DateTime.now();
					// confirmed an activity exists...refresh based on past frequency
					if (mostRecentStartDate.isBefore(now.minusYears(1))) {
						built.refreshRate = RefreshRate.NEVER;
					} else if (mostRecentStartDate.isBefore(now.minusDays(30))) {
						// nothing in the past month...no need to rush for a refresh
						built.refreshRate = RefreshRate.RARELY;
					} else if (mostRecentStartDate.isBefore(now.minusDays(7))) {
						// not a frequent user...let's not try too hard
						built.refreshRate = RefreshRate.OCCASIONALLY;
					} else {
						// upload within the past week...worth checking often
						built.refreshRate = RefreshRate.OFTEN;
					}
				}
			} else {
				// no activities before nor any now...maybe just a user or all private.
				built.refreshRate = RefreshRate.NEVER;
			}
		}

		/**
		 * @param build
		 * @return
		 */
		public Builder filter(Filter filter) {
			building.filter = filter;
			return this;
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct AccountImportScheduler */
	private AccountImportManager() {
	}

	/**
	 * @return
	 */
	public AccountId accountId() {
		return this.accountId;
	}

	/**
	 * @return the mostRecentStartTime
	 */
	public DateTime mostRecentStartTime() {
		return this.mostRecentStartTime;
	}

	/**
	 * @return the refreshRate
	 */
	public RefreshRate refreshRate() {
		return this.refreshRate;
	}

	/**
	 * @return the refreshTask
	 */
	public Task refreshTask() {
		return this.refreshTask;
	}

	/**
	 * @return the filter
	 */
	public Filter filter() {
		return this.filter;
	}

	/**
	 * @return the crawler
	 */
	public ActivityImportCrawler crawler() {
		return this.crawler;
	}

	/**
	 * @return the deepChosenByManager
	 */
	public Boolean deepChosenByManager() {
		return this.deepChosenByManager;
	}

	/**
	 * @return the deepRequested
	 */
	public Boolean deepRequested() {
		return this.deepRequested;
	}

	/**
	 * @return the lastKnownActivities
	 */
	public Activities mostRecentKnownActivities() {
		return this.mostRecentActivities;
	}

}
