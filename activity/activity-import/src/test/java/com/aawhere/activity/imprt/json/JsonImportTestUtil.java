/**
 * 
 */
package com.aawhere.activity.imprt.json;

import static org.junit.Assert.*;

import java.io.InputStream;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class JsonImportTestUtil {

	/**
	 * Standard loader of the filename path that is loaded as a system resource and the document is
	 * created.
	 * 
	 * FIXME: this seems generic enough to be in Document.
	 * 
	 * @param producer
	 * @param resourcePath
	 * @return
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 */
	@SuppressWarnings("unchecked")
	public static <D extends Document> D assertFile(DocumentProducer producer, String resourcePath,
			Class<D> expectedDocumentType, DocumentFormatKey expectedFormat)
			throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		InputStream is = ClassLoader.getSystemResourceAsStream(resourcePath);
		assertNotNull(resourcePath + " not found", is);
		Document doc = producer.produce(is);
		TestUtil.assertInstanceOf(expectedDocumentType, doc);
		assertEquals(expectedFormat, doc.getDocumentFormat());
		return (D) doc;
	}
}
