/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.activity.ApplicationActivityImportInstructions.FIELD.PARAM.*;
import static org.junit.Assert.*;

import java.net.URL;

import org.joda.time.DateTime;
import org.junit.Test;

import com.aawhere.activity.ApplicationActivityImportInstructions.Destination;
import com.aawhere.app.Application;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.persist.Filter;
import com.aawhere.queue.Task;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @see ActivityImportCrawlerDestination
 * 
 * @author aroller
 * 
 */
public class ActivityImportCrawlerAccountDestinationUnitTest {

	// build a fake url that uses all items that can get replaced by the destination.
	final String baseDestinationUrl = "http://someapplication.com/activities/";
	private String andPreviousDaysEquals = "&previousDays=";
	final String destinationUrlTemplate = baseDestinationUrl + ACCOUNT_REMOTE_ID + "?page=" + FILTER_PAGE + "&start="
			+ FILTER_START + "&since=" + SINCE_TIMESTAMP + andPreviousDaysEquals + SINCE_DAYS_AGO;
	Destination destination = new Destination(destinationUrlTemplate, DocumentFormatKey.GC_SEARCH_JSON);
	final Application application = KnownApplication.UNKNOWN.asApplication();
	ApplicationActivityImportInstructions instructions = ApplicationActivityImportInstructions.create()
			.addActivityUrl(destinationUrlTemplate, DocumentFormatKey.GPX)
			.setActivitySearchForAccountDestination(destination).setApplication(application).build();

	AccountId accountId = AccountTestUtil.accountId(application.getKey());
	Integer pageNumber = TestUtil.generateRandomInt();
	Filter filter = Filter.create().setPage(pageNumber).build();
	int daysAgo = 3;
	DateTime since = DateTime.now().minusDays(daysAgo);
	ActivityImportCrawlerDestination crawlerDestination = ActivityImportCrawlerDestination.create()
			.accountId(accountId).instructions(instructions).since(since).filter(filter).build();

	final URL url = crawlerDestination.url();
	final String urlString = url.toString();

	@Test
	public void testAccountId() {

		assertNotNull(url);
		TestUtil.assertContains(urlString, pageNumber.toString());
		TestUtil.assertContains(urlString, accountId.getRemoteId());
		TestUtil.assertContains(urlString, baseDestinationUrl);
		// TN-809 start is zero-based
		final Integer startZeroBased = filter.getStart() - 1;
		TestUtil.assertContains(urlString, startZeroBased.toString());
		assertTrue(crawlerDestination.isCrawlingByAccount());
		assertFalse(crawlerDestination.isCrawlingByCells());
	}

	@Test
	public void testSinceTimestamp() {
		TestUtil.assertContains(urlString, JodaTimeUtil.toIsoXmlFormat(since));
		TestUtil.assertContains(urlString, andPreviousDaysEquals + daysAgo);

	}

	@Test
	public void testNextPage() {
		Task nextPageTask = crawlerDestination.nextPageTask();
		final String uri = nextPageTask.getEndPoint().getUri().toString();
		TestUtil.assertContains(uri, String.valueOf(pageNumber + 1));
	}

	@Test
	public void testJaxb() {
		JAXBTestUtil<ActivityImportCrawlerDestination> testUtil = JAXBTestUtil.create(crawlerDestination)
				.adapter(ActivityTestUtil.adapters()).build();
		testUtil.assertContains(baseDestinationUrl);
	}

	public static ActivityImportCrawlerAccountDestinationUnitTest create() {
		return new ActivityImportCrawlerAccountDestinationUnitTest();
	}
}
