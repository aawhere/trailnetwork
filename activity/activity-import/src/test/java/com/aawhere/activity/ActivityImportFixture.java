/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.activity.ActivityIdFixture.*;

import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;

/**
 * @author aroller
 * 
 */
public class ActivityImportFixture {

	public static final ActivityId GC_APPLICATION_ACTIVITY_ID = new ActivityId(KnownApplication.GARMIN_CONNECT.key,
			GC_ACTIVITY_ID);
	public static final ActivityId GC_NOT_FOUND_ACTIVITY_ID = new ActivityId(KnownApplication.GARMIN_CONNECT.key,
			GC_NOT_FOUND_ID);
	public static final ActivityId TWO_PEAK_APPLICATION_ACTIVITY_ID = new ActivityId(
			KnownApplication.TWO_PEAK_ACTIVITIES.key, TWO_PEAK_ACTIVITY_ID);
	public static final ActivityId TWO_PEAK_SECTION_ACTIVITY_ID = new ActivityId(
			KnownApplication.TWO_PEAK_SECTIONS.key, TWO_PEAK_SECTION_ID);

	public static final ActivityId GC_UNAUTHORIZED_ACTIVITY_ID = new ActivityId(KnownApplication.GARMIN_CONNECT.key,
			GC_PRIVATE_ID);

	public static final ActivityId RW_EXAMPLE_ACTIVITY = new ActivityId(KnownApplication.RWGPS.key, "94");
	public static final AccountId AROLLER_TEST_GC_ALIAS = new AccountId(KnownApplication.GARMIN_CONNECT.key,
			"arollertest");
	public static final AccountId AROLLER_TEST_GC_USER_ID = new AccountId(KnownApplication.GARMIN_CONNECT.key,
			"1015102");
	public static final AccountId ZEBATHON_MMF_ACCOUNT_ID = new AccountId(KnownApplication.MMF.key, "971924");
	public static final AccountId CULLEN_RW_ACCOUNT_ID = new AccountId(KnownApplication.RWGPS.key, "1");
	public static final int HEART_ACTIVITY_NUMBER_OF_INSTALLATIONS = 4;

	/**
	 * Aaron's public activity around the hut. Short and sweet.
	 * http://www.everytrail.com/view_trip.php?trip_id=2121681
	 */
	public static final ActivityId EVERY_TRAIL_EXAMPLE = new ActivityId(KnownApplication.EVERY_TRAIL.key,
			ActivityIdFixture.EVERY_TRAIL);

	public static final String TENNESSEE_VALLEY_GEO_CELL_VALUE = "8e62f3d0";
}
