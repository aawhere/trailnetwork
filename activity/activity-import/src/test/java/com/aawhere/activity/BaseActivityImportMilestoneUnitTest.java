/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ImportMilestone.Status;
import com.aawhere.id.Identifier;
import com.aawhere.lang.exception.ExampleException;
import com.aawhere.persist.BaseEntityBaseUnitTest;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.Repository;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.MessageTestUtil;

/**
 * Base class for testing ActivityImportMilestones.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class BaseActivityImportMilestoneUnitTest<MilestoneT extends BaseActivityImportMilestone<MilestoneT, IdentifierT>, IdentifierT extends Identifier<String, MilestoneT>, BuilderT extends BaseActivityImportMilestone.Builder<BuilderT, MilestoneT, IdentifierT>, R extends Repository<MilestoneT, IdentifierT>>
		extends BaseEntityJaxbBaseUnitTest<MilestoneT, BuilderT, IdentifierT, R> {

	private static final String ROOT_CAUSE_MESSAGE = "this is the root cause";
	protected ActivityImportFailure.Status status;
	private ExampleException rootCause;
	private ActivityImportException cause;
	protected ActivityId activityId;

	@Before
	public void setBaseUp() {
		status = Status.SUCCEEDED;
		rootCause = new ExampleException(ROOT_CAUSE_MESSAGE);
		this.activityId = ActivityTestUtil.createRandomActivityId();
		this.cause = new ActivityImportException.Builder().generalError().setActivityId(this.activityId)
				.setCause(rootCause).build();
	}

	/**
	 *
	 */
	public BaseActivityImportMilestoneUnitTest() {
		super(BaseEntityBaseUnitTest.NON_MUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere
	 * .persist.BaseEntity.Builder)
	 */
	@Override
	protected void creationPopulation(BuilderT builder) {
		super.creationPopulation(builder);
		builder.setStatus(status);
		builder.setActivityId(activityId);
		builder.setCause(cause);
	}

	/*
	 * (non-Javadoc) /* (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	public void assertCreation(MilestoneT sample) {

		super.assertCreation(sample);

		assertEquals(status, sample.getStatus());
		assertEquals(activityId, sample.getActivityId());
		TestUtil.assertContains(sample.getDetails(), cause.getMessage());
		assertEquals(cause.getErrorCode(), ActivityImportMessage.GENERAL_IMPORT_ERROR.toString());
		assertEquals(rootCause.getMessage(), cause.getCause().getMessage());
		TestUtil.assertContains(sample.getDetails(), ROOT_CAUSE_MESSAGE);
	}

	/**
	 * Just a quick check to make sure only messages are being saved when a known error occurs since
	 * the unknown is in the main test
	 */
	@Test
	public void testHandledException() {

		ActivityImportException exception = new ActivityImportException.Builder().setActivityId(activityId)
				.previousImportFailed().build();
		ActivityImportFailure milestone = new ActivityImportFailure.Builder().setActivityId(activityId)
				.setCause(exception).setStatus(status).build();
		TestUtil.assertContains(milestone.getDetails(), activityId.getRemoteId());
		// stack traces shouldn't be included
		TestUtil.assertDoesntContain(milestone.getDetails(), "com.aawhere.");
	}

	@Test
	public void testMessages() {
		MessageTestUtil.assertMessages(ActivityImportMessage.class);
	}

}
