/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import com.aawhere.activity.ActivityId;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;

/**
 * @author aroller
 * 
 */
public class MapMyFitnessTestUtil {

	public static final AccountId TEST_ACCOUNT = new AccountId(KnownApplication.MMF.key, "21830");
	public static final String TEST_ACCOUNT_USERNAME = "trailblazer";
	public static final String TEST_ACCOUNT_NAME = "Aaron Roller";
	/** Activity owned by trailblazer" */
	public static final ActivityId TEST_ACTIVITY_ID = new ActivityId(KnownApplication.MMF.key, "67052788");
	public static final String WALK_DEFAULT_NAME = "Walked 1.00 mi on 4/16/13";
	/** This was apparently their naming scheme in 2012. */
	public static final String RUN_DEFAULT_NAME_WITH_TIME = "10.19 mi Run on Sep 6, 2012 9:29 PM";
	/** Produced from mm-768310391 */
	public static final String NAME_DEFAULT_WORD_MONTH = "Walk on Oct. 5, 2014";
	/** mm-312498573 */
	public static final String NAME_DEFAULT_DATE_DASHED = "Bike Ride on 2013-06-16";
	/** mm-358968237 */
	public static final String NAME_DEFAULT_WORD_MONTH_TWO_DAY_DIGITS = "Road Cycling on Aug. 18, 2013";
	/** produced from mm-90183134 */
	public static final String NAME_DEFAULT_FILE_IMPORT = "File Import - Bike Ride (12/18/2011)";
	/** produced from mm-92907218 */
	public static final String NAME_DEFAULT_GARMIN_IMPORT = "Garmin Import - Bike Ride (01/19/2012)";
	/** produced from mm-90183134 */
	public static final String NAME_DEFAULT_TIME_SMALL_HALF_DAY = "0.10 mile Walk in San Francisco on Jan 29, 2012 at 10:12 am";

}
