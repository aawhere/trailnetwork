/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.app.AccountDocument;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.io.IoException;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class MapMyFitnessUserJsonFileTest {

	@Test
	public void testUser() throws IoException, DocumentContentsNotRecognizedException, DocumentDecodingException {
		InputStream is = ClassLoader.getSystemResourceAsStream("mmf/user.json");
		assertNotNull(is);
		Document doc = MapMyFitnessDocumentProducer.user().produce(is);
		TestUtil.assertInstanceOf(AccountDocument.class, doc);
		AccountDocument userDoc = (AccountDocument) doc;
		assertEquals("adam.mcmanus", userDoc.username());
		assertEquals("1039389", userDoc.userId());
		assertEquals("Adam McManus", userDoc.name());
	}
}
