/**
 * 
 */
package com.aawhere.activity.imprt.mmf.type;

import static org.junit.Assert.*;

import java.io.InputStream;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.aawhere.activity.ActivityType;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class MapMyFitnessActivityTypesFileTest {

	/**
	 * 
	 */
	private static final int TOTAL_COUNT = 759;
	private static MapMyFitnessActivityTypes mapMyFitnessActivityTypes;

	@BeforeClass
	public static void setUpClass() {
		mapMyFitnessActivityTypes = new MapMyFitnessActivityTypes();
	}

	@Test
	public void test20() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		InputStream is = ClassLoader.getSystemResourceAsStream("mmf/type/ActivityTypes-20.json");
		assertNotNull(is);
		ActivityTypes activityTypes = MapMyFitnessActivityTypes.read(is);
		assertEquals(TOTAL_COUNT, activityTypes.getTotal_count());
		List<Activity_types> activity_types = activityTypes.get_embedded().getActivity_types();
		TestUtil.assertSize(20, activity_types);
	}

	@Test
	public void testAll() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		ActivityTypes all = mapMyFitnessActivityTypes.readAll();
		assertEquals(TOTAL_COUNT, all.getTotal_count());
		TestUtil.assertSize(TOTAL_COUNT, all.get_embedded().getActivity_types());
		assertType("9", ActivityType.WALK);
		assertType("105", ActivityType.WALK);// Power
		assertType("106", ActivityType.WALK);// Casual
		assertType("117", ActivityType.WALK);// Long
		assertType("129", ActivityType.WALK);// Sight seeing
		assertType("133", ActivityType.WALK);// Stairs
		assertType("143", ActivityType.WALK);// Race
		assertType("152", ActivityType.WALK);// quick
		assertType("179", ActivityType.WALK);// general
		assertType("204", ActivityType.WALK);// Dog
		assertType("242", ActivityType.WALK);// Group
		assertType("253", ActivityType.WALK);// Commute
		assertType("269", ActivityType.WALK);// Hill
		assertType("272", ActivityType.WALK);// Brisk
		assertType("274", ActivityType.WALK);//
		assertType("275", ActivityType.WALK);// Nordic
		assertType("766", ActivityType.WALK);// Indoor Track
		assertType("274", ActivityType.WALK);// Brisk
		assertType("825", ActivityType.WALK);// Pushing single stroller
		assertType("827", ActivityType.WALK);// Pushing double stroller

		// hike
		assertType("24", ActivityType.HIKE);
		assertType("32", ActivityType.HIKE); // hills
		// assertType("58", ActivityType.HIKE);// Mountaineering is winter sports
		assertType("109", ActivityType.HIKE);// General
		assertType("114", ActivityType.HIKE);// Trekking
		assertType("177", ActivityType.HIKE);// Cross Country
		assertType("220", ActivityType.HIKE);// Rock Climbing
		assertType("508", ActivityType.HIKE);// heavy pack
		assertType("510", ActivityType.HIKE);// light pack
		// bike
		assertType("11", ActivityType.BIKE);
		assertType("33", ActivityType.BIKE);// fixie
		assertType("36", ActivityType.ROAD);
		assertType("38", ActivityType.BIKE);// touring bike
		assertType("44", ActivityType.BIKE);// track cycling
		assertType("523", ActivityType.BIKE);// track cycling
		assertType("47", ActivityType.BIKE);// unicycling
		assertType("544", ActivityType.BIKE);// unicycling
		assertType("545", ActivityType.BIKE);// unicycling
		assertType("50", ActivityType.BIKE);// recumbant cycling
		assertType("52", ActivityType.BIKE);// CRUISER
		assertType("466", ActivityType.BIKE);// beach CRUISER
		// assertType("53", ActivityType.ROAD);// indoor road cycling repeat
		assertType("56", ActivityType.BIKE);// bmx
		assertType("60", ActivityType.BIKE);// cyclocross
		assertType("545", ActivityType.BIKE);// cyclocross
		assertType("64", ActivityType.BIKE);// hybrid
		assertType("445", ActivityType.BIKE);// touring
		// MTB
		assertType("41", ActivityType.MTB);
		assertType("448", ActivityType.MTB);// light intensity
		assertType("449", ActivityType.MTB);// light intensity
		assertType("450", ActivityType.MTB);// long distance
		assertType("451", ActivityType.MTB);// high intensity
		assertType("452", ActivityType.MTB);// event
		assertType("453", ActivityType.MTB);// group
		assertType("454", ActivityType.MTB);// downhill
		assertType("455", ActivityType.MTB);// double track
		assertType("456", ActivityType.MTB);// general
		assertType("457", ActivityType.MTB);// commute
		assertType("458", ActivityType.MTB);// easy training
		assertType("459", ActivityType.MTB);// single track
		assertType("460", ActivityType.MTB);//
		assertType("461", ActivityType.MTB);//
		assertType("462", ActivityType.MTB);//
		assertType("463", ActivityType.MTB);//
		assertType("464", ActivityType.MTB);//

		// run
		assertType("16", ActivityType.RUN);
		assertType("25", ActivityType.RUN);// indoor run
		assertType("108", ActivityType.RUN);// track run
		assertType("115", ActivityType.RUN);// track run
		// ski
		assertType("63", ActivityType.SKI);// cross country
		assertType("74", ActivityType.SKI);
		// assertType("107", ActivityType.SKI); snowboarding is winter sports
		// swim
		assertType("75", ActivityType.SWIM);// lap swim
		// skate
		// assertType("86", ActivityType.SKATE);// ice
	}

	public static void assertType(String mmfActivityTypeId, ActivityType expected) {

		ActivityType activityType = mapMyFitnessActivityTypes.type(mmfActivityTypeId);
		assertEquals(mmfActivityTypeId, expected, activityType);
	}
}
