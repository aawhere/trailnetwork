/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.AccountImportManager.RefreshRate;
import com.aawhere.activity.Activities.Builder;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.persist.Filter;
import com.aawhere.queue.QueueTestUtil;
import com.aawhere.queue.Task;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.test.TestUtil;
import com.aawhere.track.SimpleTrackpoint;
import com.aawhere.track.TrackSummaryTestUtil;
import com.aawhere.track.TrackpointTestUtil;

import com.google.common.base.Function;
import com.google.common.base.Suppliers;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * A stateful test that sets up for the most basic nothing happening and each test modifies the
 * state to setup the combinations of state that can happen when retrieving activities from a remote
 * provide mixing with activities we already know about.
 * 
 * @author aroller
 * 
 */
public class AccountImportManagerUnitTest {

	/**
	 * 
	 */
	private static final Range<DateTime> REFRESH_DAILY_RANGE = Range.lessThan(DateTime.now().plusDays(2));
	private static final Range<DateTime> REFRESH_MONTHLY_RANGE = Range.lessThan(DateTime.now().plusDays(32));
	Multimap<AccountId, ActivityId> activitiesToBeQueued;
	Builder mostRecentActivitiesBuilder;
	LocalDate mostRecentStartDateActivityRetrieved;

	AccountId accountId;
	Boolean deepExpected;
	RefreshRate refreshRateExpected;
	DateTime mostRecentStartTimeExpected;
	Integer page;
	private Range<DateTime> expectedRefreshTaskDate;

	@Before
	public void setUp() {
		activitiesToBeQueued = HashMultimap.create();
		mostRecentActivitiesBuilder = Activities.create();

		accountId = AccountTestUtil.accountId();
		deepExpected = true;
		refreshRateExpected = RefreshRate.NEVER;
		mostRecentStartTimeExpected = null;
		page = 1;
		// no activities, no recent start date
		this.mostRecentStartDateActivityRetrieved = null;
	}

	@After
	public void test() {
		Activities mostRecentActivities;
		if (mostRecentActivitiesBuilder != null) {
			mostRecentActivities = mostRecentActivitiesBuilder.build();
			// just one activity per test please
			TestUtil.assertSize(Range.closed(0, 1), mostRecentActivities);
		} else {
			mostRecentActivities = null;
		}

		// if ids are discovered then the retriever must produce a date.
		if (this.mostRecentStartDateActivityRetrieved == null
				&& CollectionUtils.isNotEmpty(this.activitiesToBeQueued.entries())) {
			this.mostRecentStartDateActivityRetrieved = LocalDate.now();
		}
		// if not otherwise indicated we should assume the retriever will provide the most recent
		// date
		if (this.mostRecentStartTimeExpected == null && this.mostRecentStartDateActivityRetrieved != null) {
			this.mostRecentStartTimeExpected = localDateToDateTime();
		}

		final Function<ActivityImportCrawlerDestination, DocumentRetriever> retrieverFunction = ActivityImportTestUtil
				.retrieverFunction(activitiesToBeQueued, false, this.mostRecentStartDateActivityRetrieved);

		final Function<Set<ActivityId>, Iterable<ActivityId>> nonExistingIdsFunction = SetUtilsExtended.constant(Sets
				.newHashSet(activitiesToBeQueued.values()));

		AccountImportManager manager = AccountImportManager.create().accountId(accountId)
				.filter(Filter.create().setPage(page).build()).retrieverFunction(retrieverFunction)
				.mostRecentActivities(Suppliers.ofInstance(mostRecentActivities))
				.queueFactory(QueueTestUtil.getQueueFactory())
				.instructionsRepository(ActivityImportTestUtil.mockRepo())
				.nonExistingIdsFunction(nonExistingIdsFunction).build();

		assertEquals(accountId, manager.accountId());
		assertEquals("deep", deepExpected, manager.crawler().deep());
		assertEquals(mostRecentActivities, manager.mostRecentKnownActivities());
		assertEquals(refreshRateExpected, manager.refreshRate());
		assertNotNull(manager.filter());
		assertEquals(mostRecentStartTimeExpected, manager.mostRecentStartTime());
		Task refreshTask = manager.refreshTask();

		if (this.expectedRefreshTaskDate != null) {
			DateTime countDown = DateTime.now().plus(refreshTask.getCountdownMillis());
			JodaTestUtil.assertRange(this.expectedRefreshTaskDate, countDown);

		} else {
			assertNull(refreshTask);
		}
	}

	/**
	 * @return
	 */
	private DateTime localDateToDateTime() {
		// FIXME:this picking the start of day is magic...reference the source
		return this.mostRecentStartDateActivityRetrieved.toDateTimeAtStartOfDay();
	}

	/**
	 * When we have no activities in our system nor the remote system this is possibly a user that
	 * doesn't exist anywhere.
	 */
	@Test
	public void testUserNotKnown() {
		// the default setup with most things empty
	}

	@Test
	public void testNoActivitiesKnownRecentSomeActivitiesReceived() {
		addActivitiesToBeQueued();
		// no activities known, but some received...we don't yet know if those are fresh...come back
		// soon after the activities have been retrieved to see if this is a fresh user
		this.refreshRateExpected = RefreshRate.OFTEN;
		// daily because they are new...so we want to revisit once uploaded
		this.expectedRefreshTaskDate = REFRESH_DAILY_RANGE;
	}

	@Test
	public void testRecentActivitiesKnown() {
		recentActivityAdded();
		// we already know about this one...it must be an update
		this.deepExpected = false;
		this.expectedRefreshTaskDate = REFRESH_DAILY_RANGE;
	}

	/**
	 * Only the first page should determine refresh rates, etc.
	 * 
	 */
	@Test
	public void testRecentActivitiesKnownPage2() {
		addActivitiesToBeQueued();
		this.deepExpected = false;
		this.page = 2;
		this.refreshRateExpected = null;
		this.mostRecentActivitiesBuilder = null;
	}

	/**
	 * 
	 */
	private void addActivitiesToBeQueued() {
		this.activitiesToBeQueued = ActivityImportTestUtil.ids();
	}

	@Test
	public void testActivitiesRarely() {
		this.deepExpected = false;
		rarelyActivitiesAdded();
	}

	@Test
	public void testOldActivityAlreadyKnownRecentActivityFound() {
		this.deepExpected = false;
		rarelyActivitiesAdded();
		// a really new activity
		this.mostRecentStartDateActivityRetrieved = LocalDate.now();
		expectedOften();

		this.mostRecentStartTimeExpected = localDateToDateTime();
	}

	/**
	 * @return
	 * 
	 */
	private Pair<RefreshRate, Integer> expectedOften() {
		this.expectedRefreshTaskDate = REFRESH_DAILY_RANGE;
		return expectedRefreshRate(RefreshRate.OFTEN, 1);
	}

	/**
	 * @param refreshRate
	 * @param daysAgo
	 * @return
	 */
	private Pair<RefreshRate, Integer> expectedRefreshRate(RefreshRate refreshRate, int daysAgo) {
		this.refreshRateExpected = refreshRate;
		this.mostRecentStartTimeExpected = DateTime.now().minusDays(daysAgo);

		return Pair.of(refreshRate, daysAgo);
	}

	/**
	 * Should only be called once per test. assigns the most recent start time and adds a recent
	 * activity
	 * 
	 * @return
	 * 
	 */
	private Activity recentActivityAdded() {
		Pair<RefreshRate, Integer> expectedOften = expectedOften();
		return knownActivityAdded(expectedOften.getKey(), expectedOften.getValue());
	}

	/**
	 * These are the activities we already know about, not the ones we discover from the retriever.
	 * 
	 * @param refreshRate
	 * @param daysAgo
	 * @return
	 */
	private Activity knownActivityAdded(RefreshRate refreshRate, int daysAgo) {
		expectedRefreshRate(refreshRate, daysAgo);
		// refresh rate updates most recent time expected
		SimpleTrackpoint start = TrackpointTestUtil.trackpoint(this.mostRecentStartTimeExpected);
		Activity sample = ActivityTestUtil.createActivity(TrackSummaryTestUtil.getSample(start));
		this.mostRecentActivitiesBuilder.add(sample);
		return sample;
	}

	private void rarelyActivitiesAdded() {
		knownActivityAdded(RefreshRate.RARELY, 120);
		this.expectedRefreshTaskDate = REFRESH_MONTHLY_RANGE;
	}

}
