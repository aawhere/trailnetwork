/**
 * 
 */
package com.aawhere.activity.imprt.csv;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.UnknownApplicationException;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;

import com.google.appengine.repackaged.com.google.common.collect.Iterators;

/**
 * @author aroller
 * 
 *         TN-143
 * 
 */
public class CsvActivityDocumentUnitTest {
	private String applicationKey;
	private String version;
	private String activityId;
	private String activityType;
	private String userId;
	private String[] trackpoint1 = { "0", "12.3456", "179.654", "33" };
	private String[] trackpoint2 = { "3", "8.132", "-33.232", "55" };
	private String[] missingDuration = { "", "8.132", "-33.232", "55" };
	private String[] missingLatitude = { "3", "", "-33.232", "55" };
	private String[] missingLongitude = { "3", "8.132", "", "55" };
	private String[] missingElevation = { "3", "8.132", "-33.232", "" };
	private String[] incorrectCount = { "3", "8.132", "-33.232" };
	private String[][] example = { trackpoint1, trackpoint2 };
	private String[] emptyLine = { "", "", "", "" };
	private String startTimeString;
	private DateTime startTime;
	private DateTime uploadTime;
	private String uploadedTimeString;
	private String device;
	private String deviceManufacturer;

	@Before
	public void setUp() {

		this.applicationKey = "2PEAK";
		this.version = "v1.0";
		this.activityId = "123999";
		this.userId = "93882";
		this.activityType = "running";
		this.startTimeString = "2013-04-01T10:11:41+02:00";
		this.uploadedTimeString = "2012-05-11T10:05:22+18:00";
		this.deviceManufacturer = "Garmin";
		this.device = "Edge 305";
		DateTimeFormatter dateTimeParser = ISODateTimeFormat.dateTimeParser();
		this.startTime = dateTimeParser.parseDateTime(startTimeString);
		this.uploadTime = dateTimeParser.parseDateTime(uploadedTimeString);
	}

	public String buildContents(String[][] trackpoints) {
		StringBuilder builder = new StringBuilder();
		final String headerLineEnd = ",,,\n";
		builder.append(this.version);
		builder.append(headerLineEnd);
		builder.append(this.applicationKey);
		builder.append(headerLineEnd);
		builder.append(this.activityId);
		builder.append(headerLineEnd);
		builder.append(this.userId);
		builder.append(headerLineEnd);
		builder.append(this.activityType);
		builder.append(headerLineEnd);

		builder.append(this.uploadedTimeString);
		builder.append(headerLineEnd);

		builder.append(this.deviceManufacturer);
		builder.append(headerLineEnd);

		builder.append(this.device);
		builder.append(headerLineEnd);

		builder.append(this.startTimeString);
		builder.append(headerLineEnd);

		// build trackpoints.
		int length = trackpoints.length;
		for (int i = 0; i < length; i++) {
			String[] strings = trackpoints[i];
			builder.append(StringUtils.join(strings, ','));
			builder.append("\n");
		}
		return builder.toString();
	}

	@Test
	public void testOneTrackpoint() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {
		String[][] expectedTrack = new String[][] { trackpoint1 };
		assertTrack(expectedTrack);
	}

	@Test
	public void testTwoTrackpoints() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {
		String[][] expectedTrack = example;
		assertTrack(expectedTrack);
	}

	@Test
	public void testMultipleTracksIteration() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {
		String[][] expectedTrack = example;
		CsvActivityDocument document = buildDocument(expectedTrack);
		assertTrack(expectedTrack, document);
		assertTrack(expectedTrack, document);
		assertTrack(expectedTrack, document);

	}

	@Test
	public void testMultipleIterationFromSameTrack() throws DocumentContentsNotRecognizedException,
			DocumentDecodingException {
		Track track = buildDocument(example).getTrack();
		assertTrack(example, track);
		assertTrack(example, track);
		assertTrack(example, track);

	}

	@Test
	public void testDoubleIterationAfterFirstIncompleteIterationFromSameTrack() throws DocumentDecodingException {
		Track track = buildDocument(example).getTrack();
		Iterator<Trackpoint> iterator = track.iterator();
		iterator.next();
		// make sure this works to call a new iterator
		assertTrack(example, track);
	}

	@Test(expected = ConcurrentModificationException.class)
	public void testConcurrentIteration() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		Track track = buildDocument(example).getTrack();
		Iterator<Trackpoint> iterator1 = track.iterator();
		iterator1.next();
		Iterator<Trackpoint> iterator2 = track.iterator();
		iterator1.next();
	}

	@Test
	public void testDoubleIterationAfterFirstIncompleteIteration() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, UnknownApplicationException {
		String[][] expectedTrack = example;

		CsvActivityDocument document = buildDocument(expectedTrack);

		Iterator<Trackpoint> iterator = document.getTrack().iterator();
		iterator.next();
		// only iterate one, but subsequent should start back at the beginning.
		assertTrack(expectedTrack, document);
	}

	@Test
	public void testMissingElevation() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {
		String[] trackpointString = missingElevation;
		Boolean expectingElevationNull = true;
		Boolean expectingLocationNull = false;
		assertMissing(trackpointString, expectingElevationNull, expectingLocationNull);

	}

	@Test(expected = InvalidArgumentException.class)
	public void testIncorrectCount() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {
		assertTrack(new String[][] { trackpoint1, incorrectCount, trackpoint2 });
	}

	/**
	 * @param trackpointString
	 * @param expectingElevationNull
	 * @param expectingLocationNull
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws UnknownApplicationException
	 */
	private void
			assertMissing(String[] trackpointString, Boolean expectingElevationNull, Boolean expectingLocationNull)
					throws DocumentDecodingException, DocumentContentsNotRecognizedException,
					UnknownApplicationException {
		CsvActivityDocument doc = CsvActivityDocument.create(buildContents(new String[][] { trackpointString }))
				.build();
		Trackpoint trackpoint = doc.getTrack().iterator().next();
		assertEquals(expectingElevationNull, trackpoint.getElevation() == null);
		assertEquals(expectingLocationNull, trackpoint.getLocation() == null);
	}

	@Test
	public void testMissingLatitude() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {
		assertMissing(missingLatitude, false, true);
	}

	@Test
	public void testMissingLongitude() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {
		assertMissing(missingLongitude, false, true);
	}

	@Test(expected = DocumentDecodingException.class)
	public void testMissingTimestamp() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {
		CsvActivityDocument.create("junk/n").build();

	}

	@Test
	public void testEmptyLine() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {
		String contents = buildContents(new String[][] { trackpoint1, emptyLine, trackpoint2, emptyLine });
		CsvActivityDocument csvDocument = CsvActivityDocument.create(contents).build();

		int size = Iterators.size(csvDocument.getTrack().iterator());
		assertEquals(2, size);
	}

	@Test(expected = NoSuchElementException.class)
	public void testMissingDuration() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			UnknownApplicationException {

		// throws no such since there is only one line and that line will be ignored.
		assertMissing(missingDuration, false, false);
	}

	@Test
	public void testBytes() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			UnknownApplicationException {
		byte[] expected = buildContents(example).getBytes();
		CsvActivityDocument doc = CsvActivityDocument.create(expected).build();
		byte[] actual = doc.toByteArray();
		assertTrue("in was not the same as the out", Arrays.equals(expected, actual));
		assertHeader(doc);
	}

	@Test
	public void testProducerWithBytes() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		byte[] expected = buildContents(example).getBytes();
		CsvActivityDocumentProducer producer = new CsvActivityDocumentProducer.Builder().build();
		Document document = producer.produce(expected);
		assertBytesResults(expected, document);
	}

	@Test
	public void testFactoryWithBytes() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			DocumentFormatNotRegisteredException {
		byte[] expected = buildContents(example).getBytes();
		Document document = createDocumentFromBytes(expected);
		assertBytesResults(expected, document);
	}

	/**
	 * @param expected
	 * @return
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentFormatNotRegisteredException
	 */
	private Document createDocumentFromBytes(byte[] expected) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		CsvActivityDocumentProducer producer = new CsvActivityDocumentProducer.Builder().build();
		DocumentFactory factory = DocumentFactory.getInstance().register(producer);
		Document document = factory.getDocument(DocumentFormatKey.ACTIVITY_CSV, expected);
		return document;
	}

	private Document createExampleDocumentFromBytes() throws DocumentContentsNotRecognizedException,
			DocumentDecodingException, DocumentFormatNotRegisteredException {
		return createDocumentFromBytes(buildContents(example).getBytes());
	}

	/**
	 * @param expected
	 * @param document
	 */
	private void assertBytesResults(byte[] expected, Document document) {
		assertTrue(document instanceof CsvActivityDocument);
		CsvActivityDocument doc = (CsvActivityDocument) document;
		assertNotNull(document);
		byte[] actual = doc.toByteArray();
		assertNotNull("bytes are null", actual);
		assertEquals(expected.length, actual.length);
		assertTrue("in was not the same as the out", Arrays.equals(expected, actual));
		assertHeader(doc);
	}

	/**
	 * @param document
	 */
	private void assertStreamResults(Document document) {
		assertTrue(document instanceof CsvActivityDocument);
		CsvActivityDocument doc = (CsvActivityDocument) document;
		assertNotNull(document);
		byte[] actual = doc.toByteArray();
		assertNull(actual);
		assertHeader(doc);
	}

	/**
	 * @param expectedTrack
	 * @return
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws UnknownApplicationException
	 */
	private CsvActivityDocument assertTrack(String[][] expectedTrack) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, UnknownApplicationException {
		CsvActivityDocument doc = buildDocument(expectedTrack);
		return assertTrack(expectedTrack, doc);
	}

	/**
	 * @param expectedTrack
	 * @param doc
	 * @return
	 */
	private CsvActivityDocument assertTrack(String[][] expectedTrack, CsvActivityDocument doc) {
		Track track = doc.getTrack();
		assertHeader(doc);
		assertTrack(expectedTrack, track);
		return doc;
	}

	/**
	 * @param expectedTrack
	 * @param track
	 */
	private void assertTrack(String[][] expectedTrack, Track track) {
		Iterator<Trackpoint> iterator = track.iterator();
		assertNotNull(iterator);
		for (int i = 0; i < expectedTrack.length; i++) {
			assertTrue(iterator.hasNext());
			String[] expectedTrackpoint = expectedTrack[i];
			Trackpoint actualTrackpoint = iterator.next();
			assertNotNull(actualTrackpoint);
			assertTrackpoint(expectedTrackpoint, actualTrackpoint);
		}

		assertFalse(iterator.hasNext());
	}

	/**
	 * @param expectedTrack
	 * @return
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 */
	private CsvActivityDocument buildDocument(String[][] expectedTrack) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException {
		CsvActivityDocument doc = CsvActivityDocument.create(buildContents(expectedTrack)).build();
		return doc;
	}

	/**
	 * @param doc
	 */
	private void assertHeader(CsvActivityDocument doc) {
		assertEquals(this.version, doc.getVersion());
		assertEquals(this.activityId, doc.getActivityId());
		assertEquals(this.activityType, doc.getActivityType());
		assertEquals(this.userId, doc.getUserId());
		assertEquals(this.uploadTime, doc.getUploadTime());
		assertEquals(this.device, doc.getDevice());
		assertEquals(this.deviceManufacturer, doc.getDeviceManufacturer());
	}

	/**
	 * @param actualTrackpoint
	 * @param expectedTrackpoint
	 */
	private void assertTrackpoint(String[] expectedTrackpoint, Trackpoint actualTrackpoint) {
		assertEquals(	startTime.plusSeconds(Integer.parseInt(expectedTrackpoint[CsvActivityDocument.DURATION_OFFSET])),
						actualTrackpoint.getTimestamp());
		assertEquals(	expectedTrackpoint[CsvActivityDocument.LATITUDE],
						String.valueOf(actualTrackpoint.getLocation().getLatitude().getInDecimalDegrees()));
		assertEquals(	expectedTrackpoint[CsvActivityDocument.LONGITUDE],
						String.valueOf(actualTrackpoint.getLocation().getLongitude().getInDecimalDegrees()));
		assertEquals(	actualTrackpoint.toString(),
						expectedTrackpoint[CsvActivityDocument.ELEVATION],
						String.valueOf((int) actualTrackpoint.getElevation().doubleValue(ExtraUnits.METER_MSL)));
	}

	@Test
	public void testProducerWithStream() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		byte[] expected = buildContents(example).getBytes();
		ByteArrayInputStream stream = new ByteArrayInputStream(expected);
		CsvActivityDocumentProducer producer = new CsvActivityDocumentProducer.Builder().build();
		Document document = producer.produce(stream);
		assertStreamResults(document);
	}

	@Test
	public void testFactoryWithStream() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			DocumentFormatNotRegisteredException {
		Document document = createExampleDocumentFromStream();
		assertStreamResults(document);
	}

	/**
	 * @return
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentFormatNotRegisteredException
	 */
	private CsvActivityDocument createExampleDocumentFromStream() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		byte[] expected = buildContents(example).getBytes();
		ByteArrayInputStream stream = new ByteArrayInputStream(expected);
		CsvActivityDocumentProducer producer = new CsvActivityDocumentProducer.Builder().build();
		DocumentFactory factory = DocumentFactory.getInstance().register(producer);
		Document document = factory.getDocument(DocumentFormatKey.ACTIVITY_CSV, stream);
		return (CsvActivityDocument) document;
	}

	@Test
	public void testStream() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			UnknownApplicationException {
		CsvActivityDocument doc = createExampleDocumentFromText();
		byte[] actual = doc.toByteArray();
		assertHeader(doc);
		assertNull("bytes are only available if created with bytes", actual);

	}

	/**
	 * @return
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws UnknownApplicationException
	 */
	private CsvActivityDocument createExampleDocumentFromText() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, UnknownApplicationException {
		byte[] expected = buildContents(example).getBytes();
		CsvActivityDocument doc = CsvActivityDocument.create(new ByteArrayInputStream(expected)).build();
		return doc;
	}

	/**
	 * The activity document is a bit crippled since it doesn't load bytes from a stream. Only
	 * serialize if bytes are available.
	 * 
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 * @throws UnknownApplicationException
	 */
	@Test(expected = IllegalStateException.class)
	public void testSerializableText() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			UnknownApplicationException {
		CsvActivityDocument doc = createExampleDocumentFromText();
		byte[] serialized = SerializationUtils.serialize(doc);
		assertNotNull(serialized);
		CsvActivityDocument actualDoc = (CsvActivityDocument) SerializationUtils.deserialize(serialized);
		assertHeader(actualDoc);
	}

	@Test
	public void testSerializableBytes() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			UnknownApplicationException, DocumentFormatNotRegisteredException {
		CsvActivityDocument expected = (CsvActivityDocument) createExampleDocumentFromBytes();

		byte[] serialized = SerializationUtils.serialize(expected);
		assertNotNull(serialized);
		CsvActivityDocument actualDoc = (CsvActivityDocument) SerializationUtils.deserialize(serialized);
		assertBytesResults(expected.toByteArray(), actualDoc);

	}

}
