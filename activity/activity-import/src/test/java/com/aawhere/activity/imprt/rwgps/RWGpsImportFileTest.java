/**
 * 
 */
package com.aawhere.activity.imprt.rwgps;

import static com.aawhere.activity.imprt.rwgps.RWGpsImportTestUtil.*;
import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportFixture;
import com.aawhere.activity.ActivityImportId;
import com.aawhere.activity.imprt.json.JsonImportTestUtil;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.track.Track;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.TrackpointTestUtil;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class RWGpsImportFileTest {

	@Test
	public void testUserActivitiesDocument() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		final String userActivitiesJsonFile = RWGpsImportTestUtil.USER_ACTIVITIES_JSON_FILE;
		final Integer userActivitiesCount = USER_ACTIVITIES_COUNT;
		assertUserActivities(userActivitiesJsonFile, userActivitiesCount);
	}

	/**
	 * Ensures no gps doesn't get queued for import.
	 * 
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 */
	@Test
	public void testUserActivitiesNoGpsDocument() throws DocumentContentsNotRecognizedException,
			DocumentDecodingException {
		RWGpsUserActivitiesJson document = assertUserActivities(RWGpsImportTestUtil.USER_ACTIVITIES_NO_GPS_JSON_FILE,
																USER_ACTIVITIES_COUNT);
		final Set<ActivityId> activityIds = document.activityIds();
		final Predicate<Object> instanceOfPredicate = Predicates.instanceOf(ActivityImportId.NotWorthImporting.class);
		Set<ActivityId> filtered = Sets.filter(activityIds, instanceOfPredicate);
		TestUtil.assertSize(USER_ACTIVITIES_WITH_NO_GPS_COUNT, filtered);
		assertEquals(RWGpsImportTestUtil.USER_ACTIVITIES_MOST_RECENT, document.mostRecentStartDate());
		assertFalse(document.finalPage());
	}

	@Test
	public void testUserActivitiesEmptyDocument() throws DocumentContentsNotRecognizedException,
			DocumentDecodingException {
		RWGpsUserActivitiesJson document = assertUserActivities(RWGpsImportTestUtil.USER_ACTIVITIES_EMPTY_JSON_FILE,
																USER_ACTIVITIES_EMPTY_COUNT);
		assertNull(document.mostRecentStartDate());
		assertTrue(document.finalPage());
	}

	/**
	 * @param userActivitiesJsonFile
	 * @param userActivitiesCount
	 * @return
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 */
	public RWGpsUserActivitiesJson assertUserActivities(final String userActivitiesJsonFile,
			final Integer userActivitiesCount) throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		RWGpsUserActivitiesJson document = JsonImportTestUtil.assertFile(	RWGpsDocumentProducers
																					.userActivitiesProducer(),
																			userActivitiesJsonFile,
																			RWGpsUserActivitiesJson.class,
																			DocumentFormatKey.RW_USER_ACTIVITIES_JSON);
		TestUtil.assertSize(userActivitiesCount, document.activityIds());
		return document;
	}

	@Test
	public void testTrip() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		RWGpsTripDetailsJsonActivityDocument document = JsonImportTestUtil
				.assertFile(RWGpsDocumentProducers.activityProducer(),
							RWGpsImportTestUtil.TRIP_JSON_FILE,
							RWGpsTripDetailsJsonActivityDocument.class,
							DocumentFormatKey.RW_TRIP_JSON);
		assertEquals("activityId", TRIP_ID, document.getActivityId());
		assertEquals("user Id", TRIP_USSER_ID, document.getUserId());
		assertEquals(ActivityImportFixture.CULLEN_RW_ACCOUNT_ID, document.getAccount().id());
		assertEquals("activity name", TRIP_NAME, document.getActivityName());
		assertEquals("activity date", TRIP_DATE, document.getDate());
		TestUtil.assertStartsWith(document.getDescription(), TRIP_DESCRIPTION_START);
		final Track track = document.getTrack();
		TestUtil.assertSize("number of trackpoints", TRIP_TRACKPOINT_COUNT, track);
		Trackpoint firstTrackpoint = track.iterator().next();
		Trackpoint expected = TrackUtil.createPoint(new Long(TRIP_TRACKPOINT_FIRST_UNIX_TIME * 1000),
													TRIP_TRACKPOINT_FIRST_LAT,
													TRIP_TRACKPOINT_FIRST_LON,
													TRIP_TRACKPOINT_FIRST_ELE);
		TrackpointTestUtil.assertTrackpointEquals(expected, firstTrackpoint);
		JodaTestUtil.assertEquals(	"first trackpiont timestamp",
									TRIP_TRACKPOINT_FIRST_TIMESTAMP,
									firstTrackpoint.getTimestamp());

	}

}
