/**
 *
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;

import com.aawhere.activity.ImportMilestone.Status;
import com.aawhere.persist.BaseEntitiesBaseUnitTest;
import com.aawhere.personalize.xml.XmlAdapterTestUtils;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author aroller
 * 
 */
public class ActivityImportFailuresUnitTest
		extends BaseEntitiesBaseUnitTest<ActivityImportFailure, ActivityImportFailures, ActivityImportFailures.Builder> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#createEntitySampleWithId()
	 */
	@Override
	public ActivityImportFailure createEntitySampleWithId() {
		return ActivityImportFailureUnitTest.getInstance().getEntitySampleWithId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return XmlAdapterTestUtils.getAdapters();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getClassesToBeBound()
	 */
	@Override
	protected Class<?>[] getClassesToBeBound() {
		return ArrayUtils.add(super.getClassesToBeBound(), ActivityImportProcessorVersion.class);
	}

	/**
	 * Ensures the status methods are accurately reporting state.
	 * 
	 * @see ActivityImportFailures#isErroneous()
	 * @see ActivityImportFailures#isQueued()
	 * 
	 */
	@Test
	public void testStatusCheckAgainstEmpty() {
		ActivityImportFailures history = new ActivityImportFailures.Builder().build();
		Boolean isErroneous = false;
		Boolean isEmpty = true;
		Boolean isQueued = false;

		assertStatus(history, isEmpty, isErroneous, isQueued);

	}

	/**
	 * Contains a single error status.
	 */
	@Test
	public void testStatusWithError() {
		Status status = Status.FAILED;
		ActivityImportFailures history = historyWithStatus(status);
		assertStatus(history, false, true, false);

	}

	@Test
	public void testStatusQueued() {
		ActivityImportFailures history = historyWithStatus(Status.QUEUED);
		assertStatus(history, false, false, true);

	}

	/**
	 * @param status
	 * @return
	 */
	public static ActivityImportFailures historyWithStatus(Status status) {
		ActivityImportFailures history = new ActivityImportFailures.Builder().add(new ActivityImportFailure.Builder()
				.setActivityId(ActivityTestUtil.createApplicationActivityId()).setStatus(status).build()).build();
		return history;
	}

	/**
	 * @param history
	 * @param isErroneous
	 * @param isEmpty
	 * @param isQueued
	 */
	public static void assertStatus(ActivityImportFailures history, Boolean isEmpty, Boolean isErroneous,
			Boolean isQueued) {
		String message = history.toString();
		assertEquals(message, isEmpty, history.isEmpty());
		assertEquals(message, isErroneous, history.isErroneous());
		assertEquals(message, isQueued, history.isQueuedForImport());
	}

	@Test
	public void testJaxb() {
		ActivityImportFailures entitiesSample = getEntitiesSample();
		JAXBTestUtil<ActivityImportFailures> util = JAXBTestUtil.create(entitiesSample)
				.classesToBeBound(getClassesToBeBound()).build();
		util.assertContains(entitiesSample.getLatestStatus().name());
		util.assertContains(ActivityImportFailures.FIELD.DOMAIN);
	}

}
