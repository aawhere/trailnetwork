/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.app.KnownApplication;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class ApplicationActivityImportInstructionsRepositoryUnitTest {

	private ApplicationActivityImportInstructionsRepository repository;

	/**
	 * TN-597
	 */
	@Test
	public void testEveryTrailUrl() {
		String webUrl = ApplicationActivityImportInstructionsRepository.everyTrailInstructions().webUrl();
		assertNotNull(webUrl);
		ApplicationActivityImportInstructionsRepository repository = new ApplicationActivityImportInstructionsRepository.Builder()
				.addInstructions(ApplicationActivityImportInstructionsRepository.everyTrailInstructions()).build();

		ActivityId activityId = ActivityTestUtil.createApplicationActivityId(KnownApplication.EVERY_TRAIL.key);
		String applicationUrl = repository.applicationUrl(activityId);
		TestUtil.assertContains(applicationUrl, activityId.getRemoteId());
		TestUtil.assertContains(applicationUrl, "?");
	}

	@Test
	public void testDefaultActivityType() {
		ActivityType actual = ApplicationActivityImportInstructionsRepository.everyTrailInstructions()
				.activityType("junk");
		assertEquals(	"the general default should be used when no override is provided",
						ApplicationActivityImportInstructions.ACTIVITY_TYPE_DEFAULT,
						actual);
	}

	@Test
	public void testDefaultActivityTypeOverride() {
		ActivityType actual = ApplicationActivityImportInstructionsRepository.rideWithGpsInstructions()
				.activityType("bogus");
		assertEquals("RWGPS has a single activity type of Bike", ActivityType.BIKE, actual);
	}

}
