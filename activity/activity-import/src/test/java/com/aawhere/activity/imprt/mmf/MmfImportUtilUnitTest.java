/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class MmfImportUtilUnitTest {

	@Test
	public void testNameWithDate() {
		assertDefaultName(false, "junk");
		assertDefaultName(true, "before 3/2/03");
		assertDefaultName(true, "before 12/10/03");
		assertDefaultName(true, "sometihgn 03/02/2003");
		assertDefaultName(true, MapMyFitnessTestUtil.WALK_DEFAULT_NAME);
		assertDefaultName(true, MapMyFitnessTestUtil.RUN_DEFAULT_NAME_WITH_TIME);
		assertDefaultName(true, MapMyFitnessTestUtil.NAME_DEFAULT_TIME_SMALL_HALF_DAY);
		assertDefaultName(true, MapMyFitnessTestUtil.NAME_DEFAULT_FILE_IMPORT);
		assertDefaultName(true, MapMyFitnessTestUtil.NAME_DEFAULT_GARMIN_IMPORT);
		assertDefaultName(true, MapMyFitnessTestUtil.NAME_DEFAULT_FILE_IMPORT);
		assertDefaultName(true, MapMyFitnessTestUtil.NAME_DEFAULT_WORD_MONTH);
		assertDefaultName(true, MapMyFitnessTestUtil.NAME_DEFAULT_WORD_MONTH_TWO_DAY_DIGITS);
		assertDefaultName(true, MapMyFitnessTestUtil.NAME_DEFAULT_DATE_DASHED);
	}

	public void assertDefaultName(Boolean expectedTrue, String name) {
		assertEquals(name, expectedTrue, MapMyFitnessImportUtil.isDefaultName(name));
	}
}
