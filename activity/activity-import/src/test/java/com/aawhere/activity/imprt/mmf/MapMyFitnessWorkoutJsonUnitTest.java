/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.io.IoException;
import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.test.TestUtil;
import com.aawhere.track.Track;
import com.aawhere.util.rb.StringMessage;

/**
 * Tests the {@link MapMyFitnessWorkoutJson} without any file dependencies.
 * 
 * @author aroller
 * 
 */
public class MapMyFitnessWorkoutJsonUnitTest {

	private DeviceCapabilitiesRepository deviceRepo = new DeviceCapabilitiesRepository();
	private MmfWorkoutJsonReader reader = new MmfWorkoutJsonReader.Builder().deviceCapabilitiesRepository(deviceRepo)
			.build();

	@Test
	public void testName() throws DocumentDecodingException {
		String expected = TestUtil.generateRandomAlphaNumeric();
		String json = name(expected);
		MapMyFitnessWorkoutJson actual = reader.read(json);
		assertEquals(expected, actual.getActivityName());
	}

	@Test
	public void testHistory() throws DocumentDecodingException {
		String expected = TestUtil.generateRandomAlphaNumeric();
		String externalId = TestUtil.generateRandomAlphaNumeric();
		String json = source(expected, externalId);
		MapMyFitnessWorkoutJson actual = reader.read(json);
		assertEquals(Application.create().setKey(new ApplicationKey(expected)).setName(new StringMessage(expected))
				.build(), actual.getHistory().getDevice().getApplication());
	}

	@Test
	@Ignore("TN-900 no longer stripping default names upon import")
	public void testNameDefault() throws DocumentDecodingException {
		String input = MapMyFitnessTestUtil.WALK_DEFAULT_NAME;
		MapMyFitnessWorkoutJson actual = reader.read(name(input));
		assertNull(actual.getActivityName());
	}

	/**
	 * @param expected
	 * @return
	 */
	private String name(String expected) {
		return "{ \"name\": \"" + expected + "\"}";
	}

	private String source(String expected, String externalId) {
		return "{ \"source\": \"" + expected + "\", \"_links\": { \"self\": [ {\"id\": \"" + externalId + "\"}] }}";
	}

	@Test
	public void testId() throws DocumentDecodingException {
		String expected = TestUtil.generateRandomString();
		String json = "{ \"_links\": { \"self\": [{ \"id\": \"" + expected + "\" }] } }";
		MapMyFitnessWorkoutJson actual = reader.read(json);
		assertEquals(expected, actual.getActivityId());
	};

	@Test
	public void testTimeSeries() throws IoException, DocumentDecodingException {
		Double timeOffset = 0.7440070833;
		Double lat = 30.3489254005;

		Double lon = -97.7587612384;
		Double ele = 231.88;
		String startString = "2012-11-06T23:08:29+00:00";

		String json = "{\"start_datetime\": \"" + startString + "\", \"time_series\": {\"position\": [ [" + timeOffset
				+ ", {\"lat\": " + lat + ",\"lng\": " + lon + " ,\"elevation\": " + ele + "}]] } }";
		// String json =
		// "{\"time_series\": {\"position\": [  {\"lat\": 30.3489254005,\"lng\": -97.7587612384,\"elevation\": 231.88}] } }";
		MapMyFitnessWorkoutJson workoutJson = reader.read(json);
		Track track = workoutJson.getTrack();
		assertNotNull(track);
		Iterator<com.aawhere.track.Trackpoint> iterator = track.iterator();
		assertTrue("no trackpoints found", iterator.hasNext());
		com.aawhere.track.Trackpoint first = iterator.next();
		GeoCoordinate location = first.getLocation();
		assertNotNull(location);
		TestUtil.assertDoubleEquals(lat, location.getLatitude().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(lon, location.getLongitude().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(ele, first.getElevation().doubleValue(ExtraUnits.METER_MSL));
		assertEquals(startString, workoutJson.getWorkout().getStart_datetime());
		DateTime time = JodaTimeUtil.parseXmlDate(startString).plus((long) (timeOffset * 1000));
		JodaTestUtil.assertEquals("offset is off", time, first.getTimestamp());
	}
}
