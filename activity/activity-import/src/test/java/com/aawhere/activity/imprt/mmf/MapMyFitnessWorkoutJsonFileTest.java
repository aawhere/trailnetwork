/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivitySearchDocument;
import com.aawhere.activity.imprt.mmf.MmfJsonReader.Builder;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.app.KnownApplication;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.io.IoException;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.track.Track;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
@SuppressWarnings("rawtypes")
public class MapMyFitnessWorkoutJsonFileTest {

	private static final String WORKOUTS_FILENAME = "workouts";
	private static final String ROUTES_FILENAME = "routes";
	DocumentFactory factory;

	@Before
	public void setUp() {
		factory = new DocumentFactory.Builder().register(MapMyFitnessDocumentProducer.routes())
				.register(MapMyFitnessDocumentProducer.workouts()).build();

	}

	@Test
	public void testWorkout() throws IoException, DocumentContentsNotRecognizedException, DocumentDecodingException {
		String filename = "mmf/workout.json";
		MapMyFitnessWorkoutJson workout = load(filename);
		assertNotNull(workout.getActivityId());
		assertNotNull("default name should be given TN-900", workout.getActivityName());
		assertNotNull(workout.getActivityType());
		assertNotNull(workout.getUserId());
		assertEquals(DocumentFormatKey.MMFW7, workout.getDocumentFormat());
		Track track = workout.getTrack();
		List<Trackpoint> trackpoints = TrackUtil.trackpointsFrom(track);
		TestUtil.assertSize(1587, trackpoints);
	}

	/**
	 * Loads a {@link MapMyFitnessWorkoutJson}.
	 * 
	 * @param filename
	 * @return
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 */
	public static MapMyFitnessWorkoutJson load(String filename) throws DocumentContentsNotRecognizedException,
			DocumentDecodingException {
		InputStream is = ClassLoader.getSystemResourceAsStream(filename);
		assertNotNull(is);
		MapMyFitnessWorkoutJson workout = MmfWorkoutJsonReader.create()
				.deviceCapabilitiesRepository(new DeviceCapabilitiesRepository()).build().read(is);
		return workout;
	}

	public static Track track(String filename) {
		try {
			return load(filename).getTrack();
		} catch (DocumentDecodingException e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testWorkouts() throws IoException, DocumentContentsNotRecognizedException, DocumentDecodingException,
			DocumentFormatNotRegisteredException, IOException {
		int expectedNumberOfWorkouts = 2;
		String localFilename = WORKOUTS_FILENAME;
		WorkoutsResults workouts = (WorkoutsResults) workoutResults(expectedNumberOfWorkouts,
																	expectedNumberOfWorkouts,
																	localFilename,
																	"2013-10-21T23:37:05+00:00",
																	DocumentFormatKey.MMFUW7,
																	KnownApplication.MMF.key,
																	WorkoutsResults.class);

		List<? extends Workout> all = workouts.get_embedded().getWorkouts();
		TestUtil.assertSize(expectedNumberOfWorkouts, all);
		Workout firstWorkout = all.get(0);
		assertEquals(MapMyFitnessTestUtil.WALK_DEFAULT_NAME, firstWorkout.getName());

	}

	@Test
	public void testRoutes() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			DocumentFormatNotRegisteredException, IOException {
		final int expectedNumberOfWorkouts = 20;
		RoutesResultsMmfJson results = (RoutesResultsMmfJson) workoutResults(	456,
																				expectedNumberOfWorkouts,
																				ROUTES_FILENAME,
																				// routes don't
																				// report start time
																				null,
																				DocumentFormatKey.MMFR7,
																				RoutesResultsMmfJson.KEY,
																				RoutesResultsMmfJson.class);
		ActivitySearchDocument document = document(ROUTES_FILENAME, DocumentFormatKey.MMFR7);
		assertEquals("incorrect activity id count", expectedNumberOfWorkouts, document.activityIds().size());
		RoutesMmfJson routes = results.get_embedded();
		List<? extends Workout> workouts = routes.getWorkouts();
		TestUtil.assertSize(expectedNumberOfWorkouts, workouts);
	}

	/**
	 * @param expectedTotal
	 * @param localFilename
	 * @param mostRecentStartTimeString
	 * @return
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 * @throws IOException
	 * @throws DocumentFormatNotRegisteredException
	 */
	private EmbeddedCollection workoutResults(int expectedTotal, int expectedNumberOfWorkouts, String localFilename,
			@Nullable String mostRecentStartTimeString, DocumentFormatKey format, ApplicationKey applicationKey,
			Class<? extends EmbeddedCollection> resultsType) throws DocumentContentsNotRecognizedException,
			DocumentDecodingException, DocumentFormatNotRegisteredException, IOException {
		InputStream is = inputStream(localFilename);
		assertNotNull(is);
		Builder<EmbeddedCollection> builder = MmfJsonReader.create();
		EmbeddedCollection workouts = builder.type(resultsType).build().read(is);
		assertEquals("number of workouts", expectedTotal, workouts.getTotal_count().intValue());

		ActivitySearchDocument document = document(localFilename, format);
		assertEquals("incorrect activity id count", expectedNumberOfWorkouts, document.activityIds().size());
		ActivityId first = document.activityIds().iterator().next();
		assertEquals("id is getting the wrong key", applicationKey, first.getApplicationKey());
		if (mostRecentStartTimeString != null) {
			assertEquals(DateTime.parse(mostRecentStartTimeString).toLocalDate(), document.mostRecentStartDate());
		}
		return workouts;
	}

	private ActivitySearchDocument document(String localFilename, DocumentFormatKey format)
			throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			DocumentFormatNotRegisteredException, IOException {
		return this.factory.getDocument(format, IOUtils.toByteArray(inputStream(localFilename)));

	}

	/**
	 * @param localFilename
	 * @return
	 */
	private InputStream inputStream(String localFilename) {
		InputStream is = ClassLoader.getSystemResourceAsStream(filename(localFilename));
		return is;
	}

	/**
	 * @param localFilename
	 * @return
	 */
	private String filename(String localFilename) {
		return "mmf/" + localFilename + ".json";
	}

}
