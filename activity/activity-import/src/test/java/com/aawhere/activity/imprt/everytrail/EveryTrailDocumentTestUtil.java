/**
 * 
 */
package com.aawhere.activity.imprt.everytrail;

import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;

/**
 * @author aroller
 * 
 */
public class EveryTrailDocumentTestUtil {
	public static final String USER_ID = "245902";
	public static final String USERNAME = "bruceco";
	public static final AccountId ACCOUNT_ID = new AccountId(KnownApplication.EVERY_TRAIL.key, USER_ID);

	public static final String FIRST_NAME = "Bruce";
	public static final String LAST_NAME = "Cohn";
	public static final String NAME = FIRST_NAME + " " + LAST_NAME;

}
