package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.aawhere.activity.imprt.mmf.MapMyFitnessTestUtil;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.test.TestUtil;
import com.google.common.base.Predicate;

public class ActivityImportUtilUnitTest {

	@Test
	public void testDefaultNameFiltered() {
		nameDefaultFilteredAssertion(KnownApplication.MMF.key, MapMyFitnessTestUtil.WALK_DEFAULT_NAME, false);
		nameDefaultFilteredAssertion(KnownApplication.MMF.key, TestUtil.generateRandomAlphaNumeric(), true);
	}

	public void nameDefaultFilteredAssertion(ApplicationKey applicationKey, String name, Boolean expectedToKeep) {
		Predicate<Entry<ActivityId, String>> predicate = ActivityImportUtil.nameDefaultRemovalPredicate();
		assertEquals(expectedToKeep, predicate.apply(Pair.of(ActivityTestUtil.activityId(applicationKey), name)));
	}
}
