/**
 *
 */
package com.aawhere.activity;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;

import com.aawhere.activity.ActivityImportFailure.ActivityImportFailureId;
import com.aawhere.activity.ActivityImportFailure.Builder;
import com.aawhere.activity.ImportMilestone.Status;
import com.aawhere.persist.MockFilterRepository;

/**
 * Tests {@link ActivityImportFailure}.
 * 
 * @author aroller
 * 
 */
public class ActivityImportFailureUnitTest
		extends
		BaseActivityImportMilestoneUnitTest<ActivityImportFailure, ActivityImportFailureId, ActivityImportFailure.Builder, ActivityImportMilestoneRepository> {

	private ActivityId activityId;
	private ActivityImportFailureId id;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		this.activityId = ActivityTestUtil.createApplicationActivityId();
		this.id = new ActivityImportFailureId(activityId);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere
	 * .persist.BaseEntity.Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.setVersion(getProcessorVersion());
		builder.setActivityId(activityId);
	}

	/*
	 * (non-Javadoc) /* (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	public void assertCreation(ActivityImportFailure sample) {

		super.assertCreation(sample);
	}

	/**
	 * Setup a unique instance with the given status.
	 * 
	 * @param status
	 * @return
	 */
	public static ActivityImportFailureUnitTest getInstance(Status status) {
		ActivityImportFailureUnitTest instance = getInstance();
		instance.status = status;
		return instance;
	}

	/**
	 * @return
	 */
	public static ActivityImportFailureUnitTest getInstance() {
		ActivityImportFailureUnitTest instance = new ActivityImportFailureUnitTest();
		instance.baseSetUp();
		instance.setBaseUp();
		instance.setUp();
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.activity.BaseActivityImportMilestoneUnitTest#getProcessorVersion()
	 */
	protected ActivityImportProcessorVersion getProcessorVersion() {
		return ActivityImportProcessorVersion.A;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return new HashSet<>();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getClassesToBeBound()
	 */
	@Override
	protected Class<?>[] getClassesToBeBound() {
		return ArrayUtils.add(super.getClassesToBeBound(), ActivityImportProcessorVersion.class);
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected ActivityImportMilestoneRepository createRepository() {
		return new ActivityImportMilestoneMockRepository();
	}

	public static class ActivityImportMilestoneMockRepository
			extends MockFilterRepository<ActivityImportFailures, ActivityImportFailure, ActivityImportFailureId>
			implements ActivityImportMilestoneRepository {

	}

}
