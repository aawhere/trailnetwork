/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Set;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.ActivityImportTestUtil.DocumentTestRetriever;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.Filter;
import com.aawhere.queue.Queue.Result;
import com.aawhere.queue.QueueTestUtil;
import com.aawhere.queue.QueueTestUtil.TestQueue;
import com.aawhere.test.TestUtil;

import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

/**
 * Tests the {@link ActivityImportCrawler} ensuring crawling will continue and stop correctly based
 * on various settings and responses.
 * 
 * @author aroller
 * 
 */
public class ActivityImportCrawlerUnitTest {

	private boolean deep;
	/** Indicates if a stop is expected because all the activities are known. */
	private boolean expectedShallowStop;
	private boolean finalPage;
	private TestQueue importQueue;
	private TestQueue crawlQueue;
	private Multimap<AccountId, ActivityId> idsExpected;
	private Set<ActivityId> expectedActivityIdsQueued;
	/** Not available until test is run. */
	private ActivityImportCrawler activityImportCrawler;
	private ActivityImportCrawlerDestination crawlerDestination;
	private LocalDate mostRecentStartDateActivityRetrieved;

	@Before
	public void setUp() {
		this.deep = false;
		this.finalPage = true;
		this.expectedShallowStop = true;
		this.idsExpected = HashMultimap.create();
		this.expectedActivityIdsQueued = Sets.newHashSet();
		this.importQueue = (TestQueue) QueueTestUtil.getQueueFactory().getQueue("activityImport");
		this.crawlQueue = (TestQueue) QueueTestUtil.getQueueFactory().getQueue("crawl");
		this.mostRecentStartDateActivityRetrieved = null;

	}

	/**
	 * Runs at the end of every test and does all the work given the setup by the individual test.
	 * 
	 */
	@After
	public void test() {
		if (this.crawlerDestination == null) {
			this.crawlerDestination = ActivityImportCrawlerAccountDestinationUnitTest.create().crawlerDestination;
		}
		DocumentTestRetriever retriever = ActivityImportTestUtil.testRetriever(	idsExpected,
																				this.finalPage,
																				mostRecentStartDateActivityRetrieved);
		this.activityImportCrawler = ActivityImportCrawler.create().activityImportQueue(importQueue)
				.nonExistingIdsFunction(nonExistingIdsFunction()).crawlerDestination(crawlerDestination).deep(deep)
				.crawlQueue(crawlQueue).retriever(retriever).build();
		TestUtil.assertContainsAll(this.idsExpected.values(), activityImportCrawler.activityIdsAvailable());
		TestUtil.assertIterablesEquals(	"account ids don't match",
										this.idsExpected.keySet(),
										activityImportCrawler.accountIdsAvailable());
		if (this.crawlerDestination.isCrawlingByAccount()) {
			TestUtil.assertContainsAll(expectedActivityIdsQueued, activityImportCrawler.idsQueued().get(Result.ADDED));
		} else {
			TestUtil.assertContainsAll(this.idsExpected.keySet(), activityImportCrawler.idsQueued().values());
		}
		assertEquals("final page", this.finalPage, this.activityImportCrawler.finalPage());
		assertEquals("deep", this.deep, this.activityImportCrawler.deep());
		assertEquals(	"shallow stop?",
						this.expectedShallowStop,
						this.activityImportCrawler.stoppedBecauseNoNewActivities());
		Integer expectedCrawlQueueSize;
		if (this.finalPage || this.expectedShallowStop) {
			expectedCrawlQueueSize = 0;
			assertNull(this.activityImportCrawler.nextPageTask());

		} else {
			// one for the next page
			expectedCrawlQueueSize = 1;
			assertNotNull("task is required", this.activityImportCrawler.nextPageTask());
		}
		// the cell crawler shares the same queue as the accounts
		if (crawlerDestination.isCrawlingByCells()) {
			expectedCrawlQueueSize += this.idsExpected.keySet().size();
		}

		QueueTestUtil.assertQueueContains(crawlQueue, expectedCrawlQueueSize);
		QueueTestUtil.assertQueueContains(importQueue, this.expectedActivityIdsQueued.size());

	}

	/**
	 * @return
	 */
	private Function<Set<ActivityId>, Iterable<ActivityId>> nonExistingIdsFunction() {
		final Set<ActivityId> ids = expectedActivityIdsQueued;

		return SetUtilsExtended.constant(ids);
	}

	/**
	 * adds the nubmer of activities to both id sets since none are existing yet.
	 * 
	 * @param count
	 */
	private void addActivitiesNoneExisting(Integer count) {
		for (int i = 0; i <= count; i++) {
			final AccountId accountId = AccountTestUtil.accountId();
			this.idsExpected.put(accountId, ActivityTestUtil.createApplicationActivityId());
		}
		this.expectedActivityIdsQueued.addAll(idsExpected.values());
		// adding this many activities its unlikely to stop
		// override in your method if you expect otherwise
		this.expectedShallowStop = false;

	}

	@Test
	public void testNoActivitiesAvailable() {
		// the default is to be empty. all default expectations should match
	}

	@Test
	public void testActivitiesAvailableNoneExisting() {
		addActivitiesNoneExisting(10);
		this.expectedActivityIdsQueued.addAll(this.idsExpected.values());
	}

	/**
	 * Tests that when some exist only those that don't exist will be posted to the queue
	 * 
	 */
	@Test
	public void testActivitiesAvailableSomeExisting() {
		int count = 10;
		addActivitiesNoneExisting(count);
		// half exist
		this.expectedActivityIdsQueued = Sets.newHashSet(Iterables.limit(this.idsExpected.values(), count / 2));
	}

	/**
	 * ensures that the last page stopps the queue procesing
	 * 
	 */
	@Test
	public void testActivityAvailableAnotherPage() {
		addActivitiesNoneExisting(5);
		this.finalPage = false;
	}

	/** Should stop even though more pages exist. */
	@Test
	public void testAllExistingMorePagesNotDeep() {
		addActivitiesAllExist();
		this.finalPage = false;
		this.expectedShallowStop = true;
	}

	@Test
	public void testAllExistingMorePagesDeep() {
		addActivitiesAllExist();
		this.finalPage = false;
		this.deep = true;
		// just for clarity we expect to continue
		this.expectedShallowStop = false;
	}

	/** The default situation for a cell crawler. Give nothing, expect nothing. */
	@Test
	public void testCellCrawler() {
		crawlByCell();
		// nothing found so similar to default settings
	}

	@Test
	public void testCellCrawlerActivitiesFound() {
		addActivitiesAllExist();
		crawlByCell();

	}

	@Test
	public void testCellCrawlerActivitiesFoundAnotherPage() {
		addActivitiesAllExist();
		crawlByCell();
		this.finalPage = false;

	}

	/** If the crawler finds accounts, but all exist in the queue already. */
	@Test
	public void testCellCrawlerActivitiesFoundNothingNew() {
		addActivitiesAllExist();
		crawlByCell();
		this.crawlQueue = new TestQueue("duplicateCrawlQueue", Result.DUPLICATE);
		this.expectedShallowStop = true;

	}

	@Test
	public void testActivitiesFoundAllTaskDuplicates() {
		// this queue will respond with all duplicates.
		this.importQueue = new TestQueue("activityImportQueueDuplicates", Result.DUPLICATE);
		// just like as if they exist, just in the queue not in the datastore
		addActivitiesAllExist();
		this.deep = false;
		this.expectedShallowStop = true;
	}

	/**
	 * 
	 */
	private void crawlByCell() {
		this.crawlerDestination = ActivityImportCrawlerDestination.create()
				.cells(GeoCellTestUtil.createRandomGeoCells(5, Resolution.EIGHT))
				.applicationKey(KnownApplication.UNKNOWN.key).filter(Filter.create().filter())
				.instructions(ActivityImportTestUtil.mockInstructionsBuilder().build()).build();
	}

	@Test
	public void testActivityImportActivityNotWorthImporting() {
		ActivityImportId activityId = ActivityImportTestUtil.activityIdNotWorthImporting();
		this.idsExpected.put(AccountTestUtil.accountId(), (ActivityId) activityId);
		// this one should be ignored so just go with the default "nothing"
	}

	/**
	 * 
	 */
	private void addActivitiesAllExist() {
		addActivitiesNoneExisting(10);
		// wipe out all of the expected indicating no new activities exist
		this.expectedActivityIdsQueued = Sets.newHashSet();
	}

}
