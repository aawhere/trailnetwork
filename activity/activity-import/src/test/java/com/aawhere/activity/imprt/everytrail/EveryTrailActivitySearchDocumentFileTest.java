/**
 * 
 */
package com.aawhere.activity.imprt.everytrail;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.joda.time.LocalDate;
import org.junit.Test;

import com.aawhere.activity.ActivitySearchDocument;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;

/**
 * @author aroller
 * 
 */
public class EveryTrailActivitySearchDocumentFileTest {

	/**
	 * TN-798 tn-api server is having problems with some pages.
	 * 
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 * @throws IOException
	 */
	@Test
	public void testBadSearchFile() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			IOException {
		String filename = "et/everyTrailSearchDocument-tn-798.xml";
		assertSearchDocument(filename);
	}

	@Test
	public void testUserSearchFile() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			IOException {
		String filename = "et/everyTrailUserSearchDocument.xml";
		ActivitySearchDocument searchDocument = assertSearchDocument(filename);

		assertEquals(	"the most recent activity is not representing",
						LocalDate.parse("2014-05-25"),
						searchDocument.mostRecentStartDate());
	}

	/**
	 * @param filename
	 * @return
	 * @throws IOException
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 */
	private ActivitySearchDocument assertSearchDocument(String filename) throws IOException, DocumentDecodingException,
			DocumentContentsNotRecognizedException {
		EveryTrailSearchDocumentProducer producer = new EveryTrailSearchDocumentProducer();
		InputStream stream = ClassLoader.getSystemResourceAsStream(filename);
		byte[] byteArray = IOUtils.toByteArray(stream);
		Document document = producer.produce(byteArray);
		ActivitySearchDocument searchDocument = (ActivitySearchDocument) document;
		assertEquals(20, searchDocument.activityIds().size());
		assertFalse(searchDocument.finalPage());
		assertEquals(DocumentFormatKey.ETSX, searchDocument.getDocumentFormat());
		LocalDate mostRecentDate = searchDocument.mostRecentStartDate();
		assertNotNull(mostRecentDate);
		return searchDocument;

	}
}
