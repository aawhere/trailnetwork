/**
 * 
 */
package com.aawhere.activity.imprt.everytrail;

import static com.aawhere.activity.imprt.everytrail.EveryTrailDocumentTestUtil.*;
import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.aawhere.activity.imprt.everytrail.EveryTrailUserDocument.Producer;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class EveryTrailUserDocumentUnitTest {
	private static final String EXAMPLE = "<etUserResponse status=\"success\"><user id=\""
			+ USER_ID
			+ "\"><username>"
			+ USERNAME
			+ "</username><firstName>Bruce</firstName><lastName>Cohn</lastName><photo>http://images.everytrail.com/userpics/245902-mecarsonfalls.jpg.JPG</photo><intro>bruceco</intro><intro><![CDATA[blah]]></intro><location/><pictureOffset>24073</pictureOffset></user></etUserResponse>";
	/**
	 * First/Last Name can be empty, but elements still exist.
	 * http://www.everytrail.com/api/user?user_id=245903
	 */
	private static final String EXAMPLE_WITHOUT_NAME = EXAMPLE.replace(FIRST_NAME, StringUtils.EMPTY)
			.replace(LAST_NAME, StringUtils.EMPTY);

	@Test
	public void testDocument() throws SAXException, IOException, ParserConfigurationException {
		EveryTrailUserDocument doc = EveryTrailUserDocument.create().contents(EXAMPLE.getBytes()).build();
		assertDoc(doc);
	}

	@Test
	public void testDocumentWithoutName() throws SAXException, IOException, ParserConfigurationException {
		EveryTrailUserDocument doc = EveryTrailUserDocument.create().contents(EXAMPLE_WITHOUT_NAME.getBytes()).build();
		TestUtil.assertNullExplanation("name wasn't given", doc.name());
	}

	@Test
	public void testDocumentWithoutFirstName() throws SAXException, IOException, ParserConfigurationException {
		EveryTrailUserDocument doc = EveryTrailUserDocument.create()
				.contents(EXAMPLE.replace(FIRST_NAME, StringUtils.EMPTY).getBytes()).build();
		assertEquals(LAST_NAME, doc.name());
	}

	@Test
	public void testDocumentWithoutLastName() throws SAXException, IOException, ParserConfigurationException {
		EveryTrailUserDocument doc = EveryTrailUserDocument.create()
				.contents(EXAMPLE.replace(LAST_NAME, StringUtils.EMPTY).getBytes()).build();
		assertEquals(FIRST_NAME, doc.name());
	}

	/**
	 * @param doc
	 */
	private void assertDoc(EveryTrailUserDocument doc) {
		assertEquals(USER_ID, doc.userId());
		assertEquals(USERNAME, doc.username());
		assertEquals(NAME, doc.name());
	}

	@Test
	public void testProducer() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		Producer producer = EveryTrailUserDocument.producer();
		Document doc = producer.produce(EXAMPLE.getBytes());
		assertDoc((EveryTrailUserDocument) doc);
	}

	@Test(expected = DocumentContentsNotRecognizedException.class)
	public void testProducerWrongType() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		Producer producer = EveryTrailUserDocument.producer();
		producer.produce("junk");
	}

	@Test(expected = DocumentContentsNotRecognizedException.class)
	public void testProducerBadBytes() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		Producer producer = EveryTrailUserDocument.producer();
		producer.produce("junk".getBytes());
	}

}
