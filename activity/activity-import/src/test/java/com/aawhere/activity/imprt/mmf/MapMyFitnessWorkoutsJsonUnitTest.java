/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;

/**
 * @author aroller
 * 
 */
public class MapMyFitnessWorkoutsJsonUnitTest {

	@Test
	public void testHasNext() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		String json = "{ \"_links\": { \"next\": [{"
				+ "\"href\": \"/v7.0/workout/?access_token=xxxx&limit=5&user=11024582&offset=5\"" + "}]}}";

		MapMyFitnessWorkoutsJsonDocument actual = (MapMyFitnessWorkoutsJsonDocument) MapMyFitnessDocumentProducer
				.workouts().produce(json);
		assertFalse("next is available so this shouldn't be final", actual.finalPage());
	}

	@Test
	public void testHasNoNext() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		String json = "{ \"_links\": {}}";

		MapMyFitnessWorkoutsJsonDocument actual = (MapMyFitnessWorkoutsJsonDocument) MapMyFitnessDocumentProducer
				.workouts().produce(json);
		assertTrue("no next, not next page", actual.finalPage());
	}
}
