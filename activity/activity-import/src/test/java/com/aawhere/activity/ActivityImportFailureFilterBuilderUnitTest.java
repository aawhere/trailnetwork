/**
 *
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Test;

import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterUnitTest;
import com.aawhere.personalize.xml.XmlAdapterTestUtils;
import com.aawhere.test.TestUtil;

/**
 * Tests specifics to {@link ActivityImportFailureFilterBuilder} by extending {@link FilterUnitTest}
 * .
 * 
 * @author aroller
 * 
 */
public class ActivityImportFailureFilterBuilderUnitTest
		extends FilterUnitTest {

	@Test
	public void testFilterByActivityId() {
		ActivityId id = ActivityTestUtil.createApplicationActivityId();
		Filter filter = new ActivityImportFailureFilterBuilder().equalsActivityId(id).build();
		@SuppressWarnings("rawtypes")
		List<FilterCondition> conditions = filter.getConditions();
		assertEquals(1, conditions.size());
		FilterCondition<?> condition = conditions.get(0);
		assertEquals(	"activity id translates to the id value itself",
						new ActivityImportFailure.ActivityImportFailureId(id),
						condition.getValue());
		TestUtil.assertEndsWith(condition.getField(), BaseEntity.BASE_FIELD.ID);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseFilterUnitTest#getSampleFilter()
	 */
	@Override
	public Filter getSampleFilter() {
		return new ActivityImportFailureFilterBuilder()
				.equalsActivityId(ActivityTestUtil.createApplicationActivityId()).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseFilterUnitTest#getXmlAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getXmlAdapters() {

		return XmlAdapterTestUtils.getAdapters(ActivityImportFailureUnitTest.getInstance().getFieldDictionaryFactory()
				.getFactory());
	}

}
