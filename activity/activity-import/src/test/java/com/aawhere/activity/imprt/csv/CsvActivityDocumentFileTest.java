/**
 * 
 */
package com.aawhere.activity.imprt.csv;

import static com.aawhere.activity.imprt.csv.CsvActivityDocumentTestUtil.getDocument;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.aawhere.app.UnknownApplicationException;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.io.FileNotFoundException;
import com.aawhere.retrieve.SystemResourceDocumentRetriever;
import com.aawhere.test.category.FileTest;
import com.aawhere.track.Trackpoint;

import com.google.appengine.repackaged.com.google.common.collect.Iterators;

/**
 * Unit testing specifically loading csv files.
 * 
 * @see CsvActivityDocumentUnitTest
 * 
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class CsvActivityDocumentFileTest {

	/**
	 * 
	 */
	private static final String RESOURCE_FOLDER = "csv/2peak/";

	@Before
	public void setUp() {
		DocumentFactory.getInstance().register(new CsvActivityDocumentProducer.Builder().build());
	}

	@Test
	public void testHugeCsv() throws FileNotFoundException, DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		Resource resource = getHugeResource();
		CsvActivityDocument document = getDocument(resource);
		assertHuge(document);

	}

	@Test
	public void testHugeCsvStreamToBytesToDoc() throws FileNotFoundException, DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException, IOException,
			UnknownApplicationException {
		Resource resource = getHugeResource();

		byte[] byteArray = IOUtils.toByteArray(resource.getInputStream());

		CsvActivityDocument document = CsvActivityDocument.create(byteArray).build();
		assertHuge(document);

	}

	/**
	 * @return
	 */
	private Resource getHugeResource() {
		Resource resource = new ClassPathResource(RESOURCE_FOLDER + "huge.csv");
		return resource;
	}

	/**
	 * @param document
	 */
	private void assertHuge(CsvActivityDocument document) {
		assertNotNull(document.getActivityId());
		assertEquals(16224, Iterators.size(document.getTrack().iterator()));
	}

	@Test
	public void test2PeakCsv() throws FileNotFoundException, DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {

		Resource resource = new ClassPathResource(RESOURCE_FOLDER + "route_199970336.csv");
		SystemResourceDocumentRetriever retriever = new SystemResourceDocumentRetriever(resource);
		retriever.retrieve(DocumentFormatKey.ACTIVITY_CSV);
		Document document = retriever.getDocument();
		assertNotNull(document);
		assertTrue(document instanceof CsvActivityDocument);
		CsvActivityDocument csv = (CsvActivityDocument) document;
		assertEquals("v1.0", csv.getVersion());
		assertEquals("199970336", csv.getActivityId());
		assertEquals("182350", csv.getUserId());
		assertEquals("running", csv.getActivityType());
		Iterator<Trackpoint> iterator = csv.getTrack().iterator();

		assertEquals(507, Iterators.size(iterator));

		// leave the rest up to unit testing
	}
}
