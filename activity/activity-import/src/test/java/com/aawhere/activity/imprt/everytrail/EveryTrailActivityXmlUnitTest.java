/**
 * 
 */
package com.aawhere.activity.imprt.everytrail;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.aawhere.activity.ActivityIdFixture;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class EveryTrailActivityXmlUnitTest {
	public static final String ID = ActivityIdFixture.EVERY_TRAIL;
	public static final String NAME = "Around The Hut";
	public static final String ACCOUNT_ID = "87486";
	public static final String ACTIVITY_TYPE_ID = "1";
	public static final String DESCRIPTION = "just cruising";
	public static final String DATE = "2012-12-31";

	public static final String TRIP_XML = tripXml(ID);

	/**
	 * @return
	 */
	private static String tripXml(String tripId) {
		return "<trip id=\""
				+ tripId
				+ "\"><name>"
				+ NAME
				+ "</name><user id=\""
				+ ACCOUNT_ID
				+ "\"></user><activity id=\""
				+ ACTIVITY_TYPE_ID
				+ "\"></activity><description><![CDATA["
				+ DESCRIPTION
				+ "]]></description><tips><![CDATA[]]></tips><date>"
				+ DATE
				+ "</date><length units=\"metric\">3270.8975606764</length><duration>6168</duration><rating votes=\"0\">0.0</rating><location lat=\"39.375611669\" lon=\"-120.182561846\">Prosser Lakeview Estates, California, United States</location><gpx>http://cdn.everytrail.com/gpx/2121681.gpx</gpx><kml>http://cdn.everytrail.com/kml/2121681.kml</kml></trip>";
	}

	public static final String SINGLE_TRIP_RESPONSE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><etTripResponse status=\"success\">"
			+ TRIP_XML + "</etTripResponse>";
	public static final String TRIP_SEARCH_RESPONSE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><etTripSearchResponse status=\"success\">"
			+ TRIP_XML + tripXml(ACTIVITY_TYPE_ID + "2") + "</etTripSearchResponse>";

	@Test
	public void testActivityDocument() throws SAXException, IOException, ParserConfigurationException {
		EveryTrailActivityDocument activityDocument = EveryTrailActivityDocument.create()
				.contents(SINGLE_TRIP_RESPONSE.getBytes()).build();
		assertEquals(ID, activityDocument.getActivityId());
		assertEquals(NAME, activityDocument.getActivityName());
		assertEquals(ACCOUNT_ID, activityDocument.getAccount().getRemoteId());
		assertEquals(ACTIVITY_TYPE_ID, activityDocument.getActivityType());
		assertEquals(DESCRIPTION, activityDocument.getDescription());
		assertEquals(LocalDate.parse(DATE), activityDocument.getDate());
	}

	@Test
	public void testSearchDocument() throws SAXException, IOException, ParserConfigurationException {
		EveryTrailActivityDocument activityDocument = EveryTrailActivityDocument.create()
				.contents(TRIP_SEARCH_RESPONSE.getBytes()).build();
		TestUtil.assertSize(2, activityDocument.activityIds());
	}
}
