/**
 * 
 */
package com.aawhere.activity.imprt.mmf;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.activity.imprt.mmf.MmfJsonReader.Builder;
import com.aawhere.app.AccountDocument;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.test.TestUtil;

/**
 * for {@link User}.
 * 
 * @author aroller
 * 
 */
public class MapMyFitnessUserJsonUnitTest {
	private final String expected = "KristenN6";
	private final String usernameJson = "{\"username\": \"" + expected + "\"}";

	private MmfJsonReader<User> reader;

	@Before
	public void setUp() {
		final Builder<User> builder = MmfJsonReader.create();
		this.reader = builder.type(User.class).build();
	}

	@Test
	public void testUsername() throws DocumentDecodingException {

		User user = reader.read(usernameJson);
		assertEquals("username", expected, user.getUsername());
	}

	@Test
	public void testDocumentProducer() throws DocumentContentsNotRecognizedException, DocumentDecodingException {
		MapMyFitnessDocumentProducer<User> producer = MapMyFitnessDocumentProducer.user();
		Document document = producer.produce(usernameJson);
		assertNotNull(document);
		TestUtil.assertInstanceOf(AccountDocument.class, document);
		AccountDocument accountDocument = (AccountDocument) document;
		assertEquals(expected, accountDocument.username());
	}
}
