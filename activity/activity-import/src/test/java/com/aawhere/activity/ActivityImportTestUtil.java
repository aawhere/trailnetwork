/**
 * 
 */
package com.aawhere.activity;

import java.util.Set;

import org.joda.time.LocalDate;

import com.aawhere.activity.ApplicationActivityImportInstructions.Destination;
import com.aawhere.activity.timing.ActivityTimingWebApiUri;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.DocumentRetriever;

import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class ActivityImportTestUtil {

	/**
	 * Provides a fake retriever completely ignoring the destination and return a test document
	 * retriever that will produce the given ids.
	 * 
	 * @param ids
	 * @param finalPage
	 * @return
	 */
	public static Function<ActivityImportCrawlerDestination, DocumentRetriever> retrieverFunction(
			final Multimap<AccountId, ActivityId> ids, final Boolean finalPage, final LocalDate mostRecentStartDate) {
		return new Function<ActivityImportCrawlerDestination, DocumentRetriever>() {

			@Override
			public DocumentRetriever apply(ActivityImportCrawlerDestination input) {
				return testRetriever(ids, finalPage, mostRecentStartDate);
			}

		};
	}

	public static ApplicationActivityImportInstructionsRepository mockRepo() {
		return ApplicationActivityImportInstructionsRepository.create()
				.addInstructions(mockInstructionsBuilder().build()).build();
	}

	public static com.aawhere.activity.ApplicationActivityImportInstructions.Builder mockInstructionsBuilder() {
		final String base = "http://httpstat.us/200/";
		DocumentFormatKey format = DocumentFormatKey.TEXT;
		return new ApplicationActivityImportInstructions.Builder()
				.setApplication(KnownApplication.UNKNOWN.asApplication())
				.addActivityUrl(base + ActivityWebApiUri.ACTIVITY_ID_PARAM, format)
				.setAccountDestination(new Destination(base + "/account/"
						+ ApplicationActivityImportInstructions.FIELD.PARAM.ACCOUNT_REMOTE_ID, format))
				.setSearchDestination(new Destination(base + "search", format))
				.setActivitySearchForAccountDestination(new Destination(base + "search/account/"
						+ ApplicationActivityImportInstructions.FIELD.PARAM.ACCOUNT_REMOTE_ID, format))
				.setCourseCompletedCallbackUrl(base + ActivityTimingWebApiUri.COURSE_PARAM + "?attempt="
						+ ActivityTimingWebApiUri.ATTEMPT_PARAM)
				.addActivityTypeMapping(ActivityType.OTHER.name(), ActivityType.OTHER)
				.addActivityTypeMapping(ActivityType.BIKE.name(), ActivityType.BIKE)
				.addActivityTypeMapping(ActivityType.HIKE.name(), ActivityType.HIKE)
				.addActivityTypeMapping(ActivityType.RUN.name(), ActivityType.RUN)
				.addActivityTypeMapping(ActivityType.SKATE.name(), ActivityType.SKATE)
				.addActivityTypeMapping(ActivityType.MTB.name(), ActivityType.MTB)
				.addActivityTypeMapping(ActivityType.SWIM.name(), ActivityType.SWIM)
				.addActivityTypeMapping(ActivityType.SKI.name(), ActivityType.SKI)
				.addActivityTypeMapping(ActivityType.WALK.name(), ActivityType.WALK);
	}

	/**
	 * @param ids
	 * @param finalPage
	 * @return
	 */
	public static DocumentTestRetriever testRetriever(final Multimap<AccountId, ActivityId> ids,
			final Boolean finalPage, LocalDate mostRecentDate) {
		return new DocumentTestRetriever(new ActivitySearchTestDocument(ids, finalPage, mostRecentDate));
	}

	@SuppressWarnings("serial")
	public static class DocumentTestRetriever
			implements DocumentRetriever {
		ActivitySearchTestDocument document;

		public DocumentTestRetriever(ActivitySearchTestDocument activitySearchTestDocument) {
			this.document = activitySearchTestDocument;
		}

		@Override
		public void retrieve(DocumentFormatKey expectedFormat) throws BaseException {
			// do nothing...we already have the data
		}

		@Override
		public String getProviderDocumentIdentifier() {
			return null;
		}

		@Override
		public Document getDocument() {
			return document;
		}

		@Override
		public Object getOrigionalData() {
			return null;
		}

	}

	public static class ActivitySearchTestDocument
			implements ActivitySearchDocument {
		private Boolean finalPage = false;
		private Multimap<AccountId, ActivityId> ids = HashMultimap.create();
		private LocalDate mostRecentStartDate;

		/**
		 * @param activityIds
		 * @param finalPage
		 */
		public ActivitySearchTestDocument(Multimap<AccountId, ActivityId> activityIds, Boolean finalPage,
				LocalDate mostRecent) {
			this.ids = activityIds;
			this.finalPage = finalPage;
			this.mostRecentStartDate = mostRecent;
		}

		@Override
		public byte[] toByteArray() {
			throw new RuntimeException("TODO:Implement this");
		}

		@Override
		public DocumentFormatKey getDocumentFormat() {
			throw new RuntimeException("TODO:Implement this");
		}

		@Override
		public Set<ActivityId> activityIds() {
			return Sets.newHashSet(this.ids.values());
		}

		@Override
		public Boolean finalPage() {
			return this.finalPage;
		}

		/*
		 * @see com.aawhere.activity.ActivitySearchDocument#ids()
		 */
		@Override
		public Multimap<AccountId, ActivityId> ids() {
			return this.ids;
		}

		/*
		 * @see com.aawhere.activity.ActivitySearchDocument#mostRecentStartDate()
		 */
		@Override
		public LocalDate mostRecentStartDate() {

			return mostRecentStartDate;
		}

	}

	/**
	 * Produces some random number account ids mapped to some random number of activity ids.
	 * 
	 * @return
	 */
	public static Multimap<AccountId, ActivityId> ids() {
		HashMultimap<AccountId, ActivityId> ids = HashMultimap.create();
		Set<AccountId> accountIds = AccountTestUtil.accountIds();
		for (AccountId accountId : accountIds) {
			ids.putAll(accountId, ActivityTestUtil.createActivityIds());
		}

		return ids;

	}

	/**
	 * Provides a random activity id that returns false for being valuable.
	 * 
	 * @return
	 */
	public static ActivityImportId activityIdNotWorthImporting() {
		ActivityId activityId = ActivityTestUtil.createApplicationActivityId();
		return new ActivityImportId.NotWorthImporting(activityId.getApplicationKey(), activityId.getRemoteId());
	}
}
