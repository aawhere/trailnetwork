/**
 * 
 */
package com.aawhere.activity.imprt.csv;

import org.springframework.core.io.Resource;

import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.io.FileNotFoundException;
import com.aawhere.retrieve.SystemResourceDocumentRetriever;

/**
 * @author aroller
 * 
 */
public class CsvActivityDocumentTestUtil {

	/**
	 * @param resource
	 * @return
	 * @throws FileNotFoundException
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentFormatNotRegisteredException
	 */
	public static CsvActivityDocument getDocument(Resource resource) throws FileNotFoundException,
			DocumentDecodingException, DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		DocumentFactory.getInstance().register(new CsvActivityDocumentProducer.Builder().build());
		SystemResourceDocumentRetriever retriever = new SystemResourceDocumentRetriever(resource);
		retriever.retrieve(DocumentFormatKey.ACTIVITY_CSV);

		CsvActivityDocument document = (CsvActivityDocument) retriever.getDocument();
		return document;
	}
}
