/**
 * 
 */
package com.aawhere.activity.imprt.rwgps;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aawhere.activity.ActivityImportFixture;

/**
 * @author aroller
 * 
 */
public class RWGpsImportTestUtil {

	public static final String USER_ACTIVITIES_JSON_FILE = "rwgps/rwgps-user-2-activities.json";
	public static final Integer USER_ACTIVITIES_COUNT = 20;
	public static final String USER_ACTIVITIES_EMPTY_JSON_FILE = "rwgps/rwgps-user-2-activities-empty.json";
	public static final Integer USER_ACTIVITIES_EMPTY_COUNT = 0;
	/** Flagged on activity to not have gps. */
	public static final String USER_ACTIVITIES_NO_GPS_JSON_FILE = "rwgps/rwgps-user-2-activities-no-gps.json";
	public static final String TRIP_JSON_FILE = "rwgps/rwgps-trip-94.json";
	public static final Integer USER_ACTIVITIES_WITH_NO_GPS_COUNT = 1;
	public static final LocalDate USER_ACTIVITIES_MOST_RECENT = LocalDate.parse("2014-06-06");
	public static final String TRIP_ID = ActivityImportFixture.RW_EXAMPLE_ACTIVITY.getRemoteId();
	public static final String TRIP_NAME = "Peak To Peak";
	public static final String TRIP_DESCRIPTION_START = "The Peak to Peak is";
	public static final String TRIP_USER_NAME = "Cullen King";
	public static final String TRIP_USSER_ID = ActivityImportFixture.CULLEN_RW_ACCOUNT_ID.getRemoteId();
	public static final Integer TRIP_TRACKPOINT_COUNT = 1464;
	public static final Double TRIP_TRACKPOINT_FIRST_LON = -122.758382;
	public static final Double TRIP_TRACKPOINT_FIRST_LAT = 45.384904;
	public static final Long TRIP_TRACKPOINT_FIRST_UNIX_TIME = 1216570740l;
	public static final Double TRIP_TRACKPOINT_FIRST_ELE = 46.7612;

	public static final LocalDate TRIP_DATE = LocalDate.parse("2008-07-20");
	public static final DateTime TRIP_TRACKPOINT_FIRST_TIMESTAMP = DateTime.parse("2008-07-20T16:19:00Z");

}
