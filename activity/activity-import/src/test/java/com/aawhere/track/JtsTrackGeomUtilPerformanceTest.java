/**
 * 
 */
package com.aawhere.track;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.aawhere.activity.imprt.csv.CsvActivityDocument;
import com.aawhere.activity.imprt.csv.CsvActivityDocumentTestUtil;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.doc.track.gpx._1._1.GpxTestUtilFactory;
import com.aawhere.doc.track.gpx._1._1.GpxTrack;
import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxTestUtil;
import com.aawhere.io.FileNotFoundException;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class JtsTrackGeomUtilPerformanceTest {

	/**
	 * This may be heavy so just load it once and keep the delay out of the test itself. It is
	 * immutable.
	 */
	private static Track track;

	@BeforeClass
	public static void setUpClass() throws GpxReadException {
		Gpx gpx = GpxTestUtilFactory.getUtil().getTestGpx(GpxTestUtil.GARMIN_CONNECT_ACTIVITY_BLOATED);
		track = new GpxTrack(gpx);
	}

	/**
	 * This is a duplicate test that warms up the system to test the others since the first one
	 * seems to take a lot longer.
	 * 
	 */
	@Test
	public void testWarmup() {
		MultiLineString lineString = JtsTrackGeomUtil.trackToMultiLineString(track);
		assertNotNull(lineString);
	}

	@Test
	public void testTrackToMultiLineStringWithoutDuplicateTrackpoints() {
		Geometry lineString = JtsTrackGeomUtil.trackToMultiLineStringWithoutDuplicateTrackpoints(track);
		assertNotNull(lineString);

	}

	@Test
	public void testTrackToMultiLineString() {
		MultiLineString lineString = JtsTrackGeomUtil.trackToMultiLineString(track);
		assertNotNull(lineString);
	}

	/**
	 * // @Ignore("https://aawhere.jira.com/browse/TN-206")
	 * 
	 * @throws DocumentContentsNotRecognizedException
	 * @throws FileNotFoundException
	 * @throws DocumentDecodingException
	 * @throws DocumentFormatNotRegisteredException
	 */
	@Test
	public void testOutOfMemoryBufferActivity() throws DocumentContentsNotRecognizedException, FileNotFoundException,
			DocumentDecodingException, DocumentFormatNotRegisteredException {
		// causes overlow if precision is not provided
		// Resource resource1 = new ClassPathResource("2p-210982104.csv");
		// causes overflow if precision is not provided
		Resource resource1 = new ClassPathResource("csv/2peak/2p-211332752.csv");
		// Resource resource1 = new ClassPathResource("2p-214943340.csv");

		CsvActivityDocument document1 = CsvActivityDocumentTestUtil.getDocument(resource1);

		Track track1 = document1.getTrack();

		Pair<? extends Geometry, ? extends Geometry> geometries = JtsTrackGeomUtil.buffer(track1, MeasurementUtil
				.createLengthInMeters(20));
		Geometry buffer = geometries.getLeft();
		List<Trackpoint> trackpointsFrom = TrackUtil.trackpointsFrom(track1);
		TestUtil.assertGreaterThan(trackpointsFrom.size() / 4, buffer.getNumPoints());
		Geometry line = geometries.getRight();
		TestUtil.assertGreaterThan(trackpointsFrom.size() / 2, line.getNumPoints());

		// lineString.buffer(bufferDistance);
		// this was causing the original problem, but the above has isolated it to a simpler test
		// BufferedLineStringSimilarityCalculator calculator = new
		// BufferedLineStringSimilarityCalculator.Builder()
		// .setLatitude(startLatitude).setSourceGeometry(multiLineString1).setTargetGeometry(multiLineString1)
		// .build();
		//
		// assertNotNull(calculator);

	}
}
