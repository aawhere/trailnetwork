package com.aawhere.track.process.calc;

import static com.aawhere.track.process.calc.TrackSignalQualityFileTestUtil.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.test.category.FileTest;
import com.aawhere.track.Track;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.AutomaticTrackProcessor.Builder;

/**
 * FIXME: these values
 * 
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class TrackSignalQualityCalculatorFileTest {

	@Test
	public void testMmfMessySanitasTrack() {

		String filename = MM_134305617_SANITAS_POOR_CHOICE_WORKOUT_JSON;
		SignalQuality expectedQuality = SignalQuality.POOR;
		String message = "TN-934 this is an activity with bad signal ";

		assertQuality(filename, expectedQuality, message);
	}

	@Test
	public void testMmfGarminEdge800MtbRide() {
		assertQuality(	MM_946555471_GARMIN_EDGE_800_WORKOUT_JSON,
						SignalQuality.GOOD,
						"Edge 800 is a strong device and the track looks good on a map");
	}

	@Test
	public void testMmfSanitasGoodChoice() {
		assertQuality(	MM_808575827_SANITAS_IPHONE_GOOD_CHOICE,
						SignalQuality.GREAT,
						"Aaron thought this looked good on the map");
	}

	@Test
	public void testMmfMapmyrideiphoneWithPoorSignal() {
		assertQuality(	MM_119469127_SANITAS_PHONE_POOR_SIGNAL_JSON,
						SignalQuality.GREAT,
						"iPhone isn't producing a quality signal. Lots of loops, and lost signals. http://dev.trailnetwork.com/activity/mm-119469127?debug");
	}

	public void assertQuality(String filename, SignalQuality expectedQuality, String message) {
		Track track = track(filename);
		Builder processorBuilder = AutomaticTrackProcessor.create(track);
		TrackSignalQualityCalculator signalQualityCalculator = ProcessingCalculatorFactory.create(processorBuilder)
				.signalQualityCalculator();
		processorBuilder.build();
		SignalQuality actualQuality = signalQualityCalculator.get();
		assertEquals(message + signalQualityCalculator.breeches(), expectedQuality, actualQuality);
	}

}
