package com.aawhere.track.process.calc;

import com.aawhere.activity.imprt.mmf.MapMyFitnessWorkoutJsonFileTest;
import com.aawhere.track.Track;

public class TrackSignalQualityFileTestUtil {
	static final String MM_119469127_SANITAS_PHONE_POOR_SIGNAL_JSON = "mm-119469127-phone-poor-signal.json";
	/**
	 * A social mountain bike ride with many stops and hanging out waiting for others. Plenty of
	 * tree coverage, but a lot of road riding to ge to/from the trailhead. Overall the signal
	 * quality looks pretty good. http://www.mapmyfitness.com/workout/946555471
	 * 
	 */
	static final String MM_946555471_GARMIN_EDGE_800_WORKOUT_JSON = "mm-946555471-garmin_edge_800.workout.json";
	/** Sanitas Run with a bad signal at the start, but it was chosen to represent the group. */
	static final String MM_134305617_SANITAS_POOR_CHOICE_WORKOUT_JSON = "mm-134305617.workout.json";

	static final String MM_808575827_SANITAS_IPHONE_GOOD_CHOICE = "mm-808575827-sanitas-good-iphone.workout.json";
	public static final String MMF_QUALITY_FOLDER_PATH = "mmf/quality/";

	public static Track track(String filename) {
		return MapMyFitnessWorkoutJsonFileTest.track(MMF_QUALITY_FOLDER_PATH + filename);
	}
}
