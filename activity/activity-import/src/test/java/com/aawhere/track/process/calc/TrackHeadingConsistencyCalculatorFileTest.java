package com.aawhere.track.process.calc;

import static com.aawhere.track.process.calc.TrackSignalQualityFileTestUtil.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.track.Track;
import com.aawhere.track.process.SupplierMultisetProcessorListener;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.AutomaticTrackProcessor.Builder;
import com.google.common.collect.ImmutableMultiset;

/**
 * Using real files this analyses the tracks and determines the quality.
 * 
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class TrackHeadingConsistencyCalculatorFileTest {

	@Test
	public void testGarminEdgeMtbSocialRideFile() {
		ImmutableMultiset<SignalQuality> expectedCounts = ImmutableMultiset.<SignalQuality> builder()
				.setCount(SignalQuality.GREAT, 4085).setCount(SignalQuality.GOOD, 327).setCount(SignalQuality.FAIR, 97)
				.setCount(SignalQuality.POOR, 149).build();

		String filename = MM_946555471_GARMIN_EDGE_800_WORKOUT_JSON;
		assertFile(filename, expectedCounts, SignalQuality.GOOD);
	}

	@Test
	public void testPoorSanitasFile() {
		ImmutableMultiset<SignalQuality> expectedCounts = ImmutableMultiset.<SignalQuality> builder()
				.setCount(SignalQuality.GREAT, 1914).setCount(SignalQuality.GOOD, 484)
				.setCount(SignalQuality.FAIR, 166).setCount(SignalQuality.POOR, 115).build();

		assertFile(MM_134305617_SANITAS_POOR_CHOICE_WORKOUT_JSON, expectedCounts, SignalQuality.FAIR);
	}

	@Test
	public void testPoorSanitasIphone() {
		ImmutableMultiset<SignalQuality> expectedCounts = ImmutableMultiset.<SignalQuality> builder()
				.setCount(SignalQuality.GREAT, 1642).setCount(SignalQuality.GOOD, 775)
				.setCount(SignalQuality.FAIR, 354).setCount(SignalQuality.POOR, 161).build();

		assertFile(MM_119469127_SANITAS_PHONE_POOR_SIGNAL_JSON, expectedCounts, SignalQuality.POOR);
	}

	@Test
	public void testGoodSanitasIphone() {
		ImmutableMultiset<SignalQuality> expectedCounts = ImmutableMultiset.<SignalQuality> builder()
				.setCount(SignalQuality.GREAT, 2543).setCount(SignalQuality.GOOD, 602)
				.setCount(SignalQuality.FAIR, 331).setCount(SignalQuality.POOR, 304).build();

		// although the track appears to be good, the analysis says otherwise. this could be a sign
		// of incorrect observation on the part of the algorithm or me (2015-04-18)
		assertFile(MM_808575827_SANITAS_IPHONE_GOOD_CHOICE, expectedCounts, SignalQuality.POOR);
	}

	private void assertFile(String filename, ImmutableMultiset<SignalQuality> expectedCounts,
			SignalQuality expectedSummary) {
		Track track = track(filename);
		assertNotNull(track);
		Builder processorBuilder = AutomaticTrackProcessor.create(track);
		TrackHeadingConsitencyCalculator consitencyCalculator = TrackHeadingConsitencyCalculator.create()
				.registerWithProcessor(processorBuilder).counter().build();
		SupplierMultisetProcessorListener<SignalQuality> qualityCounter = consitencyCalculator.counter();
		processorBuilder.build();
		// FIXME:this is too specific so when you come back here because something changed...remove
		// it and just use the category
		assertEquals("signal counts", expectedCounts, qualityCounter.get());
		assertEquals(	"summary " + MeasurementUtil.ratios(qualityCounter.get()),
						expectedSummary,
						consitencyCalculator.summary());
	}
}
