/**
 * 
 */
package com.aawhere.doc.track.gpx._1._1;

import com.aawhere.gpx._1._1.GpxJibxTestUtil;
import com.aawhere.gpx._1._1.GpxTestUtil;

/**
 * I get a valid {@link GpxTestUtil}.
 * 
 * @author Brian Chapman
 * 
 */
public class GpxTestUtilFactory {

	public static GpxTestUtil getUtil() {
		return new GpxJibxTestUtil();
	}

}
