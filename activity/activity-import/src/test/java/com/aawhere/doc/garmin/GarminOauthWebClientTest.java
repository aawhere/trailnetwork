/**
 * 
 */
package com.aawhere.doc.garmin;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.aawhere.net.HttpStatusCode;
import com.aawhere.test.category.ManualTest;
import com.aawhere.test.category.WebClientTest;

/**
 * @author aroller
 * 
 */
@Category({ WebClientTest.class, ManualTest.class })
@Ignore("v25 currently failing and not necessary for system function.")
public class GarminOauthWebClientTest {

	private OAuthService service;

	@Before
	public void setUp() {
		this.service = new ServiceBuilder().provider(GarminConnectApi.class)
				.apiKey("06bef1c6-ee89-4891-b1b4-9b93077e9d6f").apiSecret("0Zsegwx4PpVvcxj7aHQOY0J6g19HRKT68e9")
				.build();
	}

	/**
	 * The first step to get an unauthorized token to send the user to get requested and provide a
	 * verifier.
	 * 
	 */
	@Test
	public void testGetRequestToken() {

		Token requestToken = service.getRequestToken();
		assertNotNull(requestToken);

		// System.out.println(requestToken.toString() + " for " +
		// service.getAuthorizationUrl(requestToken));

	}

	/**
	 * After running {@link #testGetRequestToken()} you'll need to manually go to the url provided,
	 * authorize and grab the verifier from the forwarded URL.
	 * 
	 */
	@Test
	@Category(ManualTest.class)
	public void testGetAccessToken() {
		String verifierValue = "r1xjjja2Fy";
		String tokenValue = "95a7a4ca-52d1-423f-9aaa-c990b53f8305";
		String tokenSecret = "wVXKcVegcvz4dz7MJslZC3wF3JUqZqCdebz";
		Token requestToken = new Token(tokenValue, tokenSecret);
		Verifier verifier = new Verifier(verifierValue);
		Token accessToken = service.getAccessToken(requestToken, verifier);
		assertNotNull(accessToken);
		// System.out.println("access token: " + accessToken.toString());
	}

	/**
	 * Taken the access token provided from {@link #testGetAccessToken()} this will ensure there is
	 * access to the private and public resources alike.
	 * 
	 */
	@Test
	public void testAccessToResources() {
		// these are the two values that must be persisted.
		String tokenValue = "b36ac8d8-a477-4132-b5c0-702d6a8b13ee";
		String tokenSecret = "cT6kPGuFRuWs4u7xQqSCk22VbWM9A5dAS34";
		Token accessToken = new Token(tokenValue, tokenSecret);
		final String publicUrl = "http://connectapitest.garmin.com/activity-service-1.1/axm/activity/13132531";
		assertAccess(accessToken, publicUrl);
		final String privateUrl = "http://connectapitest.garmin.com/activity-service-1.1/axm/activity/13132532";
		assertAccess(accessToken, privateUrl);
	}

	/**
	 * @param accessToken
	 * @param url
	 */
	private void assertAccess(Token accessToken, final String url) {
		OAuthRequest request = new OAuthRequest(Verb.GET, url);
		service.signRequest(accessToken, request);
		Response publicResponse = request.send();
		assertEquals(HttpStatusCode.OK.value, publicResponse.getCode());
	}

	public static class GarminConnectApi
			extends DefaultApi10a {
		@Override
		public String getRequestTokenEndpoint() {
			return "http://connectapitest.garmin.com/oauth-service-1.0/oauth/request_token";

		}

		@Override
		public String getAccessTokenEndpoint() {
			return "http://connectapitest.garmin.com/oauth-service-1.0/oauth/access_token";
		}

		@Override
		public String getAuthorizationUrl(Token requestToken) {
			return "http://connecttest.garmin.com/oauthConfirm?oauth_token=" + requestToken.getToken();

		}

	}
}
