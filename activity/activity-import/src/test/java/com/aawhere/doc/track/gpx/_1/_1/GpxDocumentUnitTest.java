/**
 *
 */
package com.aawhere.doc.track.gpx._1._1;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.doc.DocumentTestUtil;
import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxUnitTest;
import com.aawhere.track.Track;

import com.google.common.collect.Iterators;

/**
 * In memory testing of string xml.
 * 
 * @see GpxDocumentFileUnitTest for testing GPX files.
 * 
 * @author Aaron Roller
 * 
 */
@Category(com.aawhere.test.category.UnitTest.class)
public class GpxDocumentUnitTest {

	@BeforeClass
	static public void setUpDocumentFactory() {
		DocumentFactory.getInstance().register(new GpxDocumentProducer.Builder().build());
	}

	@Test
	public void testMinimums() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		assertGpxString(GpxUnitTest.MINIMUM_WITH_NAMESPACE);
		assertGpxString(GpxUnitTest.GARMIN_CONNECT_MIN);
		assertGpxString(GpxUnitTest.GARMIN_CONNECT_COMPLETE);

	}

	@Test(expected = DocumentContentsNotRecognizedException.class)
	public void testMinimumsIncorrectDocumentFormat() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		assertGpxString(GpxUnitTest.MINIMUM);
		assertGpxString(GpxUnitTest.MINIMUM_WITH_VERSION);
	}

	static public void assertGpxString(String xml) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {

		Document doc = DocumentFactory.getInstance().getDocument(DocumentFormatKey.GPX_1_1, xml);
		assertNotNull(doc);
		assertTrue("Expecting a GpxDocument", doc instanceof GpxDocument);
		GpxDocument gpxDoc = (GpxDocument) doc;
		assertEquals(1, Iterators.size(gpxDoc.iterator()));
		assertEquals(DocumentFormatKey.GPX_1_1, gpxDoc.getDocumentFormat());
		Gpx root = gpxDoc.getGpx();
		assertNotNull(root);
		Track track = gpxDoc.getTrack();
		assertNotNull(track);
		// leave it up to more specific tests to test the details
	}

	@Test
	public void testByteArray() {
		final DocumentFormatKey format = DocumentFormatKey.GPX_1_1;
		final String xml = GpxUnitTest.GARMIN_CONNECT_COMPLETE;
		DocumentTestUtil.assertByteArray(format, xml);

	}

	@Test(expected = DocumentContentsNotRecognizedException.class)
	public void testProducerNotHandling() throws DocumentDecodingException, DocumentContentsNotRecognizedException {
		new GpxDocumentProducer.Builder().build().produce(getClass());
	}

}
