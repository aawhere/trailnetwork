/**
 *
 */
package com.aawhere.doc;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.aawhere.activity.UserNameNotProvidedException;
import com.aawhere.app.Installation;
import com.aawhere.app.InstanceKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.Account;
import com.aawhere.xml.bind.JAXBReadException;

import com.garmin.developer.schemas.axm.AxmAxm;
import com.garmin.developer.schemas.axm.AxmUnitTest;
import com.garmin.developer.schemas.axm.DefinitionAxm;
import com.garmin.developer.schemas.axm.UserAxm;

/**
 * @author Brian Chapman
 * 
 */
public class AxmDocumentUtilUnitTest {

	public static final String ACTIVITY_3006961_USERNAME = "aroller";
	public static final String ACTIVITY_3006961_ID = "1";

	/**
	 * Test method for
	 * {@link com.aawhere.doc.AxmDocumentUtil#findUserName(com.aawhere.doc.AxmDocument)}.
	 * 
	 * @throws JAXBReadException
	 * @throws UserNameNotProvidedException
	 */
	@Test
	public void testFindUserName() throws JAXBReadException, UserNameNotProvidedException {
		AxmAxm axm = getAxm();
		AxmDocument document = new AxmDocument.Builder().setRoot(axm).build();
		InstanceKey username = AxmDocumentUtil.findUserName(document);
		assertNotNull("username", username);
		assertEquals(ACTIVITY_3006961_ID, username.getValue());
	}

	@Test
	public void testFindAccount() throws JAXBReadException, UserNameNotProvidedException {
		AxmAxm axm = getAxm();
		Account account = AxmDocumentUtil.findAccount(axm);
		assertEquals("account id", ACTIVITY_3006961_ID, account.getRemoteId());
		assertEquals("aliases", 1, account.getAliases().size());
		assertEquals("aliases", ACTIVITY_3006961_USERNAME, account.getAliases().iterator().next());

	}

	@Test(expected = UserNameNotProvidedException.class)
	public void testFindUserNameWhenNonePresent() throws JAXBReadException, UserNameNotProvidedException {
		AxmAxm axm = getAxm();
		axm.getActivities().iterator().next().setUser(null);
		AxmDocument document = new AxmDocument.Builder().setRoot(axm).build();
		AxmDocumentUtil.findUserName(document);
	}

	@Test
	public void testGetAuthor() throws JAXBReadException {
		AxmAxm axm = getAxm();
		Installation author = AxmDocumentUtil.getAuthor(axm);
		assertEquals("application", KnownApplication.GARMIN_CONNECT.asApplication(), author.getApplication());
		assertNotNull("version", author.getVersion());
		assertNotNull("date", author.getTimestamp());
	}

	/**
	 * @return
	 * @throws JAXBReadException
	 */
	private AxmAxm getAxm() throws JAXBReadException {
		return AxmUnitTest.axmFrom("activity_" + 3006961 + ".axm");
	}

	@Test(expected = UserNameNotProvidedException.class)
	public void testFindUserNameWhenDefinitionNotPresent() throws JAXBReadException, UserNameNotProvidedException {
		AxmAxm axm = getAxm();
		List<DefinitionAxm> definitions = axm.getDictionary().getDefinitions();
		List<DefinitionAxm> toRemove = new ArrayList<DefinitionAxm>();
		for (DefinitionAxm def : definitions) {
			if (def instanceof UserAxm) {
				// Avoid concurrent mod exception.
				toRemove.add(def);
			}
		}
		for (DefinitionAxm def : toRemove) {
			definitions.remove(def);
		}
		AxmDocument document = new AxmDocument.Builder().setRoot(axm).build();
		AxmDocumentUtil.findUserName(document);
	}

}
