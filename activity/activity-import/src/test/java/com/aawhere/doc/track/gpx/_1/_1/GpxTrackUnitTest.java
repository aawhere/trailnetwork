/**
 * 
 */
package com.aawhere.doc.track.gpx._1._1;

import static com.aawhere.gpx._1._1.GpxTestUtil.*;
import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxTestUtil.GpxTestDocument;
import com.aawhere.gpx._1._1.Wpt;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.test.category.UnitTest;
import com.aawhere.track.Trackpoint;

/**
 * Tests the {@link GpxTrack} and {@link GpxTrackpoint} classes for completeness.
 * 
 * @author roller
 * 
 */

@Category({ UnitTest.class, FileTest.class })
public class GpxTrackUnitTest {

	@Test
	@Category(com.aawhere.test.category.FileTest.class)
	public void testIterator() throws GpxReadException {
		GpxTestDocument testDocument = GARMIN_CONNECT_ACTIVITY;
		Gpx doc = GpxTestUtilFactory.getUtil().getTestGpx(testDocument);

		assertTracks(testDocument, doc);
	}

	/**
	 * @param testDocument
	 * @param doc
	 */
	public static void assertTracks(GpxTestDocument testDocument, Gpx doc) {
		GpxTrack track = new GpxTrack(doc.getTracks().get(0));
		int trackpointCount = 0;
		DateTime previousTimestamp = new DateTime(0);
		for (Trackpoint trackpoint : track) {
			assertNotNull(trackpoint);
			DateTime timestamp = trackpoint.getTimestamp();
			assertNotNull("timestamp is required", timestamp);
			assertTrue(previousTimestamp.isBefore(timestamp));
			trackpointCount++;
		}

		assertEquals(testDocument.numberOfTrackpoints, trackpointCount);
	}

	@Test
	public void testLocationNotCapable() {

		Wpt element = new Wpt() {

			private static final long serialVersionUID = 1L;

			@Override
			public Double getLongitude() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Double getLatitude() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Double getElevation() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public DateTime getDateTime() {
				// TODO Auto-generated method stub
				return null;
			}
		};

		GpxTrackpoint trackpoint = new GpxTrackpoint(element);
		assertFalse(trackpoint.isLocationCapable());

	}

	@Test
	public void testLocationCapable() {
		Wpt element = GpxTestUtilFactory.getUtil().generateRandomWpt();
		GpxTrackpoint trackpoint = new GpxTrackpoint(element);
		assertTrue(trackpoint.isLocationCapable());
		TestUtil.assertDoubleEquals(element.getLatitude(), trackpoint.getLocation().getLatitude().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(element.getLongitude(), trackpoint.getLocation().getLongitude()
				.getInDecimalDegrees());
	}

}
