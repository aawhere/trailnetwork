/**
 *
 */
package com.aawhere.doc.track.gpx._1._1;

import static org.junit.Assert.assertNotNull;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.gpx._1._1.GpxTestUtil;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.retrieve.SystemResourceDocumentRetriever;
import com.aawhere.retrieve.crawler.SystemResourceDocumentCrawler;
import com.aawhere.retrieve.crawler.TestDocumentCrawlerListener;
import com.aawhere.test.category.FileTest;

/**
 * A heavier test that loads files using the {@link SystemResourceDocumentCrawler} to find GPX files
 * for testing.
 * 
 * @author Aaron Roller
 * 
 */
@Category(FileTest.class)
public class GpxDocumentFileUnitTest {

	@BeforeClass
	static public void setUpFactory() {
		GpxDocumentUnitTest.setUpDocumentFactory();

	}

	@Test
	public void testHeart() {
		DocumentRetriever retriever = loadFile(GpxTestUtil.HEART_FILE.resourceName, DocumentFormatKey.GPX_1_1);
		GpxDocument document = (GpxDocument) retriever.getDocument();
		assertNotNull(document);
		GpxTrackUnitTest.assertTracks(GpxTestUtil.HEART_FILE, document.getGpx());
	}

	public DocumentRetriever loadFile(String filename, DocumentFormatKey formatKey) {
		TestDocumentCrawlerListener<SystemResourceDocumentRetriever> listener = new TestDocumentCrawlerListener<SystemResourceDocumentRetriever>();
		new SystemResourceDocumentCrawler.Builder().addResourcePath(filename).setExpectedFormat(formatKey)
				.registerListener(listener).build();

		return listener.successes.get(0);

	}
}
