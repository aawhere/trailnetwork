/**
 * 
 */
package com.aawhere.doc;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityId;
import com.aawhere.app.IdentityHistory;
import com.aawhere.app.Installation;
import com.aawhere.app.KnownApplication;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.xml.bind.JAXBReadException;
import com.aawhere.xml.bind.JAXBTestUtil;

import com.garmin.developer.schemas.axm.AxmAxm;
import com.garmin.developer.schemas.axm.AxmUnitTest;

/**
 * Test {@link AxmDocument}
 * 
 * @author Brian Chapman
 * 
 */
@Category(FileTest.class)
public class AxmDocumentFileTest {

	public static final String GC_ACTIVITY_TYPE_PREFIX = "activityType_";

	public static final String SUMMARY_ACTIVITY_TYPE = GC_ACTIVITY_TYPE_PREFIX + "casual_walking";

	public static final String GC_ACTIVITY_ID = "3006961";
	public static final String GC_UNTITLED_ACTIVITY_ID = GC_ACTIVITY_ID + "_Untitled";
	public static final int HEART_ACTIVITY_NUMBER_OF_INSTALLATIONS = 4;
	public static final String HEART_ACTIVITY_TYPE = GC_ACTIVITY_TYPE_PREFIX + "paddling";

	public static final String GC_ACTIVITY_ID_RUNNING = "94312872";
	public static final int RUNNING_NUMBER_OF_INSTALLATIONS = 8;
	public static final String RUNNING_ACTIVITY_TYPE = GC_ACTIVITY_TYPE_PREFIX + "trail_running";

	public static final ActivityId GC_APPLICATION_ACTIVITY_ID = new ActivityId(KnownApplication.GARMIN_CONNECT.key,
			GC_ACTIVITY_ID);

	public static AxmDocumentFileTest getInstance() {
		return new AxmDocumentFileTest();
	}

	public AxmDocument getHeartAxmDocument() {
		return getAxmDocument(gcAxmResourcePath(GC_ACTIVITY_ID));
	}

	public AxmDocument getRunningAxmDocument() {
		return getAxmDocument(gcAxmResourcePath(GC_ACTIVITY_ID_RUNNING));
	}

	private String gcAxmResourcePath(String id) {
		return "gc/activity_" + id + ".axm";
	}

	public AxmDocument getAxmDocument(String path) {
		AxmAxm axm;
		try {
			axm = AxmUnitTest.axmFrom(path);
		} catch (JAXBReadException e) {
			throw new RuntimeException(e);
		}
		AxmDocument document = new AxmDocument.Builder().setRoot(axm).build();
		return document;
	}

	@Test
	public void testAxmActivitySummary() throws Exception {
		AxmDocument document = getAxmDocument(AxmUnitTest.SUMMARY_FILENAME);
		Message name = new StringMessage(document.getActivityName());
		TestUtil.assertContains(name.getValue(), AxmUnitTest.ACTIVITY_NAME);
		TestUtil.assertContains(document.getDescription(), AxmUnitTest.ACTIVITY_DESCRIPTION);
		assertEquals(SUMMARY_ACTIVITY_TYPE, document.getActivityType());
		assertDocument(document);
	}

	@Test
	public void testUnknownName() {
		AxmDocument axmDocument = getAxmDocument(gcAxmResourcePath(GC_UNTITLED_ACTIVITY_ID));
		assertNull("Untitled names should be null", axmDocument.getActivityName());
	}

	/**
	 * This is an activity that was imported to Garmin Connect from Motionbased.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testHeartAxm() throws Exception {
		AxmDocument document = getHeartAxmDocument();
		assertNotNull(document);
		assertDocument(document);
		IdentityHistory history = document.getHistory();
		assertEquals(HEART_ACTIVITY_NUMBER_OF_INSTALLATIONS, history.getInstallations().size());
		assertEquals(HEART_ACTIVITY_TYPE, document.getActivityType());
	}

	/**
	 * This is an activity that was imported to Garmin Connect from Motionbased.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testRunningAxm() throws Exception {
		AxmDocument document = getRunningAxmDocument();
		assertNotNull(document);
		assertDocument(document);
		IdentityHistory history = document.getHistory();
		assertEquals(RUNNING_NUMBER_OF_INSTALLATIONS, history.getInstallations().size());
		assertEquals(RUNNING_ACTIVITY_TYPE, document.getActivityType());
	}

	@Test
	public void testAxmDocumentHistory() throws JAXBReadException {
		AxmDocument document = getAxmDocument(AxmUnitTest.SUMMARY_FILENAME);
		IdentityHistory history = document.getHistory();
		List<Installation> installations = history.getInstallations();
		final int expectedNumberOfInstallations = 6;
		assertEquals(expectedNumberOfInstallations, installations.size());

		JAXBTestUtil.create(history).build();
		assertDocument(document);
	}

	public static void assertDocument(AxmDocument document) {
		assertNotNull(document);
		assertNotNull("history", document.getHistory());
		assertNotNull("activity name", document.getActivityName());
		assertNotNull("activity type", document.getActivityType());
		assertNotNull("activity id", document.getActivityId());
		assertNotNull("user id", document.getUserId());
		assertNotNull("document format key", document.getDocumentFormat());
	}
}
