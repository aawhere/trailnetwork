/**
 * 
 */
package com.aawhere.doc.garmin.activity;

import java.net.URI;

import org.junit.Test;

import com.aawhere.doc.garmin.activity.GarminConnectActivitiesFilter.ActivityType;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.test.TestUtil;

/**
 * Tests {@link GarminConnectActivitiesFilterUnitTest}.
 * 
 * @author aroller
 * 
 */
public class GarminConnectActivitiesFilterUnitTest {
	final static String path = "http://example.com";
	final static String owner = "blah";
	final static Integer pageNumber = 13;
	final static BoundingBox boundingBox = BoundingBoxTestUtils.createRandomBoundingBox();
	final static ActivityType expectedActivityType = GarminConnectActivitiesFilter.ActivityType.CYCLING;

	@Test
	public void testUri() {

		GarminConnectActivitiesFilter filter = createFilter();

		String uri = filter.appendFilterParmaters(path);
		TestUtil.assertContains(uri, owner);
		TestUtil.assertContains(uri, path);
		TestUtil.assertContains(uri, pageNumber.toString());
		TestUtil.assertContains(uri, "&");
		TestUtil.assertContains(uri, GarminConnectActivitiesFilter.BB_MAX_LAT);
		TestUtil.assertContains(uri, GarminConnectActivitiesFilter.BB_MAX_LON);
		TestUtil.assertContains(uri, GarminConnectActivitiesFilter.BB_MIN_LAT);
		TestUtil.assertContains(uri, GarminConnectActivitiesFilter.BB_MIN_LON);
		TestUtil.assertContains(uri, GarminConnectActivitiesFilter.ACTIVITY_TYPE);
		TestUtil.assertContains(uri, GarminConnectActivitiesFilter.IGNORE_UNTITLED);

		TestUtil.assertContains(uri, GarminConnectActivitiesFilter.formatActivityType(expectedActivityType));

	}

	@Test
	public void testAppendTnFilterParamaters() {
		GarminConnectActivitiesFilter filter = createFilter();

		URI uriUri = filter.appendTnFilterParamaters(path);
		String uri = uriUri.toString();
		TestUtil.assertContains(uri, owner);
		TestUtil.assertContains(uri, path);
		TestUtil.assertContains(uri, pageNumber.toString());
		TestUtil.assertContains(uri, "&");
		TestUtil.assertContains(uri, GarminConnectActivitiesFilter.QUERY_PARAM_BOUNDS);
		TestUtil.assertContains(uri, expectedActivityType.name());
		TestUtil.assertContains(uri, GarminConnectActivitiesFilter.QUERY_PARAM_IGNORE_UNTITLED);
	}

	@Test
	public void testDefaultUri() {
		GarminConnectActivitiesFilter filter = new GarminConnectActivitiesFilter.Builder().build();
		String uri = filter.appendFilterParmaters(path);
		TestUtil.assertContains(uri, path);
	}

	private GarminConnectActivitiesFilter createFilter() {
		GarminConnectActivitiesFilter filter = new GarminConnectActivitiesFilter.Builder().setOwner(owner)
				.setPageNumber(pageNumber).setBoundingBox(boundingBox).setActivityType(expectedActivityType).build();
		return filter;
	}

}
