/**
 * 
 */
package com.aawhere.doc.garmin.activity;

import static org.junit.Assert.*;

import java.io.IOException;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.aawhere.doc.garmin.activity.json.ActivitySearch;
import com.aawhere.doc.garmin.activity.json.Results;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.util.rb.MessageResourceBundleUnitTest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterables;

/**
 * Test for {@link GarminConnectActivitySearchResults}.
 * 
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class GarminConnectActivitySearchFileTest {
	static final String TWO_PAGE = "gc/search/SearchResultsPage1.json";
	static final String AROLLER_NO_BBOX = "gc/search/SearchResultsArollerNoBoundingBox.json";
	static final String LAST_ACTIVITY_ID_IN_AROLLER_NO_BBOX = "232684991";
	static final String LAST_ACTIVITY_ID_TWO_PAGE = "8434924";
	final Integer expectedOnFirstPage = 20;
	final Integer expectedOnSecondPage = 4;

	@Test
	public void testTwoPagesAvailable() throws IOException {
		Integer expectedPage = 1;
		Integer expectedPerPage = 20;
		Integer expectedIndex = 0;
		Boolean expectingAnotherPage = true;
		assertResults(	expectedPage,
						expectedPerPage,
						expectedOnFirstPage,
						expectedIndex,
						expectingAnotherPage,
						TWO_PAGE,
						"2012-07-01");
		assertResults(	2,
						expectedPerPage,
						expectedOnSecondPage,
						expectedOnFirstPage,
						true,
						TWO_PAGE.replace('1', '2'),
						"2007-01-20");
		assertResults(3, expectedPerPage, 0, expectedOnFirstPage, false, TWO_PAGE.replace('1', '3'),
		// no results, no most recent timestamp
						null);

	}

	@Test
	public void testNextFilterWithBbox() throws JsonParseException, JsonMappingException, IOException {
		GarminConnectActivitySearchResults search = loadResults(TWO_PAGE);
		assertNextFilter(search);
	}

	/**
	 * @param search
	 */
	public void assertNextFilter(GarminConnectActivitySearchResults search) {
		GarminConnectActivitiesFilter filter = search.getFilterForNextPage();
		assertNotNull(filter);
		filter.appendFilterParmaters("http://bogus.com");
	}

	@Test
	public void testNetFilterWithoutBbox() throws JsonParseException, JsonMappingException, IOException {
		GarminConnectActivitySearchResults search = loadResults(AROLLER_NO_BBOX);
		assertNextFilter(search);
	}

	@Test
	public void testValidMessages() {
		MessageResourceBundleUnitTest.validateManagedResourceBundle(GarminConnectApiMessage.class);
	}

	/**
	 * @param expectedPage
	 * @param expectedPerPage
	 * @param expectedOnCurrentPage
	 * @param expectedIndex
	 * @param expectingAnotherPage
	 * @param filename
	 * @param mostRecentStartTimeString
	 * @throws IOException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 */
	public static void assertResults(Integer expectedPage, Integer expectedPerPage, Integer expectedOnCurrentPage,
			Integer expectedIndex, Boolean expectingAnotherPage, String filename, String mostRecentStartTimeString)
			throws IOException, JsonParseException, JsonMappingException {
		GarminConnectActivitySearchResults search = loadResults(filename);
		assertResults(	expectedPage,
						expectedPerPage,
						expectedOnCurrentPage,
						expectedIndex,
						expectingAnotherPage,
						search,
						mostRecentStartTimeString);
	}

	/**
	 * @param filename
	 * @return
	 * @throws IOException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 */
	public static GarminConnectActivitySearchResults loadResults(String filename) throws IOException,
			JsonParseException, JsonMappingException {
		Resource page = new ClassPathResource(filename);
		ObjectMapper mapper = new ObjectMapper();
		Results searchResults = mapper.readValue(page.getInputStream(), ActivitySearch.class).getResults();
		GarminConnectActivitySearchResults search = new GarminConnectActivitySearchResults.Builder()
				.setResults(searchResults).build();
		return search;
	}

	/**
	 * @param expectedPage
	 * @param expectedPerPage
	 * @param expectedOnCurrentPage
	 * @param expectedIndex
	 * @param expectingAnotherPage
	 * @param searchResults
	 */
	public static void assertResults(Integer expectedPage, Integer expectedPerPage, Integer expectedOnCurrentPage,
			Integer expectedIndex, Boolean expectingAnotherPage, GarminConnectActivitySearchResults search,
			String mostRecentStartTimeString) {

		assertEquals(expectedPage, search.getCurrentPage());
		assertEquals(expectedOnCurrentPage, search.getNumberOfActivitiesOnCurrentPage());
		assertEquals(expectedIndex, search.getIndexForFirstOnThisPage());
		assertEquals(expectedPerPage, search.getNumberOfActivitiesPerPage());
		assertEquals(expectingAnotherPage, search.anotherPageMayBeAvailable());
		if (mostRecentStartTimeString != null) {
			assertEquals("start date", LocalDate.parse(mostRecentStartTimeString), search.mostRecentStartDate());
		} else {
			assertNull("date is provided, but none should be found", search.mostRecentStartDate());
		}

	}

	@Test
	public void testSearchResultsIdIterator() throws JsonParseException, JsonMappingException, IOException {
		GarminConnectActivitySearchResults search = loadResults(AROLLER_NO_BBOX);
		assertEquals(5, Iterables.size(search.activityIds()));
		TestUtil.assertContains(Iterables.toString(search.activityIds()), LAST_ACTIVITY_ID_IN_AROLLER_NO_BBOX);
	}

}
