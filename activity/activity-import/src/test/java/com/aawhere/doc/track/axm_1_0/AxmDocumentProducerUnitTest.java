/**
 *
 */
package com.aawhere.doc.track.axm_1_0;

import static org.junit.Assert.assertNotNull;

import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentProducer;

/**
 * @author brian
 * 
 */
public class AxmDocumentProducerUnitTest {

	private static final String AXM_RESOURCE_PATH = "activity_3006961.axm";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testProduce() throws DocumentDecodingException, DocumentContentsNotRecognizedException {
		String resourcePath = AXM_RESOURCE_PATH;
		DocumentProducer producer = AxmDocumentProducer.create();
		InputStream is = ClassLoader.getSystemResourceAsStream(resourcePath);
		Document axm = producer.produce(is);
		assertNotNull(axm);
	}
}
