/**
 * 
 */
package com.aawhere.activity.timing;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityFilterBuilder;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportService;
import com.aawhere.activity.ActivityService;
import com.aawhere.activity.ActivityTrackGeometryService;
import com.aawhere.activity.ApplicationActivityImportInstructions;
import com.aawhere.activity.ApplicationImportInstructionsNotFoundException;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.AttemptLineProvider;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.ScreeningResults;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.VerifiedTimingResults;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.cache.Cache;
import com.aawhere.cache.Cache.Key;
import com.aawhere.field.FieldDictionary;
import com.aawhere.id.SameIdentifierException;
import com.aawhere.io.EndpointNotFoundException;
import com.aawhere.io.IoException;
import com.aawhere.io.IoMessage;
import com.aawhere.lang.ComparatorResult;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsExploder;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.FilterRepository;
import com.aawhere.persist.RepositoryUtil;
import com.aawhere.queue.BatchQueue;
import com.aawhere.queue.BatchQueueSendFunction;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueDependencyException;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.QueueUtil;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Method;
import com.aawhere.queue.WebApiEndPoint;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.route.RouteWebApiUri;
import com.aawhere.track.Track;
import com.aawhere.track.TrackService;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.aawhere.track.story.TrackUserStories;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageStatus;
import com.aawhere.util.rb.StatusMessages;
import com.aawhere.ws.rs.ApplicationNotifier;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Standard service for {@link ActivityTiming}.
 * 
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityTimingService {

	/**
	 * 
	 */
	public static final String SECOND_PASS_TIMING_QUEUE_NAME = WebApiQueueUtil.CRAWL_QUEUE_NAME;
	private static final Level logLevel = Level.FINE;
	private static Level summaryLevel = Level.INFO;

	private static final Logger logger = LoggerFactory.getLogger(ActivityTimingService.class);
	/**
	 * 
	 */
	private static final int PAGE_SIZE_FOR_CANDIDATE_PROCESSING = 100;

	private final ActivityTimingRelationObjectifyRepository timingRelationRepository;
	private final ActivityService activityService;
	private final QueueFactory queueFactory;
	private final ActivityImportService importService;
	private final ActivityTrackGeometryService trackGeometryService;

	/**
	 * 
	 */
	@Inject
	public ActivityTimingService(@Nonnull ActivityService activityService, ActivityImportService importService,
			ActivityTrackGeometryService trackGeometryService,
			@Nonnull ActivityTimingRelationObjectifyRepository repository, @Nonnull QueueFactory queueFactory) {
		this.activityService = activityService;
		this.timingRelationRepository = repository;
		this.trackGeometryService = trackGeometryService;
		this.queueFactory = queueFactory;
		this.importService = importService;
	}

	/**
	 * Manages the storage for {@link ActivityTiming} either by creating or updating with the given
	 * relation. Only updates if the loaded version does not return true from
	 * {@link ActivityTimingRelationUtil#equalsDeeply(ActivityTiming, ActivityTiming)}
	 * 
	 * @param given
	 * @return
	 */
	public ActivityTiming createOrUpdate(final ActivityTiming given) {
		ActivityTiming stored;

		try {
			ActivityTiming loaded = timingRelationRepository.load(given.getId());
			// failures aren't persisted.
			if (!ActivityTimingRelationUtil.equalsDeeplyIgnoreFailures(given, loaded)) {
				stored = timingRelationRepository.store(ActivityTiming.mutate(loaded).setOther(given).build());
			} else { // ignore it since not enough has changed
				stored = loaded;
			}
		} catch (EntityNotFoundException e) {
			stored = timingRelationRepository.store(given);
		}

		return stored;
	}

	/**
	 * Timings are reciprocal if and only if both complete...which means both should be available
	 * for query. If so then true is assigned.
	 * 
	 * If a timing has been removed and the other previously knew it to be reciprocal it is updated
	 * to reflect non-reciprocal.
	 * 
	 * If a reciprocal timing is not found and there has been no previous knowledge reciprocal
	 * remains null until such a time it can be confirmed that the relations are not reciprocal.
	 * 
	 * @param id
	 * @param deleted
	 *            indicates that the given has been deleted so update the other if found
	 * @return the given as original or modified if reciprocity updated.
	 * @throws EntityNotFoundException
	 */
	ActivityTiming reciprocalUpdated(ActivityTiming given, ActivityTimingRelationCandidateScreener screener)
			throws EntityNotFoundException {

		ActivityTimingId id = screener.candidateId();
		// if it already knows it's a reciprocal then ignore
		if (given.reciprocal() == null) {
			ActivityTiming reciprocal;
			try {
				// found it so let's check it
				reciprocal = reciprocal(id);
			} catch (EntityNotFoundException e1) {
				// didn't find it so let's see if it's a good candidate
				// since we already have it loaded
				ActivityTimingRelationCandidateScreener reciprocalScreener = ActivityTimingRelationCandidateScreener
						.create().attempt(screener.course()).course(screener.attempt())
						.attemptLineProvider(attemptLineProvider()).build();
				if (reciprocalScreener.passed()) {
					// reciprocal not yet found so let's encourage it - TN-511
					// calling processTimings directly is recursive and asynchronous persistence may
					// cause a race
					// this goes in the storng queue since it's potential reciprocal repeated it and
					// it passed screening
					Task task = ActivityTimingWebApiUri
							.taskForCourseAttempt(	ActivityTimingRelationUtil.reciprocalId(id),
													QueueUtil.REPEAT_TASKS_SUPPRESSED).build();
					// if the given is completed add to the strong queue, otherwise the weak
					if (given.courseCompleted()) {
						getTimingCourseAttemptQueue().add(task);
					} else {
						getTimingCourseAttemptScreeningQueue().add(task);
					}
					reciprocal = null;
				} else {
					// reciprocal screener failed so let's mark it ...no need to process again
					reciprocal = createOrUpdate(reciprocalScreener.timingFailed());
				}
			}

			// the reciprocal could be in several states at this point
			// 1. Found and that's all we know
			// 2. Not Found, screened and passed...waiting to be processed. It will be back
			// 3. Not Found, screened and failed. We have it so process it
			if (reciprocal != null) {
				Boolean theseTwoAreReciprocals = reciprocal.courseCompleted() && given.courseCompleted();
				if (given.isCourseCompleted()) {
					if (given.reciprocal() == null || given.reciprocal() != theseTwoAreReciprocals) {
						// the given is still being processed so let's just update it and the
						// processing
						// will do the rest
						given = createOrUpdate(ActivityTiming.mutate(given).reciprocal(theseTwoAreReciprocals).build());
					}
				}
				// if it is a reciprocal and doesn't yet know about it...then let's record it
				if (reciprocal.courseCompleted() && !theseTwoAreReciprocals.equals(reciprocal.reciprocal())) {
					reciprocal = createOrUpdate(ActivityTiming.mutate(reciprocal).reciprocal(theseTwoAreReciprocals)
							.build());
					// notify interested parties that the timing has been further defined
					queueToNotifyApplicationsCourseCompleted(reciprocal);
				}
				// else it's not completed then it's not a reciprocal...it's redundant
			}
		}
		return given;
	}

	/**
	 * Provides the reciprocal {@link ActivityTiming} if it exists. It's non-existense does not
	 * indicate it is not possible, just not known to the system. This will not process.
	 * 
	 * @param timingId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ActivityTiming reciprocal(ActivityTimingId timingId) throws EntityNotFoundException {
		ActivityTimingId reciprocalId = ActivityTimingRelationUtil.reciprocalId(timingId);
		return getTiming(reciprocalId);
	}

	/**
	 * Simply returns a persisted timing relation or indicates it isn't available by throwing the
	 * {@link EntityNotFoundException}.
	 * 
	 * This does not satisfy idempotent situations because a timing that will be courseCompleted
	 * will return not found now, but may return the result later after it has been processed. For
	 * this reason it is unlikely this method would be exposed to web services and should only be
	 * used internally.
	 * 
	 * @see #getOrProcessTiming(ActivityTimingId)
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ActivityTiming getTiming(ActivityTimingId id) throws EntityNotFoundException {
		return timingRelationRepository.load(id);
	}

	/**
	 * An alias for {@link #getTiming(ActivityTimingId)} helping to build the id.
	 * 
	 * @param courseId
	 * @param attemptId
	 * @return
	 * @throws EntityNotFoundException
	 */
	ActivityTiming getTiming(ActivityId courseId, ActivityId attemptId) throws EntityNotFoundException {
		return getTiming(new ActivityTimingId(courseId, attemptId));
	}

	/**
	 * This retrieves the timing from persistence if available (cache or database). If no entity is
	 * available in the cache then it will be processed using
	 * {@link #getTimingRelation(ActivityId, ActivityId)} to process and update the cache if
	 * necessary.
	 * 
	 * This method intends to satisfy idempotent requirements.
	 * 
	 * @see #getTimingRelation(ActivityId, ActivityId)
	 * 
	 * @param id
	 * @throws EntityNotFoundException
	 */
	public ActivityTiming getOrProcessTiming(ActivityTimingId id) throws EntityNotFoundException {
		return getOrProcessTiming(id, false);
	}

	/**
	 * Same as {@link #getOrProcessTiming(ActivityTimingId)}, but provides the ability to indicate
	 * if screening should be done instead of the deep processing (if processing is required). If
	 * screening determines no failures then it replies with a {@link QueueDependencyException}
	 * since it will be placed in the queue for full processing. TN-338
	 * 
	 * @see #getOrProcessTiming(ActivityTimingId)
	 * @param id
	 * @param ifProcessingNeededScreenOnly
	 *            indicates this will be a screening if processing is determined necessary
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ActivityTiming getOrProcessTiming(ActivityTimingId id, Boolean ifProcessingNeededScreenOnly)
			throws EntityNotFoundException {
		return getOrProcessTiming(id, ifProcessingNeededScreenOnly, Boolean.FALSE);

	}

	/**
	 * Same as {@link #getOrProcessTiming(ActivityTimingId, ifProcessingNeededScreenOnly)}, but
	 * provides the ability to force the reprocessing of the timing. TN-398
	 * 
	 * @see #getOrProcessTiming(ActivityTimingId, ifProcessingNeededScreenOnly)
	 * @param id
	 * @param ifProcessingNeededScreenOnly
	 *            indicates this will be a screening if processing is determined necessary
	 * @param forceStale
	 *            indicates that this relation should be reprocessed.
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ActivityTiming getOrProcessTiming(ActivityTimingId id, Boolean ifProcessingNeededScreenOnly,
			Boolean forceStale) throws EntityNotFoundException {
		return processTiming(id, ifProcessingNeededScreenOnly, forceStale);
	}

	/**
	 * Returns the count of the number of completed attempts for the given course excluding
	 * subsequent completions beyond the first completion of a timing. In other words it doesn't
	 * count the second loop of a two loop attempt of a one loop course.
	 * 
	 * @see FilterRepository#count(Filter)
	 * @see ActivityTimingFilterBuilder#courseCompleted(ActivityId)
	 * @param courseId
	 * @return
	 */
	public Integer attemptsCount(ActivityId courseId) {
		return this.timingRelationRepository.count(ActivityTimingFilterBuilder.create().courseCompleted(courseId)
				.build());
	}

	/**
	 * Provides the number of courses that an activity has completed. This is not the number of
	 * completions,but the number of activities so second loops don't count.
	 * 
	 * @param attemptId
	 * @return
	 */
	public Integer coursesCount(ActivityId attemptId) {
		return this.timingRelationRepository.count(ActivityTimingFilterBuilder.create().attempt(attemptId).build());
	}

	/**
	 * @see #attemptsCount(ActivityId)
	 * 
	 * @return
	 */
	public Function<ActivityId, Integer> attemptsCountFunction() {
		return new Function<ActivityId, Integer>() {

			@Override
			public Integer apply(ActivityId input) {
				return (input == null) ? null : attemptsCount(input);
			}
		};
	}

	/**
	 * @see #coursesCount(ActivityId)
	 * @return
	 */
	public Function<ActivityId, Integer> coursesCountFunction() {
		return new Function<ActivityId, Integer>() {

			@Override
			public Integer apply(ActivityId input) {
				return (input == null) ? null : coursesCount(input);
			}
		};
	}

	/**
	 * An alias for {@link #getOrProcessTiming(ActivityTimingId)} taking advantage of caching if
	 * available.
	 * 
	 * @param courseId
	 * @param attemptId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ActivityTiming getOrProcessTiming(ActivityId courseId, ActivityId attemptId) throws EntityNotFoundException {
		return getOrProcessTiming(new ActivityTimingId(courseId, attemptId));
	}

	public ActivityTiming getOrProcessTiming(ActivityId courseId, ActivityId attemptId,
			Boolean ifProcessingNeededScreenOnly) throws EntityNotFoundException {
		return getOrProcessTiming(new ActivityTimingId(courseId, attemptId), ifProcessingNeededScreenOnly);

	}

	ActivityTiming processTiming(ActivityTimingId timingId, Boolean screeningOnly, Boolean forceStale)
			throws EntityNotFoundException {
		return processTiming(timingId, screeningOnly, forceStale, null);
	}

	/**
	 * Given a course that is 100% contained within an attempt this will find the appropriate timing
	 * results to describe ranking for that course. This will return the persisted relation if
	 * already processed or will process and persist if not.
	 * 
	 * This method will always return a result regardless of a successful match
	 * {@link ActivityTiming#courseCompleted()} or if it's been stored.
	 * 
	 * @param timingId
	 *            the relationship to be processed. Also used for caching to avoid duplicate
	 *            processing.
	 * @param force
	 *            indicates whether to force processing even if already stored.
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Cache
	ActivityTiming processTiming(@Key ActivityTimingId timingId, @Deprecated Boolean screeningOnly, Boolean forceStale,
			@Nullable ActivityTimingRelationUserStory story) throws EntityNotFoundException {
		Pair<ActivityId, ActivityId> pair = ActivityTimingRelationUtil.split(timingId);
		ActivityId courseId = timingId.getCourseId();
		ActivityId attemptId = pair.getRight();
		SameIdentifierException.throwIfEquals(courseId, attemptId);
		ActivityTiming foundRelation;
		ActivityTiming result;

		// first attempt to find in datastore so we can return or update as needed
		try {
			foundRelation = getTiming(timingId);
		} catch (EntityNotFoundException e) {
			foundRelation = null;
		}
		// process again if something has changed
		Boolean stale = isStale(courseId, attemptId, foundRelation);
		boolean previouslyCompleted = foundRelation != null && foundRelation.isCourseCompleted();
		// we must process the results if not stored or if what is stored is stale
		if (stale || foundRelation == null || forceStale == Boolean.TRUE) {

			if (story == null) {
				ActivityTimingRelationUserStory.Builder timingUserStoryBuilder = new ActivityTimingRelationUserStory.Builder();
				story = configureTimingsUserStory(courseId, attemptId, timingUserStoryBuilder);
			}
			if (story.hasResult()) {

				result = story.getResult();
				// displaying the result of the story is helpful in the logs at a level above the
				// default
				Level storyResultLogLevel = Level.INFO;
				result = createOrUpdate(story.getResult());

				result = reciprocalUpdated(result, story.screener());
				// what to do next depends if the course was completed or not
				if (result.isCourseCompleted()) {
					// if it is new or if the ranking could have changed

					if (foundRelation == null
							|| !ActivityTimingRelationUtil.equalsDeeplyIgnoreFailures(foundRelation, result)) {
						queueToNotifyApplicationsCourseCompleted(result);
						if (logger.isLoggable(logLevel)) {
							if (foundRelation == null) {
								logger.log(logLevel, story.about() + " notifying course completed (new timing)");
							} else {
								logger.log(logLevel, story.about()
										+ " notifying course completed (origional and update differ)");
							}
						}
					}
					if (logger.isLoggable(storyResultLogLevel)) {
						logger.log(storyResultLogLevel, result.getId().toString() + " completed with "
								+ result.getCompletions().size());
					}
				} else {

					// it failed, but failures aren't stored so let's add them back for explanation
					result = ActivityTiming.mutate(result).setFailures(story.getResult().failures()).build();
					if (logger.isLoggable(storyResultLogLevel)) {
						String message = result.getId() + " " + result.getFailures();
						// a failure happened, who caught it. screening is always better
						if (story.screenSucceeded()) {
							message += " by processor";
						} else {
							message += " by screener";
							if (screeningOnly) {
								message += "(only)";
							}
						}
						logger.log(storyResultLogLevel, message);
					}
					// notify if a previous contender has now been removed.
					if (previouslyCompleted) {
						// keep the database clean with only completions
						// tell those interested that the leaderboard should remove this
						logger.log(logLevel, result.getId().toString()
								+ " notifying course completed (didn't complete, deleted)");
						queueToNotifyApplicationsCourseCompleted(result);
					}
				}

			} else {
				throw new UnsupportedOperationException("This shouldn't happen.  Why is result not return from story?");
			}

		} else {
			if (logger.isLoggable(logLevel)) {
				logger.log(logLevel, foundRelation.id() + " already persisted with no force");
			}
			// not forced...return what we found.
			result = foundRelation;
		}
		return result;
	}

	/**
	 * For each {@link ActivityId} given this will post a task that will look for all activities
	 * that may potentially complete the given activity resulting in a timing completion.
	 * 
	 * @param courseId
	 * @return the number of activities found.
	 */
	public Integer queueAttempCandidates(Iterable<ActivityId> courseId) {
		BatchQueue queue = BatchQueue.create().setQueue(getTimingsQueue()).build();
		for (ActivityId activityId : courseId) {
			queue.add(ActivityTimingWebApiUri.timingAttemptsDiscovery(activityId));
		}
		return queue.finish();
	}

	/**
	 * Given an activityId, this will inspect it as a course and queue course/attempt possibilities
	 * up for processing.
	 * 
	 * @param courseId
	 * @throws EntityNotFoundException
	 */
	public Integer queueAttemptCandidates(ActivityId courseId) throws EntityNotFoundException {
		int numberOfCandidates = 0;
		Activity course = activityService.getActivity(courseId);
		if (!course.isGone()) {
			// first, if there are any existing timings we must always check those to see if they
			// still
			// qualify.
			// they may not be found in the filters applied to the selection below.
			// TN-398 reprocess timings would want to find these.
			// numberOfCandidates += queueTimings(paginatorForTimingsAsCourse(activityId));
			// numberOfCandidates += queueTimings(paginatorForTimingsAsAttempt(activityId));

			if (ActivityTimingRelationUserStory.isLongEnough(course.getTrackSummary().getDistance())) {
				// do the smart search first.

				// TN-446 is a smart crawler of leaderboards, but it's finding too many

				// TN-442 queuing these is slow, process them now to reduce latency
				numberOfCandidates += attemptsScreened(course);
				// TN-484 intersection queries produce too many negatives
				// numberOfCandidates += coursesCompleted(activity);

			}
		}
		return numberOfCandidates;
	}

	/**
	 * 
	 * Given an area, this will query that area for all activities that intersect that area, then
	 * find all other activities that may be a candidate that hasn't already been screened or
	 * attempted as a timingRelation. To do so it will query against the timing table to see if any
	 * timing yet exists, positive or negative. Any worthy attempts not already attempted will be
	 * queued for a proper attempt.
	 * 
	 * This method does no date checking since it assumes the caller knows potentially new attempts
	 * have arrived so another pass must be made.
	 * 
	 * This multi-phased approach is being used since the intersection queries produce too many
	 * results that result in no matches. It is thought this approach will still be thourough enough
	 * and more efficient since the number of items queued will be reduced. TN-484
	 * 
	 * If resolution is too low, this will explode the resolution to children resolutions and queue
	 * those to be processed. This is to force proper resolution and avoid really long queue lines
	 * as a result of too many found while process a big cell which may take a lot of time!
	 * 
	 * 
	 * FIXME: one problem is the "contains" geocell comparison has no tolerance for missed cells at
	 * the database level resulting in reciprocal matches that won't be related because one cell is
	 * different due to edge cases. The sloppy intersection query found those before.
	 * 
	 * FIXME:this only finds activities with the same resolution as the cells given. For this reason
	 * all cells given will be exploded to Resolution matching those in the track summary. Once
	 * fixed, the explosions can go beyond the track summary resolution.
	 * 
	 * FIXME: This approach produces too many timing candidates. Instead consider crawling the
	 * timing relations that have already been matched using the leaderboards to identify good
	 * potentials. Then publish all participants in a leaderboard to be compared against each other.
	 * 
	 * @param area
	 * @return the integer representing the number of candidates queued OR the number of geocells
	 *         queued to call back to this
	 */
	public Integer queueMissedTimingCandidates(GeoCells area) {
		Integer result;
		// keep it small to avoid too large of findings and long processing
		Resolution minResolution = TrackSummary.MAX_BOUNDS_RESOLUTION;
		if (ComparatorResult.isLessThan(area.getMaxResolution().compareTo(minResolution))) {
			GeoCells exploded = GeoCellsExploder.explode().these(area).to(minResolution).build().exploded();
			// using the crawl queue as the processing destination will help throttle and avoid
			// duplicate candidates queued
			Queue secondPassTimingQueue = getCrawlQueue();
			BatchQueue queue = BatchQueue.create().setQueue(secondPassTimingQueue).build();
			for (GeoCell geoCell : exploded) {
				try {
					WebApiEndPoint endPoint = WebApiEndPoint.createWeb()
							.setEndPoint(ActivityTimingWebApiUri.TIMINGS_FOR_ACTIVITIES_IN_CELLS_ENDPOINT)
							.values(geoCell.getCellAsString()).build();
					queue.add(Task.create().setEndPoint(endPoint).setMethod(Method.PUT).build());
				} catch (URISyntaxException e) {
					throw new RuntimeException(e);
				}
			}
			result = queue.finish();
		} else {
			// TN-445 ensures the course candidates are at least the minimum length
			Filter filter = activityService.getActivityFilterBuilder()
					.distance(FilterOperator.GREATER_THAN, ActivityTimingRelationUserStory.MIN_LENGTH_FOR_COURSE)
					.spatialBuffer().intersectionAtResolution(area).build();
			Iterable<Activity> activityPaginator = activityService.activityPaginator(filter);

			BatchQueue queue = BatchQueue.create().setQueue(getTimingCourseAttemptQueue()).build();
			BatchQueueSendFunction queueSendFunction = BatchQueueSendFunction.build(queue);
			ActivityTimingReleationCandidateTaskFunction taskFunction = ActivityTimingReleationCandidateTaskFunction
					.build();
			for (Activity course : activityPaginator) {

				// activities that make good attempts
				Iterable<ActivityId> attemptIdPaginator = activityService
						.idPaginator(attemptCandidatesForCourseFilter(course));
				// timings of the course + attempt
				ActivityTimingRelationIdFromAttemptIdFunction timingIdFunction = ActivityTimingRelationIdFromAttemptIdFunction
						.build(course.id());
				Iterable<ActivityTimingId> timingCandidates = Iterables.transform(attemptIdPaginator, timingIdFunction);
				// avoid nulls which are created when course=attempt
				timingCandidates = Iterables.filter(timingCandidates, Predicates.notNull());
				// find those not yet inspected
				Iterable<ActivityTimingId> nonExistingTimingCandidates = RepositoryUtil
						.nonExistingEntityIds(timingCandidates, this.timingRelationRepository);
				// queue them up
				Iterable<Task> tasks = Iterables.transform(nonExistingTimingCandidates, taskFunction);
				Iterable<Task> sentTasks = Iterables.transform(tasks, queueSendFunction);
				// size flushes out the iterables since they are lazy invoked
				Iterables.size(sentTasks);

			}
			result = queue.finish();
		}
		return result;
	}

	/**
	 * A place to put items that will crawl existing timings looking for more timings.
	 * 
	 * @return
	 */
	public Queue getCrawlQueue() {
		return queueFactory.getQueue(SECOND_PASS_TIMING_QUEUE_NAME);
	}

	public Queue getTimingsQueue() {
		return queueFactory.getQueue(ActivityTimingWebApiUri.TIMINGS_QUEUE_NAME);
	}

	/**
	 * Given an activity this will find strong candidates (those attempts that contain this course
	 * completely) and process each with screening and subsequently deep processing if screening
	 * passes.
	 * 
	 * @param course
	 * @return
	 * @throws EntityNotFoundException
	 */
	Integer attemptsScreened(Activity course) throws EntityNotFoundException {
		Iterable<Activity> attempts = attemptCandidatesForCourse(course);
		int failed = 0;
		BatchQueue queue = BatchQueue.create().setQueue(getTimingCourseAttemptScreeningQueue()).build();
		// provides an efficient way of providing the line only if needed
		AttemptLineProvider attemptLineProvider = attemptLineProvider();
		for (Activity attempt : attempts) {
			if (attempt != null) {
				ActivityId attemptId = attempt.id();
				if (!course.getId().equals(attemptId)) {
					ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
							.course(course).attempt(attempt).attemptLineProvider(attemptLineProvider).build();

					if (screener.passed()) {
						if (logger.isLoggable(logLevel)) {
							logger.log(logLevel, screener.candidateId() + " passed screening");
						}
						queue.add(ActivityTimingWebApiUri.taskForCourseAttempt(	screener.candidateId(),
																				QueueUtil.REPEAT_TASKS_SUPPRESSED)
								.build());
					} else {
						failed++;
						// TN-484 storing failures to avoid double processing
						createOrUpdate(screener.timingFailed());
						if (logger.isLoggable(logLevel)) {
							logger.log(logLevel, new ActivityTimingId(course.getId(), attemptId)
									+ " failed strong screening with " + screener.reason());
						}
					}
				}
			} else {
				logger.warning(course + " had a null attempt");
			}
		}
		int passed = queue.finish();
		// TN-484 specifically logging at higher level to understand quantities
		if (logger.isLoggable(summaryLevel)) {
			logger.log(summaryLevel, course.getId() + " attempts " + passed + " and failed " + failed);
		}
		return passed;
	}

	/**
	 * @return
	 */
	private AttemptLineProvider attemptLineProvider() {
		return attemptLineProvider(trackGeometryService);
	}

	public static AttemptLineProvider attemptLineProvider(final ActivityTrackGeometryService trackGeometryService) {
		return new ActivityTimingRelationCandidateScreener.AttemptLineProvider() {
			@Override
			public MultiLineString attemptLine(Activity attempt) {
				return trackGeometryService.getTrackAsMultiLineString(attempt);
			}
		};
	}

	/**
	 * Provides a connected function that will transform the given course {@link ActivityId} into
	 * the a connected iterable that provides the timings for that course.
	 * 
	 * @return
	 */
	public Function<ActivityId, Iterable<ActivityTimingId>> attemptsForCourseFunction() {
		return new Function<ActivityId, Iterable<ActivityTimingId>>() {

			@Override
			@Nullable
			public Iterable<ActivityTimingId> apply(@Nullable ActivityId courseId) {
				return paginatorForTimingsAsCourse(courseId);
			}
		};
	}

	/**
	 * Screens the courses given against the single attempt to see if the courses make good
	 * candidates for deep processing.
	 * 
	 * @param attempt
	 * @param courseIds
	 * @return
	 */
	public ScreeningResults coursesScreened(Activity attempt, Iterable<ActivityId> courseIds) {
		// although ensures uniqueness which already exists
		// this is here to go synchronous since long read delays causes errors which can
		// happen for big routes
		HashSet<ActivityId> uniqueCourseIds = Sets.newHashSet(courseIds);
		Set<ActivityTimingId> candidateTimingIds = Sets.newHashSet(ActivityTimingRelationUtil
				.attemptTimingIds(uniqueCourseIds, attempt.id()));
		// don't screen anything we already know about
		Set<ActivityTimingId> idsExisting = this.timingRelationRepository.idsExisting(candidateTimingIds);

		SetView<ActivityTimingId> idsNotExisting = Sets.difference(candidateTimingIds, idsExisting);
		Iterable<Activity> courses = this.activityService.activities(ActivityTimingRelationUtil
				.courseIds(idsNotExisting));

		// used by the story to verify endpoints, getting once is good!
		MultiLineString attemptLine = this.trackGeometryService.getTrackAsMultiLineString(attempt);
		ScreeningResults results = ActivityTimingRelationCandidateScreener.coursesScreened(	attempt,
																							courses,
																							attemptLine,
																							idsExisting);
		store(results);
		return results;
	}

	/**
	 * Using the {@link ActivityTimingRelationCandidateScreener} this confirms each of the attempts
	 * given passes "light" screening which only validates geocell similarity, not JTS buffer
	 * analysis (which could be added as an option, but would be costly). This will not re-screen
	 * attempts that have previously been screened and/or processed deeply, but it will report the
	 * status of the timing result found in {@link ScreeningResults#existing()}.
	 * 
	 * 
	 * @param course
	 * @param attemptIds
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ScreeningResults attemptsScreened(ActivityId courseId, Iterable<ActivityId> attemptIds)
			throws EntityNotFoundException {

		Activity course = activityService.activity(courseId);
		Set<ActivityTimingId> timingCandidateIds = Sets.newHashSet(ActivityTimingRelationUtil.timingIds(courseId,
																										attemptIds));
		Map<ActivityTimingId, ActivityTiming> timingsExisting = this.timingRelationRepository
				.loadAll(timingCandidateIds);
		SetView<ActivityTimingId> timingIdsNotExisting = Sets.difference(timingCandidateIds, timingsExisting.keySet());
		Iterable<ActivityId> attemptsNotKnown = ActivityTimingRelationUtil
				.attemptIdsFromTimingIds(timingIdsNotExisting);
		Iterable<Activity> attemptsNotKnownAndAvailable = activityService.activities(attemptsNotKnown);
		ScreeningResults results = ActivityTimingRelationCandidateScreener
				.attemptsScreened(course, attemptsNotKnownAndAvailable, timingsExisting.values());
		store(results);
		return results;
	}

	/**
	 * Persists all of those that are appropriate to be persisted and not existing...failures.
	 * 
	 * @param results
	 */
	private void store(ScreeningResults results) {
		Multimap<TimingCompletionCategory, ActivityTimingId> failers = results.failers();
		Iterable<ActivityTiming> timings = ActivityTimingRelationUtil.timings(failers);
		this.repository().storeAll(timings);
	}

	/**
	 * After screening this will actually process the passing screeners to determine if a complete
	 * match is made. This can take some time so backen processing is most appropriate.
	 * 
	 * @param screeningResults
	 * @return
	 */
	VerifiedTimingResults processScreeners(ScreeningResults screeningResults) {
		HashSet<ActivityTiming> completions = new HashSet<>();
		HashSet<ActivityTiming> failures = new HashSet<>();

		for (ActivityTimingRelationCandidateScreener passer : screeningResults.passingScreeners()) {
			ActivityTimingRelationUserStory story;
			try {
				story = configureTimingsUserStory(passer, ActivityTimingRelationUserStory.create(), null, null);
			} catch (EntityNotFoundException e1) {
				throw new RuntimeException(e1);
			}
			try {
				ActivityTiming timing = processTiming(passer.candidateId(), false, false, story);
				if (timing.courseCompleted()) {
					completions.add(timing);
				} else {
					failures.add(timing);
				}
			} catch (EntityNotFoundException e) {
				logger.warning(passer.candidateId() + " processing caused " + e.getMessage());
			}
		}

		return new VerifiedTimingResults(screeningResults, completions, failures);
	}

	/**
	 * Given the activity that as a course, this will search for those attempts that completely
	 * contain the course making them a strong candidate.
	 * 
	 * @param summary
	 * @return
	 */
	@SuppressWarnings("unchecked")
	Iterable<ActivityId> attemptStrongCandidatesForCourse(final Activity course) {
		Iterable<ActivityId> result;
		if (ActivityTimingRelationUserStory.isLongEnough(course.getTrackSummary().getDistance())) {

			// would be nice to filter out course id, but that requires another index
			final Filter attemptsForCourseFilter = attemptCandidatesForCourseFilter(course);
			result = this.activityService.idPaginator(attemptsForCourseFilter);
		} else {
			result = Collections.EMPTY_LIST;
		}
		return result;
	}

	/**
	 * Given the course this will create a filter that looks for any other activity that may contain
	 * this course making it a strong candidate for a timing.
	 * 
	 * Notice the activity id is saved and indexed only if {@link ActivityTiming#courseCompleted()}.
	 * 
	 * @param course
	 * @return
	 */
	private Filter attemptCandidatesForCourseFilter(final Activity course) {
		Length minLengthForAttempt = ActivityTimingRelationUserStory.minLengthForAttempt(course);
		final Filter attemptsForCourseFilter = activityService.getActivityFilterBuilder()
				.setLimit(PAGE_SIZE_FOR_CANDIDATE_PROCESSING).distanceGreaterThan(minLengthForAttempt)
				.contains(course.getTrackSummary()).build();
		return attemptsForCourseFilter;
	}

	/**
	 * Identifies those activities that make good timing candidates for the given course.
	 * 
	 * @param course
	 * @return
	 */
	Iterable<ActivityId> attemptCandidateIdsForCourse(final Activity course) {
		return activityService.idPaginator(attemptCandidatesForCourseFilter(course));
	}

	/**
	 * Provides a stream of Activity attempts that completed the course.
	 * 
	 * @param course
	 * @return
	 */
	Iterable<Activity> attemptCandidatesForCourse(final Activity course) {
		Filter attemptsForCourseFilter = attemptCandidatesForCourseFilter(course);

		Iterable<Activity> activities = activityService.activityPaginator(attemptsForCourseFilter);
		// TN-552 nulls are produced when memcache has a bad reference
		// This fix is not ideal in that it lacks completeness, but it does prevent the problem and
		// a second pass will find valuable relations
		// removes nulls from the resluts
		return Iterables.filter(activities, Predicates.notNull());

	}

	/**
	 * Finds possible courses when considering the given track summary as an attempt.
	 * 
	 * @param summary
	 * @return
	 */
	Iterable<ActivityId> courseIdsForAttempt(final Activity attempt) {
		Filter filter = coursesForAttemptFilter(attempt);
		Iterable<ActivityId> candidates = this.activityService.idPaginator(filter);
		return candidates;
	}

	/**
	 * @param attempt
	 * @return
	 */
	private Filter coursesForAttemptFilter(final Activity attempt) {
		// would be nice to filter out attempt id, but that requires another index
		// TN-315 Filter out non-starters decided against
		final ActivityFilterBuilder filterBuilder = activityService.getActivityFilterBuilder();
		// TN-310 - attempt should be as long as course (with a little cushion)
		Length maxLengthForCourseCandidates = ActivityTimingRelationUserStory.maxLengthForCourse(attempt);
		Filter filter = filterBuilder.setLimit(PAGE_SIZE_FOR_CANDIDATE_PROCESSING)
				.distance(FilterOperator.LESS_THAN, maxLengthForCourseCandidates)
				// TN-445 ensures the course candidates are at least the minimum length
				.distance(FilterOperator.GREATER_THAN, ActivityTimingRelationUserStory.MIN_LENGTH_FOR_COURSE)
				.intersects(attempt.getTrackSummary()).build();
		return filter;
	}

	/**
	 * Simply queues each timing id as a task using efficient techniques.
	 * 
	 * @param timingIds
	 */
	Integer queueTimings(Iterable<ActivityTimingId> timingIds) {
		final Queue destination = getTimingCourseAttemptScreeningQueue();
		return queueTimings(timingIds, destination);
	}

	/**
	 * @param timingIds
	 * @param destination
	 * @return
	 */
	public Integer queueTimings(Iterable<ActivityTimingId> timingIds, final Queue destination) {
		BatchQueue queue = BatchQueue.create().setQueue(destination).build();

		for (ActivityTimingId activityTimingRelationId : timingIds) {
			queue.add(ActivityTimingWebApiUri.taskForCourseAttempt(	activityTimingRelationId,
																	QueueUtil.REPEAT_TASKS_SUPPRESSED).build());
		}
		return queue.finish();
	}

	/**
	 * Provides a connected iterator that will provide every {@link ActivityTimingId} that has the
	 * given {@link ActivityId} as a course.
	 * 
	 * @param activityId
	 * @return
	 */
	public Iterable<ActivityTimingId> paginatorForTimingsAsCourse(ActivityId activityId) {
		return this.timingRelationRepository.idPaginator(ActivityTimingFilterBuilder.create()
				.courseCompleted(activityId).build());
	}

	/**
	 * Provides a connected iterator that will provide every {@link ActivityTimingId} that has the
	 * given {@link ActivityId} as a attempt.
	 * 
	 * @param activityId
	 * @return
	 */
	public Iterable<ActivityTimingId> paginatorForTimingsAsAttempt(ActivityId activityId) {
		return this.timingRelationRepository.idPaginator(ActivityTimingFilterBuilder.create().attempt(activityId)
				.build());
	}

	/**
	 * * Compares two activities to prove they are or are not reciprocals of each other. It does the
	 * minimum amount of work to answer that question. The work is done by
	 * {@link ActivityTimingReciprocalProcessor}, but the resulting timings are persisted here.
	 * 
	 * If the timings already exist this will ensure they know their reciprocal relationship, but
	 * the {@link ActivityTimingReciprocalProcessor} will be null since no processing will take
	 * place.
	 * 
	 * @param candidateId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Nullable
	public ActivityTimingReciprocalProcessor reciprocalsProcessed(@Nonnull ActivityTimingId candidateId)
			throws EntityNotFoundException {

		ActivityTimingId reciprocalId = ActivityTimingRelationUtil.reciprocalId(candidateId);
		ActivityTimingRelations timings = getTimings(Lists.newArrayList(candidateId, reciprocalId));
		ActivityTimingReciprocalProcessor processor = ActivityTimingReciprocalProcessor.create(candidateId)
				.timings(timings).trackProvider(this.activityService().trackFunction())
				.activityProvider(this.activityService().idFunction()).build();
		// store the timings if updated
		{
			ArrayList<ActivityTiming> timingsToBeStored = Lists.newArrayList();
			if (processor.timingUpdated()) {
				ActivityTiming timing = processor.timing();
				storageChecks(timing);
				timingsToBeStored.add(timing);
			}
			if (processor.reciprocalUpdated()) {

				ActivityTiming reciprocalTiming = processor.reciprocal();
				storageChecks(reciprocalTiming);
				timingsToBeStored.add(reciprocalTiming);
			}
			if (!timingsToBeStored.isEmpty()) {

				repository().storeAll(timingsToBeStored);
			}
		}
		return processor;

	}

	/**
	 * Checks the timing if storing should be completed based on some overall known behaviors... If
	 * a timing is not completed then the timing should not be stored more than once...unless a
	 * re-write is forced. If a timing is completed then the timing should be stored no more than
	 * twice (once extra to update reciprocal)
	 * 
	 * @param timing
	 */
	private void storageChecks(ActivityTiming timing) {
		if (EntityUtil.isPersisted(timing)) {
			if (timing.courseCompleted()) {
				if (timing.getEntityVersion() > 2) {
					// timings may be updated about the reciprocal status, but not after that
					logger.warning(timing.id() + " is being stored again even the previous revisions of "
							+ timing.getEntityVersion());
				}
			} else {
				// failures should not be stored again unless forced
				logger.warning(timing.id() + " is failing and being stored again " + timing.getEntityVersion());
			}
		}
	}

	/**
	 * @param courseId
	 * @param attemptId
	 * @param foundRelation
	 * @return
	 * @throws EntityNotFoundException
	 */
	private Boolean isStale(ActivityId courseId, ActivityId attemptId, ActivityTiming foundRelation)
			throws EntityNotFoundException {
		// being stale means we should consider updating the stored version and notify for changes
		// ActivityTimingRelationProcessorVersion mostRecentVersion = getMostRecentVersion(courseId,
		// attemptId);
		Boolean stale = false;// ActivityTimingRelationUserStory.isStale(mostRecentVersion);
		if (!stale && foundRelation != null) {
			DateTime lastModified = foundRelation.getDateUpdated();
			// see if either activity has been modified.
			stale = activityService.modifiedSince(courseId, lastModified)
					|| activityService.modifiedSince(attemptId, lastModified);

		}
		return stale;
	}

	/**
	 * Performs the processing without doing any inspection for existing timings nor updating the
	 * result in the datstore. This is intended to be used with other systems that may need to
	 * process this information in bulk and will do the persistence.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	ActivityTimingRelationUserStory timingUserStory(ActivityTimingId id) throws EntityNotFoundException {
		return configureTimingsUserStory(id.getCourseId(), id.getAttemptId(), ActivityTimingRelationUserStory.create());
	}

	/**
	 * Does most of the work setting up the timings user story for processing.
	 * 
	 * @param courseId
	 * @param attemptId
	 * @param timingUserStoryBuilder
	 * @param foundRelation
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ActivityTimingRelationUserStory configureTimingsUserStory(@Nonnull ActivityId courseId,
			@Nonnull ActivityId attemptId, ActivityTimingRelationUserStory.Builder timingUserStoryBuilder)
			throws EntityNotFoundException {
		SameIdentifierException.throwIfEquals(courseId, attemptId);
		// both throw EntityNotFoundException
		Activity course = getActivity(courseId);
		Activity attempt = getActivity(attemptId);
		ActivityTimingRelationCandidateScreener screener = ActivityTimingRelationCandidateScreener.create()
				.course(course).attempt(attempt).attemptLineProvider(attemptLineProvider()).build();
		ActivityTimingRelationUserStory story = configureTimingsUserStory(screener, timingUserStoryBuilder, null, null);
		return story;
	}

	/**
	 * A specialized configurer for the story needed for efficient batch processing that may already
	 * provide some of the heavy parameters.
	 * 
	 * @param timingUserStoryBuilder
	 * @param screener
	 * @return
	 * @throws EntityNotFoundException
	 */
	ActivityTimingRelationUserStory configureTimingsUserStory(
			@Nonnull ActivityTimingRelationCandidateScreener screener,
			@Nullable ActivityTimingRelationUserStory.Builder timingUserStoryBuilder, @Nullable Track courseTrack,
			@Nullable Track attemptTrack) throws EntityNotFoundException {

		if (timingUserStoryBuilder == null) {
			timingUserStoryBuilder = ActivityTimingRelationUserStory.create().screener(screener);
		}
		TrackService trackService = activityService.getTrackService();
		if (courseTrack == null) {
			courseTrack = trackService.getTrack(screener.course().getTrackSummary().getDocumentId());
		}
		if (attemptTrack == null) {
			attemptTrack = trackService.getTrack(screener.attempt().getTrackSummary().getDocumentId());
		}
		timingUserStoryBuilder.screener(screener);
		timingUserStoryBuilder.setCourseTrack(courseTrack);
		TrackUserStories.Builder userStoriesBuilder = TrackUserStories.create(attemptTrack);
		userStoriesBuilder.addStandardFilters();
		ActivityTimingRelationUserStory story = userStoriesBuilder.setUserStory(timingUserStoryBuilder);
		userStoriesBuilder.build();
		return story;
	}

	/**
	 * Signals the providing application that a timing relation has been found and the course has
	 * been completed.
	 * 
	 * the course and attempt both need to notify the application. each may come from different
	 * application.
	 * 
	 * This simply adds each notification to a coordQueue in case there are delays or errors in
	 * notifying and each will remain independent.
	 * 
	 * @param relation
	 */
	private void queueToNotifyApplicationsCourseCompleted(ActivityTiming relation) {
		ActivityTimingId timingRelationId = relation.getId();
		QueueFactory queueFactory = this.queueFactory;

		try {
			ApplicationActivityImportInstructions courseInstructions = importService.getInstructions(relation
					.getCourse().getApplicationKey());
			ApplicationActivityImportInstructions attemptInstructions = importService.getInstructions(relation
					.getAttempt().getApplicationKey());
			if (courseInstructions.courseCompletedCallbackDesired()) {
				ActivityTimingWebApiUri.queueToNotifyCourseApplicationCourseCompleted(timingRelationId, queueFactory);
			}
			if (attemptInstructions.courseCompletedCallbackDesired()) {
				ActivityTimingWebApiUri.queueToNotifyAttempApplicationCourseCompleted(timingRelationId, queueFactory);
			}
		} catch (ApplicationImportInstructionsNotFoundException e) {
			// if this is happening the system won't work. Hard stop
			throw ToRuntimeException.wrapAndThrow(e);
		}
		// notify our application of the completion
		// FIXME:This is wasteful during batch processing since repeats are provided.
		// Task taskForLeaderboardUpdate =
		// ActivityTimingRelationWebApiUri.taskForLeaderboardUpdate(relation.course()
		// .getActivityId());
		// getTimingCourseCompletedNotificationQueue().add(taskForLeaderboardUpdate);

		// notifies the granular update that a course is completed
		// this fixes the inefficient re-processing of the leaderboard task above
		Task taskForRouteCompletion = RouteWebApiUri.routeCompletionForTimingTask(timingRelationId);
		getTimingCourseCompletedNotificationQueue().add(taskForRouteCompletion);

	}

	/**
	 * Makes the call to the application that provided the course that a attempt has completed it.
	 * 
	 * @param relationId
	 * @return
	 * @throws EntityNotFoundException
	 * @throws UnknownApplicationException
	 * @throws ApplicationImportInstructionsNotFoundException
	 * @throws IoException
	 * @throws EndpointNotFoundException
	 */
	public StatusMessages notifyCourseApplicationCourseCompleted(ActivityTimingId relationId)
			throws EntityNotFoundException, UnknownApplicationException, EndpointNotFoundException, IoException {

		ActivityTiming timingRelation = getTiming(relationId);
		return notifyApplicationCourseCompleted(timingRelation.getCourse().getActivityId().getApplicationKey(),
												timingRelation);
	}

	/**
	 * Makes the call to the application that provided the attempt that the attempt has completed a
	 * course.
	 * 
	 * @param relationId
	 * @return
	 * @throws EntityNotFoundException
	 * @throws EndpointNotFoundException
	 * @throws IoException
	 */
	public StatusMessages notifyAttemptApplicationCourseCompleted(ActivityTimingId relationId)
			throws EntityNotFoundException, EndpointNotFoundException, IoException {
		ActivityTiming timingRelation = getTiming(relationId);
		return notifyApplicationCourseCompleted(timingRelation.getAttempt().getActivityId().getApplicationKey(),
												timingRelation);
	}

	/**
	 * Common method used to notify applications that a course is completed only if the application
	 * desires to be notified.
	 * 
	 * @see #notifyCourseApplicationCourseCompleted(ActivityTimingId)
	 * @see #notifyAttemptApplicationCourseCompleted(ActivityTimingId)
	 * @see ApplicationActivityImportInstructions#courseCompletedCallbackDesired()
	 * @see ApplicationActivityImportInstructions#getCourseCompletedCallbackUrl()
	 * 
	 * @param applicationKey
	 * @param relation
	 * @return
	 * @throws ApplicationImportInstructionsNotFoundException
	 * @throws UnknownApplicationException
	 * @throws IoException
	 * @throws EndpointNotFoundException
	 * @throws EntityNotFoundException
	 */
	private StatusMessages notifyApplicationCourseCompleted(ApplicationKey applicationKey, ActivityTiming relation)
			throws UnknownApplicationException, EndpointNotFoundException, IoException, EntityNotFoundException {
		Application application = KnownApplication.byKey(applicationKey);
		StatusMessages.Builder messages = new StatusMessages.Builder();
		// FIXME:Find a better home in a pojo for me...and test me!
		try {
			ApplicationActivityImportInstructions instructions = importService.getInstructions(applicationKey);

			ActivityId courseId = relation.getCourse().getActivityId();
			ActivityId attemptId = relation.getAttempt().getActivityId();
			if (instructions.courseCompletedCallbackDesired()) {

				String courseCompletedCallbackUrl = instructions.getCourseCompletedCallbackUrl();
				Activity attempt = activityService.getActivity(attemptId);
				Activity course = activityService.getActivity(courseId);
				if (!attempt.isGone() && !course.isGone()) {
					URL url = ActivityTimingWebApiUri
							.buildCallbackApplicationCourseCompleted(courseCompletedCallbackUrl, courseId, attemptId);
					ApplicationNotifier notifier = new ApplicationNotifier.Builder().setUrl(url).build();
					// notifying expose that the course is gone...let's update and ignore
					// FIXME:this shouldn't automatically assume it's the course that is gone...it
					// may
					// be the attempt
					if (!isGone(courseId, notifier.getStatus(), messages)) {
						// this may exit with exception
						notifier.throwIfNecessary();
						messages.addMessage(MessageStatus.OK,
											new CompleteMessage.Builder(
													ActivityTimingRelationMessage.APPLICATION_NOTIFIED_OF_COURSE_COMPLETION)
													.addParameter(	ActivityTimingRelationMessage.Param.APPLICATION,
																	application)
													.addParameter(ActivityTimingRelationMessage.Param.COURSE, courseId)
													.addParameter(	ActivityTimingRelationMessage.Param.ATTEMPT,
																	attemptId).build());
					}
				} else {
					if (course.isGone()) {
						isGone(courseId, HttpStatusCode.GONE, messages);
					}
					if (attempt.isGone()) {
						isGone(attemptId, HttpStatusCode.GONE, messages);
					}
				}
			} else {
				messages.addMessage(MessageStatus.OK,
									new CompleteMessage.Builder(
											ActivityTimingRelationMessage.APPLICATION_NOTIFIED_OF_COURSE_COMPLETION_NOT_DESIRED)
											.addParameter(ActivityTimingRelationMessage.Param.APPLICATION, application)
											.addParameter(ActivityTimingRelationMessage.Param.COURSE, courseId)
											.addParameter(ActivityTimingRelationMessage.Param.ATTEMPT, attemptId)
											.build());
			}

		} catch (ApplicationImportInstructionsNotFoundException e) {
			// not much anyone can do about this one so simply report it gracefully.
			messages.addMessage(MessageStatus.WARNING, e.getCompleteMessage());
		} catch (MalformedURLException e) {
			// this might be worth reporting better
			throw new RuntimeException(e);
		}

		return messages.build();
	}

	/**
	 * @param courseId
	 * @param messages
	 * @return
	 */
	private Boolean isGone(ActivityId courseId, HttpStatusCode status, StatusMessages.Builder messages) {

		if (status.equals(HttpStatusCode.GONE)) {
			activityService.activityIsGone(courseId);
			messages.addMessage(MessageStatus.WARNING,
								new CompleteMessage.Builder(IoMessage.GONE).param(	IoMessage.Param.URL,
																					courseId.toString()).build());
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Will run any client provided filter without any further processing.
	 * 
	 * Internal filter must be built using {@link ActivityTimingFilterBuilder} or other
	 * #get*TimingRelations methods.
	 * 
	 * @param filter
	 * @return
	 */
	public ActivityTimingRelations timings(Filter filter) {
		return timingRelationRepository.filter(ActivityTimingFilterBuilder.clone(filter).build());
	}

	/**
	 * Given the {@link ActivityId} for a course this will return all attempts that have
	 * participated on the course. The {@link ActivityTiming#getCourse()} will always be the given
	 * course and the attempts is any persisted attempt per the rules stated at
	 * {@link #getTimingRelation(ActivityId, ActivityId)}
	 * 
	 * This is essentially the "Leaderboard".
	 * 
	 * The results can be incomplete if there are results still waiting to be processed in the
	 * coordQueue.
	 * 
	 * 
	 * @see #getTimingRelationsForAttempt(ActivityId)
	 * @param activityId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ActivityTimingRelations getTimingRelationsForCourseFirstPage(ActivityId activityId)
			throws EntityNotFoundException {
		return timingsCompletedForCourse(activityId, createDefaultTimingFilter());
	}

	/** Provides completed timings related to the courses in no particular order */
	public Iterable<ActivityTiming> timingsCompletedForCourse(ActivityId course) {
		return timingRelationRepository.entityPaginator(ActivityTimingFilterBuilder.create().courseCompleted(course)
				.build());
	}

	/**
	 * Provides all timings stored for the course
	 * 
	 * @see #timingsCompletedForCourse(ActivityId)
	 * @param courseId
	 * @return
	 */
	public Iterable<ActivityTiming> timingsForCourse(ActivityId courseId) {
		return timingRelationRepository.children(courseId);
	}

	/**
	 * Provides a stream of timing ids for all children of the course (regardless if completed.
	 * 
	 * @see #timingIdsCompletedForCourse(ActivityId)
	 * @param courseId
	 * @return
	 */
	public Iterable<ActivityTimingId> timingIdsForCourse(ActivityId courseId) {
		return timingRelationRepository.childrenIds(courseId);
	}

	/**
	 * Provides all {@link ActivityTimingId}s that completed the course in no particular order.
	 */
	public Iterable<ActivityTimingId> timingIdsCompletedForCourse(ActivityId course) {
		return timingRelationRepository.idPaginator(ActivityTimingFilterBuilder.create().courseCompleted(course)
				.build());
	}

	/**
	 * Provides all timing ids that matched as an attempt for the given {@link ActivityId}.
	 * 
	 * @param attempt
	 * @return
	 */
	public Iterable<ActivityTimingId> timingIdsForAttempt(ActivityId attempt) {
		return timingRelationRepository.idPaginator(ActivityTimingFilterBuilder.create().attempt(attempt).build());
	}

	/**
	 * The concatenation of {@link #timingIdsForAttempt(ActivityId)} and
	 * {@link #timingIdsCompletedForCourse(ActivityId)}.
	 * 
	 * @param activityId
	 * @return
	 */
	public Iterable<ActivityTimingId> timingIdsCompleted(ActivityId activityId) {
		return Iterables.concat(timingIdsCompletedForCourse(activityId), timingIdsForAttempt(activityId));
	}

	/**
	 * 
	 * @param courseId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ActivityTimingRelations timingsCompletedForCourse(ActivityId courseId, Filter filter)
			throws EntityNotFoundException {
		filter = ActivityTimingFilterBuilder.clone(filter).courseCompleted(courseId).build();
		final ActivityTimingRelations timingRelations = this.timings(filter);

		return timingRelations;
	}

	/**
	 * Just a common method for a common scenario since methods overload without filter param.
	 * 
	 * @return
	 */
	private Filter createDefaultTimingFilter() {
		return ActivityTimingFilterBuilder.create().setLimitToMax().build();
	}

	public ActivityTimingRelations getTimingRelations(ActivityId activityId, Filter filter)
			throws EntityNotFoundException {

		// combine the results of two queries.
		// can this be done in 1 query?
		Filter courseFilter = ActivityTimingFilterBuilder.clone(filter).courseCompleted(activityId).build();
		Filter attemptFilter = ActivityTimingFilterBuilder.clone(filter).attempt(activityId).build();
		return ActivityTimingRelations.create().addAll(this.timingRelationRepository.filterAsIterable(courseFilter))
				.addAll(this.timingRelationRepository.filterAsIterable(attemptFilter)).setFilter(filter).build();
	}

	Queue getTimingCourseAttemptScreeningQueue() {
		return this.queueFactory.getQueue(ActivityTimingWebApiUri.TIMINGS_COURSE_ATTEMPT_SCREENING_QUEUE_NAME);

	}

	/**
	 * Provides friendly access to the coordQueue that produces the timings relations.
	 * 
	 * FIXME:make this package friendly after moving to Timing ServiceStandard
	 * 
	 * @return
	 * 
	 */
	public Queue getTimingCourseAttemptQueue() {
		return this.queueFactory.getQueue(ActivityTimingWebApiUri.TIMINGS_COURSE_ATTEMPT_QUEUE_NAME);
	}

	/**
	 * Provides friendly access to the coordQueue that manages notifying applications about a course
	 * being found for a attempt.
	 * 
	 * @see #queueToNotifyApplicationsCourseCompleted(ActivityTiming)
	 * 
	 *      FIXME:make this package friendly after moving to Timing ServiceStandard
	 * 
	 * @return
	 * 
	 */
	public Queue getTimingCourseCompletedNotificationQueue() {
		return this.queueFactory.getQueue(ActivityTimingWebApiUri.TIMINGS_COURSE_COMPLETED_QUEUE_NAME);
	}

	Activity getActivity(ActivityId activityId) throws EntityNotFoundException {
		return importService.getActivity(activityId);
	}

	/**
	 * @return the queueFactory
	 */
	public QueueFactory getQueueFactory() {
		return this.queueFactory;
	}

	public FieldDictionary getDictionary() {
		return timingRelationRepository.getFieldDictionaryFactory().getDictionary(ActivityTiming.FIELD.DOMAIN);
	}

	ActivityTimingRelationRepository repository() {
		return this.timingRelationRepository;
	}

	/**
	 * @return
	 */
	ActivityService activityService() {
		return activityService;
	}

	/**
	 * Provides a view of those entities that exist in the datastore.
	 * 
	 * @param nonExistingTimings
	 * @return
	 */
	public Set<ActivityTimingId> idsExisting(Iterable<ActivityTimingId> possibleTimings) {
		// purposely not exposing entities here to reserve the ability to do a keys only query
		Map<ActivityTimingId, ActivityTiming> map = this.timingRelationRepository.loadAll(possibleTimings);
		return map.keySet();
	}

	/**
	 * @return the importService
	 */
	public ActivityImportService getImportService() {
		return this.importService;
	}

	/**
	 * Straight-forward return of {@link ActivityTimingRelations} matching those ids given. If a
	 * timing is not found it is ignored and provides no indication in the result.
	 * 
	 * @param results
	 * @return
	 */
	public ActivityTimingRelations getTimings(Iterable<ActivityTimingId> results) {
		return this.repository().entities(results);
	}

}
