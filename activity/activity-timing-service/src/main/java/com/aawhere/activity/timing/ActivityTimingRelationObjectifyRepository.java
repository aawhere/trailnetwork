/**
 *
 */
package com.aawhere.activity.timing;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.persist.ObjectifyFilterRepositoryDelegate;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * Persists {@link ActivityTiming} into an {@link ObjectifyRepository}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityTimingRelationObjectifyRepository
		extends
		ObjectifyFilterRepository<ActivityTiming, ActivityTimingId, ActivityTimingRelations, ActivityTimingRelations.Builder>
		implements ActivityTimingRelationRepository {

	static {
		ObjectifyService.register(ActivityTiming.class);
	}

	@Inject
	public ActivityTimingRelationObjectifyRepository(FieldDictionaryFactory dictionaryFactory) {
		this(dictionaryFactory, SYNCHRONIZATION_DEFAULT);
	}

	public ActivityTimingRelationObjectifyRepository(FieldDictionaryFactory dictionaryFactory,
			ObjectifyRepository.Synchronization synchronous) {
		this(new Delegate(dictionaryFactory), synchronous);
	}

	/**
	 * @param filterRepository
	 */
	public ActivityTimingRelationObjectifyRepository(Delegate delegate, ObjectifyRepository.Synchronization synchronous) {
		super(delegate, synchronous);

	}

	/**
	 * This is transitively a singleton because it is only created internally.
	 * 
	 * @author aroller
	 * 
	 */
	static class Delegate
			extends
			ObjectifyFilterRepositoryDelegate<ActivityTimingRelations, ActivityTimingRelations.Builder, ActivityTiming, ActivityTimingId> {

		/**
		 * @param fieldDictionaryFactory
		 */
		public Delegate(FieldDictionaryFactory fieldDictionaryFactory) {
			super(fieldDictionaryFactory);
		}

	}

}
