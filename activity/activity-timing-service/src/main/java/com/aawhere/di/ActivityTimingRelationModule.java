/**
 * 
 */
package com.aawhere.di;

import com.aawhere.activity.timing.ActivityTimingRelationObjectifyRepository;
import com.aawhere.activity.timing.ActivityTimingRelationRepository;
import com.google.inject.AbstractModule;

/**
 * @author Brian Chapman
 * 
 */
public class ActivityTimingRelationModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(ActivityTimingRelationRepository.class).to(ActivityTimingRelationObjectifyRepository.class);
	}

}
