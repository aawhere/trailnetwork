/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author aroller
 * 
 */
public class ActivityTimingRelationObjectifyRepositoryTest
		extends ActivityTimingRelationUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();
	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.activity.timing.ActivityTimingRelationUnitTest#createRepository()
	 */
	@Override
	protected ActivityTimingRelationRepository createRepository() {
		return ActivityTimingServiceTestUtil.create().build().getRepository();
	}

	@Test
	public void testAttemptStoredWhenCompleted() throws EntityNotFoundException {
		ActivityTiming timing = ActivityTimingTestUtil.createWithOneCompletion();
		ActivityTiming stored = getRepository().store(timing);
		ActivityTiming loaded = getRepository().load(stored.id());
		assertNotNull("completed so attempt should exist", loaded.attemptId());
	}

	@Test
	public void testAttemptNotStoredWhenCompleted() throws EntityNotFoundException {
		ActivityTiming timing = ActivityTimingTestUtil.createWithOneCompletion();
		ActivityTiming stored = getRepository().store(timing);
		ActivityTiming loaded = getRepository().load(stored.id());
		assertNotNull("completed so attempt should exist", loaded.attemptId());
	}
}
