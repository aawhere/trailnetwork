/**
 * 
 */
package com.aawhere.activity.timing;

import static com.aawhere.lang.number.NumberUtilsExtended.*;
import static com.aawhere.test.TestUtil.*;
import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportServiceTestUtil;
import com.aawhere.activity.ActivityImportServiceTestUtil.MockActivityDocument;
import com.aawhere.activity.ActivityReferenceIds;
import com.aawhere.activity.ActivityTestUtil;
import com.aawhere.activity.ExampleTracksImportServiceTestUtil;
import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.activity.timing.ActivityTimingRelationCandidateScreener.ScreeningResults;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.id.SameIdentifierException;
import com.aawhere.id.StringIdentifier;
import com.aawhere.io.EndpointNotFoundException;
import com.aawhere.io.IoException;
import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.queue.QueueDependencyException;
import com.aawhere.queue.QueueTestUtil;
import com.aawhere.queue.QueueTestUtil.TestQueue;
import com.aawhere.queue.WebApiQueueUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.TaskQueueTest;
import com.aawhere.test.category.WebClientTest;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.aawhere.track.match.TrackTimingRelationCalculatorUnitTest;
import com.aawhere.util.rb.StatusMessages;
import com.google.appengine.repackaged.com.google.common.collect.ImmutableList;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientHandlerException;

/**
 * Specialized for testing {@link ActivityTiming} .
 * 
 * FIXME: all of this line producing stuff has been copied to
 * {@link ExampleTracksImportServiceTestUtil}.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingServiceTest {

	public static ActivityTimingServiceTest newInstance() {
		ActivityTimingServiceTest instance = new ActivityTimingServiceTest();
		instance.setUp();
		return instance;
	}

	protected LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelperGaeBuilder()
			.withTaskQueueService().withDatastoreService().build().getHelper();
	private ActivityImportServiceTestUtil importTestUtil;
	private ActivityTimingServiceTestUtil testUtil;
	private ActivityTimingService timingService;
	private ExampleTracksImportServiceTestUtil examples;

	@Before
	public void setUp() {
		helper.setUp();
		ActivityTimingServiceTestUtil.Builder utilBuilder = ActivityTimingServiceTestUtil.create();
		// FIXME:this is duplicating the example lines above
		utilBuilder.prepareExamples();
		testUtil = utilBuilder.build();
		importTestUtil = testUtil.getImportTestUtil();
		timingService = testUtil.getActivityTimingRelationService();
		this.examples = testUtil.getExamples();
	}

	@After
	public void tearDown() {
		helper.tearDown();
	}

	@Test(expected = EntityNotFoundException.class)
	public void testFindMissingId() throws EntityNotFoundException {
		ActivityId bogusId = new ActivityId(MockActivityDocument.application.key, TestUtil.generateRandomAlphaNumeric());
		ActivityTimingId id = new ActivityTimingId(lineLonger().id(), bogusId);
		timingService.getOrProcessTiming(id);
		fail("should not be found");
	}

	/**
	 * Relations with courses not completed must be stored for "caching" purposes to avoid
	 * reprocessing.
	 * 
	 * TN-484
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	public void testStoreCourseNotCompleted() throws EntityNotFoundException {
		ActivityTiming timingRelation = ActivityTimingTestUtil.builder()
				.addFailure(TimingCompletionCategory.DID_NOT_FINISH).build();
		ActivityTiming relation = timingService.createOrUpdate(timingRelation);
		ActivityTiming found = timingService.getTiming(relation.getId());
		assertEquals("can't find a relation I just created", relation, found);
		String message = "non completions are to be stored, but shouldn't be find when queried";
		Filter filter = ActivityTimingFilterBuilder.create().build();
		Collection<ActivityTiming> timingsForAttempt = timingService.getTimingRelations(relation.attemptId(), filter)
				.all();
		TestUtil.assertEmpty(message, timingsForAttempt);
		Collection<ActivityTiming> timingsForCourse = timingService.getTimingRelations(relation.courseId(), filter)
				.all();
		TestUtil.assertEmpty(message, timingsForCourse);
	}

	/**
	 * Tests the basic service/repository functions for storage, etc.
	 * 
	 * @see #testProcessTimingRelation()
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testCreateActivityTimingRelation() throws EntityNotFoundException {

		ActivityTiming result = importStandard();
		Integer importedVersion = result.getEntityVersion();
		ActivityTimingId id = result.getId();
		assertEquals("we didn't get what we requested", id, result.getId());
		ActivityTiming stored = timingService.createOrUpdate(result);
		assertEquals("nothing changed so why a new version?", importedVersion, stored.getEntityVersion());
		ActivityTiming found = timingService.getOrProcessTiming(stored.getId());
		assertEquals(stored.getId(), found.getId());
	}

	/**
	 * CourseCompleted=false is now persisted. TN-484
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testNonCompletionStored() throws BaseException {
		Activity shorter = lineLonger();
		Activity longer = beforeAndAfter();
		ActivityTiming timingRelation = timingService.getOrProcessTiming(longer.getId(), shorter.getId());
		assertFalse("longer is too big of a course", timingRelation.isCourseCompleted());
		assertNotNull("even failures are stored", timingRelation.getDateCreated());
		ActivityTimingRelations timingRelations = timingService.timings(ActivityTimingFilterBuilder.create().build());
		TestUtil.assertSize(1, timingRelations.getCollection());
	}

	/**
	 * @return
	 * @throws BaseException
	 */
	private Activity beforeAndAfter() {
		return this.examples.beforeAndAfter();
	}

	/**
	 * @return
	 * @throws BaseException
	 */
	private Activity lineLonger() {
		return this.examples.lineLonger();
	}

	@Test
	public void testCompletionStored() throws EntityNotFoundException {
		ActivityTiming timingRelation = importStandard();
		assertTrue("course should complete for standard", timingRelation.isCourseCompleted());
		assertNotNull(timingRelation.getDateCreated());
		ActivityTiming timing = timingService.getTiming(timingRelation.id());
		TestUtil.assertNotEmpty(timing.completions());
		assertTrue(timing.courseCompleted());

		TestUtil.assertEmpty(timing.failures());
	}

	@Test
	public void testReciprocals() throws BaseException {
		Activity modifiable = this.modifiable();
		final ActivityId courseId = modifiable.id();
		Activity shifted = this.examples.lineTimeShift();
		ActivityTiming normalRelation = timingService.getOrProcessTiming(modifiable.getId(), shifted.getId());
		assertTrue("same line, different time", normalRelation.courseCompleted());
		assertNullExplanation("reciprocal should be null until reciprocated", normalRelation.reciprocal());
		ActivityTiming shiftedRelation = timingService.getOrProcessTiming(shifted.getId(), modifiable.getId());
		assertTrue("same line, different time", shiftedRelation.courseCompleted());
		assertTrue("reciprocation should be detected", shiftedRelation.reciprocal());
		assertTrue("course reciprocity should be reflected", this.timingService.getTiming(normalRelation.id())
				.reciprocal());
		// updating should change to something
		importTestUtil.getActivityImportService().importOrUpdate(courseId);
		ActivityTiming normalAfterModification = timingService.getOrProcessTiming(courseId, shifted.id());
		assertFalse("modification caused non completion", normalAfterModification.courseCompleted());
		assertNullExplanation("the relationship is not reciprocal after update", normalAfterModification.reciprocal());
		ActivityTiming shiftedAfterModification = timingService.getOrProcessTiming(shifted.id(), courseId);
		assertTrue("shifted still completes the longer", shiftedAfterModification.courseCompleted());
		assertFalse("the relationship is not reciprocal after update", shiftedAfterModification.reciprocal());
	}

	/**
	 * When the course is completed, but the reciprocal fails screening one call should answer and
	 * persist all.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testReciprocalsNotCourseFirst() throws BaseException {
		Activity normal = line();
		Activity longer = lineBefore();
		ActivityTiming normalRelation = timingService.getOrProcessTiming(normal.getId(), longer.getId());
		assertTrue("line is completed", normalRelation.courseCompleted());
		assertFalse("reciprocity is updated if screen fails", normalRelation.reciprocal());
		ActivityTiming reciprocal = timingService.reciprocal(normalRelation.id());
		assertFalse("screening should have failed", reciprocal.courseCompleted());
		assertNullExplanation("non completions don't need an indication", reciprocal.reciprocal());
		assertStrongAttemptQueue();
		assertWeakAttemptQueue();
	}

	/**
	 * A special case in
	 * {@link ActivityTimingService#reciprocalUpdated(ActivityTiming, ActivityTimingRelationCandidateScreener)}
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	@Ignore("I got close to getting this to work, but not quite.  Keep working on the real tracks.")
	public void testReciprocalsFailedAlthoughReciprocalScreeningPassed() throws EntityNotFoundException {
		// must use real courses because geocells don't match when trackpoints are too far away.
		Activity line = this.examples.realCourse();
		Activity almostReciprocal = this.examples.realRepeatInMiddle();
		ScreeningResults screenCourses = timingService.coursesScreened(almostReciprocal, Lists.newArrayList(line.id()));
		TestUtil.assertEmpty(screenCourses.failers().entries());
		ScreeningResults screenCoursesReciprocal = timingService.coursesScreened(line, Lists
				.newArrayList(almostReciprocal.id()));
		TestUtil.assertEmpty(screenCoursesReciprocal.failers().entries());

		ActivityTiming timing = timingService.getOrProcessTiming(line.getId(), almostReciprocal.getId());
		assertNull("reciprocal couldn't be determined because screening passed", timing.reciprocal());
		ActivityTiming reciprocalTiming = timingService.getOrProcessTiming(almostReciprocal.getId(), line.getId());
		assertEquals("reciprocal should have been decided", Boolean.FALSE, reciprocalTiming.reciprocal());
		assertEquals("existing timing should have been updated", Boolean.FALSE, timingService.getTiming(timing.id())
				.reciprocal());

	}

	/**
	 * When the longer line is first the timing fails, but the reciprocal can't yet be determined.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testReciprocalsNotCourseSecond() throws BaseException {
		final ActivityId courseId = this.examples.modiableRetriever().document.getActivityIdentifier();
		Activity normal = importTestUtil.createPersistedActivity(courseId);
		Activity longer = lineBefore();
		ActivityTiming longerRelation = timingService.getOrProcessTiming(longer.getId(), normal.getId());
		assertFalse("longer line is too big", longerRelation.courseCompleted());
		assertNullExplanation("can't yet know reciprocity", longerRelation.reciprocal());
		try {
			timingService.reciprocal(longerRelation.id());
			fail("screening couldn't have determined reciprocity");
		} catch (EntityNotFoundException e) {
			// expected
			assertStrongAttemptQueue();
			assertWeakAttemptQueue(ActivityTimingRelationUtil.reciprocalId(longerRelation.id()));
		}
	}

	/**
	 * Uses the {@link #modiableRetriever} to first complete, then not complete against a attempt.
	 * this verifies the values are correct and that storage is appropriately handled.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testCompletedThenNot() throws BaseException {
		Activity course = modifiable();
		Activity attempt = lineBefore();
		ActivityTiming completedRelation = timingService.getOrProcessTiming(course.getId(), attempt.getId());
		assertTrue(completedRelation.getFailures().toString(), completedRelation.isCourseCompleted());
		assertFalse("reciprocals can be known if reciprocal screening fails", completedRelation.reciprocal());

		Activity updatedCourse = importTestUtil.getActivityImportService().importOrUpdate(course.getId());
		TestUtil.assertGreaterThan("update didn't work", course.getEntityVersion(), updatedCourse.getEntityVersion());
		ActivityTiming updatedNonCompletedRelation = timingService.getOrProcessTiming(course.getId(), attempt.getId());
		assertFalse("updated activity doesn't store", updatedNonCompletedRelation.isCourseCompleted());

	}

	/**
	 * @return
	 * @throws BaseException
	 */
	private Activity lineBefore() throws BaseException {
		return this.examples.lineBefore();
	}

	/**
	 * @return
	 * @throws BaseException
	 */
	private Activity modifiable() throws BaseException {

		return this.examples.modifiable();
	}

	/**
	 * The standard is longer line against an even longer line so that the returned timing will be
	 * the course/attempt relationship that is possible, but not the inverse.
	 * 
	 * @return
	 * @throws EntityNotFoundException
	 */
	private ActivityTiming importStandard() throws EntityNotFoundException {
		ActivityTimingId id = new ActivityTimingId(lineLonger().id(), beforeAndAfter().id());
		// this imports the document.
		ActivityTiming result = timingService.getOrProcessTiming(id);
		return result;
	}

	/**
	 * Tests the queue of candidates by create two almost identical activities and one that
	 * starts/ends with the others, but is way deviant.
	 * 
	 * As a course the new activity will be a candidate against the similar, but not the deviant
	 * because the contains query.
	 * 
	 * As a attempt, any activity that intersects is a candidate for a course.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testLongerBeforeAndAfter() throws BaseException {
		Activity shorter = lineLonger();
		Activity longer = beforeAndAfter();

		assertCandidatesAsCourse("short makes a good course for longer", shorter, longer.getId());
		assertCandidatesAsAttempt("shorter doesn't intersect longer's start", shorter);

		assertCandidatesAsCourse("longer is too long for short to complete as course", longer);
		assertCandidatesAsAttempt("longer intersects shorter's start", longer, shorter.getId());

	}

	/**
	 * Screening just does pre-checks for course completed. if there are no failures then the
	 * processing adds a request to the deep processing and exits with a
	 * {@link QueueDependencyException}
	 * 
	 * @throws BaseException
	 */
	@Test(expected = QueueDependencyException.class)
	@Ignore("screening is on it's way out TN-444")
	public void testScreeningNoFailures() throws BaseException {
		Activity course = line();
		Activity attempt = lineLonger();
		try {
			timingService.getOrProcessTiming(course.getId(), attempt.getId(), true);
		} finally {
			assertStrongAttemptQueue();
			assertWeakAttemptQueue();
		}
	}

	/**
	 * Relying on logic testing from
	 * {@link ActivityTimingRelationCandidateScreenerUnitTest#testAttemptsScreened()} this ensures
	 * interaction with the storage works properly.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testScreenAttempts() throws EntityNotFoundException {
		Activity course = this.examples.line();
		Activity passer = this.examples.lineLonger();
		Activity reciprocal = this.examples.lineReciprocal();
		// ensures an activity not found does not explode
		Activity failer = this.examples.deviation();
		ActivityId fake = ActivityTestUtil.activityId();

		ImmutableList<ActivityId> candidates = ImmutableList.of(passer.id(), reciprocal.id(), failer.id(), fake);

		ActivityId courseId = course.id();
		ScreeningResults results = this.timingService.attemptsScreened(courseId, candidates);

		ActivityTimingId failingTimingId = new ActivityTimingId(courseId, failer.id());
		ActivityTimingId reciprocalTiming = new ActivityTimingId(courseId, reciprocal.id());
		ActivityTimingId passingTimingId = new ActivityTimingId(courseId, passer.id());
		// first pass
		{
			TestUtil.assertContains(passingTimingId, results.passers().keySet());
			TestUtil.assertContains(reciprocalTiming, results.passers().keySet());
			TestUtil.assertContains(reciprocalTiming, results.reciprocalCandidates());
			TestUtil.assertContainsOnly(failingTimingId, results.failers().values());
		}
		// second pass should find existing failers persisted from previous run
		{
			// running again should find the failer as existing
			ScreeningResults resultsSecondPass = this.timingService.attemptsScreened(courseId, candidates);
			TestUtil.assertEmpty("the failer shold have been persisted in previous run", resultsSecondPass.failers()
					.values());
			TestUtil.assertContainsOnly("the failer didn't get marked as existing", failingTimingId, resultsSecondPass
					.existingResults().failers().values());
		}

		// third pass is after deep processing of both passing so all should be existing
		{
			timingService.getOrProcessTiming(passingTimingId);
			timingService.getOrProcessTiming(reciprocalTiming);
			ScreeningResults resultsThirdPass = this.timingService.attemptsScreened(courseId, candidates);
			TestUtil.assertEmpty(resultsThirdPass.passers().entrySet());
			TestUtil.assertContains(passingTimingId, resultsThirdPass.existingResults().passers().keySet());
			TestUtil.assertContains(reciprocalTiming, resultsThirdPass.existingResults().passers().keySet());
		}
	}

	@Test
	public void testExistingIds() {
		ActivityTiming first = this.testUtil.createPersisted();
		ActivityTiming second = this.testUtil.createPersisted();
		ActivityTiming bogus = ActivityTimingTestUtil.createWithOneCompletion();
		Set<ActivityTimingId> idsExisting = timingService.idsExisting(Lists.newArrayList(	first.id(),
																							second.id(),
																							bogus.id()));
		TestUtil.assertContains(first.id(), idsExisting);
		TestUtil.assertContains(second.id(), idsExisting);
		TestUtil.assertDoesntContain(bogus.id(), idsExisting);
	}

	/**
	 * This will screen and find a failure returning the result. Since it should be cached a
	 * subsequent request will prove caching is working. Testing of every possible screening is done
	 * in the unit level at {@link ActivityTimingRelationUserStoryUnitTest}.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testScreeningWithFailures() throws BaseException {
		Activity course = lineLonger();
		Activity attempt = line();
		ActivityTiming timing = timingService.getOrProcessTiming(course.getId(), attempt.getId(), true);
		assertFalse("the screening should have determined the course is too long for the attempt",
					timing.isCourseCompleted());
		TestUtil.assertNotEmpty(timing.failures());

		ActivityTiming foundRelation = timingService.getTiming(timing.getId());
		// TN-591 failures aren't persisted.
		TestUtil.assertEmpty(foundRelation.failures());
		TestUtil.assertEmpty(foundRelation.completions());
		assertFalse(foundRelation.courseCompleted());
	}

	/**
	 * @return
	 * @throws BaseException
	 */
	private Activity line() {
		return this.examples.line();
	}

	@Test
	public void testCourseTooShort() throws BaseException {
		Activity microActivity = this.examples.micro();
		Activity lineActivity = line();
		Activity longerActivity = lineLonger();

		assertCandidatesAsCourse("micro is too small to be a course", microActivity);
		assertCandidatesAsAttempt("micro is too small to be an attempt", microActivity);
		assertCandidatesAsCourse("line should be course for longer", lineActivity, longerActivity.getId());
		assertCandidatesAsAttempt("line should be course for longer", longerActivity, lineActivity.getId());

	}

	@Test
	public void testLongerDeviant() throws BaseException {
		Activity longer = lineLonger();
		Activity deviant = this.examples.alternate();
		assertCandidatesAsCourse("buffers don't match", longer);
		assertCandidatesAsAttempt("shorter doesn't intersect deviant's start", longer);

		assertCandidatesAsCourse("deviant's buffer is way off", deviant);
		assertCandidatesAsAttempt(	"deviant intersects shorter's start and it is long enough to complete shorter as a course",
									deviant,
									longer.getId());

	}

	/**
	 * Proves that calls to find completions will produce those that are completed and those that
	 * are not.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testFiltering() throws EntityNotFoundException {
		Activity line = line();
		Activity lineLonger = lineLonger();
		ActivityTiming completed = timingService.getOrProcessTiming(line.id(), lineLonger.id());
		ActivityTiming notCompleted = timingService.getOrProcessTiming(lineLonger.id(), line.id());
		TestUtil.assertContainsOnly(completed, this.timingService.timingsForCourse(line.id()));
		TestUtil.assertContainsOnly(completed, this.timingService.timingsCompletedForCourse(line.id()));
		TestUtil.assertEmpty(this.timingService.timingsCompletedForCourse(lineLonger.id()));
		TestUtil.assertContainsOnly(notCompleted, this.timingService.timingsForCourse(lineLonger.id()));

	}

	@Test
	public void testLineDoubleLoop() throws EntityNotFoundException {
		Activity line = examples.line();
		final Activity loop = examples.loopDouble();
		List<ActivityTiming> timings = testUtil.importAndRelate(line.id(), loop.id());
		TestUtil.assertSize(2, timings);
		ActivityTiming lineToLoopTiming = timings.get(0);
		assertEquals(new ActivityTimingId(line.id(), loop.id()), lineToLoopTiming.id());
		final List<Completion> completions = lineToLoopTiming.completions();
		assertSize(2, completions);
		Completion loop1 = completions.get(0);
		Completion loop2 = completions.get(1);
		JodaTestUtil.assertBefore(loop1.start().getTimestamp(), loop2.start().getTimestamp());

	}

	/**
	 * These two intersect start, but not finish.
	 * 
	 * <pre>
	 *  --
	 * -   .
	 *  \_/
	 * 
	 * </pre>
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testLineDeviant() throws BaseException {
		Activity shortest = line();
		Activity deviant = this.examples.deviation();
		assertCandidatesAsCourse("buffers don't match", shortest);
		assertCandidatesAsAttempt("short doesn't hit deviant's start and deviant is too long anyway", shortest);

		assertCandidatesAsCourse("deviant's buffer is way off", deviant);
		assertCandidatesAsAttempt(	"deviant hits line's start and it is long enough, although they don't finish together we aren't checking yet",
									deviant,
									deviant.getId(),
									shortest.getId());

	}

	/**
	 * @param message
	 * @param trackSummary
	 * @param expected
	 */
	private void assertCandidatesAsAttempt(final String message, final Activity focus, ActivityId... expectedIds) {
		final Iterable<ActivityId> candidatesPaginator = timingService.courseIdsForAttempt(focus);
		assertCandidates(message, focus, candidatesPaginator, expectedIds);
	}

	private void assertCandidatesAsCourse(final String message, final Activity focus, ActivityId... activityIds) {
		final Iterable<ActivityId> candidatesPaginator = timingService.attemptStrongCandidatesForCourse(focus);
		assertCandidates(message, focus, candidatesPaginator, activityIds);

	}

	/**
	 * @param message
	 * @param focus
	 * @param candidatesPaginator
	 * @param activityId
	 */
	private void assertCandidates(final String message, final Activity focus,
			final Iterable<ActivityId> candidatesPaginator, ActivityId... activityId) {
		HashSet<ActivityId> expected = Sets.newHashSet(activityId);

		// queries return self sometimes
		HashSet<ActivityId> candidates = new HashSet<>();
		Iterables.addAll(candidates, candidatesPaginator);
		if (candidates.contains(focus.getId())) {
			expected.add(focus.getId());
		}
		assertEquals(message, expected, candidates);
	}

	/**
	 * Pretty much the same line only shifted to the left.
	 * 
	 * <pre>
	 * 1. Baseline buffer is not contained in earlyStart buffer so no course candidate
	 * 2. Early start is not contained in the Baseline Buffer so no course candidate
	 * 3. Baseline intersects earlyStart
	 * 4. Early start intersects baseLine
	 * 5. baseLine starts within the buffer of early start
	 * 6. early start does not start within the buffer of baseLine
	 * </pre>
	 * 
	 * <pre>
	 * baseLine     =  ----
	 * early start  = ----
	 * </pre>
	 * 
	 * TN-315
	 * 
	 * @since 10
	 * @throws BaseException
	 */
	@Test
	@Ignore("TN-315 we aren't storing start.  re-enable this if we start again")
	public void testLineBeforeLineAfter() throws BaseException {
		Activity baseLine = lineLonger();
		Activity before = lineBefore();

		assertCandidatesAsCourse("buffers intersect, but not contain", baseLine);
		assertCandidatesAsAttempt("baseLine does not start early enough", baseLine);

		assertCandidatesAsCourse("buffers intersect, but not contain", before);
		assertCandidatesAsAttempt("before hits the start of baseline and is long enough", before);

	}

	/**
	 * Loads a short line that is contained within a longer. It calls the queue method to verify the
	 * correct activities will qualify.
	 * 
	 * @throws BaseException
	 */
	@Test
	@Ignore("TN-447 is rethinking using quues and processing everything inline")
	public void testTimingLongVsShort() throws BaseException {
		Activity shorter = lineLonger();
		Activity longer = beforeAndAfter();
		assertEquals("course within attempt, but attempt is too long to be a course", 1, timingService
				.queueAttemptCandidates(shorter.getId()).intValue());
		assertEquals("attempt is too long as a course, but long enough to be a candiate", 1, timingService
				.queueAttemptCandidates(longer.getId()).intValue());
	}

	/**
	 * Ensures timings that have already been created are being queued up.
	 * 
	 * @see #testQueueOfExistingTimingsFocusOnAttempt()
	 * @see #testQueueAttemptTimingCandidates()
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testQueueOfExistingTimingsFocusOnCourse() throws EntityNotFoundException {
		ActivityTiming timingRelation = importStandard();
		Integer timingsFound = timingService.queueAttemptCandidates(timingRelation.getCourse().getActivityId());
		final int expectedCount = 1;
		assertEquals("already stored should queue the only relationship", expectedCount, timingsFound.intValue());

		ActivityTimingId expectedTimingId = timingRelation.id();
		assertWeakAttemptQueue(expectedTimingId);
		assertStrongAttemptQueue();
		TestQueue leaderboardQueue = (TestQueue) timingService.getTimingCourseCompletedNotificationQueue();
		final ActivityId leaderboardTaskId = timingRelation.course().getActivityId();
		// 1 leaderboard task, two alert tasks
		QueueTestUtil.assertQueueContains(	leaderboardQueue,
											3,
											leaderboardTaskId.toString(),
											expectedTimingId.toString());
	}

	@Test
	public void testQueueOfActivityAsCourseShouldFilterAttemptsThatAreTooShort() throws BaseException {
		@SuppressWarnings("unused")
		Activity lineActivity = line();
		Activity lineReturnActivity = this.examples.lineLongerReturn();
		@SuppressWarnings("unused")
		Activity lineLongerActivity = lineLonger();

		Iterable<ActivityId> idsFound = timingService.attemptStrongCandidatesForCourse(lineReturnActivity);
		final int expectedCount = 1;
		assertEquals(expectedCount, CollectionUtils.size(idsFound.iterator()));
	}

	/**
	 * Makes sure that when a cell of low resolution is given that the queues are broken cells
	 * 
	 */
	@Test
	public void testQueueOfMissedCandidatesLowResolution() {
		GeoCell abcdef = GeoCellTestUtil.ABCDEF;
		GeoCells area = GeoCells.create().add(abcdef).build();
		Integer numberOfCellsQueued = this.timingService.queueMissedTimingCandidates(area);
		TestUtil.assertGreaterThan(	"nothing queued means it is searching for candidates rather than re-queuing right resolutions",
									0,
									numberOfCellsQueued);
		// ensures the queu has at least the right number of entries
		TestQueue queue = (TestQueue) testUtil.getImportTestUtil().getQueueFactory()
				.getQueue(WebApiQueueUtil.CRAWL_QUEUE_NAME);
		QueueTestUtil.assertQueueContains(queue, numberOfCellsQueued);
		TestUtil.assertContains(queue.getQueue().getFirst().getEndPoint().getUri().toString(), abcdef.toString());

	}

	/**
	 * @throws BaseException
	 * @see #testQueueOfExistingTimingsFocusOnAttempt()
	 * @see #testQueueAttemptTimingCandidates()
	 */
	@Test
	public void testQueueOfMissedCandidates() throws BaseException {
		Activity lineActivity = line();
		Activity lineLongerActivity = lineLonger();
		ActivityTimingId relationId = new ActivityTimingId(lineActivity.id(), lineLongerActivity.id());
		Integer timingsFound = timingService.queueAttemptCandidates(lineLongerActivity.getId());

		final int expectedCount = 0;
		assertEquals(	"TN-484 No Longer searching for weak candidates during initial processing",
						expectedCount,
						timingsFound.intValue());
		assertStrongAttemptQueue();
		assertWeakAttemptQueue();

		// now, try to second pass with an area that covers most
		GeoCells spatialBuffer = lineLongerActivity.getTrackSummary().getSpatialBuffer();
		Integer missedTimingCandidates = timingService.queueMissedTimingCandidates(spatialBuffer);
		assertStrongAttemptQueue(relationId);
		assertEquals("the course should now be found as a candidate", 1, missedTimingCandidates.intValue());
		// great, queue is under way. Let's invoke what the queue would do.
		ActivityTiming timing = timingService.getOrProcessTiming(relationId);
		assertTrue("the timing is a completion", timing.courseCompleted());

		assertEquals(	"now, missing search shouldn't find anything.",
						0,
						timingService.queueMissedTimingCandidates(spatialBuffer).intValue());
	}

	public void assertQueueContainsOne() {

		assertStrongAttemptQueue(lineLonger().id());
	}

	/**
	 * Provide the ids expected in the strong queue. no ids indicate none.
	 * 
	 * @param expectedCount
	 */
	void assertStrongAttemptQueue(StringIdentifier<?>... expectedIds) {
		TestQueue timingCourseAttemptQueue = (TestQueue) timingService.getTimingCourseAttemptQueue();

		assertQueue(timingCourseAttemptQueue, expectedIds);
	}

	/** provide the ids expected in the weak queue. no ids indicate none. */
	void assertWeakAttemptQueue(StringIdentifier<?>... expectedIds) {
		TestQueue queue = (TestQueue) timingService.getTimingCourseAttemptScreeningQueue();

		assertQueue(queue, expectedIds);
	}

	/**
	 * @param expectedCount
	 * @param queue
	 */
	static void assertQueue(TestQueue queue, StringIdentifier<?>... expectedIds) {
		List<String> values = IdentifierTestUtil.values(expectedIds);
		QueueTestUtil.assertQueueContains(queue, expectedIds.length, values.toArray(new String[] {}));
	}

	/**
	 * Tests the get Timings method will find and queue attempt candidates.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testGetTimings() throws EntityNotFoundException {
		Filter filter = ActivityTimingFilterBuilder.create().build();
		final ActivityId courseId = lineLonger().id();

		ActivityTiming createdRelations = importStandard();
		ActivityTimingRelations relations;
		try {
			relations = timingService.getTimingRelations(courseId, filter);
		} catch (QueueDependencyException e) {
			TestQueue timingCourseAttemptQueue = (TestQueue) timingService.getTimingCourseAttemptQueue();

			QueueTestUtil.assertQueueContains(timingCourseAttemptQueue, 2, courseId.toString(), courseId.toString());
			relations = timingService.getTimingRelations(courseId, filter);
			TestUtil.assertContains("timing created should have been found",
									createdRelations,
									relations.getCollection());
		}

	}

	@Test
	public void testCount() throws EntityNotFoundException {
		ActivityTiming standard = importStandard();
		final ActivityId courseId = standard.course().getActivityId();
		final ActivityId attemptId = standard.attempt().getActivityId();
		assertEquals("the attempt completes the course", I(1), timingService.attemptsCount(courseId));
		assertEquals("attempt has no attempts that acomplete it", I(0), timingService.attemptsCount(attemptId));

		assertEquals("attempt completes course", I(1), timingService.coursesCount(attemptId));
		assertEquals("course doesn't complete itself", I(0), timingService.coursesCount(courseId));
	}

	/**
	 * Requests service to create a timing relation based on the standard test tracks (having the
	 * same tracks). This will produce a 100% match with the same start/finish as the course. Since
	 * the race was on then the timing will be stored and able to be found.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testProcessTimingRelationCourseCompletedAlmostSameTrack() throws BaseException {

		Activity course = lineLonger();
		Activity attempt = beforeAndAfter();
		// builds and stores the actual timings since they are new
		ActivityId courseId = course.getId();
		ActivityId attemptId = attempt.getId();

		ActivityTiming timingRelation = timingService.getOrProcessTiming(courseId, attemptId);
		assertNotNull(timingRelation);
		assertFalse("reciprocal determined since course is shorter", timingRelation.reciprocal());
		assertFalse(timingRelation.getFailures().toString(), timingRelation.getCompletions().isEmpty());
		ActivityTimingRelations leaderboard = timingService.getTimingRelationsForCourseFirstPage(courseId);
		assertNotNull(leaderboard);
		int expectedCourseCount = 1;
		assertEquals("only this attempt should be matched", expectedCourseCount, leaderboard.size());
		assertEquals("attempt should be found", attemptId, leaderboard.getCollection().get(0).getAttempt()
				.getActivityId());

		// make sure the course can be found by applicationActivityId
		ActivityTimingRelations timingRelationsForCourse = timingService.getTimingRelationsForCourseFirstPage(courseId);
		assertEquals(expectedCourseCount, timingRelationsForCourse.size());

		// make ure the attempt can be found by applicationActivityId
		Iterable<ActivityTiming> timingRelationsForAttempt = timingService.timings(ActivityTimingFilterBuilder.create()
				.attempt(attemptId).build());
		Integer expectedAttemptCount = 1;
		TestUtil.assertSize(expectedAttemptCount, timingRelationsForAttempt);

		int expectedNotificationAlerts = 2;
		TestQueue courseCompletedNotificationQueue = getNotificationsQueue();
		int expectedLeaderboardsQueued = 1;
		QueueTestUtil.assertQueueContains(	courseCompletedNotificationQueue,
											expectedNotificationAlerts + expectedLeaderboardsQueued,
											timingRelation.getId().toString(),
											timingRelation.getId().toString(),
											timingRelation.getCourse().getActivityId().toString());
		// now, repeat the call to ensure no duplicate resources will be created

		ActivityTiming secondCallTiming = timingService.getOrProcessTiming(courseId, attemptId);
		assertEquals(	"nothing should have changed",
						timingRelation.getEntityVersion(),
						secondCallTiming.getEntityVersion());

	}

	/**
	 * @return
	 */
	private TestQueue getNotificationsQueue() {
		TestQueue courseCompletedNotificationQueue = (TestQueue) timingService
				.getTimingCourseCompletedNotificationQueue();
		return courseCompletedNotificationQueue;
	}

	@Test(expected = SameIdentifierException.class)
	public void testSameId() throws EntityNotFoundException {
		ActivityTiming timin = testUtil.createPersisted();
		ActivityId id = timin.getCourse().getActivityId();
		this.timingService.getOrProcessTiming(id, id);
	}

	/**
	 * Simply tests that an activity that starts/finishes on a course, but doesn't follow the course
	 * will not qualify.
	 * 
	 * @throws BaseException
	 * 
	 * @see TrackTimingRelationCalculatorUnitTest#testStartFinishFollowingAlternateCourse()
	 * 
	 */
	@Test
	public void testCourseNotCompletedRouteDeviation() throws BaseException {

		Activity course = lineLonger();
		Activity attempt = this.examples.deviation();
		ActivityTiming timingRelation = timingService.getOrProcessTiming(course.getId(), attempt.getId());
		assertFalse("huge deviation from course", timingRelation.isCourseCompleted());
		TestUtil.assertEmpty(timingRelation.getCompletions());
		TestUtil.assertNotEmpty(timingRelation.getFailures());
		assertStrongAttemptQueue();
		QueueTestUtil.assertQueueContains(getNotificationsQueue(), 0);
	}

	@Test
	@Ignore("the logic allows for removal of an activity if the processor changes, but can't test as of now")
	public void testPersistedTimingDeletedAfterReprocessing() {
		fail("not able to test yet because no way to change the user story during a test");
	}

	@Test
	@Category(WebClientTest.class)
	public void testNotifyCourseCompleted() throws EntityNotFoundException, UnknownApplicationException,
			EndpointNotFoundException, IoException {
		boolean testCourse = true;
		assertNotifyCompletion(testCourse);

	}

	@Test
	@Category(WebClientTest.class)
	public void testNotifyAttemptCompleted() throws EntityNotFoundException, UnknownApplicationException,
			EndpointNotFoundException, IoException {
		boolean testCourse = false;
		assertNotifyCompletion(testCourse);

	}

	/**
	 * @param testCourse
	 * @throws EntityNotFoundException
	 * @throws EndpointNotFoundException
	 * @throws IoException
	 */
	private void assertNotifyCompletion(boolean testCourse) throws EntityNotFoundException, EndpointNotFoundException,
			IoException {
		ActivityTiming standard = importStandard();
		try {
			final ActivityTimingId id = standard.getId();
			StatusMessages messages;
			if (testCourse) {
				messages = timingService.notifyCourseApplicationCourseCompleted(id);
			} else {
				messages = timingService.notifyAttemptApplicationCourseCompleted(id);
			}
			final String message = messages.toString();
			assertFalse(message, messages.hasError());
			TestUtil.assertSize(message, 1, messages);
		} catch (ClientHandlerException e) {
			TestUtil.assertInstanceOf(	"this is expected, anything else is a problem",
										UnknownHostException.class,
										e.getCause());
		}
	}

	/**
	 * First a successful course is completed, then the activity is updated with a track that won't
	 * complete the course. This ensures the timing is properly updated by return the failure and
	 * removing the completion from the database.
	 * 
	 * @throws BaseException
	 */
	@Test
	@Ignore("There are issues with the modifiable document after Example tracks started adjusting times...fix when needed")
	public
			void testUpdateTimingAfterActivityChangeCourseCompletedThenNot() throws BaseException {

		Activity course = modifiable();
		Activity attempt = importTestUtil.createPersistedActivity(lineLonger().id());
		ActivityTiming timingRelation = timingService.getOrProcessTiming(course.getId(), attempt.getId());
		assertTrue("course must be completed for this test", timingRelation.isCourseCompleted());
		// update the course.
		Activity updatedCourse = importTestUtil.getActivityImportService().importOrUpdate(course.getId());
		TestUtil.assertGreaterThan("course didn't update", course.getEntityVersion(), updatedCourse.getEntityVersion());
		assertTrue("udpated course is before course", course.getDateUpdated().isBefore(updatedCourse.getDateUpdated()));
		ActivityTiming updated = timingService.getOrProcessTiming(timingRelation.getId());
		assertFalse("course is no longer completed", updated.isCourseCompleted());
		assertNotSame("depends upon these name being the same", timingRelation, updated);
		assertEquals("timing should have re-processed", timingRelation.getEntityVersion() + 1, updated
				.getEntityVersion().intValue());
	}

	/**
	 * Since update timings works off of the max limit, we need to test that update timings can
	 * queue beyond that limit.
	 * 
	 */
	@Category({ TaskQueueTest.class })
	@Test
	public void testUpdateTimingsWithDifferentFailures() {
		ActivityTiming original = testUtil.createPersisted();
		ActivityTiming mutated = ActivityTiming.mutate(original)
				.addFailure(TimingCompletionCategory.NOT_SIMILAR_ENOUGH).build();
		ActivityTiming updated = timingService.createOrUpdate(mutated);
		assertEquals(	"when only failures change nothing should be stored again",
						original.getEntityVersion(),
						updated.getEntityVersion());
	}

	@SuppressWarnings("unused")
	public void testProcessTimingsForceStale() throws BaseException {
		Activity other = examples.deviation();
		ActivityTiming created = importStandard();
		// This must be true for the remainder of the tests.
		assertTrue(created.isCourseCompleted());
		// Make so we can detect a change after re-processing.
		ActivityTiming mutated = ActivityTiming.mutate(created)
				.setAttempt(ActivityReferenceIds.create().setActivity(other).build()).build();

		// Get a fresh created so it doesn't represent the mutated version.
		testUtil.getRepository().store(mutated);

		// should be here
		ActivityTiming found = testUtil.getRepository().load(created.getId());
		assertNotNull(found);
		ActivityTiming afterUpdate = this.timingService.processTiming(found.getId(), false, false);
		ActivityTiming foundAfterUpdate = testUtil.getRepository().load(afterUpdate.getId());
		assertEquals(	"should have not updated the entity",
						found.getEntityVersion(),
						foundAfterUpdate.getEntityVersion());

		ActivityTiming afterForce = this.timingService.processTiming(created.getId(), false, true);
		try {
			ActivityTiming foundAfterForce = testUtil.getRepository().load(afterUpdate.getId());
			fail("Should have been removed with the force = true");
		} catch (EntityNotFoundException e) {
			// ok.
		}
	}

	@Test
	public void testProcessTimingsShouldNotUpdateIfNothingChanged() throws BaseException {
		ActivityTiming created = importStandard();
		final Integer createdVersion = created.getEntityVersion();

		// This must be true for the remainder of the tests.
		assertTrue(created.isCourseCompleted());
		ActivityTiming found = testUtil.getRepository().load(created.getId());
		assertNotNull(found);
		final Integer foundVersion = found.getEntityVersion();
		assertEquals("version's shouldn't change on get", createdVersion, foundVersion);
		assertNotNull(found.reciprocal());
		ActivityTiming afterUpdate = this.timingService.processTiming(found.getId(), false, true);
		assertEquals("the reciprocal is determined and timing modified", foundVersion + 2, afterUpdate
				.getEntityVersion().intValue());
		ActivityTiming foundAfterUpdate = testUtil.getRepository().load(afterUpdate.getId());
		assertEquals(	"should have not updated the entity",
						afterUpdate.getEntityVersion(),
						foundAfterUpdate.getEntityVersion());
		assertEquals(	"alright, one more process shouldn't do anything",
						afterUpdate.getEntityVersion(),
						this.timingService.processTiming(found.getId(), false, false).getEntityVersion());
	}

	@Test
	public void testProcessTimingsShouldNotSendNotificationsIfNothingChanged() throws BaseException {
		ActivityTiming created = importStandard();
		// This must be true for the remainder of the tests.
		assertTrue(created.isCourseCompleted());

		TestQueue queue = (TestQueue) QueueTestUtil.getQueueFactory()
				.getQueue(ActivityTimingWebApiUri.TIMINGS_COURSE_COMPLETED_QUEUE_NAME);
		assertEquals(Integer.valueOf(0), queue.size());
		ActivityTiming found = testUtil.getRepository().load(created.getId());
		assertNotNull(found);
		this.timingService.processTiming(found.getId(), false, true);
		assertEquals(Integer.valueOf(0), queue.size());

	}

	@Test
	public void testCreateUpdateVersioning() {
		ActivityTiming created = testUtil.createPersisted();
		Integer versionAfterCreation = created.getEntityVersion();
		ActivityTiming cloned = ActivityTimingTestUtil.cloneTiming(created).build();
		ActivityTiming nothingUpdated = this.timingService.createOrUpdate(cloned);
		assertEquals("nothing changed, why the new version", versionAfterCreation, nothingUpdated.getEntityVersion());
		ActivityTiming completionAdded = ActivityTiming.mutate(nothingUpdated).addCompletion(P1, P2).build();
		ActivityTiming afterCompletionsUpdated = this.timingService.createOrUpdate(completionAdded);
		assertEquals("completions should have been stored", versionAfterCreation + 1, afterCompletionsUpdated
				.getEntityVersion().intValue());
		assertTrue("not stored", ActivityTimingRelationUtil.equalsDeeply(completionAdded, afterCompletionsUpdated));
		assertFalse("not changed form original",
					ActivityTimingRelationUtil.equalsDeeply(created, afterCompletionsUpdated));
	}

	@Test
	public void testPreScreenedReciprocalProcessed() throws EntityNotFoundException {
		Activity line = this.examples.line();
		Activity lineReciprocal = this.examples.lineReciprocal();
		assertReciprocalsProcessed(line, lineReciprocal, true);

	}

	/**
	 * @param line
	 * @param lineReciprocal
	 * @return
	 * @throws EntityNotFoundException
	 */
	private ActivityTimingReciprocalProcessor assertReciprocalsProcessed(Activity line, Activity lineReciprocal,
			Boolean reciprocals) throws EntityNotFoundException {
		ActivityTimingReciprocalProcessor reciprocalsProcessed = this.timingService
				.reciprocalsProcessed(ActivityTimingRelationUtil.timingId(line, lineReciprocal));
		ActivityTiming left = reciprocalsProcessed.timing();
		if (left != null) {
			assertEquals(reciprocals, left.reciprocal());
			assertEquals("persistence didn't happen", reciprocals, timingService.getTiming(left.id()).reciprocal());
			if (BooleanUtils.isTrue(reciprocals)) {
				ActivityTiming right = reciprocalsProcessed.reciprocal();
				assertNotNull(right);
				assertEquals(reciprocals, right.reciprocal());
				assertEquals("persistence didn't happen", reciprocals, timingService.getTiming(right.id()).reciprocal());
			}
		} else {
			assertFalse("no timing", reciprocalsProcessed.reciprocal().courseCompleted());
		}

		return reciprocalsProcessed;
	}

	@Test
	public void testPreScreenedReciprocalProcessedCourseExists() throws EntityNotFoundException {
		Activity line = this.examples.line();
		Activity lineReciprocal = this.examples.lineReciprocal();
		timingService.getOrProcessTiming(line.id(), lineReciprocal.id());
		assertReciprocalsProcessed(line, lineReciprocal, true);

	}

	@Test
	public void testPreScreenedReciprocalProcessedAttemptExists() throws EntityNotFoundException {
		Activity line = this.examples.line();
		Activity lineReciprocal = this.examples.lineReciprocal();
		timingService.getOrProcessTiming(lineReciprocal.id(), line.id());
		assertReciprocalsProcessed(line, lineReciprocal, true);

	}

	@Test
	public void testReciprocalsPreScreenedNot() throws EntityNotFoundException {
		Activity line = this.examples.line();
		Activity lineLonger = this.examples.lineLonger();
		assertReciprocalsProcessed(line, lineLonger, false);
	}
}
