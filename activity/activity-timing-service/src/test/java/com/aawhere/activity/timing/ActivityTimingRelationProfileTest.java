/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import org.joda.time.Duration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityId;
import com.aawhere.app.KnownApplication;
import com.aawhere.joda.time.DurationUtil;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ServiceTest;
import com.aawhere.test.category.WebClientTest;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * Runs scenario tests to make sure real activities are finding accurate timing results. Doesn't
 * require a web server, so it manually works through the necessary workflow instead of using
 * queues.
 * 
 * @see ActivityTiming
 * 
 * @author aroller
 * 
 */
@Category({ WebClientTest.class, ServiceTest.class })
public class ActivityTimingRelationProfileTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private ActivityTimingServiceTestUtil testUtil;
	private ActivityTimingService timingService;
	ActivityId courseId;
	ActivityId attemptId;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		helper.setUp();
		com.aawhere.activity.timing.ActivityTimingServiceTestUtil.Builder utilBuilder = ActivityTimingServiceTestUtil
				.create();
		final com.aawhere.activity.ActivityImportServiceTestUtil.Builder activityImportServiceTestUtilBuilder = utilBuilder
				.getImportUtilBuilder();
		activityImportServiceTestUtilBuilder.registerTwoPeakImportIntructions()
				.registerGarminConnectImportInstructions();

		activityImportServiceTestUtilBuilder.useTestQueue();
		this.testUtil = utilBuilder.build();
		this.timingService = testUtil.getActivityTimingRelationService();
		courseId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "303046413");
		attemptId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "287401319");
		testUtil.getImportTestUtil().getActivityImportService().getActivity(attemptId);
		testUtil.getImportTestUtil().getActivityImportService().getActivity(courseId);
	}

	@After
	public void tearDownServices() {
		helper.tearDown();
	}

	/**
	 * @param attemptId
	 * @param courseId
	 * @param expectedMaxDuration
	 * @param minimumCourseMatchRatio
	 * @throws BaseException
	 * @throws EntityNotFoundException
	 */
	private void assertTiming(ActivityId courseId, ActivityId attemptId, Duration expectedMaxDuration,
			Ratio minimumCourseMatchRatio) throws BaseException, EntityNotFoundException {

		assertTiming(new ActivityTimingId(courseId, attemptId), expectedMaxDuration);
	}

	/**
	 * @param timingRelation
	 * @param expectedMaxDuration
	 * @throws EntityNotFoundException
	 */
	private void assertTiming(ActivityTimingId timingRelationId, Duration expectedMaxDuration)
			throws EntityNotFoundException {
		ActivityTiming timingRelation = timingService.getOrProcessTiming(timingRelationId);

		if (expectedMaxDuration != null) {

			assertTrue("course not completed" + timingRelation.getFailures(), timingRelation.isCourseCompleted());
			Duration actualDuration = timingRelation.getCompletions().get(0).getDuration();
			TestUtil.assertLessThan("the climb took longer than expected",
									expectedMaxDuration.getMillis(),
									actualDuration.getMillis());
		} else {

			assertFalse("no duration means course not completed, but found completions: "
								+ timingRelation.getCompletions(),
						timingRelation.isCourseCompleted());
		}
	}

	/**
	 * Given a standard hill climb this compares a known matching attempt.
	 * 
	 * see https://aawhere.jira.com/wiki/display/AAWHERE/Hill+Climb+Case+Study
	 * 
	 * @throws BaseException
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testMarincelloHillClimb() throws EntityNotFoundException, BaseException {
		// approx duration determined by http://connect.garmin.com/jsPlayer/287401319
		Duration expectedMaxDuration = DurationUtil.durationFromMinutes(24);
		assertTiming(courseId, attemptId, expectedMaxDuration, MeasurementUtil.createRatio(.99));
	}

}
