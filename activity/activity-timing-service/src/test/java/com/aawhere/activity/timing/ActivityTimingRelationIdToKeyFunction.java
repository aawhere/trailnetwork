/**
 * 
 */
package com.aawhere.activity.timing;

import com.aawhere.persist.objectify.ObjectifyKeyUtils;

import com.google.common.base.Function;
import com.googlecode.objectify.Key;

/**
 * A simple function to create datastore keys from {@link ActivityTimingId}s. Although this could be
 * done generically using {@link ObjectifyKeyUtils}, it is important that these are efficient.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingRelationIdToKeyFunction
		implements Function<ActivityTimingId, Key<ActivityTiming>> {

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Key<ActivityTiming> apply(ActivityTimingId input) {
		return Key.create(ActivityTiming.class, input.getValue());
	}

}
