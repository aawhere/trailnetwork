/**
 *
 */
package com.aawhere.activity.timing;

import java.util.LinkedList;

import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityImportServiceTestUtil;
import com.aawhere.activity.ActivityTrackGeometryService;
import com.aawhere.activity.ExampleTracksImportServiceTestUtil;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.google.common.collect.Lists;

/**
 * TestServiceUtil for {@link ActivityTimingService} in the reactor chain depending upon
 * {@link TrackRelationServiceTestUtil}.
 * 
 * @author aroller
 * 
 */
public class ActivityTimingServiceTestUtil {

	private ActivityImportServiceTestUtil importTestUtil;
	private ActivityTimingService activityTimingRelationService;
	private ActivityTimingRelationObjectifyRepository repository;

	/**
	 * @return the activityTimingRelationService
	 */
	public ActivityTimingService getActivityTimingRelationService() {
		return this.activityTimingRelationService;
	}

	/**
	 * @return the examples
	 */
	public ExampleTracksImportServiceTestUtil getExamples() {
		return this.importTestUtil.getExamplesUtil();
	}

	/**
	 * @return the importTestUtil
	 */
	public ActivityImportServiceTestUtil getImportTestUtil() {
		return this.importTestUtil;
	}

	/**
	 * @return the repository
	 */
	public ActivityTimingRelationObjectifyRepository getRepository() {
		return this.repository;
	}

	public ActivityTiming createPersisted() {
		ActivityTiming entitySample = ActivityTimingRelationUnitTest.getInstance().getEntitySample();
		return getRepository().store(entitySample);
	}

	/**
	 * Used to construct all instances of ActivityTimingRelationServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<ActivityTimingServiceTestUtil> {

		private ActivityImportServiceTestUtil.Builder importUtilBuilder;

		public Builder() {
			super(new ActivityTimingServiceTestUtil());
			importUtilBuilder = ActivityImportServiceTestUtil.create();
		}

		/**
		 * @return the importUtilBuilder
		 */
		public ActivityImportServiceTestUtil.Builder getImportUtilBuilder() {
			return this.importUtilBuilder;
		}

		public FieldAnnotationDictionaryFactory getAnnotationDictionaryFactory() {
			return this.importUtilBuilder.getPersonServiceTestUtilBuilder().getAnnotationDictionaryFactory();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ActivityTimingServiceTestUtil build() {
			getImportUtilBuilder().useTestQueue();
			FieldAnnotationDictionaryFactory fieldDictionaryFactory = importUtilBuilder
					.getPersonServiceTestUtilBuilder().getAnnotationDictionaryFactory();
			fieldDictionaryFactory.getDictionary(ActivityTiming.class);

			building.importTestUtil = importUtilBuilder.build();

			building.repository = new ActivityTimingRelationObjectifyRepository(fieldDictionaryFactory.getFactory(),
					ObjectifyRepository.Synchronization.SYNCHRONOUS);

			building.activityTimingRelationService = new ActivityTimingService(building.importTestUtil
					.getActivityServiceTestUtil().getActivityService(),
					building.importTestUtil.getActivityImportService(), new ActivityTrackGeometryService(
							building.importTestUtil.getActivityServiceTestUtil().getTrackTestUtil().getTrackService(),
							building.importTestUtil.getActivityImportService()), building.repository,
					building.importTestUtil.getQueueFactory());

			return super.build();

		}

		/**
		 * @return
		 */
		public Builder prepareExamples() {
			getImportUtilBuilder().examplesPrepared();
			return this;
		}
	}// end Builder

	/**
	 * Given the set of ids this will import each and create timing relations for each in both
	 * directions.
	 * 
	 * @param activityIds
	 * @return
	 * @throws EntityNotFoundException
	 */
	public LinkedList<ActivityTiming> importAndRelate(ActivityId... activityIds) throws EntityNotFoundException {

		LinkedList<ActivityTimingId> results = new LinkedList<>();
		for (int i = 0; i < activityIds.length; i++) {
			ActivityId courseId = activityIds[i];
			for (int j = 0; j < activityIds.length; j++) {
				ActivityId attemptId = activityIds[j];
				if (!courseId.equals(attemptId)) {
					ActivityTimingId id = new ActivityTimingId(courseId, attemptId);
					getActivityTimingRelationService().getOrProcessTiming(id);
					results.add(id);
				}
			}

		}
		// refresh these since they could have gotten stale (specifically reciprocal).
		return Lists.newLinkedList(getActivityTimingRelationService().getTimings(results));
	}

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTimingRelationServiceTestUtil */
	private ActivityTimingServiceTestUtil() {
	}

	public ActivityTiming importStandard() {
		try {
			return importAndRelate(this.getExamples().line().id(), this.getExamples().lineLonger().id()).iterator()
					.next();
		} catch (EntityNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

}
