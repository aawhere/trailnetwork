/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.activity.timing.ActivityTiming.Completion;
import com.aawhere.app.ApplicationKey;
import com.aawhere.collections.Index;
import com.aawhere.persist.BaseObjectifyFilterRepositoryTest;
import com.aawhere.persist.Filter;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class ActivityTimingRelationObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<ActivityTimingId, ActivityTiming, ActivityTimingRelationObjectifyRepository, ActivityTimingRelationObjectifyRepository, ActivityTimingRelations, ActivityTimingRelations.Builder> {

	private ActivityTimingRelationObjectifyRepository repository;

	@Before
	public void setUp() {
		this.repository = ActivityTimingServiceTestUtil.create().build().getRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected ActivityTiming createEntity() {

		return ActivityTimingRelationUnitTest.getInstance().getEntitySample();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected ActivityTimingRelationObjectifyRepository getRepository() {
		return this.repository;
	}

	/**
	 * Ordering by duration is important so testing this at the most granular level.
	 * 
	 * @throws Exception
	 * 
	 */
	@Test
	@Ignore("this test needs to use ActivityTimingRelationUtil to get the best.")
	public void testOrderByDuration() throws Exception {
		createPersistedEntity();
		createPersistedEntity();
		Filter filter = ActivityTimingFilterBuilder.create().orderByShortestDuration().build();
		ActivityTimingRelations relations = this.repository.filter(filter);
		assertEquals(2, relations.size());
		List<ActivityTiming> list = relations.getCollection();
		// the odds of finding equivalent times is unlikely
		ActivityTiming first = list.get(Index.FIRST.index);
		Completion firstCompletion = first.getCompletions().get(0);
		ActivityTiming second = list.get(Index.SECOND.index);
		Completion secondCompletion = second.getCompletions().get(0);

		TestUtil.assertLessThanEqualTo(	"sort by ascending duration should make first right than second",
										secondCompletion.getDuration().getMillis(),
										firstCompletion.getDuration().getMillis());
	}

	@Test
	@Ignore("TN-374 removed this index to save space since the system doesn't need it.")
	public void testOrderByCreateDate() throws InterruptedException {
		ActivityTiming expectedOldest = createPersistedEntity();
		// add a 1 ms wait here if this test gets flaky.
		ActivityTiming expectedYoungest = createPersistedEntity();

		Filter filter = ActivityTimingFilterBuilder.create().orderByRecentlyCreated().build();
		ActivityTimingRelations relations = this.repository.filter(filter);
		List<ActivityTiming> list = relations.getCollection();
		TestUtil.assertNotEmpty(list);
		ActivityTiming actualYoungest = list.get(Index.FIRST.index);
		ActivityTiming actualOldest = list.get(Index.SECOND.index);
		assertEquals(expectedOldest, actualOldest);
		assertEquals(expectedYoungest, actualYoungest);

	}

	@Test
	@Ignore("TN-376 and TN-375 affect this, but it isn't needed.")
	public void testFilterTimingsByApp() {
		ActivityTiming timingRelation = createPersistedEntity();
		ApplicationKey applicationKey = timingRelation.getCourse().getApplicationKey();
		assertEquals("this test assumes course and attempt are from same app", timingRelation.getCourse()
				.getApplicationKey(), timingRelation.getAttempt().getApplicationKey());
		ApplicationKey badKey = new ApplicationKey("no way this will be an app");
		assertApplicationFilter(applicationKey, "the one created should have been found", 1);
		assertApplicationFilter(badKey, "the app shouldn't match", 0);
		ActivityTiming timingRelation2 = createPersistedEntity();
		assertEquals(timingRelation.getCourse().getApplicationKey(), timingRelation2.getCourse().getApplicationKey());
		assertApplicationFilter(applicationKey, "two created should both be found", 2);
		assertApplicationFilter(badKey, "the app shouldn't match", 0);

	}

	/**
	 * @param applicationKey
	 * @param message
	 * @param expectedCount
	 */
	private void assertApplicationFilter(ApplicationKey applicationKey, String message, int expectedCount) {
		assertFilter(message, expectedCount, ActivityTimingFilterBuilder.create().courseApplication(applicationKey)
				.build());
		assertFilter(message, expectedCount, ActivityTimingFilterBuilder.create().attemptApplication(applicationKey)
				.build());
		assertFilter(message, expectedCount, ActivityTimingFilterBuilder.create().application(applicationKey).build());
	}

}
