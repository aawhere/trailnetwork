/**
 * 
 */
package com.aawhere.activity.timing;

import static org.junit.Assert.*;

import org.joda.time.Duration;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.Activities;
import com.aawhere.activity.Activity;
import com.aawhere.activity.ActivityId;
import com.aawhere.activity.ActivityService;
import com.aawhere.app.KnownApplication;
import com.aawhere.joda.time.DurationUtil;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ManualTest;
import com.aawhere.test.category.ServiceTest;
import com.aawhere.test.category.WebClientTest;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * Runs scenario tests to make sure real activities are finding accurate timing results. Doesn't
 * require a web server, so it manually works through the necessary workflow instead of using
 * queues.
 * 
 * @see ActivityTiming
 * 
 * @author aroller
 * 
 */
@Category({ WebClientTest.class, ServiceTest.class })
public class ActivityTimingRelationIntegrationTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private ActivityTimingServiceTestUtil testUtil;
	private ActivityTimingService timingService;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		helper.setUp();
		com.aawhere.activity.timing.ActivityTimingServiceTestUtil.Builder utilBuilder = ActivityTimingServiceTestUtil
				.create();
		final com.aawhere.activity.ActivityImportServiceTestUtil.Builder activityImportServiceTestUtilBuilder = utilBuilder
				.getImportUtilBuilder();
		activityImportServiceTestUtilBuilder.registerTwoPeakImportIntructions()
				.registerGarminConnectImportInstructions();

		activityImportServiceTestUtilBuilder.useTestQueue();
		this.testUtil = utilBuilder.build();
		this.timingService = testUtil.getActivityTimingRelationService();
	}

	@After
	public void tearDownServices() {
		helper.tearDown();
	}

	/**
	 * The larger activity starts at the trailhead of the section, but does a huge loop and returns
	 * to the trailhead to complete the section giving a much longer time to ride the section than
	 * actual. This will test to make sure the duration is near actual.
	 * 
	 * https://aawhere.jira.com/browse/TN-156
	 * 
	 * <pre>
	 * attempt http://connect.garmin.com/activity/282375368 
	 * climb = http://connect.garmin.com/activity/302994809
	 * </pre>
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testActivityWithTwoStarts() throws BaseException {
		ActivityId courseId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "302994809");
		ActivityId attemptId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "282375368");
		Duration expectedMaxDuration = DurationUtil.durationFromMinutes(22);
		Ratio minimumCourseMatchRatio = MeasurementUtil.createRatio(.99);
		assertTiming(courseId, attemptId, expectedMaxDuration, minimumCourseMatchRatio);

	}

	/**
	 * @param attemptId
	 * @param courseId
	 * @param expectedMaxDuration
	 * @param minimumCourseMatchRatio
	 * @throws BaseException
	 * @throws EntityNotFoundException
	 */
	private void assertTiming(ActivityId courseId, ActivityId attemptId, Duration expectedMaxDuration,
			Ratio minimumCourseMatchRatio) throws BaseException, EntityNotFoundException {
		testUtil.getImportTestUtil().getActivityImportService().getActivity(attemptId);
		testUtil.getImportTestUtil().getActivityImportService().getActivity(courseId);

		assertTiming(new ActivityTimingId(courseId, attemptId), expectedMaxDuration);
	}

	/**
	 * @param timingRelation
	 * @param expectedMaxDuration
	 * @throws EntityNotFoundException
	 */
	private void assertTiming(ActivityTimingId timingRelationId, Duration expectedMaxDuration)
			throws EntityNotFoundException {
		ActivityTiming timingRelation = timingService.getOrProcessTiming(timingRelationId);

		if (expectedMaxDuration != null) {

			assertTrue("course not completed" + timingRelation.getFailures(), timingRelation.isCourseCompleted());
			Duration actualDuration = timingRelation.getCompletions().get(0).getDuration();
			TestUtil.assertLessThan("the climb took longer than expected",
									expectedMaxDuration.getMillis(),
									actualDuration.getMillis());
		} else {

			assertFalse("no duration means course not completed, but found completions: "
								+ timingRelation.getCompletions(),
						timingRelation.isCourseCompleted());
		}
	}

	/**
	 * Given a standard hill climb this compares a known matching attempt.
	 * 
	 * see https://aawhere.jira.com/wiki/display/AAWHERE/Hill+Climb+Case+Study
	 * 
	 * @throws BaseException
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testMarincelloHillClimb() throws EntityNotFoundException, BaseException {
		ActivityId courseId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "303046413");
		ActivityId attemptId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "287401319");

		// approx duration determined by http://connect.garmin.com/jsPlayer/287401319
		Duration expectedMaxDuration = DurationUtil.durationFromMinutes(24);
		assertTiming(courseId, attemptId, expectedMaxDuration, MeasurementUtil.createRatio(.99));
	}

	/**
	 * Since this is a spatial match, but not a sequential match this test produces
	 * courseNotCompleted.
	 * 
	 * @throws EntityNotFoundException
	 * @throws BaseException
	 */
	@Test
	public void testOldSpringsClimbNoMatch() throws EntityNotFoundException, BaseException {
		ActivityId courseId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "302994809");
		ActivityId attemptId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "276339406");

		assertTiming(courseId, attemptId, null, MeasurementUtil.createRatio(.99));
	}

	/**
	 * Introduces a specific situation where the starting line interpolation causes minor overage.
	 * calculations. see https://aawhere.jira.com/browse/TN-652
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	@Category(ManualTest.class)
	public void testRatioError() throws EntityNotFoundException {

		ActivityTimingId timingId = new ActivityTimingId("gc-79452974-gc-436068398");
		assertTimingFailed(timingId);

	}

	/**
	 * The line segment misses the goal line, but barely intersects the goal polygon at it's end. A
	 * very extreme "edge" case, no pun intended.TN-652
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	@Category(ManualTest.class)
	public void testSegmentLineMiddleIntersectsBufferEnd() throws EntityNotFoundException {

		ActivityTimingId timingId = new ActivityTimingId("gc-3257769-gc-340715121");
		assertTiming(timingId, new Duration(4579000));

	}

	/**
	 * @param timingId
	 * @throws EntityNotFoundException
	 */
	private void assertTimingFailed(ActivityTimingId timingId) throws EntityNotFoundException {
		assertTiming(timingId, null);
	}

	/**
	 * https://aawhere.jira.com/browse/TN-233
	 * 
	 * @throws EntityNotFoundException
	 * @throws BaseException
	 */

	@Test
	@Ignore("2peak activities having problems")
	public void testAawhere233FalsePositive() throws EntityNotFoundException, BaseException {
		ActivityId courseId = new ActivityId(KnownApplication.TWO_PEAK_ACTIVITIES.key, "214478513");
		ActivityId attemptId = new ActivityId(KnownApplication.TWO_PEAK_ACTIVITIES.key, "214478509");

		assertTiming(courseId, attemptId, null, MeasurementUtil.createRatio(.98));
	}

	@Test
	@Ignore("2peak activities having problems")
	public void testAawhere274FinishTooStrict() throws EntityNotFoundException, BaseException {
		ActivityId courseId = new ActivityId(KnownApplication.TWO_PEAK_ACTIVITIES.key, "214432608");
		ActivityId attemptId = new ActivityId(KnownApplication.TWO_PEAK_ACTIVITIES.key, "213651177");

		assertTiming(courseId, attemptId, DurationUtil.durationFromMinutes(150), MeasurementUtil.createRatio(.98));
	}

	@Test
	public void testMarincelloMatch() throws EntityNotFoundException, BaseException {
		ActivityId courseId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "303046413");
		ActivityId attemptId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "276339406");

		assertTiming(courseId, attemptId, DurationUtil.durationFromMinutes(25), MeasurementUtil.createRatio(.99));
	}

	/**
	 * This has a start/finish very similar. The false start detector may take precedence over the
	 * finish detector.
	 * 
	 * @throws EntityNotFoundException
	 * @throws BaseException
	 */
	@Test
	public void testMissingFinishDetectionAawhere284() throws EntityNotFoundException, BaseException {
		ActivityId courseId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "332082104");
		ActivityId attemptId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "328710584");

		assertTiming(courseId, attemptId, DurationUtil.durationFromMinutes(120), MeasurementUtil.createRatio(.98));
	}

	@Test
	public void testMicroRaceDetected395() throws EntityNotFoundException {
		ActivityTimingId timingId = new ActivityTimingId("gc-3008996-gc-178154570");

		assertTiming(timingId, null);

	}

	/**
	 * Activiites with geocells stored of different resolutions should pass, but both the screening
	 * in memory and the datastore search must handle the different resolutions.
	 * 
	 * TN-383 and TN-465 explain the problem further
	 * 
	 * This test imports and verifies direct timing relation and then proves that searching will
	 * also find
	 * 
	 * @throws BaseException
	 */
	@Test
	@Ignore("TN-383")
	public void testDifferentResolutionsAawhere383And465() throws BaseException {
		ActivityTimingId timingId = new ActivityTimingId("2ps-124-2p-217808411");
		// this will now verify that direct relationships will pass
		assertTiming(timingId, DurationUtil.durationFromMinutes(20));

		// import, but don't relate....
		ActivityService activityService = testUtil.getImportTestUtil().getActivityServiceTestUtil()
				.getActivityService();
		Activity course = activityService.getActivity(timingId.getCourseId());
		Activity attempt = activityService.getActivity(timingId.getAttemptId());

		// search for matches via the contains filter. the attempt should contain the course
		Filter filter = activityService.getActivityFilterBuilder().contains(attempt.getTrackSummary()).build();

		Activities activities = activityService.getActivities(filter);
		TestUtil.assertContains(course, activities.all());

	}

	/**
	 * A specialty test of late finish detection due to the deviation proceeding past the finish and
	 * the course buffer sliding late marking the finish much later. The duration of 51 minutes
	 * chosen was observed from Google Earth as the ideal time. Previously it was coming in at 54
	 * minutes or more.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testLateFinish289() throws EntityNotFoundException {
		ActivityTimingId timingId = new ActivityTimingId("gc-331820148-gc-353176872");

		assertTiming(timingId, DurationUtil.durationFromMinutes(51));

	}

	@Test
	public void testNullPointer440() throws EntityNotFoundException {
		ActivityTimingId timingId = new ActivityTimingId("gc-336753753-gc-105560402");
		assertTiming(timingId, null);

	}

	/**
	 * Also related to TN-289 this crosses the finish area twice.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testLateFinish289FinishAreaTwice() throws EntityNotFoundException {
		ActivityTimingId timingId = new ActivityTimingId("gc-89921293-gc-145828656");

		assertTiming(timingId, DurationUtil.durationFromMinutes(112));

	}

	/**
	 * This was observed to not work properly when 289 fix was put in so it was added as another
	 * reality check requiring a positive finish.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testBaselGoodMatch() throws EntityNotFoundException {
		ActivityTimingId timingId = new ActivityTimingId("gc-332082104-gc-328710584");

		assertTiming(timingId, DurationUtil.durationFromMinutes(90));

	}

}
