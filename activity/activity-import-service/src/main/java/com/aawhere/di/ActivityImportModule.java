/**
 *
 */
package com.aawhere.di;

import com.aawhere.activity.ActivityImportFailureObjectifyRepository;
import com.aawhere.activity.ActivityImportMilestoneRepository;
import com.aawhere.activity.ApplicationActivityImportInstructionsRepository;
import com.aawhere.activity.ApplicationActivityUrlProvider;
import com.aawhere.app.account.AccountProvider;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

/**
 * @author brian
 * 
 */
public class ActivityImportModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(ActivityImportMilestoneRepository.class).to(ActivityImportFailureObjectifyRepository.class);
		bind(ApplicationActivityUrlProvider.class).to(ApplicationActivityImportInstructionsRepository.class);
		bind(AccountProvider.ProfilePage.class).to(ApplicationActivityImportInstructionsRepository.class);
	}

	@Provides
	public ApplicationActivityImportInstructionsRepository getActivityImportInstructionsRepository() {
		ApplicationActivityImportInstructionsRepository.Builder builder = new ApplicationActivityImportInstructionsRepository.Builder();
		builder.addInstructions(ApplicationActivityImportInstructionsRepository.garminConnectInstructions());
		builder.addInstructions(ApplicationActivityImportInstructionsRepository.twoPeakActivityInstructions());
		builder.addInstructions(ApplicationActivityImportInstructionsRepository.twoPeakSectionInstructions());
		builder.addInstructions(ApplicationActivityImportInstructionsRepository.everyTrailInstructions());
		builder.addInstructions(ApplicationActivityImportInstructionsRepository.mmfInstructions());
		builder.addInstructions(ApplicationActivityImportInstructionsRepository.rideWithGpsInstructions());
		builder.addInstructions(ApplicationActivityImportInstructionsRepository.googleIdentityToolkitInstructions());
		return builder.build();

	}

}
