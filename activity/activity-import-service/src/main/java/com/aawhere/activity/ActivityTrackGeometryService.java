/**
 * 
 */
package com.aawhere.activity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import com.aawhere.cache.Cache;
import com.aawhere.cache.Cache.Key;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.jts.geom.WktContentType;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.Track;
import com.aawhere.track.TrackService;

import com.google.common.base.Function;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Provides {@link Activity} and {@link Track} in the form of JTS {@link Geometry}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityTrackGeometryService {

	private TrackService trackService;
	private ActivityImportService activityImportService;

	@Inject
	public ActivityTrackGeometryService(TrackService trackService, ActivityImportService activityImportService) {
		this.trackService = trackService;
		this.activityImportService = activityImportService;
	}

	/**
	 * @see #getTrackAsMultiLineString(Activity)
	 * @param activityId
	 * @return
	 * @throws EntityNotFoundException
	 * @throws ActivityImportException
	 */
	public MultiLineString getTrackAsMultiLineString(ActivityId activityId) throws EntityNotFoundException,
			ActivityImportException {
		return getTrackAsMultiLineString(getActivity(activityId));
	}

	/**
	 * @param activityId
	 * @return
	 * @throws ActivityImportException
	 * @throws EntityNotFoundException
	 */
	private Activity getActivity(ActivityId activityId) throws ActivityImportException, EntityNotFoundException {
		return activityImportService.getActivity(activityId);
	}

	/**
	 * Provides a line string from th activity provided. Use this if you already loaded the Activity
	 * or use {@link #getTrackAsMultiLineString(ActivityId)} if not.
	 * 
	 * @param activity
	 * @return
	 */
	public MultiLineString getTrackAsMultiLineString(Activity activity) {

		try {
			return getTrackMultiLineString(activity.getTrackSummary().getDocumentId());
		} catch (EntityNotFoundException e) {
			throw ToRuntimeException.wrapAndThrow(e);
		}

	}

	/**
	 * @see #getTrackMultiLineString(ArchivedDocumentId, String)
	 * @param id
	 * @throws EntityNotFoundException
	 */
	public MultiLineString getTrackMultiLineString(ArchivedDocumentId id) throws EntityNotFoundException {
		String cacheKey = new StringBuilder(id.toString()).append("-")
				.append(WktContentType.MULTI_LINE_STRING_MEDIA_TYPE).toString();
		return getTrackMultiLineString(id, cacheKey);
	}

	@Cache
	MultiLineString getTrackMultiLineString(ArchivedDocumentId documentId, @Key String cacheKey)
			throws EntityNotFoundException {
		LoggerFactory.getLogger(getClass()).fine(cacheKey + " missed");
		MultiLineString geometry = JtsTrackGeomUtil.trackToMultiLineString(trackService.getTrack(documentId), true);
		// provide the cacheid for others to use downstream if necessary
		geometry.setUserData(documentId);
		return geometry;

	}

	/**
	 * Produces a geometry buffer for the track identified by the activity with the given width.
	 * This will use the cache if available.
	 * 
	 * @see JtsTrackGeomUtil#buffer(Geometry, Length)
	 * @param activityIdentifier
	 * @param bufferWidth
	 * @return
	 * @throws EntityNotFoundException
	 * @throws ActivityImportException
	 */
	@Nonnull
	public Geometry bufferedTrack(@Nonnull ActivityId activityId, @Nonnull Length bufferWidth)
			throws EntityNotFoundException, ActivityImportException {
		char separator = '-';
		String cacheKey = new StringBuilder(activityId.toString()).append(separator)
				.append((int) bufferWidth.doubleValue(MetricSystem.METRE)).append(separator)
				.append(WktContentType.POLYGON_MEDIA_TYPE).toString();
		return bufferedTrack(activityId, cacheKey, bufferWidth);
	}

	/**
	 * 
	 * @param activity
	 * @param cacheKey
	 * @param geometry
	 *            optional parameter if it has already been retrieved.
	 * @return
	 * @throws ActivityImportException
	 * @throws EntityNotFoundException
	 */
	@Cache
	/* package required for cache */Geometry bufferedTrack(ActivityId activityId, @Key String cacheKey,
			@Nonnull Length bufferWidth) throws EntityNotFoundException, ActivityImportException {
		MultiLineString line = getTrackAsMultiLineString(activityId);
		Geometry result = JtsTrackGeomUtil.buffer(line, bufferWidth);

		return result;
	}

	/**
	 * Provides the geometry from the track summary of the activity identified.
	 * 
	 * @return
	 */
	public Function<ActivityId, MultiLineString> multiLineStringFunction() {
		return new Function<ActivityId, MultiLineString>() {

			@Override
			@Nullable
			public MultiLineString apply(@Nullable ActivityId input) {
				try {
					return getTrackAsMultiLineString(input);
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/**
	 * Given the buffer width this will return a function that translates the {@link ActivityId}
	 * into the buffer with the desired width.
	 * 
	 * @param bufferWidth
	 * @return
	 */
	public Function<ActivityId, Geometry> bufferFunction(final Length bufferWidth) {
		return new Function<ActivityId, Geometry>() {

			@Override
			@Nullable
			public Geometry apply(@Nullable ActivityId input) {
				try {
					return bufferedTrack(input, bufferWidth);
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

}
