/**
 *
 */
package com.aawhere.activity;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.activity.ActivityImportFailure.ActivityImportFailureId;
import com.aawhere.activity.ActivityImportFailure.Builder;
import com.aawhere.activity.ApplicationActivityImportInstructions.Destination;
import com.aawhere.activity.ImportMilestone.Status;
import com.aawhere.activity.imprt.mmf.EmbeddedCollection;
import com.aawhere.app.AccountDocument;
import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountService;
import com.aawhere.app.account.AccountWebApiUri;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.Documents;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.io.ConnectionTimeoutException;
import com.aawhere.io.ResourceGoneException;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsExploder;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.person.Person;
import com.aawhere.person.PersonService;
import com.aawhere.queue.BatchQueue;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Method;
import com.aawhere.queue.WebApiEndPoint;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.retrieve.ModifiableDocumentRetriever;
import com.aawhere.track.Track;
import com.aawhere.track.TrackContainer;
import com.aawhere.track.TrackIncapableDocumentException;
import com.aawhere.track.TrackService;
import com.aawhere.track.TrackSummary;
import com.aawhere.util.rb.StatusMessage;
import com.aawhere.util.rb.StatusMessages;
import com.aawhere.util.rb.StringMessage;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * In charge of importing {@link Activities} from {@link Application}s by downloading
 * {@link Document}s, parsing them, creating activities and {@link TrackSummaries} and even
 * {@link Person}s.
 * 
 * This is also responsible for the workflow beyond the creation of an activity such as managing
 * import status related to creating relationships and route processing. This will protect other
 * services from doing work if their import history indicates previous failures.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityImportService
		extends EntityService<ActivityImportFailures, ActivityImportFailure, ActivityImportFailureId> {

	static final Integer CELL_EXPLODE_LEVEL_MAX = 4;
	private ActivityImportMilestoneRepository activityImportRepository;
	private QueueFactory queueFactory;
	private ActivityService activityService;
	private DocumentService documentService;
	private TrackService trackService;
	private PersonService personService;
	private AccountService accountService;
	private DocumentFactory documentFactory;

	private ApplicationActivityImportInstructionsRepository knownInstructions;
	private static Logger LOG = LoggerFactory.getLogger(ActivityImportService.class);

	@Inject
	public ActivityImportService(ActivityImportMilestoneRepository repository,
			ApplicationActivityImportInstructionsRepository knownInstructions, QueueFactory queueFactory,
			ActivityService activityService, DocumentService documentService, DocumentFactory documentFactory,
			TrackService trackService, PersonService personService) {
		super(repository);
		this.activityImportRepository = repository;
		this.queueFactory = queueFactory;
		this.activityService = activityService;
		this.documentService = documentService;
		this.documentFactory = documentFactory;
		this.trackService = trackService;
		this.personService = personService;
		this.accountService = this.personService.accountService();
		this.knownInstructions = knownInstructions;
	}

	/**
	 * Retrieve the {@link ActivityImportFailures} for the supplied {@link ActivityId}.
	 * 
	 * @param id
	 * @return
	 */
	public ActivityImportFailures getActivityHistory(ActivityId id) {
		return activityImportRepository.filter(new ActivityImportFailureFilterBuilder().equalsActivityId(id)
				.setLimitToMax().build());
	}

	public Function<ActivityId, Activity> activityProvider() {
		return new Function<ActivityId, Activity>() {
			/*
			 * (non-Javadoc)
			 * @see com.google.common.base.Function#apply(java.lang.Object)
			 */
			@Override
			@Nullable
			public Activity apply(@Nullable ActivityId input) {
				try {
					return getActivity(input);
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/**
	 * Asynchronously requests that an activity is imported. This will not import the activity again
	 * if it is already imported.
	 * 
	 * @throws UnknownApplicationException
	 * 
	 * @see #importActivities(Iterator) for the actual implementation
	 * @see #getActivity(ApplicationActivityId) if you want the activity now.
	 * 
	 * */
	public ActivityImportFailure importActivity(ActivityId id) {
		return importActivities(Lists.newArrayList(id)).getCollection().get(0);
	}

	/**
	 * Given an {@link ActivityId} this will attempt an update of that Activity. If not already
	 * imported it will attempt the original import since there could be race conditions with
	 * asynchronous requests. If already imported, but the document is not modifiable then this will
	 * simply return the old version without attempting an update.
	 * 
	 * If a remote call is made and it is determined nothing has changed then an exception is thrown
	 * explaining the problem.
	 * 
	 * @param id
	 * @return
	 * @throws BaseException
	 */
	public Activity importOrUpdate(ActivityId id) throws BaseException {
		Activity result;
		try {
			Activity found = activityService.getActivity(id);
			// throws Entity not found in which case we should import, not update
			ApplicationActivityImportInstructions instructions = importInstructionsFor(id);
			if (instructions.hasModifiableDocuments()) {
				result = importActivityNow(id);
			} else {
				// just return the found activity with no update attempt.
				result = found;
			}
		} catch (EntityNotFoundException notYetCreated) {
			result = getActivity(id);
		}

		return result;

	}

	/**
	 * Calls {@link #importOrUpdate(ActivityId)}, but wrapps any exception in
	 * {@link ActivityImportException} for workflow reporting.
	 * 
	 * This separation is intended to allow {@link #importOrUpdate(ActivityId)} to provide better
	 * reporting so this may cater to task queues.
	 * 
	 * @param id
	 * @return
	 * @throws ActivityImportException
	 */
	public Activity updateActivityForWorkflow(ActivityId id) throws ActivityImportException {
		try {
			return importOrUpdate(id);
		} catch (Throwable e) {
			// purposely avoiding milestones...using log instead.
			LoggerFactory.getLogger(getClass()).warning(id.toString() + " failed to update because " + e.getMessage());
			throw ActivityImportException.create().setCause(e).setActivityId(id).build();
		}

	}

	/**
	 * @see #importActivities(Iterator)
	 * @param ids
	 * @throws UnknownApplicationException
	 */
	public ActivityImportFailures importActivities(Iterable<ActivityId> ids) {
		return importActivities(ids.iterator());
	}

	/**
	 * Asynchronously imports all the activities given in the {@link EmbeddedCollection} of
	 * {@link ApplicationActivityId}s. Each activity will be imported in a separate Task.
	 * 
	 * @param ids
	 * @throws UnknownApplicationException
	 */
	public ActivityImportFailures importActivities(Iterator<ActivityId> ids) {
		ActivityImportFailures.Builder milestones = new ActivityImportFailures.Builder();
		while (ids.hasNext()) {
			ActivityId id = ids.next();
			if (!KnownApplication.exists(id.getApplicationKey())) {
				throw new UnknownApplicationException(id.getApplicationKey());
			}
			// the known applications may provide an Unknwon as the default.
			// do a check here to avoid queue something that will fail.

			if (activityService.exists(id)) {
				milestones.add(queueActivityForUpdate(id));
			} else {
				// avoid re-imports here.
				// TODO:allow a way to force re-import
				// NOTE: If bulk imports become common this would be faster to do a bulk find
				ActivityImportFailures history = findByActivityId(id);
				if (history.isEmpty()) {
					// this throws Unknown Application
					KnownApplication.byKey(id.getApplicationKey());
					ActivityImportFailure milestone = queueActivityForImport(id);
					milestones.add(milestone);
				} else {
					// history is not empty so there was a previous error. explain.
					milestones.add(history);

				}
			}
		}
		return milestones.build();
	}

	/**
	 * @param id
	 * @return
	 */
	public ActivityImportFailure queueActivityForUpdate(ActivityId id) {
		return queueActivityForImport(id, ActivityWebApiUri.ACTIVITY_IMPORT_METHOD_FOR_UPDATE);
	}

	/**
	 * Sends the {@link ApplicationActivityId} to the coordQueue to be asynchronously imported.
	 * 
	 * This provides the ability to use a provided coordQueue.
	 * 
	 * @param coordQueue
	 * @param id
	 * @return
	 */
	public ActivityImportFailure queueActivityForImport(ActivityId id) {
		// bypass since these are live customers with one offs, not the queue.
		final Method method = ActivityWebApiUri.ACTIVITY_IMPORT_METHOD_FOR_BYPASS;
		ActivityImportFailure milestone = queueActivityForImport(id, method);
		return milestone;
	}

	/**
	 * @param id
	 * @param method
	 * @return
	 */
	private ActivityImportFailure queueActivityForImport(ActivityId id, final Method method) {
		Queue queue = getActivityImportQueue();
		Task task = activityImportTask(id, method);
		queue.add(task);
		ActivityImportFailure milestone = milestoneBuilder(id).setStatus(Status.QUEUED).build();
		return milestone;
	}

	/**
	 * @param id
	 * @param method
	 * @return
	 */
	private Task activityImportTask(ActivityId id, final Method method) {
		String path = ActivityWebApiUri.getActivityByApplicationId(id).toString();
		WebApiEndPoint endPoint;
		try {
			endPoint = (new WebApiEndPoint.Builder()).setEndPoint(path).build();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
		Task task = Task.create().setEndPoint(endPoint).setMethod(method).setAllowRepeats(false).build();
		return task;
	}

	/**
	 * Made available to friendlies.
	 * 
	 * @return
	 */
	/* package */Queue getActivityImportQueue() {
		return queueFactory.getQueue(ActivityWebApiUri.ACTIVITY_IMPORT_QUEUE_NAME);
	}

	/**
	 * Creates the given {@link ActivityImportFailure} in the system for a record of an import
	 * attempt and its results.
	 * 
	 * @param activityImport
	 * @return
	 */
	public ActivityImportFailure create(ActivityImportFailure activityImport) {

		Logger logger = LoggerFactory.getLogger(getClass());
		// only store failed status - TN-378
		if (ImportMilestone.Status.FAILED.equals(activityImport.getStatus())) {
			activityImport = this.activityImportRepository.store(activityImport);
			logger.warning(activityImport.getActivityId() + " failed because " + activityImport.getCode());
		} else {
			logger.info(activityImport.getActivityId() + " " + activityImport.getStatus() + " : "
					+ activityImport.getDetails());
		}

		return activityImport;
	}

	/**
	 * Removes an import from the datastore when providing the id.
	 * 
	 * @param id
	 */
	public void delete(ActivityImportFailureId id) {
		activityImportRepository.delete(id);
	}

	/**
	 * @param milestoneIds
	 */
	public void delete(Iterable<ActivityImportFailureId> milestoneIds) {
		activityImportRepository.deleteAllFromIds(milestoneIds);
	}

	public ActivityImportFailures filter(Filter filter) {
		return this.activityImportRepository.filter(filter);
	}

	/**
	 * Finds all history related to the {@link ActivityId} regardless of scope.
	 * 
	 * @param activityId
	 * @return
	 */
	public ActivityImportFailures findByActivityId(ActivityId activityId) {
		return filter(new ActivityImportFailureFilterBuilder().equalsActivityId(activityId).build());
	}

	/**
	 * The appropriate update when the Activity has been successfully imported and the Activity ID
	 * is available to tie them together.
	 * 
	 * @param applicationActivityId
	 * @param id
	 * @return
	 */
	private ActivityImportFailure activityImportSucceeded(ActivityId id) {
		return milestoneBuilder(id).setStatus(Status.SUCCEEDED).build();

	}

	/**
	 * @param id
	 * @return
	 */
	private Builder milestoneBuilder(ActivityId id) {
		return new ActivityImportFailure.Builder().setActivityId(id);
	}

	/**
	 * An unfortunate report that the attempted import did not succeed and the reason explaining
	 * why.
	 * 
	 * @param activityId
	 * @param errorDuringImport
	 * @return
	 */
	private ActivityImportException.Builder activityImportFailed(ActivityId activityId, Throwable cause) {
		create(milestoneBuilder(activityId).setStatus(Status.FAILED).setCause(cause).build());
		final Logger logger = LoggerFactory.getLogger(getClass());
		logger.warning(activityId + " failed to import because " + cause.getMessage());
		logger.throwing("importActivityNow", activityId + " failed to import", cause);
		return new ActivityImportException.Builder().setActivityId(activityId).setCause(cause);

	}

	public Activity getActivity(ActivityId activityId) throws ActivityImportException, EntityNotFoundException {

		final Activity activity = getActivity(activityId, false);
		return activity;
	}

	/**
	 * Synchronously retrieves the {@link Activity} from the {@link Application}, calculating
	 * supporting information and storing it in the system. If the activity already exists in the
	 * system this will use only the local version and make no attempt to retrieve the activity from
	 * the source Application again unlessed forced.
	 * 
	 * 
	 * @param activityId
	 * @param force
	 *            to bypass import checks and force and import
	 * @return
	 * @throws ActivityImportException
	 * @throws EntityNotFoundException
	 * @throws BaseException
	 */
	public Activity getActivity(ActivityId activityId, Boolean force) throws ActivityImportException,
			EntityNotFoundException {
		Activity activity;
		try {
			activity = activityService.getActivity(activityId);
		} catch (EntityNotFoundException e) {

			// we can't import activities from us
			if (ActivityIdentifierUtil.appIsUs(activityId)) {
				throw e;
			}
			// make sure previous import attempts have not been made...if so the
			// exit
			ActivityImportFailures history = findByActivityId(activityId);
			// no history means go for it.
			// any history should not be erroneous for another attempt
			if (history.isEmpty() || history.isQueuedForImport() || force) {

				try {
					activity = importActivityNow(activityId);
					activityImportSucceeded(activityId);

				} catch (BaseException errorDuringImport) {
					if (errorDuringImport instanceof ConnectionTimeoutException) {
						// pass this one through for a retry: TN-451
						ToRuntimeException.wrapAndThrow(errorDuringImport);
					}
					throw activityImportFailed(activityId, errorDuringImport).build();

				} catch (BaseRuntimeException errorDuringImport) {
					throw activityImportFailed(activityId, errorDuringImport).build();
				} catch (Throwable mishandledErrorDuringImport) {
					throw activityImportFailed(activityId, mishandledErrorDuringImport).generalError().build();
				}

			} else {
				throw new ActivityImportException.Builder().setActivityId(activityId).previousImportFailed().build();
			}

		}
		return activity;
	}

	/**
	 * When the activity already exists, but there needs to be a re-process this will go through the
	 * original import
	 * 
	 * 
	 * @param activityId
	 * @return
	 * @throws BaseException
	 */
	public Activity updateActivity(ActivityId activityId) throws BaseException {
		return importActivityNow(activityId);
	}

	/** Queues an activity update for all those activities that match the given fitler. */
	public Integer updateActivities(Filter filter) {

		Iterable<ActivityId> idPaginator = activityService.idPaginator(filter);
		BatchQueue queue = BatchQueue.create().setQueue(getActivityImportQueue()).build();
		for (ActivityId activityId : idPaginator) {
			Task task = ActivityWebApiUri.activityUpdated(activityId);
			queue.add(task);
		}
		return queue.finish();
	}

	/**
	 * The primary workflow once an activity has been decided that an import should be attempted.
	 * 
	 * This is protected since this class has checks to make sure a re-import is not attempted.
	 * 
	 * @param activityId
	 * @return
	 * @throws BaseException
	 */
	private Activity importActivityNow(ActivityId activityId) throws BaseException {
		// o.k., fine. We don't yet have it. Let's go to the application.
		Application application = KnownApplication.byKey(activityId.getApplicationKey());
		ApplicationActivityImportInstructions instructions = knownInstructions.instructionsFor(application);

		Documents documents;
		try {
			documents = retrieveDocuments(activityId, application, instructions);

			ActivityDocument activityDocument = documents.getDocument(ActivityDocument.class);
			ArchivedDocument trackContainerArchive = documents.getArchivedDocument(TrackContainer.class);

			TrackSummary trackSummary = createTrackSummary(trackContainerArchive, activityId);

			final Activity activity = createActivity(activityDocument, activityId, trackSummary, instructions);
			workflowAfterActivityCreate(activity);

			return activity;

		} catch (ResourceGoneException gone) {
			return activityService.activityIsGone(activityId);
		}

	}

	public ApplicationActivityImportInstructions importInstructionsFor(ActivityId activityId)
			throws ApplicationImportInstructionsNotFoundException {
		Application application = KnownApplication.byKey(activityId.getApplicationKey());
		ApplicationActivityImportInstructions instructions = knownInstructions.instructionsFor(application);
		return instructions;
	}

	/**
	 * @param activity
	 */
	private void workflowAfterActivityCreate(Activity activity) {

		if (!activity.isGone()) {
			// TN-300

			/*
			 * TN-787 temporarily disable timing process after activity import.
			 * ActivityTimingRelationWebApiUri.queueToCreateTimings(activity.getId(), queueFactory);
			 */
			// TN-446,TN-814 leaderboards, now route completions, finds good course candidates
			// TN-871 moving this to map reduce route focused matching
			// queueFactory.getQueue(ActivityTimingRelationWebApiUri.TIMINGS_QUEUE_NAME)
			// .add(RouteWebApiUri.routesTimingsForAttemptTask(activity.getId()));
		}
	}

	/**
	 * @param activityDocument
	 * @param activityId
	 * @param trackSummary
	 * @param person
	 * @return
	 */
	private Activity createActivity(ActivityDocument activityDocument, ActivityId activityId,
			TrackSummary trackSummary, ApplicationActivityImportInstructions instructions) {
		Activity.Builder activityBuilder;

		try {
			activityBuilder = Activity.mutate(activityService.getActivity(activityId));
		} catch (EntityNotFoundException e) {
			activityBuilder = Activity.create();
			activityBuilder.setId(activityId);
		}
		ActivityType activityType = instructions.activityType(activityDocument.getActivityType());
		activityBuilder.setActivityType(activityType);
		activityBuilder.setDescription(activityDocument.getDescription());
		activityBuilder.setHistory(activityDocument.getHistory());
		// final
		String activityName = activityDocument.getActivityName();
		if (activityName != null) {
			activityBuilder.setName(new StringMessage(activityName));
		}// leave null otherwise

		final Account accountDetails = activityDocument.getAccount();
		final AccountId accountId = accountDetails.id();
		// this finds/creates the account and person together
		Pair<Person, Account> personAccount = this.personService.person(accountDetails);
		// only account is appropriate to store with activity since person may change
		Account account = personAccount.getRight();
		if (account.getAliases().isEmpty() && this.knownInstructions.accountUpdatable(accountId)) {
			try {
				account = accountUpdated(accountId);
			} catch (BaseException e) {
				// Bummer, but that shouldn't ruin the import.
				LOG.warning("unable to update account aliases for " + accountId + " because " + e.getMessage());
			}
		}
		activityBuilder.setAccount(account);
		activityBuilder.setTrackSummary(trackSummary);
		return activityService.create(activityBuilder.build());
	}

	/**
	 * Actually retrieves the documents from their remote locations.
	 * 
	 * TODO:This is too specific to one case. It must be made more general to handle other types of
	 * retrievals.
	 * 
	 * @param applicationActivityId
	 * @param instructions
	 * @param application
	 * @return
	 * @throws UnknownApplicationException
	 * @throws BaseException
	 */

	private Documents retrieveDocuments(ActivityId applicationActivityId, Application application,
			ApplicationActivityImportInstructions instructions) throws BaseException {

		Documents.Builder documentsBuilder = new Documents.Builder();
		List<Destination> destinations = instructions.getActivityUrls();
		for (Destination destination : destinations) {
			ArchivedDocument archivedDocument;
			try {
				// get it from cache if possible
				archivedDocument = documentService.getArchivedDocument(applicationActivityId, destination.format);
				// it might need to be updated
				if (instructions.hasModifiableDocuments()) {
					DocumentRetriever retriever = this.knownInstructions
							.createActivityRetriever(	destination,
														applicationActivityId,
														this.documentFactory,
														instructions);

					if (retriever instanceof ModifiableDocumentRetriever) {
						((ModifiableDocumentRetriever) retriever).retrieve(	destination.format,
																			archivedDocument.getDateUpdated());
						archivedDocument = documentService.update(	archivedDocument.getId(),
																	retriever.getDocument(),
																	retriever.getOrigionalData());

					} else {
						throw new UnsupportedOperationException(
								instructions
										+ " for "
										+ application
										+ " says it is modifiable, but it's retriever does not implement ModifiableDocumentRetriever"
										+ retriever);
					}

				}
			} catch (EntityNotFoundException notYetStored) {
				// not yet stored, let's go get it.
				DocumentRetriever retriever = this.knownInstructions.createActivityRetriever(	destination,
																								applicationActivityId,
																								this.documentFactory,
																								instructions);
				retriever.retrieve(destination.format);

				archivedDocument = documentService.createDocument(	retriever.getDocument(),
																	applicationActivityId,
																	retriever.getOrigionalData());
			}
			documentsBuilder.addDocument(archivedDocument);
		}

		return documentsBuilder.build();
	}

	/**
	 * Gets the GPX from Garmin Connect, stores the document, creates the {@link Track} and
	 * {@link TrackSummary}.
	 * 
	 * @param applicationActivityId
	 * @throws EntityNotFoundException
	 * @throws BaseException
	 */
	private TrackSummary createTrackSummary(ArchivedDocument trackContainerArchive, ActivityId applicationActivityId)
			throws BaseException {

		Document doc = trackContainerArchive.getDocument();
		if (doc instanceof TrackContainer) {
			ArchivedDocumentId archivedDocumentId = trackContainerArchive.getId();
			Track track = ((TrackContainer) doc).getTrack();
			if (track == null) {
				throw new TrackIncapableDocumentException(archivedDocumentId);
			}
			Pair<TrackSummary, StatusMessages> summaryCreationResult = trackService
					.createAndPersistTrackSummary(archivedDocumentId, track);
			StatusMessages trackSummaryMessages = summaryCreationResult.getRight();
			for (StatusMessage statusMessage : trackSummaryMessages) {
				ActivityId activityId = applicationActivityId;
				ActivityImportFailure trackSummaryMilestone = milestoneBuilder(activityId).setStatus(Status.PROCESSED)
						.setStatusMessage(statusMessage).build();
				create(trackSummaryMilestone);
			}
			return summaryCreationResult.getLeft();
		} else {
			throw new TrackIncapableDocumentException(trackContainerArchive.getId());
		}
	}

	/**
	 * Given the {@link ApplicationKey} this will return the instructions expected for that
	 * application.
	 * 
	 * @param applicationKey
	 * @return
	 * @throws ApplicationImportInstructionsNotFoundException
	 *             when not provided. Clients may convert this to a runtime since they may be beyond
	 *             the point where this should have been reported already.
	 */
	public ApplicationActivityImportInstructions getInstructions(ApplicationKey applicationKey)
			throws ApplicationImportInstructionsNotFoundException {
		return this.knownInstructions.instructionsFor(applicationKey);
	}

	/**
	 * Retrieves the remote information about the account and updates the {@link AccountService}
	 * accordingly.
	 * 
	 * @param accountId
	 * @return
	 * @throws BaseException
	 */
	public Account accountUpdated(AccountId accountId) throws BaseException {
		final ApplicationKey applicationKey = accountId.getApplicationKey();
		ApplicationActivityImportInstructions instructions = this.knownInstructions.instructionsFor(applicationKey);
		final Destination accountDestination = instructions.accountDestination();
		if (accountDestination != null) {

			DocumentRetriever retriever = knownInstructions.createAccountRetriever(	accountDestination,
																					accountId,
																					documentFactory,
																					instructions);
			retriever.retrieve(accountDestination.format);
			AccountDocument document = (AccountDocument) retriever.getDocument();
			Account updatedAccount = accountService.accountUpdated(accountId, document.name(), document.username());
			// give person a nudge since the account may be updated
			personService.person(updatedAccount);
			return updatedAccount;
		} else {
			accountService.updateNotPossible(accountId);
			throw ApplicationImportInstructionsNotFoundException.forQueue(applicationKey);
		}
	}

	/**
	 * Crawls providers by account id.
	 * 
	 * @see #activitiesCrawled(ApplicationKey, GeoCells, AccountId, Filter)
	 * */
	public AccountImportManager activitiesCrawled(final AccountId accountId, Filter filter) throws BaseException {

		Function<ActivityImportCrawlerDestination, DocumentRetriever> retrieverFunction = ActivityImportUtil
				.webDocumentRetrieverFunction(this.documentFactory);

		Supplier<Activities> mostRecentActivitiesSupplier = mostRecentActivitiesFunction(accountId, 1);

		// This reports the entity not found, but also finds by alias since import can't use aliases
		AccountId accountIdVerified;

		try {
			// may be an alias...
			Account account = accountService.account(accountId);

			accountIdVerified = account.id();
		} catch (EntityNotFoundException e1) {
			// maybe it's new so keep going
			accountIdVerified = accountId;
		}

		AccountImportManager accountImportManager = AccountImportManager.create().accountId(accountIdVerified)
				.filter(filter).queueFactory(queueFactory).retrieverFunction(retrieverFunction)
				.instructionsRepository(knownInstructions).nonExistingIdsFunction(nonExistingNorFailingIdsFunction())
				.mostRecentActivities(mostRecentActivitiesSupplier).build();
		return accountImportManager;
	}

	/**
	 * @see #nonExistingNorFailingIds(Set)
	 * @return
	 */
	public Function<Set<ActivityId>, Iterable<ActivityId>> nonExistingNorFailingIdsFunction() {
		return new Function<Set<ActivityId>, Iterable<ActivityId>>() {

			@Override
			public Iterable<ActivityId> apply(Set<ActivityId> input) {
				return nonExistingNorFailingIds(input);
			}

		};
	}

	/**
	 * Given a set of ids, this will look for activities of the given set that are non existing in
	 * the datastore. Then it will confirm of those non existing that non are in the failure
	 * datastore.
	 * 
	 * @param input
	 * @return
	 */
	public Iterable<ActivityId> nonExistingNorFailingIds(Set<ActivityId> input) {
		// look for those non existing activities
		Iterable<ActivityId> nonExistingActivities = activityService.nonExisting(input);
		// then those that may have failed
		Iterable<ActivityImportFailureId> activityImportFailureIds = ActivityImportUtil
				.activityImportFailureIds(nonExistingActivities);
		Iterable<ActivityImportFailureId> nonExistingFailureIds = nonExisting(SetUtilsExtended
				.asSet(activityImportFailureIds));
		// conver back to activity ids
		return ActivityImportUtil.activityIds(nonExistingFailureIds);
	}

	/**
	 * @param accountId
	 * @param limit
	 * @return
	 */
	private Supplier<Activities> mostRecentActivitiesFunction(final AccountId accountId, final int limit) {
		return new Supplier<Activities>() {

			@Override
			public Activities get() {
				try {
					return activityService.activities(accountId, Filter.create().setLimit(limit).build());
				} catch (EntityNotFoundException e) {
					if (LOG.isLoggable(Level.INFO)) {
						LOG.info(accountId + " has not been imported yet.");
					}
					return null;
				}
			}
		};
	}

	/**
	 * @see #activitiesCrawledForAccounts(Iterable, Filter)
	 * 
	 * @param filter
	 * @return
	 */
	public Integer activitiesCrawledForAccounts(Filter filter) {
		return ActivityImportUtil.crawlTasksForAccounts(accountService.ids(filter), filter, queueFactory);
	}

	/**
	 * Given the accountIds this will post tasks for each of them.
	 * 
	 * @param accountIds
	 * @param filter
	 * @return
	 */
	public Integer activitiesCrawledForAccounts(Iterable<AccountId> accountIds, Filter filter) {
		return ActivityImportUtil.crawlTasksForAccounts(accountIds, filter, queueFactory);
	}

	/**
	 * Queues the first page of all geocells at the resolution provided. So if high lower resolution
	 * cells are provided than those cells given this will explode the cells to the resolution and
	 * post a task for each cell. A limit of exploded 4 levels is enforced to avoid excessive
	 * amounts of tasks being posted at once.
	 * 
	 * @param applicationKey
	 * @param geoCells
	 * @param explodeTo
	 * @return
	 * @throws ApplicationImportInstructionsNotFoundException
	 */
	public List<String> cellCrawlerTasksQueued(@Nonnull ApplicationKey applicationKey, @Nonnull GeoCells geoCells,
			@Nullable Resolution explodeTo) throws ApplicationImportInstructionsNotFoundException {
		LinkedList<String> uris = new LinkedList<>();
		// just validate there are import instructions
		ApplicationActivityImportInstructions instructionsFor = this.knownInstructions.instructionsFor(applicationKey);
		if (instructionsFor.searchDestination() == null) {
			throw new ApplicationImportInstructionsNotFoundException(applicationKey);
		}

		int max = (int) Math.pow(GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL, CELL_EXPLODE_LEVEL_MAX);
		GeoCells exploded = GeoCellsExploder.explode().these(geoCells).to(explodeTo).exploded();
		// throws if exceeded.
		InvalidArgumentException.limitExceeded(max, exploded.size());

		BatchQueue queue = BatchQueue.create().setQueue(getCrawlQueue(applicationKey)).build();

		for (GeoCell geoCell : exploded) {
			Task task = ActivityWebApiUri.applicationCellsEndpoint(	applicationKey.getValue(),
																	geoCell.getCellAsString(),
																	1,
																	null);
			uris.add(task.getEndPoint().toString());
			queue.add(task);
		}
		queue.finish();
		return uris;
	}

	public ActivityImportCrawler activitiesCrawled(ApplicationKey applicationKey, GeoCells cells, Filter filter)
			throws BaseException {

		ActivityImportCrawlerDestination destination = ActivityImportCrawlerDestination.create().cells(cells)
				.applicationKey(applicationKey).filter(filter).instructions(getInstructions(applicationKey)).build();

		Queue crawlQueue = getCrawlQueue(applicationKey);
		// we aren't going to check for existing activities.
		ActivityImportCrawler crawler = ActivityImportCrawler.create()
				.retriever(ActivityImportCrawler.createWebDocumentRetriever(destination, documentFactory))
				.deep(ActivityImportUtil.deep(filter)).crawlerDestination(destination).crawlQueue(crawlQueue).crawl();
		return crawler;
	}

	/**
	 * @return
	 */
	Queue getCrawlQueue(ApplicationKey applicationKey) {
		return AccountWebApiUri.getCrawlQueue(queueFactory, applicationKey);
	}

	/**
	 * @return the activityService
	 */
	public ActivityService getActivityService() {
		return this.activityService;
	}

}
