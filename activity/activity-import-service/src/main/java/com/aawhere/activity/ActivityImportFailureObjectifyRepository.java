/**
 *
 */
package com.aawhere.activity;

import com.aawhere.activity.ActivityImportFailure.ActivityImportFailureId;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.persist.ObjectifyFilterRepositoryDelegate;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * {@link ObjectifyFilterRepositoryDelegate} implementation for
 * {@link ActivityImportHistoryRepository}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityImportFailureObjectifyRepository
		extends
		ObjectifyFilterRepository<ActivityImportFailure, ActivityImportFailureId, ActivityImportFailures, ActivityImportFailures.Builder>
		implements ActivityImportMilestoneRepository {

	static {
		initializeObjectify();
	}

	/**
	 * 
	 */
	static void initializeObjectify() {
		// ObjectifyService.factory().getTranslators()
		// .add(new
		// FromStringTranslatorFactory<ActivityImportProcessorVersion>(ActivityImportProcessorVersion.class));
		ObjectifyService.register(ActivityImportFailure.class);
	}

	@Inject
	public ActivityImportFailureObjectifyRepository(ActivityImportFailureObjectifyFilterRepository filterRepository) {
		super(filterRepository, ObjectifyRepository.SYNCHRONIZATION_DEFAULT);
	}

	public ActivityImportFailureObjectifyRepository(ActivityImportFailureObjectifyFilterRepository filterRepository,
			Synchronization synchronization) {
		super(filterRepository, synchronization);
	}

}
