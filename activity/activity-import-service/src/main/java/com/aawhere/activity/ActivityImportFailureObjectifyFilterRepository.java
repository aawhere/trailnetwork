/**
 *
 */
package com.aawhere.activity;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * The filter implementation for {@link ActivityImportFailures}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ActivityImportFailureObjectifyFilterRepository
		extends
		ObjectifyFilterRepository<ActivityImportFailure, ActivityImportFailure.ActivityImportFailureId, ActivityImportFailures, ActivityImportFailures.Builder> {

	/**
	 * @param fieldDictionaryFactory
	 */
	@Inject
	public ActivityImportFailureObjectifyFilterRepository(FieldDictionaryFactory fieldDictionaryFactory) {
		super(fieldDictionaryFactory);
	}

}
