/**
 *
 */
package com.aawhere.activity;

import java.util.Iterator;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aawhere.activity.ApplicationActivityImportInstructions.Destination;
import com.aawhere.activity.ImportMilestone.Status;
import com.aawhere.activity.imprt.csv.CsvActivityDocumentProducer;
import com.aawhere.activity.imprt.everytrail.EveryTrailDocumentUtil;
import com.aawhere.activity.imprt.mmf.MapMyFitnessDocumentProducer;
import com.aawhere.activity.imprt.rwgps.RWGpsDocumentProducers;
import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.DeviceCapabilitiesRepository;
import com.aawhere.app.IdentityHistory;
import com.aawhere.app.Installation;
import com.aawhere.app.Instance;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.Account;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.garmin.activity.GarminConnectActivitySearchDocumentProducer;
import com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer;
import com.aawhere.doc.track.gpx._1._1.GpxxDocumentProducer;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;
import com.aawhere.person.PersonServiceTestUtil;
import com.aawhere.queue.GaeQueueTestUtil;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.QueueTestUtil;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.retrieve.ModifiableDocumentRetriever;
import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;
import com.aawhere.track.TrackContainer;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

/**
 * Configures {@link ActivityImportService} and the services it is dependent upon.
 * 
 * Call {@link Builder#getActivityServiceTestUtilBuilder()} to configure dependents.
 * 
 * @author Brian Chapman
 * 
 */
public class ActivityImportServiceTestUtil {

	private ActivityImportService activityImportService;
	private ActivityImportMilestoneRepository activityImportMilestoneRepository;
	private QueueFactory queueFactory;
	private ApplicationActivityImportInstructionsRepository importInstructionsRepo;
	private ActivityServiceTestUtil activityServiceTestUtil;
	private PersonServiceTestUtil personServiceTestUtil;
	private MockActivityDocumentProducer documentProducer;
	private Iterator<MockDocumentRetriever> activityCreatorIterator;
	private ActivityImportFailureObjectifyFilterRepository activityImportFilterRepository;
	private ActivityTrackGeometryService trackGeometryService;
	private ExampleTracksImportServiceTestUtil examplesUtil;

	/**
	 * @return the activityImportService
	 */
	public ActivityImportService getActivityImportService() {
		return this.activityImportService;
	}

	/**
	 * @return the importInstructionsRepo
	 */
	public ApplicationActivityImportInstructionsRepository getImportInstructionsRepo() {
		return this.importInstructionsRepo;
	}

	/**
	 * @return the activityServiceTestUtil
	 */
	public ActivityServiceTestUtil getActivityServiceTestUtil() {
		return this.activityServiceTestUtil;
	}

	/**
	 * @return the trackGeometryService
	 */
	public ActivityTrackGeometryService getTrackGeometryService() {
		return this.trackGeometryService;
	}

	/**
	 * @return the personServiceTestUtil
	 */
	public PersonServiceTestUtil getPersonServiceTestUtil() {
		return this.personServiceTestUtil;
	}

	/**
	 * @return the activityImportMilestoneRepository
	 */
	public ActivityImportMilestoneRepository getActivityImportMilestoneRepository() {
		return this.activityImportMilestoneRepository;
	}

	/**
	 * @return the activityImportFilterRepository
	 */
	public ActivityImportFailureObjectifyFilterRepository getActivityImportFilterRepository() {
		return this.activityImportFilterRepository;
	}

	/**
	 * @return the queueFactory
	 */
	public QueueFactory getQueueFactory() {
		return this.queueFactory;
	}

	/**
	 * @return the examplesUtil
	 */
	public ExampleTracksImportServiceTestUtil getExamplesUtil() {
		if (examplesUtil == null) {
			throw new IllegalStateException("you must call #examplesPrepared() on the builder");
		}
		return this.examplesUtil;
	}

	/**
	 * @param expectedStatus
	 * @return
	 */
	public ActivityImportFailure createPersistedMilestone(Status expectedStatus) {
		ActivityImportFailure expected = ActivityImportFailureUnitTest.getInstance(expectedStatus).getEntitySample();
		ActivityImportFailure actual = activityImportService.create(expected);
		return actual;
	}

	/**
	 * TODO: Perhaps these classes need to be moved to their own Used to construct all instances of
	 * ActivityImportServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<ActivityImportServiceTestUtil> {

		private com.aawhere.activity.ActivityServiceTestUtil.Builder activityServiceTestUtilBuilder;
		private ApplicationActivityImportInstructionsRepository.Builder importInstructionsRepositoryBuilder;
		private com.aawhere.activity.ExampleTracksImportServiceTestUtil.Builder examplesBuilder;

		public Builder() {
			super(new ActivityImportServiceTestUtil());
			this.activityServiceTestUtilBuilder = new ActivityServiceTestUtil.Builder();
			this.importInstructionsRepositoryBuilder = new ApplicationActivityImportInstructionsRepository.Builder();

		}

		/**
		 * @return the personServiceTestUtilBuilder
		 */
		public PersonServiceTestUtil.Builder getPersonServiceTestUtilBuilder() {
			return activityServiceTestUtilBuilder.getTrackTestUtilBuilder().getDocumentServiceTestUtilBuilder()
					.getPersonServiceTestUtilBuilder();
		}

		public Builder setQueueFactory(QueueFactory queueFactory) {
			building.queueFactory = queueFactory;
			return this;
		}

		/**
		 * @return the activityServiceTestUtilBuilder
		 */
		public com.aawhere.activity.ActivityServiceTestUtil.Builder getActivityServiceTestUtilBuilder() {
			return this.activityServiceTestUtilBuilder;
		}

		public Builder registerMockDocumentProducer() {
			return registerMockDocumentProducer(MockActivityDocumentProducer.create().addRetriever().build());
		}

		public Builder registerMockModifiableDocumentProducer() {
			MockActivityDocumentProducer producer = new MockActivityDocumentProducer.Builder()
					.addRetriever(new MockModifiableDocumentRetriever()).build();
			return registerMockDocumentProducer(producer);
		}

		public Builder registerMockDocumentProducer(MockActivityDocumentProducer producer) {
			building.documentProducer = producer;
			importInstructionsRepositoryBuilder = new MockImportInstructionRepo.Builder().setProducer(producer);
			getDocumentFactoryBuilder().register(producer);
			return this;
		}

		/**
		 * You must call this if you wish to have the examples util available.
		 * 
		 * @return
		 */
		public Builder examplesPrepared() {
			building.examplesUtil = ExampleTracksImportServiceTestUtil.create().registerDocuments(this)
					.importUtil(building).build();
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ActivityImportServiceTestUtil build() {
			FieldAnnotationDictionaryFactory annotationDictionaryFactory = activityServiceTestUtilBuilder
					.getTrackTestUtilBuilder().getDocumentServiceTestUtilBuilder().getPersonServiceTestUtilBuilder()
					.getAnnotationDictionaryFactory();
			annotationDictionaryFactory.getDictionary(ActivityImportFailure.class);

			building.importInstructionsRepo = importInstructionsRepositoryBuilder.build();

			building.activityServiceTestUtil = this.activityServiceTestUtilBuilder.build();
			building.personServiceTestUtil = building.activityServiceTestUtil.getTrackTestUtil().getDocumentTestUtil()
					.getPersonServiceTestUtil();

			building.activityImportFilterRepository = new ActivityImportFailureObjectifyFilterRepository(
					annotationDictionaryFactory.getFactory());
			building.activityImportMilestoneRepository = new ActivityImportFailureObjectifyRepository(
					building.activityImportFilterRepository, Synchronization.SYNCHRONOUS);
			if (building.queueFactory == null) {
				building.queueFactory = GaeQueueTestUtil.createFactory();
			}

			// FIXME:give these two their own test util?
			building.activityImportService = new ActivityImportService(building.activityImportMilestoneRepository,
					building.importInstructionsRepo, building.queueFactory,
					building.activityServiceTestUtil.getActivityService(), building.activityServiceTestUtil
							.getTrackTestUtil().getDocumentTestUtil().getDocumentService(),
					building.activityServiceTestUtil.getTrackTestUtil().getDocumentTestUtil().getDocumentFactory(),
					building.activityServiceTestUtil.getTrackTestUtil().getTrackService(),
					building.personServiceTestUtil.getPersonService());
			building.trackGeometryService = new ActivityTrackGeometryService(building.getActivityServiceTestUtil()
					.getTrackTestUtil().getTrackService(), building.activityImportService);
			return super.build();
		}

		/**
		 * @return
		 */
		public Builder registerTwoPeakImportIntructions() {

			getDocumentFactoryBuilder().register(new CsvActivityDocumentProducer.Builder().build());

			importInstructionsRepositoryBuilder
					.addInstructions(ApplicationActivityImportInstructionsRepository.twoPeakActivityInstructions())
					.addInstructions(ApplicationActivityImportInstructionsRepository.twoPeakSectionInstructions());
			return this;
		}

		/**
		 * Register garmin connect import instructions.
		 * 
		 * @return
		 */
		public Builder registerGarminConnectImportInstructions() {
			GarminConnectActivitySearchDocumentProducer.registerAll(getDocumentFactoryBuilder());

			importInstructionsRepositoryBuilder.addInstructions(ApplicationActivityImportInstructionsRepository
					.garminConnectInstructions());
			return this;
		}

		public Builder registerMmfImportInstructions() {

			MapMyFitnessDocumentProducer.registerAll(getDocumentFactoryBuilder(), new DeviceCapabilitiesRepository());

			importInstructionsRepositoryBuilder.addInstructions(ApplicationActivityImportInstructionsRepository
					.mmfInstructions());
			return this;
		}

		/**
		 * You may use Unknown instructions to make calls to the system during testing passing in
		 * custom urls as the activity id.
		 * 
		 * There is no url provided so pass in the entire url that you wish.
		 * 
		 * example:
		 * 
		 * new ActivityId("http://bogus.com");
		 * 
		 * @return
		 */
		public Builder registerUnknwonInstructions() {

			ApplicationActivityImportInstructions timeoutInstructions = ApplicationActivityImportInstructions.create()
					.addActivityUrl(ActivityWebApiUri.ACTIVITY_ID_PARAM, DocumentFormatKey.UNKNOWN)
					.setConnectionTimeoutInMillis(10).setApplication(KnownApplication.UNKNOWN.asApplication()).build();
			importInstructionsRepositoryBuilder.addInstructions(timeoutInstructions);
			return this;
		}

		/**
		 * @return
		 */
		private com.aawhere.doc.DocumentFactory.Builder getDocumentFactoryBuilder() {
			return getActivityServiceTestUtilBuilder().getTrackTestUtilBuilder().getDocumentServiceTestUtilBuilder()
					.getDocumentFactoryBuilder();
		}

		public Builder registerEveryTrailImportInstructions() {
			getDocumentFactoryBuilder().register(new GpxDocumentProducer.Builder().build())
					.register(GpxxDocumentProducer.createGpxx().build());
			// GPX 10 is covered by GPxx .register(new Gpx10DocumentProducer())
			EveryTrailDocumentUtil.registerAll(getDocumentFactoryBuilder());

			importInstructionsRepositoryBuilder.addInstructions(ApplicationActivityImportInstructionsRepository
					.everyTrailInstructions());
			return this;
		}

		/**
		 * @return
		 * 
		 */
		public Builder useTestQueue() {
			setQueueFactory(QueueTestUtil.getQueueFactory());
			return this;
		}

		/**
		 * @return
		 */
		public Builder registerRWGpsImportInstructions() {
			RWGpsDocumentProducers.registerAll(getDocumentFactoryBuilder());
			importInstructionsRepositoryBuilder.addInstructions(ApplicationActivityImportInstructionsRepository
					.rideWithGpsInstructions());
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct ActivityImportServiceTestUtil */
	private ActivityImportServiceTestUtil() {
	}

	public static Builder create() {
		return new Builder();
	}

	public Activity createPersistedActivity(ActivityId testId) throws BaseException {
		return activityImportService.getActivity(testId);
	}

	/**
	 * Use this when you want a reliable persisted simple activity that has all the components
	 * created in the system as if it were actually imported from an external source.This will
	 * create the same activity every time, but the applicationActivityId will be unique with each
	 * call.
	 * 
	 * This will iterate through the activities that are loaded with the
	 * {@link MockActivityDocumentProducer} in no particular order.
	 * 
	 * @return
	 * @throws BaseException
	 */
	public Activity createPersistedActivity() throws BaseException {
		ActivityId testId;
		if (documentProducer != null) {
			if (this.activityCreatorIterator == null) {
				this.activityCreatorIterator = documentProducer.getRetrievers().values().iterator();
			}
			if (!activityCreatorIterator.hasNext()) {
				throw new RuntimeException("not enough documents loaded into the producer"
						+ documentProducer.getRetrievers());
			}
			MockActivityDocument document = activityCreatorIterator.next().document;
			testId = document.getActivityIdentifier();
		} else {
			testId = createTestId();
		}
		return createPersistedActivity(testId);
	}

	/**
	 * @return
	 */
	public static ActivityId createTestId() {

		return new ActivityId(KnownApplication.UNKNOWN.key, TestUtil.generateRandomAlphaNumeric());
	}

	/**
	 * Used to test instructions using local resources. TODO:this could be moved to lib
	 * 
	 * */
	public static class MockImportInstructionRepo
			extends ApplicationActivityImportInstructionsRepository {

		static ApplicationActivityImportInstructions mockInstructions = ActivityImportTestUtil
				.mockInstructionsBuilder().build();

		/**
		 * Used to construct all instances of
		 * ActivityImportServiceTestUtil.MockImportInstructionRepo.
		 */
		public static class Builder
				extends ApplicationActivityImportInstructionsRepository.Builder {

			public Builder() {
				super(new MockImportInstructionRepo());
			}

			/**
			 * @param producer
			 * @return
			 */
			public Builder setProducer(MockActivityDocumentProducer producer) {
				((MockImportInstructionRepo) building).producer = producer;
				return this;
			}

			/*
			 * (non-Javadoc)
			 * @see com.aawhere.lang.ObjectBuilder#build()
			 */
			@Override
			public ApplicationActivityImportInstructionsRepository build() {

				addInstructions(ActivityImportTestUtil.mockInstructionsBuilder()
						.setModifiableDocuments(((MockImportInstructionRepo) building).producer.isModifiable()).build());
				return super.build();
			}
		}// end Builder

		private MockImportInstructionRepo() {
		}

		public static Builder create() {
			return new Builder();
		}

		private MockActivityDocumentProducer producer;

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.activity.ApplicationActivityImportInstructionsRepository#createRetriever(
		 * com.aawhere.activity.ApplicationActivityImportInstructions,
		 * com.aawhere.activity.ApplicationActivityId, com.aawhere.doc.DocumentFactory)
		 */
		@Override
		protected DocumentRetriever createActivityRetriever(Destination destination,
				ApplicationDataId applicationActivityId, DocumentFactory documentFactory,
				ApplicationActivityImportInstructions instructions) {
			return producer.getRetriever(applicationActivityId.getRemoteId());
		}

	}

	/**
	 * Retrieves only Mock Documents
	 * 
	 * @author aroller
	 * 
	 */
	public static class MockDocumentRetriever
			implements DocumentRetriever {

		private static final long serialVersionUID = -5009361718441785217L;
		final public MockActivityDocument document;
		private BaseException exception;

		/**
		 *
		 */
		public MockDocumentRetriever() {
			this.document = MockActivityDocument.create().build();
		}

		public MockDocumentRetriever(MockActivityDocument document) {
			this.document = document;
		}

		/**
		 * Used when you want to mock an exception during {@link #retrieve(DocumentFormatKey)}.
		 * 
		 * @param notFoundId
		 */
		public MockDocumentRetriever(BaseException e) {
			this((MockActivityDocument) null);
			this.exception = e;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.retrieve.DocumentRetriever#retrieve(com.aawhere.doc.DocumentFormatKey)
		 */
		@Override
		public void retrieve(DocumentFormatKey expectedFormat) throws BaseException {
			if (exception != null) {
				throw exception;
			}

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.retrieve.DocumentRetriever#getProviderDocumentIdentifier()
		 */
		@Override
		@Nonnull
		public String getProviderDocumentIdentifier() {

			return this.document.activityId;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.retrieve.DocumentRetriever#getDocument()
		 */
		@Override
		@Nonnull
		public Document getDocument() {

			return this.document;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.retrieve.DocumentRetriever#getOrigionalData()
		 */
		@Override
		public Object getOrigionalData() {
			return getDocument().toByteArray();
		}

	}

	/**
	 * Allows the updating of a document.
	 * 
	 * @author aroller
	 * 
	 */
	public static class MockModifiableDocumentRetriever
			extends MockDocumentRetriever
			implements ModifiableDocumentRetriever {
		private static final long serialVersionUID = 1L;
		public final MockActivityDocument modifiedDocument;
		private boolean modifiedDocumentRetrieved = false;

		/**
		 *
		 */
		public MockModifiableDocumentRetriever() {
			this(ExampleTracks.LINE_LONGER);
		}

		public MockModifiableDocumentRetriever(Track track) {
			super();
			modifiedDocument = MockActivityDocument.create().setActivityId(super.document.activityId).setTrack(track)
					.build();
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.retrieve.ModifiableDocumentRetriever#retrieve(com.aawhere.doc.DocumentFormatKey
		 * , org.joda.time.DateTime)
		 */
		@Override
		public void retrieve(DocumentFormatKey format, DateTime lastKnownUpdate) throws BaseException {
			if (lastKnownUpdate != null) {
				this.modifiedDocumentRetrieved = true;
			}
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.activity.ActivityImportServiceTestUtil.MockDocumentRetriever#getDocument()
		 */
		@Override
		@Nonnull
		public Document getDocument() {
			if (modifiedDocumentRetrieved) {
				return modifiedDocument;
			} else {
				return super.document;
			}
		}
	}

	/**
	 * A document that requires no parsing and should just work. TODO:move this to lib.
	 * 
	 * @author aroller
	 * 
	 */
	public static class MockActivityDocument
			implements Document, ActivityDocument, TrackContainer {

		private static final long serialVersionUID = 241059924353352779L;
		private String activityId = TestUtil.generateRandomAlphaNumeric();
		private String activityName = TestUtil.generateRandomAlphaNumeric();
		private String userId = TestUtil.generateRandomAlphaNumeric();
		public static final KnownApplication application = KnownApplication.UNKNOWN;
		private Iterable<Trackpoint> track = ExampleTracks.LINE;
		private LocalDate mostRecentDate = LocalDate.now();

		/**
		 * Used to construct all instances of ActivityImportServiceTestUtil.MockActivityDocument.
		 */
		public static class Builder
				extends ObjectBuilder<ActivityImportServiceTestUtil.MockActivityDocument> {

			public Builder() {
				super(new ActivityImportServiceTestUtil.MockActivityDocument());
			}

			public Builder setTrack(Iterable<Trackpoint> track) {
				building.track = track;
				return this;
			}

			public Builder setActivityId(String id) {
				building.activityId = id;
				return this;
			}

		}// end Builder

		public static Builder create() {
			return new Builder();
		}

		/** Use {@link Builder} to construct ActivityImportServiceTestUtil.MockActivityDocument */
		private MockActivityDocument() {
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.TrackContainer#getTrack()
		 */
		@Override
		public Track getTrack() {
			return TrackUtil.track(track);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityDocument#getActivityName()
		 */
		@Override
		@Nullable
		public String getActivityName() {

			return this.activityName;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityDocument#getActivityId()
		 */
		@Override
		public String getActivityId() {
			// random for multiple uses of this
			return this.activityId;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityDocument#getUserId()
		 */
		@Override
		public String getUserId() {

			return this.userId;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityDocument#getAccount()
		 */
		@Override
		public Account getAccount() {
			return Account.create().setApplicationKey(application.key).setRemoteId(getUserId())
					.addAlias(TestUtil.generateRandomAlphaNumeric()).build();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityDocument#getActivityType()
		 */
		@Override
		public String getActivityType() {
			//
			return ActivityTestUtil.activityType().name();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityDocument#getHistory()
		 */
		@Override
		public IdentityHistory getHistory() {

			// this is important for searching
			IdentityHistory history = new IdentityHistory.Builder().add(new Installation.Builder()
					.setExternalId(getActivityId())
					.setInstance(new Instance.Builder().setApplication(this.application.asApplication()).build())
					.build()).build();
			return history;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.activity.ActivityDocument#getDescription()
		 */
		@Override
		public String getDescription() {

			return "Description shouldn't matter";
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.Document#toByteArray()
		 */
		@Override
		public byte[] toByteArray() {

			// this ties everything together since bytes are used to decode the document
			// the producer will just key the retriever based on the id of the document.
			return this.activityId.getBytes();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.Document#getDocumentFormat()
		 */
		@Override
		public DocumentFormatKey getDocumentFormat() {

			return DocumentFormatKey.TEXT;
		}

		public ActivityId getActivityIdentifier() {
			return new ActivityId(application.key, this.getActivityId());
		}

		/*
		 * @see com.aawhere.activity.ActivityDocument#getDate()
		 */
		@Override
		public LocalDate getDate() {
			return mostRecentDate;
		}

	}
}
