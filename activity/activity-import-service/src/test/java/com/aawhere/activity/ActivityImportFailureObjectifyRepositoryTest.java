/**
 * 
 */
package com.aawhere.activity;

import org.junit.After;
import org.junit.Before;

import com.aawhere.persist.GaePersistTestUtils;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author aroller
 * 
 */
public class ActivityImportFailureObjectifyRepositoryTest
		extends ActivityImportFailureUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private ActivityImportMilestoneRepository repository;

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();
	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.activity.ActivityImportFailureUnitTest#createRepository()
	 */
	@Override
	protected ActivityImportMilestoneRepository createRepository() {
		return ActivityImportServiceTestUtil.create().build().getActivityImportMilestoneRepository();
	}

}
