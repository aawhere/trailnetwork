/**
 * 
 */
package com.aawhere.activity;

import static com.aawhere.track.ExampleTracks.*;

import java.util.List;

import org.joda.time.Duration;

import com.aawhere.activity.ActivityImportServiceTestUtil.MockActivityDocument;
import com.aawhere.activity.ActivityImportServiceTestUtil.MockDocumentRetriever;
import com.aawhere.activity.ActivityImportServiceTestUtil.MockModifiableDocumentRetriever;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.TrackTestUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.match.TrackTimingCalculatorTestUtil;

/**
 * Useful for importing {@link ExampleTracks} as legitimate activities.
 * 
 * All tracks have time adjustments to avoid time conflicts. The order of times is determined by the
 * order which {@link Builder#mock(Iterable, String)} is called.
 * 
 * @author aroller
 * 
 */
public class ExampleTracksImportServiceTestUtil {

	private MockActivityDocument line;
	private MockActivityDocument lineLonger;
	private MockActivityDocument lineBefore;
	private MockActivityDocument lineTimeShift;
	private MockActivityDocument beforeAndAfter;
	private MockActivityDocument deviatedFromLineDocument;
	private MockActivityDocument lineLongerReturn;
	private MockActivityDocument micro;
	private MockModifiableDocumentRetriever modiableRetriever;
	private ActivityImportServiceTestUtil importTestUtil;
	private MockActivityDocument returnLineOnly;
	private MockActivityDocument loop;
	private MockActivityDocument alternate;
	private MockActivityDocument loopDouble;
	private MockActivityDocument distant;
	private MockActivityDocument realCourse;
	private MockActivityDocument realRepeatInMiddle;

	/**
	 * Used to construct all instances of ActivityTrackImportServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<ExampleTracksImportServiceTestUtil> {

		// this is cumulative and increments with each call to #mock
		private Duration timeAdjustment = new Duration(0);
		// gives plenty of time between tracks
		private Duration timeDurationAdditive = new Duration(ExampleTracks.TIME_INCREMENT * 1000);
		private MockActivityDocumentProducer producer;

		public Builder() {
			super(new ExampleTracksImportServiceTestUtil());
		}

		public Builder registerDocuments(ActivityImportServiceTestUtil.Builder importTestUtil) {
			building.line = mock(LINE, "line");
			building.lineLonger = mock(LINE_LONGER, "longer");
			building.lineBefore = mock(LINE_BEFORE, "before");
			building.lineTimeShift = mock(LINE_TIME_SHIFT, "lineTimeShift");
			building.beforeAndAfter = mock(LINE_BEFORE_AFTER, "beforeAndAfter");
			building.deviatedFromLineDocument = mock(LINE_DEVIATION, "deviation");
			building.lineLongerReturn = mock(LINE_LONGER_RETURN, "longerReturn");
			building.micro = mock(MICRO, "micro");
			building.loop = mock(LOOP, "loop");
			building.loopDouble = mock(DOUBLE_LOOP, "doubleLoop");
			building.alternate = mock(LINE_BEFORE_AND_AFTER_ALTERNATE_ROUTE, "beforeAfterAlternate");
			building.modiableRetriever = new MockModifiableDocumentRetriever();
			building.returnLineOnly = mock(RETURN_POINTS, "returnOnly");
			building.distant = mock(DISTANT, "distant");
			building.realCourse = mock(TrackTimingCalculatorTestUtil.course(), "realCourse");
			building.realRepeatInMiddle = mock(	TrackTimingCalculatorTestUtil.significantReturnInTheMiddle(),
												"realRepeatInMiddle");
			this.producer = new MockActivityDocumentProducer.Builder()
					.addRetriever(new MockDocumentRetriever(building.line))
					.addRetriever(new MockDocumentRetriever(building.lineLonger))
					.addRetriever(new MockDocumentRetriever(building.lineLongerReturn))
					.addRetriever(new MockDocumentRetriever(building.beforeAndAfter))
					.addRetriever(new MockDocumentRetriever(building.lineBefore))
					.addRetriever(new MockDocumentRetriever(building.lineTimeShift))
					.addRetriever(new MockDocumentRetriever(building.micro))
					.addRetriever(new MockDocumentRetriever(building.deviatedFromLineDocument))
					.addRetriever(new MockDocumentRetriever(building.returnLineOnly))
					.addRetriever(new MockDocumentRetriever(building.loop))
					.addRetriever(new MockDocumentRetriever(building.loopDouble))
					.addRetriever(new MockDocumentRetriever(building.alternate))
					.addRetriever(new MockDocumentRetriever(building.realCourse))
					.addRetriever(new MockDocumentRetriever(building.realRepeatInMiddle))
					.addRetriever(new MockDocumentRetriever(building.distant)).addRetriever(building.modiableRetriever)
					.build();
			importTestUtil.registerMockDocumentProducer(producer);
			return this;
		}

		/**
		 * @param line
		 * @return
		 */
		private MockActivityDocument mock(Iterable<Trackpoint> line, String id) {

			// cumulate the times
			this.timeAdjustment = this.timeAdjustment.plus(this.timeDurationAdditive);
			// modifying the times to avoid time conflicts
			List<Trackpoint> timesAdjusted = TrackTestUtil.timesAdjusted(line, this.timeAdjustment);
			return new MockActivityDocument.Builder().setTrack(timesAdjusted).setActivityId(id).build();
		}

		public ExampleTracksImportServiceTestUtil build() {
			ExampleTracksImportServiceTestUtil built = super.build();

			return built;
		}

		/**
		 * @param importServiceTestUtil
		 * @return
		 */
		public Builder importUtil(ActivityImportServiceTestUtil importServiceTestUtil) {
			building.importTestUtil = importServiceTestUtil;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityTrackImportServiceTestUtil */
	private ExampleTracksImportServiceTestUtil() {
	}

	private Activity persist(MockActivityDocument doc) {
		try {
			return this.importTestUtil.createPersistedActivity(doc.getActivityIdentifier());
		} catch (BaseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return the line
	 */
	public Activity line() {
		return persist(this.line);
	}

	public Activity returnLineOnly() {
		return persist(this.returnLineOnly);
	}

	/**
	 * @return the beforeAndAfter
	 */
	public Activity beforeAndAfter() {
		return persist(this.beforeAndAfter);
	}

	/**
	 * @return the deviatedFromLineDocument
	 */
	public Activity deviation() {
		return persist(this.deviatedFromLineDocument);
	}

	/**
	 * @return the lineBefore
	 */
	public Activity lineBefore() {
		return persist(this.lineBefore);
	}

	/**
	 * @return the lineLonger
	 */
	public Activity lineLonger() {
		return persist(this.lineLonger);
	}

	/**
	 * @see ExampleTracks#LOOP
	 * 
	 * @return the loop
	 */
	public Activity loop() {
		return persist(this.loop);
	}

	/**
	 * @return the lineLongerReturn
	 */
	public Activity lineLongerReturn() {
		return persist(this.lineLongerReturn);
	}

	/**
	 * @return the lineTimeShift
	 */
	public Activity lineTimeShift() {
		return persist(this.lineTimeShift);
	}

	/**
	 * @return the micro
	 */
	public Activity micro() {
		return persist(this.micro);
	}

	/**
	 * @return the modiableRetriever
	 */
	public MockModifiableDocumentRetriever modiableRetriever() {
		return this.modiableRetriever;
	}

	public Activity modifiable() {
		return persist(this.modiableRetriever.document);
	}

	public Activity distant() {
		return persist(this.distant);
	}

	/**
	 * Just a reminder that the {@link #lineTimeShift()} is a reciprocal of {@link #line()}, but at
	 * a different time bypassing a duplicate check.
	 * 
	 * @return
	 */
	public Activity lineReciprocal() {
		return lineTimeShift();
	}

	/**
	 * @return
	 */
	public Activity alternate() {
		return persist(this.alternate);
	}

	/**
	 * @return
	 */
	public Activity loopDouble() {
		return persist(this.loopDouble);
	}

	public Activity realCourse() {
		return persist(this.realCourse);
	}

	/**
	 * @return the realRepeatInMiddle
	 */
	public Activity realRepeatInMiddle() {
		return persist(this.realRepeatInMiddle);
	}
}
