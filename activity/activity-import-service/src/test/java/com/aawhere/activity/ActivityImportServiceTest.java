/**
 *
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityImportServiceTestUtil.Builder;
import com.aawhere.activity.ActivityImportServiceTestUtil.MockActivityDocument;
import com.aawhere.activity.ActivityImportServiceTestUtil.MockDocumentRetriever;
import com.aawhere.activity.ImportMilestone.Status;
import com.aawhere.activity.relation.ActivityRelationWebApiUri;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountService;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.doc.RemoteDocumentNotFoundException;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.person.Person;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueTestUtil;
import com.aawhere.queue.QueueTestUtil.TestQueue;
import com.aawhere.queue.Task;
import com.aawhere.queue.Task.Method;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ManualTest;
import com.aawhere.test.category.ServiceTest;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;
import com.aawhere.track.TrackService;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackSummaryUtil;
import com.aawhere.track.TrackTestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * tests {@link ActivityImportService} using only local resources. See
 * {@link ActivityImportServiceConnectWebClientTest} for remote calls.
 * 
 * The ServiceStandard test does not communicate with any client. See
 * {@link ActivityImportServiceConnectWebClientTest} for such.
 * 
 * Instead, this is mocked out with {@link ActivityImportServiceTestUtil.MockImportInstructionRepo}
 * and such to provide a simulated experience without going to the destination. The mocks have
 * complex interdependency and could use some clarity. Sorry. 2013.06.05 AR
 * 
 * @author aroller
 * 
 */
@Category(ServiceTest.class)
public class ActivityImportServiceTest {

	/**
	 * 
	 */
	private static final String APP_ACTIVITY_ID_WITH_JUNK_APP = "junk-234234";
	protected ActivityImportService service;
	protected ActivityId testId;
	protected LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelperGaeBuilder()
			.withDatastoreService().build().getHelper();
	protected ActivityImportServiceTestUtil testUtil;

	@Before
	public void setUp() {
		helper.setUp();

		ActivityImportServiceTestUtil testUtil = ActivityImportServiceTestUtil.create().registerMockDocumentProducer()
		// use test coordQueue because GAE coordQueue doesn't support nameded testing since
		// coordQueue.xml is not
		// in scope
				.setQueueFactory(QueueTestUtil.getQueueFactory()).build();
		setTestUtil(testUtil);
	}

	protected void setTestUtil(ActivityImportServiceTestUtil testUtil) {
		this.testUtil = testUtil;
		service = testUtil.getActivityImportService();
		testId = ActivityImportServiceTestUtil.createTestId();
	}

	@After
	public void tearDown() {
		helper.tearDown();
	}

	/**
	 *
	 */
	@Test
	public void testCreateFailedMilestone() {
		Status status = Status.FAILED;
		Boolean expectedToBeStored = true;
		assertMilestoneCreate(status, expectedToBeStored);
	}

	/**
	 * Tests the {@link ActivityImportService#nonExistingNorFailingIds(java.util.Set)}
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testNonExistingNorFailing() throws BaseException {
		final ActivityId nonExisting1 = ActivityTestUtil.createApplicationActivityId();
		TestUtil.assertContainsOnly(nonExisting1, service.nonExistingNorFailingIds(Sets.newHashSet(nonExisting1)));
		Activity existing1 = this.testUtil.getActivityServiceTestUtil().createPersistedActivity();
		TestUtil.assertEmpty(	"the activity is created",
								service.nonExistingNorFailingIds(Sets.newHashSet(existing1.id())));
		TestUtil.assertContainsOnly(nonExisting1,
									service.nonExistingNorFailingIds(Sets.newHashSet(existing1.id(), nonExisting1)));
		final ActivityId nonExisting2 = ActivityTestUtil.createApplicationActivityId();
		Activity existing2 = this.testUtil.getActivityServiceTestUtil().createPersistedActivity();

		TestUtil.assertIterablesEquals("only non existing should be returned", Sets.newHashSet(	nonExisting1,
																								nonExisting2), service
				.nonExistingNorFailingIds(Sets.newHashSet(existing1.id(), nonExisting1, existing2.id(), nonExisting2)));
	}

	/**
	 * Success is no longer stored: TN-378
	 */
	@Test
	public void testCreateSuccessMilestone() {
		assertMilestoneCreate(Status.SUCCEEDED, false);
	}

	/**
	 * @param status
	 * @param expectedToBeStored
	 */
	private void assertMilestoneCreate(Status status, Boolean expectedToBeStored) {
		ActivityImportFailure expected = ActivityImportFailureUnitTest.getInstance(status).getEntitySample();
		ActivityImportFailure actual = service.create(expected);
		assertEquals(expected.getActivityId(), actual.getActivityId());
		assertEquals(actual + " isn't stored properly", expectedToBeStored, EntityUtil.isPersisted(actual));
		ActivityImportFailures found = service.getActivityHistory(expected.getActivityId());
		assertNotNull(found);
		final int milestoneCountExpected = (expectedToBeStored) ? 1 : 0;

		assertEquals(found.toString(), milestoneCountExpected, found.size());
		if (expectedToBeStored) {
			assertEquals(actual, found.iterator().next());
		}
	}

	@Test
	public void testQueueForImport() {
		service.queueActivityForImport(testId);
		Queue activityImportQueue = service.getActivityImportQueue();
		assertEquals("queing is the only thing this really needs to do", 1, activityImportQueue.size().intValue());
		// TN-269 removed QUEUE status milestone to reduce import costs
		// ActivityImportHistory importHistory = service.findByActivityId(testId);
		// assertFalse(importHistory.isEmpty());
	}

	@Test
	public void testCellCrawlerTasksQueuedSingleCellSameResolution()
			throws ApplicationImportInstructionsNotFoundException {
		GeoCell cell = GeoCellTestUtil.A;
		final GeoCells geoCells = GeoCells.create().add(cell).build();
		final Resolution resolution = cell.getResolution();
		int expectedTaskCount = 1;
		assertCellCrawlerTasksQueued(geoCells, resolution, expectedTaskCount);
	}

	@Test
	public void testCellCrawlerTasksQueuedSingleCellNextResolution()
			throws ApplicationImportInstructionsNotFoundException {
		GeoCell cell = GeoCellTestUtil.A;
		final GeoCells geoCells = GeoCells.create().add(cell).build();
		final Resolution resolution = cell.getResolution().higher();
		int expectedTaskCount = GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL;
		assertCellCrawlerTasksQueued(geoCells, resolution, expectedTaskCount);
	}

	@Test
	@Category(ManualTest.class)
	@Ignore("takes too long, but run it manually when you feel like confirming")
	public void testCellCrawlerTasksQueuedSingleCellThreeResolutions()
			throws ApplicationImportInstructionsNotFoundException {
		GeoCell cell = GeoCellTestUtil.A;
		final GeoCells geoCells = GeoCells.create().add(cell).build();
		final Resolution resolution = cell.getResolution().higher().higher().higher();
		int expectedTaskCount = GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL * GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL
				* GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL;
		assertCellCrawlerTasksQueued(geoCells, resolution, expectedTaskCount);
	}

	@Test(expected = UnknownApplicationException.class)
	public void testCellCrawlerInvalidApplicationKey() throws ApplicationImportInstructionsNotFoundException {
		service.cellCrawlerTasksQueued(new ApplicationKey("junk"), null, null);
	}

	@Test(expected = ApplicationImportInstructionsNotFoundException.class)
	public void testCellCrawlerNoInstructions() throws ApplicationImportInstructionsNotFoundException {
		// no way to crawl two peak at this time so it should be denied.
		service.cellCrawlerTasksQueued(KnownApplication.TWO_PEAK_ACTIVITIES.key, null, null);
	}

	@Test(expected = InvalidArgumentException.class)
	public void testCellCrawlerTasksQueuedExceededLimit() throws ApplicationImportInstructionsNotFoundException {
		GeoCell cell = GeoCellTestUtil.A;
		final GeoCells geoCells = GeoCells.create().add(cell).build();

		final Resolution resolution = Resolution.valueOf((byte) (cell.getResolution().resolution
				+ ActivityImportService.CELL_EXPLODE_LEVEL_MAX + 1));
		int expectedTaskCount = Integer.MAX_VALUE;
		assertCellCrawlerTasksQueued(geoCells, resolution, expectedTaskCount);
	}

	@Test
	public void testCrawlingThreeCells() throws ApplicationImportInstructionsNotFoundException {
		GeoCells cells = GeoCells.create().add(GeoCellTestUtil.A).add(GeoCellTestUtil.B).add(GeoCellTestUtil.C).build();
		int expectedSize = GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL * cells.size();
		assertCellCrawlerTasksQueued(cells, GeoCellTestUtil.A.getResolution().higher(), expectedSize);
	}

	/**
	 * @param geoCells
	 * @param resolution
	 * @param expectedTaskCount
	 * @throws ApplicationImportInstructionsNotFoundException
	 */
	private void assertCellCrawlerTasksQueued(final GeoCells geoCells, final Resolution resolution,
			int expectedTaskCount) throws ApplicationImportInstructionsNotFoundException {
		// over-write the service for this test
		service = ActivityImportServiceTestUtil.create().registerEveryTrailImportInstructions().useTestQueue().build()
				.getActivityImportService();
		final ApplicationKey applicationKey = KnownApplication.EVERY_TRAIL.asApplication().getKey();
		List<String> cellCrawlerTasksQueued = service.cellCrawlerTasksQueued(applicationKey, geoCells, resolution);
		TestUtil.assertSize(expectedTaskCount, cellCrawlerTasksQueued);
		QueueTestUtil.assertQueueContains(	service.getCrawlQueue(applicationKey),
											expectedTaskCount,
											cellCrawlerTasksQueued.toArray(new String[] {}));
	}

	@Test
	public void testQueueOneActivityForImport() throws UnknownApplicationException {
		ActivityId expectedId = testId;
		ActivityImportFailure milestone = service.importActivity(expectedId);
		assertNotNull(milestone);
		// TN-269 removed QUEUE status milestone to reduce import costs
		// assertActivityForImportMilestone(expectedId, milestone);F
	}

	/**
	 * @param expectedId
	 * @param milestone
	 */
	private void assertActivityForImportMilestone(ActivityId expectedId, ActivityImportFailure milestone) {
		assertNotNull(milestone);
		assertEquals(expectedId, milestone.getActivityId());
		assertEquals(ImportMilestone.Status.QUEUED, milestone.getStatus());
		ActivityImportFailures foundImports = service.getActivityHistory(expectedId);
		assertEquals(	"either too many imports are being created or tests are mixing " + foundImports,
						1,
						foundImports.size());
		ActivityImportFailure foundImport = foundImports.getCollection().get(0);
		assertEquals(foundImport.toString(), expectedId, foundImport.getActivityId());
	}

	/**
	 * Tests a random number of imports against their coordQueue milestone. If this is flaky it is
	 * because some number of imports from zero to 10 is causing a problem.
	 * 
	 * @throws UnknownApplicationException
	 */
	@Test
	public void testQueueMultipleActivitiesForImport() throws UnknownApplicationException {
		List<ActivityId> applicationActivityIds = ActivityTestUtil.createApplicationActivityIds();
		ActivityImportFailures milestones = service.importActivities(applicationActivityIds);
		assertEquals(applicationActivityIds.size(), milestones.size());
		Iterator<ActivityId> idIterator = applicationActivityIds.iterator();
		// TN-269 removed QUEUE status milestone to reduce import costs
		// for (ActivityImportMilestone activityImportMilestone : milestones) {
		// assertActivityForImportMilestone(idIterator.next(), activityImportMilestone);
		// }
	}

	@Test(expected = UnknownApplicationException.class)
	public void testUnknownApplicationOnImport() throws UnknownApplicationException {
		service.importActivity(new ActivityId(APP_ACTIVITY_ID_WITH_JUNK_APP));
	}

	@Test(expected = EntityNotFoundException.class)
	public void testGetUnknownTrailNetworkActivity() throws BaseException {

		service.getActivity(new ActivityId(KnownApplication.TRAIL_NETWORK.key, "99999"));
	}

	@Test
	public void testMissingInstructionsnOnImport() throws BaseException {

		// the repo is mocked out for service tests so no real applications should be known
		try {
			service.getActivity(new ActivityId(KnownApplication.GARMIN_CONNECT.key, "12354"));
			fail("exception should be thrown");
		} catch (BaseException e) {
			TestUtil.assertInstanceOf(ApplicationImportInstructionsNotFoundException.class, e.getCause());
		}
	}

	@Test(expected = UnknownApplicationException.class)
	public void testUnknownAppOnGet() throws BaseException {
		service.getActivity(new ActivityId(APP_ACTIVITY_ID_WITH_JUNK_APP));
	}

	@Test
	public void testImportMockDocument() throws BaseException {
		Activity activity = testUtil.createPersistedActivity();
		ActivityImportFailures activityImports = service.getActivityHistory(activity.getId());
		assertEquals("no longer storing anything but failures" + activityImports.toString(), 0, activityImports.size());
		ActivityService activityService = testUtil.getActivityServiceTestUtil().getActivityService();
		Activity foundActivity = activityService.getActivity(activity.getId());
		assertEquals(activity.getId(), foundActivity.getId());
		Track track = testUtil.getActivityServiceTestUtil().getTrackTestUtil().getTrackService()
				.getTrack(activity.getTrackSummary().getDocumentId());
		assertNotNull(track);
		foundActivity = activityService.getActivity(activity.getId());
		assertEquals("finding by app id found something else", activity.getId(), foundActivity.getId());
		assertNotNull(foundActivity.getAccountId());

	}

	@Test
	public void testOrderByMostRecent() {
		ActivityImportServiceTestUtil util = ActivityImportServiceTestUtil.create().examplesPrepared().build();
		Activity earlier = util.getExamplesUtil().line();
		Activity mostRecent = util.getExamplesUtil().lineBefore();
		final ActivityService activityService = this.testUtil.getActivityServiceTestUtil().getActivityService();
		Activities activities = activityService.getActivities(activityService.getActivityFilterBuilder()
				.orderByMostRecentStartTime().build());
		assertEquals(mostRecent, Iterables.getFirst(activities, null));
		assertEquals(earlier, Iterables.getLast(activities, null));
	}

	@Test
	public void testAccounts() throws BaseException {
		ActivityImportServiceTestUtil util = ActivityImportServiceTestUtil.create().examplesPrepared().build();
		Activity target = util.getExamplesUtil().line();
		Activity decoy = util.getExamplesUtil().lineBefore();
		TestUtil.assertNotEquals(target, decoy);
		TestUtil.assertNotEquals(target.getAccountId(), decoy.getAccountId());

		final ActivityService activityService = util.getActivityServiceTestUtil().getActivityService();
		final Filter filter = Filter.create().filter();
		Activities activities = activityService.activities(target.getAccountId(), filter);
		TestUtil.assertContainsOnly(target, activities);
		AccountService accountService = util.getActivityServiceTestUtil().getTrackTestUtil().getDocumentTestUtil()
				.getPersonServiceTestUtil().getAccountService();
		Account account = accountService.account(target.getAccountId());
		String alias = AccountTestUtil.alias();
		Account aliasAdded = accountService.addAlias(account.id(), alias);
		TestUtil.assertContains(alias, aliasAdded.aliases());
		AccountId aliasId = new AccountId(account.applicationKey(), alias);
		// confirms that searching by alias produces same results
		TestUtil.assertContainsOnly(target, activityService.activities(aliasId, filter));

		Person person = this.testUtil.getPersonServiceTestUtil().getPersonService().person(account).getKey();
		Activities activitiesForPerson = activityService.activities(person.id(), filter);
		TestUtil.assertIterablesEquals("activities by account differs from by person ", activities, activitiesForPerson);

		// now, create another for the same account to prove order
		Activity beforeAndAfter = util.getExamplesUtil().beforeAndAfter();
		beforeAndAfter = testUtil.getActivityServiceTestUtil().getFilterRepository()
				.update(Activity.mutate(beforeAndAfter).setAccount(account).build());
		Activities activitiesMostRecent = activityService.activities(person.id(), filter);
		assertEquals(beforeAndAfter, Iterables.getFirst(activitiesMostRecent, null));
		assertEquals(target, Iterables.getLast(activitiesMostRecent, null));

	}

	@Test
	public void testUpdateUnknown() throws BaseException {
		try {
			service.importOrUpdate(testId);
		} catch (ActivityImportException e) {
			if (e.getCause() instanceof RemoteDocumentNotFoundException) {
				// good, otherwise not good.
			} else {
				throw (e);
			}
		}
	}

	@Test
	public void testUpdateNotSupported() throws BaseException {
		Activity activity = testUtil.createPersistedActivity();
		// the test documents aren't able to be updated.
		Activity updated = service.importOrUpdate(activity.getId());
		assertEquals(	"this isn't modifiable so nothing should have happened",
						activity.getEntityVersion(),
						updated.getEntityVersion());
	}

	@Test
	public void testUpdate() throws BaseException {

		setTestUtil(ActivityImportServiceTestUtil.create().registerMockModifiableDocumentProducer().build());

		Activity activity = testUtil.createPersistedActivity();
		Activity updated = service.importOrUpdate(activity.getId());
		assertNotSame("if they are the same then these tests must change", activity, updated);
		assertFalse("the track summary should now be different",
					TrackSummaryUtil.isLikelyFromTheSameTrack(activity.getTrackSummary(), updated.getTrackSummary()));
		TestUtil.assertGreaterThan(	"activity should show modification",
									activity.getEntityVersion(),
									updated.getEntityVersion());
	}

	/**
	 * Following the theme of import, if the activity already exists then an update is suggested.
	 * 
	 * @throws BaseException
	 * 
	 */
	@Test
	public void testQueueForUpdate() throws BaseException {
		Activity activity = testUtil.createPersistedActivity();
		service.importActivity(activity.getId());
		TestQueue activityImportQueue = (TestQueue) service.getActivityImportQueue();
		Task second = activityImportQueue.getQueue().poll();
		assertEquals("method should be put if activity already exists", Method.PUT, second.getMethod());
	}

	/**
	 * Creating an activity signals the relations to process the newly created activity.
	 * 
	 * @throws BaseException
	 */
	@Test
	@Ignore("relying on timings now...we sould update this to use timgings")
	public void testRelationQueueSignalAfterImport() throws BaseException {
		Activity activity = testUtil.createPersistedActivity();
		assertNotNull("activity is always created by util", activity);
		Queue relationsQueue = ActivityRelationWebApiUri.getRelationsQueue(testUtil.getQueueFactory());
		assertEquals("creating an activity should coordQueue for relations processing always", 1, relationsQueue.size()
				.intValue());
	}

	@Test
	public void testGcWebUrl() {
		ActivityImportServiceTestUtil util = ActivityImportServiceTestUtil.create()
				.registerGarminConnectImportInstructions().build();
		final ApplicationKey applicationkey = KnownApplication.GARMIN_CONNECT.key;
		ApplicationActivityImportInstructionsRepository importInstructionsRepo = util.getImportInstructionsRepo();
		ActivityId id = new ActivityId(applicationkey, "bogus");
		String url = importInstructionsRepo.applicationUrl(id);
		TestUtil.assertContains(url, id.getRemoteId());
	}

	/**
	 * Searching for an activity by our id that doesn't exist in the database should report not
	 * found and not import it.
	 * 
	 * @throws BaseException
	 */
	@Test(expected = EntityNotFoundException.class)
	public void testGetOneOfOursNotFound() throws BaseException {

		service.getActivity(ActivityIdentifierUtilUnitTest.ourId);
	}

	/**
	 * provides an alternate track to the document producer and verifies that is the track that is
	 * persisted and delivered subsequently.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testAlternateMockDocumentProducerSingleActivity() throws BaseException {

		int expectedNumberOfTrackpoints = ExampleTracks.FIRST_SEGMENT_THEN_DISTANT_POINTS.size();
		Track expectedTrack = ExampleTracks.FIRST_SEGMENT_THEN_DISTANT;
		Builder utilBuilder = ActivityImportServiceTestUtil.create();
		MockActivityDocument doc = MockActivityDocument.create().setTrack(expectedTrack).build();
		MockDocumentRetriever retriever = new MockDocumentRetriever(doc);
		MockActivityDocumentProducer alternateProducer = MockActivityDocumentProducer.create().addRetriever(retriever)
				.build();
		ActivityImportServiceTestUtil alternateUtil = utilBuilder.registerMockDocumentProducer(alternateProducer)
				.build();
		Activity persistedActivity = alternateUtil.createPersistedActivity();
		TrackService trackService = alternateUtil.getActivityServiceTestUtil().getTrackTestUtil().getTrackService();
		TrackSummary trackSummary = persistedActivity.getTrackSummary();
		assertEquals(	persistedActivity.getTrackSummary().getNumberOfTrackpoints(),
						trackSummary.getNumberOfTrackpoints());
		assertEquals(expectedNumberOfTrackpoints, trackSummary.getNumberOfTrackpoints().intValue());
		Track actualTrack = trackService.getTrack(trackSummary.getDocumentId());
		TrackTestUtil.assertTrackEquals(expectedTrack, actualTrack);
	}

	//
	@Test
	@Ignore("Testing if this filter is used by code.")
	public void testFilterByStatus() {
		Status expectedStatus = Status.FAILED;
		ActivityImportFailure actual = testUtil.createPersistedMilestone(expectedStatus);
		assertEquals("status", expectedStatus, actual.getStatus());
		Filter filter = ActivityImportFailureFilterBuilder.create().equalsStatus(expectedStatus).build();
		ActivityImportFailures milestones = service.filter(filter);
		assertEquals(1, milestones.size());
		ActivityImportFailures processedMilestones = service.filter(ActivityImportFailureFilterBuilder.create()
				.equalsStatus(Status.PROCESSED).build());
		assertEquals(0, processedMilestones.size());
	}

}
