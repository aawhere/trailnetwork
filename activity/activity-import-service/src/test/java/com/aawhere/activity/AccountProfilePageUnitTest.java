/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class AccountProfilePageUnitTest {

	private ApplicationActivityImportInstructionsRepository repository;
	private ApplicationKey applicationKey;
	private boolean expectsAlias = false;
	private boolean expectsNoInstructionsException = false;
	private boolean expectsUnknownApplicationException = false;

	@Before
	public void setUp() {
		this.repository = ApplicationActivityImportInstructionsRepository.create()
				.addInstructions(ApplicationActivityImportInstructionsRepository.garminConnectInstructions())
				.addInstructions(ApplicationActivityImportInstructionsRepository.mmfInstructions())
				.addInstructions(ApplicationActivityImportInstructionsRepository.everyTrailInstructions())
				.addInstructions(ApplicationActivityImportInstructionsRepository.twoPeakActivityInstructions()).build();

	}

	@After
	public void test() {
		Account account = AccountTestUtil.account(applicationKey);
		try {
			String profilePageUrl = repository.profilePageUrl(account);

			String expected;
			if (expectsAlias) {
				expected = account.alias();
			} else {
				expected = account.id().getRemoteId();
			}
			TestUtil.assertContains(profilePageUrl, expected);
		} catch (ApplicationImportInstructionsNotFoundException e) {
			assertTrue(e.getMessage(), expectsNoInstructionsException);
		} catch (UnknownApplicationException unknown) {
			assertTrue(unknown.getMessage(), expectsUnknownApplicationException);
		}
	}

	@Test
	public void testGarminProfilePage() throws ApplicationImportInstructionsNotFoundException {
		this.applicationKey = KnownApplication.GARMIN_CONNECT.key;
		this.expectsAlias = true;
	}

	@Test
	public void testEveryTrailProfilePage() {
		this.applicationKey = KnownApplication.EVERY_TRAIL.key;

	}

	@Test
	public void testMmfProfilePage() {
		this.applicationKey = KnownApplication.MMF.key;
	}

	@Test
	public void testProfilePageNotProvided() {
		this.applicationKey = KnownApplication.TWO_PEAK_ACTIVITIES.key;
		this.expectsNoInstructionsException = true;
	}

	@Test
	public void testProfilePageForApplicationWithoutInstructions() {
		this.expectsNoInstructionsException = true;
		this.applicationKey = KnownApplication.UNKNOWN.key;
	}

	@Test
	public void testProfilePageForUnknownApplication() {
		this.expectsUnknownApplicationException = true;
		this.applicationKey = new ApplicationKey("junk");
	}
}
