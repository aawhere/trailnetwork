/**
 * 
 */
package com.aawhere.activity;

import org.junit.Before;

import com.aawhere.activity.ActivityImportFailure.ActivityImportFailureId;
import com.aawhere.persist.BaseObjectifyFilterRepositoryTest;

/**
 * @author aroller
 * 
 */
public class ActivityImportFailureObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<ActivityImportFailureId, ActivityImportFailure, ActivityImportFailureObjectifyRepository, ActivityImportFailureObjectifyRepository, ActivityImportFailures, ActivityImportFailures.Builder> {

	private ActivityImportFailureObjectifyRepository repository;
	private ActivityImportFailureObjectifyFilterRepository filterRepository;

	@Before
	public void setUp() {
		ActivityImportServiceTestUtil activityTestUtil = ActivityImportServiceTestUtil.create().build();
		this.repository = (ActivityImportFailureObjectifyRepository) activityTestUtil
				.getActivityImportMilestoneRepository();
		this.filterRepository = activityTestUtil.getActivityImportFilterRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected ActivityImportFailure createEntity() {
		return ActivityImportFailureUnitTest.getInstance().getEntitySample();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected ActivityImportFailureObjectifyRepository getRepository() {
		return this.repository;
	}

}
