/**
 * 
 */
package com.aawhere.activity;

import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.annotation.Nonnull;

import com.aawhere.activity.ActivityImportServiceTestUtil.MockDocumentRetriever;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.doc.RemoteDocumentNotFoundException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.retrieve.ModifiableDocumentRetriever;

/**
 * @author aroller
 * 
 */
public class MockActivityDocumentProducer
		implements DocumentProducer {

	/**
	 * Used to construct all instances of MockActivityDocumentProducer.
	 */
	public static class Builder
			extends ObjectBuilder<MockActivityDocumentProducer> {

		public Builder() {
			super(new MockActivityDocumentProducer());
		}

		public Builder addRetriever(MockDocumentRetriever retriever) {
			building.retrievers.put(retriever.document.getActivityId(), retriever);
			if (retriever instanceof ModifiableDocumentRetriever) {
				building.modifiable = true;
			}
			return this;
		}

		public Builder addRetriever() {
			return addRetriever(new MockDocumentRetriever());

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public MockActivityDocumentProducer build() {
			building.retrievers = Collections.unmodifiableSortedMap(building.retrievers);
			return super.build();
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct MockActivityDocumentProducer */
	private MockActivityDocumentProducer() {
	}

	// keeps order to help with simple management
	private SortedMap<String, MockDocumentRetriever> retrievers = new TreeMap<String, ActivityImportServiceTestUtil.MockDocumentRetriever>();

	/**
	 * Indicates if we are testing modifiable documents.
	 * 
	 */
	public Boolean modifiable = false;

	public Boolean isModifiable() {
		return modifiable;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	@Nonnull
	public Document produce(@Nonnull Object dataInput) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException {

		String activityId = new String((byte[]) dataInput);
		MockDocumentRetriever retriever = getRetriever(activityId);
		Document document = retriever.getDocument();
		return document;
	}

	/**
	 * @return the retrievers
	 */
	public SortedMap<String, MockDocumentRetriever> getRetrievers() {
		return this.retrievers;
	}

	/**
	 * @param activityId
	 * @return
	 */
	MockDocumentRetriever getRetriever(String activityId) {
		MockDocumentRetriever retriever = retrievers.get(activityId);
		if (retriever == null) {
			retriever = new MockDocumentRetriever(new RemoteDocumentNotFoundException(activityId));
		}
		return retriever;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	@Nonnull
	public DocumentFormatKey[] getDocumentFormatsSupported() {

		return new DocumentFormatKey[] { DocumentFormatKey.TEXT };
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {

		return DiscoveryOrder.LAST;
	}

	/**
	 * @return
	 */
	public MockDocumentRetriever getFirstRetriever() {
		return this.retrievers.get(this.retrievers.firstKey());
	}

}
