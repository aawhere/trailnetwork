/**
 * 
 */
package com.aawhere.activity;

import static org.junit.Assert.*;

import java.util.Collection;

import javax.measure.unit.MetricSystem;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.activity.ActivityImportServiceTestUtil.Builder;
import com.aawhere.activity.ImportMilestone.Status;
import com.aawhere.activity.imprt.everytrail.EveryTrailDocumentTestUtil;
import com.aawhere.activity.imprt.mmf.MapMyFitnessImportUtil;
import com.aawhere.activity.imprt.mmf.MapMyFitnessTestUtil;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.doc.RemoteDocumentNotFoundException;
import com.aawhere.doc.RemoteDocumentNotModifiedException;
import com.aawhere.doc.RemoteDocumentNotPublicException;
import com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer;
import com.aawhere.io.ConnectionTimeoutException;
import com.aawhere.io.IoException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.person.Person;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ManualTest;
import com.aawhere.test.category.WebClientTest;
import com.aawhere.track.TrackIncapableDocumentException;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.base.Predicates;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

/**
 * Tests the service methods that also make remote calls.
 * 
 * Maven Surefire seems to ignore the method level annotations so these are in their own class.
 * 
 * Although extension causes tests to be run twice in a complete build...the environment setup is
 * worth reusing for running tests again.
 * 
 * @author aroller
 * 
 */
@Category({ WebClientTest.class })
public class ActivityImportServiceWebClientTest {

	/**
	 * 
	 */

	protected ActivityImportService service;
	protected LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelperGaeBuilder()
			.withTaskQueueService().withDatastoreService().build().getHelper();
	private ActivityImportServiceTestUtil testUtil;

	@Before
	public void setUp() {
		helper.setUp();

		Builder testUtilBuilder = ActivityImportServiceTestUtil.create();

		testUtilBuilder.useTestQueue();
		this.testUtil = testUtilBuilder.registerTwoPeakImportIntructions().registerGarminConnectImportInstructions()
				.registerUnknwonInstructions().registerEveryTrailImportInstructions().registerMmfImportInstructions()
				.registerRWGpsImportInstructions().build();
		service = testUtil.getActivityImportService();
	}

	/** Retrieves an existing Activity from 2Peak or completely retrieves it . */
	@Test
	@Ignore("This is failing https://aawhere.jira.com/builds/browse/AAWHERE-COMPLETE-449/test")
	public void testGetTwoPeakActivityByApplicationId() throws BaseException {
		ActivityId id = ActivityImportFixture.TWO_PEAK_APPLICATION_ACTIVITY_ID;
		assertGetActivityByApplicationId(id);
	}

	/**
	 * Retrieves an existing Activity from 2Peak and tries to update expecting a failure . This is
	 * manual because 2peak activities reliability is not reliable.
	 */
	@Test
	@Category(ManualTest.class)
	@Ignore("this is failing due https://aawhere.jira.com/builds/browse/AAWHERE-COMPLETE-449/test")
	public void testGetAndUpdateTwoPeakSection() throws BaseException {
		ActivityId id = ActivityImportFixture.TWO_PEAK_SECTION_ACTIVITY_ID;
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull(activity);

		try {
			service.importOrUpdate(id);
			fail(id + " shouldn't be updated since nothing could have changed");
		} catch (RemoteDocumentNotModifiedException e) {
			// good, everything else is bad
		}

	}

	@Test
	public void testTimeout() throws BaseException {
		ActivityId id = new ActivityId(KnownApplication.UNKNOWN.key, "http://www.google.com:81");
		try {
			assertGetActivityByApplicationId(id);
			fail("shold have timed out");
		} catch (ToRuntimeException e) {
			TestUtil.assertInstanceOf(ConnectionTimeoutException.class, e.getCause());
		}
	}

	/** Retrieves an existing Activity or completely retrieves it . */
	@Test
	public void testGetConnectActivityByApplicationId() throws BaseException {
		ActivityId id = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "653165903");
		com.aawhere.doc.DocumentFactory.getInstance().register(new GpxDocumentProducer.Builder().build());
		assertGetActivityByApplicationId(id);
	}

	/** Basic test for EveryTrail activities */
	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testEveryTrail() throws BaseException {
		ActivityId id = ActivityImportFixture.EVERY_TRAIL_EXAMPLE;
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull(activity.getName());
	}

	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testEveryTrailProblem() throws BaseException {
		ActivityId id = new ActivityId(KnownApplication.EVERY_TRAIL.key, "80284");
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull(activity.getName());
	}

	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testEveryIndexOutOfBounds() throws BaseException {
		ActivityId id = new ActivityId(KnownApplication.EVERY_TRAIL.key, "1323441");
		try {
			assertGetActivityByApplicationId(id);
			fail("there is no track...");
		} catch (ActivityImportException e) {
			Throwable cause = e.getCause();
			assertNotNull(cause);
			TestUtil.assertInstanceOf(TrackIncapableDocumentException.class, cause);
		}
	}

	@Test
	public void testRideWithGps() throws BaseException {
		ActivityId id = ActivityImportFixture.RW_EXAMPLE_ACTIVITY;
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull(activity.getName());
	}

	/**
	 * Previously a number format was being caused since EveryTrail puts a nan in some elevation
	 * values.
	 * 
	 * @throws BaseException
	 */
	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testEveryTrailNumberFormatException() throws BaseException {
		ActivityId id = new ActivityId(KnownApplication.EVERY_TRAIL.key, "1384527");
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull(activity.getName());
	}

	/**
	 * Caused by timestamp missing a change was made to the NonProgressingTimestampFilter to ignore
	 * trackpoints with a null timestamp.
	 * 
	 * @throws BaseException
	 */
	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testEveryTrailNullPointerExceptionTimestampMissing() throws BaseException {
		ActivityId id = new ActivityId(KnownApplication.EVERY_TRAIL.key, "842782");
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull(activity.getName());
	}

	@Test
	public void testMmfWorkout() throws BaseException {
		ActivityId id = MapMyFitnessTestUtil.TEST_ACTIVITY_ID;
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull("TN-900 default names are stored, stripped later", activity.getName());
		assertTrue(	"the default name is used for this activity",
					MapMyFitnessImportUtil.isDefaultName(String.valueOf(activity.getName())));
		assertNotNull(activity.getTrackSummary());
		assertEquals(ActivityType.BIKE, activity.getActivityType());
		TestUtil.assertGreaterThan(0, activity.getTrackSummary().getNumberOfTrackpoints());
		Account account = this.testUtil.getPersonServiceTestUtil().getAccountService().account(activity.getAccountId());
		TestUtil.assertNotEmpty(account.getAliases());
		TestUtil.assertContains(MapMyFitnessTestUtil.TEST_ACCOUNT_USERNAME, account.getAliases());
	}

	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testEveryTrailGpx10() throws BaseException {
		ActivityId id = new ActivityId(KnownApplication.EVERY_TRAIL.key, "828094");
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull(activity.getName());
	}

	/** Basic test for MMF activities */
	@Test
	public void testMmf() throws BaseException {
		ActivityId id = new ActivityId(KnownApplication.MMF.key, "869944021");
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull(activity.getName());
		assertNotNull(activity.getDevice());
	}

	/** Basic test for EveryTrail activities */
	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testeveryTrailCrawl() throws BaseException {
		ActivityImportCrawler activities = service.activitiesCrawled(	KnownApplication.EVERY_TRAIL.key,
																		GeoCells.valueOf("8e62f3"),
																		Filter.create().build());
		TestUtil.assertSize(Range.atLeast(1), activities.accountIdsAvailable());
		TestUtil.assertSize(Range.atLeast(1), activities.activityIdsAvailable());
	}

	@Test
	public void testCrawlGarminConnect() throws BaseException {
		final ApplicationKey applicationKey = KnownApplication.GARMIN_CONNECT.key;
		assertCellCrawl(applicationKey);
	}

	/**
	 * @param applicationKey
	 * @throws BaseException
	 */
	private void assertCellCrawl(final ApplicationKey applicationKey) throws BaseException {
		ActivityImportCrawler activities = service.activitiesCrawled(applicationKey, GeoCells
				.valueOf(ActivityImportFixture.TENNESSEE_VALLEY_GEO_CELL_VALUE), Filter.create().build());
		TestUtil.assertSize(Range.atLeast(1), activities.accountIdsAvailable());
		TestUtil.assertSize(Range.atLeast(1), activities.activityIdsAvailable());
	}

	@Test
	public void testCrawlGarminConnectUser() throws BaseException {
		final AccountImportManager manager = assertUserCrawl(ActivityImportFixture.AROLLER_TEST_GC_USER_ID);

		// arollertest is used for testing and doesn't change often. first page has an untitled
		// activity TN-818
		final ActivityId untitledActivityId = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "164395890");
		TestUtil.assertContains(untitledActivityId, manager.crawler().activityIdsAvailable());

	}

	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testCrawlEveryTrailUser() throws BaseException {
		assertUserCrawl(EveryTrailDocumentTestUtil.ACCOUNT_ID);
	}

	/**
	 * verifies remote call to MMF works with an account.
	 * 
	 * @see AccountImportManagerUnitTest
	 * @throws BaseException
	 */
	@Test
	public void testCrawlMmfUser() throws BaseException {

		AccountId accountId = ActivityImportFixture.ZEBATHON_MMF_ACCOUNT_ID;
		assertUserCrawl(accountId);

	}

	@Test
	public void testCrawlRWGpsUser() throws BaseException {

		AccountId accountId = ActivityImportFixture.CULLEN_RW_ACCOUNT_ID;
		assertUserCrawl(accountId);

	}

	/**
	 * Loads the first page of a user from an application. The test user must have more than 1 page
	 * of activities. Manager is returned for specific application testing.
	 * 
	 * @param accountId
	 * @return
	 * @throws BaseException
	 */
	private AccountImportManager assertUserCrawl(AccountId accountId) throws BaseException {
		AccountImportManager manager = service.activitiesCrawled(accountId, Filter.create().build());
		assertEquals("the manager switched accounts", accountId, manager.accountId());
		assertFalse("we didn't request to go deep  for " + accountId, manager.deepRequested());
		assertTrue(accountId + "'s activities are new to us so we should be going deep", manager.deepChosenByManager());
		assertNotNull(accountId + " we should be checking back soon to look at his imports", manager.refreshTask());
		TestUtil.assertEmpty(	accountId + " should have no activities since this is a test",
								manager.mostRecentKnownActivities());
		// assert crawler
		ActivityImportCrawler crawler = manager.crawler();
		TestUtil.assertSize(Range.atLeast(1), crawler.activityIdsAvailable());
		TestUtil.assertNotEmpty(crawler.idsDiscovered().entries());
		TestUtil.assertNotEmpty(crawler.idsQueued().entries());
		assertNotNull(accountId + " has activities so a date should be available", crawler.mostRecentStartDate());
		assertFalse(accountId + " has activities and we don't know about any", crawler.stoppedBecauseNoNewActivities());
		assertNotNull(accountId + " has more than 1 page", crawler.nextPageTask());
		assertTrue("the manager chose to go deep for " + accountId, crawler.deep());
		// avoid the id validation if the document can't produce the id...it happens
		if (!(crawler.accountIdsAvailable().size() == 1 && crawler.accountIdsAvailable()
				.contains(ActivityImportCrawler.ACCOUNT_ID_UNAVAILABLE))) {
			TestUtil.assertContainsOnly(accountId, crawler.accountIdsAvailable());
		}
		assertFalse(accountId + " should have more than one page", crawler.finalPage());
		return manager;
	}

	/**
	 * Crawls the MMF routes looking for users creating routes in the area.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testCrawlMmf() throws BaseException {
		assertCellCrawl(KnownApplication.MMF.key);
	}

	@Test
	public void testGcGetNotFound() throws BaseException {
		ActivityId id = ActivityImportFixture.GC_NOT_FOUND_ACTIVITY_ID;
		assertException(id, RemoteDocumentNotFoundException.class);
	}

	@Test
	public void testGcInternalError() throws BaseException {
		ActivityId id = new ActivityId(KnownApplication.GARMIN_CONNECT.key, "375731233");
		assertException(id, IoException.class);
	}

	/**
	 * @param id
	 */
	private void assertException(ActivityId id, Class<? extends Exception> expectedException) {
		try {
			assertGetActivityByApplicationId(id);
		} catch (BaseException e) {
			TestUtil.assertInstanceOf(expectedException, e.getCause());
		}
	}

	@Test
	public void testGcPrivate() throws BaseException {
		assertException(ActivityImportFixture.GC_UNAUTHORIZED_ACTIVITY_ID, RemoteDocumentNotPublicException.class);
	}

	private Activity assertGetActivityByApplicationId(ActivityId id) throws BaseException {
		Activity activity;
		try {
			activity = service.getActivity(id);
		} catch (BaseException e) {
			// every exception must be converted to ActivityImportException besides
			// ApplicationNotFoundException
			TestUtil.assertInstanceOf(ActivityImportException.class, e);
			ActivityImportFailures activityImports = service.getActivityHistory(id);
			assertTrue(activityImports.isErroneous());
			throw (e);
		}
		assertNotNull("activity", activity);

		Activity foundActivity = service.getActivity(id);
		assertEquals("we are importing multiple times for the same activity", activity.getId(), foundActivity.getId());

		// let's search by our id to make sure we can find it.
		ActivityId ourId = new ActivityId(activity.getId().getValue().toString());

		Activity tnFoundActivity = service.getActivity(ourId);
		assertEquals(activity.getId(), tnFoundActivity.getId());

		ActivityImportFailures activityImports = service.getActivityHistory(id);
		assertFalse(activityImports.isErroneous());
		Collection<Status> processedStatus = activityImports.filterStatus(Predicates
				.equalTo(ImportMilestone.Status.PROCESSED));
		assertEquals("only failures store milestones", 0, processedStatus.size());
		return activity;
	}

	@Test
	public void testCells() throws UnknownApplicationException, ActivityImportException, EntityNotFoundException {
		String expectedCellString = "8 8e 8e6 8e62 8e62f 8e62f3 8e62f33 8e62f33e 8e62f33f 8e62f35 8e62f357 8e62f35c 8e62f35d 8e62f35e 8e62f36 8e62f361 8e62f363 8e62f364 8e62f365 8e62f369 8e62f36a 8e62f36b 8e62f37 8e62f370 8e62f372 8e62f373 8e62f374 8e62f376 8e62f378 8e62f379 8e62f37a 8e62f37b 8e62f37f 8e62f39 8e62f394 8e62f395 8e62f396 8e62f397 8e62f3c 8e62f3c0 8e62f3c1 8e62f3c2 8e62f3c3 8e62f3c9 8e62f3cc 8e62f3ce 8e62f3cf 8e62f3d 8e62f3d0 8e62f3d1 8e62f3d2 8e62f3d3 8e62f3d5 8e62f3d6 8e62f3d7 8e62f3d8 8e62f3d9 8e62f3da 8e62f3dc 8e62f3e 8e62f3e5 8e62f3f 8e62f3f0 8e62f4 8e62f4a 8e62f4aa 8e62f6 8e62f60 8e62f600 8e62f602 8e62f603 8e62f609 8e62f60b 8e62f60c 8e62f60e 8e62f62 8e62f624 8e62f626 8e62f628 8e62f629 8e62f62a 8e62f62c";
		GeoCells expectedCells = GeoCells.valueOf(expectedCellString);
		Activity activity = this.service.getActivity(new ActivityId("gc-3333992"));
		SetView<GeoCell> difference = Sets.difference(expectedCells.getAll(), activity.getTrackSummary()
				.getSpatialBuffer().getAll());
		TestUtil.assertEmpty(difference);
	}

	/**
	 * Attempts to load a bad activity verifying it is not found, then tries again making sure it is
	 * denied, then tries again with a force to make sure another remote attempt is made.
	 * 
	 */
	@Test
	public void testForceImport() {

		final ActivityId badId = new ActivityId(KnownApplication.TWO_PEAK_ACTIVITIES.key, "junk");
		try {
			service.getActivity(badId);
			fail("this should have failed");
		} catch (BaseException e) {
			// good, that is expected
			Class<ActivityImportException> expectedFailureDuringImport = ActivityImportException.class;
			assertEquals(expectedFailureDuringImport, e.getClass());
			try {
				service.getActivity(badId);
				fail("this should have failed");
			} catch (BaseException e1) {
				assertEquals(HttpStatusCode.ACCEPTED, e1.getStatusCode());
				try {
					service.getActivity(badId, true);
					fail("this should have failed");
				} catch (BaseException e3) {
					assertEquals(expectedFailureDuringImport, e3.getClass());
				}
			}
		}
	}

	@Test
	public void testTn554InfinityAverageSpeed() throws BaseException {
		ActivityId id = new ActivityId("mm-186895631");
		Activity activity = assertGetActivityByApplicationId(id);
		assertNotNull(activity.getAccountId());
		assertNotNull(activity.getTrackSummary().getAverageSpeed().doubleValue(MetricSystem.METRES_PER_SECOND) != Double.POSITIVE_INFINITY);
	}

	@Test
	public void testMmfAccountUpdate() throws BaseException {
		ActivityImportServiceTestUtil testUtil = ActivityImportServiceTestUtil.create().registerMmfImportInstructions()
				.build();
		String expectedUsername = MapMyFitnessTestUtil.TEST_ACCOUNT_USERNAME;
		AccountId expectedAccountId = MapMyFitnessTestUtil.TEST_ACCOUNT;
		assertAccount(testUtil, expectedAccountId, expectedUsername, MapMyFitnessTestUtil.TEST_ACCOUNT_NAME);
	}

	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testEveryTrailAccountUpdate() throws EntityNotFoundException, BaseException {
		assertAccount(	ActivityImportServiceTestUtil.create().registerEveryTrailImportInstructions().build(),
						EveryTrailDocumentTestUtil.ACCOUNT_ID,
						EveryTrailDocumentTestUtil.USERNAME,
						EveryTrailDocumentTestUtil.NAME);

	}

	/**
	 * Based on user with incomplete information this ensures names not provided will work as well
	 * as lower case usernames.
	 * 
	 * http://www.everytrail.com/api/user?user_id=245903
	 */
	@Test
	@Ignore("TN-888 EveryTrail bites the dust")
	public void testEveryTrailAccountUpdateNoName() throws EntityNotFoundException, BaseException {
		final String expectedUsername = "schmidt";
		assertAccount(	ActivityImportServiceTestUtil.create().registerEveryTrailImportInstructions().build(),
						new AccountId(KnownApplication.EVERY_TRAIL.key, "245903"),
						expectedUsername,
						null,
						expectedUsername);
	}

	private void assertAccount(ActivityImportServiceTestUtil testUtil, AccountId expectedAccountId,
			String expectedUsername, String expectedAccountName) throws BaseException, EntityNotFoundException {
		assertAccount(testUtil, expectedAccountId, expectedUsername, expectedAccountName, expectedAccountName);
	}

	/**
	 * @param testUtil
	 * @param expectedAccountId
	 * @throws BaseException
	 * @throws EntityNotFoundException
	 */
	private void assertAccount(ActivityImportServiceTestUtil testUtil, AccountId expectedAccountId,
			String expectedUsername, String expectedAccountName, String expectedPersonName) throws BaseException,
			EntityNotFoundException {
		assertTrue("accounts not updatable for " + expectedAccountId, testUtil.getImportInstructionsRepo()
				.accountUpdatable(expectedAccountId));
		Account accountUpdated = testUtil.getActivityImportService().accountUpdated(expectedAccountId);
		TestUtil.assertContains(expectedUsername, accountUpdated.getAliases());
		Account found = testUtil.getPersonServiceTestUtil().getAccountService().account(expectedAccountId);
		TestUtil.assertContains(expectedUsername, found.getAliases());
		assertEquals(expectedAccountName, found.name());

		Person person = testUtil.getPersonServiceTestUtil().getPersonService().person(expectedAccountId);
		assertEquals(expectedPersonName, person.name());
	}
}
