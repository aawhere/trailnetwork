Tools relating to Google Cloud Storage file system.

> gsconfig

run gsconfig first to get an oauth token.

# CORS

> https://cloud.google.com/storage/docs/gsutil/commands/cors

Run this file on any environment that you need cors access. 

> cors-all.json

allows access to all of our environments.  You must run the command targeting the bucket...

> gsutil cors set cors-all.json gs://tn-api-dev.appspot.com

