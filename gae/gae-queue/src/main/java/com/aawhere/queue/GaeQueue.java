/**
 *
 */
package com.aawhere.queue;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aawhere.backends.Backend;
import com.aawhere.backends.BackendUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;
import com.aawhere.ws.rs.MediaTypes;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueConstants;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.QueueStatistics;
import com.google.appengine.api.taskqueue.TaskAlreadyExistsException;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.google.common.base.Charsets;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.google.common.net.HttpHeaders;
import com.google.inject.Singleton;

/**
 * An implementation of Queue that specifically works for Google App Engine.
 * 
 * @author Brian Chapman
 * 
 */
public class GaeQueue
		implements com.aawhere.queue.Queue {

	/**
	 * The delay can only go out so long and we take alittle extra off for a buffer so we don't
	 * exceed their limits
	 */
	private static final long MAX_ETA_DELTA_MILLIS = QueueConstants.getMaxEtaDeltaMillis() - 10000;
	/**
	 * 
	 */
	private static Logger LOG = LoggerFactory.getLogger(GaeQueue.class);
	private static Long GAE_ETA_CONVERSION_FACTOR = 1000l;

	/** Used to give a time definition to named tasks by appending to the name. */
	private static final String TIME_SUFFIX;

	static {
		// Daily is generally good enough. Crossing midnight will void all names which is fine once
		// per day.
		// this is static because it is assumed the servers will restart more often than once per
		// day.
		SimpleDateFormat format = new SimpleDateFormat(Task.RepeatFrequency.DAILY.pattern);
		Date date = new Date();
		TIME_SUFFIX = format.format(date);
	}

	/**
	 * Used to construct all instances of GaeQueue.
	 */
	public static class Builder
			extends ObjectBuilder<GaeQueue> {

		public Builder() {
			super(new GaeQueue());
		}

		public Builder setToDefault() {
			building.queue = QueueFactory.getDefaultQueue();
			return this;
		}

		public Builder lastTaskEtaAlwaysReturnsNow() {
			building.lastTaskEtaAlwaysReturnsNow = Boolean.TRUE;
			return this;
		}

		public Builder setName(String name) {
			building.queue = QueueFactory.getQueue(name);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("queue", building.queue);
		}

	}// end Builder

	private Queue queue;
	// TN-661 used to store statistics for a set period to avoid problems accessing the stats with
	// GAE.
	private static final String STATS_CACHE_KEY = "stats";
	private Cache<String, QueueStatistics> statsCache = CacheBuilder.newBuilder()
			.expireAfterWrite(30, TimeUnit.SECONDS).build();

	@Singleton
	public static class Producer
			implements com.aawhere.queue.QueueFactory.Producer {

		private static final long serialVersionUID = 8067806652542386749L;

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.queue.QueueFactory.Producer#createQueue(java.lang.String)
		 */
		@Override
		public com.aawhere.queue.Queue getQueue(String queueName) {
			return new Builder().setName(queueName).build();
		}

	}

	/** Use {@link Builder} to construct GaeQueue */
	private GaeQueue() {
	}

	private Boolean lastTaskEtaAlwaysReturnsNow = Boolean.FALSE;

	public GaeQueue(Queue queue) {
		this.queue = queue;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.queue.Queue#add(com.aawhere.queue.Parameter<?>[])
	 */
	@Override
	public Result add(Task task) {
		return add(Lists.newArrayList(task), false);
	}

	@Override
	public Result add(Iterable<Task> tasks, Boolean asynch) {
		// hashmap ensures request uniqueness within the batch
		HashMap<String, TaskOptions> allOptions = new HashMap<>();
		for (Task task : tasks) {

			String endpointString = task.getEndPoint().getUri().toString();
			TaskOptions options = TaskOptions.Builder.withUrl(endpointString);

			// unique name avoids duplicate task entries TN-207
			/*
				 */
			// named tasks reject duplicates within some undefined time frame (like a week).
			if (!task.isRepeatsAllowed()) {
				String timeSuffix;

				if (task.hasRepeatFrequencyAllowed()) {
					DateTimeFormatter formatter = DateTimeFormat.forPattern(task.getRepeatFrequencyAllowed().pattern);
					timeSuffix = formatter.print(DateTime.now());
				} else {
					timeSuffix = TIME_SUFFIX;
				}
				String unformattedTaskName = new StringBuilder().append(endpointString).append("-").append(timeSuffix)
						.toString();
				String taskName = unformattedTaskName.replaceAll("[^0-9a-zA-Z\\-\\_]", "-");
				options.taskName(taskName);
			}

			if (task.hasCountdown()) {
				// eta shows up in the console, delay does not leaving a mystery.
				// they are pretty much the same thing?
				// http://stackoverflow.com/questions/14987039/whats-the-most-effective-way-to-execute-a-task-some-distant-time-in-the-future/26576612#26576612
				long millis = Math.min(MAX_ETA_DELTA_MILLIS, task.getCountdownMillis());
				options.countdownMillis(millis);
			}

			// TN-329 this should be provided by the task so the application can decide, not
			// this generic library
			options = BackendUtil.setBackendTarget(options, Backend.PROCESS, task.isVersionTargeted());

			for (Parameter<?> param : task.getParams()) {
				if (param instanceof StringParameter) {
					options.param(param.getName(), (String) param.getValue());
				} else {
					options.param(param.getName(), param.getValueAsByte());
				}
			}
			switch (task.getMethod()) {
				case POST:
					options.method(Method.POST);
					break;
				case GET:
					options.method(Method.GET);
					break;
				case PUT:
					options.method(Method.PUT);
					break;
				default:
					throw new InvalidChoiceException(task.getMethod());
			}
			// transfer all headers
			Set<Entry<String, String>> headers = task.getHeaders().entrySet();
			for (Entry<String, String> entry : headers) {
				options.header(entry.getKey(), entry.getValue());
			}

			// TN-659 the queue doesn't care about the body, only the status
			options.header(HttpHeaders.ACCEPT, MediaTypes.APPLICATION_NONE);
			if (task.hasPayload()) {
				if (task.hasStringPayload()) {
					options.payload(task.getStringPayload(), Charsets.UTF_8.toString());
				} else {
					options.payload(task.getBinaryPayload(), task.getPayloadContentType().toString());
				}
			}
			// putting by endpoint ensures repeats won't be allowed
			if (allOptions.put(endpointString, options) != null) {
				LoggerFactory.getLogger(getClass()).warning(endpointString + " already requested in same batch");
			}
		}

		Result result;
		Collection<TaskOptions> allUniqueOptions = allOptions.values();
		if (asynch) {
			addAsynch(allUniqueOptions);
			result = Result.ASYNCH;
		} else {
			result = add(allUniqueOptions);
		}

		return result;
	}

	/**
	 * Asynchronously adds the taskst to the queue, but if there is a duplicate task detected then
	 * this will make another attempt synchronously since synchrnous handling of duplicate task
	 * names is better.
	 * 
	 * Asynchronously submits tasks to this queue. Submission is not atomic i.e. if this method
	 * fails then some tasks may have been added to the queue.
	 * 
	 * @param allOptions
	 */
	private void addAsynch(Collection<TaskOptions> allOptions) {
		// any exception thrown here is likely not recoverable.
		// TaskAlreadyExistsException is not being thrown
		queue.addAsync(allOptions);
	}

	/**
	 * According to the docs, this will queue the others. Asynch does not.
	 * 
	 * TaskAlreadyExistsException - If any of the provided TaskOptions contained a name of a task
	 * that was previously created, and if no other Exception would be thrown. Note that if a
	 * TaskAlreadyExistsException is caught, the caller can be guaranteed that for each one of the
	 * provided TaskOptions, either the corresponding task was successfully added, or a task with
	 * the given name was successfully added in the past.
	 * 
	 * @param allOptions
	 * @return
	 */
	private Result add(Collection<TaskOptions> allOptions) {
		try {
			queue.add(allOptions);
			return Result.ADDED;
		} catch (TaskAlreadyExistsException e) {
			// catching runtime is bad, but they provide no preventative alternative
			// that's fine...just ignore
			// TN-207
			LoggerFactory.getLogger(getClass()).fine("A task has recently existed in the queue TN-207: "
					+ e.getMessage());
			return Result.DUPLICATE;
		}
	}

	@Override
	public Integer size() {
		return stats().getNumTasks();
	}

	/**
	 * 
	 */
	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.queue.Queue#lastTaskEta()
	 */
	@Override
	public DateTime lastTaskEta() {
		Long eta = null;
		if (!lastTaskEtaAlwaysReturnsNow) {
			eta = stats().getOldestEtaUsec();
		}
		if (eta == null) {
			return DateTime.now();
		} else {
			return new DateTime(eta / GAE_ETA_CONVERSION_FACTOR);
		}
	}

	/**
	 * Internal method used to gather the stats...either in the cache if they are fresh or from the
	 * gae queue directly.
	 * 
	 * TN-661 introduced the need for this.
	 * 
	 * @since 25
	 * @return
	 */
	QueueStatistics stats() {
		QueueStatistics statistics = this.statsCache.getIfPresent(STATS_CACHE_KEY);
		if (statistics == null) {
			try {
				statistics = this.queue.fetchStatistics();
				this.statsCache.put(STATS_CACHE_KEY, statistics);
			} catch (Exception e) {
				LOG.warning(e.getMessage());
				throw QueueTaskRetryException.statsUnavailable(this.queue.getQueueName());
			}
		}
		return statistics;
	}

}
