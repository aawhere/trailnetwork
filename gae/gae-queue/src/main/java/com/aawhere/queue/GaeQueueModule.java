/**
 * 
 */
package com.aawhere.queue;

import com.google.inject.AbstractModule;

/**
 * Sets up the generic {@link QueueFactory} to use specific {@link GaeQueue} implementations.
 * 
 * @author aroller
 * 
 */
public class GaeQueueModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(QueueFactory.Producer.class).to(GaeQueue.Producer.class);

	}

}
