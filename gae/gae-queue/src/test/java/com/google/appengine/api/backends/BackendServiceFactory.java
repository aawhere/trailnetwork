/**
 * 
 */
package com.google.appengine.api.backends;

import com.google.appengine.tools.development.testing.LocalServiceTestConfig;

/**
 * Google doesn't provide us a {@link LocalServiceTestConfig} for backends. So this is necessary.
 * 
 * @author Brian Chapman
 * 
 */
public class BackendServiceFactory {

	public static BackendService getBackendService() {
		return new MockBackendService();
	}

	public static class MockBackendService
			implements BackendService {

		/*
		 * (non-Javadoc)
		 * @see com.google.appengine.api.backends.BackendService#getBackendAddress(java.lang.String)
		 */
		@Override
		public String getBackendAddress(String arg0) {
			//
			return "AnyValueWorksHere";
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.appengine.api.backends.BackendService#getBackendAddress(java.lang.String,
		 * int)
		 */
		@Override
		public String getBackendAddress(String arg0, int arg1) {
			// TODO Auto-generated method stub
			return null;
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.appengine.api.backends.BackendService#getCurrentBackend()
		 */
		@Override
		public String getCurrentBackend() {
			// TODO Auto-generated method stub
			return null;
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.appengine.api.backends.BackendService#getCurrentInstance()
		 */
		@Override
		public int getCurrentInstance() {
			// TODO Auto-generated method stub
			return 0;
		}

	}
}