/**
 *
 */
package com.aawhere.queue;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.io.Marshal;
import com.aawhere.persist.GaeServiceTestHelper;
import com.aawhere.queue.Queue.Result;
import com.aawhere.queue.Task.RepeatFrequency;
import com.aawhere.test.category.TaskQueueTest;

import com.google.appengine.api.taskqueue.QueueStatistics;
import com.google.appengine.api.taskqueue.dev.QueueStateInfo;
import com.google.appengine.api.taskqueue.dev.QueueStateInfo.HeaderWrapper;
import com.google.appengine.api.taskqueue.dev.QueueStateInfo.TaskStateInfo;
import com.google.appengine.repackaged.com.google.common.collect.Lists;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.net.HttpHeaders;

/**
 * @author brian
 * 
 */
@Category(TaskQueueTest.class)
public class GaeQueueUnitTest {

	LocalServiceTestHelper helper = GaeServiceTestHelper.createTestHelper().withTaskQueueService().build();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		helper.setUp();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		helper.tearDown();
	}

	@Test
	public void testAdd() {
		Queue q = new GaeQueue.Builder().setToDefault().build();
		Task task = QueueTestUtil.createTaskWithBinaryPayload();
		q.add(task);

		QueueStateInfo qsi = GaeQueueTestUtil.assertOneEntryInDefaultQueue();

		ArrayList<String> payloadResult = Marshal.unMarshal(GaeQueueTestUtil.getBinaryPayload(qsi));
		assertEquals(QueueTestUtil.DEFAULT_PAYLOAD, payloadResult);

	}

	@Test
	public void testAddBulk() {
		final boolean asynch = false;
		ArrayList<Task> list = assertBulk(asynch);
		GaeQueueTestUtil.assertEntriesInDefaultQueue(list.size());
	}

	@Test
	public void testMonthly() {
		GaeQueue queue = GaeQueue.create().setToDefault().build();
		Task task = Task.create().setRepeatFrequencyAllowed(RepeatFrequency.MONTHLY)
				.setEndPoint(QueueTestUtil.getEndPoint()).build();
		queue.add(task);

	}

	/**
	 * @param asynch
	 * @return
	 */
	private ArrayList<Task> assertBulk(final boolean asynch) {
		ArrayList<Task> list = Lists
				.newArrayList(	QueueTestUtil.createTaskWithParameters(QueueTestUtil.getEndPoint()),
								QueueTestUtil.createTaskWithParameters(QueueTestUtil.getEndPoint2()));
		GaeQueue queue = GaeQueue.create().setToDefault().build();
		queue.add(list, asynch);
		return list;
	}

	@Test
	public void testAddBulkAsynch() throws InterruptedException {
		assertAsynchQueue(assertBulk(true));

	}

	/**
	 * @param list
	 * @throws InterruptedException
	 */
	private void assertAsynchQueue(List<Task> list) throws InterruptedException {
		int count = 0;
		// wait at least once to give the queue a chance
		do {
			count++;
			Thread.sleep(100 * count);
		} while (GaeQueueTestUtil.getQueueSize() != list.size() && count < 10);
		GaeQueueTestUtil.assertEntriesInDefaultQueue(list.size());
	}

	/**
	 * TN-391
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testAddDuplicateInSameRequest() throws InterruptedException {
		assertDuplicateInSameRequest(true);
		assertDuplicateInSameRequest(false);
	}

	@Test
	public void testCountDown() {
		Long etaMillis = 1234566l;
		Task task = QueueTestUtil.createTaskWithCountdown(etaMillis);
		GaeQueue queue = GaeQueue.create().setToDefault().build();
		queue.add(task);

	}

	/**
	 * @param asynch
	 * @throws InterruptedException
	 */
	private void assertDuplicateInSameRequest(final boolean asynch) throws InterruptedException {
		Queue queue = GaeQueueTestUtil.getDefaultQueue();
		Task task = Task.create().setEndPoint(QueueTestUtil.getEndPoint()).setAllowRepeats(false).build();
		final ArrayList<Task> expected = Lists.newArrayList(task);
		queue.add(expected, asynch);
		// only one gets kept, and the duplicate should just be ignored.
		assertAsynchQueue(Lists.newArrayList(task));
	}

	/**
	 * TN-207
	 */
	@Test
	public void testAddDuplicate() {
		Task task = QueueTestUtil.createTaskWithParameters();
		Queue queue = GaeQueueTestUtil.getDefaultQueue();
		assertEquals(Result.ADDED, queue.add(task));
		GaeQueueTestUtil.assertEntriesInDefaultQueue(1);
		assertEquals(Result.ADDED, queue.add(task));
		GaeQueueTestUtil.assertEntriesInDefaultQueue(2);

	}

	/**
	 * TN-207
	 */
	@Test
	public void testAddDuplicateNotAllowed() {
		Queue queue = GaeQueueTestUtil.getDefaultQueue();
		Task task = Task.create().setEndPoint(QueueTestUtil.getEndPoint()).setAllowRepeats(false).build();
		assertEquals(Result.ADDED, queue.add(task));
		GaeQueueTestUtil.assertEntriesInDefaultQueue(1);
		assertEquals(Result.DUPLICATE, queue.add(task));
		GaeQueueTestUtil.assertEntriesInDefaultQueue(1);

	}

	/**
	 * Perhaps not a perfect test. This will make two attempts at the asynchronous queue checking
	 * that only a single task is added.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testAddDuplicateNotAllowedAsynch() throws InterruptedException {
		final boolean asynch = true;
		Queue queue = GaeQueueTestUtil.getDefaultQueue();
		Task task = Task.create().setEndPoint(QueueTestUtil.getEndPoint()).setAllowRepeats(false).build();
		ArrayList<Task> list = Lists.newArrayList(task);
		assertEquals(Result.ASYNCH, queue.add(list, asynch));
		assertAsynchQueue(list);
		assertEquals(Result.ASYNCH, queue.add(list, asynch));
		assertAsynchQueue(list);

	}

	@Test
	public void testAddHeaders() {
		GaeQueue queue = (GaeQueue) GaeQueueTestUtil.getDefaultQueue();
		Task task = QueueTestUtil.createTaskWithHeaders();
		queue.add(task);
		QueueStateInfo qsi = GaeQueueTestUtil.assertOneEntryInDefaultQueue();
		List<TaskStateInfo> taskInfo = qsi.getTaskInfo();
		TaskStateInfo taskStateInfo = taskInfo.get(0);
		List<HeaderWrapper> headers = taskStateInfo.getHeaders();
		boolean header1Exists = false;
		boolean header2Exists = false;
		boolean hostHeaderExists = false;
		for (HeaderWrapper headerWrapper : headers) {
			header1Exists |= headerWrapper.getKey().equals(QueueTestUtil.TEST_HEADER_KEY1)
					&& headerWrapper.getValue().equals(QueueTestUtil.TEST_HEADER_VALUE1);
			header2Exists |= headerWrapper.getKey().equals(QueueTestUtil.TEST_HEADER_KEY2)
					&& headerWrapper.getValue().equals(QueueTestUtil.TEST_HEADER_VALUE2);
			hostHeaderExists |= headerWrapper.getKey().equals(HttpHeaders.HOST);
		}
		assertTrue("header1", header1Exists);
		assertTrue("header2", header2Exists);
		assertTrue("backends host is missing", hostHeaderExists);

	}

	@Test
	public void testLastTaskEta() {
		GaeQueue queue = (GaeQueue) GaeQueueTestUtil.getDefaultQueue();
		Task task = QueueTestUtil.createTaskWithHeaders();
		queue.add(task);
		DateTime eta = queue.lastTaskEta();
		assertNotNull(eta);
		// TODO: force the ETA old enough to test.
	}

	@Test
	public void testStatsCache() {
		GaeQueue queue = (GaeQueue) GaeQueueTestUtil.getDefaultQueue();
		QueueStatistics stats = queue.stats();
		assertSame("cache isn't producing the same stats", stats, queue.stats());
		// TODO:make sure cache time's out, but that's a long painful test!
	}

}
