/**
 * 
 */
package com.aawhere.queue;

import static org.junit.Assert.*;

import com.aawhere.test.TestUtil;

import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.dev.LocalTaskQueue;
import com.google.appengine.api.taskqueue.dev.QueueStateInfo;
import com.google.appengine.api.taskqueue.dev.QueueStateInfo.TaskStateInfo;
import com.google.appengine.tools.development.testing.LocalTaskQueueTestConfig;

/**
 * Useful utilities that will interact with the GAE Queue. Specifically, {@link QueueStateInfo} is
 * used to inspect the queue.
 * 
 * @author aroller
 * 
 */
public class GaeQueueTestUtil {

	/**
	 * Shorthand for a common situation where one entry should be in the default queue.
	 * 
	 * @return
	 */
	public static QueueStateInfo assertOneEntryInDefaultQueue() {
		return assertEntriesInDefaultQueue(1);
	}

	/**
	 * Looks at the ccurrent queue expecting to find the given number of entries in the given queue.
	 * 
	 * @param expectedCount
	 * @return
	 */
	public static QueueStateInfo assertEntriesInDefaultQueue(Integer expectedCount) {
		QueueStateInfo qsi = getQsi();
		assertEquals(qsi.getTaskInfo().toString(), expectedCount, getQueueSize());
		return qsi;
	}

	/**
	 * Runs the {@link Task}s in the default queue.
	 */
	public static void flush() {
		LocalTaskQueue ltq = LocalTaskQueueTestConfig.getLocalTaskQueue();
		ltq.flushQueue(QueueFactory.getDefaultQueue().getQueueName());
	}

	/**
	 * This assumes you marshalled it in the first place.
	 * 
	 * @param qsi
	 * @return
	 */
	public static byte[] getBinaryPayload(QueueStateInfo qsi) {
		byte[] payloadResult = qsi.getTaskInfo().get(0).getBodyAsBytes();
		return payloadResult;
	}

	/**
	 * @return
	 */
	public static com.aawhere.queue.QueueFactory createFactory() {
		return new com.aawhere.queue.QueueFactory(new GaeQueueTestProducer());
	}

	/**
	 * Provides a default queue for testing. A convienience to calling the factory yourself.
	 * 
	 * @return
	 */
	public static Queue getDefaultQueue() {
		return createFactory().getQueue(null);

	}

	public static void assertQueueContains(Queue queue, Integer expectedSize, String... expectedInEndpoint) {
		QueueStateInfo qsi = getQsi();

		if (expectedSize != null) {
			assertEquals("wrong queue size ", expectedSize.intValue(), qsi.getCountTasks());
		}
		for (TaskStateInfo tsi : qsi.getTaskInfo()) {
			String endpoint = tsi.getUrl();
			for (int i = 0; i < expectedInEndpoint.length; i++) {
				String string = expectedInEndpoint[i];
				TestUtil.assertContains(endpoint, string);

			}
		}
	}

	/**
	 * @return
	 */
	public static Integer getQueueSize() {
		return getQsi().getCountTasks();
	}

	/**
	 * Usefull for troubleshooting.
	 */
	public static void printTasksInDefaultQueue() {
		System.out.println("Default Task Queue Count: " + getQueueSize());
		for (TaskStateInfo task : getQsi().getTaskInfo()) {
			System.out.println("Task: " + task.getUrl() + ", " + task.getBody());
		}
	}

	private static QueueStateInfo getQsi() {
		LocalTaskQueue ltq = LocalTaskQueueTestConfig.getLocalTaskQueue();
		QueueStateInfo qsi = ltq.getQueueStateInfo().get(QueueFactory.getDefaultQueue().getQueueName());
		return qsi;
	}

	/**
	 * Testing doesn't use the queue.xml in the web application so just send everything to the
	 * default.
	 * 
	 * We could use:
	 * 
	 * new LocalTaskQueueTestConfig().setQueueXmlPath(queueName)
	 * 
	 * This seems a bit fragile pointing to the file system or making classes higher in the reactor
	 * depend on the web app so just test against the default.
	 * 
	 * @author aroller
	 * 
	 */
	public static class GaeQueueTestProducer
			implements com.aawhere.queue.QueueFactory.Producer {

		private static final long serialVersionUID = 3990346967024831861L;

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.queue.QueueFactory.Producer#getQueue(java.lang.String)
		 */
		@Override
		public Queue getQueue(String queueName) {

			return new GaeQueue.Builder().setToDefault().lastTaskEtaAlwaysReturnsNow().build();
		}

	}

}
