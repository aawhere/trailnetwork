/**
 * 
 */
package com.aawhere.queue;

import org.junit.After;
import org.junit.Before;
import org.junit.experimental.categories.Category;

import com.aawhere.persist.GaeServiceTestHelper;
import com.aawhere.test.category.TaskQueueTest;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author aroller
 * 
 */
@Category(TaskQueueTest.class)
public class GaeBatchQueueUnitTest
		extends BatchQueueUnitTest {

	LocalServiceTestHelper helper = GaeServiceTestHelper.createTestHelper().withTaskQueueService().build();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		helper.setUp();
		super.setUp(GaeQueueTestUtil.getDefaultQueue());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		helper.tearDown();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.queue.BatchQueueUnitTest#queueSize(com.aawhere.queue.Queue)
	 */
	@Override
	protected Integer queueSize(Queue queue) {
		return GaeQueueTestUtil.getQueueSize();
	}

}
