package com.aawhere.persist;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;

import com.aawhere.lang.AbstractObjectBuilder;
import com.google.appengine.tools.development.testing.LocalAppIdentityServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalBlobstoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalFileServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalMemcacheServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalSearchServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.appengine.tools.development.testing.LocalTaskQueueTestConfig;

/*
 * GAE datastore service test helper.
 * All tests using the datastore should extend this class
 * and call the setuUp and tearDown methods.
 *
 * e.g.
 *
 * public class MyDatastoreTest extends LocalDatastoreServiceTestHelper {
 *   @Before
 *   public void setUp() throws Exception {
 *     super.setUp();
 *   }
 *
 *   @After
 *   public void tearDown() throws Exception {
 *     super.tearDown();
 * 	 }
 * }
 * @author Brian Chapman
 */
public class GaeServiceTestHelper {

	protected LocalServiceTestHelper helper = GaeServiceTestHelper.createTestHelper().withDatastoreService()
			.withSearchService().build();
	private Set<LocalServiceTestConfig> configs;

	@Before
	public void gaeServiceTestHelperSetUp() throws Exception {
		helper.setUp();
	}

	@After
	public void gaeServiceTestHelperTearDown() throws Exception {
		helper.tearDown();
	}

	/**
	 * @return the helper
	 */
	public LocalServiceTestHelper getHelper() {
		return this.helper;
	}

	/**
	 * Get all configured {@link LocalServiceTestConfig}s.
	 * 
	 * @return the configs
	 */
	public Set<LocalServiceTestConfig> getConfigs() {
		return this.configs;
	}

	public static class GaeServiceTestHelperBuilder
			extends BaseBuilder<GaeServiceTestHelperBuilder, GaeServiceTestHelper> {

		/**
		 * @param helper
		 */
		protected GaeServiceTestHelperBuilder() {
			super(new GaeServiceTestHelper());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public GaeServiceTestHelper build() {
			building.helper = new LocalServiceTestHelper(configsToArray());
			building.configs = configs;
			return super.build();
		}
	}

	public static class Builder
			extends BaseBuilder<Builder, LocalServiceTestHelper> {

		public Builder() {
			super(new LocalServiceTestHelper());
		}

		@Override
		public LocalServiceTestHelper build() {
			return new LocalServiceTestHelper(configsToArray());
		}
	}

	public static class BaseBuilder<BuilderT extends BaseBuilder<BuilderT, HelperT>, HelperT>
			extends AbstractObjectBuilder<HelperT, BuilderT> {

		Set<LocalServiceTestConfig> configs = new HashSet<LocalServiceTestConfig>();

		protected BaseBuilder(HelperT helper) {
			super(helper);
		}

		public BuilderT withDatastoreService() {
			LocalDatastoreServiceTestConfig config = new LocalDatastoreServiceTestConfig();
			config.setApplyAllHighRepJobPolicy();
			return withDatastoreService(config);
		}

		public BuilderT withDatastoreService(LocalDatastoreServiceTestConfig config) {
			configs.add(config);
			return dis;
		}

		public BuilderT withMemcacheService() {
			configs.add(new LocalMemcacheServiceTestConfig());
			return dis;
		}

		public BuilderT withAppIdentityService() {
			configs.add(new LocalAppIdentityServiceTestConfig());
			return dis;
		}

		public BuilderT withBlobstoreService() {
			configs.add(new LocalBlobstoreServiceTestConfig());
			return dis;
		}

		public BuilderT withFileService() {
			configs.add(new LocalFileServiceTestConfig());
			return dis;
		}

		public BuilderT withTaskQueueService() {
			LocalTaskQueueTestConfig q = new LocalTaskQueueTestConfig();
			configs.add(q);
			return dis;
		}

		public BuilderT withSearchService() {
			configs.add(new LocalSearchServiceTestConfig());
			return dis;
		}

		public LocalServiceTestConfig[] configsToArray() {
			return configs.toArray(new LocalServiceTestConfig[configs.size()]);
		}

	}

	public static Builder createTestHelper() {
		return new Builder();
	}

}
