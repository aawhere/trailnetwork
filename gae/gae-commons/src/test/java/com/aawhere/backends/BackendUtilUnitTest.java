/**
 * 
 */
package com.aawhere.backends;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests {@link BackendUtil}.
 * 
 * @author aroller
 * 
 */
public class BackendUtilUnitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testBackendNames() {

		Backend[] values = Backend.values();
		for (int i = 0; i < values.length; i++) {
			Backend backend = values[i];
			String nameVersioned = BackendUtil.getNameVersioned(backend);
			assertNotNull(nameVersioned);
			String name = BackendUtil.getName(backend);
			assertNotNull(name);

		}
	}

}
