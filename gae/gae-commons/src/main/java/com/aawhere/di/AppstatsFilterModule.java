package com.aawhere.di;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.aawhere.lang.If;
import com.aawhere.log.LoggerFactory;
import com.google.appengine.tools.appstats.AppstatsServlet;
import com.google.inject.Guice;
import com.google.inject.Singleton;
import com.google.inject.servlet.ServletModule;

/**
 * Add an {@link AppstatsFilterModule} and {@link AppstatsServlet} to your {@link Guice} config.
 * 
 * @author Brian Chapman
 * 
 */
public class AppstatsFilterModule
		extends ServletModule {

	private static final String APPSTATS_PATH = "/appstats/*";
	private static final String SYSTEM_PROP_ENABLED_KEY = "com.aawhere.di.appstatsFilter.enabled";
	private static final String SYSTEM_PROP_ENABLED_DEFAULT = Boolean.FALSE.toString();

	/**
	 * Setup GAE Appstats Administrative Interface
	 */
	@Override
	protected void configureServlets() {
		String enabled = System.getProperty(SYSTEM_PROP_ENABLED_KEY);
		if (Boolean.valueOf(If.nil(enabled).use(SYSTEM_PROP_ENABLED_DEFAULT))) {
			bind(com.google.appengine.tools.appstats.AppstatsFilter.class).in(Singleton.class);
			filterRegex("^/(?!appstats|_ah).*").through(com.google.appengine.tools.appstats.AppstatsFilter.class);

			bind(com.google.appengine.tools.appstats.AppstatsServlet.class).in(Singleton.class);
			Map<String, String> params = new HashMap<String, String>();

			params.put("calculateRpcCosts", Boolean.TRUE.toString());
			serve(APPSTATS_PATH).with(com.google.appengine.tools.appstats.AppstatsServlet.class, params);
			Logger logger = LoggerFactory.getLogger(getClass());
			StringBuilder message = new StringBuilder();
			message.append("\n***********************************************************************************");
			message.append("\n****************** Appstats is running.  The system may be slow *******************");
			message.append("\n***********************************************************************************");
			logger.warning(message.toString());

		}

	}

}
