/**
 * 
 */
package com.aawhere.backends;

import java.util.Properties;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.ResourceUtils;
import com.aawhere.log.LoggerFactory;

import com.google.appengine.api.backends.BackendServiceFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskQueuePb.TaskQueueQueryTasksResponse.Task;
import com.google.appengine.api.utils.SystemProperty;
import com.google.common.net.HttpHeaders;

/**
 * Utililties for working with Google App Engine Backends.
 * 
 * @author Brian Chapman
 * 
 */
public class BackendUtil {
	private static final String VERSIONED_SUFFIX = "versioned";
	private static final Properties properties;
	private static final Boolean DEV_MODE;
	/**
	 * 
	 */
	static {
		DEV_MODE = SystemProperty.environment.value() == SystemProperty.Environment.Value.Development;
		Properties names;
		try {
			// load your properties file from your app
			names = ResourceUtils.loadProperties(Backend.class);
		} catch (Exception e) {
			names = new Properties();
			Backend[] values = Backend.values();
			for (int i = 0; i < values.length; i++) {
				Backend backend = values[i];
				names.put(backend.name(), backend.name());
			}
			LoggerFactory.getLogger(BackendUtil.class).warning("properties not found...using default backend names "
					+ names);
		}
		properties = names;
	}

	/**
	 * Sets the {@link Task} to use a backend for processing. The workaround for the development
	 * environment is necessary as google doesn't have a way of dealing with backends in
	 * development. @See
	 * http://stackoverflow.com/questions/16142025/junit-for-gae-is-there-localtestconfig
	 * -for-backend-service
	 * 
	 * @See https://code.google.com/p/googleappengine/issues/detail?id=9235
	 * 
	 * @param options
	 * @param backend
	 * @return
	 */
	public static TaskOptions setBackendTarget(TaskOptions options, Backend backend) {
		return setBackendTarget(options, backend, true);
	}

	public static TaskOptions setBackendTarget(TaskOptions options, Backend backend, Boolean includeVersion) {

		if (!DEV_MODE) {
			String name = (BooleanUtils.isTrue(includeVersion)) ? getNameVersioned(backend) : getName(backend);
			// FIXME:use ModuleService, maybe get the version from the service
			options.header(HttpHeaders.HOST, BackendServiceFactory.getBackendService().getBackendAddress(name));
		}
		return options;
	}

	/**
	 * Given a Backend Enumeration this will return the configured module name with it's version to
	 * be targeted.
	 * 
	 * @param backend
	 * @return
	 */
	public static String getNameVersioned(Backend backend) {
		return properties.getProperty(IdentifierUtils.createCompositeIdValue(backend.name(), VERSIONED_SUFFIX));
	}

	/**
	 * Given the {@link Backend} this provide the name of the module without version information.
	 * 
	 * @param backend
	 * @return
	 */
	public static String getName(Backend backend) {
		return properties.getProperty(backend.name());
	}
}
