/**
 * 
 */
package com.aawhere.backends;

/**
 * List of backends used in the architecture of the current project.
 * 
 * @author Brian Chapman
 * 
 */
public enum Backend {

	/** A general purpose backend server used for processing. */
	PROCESS;

}
