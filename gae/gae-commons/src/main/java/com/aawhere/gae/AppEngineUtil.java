package com.aawhere.gae;

import com.google.appengine.api.utils.SystemProperty;

public class AppEngineUtil {

	/**
	 * Indicates if this is running in the appengine devserver or at appspot.
	 * 
	 * 
	 * @return
	 */
	public static boolean atAppspot() {
		return SystemProperty.environment.value() == SystemProperty.Environment.Value.Production;
	}

	public static boolean onDevServer() {
		// could use this check, but it's an either or right now
		// return SystemProperty.environment.value() ==
		// SystemProperty.Environment.Value.Development;
		return !atAppspot();
	}
}
