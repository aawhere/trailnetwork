package com.googlecode.objectify.impl;

import com.google.appengine.api.datastore.Key;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.cache.CacheControl;

/**
 * Implements CacheControl for Objectify
 * 
 * This overrides the CacheControlImpl for Objectify 4.0.1, which precedes objectify 4.0b1 in maven
 * central (4.0.1 is an aawhere build before Objectify found out about releases and maven central).
 * 
 * 
 * 
 * @See TN-141
 * 
 */
public class CacheControlImpl
		implements CacheControl {
	/** */
	ObjectifyFactory fact;

	/** */
	public CacheControlImpl(ObjectifyFactory fact) {
		this.fact = fact;
	}

	/** */
	@Override
	public Integer getExpirySeconds(Key key) {
		try {
			return fact.getMetadata(key).getCacheExpirySeconds();
		} catch (NullPointerException e) {
			// expire the cache.
			return null;
		}
	}
}