/**
 * 
 */
package com.aawhere.cache;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;

import com.aawhere.persist.GaeServiceTestHelper;

import com.google.appengine.api.memcache.dev.LocalMemcacheService;
import com.google.appengine.tools.development.testing.LocalMemcacheServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * Tests the {@link CacheService} use of memcached.
 * 
 * @see LocalMemcacheServiceTestConfig
 * @see LocalMemcacheService
 * 
 * @author aroller
 * 
 */
@Ignore("this isn't working, but should inspire all to get it to work since caching isn't part of any service tests")
public class MemcachedServiceTest
		extends CacheServiceUnitTest {

	private LocalServiceTestHelper helper;

	@Before
	@Override
	public void setUp() {
		helper = GaeServiceTestHelper.createTestHelper().withDatastoreService().withMemcacheService().build();
		helper.setUp();
		cacheService = new CacheService();

	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

}
