/**
 * 
 */
package com.aawhere.lang.net;

import java.net.URI;
import java.net.URISyntaxException;

import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.googlecode.objectify.annotation.Entity;

/**
 * Test {@link UriTranslatorFactory}.
 * 
 * @author Brian Chapman
 * 
 */
public class UriTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<UriTranslatorFactoryUnitTest.UriTestEntity> {

	public UriTranslatorFactoryUnitTest() {
		super(new UriTranslatorFactory(), UriTestEntity.class);
	}

	@Entity
	public static class UriTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {
		public URI pojo;
	}

	private final String URI_STRING = "http://www.example.com";

	@Override
	public void testBasic() {
		UriTestEntity entity = new UriTestEntity();
		try {
			entity.pojo = new URI(URI_STRING);
			test(URI_STRING, entity);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}

	}
}
