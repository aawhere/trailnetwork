/**
 *
 */
package com.aawhere.persist;

import com.aawhere.field.FieldDictionaryFactory;

/**
 * @author Brian Chapman
 * 
 */
public class MockObjectifyFilterRepository
		extends
		ObjectifyFilterRepositoryDelegate<LongMockFilterEntities, LongMockFilterEntities.Builder, LongMockEntity, LongMockEntityId> {

	/**
	 * @param fieldDictionaryFactory
	 */
	public MockObjectifyFilterRepository(FieldDictionaryFactory fieldDictionaryFactory) {
		super(fieldDictionaryFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ObjectifyFilterRepository#getEntitiesBuilder()
	 */
	protected LongMockFilterEntities.Builder getEntitiesBuilder() {
		return new LongMockFilterEntities.Builder();
	}

}
