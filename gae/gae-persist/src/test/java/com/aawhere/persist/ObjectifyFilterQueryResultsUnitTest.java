/**
 * 
 */
package com.aawhere.persist;

import static com.aawhere.persist.ObjectifyRepositoryTestUtil.QueryResultMockIterable.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.persist.ObjectifyFilterQueryResults.Builder;
import com.aawhere.persist.ObjectifyRepositoryTestUtil.MockQuery;
import com.aawhere.persist.ObjectifyRepositoryTestUtil.QueryResultMockIterable;
import com.aawhere.test.TestUtil;

import com.google.appengine.api.datastore.DatastoreTimeoutException;
import com.google.common.base.Function;

/**
 * Provides granular tests to ensure the entitiesOut of {@link ObjectifyFilterQueryWriter} is
 * properly setup. Does not deal with entitiesOut processing since that is better left up to
 * Objectify repository testing.
 * 
 * The {@link #test()} method does most of the work and each test sets up the standard situation
 * using special indicators. The {@link QueryResultMockIterable}is set up to throw exceptions at
 * various points to ensure we handle them correctly in the {@link ObjectifyFilterQueryResults}.
 * 
 * @author aroller
 * 
 */
public class ObjectifyFilterQueryResultsUnitTest {

	private static FieldAnnotationDictionaryFactory fieldDictionaryFactory;

	@BeforeClass
	public static void setUpClass() {
		fieldDictionaryFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		fieldDictionaryFactory.getDictionary(LongMockEntity.class);
	}

	private ArrayList<MockQuery> queries = new ArrayList<MockQuery>();
	// provides the target query when calling query()
	private int queryIndex;
	private boolean tested;
	private ArrayList<LongMockEntity> entitiesIn;
	private ArrayList<Filter> filters;
	private ArrayList<LongMockEntity> entitiesOut;
	private Integer expectedNumberOfResults;
	private boolean handleTimeouts;
	private int expectedFilterCount;

	@Before
	public void setUp() {
		entitiesIn = new ArrayList<LongMockEntity>();
		filters = new ArrayList<Filter>();
		entitiesOut = new ArrayList<LongMockEntity>();
		handleTimeouts = false;
		tested = false;
		queryIndex = 0;
		expectedFilterCount = 1;
	}

	@After
	public void test() {
		if (!tested) {
			this.tested = true;

			if (this.queries.isEmpty()) {
				this.queries.add(new MockQuery(this.entitiesIn));
			}

			final Filter filter = Filter.create().filter();
			final Builder<LongMockEntity, LongMockEntityId> builder = new ObjectifyFilterQueryResults.Builder<LongMockEntity, LongMockEntityId>();
			if (this.handleTimeouts) {
				builder.continueAfterTimeout();
			}
			ObjectifyFilterQueryResults<LongMockEntity, LongMockEntityId> results = builder.filter(filter)
					.writerFunction(function()).build();

			while (results.hasNext()) {
				entitiesOut.add(results.next());
			}

			if (this.expectedNumberOfResults == null) {
				this.expectedNumberOfResults = entitiesIn.size();
			}

			assertEquals(filter.toString(), results.getFilter().toString());
			TestUtil.assertSize(this.expectedNumberOfResults, this.entitiesOut);
			assertEquals("incorrect count", this.expectedNumberOfResults, results.getResultsCount());

			assertEquals(this.expectedFilterCount, this.filters.size());
		}
	}

	@Test
	public void testNoResults() {

	}

	@Test
	public void testSingleEntity() {
		add();
	}

	@Test
	public void testDoubleEntity() {
		add();
		add();
	}

	@Test
	public void testTimeoutHandledOnNextSingleEntity() {
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_NEXT);
		// since hasnext already said yes, then there must be a next.
		add();
		this.expectedNumberOfResults = 1;
		this.handleTimeouts = true;
		this.expectedFilterCount = 2;
	}

	@Test(expected = DatastoreTimeoutException.class)
	public void testTimeoutNotHandledOnNext() {
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_NEXT);
		this.expectedNumberOfResults = 0;
		test();
	}

	@Test
	public void testTimeoutHandledOnNexMultipleTimes() {
		handleTimeoutsMultipleTimes(COUNT_MEANS_THROW_TIMEOUT_DURING_NEXT);
	}

	@Test
	public void testTimeoutHandledOnHasNexMultipleTimes() {
		handleTimeoutsMultipleTimes(COUNT_MEANS_THROW_TIMEOUT_DURING_HAS_NEXT);
	}

	@Test
	@Ignore("this test is too senstive now that we are quitting if no increment in count")
	public void testTimeoutHandledOnCreationMultipleTimes() {
		this.handleTimeouts = true;
		add();
		// must first cause a failure for creation to take place
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_NEXT);
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_ITERATOR_CREATION);
		add();
		add();
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_NEXT);
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_ITERATOR_CREATION);
		add();
		this.expectedNumberOfResults = 3;
		this.expectedFilterCount = 5;
	}

	/**
	 * @param action
	 */
	private void handleTimeoutsMultipleTimes(final int action) {
		this.handleTimeouts = true;
		add();
		add(action);
		add();
		add(action);
		add();
		add(action);
		add();
		this.expectedNumberOfResults = 4;
		this.expectedFilterCount = 4;
	}

	@Test(expected = DatastoreTimeoutException.class)
	public void testTimeoutNotHandledOnHasNext() {
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_HAS_NEXT);
		test();
	}

	@Test(expected = DatastoreTimeoutException.class)
	public void testTimeoutNotHandledOnIterator() {
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_ITERATOR_CREATION);
		test();
	}

	@Test
	public void testTimeoutHandledOnHasNextOnlyEntity() {
		add();
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_HAS_NEXT);
		this.handleTimeouts = true;
		this.expectedNumberOfResults = 1;
		this.expectedFilterCount = 2;
	}

	@Test(expected = DatastoreTimeoutException.class)
	public void testTimeoutHandledOnIteratorOnlyEntityNoIncrement() {
		// if the count hasn't received any then this will throw.
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_ITERATOR_CREATION);
		test();
	}

	@Test(expected = DatastoreTimeoutException.class)
	public void testTimeoutNotHandledOnIteratorSecondTry() {
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_ITERATOR_CREATION);
		add(COUNT_MEANS_THROW_TIMEOUT_DURING_ITERATOR_CREATION);
		// we must invoke the test since after won't throw the exception here
		test();
	}

	/**
	 * @param action
	 * @return
	 */
	private LongMockEntity add(final int action) {
		final LongMockEntity entity = EntityTestUtil.generateRandomLongEntity();
		this.entitiesIn.add(entity);
		entity.count = action;
		return entity;
	}

	/**
	 * @return
	 * 
	 */
	private LongMockEntity add() {
		return add(NO_ACTIONS);
	}

	public Function<Filter, ObjectifyFilterQueryWriter> function() {
		return new Function<Filter, ObjectifyFilterQueryWriter>() {

			@Override
			public ObjectifyFilterQueryWriter apply(Filter input) {
				filters.add(input);
				return ObjectifyFilterQueryWriter.create().setFilter(input).setEntityType(LongMockEntity.class)
						.setQuery(query()).setFieldDictionaryFactory(fieldDictionaryFactory.getFactory()).build();
			}
		};
	}

	public MockQuery query() {
		return this.queries.get(queryIndex);
	}
}
