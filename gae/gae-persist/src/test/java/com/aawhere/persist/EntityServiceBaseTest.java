/**
 * 
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.id.Identifier;
import com.aawhere.test.TestUtil;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * Base for other service tests that implement {@link EntityService}.
 * 
 * @author aroller
 * 
 */
abstract public class EntityServiceBaseTest<S extends EntityService<ES, E, I>, ES extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>> {

	private LocalServiceTestHelper helper;

	@Before
	public void setUpServices() {
		helper = GaePersistTestUtils.createRepositoryTestHelper();
		helper.setUp();
	}

	@After
	public void tearDownServices() {
		helper.tearDown();
	}

	abstract public S service();

	/**
	 * Provides a non-persisted pojo entity to be used with tests.
	 * 
	 * @see #persisted()
	 * 
	 * @return
	 */
	abstract public E entity();

	/**
	 * provides an id of that can identify the Entity, but not necessarily a persisted one.
	 * 
	 * Self identifying entities will be handled automatically. Others should override this and
	 * provide an id.
	 * 
	 * @return
	 */
	public I id() {
		I id = entity().id();
		if (id == null) {
			throw new IllegalArgumentException(
					"your entity is return no id...ovreride id() since your entity is apparently not self identifying");
		}
		return id;
	}

	/**
	 * Persists {@link #entity()}.
	 * 
	 * @return
	 */
	public E persisted() {
		E entity = entity();
		return persisted(entity);
	}

	/**
	 * @param entity
	 * @return
	 */
	public E persisted(E entity) {
		return service().getRepository().store(entity);
	}

	@Test
	public void testFunction() throws EntityNotFoundException {
		E first = persisted();
		assertTrue("doesn't exist? " + first, service().exists(first.id()));
		E second = persisted();
		service().existsEnforced(second.id());
		@SuppressWarnings("unchecked")
		ArrayList<I> ids = Lists.newArrayList(first.id(), second.id());
		Function<I, E> function = service().idFunction();
		List<E> entities = Lists.transform(ids, function);
		assertEquals(first, entities.get(Index.FIRST.index));
		assertEquals(second, entities.get(Index.SECOND.index));
	}

	@Test(expected = EntityNotFoundException.class)
	public void testExistsNot() throws EntityNotFoundException {
		I id = id();
		assertFalse("nothing persisted..nothing exists", service().exists(id));
		service().existsEnforced(id);
	}

	@Test(expected = EntityNotFoundException.class)
	public void testExistsMultipleNot() throws EntityNotFoundException {
		// one exists and the other does not.
		@SuppressWarnings("unchecked")
		ArrayList<I> ids = Lists.newArrayList(persisted().id(), id());
		service().existsEnforced(ids);
	}

	@Test
	public void testExistsMultiple() throws EntityNotFoundException {
		@SuppressWarnings("unchecked")
		ArrayList<I> expected = Lists.newArrayList(persisted().id(), persisted().id());
		Set<I> exists = service().existsEnforced(expected);
		TestUtil.assertIterablesEquals("exists didn't return what was asked", expected, exists);
	}

	@Test(expected = DeletionConfirmationException.class)
	public void testDeletionsNotConfirmed() throws DeletionConfirmationException {
		service().deleted(Filter.create().build());
	}

}
