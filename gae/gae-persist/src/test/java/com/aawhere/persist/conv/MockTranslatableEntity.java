/**
 * 
 */
package com.aawhere.persist.conv;

import java.lang.reflect.Field;
import java.util.Collection;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * Help tests when unit testing translators
 * 
 * @author Brian Chapman
 * 
 */
@Entity
public class MockTranslatableEntity {

	@Id
	public Long id;

	public String name = "a name";
	@Polymorphic
	public Animal animal = new Dog("Oby");
	public Integer integer = 3;
	public Class<?> type = String.class;
	@Polymorphic
	public Collection<Animal> animals;
	public Bike bike;

	public MockTranslatableEntity() {
	}

	public static Field typeField() {
		return fieldFor("type");
	}

	public static Field animalField() {
		return fieldFor("animal");
	}

	public static Field integerField() {
		return fieldFor("integer");
	}

	public static Field nameField() {
		return fieldFor("name");
	}

	public static Field idField() {
		return fieldFor("id");
	}

	public static Field animalsField() {
		return fieldFor("animals");
	}

	public static Field bikeField() {
		return fieldFor("bike");
	}

	private static Field fieldFor(String fieldName) {
		try {
			return MockTranslatableEntity.class.getField(fieldName);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.animal == null) ? 0 : this.animal.hashCode());
		result = prime * result + ((this.animals == null) ? 0 : this.animals.hashCode());
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		result = prime * result + ((this.integer == null) ? 0 : this.integer.hashCode());
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		result = prime * result + ((this.bike == null) ? 0 : this.bike.hashCode());
		result = prime * result + ((this.type == null) ? 0 : this.type.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MockTranslatableEntity))
			return false;
		MockTranslatableEntity other = (MockTranslatableEntity) obj;
		if (this.animal == null) {
			if (other.animal != null)
				return false;
		} else if (!this.animal.equals(other.animal))
			return false;
		if (this.animals == null) {
			if (other.animals != null)
				return false;
		} else if (!this.animals.equals(other.animals))
			return false;
		if (this.id == null) {
			if (other.id != null)
				return false;
		} else if (!this.id.equals(other.id))
			return false;
		if (this.integer == null) {
			if (other.integer != null)
				return false;
		} else if (!this.integer.equals(other.integer))
			return false;
		if (this.name == null) {
			if (other.name != null)
				return false;
		} else if (!this.name.equals(other.name))
			return false;
		if (this.bike == null) {
			if (other.bike != null)
				return false;
		} else if (!this.bike.equals(other.bike))
			return false;
		if (this.type == null) {
			if (other.type != null)
				return false;
		} else if (!this.type.equals(other.type))
			return false;
		return true;
	}

	public static class Animal {
		public String name;

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Animal))
				return false;
			Animal other = (Animal) obj;
			if (this.name == null) {
				if (other.name != null)
					return false;
			} else if (!this.name.equals(other.name))
				return false;
			return true;
		}

	}

	public static class Tiger
			extends Animal {
	}

	public static class Dog
			extends Animal {
		public String breed;

		public Dog(String name) {
			this.name = name;
		}

		public Dog() {

		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.breed == null) ? 0 : this.breed.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Dog))
				return false;
			Dog other = (Dog) obj;
			if (this.breed == null) {
				if (other.breed != null)
					return false;
			} else if (!this.breed.equals(other.breed))
				return false;
			return true;
		}
	}

	@Polymorphic
	public static class Bike {
		Integer numOfWheels = 2;
		Boolean suitableForDirt;

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.numOfWheels == null) ? 0 : this.numOfWheels.hashCode());
			result = prime * result + ((this.suitableForDirt == null) ? 0 : this.suitableForDirt.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Bike))
				return false;
			Bike other = (Bike) obj;
			if (this.numOfWheels == null) {
				if (other.numOfWheels != null)
					return false;
			} else if (!this.numOfWheels.equals(other.numOfWheels))
				return false;
			if (this.suitableForDirt == null) {
				if (other.suitableForDirt != null)
					return false;
			} else if (!this.suitableForDirt.equals(other.suitableForDirt))
				return false;
			return true;
		}
	}

	public static class MountainBike
			extends Bike {
		public MountainBike() {
			suitableForDirt = true;
		}
	}

	public static class Road
			extends Bike {
		Double tireWidth = 0.023; // meters

		public Road() {
			suitableForDirt = false;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((this.tireWidth == null) ? 0 : this.tireWidth.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (!(obj instanceof Road))
				return false;
			Road other = (Road) obj;
			if (this.tireWidth == null) {
				if (other.tireWidth != null)
					return false;
			} else if (!this.tireWidth.equals(other.tireWidth))
				return false;
			return true;
		}
	}
}
