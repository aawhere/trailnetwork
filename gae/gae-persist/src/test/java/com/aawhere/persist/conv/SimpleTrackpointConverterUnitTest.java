//package com.aawhere.persist.conv;
//
//import java.util.ArrayList;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import com.aawhere.measure.GeoCoordinate;
//import com.aawhere.measure.MeasurementUtil;
//import com.aawhere.measure.QuantityTranslatorFactory;
//import com.aawhere.track.SimpleTrackpoint;
//import com.aawhere.track.TrackUtil;
//
//import com.googlecode.objectify.ObjectifyService;
//import com.googlecode.objectify.impl.conv.Conversions;
//import com.googlecode.objectify.impl.conv.joda.DateTimeConverter;
//
//public class SimpleTrackpointConverterUnitTest extends BaseConverterTest {
//
//	SimpleTrackpoint trackpoint;
//	ArrayList<Object> list = new ArrayList<Object>();
//
//	@Before
//	public void setUp() throws Exception {
//		setupConversions();
//		super.setUp(converter = new SimpleTrackpointConverter(ObjectifyService.factory()
//				.getConversions()));
//
//		// Create trackpoint
//		int startTimeInMillis = 1000000000;
//		trackpoint = (SimpleTrackpoint) TrackUtil.createPoint(startTimeInMillis, 10, 20, 30);
//
//		// Add converted values to list.
//		// timestamp
//		list.add(trackpoint.getTimestamp().toDate());
//		// elevation
//		list.add((MeasurementUtil.getNormalizedQuantity(trackpoint.getElevation().getQuantity()))
//				.getValue().doubleValue());
//		// location
//		GeoCoordinate location = trackpoint.getLocation();
//		list.add(MeasurementUtil.getNormalizedQuantity(location.getLatitude().getQuantity())
//				.getValue().doubleValue());
//		list.add(MeasurementUtil.getNormalizedQuantity(location.getLongitude().getQuantity())
//				.getValue().doubleValue());
//	}
//
//	@Test
//	public void testForDatastore() {
//		testConvToDatastore(trackpoint, list);
//	}
//
//	@Test
//	public void testForPojo() {
//		testConvFromDatastore(list, SimpleTrackpoint.class, trackpoint);
//	}
//
//	/*
//	 * Only setup the conversions we need for this unit test.
//	 */
//	private void setupConversions() {
//		Conversions conv = ObjectifyService.factory().getConversions();
//		conv.add(new DateTimeConverter());
//		conv.add(new QuantityTranslatorFactory());
//		conv.add(new LatitudeConverter(conv));
//		conv.add(new LongitudeConverter(conv));
//		conv.add(new GeoCoordinateConverter(conv));
//		conv.add(new ElevationConverter(conv));
//	}
// }
