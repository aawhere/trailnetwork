package com.aawhere.persist.conv;

import com.googlecode.objectify.annotation.Entity;

public class ClassTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<ClassTranslatorFactoryUnitTest.ClassTestEntity> {

	public ClassTranslatorFactoryUnitTest() {
		super(new ClassTranslatorFactory());

	}

	@Entity
	static class ClassTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {
		public Class<?> pojo;

		public ClassTestEntity(Class<?> type) {
			this.pojo = type;
		}

	}

	@Override
	public void testBasic() {
		expected("String class", new ClassTestEntity(String.class));
		testRoundTrip();
	}

	@Override
	public void testNull() {
		expected("null", new ClassTestEntity(null));
		testRoundTrip();
	}

}
