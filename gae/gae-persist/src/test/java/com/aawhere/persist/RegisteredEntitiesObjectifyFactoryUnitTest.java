/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.StringIdentifier;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;

/**
 * @author aroller
 * 
 */
public class RegisteredEntitiesObjectifyFactoryUnitTest {

	private RegisteredEntitiesObjectifyFactory registeredEntities;
	private Class<LongMockEntityId> idType;

	@Before
	public void setUp() {
		this.registeredEntities = new RegisteredEntitiesObjectifyFactory();
		idType = LongMockEntityId.class;
		this.registeredEntities.register(idType);
	}

	@Test
	public void testisIdKnown() {
		assertTrue(idType + " is not known to " + this.registeredEntities, this.registeredEntities.isIdKnown(idType));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDuplicateKindRegistration() {
		// already registered during setup...we can't register again.
		StringIdentifier<LongMockEntity> kind1 = new StringIdentifier<LongMockEntity>() {
			private static final long serialVersionUID = 567842492050370548L;
		};
		registeredEntities.register(kind1.getClass());

	}

	@Test
	public void testisKindString() {
		Class<LongMockEntity> kind = IdentifierUtils.kindFrom(idType);
		String kindString = ObjectifyKeyUtils.getKind(kind);
		assertTrue(	kindString + " is not known to " + this.registeredEntities,
					this.registeredEntities.isKindKnown(kindString));
		Class<? extends Identifier> idTypeForKind = this.registeredEntities.idType(kindString);
		assertEquals(kindString + " Found wrong type", idType, idTypeForKind);
	}
}
