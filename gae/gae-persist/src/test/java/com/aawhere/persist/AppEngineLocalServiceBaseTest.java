/**
 * 
 */
package com.aawhere.persist;

import org.junit.After;
import org.junit.Before;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * Simple base class that sets up the datastore and other AppEngine {@link LocalServiceTestHelper}
 * capabilities.
 * 
 * @author aroller
 * 
 */
public class AppEngineLocalServiceBaseTest {

	private LocalServiceTestHelper helper;

	@Before
	public void setUpServices() {
		helper = helper();
		helper.setUp();
	}

	@After
	public void tearDownServices() {
		helper.tearDown();
	}

	/**
	 * Override if you want something besides the default.
	 * 
	 * @return
	 */
	protected LocalServiceTestHelper helper() {
		return GaePersistTestUtils.createRepositoryTestHelper();
	}
}
