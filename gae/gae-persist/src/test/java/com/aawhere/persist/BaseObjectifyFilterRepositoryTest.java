/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.id.Identifier;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.StringAdapterFactory;
import com.aawhere.test.TestUtil;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Lists;
import com.googlecode.objectify.cmd.Query;

/**
 * @author Brian Chapman
 * 
 */
/* @formatter:off */
public abstract class BaseObjectifyFilterRepositoryTest<IdentifierT extends Identifier<?, EntityT>,
		EntityT extends BaseEntity<EntityT, IdentifierT>,
		FilterRepoT extends ObjectifyFilterRepository< EntityT, IdentifierT,EntitiesT, EntitiesBuilderT>,
		RepoT extends Repository<EntityT, IdentifierT>,
		EntitiesT extends BaseFilterEntities<EntityT>,
		EntitiesBuilderT extends BaseFilterEntities.Builder<EntitiesBuilderT, EntityT, EntitiesT>> {
	/* @formatter:on */

	/**
	 *
	 */
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void baseSetUp() throws Exception {
		helper.setUp();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void baseTearDown() throws Exception {
		helper.tearDown();
	}

	@Test
	public void testFilter_EmptyRepository() {
		Filter filter = createFilterBuilder().build();
		EntitiesT entities = getFilterRepository().filter(filter);
		assertFilter(entities, 0);
		Filter filter2 = entities.getFilter();
		assertEquals((Integer) 1, filter2.getPage());
		EntitiesT entities2 = getFilterRepository().filter(filter2);
		assertFilter(entities2, 0);
	}

	@Test
	public void testFilterIdsIn() {
		int count = 3;
		List<EntityT> createEntities = createEntities(count);
		final EntityIdFunction<EntityT, IdentifierT> function = EntityIdFunction.build();
		List<IdentifierT> list = Lists.transform(createEntities, function);
		Filter filter = Filter.create().idsIn(list).build();
		EntitiesT found = getFilterRepository().filter(filter);
		TestUtil.assertCollectionEquals(createEntities, found.all());
	}

	@Test
	public void testFilter_IncrementsPage() {
		createEntities(3);
		Filter filter = createFilterBuilder().build();
		assertEquals((Integer) 1, filter.getPage());
		EntitiesT entities = getFilterRepository().filter(filter);
		assertEquals((Integer) 1, entities.getFilter().getPage());
	}

	@Test
	public void testFilter() {
		createEntities(3);
		Integer limit = 2;
		Filter filter = createFilterBuilder().setLimit(limit).build();
		EntitiesT entities = getFilterRepository().filter(filter);
		assertEquals(limit, entities.getFilter().getLimit());
		assertFalse(entities.isEmpty());
		assertFilter(entities, limit);

		EntitiesT entities2 = getFilterRepository().filter(incrementPage(entities.getFilter()));
		assertFilter(entities2, 1);
	}

	@Test
	public void testFilter_Cursors() {
		createEntities(3);
		Filter filter = createFilterBuilder().setLimit(2).build();
		EntitiesT entities = getFilterRepository().filter(filter);
		assertFilter(entities, 2);
		Filter filter2 = incrementPage(entities.getFilter());
		assertHasFilter(filter2);
		String cursor1 = getCursor(filter2);
		assertNotNull(cursor1);
		EntitiesT entities2 = getFilterRepository().filter(filter2);
		assertFilter(entities2, 1);
		Filter filter3 = incrementPage(entities2.getFilter());
		assertHasFilter(filter3);
		String cursor2 = getCursor(filter3);
		assertNotNull(cursor2);
		TestUtil.assertNotEquals(cursor1, cursor2);
	}

	private void assertHasFilter(Filter filter) {
		if (filter instanceof Filter) {
			assertTrue("a cusor should have been set", filter.hasCursor());
			assertNotNull(filter.getCursor());
		} else {
			fail("Filter is not of type Filter");
		}
	}

	private String getCursor(Filter filter) {
		if (filter instanceof Filter) {
			return filter.getCursor();
		} else {
			return null;
		}
	}

	@Test
	public void testFilter_CursorsUsedInQuery() throws IllegalAccessException, EmptyQueryException {
		createEntities(3);
		Filter filter = createFilterBuilder().setLimit(2).build();
		EntitiesT entities = getFilterRepository().filter(filter);
		assertFilter(entities, 2);
		Filter filter2 = incrementPage(entities.getFilter());
		assertHasFilter(filter2);
		Query<EntityT> query = (Query<EntityT>) getFilterRepository().writer(filter2).getQuery();
		assertNotNull(query);
		Object startAt = FieldUtils.readField(query, "startAt", true);
		assertNotNull(startAt);
	}

	@Test
	public void testFilter_filterAsIterable() {
		createEntities(1);
		Filter filter = createFilterBuilder().build();
		assertFilterAsIterable(getFilterRepository().filterAsIterable(filter));
	}

	/**
	 * Runs a test starting with a filter string and querying the datastore.
	 * 
	 * @throws BaseException
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testFilterByIdCondition() throws BaseException {
		EntityT entity = createPersistedEntity();
		FieldKey key = FieldAnnotationDictionaryFactory.keyFor(entity.getClass(), BaseEntity.BASE_FIELD.ID);
		FilterCondition<IdentifierT> condition = new FilterCondition.Builder<IdentifierT>().field(key)
				.operator(FilterOperator.EQUALS).value(entity.getId()).build();
		FieldAnnotationDictionaryFactory annoationFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		FieldDictionary dictionary = annoationFactory.getDictionary(entity.getClass());

		// run condition through adaptation
		StringAdapterFactory adapterFactory = new StringAdapterFactory();
		FilterConditionStringAdapter<?> adapter = (FilterConditionStringAdapter<?>) new FilterConditionStringAdapter.Builder()
				.setAdapterFactory(adapterFactory).setDictionary(dictionary)
				.setDictionaryFactory(annoationFactory.getFactory())
				.setRegistry(new FilterConditionSupportedTypeRegistry.Builder().build()).build();
		FilterCondition adaptedCondition = adapter.unmarshal(adapter.marshal(condition), null);
		assertNotNull(adaptedCondition);
		Filter filter = createFilterBuilder().addCondition(adaptedCondition).build();
		EntitiesT entities = getFilterRepository().filter(filter);
		assertEquals(1, entities.size());
	}

	@Test
	public void testIdEqualToCondition() {
		int numberToCreate = 2;
		List<EntityT> entities = createEntities(numberToCreate);
		EntityT first = entities.get(Index.FIRST.index);
		IdentifierT expectedId = first.getId();
		EntityT second = entities.get(Index.SECOND.index);
		FilterRepoT repository = getFilterRepository();
		{
			TestUtil.assertNotEquals(first.getId(), second.getId());
			Filter filter = createFilterBuilder().build();
			EntitiesT results = repository.filter(filter);
			assertEquals(numberToCreate, results.size());
		}
		// now, test equals
		{
			Filter filter = createFilterBuilder().idEqualTo(expectedId).build();
			EntitiesT results = repository.filter(filter);
			assertEquals(1, results.size());
			IdentifierT actualId = results.getCollection().get(0).getId();
			assertEquals(expectedId, actualId);
		}
		{
			// now, test not equals
			Filter filter = createFilterBuilder().idNotEqualTo(expectedId).build();
			EntitiesT notEqualResults = repository.filter(filter);
			assertEquals(1, notEqualResults.size());
			IdentifierT actualId = notEqualResults.getCollection().get(0).getId();
			TestUtil.assertNotEquals(expectedId, actualId);
			assertEquals(second.getId(), actualId);
		}

	}

	@Test
	public void testFilterByIds() {
		int expectedCount = 3;
		List<EntityT> entities = createEntities(expectedCount);
		List<IdentifierT> allIds = Lists.transform(entities, new EntityIdFunction<EntityT, IdentifierT>());
		ArrayList<IdentifierT> expectedIds = Lists.newArrayList(allIds.get(0), allIds.get(2));
		assertEquals(expectedCount, entities.size());
		FieldKey idFieldKey = FieldAnnotationDictionaryFactory
				.keyFor(allIds.get(0).getKind(), BaseEntity.BASE_FIELD.ID);
		Filter filter = createFilterBuilder().addCondition(new FilterCondition.Builder<ArrayList<IdentifierT>>()
				.field(idFieldKey).operator(FilterOperator.IN).value(expectedIds).build()).build();
		EntitiesT actualEntities = getFilterRepository().filter(filter);
		assertEquals(actualEntities.toString(), expectedIds.size(), actualEntities.size());
		List<IdentifierT> actualIds = Lists.transform(	actualEntities.getCollection(),
														new EntityIdFunction<EntityT, IdentifierT>());
		List<IdentifierT> intersection = ListUtils.intersection(expectedIds, actualIds);

		assertEquals(expectedIds.toString() + " != " + actualIds, expectedIds.size(), intersection.size());
	}

	@Test
	public void testDictionary() {
		FieldDictionary dictionary = this.getFilterRepository().dictionary();
		assertNotNull("dictionary", dictionary);
	}

	/** Focus on the function of the mutator using some of the baseFilter fields. */
	@Test
	public void testMutator() {
		final Integer limit = Filter.DEFAULT_LIMIT + 11;
		Filter.Builder builder = createFilterBuilder();
		builder.setLimit(limit);
		Filter filter = builder.build();
		assertEquals(limit, filter.getLimit());
		Filter.Builder createMutator = createMutator(filter);
	}

	protected Filter.Builder createFilterBuilder() {
		return new Filter.Builder();
	}

	protected Filter.Builder createMutator(Filter filter) {
		return Filter.mutate(filter);
	}

	/**
	 * Create a persisted Entity.
	 * 
	 * @return
	 */
	protected abstract EntityT createEntity();

	protected FilterRepoT getFilterRepository() {
		return (FilterRepoT) getRepository();
	}

	protected abstract RepoT getRepository();

	private void assertFilterAsIterable(Iterable<?> iterable) {
		Iterator<?> iterator = iterable.iterator();
		assertTrue(iterator.hasNext());
		assertNotNull(iterator.next());
		assertFalse(iterator.hasNext());
	}

	protected void assertFilter(EntitiesT result, Integer expectedSize) {
		Filter filter = result.getFilter();
		assertNotNull(result);
		assertEquals(expectedSize, filter.getPageTotal());
		assertEquals(expectedSize, (Integer) result.size());
	}

	protected List<EntityT> createEntities(Integer numberToCreate) {
		ArrayList<EntityT> created = new ArrayList<EntityT>(numberToCreate);
		for (int i = 0; i < numberToCreate; i++) {
			created.add(createPersistedEntity());
		}

		return created;
	}

	protected EntityT createPersistedEntity() {
		EntityT entity = createEntity();
		if (entity.getId() == null || entity.getDateCreated() == null) {
			entity = getRepository().store(entity);
		}
		return entity;
	}

	private Filter incrementPage(Filter filter) {
		return createMutator(filter).incrementPage().build();
	}

	/**
	 * @param message
	 * @param expectedCount
	 * @param filter
	 * @return
	 */
	protected EntitiesT assertFilter(String message, int expectedCount, Filter filter) {
		EntitiesT foundRelations = this.getFilterRepository().filter(filter);
		TestUtil.assertSize(message, expectedCount, foundRelations.getCollection());
		return foundRelations;
	}
}
