package com.aawhere.persist;

import com.aawhere.field.FieldDictionaryFactory;

import com.googlecode.objectify.ObjectifyService;

public class LongMockEntityObjectifyRepository
		extends
		ObjectifyFilterRepository<LongMockEntity, LongMockEntityId, LongMockFilterEntities, LongMockFilterEntities.Builder>
		implements LongMockEntityRepository {

	static {
		ObjectifyService.register(LongMockEntity.class);
	}

	/**
	 * Creates a MockEntityObjectifyRepository with the synchronous flag set to true.
	 */
	public LongMockEntityObjectifyRepository(FieldDictionaryFactory fieldDictionaryFactory) {
		super(new Delegate(fieldDictionaryFactory), Synchronization.SYNCHRONOUS);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.objectify.MockEntityRepository#updateName(com.aawhere.id.l.Id,
	 * java.lang.String)
	 */
	@Override
	public LongMockEntity updateName(LongMockEntityId id, String name) throws EntityNotFoundException {
		LongMockEntity e = load(id);
		return update(new LongMockEntity.Builder(e).setName(name).build());
	}

	static public class Delegate
			extends
			ObjectifyFilterRepositoryDelegate<LongMockFilterEntities, LongMockFilterEntities.Builder, LongMockEntity, LongMockEntityId> {

		/**
		 * @param fieldDictionaryFactory
		 */
		public Delegate(FieldDictionaryFactory fieldDictionaryFactory) {
			super(fieldDictionaryFactory);
		}
	}
}
