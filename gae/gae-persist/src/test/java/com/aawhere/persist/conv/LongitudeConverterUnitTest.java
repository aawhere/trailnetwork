//package com.aawhere.persist.conv;
//
//import javax.measure.quantity.Angle;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import com.aawhere.measure.Longitude;
//import com.aawhere.measure.MeasurementUtil;
//import com.aawhere.measure.QuantityTranslatorFactory;
//
//import com.googlecode.objectify.ObjectifyService;
//import com.googlecode.objectify.impl.conv.Conversions;
//
//public class LongitudeConverterUnitTest extends BaseConverterTest {
//
//	Double angleDouble;
//	Double angleDoubleRadians;
//	Longitude lon;
//
//	@Before
//	public void setuUp() {
//		setupConversions();
//		super.setUp(converter = new LongitudeConverter(ObjectifyService.factory().getConversions()));
//		angleDouble = new Double(10);
//		angleDoubleRadians = new Double(0.174532925);
//		lon = new Longitude(angleDouble);
//	}
//
//	@Test
//	public void testForDatastore() {
//		testConvToDatastore(lon, angleDoubleRadians);
//	}
//
//	@Test
//	public void testForPojo() {
//		// Creating angle of type Radians because the Measurement.equals method
//		// Does not agree that numerically equal quantities of different units
//		// are
//		// equal.
//		Angle radAngle = MeasurementUtil.createAngleInRadians(angleDoubleRadians);
//		testConvFromDatastore(angleDoubleRadians, Longitude.class, new Longitude(radAngle));
//	}
//
//	/*
//	 * Only setup the conversions we need for this unit test.
//	 */
//	private void setupConversions() {
//		Conversions conv = ObjectifyService.factory().getConversions();
//		conv.add(new QuantityTranslatorFactory());
//	}
// }
