/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.StatusMessages;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.googlecode.objectify.Key;

/**
 * @author Brian Chapman
 * 
 */
public class ObjectifyKeyUtilsUnitTest {

	LocalServiceTestHelper helper = GaeServiceTestHelper.createTestHelper().build();
	private Key<LongMockEntity> key1;
	private Key<LongMockEntity> key2;
	private LongMockEntity e1;
	private LongMockEntity e2;
	private HashMap<Key<LongMockEntity>, LongMockEntity> map;
	private LongMockEntityId id1;
	private LongMockEntityId id2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		helper.setUp();
		this.e1 = EntityTestUtil.generateRandomLongEntity();
		this.e2 = EntityTestUtil.generateRandomLongEntity();
		this.id1 = this.e1.id();
		this.id2 = this.e2.id();
		this.key1 = Key.create(LongMockEntity.class, id1.getValue());
		this.key2 = Key.create(LongMockEntity.class, id2.getValue());
		this.map = Maps.newHashMap();
		this.map.put(key1, e1);
		this.map.put(key2, e2);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		helper.tearDown();
	}

	/**
	 * Test method for
	 * {@link com.aawhere.persist.objectify.ObjectifyKeyUtils#keyToId(com.googlecode.objectify.Key, java.lang.Class, java.lang.Class)}
	 * .
	 */
	@Test
	public void testKeyToId() {
		Key<LongMockEntity> expected = Key.create(LongMockEntity.class, 14);
		Identifier<Long, LongMockEntity> result = ObjectifyKeyUtils.id(expected, LongMockEntityId.class);
		assertNotNull(result);
		assertEquals((Long) expected.getId(), result.getValue());
		assertEquals(LongMockEntityId.class, result.getClass());

	}

	/**
	 * Test method for
	 * {@link com.aawhere.persist.objectify.ObjectifyKeyUtils#idToKey(com.aawhere.id.Identifier)} .
	 */
	@Test
	public void testIdToKey() {
		LongMockEntityId expected = new LongMockEntityId(14l);
		Key<LongMockEntity> result = ObjectifyKeyUtils.key(expected);
		assertIdToKey(expected, result);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.persist.objectify.ObjectifyKeyUtils#idToKey(com.aawhere.id.Identifier)} .
	 */
	@Test
	public void testIdToKeyWithParent() {
		LongMockEntityId parent = new LongMockEntityId(100l);
		LongMockEntityWithParentId expected = new LongMockEntityWithParentId(10l, parent);
		Key<LongMockEntityWithParent> result = ObjectifyKeyUtils.key(expected);
		assertIdToKey(expected, result);
		assertEquals(ObjectifyKeyUtils.key(parent), result.getParent());
	}

	/**
	 * Test method for
	 * {@link com.aawhere.persist.objectify.ObjectifyKeyUtils#idsToRawKeys(java.lang.Iterable)} .
	 */
	@Test
	public void testIdsToRawKeys() {
		LongMockEntityId expected1 = new LongMockEntityId(14l);
		LongMockEntityId expected2 = new LongMockEntityId(15l);
		Set<LongMockEntityId> ids = Sets.newHashSet(expected1, expected2);
		Iterable<Key<LongMockEntity>> result = ObjectifyKeyUtils.keys(ids);
		assertNotNull(result);
		assertEquals(2, Iterables.size(result));
	}

	/**
	 * Test method for
	 * {@link com.aawhere.persist.objectify.ObjectifyKeyUtils#keysToIds(java.util.Collection, java.lang.Class, java.lang.Class)}
	 * .
	 */
	@Test
	public void testKeysToIds() {
		Key<LongMockEntity> key1 = Key.create(LongMockEntity.class, 1l);
		Key<LongMockEntity> key2 = Key.create(LongMockEntity.class, 2l);
		@SuppressWarnings("unchecked")
		Set<Key<LongMockEntity>> keys = Sets.newHashSet(key1, key2);
		Iterable<LongMockEntityId> results = ObjectifyKeyUtils.ids(keys, LongMockEntityId.class, LongMockEntity.class);
		assertNotNull(results);
		assertEquals(2, Iterables.size(results));
		assertEquals(LongMockEntityId.class, results.iterator().next().getClass());
	}

	/**
	 * Test method for
	 * {@link com.aawhere.persist.objectify.ObjectifyKeyUtils#keyMapToIdMap(java.util.Map, java.lang.Class)}
	 * .
	 */
	@Test
	public void testKeyMapToIdMap() {

		final Iterable<LongMockEntityId> ids = IdentifierUtils.ids(map.values());
		Map<LongMockEntityId, LongMockEntity> results = ObjectifyKeyUtils.idEntityMap(ids, map, LongMockEntity.class);
		assertNotNull(results);
		assertEquals(2, results.size());
		assertEquals(LongMockEntityId.class, results.keySet().iterator().next().getClass());
	}

	/**
	 * Test method for
	 * {@link com.aawhere.persist.objectify.ObjectifyKeyUtils#getIdValue(com.googlecode.objectify.Key)}
	 * .
	 */
	@Test
	public void testGetIdValue() {
		Long longValue = 1l;
		String stringValue = "StringKey";
		// TODO: test with Key where id = null instead of 0 as is the case here.
		// Note: name =
		// stringValue here.
		Key<StringMockEntity> stringKey = Key.create(StringMockEntity.class, stringValue);
		Key<LongMockEntity> longKey = Key.create(LongMockEntity.class, longValue);
		Long longActual = (Long) ObjectifyKeyUtils.getIdValue(longKey);
		String stringActual = (String) ObjectifyKeyUtils.getIdValue(stringKey);
		assertNotNull(longActual);
		assertNotNull(stringActual);
		assertEquals(longValue, longActual);
		assertEquals(stringValue, stringActual);
	}

	@Test
	public void testKeyToIdIterator() {
		Key<LongMockEntity> key1 = Key.create(LongMockEntity.class, 1l);
		Key<LongMockEntity> key2 = Key.create(LongMockEntity.class, 2l);
		@SuppressWarnings("unchecked")
		Set<Key<LongMockEntity>> keys = Sets.newHashSet(key1, key2);

		Iterable<LongMockEntityId> ids = new ObjectifyKeyUtils.KeyToIdIterator.Builder<LongMockEntity, LongMockEntityId>()
				.setIterable(keys).setEntityT(LongMockEntity.class).setIdentifierType(LongMockEntityId.class).build();
		assertNotNull(ids);
		Iterator<LongMockEntityId> it = ids.iterator();
		assertEquals(LongMockEntityId.class, it.next().getClass());
		assertEquals(LongMockEntityId.class, it.next().getClass());
		assertFalse(it.hasNext());
	}

	private void assertIdToKey(Identifier<?, ?> id, Key<?> result) {
		assertNotNull(result);
		assertEquals(id.getValue(), result.getId());
		final Class<?> idKind = id.getKind();
		assertEquals(ObjectifyKeyUtils.getKind(idKind), result.getKind());
	}

	@Test
	public void testEntitiesLoadAllMapResults() {
		Boolean expectedError = false;
		Set<LongMockEntity> expectedEntities = Sets.newHashSet(e1, e2);
		assertEntities(expectedError, expectedEntities);
	}

	/**
	 * Tests when a null reference is provided.
	 */
	@Test
	public void testEntitiesOneError() {
		this.map.put(key1, null);
		this.map.put(key2, e2);
		assertEntities(true, Sets.newHashSet(e2));
	}

	/**
	 * Tests when a null reference is provided.
	 */
	@Test
	public void testEntitiesTwoErrors() {
		this.map.put(key1, null);
		this.map.put(key2, null);
		final Set<LongMockEntity> emptySet = Collections.emptySet();
		assertEntities(true, emptySet);
	}

	/**
	 * Proves that when we provide more requested ids than the results found an error will be
	 * logged.
	 */
	@Test
	public void testEntitiesResultsLessThanRequested() {
		LongMockEntityId notFoundId = EntityTestUtil.generateRandomMockLongEntityId();
		HashSet<LongMockEntityId> requestedIds = Sets.newHashSet(id1, id2, notFoundId);
		LongMockFilterEntities entities = assertEntities(true, Sets.newHashSet(e1, e2), requestedIds);
		TestUtil.assertContains(entities.getStatusMessages().toString(), notFoundId.toString());
	}

	/**
	 * @param expectedError
	 * @param keyMap
	 * @param expectedEntities
	 * @return
	 */
	private LongMockFilterEntities assertEntities(Boolean expectedError, Set<LongMockEntity> expectedEntities) {
		final Iterable<LongMockEntityId> idsRequested = ObjectifyKeyUtils.ids(	this.map.keySet(),
																				LongMockEntityId.class,
																				LongMockEntity.class);
		return assertEntities(expectedError, expectedEntities, idsRequested);

	}

	/**
	 * @param expectedError
	 * @param expectedEntities
	 * @param idsRequested
	 * @return
	 */
	private LongMockFilterEntities assertEntities(Boolean expectedError, Set<LongMockEntity> expectedEntities,
			final Iterable<LongMockEntityId> idsRequested) {
		LongMockFilterEntities entities = ObjectifyKeyUtils.entities(	this.map,
																		idsRequested,
																		new LongMockFilterEntities.Builder(),
																		LongMockEntityId.class);
		StatusMessages statusMessages = entities.getStatusMessages();
		assertEquals("Incorrect status" + statusMessages.toString(), expectedError, statusMessages.hasError());
		TestUtil.assertCollectionEquals(expectedEntities, entities.all());
		return entities;
	}

	/**
	 * @see #assertKeyAncestorsParentType(StringMockEntityWithParentId)
	 */
	@Test
	public void testKeyAncestorsSingleParentType() {
		StringMockEntityWithParentId child = DatastoreTestUtil.child();
		StringMockEntityWithParentId childResult = assertKeyAncestorsParentType(child);
		assertNotNull(childResult.getParent());
	}

	/**
	 * @see #assertKeyAncestorsParentType(StringMockEntityWithParentId)
	 */
	@Test
	public void testKeyRootSingleParentType() {
		assertKeyAncestorsParentType(DatastoreTestUtil.greatGranparent());
	}

	@Test
	public void testKeyAncestorsSingleParentDifferentType() {
		long parentValue = 10l;
		long childValue = 1l;
		LongMockEntityWithParentId id = new LongMockEntityWithParentId(childValue, new LongMockEntityId(parentValue));
		Key<LongMockEntityWithParent> key = ObjectifyKeyUtils.key(id);
		assertEquals("child value is incorrect", childValue, key.getId());
		assertNotNull(key.getParent());
		assertEquals("parent value is incorrect", parentValue, key.getParent().getId());

		Identifier<?, ?> idBuilt = ObjectifyKeyUtils.idWithParents(	key,
																	LongMockEntityWithParentId.class,
																	LongMockEntityId.class);
		assertEquals("id not rebuilt correctly", id, idBuilt);
	}

	/**
	 * In the cases where the ancestors are all the same type we don't need to use
	 * {@link RegisteredEntities}, but can provide the parent type.
	 * 
	 * @param child
	 * @return
	 */
	public StringMockEntityWithParentId assertKeyAncestorsParentType(StringMockEntityWithParentId child) {
		Key<StringMockEntityWithParent> key = ObjectifyKeyUtils.key(child);

		StringMockEntityWithParentId transformedChild = (StringMockEntityWithParentId) ObjectifyKeyUtils
				.idWithParents(key, StringMockEntityWithParentId.class);
		assertEquals(child, transformedChild);
		return transformedChild;
	}

	@Test
	public void testKeyRootWithRegisteredEntities() {
		assertKeyWithRegisteredEntities(DatastoreTestUtil.greatGranparent());
	}

	@Test
	public void testKeyAncestorsWithRegisteredEntities() {
		StringMockEntityWithParentId child = DatastoreTestUtil.child();

		assertKeyWithRegisteredEntities(child);
	}

	/**
	 * @param child
	 */
	public void assertKeyWithRegisteredEntities(StringMockEntityWithParentId child) {
		Key<StringMockEntityWithParent> key = ObjectifyKeyUtils.key(child);
		StringMockEntityWithParentId transformedChild = (StringMockEntityWithParentId) ObjectifyKeyUtils
				.id(key, DatastoreTestUtil.registeredEntities());
		assertEquals(child, transformedChild);
	}
}
