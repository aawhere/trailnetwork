/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.test.TestUtil;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author brian
 * 
 */
public class DatastoreKeyUtilUnitTest {

	LocalServiceTestHelper helper = GaeServiceTestHelper.createTestHelper().build();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		helper.setUp();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		helper.tearDown();
	}

	@Test
	public void testGetIdValue() {
		String kind = "kind";
		String valueString = "test";
		Long valueLong = TestUtil.generateRandomId();
		Key keyString = KeyFactory.createKey(kind, valueString);
		Key keyLong = KeyFactory.createKey(kind, valueLong);
		Object resultString = DatastoreKeyUtil.getIdValue(keyString);
		Object resultLong = DatastoreKeyUtil.getIdValue(keyLong);

		assertNotNull(resultString);
		assertNotNull(resultLong);
		assertEquals(valueString, resultString);
		assertEquals(valueLong, resultLong);
	}

	@Test
	public void testKeyToId() {
		StringMockEntityId expectedId = EntityTestUtil.generateRandomMockStringEntityId();
		Key actualKey = DatastoreKeyUtil.key(expectedId);
		StringMockEntityId actualId = DatastoreKeyUtil.id(actualKey, StringMockEntityId.class);
		assertEquals(expectedId, actualId);
		assertEquals(expectedId.getValue(), DatastoreKeyUtil.getIdValue(actualKey));

	}

	/** Tests deep ancestry conversion from id to key and back. */
	@Test
	public void testAncestorTransformation() {
		StringMockEntityWithParentId child = DatastoreTestUtil.child();

		Key key = DatastoreKeyUtil.key(child);
		StringMockEntityWithParentId transformedChild = DatastoreKeyUtil.id(key,
																			StringMockEntityWithParentId.class,
																			DatastoreTestUtil.registeredEntities());
		assertEquals(child, transformedChild);

	}
}
