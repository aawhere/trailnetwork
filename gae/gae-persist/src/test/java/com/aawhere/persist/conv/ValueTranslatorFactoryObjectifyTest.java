package com.aawhere.persist.conv;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.persist.GaeServiceTestHelper;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.LoadResult;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

abstract public class ValueTranslatorFactoryObjectifyTest<E extends ValueTranslatorFactoryObjectifyTest.TestEntity> {

	protected final LocalServiceTestHelper helper = GaeServiceTestHelper.createTestHelper().withDatastoreService()
			.build();

	ObjectifyFactory fact;

	private E expected;

	private ValueTranslatorFactory<?, ?> valueTranslatorFactory;
	private Class<E> testEntityType;
	private String message;

	public ValueTranslatorFactoryObjectifyTest(ValueTranslatorFactory<?, ?> factory) {
		this(factory, null);
	}

	public ValueTranslatorFactoryObjectifyTest(ValueTranslatorFactory<?, ?> factory, Class<E> testEntityType) {
		this.valueTranslatorFactory = factory;
		this.testEntityType = testEntityType;
	}

	@Before
	final public void setUpBase() {
		fact = ObjectifyService.factory();
		helper.setUp();
	}

	@After
	final public void tearDownBase() {
		helper.tearDown();
	}

	/**
	 * At minimum this test is run once for every subclass. To run extra tests, which is encouraged,
	 * you can implement your specific test to provide a different expected value to be translated.
	 * The most common being a null value in your {@link TestEntity}.
	 * 
	 * <pre>
	 * @Test
	 * public void testNull(){
	 *  expected("null",new MyEntity(null));
	 *  testRoundTrip();
	 * }
	 */
	protected void testRoundTrip() {
		assertNotNull(" you must call expected to provide the TestEntity", this.expected);
		Objectify objectify = fact.begin();
		fact.getTranslators().add(this.valueTranslatorFactory);
		fact.register(expected.getClass());
		Result<Key<E>> saveResult = objectify.save().entity(expected);
		Key<E> key = saveResult.now();
		assertNotNull(key);
		LoadResult<E> loadResult = objectify.load().key(key);
		E actual = loadResult.now();
		assertEquals("keys not equal", key.getId(), actual.id.longValue());
		assertActual(this.message, this.expected, actual);
	}

	/**
	 * Required call to provide the values, but does not invoke {@link #testRoundTrip()}
	 * 
	 * @param message
	 * @param expected
	 */
	protected void expected(String message, E expected) {
		this.message = message;
		this.expected = expected;
	}

	/**
	 * A shortcut calling {@link #expected(String, TestEntity)}, then {@link #testRoundTrip()}.
	 * 
	 * @param message
	 * @param expected
	 */
	protected void test(String message, E expected) {
		expected(message, expected);
		testRoundTrip();
	}

	/**
	 * A friendly reminder to provide the most basic implementation of your value.
	 * 
	 */
	@Test
	abstract public void testBasic();

	/**
	 * A friendly reminder that you should validate the translator works if your value is null.
	 * 
	 */
	@Test
	public void testNull() {
		try {
			testEntityType.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			Constructor<?> constructor = testEntityType.getConstructors().clone()[0];
			try {
				E object = (E) constructor.newInstance(null);
				test("null", object);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e1) {
				fail("unable to test null since i'm unable to instantiate the test entity " + testEntityType);
			}
		}
	}

	/**
	 * Provides the basic equality check after running {@link #testRoundTrip()}. Override if pojo
	 * equality is not possible or equality is different.
	 * 
	 * @param message
	 * @param expected
	 * @param actual
	 */
	protected void assertActual(String message, E expected, E actual) {
		assertNotNull(expected);
		assertNotNull(actual);
		Field pojoField = FieldUtils.getDeclaredField(expected.getClass(), "pojo");
		if (pojoField == null) {
			fail("if you name your field 'pojo' and make it public I can test equality for you. override this method if you wish otherwise.");
		}
		pojoField.setAccessible(true);
		try {
			Object expectedPojo = pojoField.get(expected);
			Object actualPojo = pojoField.get(actual);
			assertEquals(message, expectedPojo, actualPojo);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public abstract static class TestEntity {

		@Id
		Long id;

	}

}
