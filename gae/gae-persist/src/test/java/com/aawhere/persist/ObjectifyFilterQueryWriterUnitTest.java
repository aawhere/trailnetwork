/**
 * 
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldNotIndexedException;
import com.aawhere.field.FieldTestUtil;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.persist.ObjectifyRepositoryTestUtil.MockQuery;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.MessageStatus;
import com.aawhere.util.rb.StatusMessage;
import com.aawhere.util.rb.StatusMessages;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.labs.repackaged.com.google.common.collect.Iterables;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.google.common.base.Predicate;
import com.googlecode.objectify.cmd.Query;

/**
 * Tests the building of the query and inspection into fields. It does no validation for the
 * Objectify compatibility of the query itself as that should be done in a RepositoryTest.
 * 
 * @author aroller
 * 
 */
public class ObjectifyFilterQueryWriterUnitTest {

	/**
	 * 
	 */
	private static final FieldKey ID_KEY = LongMockEntity.FIELD.KEY.ID;
	private ObjectifyFilterQueryWriter.Builder writerBuilder;
	// the writer built by the method configuring the test.
	private ObjectifyFilterQueryWriter writer;
	private MockQuery query = new MockQuery();
	String nameValue = "what the trash?";
	FilterOperator operator = FilterOperator.EQUALS;
	FieldKey nameKey = LongMockEntity.FIELD.KEY.NAME;
	FilterCondition<String> nameFilterCondition;
	FilterCondition<LongMockEntityId> idFilterCondition;
	FilterCondition<Object> countCondition;
	LongMockEntityId id;
	FieldKey notIndexed = LongMockEntity.FIELD.KEY.RELATED;

	@org.junit.Before
	public void setUp() {
		FieldAnnotationDictionaryFactory annoationFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		Class<LongMockEntity> entityType = LongMockEntity.class;
		annoationFactory.getDictionary(entityType);
		annoationFactory.getDictionary(StringMockEntity.class);
		FieldDictionaryFactory dictionaryFactory = annoationFactory.getFactory();
		this.nameFilterCondition = GaePersistTestUtils.getLongMockEntityFilter(nameValue);
		this.writerBuilder = new ObjectifyFilterQueryWriter.Builder().setEntityType(entityType).setQuery(query)
				.setFieldDictionaryFactory(dictionaryFactory);
		id = new LongMockEntityId(22l);
		idFilterCondition = new FilterCondition.Builder<LongMockEntityId>().field(ID_KEY).operator(operator).value(id)
				.build();
		countCondition = FilterCondition.create().field(LongMockEntity.FIELD.KEY.COUNT).operator(FilterOperator.EQUALS)
				.value(3).build();
		countCondition = FilterCondition.create().field(LongMockEntity.FIELD.KEY.COUNT).operator(FilterOperator.EQUALS)
				.value(3).build();

	}

	/**
	 * Test method for {@link com.aawhere.persist.ObjectifyFilterQueryWriter#isEmptyResults()}.
	 */
	@Test
	public void testEmptyCollection() {
		@SuppressWarnings("unchecked")
		Filter filter = new Filter.Builder().addCondition(new FilterCondition.Builder<List<String>>()
				.field(LongMockEntity.FIELD.KEY.COLLECTION).operator(operator).value(Collections.EMPTY_LIST).build())

		.build();
		buildWriter(filter);
		assertTrue(writer.isEmptyResults());
	}

	/**
	 */
	@Test
	public void testCollection() {
		Filter filter = new Filter.Builder().addCondition(new FilterCondition.Builder<List<String>>()
				.field(LongMockEntity.FIELD.KEY.COLLECTION).operator(operator)
				.value(Lists.newArrayList("a string", "another string")).build()).build();
		buildWriter(filter);
		assertNoError();
	}

	/**
	 */
	@Test
	public void testCollectionWithOneElement() {
		Filter filter = new Filter.Builder().addCondition(new FilterCondition.Builder<List<String>>()
				.field(LongMockEntity.FIELD.KEY.COLLECTION).operator(operator).value(Lists.newArrayList("a string"))
				.build()).build();
		buildWriter(filter);
		assertNoError();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRawQuery() {
		buildWriter(Filter.create().build());
		this.writer.getRawQuery();
	}

	/**
	 * If an equals operator is provided with a collection, the datastore doesn't like it. We should
	 * switch automatically to a IN query rather than exploding.
	 * 
	 */
	@Test
	public void testEqualsCollection() {
		String one = "one";
		String two = "two";
		FilterCondition condition = FilterCondition.create().field(LongMockEntity.FIELD.KEY.COLLECTION)
				.operator(FilterOperator.EQUALS).value(Lists.newArrayList(one, two)).build();
		Filter filter = new Filter.Builder().addCondition(condition).build();
		buildWriter(filter);
		assertNoError();
		TestUtil.assertSize(1, query.conditions);
		final String conditionString = StringUtils.join(this.query.values, ",");
		TestUtil.assertContains(conditionString, one);
		TestUtil.assertContains(conditionString, two);
	}

	/**
	 * Create date is special since the index exists in the {@link BaseEntity} so it must be
	 * declared at the dictionary level.
	 * 
	 */
	@Test
	public void testSingleIndexDeclaredInDictionary() {
		Filter filter = Filter
				.create()
				.addCondition(FilterCondition.create().field(LongMockEntity.FIELD.KEY.DATE_CREATED)
						.operator(FilterOperator.EQUALS).value(new Date()).build()).build();
		buildWriter(filter);
		assertNoError();
	}

	@Test
	public void testIndexNotRequiredAcrossDomains() {
		FilterCondition<Object> goLong = FilterCondition.create().field(LongMockEntity.FIELD.KEY.DATE_CREATED)
				.operator(FilterOperator.EQUALS).value(new Date()).build();
		FilterCondition<Object> goString = FilterCondition.create().field(StringMockEntity.FIELD.KEY.NAME)
				.operator(FilterOperator.EQUALS).value("junk").build();
		Filter filter = Filter.create().addCondition(goLong).addCondition(goString).build();
		buildWriter(filter);
		assertNoError();
	}

	@Test
	public void testSortIgnoredAcrossDomains() {
		final FieldKey conditionKey = LongMockEntity.FIELD.KEY.DATE_CREATED;
		FilterCondition<Object> goLong = FilterCondition.create().field(conditionKey).operator(FilterOperator.EQUALS)
				.value(new Date()).build();
		final FieldKey sortKey = StringMockEntity.FIELD.KEY.NAME;
		Filter filter = Filter.create().addCondition(goLong).orderBy(sortKey).build();
		buildWriter(filter);
		assertNoError();
		TestUtil.assertDoesntContain(sortKey.getRelativeKey(), this.query.orders);

	}

	@Test
	public void testSortInMemoryNotComparable() {
		buildWriter(Filter.create()
				.orderByInMemory(FilterSort.create().setFieldKey(LongMockEntity.FIELD.KEY.COLLECTION).build()).build());
		assertErrorHappens();
	}

	@Test
	public void testSortInMemory() {
		final FieldKey sortKey = LongMockEntity.FIELD.KEY.COUNT;
		FilterSort filterSort = FilterSort.create().setFieldKey(sortKey).setDirection(Direction.DESCENDING).build();
		Filter filter = Filter.create().orderByInMemory(filterSort).build();
		buildWriter(filter);
		assertNoError();
		final Filter writerFilter = this.writer.getFilter();
		assertNotNull(writerFilter.getOrderByInMemory());
		assertNotNull(writerFilter.getOrdering());

		// now verify date is sorted when entities are provided.the direction is descending
		final int count = 5;

		LongMockEntity first = LongMockEntity.create().setId(new LongMockEntityId(1l)).setCount(count).build();
		LongMockEntity last = LongMockEntity.create().setId(new LongMockEntityId(2l)).setCount(count - 1).build();
		// add the entities in wrong order and set the filter with the memory ordering.
		LongMockFilterEntities entities = new LongMockFilterEntities.Builder().add(last).add(first)
				.setFilter(writerFilter).build();
		assertNotNull("ordering wasn't assinged by the filter", entities.ordering());
		assertEquals(entities.toString(), first, entities.iterator().next());
		assertEquals(entities.toString(), last, Iterables.getLast(entities));
	}

	/**
	 * 
	 */
	private void assertNoError() {
		assertFalse(this.writer.getStatusMessages().toString(), this.writer.getStatusMessages().hasError());
	}

	private void assertErrorHappens() {
		assertTrue(this.writer.getStatusMessages().toString(), this.writer.getStatusMessages().hasError());
	}

	@Test
	public void testDifferentColumnName() {
		FilterCondition<Object> differentFilterCondition = FilterCondition.create()
				.field(LongMockEntity.FIELD.KEY.DIFFERENT).operator(FilterOperator.EQUALS).value("junk").build();
		Filter filter = new Filter.Builder().addCondition(differentFilterCondition).build();
		buildWriter(filter);
		assertTrue("column name not being respected", query.conditions.get(0).contains(LongMockEntity.FIELD.THAN_THIS));
	}

	@Test
	public void testDifferentColumnNameForEmbedded() {
		FilterCondition<Object> differentFilterCondition = FilterCondition.create()
				.field(LongMockEntity.FIELD.KEY.EMBEDDED_IN_DIFFERENT_EMBED).operator(FilterOperator.EQUALS)
				.value("trash").build();
		Filter filter = new Filter.Builder().addCondition(differentFilterCondition).build();
		buildWriter(filter);
		TestUtil.assertContains(query.conditions.get(0), LongMockEntity.FIELD.EMBED3);
	}

	@Test
	public void testNameQuery() {
		Filter filter = new Filter.Builder().addCondition(nameFilterCondition).build();
		buildWriter(filter);
		// the query is mutable so all tests can go against the query itself.
		Query<?> query = writer.getQuery();
		assertNotNull(query);
		assertSame(this.query, query);
		int expectedIndex = 0;
		assertNameCondition(expectedIndex, 1);

	}

	/**
	 * @param expectedIndex
	 */
	private void assertNameCondition(int expectedIndex, int expectedNumberOfFilters) {
		FieldKey expectedKey = nameKey;
		Object expectedValue = nameValue;

		assertFilterConditions(expectedIndex, expectedNumberOfFilters, expectedKey, expectedValue);
	}

	@Test
	public void testIdQuery() {
		Filter filter = new Filter.Builder().addCondition(idFilterCondition).build();
		buildWriter(filter);
		int expectedIndex = 0;
		assertIdCondition(expectedIndex, 1);
	}

	/**
	 * @param expectedIndex
	 */
	private void assertIdCondition(int expectedIndex, int expectedNumberOfFilters) {
		assertFilterConditions(expectedIndex, expectedNumberOfFilters, ID_KEY, this.id);
		assertTrue("shouldhave called the specific key function", this.query.keyFilter);
	}

	/**
	 * @param expectedNumberOfFilters
	 * @param expectedKey
	 * @param expectedValue
	 */
	private void assertFilterConditions(int expectedIndex, int expectedNumberOfFilters, FieldKey expectedKey,
			Object expectedValue) {
		assertEquals(expectedNumberOfFilters, this.query.conditions.size());
		assertEquals(expectedNumberOfFilters, this.query.values.size());

		TestUtil.assertContains(this.query.conditions.get(expectedIndex), operator.operator);
		// ids don't get inserted into the query.
		if (expectedKey != ID_KEY) {
			TestUtil.assertContains(this.query.conditions.get(expectedIndex), expectedKey.getRelativeKey());
		}
		assertEquals(expectedValue, this.query.values.get(expectedIndex));

		StatusMessages messages = this.writer.getStatusMessages();
		assertNotNull(messages);
		assertEquals(expectedNumberOfFilters, messages.getMessages().size());
		assertFalse(writer.isEmptyResults());
	}

	/**
	 * Attempts a combination query that has not yet been declared in the dictionary.
	 * 
	 */
	@Ignore("TN-182")
	@Test(expected = FieldNotIndexedException.class)
	public void testDoubleConditionNotDeclared() {
		Filter filter = new Filter.Builder().addCondition(idFilterCondition).addCondition(nameFilterCondition).build();
		buildWriter(filter);
		int expectedNumberOfFilters = 2;
		assertEquals(expectedNumberOfFilters, this.query.conditions.size());
		assertIdCondition(0, expectedNumberOfFilters);
		assertNameCondition(1, expectedNumberOfFilters);
	}

	@Test(expected = FieldNotIndexedException.class)
	public void testNotIndexedForSort() {
		Filter filter = new Filter.Builder().orderBy(notIndexed).build();
		buildWriter(filter);
	}

	@Ignore("TN-182")
	@Test(expected = FieldNotIndexedException.class)
	public void testNotIndexedForCondition() {
		Filter filter = new Filter.Builder().addCondition(FilterCondition.create().field(notIndexed)
				.operator(FilterOperator.EQUALS).value(id).build()).build();
		buildWriter(filter);
	}

	/**
	 * @see LongMockEntityNameCountCombinationIndex
	 */
	@Test
	public void testDoubleQuery() {

		Filter filter = new Filter.Builder().addCondition(countCondition).addCondition(nameFilterCondition).build();
		buildWriter(filter);
		int expectedNumberOfFilters = 2;
		assertEquals(expectedNumberOfFilters, this.query.conditions.size());
		assertNameCondition(1, expectedNumberOfFilters);
	}

	@Ignore("TN-182")
	@Test(expected = FieldNotIndexedException.class)
	public void testConditionAndOrderNotCombinedIndexed() {

		Filter filter = new Filter.Builder().addCondition(countCondition).orderBy(ID_KEY).build();
		buildWriter(filter);
	}

	@Test
	public void testPredicateReplacingCondition() {
		Integer value = TestUtil.generateRandomInt();
		@SuppressWarnings("rawtypes")
		FilterCondition condition = FilterCondition.create().field(LongMockEntity.FIELD.KEY.COMPARABLE)
				.operator(FilterOperator.EQUALS).value(value).build();
		Filter filter = new Filter.Builder().addCondition(condition).setAutoPredicates(true).build();
		buildWriter(filter);
		Filter modifiedFilter = this.writer.getFilter();
		TestUtil.assertEmpty(modifiedFilter.getConditions());
		TestUtil.assertNotEmpty(modifiedFilter.getPredicates());
		TestUtil.assertEmpty(this.query.conditions);
	}

	@Test
	public void testPredicate() {
		Integer value = TestUtil.generateRandomInt();
		@SuppressWarnings("rawtypes")
		FilterCondition condition = FilterCondition.create().field(LongMockEntity.FIELD.KEY.COMPARABLE)
				.operator(FilterOperator.EQUALS).value(value).build();
		Filter filter = new Filter.Builder().addPredicateCondition(condition).build();
		buildWriter(filter);
		final List<Predicate<?>> predicates = writer.getFilter().getPredicates();
		TestUtil.assertNotEmpty(predicates);
		TestUtil.assertSize(1, predicates);

	}

	@Test
	public void testPredicateFieldNotRegistered() {
		buildWriter(Filter
				.create()
				.addPredicateCondition(FilterCondition.create().field(FieldTestUtil.generateRandomFieldKey())
						.operator(FilterOperator.EQUALS).value(new Integer(3)).build()).build());
		// no field to find.
		assertErrorHappens();
	}

	@Test
	public void testPredicateNotComparable() {
		buildWriter(Filter
				.create()
				.addPredicateCondition(FilterCondition.create().field(LongMockEntity.FIELD.KEY.COLLECTION)
						.operator(FilterOperator.IN).value(Collections.EMPTY_LIST).build()).build());
		assertErrorHappens();
	}

	/**
	 * @see LongMockEntityNameCountCombinationIndex
	 */
	@Test
	public void testConditionAndOrderCombinedIndexed() {

		Filter filter = new Filter.Builder().addCondition(nameFilterCondition).orderBy(LongMockEntity.FIELD.KEY.COUNT)
				.build();
		buildWriter(filter);
	}

	/**
	 * @param filter
	 */
	private void buildWriter(Filter filter) {
		this.writer = writerBuilder.setFilter(filter).build();
	}

	@Test
	public void testFieldNotRegistered() {
		FieldKey BAD_ID = new FieldKey(LongMockEntity.FIELD.DOMAIN_KEY, "junk");
		Filter filter = new Filter.Builder()
				.addCondition(new FilterCondition.Builder<String>().field(BAD_ID).operator(operator).value(nameValue)
						.build()).addCondition(idFilterCondition).build();
		buildWriter(filter);
		StatusMessages statusMessages = this.writer.getStatusMessages();
		assertTrue(statusMessages.hasError());
		assertEquals(statusMessages.toString(), 2, statusMessages.getMessages().size());
		Collection<StatusMessage> okMessages = statusMessages.getMessagesByStatus(MessageStatus.OK);
		assertEquals(okMessages.toString(), 1, okMessages.size());
	}

	@Test
	@Ignore("currently this isn't tested, but this class should validate type")
	public void testWrongValueTypeProvided() {

	}

	@Test
	public void testOrderByAscending() {
		FieldKey fieldKey = nameKey;
		assertOrderBy(fieldKey);

	}

	/**
	 * @param fieldKey
	 * @return
	 */
	private String assertOrderBy(FieldKey fieldKey) {
		FilterSort orderBy = FilterSort.create().setFieldKey(fieldKey).build();
		Filter filter = Filter.create().orderBy(orderBy).build();
		buildWriter(filter);
		assertEquals(1, query.orders.size());
		String key = query.orders.get(0);
		TestUtil.assertDoesntContain(key, ObjectifyFilterQueryWriter.DESCENDING_PREFIX);
		String queryString = query.orders.get(0);
		TestUtil.assertContains(queryString, fieldKey.getRelativeKey());
		return queryString;
	}

	@Test
	public void testOrderByEmbeddedAscending() {
		FieldKey embeddedNameKey = LongMockEntity.FIELD.KEY.EMBEDDED_PROPERTY_IN_EMBED_1;
		String queryString = assertOrderBy(embeddedNameKey);
		TestUtil.assertContains(queryString, embeddedNameKey.getNested().getRelativeKey());
	}

	@Test
	public void testLimit() {
		Integer expectedLimit = 37;
		Filter filter = Filter.create().setLimit(expectedLimit).build();
		buildWriter(filter);
		assertEquals(expectedLimit, this.query.limit);
		assertEquals(MockQuery.DEFAULT_OFFSET, this.query.offset);
	}

	@Test
	public void testSecondPage() {
		Integer limit = 3;
		Integer page = 2;
		Filter filter = Filter.create().setPage(page).setLimit(limit).build();
		buildWriter(filter);
		assertEquals(limit, this.query.limit);
		assertEquals("the offset should point to the start of page 2", limit, this.query.offset);
	}

	@Test
	public void testCursor() {
		String cursor = ObjectifyRepositoryTestUtil.cursor1();
		Filter filter = Filter.create().setPageCursor(cursor).build();
		buildWriter(filter);
		assertEquals(Cursor.fromWebSafeString(cursor), this.query.startAt);
		assertEquals(MockQuery.DEFAULT_OFFSET, this.query.offset);
		assertEquals(Filter.DEFAULT_LIMIT, this.query.limit);
	}

}
