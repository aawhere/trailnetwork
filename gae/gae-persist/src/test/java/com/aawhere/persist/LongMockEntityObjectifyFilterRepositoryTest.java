/**
 * 
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.test.TestUtil;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * Example how to properly test {@link FilterRepository} using {@link LongMockEntity}.
 * 
 * @author aroller
 * 
 */
@Ignore("TN-756 temporary disabled while working through objectify 5.1 upgrades due to __key__ filter value must be a Key in maven, but not eclipse")
public class LongMockEntityObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<LongMockEntityId, LongMockEntity, LongMockEntityObjectifyRepository, LongMockEntityObjectifyRepository, LongMockFilterEntities, LongMockFilterEntities.Builder> {

	private LongMockEntityObjectifyRepository repository;

	@org.junit.Before
	public void setUp() {
		this.repository = GaePersistTestUtils.createMockEntityObjectifyRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected LongMockEntity createEntity() {
		return LongMockEntityUnitTest.getInstance().getEntitySample();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected LongMockEntityObjectifyRepository getRepository() {
		return this.repository;
	}

	@Test
	public void testFilterByName() {
		LongMockEntity createEntity = createPersistedEntity();
		Filter filter = new Filter.Builder().addCondition(GaePersistTestUtils.getLongMockEntityFilter(createEntity
				.getName())).build();
		LongMockFilterEntities entities = getRepository().filter(filter);
		assertEquals(entities.toString(), 1, entities.size());
	}

	@Test
	public void testFilterByBadName() {
		LongMockEntity createEntity = createPersistedEntity();
		Filter filter = new Filter.Builder().addCondition(GaePersistTestUtils.getLongMockEntityFilter(createEntity
				.getName() + "?")).build();
		LongMockFilterEntities entities = getRepository().filter(filter);
		assertEquals(entities.toString(), 0, entities.size());
	}

	@Test
	public void testFilterByCount() {
		LongMockEntity createEntity = createPersistedEntity();
		LongMockEntity createEntity2 = createPersistedEntity();
		TestUtil.assertNotEquals(createEntity.getCount(), createEntity2.getCount());
		FilterCondition<Integer> condition = new FilterCondition.Builder<Integer>()
				.field(LongMockEntity.FIELD.KEY.COUNT).operator(FilterOperator.EQUALS).value(createEntity.getCount())
				.build();
		Filter filter = new Filter.Builder().addCondition(condition).build();
		LongMockFilterEntities entities = getRepository().filter(filter);
		final int expectedCount = 1;
		assertEquals(entities.toString(), expectedCount, entities.size());
		assertEquals("count is wrong", expectedCount, getRepository().count(filter).intValue());
	}

	@Test
	public void testFilterByEmbedded() {
		LongMockEntity createEntity = createPersistedEntity();
		FilterCondition<Integer> condition = new FilterCondition.Builder<Integer>()
				.field(LongMockEntity.FIELD.KEY.EMBEDDED_PROPERTY_IN_EMBED_1).operator(FilterOperator.EQUALS)
				.value(createEntity.getEmbed1().property).build();
		Filter filter = new Filter.Builder().addCondition(condition).build();
		LongMockFilterEntities entities = getRepository().filter(filter);
		assertEquals(entities.toString(), 1, entities.size());
	}

	@Test
	public void testIdIteratorOnePage() {
		LongMockEntity first = createPersistedEntity();
		LongMockEntity second = createPersistedEntity();
		assertIdIterator(first.getId(), second.getId());
		assertIdIterator(true, first.getId(), second.getId());
	}

	@Test
	public void testIdIteratorHalfPage() {
		LongMockEntity first = createPersistedEntity();
		assertIdIterator(first.getId());
		assertIdIterator(true, first.getId());
	}

	@Test
	public void testPredicate() {
		LongMockEntity first = createPersistedEntity();
		LongMockEntity second = createPersistedEntity();
		Predicate<LongMockEntity> equalToFirst = Predicates.equalTo(first);
		Predicate<LongMockEntity> equalToSecond = Predicates.equalTo(second);
		{
			final int expectedNumberOfResults = 1;
			Filter firstOnlyFilter = Filter.create().addPredicate(equalToFirst).build();
			LongMockFilterEntities firstOnlyResults = this.repository.filter(firstOnlyFilter);
			TestUtil.assertSize(expectedNumberOfResults, firstOnlyResults);
			TestUtil.assertContains(first, firstOnlyResults);
			final Filter resultsFilter = firstOnlyResults.getFilter();
			assertEquals("the filter should reflect one is filtered", expectedNumberOfResults, resultsFilter
					.getFilteredByPredicates().intValue());
			assertEquals("page total reflects how many are in the page", expectedNumberOfResults, resultsFilter
					.getPageTotal().intValue());
			assertTrue("final page", resultsFilter.isFinalPage());
		}
		{
			final int expectedNumberOfResults = 1;
			LongMockFilterEntities secondOnlyResults = this.repository.filter(Filter.create()
					.addPredicate(equalToSecond).build());
			TestUtil.assertSize(expectedNumberOfResults, secondOnlyResults);
			TestUtil.assertContains(second, secondOnlyResults);
			final Filter resultsFilter = secondOnlyResults.getFilter();
			assertEquals("the filter should reflect one is filtered", expectedNumberOfResults, resultsFilter
					.getFilteredByPredicates().intValue());
			assertEquals("page total reflects how many are in the page", expectedNumberOfResults, resultsFilter
					.getPageTotal().intValue());
			assertTrue("final page", resultsFilter.isFinalPage());
		}
		{
			LongMockFilterEntities bothResults = this.repository.filter(Filter.create()
					.addPredicate(Predicates.or(equalToFirst, equalToSecond)).build());
			final int expectedNumberOfResults = 2;
			TestUtil.assertSize(expectedNumberOfResults, bothResults);
			TestUtil.assertContains(first, bothResults);
			TestUtil.assertContains(second, bothResults);
			final Filter resultsFilter = bothResults.getFilter();
			assertEquals("none got filtered", 0, resultsFilter.getFilteredByPredicates().intValue());
			assertEquals("both are in the total", expectedNumberOfResults, resultsFilter.getPageTotal().intValue());
			assertTrue("final page", resultsFilter.isFinalPage());
		}
		{
			// no results since neither entity can be equal to each other
			LongMockFilterEntities noResults = this.repository.filter(Filter.create().addPredicate(equalToFirst)
					.addPredicate(equalToSecond).build());
			TestUtil.assertSize(0, noResults);
		}
	}

	/**
	 * id paginator tests paginator extensively. this just does the basics.
	 */
	@Test
	public void testEntityPaginator() {
		LongMockEntity first = createPersistedEntity();
		LongMockEntity second = createPersistedEntity();
		HashSet<LongMockEntity> expected = Sets.newHashSet(first, second);
		Iterable<LongMockEntity> entityPaginator = this.repository.entityPaginator(Filter.create().build());
		HashSet<LongMockEntity> actual = Sets.newHashSet(entityPaginator);
		assertEquals("iterator didn't find the right results", expected, actual);
	}

	@Test
	public void testEntitiesFromIds() {
		LongMockEntity target = createPersistedEntity();
		LongMockEntity decoy = createPersistedEntity();
		TestUtil.assertNotEquals(target, decoy);
		HashSet<LongMockEntityId> expected = Sets.newHashSet(target.id());
		LongMockFilterEntities actual = this.repository.entities(expected);
		TestUtil.assertIterablesEquals("entities returned more than requested", expected, IdentifierUtils.ids(actual));
		assertFalse("all should be happy", actual.getStatusMessages().hasError());
	}

	@Test
	public void testEntitiesFromIdsOneNotFound() {
		LongMockEntity target = createPersistedEntity();
		LongMockEntity decoy = createPersistedEntity();
		TestUtil.assertNotEquals(target, decoy);
		LongMockEntityId notFoundId = EntityTestUtil.generateRandomMockLongEntityId();
		HashSet<LongMockEntityId> expected = Sets.newHashSet(target.id(), notFoundId);
		LongMockFilterEntities actual = this.repository.entities(expected);
		TestUtil.assertIterablesEquals(	"entities returned more than requested",
										Sets.newHashSet(target.id()),
										IdentifierUtils.ids(actual));
		assertTrue("not found should be reported", actual.getStatusMessages().hasError());
		assertTrue(	"notFoundId should be reported",
					actual.getStatusMessages().getMessages().toString().contains(notFoundId.toString()));
	}

	/**
	 * Calls the {@link FilterRepository#entityPaginator(Filter)} to verify there are no limits.
	 * 
	 */
	@Test
	public void testPaginatorMultiplePages() {
		int numberToCreate = 101;
		List<LongMockEntity> createEntities = createEntities(numberToCreate);
		assertEquals(numberToCreate, createEntities.size());
		{
			Iterable<LongMockEntity> entityPaginator = this.repository.entityPaginator(Filter.create().build());
			int numberOfEntitiesFound = Iterables.size(entityPaginator);
			assertEquals("entities", numberToCreate, numberOfEntitiesFound);
		}
		{
			Iterable<LongMockEntityId> idPaginator = this.repository.idPaginator(Filter.create().build());
			int numberOfIdsFound = Iterables.size(idPaginator);
			assertEquals("ids", numberToCreate, numberOfIdsFound);
		}
		int count = this.repository.count(Filter.create().build());
		assertEquals("count is not accurate", numberToCreate, count);
	}

	@Test
	public void testIdIteratorPageAndOneHalf() throws com.google.appengine.api.datastore.EntityNotFoundException,
			InterruptedException {
		LongMockEntity first = createPersistedEntity();
		LongMockEntity second = createPersistedEntity();
		// notice third excluded from results below
		LongMockEntity third = createPersistedEntity();

		assertIdIterator(first.getId(), second.getId());
		assertIdIterator(true, first.getId(), second.getId(), third.getId());
	}

	@Test
	public void testIdIteratorNone() {
		assertIdIterator();
	}

	private void assertIdIterator(LongMockEntityId... expectedIds) {
		assertIdIterator(false, expectedIds);
	}

	/**
	 * Iterates through the existing tests created entities with a page limit of 2. Various tests
	 * should be run to test the boundaries for validity.
	 * 
	 * @param first
	 * @param second
	 */
	private void assertIdIterator(Boolean paginate, LongMockEntityId... expectedIds) {
		Filter filter = Filter.create().setLimit(2).orderBy(LongMockEntity.FIELD.KEY.DATE_CREATED).build();
		Iterable<LongMockEntityId> iterable;
		if (paginate) {
			iterable = repository.idPaginator(filter);
		} else {
			iterable = this.repository.ids(filter);
		}
		String message = " not found in " + Arrays.toString(expectedIds);
		Integer count = 0;
		for (LongMockEntityId longMockEntityId : iterable) {
			TestUtil.assertGreaterThan(	longMockEntityId + message,
										Index.NONE.index,
										Arrays.binarySearch(expectedIds, longMockEntityId));
			count++;
		}
		assertEquals("acutal not the same as expected", expectedIds.length, count.intValue());
	}

	/**
	 * Tests the automatic inclusion of predicates when filtering by a comparable field that has no
	 * index.
	 * 
	 */
	@Test
	public void testFilterByUnindexedDateUpdated() {
		LongMockEntity entity = createPersistedEntity();
		entity = repository.update(entity);
		Filter filter = Filter
				.create()
				.setAutoPredicates(true)
				.addCondition(FilterCondition.create().field(LongMockEntity.FIELD.KEY.DATE_UPDATED)
						.operator(FilterOperator.LESS_THAN_OR_EQUALS).value(entity.dateUpdated()).build()).build();
		LongMockFilterEntities entities = this.repository.filter(filter);
		TestUtil.assertNotEmpty(entities.getFilter().getPredicates());
		TestUtil.assertContainsOnly(entity, entities);
	}

	@Test
	public void testOrderByName() throws EntityNotFoundException {
		LongMockEntity brian = createPersistedEntity();
		brian = repository.updateName(brian.getId(), "Brian");
		LongMockEntity carol = createPersistedEntity();
		carol = repository.updateName(carol.getId(), "Carol");
		LongMockEntity aaron = createPersistedEntity();
		aaron = repository.updateName(aaron.getId(), "Aaron");
		{
			Filter ascendingFilter = Filter.create().orderBy(LongMockEntity.FIELD.KEY.NAME, Direction.ASCENDING)
					.build();
			LongMockFilterEntities ascendingEntities = repository.filter(ascendingFilter);
			assertEquals(3, ascendingEntities.size());
			List<LongMockEntity> ascendingList = ascendingEntities.getCollection();
			assertEquals(aaron, ascendingList.get(Index.FIRST.index));
			assertEquals(brian, ascendingList.get(Index.SECOND.index));
			assertEquals(carol, ascendingList.get(Index.THIRD.index));
		}
		{
			Filter descendingFilter = Filter.create().orderBy(LongMockEntity.FIELD.KEY.NAME, Direction.ASCENDING)
					.build();
			List<LongMockEntity> descendingList = repository.filter(descendingFilter).getCollection();
			assertEquals(carol, descendingList.get(Index.THIRD.index));
			assertEquals(brian, descendingList.get(Index.SECOND.index));
			assertEquals(aaron, descendingList.get(Index.FIRST.index));
		}

	}

	@Test
	public void testNonExistentEntitiesLowNumber() {
		int numberOfEntities = NonExistingEntitiesFunction.INPUT_SIZE_LIMIT / 2;
		testNonExistingEntities(numberOfEntities);
	}

	@Test
	public void testNonExistentEntitiesMediumNumber() {
		int numberOfEntities = NonExistingEntitiesFunction.INPUT_SIZE_LIMIT;
		testNonExistingEntities(numberOfEntities);
	}

	@Test
	public void testNonExistentEntitiesBigNumber() {
		int numberOfEntities = NonExistingEntitiesFunction.INPUT_SIZE_LIMIT * 5;
		testNonExistingEntities(numberOfEntities);
	}

	/**
	 * Persists a bunch of entities, then searches for those entities and others that don't yet
	 * exist expecting to get back the ids that don't yet exist.
	 * 
	 * @see {@link RepositoryUtil#nonExistingEntityIds(Iterable, FilterRepository)}
	 */
	private void testNonExistingEntities(int numberOfEntities) {
		List<LongMockEntity> existingEntities = createEntities(numberOfEntities);
		Set<LongMockEntityId> existingIds = IdentifierUtils.idsFrom(existingEntities);
		Set<LongMockEntityId> possibleNonExistentIds = IdentifierTestUtil.generateRandomLongIds(LongMockEntity.class,
																								LongMockEntityId.class);
		// makese sure the non-existing isn't already found in the existing
		Set<LongMockEntityId> expectedNonExistingIds = Sets.difference(	Sets.newHashSet(possibleNonExistentIds),
																		existingIds);
		Set<LongMockEntityId> combinedIds = Sets.union(existingIds, expectedNonExistingIds);
		Iterable<LongMockEntityId> nonExistingIdsIterable = RepositoryUtil.nonExistingEntityIds(combinedIds,
																								this.repository);
		HashSet<LongMockEntityId> actualNonExistingIds = Sets.newHashSet(nonExistingIdsIterable);
		assertEquals("incorrect set of ids found", expectedNonExistingIds, actualNonExistingIds);
	}
}
