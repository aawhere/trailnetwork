/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

import org.unitils.reflectionassert.ReflectionAssert;

import com.aawhere.id.Identifier;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

/**
 * utilities for interacting with the datastore. Useful for troubleshooting
 * 
 * @author Brian Chapman
 * 
 */
public class DatastoreTestUtil {

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Entity getEntity(I id)
			throws com.aawhere.persist.EntityNotFoundException {
		try {
			com.googlecode.objectify.Key<?> key = ObjectifyKeyUtils.key(id);
			Key rawKey = key.getRaw();

			DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
			Entity entity;
			entity = ds.get(rawKey);
			return entity;
		} catch (EntityNotFoundException e) {
			throw new com.aawhere.persist.EntityNotFoundException(id);
		}
	}

	public static Iterable<Entity> getEntities(Class<?> entityType) {
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(ObjectifyKeyUtils.getKind(entityType));
		PreparedQuery pq = ds.prepare(q);
		return pq.asIterable();
	}

	public static void truncate(Class<?> kind) {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		Query q = new Query(ObjectifyKeyUtils.getKind(kind));
		PreparedQuery pq = datastore.prepare(q);
		for (Entity result : pq.asIterable()) {
			datastore.delete(result.getKey());
		}
	}

	/*
	 * Verify that the entity has been persisted in the gae datastore.
	 */
	public static <T extends BaseEntity<T, ?>> void assertPersisted(T entity) {
		DatastoreTestUtil instance = new DatastoreTestUtil();

		assertTrue(	new StringBuilder().append("Expected entity ").append(entity.getClass()).append("#")
							.append(entity.getId()).append(" to be persisted but it was not").toString(),
					instance.isPersisted(entity));
	}

	/**
	 * @param entities
	 */
	public static <E extends BaseEntity<E, ?>> void assertAllPersisted(Collection<E> entities) {
		for (E e : entities) {
			assertPersisted(e);
		}
	}

	public static <T extends BaseEntity<T, ?>> void assertIdAvailable(T entity) {
		EntityTestUtil.assertIdAvailable(entity.getClass().getSimpleName() + " doesn't have an ID", entity);
	}

	private <T extends BaseEntity<T, ?>> boolean isPersisted(T entity) {
		EntityTestUtil.assertIdAvailable("Cannot determine if an entity is null if the id is null", entity);
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Entity result;
		try {
			Object idValue = entity.getId().getValue();
			String kind = entity.getClass().getSimpleName();
			Key key;
			if (idValue instanceof Long) {
				key = KeyFactory.createKey(kind, (Long) idValue);
			} else if (idValue instanceof String) {
				key = KeyFactory.createKey(kind, (String) idValue);
			} else {
				throw new IllegalArgumentException("Ids ValueT must be of type Long or String");
			}
			result = ds.get(key);
			if (!equal(result, entity)) {
				return false;
			}
		} catch (EntityNotFoundException e) {
			return false;
		}
		return true;
	}

	/*
	 * Test that an (@See Entity) retrieved from the datastore is equal to an instance of an (@See
	 * EntityObject).
	 */
	@SuppressWarnings("unchecked")
	private <T extends BaseEntity<T, ?>> boolean equal(Entity dsEntity, T entity) {
		if (dsEntity == null && entity == null) {
			return true;
		} else if (entity == null || dsEntity == null) {
			return false;
		} else {
			Object idValue = DatastoreKeyUtil.getIdValue(dsEntity.getKey());
			if (!idValue.equals(entity.getId().getValue())) {
				return false;
			}
		}

		@SuppressWarnings("rawtypes")
		Class clazz = entity.getClass();
		return compareClass(clazz, dsEntity, entity);
	}

	/**
	 * TODO: Note that the call to compareFields does nothing because getClass().getDeclaredFields()
	 * is always empty. We would be using annotations to do the comparison instead of relying on
	 * fields.
	 * 
	 * @deprecated TN-117
	 * 
	 * @param clazz
	 * @param dsEntity
	 * @param entity
	 * @return
	 */
	@Deprecated
	private <T extends BaseEntity<T, ?>> boolean compareClass(Class<T> clazz, Entity dsEntity, BaseEntity<T, ?> entity) {
		Field[] fields = getClass().getDeclaredFields();
		return compareFields(fields, dsEntity, entity);
	}

	private <T extends BaseEntity<T, ?>> boolean
			compareFields(Field[] fields, Entity dsEntity, BaseEntity<T, ?> entity) {
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				if (field.getName() == "id") {
					continue;
				}
				Object dsEntityFieldValue = dsEntity.getProperty(field.getName());
				Object entityFieldValue = field.get(entity);
				ReflectionAssert.assertReflectionEquals(dsEntityFieldValue, entityFieldValue);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				return false;
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> void inspectDataStoreObject(I id)
			throws com.aawhere.persist.EntityNotFoundException {
		Entity entity = getEntity(id);
		printMessage("Inspecting DataStore Object: " + id.toString());
		printEntity(entity);
	}

	public static void inspectDatastoreQuery(Class<?> entityType, String filter, FilterOperator operator, Object value)
			throws EntityNotFoundException {
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		Query query = new Query(ObjectifyKeyUtils.getKind(entityType));
		query.addFilter(filter, operator, value);
		PreparedQuery pq = ds.prepare(query);
		List<Entity> entities = pq.asList(FetchOptions.Builder.withLimit(100));
		printMessage("Inspecting DataStore Objects for: " + entityType.toString() + " with query paramaters " + filter
				+ ", " + operator.toString() + ", " + value.toString());
		printEntities(entities);
	}

	public static void inspectDatastoreObjects(Class<?> entityType) throws EntityNotFoundException {
		Iterable<Entity> entities = getEntities(entityType);
		printMessage("Inspecting DataStore Objects for: " + entityType.toString());
		printEntities(entities);
	}

	public static void printEntities(Iterable<Entity> entities) {
		for (Entity entity : entities) {
			printEntity(entity);
		}
	}

	public static void printMessage(String message) {
		System.out.println("");
		System.out.println("**** " + message + " ****");
	}

	public static void printEntity(Entity entity) {
		// System.out.println("<Entity Parent: " + entity.getParent() + ">");
		System.out.println(entity.toString());
	}

	/**
	 * Provides the leaf node of ancestors to {@link #greatGranparent()}.
	 * 
	 * @return
	 */
	public static StringMockEntityWithParentId child() {
		return new StringMockEntityWithParentId("child", parent());
	}

	/**
	 * @return
	 */
	public static StringMockEntityWithParentId parent() {
		return new StringMockEntityWithParentId("parent", grandParent());
	}

	/**
	 * @param greatGrandparent
	 * @return
	 */
	public static StringMockEntityWithParentId grandParent() {
		return new StringMockEntityWithParentId("grandpa", greatGranparent());
	}

	/**
	 * @return
	 */
	public static StringMockEntityWithParentId greatGranparent() {
		return new StringMockEntityWithParentId("great_grandpa");
	}

	/**
	 * provides a registered entities used in testing the gae-pesist project
	 * 
	 * @return
	 */
	public static RegisteredEntities registeredEntities() {
		RegisteredEntitiesObjectifyFactory registerEntities = new RegisteredEntitiesObjectifyFactory();
		registerEntities.register(LongMockEntityId.class);
		registerEntities.register(StringMockEntityId.class);
		registerEntities.register(LongMockEntityWithParentId.class);
		registerEntities.register(StringMockEntityWithParentId.class);
		return registerEntities;
	}
}
