/**
 * 
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.PersistenceTest;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @see FieldPropertyEntityUtil
 * @author aroller
 * 
 */
@Category(PersistenceTest.class)
public class FieldPropertyEntityPersistenceTest {

	private LocalServiceTestHelper helper;

	@Before
	public void setUpServices() {
		helper = GaePersistTestUtils.createRepositoryTestHelper();
		helper.setUp();
	}

	@After
	public void tearDownServices() {
		helper.tearDown();
	}

	@Test
	public void testStringKey() {
		FieldDefinition<String> propertyDef = FieldTestUtil.generateRandomFieldDefinition();
		FieldDefinition<String> keyDef = FieldTestUtil.generateRandomFieldDefinition(propertyDef.getKey()
				.getDomainKey());

		String key = TestUtil.generateRandomAlphaNumeric();
		String value = TestUtil.generateRandomAlphaNumeric();

		Entity entity = FieldPropertyEntityUtil.entity(key, propertyDef, value);
		assertNotNull(entity);
		assertEquals("entity kind", propertyDef.getKey().getAbsoluteKey(), entity.getKind());
		assertEquals("key value", key, entity.getKey().getName());
		assertEquals("property value", value, entity.getProperty(propertyDef.getColumnName()));
	}

	@Test
	public void testLongKey() {
		// TODO:test the long key is working
	}

}
