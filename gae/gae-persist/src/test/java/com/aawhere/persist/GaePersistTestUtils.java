/**
 *
 */
package com.aawhere.persist;

import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.cache.AsyncCacheFilter;
import com.googlecode.objectify.cache.PendingFutures;

/**
 * @author Brian Chapman
 * 
 */
public class GaePersistTestUtils {

	public static FilterCondition<String> getLongMockEntityFilter(String nameValue) {
		return new FilterCondition.Builder<String>().field(LongMockEntity.FIELD.KEY.NAME)
				.operator(FilterOperator.EQUALS).value(nameValue).build();

	}

	public static LongMockEntityObjectifyRepository createMockEntityObjectifyRepository() {
		FieldAnnotationDictionaryFactory annoationFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		annoationFactory.getDictionary(LongMockEntity.class);
		annoationFactory.getDictionary(StringMockEntity.class);

		LongMockEntityObjectifyRepository repository = new LongMockEntityObjectifyRepository(
				annoationFactory.getFactory());
		return repository;
	}

	/**
	 * When using cache, objectify needs to sync the cache with the datastore on puts and deletes.
	 * Call this method after performing one of those operations in a unit test.
	 * 
	 * @see http ://code.google.com/p/objectify-appengine/wiki/IntroductionToObjectify
	 * @see AsyncCacheFilter
	 */
	public static void syncCache() {
		// AsyncCacheFilter.complete();
		PendingFutures.completeAllPendingFutures();
	}

	public static GaeServiceTestHelper.Builder createRepositoryTestHelperBuilder() {
		GaeServiceTestHelper.Builder builder = GaeServiceTestHelper.createTestHelper().withDatastoreService()
				.withSearchService();
		return builder;
	}

	public static GaeServiceTestHelper.GaeServiceTestHelperBuilder createRepositoryTestHelperGaeBuilder() {
		GaeServiceTestHelper.GaeServiceTestHelperBuilder builder = new GaeServiceTestHelper.GaeServiceTestHelperBuilder()
				.withDatastoreService().withSearchService();
		return builder;
	}

	public static LocalServiceTestHelper createRepositoryTestHelper() {
		LocalServiceTestHelper helper = createRepositoryTestHelperBuilder().build();
		return helper;
	}
}
