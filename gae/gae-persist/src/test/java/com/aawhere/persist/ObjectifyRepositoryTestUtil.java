/**
 *
 */
package com.aawhere.persist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.mutable.MutableInt;

import com.aawhere.id.Identifier;
import com.aawhere.lang.If;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.persist.objectify.DaoBase;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreTimeoutException;
import com.google.appengine.api.datastore.Index;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.LoadResult;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.cmd.Query;
import com.googlecode.objectify.cmd.QueryKeys;
import com.googlecode.objectify.cmd.SimpleQuery;

/**
 * Sometimes you need to bypass the restrictions that repositories present to setup your test
 * fixtures ...
 * 
 * @author Brian Chapman
 * 
 */
public class ObjectifyRepositoryTestUtil<EntityT extends BaseEntity<EntityT, IdentifierT>, IdentifierT extends Identifier<?, EntityT>>
		extends DaoBase {

	public static <EntityT extends BaseEntity<EntityT, IdentifierT>, IdentifierT extends Identifier<?, EntityT>>
			ObjectifyRepositoryTestUtil<EntityT, IdentifierT> create(Class<EntityT> entityType) {
		return new ObjectifyRepositoryTestUtil<EntityT, IdentifierT>();
	}

	/**
	 * put or update the entity.
	 * 
	 * @param entity
	 */
	public EntityT store(EntityT entity) {
		Result<Key<EntityT>> result = ofy().save().entity(entity);
		result.now();
		return entity;
	}

	/**
	 * Cursors must be valid even in unit testing so provide some to get past basic tests.
	 * 
	 * @return
	 */
	public static String cursor1() {
		return "E-ABAIICHmoEdGVzdHIWCxIHQWNjb3VudCIJZXQtMTYzMjAzDBQ";
	}

	/**
	 * Unimplemented query except for those things we want to verify
	 * 
	 * @author aroller
	 * 
	 */
	public static class MockQuery
			implements Query<LongMockEntity> {
		static final Integer DEFAULT_OFFSET = 0;
		ArrayList<String> conditions = new ArrayList<String>();
		ArrayList<Object> values = new ArrayList<Object>();
		boolean keyFilter = false;
		ArrayList<String> orders = new ArrayList<String>();
		Integer offset = DEFAULT_OFFSET;
		Integer limit;
		Integer chunk;
		Cursor startAt;
		QueryResultIterable<LongMockEntity> iterable = new QueryResultMockIterable();

		public MockQuery() {
		}

		public MockQuery(ArrayList<LongMockEntity> iterable) {
			this.iterable = new QueryResultMockIterable(iterable);
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.SimpleQuery#filterKey(java.lang.Object)
		 */
		@Override
		public SimpleQuery<LongMockEntity> filterKey(Object value) {
			throw new NotImplementedException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.SimpleQuery#keys()
		 */
		@Override
		public QueryKeys<LongMockEntity> keys() {
			throw new NotImplementedException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.SimpleQuery#count()
		 */
		@Override
		public int count() {
			throw new NotImplementedException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.QueryExecute#iterable()
		 */
		@Override
		public QueryResultIterable<LongMockEntity> iterable() {
			return this.iterable;
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.QueryExecute#list()
		 */
		@Override
		public List<LongMockEntity> list() {
			throw new NotImplementedException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.appengine.api.datastore.QueryResultIterable#iterator()
		 */
		@Override
		public QueryResultIterator<LongMockEntity> iterator() {
			return this.iterable.iterator();
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#filter(java.lang.String, java.lang.Object)
		 */
		@Override
		public Query<LongMockEntity> filter(String condition, Object value) {
			conditions.add(condition);
			values.add(value);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#order(java.lang.String)
		 */
		@Override
		public Query<LongMockEntity> order(String condition) {
			orders.add(condition);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#filterKey(java.lang.String, java.lang.Object)
		 */
		@Override
		public Query<LongMockEntity> filterKey(String condition, Object value) {
			this.keyFilter = true;
			this.filter(condition, value);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#ancestor(java.lang.Object)
		 */
		@Override
		public Query<LongMockEntity> ancestor(Object keyOrEntity) {
			throw new NotImplementedException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#limit(int)
		 */
		@Override
		public Query<LongMockEntity> limit(int value) {
			this.limit = value;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#offset(int)
		 */
		@Override
		public Query<LongMockEntity> offset(int value) {
			this.offset = value;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.googlecode.objectify.cmd.Query#startAt(com.google.appengine.api.datastore.Cursor)
		 */
		@Override
		public Query<LongMockEntity> startAt(Cursor value) {
			this.startAt = value;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#endAt(com.google.appengine.api.datastore.Cursor)
		 */
		@Override
		public Query<LongMockEntity> endAt(Cursor value) {
			throw new NotImplementedException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#chunk(int)
		 */
		@Override
		public Query<LongMockEntity> chunk(int value) {
			this.chunk = value;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#chunkAll()
		 */
		@Override
		public Query<LongMockEntity> chunkAll() {
			throw new NotImplementedException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.cmd.Query#hybrid(boolean)
		 */
		@Override
		public Query<LongMockEntity> hybrid(boolean force) {
			throw new NotImplementedException();
		}

		@Override
		public LoadResult<LongMockEntity> first() {
			// TODO Auto-generated method stub
			com.aawhere.lang.NotImplementedException.throwIt();
			return null;
		}

		@Override
		public Query<LongMockEntity> reverse() {
			// TODO Auto-generated method stub
			com.aawhere.lang.NotImplementedException.throwIt();
			return null;
		}

		@Override
		public Query<LongMockEntity> filter(Filter filter) {
			// TODO Auto-generated method stub
			com.aawhere.lang.NotImplementedException.throwIt();
			return null;
		}

		@Override
		public Query<LongMockEntity> orderKey(boolean descending) {
			// TODO Auto-generated method stub
			com.aawhere.lang.NotImplementedException.throwIt();
			return null;
		}

		@Override
		public Query<LongMockEntity> project(String... fields) {
			// TODO Auto-generated method stub
			com.aawhere.lang.NotImplementedException.throwIt();
			return null;
		}

		@Override
		public Query<LongMockEntity> distinct(boolean value) {
			// TODO Auto-generated method stub
			com.aawhere.lang.NotImplementedException.throwIt();
			return null;
		}

	}

	static public class QueryResultMockIterable
			implements QueryResultIterable<LongMockEntity> {
		public static final int NO_ACTIONS = 0;
		public static final int COUNT_MEANS_THROW_TIMEOUT_DURING_NEXT = 1;
		public static final int COUNT_MEANS_THROW_TIMEOUT_DURING_HAS_NEXT = 2;
		public static final int COUNT_MEANS_THROW_TIMEOUT_DURING_ITERATOR_CREATION = 3;

		final private ArrayList<LongMockEntity> iterable;
		// the index doesn't reset upon new iteration to accommodate "cursor".
		final private MutableInt index = new MutableInt(0);

		/**
		 * 
		 */
		public QueryResultMockIterable(ArrayList<LongMockEntity> iterable) {
			this.iterable = iterable;
		}

		public QueryResultMockIterable() {
			this(new ArrayList<LongMockEntity>());
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.appengine.api.datastore.QueryResultIterable#iterator()
		 */
		@Override
		public QueryResultIterator<LongMockEntity> iterator() {

			return new QueryResultIterator<LongMockEntity>() {

				{
					peek(COUNT_MEANS_THROW_TIMEOUT_DURING_ITERATOR_CREATION);
				}

				/**
				 * @return
				 * 
				 */
				private boolean peek(int context) {

					boolean hasNext = iterable.size() > index.intValue();
					if (hasNext) {
						handle(iterable.get(index.intValue()), context);
					}
					return hasNext;
				}

				@Override
				public void remove() {
					// the index automatically behaves like an ignore
				}

				@Override
				public LongMockEntity next() {

					final LongMockEntity next = iterable.get(index.intValue());
					index.add(1);
					handle(next, COUNT_MEANS_THROW_TIMEOUT_DURING_NEXT);
					return next;
				}

				/**
				 * @param next
				 */
				private void handle(final LongMockEntity next, int context) {
					final int count = If.nil(next.getCount()).use(NO_ACTIONS);
					if (count == context) {

						switch (count) {
							case QueryResultMockIterable.COUNT_MEANS_THROW_TIMEOUT_DURING_NEXT:
								throw new DatastoreTimeoutException("timeout during next call");
							case QueryResultMockIterable.COUNT_MEANS_THROW_TIMEOUT_DURING_HAS_NEXT:
								// this was a bad one so we must get past it
								index.add(1);
								throw new DatastoreTimeoutException("timeout during has next call");
							case QueryResultMockIterable.COUNT_MEANS_THROW_TIMEOUT_DURING_ITERATOR_CREATION:
								// this was a bad one so we must get past it
								index.add(1);
								throw new DatastoreTimeoutException("timeout during iterator creation");
							default:
								throw new InvalidChoiceException(context);
						}

					}
				}

				@Override
				public boolean hasNext() {
					return peek(COUNT_MEANS_THROW_TIMEOUT_DURING_HAS_NEXT);
				}

				@Override
				public List<Index> getIndexList() {
					return Collections.emptyList();
				}

				@Override
				public Cursor getCursor() {
					return Cursor.fromWebSafeString(cursor1());
				}
			};
		}

	}
}
