//package com.aawhere.persist.conv;
//
//import java.util.ArrayList;
//
//import javax.measure.quantity.Angle;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import com.aawhere.measure.GeoCoordinate;
//import com.aawhere.measure.Latitude;
//import com.aawhere.measure.Longitude;
//import com.aawhere.measure.MeasurementUtil;
//import com.aawhere.measure.QuantityTranslatorFactory;
//
//import com.googlecode.objectify.ObjectifyService;
//import com.googlecode.objectify.impl.conv.Conversions;
//
//public class GeoCoordinateConverterUnitTest extends BaseConverterTest {
//
//	Double latDouble;
//	Double lonDouble;
//	GeoCoordinate geo;
//	ArrayList<Double> expected = new ArrayList<Double>();
//
//	@Before
//	public void setuUp() {
//		setupConversions();
//		super.setUp(converter = new GeoCoordinateConverter(ObjectifyService.factory()
//				.getConversions()));
//		latDouble = new Double(0.17);
//		lonDouble = new Double(0.27);
//		Angle latAngle = MeasurementUtil.createAngleInRadians(latDouble);
//		Angle lonAngle = MeasurementUtil.createAngleInRadians(lonDouble);
//		geo = new GeoCoordinate(new Latitude(latAngle), new Longitude(lonAngle));
//		expected.add(latDouble);
//		expected.add(lonDouble);
//	}
//
//	@After
//	public void tearDown() {
//		// Brian: Would like to set ObjectifyService.factory().getConversions()'s list of
//		// Conversions to 0. But how?
//		// Why? because that is a global variable and it bit me once already.
//	}
//
//	@Test
//	public void testForDatastore() {
//		testConvToDatastore(geo, expected);
//	}
//
//	@Test
//	public void testForPojo() {
//		testConvFromDatastore(expected, GeoCoordinate.class, geo);
//	}
//
//	/*
//	 * Only setup the conversions we need for this unit test.
//	 */
//	private void setupConversions() {
//		Conversions conv = ObjectifyService.factory().getConversions();
//		conv.add(new QuantityTranslatorFactory());
//		conv.add(new LongitudeConverter(conv));
//		conv.add(new LatitudeConverter(conv));
//	}
// }
