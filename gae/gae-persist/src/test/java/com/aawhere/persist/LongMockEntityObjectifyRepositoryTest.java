/**
 *
 */
package com.aawhere.persist;

import org.junit.After;
import org.junit.Before;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @see LongMockEntityObjectifyFilterRepositoryTest
 * @author Aaron Roller
 * 
 */
public class LongMockEntityObjectifyRepositoryTest
		extends LongMockEntityUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void before() {
		helper.setUp();
	}

	@After
	public void tearDown() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.persist.LongMockEntityUnitTest#createRepository()
	 */
	@Override
	protected LongMockEntityRepository createRepository() {
		return GaePersistTestUtils.createMockEntityObjectifyRepository();
	}

}
