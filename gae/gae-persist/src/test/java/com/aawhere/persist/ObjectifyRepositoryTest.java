package com.aawhere.persist;

import static com.aawhere.persist.EntityTestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.aawhere.test.TestUtil;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Sets;
import com.googlecode.objectify.Key;

/**
 * Tests the common functionality of {@link ObjectifyRepository}.
 * 
 * @see use LongMockEntityObjectifyRepositoryTest
 * 
 * @author aroller
 * 
 */
public class ObjectifyRepositoryTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private LongMockEntityObjectifyRepository repository;
	private LongMockEntity entity;
	private String name;
	private StringMockEntityId relationId;

	@Before
	public void setUp() throws Exception {
		helper.setUp();
		relationId = new StringMockEntityId(TestUtil.generateRandomString());
		entity = new LongMockEntity.Builder().setName(name).setRelated(relationId).build();
		repository = GaePersistTestUtils.createMockEntityObjectifyRepository();
	}

	@After
	public void tearDown() throws Exception {
		helper.tearDown();
	}

	@Test
	public void testCreate() {
		LongMockEntity entity = new LongMockEntity.Builder().build();
		assertIdNull("", entity);
	}

	@Test
	public void testStore() throws EntityNotFoundException {
		repository.store(entity);
		LongMockEntityId id = entity.getId();
		assertNotNull("Id should not be null", id);
		Integer expectedVersion = 1;
		assertEquals("Version should have been incremented", expectedVersion, entity.getEntityVersion());
		DatastoreTestUtil.assertPersisted(entity);
		LongMockEntity found = repository.load(id);
		assertEquals(relationId, found.getRelated());
		assertEquals(name, found.getName());
	}

	@Test
	public void testStoreLoadAll() {
		List<LongMockEntity> entities = createListOfEntities();

		Map<LongMockEntityId, LongMockEntity> stored = repository.storeAll(entities);
		DatastoreTestUtil.assertAllPersisted(entities);
		DatastoreTestUtil.assertAllPersisted(stored.values());
		IdentifierTestUtil.assertIds(stored.keySet(), stored.values());

		final Set<LongMockEntityId> ids = IdentifierUtils.idsFrom(entities);
		Map<LongMockEntityId, LongMockEntity> loadedEntities = repository.loadAll(ids);
		IdentifierTestUtil.assertIds(ids, loadedEntities.values());

	}

	@Test
	public void testLoadAllIterable() {
		List<LongMockEntity> entities = createListOfEntities();
		Map<LongMockEntityId, LongMockEntity> stored = repository.storeAll(entities);
		Map<LongMockEntityId, LongMockEntity> loaded = repository.loadAll(stored.keySet());
		assertEquals("loading didn't find same as stored", stored, loaded);
		// let's mess with it and make sure it handles not finding anything
		HashSet<LongMockEntityId> withMissing = Sets.newHashSet(loaded.keySet());
		withMissing.add(new LongMockEntityId(-1l));

		// demonstrates ids not in the datastore are not returned
		Map<LongMockEntityId, LongMockEntity> loadedWithMissing = repository.loadAll(withMissing);
		assertEquals("supposed to ignore the missing id", stored, loadedWithMissing);

	}

	@Test
	public void testStoreAllWithEmptyList() {
		List<LongMockEntity> entities = new ArrayList<LongMockEntity>();
		repository.storeAll(entities);
		// Should work without exceptions.
	}

	@Test(expected = InvalidArgumentException.class)
	public void testStoreAlreadyStored() {
		LongMockEntity mockEntity = repository.store(entity);
		LongMockEntity restored = repository.store(mockEntity);
		TestUtil.assertNotEquals(mockEntity.getEntityVersion(), restored.getEntityVersion());
		assertEquals(mockEntity.getId(), restored.getId());

	}

	@Test(expected = InvalidArgumentException.class)
	public void testUpdateWithoutId() {
		repository.update(entity);

	}

	@Test
	public void testUpdate() throws EntityNotFoundException {
		Integer origionalVersion = entity.getEntityVersion();

		repository.store(entity);
		String mutatedName = TestUtil.generateRandomString();
		LongMockEntity mutated = repository.updateName(entity.getId(), mutatedName);

		DatastoreTestUtil.assertPersisted(mutated);
		assertEquals(	"Version should have been incremented",
						new Integer(origionalVersion + 2),
						mutated.getEntityVersion());
		assertEquals(mutatedName, mutated.getName());
	}

	@Test(expected = NullPointerException.class)
	public void testUpdateWithNullId() throws EntityNotFoundException {
		repository.updateName(null, TestUtil.generateRandomString());
	}

	@Test
	public void testIdToKey() {
		LongMockEntityId id = new LongMockEntityId(1l);
		Key<LongMockEntity> key = ObjectifyKeyUtils.key(id);
		assertEquals((long) id.getValue(), key.getId());
		assertEquals(Key.getKind(id.getKind()), key.getKind());

	}

	@Test
	public void testKeyToId() {
		final Long idValue = TestUtil.generateRandomLong();
		Key<LongMockEntity> key = Key.create(LongMockEntity.class, idValue);
		LongMockEntityId id = ObjectifyKeyUtils.id(key, LongMockEntityId.class);
		assertEquals(idValue, id.getValue());
		assertEquals(LongMockEntity.class, id.getKind());
	}

	private List<LongMockEntity> createListOfEntities() {
		List<LongMockEntity> entities = new ArrayList<LongMockEntity>();
		entities.add(new LongMockEntity.Builder().build());
		entities.add(new LongMockEntity.Builder().build());
		return entities;
	}

	/**
	 * Getting a raw query requires a datastore environment.
	 * 
	 */
	@Test
	public void testRawQuery() {
		Class<? extends LongMockEntity> entityType = this.entity.getClass();
		com.googlecode.objectify.cmd.Query<?> inQuery = repository.ofy().load().type(entityType);

		Filter filter = Filter.create().build();
		Query rawQuery = ObjectifyFilterQueryWriter.create().setQuery(inQuery).setEntityType(entityType)
				.setFieldDictionaryFactory(repository.dictionaryFactory()).setFilter(filter).build().getRawQuery();
		assertNotNull(rawQuery);

	}

}
