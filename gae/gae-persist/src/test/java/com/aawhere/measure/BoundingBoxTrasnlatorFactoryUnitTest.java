/**
 * 
 */
package com.aawhere.measure;

import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.googlecode.objectify.annotation.Entity;

/**
 * @author aroller
 * 
 */
public class BoundingBoxTrasnlatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<BoundingBoxTrasnlatorFactoryUnitTest.BoundingBoxTestEntity> {

	public BoundingBoxTrasnlatorFactoryUnitTest() {
		super(new BoundingBoxTranslatorFactory(), BoundingBoxTestEntity.class);

	}

	@Entity
	public static class BoundingBoxTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {
		public BoundingBox pojo;
	}

	@Override
	public void testBasic() {
		BoundingBoxTestEntity entity = new BoundingBoxTestEntity();
		entity.pojo = BoundingBoxTestUtils.createRandomBoundingBox();
		test(entity.pojo.getBounds(), entity);

	}
}
