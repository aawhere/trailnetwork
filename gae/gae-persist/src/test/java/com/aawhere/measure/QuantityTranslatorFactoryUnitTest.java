package com.aawhere.measure;

import javax.measure.quantity.Length;

import com.aawhere.measure.geocell.QuantityTranslatorFactory;
import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.googlecode.objectify.annotation.Entity;

public class QuantityTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<QuantityTranslatorFactoryUnitTest.QuantityTestEntity> {

	public QuantityTranslatorFactoryUnitTest() {
		super(new QuantityTranslatorFactory(), QuantityTestEntity.class);

	}

	@Entity
	public static class QuantityTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {
		public Length pojo;
	}

	@Override
	public void testBasic() {
		QuantityTestEntity quantityTestEntity = new QuantityTestEntity();
		quantityTestEntity.pojo = MeasureTestUtil.createRandomLength();
		test(quantityTestEntity.pojo.toString(), quantityTestEntity);

	}

}
