/**
 *
 */
package com.aawhere.util.rb;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.aawhere.test.TestUtil;
import com.googlecode.objectify.annotation.Entity;

/**
 * @author roller
 * @see MessageTranslatorFactory
 */
public class MessageTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<MessageTranslatorFactoryUnitTest.MessageTestEntity> {

	public MessageTranslatorFactoryUnitTest() {
		super(new MessageTranslatorFactory(), MessageTestEntity.class);
	}

	@Entity
	public static class MessageTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {
		public Message pojo;
		ExampleMessage exposed;
	}

	/**
	 * If the field exposes exactly the enum that is also a message, we should treat it as an enum
	 * rather than the more difficult Message that is hiding what is actually stored.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testEnumeration() throws Exception {
		ExampleMessage message = ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE;
		MessageTestEntity entity = new MessageTestEntity();
		entity.pojo = message;
		entity.exposed = message;
		test("enum  " + message, entity);
	}

	@Override
	protected void assertActual(String message, MessageTestEntity expected, MessageTestEntity actual) {
		super.assertActual(message, expected, actual);
		assertEquals(message + " for exposed", expected.exposed, actual.exposed);
	}

	@Test
	@Override
	public void testBasic() {
		String messageValue = TestUtil.generateRandomString();
		StringMessage message = new StringMessage(messageValue);
		MessageTestEntity entity = new MessageTestEntity();
		entity.pojo = message;
		test("string message " + messageValue, entity);
	}

}
