/**
 *
 */
package com.aawhere.search;

import static com.aawhere.search.SearchTestUtil.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldTestUtil;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.GaeServiceTestHelper;
import com.aawhere.persist.LongMockEntity;
import com.aawhere.persist.LongMockEntityId;
import com.aawhere.persist.LongMockEntityWithParent;
import com.aawhere.persist.StringMockEntity;
import com.aawhere.persist.StringMockEntityId;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;
import com.aawhere.search.SearchDocument.Field;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ServiceTest;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.base.Function;

/**
 * @author Brian Chapman
 * 
 */
@Category(ServiceTest.class)
public class GaeSearchServiceTest {

	private SearchService searchService;
	private LocalServiceTestHelper helper = GaeServiceTestHelper.createTestHelper().withSearchService().build();

	protected FieldAnnotationDictionaryFactory dictionaryFactory;
	protected SearchDocument entity;

	@Before
	public void setUp() throws MaxIndexAttemptsExceeded {
		dictionaryFactory = FieldTestUtil.getFieldAnnotationDictionaryFactory();

		dictionaryFactory.getFactory().register(dictionaryFactory.getDictionary(LongMockEntity.class));
		dictionaryFactory.getFactory().register(dictionaryFactory.getDictionary(LongMockEntityWithParent.class));

		entity = SearchTestUtil
				.getSampleSearchDocument(	Lists.newArrayList(	MOCK_SEARCH_FIELD1,
																MOCK_SEARCH_FIELD2,
																MOCK_SEARCH_FIELD3),
											MOCK_SEARCH_DOC_DOMAIN);
		helper.setUp();

		searchService = new GaeSearchService(dictionaryFactory, GaeSearchTestUtils.createAdapterFactory(),
				Synchronization.SYNCHRONOUS);
		searchService.index(entity);
	}

	@After
	public void tearDown() throws IOException {
		helper.tearDown();
	}

	@Test
	public void testIndexEntity() throws FieldNotSearchableException, UnknownIndexException {

		Filter filter = new Filter.Builder().addCondition(createKey1FitlerCondition()).build();

		List<SearchDocument.Field> expectedFields = Lists.newArrayList();
		expectedFields.add(MOCK_SEARCH_FIELD1);
		expectedFields.add(MOCK_SEARCH_FIELD2);
		expectedFields.add(MOCK_SEARCH_FIELD3);

		QueryTest queryTest = new QueryTest().with(filter).expectedSize(1).withDomain(MOCK_SEARCH_DOC_DOMAIN)
				.with(expectedFields);
		assertQuery(queryTest);
	}

	/**
	 * Not every index is a perfect match with a domain. Using a suffix from
	 * {@link SearchUtils#domainFromIndex(String)} or
	 * {@link SearchUtils#indexName(String, String...)} will allow extensions to the domain for
	 * specialty indexes
	 * 
	 * @throws MaxIndexAttemptsExceeded
	 * @throws UnableToIndexException
	 * @throws UnknownIndexException
	 * @throws FieldNotSearchableException
	 * 
	 */
	@Test
	public void testIndexNameWithSuffix() throws UnableToIndexException, MaxIndexAttemptsExceeded,
			FieldNotSearchableException, UnknownIndexException {
		String indexName = SearchUtils.indexName(MOCK_SEARCH_DOC_DOMAIN, "test");

		SearchDocument expected = SearchTestUtil.searchDocumentBuilder().index(indexName).addField(MOCK_SEARCH_FIELD1)
				.id(id()).build();
		SearchDocument notExpected = SearchTestUtil.searchDocumentBuilder().index(MOCK_SEARCH_DOC_DOMAIN)
				.addField(MOCK_SEARCH_FIELD1).id(id()).build();

		getSearchService().index(expected);
		SearchDocuments searchDocuments = getSearchService().query(Filter.create().build(), indexName);
		TestUtil.assertContainsOnly(expected, searchDocuments);
		TestUtil.assertDoesntContain(notExpected, searchDocuments);
	}

	/**
	 * Ensures those numerous substring fields are being removed when all fields are being returned.
	 */
	@Test
	public void testAllFieldsWithoutSubstrings() {
		String indexName = MOCK_SEARCH_DOC_DOMAIN_WITHOUT_FIELD_DECLARATOIN;
		SearchDocument searchDocument = SearchTestUtil.searchDocumentBuilder().index(indexName).id(id())
				.addField(MOCK_SEARCH_FIELD1).addField(MOCK_SEARCH_FIELD2).addField(MOCK_SEARCH_FIELD3)
				.addFields(GaeSearchUtil.substringFields("junk", MOCK_SEARCH_FIELD_NAME1)).build();
		index(searchDocument);
		SearchDocuments searchDocuments = getSearchService().query(	Filter.create().addSearchIndex(indexName).build(),
																	indexName);
		TestUtil.assertSize(1, searchDocuments);
		SearchDocument document = searchDocuments.iterator().next();
		List<Field> fields = document.getFields();
		TestUtil.assertSize("expected to remove substrings", 3, fields);
	}

	private void index(SearchDocument searchDocument) {
		try {
			getSearchService().index(searchDocument);
		} catch (UnableToIndexException | MaxIndexAttemptsExceeded e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}

	}

	/**
	 * Select requesting of fields should only return those requested.
	 * 
	 * @throws MaxIndexAttemptsExceeded
	 * @throws UnableToIndexException
	 * @throws UnknownIndexException
	 * @throws FieldNotSearchableException
	 */
	@Test
	public void testFieldRequest() throws UnableToIndexException, MaxIndexAttemptsExceeded,
			FieldNotSearchableException, UnknownIndexException {
		SearchService searchService = getSearchService();
		String index = MOCK_SEARCH_DOC_DOMAIN_WITHOUT_FIELD_DECLARATOIN;
		SearchDocument searchDocument = SearchTestUtil.searchDocumentBuilder().index(index).id(id())
				.addField(MOCK_SEARCH_FIELD1).addField(MOCK_SEARCH_FIELD2).addField(MOCK_SEARCH_FIELD3).build();
		searchService.index(searchDocument);

		// by default, all fields should be returned
		{
			Filter filter = Filter.create().build();
			SearchDocuments searchDocuments = searchService.query(filter, index);
			TestUtil.assertIterablesEquals(	"Fields for search document",
											SearchUtils.keys(searchDocument.fields()),
											SearchUtils.keys(searchDocuments.iterator().next().fields()));
		}

		// request a single field
		{
			String requested = MOCK_SEARCH_FIELD1.key();
			Filter filter = Filter.create().addField(requested).build();
			SearchDocuments searchDocuments = searchService.query(filter, index);
			TestUtil.assertContainsOnly(requested, SearchUtils.keys(searchDocuments.iterator().next().fields()));
		}
	}

	@Test
	public void testQueryLimit() throws MaxIndexAttemptsExceeded, FieldNotSearchableException, UnknownIndexException {
		SearchDocument doc1 = SearchTestUtil.searchDocumentBuilder().id(id()).domain(MOCK_SEARCH_DOC_DOMAIN)
				.addField(MOCK_SEARCH_FIELD1).build();
		getSearchService().index(doc1);
		SearchDocument doc2 = SearchTestUtil.searchDocumentBuilder().id(id()).domain(MOCK_SEARCH_DOC_DOMAIN)
				.addField(MOCK_SEARCH_FIELD2).build();
		getSearchService().index(doc2);

		Filter filter20 = new Filter.Builder().setLimit(20).build();
		SearchDocuments result20 = getSearchService().query(filter20, MOCK_SEARCH_DOC_DOMAIN);
		assertEquals(Integer.valueOf(3), result20.size());
		assertEquals(Integer.valueOf(3), result20.getFilter().getPageTotal());
		assertTrue(result20.getFilter().isFinalPage());

		Filter filter1 = new Filter.Builder().setLimit(1).build();
		SearchDocuments result1 = getSearchService().query(filter1, MOCK_SEARCH_DOC_DOMAIN);
		assertEquals(Integer.valueOf(1), result1.size());
		assertEquals(Integer.valueOf(1), result1.getFilter().getPageTotal());
		assertTrue(!result1.getFilter().isFinalPage());
		assertTrue(result1.getFilter().isFirstPage());

		Filter filter1NextPage = Filter.clone(filter1).incrementPage().build();
		SearchDocuments result1NextPage = getSearchService().query(filter1NextPage, MOCK_SEARCH_DOC_DOMAIN);
		assertEquals(Integer.valueOf(1), result1NextPage.size());
		assertEquals(Integer.valueOf(1), result1NextPage.getFilter().getPageTotal());
		assertTrue(!result1NextPage.getFilter().isFinalPage());
		assertTrue(!result1NextPage.getFilter().isFirstPage());
	}

	private LongMockEntityId id() {
		return new LongMockEntityId(TestUtil.generateRandomId());
	}

	@Test(expected = MultipleIndexesNotSupportedException.class)
	public void testQueryMultipleIndexes() throws MaxIndexAttemptsExceeded, FieldNotSearchableException,
			UnknownIndexException {
		dictionaryFactory.getFactory().register(dictionaryFactory.getDictionary(StringMockEntity.class));

		String doc1Domain = StringMockEntity.FIELD.DOMAIN;
		FieldKey doc1Field1Key = new FieldKey(doc1Domain, StringMockEntity.FIELD.NAME);
		SearchDocument.Field doc1Field1 = new SearchDocument.Field.Builder().key(doc1Field1Key.getAbsoluteKey())
				.dataType(FieldDataType.TEXT).value(doc1Field1Key.getAbsoluteKey()).build();
		SearchDocument doc1 = SearchTestUtil.searchDocumentBuilder()
				.id(new StringMockEntityId(TestUtil.generateRandomId().toString())).domain(doc1Domain)
				.addField(doc1Field1).build();
		getSearchService().index(doc1);

		Filter filter = new Filter.Builder().setLimit(1).build();
		SearchDocuments result = getSearchService().query(	filter,
															Lists.newArrayList(MOCK_SEARCH_DOC_DOMAIN, doc1Domain));
		assertEquals(Integer.valueOf(1), result.size());
		assertEquals(Integer.valueOf(1), result.getFilter().getPageTotal());
		assertTrue(!result.getFilter().isFinalPage());

	}

	@Test
	public void testQueryNoResults() throws FieldNotSearchableException, UnknownIndexException {
		FilterCondition<String> condition = new FilterCondition.Builder<String>().field("junk-garbage")
				.operator(FilterOperator.EQUALS).value("doesnt exist").build();
		String domain = LongMockEntity.FIELD.DOMAIN_KEY;
		Filter filter = new Filter.Builder().addCondition(condition).build();

		QueryTest queryTest = new QueryTest().with(filter).withDomain(domain).expectedSize(0);
		assertQuery(queryTest);
	}

	@Test
	public void testDeleteAllFilter() throws MaxIndexAttemptsExceeded, FieldNotSearchableException,
			UnknownIndexException {
		final String indexName = MOCK_SEARCH_DOC_DOMAIN;
		Function<List<SearchDocument>, Integer> deleteSupplier = new Function<List<SearchDocument>, Integer>() {

			@Override
			public Integer apply(List<SearchDocument> input) {
				Filter filter = Filter.create().addSearchIndex(indexName).build();
				getSearchService().delete(filter);
				return 0;
			}
		};

		deleteAsserted(indexName, deleteSupplier);
		// testing deleting many confirms that the delete batch size is respected as is the filter
		// limits
		deleteAsserted(indexName, deleteSupplier, GaeSearchService.LIMIT_MAX + 10);

	}

	public void deleteAsserted(final String indexName, Function<List<SearchDocument>, Integer> deleteSupplier)
			throws MaxIndexAttemptsExceeded {
		int docsToCreate = Filter.DEFAULT_LIMIT + 1;
		deleteAsserted(indexName, deleteSupplier, docsToCreate);
	}

	public void deleteAsserted(final String indexName, Function<List<SearchDocument>, Integer> deleteSupplier,
			int docsToCreate) throws MaxIndexAttemptsExceeded {
		ArrayList<SearchDocument> docs = new ArrayList<SearchDocument>();
		for (int i = 0; i < docsToCreate; i++) {
			docs.add(SearchTestUtil.searchDocumentBuilder().id(id()).domain(indexName).addField(MOCK_SEARCH_FIELD1)
					.build());

		}
		getSearchService().index(docs);

		Integer expectedDocumentCountRemaining = deleteSupplier.apply(docs);
		SearchDocuments result = getSearchService().searchDocuments(Filter.create().addSearchIndex(indexName)
		// limit to 1 more than expected will prove if we receive expected
				.setLimit(expectedDocumentCountRemaining + 1).build());
		TestUtil.assertSize(expectedDocumentCountRemaining, result);
	}

	@Test
	@Ignore("TN-894 introduces this delete method and its mostly working, but always leaves 1 orphan? revisit later")
	public void testDeleteAllDocumentsById() throws MaxIndexAttemptsExceeded {
		final String index = MOCK_SEARCH_DOC_DOMAIN;
		Function<List<SearchDocument>, Integer> deleteFunction = new Function<List<SearchDocument>, Integer>() {

			@Override
			public Integer apply(List<SearchDocument> input) {
				searchService.delete(SearchUtils.ids(input), index);
				return 0;
			}
		};
		deleteAsserted(index, deleteFunction);
	}

	@Test
	@Ignore("TN-894 introduces this delete method and its mostly working, but always leaves 1 orphan? revisit later")
	public void testDeleteAllDocuments() throws MaxIndexAttemptsExceeded {
		final String index = MOCK_SEARCH_DOC_DOMAIN;
		Function<List<SearchDocument>, Integer> deleteFunction = new Function<List<SearchDocument>, Integer>() {

			@Override
			public Integer apply(List<SearchDocument> input) {
				searchService.delete(input);
				return 0;
			}
		};
		deleteAsserted(index, deleteFunction);
	}

	private void assertQuery(QueryTest queryTest) throws FieldNotSearchableException, UnknownIndexException {
		SearchDocuments result = getSearchService().query(queryTest.filter, queryTest.domain);

		assertNotNull(result);
		assertEquals(queryTest.expectedResultSize, result.size());
		assertEquals(queryTest.expectedResultSize, result.getFilter().getPageTotal());
		assertEquals(Integer.valueOf(1), result.getFilter().getPage());
		assertTrue(result.getFilter().isFirstPage());
		assertTrue(result.getFilter().isFinalPage());

		for (SearchDocument.Field expectedField : queryTest.expectedFields) {
			Boolean found = false;
			for (SearchDocument document : result) {
				for (SearchDocument.Field resultField : document.fields()) {
					if (resultField.key().equals(expectedField.key())
							&& resultField.value().equals(expectedField.value())) {
						found = true;
					}
				}
			}
			assertTrue("Could not find " + expectedField.key() + " in result list.", found);
		}
	}

	public static class QueryTest {
		public Filter filter;
		public String domain;
		public Integer expectedResultSize;
		List<SearchDocument.Field> expectedFields = Lists.newArrayList();

		public QueryTest with(Filter filter) {
			this.filter = filter;
			return this;
		}

		public QueryTest withDomain(String domain) {
			this.domain = domain;
			return this;
		}

		public QueryTest expectedSize(Integer expectedResultSize) {
			this.expectedResultSize = expectedResultSize;
			return this;
		}

		public QueryTest with(List<SearchDocument.Field> expectedFields) {
			this.expectedFields = expectedFields;
			return this;
		}

	}

	private FilterCondition<?> createKey1FitlerCondition() {
		return new FilterCondition.Builder<String>().field(MOCK_SEARCH_FIELD_NAME1.getAbsoluteKey())
				.operator(FilterOperator.EQUALS).value(MOCK_SEARCH_FIELD_NAME1.getAbsoluteKey())//
				.build();
	}

	@Test
	public void testJaxbRoundTrip() throws FieldNotSearchableException, MaxIndexAttemptsExceeded, UnknownIndexException {

		Filter filter = Filter.create().build();
		Class<?>[] classes = { SearchDocument.class, SearchDocument.Field.class, SearchDocuments.class };
		SearchDocuments docs = getSearchService().query(filter, Lists.newArrayList(entity.index()));
		JAXBTestUtil<SearchDocuments> util = JAXBTestUtil.create(docs).classesToBeBound(classes).writeOnly().build();
		assertNotNull(util.getXml());
		util.assertContains(entity.index());
		// System.out.println(util.getXml());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.search.SearchServiceUnitTest#getSearchService()
	 */
	protected SearchService getSearchService() {
		return searchService;
	}

}
