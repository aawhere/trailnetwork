/**
 *
 */
package com.aawhere.search;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.field.FieldKey;

/**
 * @author brian
 * 
 */
public class GaeSearchUtilsUnitTest {

	@Test
	public void testNormalizeKey() {
		String domain = "domain";
		String relativeKey = "myField_Key";
		String searchKeyNormalized = "domain_myField__Key";
		FieldKey fieldKey = new FieldKey(domain, relativeKey);
		String normalizedKey = GaeSearchUtil.normalizeKey(fieldKey);
		assertEquals(searchKeyNormalized, normalizedKey);
		String denormalized = GaeSearchUtil.denormalizeKey(normalizedKey);
		assertEquals(fieldKey.getAbsoluteKey(), denormalized);
	}
}
