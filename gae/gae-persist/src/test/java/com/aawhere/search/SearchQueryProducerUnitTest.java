/**
 *
 */
package com.aawhere.search;

import static org.junit.Assert.*;

import javax.measure.quantity.Length;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.Builder;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.LongMockEntity;
import com.aawhere.test.TestUtil;
import com.google.appengine.api.search.Query;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;

/**
 * @author Brian Chapman
 * 
 */
public class SearchQueryProducerUnitTest {

	private final String NAME = "MyName";
	private DataTypeAdapterFactory adapterFactory;
	private FieldAnnotationDictionaryFactory definitionFactory;

	@Before
	public void setUp() {
		adapterFactory = GaeSearchTestUtils.createAdapterFactory();
		this.definitionFactory = FieldAnnotationTestUtil.getAnnoationFactory().register(LocationMockEntity.class);
	}

	@Test
	@Ignore
	public void testProduceQuery() throws FieldNotSearchableException {

		Filter filter = GaeSearchTestUtils.createFilter(SearchTestUtil.createSearchDocumentIdFilterCondition(10L),
														createCondition("MyName", "MyName", FilterOperator.EQUALS));
		Query query = new GaeSearchQueryProducer.Builder().setFilter(filter).setAdapterFactory(adapterFactory)
				.setFieldDictionaryFactory(definitionFactory).buildQuery().query();
		assertNotNull(query);
		assertEquals("id = 10 AND name = " + NAME, query.getQueryString());
	}

	@Test
	public void testInOperator() {
		Filter filter = filterBuilder().addCondition(FilterCondition.create()
				.field(TestUtil.generateRandomAlphaNumeric()).operator(FilterOperator.IN).build()).build();
		String queryString = query(filter).getQueryString();
		TestUtil.assertContains(queryString, FilterOperator.EQUALS.operator);
	}

	private Builder filterBuilder() {
		return Filter.create().addSearchIndex(LocationMockEntity.DOMAIN_KEY);
	}

	private Query query(Filter filter) {

		return builder().setFilter(filter).build().query();
	}

	private com.aawhere.search.GaeSearchQueryProducer.Builder builder() {
		return new GaeSearchQueryProducer.Builder().setAdapterFactory(adapterFactory)
				.setFieldDictionaryFactory(definitionFactory);
	}

	@Test
	@Ignore
	public void testProduceQueryWithQuery() throws FieldNotSearchableException {
		String queryString = "Chainsaw";
		Filter filterNoQuery = GaeSearchTestUtils
				.createFilter(	SearchTestUtil.createSearchDocumentIdFilterCondition(10L),
								createCondition("MyName", "MyName", FilterOperator.EQUALS));
		Filter filter = Filter.clone(filterNoQuery).setQuery(queryString).build();
		Query query = new GaeSearchQueryProducer.Builder().setFilter(filter).setAdapterFactory(adapterFactory)
				.setAdapterFactory(adapterFactory).buildQuery().query();
		assertNotNull(query);
		assertEquals("id = 10 AND name = " + NAME + " AND " + queryString, query.getQueryString());
	}

	private FilterCondition<Object> createCondition(String key, String value, FilterOperator operator) {
		return FilterCondition.create().field(key).value(value).operator(operator).build();
	}

	@Test
	public void testLocationQuery() throws FieldNotSearchableException, FieldNotRegisteredException {

		Filter filter = filterBuilder().setBounds(GeoCellTestUtil.MARIN_VIEW_BOUNDS).build();
		Query query = new GaeSearchQueryProducer.Builder().setFilter(filter).setAdapterFactory(adapterFactory)
				.setFieldDictionaryFactory(definitionFactory).buildQuery().query();
		assertNotNull(query);

		TestUtil.assertContains(query.getQueryString(), "distance(" + GaeSearchQueryProducer.FIELD.GAE.LOCATION
				+ ", geopoint(");
	}

	@Test
	public void testLocationWW_306() throws FieldNotSearchableException, FieldNotRegisteredException {

		Filter filter = filterBuilder()
				.setBounds("37.867918513661614,-122.54911076687011|37.89000403075805,-122.49503743312988").build();

		String geoQuery = new GaeSearchQueryProducer.Builder().setFilter(filter).setAdapterFactory(adapterFactory)
				.setFieldDictionaryFactory(definitionFactory).geoQueryAppended(filter, StringUtils.EMPTY);
		assertNotNull(geoQuery);
		// this parsing is based on distance(search_location,
		// geopoint(37.878961272209835,-122.5220741)) < 1229.2686767578125
		String[] strings = StringUtils.split(geoQuery, "<");
		Length radius = MeasurementUtil.createLengthInMeters(Double.valueOf(strings[1]));
		Range<Comparable<Length>> range = Range.lessThan(MeasurementUtil.comparable(MeasurementUtil
				.createLengthInMeters(1500)));
		MeasureTestUtil.assertRange(range, radius);

	}

	/**
	 * A condition with a collection will invoke an "OR" condition
	 * 
	 */
	@Test
	public void testConditionWithMultipleValuesResultingInOrCondition() {
		String one = TestUtil.generateRandomAlphaNumeric();
		String two = TestUtil.generateRandomAlphaNumeric();
		ImmutableList<String> conditionValue = ImmutableList.of(one, two);
		assertCondition(conditionValue, one, two, GaeSearchOperator.OR.operator);
	}

	@Test
	public void testSingleCondition() {
		String expected = TestUtil.generateRandomAlphaNumeric();
		assertCondition(expected, expected, FilterOperator.EQUALS.operator);
	}

	private void assertCondition(Object conditionValue, String... expectedToken) {
		String field = TestUtil.generateRandomAlphaNumeric();
		FilterCondition<?> condition = FilterCondition.create().field(field).value(conditionValue).build();
		Filter filter = filterBuilder().addCondition(condition).build();
		GaeSearchQueryProducer searchQueryProducer = builder().setFilter(filter).build();
		Query query = searchQueryProducer.query();
		String queryString = query.getQueryString();
		for (String string : ImmutableList.<String> builder().add(expectedToken).build()) {
			TestUtil.assertContains(queryString, string);
		}
		TestUtil.assertContains(queryString, field);
	}

	/**
	 * Extension of {@link LongMockEntity} to provide location properties.
	 * 
	 * @author Brian Chapman
	 * 
	 */
	@Dictionary(domain = LocationMockEntity.DOMAIN_KEY)
	public static class LocationMockEntity
			extends LongMockEntity {

		private static final long serialVersionUID = 8101541321436756337L;
		public static final String DOMAIN_KEY = "locationMock";

		public LocationMockEntity() {
		}

		@com.aawhere.field.annotation.Field(searchable = true, dataType = FieldDataType.GEO)
		private GeoCoordinate location = new GeoCoordinate.Builder().setAsString("1,2").build();

		/**
		 * @return the location
		 */
		public GeoCoordinate getLocation() {
			return this.location;
		}
	}
}
