/**
 *
 */
package com.aawhere.search;

import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.measure.GeoCoordinateDataTypeGeoAdapter;
import com.aawhere.measure.QuantityDataTypeNumberAdapter;
import com.aawhere.measure.geocell.GeoCellDataTypeTextAdapter;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;

/**
 * @author Brian Chapman
 * 
 */
public class GaeSearchTestUtils {

	public static DataTypeAdapterFactory createAdapterFactory() {
		DataTypeAdapterFactory adapterFactory = new DataTypeAdapterFactory();
		adapterFactory.register(new QuantityDataTypeNumberAdapter(), FieldDataType.NUMBER);
		adapterFactory.register(new GeoCellDataTypeTextAdapter(), FieldDataType.TEXT);
		adapterFactory.register(new GeoCoordinateDataTypeGeoAdapter(), FieldDataType.GEO);
		return adapterFactory;
	}

	public static GaeSearchService createGaeSearchService() {
		DataTypeAdapterFactory adapterFactory = GaeSearchTestUtils.createAdapterFactory();
		return createGaeSearchService(adapterFactory);
	}

	public static GaeSearchService createGaeSearchService(DataTypeAdapterFactory adapterFactory) {
		return createGaeSearchService(FieldAnnotationTestUtil.getAnnoationFactory(), adapterFactory);
	}

	public static GaeSearchService createGaeSearchService(FieldAnnotationDictionaryFactory dictionaryFactory,
			DataTypeAdapterFactory adapterFactory) {

		return new GaeSearchService(dictionaryFactory, adapterFactory, Synchronization.SYNCHRONOUS);

	}

	public static GaeSearchService createGaeSearchService(FieldAnnotationDictionaryFactory dictionaryFactory) {
		return createGaeSearchService(dictionaryFactory, GaeSearchTestUtils.createAdapterFactory());
	}

	public static Filter createFilter(FilterCondition<?>... conditions) {
		Integer limit = 10;
		Filter.Builder filterBuilder = new Filter.Builder().setLimit(limit);
		for (FilterCondition<?> condition : conditions) {
			filterBuilder.addCondition(condition);
		}
		return filterBuilder.build();
	}

}
