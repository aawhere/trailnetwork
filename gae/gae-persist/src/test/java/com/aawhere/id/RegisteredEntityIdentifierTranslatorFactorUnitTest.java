/**
 * 
 */
package com.aawhere.id;

import org.junit.Test;

import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.persist.EntityTestUtil;
import com.aawhere.persist.LongMockEntityId;
import com.aawhere.persist.LongMockEntityWithParentId;
import com.aawhere.persist.RegisteredEntities;
import com.aawhere.persist.RegisteredEntitiesObjectifyFactory;
import com.aawhere.persist.StringMockEntityId;
import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.googlecode.objectify.SaveException;
import com.googlecode.objectify.annotation.Entity;

/**
 * Runs the same test as declared identifiers, but also handles generic.
 * 
 * @author aroller
 * 
 */
public class RegisteredEntityIdentifierTranslatorFactorUnitTest
		extends
		ValueTranslatorFactoryObjectifyTest<RegisteredEntityIdentifierTranslatorFactorUnitTest.IdentifierTestEntity> {

	public RegisteredEntityIdentifierTranslatorFactorUnitTest() {
		super(new RegisteredEntityIdentifierTranslatorFactory(registeredEntities()), IdentifierTestEntity.class);

	}

	@Entity
	public static class IdentifierTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {
		public Identifier<?, ?> pojo;
	}

	public static RegisteredEntities registeredEntities() {
		RegisteredEntitiesObjectifyFactory registeredEntities = new RegisteredEntitiesObjectifyFactory();
		registeredEntities.register(LongMockEntityId.class);
		registeredEntities.register(LongMockEntityWithParentId.class);
		// registeredEntities.register(ChildId.class);
		registeredEntities.register(StringMockEntityId.class);
		return registeredEntities;
	}

	@Override
	public void testBasic() {
		test(EntityTestUtil.generateRandomMockLongEntityId());
	}

	@Test
	public void testWithParent() {
		test(EntityTestUtil.generateRandomEntityWithParentId());
	}

	@Test(expected = SaveException.class)
	public void testChildId() {
		test(new ChildId());
	}

	@Test
	public void testString() {
		test(EntityTestUtil.generateRandomMockLongEntityId());
	}

	private void test(Identifier<?, ?> id) {
		IdentifierTestEntity entity = new IdentifierTestEntity();
		entity.pojo = id;
		test(id.getClass().toString(), entity);
	}

}
