/**
 * 
 */
package com.aawhere.id;

import com.aawhere.persist.EntityTestUtil;
import com.aawhere.persist.LongMockEntityId;
import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.googlecode.objectify.annotation.Entity;

/**
 * @author aroller
 * 
 */
public class LongIdentifierTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<LongIdentifierTranslatorFactoryUnitTest.LongIdentifierTestEntity> {

	public LongIdentifierTranslatorFactoryUnitTest() {
		super(new LongIdentifierTranslatorFactory(), LongIdentifierTestEntity.class);
	}

	@Entity
	public static class LongIdentifierTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {
		public LongMockEntityId pojo;
	}

	@Override
	public void testBasic() {
		LongMockEntityId id = EntityTestUtil.generateRandomMockLongEntityId();
		LongIdentifierTestEntity entity = new LongIdentifierTestEntity();
		entity.pojo = id;
		test("long id", entity);

	}

}
