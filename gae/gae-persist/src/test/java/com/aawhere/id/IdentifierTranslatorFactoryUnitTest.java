/**
 *
 */
package com.aawhere.id;

import org.junit.Ignore;

import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.aawhere.test.TestUtil;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Subclass;

/**
 * @author roller
 * 
 */
@Ignore("This one is failing in maven, but passing in eclipse")
public class IdentifierTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<IdentifierTranslatorFactoryUnitTest.IdentifierTestEntity> {

	public IdentifierTranslatorFactoryUnitTest() {
		super(new IdentifierTranslatorFactory(), IdentifierTestEntity.class);

	}

	@Entity
	@Subclass
	public static class IdentifierTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {
		public ChildId pojo;
	}

	@Override
	public void testBasic() {
		String expectedValue = TestUtil.generateRandomString();
		ChildId expected = new ChildId(expectedValue);
		IdentifierTestEntity entity = new IdentifierTestEntity();
		entity.pojo = expected;
		test("child id", entity);

	}
}
