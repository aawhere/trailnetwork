package com.googlecode.objectify.impl.conv.joda;

import org.joda.time.Interval;

import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest.TestEntity;
import com.googlecode.objectify.annotation.Entity;

public class IntervalTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<TestEntity> {

	public IntervalTranslatorFactoryUnitTest() {
		super(new IntervalTranslatorFactory());
	}

	@Entity
	static class IntervalTestEntity
			extends TestEntity {

		public IntervalTestEntity(Interval interval) {
			this.pojo = interval;
		}

		public Interval pojo;

	}

	@Override
	public void testBasic() {
		test("epoch til now", new IntervalTestEntity(new Interval(0, System.currentTimeMillis())));

	}

	@Override
	public void testNull() {
		test("null", new IntervalTestEntity(null));
	}

}
