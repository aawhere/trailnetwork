package com.googlecode.objectify.impl.conv.joda;

import org.joda.time.DateTime;

import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.googlecode.objectify.annotation.Entity;

public class DateTimeTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<DateTimeTranslatorFactoryUnitTest.DateTimeTestEntity> {

	public DateTimeTranslatorFactoryUnitTest() {
		super(new DateTimeTranslatorFactory());
	}

	@Entity
	public static class DateTimeTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {

		public DateTimeTestEntity(DateTime dateTime) {
			this.pojo = dateTime;
		}

		public DateTime pojo;

	}

	@Override
	public void testBasic() {
		expected("now", new DateTimeTestEntity(DateTime.now()));
		super.testRoundTrip();

	}

	@Override
	public void testNull() {
		expected("null", new DateTimeTestEntity(null));
		testRoundTrip();
	}

}
