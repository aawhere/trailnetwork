package com.googlecode.objectify.impl.conv.joda;

import org.joda.time.Duration;

import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.googlecode.objectify.annotation.Entity;

public class DurationTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<DurationTranslatorFactoryUnitTest.DurationTestEntity> {

	public DurationTranslatorFactoryUnitTest() {
		super(new DurationTranslatorFactory());
	}

	@Entity
	static class DurationTestEntity
			extends com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest.TestEntity {
		public Duration pojo;

		public DurationTestEntity(Duration duration) {
			this.pojo = duration;
		}
	}

	@Override
	public void testBasic() {
		test("random", new DurationTestEntity(JodaTestUtil.duration()));
	}

	@Override
	public void testNull() {
		test("random", new DurationTestEntity(null));
	}

}
