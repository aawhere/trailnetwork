package com.googlecode.objectify.impl.conv.joda;

import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.ISOChronology;

import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.googlecode.objectify.annotation.Entity;

public class ChronologyConverterUnitTest
		extends ValueTranslatorFactoryObjectifyTest<ChronologyConverterUnitTest.ChronologyTestEntity> {

	public ChronologyConverterUnitTest() {
		super(new ChronologyTranslatorFactory(), ChronologyConverterUnitTest.ChronologyTestEntity.class);
	}

	@Entity
	public static class ChronologyTestEntity
			extends ValueTranslatorFactoryObjectifyTest.TestEntity {
		public Chronology pojo;

		public ChronologyTestEntity() {
		}

		public ChronologyTestEntity(Chronology chronology) {
			this.pojo = chronology;
		}
	}

	private String timeZoneId = "America/Los_Angeles";

	@Override
	public void testBasic() {
		test(timeZoneId, new ChronologyTestEntity(ISOChronology.getInstance(DateTimeZone.forID(timeZoneId))));
	}

}
