/**
 * 
 */
package com.googlecode.objectify.impl.conv.joda;

import java.lang.reflect.Field;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

/**
 * Container so we can get {@link Field}s.
 * 
 * @author Brian Chapman
 * 
 */
public class DateTimeContainer {

	Chronology chronology;
	DateTime dateTime;
	Duration duration;
	Interval interval;

	public static Field chronologogyField() {
		return FieldUtils.getField(DateTimeContainer.class, "chronology", true);
	}

	public static Field dateTimeField() {
		return FieldUtils.getField(DateTimeContainer.class, "dateTime", true);
	}

	public static Field durationField() {
		return FieldUtils.getField(DateTimeContainer.class, "duration", true);
	}

	public static Field intervalField() {
		return FieldUtils.getField(DateTimeContainer.class, "interval", true);
	}
}
