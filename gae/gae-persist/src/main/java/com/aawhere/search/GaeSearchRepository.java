package com.aawhere.search;

import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.MutableRepository;
import com.aawhere.persist.Repository;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.inject.Inject;

/**
 * Searches and updates the {@link SearchService} when appropriate and delegates to the given
 * datastore {@link Repository} when appropriate. Many of the methods are pass-through to the
 * datastore repository. Others selectively call the search service based on the query given. There
 * are no search-service only methods.
 * 
 * This assists in keeping the two repositories in sync. During retrieval from the search service
 * and subsequent loading of entities, if there is no entity found matching a search result the
 * missing item is logged and the call to delete the search document is made. The reciprocal does
 * not happen, but instead during updates to the entity this will update both the datstore
 * repository and the search search repository.
 * 
 * The datastore
 * 
 * @author aroller
 * 
 * @param <S>
 * @param <E>
 * @param <I>
 */
abstract public class GaeSearchRepository<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		implements CompleteRepository<S, E, I>, MutableRepository<E, I> {

	private static final Logger LOGGER = LoggerFactory.getLogger(GaeSearchRepository.class);
	final protected SearchService searchService;
	final protected CompleteRepository<S, E, I> datastoreRepository;

	@Inject
	protected GaeSearchRepository(CompleteRepository<S, E, I> datastoreRepository, SearchService searchService) {
		this.searchService = searchService;
		this.datastoreRepository = datastoreRepository;
	}

	@Override
	public S filter(Filter filter) {
		if (SearchUtils.isDatastoreFilter(filter)) {
			return datastoreRepository.filter(prepareForDatastore(filter));
		} else {
			Pair<Iterable<E>, SearchDocuments> results = search(filter);
			return entities(results.getKey(), results.getRight());
		}
	}

	@Override
	public Iterable<E> filterAsIterable(Filter filter) {
		if (SearchUtils.isDatastoreFilter(filter)) {
			return datastoreRepository.filter(prepareForDatastore(filter));
		} else {
			return search(prepareForSearch(filter)).getKey();
		}
	}

	/**
	 * Internal method allowing access to iterable without blocking.
	 * 
	 * @param filter
	 * @return
	 */
	private Pair<Iterable<E>, SearchDocuments> search(Filter filter) {
		// notice there is nothing here that blocks
		Filter searchFilter = prepareForSearch(filter);
		SearchDocuments searchDocuments = searchService.searchDocuments(searchFilter);
		Iterable<I> ids = idsFromSearchDocumentIds(SearchUtils.ids(searchDocuments));
		Map<I, E> all = datastoreRepository.loadAll(ids);
		return Pair.of(Iterables.transform(searchDocuments, loadAllResultFunction(all)), searchDocuments);
	}

	/**
	 * a stream of search documents come and the corresponding trailhead, pre-loaded, is found and
	 * returned. If no trailhead is found when the search document provided the id the the
	 * missingHandler is notified and how to handle it is up to the client.
	 * 
	 * @param trailheadMap
	 * @param missingHandler
	 * @return
	 */
	private Function<SearchDocument, E> loadAllResultFunction(final Map<I, E> trailheadMap) {
		return new Function<SearchDocument, E>() {
			@Override
			public E apply(SearchDocument searchDocument) {
				if (searchDocument == null) {
					return null;
				}
				String searchDocumentId = searchDocument.id();
				I trailheadId = idFromSearchDocumentId(searchDocumentId);
				E trailhead = trailheadMap.get(trailheadId);
				// TODO: populate entity with search document results here
				if (trailhead == null) {
					searchService.delete(searchDocumentId, searchDocument.index());
					LOGGER.warning(searchDocumentId + " didn't find a corresponding "
							+ getEntityClassRepresented().getSimpleName());
				}
				return trailhead;
			}
		};
	}

	/**
	 * 
	 * 
	 * @param ids
	 * @return
	 */
	@Override
	public S entities(Iterable<I> ids) {
		// repository is better for id access
		return datastoreRepository.entities(ids);
	}

	@Override
	public Integer count(Filter filter) {
		return datastoreRepository.count(filter);
	}

	/**
	 * May use search {@link SearchService}.
	 */
	@Override
	public Iterable<I> idPaginator(Filter filter) {
		if (SearchUtils.isDatastoreFilter(filter)) {
			return datastoreRepository.idPaginator(prepareForDatastore(filter));
		} else {
			Filter searchFilter = prepareForSearch(filter);
			Iterable<String> searchDocumentIds = searchService.searchDocumentIds(searchFilter, null);
			return idsFromSearchDocumentIds(searchDocumentIds);
		}
	}

	/**
	 * Each entity should be able to decipher it's id from the SearchDocumentId.
	 * 
	 * @param searchDocumentIds
	 * @return
	 */
	abstract protected Iterable<I> idsFromSearchDocumentIds(Iterable<String> searchDocumentIds);

	protected Iterable<I> idsFromSearchDocuments(Iterable<SearchDocument> searchDocuments) {
		return idsFromSearchDocumentIds(SearchUtils.ids(searchDocuments));
	}

	/**
	 * Helper method to call {@link #idsFromSearchDocumentIds(Iterable)}. Override if you like, but
	 * not necessary.
	 * 
	 * @param searchDocumentId
	 * @return
	 */
	protected I idFromSearchDocumentId(String searchDocumentId) {
		return idsFromSearchDocumentIds(ImmutableList.of(searchDocumentId)).iterator().next();
	}

	/**
	 * An optional method for an entity handler to prepare the filter for defaults that should be
	 * applied if the filter has been built without using the standard FiltBuilder. This is common
	 * from the API. Applyling a default sort is a good example.
	 */
	protected Filter prepareForDatastore(Filter filter) {
		return filter;
	}

	/**
	 * Same as {@link #prepareForDatastore(Filter)}, but for the {@link SearchService}.
	 * 
	 * @param filter
	 * @return
	 */
	protected Filter prepareForSearch(Filter filter) {
		return filter;
	}

	@Override
	public Iterable<I> ids(Filter filter) {
		return idPaginator(filter);
	}

	@Override
	public Iterable<E> entityPaginator(Filter filter) {
		return filterAsIterable(filter);
	}

	@Override
	public FieldDictionary dictionary() {
		return datastoreRepository.dictionary();
	}

	@Override
	public FieldDictionaryFactory dictionaryFactory() {
		return datastoreRepository.dictionaryFactory();
	}

	@Override
	public E store(E entity) {
		E stored = datastoreRepository.store(entity);
		// we need the id to index
		index(stored);
		return stored;
	}

	@Override
	public E update(E entity) {
		return store(entity);
	}

	/**
	 * updates the appropriate search indexes
	 * 
	 * @param entity
	 *            the same entity passed in unchanged, but useful for chaining
	 * @return
	 */
	protected E index(E entity) {
		return index(entity, null);
	}

	/**
	 * Provided to the implementation to provide situations where the number of documents may change
	 * based on the logic of it's search document producer. If a change has been made where the
	 * search documents "may" have changed, this will make extra effort to run search documents for
	 * the old and the new and compare the two. If the old produces more than the new, those
	 * documents will be removed.
	 * 
	 * @param incoming
	 * @param previous
	 * @return
	 */
	protected E index(@Nonnull E incoming, @Nullable E previous) {
		try {
			Set<SearchDocument> searchDocuments = ImmutableSet.<SearchDocument> builder()
					.addAll(searchDocuments(ImmutableList.of(incoming))).build();
			searchService.index(searchDocuments);
			// TN-894 calling #searchDocumentOrphansRemoved here causes WAY TOO MANY reads
			// look at the difference of documents produce and remove orphans directly
			if (previous != null) {
				Set<SearchDocument> previousDocuments = ImmutableSet.<SearchDocument> builder()
						.addAll(searchDocuments(ImmutableList.of(previous))).build();
				//
				SetView<SearchDocument> orphans = Sets.difference(previousDocuments, searchDocuments);
				if (!orphans.isEmpty()) {
					searchService.delete(orphans);
					if (LOGGER.isLoggable(Level.INFO)) {
						LOGGER.info(incoming.id() + " removing orphaned search documents " + orphans);
					}
				}
			}
			return incoming;
		} catch (MaxIndexAttemptsExceeded e) {
			throw ToRuntimeException.wrapAndThrow(e);
		}
	}

	/**
	 * Searches for orphans to be removed
	 * 
	 * @param entityId
	 * @param searchDocuments
	 */
	public void searchDocumentOrphansRemoved(I entityId, Iterable<SearchDocument> searchDocuments) {
		Set<String> updatingIds = ImmutableSet.<String> builder().addAll(SearchUtils.ids(searchDocuments)).build();

		// remove any orphans
		ImmutableSet<String> indexNames = indexNames();
		for (String indexName : indexNames) {
			Filter filter = Filter.create().idEqualTo(entityId).addSearchIndex(indexName).build();
			Set<String> allRelatedIds = ImmutableSet.<String> builder().addAll(searchService.searchDocumentIds(filter))
					.build();
			SetView<String> orphanIds = Sets.difference(allRelatedIds, updatingIds);
			if (!orphanIds.isEmpty()) {
				searchService.delete(orphanIds, indexName);
				LOGGER.info(entityId + " index being updated and orphans were found and removed " + orphanIds);
			}
		}
	}

	@Override
	public void persist(E entity) {

		// the index needs an id so we must block for it if it didn't come with
		if (EntityUtil.isPersisted(entity)) {
			datastoreRepository.persist(entity);
		} else {
			entity = datastoreRepository.store(entity);
		}
		index(entity);
	}

	@Override
	public Map<I, E> storeAll(Iterable<E> entities) {
		Map<I, E> stored = datastoreRepository.storeAll(entities);
		try {
			// this has to block to get the ids
			searchService.index(searchDocuments(stored.values()));
		} catch (MaxIndexAttemptsExceeded e) {
			throw ToRuntimeException.wrapAndThrow(e);
		}
		return stored;
	}

	@Override
	public E load(I id) throws EntityNotFoundException {
		return datastoreRepository.load(id);
	}

	@Override
	public Map<I, E> loadAll(Iterable<I> ids) {
		return datastoreRepository.loadAll(ids);
	}

	@Override
	public Set<I> idsExisting(Iterable<I> ids) {
		return datastoreRepository.idsExisting(ids);
	}

	@Override
	public void delete(E entity) {
		deleteAll(ImmutableList.of(entity));
	}

	@Override
	public void delete(I entityId) {
		this.deleteAllFromIds(ImmutableList.of(entityId));
	}

	@Override
	public void deleteAll(Iterable<E> entities) {
		// although tempting to get teh trailheads and delete the search documents found..
		// if the trailhead is already gone we won't find it and the orphan in the search service
		// remains
		// filtering for the trailhead by id in the search service is the best way
		// must make separate delate calls for every entity
		deleteAllFromIds(IdentifierUtils.ids(entities));
	}

	@Override
	public void deleteAllFromIds(Iterable<I> entityIds) {

		Function<I, Iterable<Filter>> function = deleteAllFromIdsFilterFunction();
		// using a loop because lazy transform won't ever commit this.
		// the delete is async anyway
		for (I id : entityIds) {
			Iterable<Filter> filters = function.apply(id);
			for (Filter filter : filters) {
				this.searchService.delete(filter);
			}
		}
		this.datastoreRepository.deleteAllFromIds(entityIds);
	}

	/**
	 * When an entity is removed from the datastore we must also remove it from the search service.
	 * Given a single identifier the function must provide one or more filters for each document
	 * that must be deleted. The {@link SearchService#delete(Filter)} supports multiple indexes as
	 * long as the targeted indexes follow the same conditions. If not then provide multiple filters
	 * targeting each index separately with their own conditions.
	 * 
	 * @param entityIds
	 */
	protected abstract Function<I, Iterable<Filter>> deleteAllFromIdsFilterFunction();

	/**
	 * All index names managed for the implementing repository. This may not mean all indexes
	 * related to a particular entity, but all those indexes that may possibly be modified by this
	 * repository.
	 * 
	 * @return
	 */
	protected abstract ImmutableSet<String> indexNames();

	@Override
	public Class<E> getEntityClassRepresented() {
		return datastoreRepository.getEntityClassRepresented();
	}

	@Override
	public Boolean exists(I id) {
		return datastoreRepository.exists(id);
	}

	/**
	 * Given the entity this will translate the entity into a search document specific to the type
	 * of entity
	 * 
	 * @param entity
	 * @return
	 */
	abstract protected Iterable<SearchDocument> searchDocuments(Iterable<E> entities);

	/**
	 * Implementor should provide the {@link BaseFilterEntities} provided the entities and filter.
	 * 
	 * 
	 * @param entities
	 * @param supplier
	 *            provides access to the filter without blocking...presumably the search filter
	 * @return
	 */
	abstract protected S entities(Iterable<E> entities, SearchDocuments searchDocuments);
}
