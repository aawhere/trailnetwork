/**
 *
 */
package com.aawhere.search;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang.ClassUtils;

import com.aawhere.adapter.Adapter;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifier;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.number.NumberUtilsExtended;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.Builder;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.search.SearchDocument.Field;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field.FieldType;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * @author Brian Chapman
 * 
 */
public class GaeSearchUtil
		extends FieldUtils {

	public static final String SEARCH_KEY_DELIMINATOR = "_";
	private static final String FIELD_KEY_DELIMINATOR = String.valueOf(FieldUtils.KEY_DILIM);
	private static final String FIELD_KEY_SUBSTRING = "Substrings";

	/**
	 * Convert from {@link FieldKey} notation to a notation that is compatible with Google
	 * AppEngine's search keys.
	 * 
	 * @param fieldKey
	 * @return
	 */
	public static String normalizeKey(FieldKey fieldKey) {
		return normalizeKey(fieldKey.getAbsoluteKey());
	}

	/**
	 * Same as {@link #normalizeKey(FieldKey)} except this one accepts a string version of the key.
	 * 
	 * @param key
	 * @return
	 */
	public static String normalizeKey(String key) {
		// escape any SEARCH_KEY_DELIMINATOR.
		key = key.replace(SEARCH_KEY_DELIMINATOR, SEARCH_KEY_DELIMINATOR + SEARCH_KEY_DELIMINATOR);
		key = key.replace(FIELD_KEY_DELIMINATOR, SEARCH_KEY_DELIMINATOR);
		return key;
	}

	public static String denormalizeKey(String searchKey) {
		String fieldKey;
		fieldKey = searchKey.replace(SEARCH_KEY_DELIMINATOR, FIELD_KEY_DELIMINATOR);
		// Unescape any SEARCH_KEY_DELIMINATOR
		fieldKey = fieldKey.replace(FIELD_KEY_DELIMINATOR + FIELD_KEY_DELIMINATOR, SEARCH_KEY_DELIMINATOR);
		return fieldKey;
	}

	/**
	 * Get the value out of the field.
	 * 
	 * @param field
	 * @return
	 */
	public static Object valueFrom(com.google.appengine.api.search.Field field) {

		switch (field.getType()) {
			case TEXT:
				return field.getText();
			case NUMBER:
				return field.getNumber();
			case ATOM:
				return field.getAtom();
			case DATE:
				return field.getDate();
			case GEO_POINT:
				GeoPoint point = field.getGeoPoint();
				return new GeoCoordinate.Builder().setLatitude(point.getLatitude()).setLongitude(point.getLongitude())
						.build().getAsString();
			case HTML:
				return field.getHTML();
			default:
				throw new IllegalStateException("Don't know how to get the value from the Field " + field);
		}

	}

	public static com.google.appengine.api.search.Field.Builder searchFieldFrom(@Nonnull Object value,
			@Nonnull FieldDataType dataType, DataTypeAdapterFactory adapterFactory) {

		Object adapted = value;
		if (adapterFactory.handles(value.getClass(), dataType)) {
			Adapter<?, FieldData> adapter = adapterFactory.getAdapter(value.getClass(), dataType);
			adapted = adapter.marshal(value).getValue();
		}

		com.google.appengine.api.search.Field.Builder builder = com.google.appengine.api.search.Field.newBuilder();

		if (adapted instanceof String) {
			String text = (String) adapted;
			if (dataType.equals(FieldDataType.ATOM)) {
				return builder.setAtom(text);
			} else if (dataType.equals(FieldDataType.TEXT)) {
				return builder.setText(text);
				// No HTML equivalent in DataType.
				// } else if (dataType.equals(FieldDataType.)) {
				// return builder.setHTML(text);
			} else if (dataType.equals(FieldDataType.GEO)) {
				Double[] doubles = NumberUtilsExtended.split(text);
				return builder.setGeoPoint(new GeoPoint(doubles[FieldDataType.STRING_LAT_INDEX],
						doubles[FieldDataType.STRING_LON_INDEX]));
			}
		} else if (adapted instanceof Number) {
			Number number = (Number) adapted;
			if (dataType.equals(FieldDataType.NUMBER)) {
				return builder.setNumber(number.doubleValue());
			}
		} else if (adapted instanceof Date) {
			if (dataType.equals(FieldDataType.DATE)) {
				return builder.setDate((Date) adapted);
			}
		}
		throw new IllegalStateException("Don't know how to get the value from the Field for field data type:"
				+ dataType.toString());
	}

	public static FieldType fieldTypeFrom(FieldDataType dataType) {
		FieldType fieldType = null;
		switch (dataType) {
			case TEXT:
				fieldType = FieldType.TEXT;
				break;
			case ATOM:
				fieldType = FieldType.ATOM;
				break;
			case NUMBER:
				fieldType = FieldType.NUMBER;
				break;
			case DATE:
				fieldType = FieldType.DATE;
				break;
			case GEO:
				fieldType = FieldType.GEO_POINT;
				break;
			default:
				break;
		}
		return fieldType;
	}

	/**
	 * Convert a Field into a form acceptable to GAE queries.
	 * 
	 * @param field
	 * @param dataTypeAdapterFactory
	 * @return
	 */
	public static String adaptToQueryString(FieldDefinition<?> definition, Object value,
			DataTypeAdapterFactory dataTypeAdapterFactory, FieldDataType /*
																		 * any NONE types already
																		 * mapped
																		 */mappedDataType) {
		String adapted;
		if (definition.getType().equals(value.getClass()) || definition.getParameterType().equals(value.getClass())) {
			adapted = adaptToQueryString(	definition.getType(),
											definition.getParameterType(),
											mappedDataType,
											value,
											dataTypeAdapterFactory);
		} else {
			adapted = adaptToQueryString(value.getClass(), null, mappedDataType, value, dataTypeAdapterFactory);
		}

		return adapted;
	}

	public static String adaptToQueryString(Class<?> type, Class<?> paramaterType, FieldDataType dataType,
			Object value, DataTypeAdapterFactory factory) {

		FieldData fieldData = FieldUtils.adaptToDataType(type, paramaterType, dataType, value, factory);
		return adaptToQueryString(fieldData);
	}

	/**
	 * Convert a FieldData into a form acceptable to GAE queries.
	 * 
	 * @param fieldData
	 * @return
	 */
	public static String adaptToQueryString(FieldData fieldData) {
		String queryString;
		switch (fieldData.getType()) {
			case GEO:
				queryString = "geopoint(" + fieldData.getValue() + ")";
				break;
			default:
				queryString = fieldData.getValue().toString();
				break;
		}

		return queryString;
	}

	public static String substringKey(FieldKey fieldKey) {
		return substringKey(SearchUtils.key(fieldKey));
	}

	public static String substringKey(String key) {
		return key + FIELD_KEY_SUBSTRING;
	}

	/**
	 * Returns the identifier class or null if not found.
	 * 
	 * @param dictionary
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Nullable
	public static Class<? extends Identifier<?, ?>> identifierClass(FieldDictionary dictionary) {
		if (ClassUtils.isAssignable(dictionary.getType(), BaseEntity.class)) {
			try {
				Class<?> type = dictionary.findDefinition(BaseEntity.BASE_FIELD.ID).getType();
				if (ClassUtils.isAssignable(type, Identifier.class)) {
					return (Class<? extends Identifier<?, ?>>) type;
				}
			} catch (FieldNotRegisteredException e) {
				return null;
			}

		}
		return null;
	}

	/**
	 * Indicates if the field is the {@link #SUBSTRING_FIELD_NAME_SUFFIX} requirement for doing
	 * partial word matches
	 * 
	 * @param key
	 * @return
	 */
	public static boolean isSubstringField(@Nullable String key) {
		return (key != null) ? key.endsWith(FIELD_KEY_SUBSTRING) : false;

	}

	/**
	 * Filters out any fieldkeys that are predicates.
	 * 
	 * TODO: use the schema to filter out the names upon the request.
	 * 
	 * @param fieldKeys
	 * @return
	 */
	public static Iterable<String> fieldValuesWithoutSubstrings(Iterable<String> fieldKeys) {
		return Iterables.filter(fieldKeys, substringFieldKeyRemovePredicate());
	}

	private static Iterable<com.google.appengine.api.search.Field> resultFieldsWithoutSubstrings(
			Iterable<com.google.appengine.api.search.Field> resultFields) {

		Predicate<com.google.appengine.api.search.Field> predicate = Predicates
				.compose(substringFieldKeyRemovePredicate(), resultFieldNameFunction());
		return Iterables.filter(resultFields, predicate);
	}

	private static Function<com.google.appengine.api.search.Field, String> resultFieldNameFunction() {
		return new Function<com.google.appengine.api.search.Field, String>() {

			@Override
			public String apply(com.google.appengine.api.search.Field input) {
				return (input != null) ? input.getName() : null;
			}
		};
	}

	public static Predicate<String> substringFieldKeyRemovePredicate() {
		return new Predicate<String>() {

			@Override
			public boolean apply(String input) {
				return !isSubstringField(input);
			}
		};
	}

	public static Iterable<Document> documents(Iterable<? extends SearchDocument> searchDocuments,
			DataTypeAdapterFactory adapterFactory) {
		return Iterables.transform(searchDocuments, documentFunction(adapterFactory));
	}

	public static Function<SearchDocument, Document> documentFunction(final DataTypeAdapterFactory adapterFactory) {
		return new Function<SearchDocument, Document>() {

			@Override
			public Document apply(SearchDocument searchDocument) {
				return document(searchDocument, adapterFactory);
			}
		};
	}

	public static Document document(SearchDocument searchDocument, final DataTypeAdapterFactory adapterFactory) {
		Document.Builder builder = Document.newBuilder();

		builder.setId(searchDocument.id());
		for (SearchDocument.Field field : searchDocument.fields()) {
			String normalizedKey = GaeSearchUtil.normalizeKey(field.key());
			com.google.appengine.api.search.Field gaeField = GaeSearchUtil
					.searchFieldFrom(field.value(), field.dataType(), adapterFactory).setName(normalizedKey).build();
			builder.addField(gaeField);
		}
		if (searchDocument.hasRank()) {
			builder.setRank(searchDocument.rank());
		}
		return builder.build();
	}

	/**
	 * Provides operator equivalents.
	 * 
	 * @param condition
	 * @return
	 */
	public static FilterOperator operator(FilterCondition<?> condition) {
		FilterOperator given = condition.getOperator();
		return operator(given);
	}

	public static FilterOperator operator(FilterOperator given) {
		FilterOperator result;
		switch (given) {
			case IN:
				result = FilterOperator.EQUALS;
				break;
			default:
				result = given;
				break;
		}
		return result;
	}

	public static Function<ScoredDocument, String> idFromScoredDocumentFunction() {
		return new Function<ScoredDocument, String>() {

			@Override
			public String apply(ScoredDocument input) {
				return (input != null) ? input.getId() : null;
			}
		};
	}

	/**
	 * @see #normalizeKey(FieldKey)
	 * @param fields
	 * @return
	 */
	public static String[] normalizeKeyArray(List<FieldKey> fields) {
		return Iterables.toArray(normalizeKeys(fields), String.class);
	}

	/**
	 * @see #normalizeKey(FieldKey)
	 * @param fields
	 * @return
	 */
	public static List<String> normalizeKeys(List<FieldKey> fields) {
		return Lists.transform(fields, normalizeKeyFunction());
	}

	/**
	 * @see #normalizeKey(FieldKey)
	 * @return
	 */
	public static Function<FieldKey, String> normalizeKeyFunction() {
		return new Function<FieldKey, String>() {

			@Override
			public String apply(FieldKey input) {
				return normalizeKey(input);
			}
		};
	}

	/**
	 * From the {@link Result} is a {@link com.google.appengine.api.search.Field} that needs to be
	 * translated to our {@link Field}.
	 * 
	 * @return
	 */
	public static Function<com.google.appengine.api.search.Field, Field> fieldResultFunction(
			final FieldDictionaryFactory fieldDictionaryFactory, final DataTypeAdapterFactory adapterFactory) {
		return new Function<com.google.appengine.api.search.Field, Field>() {

			@Override
			public Field apply(com.google.appengine.api.search.Field input) {
				return (input != null) ? field(input, fieldDictionaryFactory, adapterFactory) : null;
			}
		};
	}

	/**
	 * Helper method to retrieve the value from the result and legitimize it into system objects..if
	 * possible.
	 * 
	 * @param key
	 * @param resultField
	 * @return
	 */
	public static Field field(com.google.appengine.api.search.Field resultField,
			FieldDictionaryFactory fieldDictionaryFactory, DataTypeAdapterFactory adapterFactory) {
		Object value = GaeSearchUtil.valueFrom(resultField);
		String key = denormalizeKey(resultField.getName());
		if (fieldDictionaryFactory.hasDefinition(key)) {
			FieldDefinition<Object> definition = fieldDictionaryFactory.getDefinition(key);
			FieldDataType dataType = SearchUtils.dataTypeFor(definition);
			Class<?> type;

			// we don't want collections in this case. each field is only a single value , not a
			// collection
			// a collection must be built of fields with the same key
			if (definition.hasParameterType()) {
				type = definition.getParameterType();
			} else {
				type = definition.getType();
			}

			Adapter<Object, FieldData> adapter = adapterFactory.getAdapterOrNull(type, dataType);
			if (adapter != null) {
				try {
					value = adapter.unmarshal(FieldData.create().setType(dataType).setValue(value).build(), type);
				} catch (BaseException e) {
					LoggerFactory.getLogger(GaeSearchUtil.class).warning(resultField
							+ " had difficulties being unmarshaled " + e.getMessage());
					// just stick with the value as given
				}
			}
		}
		return SearchDocument.Field.create().key(key).value(value).build();
	}

	/**
	 * Transforms the given {@link ScoredDocument} into a stream of {@link SearchDocument} without
	 * forcing the search to block for results allowing parallel processing until the results are
	 * needed.
	 * 
	 * @param scoredDocuments
	 * @param queryProducer
	 * @param fieldDictionaryFactory
	 * @param adapterFactory
	 * @return
	 */
	public static Iterable<SearchDocument> searchDocuments(Iterable<ScoredDocument> scoredDocuments,
			final GaeSearchQueryProducer queryProducer, final FieldDictionaryFactory fieldDictionaryFactory,
			final DataTypeAdapterFactory adapterFactory) {

		return Iterables.transform(	scoredDocuments,
									searchDocumentFunction(queryProducer, fieldDictionaryFactory, adapterFactory));
	}

	public static Function<ScoredDocument, SearchDocument> searchDocumentFunction(
			final GaeSearchQueryProducer queryProducer, final FieldDictionaryFactory fieldDictionaryFactory,
			final DataTypeAdapterFactory adapterFactory) {
		return new Function<ScoredDocument, SearchDocument>() {

			@Override
			public SearchDocument apply(ScoredDocument input) {
				return searchDocument(input, queryProducer, fieldDictionaryFactory, adapterFactory);
			}
		};
	}

	/**
	 * Transforms the ScoredDocument result into our {@link SearchDocument} without forcing
	 * commitment right now allowing search to continue in parallel.
	 * 
	 * @param scoredDocument
	 * @param queryProducer
	 * @param fieldDictionaryFactory
	 * @param adapterFactory
	 * @return
	 */
	public static SearchDocument searchDocument(ScoredDocument scoredDocument, GaeSearchQueryProducer queryProducer,
			FieldDictionaryFactory fieldDictionaryFactory, DataTypeAdapterFactory adapterFactory) {
		SearchDocument.Builder docBuilder = new SearchDocument.Builder();
		docBuilder.index(queryProducer.indexName());
		docBuilder.id(scoredDocument.getId());
		docBuilder.rank(scoredDocument.getRank());

		Iterable<com.google.appengine.api.search.Field> resultFields = scoredDocument.getFields();
		// if all fields are requested then they won't want the substrings
		if (queryProducer.isAllFieldsRequested()) {
			// it's not worth checking if fields were requested since substrings wont' come back
			resultFields = resultFieldsWithoutSubstrings(resultFields);
		}
		Iterable<Field> fields = Iterables.transform(	resultFields,
														fieldResultFunction(fieldDictionaryFactory, adapterFactory));
		// assignment does not block
		docBuilder.fields(fields);
		return docBuilder.build();
	}

	/**
	 * Modifies the {@link GaeSearchQueryProducer#filter()} setting the
	 * {@link Results#getNumberReturned()} and {@link Results#getCursor()}. This is done in a
	 * {@link Supplier} to delay calling the results until necessary so this doesn't block just to
	 * get the size.
	 * 
	 * @param queryProducer
	 * @param supplier
	 * @return
	 */
	public static Supplier<Filter> filterSupplier(final GaeSearchQueryProducer queryProducer,
			final Supplier<Results<ScoredDocument>> supplier) {
		return new Supplier<Filter>() {

			@Override
			public Filter get() {
				final Filter original = queryProducer.filter();
				Builder clone = Filter.clone(original);

				Results<ScoredDocument> results = supplier.get();
				Cursor cursor = results.getCursor();
				if (cursor != null) {
					clone.setPageCursor(cursor.toWebSafeString());
				}
				clone.setPageTotal(results.getNumberReturned());
				return clone.build();
			}
		};

	}

	/**
	 * @deprecated use {@link #suggestWords(String, FieldKey, boolean)}
	 * 
	 * @param textField
	 * @param key
	 * @return
	 */
	@Deprecated
	public static Set<Field> substringFields(String textField, FieldKey key) {
		Field field = suggestWords(textField, key);
		return ImmutableSet.of(field);
	}

	/**
	 * Provides a single {@link com.google.appengine.api.search.Field} of type
	 * {@link FieldDataType#TEXT} that includes a series of partial words allowing the GAE search
	 * service to find a document by partial words (especially useful for suggest lists).
	 * 
	 * TN-848 previously this provided all embedded words, but start words only provide better
	 * results TN-848 TN-850 Many fields cause slow performance. Previously this returned one field
	 * for each token as ATOM. Using Text field allows the search service to provide the tokens.
	 * 
	 * @param textField
	 * @param key
	 * @return
	 */
	private static Field suggestWords(String textField, FieldKey key, Boolean wordStartsOnly) {
		String substringKey = GaeSearchUtil.substringKey(key);
		String suggestWords = TextSubstringGenerator.create().input(textField).wordStartsOnly(wordStartsOnly).build()
				.asString();
		Field field = Field.create().dataType(FieldDataType.TEXT).key(substringKey).value(suggestWords).build();
		return field;
	}

	/**
	 * Provides substrings of the words to be used in a search service put into a single field with
	 * a standardized name to be indexed and searched as a text field. Just the start of the words
	 * are provided.
	 * 
	 * @see #suggestWordsVerbose(String, FieldKey)
	 * 
	 * @param textField
	 * @param key
	 * @return
	 */
	public static Field suggestWords(String textField, FieldKey key) {
		return suggestWords(textField, key, true);
	}

	/**
	 * same as {@link #suggestWords(String, FieldKey)}, but includes inner tokens, not just the
	 * start of the word.
	 * 
	 * @see #suggestWords(String, FieldKey)
	 * 
	 * @param textField
	 * @param key
	 * @return
	 */
	public static Field suggestWordsVerbose(String textField, FieldKey key) {
		return suggestWords(textField, key, false);
	}
}
