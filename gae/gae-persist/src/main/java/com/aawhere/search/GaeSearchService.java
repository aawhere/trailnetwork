/**
 *
 */
package com.aawhere.search;

import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Future;

import javax.annotation.Nonnull;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.Filter;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.aawhere.util.concurrent.FuturesExtended;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.PutException;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SearchServiceFactory;
import com.google.appengine.api.search.StatusCode;
import com.google.appengine.api.search.checkers.SearchApiLimits;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import com.google.inject.Inject;

/**
 * @see SearchService
 * 
 * @author Brian Chapman
 * 
 */
public class GaeSearchService
		implements SearchService {

	static final Integer LIMIT_MAX = 1000;
	static final Integer DELETE_MAX = 200;
	private final Integer MAX_INDEX_ATTEMPTS = 10;
	private FieldAnnotationDictionaryFactory dictionaryfactory;
	FieldDictionaryFactory fieldDictionaryFactory;
	private DataTypeAdapterFactory adapterFactory;

	private Boolean asynchronous = false;

	@Inject
	public GaeSearchService(FieldAnnotationDictionaryFactory dictionaryFactory, DataTypeAdapterFactory adapterFactory,
			ObjectifyRepository.Synchronization synchronization) {
		this.dictionaryfactory = dictionaryFactory;
		this.adapterFactory = adapterFactory;
		this.fieldDictionaryFactory = this.dictionaryfactory.getFactory();
		this.asynchronous = ObjectifyRepository.Synchronization.ASYNCHRONOUS.equals(synchronization);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.search.SearchService#indexEntities(java.lang.Iterable)
	 */
	@Override
	public void index(Iterable<? extends SearchDocument> searchDocuments) throws MaxIndexAttemptsExceeded {
		index(searchDocuments, false);
	}

	public void indexAsync(Iterable<? extends SearchDocument> searchDocuments) {
		try {
			index(searchDocuments, true);
		} catch (MaxIndexAttemptsExceeded e) {
			// this shouldn't happen since it is async
			throw new RuntimeException(e);
		}
	}

	public void index(Iterable<? extends SearchDocument> searchDocuments, Boolean async)
			throws MaxIndexAttemptsExceeded {

		if (searchDocuments != null) {
			// searpate the search documents into their corresponding index
			ImmutableListMultimap<String, ? extends SearchDocument> byIndex = Multimaps
					.index(searchDocuments, SearchUtils.indexNameFunction());

			for (String indexName : byIndex.keySet()) {
				ImmutableList<? extends SearchDocument> searchDocumentsForIndex = byIndex.get(indexName);
				Index index = getIndex(indexName);

				// they have a max size so automatically handle
				Iterable<?> partitions = Iterables.partition(	searchDocumentsForIndex,
																SearchApiLimits.PUT_MAXIMUM_DOCS_PER_REQUEST);

				for (Object partition : partitions) {
					// partition is having a difficult time with ? extends searchdocument so
					// unchecked cast
					@SuppressWarnings("unchecked")
					List<? extends SearchDocument> searchDocumentsPartition = (List<? extends SearchDocument>) partition;

					Iterable<Document> documents = GaeSearchUtil.documents(searchDocumentsPartition, adapterFactory);
					if (async) {
						index.putAsync(documents);
					} else {
						int attempts = 0;
						while (attempts < MAX_INDEX_ATTEMPTS) {
							try {
								// not going async
								index.put(documents);
								break;
							} catch (PutException e) {
								if (StatusCode.TRANSIENT_ERROR.equals(e.getOperationResult().getCode())) {
									attempts++;
									LoggerFactory.getLogger(getClass()).warning(attempts + " failures for " + index
											+ " because " + e.getMessage());
								}
							}
						}
						if (attempts >= MAX_INDEX_ATTEMPTS) {
							throw new MaxIndexAttemptsExceeded(Integer.valueOf(attempts));
						}
					}
				}

			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.search.SearchService#indexEntity(com.aawhere.persist. LongMockEntity)
	 */
	@Override
	public void index(SearchDocument... searchDocuments) throws MaxIndexAttemptsExceeded {
		index(ImmutableList.<SearchDocument> builder().add(searchDocuments).build());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.search.SearchService#search(com.aawhere.search.Query)
	 */
	@Override
	@Deprecated
	public SearchDocuments query(Filter filter, String index) throws FieldNotSearchableException, UnknownIndexException {
		return searchDocuments(Filter.clone(filter).addSearchIndex(index).build());
	}

	/**
	 * Provides all of the ids matching the query using efficient methods (id only, cursor
	 * positioning, Guava iterables). This ignores limit offset and it will return all of the ids
	 * using iterables that demand as needed.
	 * 
	 * @deprecated use {@link #searchDocumentIds(Filter)}
	 * @see SearchService#searchDocumentIds(Filter, String)
	 * @param filter
	 * @param indexName
	 * @return the ids and the cloned filter with the new cursor available (instead of offset)
	 * @throws FieldNotSearchableException
	 * @throws UnknownIndexException
	 */
	@Deprecated
	@Override
	public Iterable<String> searchDocumentIds(Filter filter, String indexName) throws UnknownIndexException {

		if (indexName != null) {
			filter = Filter.clone(filter).addSearchIndex(indexName).build();
		}
		return searchDocumentIds(filter);
	}

	/**
	 * Does all the work of searching for the results in the index identified. The filter is
	 * provided for additional option triggering and inclusion in the search documents result.
	 * 
	 * @param query
	 * @param indexName
	 * @param filter
	 * @param fieldKeys
	 * @return
	 * @throws FieldNotSearchableException
	 */
	@Override
	public SearchDocuments searchDocuments(@Nonnull Filter filter) throws FieldNotSearchableException,
			UnknownIndexException {
		GaeSearchQueryProducer queryProducer = GaeSearchQueryProducer.create().setFilter(filter)
				.setAdapterFactory(adapterFactory).setFieldDictionaryFactory(dictionaryfactory).buildQuery();

		Query query = queryProducer.query();
		Index index = getIndex(queryProducer.indexName());

		// future and it's supplier wrapper delays retrieval of results until needed by client
		final Future<Results<ScoredDocument>> futureResults = index.searchAsync(query);

		Supplier<Results<ScoredDocument>> supplier = FuturesExtended.supplier(futureResults);

		return GaeSearchResults.create().adapterFactory(adapterFactory).fieldDictionaryFactory(fieldDictionaryFactory)
				.queryProducer(queryProducer).results(supplier).build().searchDocuments();
	}

	/*
	 * TODO: could/should the indexes be cached?
	 */
	private Index getIndex(String name) {
		IndexSpec indexSpec = IndexSpec.newBuilder().setName(name).build();
		return SearchServiceFactory.getSearchService().getIndex(indexSpec);
	}

	@Override
	public SearchDocuments query(Filter filter, List<String> indexes) throws FieldNotSearchableException,
			UnknownIndexException {
		return searchDocuments(Filter.clone(filter).addSearchIndex(Iterables.toArray(indexes, String.class)).build());
	}

	@Override
	public void delete(String documentId, String indexName) {
		Index index = getIndex(indexName);
		index.delete(documentId);
	}

	@Override
	public void delete(Iterable<String> ids, String indexName) {
		Index index = getIndex(indexName);
		// TN-879 - limited to 200
		Iterable<List<String>> partitions = Iterables.partition(ids, DELETE_MAX);
		for (List<String> partition : partitions) {
			if (asynchronous) {
				index.deleteAsync(partition);
			} else {
				index.delete(partition);
			}
		}
	}

	@Override
	public void delete(Filter filter) throws FieldNotSearchableException, UnknownIndexException {

		if (filter.hasSearchIndexes()) {
			List<String> searchIndexes = filter.getSearchIndexes();
			for (String searchIndex : searchIndexes) {
				Filter singleIndexFilter = Filter.clone(filter).clearSearchIndexes().addSearchIndex(searchIndex)
						.build();
				// the search document ids has no limit
				Iterable<String> searchDocumentIds = searchDocumentIds(singleIndexFilter);
				delete(searchDocumentIds, searchIndex);
			}
		} else {
			InvalidArgumentException.required(Filter.FIELD.SEARCH_INDEXES);
		}
	}

	@Override
	public Iterable<String> searchDocumentIds(Filter filter) throws UnknownIndexException {
		Iterable<String> ids;
		Results<ScoredDocument> results;
		ids = Lists.newArrayList();

		String indexName = indexName(filter);
		// this is the original query, subsequent will use this query and paginate using the
		// cursor
		Filter unlimitedFilter = Filter.clone(filter).setLimit(LIMIT_MAX).build();
		Query query = GaeSearchQueryProducer.create().setFilter(unlimitedFilter).idsOnly()
				.setFieldDictionaryFactory(dictionaryfactory).setAdapterFactory(adapterFactory).idsOnly().buildQuery()
				.query();
		// per the docs, must give a default cursor or one won't come back
		Cursor cursor = Cursor.newBuilder().build();
		do {
			QueryOptions options = QueryOptions.newBuilder(query.getOptions()).setCursor(cursor).build();
			query = Query.newBuilder(query).setOptions(options).build();
			Index index = getIndex(indexName);
			// TODO: going async here will be necessary for full scale.
			// the iterator can use Future to force and wait if the client is ahead of us
			results = index.search(query);
			// if there is no next page the cursor is null
			cursor = results.getCursor();
			Iterable<String> thisPageIds = Iterables.transform(results, GaeSearchUtil.idFromScoredDocumentFunction());
			ids = Iterables.concat(ids, thisPageIds);
		} while (cursor != null);

		return ids;
	}

	/**
	 * Provides the only index name from the filter while validating that what is given is a single
	 * index.
	 * 
	 * @param filter
	 * @return
	 */
	private String indexName(Filter filter) {
		if (filter.hasSearchIndexes()) {
			HashSet<String> searchIndexes = Sets.newHashSet(filter.getSearchIndexes());
			InvalidArgumentException.range(Range.singleton(1), searchIndexes.size());
			return searchIndexes.iterator().next();
		} else {
			throw InvalidArgumentException.required("filter.searchIndex");
		}
	}

	@Override
	public void delete(SearchDocument document) {
		delete(document.id(), document.index());
	}

	@Override
	public void delete(Iterable<SearchDocument> documents) {
		ImmutableListMultimap<String, SearchDocument> documentsPerIndex = Multimaps.index(documents, SearchUtils
				.indexNameFunction());
		ImmutableSet<String> indexes = documentsPerIndex.keySet();
		for (String indexName : indexes) {
			delete(SearchUtils.ids(documentsPerIndex.get(indexName)), indexName);
		}

	}
}
