/**
 *
 */
package com.aawhere.search;

import com.google.apphosting.api.DatastorePb.Query.Filter.Operator;

/**
 * Gae operators for building search queries
 * 
 * @author Brian Chapman
 * 
 */
enum GaeSearchOperator {
	/** Logical AND two query statements. */
	AND,
	/**
	 * Logical OR ... used in a similar way as the {@link Operator#IN}.
	 */
	OR;

	/** The operator ready to use in a sentence (with separators). */
	public final String operator;

	GaeSearchOperator() {
		this.operator = GaeSearchQueryProducer.SEPERATOR + name() + GaeSearchQueryProducer.SEPERATOR;
	}

	public String getOperator() {
		return operator;
	}
}
