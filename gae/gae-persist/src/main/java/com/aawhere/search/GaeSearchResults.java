package com.aawhere.search;

import com.aawhere.collections.IterablesExtended;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.Filter;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

/**
 * Receiving the {@link Results} this will translate them into {@link SearchDocuments}.
 * 
 * @author aroller
 * 
 */
public class GaeSearchResults {

	private SearchDocuments searchDocuments;

	/**
	 * Used to construct all instances of SearchGaeResults.
	 */
	public static class Builder
			extends ObjectBuilder<GaeSearchResults> {

		private Supplier<Results<ScoredDocument>> results;
		private GaeSearchQueryProducer queryProducer;
		private FieldDictionaryFactory fieldDictionaryFactory;
		private DataTypeAdapterFactory adapterFactory;

		/** @see #adapterFactory */
		public Builder adapterFactory(DataTypeAdapterFactory adapterFactory) {
			this.adapterFactory = adapterFactory;
			return this;
		}

		/** @see #fieldDictionaryFactory */
		public Builder fieldDictionaryFactory(FieldDictionaryFactory fieldDictionaryFactory) {
			this.fieldDictionaryFactory = fieldDictionaryFactory;
			return this;
		}

		public Builder() {
			super(new GaeSearchResults());
		}

		/** @see #results */
		public Builder results(Results<ScoredDocument> results) {
			this.results = Suppliers.ofInstance(results);
			return this;
		}

		/** @see #queryProducer */
		public Builder queryProducer(GaeSearchQueryProducer queryProducer) {
			this.queryProducer = queryProducer;
			return this;
		}

		@Override
		public GaeSearchResults build() {
			GaeSearchResults built = super.build();

			Supplier<Filter> filterSupplier = GaeSearchUtil.filterSupplier(this.queryProducer, this.results);
			// this will delay the iterating of the results allowing the search to finish
			// a call for the iterator will block when it calls get
			Iterable<ScoredDocument> delayedIterable = IterablesExtended.supplier(this.results);
			built.searchDocuments = SearchDocuments
					.create()
					.filter(filterSupplier)
					.searchDocuments(GaeSearchUtil.searchDocuments(	delayedIterable,
																	queryProducer,
																	fieldDictionaryFactory,
																	adapterFactory))

					.build();
			return built;
		}

		public Builder results(Supplier<Results<ScoredDocument>> supplier) {
			this.results = supplier;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct SearchGaeResults */
	private GaeSearchResults() {
	}

	public SearchDocuments searchDocuments() {
		return this.searchDocuments;
	}

}
