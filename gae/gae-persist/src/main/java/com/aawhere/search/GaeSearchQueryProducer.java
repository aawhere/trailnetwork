/**
 *
 */
package com.aawhere.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.collections4.list.SetUniqueList;
import org.apache.commons.lang.ArrayUtils;

import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.GeoCoordinateDataTypeGeoAdapter;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.common.base.Functions;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Creates a {@link Query} from a {@link Filter}
 * 
 * @author Brian Chapman
 * 
 */
public class GaeSearchQueryProducer {

	private static final SetUniqueList<FieldKey> ALL_FIELDS = null;
	final static String SEPERATOR = " ";
	private Query query;
	private String indexName;
	private List<FieldKey> fields;
	private Filter filter;

	/**
	 * Used to construct all instances of SearchQueryProducer.
	 */
	public static class Builder
			extends ObjectBuilder<GaeSearchQueryProducer> {

		private DataTypeAdapterFactory dataTypeAdapterFactory;
		private FieldAnnotationDictionaryFactory fieldAnnotationDictionaryFactory;
		private boolean idsOnly;
		private String indexName;
		// TODO:automatically add stemming to query strings
		// https://cloud.google.com/appengine/docs/java/search/query_strings#Java_Stemming
		private static final char STEMMING = '~';

		public Builder() {
			super(new GaeSearchQueryProducer());
		}

		/**
		 * @param filter
		 * @return
		 */
		public Builder setFilter(Filter filter) {
			building.filter = filter;
			return this;
		}

		public Builder setAdapterFactory(DataTypeAdapterFactory adapterFactory) {
			this.dataTypeAdapterFactory = adapterFactory;
			return this;
		}

		public Builder setFieldDictionaryFactory(FieldAnnotationDictionaryFactory definitionFactory) {
			this.fieldAnnotationDictionaryFactory = definitionFactory;
			return this;
		}

		public Builder idsOnly() {
			this.idsOnly = true;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(dataTypeAdapterFactory);
			Assertion.exceptions().notNull("fieldDictionaryFactory", this.fieldAnnotationDictionaryFactory);
			Assertion.exceptions().notNull("filter", building.filter);
			Assertion.assertNotEmpty(Filter.FIELD.SEARCH_INDEXES, building.filter.getSearchIndexes());

		}

		private String indexName(Filter filter) {
			try {
				return Iterables.getOnlyElement(ImmutableSet.<String> builder().addAll(filter.getSearchIndexes())
						.build());
			} catch (IllegalArgumentException e) {
				throw new MultipleIndexesNotSupportedException(filter.getSearchIndexes());
			}
		}

		/**
		 * Provides the field keys requested by the caller to be shown.
		 * 
		 * @param filter
		 * @param index
		 * @return
		 */
		private List<FieldKey> fieldKeys(Filter filter) {
			// ensures uniqueness while maintaining order
			SetUniqueList<FieldKey> fieldKeys = ALL_FIELDS;
			if (filter.hasFields()) {
				fieldKeys = SetUniqueList.setUniqueList(Lists.newArrayList(filter.getFieldKeys()));
			} else {
				Class<?> indexType;
				String domain = SearchUtils.domainFromIndex(indexName(filter));
				indexType = this.fieldAnnotationDictionaryFactory.getFactory().getDictionary(domain).getType();
				Searchable searchable = indexType.getAnnotation(Searchable.class);
				if (searchable != null) {
					String[] searchableFieldKeys = searchable.fieldKeys();
					if (ArrayUtils.isNotEmpty(searchableFieldKeys)) {
						Set<String> fieldRelativeKeys = Sets.newHashSet(searchableFieldKeys);
						fieldKeys = SetUniqueList.setUniqueList(Lists.newArrayList(FieldUtils
								.fieldKeys(domain, fieldRelativeKeys)));
					}
				}
			}
			return fieldKeys;
		}

		public GaeSearchQueryProducer buildQuery() throws FieldNotSearchableException {
			GaeSearchQueryProducer built = super.build();
			com.google.appengine.api.search.QueryOptions.Builder queryOptionsBuilder = QueryOptions.newBuilder();
			String queryString = createQueryString(built.filter);
			queryString = geoQueryAppended(built.filter, queryString);
			queryOptionsBuilder.setLimit(built.filter.getLimit());
			if (built.filter.hasCursor()) {
				queryOptionsBuilder.setCursor(Cursor.newBuilder().build(queryString));
			} else {
				queryOptionsBuilder.setOffset(getOffset(built.filter));
			}
			// ids only does get fields back
			if (!this.idsOnly) {
				built.fields = fieldKeys(built.filter);
				requestFields(built.fields, queryOptionsBuilder);
			}
			QueryOptions options = queryOptionsBuilder.build();
			built.query = Query.newBuilder().setOptions(options).build(queryString);
			built.indexName = indexName(built.filter);
			return built;
		}

		/**
		 * the fields given will be requested from the search service.
		 * 
		 * @param fields
		 * @param queryOptionsBuilder
		 */
		private void requestFields(List<FieldKey> fields, QueryOptions.Builder queryOptionsBuilder) {

			// TODO:This could be improved if we had the schema by selecting fields declared that
			// don't match our "substring" necessity
			// TODO: validate fields requested here against the schema
			// by default they return all fields
			if (!isAllFields(fields)) {
				queryOptionsBuilder.setFieldsToReturn(GaeSearchUtil.normalizeKeyArray(fields));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public GaeSearchQueryProducer build() {
			return buildQuery();
		}

		@Nonnull
		private String createQueryString(Filter filter) throws FieldNotSearchableException {
			String queryString = createQueryString(filter.getConditions());
			if (filter.getQuery() != null) {
				if (!queryString.isEmpty()) {
					queryString = queryString + GaeSearchOperator.AND.getOperator();
				}

				queryString = queryString + filter.getQuery();
			}
			return queryString;
		}

		private String createQueryString(@SuppressWarnings("rawtypes") List<FilterCondition> conditions)
				throws FieldNotSearchableException {
			List<String> queries = new ArrayList<String>();
			for (FilterCondition<?> condition : conditions) {
				queries.add(createQuery(condition));
			}
			Joiner joiner = Joiner.on(GaeSearchOperator.AND.getOperator()).skipNulls();
			return joiner.join(queries);
		}

		private String createQuery(FilterCondition<?> condition) throws FieldNotSearchableException {

			String queryValue;
			Object value = condition.getValue();

			// place collections into OR statements.
			if (value instanceof Iterable<?>) {
				Iterable<String> values = Iterables.transform(((Iterable<?>) value), Functions.toStringFunction());
				queryValue = Joiner.on(GaeSearchOperator.OR.operator).join(values);
			} else {
				queryValue = String.valueOf(value);
			}

			return new StringBuilder().append(GaeSearchUtil.normalizeKey(condition.getField())).append(SEPERATOR)
					.append(GaeSearchUtil.operator(condition).operator).append(SEPERATOR).append(queryValue).toString();

		}

		/**
		 * Creates a query string for doing a proximity query against a {@link GeoPoint}. It is
		 * expected that the queryValue is a {@link BoundingBox}, although in the future this may be
		 * expanded to include proximity searches. This formula approximates the actual bounding box
		 * with a circle that fully contains the bounding box. Expected format:
		 * 
		 * "distance(search_field, geopoint(centerOfBox)) < boxRadius"
		 * 
		 * Where centerOfBox is a point representing the center of the box (lat,lon) and boxRadius
		 * is half the the boundingBox's diagonal distance.
		 * 
		 * @param queryString
		 * 
		 * @param definition
		 * @param queryValue
		 * @return
		 */
		@Nullable
		String geoQueryAppended(Filter filter, String queryString) {

			if (filter.hasBounds()) {
				BoundingBox box = GeoCellUtil.boundingBox(filter.getBounds());
				GeoCoordinate centerOfBox = box.getCenter();

				String centerOfBoxString = GaeSearchUtil.adaptToQueryString(new GeoCoordinateDataTypeGeoAdapter()
						.marshal(centerOfBox));

				// WW-294 reduced this from half diagonal to half the smaller of the width or height
				Length radius = QuantityMath.create(box.getWidth()).min(box.getHeight()).dividedBy(2).getQuantity();
				String radiusString = GaeSearchUtil.adaptToQueryString(	radius.getClass(),
																		null,
																		FieldDataType.NUMBER,
																		radius,
																		dataTypeAdapterFactory);

				StringBuilder sb = new StringBuilder();
				sb.append(queryString);
				// the space separates the query string
				sb.append(SEPERATOR);
				sb.append("distance(");
				sb.append(FIELD.GAE.LOCATION);
				sb.append(", ");
				sb.append(centerOfBoxString);
				sb.append(") < ");
				sb.append(radiusString);
				queryString = sb.toString();
			}
			return queryString;

		}

		private int getOffset(Filter filter) {
			// BaseFilter offsets start at 1, GAE search offsets start at 0;
			return filter.getStart() - 1;
		}

	}// end Builder

	/** Use {@link Builder} to construct SearchQueryProducer */
	private GaeSearchQueryProducer() {
	}

	public static Builder create() {
		return new Builder();
	}

	public Query query() {
		return query;
	}

	public List<FieldKey> fields() {
		return fields;
	}

	public String indexName() {
		return indexName;
	}

	public Filter filter() {
		return filter;
	}

	public boolean isAllFieldsRequested() {
		return isAllFields(fields);
	}

	static final boolean isAllFields(List<FieldKey> fields) {
		// all fields is currently null so use operator
		return fields == ALL_FIELDS;
	}

	public static final class FIELD {
		public static final String DOMAIN = "search";
		public static final String LOCATION = "location";

		public static final class KEY {
			public static final FieldKey LOCATION = new FieldKey(DOMAIN, FIELD.LOCATION);
		}

		public static final class GAE {
			public static final String LOCATION = GaeSearchUtil.normalizeKey(KEY.LOCATION);
		}
	}
}
