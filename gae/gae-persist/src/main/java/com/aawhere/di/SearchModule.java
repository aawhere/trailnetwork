/**
 *
 */
package com.aawhere.di;

import com.google.inject.AbstractModule;

/**
 * Guice Module for com.aawhere.search
 * 
 * @author Brian Chapman
 * 
 */
public class SearchModule
		extends AbstractModule {

	@Override
	protected void configure() {

	}

}
