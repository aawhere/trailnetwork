/**
 *
 */
package com.aawhere.util.rb;

import com.googlecode.objectify.ObjectifyFactory;

/**
 * <p>
 * A convenient static method that adds all the track related translators to your factory's
 * conversions.
 * </p>
 * 
 * <p>
 * {@code JodaTimeConverters.add(ObjectifyService.factory());}
 * 
 * @author Brian Chapman
 */
public class RbTranslators {
	public static void add(ObjectifyFactory fact) {
		fact.getTranslators().add(new MessageTranslatorFactory());
	}
}