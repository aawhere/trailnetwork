/**
 *
 */
package com.aawhere.util.rb;

import java.lang.reflect.Type;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Handles any {@link Message} going to the datastore and back.
 * 
 * It will store the {@link Enum#name()} like standard enums in the system if the field is declared
 * as the enum. If the field is declared as a {@link Message} then this will store a key from
 * {@link MessageFactory} if the Message is enumeratored or the value of the message if it's not.
 * 
 * @author roller
 * 
 */
public class MessageTranslatorFactory
		extends ValueTranslatorFactory<Message, String> {

	public MessageTranslatorFactory() {
		super(Message.class);
	}

	@Override
	protected ValueTranslator<Message, String> createValueTranslator(final TypeKey<Message> tk, CreateContext ctx,
			Path path) {
		Type type = tk.getType();
		final boolean typeIsEnum = (type instanceof Class) && ((Class<?>) type).isEnum();

		return new ValueTranslator<Message, String>(String.class) {

			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			protected Message loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				Message result;
				if (typeIsEnum) {
					result = MessageFactory.valueOf((Class<? extends Enum>) tk.getType(), value);
				} else {
					final MessageFactory messageFactory = MessageFactory.getInstance();
					result = messageFactory.bestMessage(value, value);
				}
				return result;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected String saveValue(Message value, boolean index, SaveContext ctx, Path path) throws SkipException {
				String result;
				// if the type exposes enum then store it as an enum
				if (typeIsEnum) {
					result = ((Enum<? extends Message>) value).name();
				} else if (value.getClass().isEnum()) {
					// type is not exposed so the load won't know just the Enum#name()...use the
					// factory
					result = MessageFactory.getInstance().keyForEnum((Enum<? extends Message>) value);
				} else {
					result = value.getValue();
				}
				return result;
			}
		};
	}
}