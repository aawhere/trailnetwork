package com.aawhere.measure.geocell;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Convert from {@link GeoCell} to its String value. This translator prevents the default
 * translators from creating a "Map" Node, which causes objectify's translation system to explode
 * when making a list of GeoCells filterable.
 * 
 * @author Brian Chapman
 * 
 */
public class GeoCellTranslatorFactory
		extends ValueTranslatorFactory<GeoCell, String> {

	public GeoCellTranslatorFactory() {
		super(GeoCell.class);
	}

	@Override
	protected ValueTranslator<GeoCell, String> createValueTranslator(TypeKey<GeoCell> tk, CreateContext ctx, Path path) {
		return new ValueTranslator<GeoCell, String>(String.class) {

			@Override
			protected String saveValue(GeoCell value, boolean index, SaveContext ctx, Path path) throws SkipException {
				return value.getCellAsString();
			}

			@Override
			protected GeoCell loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				return new GeoCell(value);
			}
		};
	}
}