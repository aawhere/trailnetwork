package com.aawhere.measure.geocell;

import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;

import com.aawhere.measure.MeasurementUtil;
import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Convert from {@link Length} to its value (double) and its base unit to store in datastore.
 * 
 * @author Brian Chapman
 * 
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class QuantityTranslatorFactory
		extends ValueTranslatorFactory<Quantity, Double> {

	public QuantityTranslatorFactory() {
		super(Quantity.class);
	}

	@Override
	protected ValueTranslator<Quantity, Double> createValueTranslator(final TypeKey<Quantity> tk, CreateContext ctx,
			Path path) {
		return new ValueTranslator<Quantity, Double>(Double.class) {

			@Override
			protected Double saveValue(Quantity value, boolean index, SaveContext ctx, Path path) throws SkipException {
				Unit<?> normalizedUnit = value.getUnit().toMetric();
				return value.doubleValue(normalizedUnit);
			}

			@Override
			protected Quantity loadValue(Double value, LoadContext ctx, Path path) throws SkipException {
				return MeasurementUtil.normalizedQuantityOrNull(value, (Class<Quantity>) tk.getType());
			}
		};
	}

}