/**
 * 
 */
package com.aawhere.measure;

import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Converts the given bounding box into a string for simple storage. This treats
 * {@link GeoCellBoundingBox} as a regular bounding box and will not reproduce a
 * {@link GeoCellBoundingBox} on from loadValue if one is given during saveValue.
 * 
 * @author aroller
 * 
 */
public class BoundingBoxTranslatorFactory
		extends ValueTranslatorFactory<BoundingBox, String> {

	/**
	 * @param pojoType
	 */
	public BoundingBoxTranslatorFactory() {
		super(BoundingBox.class);

	}

	@Override
	protected ValueTranslator<BoundingBox, String> createValueTranslator(TypeKey<BoundingBox> tk, CreateContext ctx,
			Path path) {
		return new ValueTranslator<BoundingBox, String>(String.class) {

			@Override
			protected String saveValue(BoundingBox value, boolean index, SaveContext ctx, Path path)
					throws SkipException {
				return (value == null) ? null : value.getBounds();
			}

			@Override
			protected BoundingBox loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				return (value == null) ? null : BoundingBox.valueOf(value);
			}
		};
	}

}
