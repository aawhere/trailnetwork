/**
 *
 */
package com.aawhere.measure.geocell;

import com.aawhere.measure.BoundingBoxTranslatorFactory;

import com.googlecode.objectify.ObjectifyFactory;

/**
 * <p>
 * A convenient static method that adds all the joda-time related converters to your factory's
 * conversions. We can't enable the joda-time converters automatically or it would force everyone to
 * add joda-time.jar whether they use it or not. To enable, call this:
 * </p>
 * 
 * <p>
 * {@code JodaTimeConverters.add(ObjectifyService.factory());}
 * 
 * @author Brian Chapman
 */
public class MeasureTranslators {
	public static void add(ObjectifyFactory fact) {
		fact.getTranslators().add(new QuantityTranslatorFactory());
		fact.getTranslators().add(new GeoCellTranslatorFactory());
		fact.getTranslators().add(new BoundingBoxTranslatorFactory());
	}
}