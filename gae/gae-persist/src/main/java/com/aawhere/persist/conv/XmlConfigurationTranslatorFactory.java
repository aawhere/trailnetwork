package com.aawhere.persist.conv;

import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Convert from {@link Class} to a String.
 * 
 * @author Brian Chapman
 * 
 */
public class XmlConfigurationTranslatorFactory
		extends ValueTranslatorFactory<XMLConfiguration, String> {

	public XmlConfigurationTranslatorFactory() {
		super(XMLConfiguration.class);
	}

	@Override
	protected ValueTranslator<XMLConfiguration, String> createValueTranslator(TypeKey<XMLConfiguration> tk,
			CreateContext ctx, Path path) {
		return new ValueTranslator<XMLConfiguration, String>(String.class) {

			@Override
			protected String saveValue(XMLConfiguration value, boolean index, SaveContext ctx, Path path)
					throws SkipException {
				Writer writer = new StringWriter();
				try {
					value.save(writer);
				} catch (ConfigurationException e) {
					throw new RuntimeException("An unexpected error occured writing preferences to XML", e);
				}
				return writer.toString();
			}

			@Override
			protected XMLConfiguration loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				XMLConfiguration prefs = new XMLConfiguration();
				Reader reader = new StringReader(value);
				try {
					prefs.load(reader);
				} catch (ConfigurationException e) {
					throw new RuntimeException("Could not create XmlConfiguration with xml: " + value, e);
				}
				return prefs;
			}
		};
	}

}