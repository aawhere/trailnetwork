/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.google.common.collect.HashBiMap;
import com.google.inject.Singleton;
import com.googlecode.objectify.annotation.Entity;

/**
 * An Objectify implementation of RegisteredEntities.
 * 
 * This perhaps could be done using Guice Named annotations, but the name is dynamically provided
 * from key utils so we create our own map.
 * 
 * @author aroller
 * 
 */
@Singleton
@SuppressWarnings({ "rawtypes", "unchecked" })
public class RegisteredEntitiesObjectifyFactory
		implements RegisteredEntities {

	/**
	 * Stores the registered types in the system.
	 * 
	 * Yes, this should be an instance, but Objectify registration is static so during testing and
	 * other situations objectify may get an incomplete instance and ignore a new registration.
	 * 
	 */
	private static final HashBiMap<String, Class<? extends Identifier>> idTypeForKind = HashBiMap.create();

	/**
	 * Given the identifier this will register all that can be known from the id type alone. FIXME:
	 * figure out a better way to register than public mutation. A builder singleton?
	 * */
	public <K> RegisteredEntitiesObjectifyFactory register(Class<? extends Identifier> idType) {
		Class<K> kind = IdentifierUtils.kindFrom((Class<? extends Identifier<?, K>>) idType);
		String kindString;
		if (kind.getAnnotation(Entity.class) != null) {
			kindString = ObjectifyKeyUtils.getKind(kind);
		} else {
			kindString = kind.getSimpleName();
		}

		Class<? extends Identifier> alreadyRegistered = idTypeForKind.get(kindString);
		if (alreadyRegistered != null) {
			if (!alreadyRegistered.equals(idType)) {
				throw new IllegalArgumentException(kindString + " registering " + idType + " already registed with "
						+ alreadyRegistered);
			}
		} else {
			// register it only after the check since we don't want it to have
			// residue of the replacement
			idTypeForKind.put(kindString, idType);

		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.RegisteredEntities#idType(java.lang.String)
	 */
	@Override
	public <I extends Identifier<?, ?>> Class<I> idType(String kind) {

		Class<I> class1 = (Class<I>) idTypeForKind.get(kind);
		if (class1 == null) {
			throw new IllegalArgumentException(kind + " not registered in " + idTypeForKind.toString());
		}
		return class1;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.RegisteredEntities#isKindKnown(java.lang.String)
	 */
	@Override
	public Boolean isKindKnown(String kind) {
		return idTypeForKind.containsKey(kind);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.RegisteredEntities#isIdKnown(java.lang.Class)
	 */
	@Override
	public Boolean isIdKnown(Class<? extends Identifier<?, ?>> idType) {
		return idTypeForKind.inverse().containsKey(idType);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return idTypeForKind.toString();
	}
}
