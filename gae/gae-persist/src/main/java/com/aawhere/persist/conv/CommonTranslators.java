/**
 *
 */
package com.aawhere.persist.conv;

import com.aawhere.lang.net.UriTranslatorFactory;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.impl.translate.Translators;

/**
 * <p>
 * A convenient static method that adds all the common converters to your factory's conversions.
 * </p>
 * 
 * @author Brian Chapman
 */
public class CommonTranslators {
	public static void add(ObjectifyFactory fact) {
		Translators translators = fact.getTranslators();
		translators.add(new ClassTranslatorFactory());
		translators.add(new XmlConfigurationTranslatorFactory());
		translators.add(new UriTranslatorFactory());
	}
}