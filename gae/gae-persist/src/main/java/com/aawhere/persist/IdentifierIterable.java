/**
 *
 */
package com.aawhere.persist;

import java.util.Iterator;

import com.aawhere.id.Identifier;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.googlecode.objectify.Key;

/**
 * Allows for iterating over a collection of objectify keys as if they were {@link Identifier}s.
 * 
 * @author Brian Chapman
 * 
 */
public class IdentifierIterable<EntityT extends BaseEntity<EntityT, IdentifierT>, IdentifierT extends Identifier<?, EntityT>>
		implements Iterable<IdentifierT> {

	Iterable<Key<EntityT>> objectifyIterable;
	Class<EntityT> entityType;
	Class<IdentifierT> identifierType;

	public IdentifierIterable(Iterable<Key<EntityT>> objectifyIterable, Class<EntityT> entityType,
			Class<IdentifierT> identifierType) {
		this.objectifyIterable = objectifyIterable;
		this.entityType = entityType;
		this.identifierType = identifierType;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<IdentifierT> iterator() {
		return new IdentifierIterator<EntityT, IdentifierT>(objectifyIterable.iterator(), entityType, identifierType);
	}

	public static class IdentifierIterator<EntityT extends BaseEntity<EntityT, IdentifierT>, IdentifierT extends Identifier<?, EntityT>>
			implements Iterator<IdentifierT> {

		Iterator<Key<EntityT>> objectifyIterator;
		Class<EntityT> entityType;
		Class<IdentifierT> identifierType;

		public IdentifierIterator(Iterator<Key<EntityT>> objectifyIterator, Class<EntityT> entityType,
				Class<IdentifierT> identifierType) {
			this.objectifyIterator = objectifyIterator;
			this.entityType = entityType;
			this.identifierType = identifierType;
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return objectifyIterator.hasNext();
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#next()
		 */
		@Override
		public IdentifierT next() {
			Key<EntityT> key = objectifyIterator.next();
			return ObjectifyKeyUtils.id(key, identifierType);
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			objectifyIterator.remove();
		}

	}

}
