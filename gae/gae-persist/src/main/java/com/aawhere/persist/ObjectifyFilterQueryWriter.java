/**
 * 
 */
package com.aawhere.persist;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.collections.Index;
import com.aawhere.field.DictionaryIndex;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldMessage;
import com.aawhere.field.FieldNotIndexedException;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageStatus;
import com.aawhere.util.rb.StatusMessages;
import com.google.appengine.api.datastore.Cursor;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.cmd.Query;
import com.googlecode.objectify.impl.DatastoreQueryAccessor;

/**
 * A pojo used to fill {@link Query} from a {@link Filter} adding {@link FilterConditions}.
 * 
 * This is only in GAE because of the Objectify Query. It could be generalized easily if needed.
 * 
 * @author aroller
 * 
 */
public class ObjectifyFilterQueryWriter {

	private static final String FIELD_OPERATOR_SEPARATOR = " ";
	static final String DESCENDING_PREFIX = "-";

	/**
	 * Used to construct all instances of GaeFilterConditionQueryWriter.
	 */
	public static class Builder
			extends ObjectBuilder<ObjectifyFilterQueryWriter> {

		public Builder() {
			super(new ObjectifyFilterQueryWriter());
		}

		public Builder setFilter(Filter filter) {
			building.filter = filter;
			return this;
		}

		public Builder setQuery(Query<?> query) {
			building.query = query;
			return this;
		}

		public Builder setEntityType(Class<?> entityType) {
			building.entityType = entityType;
			return this;
		}

		public Builder setFieldDictionaryFactory(FieldDictionaryFactory fieldDictionaryFactory) {
			building.fieldDictionaryFactory = fieldDictionaryFactory;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.exceptions().notNull("query", building.query);
			Assertion.exceptions().notNull("filter", building.filter);
			Assertion.exceptions().notNull("entityType", building.entityType);
			Assertion.exceptions().notNull("fieldDictionaryFactory", building.fieldDictionaryFactory);

			super.validate();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ObjectifyFilterQueryWriter build() {
			ObjectifyFilterQueryWriter built = super.build();
			built.process();
			return built;
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private Filter filter;
	private Class<?> entityType;
	private Query<?> query;
	private Boolean emptyResults = false;
	private HashSet<FieldKey> keysRequiredIndexes = new HashSet<>();

	/**
	 * Reports the problems and successes with each component of the query. This allows for multiple
	 * problem reporting rather than throwing an exception when the first problem is found during
	 * processing.
	 */
	private StatusMessages statusMessages;
	private FieldDictionaryFactory fieldDictionaryFactory;
	private FieldDictionary dicitionary;
	private String entityDomain;

	/** Use {@link Builder} to construct GaeFilterConditionQueryWriter */
	private ObjectifyFilterQueryWriter() {

	}

	/**
	 * Prepares the query given all the conditions. It looks up the {@link FieldDefinition} for each
	 * condition verifying the field was properly registered in the system.
	 * 
	 * All successes, warnings and errors are reported in {@link #statusMessages}.
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void process() {
		StatusMessages.Builder statusMessagesBuilder = new StatusMessages.Builder();
		processPaging();
		// filter conditions can only be applied to filters that represent field dictionaries.
		if (AnnotatedDictionaryUtils.isAnnotatedDictionary(entityType)) {

			Dictionary dictionaryAnnotation = AnnotatedDictionaryUtils.dictionaryFrom(entityType);
			this.entityDomain = dictionaryAnnotation.domain();
			this.dicitionary = new FieldAnnotationDictionaryFactory(fieldDictionaryFactory).getDictionary(entityType);
			fieldDictionaryFactory.register(dicitionary);
			if (filter.hasParent()) {
				Identifier<?, BaseEntity> parentId = (Identifier<?, BaseEntity>) filter.getParent();
				Key<BaseEntity> parentKey = ObjectifyKeyUtils.key(parentId);
				query = query.ancestor(parentKey);
			}
			// FIXME:This routine is too large. Break it up into smaller components to improve
			// readability
			List<FilterCondition> conditions = filter.getConditions();
			for (Iterator<FilterCondition> iterator = conditions.iterator(); iterator.hasNext();) {
				FilterCondition<?> condition = iterator.next();

				// try to find the field for each condition. Handle multiple errors.
				try {
					FieldDefinition<?> conditionFieldDefinition = this.fieldDictionaryFactory.findDefinition(condition
							.getField());
					try {
						FieldKey conditionFieldKey = conditionFieldDefinition.getKey();

						// only apply the filter condition if it is against the domain this filter
						// represents
						if (FieldUtils.isFromDomain(entityDomain, conditionFieldKey)) {

							if (!usePredicateInstead(conditionFieldDefinition)) {
								// make sure the index is available
								assertIndexed(conditionFieldDefinition);

								String fieldName = fieldName(conditionFieldDefinition);
								// here would should look up the persistence alias if it provides
								// one
								// the field keys shouldn't have to match the database columns?
								FilterOperator operator = condition.getOperator();

								Object value = condition.getValue();
								if (value != null) {
									// Deal with Iterables and lists here.
									if (value instanceof Iterable) {
										value = Lists.newArrayList((Iterable<?>) value);
										if (((List) value).size() == 1 && operator.equals(FilterOperator.EQUALS)) {
											// Treat the value as a singular and not a list.
											value = ((List) value).get(Index.FIRST.ordinal());
										} else if (operator.equals(FilterOperator.EQUALS)) {
											// the natural beahvior for an equals against a
											// collection
											// is an IN..so switch it, but only if there are more
											// than one elements
											// in the list.
											operator = FilterOperator.IN;
										}
									}
								}
								if (value instanceof Iterable<?> && Iterables.isEmpty((Iterable<?>) value)) {
									/*
									 * Cannot query against an empty collection. However, the query
									 * expects the results to have one of the values in the in query
									 * to match the results, so essentially this query must return 0
									 * results.
									 */
									this.emptyResults = true;
									statusMessagesBuilder
											.addMessage(MessageStatus.WARNING,
														new CompleteMessage.Builder(
																PersistMessage.EMPTY_COLLECTION_QUERY)
																.addParameter(	FieldMessage.Param.FIELD_KEY,
																				conditionFieldKey).build());
								} else {
									String operatorString = operator.operator;
									// only if we query against the key column do we need to do it
									// differently
									if (fieldName.equals(BaseEntity.BASE_FIELD.ID)) {
										query = query.filterKey(operatorString, value);
									} else {
										String fieldAndOperator = new StringBuilder().append(fieldName)
												.append(FIELD_OPERATOR_SEPARATOR).append(operatorString).toString();
										query = query.filter(fieldAndOperator, value);
									}
									statusMessagesBuilder
											.addMessage(MessageStatus.OK,
														new CompleteMessage.Builder(PersistMessage.OK_QUERY)
																.addParameter(	FieldMessage.Param.FIELD_KEY,
																				conditionFieldKey)
																.addParameter(	PersistMessage.Param.FILTER_OPERATOR,
																				operator)
																.addParameter(PersistMessage.Param.VALUE, value)
																.build());
								}
							} else {
								reportWrongDomain(statusMessagesBuilder, conditionFieldKey);

							}
						}
					} catch (RuntimeException e) {
						throw new RuntimeException("storage problem with " + condition, e);
					}
				} catch (FieldNotRegisteredException e) {

					reportFieldNotFound(statusMessagesBuilder, e);
				}

			}
			processPredicates(statusMessagesBuilder);
			processSort(statusMessagesBuilder);
			// all keys have passed individual indexes, now check for combined indexes
			if (this.keysRequiredIndexes.size() > 1) {
				HashSet<FieldKey> keysRequiringIndex = this.keysRequiredIndexes;
				assertIndexed(keysRequiringIndex);
			}
		}
		this.statusMessages = statusMessagesBuilder.build();
	}

	/**
	 * @param statusMessagesBuilder
	 * @param conditionFieldKey
	 */
	private void reportWrongDomain(StatusMessages.Builder statusMessagesBuilder, FieldKey conditionFieldKey) {
		// wrong domain
		statusMessagesBuilder.addMessage(	MessageStatus.OK,
											new CompleteMessage.Builder(PersistMessage.DOMAIN_DIFFERENT)
													.addParameter(FieldMessage.Param.FIELD_KEY, conditionFieldKey)
													.addParameter(PersistMessage.Param.DOMAIN, entityDomain).build());
	}

	/**
	 * Validates that the given field has a single index. Combination indexes require individual
	 * indexes so all individual indexes are checked first.
	 * 
	 * @param conditionFieldDefinition
	 */
	private void assertIndexed(FieldDefinition<?> conditionFieldDefinition) {

		if (FieldUtils.isFromDomain(entityDomain, conditionFieldDefinition.getKey())) {
			final FieldKey key = conditionFieldDefinition.getKey();
			// verify it is indexed for querying (TN-182)
			// multiple property indexes require each property to be indexed
			if (ArrayUtils.isEmpty(conditionFieldDefinition.getIndexedFor())) {
				// fine, let's see if it is declared class level
				assertIndexed(Sets.newHashSet(key));
			}

			this.keysRequiredIndexes.add(key);
		}
	}

	/**
	 * asserts combined indexes are properly declared.
	 * 
	 * @param keysRequiringIndex
	 */
	private void assertIndexed(Set<FieldKey> keysRequiringIndex) {
		List<DictionaryIndex> indexes = this.dicitionary.getIndexes();
		boolean indexFound = false;
		for (DictionaryIndex combinationIndex : indexes) {
			final Set<FieldKey> keysAvailable = combinationIndex.keys();
			if (Sets.difference(keysRequiringIndex, keysAvailable).isEmpty()) {
				indexFound = true;
			}
		}
		if (!indexFound) {

			throw new FieldNotIndexedException(keysRequiringIndex, this.entityDomain, indexes);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private boolean usePredicateInstead(FieldDefinition definition) throws FieldNotRegisteredException {
		boolean replaced = false;
		// only deal with fields not indexed
		if (filter.isAutoPredicates() && ArrayUtils.isEmpty(definition.getIndexedFor())) {
			FieldKey fieldKey = definition.getKey();
			if (filter.containsCondition(fieldKey)) {
				if (FieldUtils.definesComparableType(definition)) {
					Collection<FilterCondition> conditions = this.filter.conditions(fieldKey);
					Class<? extends BaseEntity> type = (Class<? extends BaseEntity>) this.entityType;
					for (FilterCondition filterCondition : conditions) {
						Predicate filterConditionPredicate = FilterUtil
								.filterConditionPredicate(filterCondition, fieldDictionaryFactory, type);
						filter = Filter.clone(filter).replaceCondition(filterCondition, filterConditionPredicate)
								.filter();
						replaced = true;
					}
				}

			}
		}
		return replaced;
	}

	/** Adds the appropriate paging, start, limit restrictions to the query. */
	private void processPaging() {
		// determine the start
		if (filter instanceof Filter && filter.hasCursor()) {
			final Cursor cursor = Cursor.fromWebSafeString(filter.getCursor());
			query = query.startAt(cursor);
		} else {
			if (filter.hasLimit()) {
				query = query.offset(filter.getStart() - 1);
			}
		}
		// TN-501 improved handling of limits and chunks
		if (filter.hasLimit()) {
			query = query.limit(filter.getLimit());
		}
		if (filter.hasChunkSize()) {
			query = query.chunk(filter.getChunkSize());
		} else if (filter.hasLimit()) {
			// might as well get chunks at least the size of the limit?
			query = query.chunk(filter.getLimit());
		}
	}

	/**
	 * Given a field Key this will return the fieldName that can be used in a query. This will write
	 * embedded fields in a way google datastore can understand.
	 * 
	 * @see Field#columnName()
	 * @see FieldAnnotationDictionaryFactory
	 * 
	 * @param conditionFieldDefinition
	 * @return
	 */
	static String fieldName(FieldDefinition<?> conditionFieldDefinition) {
		return conditionFieldDefinition.getColumnName();
	}

	/**
	 * @param statusMessagesBuilder
	 * @param e
	 */
	private void reportFieldNotFound(StatusMessages.Builder statusMessagesBuilder, FieldNotRegisteredException e) {
		statusMessagesBuilder.addMessage(MessageStatus.ERROR, e.getCompleteMessage());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void processPredicates(final StatusMessages.Builder statusMessagesBuilder) {
		List<FilterCondition> predicateConditions = filter.getPredicateConditions();
		com.aawhere.persist.Filter.Builder clone = Filter.clone(filter);
		if (CollectionUtils.isNotEmpty(predicateConditions)) {
			for (FilterCondition filterCondition : predicateConditions) {
				try {
					final FieldDefinition definition = fieldDictionaryFactory
							.findDefinition(filterCondition.getField());
					if (FieldUtils.definesComparableType(definition)) {
						Predicate filterConditionPredicate;
						filterConditionPredicate = FilterUtil
								.filterConditionPredicate(	filterCondition,
															fieldDictionaryFactory,
															(Class<? extends BaseEntity>) this.entityType);
						clone.addPredicate(filterConditionPredicate);
					} else {
						reportNotComparable(statusMessagesBuilder, definition);
					}
				} catch (FieldNotRegisteredException e) {
					reportFieldNotFound(statusMessagesBuilder, e);
				}
			}
			filter = clone.build();
		}
	}

	private void processSort(final StatusMessages.Builder statusMessagesBuilder) {

		// deal with datastore sort requests
		processSort(statusMessagesBuilder,
					this.filter.getOrderBy(),
					new Function<Pair<FilterSort, FieldDefinition<Object>>, Object>() {

						@Override
						public Object apply(Pair<FilterSort, FieldDefinition<Object>> input) {
							FilterSort filterSort = input.getLeft();
							FieldDefinition<Object> definition = input.getRight();
							StringBuilder queryString = new StringBuilder();
							if (filterSort.getDirection().equals(FilterSort.Direction.DESCENDING)) {
								queryString.append(DESCENDING_PREFIX);
							}
							// get definition of the field
							assertIndexed(definition);
							queryString.append(fieldName(definition));
							query = query.order(queryString.toString());
							return null;
						}
					});

		// now deal with in-memory sorting...if any
		processSort(statusMessagesBuilder,
					filter.getOrderByInMemory(),
					new Function<Pair<FilterSort, FieldDefinition<Object>>, Object>() {

						@Override
						public Object apply(Pair<FilterSort, FieldDefinition<Object>> input) {
							FilterSort filterSort = input.getLeft();
							FieldDefinition<Object> definition = input.getRight();
							if (FieldUtils.definesComparableType(definition)) {
								Ordering<Comparable<?>> ordering = Ordering.natural();
								if (filterSort.getDirection().equals(FilterSort.Direction.DESCENDING)) {
									ordering = ordering.reverse();
								}
								Function<?, ? extends Comparable<?>> fieldValueFunction = AnnotatedDictionaryUtils
										.fieldComparableValueFunction(definition.getKey());
								filter = Filter.clone(filter).ordering(ordering.onResultOf(fieldValueFunction)).build();
							} else {
								reportNotComparable(statusMessagesBuilder, definition);
							}
							return null;

						}

					});
	}

	/**
	 * @param statusMessagesBuilder
	 * @param definition
	 */
	private void reportNotComparable(final StatusMessages.Builder statusMessagesBuilder,
			FieldDefinition<Object> definition) {
		statusMessagesBuilder.addMessage(	MessageStatus.ERROR,
											CompleteMessage.create(PersistMessage.SORT_FIELD_NOT_COMPARABLE)
													.param(FieldMessage.Param.FIELD_KEY, definition.getKey())
													.param(PersistMessage.Param.FIELD_DATA_TYPE, definition.getType())
													.build());
	}

	/**
	 * delegate method that does the common items for sorting, but delegates the type of sorting to
	 * the function provided.
	 * 
	 * @param statusMessagesBuilder
	 * @param orderBy
	 * @param sortFunction
	 */
	private void processSort(StatusMessages.Builder statusMessagesBuilder, List<FilterSort> orderBy,
			Function<Pair<FilterSort, FieldDefinition<Object>>, Object> sortFunction) {
		if (orderBy != null) {
			for (FilterSort filterSort : orderBy) {
				FieldDefinition<Object> definition;
				try {
					definition = this.fieldDictionaryFactory.findDefinition(filterSort.getField());
					final FieldKey key = definition.getKey();
					if (FieldUtils.isFromDomain(entityDomain, key)) {
						sortFunction.apply(Pair.of(filterSort, definition));
					} else {
						reportWrongDomain(statusMessagesBuilder, key);
					}
				} catch (FieldNotRegisteredException e) {
					reportFieldNotFound(statusMessagesBuilder, e);
				}
			}
		}
	}

	/**
	 * @return the query
	 */
	public Query<?> getQuery() {
		return this.query;
	}

	/**
	 * Provides the Datastore version of the {@link #query}.
	 * 
	 * @return
	 */
	public com.google.appengine.api.datastore.Query getRawQuery() {
		return DatastoreQueryAccessor.query(query);
	}

	/**
	 * Provides access to the updated filter that may have been prepared to handle situations that
	 * the datastore is not.
	 * 
	 * @see #usePredicatesInstead(Set)
	 * @return
	 */
	public Filter getFilter() {
		return this.filter;
	}

	/**
	 * A flag to users of this that it is guaranteed to produce no results because a collection that
	 * was provided is empty.
	 * 
	 * @return the emptyResults
	 */
	public Boolean isEmptyResults() {
		return this.emptyResults;
	}

	/**
	 * @return the statusMessages
	 */
	public StatusMessages getStatusMessages() {
		return this.statusMessages;
	}
}
