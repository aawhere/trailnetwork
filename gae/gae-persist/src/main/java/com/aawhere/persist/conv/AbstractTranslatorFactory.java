/**
 *
 */
package com.aawhere.persist.conv;

import java.lang.reflect.Type;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.Property;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.Translator;
import com.googlecode.objectify.impl.translate.TranslatorFactory;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.repackaged.gentyref.GenericTypeReflector;

/**
 * Base class for creating Translator Factories. Does basic type checking for you.
 * 
 * @author Brian Chapman
 * 
 * @param <P>
 *            pojo
 * @param <D>
 *            datastore
 */
public abstract class AbstractTranslatorFactory<P, D>
		implements TranslatorFactory<P, D> {

	/** */
	Class<? extends P> pojoType;

	/** */
	protected AbstractTranslatorFactory(Class<? extends P> pojoType) {
		this.pojoType = pojoType;
	}

	@Override
	public Translator<P, D> create(TypeKey<P> tk, CreateContext ctx, Path path) {
		Type type = tk.getType();
		if (this.pojoType.isAssignableFrom(GenericTypeReflector.erase(type))) {
			return createSafe(path, null, type, ctx);
		} else {
			return null;
		}
	}

	/**
	 * Create a translator, knowing that we have the appropriate type. You don't need to check for
	 * type matching.
	 * 
	 * @param type
	 *            is guaranteed to erase to something assignable to Class
	 *            <P>
	 */
	abstract protected Translator<P, D> createSafe(Path path, @Deprecated Property property, Type type,
			CreateContext ctx);
}