/**
 *
 */
package com.aawhere.persist;

import static com.aawhere.persist.FilterUtil.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Nullable;

import com.aawhere.collections.Index;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.id.Identifier;
import com.aawhere.lang.reflect.GenericUtils;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.aawhere.util.rb.StatusMessages;
import com.aawhere.util.rb.StatusMessagesRuntimeException;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreTimeoutException;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.common.base.Function;
import com.google.common.collect.Iterators;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.cmd.Query;

/**
 * A single repository combing the {@link ObjectifyRepository} and the {@link FilterRepository}
 * behavior using {@link com.aawhere.persist.ObjectifyFilterRepositoryDelegate} as a composition.
 * 
 * TODO:Migrate {@link ObjectifyFilterRepositoryDelegate} into this class. Why do we need a
 * delegate?
 * 
 * @author aroller
 * 
 */
/* @formatter:off */
abstract public class ObjectifyFilterRepository<
                                       E extends BaseEntity<E, I>,
                                       I extends Identifier<?, E>,
                                       S extends BaseFilterEntities<E>,
									   B extends BaseFilterEntities.Builder<B, E, S>
                                       >
             extends ObjectifyRepository<E, I>
			 implements FilterRepository<S, E, I>{
/* @formatter:on */

	private final Class<B> builderType;
	private final FieldDictionaryFactory fieldDictionaryFactory;
	private final FieldDictionary dictionary;

	/**
	 * Provide the custom filter repository to be used to perform the filtering actions.
	 * 
	 * @deprecated filter repository is now "this"
	 */
	@Deprecated
	protected ObjectifyFilterRepository(ObjectifyFilterRepositoryDelegate<S, B, E, I> filterRepository,
			ObjectifyRepository.Synchronization synchronous) {
		this(filterRepository.dictionaryFactory(), synchronous);

	}

	/**
	 * @deprecated temporary during removal of delgate
	 * 
	 * @param filterRepository
	 * @param synchronous
	 */
	@Deprecated
	protected ObjectifyFilterRepository(ObjectifyFilterRepository<E, I, S, B> filterRepository,
			ObjectifyRepository.Synchronization synchronous) {
		this(filterRepository.dictionaryFactory(), synchronous);

	}

	protected ObjectifyFilterRepository(FieldDictionaryFactory fieldDictionaryFactory, Synchronization synchronization) {
		super(synchronization);
		this.fieldDictionaryFactory = fieldDictionaryFactory;
		this.dictionary = new FieldAnnotationDictionaryFactory(fieldDictionaryFactory).getDictionary(entityType());
		this.builderType = GenericUtils.getTypeArgument(this, Index.FOURTH);
	}

	protected ObjectifyFilterRepository(FieldDictionaryFactory fieldDictionaryFactory) {
		this(fieldDictionaryFactory, SYNCHRONIZATION_DEFAULT);
	}

	protected ObjectifyFilterRepository(ObjectifyFilterRepositoryDelegate<S, B, E, I> filterRepository) {
		this(filterRepository.dictionaryFactory(), SYNCHRONIZATION_DEFAULT);
	}

	/**
	 * Provides the children of the parent id given as an Entities object.
	 * 
	 * @see #children(Identifier) for the paginator.
	 * @param parentId
	 * @return
	 */
	public S childrenEntities(Identifier<?, ? extends BaseEntity<?, ?>> parentId) {
		Iterable<E> children = children(parentId);
		return getEntitiesBuilder().addAll(children).setFilter(Filter.create().setParent(parentId).build()).build();
	}

	public Iterable<E> entityPaginator() {
		return entityPaginator(null);
	}

	/**
	 * re-stores every entity found in the filter paginating in batch form until all results have
	 * been touched.
	 * 
	 * @param filter
	 */
	public Integer touched(Filter filter) {
		int batchSize = 50;
		int totalCount = 0;

		Iterable<E> entityPaginator = entityPaginator(filter);
		ArrayList<E> batch = new ArrayList<>(batchSize);
		for (E entityT : entityPaginator) {
			totalCount++;
			batch.add(entityT);
			if (batch.size() == batchSize) {
				storeAll(batch);
				batch = new ArrayList<>(batchSize);
			}
		}
		storeAll(batch);
		return totalCount;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#filter(com.aawhere.persist.BaseFilter )
	 */
	@Override
	public S filter(final Filter filter) {
		ObjectifyFilterQueryResults<E, I> results = results(filter);

		// allows a child to transform the results being assigned.
		B entitiesBuilder = getEntitiesBuilder();
		entitiesBuilder.addAll(results);
		S entities = entitiesBuilder.setFilter(results.getFilter()).build();
		return entities;
	}

	/**
	 * Counts all that match without limit.
	 * 
	 * @param filter
	 * @return
	 */
	@Override
	public Integer count(Filter filter) {
		return writer(unrestricted(filter)).getQuery().count();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#filterAsIterable(com.aawhere.persist .BaseFilter)
	 */
	@Override
	public Iterable<E> filterAsIterable(Filter filter) {
		return results(filter).asIterable();
	}

	/**
	 * Provides the conversion function for a key to {@link Identifier}. Override if you would like
	 * better efficiency since {@link ObjectifyKeyUtils#id(Key, Class)} isn't particularly efficient
	 * 
	 * @return
	 */
	protected Function<Key<E>, I> keyToIdFunction() {
		return ObjectifyKeyUtils.idFunction(idType(), entityType());
	}

	/**
	 * For those that need access to the filter and query.
	 * 
	 * @param filter
	 * @return
	 * @throws EmptyQueryException
	 */
	private ObjectifyFilterQueryResults<E, I> results(Filter filter) {
		final com.aawhere.persist.ObjectifyFilterQueryResults.Builder<E, I> resultsBuilder = ObjectifyFilterQueryResults
				.create();
		ObjectifyFilterQueryResults<E, I> results = resultsBuilder.writerFunction(writerFunction()).filter(filter)
				.build();

		StatusMessages statusMessages = results.getWriter().getStatusMessages();

		if (statusMessages.hasError()) {
			// this is considered unrecoverable since an invalid query has been made
			// if it came from the web services then this will report to the web services just fine.
			throw new StatusMessagesRuntimeException(statusMessages);
		}
		return results;
	}

	/**
	 * @param filter
	 * @return
	 */
	protected ObjectifyFilterQueryWriter writer(Filter filter) {
		Query<E> query = newQuery();
		ObjectifyFilterQueryWriter queryWriter = new ObjectifyFilterQueryWriter.Builder().setEntityType(entityType())
				.setFilter(filter).setFieldDictionaryFactory(dictionaryFactory()).setFilter(filter).setQuery(query)
				.build();
		return queryWriter;
	}

	/**
	 * For those systems requiring a raw datastore query this will provide the query using the
	 * standard processing {@link ObjectifyFilterQueryWriter} so the query is prepared the same way
	 * it would be through standard processing.
	 * 
	 * @param activityFilter
	 * @return
	 */
	public com.google.appengine.api.datastore.Query datastoreQuery(Filter filter) {
		// the filter builder provides any standard processing for activity filters
		return writer(filter).getRawQuery();
	}

	public Function<Filter, ObjectifyFilterQueryWriter> writerFunction() {
		return new Function<Filter, ObjectifyFilterQueryWriter>() {

			@Override
			public ObjectifyFilterQueryWriter apply(Filter filter) {
				return writer(filter);
			}
		};
	}

	protected B getEntitiesBuilder() {
		BaseEntitiesReflection<S, B> reflector = BaseEntitiesReflection.newInstance();
		try {
			return reflector.constructDefaultBuilder(builderType);
		} catch (InstantiationException e) {
			throw new RuntimeException("Could not initiate builder for " + builderType, e);
		}
	}

	private Query<E> newQuery() {
		return ofy().load().type(entityType()).hybrid(Boolean.FALSE);
	}

	final protected Filter.Builder getFilterBuilder(Filter filter) {
		return Filter.clone(filter);
	}

	/**
	 * Provides access to the field dictionary factory used to verify fields and build field
	 * queries.
	 * 
	 * @return
	 */
	public FieldDictionaryFactory getFieldDictionaryFactory() {
		return dictionaryFactory();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#ids(com.aawhere.persist.Filter)
	 */
	@Override
	public Iterable<I> ids(Filter filter) {
		@SuppressWarnings("unchecked")
		final Query<E> query = (Query<E>) writer(filter).getQuery();
		return ObjectifyKeyUtils.ids(query.keys(), idType(), entityType());
	}

	/**
	 * Retrieves all results making multiple requests in an efficient manner reducing read costs.
	 * 
	 * If you don't need all results then consider using {@link #filterAsIds(Filter)} as it will
	 * respect your filter limits.
	 * 
	 * FIXME:This should use {@link ObjectifyFilterQueryResults}.
	 * 
	 */
	@Override
	public Iterable<I> idPaginator(@Nullable final Filter filter) {
		final Filter unrestrictedFilter = unrestricted(filter);

		return new Iterable<I>() {
			// an exception to using results since this filters ids only
			Query<E> query = (Query<E>) writer(unrestrictedFilter).getQuery();

			@Override
			public Iterator<I> iterator() {
				return new Iterator<I>() {
					QueryResultIterator<Key<E>> keyIterator;
					private Iterator<I> ids = ids(query.keys());

					private Iterator<I> ids(QueryResultIterable<Key<E>> entityIterable) {
						keyIterator = entityIterable.iterator();

						Function<Key<E>, I> function = keyToIdFunction();
						return Iterators.transform(keyIterator, function);
					}

					@Override
					public boolean hasNext() {

						return ids.hasNext();
					}

					@Override
					public I next() {
						try {
							return ids.next();
						} catch (DatastoreTimeoutException | IllegalArgumentException timeout) {
							// java.lang.IllegalArgumentException: The requested query has
							// expired. Please restart it with the last cursor to read more
							// results.
							Cursor cursor = keyIterator.getCursor();
							LoggerFactory.getLogger(getClass()).warning("hadling a timeout while iterating " + filter
									+ " startin at " + cursor);
							ids = ids(query.startAt(cursor).keys());
							return ids.next();
						}
					}

					@Override
					public void remove() {
						ids.remove();
					}
				};
			}
		};
	}

	/**
	 * Provides all the entities in an efficient, consistent manner. It will self-recover and
	 * continue where it left off after a {@link DatastoreTimeoutException}.
	 * 
	 * @see #idPaginator(Filter)
	 * 
	 * @param filter
	 * @return
	 */
	@Override
	public Iterable<E> entityPaginator(@Nullable Filter filter) {

		final Filter unrestrictedFilter = unrestricted(filter);
		final com.aawhere.persist.ObjectifyFilterQueryResults.Builder<E, I> resultsBuilder = ObjectifyFilterQueryResults
				.create();
		return resultsBuilder.writerFunction(writerFunction()).filter(unrestrictedFilter).continueAfterTimeout()
				.build().asIterable();

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#entities(java.lang.Iterable)
	 */
	@Override
	public S entities(Iterable<I> ids) {
		if (ids == null) {
			return null;
		}

		Map<Key<E>, E> map = ofy().load().keys(ObjectifyKeyUtils.keys(ids));
		B entitiesBuilder = getEntitiesBuilder();
		Class<I> idType = idType();
		return ObjectifyKeyUtils.entities(map, ids, entitiesBuilder, idType);
	}

	/**
	 * Provides the dictionary for {@link #entityType}.
	 * 
	 * @return
	 */
	@Override
	public FieldDictionary dictionary() {
		return this.dictionary;
	}

	@Override
	public FieldDictionaryFactory dictionaryFactory() {
		return this.fieldDictionaryFactory;
	}

	/**
	 * @deprecated don't use the delegate anymore. just use the filter repository
	 * 
	 * @return
	 */
	@Deprecated
	public ObjectifyFilterRepositoryDelegate<S, B, E, I> getFilterDelegate() {
		return new ObjectifyFilterRepositoryDelegate<S, B, E, I>(this);
	}

}
