/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.id.Identifier;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;

/**
 * General helper for accessing {@link DatastoreService} products.
 * 
 * @see DatastoreKeyUtil
 * @author aroller
 * 
 */
public class DatastoreUtil {

	/**
	 * Retrieves the value from the Entity, that is assumed to be the dictionary,
	 * 
	 * @param entity
	 * @param definition
	 * @return
	 * @throws ClassCastException
	 *             when the definition defines a type not matching the entity property
	 */
	@SuppressWarnings("unchecked")
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>, T> T value(Entity entity,
			FieldDefinition<T> definition) {
		if (definition.getKey().getRelativeKey().equals(BaseEntity.BASE_FIELD.ID)) {
			Class<E> type = (Class<E>) definition.getDictionary().getType();
			Class<I> identifierType = EntityUtil.getIdentifierType(type);
			return (T) DatastoreKeyUtil.id(entity.getKey(), identifierType);
		} else {
			return (T) entity.getProperty(definition.getColumnName());
		}
	}

	/**
	 * Indicates true if the Entity is defined by the {@link FieldDictionary}.
	 * 
	 * @param dictionary
	 * @param entity
	 * @return
	 */
	public static Boolean defines(FieldDictionary dictionary, Entity entity) {
		return dictionary.getType().getSimpleName().equals(entity.getKind());
	}

	/**
	 * Provide the most basic query givent the entity type by assigning the kind.
	 * 
	 * @param entityClass
	 * @return
	 */
	public static Query query(Class<? extends BaseEntity<?, ?>> entityClass) {
		return new Query(ObjectifyKeyUtils.getKind(entityClass));
	}
}
