/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.lang.exception.BaseException;

/**
 * A simple indicator
 * 
 * @author aroller
 * 
 */
public class EmptyQueryException
		extends BaseException {

	private static final long serialVersionUID = -1715805252058259102L;

	/**
	 * 
	 */
	public EmptyQueryException() {
	}

}
