/**
 *
 */
package com.aawhere.persist.objectify;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 * <p>
 * Useful class for creating a basic DAO. Typically you would extend this class and register your
 * entites in a static initializer, then provide higher-level data manipulation methods as desired.
 * </p>
 * 
 * @author Jeff Schnitzer
 * 
 */
public class DaoBase {

	/** Creates a DAO without a transaction */
	public DaoBase() {
	}

	/**
	 * Easy access to the factory object. This is convenient shorthand for
	 * {@code ObjectifyService.factory()}.
	 */
	public ObjectifyFactory fact() {
		return ObjectifyService.factory();
	}

	/**
	 * Easy access to the objectify object (which is lazily created).
	 */
	public Objectify ofy() {
		return ObjectifyService.factory().begin();
	}
}
