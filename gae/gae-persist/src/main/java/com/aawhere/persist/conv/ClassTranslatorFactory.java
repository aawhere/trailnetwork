package com.aawhere.persist.conv;

import org.apache.commons.lang3.ClassUtils;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Convert from {@link Class} to a String.
 * 
 * @author Brian Chapman
 * 
 */
@SuppressWarnings("rawtypes")
public class ClassTranslatorFactory
		extends ValueTranslatorFactory<Class, String> {

	public ClassTranslatorFactory() {
		super(Class.class);
	}

	@Override
	protected ValueTranslator<Class, String> createValueTranslator(TypeKey<Class> tk, CreateContext ctx, Path path) {
		return new ValueTranslator<Class, String>(String.class) {

			@Override
			protected String saveValue(Class value, boolean index, SaveContext ctx, Path path) throws SkipException {
				return value.getName();
			}

			@Override
			protected Class loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				try {
					return ClassUtils.getClass(value);
				} catch (ClassNotFoundException e) {
					throw new RuntimeException("Could not create class for type " + value, e);
				}
			}
		};
	}

}
