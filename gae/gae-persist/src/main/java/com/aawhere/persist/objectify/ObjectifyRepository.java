package com.aawhere.persist.objectify;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.Duration;

import com.aawhere.collections.FunctionsExtended;
import com.aawhere.collections.Index;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierTranslators;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.reflect.ClassUtilsExtended;
import com.aawhere.lang.reflect.GenericUtils;
import com.aawhere.measure.geocell.MeasureTranslators;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.ChildRepository;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.KeyedEntityFinder;
import com.aawhere.persist.MutableRepository;
import com.aawhere.persist.PersistMessage;
import com.aawhere.persist.Repository;
import com.aawhere.persist.RepositoryUtil;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.conv.CommonTranslators;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.RbTranslators;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.cmd.Query;
import com.googlecode.objectify.cmd.SimpleQuery;
import com.googlecode.objectify.impl.conv.joda.JodaTranslators;
import com.googlecode.objectify.impl.translate.opt.joda.JodaTimeTranslators;

/**
 * Generic DAO for use with Objectify
 * 
 * @param <EntityT>
 */
public abstract class ObjectifyRepository<EntityT extends BaseEntity<EntityT, IdentifierT>, IdentifierT extends Identifier<?, EntityT>>
		extends DaoBase
		implements Repository<EntityT, IdentifierT>, MutableRepository<EntityT, IdentifierT>,
		ChildRepository<EntityT, IdentifierT> {

	public static enum Synchronization {
		SYNCHRONOUS, ASYNCHRONOUS
	};

	protected static final Synchronization SYNCHRONIZATION_DEFAULT = Synchronization.ASYNCHRONOUS;
	/**
	 * Maximum writes per second according to the docs: 1 write per second (the supported limit for
	 * entity groups). https://developers.google.com/appengine/docs/java/datastore/
	 * structuring_for_strong_consistency
	 */
	public static final Duration MIN_DURATION_PER_WRITE_PER_ENTITY_GROUP = Duration.standardSeconds(1);
	private static final Integer MAX_RESULT_LIMIT = 1000;
	private boolean synchronous = false;

	static {
		// NOTE:Translators must be added before the Entities that need them.
		ObjectifyFactory fact = ObjectifyService.factory();
		JodaTranslators.add(fact);
		JodaTimeTranslators.add(fact);
		CommonTranslators.add(fact);
		MeasureTranslators.add(fact);
		RbTranslators.add(fact);
		IdentifierTranslators.add(fact);
	}

	/** The class managed by this repository so objects returned will be of this instance. */
	private Class<EntityT> entityType;
	private Class<IdentifierT> idType;

	protected Class<EntityT> getClazz() {
		return entityType;
	}

	public ObjectifyRepository() {
		this.entityType = GenericUtils.getTypeArgument(this, Index.FIRST);
		this.idType = GenericUtils.getTypeArgument(this, Index.SECOND);
	}

	/**
	 * Don't send datastore requests asynchronously, which is the default.
	 * 
	 * @param synchronization
	 */
	public ObjectifyRepository(Synchronization synchronization) {
		this();
		this.synchronous = synchronization == Synchronization.SYNCHRONOUS;
	}

	/**
	 * This operation is synchronous due to the fact that we need to obtain the {@link Key} for the
	 * return value. This means that this operation could take up to 1000 milliseconds to complete.
	 * 
	 * @see com.aawhere.persit.Repository#store(java.lang.Object)
	 */
	@Override
	public EntityT store(EntityT entity) {
		validateForStore(entity);
		return storeWithoutKeyCheck(entity);
	}

	/**
	 * @param entity
	 */
	private void validateForStore(EntityT entity) {
		if (entity.getId() != null && !(entity instanceof SelfIdentifyingEntity)) {
			throw new InvalidArgumentException(new CompleteMessage.Builder(PersistMessage.CANNOT_STORE)
					.addParameter(PersistMessage.Param.OBJECT_ID, entity.getId()).build());
		}
	}

	/**
	 * If {@link #synchronous} is {@link Synchronization#ASYNCHRONOUS} this will store without
	 * confirmation...hence the lack of the return value.
	 * 
	 * @see #store(BaseEntity)
	 * @see #storeWithoutKeyChecks(Iterable)
	 * 
	 * @deprecated use {@link #persist(BaseEntity)}
	 * @param entity
	 */
	@Deprecated
	public void storeAsynch(EntityT entity) {
		validateForStore(entity);
		storeWithoutKeyChecks(Arrays.asList(entity));
	}

	@Override
	public void persist(EntityT entity) {
		final ImmutableList<EntityT> list = ImmutableList.<EntityT> builder().add(entity).build();
		storeWithoutKeyChecks(list);
	}

	/**
	 * useful to update indexes or other changes that the entity iteself will handle with it's new
	 * settings.
	 */
	public EntityT touch(IdentifierT id) throws EntityNotFoundException {
		return store(load(id));
	}

	/**
	 * Store the entity without checking for null key, useful if your key is a composite or business
	 * key.
	 * 
	 * @param entity
	 * @return
	 */
	protected EntityT storeWithoutKeyCheck(EntityT entity) {
		Result<Map<Key<EntityT>, EntityT>> result = storeWithoutKeyChecks(Arrays.asList(entity));
		// this line is forcing synchronous
		return result.now().values().iterator().next();
	}

	/**
	 * public void storeAsynch(EntityT entity) { storeWithoutKeyChecks(Arrays.asList(entity)); }
	 * 
	 * /** Store multiple entities without checking for null key, useful if your key is a composite
	 * or business key.
	 * 
	 * @param entity
	 * @return
	 */
	protected Result<Map<Key<EntityT>, EntityT>> storeWithoutKeyChecks(Iterable<EntityT> entities) {
		final Result<Map<Key<EntityT>, EntityT>> result = ofy().save().entities(entities);
		applySynchronosity(result);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persit.Repository#storeAll(java.lang.Iterable)
	 */
	@Override
	public Map<IdentifierT, EntityT> storeAll(Iterable<EntityT> entities) {
		final Result<Map<Key<EntityT>, EntityT>> result = storeWithoutKeyChecks(entities);
		return ObjectifyKeyUtils.idEntityMap(IdentifierUtils.ids(entities), result.now(), getEntityClassRepresented());
	}

	/**
	 * Available only to implementing Repositories, this update will modify the contents of the
	 * existing Entity. If the synchronous flag is set, results will be applied immediately,
	 * otherwise the operation happens asynchronously. Note that this is DIFFERENT from the concept
	 * of eventual consistency, which affects queries (though not load requests).
	 * 
	 * @param entity
	 */
	@Override
	public EntityT update(EntityT entity) {

		validateForUpdate(entity);

		// FIXME: this should call storeAsynch always? just return the provided entity
		// Has id, we can update
		EntityT storedEntity = storeWithoutKeyCheck(entity);
		return storedEntity;
	}

	/**
	 * @param entity
	 */
	private void validateForUpdate(EntityT entity) {
		if (entity.getId() == null) {
			throw new InvalidArgumentException(new CompleteMessage.Builder(PersistMessage.CANNOT_UPDATE)
					.addParameter(PersistMessage.Param.OBJECT_ID, entity.getId()).build());
		}

	}

	/**
	 * Available only to implementing Repositories, this updates all entities with the contents of
	 * the entities provided. If the synchronous flag is set, results will be applied immediately,
	 * otherwise the operation happens asynchronously. Note that this is DIFFERENT from the concept
	 * of eventual consistency, which affects queries (though not load requests).
	 * 
	 * @param entities
	 */
	protected void updateAll(Iterable<EntityT> entities) {
		for (EntityT entity : entities) {
			validateForUpdate(entity);
		}
		storeWithoutKeyChecks(entities);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persit.Repository#load(com.googlecode.objectify.Key)
	 */
	@Override
	public EntityT load(IdentifierT id) throws EntityNotFoundException {
		try {
			return ofy().load().key(ObjectifyKeyUtils.key(id)).safe();
		} catch (NotFoundException e) {
			throw new EntityNotFoundException(id, e);
		}
	}

	@Override
	public Boolean exists(IdentifierT id) {
		// this should be free since it's a keys only query. Filter is a misnomer when providing a
		// key...it think.
		return ofy().load().filterKey(ObjectifyKeyUtils.key(id)).keys().iterator().hasNext();
	}

	@Override
	public Set<IdentifierT> idsExisting(Iterable<IdentifierT> ids) {
		// ideally ofy().keys(idsToKeys(ids)).keys().iterable() would exist
		// https://groups.google.com/forum/#!topic/objectify-appengine/zFI2YWP5DTI
		return loadAll(ids).keySet();
	}

	/**
	 * Provides all children for a given parent with no limit or filter.
	 * 
	 * @param parentId
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Iterable<EntityT> children(Identifier<?, ? extends BaseEntity> parentId) {
		Key<?> parentKey = ObjectifyKeyUtils.key(parentId);
		SimpleQuery<Object> entities = ofy().load().ancestor(parentKey);

		// Objectify includes the parent route in the results...remove it
		Predicate<Object> instanceOf = Predicates.instanceOf(this.entityType);
		final Iterable<Object> filteredEntities = Iterables.filter(entities, instanceOf);
		final Function<Object, EntityT> castFunction = FunctionsExtended.castFunction();
		return Iterables.transform(filteredEntities, castFunction);
	}

	/**
	 * Provides an efficient way to iterate the ids related to a parent. FIXME:upgrade this to live
	 * in {@link Repository}.
	 * 
	 * @param parentId
	 * @return
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Iterable<IdentifierT> childrenIds(Identifier<?, ? extends BaseEntity> parentId) {
		Key<?> parentKey = ObjectifyKeyUtils.key(parentId);
		final Iterable<Key<Object>> keys = ofy().load().ancestor(parentKey).keys().iterable();
		HashSet objects = Sets.newHashSet(keys);
		objects.toString();
		/*
		 * the parent is returned in the iterable so filter it out
		 * https://cloud.google.com/appengine/docs/java/datastore/queries#Java_Ancestor_filters //
		 * By default, ancestor queries include the specified ancestor itself. // The following
		 * filter excludes the ancestor from the query results. Filter keyFilter = new
		 * FilterPredicate(Entity.KEY_RESERVED_PROPERTY, FilterOperator.GREATER_THAN, tomKey)
		 */
		Iterable<Key<Object>> childrenKeys = Iterables.filter(keys, ObjectifyKeyUtils.keyKindPredicate(entityType));
		// helps make the keys acceptable to the key utility
		Function<Object, Key<EntityT>> castFunction = FunctionsExtended.castFunction();
		Iterable<Key<EntityT>> typedChildrensKeys = Iterables.transform(childrenKeys, castFunction);
		return ObjectifyKeyUtils.ids(typedChildrensKeys, idType, entityType);
	}

	/**
	 * @see #loadAll(Collection)
	 * @param ids
	 * @return
	 */
	@Override
	public Map<IdentifierT, EntityT> loadAll(Iterable<IdentifierT> ids) {
		Iterable<Key<EntityT>> keys = ObjectifyKeyUtils.keys(ids);
		Map<Key<EntityT>, EntityT> map = ofy().load().keys(keys);
		return ObjectifyKeyUtils.idEntityMap(ids, map, getEntityClassRepresented());
	}

	/**
	 * If the synchronous flag is set, results will be applied immediately, otherwise the operation
	 * happens asynchronously. Note that this is DIFFERENT from the concept of eventual consistency,
	 * which affects queries (though not load requests).
	 * 
	 * @see com.aawhere.persit.Repository#delete(java.lang.Object)
	 */
	@Override
	public void delete(EntityT entity) {
		Result<Void> result = ofy().delete().entity(entity);
		applySynchronosity(result);
	}

	/**
	 * If the synchronous flag is set, results will be applied immediately, otherwise the operation
	 * happens asynchronously. Note that this is DIFFERENT from the concept of eventual consistency,
	 * which affects queries (though not load requests).
	 * 
	 * @see com.aawhere.persit.Repository#deleteKey(com.googlecode.objectify .Key)
	 */
	@Override
	public void delete(IdentifierT entityId) {
		Result<Void> result = ofy().delete().entity(ObjectifyKeyUtils.key(entityId));
		applySynchronosity(result);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persit.Repository#deleteAll(java.lang.Iterable)
	 */
	@Override
	public void deleteAll(Iterable<EntityT> entities) {
		Result<Void> result = ofy().delete().entities(entities);
		applySynchronosity(result);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persit.Repository#deleteAllKeys(java.lang.Iterable)
	 */
	@Override
	public void deleteAllFromIds(Iterable<IdentifierT> ids) {
		if (ids != null) {
			final Iterable<Key<EntityT>> keys = ObjectifyKeyUtils.keys(ids);
			Result<Void> result = ofy().delete().keys(keys);
			applySynchronosity(result);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persit.Repository#findAll()
	 */
	public List<EntityT> findAll(Integer maxLimit) {
		Query<EntityT> q = ofy().load().type(entityType).limit(limit(maxLimit));
		return q.list();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persit.Repository#findRange(java.lang.Integer, java.lang.Integer)
	 */
	public List<EntityT> findRange(Integer start, Integer length) {
		Query<EntityT> q = ofy().load().type(entityType).offset(start).limit(limit(length));
		return q.list();
	}

	/**
	 * Finds the application by the business identify key (for those that have keys).
	 * 
	 * This is a helper method that should be a simple call through for any repository implementing
	 * {@link KeyedEntityFinder}.
	 * 
	 * @param key
	 * @return
	 */
	protected <K extends Identifier<?, EntityT>> EntityT findEntityByKey(K key) throws EntityNotFoundException {

		Object keyValue = key.getValue();

		// find the key field name so we can query it. (yes, this could be done once per class
		// instance
		Set<Field> keyFields = ClassUtilsExtended.getInstance().findFieldByType(entityType, key.getClass());
		String keyFieldName;
		if (keyFields.size() == 1) {
			keyFieldName = keyFields.iterator().next().getName();
		} else {
			// if there is only one of the type, fine...otherwise just assume a standard name of
			// "key" to differentiate.
			keyFieldName = "key";
		}

		// run the finder.
		String filterValue = keyFieldName + ".value";
		EntityT result;
		try {
			result = ofy().load().type(entityType).filter(filterValue, keyValue).first().safe();
		} catch (NotFoundException e) {
			throw new EntityNotFoundException(key);
		}
		return RepositoryUtil.finderResult(key, result);
	}

	private Integer limit(Integer length) {
		return Math.min(length, MAX_RESULT_LIMIT);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persit.Repository#countAll()
	 */
	public Integer countAll() {
		return ofy().load().type(entityType).count();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.objectify.Repository#getEntityClassRepresented()
	 */
	@Override
	public Class<EntityT> getEntityClassRepresented() {
		return entityType;
	}

	protected Class<EntityT> entityType() {
		return entityType;
	}

	protected Class<IdentifierT> idType() {
		return idType;
	}

	/**
	 * If the synchronous flag is set to true, force this result to occur immediatly.
	 * 
	 * @See Result
	 */
	private void applySynchronosity(Result<?> result) {
		if (synchronous == true) {
			result.now();
		}
	}

}
