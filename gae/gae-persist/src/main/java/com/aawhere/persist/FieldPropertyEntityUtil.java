/**
 * 
 */
package com.aawhere.persist;

import java.util.Date;

import com.aawhere.field.DictionaryInstanceNotFoundException;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldValueProvider;
import com.aawhere.id.LongIdentifier;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.common.base.Function;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * A specialty situation where an entity is created for a single property. The Entity is named after
 * the property key (application-activityCount) and the key value is given to uniquely identify the
 * Entity from others.
 * 
 * This approach allows a Dictionary to be persisted in many Entities to avoid loading every field
 * in an Entity in order to update a single field. See MapReduce DatastoreOutput for further
 * understanding of necessity.
 * 
 * @author aroller
 * 
 */
public class FieldPropertyEntityUtil {

	/**
	 * Provides an Entity given the property definition and the value of the key.
	 * 
	 * @param key
	 *            an identifier, if it is a {@link LongIdentifier} then long key will be used,
	 *            otherwise the {@link Object#toString()} provides the key value.
	 * @param propertyDefinition
	 *            The custom "kind" of the entity.
	 * @return
	 */
	public static final Entity entity(Object key, FieldDefinition<?> propertyDefinition) {
		// the entity is named after the domain and property being stored
		final String kind = propertyDefinition.getKey().getAbsoluteKey();
		Entity entity;
		if (key instanceof LongIdentifier<?>) {
			entity = new Entity(kind, ((LongIdentifier<?>) key).getValue());
		} else {
			entity = new Entity(kind, key.toString());
		}
		entity.setProperty(BaseEntity.BASE_FIELD.DATE_UPDATED, new Date());
		return entity;
	}

	/**
	 * Provides an Entity which is the kind defined by the property definition absolute key,
	 * identified by the key given and the property is the {@link FieldDefinition#getColumnName()}
	 * name.
	 * 
	 * @param key
	 * @param propertyDefinition
	 * @param propertyValue
	 * @return
	 */
	public static final <T> Entity entity(Object key, FieldDefinition<T> propertyDefinition, Object propertyValue) {
		Entity entity = entity(key, propertyDefinition);
		entity.setProperty(propertyDefinition.getColumnName(), propertyValue);
		return entity;
	}

	/**
	 * Provides just the key for an entity lookup when given the value of the key and the definition
	 * of the property of interest.
	 * 
	 * @param key
	 * @param propertyDefinition
	 * @return
	 */
	public static final Key key(Object key, FieldDefinition<?> propertyDefinition) {
		return entity(key, propertyDefinition).getKey();
	}

	/**
	 * Given the {@link FieldDefinition} of the property and the key value for the record of
	 * interest this will provide the value corresponding to the property.
	 * 
	 * @param key
	 * @param propertyDefinition
	 * @return
	 * @throws EntityNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static final <K, V> V value(Object key, FieldDefinition<V> propertyDefinition)
			throws EntityNotFoundException {
		Key datastoreKey = key(key, propertyDefinition);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Entity entity = datastore.get(datastoreKey);
		Object rawValue = entity.getProperty(propertyDefinition.getColumnName());
		if (propertyDefinition.getType().equals(Integer.class) && rawValue instanceof Number) {
			rawValue = ((Number) rawValue).intValue();
		}

		return (V) rawValue;

	}

	/**
	 * Given the definition upfront this will transform the key into the property value
	 * corresponding with the datastore.
	 * 
	 * @param propertyDefinition
	 * @return the value in the datastore or null if the property value is null or the entity is not
	 *         found.
	 */
	public static <K, V> Function<K, V> function(final FieldDefinition<V> propertyDefinition) {
		return new Function<K, V>() {

			@Override
			public V apply(K input) {
				try {
					return (input != null) ? value(input, propertyDefinition) : null;
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/**
	 * Provides datastore access to those fields with values stored using techniques from
	 * {@link FieldPropertyEntityUtil}.
	 * 
	 * @author aroller
	 * 
	 */
	@Singleton
	public static class FieldPropertyEntityProvider
			implements FieldValueProvider {

		final private FieldDictionaryFactory factory;

		/**
		 * 
		 */
		@Inject
		public FieldPropertyEntityProvider(FieldDictionaryFactory fieldDictionaryFactory) {
			this.factory = fieldDictionaryFactory;
		}

		/*
		 * @see com.aawhere.field.FieldValueProvider#value(com.aawhere.field.FieldKey,
		 * java.lang.Object)
		 */
		@Override
		public <K, V> V value(FieldKey fieldKey, K dictionaryInstanceKey) throws DictionaryInstanceNotFoundException {
			FieldDefinition<V> definition = factory.getDefinition(fieldKey);
			try {
				return FieldPropertyEntityUtil.value(dictionaryInstanceKey, definition);
			} catch (EntityNotFoundException e) {
				throw new DictionaryInstanceNotFoundException(dictionaryInstanceKey, definition.getKey().getDomainKey());
			}
		}

	}
}
