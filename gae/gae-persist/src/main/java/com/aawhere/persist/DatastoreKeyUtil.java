/**
 *
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.google.appengine.api.datastore.Key;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Utilities for working with appengine keys
 * 
 * @author Brian Chapman
 * 
 */
public class DatastoreKeyUtil {

	/**
	 * Get the value of the id from the key. Could be a Long or a String.
	 * 
	 * @param id
	 * @return the id as an Object
	 */
	public static Object getIdValue(Key id) {
		Object idValue;
		Long idLong = id.getId();
		if (idLong == 0) {
			idValue = id.getName();
		} else {
			idValue = id.getId();
		}
		return idValue;
	}

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Key key(I id) {
		return ObjectifyKeyUtils.keyWithParents(id).getRaw();
	}

	/**
	 * When given the raw {@link Key} this will return the identifier complete with all ancestors.
	 * 
	 * @see ObjectifyKeyUtils#id(com.googlecode.objectify.Key, RegisteredEntities)
	 * @param rawKey
	 * @param registeredEntities
	 * @param idType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> I id(Key rawKey, Class<I> idType,
			RegisteredEntities registeredEntities) {
		com.googlecode.objectify.Key<E> key = com.googlecode.objectify.Key.create(rawKey);
		return (I) ObjectifyKeyUtils.id(key, registeredEntities);
	}

	/**
	 * When the idType is known for the key given this will transform the key into the expected
	 * Identifier.
	 * 
	 * @param rawKey
	 * @param idType
	 * @return
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> I id(Key rawKey, Class<I> idType) {
		com.googlecode.objectify.Key<E> key = com.googlecode.objectify.Key.create(rawKey);
		return ObjectifyKeyUtils.id(key, idType);
	}

	/**
	 * @param allCandidateIds
	 * @return
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<Key> keys(Iterable<I> ids) {
		return Iterables.transform(ids, DatastoreKeyUtil.<E, I> keyFunction());
	}

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Function<I, Key> keyFunction() {
		return new Function<I, Key>() {

			@Override
			public Key apply(I input) {
				return (input != null) ? key(input) : null;
			}
		};
	}

}
