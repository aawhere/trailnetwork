/**
 *
 */
package com.aawhere.persist;

import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.id.Identifier;
import com.aawhere.persist.objectify.ObjectifyRepository;

/**
 * Provides a base implementation for processing filters. All repositories implementing
 * FilterRepository should use this object in a composite relationship, hence the name Delegate.
 * 
 * It is encouraged to extend {@link ObjectifyFilterRepository} from your
 * {@link ObjectifyRepository}.
 * 
 * TODO: Eventually it would be nice to have query validation here instead of getting some abstract
 * error or an empty list from Objectify. Actually, implementing classes could be auto-generated,
 * which would be nice ;)/
 * 
 * This class remains abstract due to limitations of Generic Utils grabbing information from the
 * Parameters required at compile time.
 * 
 * @deprecated use {@link ObjectifyFilterRepository}
 * 
 * @author Brian Chapman
 * 
 */
/* @formatter:off */
@Deprecated
public  class ObjectifyFilterRepositoryDelegate<S extends BaseFilterEntities<E>,
	B extends BaseFilterEntities.Builder<B, E, S>,
	E extends BaseEntity<E, I>,
	I extends Identifier<?, E>>
		extends ObjectifyRepository<E, I> implements
		FilterRepository<S, E, I> {
	private ObjectifyFilterRepository<E, I, S, B> delegate;
	private FieldDictionaryFactory fieldDictionaryFactory;
	/* @formatter:on */

	public ObjectifyFilterRepositoryDelegate(ObjectifyFilterRepository<E, I, S, B> objectifyFilterRepository) {
		this.delegate = objectifyFilterRepository;
	}

	public ObjectifyFilterRepositoryDelegate(FieldDictionaryFactory fieldDictionaryFactory) {
		this.fieldDictionaryFactory = fieldDictionaryFactory;
	}

	@Override
	public S filter(Filter filter) {
		return delegate.filter(filter);
	}

	@Override
	public Iterable<E> filterAsIterable(Filter filter) {
		return delegate.filterAsIterable(filter);
	}

	@Override
	public S entities(Iterable<I> ids) {
		return delegate.entities(ids);
	}

	@Override
	public Integer count(Filter filter) {
		return delegate.count(filter);
	}

	@Override
	public Iterable<I> idPaginator(Filter filter) {
		return delegate.idPaginator(filter);
	}

	@Override
	public Iterable<I> ids(Filter filter) {
		return delegate.ids(filter);
	}

	@Override
	public Iterable<E> entityPaginator(Filter filter) {
		return delegate.entityPaginator(filter);
	}

	@Override
	public FieldDictionary dictionary() {
		return delegate.dictionary();
	}

	@Override
	public FieldDictionaryFactory dictionaryFactory() {
		if (this.fieldDictionaryFactory != null) {
			return this.fieldDictionaryFactory;
		}
		return delegate.dictionaryFactory();
	}

}
