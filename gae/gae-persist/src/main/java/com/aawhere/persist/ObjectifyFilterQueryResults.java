/**
 * 
 */
package com.aawhere.persist;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import com.aawhere.collections.CountingPredicate;
import com.aawhere.id.Identifier;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreTimeoutException;
import com.google.appengine.api.datastore.Index;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.google.common.collect.UnmodifiableIterator;

/**
 * Encapsulates the {@link QueryResultIterator} features mostly with a pass through while managing
 * our {@link Filter} by updating it and applying predicates to limit the results.
 * 
 * The results also applies programmatic filters using {@link Iterators#filter(Iterator, Predicate)}
 * if the {@link Filter#hasPredicates()}.
 * 
 * If this{@link #handleTimeouts} is true then a {@link DatastoreTimeoutException} will be caught
 * and the {@link #getCursor()} will be applied to the {@link #getFilter()} and a new
 * {@link #writer} will be provided from the {@link #writerFunction}.
 * 
 * @author aroller
 * 
 */
public class ObjectifyFilterQueryResults<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		implements QueryResultIterator<E> {

	// the originalFilter is required for the initial call. Subsequent calls the originalFilter is
	// provided from #getFilter
	private Filter originalFilter;
	private Function<Filter, ObjectifyFilterQueryWriter> writerFunction;
	private ObjectifyFilterQueryWriter writer;
	private QueryResultIterator<E> iterator;
	private Iterator<E> filteredIterator;
	private int resultsCount = 0;
	private int resultsCountAtPreviousTimeout = 0;
	/**
	 * The number that is filtered out by the predicates if any, but it must be combined with what
	 * is in the {@link #counterBeforeFiltering} since that may be reset during
	 * {@link #setup(Filter)}
	 */
	private int countBeforeFiltering = 0;
	private boolean handleTimeouts = false;
	private CountingPredicate<E> counterBeforeFiltering;
	/** Used to give up if the datastore is timing out too regularly to avoid infinite loop. */
	private boolean finalChance = false;

	/**
	 * 
	 */
	public ObjectifyFilterQueryResults() {

	}

	@Override
	public boolean hasNext() {
		try {
			return filteredIterator.hasNext();
		} catch (DatastoreTimeoutException | IllegalArgumentException e) {
			return handleTimeout(e).hasNext();
		}
	}

	@Override
	public E next() {
		try {
			resultsCount++;
			return filteredIterator.next();
		} catch (DatastoreTimeoutException | IllegalArgumentException e) {
			return handleTimeout(e).next();
		}
	}

	/**
	 * common place to handle the timeout since it may be called from next or hasNext. TN-546
	 * 
	 * @param e
	 * @return
	 */
	private Iterator<E> handleTimeout(RuntimeException e) {

		// java.lang.IllegalArgumentException: The requested query has expired.
		// Please restart it with the last cursor to read more results.
		// the originalFilter provided has the updated cursor.

		final Logger logger = LoggerFactory.getLogger(getClass());
		final Filter filter = getFilter();
		final String message = resultsCount + " entities read when a timeout occurred for " + filter + " at "
				+ getCursor();
		if (this.handleTimeouts) {
			if (this.resultsCount == this.resultsCountAtPreviousTimeout) {
				logger.warning("quitting because we aren't making progress...stuck at results=" + this.resultsCount);
				throw (e);
			}
			this.resultsCountAtPreviousTimeout = this.resultsCount;
			logger.warning(message + "...continuing");
			setup(filter);
			return filteredIterator;
		} else {
			logger.warning(message + "...discontinuing since handleTimeouts=false");
			throw (e);
		}
	}

	@Override
	public void remove() {
		filteredIterator.remove();
	}

	@Override
	public Cursor getCursor() {
		if (iterator == null) {
			return null;
		}
		return iterator.getCursor();
	}

	@Override
	public List<Index> getIndexList() {
		return iterator.getIndexList();
	}

	@SuppressWarnings("unchecked")
	private void setup(Filter filter) {
		this.writer = writerFunction.apply(filter);
		try {
			this.iterator = (QueryResultIterator<E>) this.writer.getQuery().iterator();
			this.finalChance = false;
		} catch (DatastoreTimeoutException | IllegalArgumentException e) {
			// this is recursive...so we must only try once and give up
			if (this.finalChance) {
				// this thing is timing out so we have to give up to avoid an infinite loop
				throw (e);
			} else {
				this.finalChance = true;
				handleTimeout(e);
			}
		}
		// setup programatic filtering if predicates are provided
		if (filter.hasPredicates()) {
			if (this.counterBeforeFiltering != null) {
				this.countBeforeFiltering += this.counterBeforeFiltering.count();
			}
			this.counterBeforeFiltering = CountingPredicate.build();
			UnmodifiableIterator<E> filteredWithCounter = Iterators.filter(this.iterator, counterBeforeFiltering);

			Predicate<E> predicates = FilterUtil.predicates(filter);

			this.filteredIterator = Iterators.filter(filteredWithCounter, predicates);
		} else {
			this.filteredIterator = iterator;
		}
	}

	/**
	 * Provides the updated originalFilter included the latest cursor position and page total based
	 * on the number of items provided in {@link #next()}.
	 * 
	 * @return
	 */
	public Filter getFilter() {
		Filter.Builder clone = Filter.clone(writer.getFilter());
		Cursor cursor = getCursor();
		// We won't have a cursor for some types of Queries such as "IN"
		// queries.
		if (cursor != null) {
			String pageCursor = cursor.toWebSafeString();
			clone.setPageCursor(pageCursor);
		}
		clone.setPageTotal(resultsCount);
		if (this.counterBeforeFiltering != null) {
			// the counter is counting the total so subtract the results from the total and that is
			// the number filtered
			clone.filteredByPredicates((this.countBeforeFiltering + this.counterBeforeFiltering.count())
					- this.resultsCount);
		}
		return clone.filter();
	}

	/**
	 * @return the resultsCount
	 */
	public Integer getResultsCount() {
		return this.resultsCount;
	}

	/**
	 * @return the writer
	 */
	public ObjectifyFilterQueryWriter getWriter() {
		return this.writer;
	}

	/**
	 * Used to construct all instances of ObjectifyFilterQueryResults.
	 */
	public static class Builder<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			extends ObjectBuilder<ObjectifyFilterQueryResults<E, I>> {

		public Builder() {
			super(new ObjectifyFilterQueryResults<E, I>());
		}

		Builder(ObjectifyFilterQueryResults<E, I> mutant) {
			super(mutant);
		}

		public Builder<E, I> writerFunction(Function<Filter, ObjectifyFilterQueryWriter> writer) {
			building.writerFunction = writer;
			return this;
		}

		/** Set this if you wish to automatically handle timeouts. */
		public Builder<E, I> continueAfterTimeout() {
			building.handleTimeouts = true;
			return this;
		}

		public Builder<E, I> filter(Filter filter) {
			building.originalFilter = filter;
			return this;
		}

		@Override
		public ObjectifyFilterQueryResults<E, I> build() {
			ObjectifyFilterQueryResults<E, I> built = super.build();
			built.setup(built.originalFilter);
			return built;
		}

	}// end Builder

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Builder<E, I> create() {
		return new Builder<E, I>();
	}

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Builder<E, I> mutate(
			ObjectifyFilterQueryResults<E, I> mutant) {
		return new Builder<E, I>(mutant);
	}

	/**
	 * An iterable that produces new iterators as needed starting from the beginning regardless if
	 * anything has been iterated.
	 * 
	 * @return
	 */
	public Iterable<E> asIterable() {
		return new Iterable<E>() {

			@Override
			public Iterator<E> iterator() {
				final Builder<E, I> builder = ObjectifyFilterQueryResults.mutate(ObjectifyFilterQueryResults.this);
				// start from the beginning every time so use the original filter
				// build does this for us
				return builder.build();
			}
		};
	}

}
