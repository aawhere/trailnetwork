/**
 * 
 */
package com.aawhere.lang.net;

import java.net.URI;
import java.net.URISyntaxException;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.Translator;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Objectify {@link Translator} for {@link URI}.
 * 
 * @author Brian Chapman
 * 
 */
public class UriTranslatorFactory
		extends ValueTranslatorFactory<URI, String> {

	/**
	 * @param pojoType
	 */
	public UriTranslatorFactory() {
		super(URI.class);
	}

	@Override
	protected ValueTranslator<URI, String> createValueTranslator(TypeKey<URI> tk, CreateContext ctx, Path path) {
		return new ValueTranslator<URI, String>(String.class) {

			@Override
			protected String saveValue(URI value, boolean index, SaveContext ctx, Path path) throws SkipException {
				return value.toString();
			}

			@Override
			protected URI loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				URI uri;
				try {
					uri = new URI(value);
				} catch (URISyntaxException e) {
					throw new RuntimeException(e);
				}
				return uri;
			}
		};
	}

}
