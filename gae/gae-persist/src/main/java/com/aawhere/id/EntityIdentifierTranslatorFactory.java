/**
 *
 */
package com.aawhere.id;

import java.lang.reflect.Type;

import javax.persistence.Entity;

import com.aawhere.persist.DatastoreKeyUtil;
import com.aawhere.persist.EntityUtil;
import com.google.appengine.api.datastore.Key;
import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Converts a {@link StringIdentifier} and {@link LongIdentifier} into a persistable value of
 * {@link String} only for Entities (i.e. those with the {@link Entity} annotation) and only those
 * that are declared as a concrete type.
 * 
 * @see IdentifierTranslatorFactory
 * 
 * @author roller
 * 
 */
@SuppressWarnings("rawtypes")
public class EntityIdentifierTranslatorFactory
		extends ValueTranslatorFactory<Identifier, Key> {

	/**
	 * @param pojoType
	 */
	public EntityIdentifierTranslatorFactory() {
		super(Identifier.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ValueTranslator<Identifier, Key> createValueTranslator(TypeKey<Identifier> tk, CreateContext ctx,
			Path path) {
		Type type = tk.getType();
		ValueTranslator<Identifier, Key> result = null;
		if (IdentifierUtils.isIdentifierType(tk.getType())) {
			final Class<? extends Identifier> idClass = IdentifierUtils.typeToClass(type);

			if (EntityUtil.identifiesEntity((Class<? extends Identifier<?, ?>>) idClass)) {
				result = new ValueTranslator<Identifier, Key>(Key.class) {

					@Override
					protected Identifier loadValue(Key key, LoadContext ctx, Path path) throws SkipException {

						return DatastoreKeyUtil.id(key, idClass);
					}

					@Override
					protected Key saveValue(Identifier id, boolean index, SaveContext ctx, Path path)
							throws SkipException {
						return DatastoreKeyUtil.key(id);
					}
				};
			}
		}
		return result;
	}

}
