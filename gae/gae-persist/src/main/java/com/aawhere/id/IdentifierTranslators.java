/**
 * 
 */
package com.aawhere.id;

import javax.inject.Inject;

import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.RegisteredEntities;
import com.aawhere.persist.RegisteredEntitiesObjectifyFactory;

import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyFactory;

/**
 * @author aroller
 * 
 */
@Singleton
public class IdentifierTranslators {

	@Inject
	public static RegisteredEntities registeredEntitiesSingleton;

	public static void add(ObjectifyFactory fact) {
		add(fact, registeredEntitiesSingleton);
	}

	/**
	 * When the registered entities can be provided beyond injection call this method.
	 */
	public static void add(ObjectifyFactory fact, RegisteredEntities registeredEntities) {
		// registeredEntities also provides non-registered first.
		// fact.getTranslators().add(new EntityIdentifierTranslatorFactory());

		// FIXME:This is a complete hack trying to satisfy melding tests that
		// apparently registered a factory only once in static time

		if (registeredEntitiesSingleton == null) {
			if (registeredEntities != null) {
				registeredEntitiesSingleton = registeredEntities;
			} else {
				LoggerFactory.getLogger(IdentifierTranslators.class)
						.warning("Registered entities not injected so we are creating our own");
				registeredEntitiesSingleton = new RegisteredEntitiesObjectifyFactory();
			}
		}
		// if the type isn't declared this one will find it if registered
		fact.getTranslators().add(new RegisteredEntityIdentifierTranslatorFactory(registeredEntitiesSingleton));

		// this one is long and doesn't matter.
		fact.getTranslators().add(new LongIdentifierTranslatorFactory());
		// order matters. Identifer is the catch all that will provide strings
		fact.getTranslators().add(new IdentifierTranslatorFactory());
	}
}
