/**
 * 
 */
package com.aawhere.id;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * @author aroller
 * 
 */
@SuppressWarnings("rawtypes")
public class LongIdentifierTranslatorFactory
		extends ValueTranslatorFactory<LongIdentifier, Long> {

	/**
	 * @param pojoType
	 */
	protected LongIdentifierTranslatorFactory() {
		super(LongIdentifier.class);
	}

	@Override
	protected ValueTranslator<LongIdentifier, Long> createValueTranslator(TypeKey<LongIdentifier> tk,
			CreateContext ctx, Path path) {
		@SuppressWarnings("unchecked")
		final Class<LongIdentifier<?>> idClass = (Class<LongIdentifier<?>>) tk.getType();
		if (LongIdentifier.class.isAssignableFrom(idClass)) {

			return new ValueTranslator<LongIdentifier, Long>(Long.class) {

				@Override
				protected LongIdentifier loadValue(Long value, LoadContext ctx, Path path) throws SkipException {
					return IdentifierUtils.idFrom(value, idClass);
				}

				@Override
				protected Long saveValue(LongIdentifier value, boolean index, SaveContext ctx, Path path)
						throws SkipException {
					return value.getValue();
				}
			};
		} else {
			return null;
		}
	}

}
