/**
 *
 */
package com.aawhere.id;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Converts a {@link Identifier} and {@link LongIdentifier} into a persistable value of
 * {@link String}.
 * 
 * This handles any Identifier who's {@link Identifier#getKind()} is not an Entity.
 * 
 * @see EntityIdentifierTranslatorFactory
 * @see LongIdentifierTranslatorFactory
 * 
 * @author roller
 * 
 */
@SuppressWarnings("rawtypes")
public class IdentifierTranslatorFactory
		extends ValueTranslatorFactory<Identifier, String> {

	/**
	 * @param pojoType
	 */
	public IdentifierTranslatorFactory() {
		super(Identifier.class);
	}

	@Override
	protected ValueTranslator<Identifier, String> createValueTranslator(final TypeKey<Identifier> tk,
			CreateContext ctx, Path path) {
		return new ValueTranslator<Identifier, String>(String.class) {

			@Override
			protected String saveValue(Identifier value, boolean index, SaveContext ctx, Path path)
					throws SkipException {
				return translate(value);
			}

			@Override
			protected Identifier loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				@SuppressWarnings("unchecked")
				Class<Identifier> idClass = (Class<Identifier>) tk.getType();
				return IdentifierUtils.idFrom(value, idClass);
			}
		};
	}

	/**
	 * Translates the id into a string value.
	 * 
	 * @param value
	 * @return
	 */
	static String translate(Identifier value) {
		// TODO: this should output the entire key..parent and all?
		return value.getValue().toString();
	}
}
