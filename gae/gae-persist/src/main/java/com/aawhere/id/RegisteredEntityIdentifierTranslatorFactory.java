/**
 *
 */
package com.aawhere.id;

import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.inject.Inject;

import com.aawhere.lang.Assertion;
import com.aawhere.persist.DatastoreKeyUtil;
import com.aawhere.persist.EntityUtil;
import com.aawhere.persist.RegisteredEntities;
import com.google.appengine.api.datastore.Key;
import com.google.inject.Singleton;
import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * When an Entity declares a key as Identifier the translator factory cannot load it because the
 * type is not exposed. This will use RegisteredEntities to retrieve the identifier.
 * 
 * @see IdentifierTranslatorFactory
 * 
 * @author roller
 * 
 */
@SuppressWarnings("rawtypes")
@Singleton
public class RegisteredEntityIdentifierTranslatorFactory
		extends ValueTranslatorFactory<Identifier, Key> {

	final private RegisteredEntities registeredEntities;
	/** simplifies registration and handling if we handle this one too. */
	final private EntityIdentifierTranslatorFactory entityIdentifierTranslatorFactory;

	/**
	 * @param pojoType
	 */
	@Inject
	public RegisteredEntityIdentifierTranslatorFactory(RegisteredEntities registeredEntities) {
		super(Identifier.class);

		this.registeredEntities = registeredEntities;
		Assertion.assertNotNull(this.registeredEntities);
		this.entityIdentifierTranslatorFactory = new EntityIdentifierTranslatorFactory();
	}

	/**
	 * @param type
	 * @return
	 */
	private boolean handleIt(Type type) {
		boolean handleIt;

		// parmaeterized type may expose a class in it's raw type
		if (type instanceof ParameterizedType) {
			type = ((ParameterizedType) type).getRawType();
		}
		// if we can inspect if it's a class then ensure it is an entity
		if (type instanceof Class) {
			Class idType = IdentifierUtils.typeToClass(type);
			if (Modifier.isAbstract(idType.getModifiers())) {
				handleIt = true;
			} else {

				if (EntityUtil.identifiesEntity(idType)) {
					throw new IllegalArgumentException(idType + " should be handled by "
							+ entityIdentifierTranslatorFactory);
				} else {
					// not an entity so a Key can't be created. let it
					// convert to a string with IdentifierTranslatorFactory
					handleIt = false;
				}
			}
		} else {
			handleIt = true;
		}
		return handleIt;
	}

	@Override
	protected ValueTranslator<Identifier, Key> createValueTranslator(TypeKey<Identifier> tk, CreateContext ctx,
			Path path) {
		final Type type = tk.getType();
		// as the concrete handler first
		ValueTranslator<Identifier, Key> result = entityIdentifierTranslatorFactory
				.createValueTranslator(tk, ctx, path);

		if (result == null && IdentifierUtils.isIdentifierType(type)) {

			if (handleIt(type)) {
				// we can't yet know if it identifies an entity...
				// if (EntityUtil.identifiesEntity(idClass)) {
				// we can't yet check if the type is known since the type may be
				// generic
				// we must have the actual key to know

				result = new ValueTranslator<Identifier, Key>(Key.class) {

					@SuppressWarnings("unchecked")
					@Override
					protected Identifier loadValue(Key key, LoadContext ctx, Path path) throws SkipException {
						Class<? extends Identifier> idTypeFromKind = registeredEntities.idType(key.getKind());
						return DatastoreKeyUtil.id(key, idTypeFromKind, registeredEntities);
					}

					@SuppressWarnings("unchecked")
					@Override
					protected Key saveValue(Identifier id, boolean index, SaveContext ctx, Path path)
							throws SkipException {
						if (!EntityUtil.identifiesEntity(id.getClass())) {
							throw new IllegalArgumentException(
									id.getClass()
											+ " does not represent an entity and you are searching against a property that does not delcare that class directly, but it's parent class...you must declare the type explicity in your pojo.  ");
						}
						return DatastoreKeyUtil.key(id);
					}
				};
			}
		}

		return result;
	}

}
