package com.googlecode.objectify.impl;

import com.google.appengine.api.datastore.Query;

/**
 * A complete hack into the {@link QueryImpl} to retrieve the native {@link Query} for the
 * datastore. Made possible by some bad coding! He has a private accessor method, but a package
 * available field. Classic!
 * 
 * @author aroller
 * 
 */
public class DatastoreQueryAccessor {

	public static Query query(com.googlecode.objectify.cmd.Query<?> objectifyQuery) {
		if (objectifyQuery instanceof QueryImpl<?>) {
			return ((QueryImpl<?>) objectifyQuery).actual;
		} else {
			throw new IllegalArgumentException(objectifyQuery.getClass() + " is not supported for this operation.");
		}
	}
}
