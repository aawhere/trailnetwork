/**
 *
 */
package com.googlecode.objectify.impl.conv.joda;

import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.impl.translate.Translators;

/**
 * <p>
 * A convenient static method that adds all the common converters to your factory's conversions.
 * </p>
 * 
 * @author Brian Chapman
 */
public class JodaTranslators {
	public static void add(ObjectifyFactory fact) {
		Translators translators = fact.getTranslators();
		translators.add(new ChronologyTranslatorFactory());
		translators.add(new DateTimeTranslatorFactory());
		translators.add(new DurationTranslatorFactory());
		translators.add(new IntervalTranslatorFactory());
	}
}