package com.googlecode.objectify.impl.conv.joda;

import java.util.Date;

import org.joda.time.DateTime;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

public class DateTimeTranslatorFactory
		extends ValueTranslatorFactory<DateTime, Date> {

	protected DateTimeTranslatorFactory() {
		super(DateTime.class);
	}

	@Override
	protected ValueTranslator<DateTime, Date> createValueTranslator(TypeKey<DateTime> tk, CreateContext ctx, Path path) {
		return new ValueTranslator<DateTime, Date>(Date.class) {

			@Override
			protected Date saveValue(DateTime value, boolean index, SaveContext ctx, Path path) throws SkipException {
				return value.toDate();
			}

			@Override
			protected DateTime loadValue(Date value, LoadContext ctx, Path path) throws SkipException {
				return new DateTime(value);
			}
		};
	}

}
