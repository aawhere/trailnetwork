package com.googlecode.objectify.impl.conv.joda;

import org.joda.time.Interval;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Convert {@link Interval} to an ArrayList containing the start instance in mills, the end instance
 * in mills, and the {@see Chronology} to store in datastore. Note that Chronology has its own
 * converter
 * 
 * @author Brian Chapman
 * 
 */
public class IntervalTranslatorFactory
		extends ValueTranslatorFactory<Interval, String> {

	/** */
	public IntervalTranslatorFactory() {
		super(Interval.class);
	}

	@Override
	protected ValueTranslator<Interval, String>
			createValueTranslator(TypeKey<Interval> tk, CreateContext ctx, Path path) {
		return new ValueTranslator<Interval, String>(String.class) {

			@Override
			protected String saveValue(Interval value, boolean index, SaveContext ctx, Path path) throws SkipException {
				return value.toString();
			}

			@Override
			protected Interval loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				return Interval.parse(value);
			}
		};
	}
}
