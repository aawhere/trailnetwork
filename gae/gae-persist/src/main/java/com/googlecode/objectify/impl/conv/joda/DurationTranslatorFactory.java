package com.googlecode.objectify.impl.conv.joda;

import org.joda.time.Duration;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Convert from {@link Duration} to mills (double) to store in datastore.
 * 
 * @author Brian Chapman
 * 
 */
public class DurationTranslatorFactory
		extends ValueTranslatorFactory<Duration, Long> {

	protected DurationTranslatorFactory() {
		super(Duration.class);
	}

	@Override
	protected ValueTranslator<Duration, Long> createValueTranslator(TypeKey<Duration> tk, CreateContext ctx, Path path) {
		return new ValueTranslator<Duration, Long>(Long.class) {

			@Override
			protected Long saveValue(Duration value, boolean index, SaveContext ctx, Path path) throws SkipException {
				return value.getMillis();
			}

			@Override
			protected Duration loadValue(Long value, LoadContext ctx, Path path) throws SkipException {
				return new Duration(value);
			}
		};
	}

}
