package com.googlecode.objectify.impl.conv.joda;

import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.ISOChronology;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Convert {@link Chronology} to a String representing the class. For now it always restores a new
 * ISOChronology, which is the default for Joda Time.
 * 
 * @author Brian Chapman
 * 
 */
public class ChronologyTranslatorFactory
		extends ValueTranslatorFactory<Chronology, String> {

	protected ChronologyTranslatorFactory() {
		super(Chronology.class);
	}

	@Override
	protected ValueTranslator<Chronology, String> createValueTranslator(TypeKey<Chronology> tk, CreateContext ctx,
			Path path) {
		return new ValueTranslator<Chronology, String>(String.class) {

			@Override
			protected String saveValue(Chronology value, boolean index, SaveContext ctx, Path path)
					throws SkipException {
				String timeZoneId = value.getZone().getID();
				return timeZoneId;
			}

			@Override
			protected Chronology loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				DateTimeZone timeZone = DateTimeZone.forID(value);
				return ISOChronology.getInstance(timeZone);
			}
		};
	}
}
