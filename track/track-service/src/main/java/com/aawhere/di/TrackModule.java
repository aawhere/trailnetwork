package com.aawhere.di;

import com.aawhere.track.TrackService;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

/**
 * Guice DI for Track
 * 
 * @author Brian Chapman
 * 
 */
public class TrackModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(TrackService.class).in(Singleton.class);
	}

}
