/**
 *
 */
package com.aawhere.track;

import java.util.Map;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.cache.Cache;
import com.aawhere.cache.Cache.Key;
import com.aawhere.cache.CacheService;
import com.aawhere.doc.Document;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.story.TrackSummaryUserStories;
import com.aawhere.track.story.TrackUserStories;
import com.aawhere.util.rb.StatusMessages;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.Inject;

/**
 * Used to for {@link Track}s and related objects.
 * 
 * Purposely stored in the same package as Track so it may take advantage of package level
 * accessibility.
 * 
 * @author Aaron Roller
 * 
 */
public class TrackService {

	private DocumentService documentService;
	private CacheService cacheService;

	/**
	 * 
	 * @param trackSummaryRepository
	 */
	@Inject
	public TrackService(DocumentService documentService, CacheService cacheService) {
		this.documentService = documentService;
		this.cacheService = cacheService;
	}

	/**
	 * Retrieve a track.
	 * 
	 * @param documentId
	 * @return
	 * @throws EntityNotFoundException
	 * @throws TrackIncapableDocumentException
	 *             when the document type does not allow for Tracks
	 */
	@Nonnull
	public Track getTrack(@Nonnull ArchivedDocumentId documentId) throws EntityNotFoundException,
			TrackIncapableDocumentException {
		String cacheKey = createCacheKey(documentId);
		return getCachedTrack(cacheKey, documentId);
	}

	/**
	 * Uses the cache service and some transformer magic to convert the document ids into tracks
	 * using the most efficient method possible. It attempts to retrieve the track from the cache
	 * first, but if not then all those tracks missing the service bulk loads the archived documents
	 * and transforms them into tracks. The result is an asynchronous map that will block when
	 * called if it is not already ready.
	 * 
	 * If the archived document is not a track then null will be returned.
	 * 
	 * @param documentIds
	 * @return
	 */
	public Map<ArchivedDocumentId, Track> getTracks(Iterable<ArchivedDocumentId> documentIds) {
		// bulk loads those ids provided
		Function<Iterable<ArchivedDocumentId>, Map<ArchivedDocumentId, ArchivedDocument>> idsFunction = this.documentService
				.idsFunction();
		// transforms the documents into tracks
		Function<Map<ArchivedDocumentId, ArchivedDocument>, Map<ArchivedDocumentId, Track>> tracksTransformer = tracksTransformerFunction();
		// archvied document ids into tracks to match the required policy of the cache service
		Function<Iterable<ArchivedDocumentId>, Map<ArchivedDocumentId, Track>> tracksProvider = Functions
				.compose(tracksTransformer, idsFunction);
		return cacheService.getAll(Sets.newHashSet(documentIds), cacheKeyFunction(), tracksProvider);
	}

	private Function<Map<ArchivedDocumentId, ArchivedDocument>, Map<ArchivedDocumentId, Track>>
			tracksTransformerFunction() {
		return new Function<Map<ArchivedDocumentId, ArchivedDocument>, Map<ArchivedDocumentId, Track>>() {

			@Override
			public Map<ArchivedDocumentId, Track> apply(Map<ArchivedDocumentId, ArchivedDocument> input) {
				return Maps.transformValues(input, TrackUtil.trackFromArchivedDocumentFunction());
			}
		};
	}

	/**
	 * The cache key can't be the archived doc...because that is already cached using the archived
	 * doc id.
	 * 
	 * @param documentId
	 * @return
	 */
	private static String createCacheKey(ArchivedDocumentId documentId) {
		return StringUtils.join("TRACKS" + "-" + documentId.toString());
	}

	private static Function<ArchivedDocumentId, String> cacheKeyFunction() {
		return new Function<ArchivedDocumentId, String>() {

			@Override
			public String apply(ArchivedDocumentId input) {
				return createCacheKey(input);
			}
		};
	}

	Track getCachedTrack(Pair<String, ArchivedDocumentId> cacheKey) throws EntityNotFoundException,
			TrackIncapableDocumentException {
		return getCachedTrack(cacheKey.toString(), cacheKey.getRight());
	}

	/**
	 * Custom method specializing in caching the track using the a custom id. Previously it used
	 * {@link ArchivedDocumentId} which should be reserved for the document itself.
	 * 
	 * @param cacheKey
	 * @return
	 * @throws EntityNotFoundException
	 * @throws TrackIncapableDocumentException
	 */
	@Cache
	Track getCachedTrack(@Key String cacheKey, ArchivedDocumentId documentId) throws EntityNotFoundException,
			TrackIncapableDocumentException {
		ArchivedDocument archived = documentService.getArchivedDocument(documentId);
		return TrackUtil.track(archived);
	}

	/**
	 * Runs the {@link Track} through the {@link TrackProcessor} to create a {@link TrackSummary}
	 * and a {@link TrackRepresentation}. Both objects are persisted.
	 * 
	 * This method is more efficient than creating {@link TrackSummary} and
	 * {@link TrackRepresentation} separately since the Track is parsed once.
	 * 
	 * @deprecated encouraging use of
	 *             {@link #createAndPersistTrackSummary(ArchivedDocumentId, Track)}
	 * 
	 * @param documentId
	 * @param track
	 */

	@Deprecated
	public StatusMessages
			createAndPersistTrackSummaryAndTrackRepresentation(ArchivedDocumentId documentId, Track track) {
		return createAndPersistTrackSummary(documentId, track).getRight();
	}

	/**
	 * Runs the {@link Track} through the {@link TrackProcessor} to create a {@link TrackSummary}
	 * and a {@link TrackRepresentation}. Both objects are persisted.
	 * 
	 * This method is more efficient than creating {@link TrackSummary} and
	 * {@link TrackRepresentation} separately since the Track is parsed once.
	 * 
	 * 
	 * @param documentId
	 * @param track
	 * @return trackSummary created and messages related to the creation.
	 */
	public Pair<TrackSummary, StatusMessages> createAndPersistTrackSummary(ArchivedDocumentId documentId, Track track) {

		// this assumes there are not multiple calls to this method in the workflow
		this.cacheService.put(createCacheKey(documentId), track);

		TrackUserStories.Builder storiesBuilder = new TrackUserStories.Builder(track);
		storiesBuilder.addStandardFilters();
		TrackSummaryUserStories tsStory = storiesBuilder.setUserStory(new TrackSummaryUserStories.Builder()
				.setDocumentId(documentId));
		TrackUserStories stories = storiesBuilder.build();
		TrackSummary ts = tsStory.getResult();

		return Pair.of(ts, stories.getMessages());
	}

	/**
	 * @return the documentService
	 */
	public DocumentService getDocumentService() {
		return this.documentService;
	}

}
