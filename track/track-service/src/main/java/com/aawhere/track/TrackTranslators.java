/**
 *
 */
package com.aawhere.track;

import com.googlecode.objectify.ObjectifyFactory;

/**
 * <p>
 * A convenient static method that adds all the track related translators to your factory's
 * conversions.
 * </p>
 * 
 * <p>
 * {@code JodaTimeConverters.add(ObjectifyService.factory());}
 * 
 * @author Brian Chapman
 */
public class TrackTranslators {
	public static void add(ObjectifyFactory fact) {
	}
}