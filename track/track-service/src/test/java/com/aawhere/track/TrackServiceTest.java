package com.aawhere.track;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.cache.CacheTestUtil;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.document.DocumentServiceTestUtil;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.ServiceTest;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

@Category(ServiceTest.class)
public class TrackServiceTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private TrackServiceTestUtil trackTestUtil;
	private DocumentServiceTestUtil documentTestUtil;
	private TrackService trackService;

	@Before
	public void setUp() throws Exception {
		helper.setUp();
		CacheTestUtil.setUpCachManager();
		trackTestUtil = TrackServiceTestUtil.create().build();
		documentTestUtil = trackTestUtil.getDocumentTestUtil();
		trackService = trackTestUtil.getTrackService();
	}

	@After
	public void tearDown() throws Exception {
		helper.tearDown();
	}

	public static TrackServiceTest getInstance() throws Exception {
		TrackServiceTest test = new TrackServiceTest();
		test.setUp();
		return test;
	}

	@Test
	public void testGetTrack() throws EntityNotFoundException, DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException,
			TrackIncapableDocumentException {

		ArchivedDocument doc = documentTestUtil.createPersistedArchivedDocument();

		Track track = trackService.getTrack(doc.getId());
		assertNotNull(track);

	}

	@Test(expected = EntityNotFoundException.class)
	public void testGetTrack_EntityNotFoundException() throws EntityNotFoundException, TrackIncapableDocumentException {
		trackService.getTrack(new ArchivedDocumentId(TestUtil.generateRandomId()));
	}

	@Test(expected = TrackIncapableDocumentException.class)
	public void testGetTrack_TrackIncapableDocumentException() throws EntityNotFoundException,
			TrackIncapableDocumentException, DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {

		// create a limited document service that doesn't know GPX.
		TrackServiceTestUtil trackServiceTestUtil = TrackServiceTestUtil.create().build();
		TrackService textTrackService = trackServiceTestUtil.getTrackService();
		DocumentService textDocumentService = trackServiceTestUtil.getDocumentTestUtil().getDocumentService();

		ArchivedDocument archivedDocument = textDocumentService.createDocument("test", DocumentFormatKey.TEXT);

		textTrackService.getTrack(archivedDocument.getId());
	}

	@Test(expected = EntityNotFoundException.class)
	public void testGetTrackSummaryNotFound() throws EntityNotFoundException, TrackIncapableDocumentException {
		trackService.getTrack(new ArchivedDocumentId(-3l));
	}

}
