/**
 *
 */
package com.aawhere.track;

import java.util.Set;

import com.aawhere.cache.CacheService;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer;
import com.aawhere.document.DocumentServiceTestUtil;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;

/**
 * Utility providing access to singleton instances. All direct dependencies to TrackService are made
 * available.
 * 
 * @author Brian Chapman
 * 
 */
public class TrackServiceTestUtil {

	private TrackService trackService;
	private DocumentServiceTestUtil documentTestUtil;

	/**
	 * @return the trackService
	 */
	public TrackService getTrackService() {
		return this.trackService;
	}

	/**
	 * @return the documentTestUtil
	 */
	public DocumentServiceTestUtil getDocumentTestUtil() {
		return this.documentTestUtil;
	}

	public TrackSummary createPersistedTrackSummary() {
		return createPersistedTrackSummary(documentTestUtil.createPersistedArchivedDocument());
	}

	public TrackSummary createPersistedTrackSummaryReplacement(GeoCellBoundingBox box) {
		TrackSummary ts = TrackSummaryTestUtil.getSampleTrackSummary(box);
		ts = TrackSummary.mutate(ts).setDocumentId(documentTestUtil.createPersistedArchivedDocument().getId()).build();
		return ts;
	}

	public TrackSummary createPersistedTrackSummary(GeoCellBoundingBox box) {
		return createPersistedTrackSummary(documentTestUtil.createPersistedArchivedDocument(), box);
	}

	public TrackSummary createPersistedTrackSummary(ArchivedDocument archivedDocument) {
		return createPersistedTrackSummary(	documentTestUtil.createPersistedArchivedDocument(),
											BoundingBoxTestUtils.createRandomGeoCellBoundingBox());
	}

	/**
	 * @deprecated this overwrites the cells provided by track summary. it should delegate cell
	 *             creation to the track summary itself.
	 * 
	 * @param archivedDocument
	 * @param box
	 * @return
	 */
	@Deprecated
	public TrackSummary createPersistedTrackSummary(ArchivedDocument archivedDocument, GeoCellBoundingBox box) {
		TrackSummary ts = TrackSummaryTestUtil.getSampleTrackSummary();
		Set<GeoCell> geoCells = GeoCellUtil.translateToAllResolutions(box.getGeoCells().getAll());

		ts = TrackSummary.mutate(ts).setDocumentId(archivedDocument.getId())
				.setSpatialBuffer(GeoCells.create().addAll(geoCells).build()).build();
		return ts;
	}

	// Use TrackSummaryUnitTest instead
	@Deprecated
	public TrackSummary createTrackSummary() {
		return TrackSummaryTestUtil.getSampleTrackSummary();
	}

	/**
	 * Used to construct all instances of TrackServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<TrackServiceTestUtil> {

		private DocumentServiceTestUtil.Builder documentServiceTestUtilBuilder;

		public Builder() {
			super(new TrackServiceTestUtil());
			documentServiceTestUtilBuilder = DocumentServiceTestUtil.create();

		}

		/**
		 * Use this to configure the doucment service.
		 * 
		 * @return the documentServiceTestUilBuilder
		 */
		public DocumentServiceTestUtil.Builder getDocumentServiceTestUtilBuilder() {
			return this.documentServiceTestUtilBuilder;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public TrackServiceTestUtil build() {

			FieldAnnotationDictionaryFactory annotationDictionaryFactory = documentServiceTestUtilBuilder
					.getPersonServiceTestUtilBuilder().getAnnotationDictionaryFactory();
			annotationDictionaryFactory.getDictionary(TrackSummary.class);
			documentServiceTestUtilBuilder.getDocumentFactoryBuilder()
					.register(new GpxDocumentProducer.Builder().build());
			building.documentTestUtil = documentServiceTestUtilBuilder.build();
			building.trackService = new TrackService(building.documentTestUtil.getDocumentService(), new CacheService());

			return super.build();
		}

	}// end Builder

	/** Use {@link Builder} to construct TrackServiceTestUtil */
	private TrackServiceTestUtil() {
	}

	public static Builder create() {
		return new Builder();
	}

}
