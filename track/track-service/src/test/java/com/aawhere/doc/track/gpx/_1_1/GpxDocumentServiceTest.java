/**
 *
 */
package com.aawhere.doc.track.gpx._1_1;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentObjectifyRepository;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.doc.track.gpx._1._1.GpxDocumentProducer;
import com.aawhere.gpx._1._1.GpxUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * Tests the GPX document capaibilities of {@link DocumentService}.
 * 
 * TODO: Determine a better location than track-service module since there isn't much related to
 * Track in here right now.
 * 
 * @author Aaron Roller
 * 
 */
public class GpxDocumentServiceTest {

	private DocumentService documentService;
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpServices() {
		helper.setUp();
		DocumentFactory factory = new DocumentFactory.Builder().register(new GpxDocumentProducer.Builder().build())
				.build();
		ArchivedDocumentObjectifyRepository archivedDocumentRepository = new ArchivedDocumentObjectifyRepository();
		documentService = new DocumentService(archivedDocumentRepository, factory);
	}

	@After
	public void tearDownServices() {
		helper.tearDown();
	}

	@Test
	public void testCreateDocumentFromString() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException, EntityNotFoundException {
		ArchivedDocument doc = createArchivedDocument();
		ArchivedDocument archivedDocument = documentService.getArchivedDocument(doc.getId());
		assertNotNull(archivedDocument);

	}

	@Test
	public void testDocumentContentsDiscovery() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException, EntityNotFoundException {
		ArchivedDocument archivedDocument = createArchivedDocument();
		Document doc = archivedDocument.getDocument();
		assertEquals(DocumentFormatKey.GPX_1_1, doc.getDocumentFormat());

	}

	@Test(expected = DocumentContentsNotRecognizedException.class)
	public void testContentsNotRecognized() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		documentService.createDocument(TestUtil.generateRandomString(), DocumentFormatKey.GPX_1_1);
	}

	private ArchivedDocument createArchivedDocument() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		ArchivedDocument doc = documentService.createDocument(	GpxUnitTest.GARMIN_CONNECT_COMPLETE,
																DocumentFormatKey.GPX_1_1);
		assertNotNull(doc.getId());
		return doc;
	}
}
