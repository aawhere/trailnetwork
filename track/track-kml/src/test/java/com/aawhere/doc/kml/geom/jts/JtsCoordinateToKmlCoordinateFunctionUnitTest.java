/**
 * 
 */
package com.aawhere.doc.kml.geom.jts;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.doc.kml.geom.jts.JtsCoordinateToKmlCoordinateFunction;
import com.aawhere.jts.geom.JtsTestUtil;

/**
 * @author aroller
 * 
 */
public class JtsCoordinateToKmlCoordinateFunctionUnitTest {

	@Test
	public void test2d() {
		JtsCoordinateToKmlCoordinateFunction function = JtsCoordinateToKmlCoordinateFunction.create();
		String kmlCoordinate = function.apply(JtsTestUtil.CENTER);
		assertEquals("0.0,0.0", kmlCoordinate);
	}

}
