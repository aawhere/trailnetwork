/**
 * 
 */
package com.aawhere.doc.kml.geom.jts;

import static org.junit.Assert.*;

import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractGeometryType;
import net.opengis.kml.v_2_2_0.LineStringType;
import net.opengis.kml.v_2_2_0.MultiGeometryType;
import net.opengis.kml.v_2_2_0.PolygonType;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.jts.geom.JtsTestUtil;
import com.aawhere.jts.geom.JtsUtil;
import com.aawhere.measure.JtsMeasureUtil;
import com.aawhere.test.TestUtil;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;

/**
 * @author aroller
 * 
 */
public class JtsGeometryToKmlFunctionUnitTest {
	private JtsGeometryToKmlGeometryFunction function;

	@Before
	public void setUp() {
		function = JtsGeometryToKmlGeometryFunction.create().build();
	}

	@Test
	public void testLineString() {
		LineString jts = JtsTestUtil.getHorizontalCenterLine();

		Class<LineStringType> expectedResultType = LineStringType.class;
		LineStringType kmlLineString = assertApply(jts, expectedResultType);
		assertEquals("Coordinates did not get set properly", jts.getCoordinates().length, kmlLineString
				.getCoordinates().size());

	}

	/**
	 * @param jts
	 * @param expectedResultType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <J extends Geometry, K extends AbstractGeometryType> K assertApply(J jts, Class<K> expectedResultType) {
		AbstractGeometryType kml = function.apply(jts);
		TestUtil.assertInstanceOf(expectedResultType, kml);
		return (K) kml;
	}

	@Test
	public void testPolygon() {
		Geometry jts = JtsTestUtil.createLeftLoopPolygon();
		assertApply(jts, PolygonType.class);
	}

	@Test
	public void testMultiLineString() {
		MultiLineString jts = JtsTestUtil.getOutAndBack();
		Class<LineStringType> expectedInnerKmlType = LineStringType.class;
		assertMultiGeometry(jts, expectedInnerKmlType);
	}

	@Test
	public void testSingleMultiLineString() {
		MultiLineString jts = JtsUtil.multiLineString(new GeometryFactory(), JtsTestUtil.getLeftLoopStartingUp());
		Class<LineStringType> expectedInnerKmlType = LineStringType.class;
		assertMultiGeometry(jts, expectedInnerKmlType);
	}

	@Test
	public void testMultiPolygon() {
		MultiPolygon jts = JtsTestUtil.createMultiPolygon();
		assertMultiGeometry(jts, PolygonType.class);
	}

	/**
	 * @param jts
	 * @param expectedInnerKmlType
	 */
	private void assertMultiGeometry(Geometry jts, Class<? extends AbstractGeometryType> expectedInnerKmlType) {
		MultiGeometryType kml = assertApply(jts, MultiGeometryType.class);
		List<JAXBElement<? extends AbstractGeometryType>> kmlGeometries = kml.getAbstractGeometryGroup();
		assertEquals("jts geom count differn than kml", jts.getNumGeometries(), kmlGeometries.size());
		for (JAXBElement<? extends AbstractGeometryType> jaxbElement : kmlGeometries) {
			TestUtil.assertInstanceOf(expectedInnerKmlType, jaxbElement.getValue());
		}
	}
}
