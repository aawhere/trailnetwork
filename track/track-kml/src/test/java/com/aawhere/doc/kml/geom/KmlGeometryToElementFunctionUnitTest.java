/**
 * 
 */
package com.aawhere.doc.kml.geom;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractGeometryType;
import net.opengis.kml.v_2_2_0.LineStringType;
import net.opengis.kml.v_2_2_0.MultiGeometryType;
import net.opengis.kml.v_2_2_0.PolygonType;

import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class KmlGeometryToElementFunctionUnitTest {

	private KmlGeometryToElementFunction function = KmlGeometryToElementFunction.create().build();

	@Test
	public void testLineString() {
		LineStringType kml = new LineStringType();
		assertElement(kml);
	}

	/**
	 * @param kml
	 */
	private void assertElement(AbstractGeometryType kml) {
		JAXBElement<AbstractGeometryType> element = function.apply(kml);
		assertSame("the kml didn't get assigned into the element", kml, element.getValue());
	}

	@Test
	public void testPolygon() {

		assertElement(new PolygonType());
	}

	@Test
	public void testMultiGeometry() {

		assertElement(new MultiGeometryType());
	}

}
