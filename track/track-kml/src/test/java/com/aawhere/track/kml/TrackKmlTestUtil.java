/**
 * 
 */
package com.aawhere.track.kml;

import net.opengis.kml.v_2_2_0.MultiGeometryType;

import com.aawhere.track.ExampleTracks;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.AutomaticTrackProcessor.Builder;

/**
 * Useful for testing {@link TrackKml}.
 * 
 * @author aroller
 * 
 */
public class TrackKmlTestUtil {

	/**
	 * provides a {@link TrackKml} using {@link ExampleTracks#LINE}
	 * 
	 * @return
	 */
	public static MultiGeometryType line() {
		Builder builder = AutomaticTrackProcessor.create(ExampleTracks.LINE);
		KmlLineStringTrackpointListener listener = KmlLineStringTrackpointListener.create()
				.registerWithProcessor(builder).build();
		builder.build();
		return listener.getLineStrings();
	}
}
