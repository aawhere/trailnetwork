/**
 * 
 */
package com.aawhere.track.kml;

import static org.junit.Assert.*;
import net.opengis.kml.v_2_2_0.TimeSpanType;

import org.junit.Test;

import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.Trackpoint;

/**
 * @author aroller
 * 
 */
public class KmlTrackUtilUnitTest {

	/**
	 * Test method for
	 * {@link com.aawhere.track.kml.KmlTrackUtil#toCoordinate(com.aawhere.track.Trackpoint)}.
	 */
	@Test
	public void testToCoordinate() {
		Trackpoint expected = ExampleTracks.P1;
		String coordinate = KmlTrackUtil.toCoordinate(expected);
		TestUtil.assertContains(coordinate, String.valueOf(expected.getLocation().getLatitude().getInDecimalDegrees()));
		TestUtil.assertContains(coordinate, String.valueOf(expected.getLocation().getLongitude().getInDecimalDegrees()));
	}

	/**
	 * Test method for
	 * {@link com.aawhere.track.kml.KmlTrackUtil#timeSpan(com.aawhere.track.TrackSegment, java.lang.Boolean, java.lang.Boolean)}
	 * .
	 */
	@Test
	public void testTimeSpanForSegmentStartOnly() {
		boolean includeStart = true;
		boolean includeEnd = false;
		TrackSegment segment = ExampleTracks.S1;
		assertTimeSpanForSegment(includeStart, includeEnd, segment);
	}

	@Test
	public void testTimeSpanForSegmentEndOnly() {
		assertTimeSpanForSegment(false, true, ExampleTracks.S1);
	}

	@Test
	public void testTimeSpanForSegment() {
		assertTimeSpanForSegment(true, true, ExampleTracks.S1);
	}

	/**
	 * @param includeStart
	 * @param includeEnd
	 * @param segment
	 */
	private void assertTimeSpanForSegment(boolean includeStart, boolean includeEnd, TrackSegment segment) {
		TimeSpanType spanFor = KmlTrackUtil.timeSpan(segment, includeStart, includeEnd);
		if (includeStart) {
			JodaTestUtil.assertParseableXmlDate("begin didn't get set properly", spanFor.getBegin());
		} else {

			assertNull("start wasn't requested", spanFor.getBegin());
		}
		if (includeEnd) {
			JodaTestUtil.assertParseableXmlDate("end didn't get set properly", spanFor.getEnd());
		} else {
			assertNull("end wasn't requested", spanFor.getEnd());
		}
	}

}
