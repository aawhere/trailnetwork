/**
 * 
 */
package com.aawhere.track.kml;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractGeometryType;
import net.opengis.kml.v_2_2_0.LineStringType;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.ManualTrackProcessor;

/**
 * Simple tests for {@link KmlLineStringTrackpointListener} using {@link ExampleTracks}.
 * 
 * @author aroller
 * 
 */
public class KmlLineStringTrackpointListenerUnitTest {

	@Test
	public void testLine() {

		int numberOfLineStrings = 1;
		int numberOfCoordinates = 1;
		Track track = ExampleTracks.LINE;
		Pair<KmlLineStringTrackpointListener, Iterator<Trackpoint>> processor = processor(track);
		assertTrackpointHandled("1", numberOfLineStrings, numberOfCoordinates, processor);
		assertTrackpointHandled("2", 1, 2, processor);
		assertTrackpointHandled("3", 1, 3, processor);
	}

	/**
	 * @param track
	 * @return
	 */
	private Pair<KmlLineStringTrackpointListener, Iterator<Trackpoint>> processor(Track track) {
		KmlLineStringTrackpointListener listener = KmlLineStringTrackpointListener.create()
				.registerWithProcessor(new ManualTrackProcessor.Builder()).build();
		Iterator<Trackpoint> iterator = track.iterator();

		Pair<KmlLineStringTrackpointListener, Iterator<Trackpoint>> processor = Pair.of(listener, iterator);
		return processor;
	}

	@Test
	public void testMissingLocation() {
		Pair<KmlLineStringTrackpointListener, Iterator<Trackpoint>> processor = processor(ExampleTracks.LINE_BEFORE_AND_AFTER_MISSING_LOCATION);
		assertTrackpointHandled("first point", 1, 1, processor);
		assertTrackpointHandled("second point", 1, 2, processor);
		assertTrackpointHandled("third missing location", 1, 2, processor);
		assertTrackpointHandled("fourth has locaiton, new LineString", 2, 1, processor);
		assertTrackpointHandled("fifth has location added to second lineString", 2, 2, processor);
	}

	/**
	 * @param numberOfLineStrings
	 * @param numberOfCoordinates
	 * @param listener
	 * @param iterator
	 */
	private static void assertTrackpointHandled(String message, int numberOfLineStrings, int numberOfCoordinates,
			Pair<KmlLineStringTrackpointListener, Iterator<Trackpoint>> processor) {
		KmlLineStringTrackpointListener listener = processor.getLeft();
		Iterator<Trackpoint> iterator = processor.getRight();
		listener.handle(iterator.next(), null);
		List<JAXBElement<? extends AbstractGeometryType>> lineStrings = listener.getLineStrings()
				.getAbstractGeometryGroup();
		assertEquals(message, numberOfLineStrings, lineStrings.size());
		LineStringType lineString = (LineStringType) lineStrings.get(numberOfLineStrings - 1).getValue();
		assertEquals(message, numberOfCoordinates, lineString.getCoordinates().size());
	}

}
