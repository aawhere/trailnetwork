/**
 * 
 */
package com.aawhere.track.kml;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractFeatureType;
import net.opengis.kml.v_2_2_0.AbstractTimePrimitiveType;
import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.LineStringType;
import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PlacemarkType;
import net.opengis.kml.v_2_2_0.TimeSpanType;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.collections.ListUtilsExtended;
import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.doc.kml.KmlTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.SimpleTrackSegment;
import com.aawhere.track.TrackSegment;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class KmlTrackSegmentListenerUnitTest {
	private KmlTrackSegmentListener listener;
	private ObjectFactory factory;

	@Before
	public void setUp() {
		listener = KmlTrackSegmentListener.create().build();
		factory = new ObjectFactory();

	}

	@Test
	public void testGoodPoints() {
		int expectedSize = 1;
		TrackSegment segment = S1;
		assertHandle(expectedSize, segment);
		assertHandle(2, ExampleTracks.S2);

	}

	/**
	 * Time span is set with the line string when requested during building. This is used for
	 * animation of line drawing.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testWithTimeStart() throws IOException {
		listener = KmlTrackSegmentListener.create().growLineWithTimeline().build();
		JAXBElement<PlacemarkType> element = assertHandle(1, S1);
		LineStringType lineString = (LineStringType) element.getValue().getAbstractGeometryGroup().getValue();
		AbstractTimePrimitiveType timePrimitiveType = element.getValue().getAbstractTimePrimitiveGroup().getValue();
		assertNotNull("time span didn't make it", timePrimitiveType);
		TestUtil.assertInstanceOf(TimeSpanType.class, timePrimitiveType);
		TimeSpanType span = (TimeSpanType) timePrimitiveType;
		assertNotNull("start must be set", span.getBegin());
		assertNull("end must not be set", span.getEnd());
		// good, let's ad another
		assertHandle(2, S2);
		writeToFile("testWithTimeStart");
	}

	@Test
	public void testMissingLocation() {
		assertHandle(0, new SimpleTrackSegment(ExampleTracks.P1, ExampleTracks.P2_MISSING_LOCATION));
		assertHandle(1, ExampleTracks.S2);
	}

	@Test
	public void testWriteToXml() throws IOException {
		assertHandle(1, S1);
		assertHandle(2, S2);
		String methodName = "testWriteToXml";
		writeToFile(methodName);

	}

	/**
	 * @param methodName
	 * @throws IOException
	 */
	private void writeToFile(String methodName) throws IOException {
		KmlDocument doc = KmlDocument.create().setMain(listener.getElement()).build();
		KmlTestUtil.writeToFile(doc, getClass().getSimpleName() + "-" + methodName);
	}

	/**
	 * @param expectedSize
	 * @param segment
	 * @return
	 */
	private JAXBElement<PlacemarkType> assertHandle(int expectedSize, TrackSegment segment) {
		listener.handle(segment, null);
		FolderType segments = listener.getSegments();
		assertNotNull(segments);
		List<JAXBElement<? extends AbstractFeatureType>> abstractGeometryGroup = segments.getAbstractFeatureGroup();
		assertEquals(expectedSize, abstractGeometryGroup.size());
		return (JAXBElement<PlacemarkType>) ListUtilsExtended.lastOrNull(abstractGeometryGroup);
	}

}
