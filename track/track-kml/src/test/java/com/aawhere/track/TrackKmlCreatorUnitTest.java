/**
 * 
 */
package com.aawhere.track;

import java.util.List;

import net.opengis.kml.v_2_2_0.PlacemarkType;

import org.joda.time.DateTime;
import org.junit.Test;

import com.aawhere.doc.kml.KmlUnitTest;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.process.calc.TrackElapsedDurationCalculator;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.AutomaticTrackProcessor.Builder;

/**
 * @author aroller
 * 
 */
public class TrackKmlCreatorUnitTest
		extends KmlUnitTest<TrackKmlCreator, PlacemarkType> {

	private TrackKmlCreator listener;

	@Test
	public void singleTrackRace() {
		List<Trackpoint> track = ExampleRealisticTracks.create().atLeast(MeasurementUtil.createLengthInMeters(1000))
				.build().track();
		Builder processorBuilder = AutomaticTrackProcessor.create(track);
		TrackElapsedDurationCalculator durationCalculator = new TrackElapsedDurationCalculator.Builder()
				.registerWithProcessor(processorBuilder).build();
		this.listener = TrackKmlCreator.create().registerWithProcessor(processorBuilder)
				.durationProvider(durationCalculator).startTime(DateTime.now()).build();
		processorBuilder.build();

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.kml.KmlUnitTest#main()
	 */
	@Override
	protected TrackKmlCreator main() {
		return this.listener;
	}

}
