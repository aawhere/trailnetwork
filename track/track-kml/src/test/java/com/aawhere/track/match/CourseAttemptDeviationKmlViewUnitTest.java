/**
 * 
 */
package com.aawhere.track.match;

import java.io.IOException;

import org.junit.After;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;

import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.doc.kml.KmlTestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.track.process.processor.ManualTrackProcessor.Builder;

/**
 * Runs each test of it's parent {@link CourseAttemptDeviationProcessorUnitTest} and writes out to a
 * file in the target directory for viewing in Google Earth. It hooks into the processing of each
 * test as it is run in the parent and during the {@link #finish()} method it writes the file out.
 * 
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class CourseAttemptDeviationKmlViewUnitTest
		extends CourseAttemptDeviationProcessorUnitTest {

	private CourseAttemptDeviationKmlView view;
	@Rule
	public TestName name = new TestName();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.match.CourseAttemptDeviationProcessorUnitTest#setUpDriverProcessor(
	 * com.aawhere.track.process.processor.ManualTrackProcessor.Builder,
	 * com.aawhere.track.match.CourseAttemptDeviationProcessor)
	 */
	@Override
	protected void setUpDriverProcessor(Builder driverBuilder, CourseAttemptDeviationProcessor deviationProcessor) {
		super.setUpDriverProcessor(driverBuilder, deviationProcessor);
		CourseAttemptDeviationKmlView.Builder viewBuilder = CourseAttemptDeviationKmlView.create();
		viewBuilder.registerWithProcessor(driverBuilder);
		viewBuilder.setDeviationProcessor(deviationProcessor);
		this.view = viewBuilder.build();
	}

	/**
	 * Writes out the current test results to the target directly in kml format named with the
	 * method name.
	 * <p/>
	 * <b>HEY, LOOK AT ME! This is disabled unless you want it for testing sake. </b> remove the
	 * comment on the {@link After} annotation. This is done instead of ignoring the whole class
	 * since testing the KML generation is valuable even if the output is not wanted.
	 * 
	 * @throws IOException
	 */
	@After
	public void writeKmlFiles() throws IOException {
		KmlDocument doc = KmlDocument.create().setMain(view.getElement()).build();

		KmlTestUtil.writeToFile(doc, name.getMethodName());
	}
}
