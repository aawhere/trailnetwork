/**
 * 
 */
package com.aawhere.track.match;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;

import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.doc.kml.KmlTestUtil;
import com.aawhere.test.category.FileTest;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.match.TrackTimingRelationCalculator.Builder;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class TrackTimingRelationCalculatorKmlUnitTest
		extends TrackTimingRelationCalculatorUnitTest {

	private TrackTimingRelationCalculatorKml kml;
	@Rule
	public TestName name = new TestName();

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.match.TrackTimingRelationCalculatorUnitTest#createTester(com.aawhere.track
	 * .Track, java.util.List)
	 */
	@Override
	public Tester createTester(Iterable<Trackpoint> courseTrack, List<Trackpoint> attemptTrackpoints) {

		KmlTester kmlTester = new KmlTester(courseTrack, attemptTrackpoints);
		kml = (TrackTimingRelationCalculatorKml) kmlTester.calculator;
		return kmlTester;
	}

	public class KmlTester
			extends TrackTimingRelationCalculatorUnitTest.Tester {

		/**
		 * @param courseTrack
		 * @param attemptTrackpoints
		 */
		public KmlTester(Iterable<Trackpoint> courseTrack, List<Trackpoint> attemptTrackpoints) {
			super(courseTrack, attemptTrackpoints);

		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.track.match.TrackTimingRelationCalculatorUnitTest.Tester#createCalculatorBuilder
		 * ()
		 */
		@Override
		protected Builder createCalculatorBuilder() {
			return new TrackTimingRelationCalculatorKml.Builder();
		}
	}

	/**
	 * Enable this for file output to target directory.
	 * 
	 * @throws IOException
	 */
	@After
	public void writeKmlFiles() throws IOException {
		List<CourseAttemptDeviationKmlView> views = this.kml.getViews();
		for (int i = 0; i < views.size(); i++) {
			CourseAttemptDeviationKmlView view = views.get(i);
			String name = "Calculator-" + this.name.getMethodName() + (i + 1);
			KmlDocument doc = KmlDocument.create().setMain(view.getElement()).build();
			KmlTestUtil.writeToFile(doc, name);
		}
	}

}
