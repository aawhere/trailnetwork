/**
 * 
 */
package com.aawhere.measure;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractFeatureType;
import net.opengis.kml.v_2_2_0.FolderType;

import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;

import com.aawhere.doc.kml.KmlTestUtil;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.FileTest;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class BoundingBoxKmlUnitTest {

	@Rule
	public TestName name = new TestName();

	@Test
	public void testBoundingBox() {
		GeoCellBoundingBox box = GeoCellBoundingBox.createGeoCell().setGeoCells(GeoCellTestUtil.A).build();
		assertBoundingBox(box);
	}

	/**
	 * @param box
	 * @return
	 */
	private FolderType assertBoundingBox(GeoCellBoundingBox box) {
		FolderType folder = KmlMeasureUtil.folder(box);
		List<JAXBElement<? extends AbstractFeatureType>> features = folder.getAbstractFeatureGroup();
		TestUtil.assertSize(2, features);
		return folder;
	}

	@Test
	public void testGeoCellBoundingBoxes() throws IOException {
		assertBoundingBoxes(GeoCellTestUtil.childrenBoundingBoxes());
	}

	@Test
	public void testBoundingBoxes() throws IOException {
		BoundingBoxes boxes = BoundingBoxTestUtils.createRandomBoundingBoxes();
		assertBoundingBoxes(boxes);
	}

	/**
	 * @param boxes
	 * @throws IOException
	 */
	private void assertBoundingBoxes(BoundingBoxes boxes) throws IOException {
		Collection<FolderType> folders = KmlMeasureUtil.folders(boxes);
		TestUtil.assertSize(boxes.getBoundingBoxes().size(), folders);

		KmlTestUtil.writeToFile(KmlMeasureUtil.document(boxes), name.getMethodName());
	}

}
