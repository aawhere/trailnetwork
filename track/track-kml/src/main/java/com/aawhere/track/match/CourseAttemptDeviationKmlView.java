/**
 * 
 */
package com.aawhere.track.match;

import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractColorStyleType;
import net.opengis.kml.v_2_2_0.AbstractGeometryType;
import net.opengis.kml.v_2_2_0.DocumentType;
import net.opengis.kml.v_2_2_0.LineStyleType;
import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PlacemarkType;
import net.opengis.kml.v_2_2_0.PolyStyleType;
import net.opengis.kml.v_2_2_0.StyleType;
import net.opengis.kml.v_2_2_0.TimeSpanType;

import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.doc.kml.geom.KmlGeometryToElementFunction;
import com.aawhere.doc.kml.geom.jts.JtsGeometryToKmlGeometryFunction;
import com.aawhere.lang.Assertion;
import com.aawhere.style.AlphaChannel;
import com.aawhere.style.Color;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.SimpleTrackSegment;
import com.aawhere.track.Track;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.kml.KmlTrackUtil;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.UnfinishedProcessorListener;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Visualizes the processing of a {@link CourseAttemptDeviationProcessor} by showing the buffer from
 * {@link SlidingBufferTrackpointSeeker} and the deviations from the
 * {@link CourseAttemptDeviationSeeker}. The kml uses animation to show snapshots of where each
 * geometry is in time.
 * 
 * This listener is to be registered with the same processor that is providing attempt trackpoints
 * to the {@link CourseAttemptDeviationProcessor} which this possesses a reference. During each
 * iteration the appropriate snapshot will be provided.
 * 
 * @author aroller
 * 
 */
public class CourseAttemptDeviationKmlView
		extends ProcessorListenerBase
		implements TrackSegmentListener, UnfinishedProcessorListener {

	protected static final String DEVIATION_STYLE_ID = "deviation";
	protected static final String MISSING_STYLE_ID = "missing";
	protected static final String COURSE_STYLE_ID = "course";
	protected static final String SEGMENT_STYLE_ID = "segment";

	/**
	 * Used to construct all instances of CourseAttemptDeviationKmlView.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<CourseAttemptDeviationKmlView, Builder> {

		public Builder() {
			super(new CourseAttemptDeviationKmlView());
		}

		public Builder setDeviationProcessor(CourseAttemptDeviationProcessor deviationProcessor) {
			building.deviationProcessor = deviationProcessor;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.ProcessorListenerBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("deviationProcessor", building.deviationProcessor);
		}

		@Override
		public CourseAttemptDeviationKmlView build() {
			CourseAttemptDeviationKmlView built = super.build();
			built.factory = new ObjectFactory();
			built.doc = built.factory.createDocumentType();
			built.defineStyles();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private CourseAttemptDeviationProcessor deviationProcessor;
	private DocumentType doc;
	private ObjectFactory factory;
	private JtsGeometryToKmlGeometryFunction jtsFunction = JtsGeometryToKmlGeometryFunction.create().build();
	private KmlGeometryToElementFunction elementFunction = KmlGeometryToElementFunction.create().build();
	private int numberOfDeviationsDrawn = 0;
	private TrackSegment lastKnownSegment;
	private JAXBElement<TimeSpanType> startNowTimeSpan;
	private JAXBElement<TimeSpanType> segmentTimeSpan;
	private TrackSegment attemptSegmentWhenBufferStarted;
	/** The start point of the last drawn buffer. */
	private Trackpoint firstPointOfBuffer;
	private Geometry courseBuffer;

	/** Use {@link Builder} to construct CourseAttemptDeviationKmlView */
	private CourseAttemptDeviationKmlView() {
	}

	/**
	 * Every point received is a queue to provide another view snapshot of where the buffer and
	 * current deviations are located.
	 * 
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track.TrackSegment,
	 *      com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		String placemarkName = segment.getFirst().getTimestamp().toLocalTime().toString();
		this.segmentTimeSpan = factory.createTimeSpan(KmlTrackUtil.timeSpan(segment, true, true));
		// make this available for finish
		this.startNowTimeSpan = factory.createTimeSpan(KmlTrackUtil.timeSpan(segment, true, false));
		makeCourse(segment);
		makeSegment(segment, SEGMENT_STYLE_ID);
		makeMisses(placemarkName, segmentTimeSpan);
		makeDeviations(startNowTimeSpan);
		this.lastKnownSegment = segment;
	}

	private void defineStyles() {
		defineLineStyle(Color.RED, DEVIATION_STYLE_ID, KmlUtil.REALLY_WIDE_LINE_WIDTH);
		defineLineStyle(Color.ORANGE, MISSING_STYLE_ID, KmlUtil.MEDIUM_LINE_WIDTH);
		defineLineStyle(Color.GREEN, SEGMENT_STYLE_ID, KmlUtil.MEDIUM_LINE_WIDTH);
		Color courseColor = AlphaChannel.HALF.apply(new Color(100, 149, 237));
		defineLineStyle(courseColor, COURSE_STYLE_ID, KmlUtil.REALLY_WIDE_LINE_WIDTH);
	}

	/**
	 * @param color
	 * @param id
	 * @param lineWidth
	 */
	protected void defineLineStyle(Color color, String id, Double lineWidth) {
		LineStyleType geometryStyle = factory.createLineStyleType();
		geometryStyle.setWidth(lineWidth);
		defineColorStyle(color, id, geometryStyle).setLineStyle(geometryStyle);
	}

	protected void defineBufferStyle(Color color, String styleId) {
		PolyStyleType geometryStyle = factory.createPolyStyleType();
		color = AlphaChannel.HALF.apply(color);
		defineColorStyle(color, styleId, geometryStyle).setPolyStyle(geometryStyle);
	}

	/**
	 * @param color
	 * @param id
	 * @param geometryStyle
	 * @return
	 */
	private StyleType defineColorStyle(Color color, String id, AbstractColorStyleType geometryStyle) {
		geometryStyle.setColor(KmlUtil.color(color));
		StyleType styleType = factory.createStyleType();
		styleType.setId(id);
		this.doc.getAbstractStyleSelectorGroup().add(factory.createStyle(styleType));
		return styleType;
	}

	/**
	 * @param startNowTimeSpan
	 */
	private void makeDeviations(JAXBElement<TimeSpanType> startNowTimeSpan) {
		List<Track> deviations = deviationProcessor.getDeviations();
		if (deviations.size() > numberOfDeviationsDrawn) {
			for (int i = numberOfDeviationsDrawn; i < deviations.size(); i++) {

				PlacemarkType placemark = factory.createPlacemarkType();
				numberOfDeviationsDrawn++;
				placemark.setName("Deviation " + numberOfDeviationsDrawn);
				placemark.setAbstractTimePrimitiveGroup(startNowTimeSpan);
				Track currentDeviation = deviations.get(i);
				MultiLineString lineString = JtsTrackGeomUtil.trackToMultiLineString(currentDeviation);
				AbstractGeometryType kmlGeometry = jtsFunction.apply(lineString);
				JAXBElement<AbstractGeometryType> element = elementFunction.apply(kmlGeometry);
				placemark.setAbstractGeometryGroup(element);
				placemark.setStyleUrl(KmlUtil.styleUrl(DEVIATION_STYLE_ID));
				addPlacemark(placemark);

			}
		}
	}

	/**
	 * @param placemarkName
	 * @param timeSpan
	 */
	private void makeMisses(String placemarkName, JAXBElement<TimeSpanType> timeSpan) {
		LineString currentMisses = deviationProcessor.getDeviationSeeker().lineStringOfCurrentMisses();
		if (!currentMisses.isEmpty()) {
			PlacemarkType placemark = factory.createPlacemarkType();
			placemark.setName(MISSING_STYLE_ID + placemarkName);
			placemark.setStyleUrl(KmlUtil.styleUrl(MISSING_STYLE_ID));
			placemark.setAbstractGeometryGroup(elementFunction.apply(jtsFunction.apply(currentMisses)));
			placemark.setAbstractTimePrimitiveGroup(timeSpan);
			addPlacemark(placemark);
		}
	}

	/**
	 * @param placemarkName
	 * @param timeSpan
	 */
	protected void makeSegment(TrackSegment segment, String styleId) {

		JAXBElement<TimeSpanType> timeSpan = factory.createTimeSpan(KmlTrackUtil.timeSpan(segment, true, true));
		String name = styleId + segment.getInterval();
		LineString lineString = JtsTrackGeomUtil.trackSegmentToLineString(segment);
		drawLine(name, lineString, styleId, timeSpan);
	}

	/**
	 * Draws the line given the Line and supporting attributes. Made available for friends to add
	 * lines to the document.
	 * 
	 * @param timeSpan
	 * @param styleId
	 * @param name
	 * @param lineString
	 */
	protected void drawLine(String name, LineString lineString, String styleId,
			@Nullable JAXBElement<TimeSpanType> timeSpan) {
		PlacemarkType placemark = factory.createPlacemarkType();
		placemark.setStyleUrl(KmlUtil.styleUrl(styleId));
		placemark.setName(name);
		placemark.setAbstractGeometryGroup(elementFunction.apply(jtsFunction.apply(lineString)));
		if (timeSpan != null) {
			placemark.setAbstractTimePrimitiveGroup(timeSpan);
		}
		addPlacemark(placemark);
	}

	protected void drawGeometry(String name, Geometry geometry, String styleId,
			@Nullable JAXBElement<TimeSpanType> timeSpan) {
		PlacemarkType placemark = factory.createPlacemarkType();
		placemark.setStyleUrl(KmlUtil.styleUrl(styleId));
		placemark.setName(name);
		placemark.setAbstractGeometryGroup(elementFunction.apply(jtsFunction.apply(geometry)));
		if (timeSpan != null) {
			placemark.setAbstractTimePrimitiveGroup(timeSpan);
		}
		addPlacemark(placemark);
	}

	/**
	 * Buffers are drawn only when they change. THis means they are drawn only when the next buffer
	 * is available and they are displayed from the time the buffer started til the time of the next
	 * buffer. Times are attempt times, not course times.
	 * 
	 * @param placemarkName
	 * @param timeSpan
	 */
	private void makeCourse(TrackSegment segment) {
		makeCourse(segment, false);
	}

	private void makeCourse(TrackSegment segment, Boolean force) {

		// used to compare if the buffer has changed
		Trackpoint bufferStart = deviationProcessor.getCourseBufferSeeker().getBufferTrackpoints().iterator().next();
		if (this.attemptSegmentWhenBufferStarted == null) {
			// initialize what is necessary to make a buffer.
			this.attemptSegmentWhenBufferStarted = segment;
			this.firstPointOfBuffer = bufferStart;
			this.courseBuffer = deviationProcessor.getCourseBufferSeeker().getBuffer();
		} else {
			// only draw a new buffer if a new buffer exists
			if (!bufferStart.getTimestamp().equals(this.firstPointOfBuffer.getTimestamp()) || force) {

				// draw the previous course buffer since we know when it disappears.
				Geometry buffer = this.courseBuffer;
				AbstractGeometryType bufferKml = jtsFunction.apply(buffer);

				PlacemarkType bufferPlacemark = factory.createPlacemarkType();

				String courseStyleId = COURSE_STYLE_ID;

				bufferPlacemark.setStyleUrl(KmlUtil.styleUrl(courseStyleId));
				bufferPlacemark.setAbstractGeometryGroup(elementFunction.apply(bufferKml));

				SimpleTrackSegment timeSpanSegment = new SimpleTrackSegment(
						this.attemptSegmentWhenBufferStarted.getFirst(), segment.getSecond());
				TimeSpanType timeSpan = KmlTrackUtil.timeSpan(timeSpanSegment, true, true);
				bufferPlacemark.setAbstractTimePrimitiveGroup(factory.createTimeSpan(timeSpan));

				// give a name explaining this is buffer and when
				String placemarkName = courseStyleId + "-" + timeSpanSegment.getInterval().toString();
				bufferPlacemark.setName(placemarkName);
				addPlacemark(bufferPlacemark);
				this.courseBuffer = deviationProcessor.getCourseBufferSeeker().getBuffer();
				this.firstPointOfBuffer = bufferStart;
				this.attemptSegmentWhenBufferStarted = segment;
			}
		}

	}

	/**
	 * Simply adds a placemark to the list of placemarks. This is available to friends and family to
	 * be notified when something is added and/or to add something themselves.
	 * 
	 * @param placemark
	 */
	protected void addPlacemark(PlacemarkType placemark) {
		doc.getAbstractFeatureGroup().add(factory.createPlacemark(placemark));
	}

	/**
	 * @return the placemarks
	 */
	public DocumentType getPlacemarks() {
		return this.doc;
	}

	public JAXBElement<DocumentType> getElement() {
		return factory.createDocument(doc);
	}

	/**
	 * Adds deviations that may have been detected during the finish of processing.
	 * 
	 * @see com.aawhere.track.process.UnfinishedProcessorListener#finish()
	 */
	@Override
	public void finish() {
		makeDeviations(this.startNowTimeSpan);
		makeCourse(lastKnownSegment, true);
	}

	/**
	 * Gets the current time span without ever hiding.
	 * 
	 * @return the startNowTimeSpan
	 */
	protected JAXBElement<TimeSpanType> getStartNowTimeSpan() {
		return this.startNowTimeSpan;
	}

	/**
	 * Gets the current segment's time span which will hide when the segment time completes.
	 * 
	 * @return the segmentTimeSpan
	 */
	protected JAXBElement<TimeSpanType> getSegmentTimeSpan() {
		return this.segmentTimeSpan;
	}
}
