/**
 * 
 */
package com.aawhere.track.match;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PlacemarkType;
import net.opengis.kml.v_2_2_0.TimeSpanType;

import org.joda.time.Interval;

import com.aawhere.doc.kml.KmlStandardIcon;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.doc.kml.StyleKml;
import com.aawhere.style.Color;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.kml.KmlTrackUtil;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor;

/**
 * A way to visualize the {@link TrackTimingRelationCalculator} using Google Earth.
 * 
 * @author aroller
 * 
 */
public class TrackTimingRelationCalculatorKml
		extends TrackTimingRelationCalculator {

	/**
	 * 
	 */
	private static final String START_LINE_STYLE_ID = "startLine";
	private static final String START_AREA_STYLE_ID = "startArea";
	private static final String FINISH_LINE_STYLE_ID = "finishLine";
	private static final String FINISH_AREA_STYLE_ID = "finishArea";
	private static final String SEGMENT_STYLE_ID = CourseAttemptDeviationKmlView.SEGMENT_STYLE_ID + "-off";

	/**
	 * Used to construct all instances of TrackTimingRelationCalculatorKml.
	 */
	public static class Builder
			extends TrackTimingRelationCalculator.Builder {

		public Builder() {
			super(new TrackTimingRelationCalculatorKml());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private ArrayList<CourseAttemptDeviationKmlView> views = new ArrayList<CourseAttemptDeviationKmlView>();
	private CourseAttemptDeviationKmlView currentView;
	private TrackSegment lastKnownSegment;

	/** Use {@link Builder} to construct TrackTimingRelationCalculatorKml */
	private TrackTimingRelationCalculatorKml() {
	}

	/**
	 * Creates a KML view for every race processor created.
	 * 
	 * @see TrackTimingRelationCalculator#configureRaceProcessor(ManualTrackProcessor.Builder,
	 *      CourseAttemptDeviationProcessor )
	 */
	@Override
	protected void configureRaceProcessor(ManualTrackProcessor.Builder builder,
			CourseAttemptDeviationProcessor deviationProcessor) {
		super.configureRaceProcessor(builder, deviationProcessor);
		// this may be called to setup a processor, but that processor never used.
		// if this method is called again after one has been used then it is likely worth keeping
		if (this.currentView != null) {
			this.views.add(currentView);
		}
		// first, finish up the old one.
		this.currentView = CourseAttemptDeviationKmlView.create().registerWithProcessor(builder)
				.setDeviationProcessor(deviationProcessor).build();
		this.currentView.defineBufferStyle(Color.BLACK, START_LINE_STYLE_ID);
		this.currentView.defineBufferStyle(Color.GREEN, START_AREA_STYLE_ID);
		this.currentView.defineBufferStyle(Color.BLACK, FINISH_LINE_STYLE_ID);
		this.currentView.defineBufferStyle(Color.RED, FINISH_AREA_STYLE_ID);
		this.currentView.defineLineStyle(Color.YELLOW, SEGMENT_STYLE_ID, KmlUtil.MEDIUM_LINE_WIDTH);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.match.TrackTimingRelationCalculator#handle(com.aawhere.track.TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		super.handle(segment, processor);
		// draw the segment when not racing since the CPDP won't
		if (!this.racing()) {
			this.currentView.makeSegment(segment, SEGMENT_STYLE_ID);
		}

		this.lastKnownSegment = segment;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.match.TrackTimingRelationCalculator#addResult(com.aawhere.track.match.
	 * TrackTimingRelationCalculator.TimingResult)
	 */
	@Override
	protected void addResult(TimingResult result) {
		super.addResult(result);
		// mark the point where the result finished.
		ObjectFactory factory = KmlUtil.FACTORY;
		PlacemarkType resultPlacemark = factory.createPlacemarkType();
		resultPlacemark.setName(result.getReason().name());

		TrackSummary summary = result.getSummary();
		Trackpoint markerPoint;

		if (summary != null) {
			Interval interval = summary.getInterval();
			resultPlacemark.setDescription(interval.toDuration().toString() + " from " + interval.toString());
			markerPoint = summary.getFinish();
		} else {
			if (this.lastKnownSegment != null) {
				markerPoint = this.lastKnownSegment.getSecond();
			} else {
				markerPoint = result.getStartSearcher().getGoal();
			}
		}
		resultPlacemark.setAbstractGeometryGroup(factory.createPoint(KmlTrackUtil.point(markerPoint)));
		JAXBElement<TimeSpanType> startNowTimeSpan = this.currentView.getStartNowTimeSpan();
		if (startNowTimeSpan == null) {
			if (this.lastKnownSegment != null) {
				startNowTimeSpan = factory.createTimeSpan(KmlTrackUtil.timeSpan(lastKnownSegment, true, false));
			}
		}
		resultPlacemark.setAbstractTimePrimitiveGroup(startNowTimeSpan);
		final com.aawhere.doc.kml.StyleKml.Builder style = StyleKml.create();
		style.icon().icon(KmlStandardIcon.FLAG);
		resultPlacemark.getAbstractStyleSelectorGroup().add(style.build().get());
		this.currentView.addPlacemark(resultPlacemark);

		showGoal(START_LINE_STYLE_ID, START_AREA_STYLE_ID, result.getStartSearcher());
		showGoal(FINISH_LINE_STYLE_ID, FINISH_AREA_STYLE_ID, result.getFinishSearcher());

	}

	/**
	 * @param startLineStyleId
	 * @param startAreaStyleId
	 * @param startSearcher
	 */
	private void showGoal(String lineStyleId, String areaStyleId, GoalPointSearcher searcher) {
		if (searcher.hasGoalLine()) {
			this.currentView.drawGeometry(lineStyleId, searcher.getGoalLine(), lineStyleId, null);
		}
		this.currentView.drawGeometry(areaStyleId, searcher.getGoalArea(), areaStyleId, null);

	}

	/**
	 * @return the views
	 */
	public List<CourseAttemptDeviationKmlView> getViews() {
		return Collections.unmodifiableList(this.views);
	}

}
