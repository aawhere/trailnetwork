/**
 * 
 */
package com.aawhere.track.kml;

import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.LineStringType;
import net.opengis.kml.v_2_2_0.MultiGeometryType;
import net.opengis.kml.v_2_2_0.ObjectFactory;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * A {@link TrackpointListener} that converts a {@link Trackpoint} to a {@link LineStringType}.
 * 
 * @author aroller
 * 
 */
public class KmlLineStringTrackpointListener
		extends ProcessorListenerBase
		implements TrackpointListener {

	private MultiGeometryType lineStrings;
	private List<String> coordinates;

	/**
	 * Used to construct all instances of KmlLineStringTrackpointListener.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<KmlLineStringTrackpointListener, Builder> {

		public Builder() {
			super(new KmlLineStringTrackpointListener());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.ProcessorListenerBase.Builder#build()
		 */
		@Override
		public KmlLineStringTrackpointListener build() {
			KmlLineStringTrackpointListener built = super.build();
			built.lineStrings = new MultiGeometryType();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Creates a line string if necessary and adds coordinates to the most recently created.
	 * 
	 * @see #endLineString()
	 * 
	 */
	private void addCoordinate(String coord) {
		if (this.coordinates == null) {
			LineStringType lineString = new LineStringType();
			JAXBElement<LineStringType> jaxbElement = new ObjectFactory().createLineString(lineString);
			this.lineStrings.getAbstractGeometryGroup().add(jaxbElement);
			this.coordinates = lineString.getCoordinates();
		}
		this.coordinates.add(coord);

	}

	/** Use {@link Builder} to construct KmlLineStringTrackpointListener */
	private KmlLineStringTrackpointListener() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {

		if (trackpoint.isLocationCapable()) {

			addCoordinate(KmlTrackUtil.toCoordinate(trackpoint));
		} else {
			// deal with broken segments
			endLineString();
		}
	}

	/**
	 * 
	 */
	private void endLineString() {
		this.coordinates = null;
	}

	/**
	 * Contains zero to many {@link LineStringType}s.
	 * 
	 * @return the lineStrings
	 */
	public MultiGeometryType getLineStrings() {
		return this.lineStrings;
	}

}
