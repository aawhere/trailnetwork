/**
 * 
 */
package com.aawhere.track.kml;

import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PointType;
import net.opengis.kml.v_2_2_0.TimeSpanType;

import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.measure.KmlMeasureUtil;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.Trackpoint;

/**
 * @author aroller
 * 
 */
public class KmlTrackUtil {

	private static ObjectFactory factory = new ObjectFactory();

	/**
	 * Given a 2 or 3 D {@link Trackpoint} this will return a coordinate String ready for KML
	 * LineString use.
	 * 
	 * @param trackpoint
	 * @return
	 */
	public static String toCoordinate(Trackpoint trackpoint) {
		return KmlMeasureUtil.coordinate(trackpoint.getLocation(), trackpoint.getElevation());
	}

	/**
	 * Given a {@link TrackSegment} this will set the start and end if desired as indicated by the
	 * provided parameters.
	 * 
	 * @param segment
	 * @param includeStart
	 * @param includeEnd
	 * @return
	 */
	public static TimeSpanType timeSpan(TrackSegment segment, Boolean includeStart, Boolean includeEnd) {
		TimeSpanType timeSpan = factory.createTimeSpanType();
		if (includeStart) {
			Trackpoint trackpoint = segment.getFirst();
			String start = JodaTimeUtil.toIsoXmlFormat(trackpoint.getTimestamp());
			timeSpan.setBegin(start);
		}
		if (includeEnd) {
			timeSpan.setEnd(JodaTimeUtil.toIsoXmlFormat(segment.getSecond().getTimestamp()));
		}
		return timeSpan;
	}

	/**
	 * Creates a timespan from a single trackpoint. Assigns the time to start and/or finish based on
	 * provided flags.
	 * 
	 * @param trackpoint
	 * @param includeStart
	 * @param includeEnd
	 * @return
	 */
	public static TimeSpanType timeSpan(Trackpoint trackpoint, Boolean includeStart, Boolean includeEnd) {
		// FIXME:Find a better way to re-use this logic shared with segment
		TimeSpanType timeSpan = factory.createTimeSpanType();
		if (includeStart) {
			String start = JodaTimeUtil.toIsoXmlFormat(trackpoint.getTimestamp());
			timeSpan.setBegin(start);
		}
		if (includeEnd) {
			timeSpan.setEnd(JodaTimeUtil.toIsoXmlFormat(trackpoint.getTimestamp()));
		}
		return timeSpan;
	}

	/**
	 * Given a {@link Trackpoint} this will return a {@link PointType} at the coordinate.
	 * 
	 * @see #toCoordinate(Trackpoint)
	 * 
	 * @param trackpoint
	 * @return
	 */
	public static PointType point(Trackpoint trackpoint) {
		PointType pointType = factory.createPointType();
		pointType.getCoordinates().add(toCoordinate(trackpoint));
		return pointType;

	}
}
