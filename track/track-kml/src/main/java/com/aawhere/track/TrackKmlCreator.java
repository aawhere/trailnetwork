/**
 * 
 */
package com.aawhere.track;

import static com.aawhere.doc.kml.KmlUtil.*;

import java.util.List;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.PlacemarkType;

import org.joda.time.DateTime;

import com.aawhere.doc.kml.Kml;
import com.aawhere.doc.kml.KmlStandardIcon;
import com.aawhere.doc.kml.StyleKml;
import com.aawhere.measure.KmlMeasureUtil;
import com.aawhere.style.AlphaChannel;
import com.aawhere.style.Color;
import com.aawhere.track.kml.KmlTrackUtil;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.calc.TrackElapsedDurationCalculator;

import com.google.kml.ext._2.TrackType;

/**
 * Produces the Google Earth KML Extension {@link TrackType} used for animating markers along a line
 * using the timelien slider.
 * 
 * https://developers.google.com/kml/documentation/kmlreference#gxtrack
 * 
 * @author aroller
 * 
 */
public class TrackKmlCreator
		extends ProcessorListenerBase
		implements TrackpointListener, Kml<PlacemarkType> {

	private PlacemarkType dot;
	private TrackType track;
	private TrackElapsedDurationCalculator durationProvider;

	private List<String> coords;
	private List<String> when;
	/**
	 * The time all timestamps must be relative.
	 * 
	 */
	private DateTime startTime = new DateTime(0);

	/**
	 * Used to construct all instances of ReplayTourTrackListener.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackKmlCreator, Builder> {

		private com.aawhere.doc.kml.StyleKml.Builder style;

		public Builder() {
			super(new TrackKmlCreator());
			building.dot = new PlacemarkType();
			building.track = new TrackType();
			building.dot.setAbstractGeometryGroup(GX_FACTORY.createTrack(building.track));
			building.coords = building.track.getCoord();
			building.when = building.track.getWhen();
		}

		@Override
		public TrackKmlCreator build() {
			TrackKmlCreator built = super.build();

			if (this.style != null) {
				built.dot.getAbstractStyleSelectorGroup().add(this.style.build().get());
			}
			return built;
		}

		/** Sets up the style to hide the line, but show the standard icon. */
		public Builder standardLook() {
			standardIcon();
			hideLine();
			return this;
		}

		public Builder name(String name) {
			building.dot.setName(name);
			return this;
		}

		/** The information that will appear the balloon if the dot is clicked. */
		public Builder description(String description) {
			building.dot.setDescription(description);
			return this;
		}

		public Builder hideLine() {
			style().line().color(AlphaChannel.TRANSPARENT.apply(Color.WHITE));
			return this;
		}

		public Builder iconColor(Color color) {
			style().icon().color(color);
			return this;
		}

		public Builder standardIcon() {
			style().icon().scale(0.6).icon(KmlStandardIcon.DOT).color(Color.RED);
			return this;
		}

		public StyleKml.Builder style() {
			if (this.style == null) {
				this.style = StyleKml.create();
			}
			return this.style;
		}

		/**
		 * @param durationCalculator
		 * @return
		 */
		public Builder durationProvider(TrackElapsedDurationCalculator durationCalculator) {
			building.durationProvider = durationCalculator;
			return this;
		}

		public Builder startTime(DateTime startTime) {
			building.startTime = startTime;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ReplayTourTrackListener */
	private TrackKmlCreator() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		this.coords.add(KmlTrackUtil.toCoordinate(trackpoint));
		DateTime time;
		if (startTime != null) {
			time = this.startTime.plus(this.durationProvider.getDuration());
		} else {
			time = trackpoint.getTimestamp();
		}
		this.when.add(KmlMeasureUtil.when(time));
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public JAXBElement<PlacemarkType> get() {

		return FACTORY.createPlacemark(this.dot);
	}

	private static final long serialVersionUID = 8179988132627449412L;

}
