/**
 * 
 */
package com.aawhere.track.kml;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.LineStringType;
import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PlacemarkType;
import net.opengis.kml.v_2_2_0.TimeSpanType;

import com.aawhere.doc.kml.geom.KmlLineStringToElementFunction;
import com.aawhere.doc.kml.geom.jts.JtsLineStringToKmlFunction;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;

import com.vividsolutions.jts.geom.LineString;

/**
 * A flexible {@link TrackSegmentListener} that visualizes the {@link TrackSegment}s based on the
 * preferences set during building.
 * 
 * @author aroller
 * 
 */
public class KmlTrackSegmentListener
		extends ProcessorListenerBase
		implements TrackSegmentListener {

	public ObjectFactory factory;

	/**
	 * Used to construct all instances of KmlTrackSegmentListener.
	 */
	public static class Builder
			extends ObjectBuilder<KmlTrackSegmentListener> {

		public Builder() {
			super(new KmlTrackSegmentListener());
		}

		/**
		 * Uses {@link TimeSpanType} to hide the segment until it's time is reached in the timeline
		 * slider.
		 * 
		 * @return
		 */
		public Builder growLineWithTimeline() {
			building.includeTimeSpanStart = true;
			return this;
		}

		@Override
		public KmlTrackSegmentListener build() {
			KmlTrackSegmentListener built = super.build();
			built.factory = new ObjectFactory();
			built.segments = built.factory.createFolderType();
			built.function = JtsLineStringToKmlFunction.create().setFactory(built.factory).build();
			built.elementFunction = KmlLineStringToElementFunction.create().setFactory(built.factory).build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/**
	 * internal indicator whether start time spans are to be used.
	 * 
	 */
	public boolean includeTimeSpanStart = false;

	/** Use {@link Builder} to construct KmlTrackSegmentListener */
	private KmlTrackSegmentListener() {
	}

	private FolderType segments;

	private JtsLineStringToKmlFunction function;
	private KmlLineStringToElementFunction elementFunction;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track.TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {

		if (TrackUtil.isLocationCapable(segment)) {
			PlacemarkType placemark = factory.createPlacemarkType();
			placemark.setName(String.valueOf(segment.getFirst().getTimestamp().getSecondOfDay()));
			LineString lineString = JtsTrackGeomUtil.trackSegmentToLineString(segment);
			LineStringType lineStringKml = function.apply(lineString);
			placemark.setAbstractGeometryGroup(elementFunction.apply(lineStringKml));
			if (includeTimeSpanStart) {
				TimeSpanType timeSpan = KmlTrackUtil.timeSpan(segment, includeTimeSpanStart, false);
				placemark.setAbstractTimePrimitiveGroup(factory.createTimeSpan(timeSpan));
			}
			segments.getAbstractFeatureGroup().add(factory.createPlacemark(placemark));
		}
	}

	/**
	 * @return the segments
	 */
	public FolderType getSegments() {
		return this.segments;
	}

	public JAXBElement<FolderType> getElement() {
		return factory.createFolder(segments);
	}
}
