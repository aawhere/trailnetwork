/**
 * 
 */
package com.aawhere.track.kml;

import net.opengis.kml.v_2_2_0.MultiGeometryType;
import net.opengis.kml.v_2_2_0.PointType;

import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.KmlMeasureUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.calc.BoundingBoxCalculator;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;

/**
 * A {@link TrackProcessor} that processes KML components from the same track. {@link KmlDocument}.
 * 
 * TODO:Display TrackSummary TODO:Persoalize This
 * 
 * @author aroller
 * 
 */
public class TrackKmlProcessor {

	private MultiGeometryType trackLineStrings;

	/**
	 * Used to construct all instances of ActivityKmlTrackProcessor.
	 */
	public static class Builder
			extends ObjectBuilder<TrackKmlProcessor> {

		private Iterable<Trackpoint> track;

		public Builder() {
			super(new TrackKmlProcessor());
		}

		public Builder setTrack(Iterable<Trackpoint> track) {
			this.track = track;
			return this;
		}

		@Override
		public TrackKmlProcessor build() {
			TrackKmlProcessor built = super.build();
			AutomaticTrackProcessor.Builder processorBuilder = AutomaticTrackProcessor.create(this.track);
			BoundingBoxCalculator boundingBoxCalculator = new BoundingBoxCalculator.Builder()
					.registerWithProcessor(processorBuilder).build();
			KmlLineStringTrackpointListener lineStringListener = KmlLineStringTrackpointListener.create()
					.registerWithProcessor(processorBuilder).build();
			processorBuilder.build();
			built.trackLineStrings = lineStringListener.getLineStrings();
			// https://groups.google.com/forum/#!topic/kml-support-getting-started/JHFbwg2g49Y
			// the centerpoint is the location of the label when used with a placemark
			BoundingBox boundingBox = boundingBoxCalculator.getBoundingBox();
			PointType centerPoint = KmlMeasureUtil.point(boundingBox.getCenter());
			built.trackLineStrings.getAbstractGeometryGroup().add(KmlUtil.FACTORY.createPoint(centerPoint));
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ActivityKmlTrackProcessor */
	private TrackKmlProcessor() {
	}

	/**
	 * @return the trackLineStrings
	 */
	public MultiGeometryType getTrackLineStrings() {
		return this.trackLineStrings;
	};
}
