/**
 * 
 */
package com.aawhere.track.kml;

import net.opengis.kml.v_2_2_0.AbstractGeometryType;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * Receives any valid {@link AbstractGeometryType} and manages the timespan associated with it so it
 * will appear only at times appropriate.
 * 
 * @author aroller
 * 
 */
public class KmlGeometryTimespanListener
		extends ProcessorListenerBase
		implements TrackpointListener {

	/**
	 * 
	 */
	public KmlGeometryTimespanListener() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		throw new RuntimeException("TODO:Implement this");
	}

}
