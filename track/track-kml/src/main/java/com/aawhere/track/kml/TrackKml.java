/**
 * 
 */
package com.aawhere.track.kml;

import static com.aawhere.doc.kml.KmlUtil.*;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.LineStyleType;
import net.opengis.kml.v_2_2_0.MultiGeometryType;
import net.opengis.kml.v_2_2_0.PlacemarkType;
import net.opengis.kml.v_2_2_0.StyleType;

import com.aawhere.doc.kml.Kml;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.style.Color;
import com.aawhere.track.Trackpoint;

/**
 * Useful for assigning the track geometry to the given {@link PlacemarkType} that would be the
 * container of the Track (such as an activity or route).
 * 
 * @author aroller
 * 
 */
public class TrackKml
		implements Kml<MultiGeometryType> {

	private LineStyleType lineStyle;
	private StyleType style;
	private MultiGeometryType line;

	/**
	 * Used to construct all instances of TrackKml.
	 */
	public static class Builder
			extends ObjectBuilder<TrackKml> {

		private Double lineWidth = MEDIUM_LINE_WIDTH;
		private Color lineColor;
		private PlacemarkType placemark;
		private Iterable<Trackpoint> track;

		public Builder placemark(PlacemarkType placemark) {
			this.placemark = placemark;
			return this;
		}

		public Builder lineColor(Color lineColor) {
			this.lineColor = lineColor;
			return this;
		}

		public Builder line(MultiGeometryType line) {
			building.line = line;
			return this;
		}

		/**
		 * If the line is not provided, then provide the track and it will be generated.
		 * 
		 * @param track
		 * @return
		 */
		public Builder track(Iterable<Trackpoint> track) {
			this.track = track;
			return this;
		}

		public Builder lineWidth(Double lineWidth) {
			this.lineWidth = lineWidth;
			return this;
		}

		public Builder() {
			super(new TrackKml());
		}

		@Override
		public TrackKml build() {
			TrackKml built = super.build();
			if (built.line == null) {
				TrackKmlProcessor processor = new TrackKmlProcessor.Builder().setTrack(track).build();
				built.line = processor.getTrackLineStrings();
			}
			built.line.setId(id("line"));

			// use LineStyleKml now
			/*
			 * if (false) { built.lineStyle = FACTORY.createLineStyleType();
			 * built.lineStyle.setId(id("lineStyle")); built.lineStyle.setWidth(lineWidth); if
			 * (this.lineColor == null) { built.lineStyle.setColorMode(ColorModeEnumType.RANDOM);
			 * built.lineStyle.setColor(color(Color.WHITE)); } else {
			 * built.lineStyle.setColor(color(lineColor)); } StyleType style =
			 * FACTORY.createStyleType(); style.setLineStyle(built.lineStyle);
			 * placemark.getAbstractStyleSelectorGroup().add(FACTORY.createStyle(style)); }
			 */
			placemark.setAbstractGeometryGroup(FACTORY.createMultiGeometry(built.line));
			return built;
		}

		/**
		 * @param string
		 * @return
		 */
		private String id(String string) {
			return IdentifierUtils.createCompositeIdValue(	"track",
															IdentifierUtils.createCompositeIdValue(	string,
																									placemark.getId()));
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrackKml */
	private TrackKml() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public JAXBElement<MultiGeometryType> get() {
		return KmlUtil.FACTORY.createMultiGeometry(line);
	}

	public StyleType style() {
		return style;
	}

	public LineStyleType changeTrackStyle() {
		final LineStyleType changeStyle = KmlUtil.FACTORY.createLineStyleType();
		changeStyle.setTargetId(this.lineStyle.getId());
		return changeStyle;
	}

	private static final long serialVersionUID = -2102818311760101317L;

}
