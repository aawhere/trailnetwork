/**
 * 
 */
package com.aawhere.measure;

import javax.annotation.Nullable;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class GeoCoordinateToKmlCoordinateFunction
		implements Function<GeoCoordinate, String> {

	/**
	 * Used to construct all instances of GeoCoordinateToKmlCoordinateFunction.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCoordinateToKmlCoordinateFunction> {

		public Builder() {
			super(new GeoCoordinateToKmlCoordinateFunction());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	public static GeoCoordinateToKmlCoordinateFunction build() {
		return create().build();
	}

	/** Use {@link Builder} to construct GeoCoordinateToKmlCoordinateFunction */
	private GeoCoordinateToKmlCoordinateFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public String apply(@Nullable GeoCoordinate input) {
		return KmlMeasureUtil.coordinate(input);
	}

}
