/**
 * 
 */
package com.aawhere.measure;

import java.util.List;

import javax.annotation.Nullable;

import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.LineStringType;
import net.opengis.kml.v_2_2_0.PlacemarkType;
import net.opengis.kml.v_2_2_0.PointType;

import com.aawhere.doc.kml.KmlFunctionBase;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellBoundingBox;

import com.google.common.collect.Lists;

/**
 * Converts a {@link BoundingBox} into a KML {@link LineStringType} with the {@link GeoCell} value
 * as it's label (or just the bounds if not a {@link GeoCellBoundingBox}) at the
 * {@link BoundingBox#getCenter()} as a {@link PointType} all returned in a {@link FolderType} with
 * the geocell as a label.
 * 
 * @author aroller
 * 
 */
public class BoundingBoxKmlFunction
		extends KmlFunctionBase<BoundingBox, FolderType> {

	/**
	 * Used to construct all instances of GeoCellBoundingBoxKmlFunction.
	 */
	public static class Builder
			extends KmlFunctionBase.Builder<BoundingBox, FolderType, BoundingBoxKmlFunction, Builder> {

		public Builder() {
			super(new BoundingBoxKmlFunction());
		}

		public Builder setIconStyleId(String centerpointPlacemarkStyleId) {
			building.centerPointPlacemarkStyleId = KmlUtil.styleUrl(centerpointPlacemarkStyleId);
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private String centerPointPlacemarkStyleId;

	/** Use {@link Builder} to construct GeoCellBoundingBoxKmlFunction */
	private BoundingBoxKmlFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public FolderType apply(@Nullable BoundingBox input) {
		FolderType folder = factory.createFolderType();
		applyLineString(input, folder);
		applyCenterPoint(input, folder);
		return folder;
	}

	/**
	 * @param input
	 * @param folder
	 */
	private void applyCenterPoint(BoundingBox input, FolderType folder) {
		String name;
		if (input instanceof GeoCellBoundingBox) {
			name = ((GeoCellBoundingBox) input).getGeoCells().toString();
		} else {
			name = input.getBounds();
		}

		PlacemarkType centerPoint = factory.createPlacemarkType();
		centerPoint.setName(name);
		centerPoint.setDescription(input.getBounds());
		centerPoint.setAbstractGeometryGroup(factory.createPoint(KmlMeasureUtil.point(input.getCenter())));
		if (this.centerPointPlacemarkStyleId != null) {
			centerPoint.setStyleUrl(centerPointPlacemarkStyleId);
		}
		folder.setName(name);
		folder.getAbstractFeatureGroup().add(factory.createPlacemark(centerPoint));
	}

	/**
	 * @param input
	 * @param folder
	 */
	private void applyLineString(BoundingBox input, FolderType folder) {
		List<GeoCoordinate> coordinates = BoundingBoxUtil.coordinates(input);
		List<String> kmlCoords = Lists.transform(coordinates, GeoCoordinateToKmlCoordinateFunction.build());
		LineStringType lineString = new LineStringType();
		lineString.getCoordinates().addAll(kmlCoords);
		PlacemarkType lineStringPlacemark = new PlacemarkType();
		lineStringPlacemark.setAbstractGeometryGroup(factory.createLineString(lineString));
		folder.getAbstractFeatureGroup().add(factory.createPlacemark(lineStringPlacemark));
	}

}
