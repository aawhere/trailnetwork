/**
 * 
 */
package com.aawhere.measure;

import java.util.Collection;

import javax.annotation.Nullable;

import net.opengis.kml.v_2_2_0.AbstractLatLonBoxType;
import net.opengis.kml.v_2_2_0.AltitudeModeEnumType;
import net.opengis.kml.v_2_2_0.DocumentType;
import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.LatLonAltBoxType;
import net.opengis.kml.v_2_2_0.LatLonBoxType;
import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PointType;
import net.opengis.kml.v_2_2_0.RegionType;

import org.joda.time.DateTime;

import com.aawhere.doc.kml.KmlDocument;
import com.aawhere.doc.kml.KmlStandardIcon;
import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.doc.kml.StyleKml;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.measure.unit.ExtraUnits;

import com.google.common.collect.Collections2;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class KmlMeasureUtil {

	private static final char COORDINATE_VALUE_DILIMETER = ',';
	public static final ObjectFactory factory = new ObjectFactory();

	@Nullable
	public static String coordinate(@Nullable GeoCoordinate location, @Nullable Elevation elevation) {
		String result = null;
		if (location != null) {
			Double elevationValue = null;
			if (elevation != null) {
				elevationValue = elevation.doubleValue(ExtraUnits.METER_MSL);
			}
			result = coordinate(location.getLatitude().getInDecimalDegrees(), location.getLongitude()
					.getInDecimalDegrees(), elevationValue);
		}
		return result;
	}

	/**
	 * Given the details of a coordinate this will return the value of the KML coordinate to be used
	 * in the collection.
	 * 
	 * @param latInDecimalDegrees
	 * @param lonInDecimalDegrees
	 * @param elevationInMeters
	 * @return
	 */
	@Nullable
	public static String coordinate(@Nullable Double latInDecimalDegrees, @Nullable Double lonInDecimalDegrees,
			@Nullable Double elevationInMeters) {
		String result;
		if (latInDecimalDegrees != null && lonInDecimalDegrees != null) {
			StringBuilder builder = new StringBuilder();
			builder.append(lonInDecimalDegrees);
			builder.append(COORDINATE_VALUE_DILIMETER);
			builder.append(latInDecimalDegrees);
			if (elevationInMeters != null) {
				builder.append(COORDINATE_VALUE_DILIMETER);
				builder.append(elevationInMeters);
			}
			result = builder.toString();
		} else {
			result = null;
		}

		return result;

	}

	@Nullable
	public static String coordinate(final @Nullable GeoCoordinate location) {
		return coordinate(location, null);
	}

	public static PointType point(final @Nullable GeoCoordinate location) {
		PointType pointType = factory.createPointType();
		pointType.getCoordinates().add(coordinate(location));
		return pointType;
	}

	public static FolderType folder(BoundingBox boundingBox) {
		return BoundingBoxKmlFunction.create().build().apply(boundingBox);
	}

	public static Collection<FolderType> folders(BoundingBoxes boundingBoxes) {
		return Collections2.transform(boundingBoxes.getBoundingBoxes(), BoundingBoxKmlFunction.create().build());
	}

	public static LatLonBoxType boundingBox(BoundingBox from) {
		return boundingBoxPopulateLocations(from, factory.createLatLonBoxType());
	}

	/**
	 * Allows common population of KML bounding box types.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static <B extends AbstractLatLonBoxType> B boundingBoxPopulateLocations(BoundingBox from, B to) {
		to.setNorth(from.getNorth().getInDecimalDegrees());
		to.setSouth(from.getSouth().getInDecimalDegrees());
		to.setEast(from.getEast().getInDecimalDegrees());
		to.setWest(from.getWest().getInDecimalDegrees());
		return to;
	}

	/**
	 * Produces a bounding box with altitude.
	 * 
	 * @param from
	 * @param range
	 * @return
	 */

	public static LatLonAltBoxType boundingBox(BoundingBox from, Range<Elevation> range) {
		LatLonAltBoxType to = factory.createLatLonAltBoxType();
		to = boundingBoxPopulateLocations(from, to);
		if (range != null) {
			to.setAltitudeModeGroup(factory.createAltitudeMode(AltitudeModeEnumType.CLAMP_TO_GROUND));
			if (range.hasLowerBound()) {
				to.setMinAltitude(range.lowerEndpoint().doubleValue(ExtraUnits.METER_MSL));
			}
			if (range.hasUpperBound()) {
				to.setMaxAltitude(range.upperEndpoint().doubleValue(ExtraUnits.METER_MSL));
			}
		}
		return to;
	}

	/**
	 * Region describes an area and an elevation range used for viewing preferences.
	 * 
	 * @param box
	 * @param range
	 * @return
	 */
	public static RegionType region(BoundingBox box, Range<Elevation> range) {
		RegionType region = factory.createRegionType();
		region.setLatLonAltBox(boundingBox(box, range));
		return region;
	}

	/**
	 * Produces a KML document that produces the best fit of bounding boxes with geocells for a
	 * given area.
	 * 
	 * @param boxes
	 * @return
	 */
	public static KmlDocument document(BoundingBoxes boxes) {
		BoundingBox boundingBox = BoundingBoxUtil.combined(boxes);
		final String centerpointStyleId = "centerPoint";

		StyleKml.Builder style = StyleKml.create().id(centerpointStyleId);
		style.icon().icon(KmlStandardIcon.NONE);
		final Collection<FolderType> boxFolders = Collections2
				.transform(boxes.getBoundingBoxes(), BoundingBoxKmlFunction.create().setIconStyleId(style.id()).build());
		final DocumentType document = KmlUtil.document(boxFolders);
		document.getAbstractStyleSelectorGroup().add(style.build().get());
		// add region to document
		final RegionType region = region(boundingBox, null);
		document.setRegion(region);
		return KmlDocument.create().setMain(document).build();
	}

	/**
	 * Produces the value of the when element.
	 * https://developers.google.com/kml/documentation/kmlreference?csw=1#when
	 * 
	 * @param timestamp
	 * @return
	 */
	public static String when(DateTime timestamp) {

		return JodaTimeUtil.toIsoXmlFormat(timestamp);
	}
}
