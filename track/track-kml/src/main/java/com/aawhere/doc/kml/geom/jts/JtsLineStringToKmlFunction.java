/**
 * 
 */
package com.aawhere.doc.kml.geom.jts;

import java.util.Arrays;
import java.util.Collection;

import javax.annotation.Nullable;

import net.opengis.kml.v_2_2_0.AltitudeModeEnumType;
import net.opengis.kml.v_2_2_0.LineStringType;

import com.aawhere.doc.kml.KmlFunctionBase;
import com.aawhere.doc.kml.KmlUtil;

import com.google.common.collect.Collections2;
import com.vividsolutions.jts.geom.LineString;

/**
 * Configurable converter of JTS {@link LineString} to KML {@link LineStringType}.
 * 
 * @author aroller
 * 
 */
public class JtsLineStringToKmlFunction
		extends KmlFunctionBase<LineString, LineStringType> {

	public static class Builder
			extends KmlFunctionBase.Builder<LineString, LineStringType, JtsLineStringToKmlFunction, Builder> {

		public Builder() {
			super(new JtsLineStringToKmlFunction());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct JtsLineStringToKmlFunction */
	private JtsLineStringToKmlFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public LineStringType apply(@Nullable LineString input) {
		LineStringType kmlLineString = new LineStringType();
		kmlLineString.setAltitudeModeGroup(KmlUtil.FACTORY.createAltitudeMode(AltitudeModeEnumType.CLAMP_TO_GROUND));
		Collection<String> convertedCoordinates = Collections2.transform(	Arrays.asList(input.getCoordinates()),
																			JtsCoordinateToKmlCoordinateFunction
																					.create());
		kmlLineString.getCoordinates().addAll(convertedCoordinates);
		return kmlLineString;

	}

}
