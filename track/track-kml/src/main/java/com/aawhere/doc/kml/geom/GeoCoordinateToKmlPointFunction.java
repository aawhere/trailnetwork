/**
 * 
 */
package com.aawhere.doc.kml.geom;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.PlacemarkType;
import net.opengis.kml.v_2_2_0.PointType;

import com.aawhere.doc.kml.KmlUtil;
import com.aawhere.doc.kml.geom.jts.JtsCoordinateToKmlCoordinateFunction;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.JtsMeasureUtil;

import com.google.common.base.Function;

/**
 * A {@link Function} used to convert {@link GeoCoordinate} to a {@link PointType} to be used with
 * {@link PlacemarkType}, etc.
 * 
 * TODO:add configurations such as altitude mode, extrude, etc.
 * 
 * @author aroller
 * 
 */
public class GeoCoordinateToKmlPointFunction
		implements Function<GeoCoordinate, PointType> {

	/**
	 * Used to construct all instances of GeoCoordinateToKmlPointFunction.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCoordinateToKmlPointFunction> {

		public Builder() {
			super(new GeoCoordinateToKmlPointFunction());
		}

		@Override
		public GeoCoordinateToKmlPointFunction build() {
			GeoCoordinateToKmlPointFunction built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	public static GeoCoordinateToKmlPointFunction build() {
		return create().build();
	}

	/** Use {@link Builder} to construct GeoCoordinateToKmlPointFunction */
	private GeoCoordinateToKmlPointFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public PointType apply(@Nullable GeoCoordinate input) {
		if (input == null) {
			return null;
		}
		final PointType point = KmlUtil.FACTORY.createPointType();

		point.getCoordinates().add(JtsCoordinateToKmlCoordinateFunction.create().apply(JtsMeasureUtil.convert(input)));
		return point;
	}

	public JAXBElement<PointType> element(@Nullable GeoCoordinate input) {
		return KmlUtil.FACTORY.createPoint(apply(input));
	}

}
