/**
 * 
 */
package com.aawhere.doc.kml.geom;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.LineStringType;

import com.aawhere.doc.kml.KmlFunctionBase;

/**
 * A simple converter of a {@link LineStringType} to it's {@link JAXBElement} version.
 * 
 * @author aroller
 * 
 */
public class KmlLineStringToElementFunction
		extends KmlFunctionBase<LineStringType, JAXBElement<LineStringType>> {

	/**
	 * Used to construct all instances of KmlLineStringToElementFunction.
	 */
	public static class Builder
			extends
			KmlFunctionBase.Builder<LineStringType, JAXBElement<LineStringType>, KmlLineStringToElementFunction, Builder> {

		public Builder() {
			super(new KmlLineStringToElementFunction());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct KmlLineStringToElementFunction */
	private KmlLineStringToElementFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public JAXBElement<LineStringType> apply(@Nullable LineStringType input) {
		return factory.createLineString(input);
	}

}
