/**
 * 
 */
package com.aawhere.doc.kml.geom.jts;

import java.util.HashMap;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractGeometryType;
import net.opengis.kml.v_2_2_0.MultiGeometryType;
import net.opengis.kml.v_2_2_0.ObjectFactory;

import org.apache.commons.lang.NotImplementedException;

import com.aawhere.doc.kml.geom.KmlGeometryToElementFunction;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Converts a JTS geometry to it's corresponding Kml {@link AbstractGeometryType}.
 * 
 * FIXME: HashMap is not generic so it lacks type safety. add it if you can, but avoid
 * parameterizing the class in general which doesn't make sense, but does work.
 * 
 * @author aroller
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class JtsGeometryToKmlGeometryFunction
		implements Function<Geometry, AbstractGeometryType> {

	private HashMap functions;
	private KmlGeometryToElementFunction toElementFunction;

	/** Use {@link Builder} to construct JtsGeometryToKmlGeometryFunction */
	private JtsGeometryToKmlGeometryFunction() {
	}

	/**
	 * Used to construct all instances of JtsGeometryToKmlGeometryFunction.
	 */
	public static class Builder
			extends ObjectBuilder<JtsGeometryToKmlGeometryFunction> {

		public Builder() {
			super(new JtsGeometryToKmlGeometryFunction());
		}

		@Override
		public JtsGeometryToKmlGeometryFunction build() {
			JtsGeometryToKmlGeometryFunction built = super.build();
			built.functions = new HashMap();
			ObjectFactory factory = new ObjectFactory();
			built.functions.put(LineString.class, JtsLineStringToKmlFunction.create().setFactory(factory).build());
			built.functions.put(Polygon.class, JtsPolygonToKmlFunction.create().setFactory(factory).build());
			built.toElementFunction = KmlGeometryToElementFunction.create().build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public AbstractGeometryType apply(@Nullable Geometry input) {
		// NOTE:there is a recusive call in the middle of the multi processing

		AbstractGeometryType result;
		// JTS provides many types of Multi Geometries, but KML handles generally so we will too
		if (input instanceof GeometryCollection) {
			int numGeometries = input.getNumGeometries();
			// kml uses multi geometry type for it's multi.
			MultiGeometryType multi = new MultiGeometryType();
			for (int i = 0; i < numGeometries; i++) {
				Geometry geometryN = input.getGeometryN(i);
				// NOTE: recursive call made here, but eventually there will be a sole geometry
				AbstractGeometryType kmlGeometry = apply(geometryN);
				JAXBElement<AbstractGeometryType> element = toElementFunction.apply(kmlGeometry);
				multi.getAbstractGeometryGroup().add(element);
			}
			result = multi;
		} else {
			// not multiple so use a function for direct translation based on the geomtry type
			Function<Geometry, AbstractGeometryType> function = (Function<Geometry, AbstractGeometryType>) this.functions
					.get(input.getClass());
			if (function == null) {
				throw new NotImplementedException(input.getClass() + " doesn't have a function assigned");
			}
			result = function.apply(input);
		}

		return result;
	}

}
