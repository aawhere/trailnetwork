/**
 * 
 */
package com.aawhere.doc.kml.geom;

import java.util.HashMap;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractGeometryType;
import net.opengis.kml.v_2_2_0.LineStringType;
import net.opengis.kml.v_2_2_0.MultiGeometryType;
import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PolygonType;

import org.apache.commons.lang.NotImplementedException;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;
import com.google.inject.Singleton;

/**
 * Useful to convert a KML {@link AbstractGeometryType} to its corresponding {@link JAXBElement}.
 * Since the type may not be known this will inspect the provided input for a capable converter.
 * 
 * TODO:this could be a {@link Singleton} although the ability to configure individul functions
 * should be reserved.
 * 
 * FIXME:yeah, it would be nice to use Generics for HashMap, but it's a lot nicer without it!
 * 
 * @author aroller
 * 
 */
@SuppressWarnings("unchecked")
public class KmlGeometryToElementFunction
		implements Function<AbstractGeometryType, JAXBElement<AbstractGeometryType>> {

	/**
	 * Used to construct all instances of KmlGeometryToElementFunction.
	 */
	public static class Builder
			extends ObjectBuilder<KmlGeometryToElementFunction> {

		public Builder() {
			super(new KmlGeometryToElementFunction());
		}

		@Override
		public KmlGeometryToElementFunction build() {
			KmlGeometryToElementFunction built = super.build();
			ObjectFactory factory = new ObjectFactory();
			built.functions.put(LineStringType.class, KmlLineStringToElementFunction.create().setFactory(factory)
					.build());
			built.functions.put(PolygonType.class, KmlPolygonToElementFunction.create().setFactory(factory).build());
			built.functions.put(MultiGeometryType.class, KmlMultiGeometryToElementFunction.create().setFactory(factory)
					.build());
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct KmlGeometryToElementFunction */
	private KmlGeometryToElementFunction() {
	}

	@SuppressWarnings("rawtypes")
	private HashMap functions = new HashMap();

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public JAXBElement<AbstractGeometryType> apply(@Nullable AbstractGeometryType input) {

		Class<? extends AbstractGeometryType> type = input.getClass();
		Function<AbstractGeometryType, JAXBElement<AbstractGeometryType>> function = (Function<AbstractGeometryType, JAXBElement<AbstractGeometryType>>) functions
				.get(type);
		if (function == null) {
			throw new NotImplementedException(type);
		}
		return function.apply(input);
	}

}
