/**
 * 
 */
package com.aawhere.doc.kml.geom.jts;

import javax.annotation.Nullable;

import com.aawhere.measure.KmlMeasureUtil;

import com.google.common.base.Function;
import com.vividsolutions.jts.geom.Coordinate;

/**
 * A simple {@link Function} to convert a JTS {@link Coordinate} into a KML formatted LineString.
 * 
 * @author aroller
 * 
 */
public class JtsCoordinateToKmlCoordinateFunction
		implements Function<Coordinate, String> {

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public String apply(@Nullable Coordinate input) {
		Double elevation = null;
		if (input.z != Coordinate.NULL_ORDINATE && !Double.isNaN(input.z)) {
			elevation = input.z;
		}
		return KmlMeasureUtil.coordinate(input.y, input.x, elevation);

	}

	/**
	 * 
	 */
	public static JtsCoordinateToKmlCoordinateFunction create() {
		return new JtsCoordinateToKmlCoordinateFunction();
	}

}
