/**
 * 
 */
package com.aawhere.doc.kml.geom;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.PolygonType;

import com.aawhere.doc.kml.KmlFunctionBase;

/**
 * @author aroller
 * 
 */
public class KmlPolygonToElementFunction
		extends KmlFunctionBase<PolygonType, JAXBElement<PolygonType>> {

	/**
	 * Used to construct all instances of KmlPolygonToElementFunction.
	 */
	public static class Builder
			extends
			KmlFunctionBase.Builder<PolygonType, JAXBElement<PolygonType>, KmlPolygonToElementFunction, Builder> {

		public Builder() {
			super(new KmlPolygonToElementFunction());
		}

		@Override
		public KmlPolygonToElementFunction build() {
			KmlPolygonToElementFunction built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct KmlPolygonToElementFunction */
	private KmlPolygonToElementFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public JAXBElement<PolygonType> apply(@Nullable PolygonType input) {
		return factory.createPolygon(input);
	}

}
