/**
 * 
 */
package com.aawhere.doc.kml.geom;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.MultiGeometryType;

import com.aawhere.doc.kml.KmlFunctionBase;

/**
 * 
 * @author aroller
 * 
 */
public class KmlMultiGeometryToElementFunction
		extends KmlFunctionBase<MultiGeometryType, JAXBElement<MultiGeometryType>> {

	public static class Builder
			extends
			KmlFunctionBase.Builder<MultiGeometryType, JAXBElement<MultiGeometryType>, KmlMultiGeometryToElementFunction, Builder> {

		public Builder() {
			super(new KmlMultiGeometryToElementFunction());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public JAXBElement<MultiGeometryType> apply(@Nullable MultiGeometryType input) {
		return factory.createMultiGeometry(input);
	}

}
