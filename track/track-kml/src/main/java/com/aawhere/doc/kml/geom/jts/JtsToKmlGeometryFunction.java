/**
 * 
 */
package com.aawhere.doc.kml.geom.jts;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractGeometryType;
import net.opengis.kml.v_2_2_0.AltitudeModeEnumType;
import net.opengis.kml.v_2_2_0.BoundaryType;
import net.opengis.kml.v_2_2_0.LineStringType;
import net.opengis.kml.v_2_2_0.LinearRingType;
import net.opengis.kml.v_2_2_0.MultiGeometryType;
import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PolygonType;

import org.apache.commons.lang.NotImplementedException;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Polygon;

/**
 * A recursive function that will iterate through each Geometry until their is a Geometry with only
 * a single Geometry. It then converts the geomety to its appropriate type.
 * 
 * @deprecated use {@link JtsGeometryToKmlGeometryFunction}
 * @author aroller
 * 
 */
public class JtsToKmlGeometryFunction
		implements Function<Geometry, JAXBElement<? extends AbstractGeometryType>> {
	private ObjectFactory factory = new ObjectFactory();

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public JAXBElement<? extends AbstractGeometryType> apply(@Nullable Geometry input) {
		JAXBElement<? extends AbstractGeometryType> result;
		int numGeometries = input.getNumGeometries();
		if (numGeometries > 1) {
			MultiGeometryType multi = new MultiGeometryType();
			for (int i = 0; i < numGeometries; i++) {
				Geometry geometryN = input.getGeometryN(i);
				// NOTE: recursive call made here
				JAXBElement<? extends AbstractGeometryType> kmlGeometry = apply(geometryN);
				multi.getAbstractGeometryGroup().add(kmlGeometry);
			}
			result = factory.createMultiGeometry(multi);
		} else {
			// get the coordinates list for the appropriate geometry
			List<String> kmlCoordinates;
			// single geometry...convert it for real.
			if (input instanceof LineString) {
				LineStringType kmlLineString = new LineStringType();
				kmlLineString.setAltitudeModeGroup(factory.createAltitudeMode(AltitudeModeEnumType.CLAMP_TO_GROUND));
				kmlCoordinates = kmlLineString.getCoordinates();
				result = factory.createLineString(kmlLineString);
			} else if (input instanceof Polygon) {
				PolygonType kmlPolygon = factory.createPolygonType();
				kmlPolygon.setAltitudeModeGroup(factory.createAltitudeMode(AltitudeModeEnumType.CLAMP_TO_GROUND));
				BoundaryType boundaryType = factory.createBoundaryType();
				kmlPolygon.setOuterBoundaryIs(boundaryType);
				LinearRingType linearRing = factory.createLinearRingType();
				boundaryType.setLinearRing(linearRing);
				kmlCoordinates = linearRing.getCoordinates();
				result = factory.createPolygon(kmlPolygon);
			} else {
				throw new NotImplementedException(input.getClass());
			}
			Collection<String> convertedCoordinates = Collections2.transform(	Arrays.asList(input.getCoordinates()),
																				JtsCoordinateToKmlCoordinateFunction
																						.create());
			kmlCoordinates.addAll(convertedCoordinates);
		}
		return result;
	}

	/**
	 * @return
	 */
	public static JtsToKmlGeometryFunction create() {
		return new JtsToKmlGeometryFunction();
	}

}
