/**
 * 
 */
package com.aawhere.doc.kml.geom.jts;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;

import net.opengis.kml.v_2_2_0.AltitudeModeEnumType;
import net.opengis.kml.v_2_2_0.BoundaryType;
import net.opengis.kml.v_2_2_0.LinearRingType;
import net.opengis.kml.v_2_2_0.PolygonType;

import com.aawhere.doc.kml.KmlFunctionBase;

import com.google.common.collect.Collections2;
import com.vividsolutions.jts.geom.Polygon;

/**
 * @author aroller
 * 
 */
public class JtsPolygonToKmlFunction
		extends KmlFunctionBase<Polygon, PolygonType> {

	public static class Builder
			extends KmlFunctionBase.Builder<Polygon, PolygonType, JtsPolygonToKmlFunction, Builder> {

		public Builder() {
			super(new JtsPolygonToKmlFunction());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public PolygonType apply(@Nullable Polygon input) {
		PolygonType kmlPolygon = factory.createPolygonType();
		kmlPolygon.setAltitudeModeGroup(factory.createAltitudeMode(AltitudeModeEnumType.CLAMP_TO_GROUND));
		BoundaryType boundaryType = factory.createBoundaryType();
		kmlPolygon.setOuterBoundaryIs(boundaryType);
		LinearRingType linearRing = factory.createLinearRingType();
		boundaryType.setLinearRing(linearRing);

		List<String> kmlCoordinates = linearRing.getCoordinates();
		Collection<String> convertedCoordinates = Collections2.transform(	Arrays.asList(input.getCoordinates()),
																			JtsCoordinateToKmlCoordinateFunction
																					.create());
		kmlCoordinates.addAll(convertedCoordinates);
		return kmlPolygon;
	}

}
