package com.aawhere.track.story;

import static org.junit.Assert.*;

import org.junit.Before;

import com.aawhere.track.ExampleTracks;

/**
 * Test {@link GoogleMapEncodedPathUserStory}.
 * 
 * @author Brian Chapman
 * 
 */
public class GoogleMapUserStoriesUnitTest
		extends BaseTrackUserStoryUnitTest<GoogleMapEncodedPathUserStory, String> {

	/**
	 * @param track
	 */
	public GoogleMapUserStoriesUnitTest() {
		super(ExampleTracks.LINE);
	}

	GoogleMapEncodedPathUserStory story;

	@Before
	public void setUpTs() {
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.story.BaseTrackUserStoryUnitTest#addUserStories(com.aawhere.track.story
	 * .TrackUserStoryBuilder)
	 */
	@Override
	protected void addUserStories(TrackUserStories.Builder builder) {
		this.story = builder.setUserStory(new GoogleMapEncodedPathUserStory.Builder());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.story.BaseTrackUserStoryUnitTest#assertCreated()
	 */
	@Override
	protected void assertCreated() {
		String result = story.getResult();
		assertNotNull(result);
		assertEquals(ExampleTracks.ENCODED_LINE, result);
	}
}
