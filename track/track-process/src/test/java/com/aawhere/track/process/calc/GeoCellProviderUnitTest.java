/**
 *
 */
package com.aawhere.track.process.calc;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.test.TestUtil;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor.Builder;

/**
 * @author brian
 * 
 */
public class GeoCellProviderUnitTest {

	GeoCellProvider provider;

	@Before
	public void setUp() {
		Builder processorBuilder = new ManualTrackProcessor.Builder();
		provider = new GeoCellProvider.Builder().registerWithProcessor(processorBuilder)
				.setBufferLength(MeasurementUtil.createLengthInMeters(10))
				.setTargetResolution(TrackSummary.MAX_BOUNDS_RESOLUTION).build();
	}

	@Test
	public void testGeoCell() {
		ArrayList<Trackpoint> trackpoints = createTestTrackPoints();

		for (Trackpoint point : trackpoints) {
			provider.handle(point, null);
		}

		GeoCells result = provider.getGeoCells();
		TestUtil.assertSize(3, result.getAll());
	}

	/*
	 * Test points and resulting encodedPath come from {@link
	 * http://code.google.com/apis/maps/documentation/utilities/polylinealgorithm.html}
	 */
	private ArrayList<Trackpoint> createTestTrackPoints() {
		ArrayList<Trackpoint> list = new ArrayList<Trackpoint>();
		long timeInMills = 10000; // Isn't used.
		list.add(TrackUtil.createPoint(timeInMills, 38.5, -120.2));
		list.add(TrackUtil.createPoint(timeInMills, 40.7, -120.95));
		list.add(TrackUtil.createPoint(timeInMills, 43.252, -126.453));
		return list;
	}

}
