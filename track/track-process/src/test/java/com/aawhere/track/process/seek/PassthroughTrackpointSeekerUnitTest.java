/**
 *
 */
package com.aawhere.track.process.seek;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;

/**
 * @author Brian Chapman
 * @author aroller (rewrite)
 * 
 */
public class PassthroughTrackpointSeekerUnitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPassThrough() {
		Track sourceTrack = ExampleTracks.LINE;

		TrackpointSeekerChainBuilder<AutomaticTrackProcessor> chainBuilder = TrackpointSeekerChainBuilder
				.create(new AutomaticTrackProcessor.Builder(sourceTrack));

		chainBuilder.addSeeker(PassthroughTrackpointSeeker.create());
		TrackpointCounter counter = chainBuilder.registerWithCurrentProcessor(TrackpointCounter.create());
		chainBuilder.build();

		assertEquals("passthrough didn't pass", ExampleTracks.LINE_POINTS.size(), counter.getCount().intValue());
	}
}
