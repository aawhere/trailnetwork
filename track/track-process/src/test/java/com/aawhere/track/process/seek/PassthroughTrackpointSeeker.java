/**
 *
 */
package com.aawhere.track.process.seek;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * A {@link TrackpointSeeker} that passes through every target trackpoint to be handled used for
 * basic testing.
 * 
 * 
 * 
 * @author Brian Chapman
 * 
 */
public class PassthroughTrackpointSeeker
		extends TrackpointSeekerBase
		implements TrackpointListener {

	/**
	 * Passes the next trackpoint to the handler every time the source track advances.
	 * 
	 * @see com.aawhere.track.process.TrackpointListener #handle(com.aawhere.track.Trackpoint,
	 *      com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint sourceTrackpoint, TrackProcessor processor) {
		getReceiver().desiredTrackpointFound(sourceTrackpoint);
	}

	public static class Builder
			extends TrackpointSeekerBase.Builder<PassthroughTrackpointSeeker, Builder> {
		public Builder() {
			super(new PassthroughTrackpointSeeker());
		}
	}

	private PassthroughTrackpointSeeker() {
	}

	public static Builder create() {
		return new Builder();
	}

}
