package com.aawhere.track.story;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import org.junit.Before;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.calc.TrackSummaryCreatorListener;

/**
 *
 */

/**
 * Test {@link TrackSummaryUserStories} and {@link TrackSummaryCreatorListener}.
 * 
 * @author Aaron Roller
 * 
 */
public class TrackSummaryUserStoriesUnitTest
		extends BaseTrackUserStoryUnitTest<TrackSummaryUserStories, TrackSummary> {

	private TrackSummaryUserStories story;
	private ArchivedDocumentId documentId;

	/**
	 * @param track
	 */
	public TrackSummaryUserStoriesUnitTest() {
		super(LINE);
	}

	protected TrackSummaryUserStoriesUnitTest(Iterable<Trackpoint> track) {
		super(track);
	}

	@Before
	public void setUpTs() {
		documentId = IdentifierTestUtil.generateRandomLongId(ArchivedDocument.class, ArchivedDocumentId.class);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.story.BaseTrackUserStoryUnitTest#addUserStories(com.aawhere.track.story
	 * .TrackUserStoryBuilder)
	 */
	@Override
	protected void addUserStories(TrackUserStories.Builder builder) {
		this.story = builder.setUserStory(new TrackSummaryUserStories.Builder().setDocumentId(documentId));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.story.BaseTrackUserStoryUnitTest#assertCreated()
	 */
	@Override
	protected void assertCreated() {
		TrackSummary summary = story.getResult();
		assertEquals(documentId, summary.getDocumentId());
		assertEquals(LINE_POINTS.size(), (int) summary.getNumberOfTrackpoints());
		assertNotNull(summary.getStart());
		assertEquals(P1.getTimestamp(), summary.getStartTime());
		assertNotNull(summary.getFinish());
		MeasureTestUtil.assertEquals(LINE_LENGTH, summary.getDistance());
		// all the segments are the same length
		MeasureTestUtil.assertEquals(S1_LENGTH, summary.getMeanDistanceBetweenTrackpoints());
		assertEquals(S1.getDuration(), summary.getMeanDurationBetweenTrackpoints());
		assertEquals(TrackUtil.elevationGain(P1, P3), summary.getElevationGain());
		ElevationChange expectedElevationGain = (new QuantityMath<ElevationChange>(TrackUtil.elevationGain(P1, P2)))
				.plus(TrackUtil.elevationGain(P2, P3)).getQuantity();
		assertEquals(expectedElevationGain, summary.getElevationGain());
		assertTrue("bounds got smaller", BoundingBoxUtil.contains(summary.getBoundingBox(), LINE_BOUNDING_BOX));
		assertNotNull(summary.getSpatialBuffer());
		assertEquals(SignalQuality.POOR, summary.getSignalQuality());
	}

	/**
	 * @return the story
	 */
	public TrackSummaryUserStories getStory() {
		return this.story;
	}
}
