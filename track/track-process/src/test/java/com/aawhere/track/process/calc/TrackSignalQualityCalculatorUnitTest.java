package com.aawhere.track.process.calc;

import static org.junit.Assert.*;

import java.util.List;

import javax.measure.quantity.Acceleration;
import javax.measure.quantity.Length;
import javax.measure.quantity.Velocity;

import org.junit.Test;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleRealisticTracks;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.calc.TrackSignalQualityCalculator.SignalQualityObservation;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.google.common.collect.ImmutableList;

/**
 * Simple tests challenging {@link TrackSignalQualityCalculator}.
 * 
 * @author aroller
 * 
 */
public class TrackSignalQualityCalculatorUnitTest {

	private static final Length REASONABLE_TRACK_DISTANCE = MeasurementUtil.createLengthInMeters(100);

	@Test
	public void testTrackHugeFastSegments() {
		// the example track is not realistic and makes huge leaps from one latitude to the next in
		// 1 second
		Iterable<Trackpoint> track = ExampleTracks.LINE;
		SignalQuality expectedSignalQuality = SignalQuality.POOR;
		asssertCalculator(track, expectedSignalQuality, Velocity.class, Length.class);
	}

	@Test
	public void testNoBreeches() {
		List<Trackpoint> track = ExampleRealisticTracks.create().atLeast(REASONABLE_TRACK_DISTANCE).build().track();
		asssertCalculator(track, SignalQuality.GREAT);
	}

	@Test
	public void testAcceleration() {
		com.aawhere.track.ExampleRealisticTracks.Builder builder = ExampleRealisticTracks.create();
		builder.current().atLeast(REASONABLE_TRACK_DISTANCE);
		// a micro time will create some crazy acel and velocity, but not breech the distance
		// going half the distance of the first part will result in 1/3rd of the points being
		// breeched
		builder.instruct().timeIncrementMillis(10)
				.atLeast(QuantityMath.create(REASONABLE_TRACK_DISTANCE).dividedBy(2).getQuantity());
		List<Trackpoint> track = builder.build().track();
		asssertCalculator(track, SignalQuality.POOR, Acceleration.class, Velocity.class);
	}

	public void asssertCalculator(Iterable<Trackpoint> track, SignalQuality expectedSignalQuality,
			Class<?>... expectedBreeches) {

		AutomaticTrackProcessor.Builder processorBuilder = AutomaticTrackProcessor.create(track);
		TrackSignalQualityCalculator calculator = ProcessingCalculatorFactory.create(processorBuilder)
				.signalQualityCalculator();
		processorBuilder.build();
		SignalQuality signalQuality = calculator.get();
		String message = "expected a breech ";
		List<SignalQualityObservation> breeches = calculator.breeches();
		ImmutableList<Class<?>> expectedBreechesList = ImmutableList.copyOf(expectedBreeches);
		assertEquals(message + breeches, expectedSignalQuality, signalQuality);
		for (SignalQualityObservation breech : breeches) {
			TestUtil.assertContainsAny(expectedBreechesList, breech.breeches);
		}
	}
}
