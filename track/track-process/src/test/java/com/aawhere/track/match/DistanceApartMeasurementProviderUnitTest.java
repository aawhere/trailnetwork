/**
 * 
 */
package com.aawhere.track.match;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.assertEquals;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.track.SimpleTrackSegment;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.process.processor.ManualTrackProcessor;

/**
 * Tests the {@link DistanceApartMeasurementProvider}.
 * 
 * @author roller
 * 
 */
public class DistanceApartMeasurementProviderUnitTest {
	@Test
	public void testDistanceApartExactMatch() {
		TrackSegment source = S1;
		TrackSegment target = S1;
		Boolean synchExpectation = true;

		assertDistanceProvider(source, target, synchExpectation);
	}

	/**
	 * @param source
	 * @param target
	 * @param expected
	 * @param synchExpectation
	 */
	private void assertDistanceProvider(TrackSegment source, TrackSegment target, Boolean synchExpectation) {
		ManualTrackProcessor.Builder sourceProcesorBuilder = new ManualTrackProcessor.Builder();
		ManualTrackProcessor.Builder targetProcesorBuilder = new ManualTrackProcessor.Builder();
		DistanceApartMeasurementProvider provider = new DistanceApartMeasurementProvider.Builder()
				.registerWithSourceProcessor(sourceProcesorBuilder).registerWithTargetProcessor(targetProcesorBuilder)
				.build();
		ManualTrackProcessor sourceProcessor = sourceProcesorBuilder.build();
		ManualTrackProcessor targetProcessor = targetProcesorBuilder.build();

		Length expected = TrackUtil.averageDistanceApart(source, target);
		provider.handle(source, sourceProcessor);
		provider.handle(target, targetProcessor);
		assertEquals(synchExpectation, provider.isSegmentsSynched());
		assertEquals(expected, provider.getQuantity());
	}

	@Test
	public void testFarDistanceApartSynchedTimes() {
		TrackSegment source = S1;
		TrackSegment target = new SimpleTrackSegment(DISTANT_LOCATION_T1, P2);
		Boolean synchExpectation = true;

		assertDistanceProvider(source, target, synchExpectation);
	}

	@Test
	public void testFarDistanceApartUnsynchedTimes() {
		TrackSegment source = S2;
		TrackSegment target = new SimpleTrackSegment(DISTANT_LOCATION_T1, P2);
		Boolean synchExpectation = false;

		assertDistanceProvider(source, target, synchExpectation);
	}
}
