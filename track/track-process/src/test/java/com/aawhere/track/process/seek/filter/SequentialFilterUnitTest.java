/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.track.ExampleTracks;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;

/**
 * Tests any of the {@link ProcessorFilter}s that extend {@link SequentialTrackpointFilterBase}.
 * 
 * @author roller
 * 
 */
public class SequentialFilterUnitTest {

	/**
	 * Ensures the duplicate point is removed.
	 * 
	 * @see ExampleTracks#REPEATED_POINT
	 * 
	 */
	@Test
	public void testDuplicateTimestamp() {

		final NonSequentialTimestampFilter.Builder filterBuilder = new NonSequentialTimestampFilter.Builder();

		assertFilter(filterBuilder);

	}

	@Test
	public void testDuplicateLocation() {
		assertFilter(new DuplicateLocationFilter.Builder());
	}

	@Test
	public void testDuplicateLocationCustomThreshold() {
		// this is the length that fails, but is not set up to be run all the time.
		// Length failingLength = new QuantityMath<Length>(S1_LENGTH).plus(S1_LENGTH).getQuantity();
		final Length expected = S1_HALF_LENGTH;
		DuplicateLocationFilter filter = (DuplicateLocationFilter) assertFilter(new DuplicateLocationFilter.Builder()
				.setDistanceApartThreshold(expected));
		assertEquals(expected, filter.getDistanceApartThreshold());
	}

	/**
	 * Tests the filters by providing proper sequential trackpoints and duplicate trackpoints making
	 * sure the duplicates are removed by the filter.
	 * 
	 * @param filterBuilder
	 */
	private SequentialTrackpointFilterBase
			assertFilter(final SequentialTrackpointFilterBase.Builder<?, ?> filterBuilder) {
		TrackpointSeekingProcessor.Builder receiverBuilder = new TrackpointSeekingProcessor.Builder();
		TrackpointCounter counter = new TrackpointCounter.Builder().registerWithProcessor(receiverBuilder).build();

		ManualTrackProcessor.Builder processorBuilder = new ManualTrackProcessor.Builder();
		SequentialTrackpointFilterBase filter = filterBuilder.registerWithProcessor(processorBuilder)
				.setReceiver(receiverBuilder.build()).build();

		ManualTrackProcessor processor = processorBuilder.build();
		processor.notifyTrackpointListeners(P1);
		assertEquals("the first should always be included", 1, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P2);
		assertEquals("the filter is too aggressive and cleaning out non duplicates", 2, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P2);
		assertEquals("the filter did not clean the duplicate", 2, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P3);
		assertEquals("the filter is stuck and is cleaning everything", 3, counter.getCount().intValue());
		return filter;
	}
}
