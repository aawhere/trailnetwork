/**
 *
 */
package com.aawhere.track.process.seek.virtual;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.IteratorUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.Test;

import com.aawhere.track.SimpleTrackSegment;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.calc.TrackCreatorTrackpointCalculator;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;
import com.aawhere.track.process.seek.virtual.SequentialTimestampTrackpointSeeker.Builder;

/**
 * Tests {@link SequentialTimestampTrackpointSeeker}, {@link FixedDurationTrackpointSeeker} and
 * {@link TargetTimeVirtualTrackpointSeeker}.
 * 
 * @author roller
 */
public class VirtualTrackpointSeekerUnitTest {
	/**
	 *
	 */
	private static final SimpleTrackSegment P1_P3_SEGMENT = new SimpleTrackSegment(P1, P3);

	/**
	 * Test the whole processor given a track. Interpolated points are produced as well as
	 * midpoints.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testFixedDurationProcessor() {
		TrackpointSeekingProcessor.Builder processorBuilder = new TrackpointSeekingProcessor.Builder();
		TrackCreatorTrackpointCalculator trackCreator = new TrackCreatorTrackpointCalculator.Builder()
				.registerWithProcessor(processorBuilder).build();
		TrackpointSeekingProcessor receiver = processorBuilder.build();
		// 1.5 requires some ponts to match and others to interpolate, but the
		// last one does not match
		Duration fixedDuration = new Duration((long) (TIME_INCREMENT * 1.5));
		com.aawhere.track.process.TrackProcessorBase.Builder<AutomaticTrackProcessor> automaticProcessorBuilder = new AutomaticTrackProcessor.Builder(
				LINE_LONGER_RETURN);
		new FixedDurationTrackpointSeeker.Builder().setDurationBetweenTrackpoints(fixedDuration)
				.setIncludeOriginals(false).setReceiver(receiver).registerWithProcessor(automaticProcessorBuilder)
				.build();
		// processor is not used by fixed seekers.
		automaticProcessorBuilder.build();
		Track resultingTrack = trackCreator.getTrack();
		List<Trackpoint> trackpoints = IteratorUtils.toList(resultingTrack.iterator());
		// expected 0, 1.5T,3T,4.5T,6T
		assertEquals(5, trackpoints.size());
		int index = 0;
		assertSame(P1, trackpoints.get(index));
		index++;
		assertEquals(fixedDuration.getMillis(), trackpoints.get(index).getTimestamp().minus(T1).getMillis());
		index++;
		assertSame(P4, trackpoints.get(index));
		index++;
		assertEquals(fixedDuration.getMillis() * index, trackpoints.get(index).getTimestamp().minus(T1).getMillis());
		index++;
		assertSame(P1_X2, trackpoints.get(index));
	}

	/**
	 * when only the first and last point of the segment match.
	 */
	@Test
	public void testFixedDurationSeekerExactMatch() {
		Duration fixedDuration = P1_P3_SEGMENT.getDuration();
		FixedDurationTrackpointSeeker.Builder seekerBuilder = new FixedDurationTrackpointSeeker.Builder()
				.setDurationBetweenTrackpoints(fixedDuration);
		assertExactMatch(seekerBuilder, P1_P3_SEGMENT);
	}

	/**
	 * Set's up the receiver to receive the trackpoints from the given seeker. A
	 * {@link TrackCreatorTrackpointCalculator} is registered to build a new track from the points
	 * found so we can test the results.
	 * 
	 * @param seekerBuilder
	 * @return
	 */
	public static
			VirtualTrackpointSeeker
			setupSeeker(
					VirtualTrackpointSeeker.Builder<? extends VirtualTrackpointSeeker, ? extends VirtualTrackpointSeeker.Builder<?, ?>> seekerBuilder) {
		ManualTrackProcessor.Builder fakeProcessorBuilder = new ManualTrackProcessor.Builder();
		seekerBuilder.registerWithProcessor(fakeProcessorBuilder);
		TrackProcessorBase.Builder<TrackpointSeekingProcessor> receiverBuilder = new TrackpointSeekingProcessor.Builder();
		new TrackCreatorTrackpointCalculator.Builder().registerWithProcessor(receiverBuilder).build();
		TrackpointSeekingProcessor receiver = receiverBuilder.build();
		seekerBuilder.setReceiver(receiver);
		return seekerBuilder.build();
	}

	@SuppressWarnings("unchecked")
	public static List<Trackpoint> extractTrackpoints(TrackpointSeekingProcessor receiver) {
		TrackCreatorTrackpointCalculator trackCreator = (TrackCreatorTrackpointCalculator) receiver
				.getListener(TrackCreatorTrackpointCalculator.class);
		Track resultingTrack = trackCreator.getTrack();
		return IteratorUtils.toList(resultingTrack.iterator());
	}

	public static
			void
			assertExactMatch(
					TargetTimeVirtualTrackpointSeeker.Builder<? extends TargetTimeVirtualTrackpointSeeker, ? extends TargetTimeVirtualTrackpointSeeker.Builder<?, ?>> builder,
					SimpleTrackSegment segment) {
		TargetTimeVirtualTrackpointSeeker seeker = (TargetTimeVirtualTrackpointSeeker) setupSeeker(builder);
		// 2 because first and last matched
		final int expectedSize = 2;
		final int indexOfFirst = 0;
		final int indexOfSecond = 1;
		assertSegment(seeker, segment, expectedSize, indexOfFirst, indexOfSecond);
	}

	/**
	 * Takes the given segment and feeds it to the seeker acting as the processor and tests the
	 * resulting trackpoints.
	 * 
	 * @param seeker
	 * @param segment
	 * @param expectedSize
	 * @param indexOfFirst
	 * @param indexOfSecond
	 */
	private static List<Trackpoint> assertSegment(TargetTimeVirtualTrackpointSeeker seeker,
			final SimpleTrackSegment segment, final int expectedSize, final Integer indexOfFirst,
			final Integer indexOfSecond) {
		// processor is not used by fixed seekers.
		TrackProcessor controllingProcessor = null;
		final Trackpoint firstPoint = segment.getFirst();
		seeker.handle(firstPoint, controllingProcessor);
		seeker.handle(segment, controllingProcessor);
		seeker.finish();
		List<Trackpoint> trackpoints = extractTrackpoints(seeker.getReceiver());
		assertEquals(trackpoints.toString(), expectedSize, trackpoints.size());
		if (expectedSize > 0) {
			assertSame(segment.getFirst(), trackpoints.get(indexOfFirst));
		}
		if (expectedSize > 1) {
			assertSame(segment.getSecond(), trackpoints.get(indexOfSecond));
		}
		return trackpoints;
	}

	/**
	 * when the first and last match along with a required midway point between the two.* This test
	 * takes a single segment and requests that a fixed duration requirement of half of the total
	 * duration is used. The result should be a track of three equally spaced points.
	 */
	@Test
	public void testFixedDurationSeekerMidway() {
		SimpleTrackSegment segment = new SimpleTrackSegment(P1, P3);
		Duration fixedDuration = new Duration(segment.getDuration().getMillis() / 2);
		final FixedDurationTrackpointSeeker.Builder builder = new FixedDurationTrackpointSeeker.Builder()
				.setDurationBetweenTrackpoints(fixedDuration);
		assertMidway(builder, segment, P2);
	}

	/**
	 * Asserts that ponts will be created midway along the segment because the Seeker is setup to
	 * find a point exactly there.
	 * 
	 * @param builder
	 * @param segment
	 */
	public
			void
			assertMidway(
					VirtualTrackpointSeeker.Builder<? extends TargetTimeVirtualTrackpointSeeker, ? extends TargetTimeVirtualTrackpointSeeker.Builder<?, ?>> builder,
					SimpleTrackSegment segment, Trackpoint expectedMidpoint) {
		TargetTimeVirtualTrackpointSeeker seeker = (TargetTimeVirtualTrackpointSeeker) setupSeeker(builder);
		List<Trackpoint> trackpoints = assertSegment(seeker, segment, 3, 0, 2);
		// p4 is the midpoint of the line from p1 to p4 and back to p1_x2
		Trackpoint midpoint = trackpoints.get(1);
		assertEquals(expectedMidpoint, midpoint);
	}

	public
			void
			assertNoMatch(
					VirtualTrackpointSeeker.Builder<? extends TargetTimeVirtualTrackpointSeeker, ? extends TargetTimeVirtualTrackpointSeeker.Builder<?, ?>> builder,
					SimpleTrackSegment segment) {
		TargetTimeVirtualTrackpointSeeker seeker = (TargetTimeVirtualTrackpointSeeker) setupSeeker(builder);
		assertSegment(seeker, segment, 0, 0, 0);
	}

	/** Looks for a time that does not fall within the segment. */
	@Test
	public void testSequentialTimestampOneMatchEarlyTimestamps() {
		final SimpleTrackSegment segment = new SimpleTrackSegment(P2, P3);
		ArrayList<DateTime> timestamps = new ArrayList<DateTime>();
		timestamps.add(P1.getTimestamp());
		timestamps.add(P2.getTimestamp());
		SequentialTimestampTrackpointSeeker.Builder builder = (Builder) new SequentialTimestampTrackpointSeeker.Builder()
				.setTargetTimestamps(timestamps);
		TargetTimeVirtualTrackpointSeeker seeker = (TargetTimeVirtualTrackpointSeeker) setupSeeker(builder);
		assertSegment(seeker, segment, 1, 0, null);
	}

	/**
	 * The first trackpoint is before the segment and should be forwarded. The second is a match,
	 * the third should be interpolated and the fourth should be a match. This tests forwarding both
	 * before times are found and after.
	 */
	@Test
	public void testSequentialTimestampTwoMatchesRequiresForwarding() {
		final SimpleTrackSegment segment = new SimpleTrackSegment(P2, P4);
		ArrayList<DateTime> timestamps = new ArrayList<DateTime>();
		timestamps.add(P1.getTimestamp());
		timestamps.add(P2.getTimestamp());
		timestamps.add(P3.getTimestamp());
		timestamps.add(P4.getTimestamp());
		SequentialTimestampTrackpointSeeker.Builder builder = (Builder) new SequentialTimestampTrackpointSeeker.Builder()
				.setTargetTimestamps(timestamps);
		TargetTimeVirtualTrackpointSeeker seeker = (TargetTimeVirtualTrackpointSeeker) setupSeeker(builder);
		final int expectedSize = 3;
		List<Trackpoint> trackpoints = assertSegment(seeker, segment, expectedSize, 0, expectedSize - 1);
		assertEquals(P3, trackpoints.get(1));
	}

	/** Looks for a time that does not fall within the segment. */
	@Test
	public void testSequentialTimestampNoMatchEarlyTimestamps() {
		final SimpleTrackSegment segment = new SimpleTrackSegment(P2, P3);
		ArrayList<DateTime> timestamps = new ArrayList<DateTime>();
		timestamps.add(P1.getTimestamp());
		SequentialTimestampTrackpointSeeker.Builder builder = (Builder) new SequentialTimestampTrackpointSeeker.Builder()
				.setTargetTimestamps(timestamps);
		assertNoMatch(builder, segment);
	}

	/** Looks for a time that does not fall within the segment. */
	@Test
	public void testSequentialTimestampNoMatchLateTimestamps() {
		final SimpleTrackSegment segment = new SimpleTrackSegment(P1, P2);
		ArrayList<DateTime> timestamps = new ArrayList<DateTime>();
		timestamps.add(P3.getTimestamp());
		SequentialTimestampTrackpointSeeker.Builder builder = (Builder) new SequentialTimestampTrackpointSeeker.Builder()
				.setTargetTimestamps(timestamps);
		assertNoMatch(builder, segment);
	}

	@Test
	public void testSequentialTimetstampMidway() {
		final SimpleTrackSegment segment = new SimpleTrackSegment(P1, P3);
		ArrayList<DateTime> timestamps = new ArrayList<DateTime>();
		timestamps.add(segment.getFirst().getTimestamp());
		timestamps.add(segment.getFirst().getTimestamp().plus(segment.getDuration().getMillis() / 2));
		timestamps.add(segment.getSecond().getTimestamp());
		SequentialTimestampTrackpointSeeker.Builder builder = (Builder) new SequentialTimestampTrackpointSeeker.Builder()
				.setTargetTimestamps(timestamps);
		assertMidway(builder, segment, P2);
	}

	@Test
	public void testSequentialTimestampExactMatch() {
		final SimpleTrackSegment segment = P1_P3_SEGMENT;
		ArrayList<DateTime> timestamps = setupTimestamps(segment);
		SequentialTimestampTrackpointSeeker.Builder builder = (Builder) new SequentialTimestampTrackpointSeeker.Builder()
				.setTargetTimestamps(timestamps);
		assertExactMatch(builder, segment);
	}

	/**
	 * @param segment
	 * @return
	 */
	private static ArrayList<DateTime> setupTimestamps(final SimpleTrackSegment segment) {
		ArrayList<DateTime> timestamps = new ArrayList<DateTime>();
		timestamps.add(segment.getFirst().getTimestamp());
		timestamps.add(segment.getSecond().getTimestamp());
		return timestamps;
	}
}
