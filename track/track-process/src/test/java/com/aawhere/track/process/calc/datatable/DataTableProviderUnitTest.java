/**
 *
 */
package com.aawhere.track.process.calc.datatable;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Locale;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.person.pref.PrefUtil;
import com.aawhere.personalize.DateTimePersonalizer;
import com.aawhere.personalize.QuantityPersonalizer;
import com.aawhere.test.TestUtil;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.TrackpointTestUtil;
import com.aawhere.track.process.calc.ProcessingCalculatorFactory;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;

import com.google.visualization.datasource.datatable.DataTable;
import com.google.visualization.datasource.datatable.TableRow;
import com.google.visualization.datasource.datatable.value.Value;
import com.google.visualization.datasource.datatable.value.ValueType;
import com.ibm.icu.util.Calendar;

/**
 * Unit Test for DataTableProvider.
 * 
 * @author Brian Chapman
 * 
 */
public class DataTableProviderUnitTest {

	AutomaticTrackProcessor.Builder processorBuilder;
	List<Trackpoint> trackpoints;
	DateTableCellProvider dateProvider;
	QuantityTableCellProvider<Length> distanceProvider;
	Configuration prefs;

	@Before
	public void setUp() throws Exception {
		prefs = PrefUtil.getPreferences(Locale.US, PrefUtil.getDefaultUsCustomaryPreferences());
		trackpoints = TrackpointTestUtil.generateOrderedTrackpoints();
		processorBuilder = new AutomaticTrackProcessor.Builder(trackpoints);
		dateProvider = new DateTableCellProvider.Builder().setColumnLabel(TableCellMessage.DATE_LABEL)
				.setPersonalizer(new DateTimePersonalizer.Builder().setPreferences(prefs).build())
				.registerWithProcessor(processorBuilder).build();
		distanceProvider = new QuantityTableCellProvider.Builder<Length>()
				.provider(ProcessingCalculatorFactory.getInstance().newTotalDistanceCalculator(processorBuilder))
				.columnId("distance").setColumnLabel(TableCellMessage.DISTANCE_LABEL)
				.setPersonalizer(new QuantityPersonalizer.Builder().setPreferences(prefs).build())
				.registerWithProcessor(processorBuilder).build();
	}

	@Test
	public void testBaseTableCellProvider() {
		DataTableProvider dataTableProvider = new DataTableProvider.Builder().registerWithProcessor(processorBuilder)
				.setProvider(dateProvider).setProvider(distanceProvider).build();
		processorBuilder.build();

		assertTrue(dateProvider.hasCell());
		assertNotNull(dateProvider.getCell());
		assertEquals(TableCellMessage.DATE_LABEL, dateProvider.getColumnLabel());
	}

	@Test
	public void testDataTableCellProvider() {
		DataTableProvider dataTableProvider = new DataTableProvider.Builder().registerWithProcessor(processorBuilder)
				.setProvider(dateProvider).setProvider(distanceProvider).build();
		processorBuilder.build();

		assertEquals(ValueType.DATETIME, dateProvider.getColumnType());
		assertEquals("date", dateProvider.getColumnId());

		assertTrue(dataTableProvider.hasDataTable());
		DataTable dt = dataTableProvider.getDataTable();
		testDataTable(trackpoints, dt);
	}

	private <Q extends Quantity<Q>> void compareInUnit(Q q, Unit<Q> u, Value v) {
		TestUtil.assertDoubleEquals(q.doubleValue(u), (Double) v.getObjectToFormat());
	}

	public void testDataTable(List<Trackpoint> trackpoints, DataTable dt) {
		assertNotNull(dt);
		assertEquals(trackpoints.size(), dt.getNumberOfRows());

		TableRow tr = dt.getRow(0);
		assertNotNull(tr);
		final int indexOfDate = 0;
		final int indexOfDistance = 1;
		assertEquals(trackpoints.get(0).getTimestamp().getMillis(), ((Calendar) tr.getCell(indexOfDate).getValue()
				.getObjectToFormat()).getTimeInMillis());
		compareInUnit(MeasurementUtil.ZERO_LENGTH, USCustomarySystem.MILE, tr.getCell(indexOfDistance).getValue());

		tr = dt.getRow(1);
		compareInUnit(TrackUtil.distanceBetween(trackpoints.get(0), trackpoints.get(1)), USCustomarySystem.MILE, tr
				.getCell(indexOfDistance).getValue());

	}
}
