package com.aawhere.track.process.calc;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor.Builder;

public class GoogleMapsEncodedPathCalculatorUnitTest {

	GoogleMapsEncodedPathCalculator calc;

	@Before
	public void setUp() {
		Builder processorBuilder = new ManualTrackProcessor.Builder();
		calc = new GoogleMapsEncodedPathCalculator.Builder().registerWithProcessor(processorBuilder).build();
	}

	@Test
	public void testLineTrack() {
		ArrayList<Trackpoint> trackpoints = createTestTrackPoints();

		for (Trackpoint point : trackpoints) {
			calc.handle(point, null);
		}

		String result = calc.getEncodedPath();
		String expectedResult = "_p~iF~ps|U_ulLnnqC_mqNvxq`@";
		assertEquals(expectedResult, result);
	}

	/*
	 * Test points and resulting encodedPath come from {@link
	 * http://code.google.com/apis/maps/documentation/utilities/polylinealgorithm.html}
	 */
	private ArrayList<Trackpoint> createTestTrackPoints() {
		ArrayList<Trackpoint> list = new ArrayList<Trackpoint>();
		long timeInMills = 10000; // Isn't used.
		list.add(TrackUtil.createPoint(timeInMills, 38.5, -120.2));
		list.add(TrackUtil.createPoint(timeInMills, 40.7, -120.95));
		list.add(TrackUtil.createPoint(timeInMills, 43.252, -126.453));
		return list;
	}
}
