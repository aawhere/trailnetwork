package com.aawhere.track.process.calc;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.BoundingBox;
import com.aawhere.test.TestUtil;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor.Builder;

public class BoundingBoxCalculatorUnitTest {

	BoundingBoxCalculator calc;
	ManualTrackProcessor proc;

	@Before
	public void setUp() {
		Builder processorBuilder = new ManualTrackProcessor.Builder();
		calc = new BoundingBoxCalculator.Builder().registerWithProcessor(processorBuilder).build();
		proc = processorBuilder.build();
	}

	@Test
	public void testBox() {
		ArrayList<Trackpoint> trackpoints = createTestTrack();

		for (Trackpoint point : trackpoints) {
			proc.notifyTrackpointListeners(point);
		}
		proc.finish();

		BoundingBox box = calc.getBoundingBox();
		assertNotNull(box);
		assertNotNull(box.getNorthEast());
		assertNotNull(box.getSouthWest());

	}

	@Test
	public void testTestTrack() {
		ArrayList<Trackpoint> trackpoints = createTestTrack();
		processTracks(trackpoints);

		TestUtil.assertDoubleEquals(LAT_N, calc.getNorth().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LAT_S, calc.getSouth().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LON_NEG, calc.getWest().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LON_POS, calc.getEast().getInDecimalDegrees());
	}

	@Test
	public void testTestTrackFlippingLatitude() {
		ArrayList<Trackpoint> trackpoints = createTestTrack180Meridian();
		processTracks(trackpoints);

		TestUtil.assertDoubleEquals(LAT_N, calc.getNorth().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LAT_S, calc.getSouth().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LON_FAR_WEST, calc.getEast().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LON_FAR_EAST, calc.getWest().getInDecimalDegrees());
	}

	@Test
	public void testTestTrackNorthPole() {
		ArrayList<Trackpoint> trackpoints = createTestTrackNorthPole();
		processTracks(trackpoints);

		TestUtil.assertDoubleEquals(LAT_90, calc.getNorth().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LAT_FAR_NORTH, calc.getSouth().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LON_180, calc.getWest().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LON_POS_90, calc.getEast().getInDecimalDegrees());
	}

	@Test
	public void testTestTrack180Meridian2() {
		ArrayList<Trackpoint> trackpoints = createTestTrack180Meridian2();
		processTracks(trackpoints);

		TestUtil.assertDoubleEquals(LAT_N, calc.getNorth().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LAT_N, calc.getSouth().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LON_FAR_EAST, calc.getWest().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LON_FAR_WEST, calc.getEast().getInDecimalDegrees());
	}

	private void processTracks(ArrayList<Trackpoint> trackpoints) {
		for (Trackpoint point : trackpoints) {
			proc.notifyTrackpointListeners(point);
		}
		proc.finish();
	}

	private ArrayList<Trackpoint> createTestTrack() {
		ArrayList<Trackpoint> list = new ArrayList<Trackpoint>();
		long timeInMills = 10000; // Isn't used.
		list.add(TrackUtil.createPoint(timeInMills, LAT_N, LON_ZERO));
		list.add(TrackUtil.createPoint(timeInMills, LAT_ZERO, LON_POS));
		list.add(TrackUtil.createPoint(timeInMills, LAT_S, LON_ZERO));
		list.add(TrackUtil.createPoint(timeInMills, LAT_ZERO, LON_NEG));
		return list;
	}

	private ArrayList<Trackpoint> createTestTrack180Meridian() {
		ArrayList<Trackpoint> list = new ArrayList<Trackpoint>();
		long timeInMills = 10000; // Isn't used.
		list.add(TrackUtil.createPoint(timeInMills, LAT_N, LON_180));
		list.add(TrackUtil.createPoint(timeInMills, LAT_ZERO, LON_FAR_WEST));
		list.add(TrackUtil.createPoint(timeInMills, LAT_S, LON_180));
		list.add(TrackUtil.createPoint(timeInMills, LAT_ZERO, LON_FAR_EAST));
		return list;
	}

	private ArrayList<Trackpoint> createTestTrack180Meridian2() {
		ArrayList<Trackpoint> list = new ArrayList<Trackpoint>();
		long timeInMills = 10000; // Isn't used.
		list.add(TrackUtil.createPoint(timeInMills, LAT_N, LON_180));
		list.add(TrackUtil.createPoint(timeInMills, LAT_N, LON_FAR_WEST2));
		list.add(TrackUtil.createPoint(timeInMills, LAT_N, LON_FAR_EAST2));
		list.add(TrackUtil.createPoint(timeInMills, LAT_N, LON_FAR_WEST));
		list.add(TrackUtil.createPoint(timeInMills, LAT_N, LON_FAR_EAST));
		return list;
	}

	private ArrayList<Trackpoint> createTestTrackNorthPole() {
		ArrayList<Trackpoint> list = new ArrayList<Trackpoint>();
		long timeInMills = 10000; // Isn't used.
		list.add(TrackUtil.createPoint(timeInMills, LAT_FAR_NORTH, LON_POS_90));
		list.add(TrackUtil.createPoint(timeInMills, LAT_90, LON_ZERO));
		list.add(TrackUtil.createPoint(timeInMills, LAT_FAR_NORTH, LON_NEG_90));
		list.add(TrackUtil.createPoint(timeInMills, LAT_90, LON_180));
		return list;
	}

	private final Double LAT_N = 30D;
	private final Double LAT_S = -30D;
	private final Double LAT_ZERO = 0D;
	private final Double LAT_FAR_NORTH = 80D;
	private final Double LAT_90 = 90D;
	private final Double LON_ZERO = 0D;
	private final Double LON_POS = 10D;
	private final Double LON_NEG = -10D;
	private final Double LON_180 = 180D;
	private final Double LON_FAR_EAST = 175D;
	private final Double LON_FAR_WEST = -175D;
	private final Double LON_FAR_EAST2 = 177D;
	private final Double LON_FAR_WEST2 = -177D;
	private final Double LON_POS_90 = 90D;
	private final Double LON_NEG_90 = -90D;
}
