/**
 *
 */
package com.aawhere.track.process.seek;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.DuelingTrackpointSeekerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeeker;

/**
 * A {@link TrackpointSeeker} that iterates through the target track for each trackpoint received.
 * This is required when doing Longest Common Subsequence analysis between two tracks.
 * 
 * This type of seeker is only useful during testing since it's performance in a system with real
 * tracks would be crippling. It remains in the test code for demonstration and testing purposes.
 * 2013-07 AR
 * 
 * @author Brian Chapman
 * 
 */
public class EveryTrackpointSeeker
		extends DuelingTrackpointSeekerBase
		implements TrackpointListener {

	/**
	 * Passes through any trackpoint that follows another by the
	 * {@link #minDurationBetweenTrackpoints}.
	 * 
	 * @see com.aawhere.track.process.TrackpointListener #handle(com.aawhere.track.Trackpoint,
	 *      com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint sourceTrackpoint, TrackProcessor processor) {
		for (Trackpoint trackpoint : getTargetTrack()) {
			desiredTrackpointFound(trackpoint);
		}
		getReceiver().reset();
	}

	public static class Builder
			extends DuelingTrackpointSeekerBase.Builder<EveryTrackpointSeeker, Builder> {
		public Builder() {
			super(new EveryTrackpointSeeker());
		}
	}

	private EveryTrackpointSeeker() {
	}

	public static Builder create() {
		return new Builder();
	}

}
