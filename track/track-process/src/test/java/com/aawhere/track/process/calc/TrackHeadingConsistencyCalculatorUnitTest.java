package com.aawhere.track.process.calc;

import static org.junit.Assert.*;

import java.util.List;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleRealisticTracks;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.SupplierMultisetProcessorListener;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Range;

/**
 * Creating realistic tracks that travel in straight lines, turn and reverse directions this
 * challengest the {@link TrackHeadingConsitencyCalculator} to provide it's oppinion of a track
 * heading consistently or not.
 * 
 * @author aroller
 * 
 */
public class TrackHeadingConsistencyCalculatorUnitTest {

	private static final int BUFFER_SIZE = TrackHeadingConsitencyCalculator.BUFFER_SIZE;
	/** approximate length that will create enough trackpoints used to fill up the buffer. */
	private static final Length BUFFER_LENGTH = QuantityMath.create(ExampleRealisticTracks.SEGMENT_LENGTH)
			.times(BUFFER_SIZE).getQuantity();

	/** Consistent straight line should be great. */
	@Test
	public void testStraightLine() {
		List<Trackpoint> track = ExampleRealisticTracks.create().current().track();
		allGreat(track);
	}

	private void allGreat(List<Trackpoint> track) {
		int numberOfSegments = track.size() - 1;
		int expectedGreat = numberOfSegments;
		int expectedGood = 0;
		int expectedFair = 0;
		int expectedPoor = 0;
		assertConsistency(track, expectedGreat, expectedGood, expectedFair, expectedPoor);
	}

	private void assertConsistency(List<Trackpoint> track, int expectedGreat, int expectedGood) {
		assertConsistency(track, expectedGreat, expectedGood, 0, 0);
	}

	private void assertConsistency(List<Trackpoint> track, int expectedGreat, int expectedGood, int expectedFair) {
		assertConsistency(track, expectedGreat, expectedGood, expectedFair, 0);
	}

	private void assertConsistency(List<Trackpoint> track, int expectedGreat, int expectedGood, int expectedFair,
			int expectedPoor) {
		ImmutableMultiset<SignalQuality> expected = ImmutableMultiset.<SignalQuality> builder()
				.addCopies(SignalQuality.GREAT, expectedGreat).addCopies(SignalQuality.GOOD, expectedGood)
				.addCopies(SignalQuality.FAIR, expectedFair).addCopies(SignalQuality.POOR, expectedPoor).build();
		assertConsistency(track, expected);
	}

	/** Reversing direction is common and shouldn't cause any issue. */
	@Test
	public void testSingleReverse() {
		List<Trackpoint> track = ExampleRealisticTracks.create().start().segments(BUFFER_SIZE).repeat().reverse()
				.track();
		allGreat(track);
	}

	/**
	 * Goes the default length, turns around quickly, then again quickly.. Could be indecision or
	 * bad signal.
	 * 
	 */
	@Test
	public void testQuickDoubleReverse() {
		List<Trackpoint> track = ExampleRealisticTracks.create().
		// forward the length of the buffer
				start().segments(BUFFER_SIZE).
				// reverse a couple of segments
				instruct().reverse().segments(2).
				// continue the length of the buffer
				instruct().segments(BUFFER_SIZE).track();
		assertConsistency(track, 18, 0, 7);
	}

	@Test
	public void testDoubleDoubleReverse() {
		List<Trackpoint> track = ExampleRealisticTracks.create().
		// forward the length of the buffer
				start().segments(BUFFER_SIZE).
				// 1 reverse a couple of segments
				instruct().reverse().segments(2)
				// 2
				.repeat().reverse()
				// 3
				.repeat().reverse()
				// 4
				.repeat().reverse().track();
		assertConsistency(track, 14, 0, 3, 6);
	}

	@Test
	public void testBeginsWithChaos() {
		List<Trackpoint> track = ExampleRealisticTracks.create().start().segments(2).east().repeat().west().repeat()
				.south().repeat().east().repeat().north().repeat().south().track();
		assertConsistency(track, 4, 3, 2, 6);
	}

	/**
	 * Consistently turning around in a circle. should be similar to a reverse
	 * 
	 * <pre>
	 *  ^
	 *  | ___
	 *  | | |
	 *  |_|_|
	 *    |
	 *    |
	 * </pre>
	 * */
	@Test
	public void testFullTurn() {
		List<Trackpoint> track = ExampleRealisticTracks.create()
		// start north
				.start().north().segments(BUFFER_SIZE)
				// small east
				.instruct().segments(2).east()
				// small south
				.repeat().south()
				// small west
				.repeat().west()
				// long north
				.instruct().north().segments(BUFFER_SIZE).track();
		assertConsistency(track, 16, 3, 7, 3);
	}

	private void assertConsistency(List<Trackpoint> track, ImmutableMultiset<SignalQuality> expected) {
		AutomaticTrackProcessor.Builder processorBuilder = AutomaticTrackProcessor.create(track);
		TrackHeadingConsitencyCalculator calculator = TrackHeadingConsitencyCalculator.create()
				.registerWithProcessor(processorBuilder).counter().build();
		SupplierMultisetProcessorListener<SignalQuality> qualityListener = calculator.counter();
		processorBuilder.build();

		TestUtil.assertSize(Range.atLeast(BUFFER_SIZE), track);
		Multiset<SignalQuality> qualityCounter = qualityListener.get();
		assertEquals("quality count", expected, qualityCounter);
	}
}
