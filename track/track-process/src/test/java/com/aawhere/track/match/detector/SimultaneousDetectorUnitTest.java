/**
 *
 */
package com.aawhere.track.match.detector;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.track.SimpleTrackpoint;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.Trackpoint;

/**
 * @author Brian Chapman
 * 
 */
public class SimultaneousDetectorUnitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDetector() {
		// only 50% overlap, need SimultaneousDetector.MIN_OVERLAP_TO_BE_CONSIDERED_TOGETHER
		assertDetector(P1, P2, P1, P3, false);
	}

	@Test
	public void testDetector2() {
		// No overlap
		assertDetector(P1, P2, P3, P4, false);
	}

	@Test
	public void testDetector3() {
		// 100% overlap
		assertDetector(P1, P2, P1, P2, true);
	}

	@Test
	public void testDetector4() {
		Trackpoint start = P1;
		Trackpoint finish = P2;
		Trackpoint secondFinish = findBorderCondition(start, finish, P3);
		secondFinish = shiftTime(secondFinish, 1l);
		// one second smaller than SimultaneousDetector.MIN_OVERLAP_TO_BE_CONSIDERED_TOGETHER
		// overlap.
		assertDetector(start, finish, start, secondFinish, false);
	}

	@Test
	public void testDetector5() {
		Trackpoint start = P1;
		Trackpoint finish = P2;
		Trackpoint secondFinish = findBorderCondition(start, finish, P3);
		// exactly SimultaneousDetector.MIN_OVERLAP_TO_BE_CONSIDERED_TOGETHER overlap.
		assertDetector(start, finish, start, secondFinish, true);
	}

	@Test
	public void testDetector6() {
		Trackpoint start = P1;
		Trackpoint finish = P2;
		Trackpoint secondFinish = findBorderCondition(start, finish, P3);
		secondFinish = shiftTime(secondFinish, -1l);
		// one second larger than SimultaneousDetector.MIN_OVERLAP_TO_BE_CONSIDERED_TOGETHER
		// overlap.
		assertDetector(start, finish, start, secondFinish, true);
	}

	@Test
	public void testDetector7() {
		// times abut.
		assertDetector(P1, P2, P2, P3, false);
	}

	private void assertDetector(Trackpoint start1, Trackpoint finish1, Trackpoint start2, Trackpoint finish2,
			Boolean expected) {
		TrackSummary source = TrackSummary.create().populatePoints(start1, finish1).setStart(start1).setFinish(finish1)
				.build();
		TrackSummary target = TrackSummary.create().populatePoints(start2, finish2).setStart(start2).setFinish(finish2)
				.build();
		SimultaneousDetector detector = SimultaneousDetector.create().setSourceTrackSummary(source)
				.setTargetTrackSummary(target).build();

		Boolean isSimulataneous = detector.isSimultaneous();
		assertTrue(expected.equals(isSimulataneous));
	}

	/**
	 * Given the two {@link Trackpoint}s, find a third Trackpoint such that the interval between the
	 * two trackpoints is {@link SimultaneousDetector#MIN_OVERLAP_TO_BE_CONSIDERED_TOGETHER} of the
	 * interval between the start and new Trackpoints.
	 * 
	 * @param start
	 * @param finish
	 * @return
	 */
	private Trackpoint findBorderCondition(Trackpoint start, Trackpoint finish, Trackpoint tp2Change) {
		// System.out.println("start: " + start.getTimestamp().getMillis());
		// System.out.println("finish: " + finish.getTimestamp().getMillis());
		long timeToShift = (long) ((long) (P2.getTimestamp().getMillis() - P1.getTimestamp().getMillis() + SimultaneousDetector.MIN_OVERLAP_TO_BE_CONSIDERED_TOGETHER
				* P1.getTimestamp().getMillis()) / SimultaneousDetector.MIN_OVERLAP_TO_BE_CONSIDERED_TOGETHER);
		// System.out.println("secondFinish: " + timeToShift);
		return SimpleTrackpoint.copy(finish).setTimestamp(new DateTime(timeToShift)).build();
	}

	private Trackpoint shiftTime(Trackpoint tp, Long timeToShift) {
		DateTime modifiedTimeStamp = tp.getTimestamp().plus(timeToShift);
		return SimpleTrackpoint.create().setElevation(tp.getElevation()).setLocation(tp.getLocation())
				.setTimestamp(modifiedTimeStamp).build();
	}
}
