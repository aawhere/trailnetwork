/**
 * 
 */
package com.aawhere.track.process.processor;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.seek.PassthroughTrackpointSeeker;

/**
 * Tests {@link SemiAutomaticTrackProcessor}.
 * 
 * @author aroller
 * 
 */
public class SemiAutomaticTrackProcessorUnitTest {

	Track track = ExampleTracks.LINE;
	Integer numberOfPoints = ExampleTracks.LINE_POINTS.size();
	private SemiAutomaticTrackProcessor.Builder processorBuilder;
	private TrackpointCounter counter;
	private SemiAutomaticTrackProcessor processor;

	@Before
	public void setUp() {

		processorBuilder = SemiAutomaticTrackProcessor.createSemiAutomatic();
		processorBuilder.setTrack(track);
		counter = TrackpointCounter.create().registerWithProcessor(processorBuilder).build();

	}

	void buildProcessor() {
		this.processor = processorBuilder.build();
	}

	@Test
	public void testHaltAtEveryPoint() {
		processorBuilder.registerLimitSeeker(PassthroughTrackpointSeeker.create());

		buildProcessor();
		assertEquals("first iteration is ran during build", 1, counter.getCount().intValue());
		assertTrue("There are 3", processor.resume());
		assertEquals("stop at 2", 2, counter.getCount().intValue());
		assertFinished();

	}

	/**
	 * 
	 */
	private void assertFinished() {
		assertFalse("should have reached the end", processor.resume());
		assertEquals("only 3", numberOfPoints, counter.getCount());
	}

	@Test
	public void testNoHalt() {
		// don't register any seeker
		buildProcessor();
		assertFinished();
	}
}
