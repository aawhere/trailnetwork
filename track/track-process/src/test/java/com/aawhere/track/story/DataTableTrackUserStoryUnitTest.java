/**
 *
 */
package com.aawhere.track.story;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Before;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.person.pref.PrefUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleRealisticTracks;
import com.aawhere.track.TrackSummary;

import com.google.common.collect.Range;
import com.google.visualization.datasource.datatable.DataTable;
import com.google.visualization.datasource.datatable.TableCell;
import com.google.visualization.datasource.datatable.TableRow;

/**
 * Unit test for {@link DataTableTrackUserStory}
 * 
 * @author Brian Chapman
 * 
 */
public class DataTableTrackUserStoryUnitTest
		extends BaseTrackUserStoryUnitTest<DataTableTrackUserStory, DataTable> {

	private static final int LENGTH_IN_METERS = 20000;

	/**
	 * @param track
	 */
	public DataTableTrackUserStoryUnitTest() {
		super(ExampleRealisticTracks.create().atLeast(MeasurementUtil.createLengthInMeters(LENGTH_IN_METERS)).build()
				.track());
	}

	private DataTableTrackUserStory story;
	private Integer numberOfTrackpoints = 100;
	private TrackSummary summary;

	@Before
	public void setUpTs() {
		this.summary = TrackSummaryUserStoriesTestUtil.create(track);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.story.BaseTrackUserStoryUnitTest#addUserStories(com.aawhere.track.story
	 * .TrackUserStoryBuilder)
	 */
	@Override
	protected void addUserStories(TrackUserStories.Builder builder) {
		DataTableTrackUserStory.Builder dtBuilder = new DataTableTrackUserStory.Builder()
				.setPreferences(PrefUtil.getPreferences(Locale.US, PrefUtil.getDefaultUsCustomaryPreferences()))
				.setMaxNumberOfTrackpoints(this.numberOfTrackpoints)
				.setSeekerChainBuilder(builder.getSeekerChainBuilder()).setSummary(summary);
		this.story = builder.setUserStory(dtBuilder);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.story.BaseTrackUserStoryUnitTest#assertCreated()
	 */
	@Override
	protected void assertCreated() {
		DataTable result = story.getResult();
		assertNotNull(result);

		TableRow tr = result.getRow(0);
		assertNotNull(tr);
		assertNotNull(tr.getCell(0).getValue());
		assertNotNull(tr.getCell(1).getValue());
		TestUtil.assertRange(	"shouldn't exceed requested points",
								Range.closed(numberOfTrackpoints / 10, this.numberOfTrackpoints),
								result.getNumberOfRows());
		TableCell firstElevation = tr.getCell(0);
		assertEquals(0.0, Double.parseDouble(firstElevation.getValue().toQueryString()), 0.1);
		TableCell firstDistance = tr.getCell(0);
		assertEquals(0.0, Double.parseDouble(firstDistance.getValue().toQueryString()), 0.1);

	}

}
