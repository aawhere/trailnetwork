/**
 *
 */
package com.aawhere.track.process.calc;

import static com.aawhere.track.TrackpointTestUtil.*;
import static org.junit.Assert.*;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.track.SimpleTrack;
import com.aawhere.track.SimpleTrackpoint;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor;

/**
 * Validates the function of {@link TrackElapsedDurationCalculator}.
 * 
 * @author roller
 * 
 */
public class TrackDurationCalculatorUnitTest {

	@Test
	public void testBasics() {

		TrackElapsedDurationCalculator calculator = new TrackElapsedDurationCalculator.Builder()
				.registerWithProcessor(new ManualTrackProcessor.Builder()).build();
		assertNull(calculator.getStartTime());
		assertEquals(new Duration(0), calculator.getDuration());
		assertNull(calculator.getEndTime());

		Trackpoint trackpoint = generateRandomTrackpoint();
		calculator.handle(trackpoint, null);
		assertEquals(trackpoint.getTimestamp(), calculator.getStartTime());
		assertEquals(trackpoint.getTimestamp(), calculator.getEndTime());
		assertEquals(new Duration(0), calculator.getDuration());

		Interval interval = calculator.getInterval();
		assertEquals(trackpoint.getTimestamp(), interval.getStart());
		assertEquals(trackpoint.getTimestamp(), interval.getEnd());

		final int ONE_MINUTE = 60000;
		Duration duration = new Duration(TestUtil.generateRandomLong(ONE_MINUTE));
		DateTime secondTimestamp = trackpoint.getTimestamp().plus(duration);
		SimpleTrackpoint secondTrackpoint = SimpleTrackpoint.copy(trackpoint).setTimestamp(secondTimestamp).build();

		calculator.handle(secondTrackpoint, null);
		assertEquals(trackpoint.getTimestamp(), calculator.getStartTime());
		assertEquals(secondTimestamp, calculator.getEndTime());
		assertEquals(duration, calculator.getDuration());
		assertEquals(secondTimestamp, calculator.getInterval().getEnd());

	}

	@Test
	public void testDuration() {
		List<Trackpoint> trackpoints = generateOrderedTrackpoints();
		Track track = new SimpleTrack.Builder(trackpoints).build();
		AutomaticTrackProcessor.Builder processorBuilder = new AutomaticTrackProcessor.Builder(track);
		TrackElapsedDurationCalculator listener = new TrackElapsedDurationCalculator.Builder()
				.registerWithProcessor(processorBuilder).build();
		processorBuilder.build();
		DateTime startTime = trackpoints.get(0).getTimestamp();
		assertEquals(startTime, listener.getStartTime());
		DateTime endTime = trackpoints.get(trackpoints.size() - 1).getTimestamp();
		assertEquals(endTime, listener.getEndTime());

		assertEquals(new Duration(startTime, endTime), listener.getDuration());
	}
}
