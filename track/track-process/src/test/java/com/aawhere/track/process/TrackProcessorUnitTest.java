/**
 *
 */
package com.aawhere.track.process;

import static com.aawhere.track.TrackpointTestUtil.generateRandomTrackpoints;
import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.SimpleTrack;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.calc.TrackSegmentCounter;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.AutomaticTrackProcessor.Builder;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.MultipleTracksProcessor;

/**
 * Basic tests to ensure the {@link AutomaticTrackProcessor} is working properly for core functions.
 * 
 * @author roller
 * 
 */
public class TrackProcessorUnitTest {

	public static class BogusListener
			extends ProcessorListenerBase
			implements TrackpointListener {

		/**
		 * Used to construct all instances of TrackProcessorUnitTest.BogusListener.
		 */
		public static class Builder
				extends ProcessorListenerBase.Builder<TrackProcessorUnitTest.BogusListener, Builder> {

			public Builder() {
				super(new TrackProcessorUnitTest.BogusListener());
			}

			public Builder setTheTruth(Boolean theTruth) {
				building.theTruth = theTruth;
				return this;
			}

		}// end Builder

		public Boolean theTruth;

		/** Use {@link Builder} to construct TrackProcessorUnitTest.BogusListener */
		private BogusListener() {
		}

		/** Used to demonstrate the importance of equality with processor listeners. */
		@Override
		public boolean equals(Object obj) {

			Boolean result = false;
			if (obj instanceof BogusListener) {
				BogusListener that = (BogusListener) obj;
				result = this.theTruth.equals(that.theTruth);
			}
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
		 * com.aawhere.track.process.TrackProcessor)
		 */
		@Override
		public void handle(Trackpoint trackpoint, TrackProcessor processor) {
			// TODO Auto-generated method stub

		}
	}

	private void assertCounters(List<Trackpoint> trackpoints, TrackSegmentCounter segmentCounter,
			TrackpointCounter trackpointCounter) {
		assertEquals(trackpoints.size(), (int) trackpointCounter.getCount());
		assertEquals(trackpoints.size() - 1, (int) segmentCounter.getCount());
	}

	@Test
	public void testAutomaticProcessor() {

		List<Trackpoint> trackpoints = generateRandomTrackpoints();
		Track track = new SimpleTrack.Builder(trackpoints).build();
		TrackProcessorBase.Builder<AutomaticTrackProcessor> processorBuilder = new AutomaticTrackProcessor.Builder(
				track);
		TrackSegmentCounter segmentCounter = new TrackSegmentCounter.Builder().registerWithProcessor(processorBuilder)
				.build();
		TrackpointCounter trackpointCounter = new TrackpointCounter.Builder().registerWithProcessor(processorBuilder)
				.build();

		processorBuilder.build();

		assertCounters(trackpoints, segmentCounter, trackpointCounter);
	}

	/** Duplicate registration results in only a single registration */
	@Test
	public void testDuplicateRegistration() {
		Builder processorBuilder = new AutomaticTrackProcessor.Builder(ExampleTracks.LINE);
		TrackpointCounter counter1 = new TrackpointCounter.Builder().registerWithProcessor(processorBuilder).build();
		TrackpointCounter counter2 = new TrackpointCounter.Builder().registerWithProcessor(processorBuilder).build();
		assertSame("counter1 is supposed to be the only remaining survivor", counter1, counter2);
		processorBuilder.build();
	}

	@Test
	public void testDuplicateRegistrationUniqueProcessors() {
		Builder processorBuilder = new AutomaticTrackProcessor.Builder(ExampleTracks.LINE);
		BogusListener bogus1 = new BogusListener.Builder().setTheTruth(false).registerWithProcessor(processorBuilder)
				.build();
		BogusListener bogus2 = new BogusListener.Builder().setTheTruth(true).registerWithProcessor(processorBuilder)
				.build();
		// same, but not equals
		assertNotSame("these are supposed to be unique since they are different", bogus1, bogus2);
		TestUtil.assertNotEquals(bogus1, bogus2);
		processorBuilder.build();
	}

	@Test
	public void testGetListener() {
		AutomaticTrackProcessor.Builder builder = new AutomaticTrackProcessor.Builder(ExampleTracks.LINE);
		final TrackpointCounter listener = new TrackpointCounter.Builder().registerWithProcessor(builder).build();
		ProcessorListener result = builder.build().getListener(listener.getClass());
		assertNotNull(result);
		assertEquals(listener.getClass(), result.getClass());
		assertEquals(listener, result);
	}

	@Test
	public void testManualProcessor() {

		List<Trackpoint> trackpoints = generateRandomTrackpoints();

		ManualTrackProcessor.Builder builder = new ManualTrackProcessor.Builder();
		TrackpointCounter trackpointCounter = new TrackpointCounter.Builder().registerWithProcessor(builder).build();
		TrackSegmentCounter segmentCounter = new TrackSegmentCounter.Builder().registerWithProcessor(builder).build();
		ManualTrackProcessor inspector = builder.build();

		for (Trackpoint trackpoint : trackpoints) {
			inspector.notifyTrackpointListeners(trackpoint);
		}

		assertCounters(trackpoints, segmentCounter, trackpointCounter);
	}

	@Test
	public void testMultipleTracksProcess() {
		MultipleTracksProcessor.Builder builder = new MultipleTracksProcessor.Builder();
		final TrackpointCounter counter = new TrackpointCounter.Builder().registerWithProcessor(builder).build();
		LinkedList<Track> tracks = new LinkedList<Track>();
		tracks.add(ExampleTracks.LINE);
		tracks.add(ExampleTracks.LINE_LONGER);
		Integer numberOfTrackpoints = ExampleTracks.LINE_POINTS.size() + ExampleTracks.LINE_LONGER_POINTS.size();
		builder.setTracks(tracks).build();

		assertEquals(numberOfTrackpoints, counter.getCount());
	}

	@Test(expected = RuntimeException.class)
	public void testProcessingBeforeBuildingListener() {
		Builder processorBuilder = new AutomaticTrackProcessor.Builder(ExampleTracks.LINE);
		// purposely forget to build
		new TrackpointCounter.Builder().registerWithProcessor(processorBuilder);
		processorBuilder.build();

	}

	@Test(expected = UnsupportedOperationException.class)
	public void testRegisteringWithBuiltProcessor() {
		Builder processorBuilder = new AutomaticTrackProcessor.Builder(ExampleTracks.LINE);
		processorBuilder.build();
		new TrackpointCounter.Builder().registerWithProcessor(processorBuilder).build();
	}

}
