/**
 *
 */
package com.aawhere.track.process.calc;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.measure.ElevationChange;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.provider.ElevationChangeSegmentProvider;

/**
 * @author Brian Chapman
 * 
 */
public class TestElevationGainSegmentProviderUnitTest {

	@Test
	public void testSingleSegment() {
		TrackSegment segment = ExampleTracks.S1;
		ElevationChangeSegmentProvider elevationGainCalculator = new ElevationChangeSegmentProvider();

		elevationGainCalculator.handle(segment, null);

		ElevationChange gain = elevationGainCalculator.getQuantity();
		assertEquals(segment.getElevationChange(), gain);
	}
}
