/**
 *
 */
package com.aawhere.track.match.detector;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.SimpleTrackpoint;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.Trackpoint;

/**
 * @author Brian Chapman
 * 
 */
public class SameLocationDetectorUnitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDetector1() {
		assertDetector(P1, P2, P1, P3, true, false);
	}

	@Test
	public void testDetector2() {
		assertDetector(P1, P3, P1, P2, true, false);
	}

	@Test
	public void testDetector3() {
		assertDetector(P1, P2, P1, P2, true, true);
	}

	@Test
	public void testDetectorSetsDistanceToBeConsideredTogether() {
		assertDetector(P1, P2, P1, P2, true, true, MeasurementUtil.createLengthInMeters(1d));
	}

	@Test
	public void testDetector4() {
		assertDetector(P1, P2, P3, P4, false, false);
	}

	@Test
	public void testDetectorDistance() {
		Trackpoint p2plus = shiftBy10Meters(P2, ShiftDirection.PLUS);
		Trackpoint p2minus = shiftBy10Meters(P2, ShiftDirection.MINUS);
		// Note that the calculations are NOT exact!!! this should pass with 10 meters, but the
		// error requires a little more.
		assertDetector(P1, P2, p2plus, p2minus, false, true, MeasurementUtil.createLengthInMeters(10.02d));
	}

	public void assertDetector(Trackpoint start1, Trackpoint finish1, Trackpoint start2, Trackpoint finish2,
			Boolean sameStart, Boolean sameFinish) {
		assertDetector(start1, finish1, start2, finish2, sameStart, sameFinish, SameLocationDetector.create());
	}

	public void assertDetector(Trackpoint start1, Trackpoint finish1, Trackpoint start2, Trackpoint finish2,
			Boolean sameStart, Boolean sameFinish, Length distanceSameLocation) {
		SameLocationDetector.Builder sameLocationDetectorBuilder = SameLocationDetector.create()
				.setDistanceToBeConsideredSameLocation(distanceSameLocation);
		assertDetector(start1, finish1, start2, finish2, sameStart, sameFinish, sameLocationDetectorBuilder);
	}

	public void assertDetector(Trackpoint start1, Trackpoint finish1, Trackpoint start2, Trackpoint finish2,
			Boolean sameStart, Boolean sameFinish, SameLocationDetector.Builder sameLocationDetectorBuilder) {
		TrackSummary source = TrackSummary.create().populatePoints(start1, finish1).build();
		TrackSummary target = TrackSummary.create().populatePoints(start2, finish2).build();

		SameLocationDetector sameLocationDetector = sameLocationDetectorBuilder.setSourceTrackSummary(source)
				.setTargetTrackSummary(target).build();

		assertTrue(	"Expecting Start Locations to be same? " + sameStart,
					sameStart.equals(sameLocationDetector.hasSameStart()));
		assertTrue(	"Expecting Finish Locations to be same? " + sameFinish,
					sameFinish.equals(sameLocationDetector.hasSameFinish()));
	}

	private Trackpoint shiftBy10Meters(Trackpoint tp, ShiftDirection direction) {
		// Degrees lon = to 10 meters, found iteratively using
		// http://www.movable-type.co.uk/scripts/latlong.html
		double degressEqualTo10Meters = 0.00008995;
		Longitude lon = tp.getLocation().getLongitude();
		double shiftedDegrees;
		if (direction == ShiftDirection.PLUS) {
			shiftedDegrees = lon.getInDecimalDegrees() + degressEqualTo10Meters;
		} else {
			shiftedDegrees = lon.getInDecimalDegrees() - degressEqualTo10Meters;
		}
		Angle shiftedAngle = MeasurementUtil.createAngleInDegrees(shiftedDegrees);
		Longitude lonShifted = MeasurementUtil.createLongitude(shiftedAngle);
		GeoCoordinate shiftedLocation = new GeoCoordinate(tp.getLocation().getLatitude(), lonShifted);
		return SimpleTrackpoint.copy(tp).setLocation(shiftedLocation).build();
	}

	enum ShiftDirection {
		PLUS, MINUS;
	}
}
