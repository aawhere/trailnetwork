/**
 *
 */
package com.aawhere.track.match;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.assertEquals;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import org.junit.Test;

import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.match.TracksSimultaneousMetListener;
import com.aawhere.track.match.TracksSimultaneousMetListener.Builder;
import com.aawhere.track.process.processor.ManualTrackProcessor;

/**
 * Simple unit test for {@link TracksSimultaneousMetListener}.
 * 
 * @author Aaron Roller
 * 
 */
public class TracksSimultaneousMetListenerUnitTest {

	@Test
	public void testMetSamePoint() {

		Trackpoint sourcePoint = P1;
		Trackpoint targetPoint = P1;
		Boolean expectedMeeting = true;

		assertMet(sourcePoint, targetPoint, expectedMeeting);

	}

	private void assertMet(Trackpoint sourcePoint, Trackpoint targetPoint, Boolean expectedMeeting) {
		Length maxDistanceForTogether = MeasurementQuantityFactory.getInstance(Length.class).create(50,
																									MetricSystem.METRE);
		ManualTrackProcessor.Builder sourceProcessorBuilder = new ManualTrackProcessor.Builder();
		ManualTrackProcessor.Builder targetProcessorBuilder = new ManualTrackProcessor.Builder();

		TracksSimultaneousMetListener detector = new TracksSimultaneousMetListener.Builder()
				.registerWithSourceProcessor(sourceProcessorBuilder)
				.registerWithTargetProcessor(targetProcessorBuilder)
				.setMaxDistanceToBeConsideredTogether(maxDistanceForTogether).build();

		sourceProcessorBuilder.build().notifyTrackpointListeners(sourcePoint);
		targetProcessorBuilder.build().notifyTrackpointListeners(targetPoint);
		assertEquals(expectedMeeting, detector.isMet());
	}

	@Test
	public void testNoMeetFarOff() {

		assertMet(P1, DISTANT_LOCATION_T1, false);
	}

	@Test(expected = RuntimeException.class)
	public void testTimesNotMatching() {
		assertMet(P1, P2, false);
	}

}
