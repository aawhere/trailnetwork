/**
 * 
 */
package com.aawhere.track.process.calc;

import java.util.ArrayList;

import javax.measure.quantity.Length;

import org.apache.commons.math.stat.descriptive.summary.Sum;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.math.stats.Gain;
import com.aawhere.math.stats.Loss;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.ManualTrackProcessor;

/**
 * Test various Calculators created by the factory.
 * 
 * @author Brian Chapman
 * 
 */
public class ProcessingCalculatorFactoryUnitTest {

	ManualTrackProcessor.Builder processorBuilder;

	@Before
	public void setUp() {
		processorBuilder = new ManualTrackProcessor.Builder();
	}

	@Test
	public void TestElevationChangeCalculator() {
		QuantityUnivariateStatisticProcessorCalculator<ElevationChange, Sum> calc = ProcessingCalculatorFactory
				.getInstance().newElevationChangeCalculator(processorBuilder);

		testElevation(MeasurementUtil.createLengthInMeters(2D), calc);
	}

	@Test
	public void TestElevationGainCalculator() {
		QuantityUnivariateStatisticProcessorCalculator<ElevationChange, Gain> calc = ProcessingCalculatorFactory
				.getInstance().newElevationGainCalculator(processorBuilder);

		testElevation(MeasurementUtil.createLengthInMeters(3D), calc);
	}

	@Test
	public void TestElevationLossCalculator() {
		QuantityUnivariateStatisticProcessorCalculator<ElevationChange, Loss> calc = ProcessingCalculatorFactory
				.getInstance().newElevationLossCalculator(processorBuilder);

		testElevation(MeasurementUtil.createLengthInMeters(1D), calc);
	}

	private void
			testElevation(Length expected, QuantityUnivariateStatisticProcessorCalculator<ElevationChange, ?> calc) {
		testElevation(MeasurementUtil.createElevationChange(expected), calc);
	}

	private void testElevation(ElevationChange expected,
			QuantityUnivariateStatisticProcessorCalculator<ElevationChange, ?> calc) {
		ArrayList<Trackpoint> trackpoints = createTestTrackPoints();
		ManualTrackProcessor proc = processorBuilder.build();
		for (Trackpoint point : trackpoints) {
			proc.notifyTrackpointListeners(point);
		}
		proc.finish();

		ElevationChange result = calc.getQuantity();
		MeasureTestUtil.assertEquals(expected, result);
	}

	/*
	 * Test points and resulting encodedPath come from {@link http://code.google.
	 * com/apis/maps/documentation/utilities/polylinealgorithm.html}
	 */
	private ArrayList<Trackpoint> createTestTrackPoints() {
		ArrayList<Trackpoint> list = new ArrayList<Trackpoint>();
		long timeInMills = 10000; // Isn't used.
		list.add(TrackUtil.createPoint(timeInMills, 0, 0, 1));
		list.add(TrackUtil.createPoint(timeInMills, 0, 0, 2));
		list.add(TrackUtil.createPoint(timeInMills, 0, 0, 1));
		list.add(TrackUtil.createPoint(timeInMills, 0, 0, 3));
		return list;
	}
}
