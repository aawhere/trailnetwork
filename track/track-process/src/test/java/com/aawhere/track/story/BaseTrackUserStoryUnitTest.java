/**
 *
 */
package com.aawhere.track.story;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.calc.TrackSummaryCreatorListener;

/**
 * Provides common test methods between unit tests of {@link UserStory}s.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class BaseTrackUserStoryUnitTest<StoryT extends BaseTrackUserStory<ResultT>, ResultT> {

	Iterable<Trackpoint> track;

	public BaseTrackUserStoryUnitTest(Iterable<Trackpoint> track) {
		this.track = track;
	}

	/**
	 * Tests only the actions taken by the user story and relies on TrackSummary and
	 * {@link TrackSummaryCreatorListener} to be tested for it's own competency.
	 * 
	 */
	@Test
	public void testCreation() {
		TrackUserStories.Builder builder = TrackUserStories.create(track);
		addUserStories(builder);
		TrackUserStories stories = builder.build();
		for (UserStory<?> story : stories.getStories()) {
			if (!story.hasResult()) {
				fail("We should have a result");
			}
		}
		assertCreated();
	}

	protected abstract void addUserStories(TrackUserStories.Builder builder);

	protected abstract void assertCreated();
}
