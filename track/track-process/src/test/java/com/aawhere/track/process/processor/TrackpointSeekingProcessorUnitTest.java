/**
 *
 */
package com.aawhere.track.process.processor;

import static com.aawhere.track.TrackpointTestUtil.*;
import static org.junit.Assert.*;

import org.joda.time.Duration;
import org.junit.Test;

import com.aawhere.joda.time.InvalidIntervalRuntimeException;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.SimpleTrackpoint;
import com.aawhere.track.Track;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessorBase.Builder;
import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.calc.TrackElapsedDurationCalculator;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.seek.DurationTrackpointSeeker;
import com.aawhere.track.process.seek.TrackpointSeekerChainBuilder;
import com.aawhere.track.process.seek.TrackpointSeekerChainBuilderUnitTest;

/**
 * Tests {@link TrackpointSeekingProcessor} and other {@link TrackpointSeeker}s.
 * 
 * chaining seekers together can be confusing so {@link TrackpointSeekerChainBuilder} should be used
 * 
 * @see TrackpointSeekerChainBuilderUnitTest
 * 
 * @author roller
 * 
 */
public class TrackpointSeekingProcessorUnitTest {

	/**
	 * @see TrackpointSeekingProcessor.DuplicationDetector
	 * 
	 */
	@Test(expected = InvalidIntervalRuntimeException.class)
	public void testDuplicationDetector() {
		TrackpointSeekingProcessor.DuplicationDetector detector = new TrackpointSeekingProcessor.DuplicationDetector();
		Trackpoint base = generateRandomTrackpoint();
		Trackpoint resultOfFirstTrackpoint = detector.duplicateDetected(base);
		assertNull("First Trackpoint must always not detect a duplication", resultOfFirstTrackpoint);

		// now, send in the equivalent and be notified of duplicate
		Trackpoint copy = SimpleTrackpoint.copy(base).build();
		Trackpoint resultOfDuplicateDetected = detector.duplicateDetected(copy);
		assertNotNull(resultOfDuplicateDetected);
		assertNotSame(copy, resultOfDuplicateDetected);
		assertSame(base, resultOfDuplicateDetected);

		// now, send in same timestamp, but different values
		Trackpoint sameTimestampDifferentValue = TrackUtil.createPoint(base.getTimestamp());
		detector.duplicateDetected(sameTimestampDifferentValue);
		fail("should have thrown illegal argument because trackpoint with same timestamp, but different values detected");
	}

	@Test
	public void testDurationTrackpointSeeker() {

		assertDurationTrackpointSeeker(ExampleTracks.LINE);
		assertDurationTrackpointSeeker(ExampleTracks.LINE_LONGER);

	}

	/**
	 * @param sourceTrack
	 */
	private void assertDurationTrackpointSeeker(Track sourceTrack) {
		final int minDurationBetweenTrackpointsMillis = ExampleTracks.TIME_INCREMENT * 2;
		final Duration minDurationBetweenTrackpoints = new Duration(minDurationBetweenTrackpointsMillis);

		// setup reciever
		TrackpointSeekingProcessor.Builder receiverBuilder = new TrackpointSeekingProcessor.Builder();
		TrackpointCounter receiverCounter = new TrackpointCounter.Builder().registerWithProcessor(receiverBuilder)
				.build();

		Builder<AutomaticTrackProcessor> processorBuilder = new AutomaticTrackProcessor.Builder(sourceTrack);
		final TrackElapsedDurationCalculator durationCalculator = new TrackElapsedDurationCalculator.Builder()
				.registerWithProcessor(processorBuilder).build();
		// setup seeker
		final DurationTrackpointSeeker.Builder seekerBuilder = new DurationTrackpointSeeker.Builder();
		seekerBuilder.setMinDurationBetweenTrackpoints(minDurationBetweenTrackpoints);
		seekerBuilder.setReceiver(receiverBuilder.build());
		seekerBuilder.registerWithProcessor(processorBuilder);
		seekerBuilder.build();
		processorBuilder.build();
		// we always want to add one because regardless of the total duration
		// the first trackpoint is always counted.
		final int alwaysAtLeastOne = 1;
		// intentionally flooring since
		Integer expectedCount = (int) durationCalculator.getDuration().getMillis()
				/ minDurationBetweenTrackpointsMillis + alwaysAtLeastOne;
		assertEquals(expectedCount, receiverCounter.getCount());
	}

	@Test(expected = NullPointerException.class)
	public void testSeekerNotRegisteredWithProcessor() {
		TrackpointSeekingProcessor receiver = new TrackpointSeekingProcessor.Builder().build();
		new DurationTrackpointSeeker.Builder().setReceiver(receiver).build();

	}

	@Test(expected = NullPointerException.class)
	public void testSeekerNotRegisteredWithReceiver() {
		new DurationTrackpointSeeker.Builder().build();

	}

	@Test(expected = NullPointerException.class)
	public void testReceiverNotRegistered() {
		AutomaticTrackProcessor.Builder builder = new AutomaticTrackProcessor.Builder(ExampleTracks.LINE);
		new DurationTrackpointSeeker.Builder().registerWithProcessor(builder).build();
	}

}
