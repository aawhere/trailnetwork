/**
 * 
 */
package com.aawhere.track.process.seek;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.seek.filter.FrequencyTrackpointFilter;
import com.aawhere.track.process.seek.filter.ProcessFilterUnitTest;

/**
 * Tests the basic functions of seeker builder.
 * 
 * @author aroller
 * 
 *         For good use of the chain see...
 * @see ProcessFilterUnitTest#testFilterChain()
 */
public class TrackpointSeekerChainBuilderUnitTest {

	TrackpointSeekerChainBuilder<ManualTrackProcessor> builder;
	TrackpointCounter driverCounter;

	@Before
	public void setUp() {
		builder = TrackpointSeekerChainBuilder.create(new ManualTrackProcessor.Builder());
		driverCounter = builder.registerWithCurrentProcessor(new TrackpointCounter.Builder());
	}

	@Test
	public void testDriverOnly() {

		assertNotNull(driverCounter);
		ManualTrackProcessor driver = builder.build();
		assertNotNull(driver);
		assertCount(driver, null, null);
	}

	@Test
	public void testOneSeeker() {
		// default frequency is to skip every other.
		builder.addSeeker(new FrequencyTrackpointFilter.Builder());
		TrackpointCounter frequencyCounter = builder.registerWithCurrentProcessor(new TrackpointCounter.Builder());
		ManualTrackProcessor driver = builder.build();
		assertCount(driver, frequencyCounter, null);
	}

	@Test
	public void testTwoSeekers() {
		// default frequency is to skip every other.
		builder.addSeeker(new FrequencyTrackpointFilter.Builder());
		TrackpointCounter frequencyCounter = builder.registerWithCurrentProcessor(new TrackpointCounter.Builder());
		builder.addSeeker(new FrequencyTrackpointFilter.Builder());
		TrackpointCounter frequencyCounter2 = builder.registerWithCurrentProcessor(new TrackpointCounter.Builder());
		ManualTrackProcessor driver = builder.build();
		assertCount(driver, frequencyCounter, frequencyCounter2);
	}

	/**
	 * Verifys the frequency counters provided are getting filtered properly by the chain.
	 * 
	 * @param frequencyCounter
	 * @param driver
	 */
	private void assertCount(ManualTrackProcessor driver, TrackpointCounter frequencyCounter,
			TrackpointCounter frequencyCounter2) {

		// everyone should see the first trackpoint
		driver.notifyTrackpointListeners(P1);
		assertEquals("all should see the first", 1, this.driverCounter.getCount().intValue());
		if (frequencyCounter != null) {
			assertEquals("all should see the first", 1, frequencyCounter.getCount().intValue());
		}
		if (frequencyCounter2 != null) {
			assertEquals("all should see the first", 1, frequencyCounter2.getCount().intValue());
		}

		// the first filter should remove this one and the second shouldn't see at all
		driver.notifyTrackpointListeners(P2);
		assertEquals("driver should see all", 2, this.driverCounter.getCount().intValue());
		if (frequencyCounter != null) {
			assertEquals("this should be filtered out", 1, frequencyCounter.getCount().intValue());
		}
		if (frequencyCounter2 != null) {
			assertEquals("this should be filtered out by first filter", 1, frequencyCounter2.getCount().intValue());
		}

		// the first filter should see this one, but the second should filter it out
		driver.notifyTrackpointListeners(P3);
		assertEquals("driver should see all", 3, this.driverCounter.getCount().intValue());
		if (frequencyCounter != null) {
			assertEquals("this should have seen the current", 2, frequencyCounter.getCount().intValue());
		}
		if (frequencyCounter2 != null) {
			assertEquals("this should be filtered out by this filter", 1, frequencyCounter2.getCount().intValue());
		}

		// the first filter should filter this one out and the second shouldn't see at all
		driver.notifyTrackpointListeners(P4);
		assertEquals("driver should see all", 4, this.driverCounter.getCount().intValue());
		if (frequencyCounter != null) {
			assertEquals("this should be filtered out", 2, frequencyCounter.getCount().intValue());
		}
		if (frequencyCounter2 != null) {
			assertEquals("this should have been filtered out by previous", 1, frequencyCounter2.getCount().intValue());
		}

		// finally the second one should see this because the first filter saw it and the second
		// already skipped one.
		driver.notifyTrackpointListeners(P3_X2);
		assertEquals("driver should see all", 5, this.driverCounter.getCount().intValue());
		if (frequencyCounter != null) {
			assertEquals("this should be filtered out", 3, frequencyCounter.getCount().intValue());
		}
		if (frequencyCounter2 != null) {
			assertEquals("this should be seen since it has already filtered one", 2, frequencyCounter2.getCount()
					.intValue());
		}
	}
}
