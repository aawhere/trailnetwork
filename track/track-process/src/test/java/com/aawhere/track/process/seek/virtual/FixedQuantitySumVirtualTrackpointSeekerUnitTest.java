/**
 * 
 */
package com.aawhere.track.process.seek.virtual;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.measure.quantity.Length;
import javax.measure.unit.BaseUnit;
import javax.measure.unit.MetricSystem;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.ExampleRealisticTracks;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackProcessorUtil;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.calc.TrackCreatorTrackpointCalculator;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.provider.TrackSegmentDistanceProvider;
import com.aawhere.track.process.seek.TrackpointSeekerChainBuilder;
import com.aawhere.track.process.seek.virtual.FixedQuantitySumVirtualTrackpointSeeker.Builder;

import com.google.common.collect.Lists;

/**
 * Tests the generic {@link FixedQuantitySumVirtualTrackpointSeeker} using {@link Length} from
 * points provided in {@link ExampleTracks}.
 * 
 * @author aroller
 * 
 */
public class FixedQuantitySumVirtualTrackpointSeekerUnitTest {

	Length targetSum;
	HashMap<Index, Trackpoint> expectedTrackpoints = new HashMap<>();
	private List<Trackpoint> trackpoints = LINE_POINTS;

	@Before
	public void setUp() {

	}

	@After
	public void test() {
		AutomaticTrackProcessor.Builder processorBuilder = AutomaticTrackProcessor.create(this.trackpoints);
		TrackpointSeekerChainBuilder<AutomaticTrackProcessor> seekerChainBuilder = TrackpointSeekerChainBuilder
				.create(processorBuilder);
		TrackSegmentDistanceProvider segmentDistanceProvider = seekerChainBuilder
				.registerWithCurrentProcessor(TrackSegmentDistanceProvider.create());

		final Builder<Length> seekerBuilder = FixedQuantitySumVirtualTrackpointSeeker.create();
		seekerBuilder.targetSum(targetSum).provider(segmentDistanceProvider);
		seekerChainBuilder.addSeeker(seekerBuilder);
		TrackCreatorTrackpointCalculator trackCreator = seekerChainBuilder
				.registerWithCurrentProcessor(TrackCreatorTrackpointCalculator.create());
		AutomaticTrackProcessor processor = seekerChainBuilder.build();
		ArrayList<Trackpoint> resultTrack = Lists.newArrayList(trackCreator.getTrack());
		Trackpoint previous = null;
		final BaseUnit<Length> metre = MetricSystem.METRE;
		// last trackpoint may not equal the fixed distance
		for (int i = 0; i < resultTrack.size() - 1; i++) {
			Trackpoint trackpoint = resultTrack.get(i);
			if (previous != null) {
				Length distanceBetween = TrackUtil.distanceBetween(previous, trackpoint);
				// each segment must be within close enough tolerance
				assertEquals(	trackpoint.toString() + " #" + i,
								targetSum.doubleValue(metre),
								distanceBetween.doubleValue(metre),
								0.5);
			}
			previous = trackpoint;
		}

		Length expectedLength = TrackProcessorUtil.length(trackpoints);
		Length actualLength = TrackProcessorUtil.length(resultTrack);
		final double expectedLengthInMeters = expectedLength.doubleValue(metre);
		assertEquals(	"total length",
						expectedLengthInMeters,
						actualLength.doubleValue(metre),
						expectedLengthInMeters * 0.001);
	}

	public TrackSegmentListener targetSumAssertionListener() {
		return new TrackSegmentListener() {

			@Override
			public void handle(TrackSegment segment, TrackProcessor processor) {

			}
		};
	}

	@Test
	public void testExactMatches() {
		targetSum = S1_LENGTH;
		expectedTrackpoints.put(Index.SECOND, P2);

	}

	@Test
	public void testHalf() {
		targetSum = S1_HALF_LENGTH;
		expectedTrackpoints.put(Index.THIRD, P2);

	}

	@Test
	public void testQuarter() {
		targetSum = QuantityMath.create(S1_LENGTH).dividedBy(4).getQuantity();
		expectedTrackpoints.put(Index.FIFTH, P2);

	}

	/** This one provides a trackpoint at the beginning, then at 3/4 length, 1.5 length, then 2. */
	@Test
	public void testThreeQuarters() {
		targetSum = QuantityMath.create(S1_LENGTH).times(3).dividedBy(4).getQuantity();
		Trackpoint secondPoint = TrackUtil.interpolate(S1, MeasurementUtil.ratio(3, 4));
		expectedTrackpoints.put(Index.SECOND, secondPoint);
		// third point is half way along segment 2
		Trackpoint thirdPoint = TrackUtil.interpolate(S2, MeasurementUtil.ratio(1, 2));
		expectedTrackpoints.put(Index.THIRD, thirdPoint);
	}

	@Test
	@Ignore("upgrade to openmap 5.0.3")
	public void testRealisticLongTrack() {
		this.trackpoints = ExampleRealisticTracks.create().atLeast(MeasurementUtil.createLengthInMeters(10000))
				.lonIncrement(ExampleRealisticTracks.LAT_REAL_INC).build().track();

		final int targetLengthInMeters = 100;
		this.targetSum = MeasurementUtil.createLengthInMeters(targetLengthInMeters);
	}
}
