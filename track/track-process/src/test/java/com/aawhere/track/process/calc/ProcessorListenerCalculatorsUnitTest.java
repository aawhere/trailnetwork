/**
 * 
 */
package com.aawhere.track.process.calc;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import javax.measure.quantity.Length;

import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.summary.Sum;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.calc.QuantityUnivariateStatistic;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListener;
import com.aawhere.track.process.listener.FinishTrackpointListener;
import com.aawhere.track.process.listener.StartTrackpointListener;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.provider.TrackSegmentDistanceProvider;

/**
 * Provides a place for the many calculating {@link ProcessorListener}s to be tested.
 * 
 * @author Aaron Roller
 * 
 */
public class ProcessorListenerCalculatorsUnitTest {

	private ManualTrackProcessor.Builder processorBuilder;

	@Before
	public void setUp() {
		processorBuilder = new ManualTrackProcessor.Builder();
	}

	@Test
	public void testStart() {

		StartTrackpointListener listener = new StartTrackpointListener.Builder()
				.registerWithProcessor(processorBuilder).build();

		Trackpoint start = P1;
		listener.handle(start, null);
		assertEquals(start, listener.getStart());

		listener.handle(P2, null);
		assertEquals("apparently start was replaced with more recent", start, listener.getStart());

	}

	@Test
	public void testFinish() {
		FinishTrackpointListener listener = new FinishTrackpointListener.Builder()
				.registerWithProcessor(processorBuilder).build();
		listener.handle(P1, null);
		assertEquals(P1, listener.getFinish());
		listener.handle(P2, null);
		assertEquals("listener isn't choosing the latest", P2, listener.getFinish());
	}

	@Test
	public void testDistanceSum() {

		ManualTrackProcessor.Builder processorBuilder = new ManualTrackProcessor.Builder();
		QuantityUnivariateStatistic<Length, Sum> lengthCumulator = QuantityUnivariateStatistic.newSum(Length.class);
		TrackSegmentDistanceProvider distanceProvider = new TrackSegmentDistanceProvider.Builder()
				.registerWithProcessor(processorBuilder).build();
		QuantityUnivariateStatisticProcessorCalculator<Length, Sum> calculator = new QuantityUnivariateStatisticProcessorCalculator.Builder<Length, Sum>()
				.setCalculator(lengthCumulator).setProvider(distanceProvider).registerWithProcessor(processorBuilder)
				.build();

		ManualTrackProcessor processor = processorBuilder.build();
		// p1
		processor.notifyTrackpointListeners(P1);
		assertTrue("sum starts at zero", calculator.hasQuantity());
		assertFalse(distanceProvider.hasQuantity());

		// p2
		processor.notifyTrackpointListeners(P2);
		assertTrue(calculator.hasQuantity());
		MeasureTestUtil.assertEquals(S1_LENGTH, distanceProvider.getQuantity());
		MeasureTestUtil.assertEquals(S1_LENGTH, calculator.getQuantity());

		// p3
		processor.notifyTrackpointListeners(P3);
		MeasureTestUtil.assertEquals(S2_LENGTH, distanceProvider.getQuantity());
		MeasureTestUtil.assertEquals(LINE_LENGTH, calculator.getQuantity());
	}

	@Test
	public void testMeanLength() {
		QuantityUnivariateStatisticProcessorCalculator<Length, Mean> meanListener = ProcessingCalculatorFactory
				.getInstance().newMeanDistanceBetweenTrackpointsCalculator(processorBuilder);
		ManualTrackProcessor processor = processorBuilder.build();
		assertFalse(meanListener.hasQuantity());
		processor.notifyTrackpointListeners(P1);
		assertFalse(meanListener.hasQuantity());
		processor.notifyTrackpointListeners(P2);
		assertTrue(meanListener.hasQuantity());
		MeasureTestUtil.assertEquals(S1_LENGTH, meanListener.getQuantity());
		processor.notifyTrackpointListeners(P3);
		MeasureTestUtil.assertEquals(S1_LENGTH, meanListener.getQuantity());
		processor.notifyTrackpointListeners(P4);
		MeasureTestUtil.assertEquals(S1_LENGTH, meanListener.getQuantity());

	}
}
