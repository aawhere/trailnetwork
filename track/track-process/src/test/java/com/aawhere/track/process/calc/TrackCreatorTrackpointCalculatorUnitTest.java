/**
 *
 */
package com.aawhere.track.process.calc;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.aawhere.track.Track;
import com.aawhere.track.TrackUnitTest;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.util.ListUtilsExtended;

/**
 * Tests basic functions of {@link TrackCreatorTrackpointCalculator}.
 * 
 * @author Aaron Roller on Apr 15, 2011
 * 
 */
public class TrackCreatorTrackpointCalculatorUnitTest {

	@Test
	public void testReset() {

		Track track = TrackUnitTest.getInstance().generateRandomTrack();
		List<Trackpoint> trackpoints = TrackUtil.trackpointsFrom(track);

		AutomaticTrackProcessor.Builder processor = new AutomaticTrackProcessor.Builder(track);
		TrackCreatorTrackpointCalculator listener = new TrackCreatorTrackpointCalculator.Builder()
				.registerWithProcessor(processor).build();
		processor.build();

		Track trackCopy = listener.getTrack();
		List<Trackpoint> trackpointsCopy = TrackUtil.trackpointsFrom(trackCopy);

		assertEquals(trackpoints.size(), trackpointsCopy.size());
		assertTrue(ListUtils.isEqualList(trackpoints, trackpointsCopy));

		// test reset keep last
		listener.resetKeepLastPoint();
		Track keptLast = listener.getTrack();
		List<Trackpoint> keptLastTrackpoints = TrackUtil.trackpointsFrom(keptLast);
		assertEquals(1, keptLastTrackpoints.size());
		assertEquals(ListUtilsExtended.lastOrNull(trackpoints), ListUtilsExtended.lastOrNull(keptLastTrackpoints));
		// test reset
		listener.reset();
		Track empty = listener.getTrack();
		assertFalse(empty.iterator().hasNext());

	}
}
