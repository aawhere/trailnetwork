/**
 * 
 */
package com.aawhere.track.story;

import static org.junit.Assert.*;

import org.junit.Ignore;

import com.aawhere.track.Track;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.Trackpoint;

/**
 * A hack of sorts to use the handy {@link TrackSummaryUserStoriesUnitTest} to produce a summary
 * from a track.
 * 
 * @see #create(Track)
 * 
 * @author aroller
 * 
 */
@Ignore
public class TrackSummaryUserStoriesTestUtil
		extends TrackSummaryUserStoriesUnitTest {

	/**
	 * @see #create(Track)
	 */
	private TrackSummaryUserStoriesTestUtil(Iterable<Trackpoint> track) {
		super(track);
	}

	/**
	 * Give me a track and I give you the summary.
	 * 
	 * @param track
	 * @return
	 */
	public static TrackSummary create(Iterable<Trackpoint> track) {
		final TrackSummaryUserStoriesTestUtil test = new TrackSummaryUserStoriesTestUtil(track);
		test.setUpTs();
		test.testCreation();
		return test.getStory().getResult();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.story.TrackSummaryUserStoriesUnitTest#assertCreated()
	 */
	@Override
	protected void assertCreated() {
		assertNotNull("track", super.track);
		assertNotNull("story", super.getStory());
	}
}
