/**
 * 
 */
package com.aawhere.track.process.calc;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.assertEquals;

import org.joda.time.Duration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor;

/**
 * Tests the functionality of {@link ObservedDurationCalculator}.
 * 
 * @author roller
 * 
 */
public class TrackSegmentDurationCalculatorUnitTest {

	private ManualTrackProcessor.Builder processorBuilder;

	@Before
	public void setUp() {
		processorBuilder = new ManualTrackProcessor.Builder();
	}

	@Test
	public void testOneSegment() {
		ObservedDurationCalculator calculator = new ObservedDurationCalculator.Builder()
				.registerWithProcessor(processorBuilder).build();
		final TrackSegment segment = S1;
		calculator.handle(segment, null);
		Duration duration = calculator.getTotalDuration();

		assertEquals(segment.getDuration(), duration);
	}

	@Test
	public void testTwoSegments() {
		ObservedDurationCalculator calculator = new ObservedDurationCalculator.Builder()
				.registerWithProcessor(processorBuilder).build();
		final TrackSegment s1 = S1;
		final TrackSegment s2 = S3;
		TrackProcessor processor = null;
		calculator.handle(s1, processor);
		calculator.handle(s2, processor);
		assertEquals(s1.getDuration().plus(s2.getDuration()), calculator.getTotalDuration());
	}
}
