/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import org.joda.time.Interval;
import org.junit.Test;

import com.aawhere.measure.Elevation;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.calc.TrackCreatorTrackpointCalculator;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;
import com.aawhere.track.process.seek.TrackpointSeekerChainBuilder;
import com.aawhere.track.process.seek.filter.DuplicateLocationFilter.Builder;

/**
 * Tests {@link ProcessorFilter}s when working with a {@link TrackProcessorBase}.
 * 
 * @author Aaron Roller on Apr 20, 2011
 * 
 */
public class ProcessFilterUnitTest {

	/**
	 * @see BetweenFilter
	 */
	@Test
	public void testBetweenFilter() {

		TrackpointSeekingProcessor.Builder receiverBuilder = new TrackpointSeekingProcessor.Builder();

		// used to test the results of the filter
		TrackCreatorTrackpointCalculator resultTrackCreator = new TrackCreatorTrackpointCalculator.Builder()
				.registerWithProcessor(receiverBuilder).build();
		TrackpointCounter counter = new TrackpointCounter.Builder().registerWithProcessor(receiverBuilder).build();

		// the receiver of filtered trackpoints
		TrackpointSeekingProcessor receiver = receiverBuilder.build();

		// create a filter that should only accept a single point.
		Elevation targetElevation = ExampleTracks.P2.getElevation();
		// between is exclusive to pick the boundaries around and P2 will be found
		Elevation min = ExampleTracks.P1.getElevation();
		Elevation max = ExampleTracks.P3.getElevation();

		// the source is what drives the whole process looking at every point
		AutomaticTrackProcessor.Builder sourceProcessorBuilder = new AutomaticTrackProcessor.Builder(ExampleTracks.LINE);
		ElevationBetweenFilter filter = new ElevationBetweenFilter.Builder().setMax(max).setMin(min)
				.setReceiver(receiver).registerWithProcessor(sourceProcessorBuilder).build();

		// execute the processing
		sourceProcessorBuilder.build();

		assertNotNull(filter);
		assertNotNull(filter.getReceiver());
		assertEquals(min, filter.getMin());
		assertEquals(max, filter.getMax());

		Track result = resultTrackCreator.getTrack();
		assertEquals(result.toString(), 1, (int) counter.getCount());
		Trackpoint onlyTrackpoint = result.iterator().next();
		assertEquals(targetElevation, onlyTrackpoint.getElevation());

	}

	/**
	 * Tests and demonstrates how to use more than one filter. It is a 3 link chain where the first
	 * link listeners receives all trackpoints and the third link listeners receive those filtered
	 * from link2 and link3.
	 * 
	 * 
	 */
	@Test
	public void testFilterChain() {

		// this drives the chain receiving the trackpoints and passing on to the filters.
		// the first filter in the chain this receives from the driver and passes downstream to the
		// elevation filter processor

		// use the chain to abstract the process of linking together processors
		TrackpointSeekerChainBuilder<ManualTrackProcessor> seekerChainBuilder = TrackpointSeekerChainBuilder
				.create(new ManualTrackProcessor.Builder());
		TrackpointCounter link1Counter = seekerChainBuilder
				.registerWithCurrentProcessor(new TrackpointCounter.Builder());

		seekerChainBuilder.addSeeker(MissingLocationTrackpointFilter.create());

		TrackpointCounter link2Counter = seekerChainBuilder
				.registerWithCurrentProcessor(new TrackpointCounter.Builder());
		// must build first link making it able to receive from driver.

		seekerChainBuilder.addSeeker(MissingElevationTrackpointFilter.create());
		TrackpointCounter link3Counter = seekerChainBuilder
				.registerWithCurrentProcessor(new TrackpointCounter.Builder());

		ManualTrackProcessor link1 = seekerChainBuilder.build();

		Integer one = new Integer(1);
		Integer two = new Integer(2);
		Integer three = new Integer(3);
		Integer four = new Integer(4);

		link1.notifyTrackpointListeners(P1);
		assertEquals(one, link1Counter.getCount());
		assertEquals(one, link2Counter.getCount());
		assertEquals(one, link3Counter.getCount());
		link1.notifyTrackpointListeners(P2_MISSING_LOCATION);
		assertEquals("missing location shouldn't affect first link", two, link1Counter.getCount());
		assertEquals("missing location shouldn't notify", one, link2Counter.getCount());
		assertEquals("missing location shouldn't notify downstream", one, link3Counter.getCount());

		link1.notifyTrackpointListeners(P3_MISSING_ELEVATION);
		assertEquals("missing elevation shouldn't affect first link", three, link1Counter.getCount());
		assertEquals("missing elevation shouldn't affect second link", two, link2Counter.getCount());
		assertEquals("missing location shouldn't notify downstream", one, link3Counter.getCount());

		link1.notifyTrackpointListeners(P4);
		assertEquals("all should receive", four, link1Counter.getCount());
		assertEquals("all should receive", three, link2Counter.getCount());
		assertEquals("all should receive", two, link3Counter.getCount());

	}

	/**
	 * Given a 3 point track log with one trackpoint missing location this makes sure only two are
	 * passed on.
	 * 
	 */
	@Test
	public void testMissingLocationFilter() {
		TrackpointSeekingProcessor.Builder receiverBuilder = new TrackpointSeekingProcessor.Builder();
		TrackpointCounter counter = new TrackpointCounter.Builder().registerWithProcessor(receiverBuilder).build();
		ManualTrackProcessor.Builder processorBuilder = new ManualTrackProcessor.Builder();
		TrackpointSeekingProcessor receiver = receiverBuilder.build();
		new MissingLocationTrackpointFilter.Builder().registerWithProcessor(processorBuilder).setReceiver(receiver)
				.build();
		// the receiver of filtered trackpoints

		ManualTrackProcessor processor = processorBuilder.build();
		processor.notifyTrackpointListeners(P1);
		assertEquals("first one should be respected", 1, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P2_MISSING_LOCATION);
		assertEquals("second one is missing location and should be removed", 1, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P3);
		assertEquals("third trackpoint should be respected", 2, counter.getCount().intValue());
	}

	@Test
	public void testDuplicateLocationFilter() {
		final Builder builder = DuplicateLocationFilter.create();

		TrackpointSeekerChainBuilder<ManualTrackProcessor> seekerChainBuilder = TrackpointSeekerChainBuilder
				.create(new ManualTrackProcessor.Builder());

		seekerChainBuilder.addSeeker(builder);

		TrackpointCounter counter = seekerChainBuilder.registerWithCurrentProcessor(new TrackpointCounter.Builder());
		ManualTrackProcessor processor = seekerChainBuilder.build();
		processor.notifyTrackpointListeners(P1);
		assertEquals("first time added", 1, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P2);
		assertEquals("first time added", 2, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P2);
		assertEquals("second in a row", 2, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P1);
		assertEquals("only sequential duplicates are removed", 3, counter.getCount().intValue());

	}

	@Test
	public void testTimeIntervalFilter() {
		TrackpointSeekerChainBuilder<ManualTrackProcessor> seekerBuilder = TrackpointSeekerChainBuilder
				.create(new ManualTrackProcessor.Builder());
		Interval interval = new Interval(P2.getTimestamp(), P4.getTimestamp());
		seekerBuilder.addSeeker(TimeIntervalTrackpointFilter.create().interval(interval));
		TrackpointCounter counter = seekerBuilder.registerWithCurrentProcessor(TrackpointCounter.create());
		ManualTrackProcessor processor = seekerBuilder.build();
		processor.notifyTrackpointListeners(P1);
		assertEquals("before", 0, (int) counter.getCount());
		processor.notifyTrackpointListeners(P2);
		assertEquals("at start", 1, (int) counter.getCount());
		processor.notifyTrackpointListeners(P3);
		assertEquals("between", 2, (int) counter.getCount());
		processor.notifyTrackpointListeners(P4);
		assertEquals("at end", 3, (int) counter.getCount());
		processor.notifyTrackpointListeners(P5);
		assertEquals("after", 3, (int) counter.getCount());

	}

	@Test
	public void testNegativeDurationFilter() {
		TrackpointSeekerChainBuilder<ManualTrackProcessor> seekerChainBuilder = TrackpointSeekerChainBuilder
				.create(new ManualTrackProcessor.Builder());
		seekerChainBuilder.addSeeker(NonProgressingTimestampTrackpointFilter.create());
		TrackpointCounter counter = seekerChainBuilder.registerWithCurrentProcessor(TrackpointCounter.create());
		ManualTrackProcessor processor = seekerChainBuilder.build();
		processor.notifyTrackpointListeners(P1);
		assertEquals("first is always a winner", 1, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P2);
		assertEquals("p2 is after p1", 2, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P2);
		assertEquals("P2 is a repeat", 2, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P3);
		assertEquals("proves it can recover", 3, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P1);
		assertEquals("P1 is way before P3", 3, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P3);
		assertEquals("proves that P3 is still the one that must be beat", 3, counter.getCount().intValue());
		processor.notifyTrackpointListeners(P4);
		assertEquals("a nice finish", 4, counter.getCount().intValue());
	}
}
