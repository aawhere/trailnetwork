/**
 *
 */
package com.aawhere.track.process.seek;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;

/**
 * @author brian
 * 
 */
public class EveryTrackpointSeekerUnitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEveryTrackpointSeeker() {
		Track sourceTrack = ExampleTracks.LINE;
		Track targetTrack = ExampleTracks.LINE_LONGER;
		AutomaticTrackProcessor.Builder sourceProcessorBuilder = new AutomaticTrackProcessor.Builder(sourceTrack);
		TrackpointSeekingProcessor.Builder targetProcessorBuilder = new TrackpointSeekingProcessor.Builder();
		TrackpointCounter countingTrackpointListener = TrackpointCounter.create()
				.registerWithProcessor(targetProcessorBuilder).build();
		TrackpointSeekingProcessor targetProcessor = targetProcessorBuilder.build();
		new EveryTrackpointSeeker.Builder().setReceiver(targetProcessor).setTargetTrack(targetTrack)
				.registerWithProcessor(sourceProcessorBuilder).build();
		sourceProcessorBuilder.build();

		assertEquals(	TrackUtil.trackpointsFrom(targetTrack).size() * TrackUtil.trackpointsFrom(sourceTrack).size(),
						(int) countingTrackpointListener.getCount());
	}
}
