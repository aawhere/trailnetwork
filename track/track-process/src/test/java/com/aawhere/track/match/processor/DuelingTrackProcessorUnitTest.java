/**
 *
 */
package com.aawhere.track.match.processor;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.seek.EveryTrackpointSeeker;

/**
 * @author brian
 * 
 */
public class DuelingTrackProcessorUnitTest {

	private final Track sourceTrack = ExampleTracks.LINE;
	private final Track targetTrack = ExampleTracks.LINE_LONGER;
	private final DuelingTrackProcessor.Builder duelingProcessorBuilder = new DuelingTrackProcessor.Builder(sourceTrack);
	private final EveryTrackpointSeeker.Builder everyTrackpointSeekerBuilder = new EveryTrackpointSeeker.Builder();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDuelingTrackProcessor() {
		duelingProcessorBuilder.setTarget(targetTrack);
		duelingProcessorBuilder.setTrackpointSeeker(everyTrackpointSeekerBuilder);

		TrackpointCounter countingTrackpointListener = TrackpointCounter.create()
				.registerWithProcessor(duelingProcessorBuilder.getTargetProcessor()).build();

		duelingProcessorBuilder.build();

		assertEquals(	TrackUtil.trackpointsFrom(targetTrack).size() * TrackUtil.trackpointsFrom(sourceTrack).size(),
						(int) countingTrackpointListener.getCount());
	}

	@Test(expected = NullPointerException.class)
	public void testNullTrackDuringBuild() {
		duelingProcessorBuilder.setTrackpointSeeker(everyTrackpointSeekerBuilder);
		duelingProcessorBuilder.build();
	}

	@Test(expected = NullPointerException.class)
	public void testNullSeekerDuringBuild() {
		duelingProcessorBuilder.setTarget(targetTrack);
		duelingProcessorBuilder.build();
	}

}
