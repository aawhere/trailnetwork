/**
 * 
 */
package com.aawhere.track.process;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;

import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class TrackProcessorUtilUnitTest {

	@Test
	public void testLength() {
		Track line = ExampleTracks.LINE;
		Length expectedLength = ExampleTracks.LINE_LENGTH;
		Length actual = TrackProcessorUtil.length(line);
		MeasureTestUtil.assertEquals("line length", expectedLength, actual);
	}

	@Test
	public void testLengthOfTracks() {
		Iterable<Track> tracks = Lists.newArrayList(ExampleTracks.LINE, ExampleTracks.LINE_LONGER);
		Length expected = QuantityMath.create(ExampleTracks.LINE_LENGTH).plus(ExampleTracks.LINE_LONGER_LENGTH)
				.getQuantity();

		Length actual = TrackProcessorUtil.lengthOfTracks(tracks);
		MeasureTestUtil.assertEquals("lenght of tracks", expected, actual);
	}

	@Test
	public void testMissingLocation() {
		Track line = ExampleTracks.LINE_BEFORE_AND_AFTER_MISSING_LOCATION;
		Length expectedLength = ExampleTracks.LINE_LENGTH;
		Length actual = TrackProcessorUtil.length(line);
		MeasureTestUtil.assertEquals("line length", expectedLength, actual);
	}

	@Test
	public void testMissingElevation() {
		Track line = ExampleTracks.LINE_LONGER_MISSING_ELEVATION;
		Length expectedLength = ExampleTracks.LINE_LONGER_LENGTH;
		Length actual = TrackProcessorUtil.length(line);
		MeasureTestUtil.assertEquals("line length", expectedLength, actual);
	}

}
