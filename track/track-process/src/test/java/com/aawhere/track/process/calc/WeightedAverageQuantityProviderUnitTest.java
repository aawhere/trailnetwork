/**
 * 
 */
package com.aawhere.track.process.calc;

import static com.aawhere.test.TestUtil.assertDoubleEquals;
import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.measure.Elevation;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackSegmentUnitTest;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.provider.TrackSegmentElevationProvider;

/**
 * Tests {@link WeightedMeanQuantityCalculator}.
 * 
 * @author roller
 * 
 */
public class WeightedAverageQuantityProviderUnitTest {

	/** Maybe too simple, but ensures basics are working. */
	@Test
	public void testSingleSegment() {

		TrackSegment segment = ExampleTracks.S1;
		WeightedMeanQuantityCalculator<Elevation> elevationCalculator = calculateSegments(segment);

		Elevation averageElevation = elevationCalculator.getQuantity();
		assertEquals(segment.getAverageElevation(), averageElevation);
	}

	/**
	 * @param processor
	 * @param segments
	 * @return
	 */
	private WeightedMeanQuantityCalculator<Elevation> calculateSegments(TrackSegment... segments) {
		ManualTrackProcessor.Builder processorBuilder = new ManualTrackProcessor.Builder();
		TrackProcessor processor = null;
		TrackSegmentElevationProvider provider = new TrackSegmentElevationProvider();
		ObservedDurationCalculator durationCalculator = new ObservedDurationCalculator.Builder()
				.registerWithProcessor(processorBuilder).build();

		WeightedMeanQuantityCalculator<Elevation> elevationCalculator = new WeightedMeanQuantityCalculator.Builder<Elevation>()
				.setProvider(provider).registerWithProcessor(processorBuilder).build();
		for (TrackSegment segment : segments) {
			provider.handle(segment, processor);
			durationCalculator.handle(segment, processor);
			elevationCalculator.handle(segment, processor);
		}
		return elevationCalculator;
	}

	@Test
	public void testWeighting() {
		WeightedMeanQuantityCalculator<Elevation> elevationCalculator = calculateSegments(new TrackSegment[] { S1,
				S3_RETURN });

		Elevation averageElevation = elevationCalculator.getQuantity();
		// Time segment is a constant so no need to use in multiplication
		double expected = (TrackSegmentUnitTest.S1_EXPECTED_ELEVATION + (E3 * 2)) / 3;
		assertDoubleEquals(expected, averageElevation.doubleValue(UNIT_FOR_ELE));
	}

}
