/**
 *
 */
package com.aawhere.track.match;

import static com.aawhere.track.ExampleTracks.LINE;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.calc.TrackCreatorTrackpointCalculator;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;

/**
 * Tests {@link SimultaneousTrackpointSeeker}.
 * 
 * Mostly makes sure the seeker is finding the right points for proper synching by making sure
 * target trackpoint times match the source's times.
 * 
 * @author Aaron Roller
 * 
 */
public class SimultaneousTrackpointSeekerUnitTest {

	@Test
	public void testSameTracks() {

		Track target = LINE;
		Track source = target;
		// feeds the source points to the seeker
		ManualTrackProcessor.Builder sourceProcessorBuilder = new ManualTrackProcessor.Builder();

		SimultaneousTrackpointSeeker.Builder seekerBuilder = new SimultaneousTrackpointSeeker.Builder()
				.setTarget(target);

		TrackpointSeekingProcessor.Builder receiverBuilder = new TrackpointSeekingProcessor.Builder();
		new TrackCreatorTrackpointCalculator.Builder().registerWithProcessor(receiverBuilder).build();
		seekerBuilder.setReceiver(receiverBuilder.build());
		seekerBuilder.registerWithProcessor(sourceProcessorBuilder);

		SimultaneousTrackpointSeeker seeker = seekerBuilder.build();
		ManualTrackProcessor sourceProcessor = sourceProcessorBuilder.build();
		int whichTrackpoint = 0;
		for (Trackpoint sourceTrackpoint : source) {

			sourceProcessor.notifyTrackpointListeners(sourceTrackpoint);
			Trackpoint targetTrackpoint = seeker.getCurrentTargetTrackpoint();
			assertEquals(	"Trackpoint's timestamps don't match.  Index of " + whichTrackpoint,
							sourceTrackpoint.getTimestamp(),
							targetTrackpoint.getTimestamp());

			assertEquals(whichTrackpoint + " doesn't match", sourceTrackpoint, targetTrackpoint);
		}

		sourceProcessor.finish();
	}

	public void testTargetTrackBeforeSourceAsynch() {

	}

	public void testSourceTrackBeforeTargetAsynch() {

	}

	public void testTargetTrackBeforeSourceSynch() {

	}

	public void testInterpolation() {

	}
}
