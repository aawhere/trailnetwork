/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * A trackpoint reducer using a simple algorithm of choosing every nth trackpoint as defined by the
 * {@link #frequencyNumber}.
 * 
 * The default behavior is to skip every other.
 * 
 * @author roller
 * 
 */
public class FrequencyTrackpointFilter
		extends TrackpointSeekerBase
		implements TrackpointListener, ProcessorFilter {

	public static class Builder
			extends TrackpointSeekerBase.Builder<FrequencyTrackpointFilter, Builder> {

		/**
		 * @param building
		 */
		public Builder() {
			super(new FrequencyTrackpointFilter());

		}

		/** For every one seen this will be the amount to skip. */
		public Builder setCountToSkip(Integer skipThisMany) {
			// we add 1 since we use mod operator
			building.frequencyNumber = skipThisMany + 1;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
		}

	}

	public static Builder create() {
		return new Builder();
	}

	private Integer frequencyNumber = 2;
	private int numberOfTrackpointsSeen = 0;

	private FrequencyTrackpointFilter() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {

		if (numberOfTrackpointsSeen % frequencyNumber == 0) {
			desiredTrackpointFound(trackpoint);
		}
		// incrementing afterwards ensures the first trackpoint is kept.
		numberOfTrackpointsSeen++;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.frequencyNumber == null) ? 0 : this.frequencyNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof FrequencyTrackpointFilter))
			return false;
		FrequencyTrackpointFilter other = (FrequencyTrackpointFilter) obj;
		if (this.frequencyNumber == null) {
			if (other.frequencyNumber != null)
				return false;
		} else if (!this.frequencyNumber.equals(other.frequencyNumber))
			return false;
		return true;
	}

}
