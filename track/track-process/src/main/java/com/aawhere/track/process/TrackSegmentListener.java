/**
 * 
 */
package com.aawhere.track.process;

import com.aawhere.track.TrackSegment;

/**
 * Used by calculators and such that required more than one trackpoint (like
 * {@link TrackpointListener}) to satisfy their duties by receiving only {@link TrackSegment}s.
 * 
 * @author roller
 * 
 */
public interface TrackSegmentListener
		extends ProcessorListener {

	public void handle(TrackSegment segment, TrackProcessor processor);
}
