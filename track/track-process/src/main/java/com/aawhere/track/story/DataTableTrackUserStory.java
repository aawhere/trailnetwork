/**
 *
 */
package com.aawhere.track.story;

import javax.measure.quantity.Length;

import org.apache.commons.configuration.Configuration;

import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.personalize.QuantityPersonalizer;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.process.calc.ProcessingCalculatorFactory;
import com.aawhere.track.process.calc.datatable.DataTableProvider;
import com.aawhere.track.process.calc.datatable.GeoCoordinateTableCellProvider;
import com.aawhere.track.process.calc.datatable.QuantityTableCellProvider;
import com.aawhere.track.process.calc.datatable.TableCellMessage;
import com.aawhere.track.process.provider.TrackSegmentDistanceProvider;
import com.aawhere.track.process.provider.TrackpointElevationProvider;
import com.aawhere.track.process.seek.TrackpointSeekerChainBuilder;
import com.aawhere.track.process.seek.virtual.FixedQuantitySumVirtualTrackpointSeeker;

import com.google.visualization.datasource.datatable.DataTable;

/**
 * Provides the implementation to satisfy the user stories related to creating {@link DataTable} for
 * use in the Google Chart Data Table Api. TODO:Move this to activity-export.
 * 
 * 
 * @author Brian Chapman
 * 
 */
public class DataTableTrackUserStory
		extends BaseTrackUserStory<DataTable> {

	public ArchivedDocumentId documentId;

	private Configuration prefs;

	private DataTableProvider dataTableProvider;

	/** Use {@link Builder} to construct DataTableTrackUserStory */
	private DataTableTrackUserStory() {
	}

	@Override
	public void process() {
		setResult(dataTableProvider.getDataTable());
	}

	/**
	 * Used to construct all instances of DataTableTrackUserStory.
	 * 
	 */
	public static class Builder
			extends BaseTrackUserStory.Builder<DataTableTrackUserStory, Builder> {

		/**
		 * Provides a fixed number of trackpoints (rows) by creating virtual trackpoints at fixed
		 * intervals.
		 */
		private Integer maxNumberOfTrackpoints = 1000;
		private TrackSummary summary;
		private TrackpointSeekerChainBuilder<?> seekerChainBuilder;
		private Length fixedDistanceApart;
		private Length fixedDistanceScalar = MeasurementUtil.createLengthInMeters(10);

		public Builder() {
			super(new DataTableTrackUserStory());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public DataTableTrackUserStory build() {

			DataTableTrackUserStory built = super.build();
			built.process();
			return built;
		}

		/**
		 * @param docId
		 * @return
		 */
		public Builder setPreferences(Configuration prefs) {
			building.prefs = prefs;
			return this;
		}

		/**
		 * Provides an upper limit, but the actual number of trackpoints is determined by
		 * {@link #fixedDistanceScalar} finding an even fixed distance with as many trackpoints as
		 * possible without exceeding the max.
		 * 
		 * @param i
		 * @return
		 */
		public Builder setMaxNumberOfTrackpoints(Integer numberOfTrackpoints) {
			this.maxNumberOfTrackpoints = numberOfTrackpoints;
			return this;
		}

		/**
		 * @param trackSummary
		 * @return
		 */
		public Builder setSummary(TrackSummary summary) {
			this.summary = summary;
			return this;
		}

		/**
		 * @param seekerChainBuilder
		 * @return
		 */
		public Builder setSeekerChainBuilder(TrackpointSeekerChainBuilder<?> seekerChainBuilder) {
			this.seekerChainBuilder = seekerChainBuilder;
			return this;
		}

		/**
		 * Overrides any calculations targeting {@link #maxNumberOfTrackpoints}.
		 * 
		 * @param length
		 * @return
		 */
		public Builder setFixedDistanceApart(Length length) {
			this.fixedDistanceApart = length;
			return this;
		}

		@Override
		public void validate() {
			super.validate();
			Assertion.assertNotNull("Preferences", building.prefs);
			Assertion.assertNotNull("summary", this.summary);
			Assertion.exceptions().notNull("seekerChainBuilder", this.seekerChainBuilder);

		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.track.story.BaseTrackUserStory.Builder#configureProcessorBuilder(com.aawhere
		 * .track.story.BaseTrackUserStory)
		 */
		@Override
		protected void configureProcessorBuilder(DataTableTrackUserStory built) {
			final QuantityPersonalizer personalizer = new QuantityPersonalizer.Builder().setPreferences(built.prefs)
					.build();

			if (this.fixedDistanceApart == null) {
				this.fixedDistanceApart = TrackUtil.segmentLength(	this.maxNumberOfTrackpoints,
																	this.summary.getDistance(),
																	this.fixedDistanceScalar);
			}
			// original segment distances provided to the seeker
			TrackSegmentDistanceProvider segmentDistanceProvider = seekerChainBuilder
					.registerWithCurrentProcessor(TrackSegmentDistanceProvider.create());
			final com.aawhere.track.process.seek.virtual.FixedQuantitySumVirtualTrackpointSeeker.Builder<Length> fixedDistanceSeekerBuilder = FixedQuantitySumVirtualTrackpointSeeker
					.create();

			seekerChainBuilder.addSeeker(fixedDistanceSeekerBuilder.targetSum(fixedDistanceApart)
					.provider(segmentDistanceProvider));

			final ProcessingCalculatorFactory calculatorFactory = ProcessingCalculatorFactory.getInstance();

			// Distance
			QuantityTableCellProvider<Length> distanceProvider = seekerChainBuilder
					.registerWithCurrentProcessor(new QuantityTableCellProvider.Builder<Length>().columnId("distance")
							.setColumnLabel(TableCellMessage.DISTANCE_LABEL).setPersonalizer(personalizer)
							.provider(calculatorFactory.newTotalDistanceCalculator(getProcessorBuilder())));
			// Elevation
			TrackpointElevationProvider elevationQuantityProvider = seekerChainBuilder
					.registerWithCurrentProcessor(TrackpointElevationProvider.create());
			QuantityTableCellProvider<Elevation> elevationProvider = seekerChainBuilder
					.registerWithCurrentProcessor(new QuantityTableCellProvider.Builder<Elevation>()
							.columnId("elevation").setColumnLabel(TableCellMessage.ELEVATION_LABEL)
							.setPersonalizer(personalizer).provider(elevationQuantityProvider));

			// coorindates
			GeoCoordinateTableCellProvider coordinateProvider = seekerChainBuilder
					.registerWithCurrentProcessor(new GeoCoordinateTableCellProvider.Builder().columnId("location1")
							.setColumnLabel(TableCellMessage.GEO_COORDINATE_LABEL));

			// elevation loss
			QuantityTableCellProvider<ElevationChange> elevationLossProvider = seekerChainBuilder
					.registerWithCurrentProcessor(new QuantityTableCellProvider.Builder<ElevationChange>()
							.columnId("elevationLoss").setColumnLabel(TableCellMessage.ELEVATION_LOSS_LABEL)
							.setPersonalizer(personalizer)
							.provider(calculatorFactory.newElevationLossCalculator(getProcessorBuilder())));

			// elevation gain
			QuantityTableCellProvider<ElevationChange> elevationGainProvider = seekerChainBuilder
					.registerWithCurrentProcessor(new QuantityTableCellProvider.Builder<ElevationChange>()
							.columnId("elevationGain").setColumnLabel(TableCellMessage.ELEVATION_GAIN_LABEL)
							.setPersonalizer(personalizer)
							.provider(calculatorFactory.newElevationGainCalculator(getProcessorBuilder())));

			built.dataTableProvider = seekerChainBuilder.registerWithCurrentProcessor(new DataTableProvider.Builder()
					.setProvider(distanceProvider).setProvider(elevationProvider).setProvider(elevationGainProvider)
					.setProvider(elevationLossProvider).setProvider(coordinateProvider));

		}

	}// end Builder
}
