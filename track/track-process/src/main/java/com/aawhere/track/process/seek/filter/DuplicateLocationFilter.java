/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import javax.measure.quantity.Length;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

/**
 * Filters locations removing locations that are too close to the previous as defined by
 * {@link Builder#setDistanceApartThreshold(Length)}.
 * 
 * This only removes sequential duplicates so if no duplicate locations are required then a
 * different filter should be implemented.
 * 
 * WARNING:This class has demonstrated poor performance in performance tests.
 * 
 * @author roller
 * 
 */
public class DuplicateLocationFilter
		extends SequentialTrackpointFilterBase {

	/** A convience method for constructing a builder */
	public static Builder create() {
		return new Builder();
	}

	/**
	 * Used to construct all instances of DuplicateLocationFilter.
	 */
	public static class Builder
			extends SequentialTrackpointFilterBase.Builder<DuplicateLocationFilter, Builder> {

		public Builder() {
			super(new DuplicateLocationFilter());
		}

		public Builder setDistanceApartThreshold(Length distanceApartThreshold) {
			building.distanceApartThreshold = distanceApartThreshold;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public DuplicateLocationFilter build() {
			building.math = new QuantityMath<Length>(building.distanceApartThreshold);
			return super.build();
		}
	}// end Builder

	/**
	 * 
	 */
	private static final double DEFAULT_THRESHOLD_IN_METERS = 0.01;
	private Length distanceApartThreshold = MeasurementUtil.createLengthInMeters(DEFAULT_THRESHOLD_IN_METERS);
	private QuantityMath<Length> math;

	/**
	 * 
	 */
	private DuplicateLocationFilter() {

	}

	/**
	 * Includes if the locations are too close to differentiate.
	 * 
	 */
	@Override
	protected Boolean includeCurrentTrackpoint(Trackpoint previousTrackpoint, Trackpoint currentTrackpoint) {

		Length distanceBetween = TrackUtil.distanceBetween(previousTrackpoint, currentTrackpoint);
		return math.lessThan(distanceBetween);
	}

	/**
	 * @return the distanceApartThreshold
	 */
	public Length getDistanceApartThreshold() {
		return this.distanceApartThreshold;
	}

}
