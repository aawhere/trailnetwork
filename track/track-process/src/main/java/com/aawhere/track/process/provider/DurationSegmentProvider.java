/**
 * 
 */
package com.aawhere.track.process.provider;

import org.joda.time.Duration;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;

/**
 * Simply provides the duration from the current Segment.
 * 
 * @author Aaron Roller on Apr 7, 2011
 * 
 */
public class DurationSegmentProvider
		implements TrackSegmentListener {

	private Duration duration;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track .TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		this.duration = segment.getDuration();

	}

	public Duration getDuration() {
		return duration;
	}
}
