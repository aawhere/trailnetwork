/**
 * 
 */
package com.aawhere.track.process.calc.datatable;

import javax.measure.quantity.Quantity;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerQuantityProvider;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.TrackpointListener;

/**
 * Depending upon another {@link ProcessorListenerQuantityProvider} this translates the given
 * quantity into a table cell value and display value. The provider may be
 * {@link TrackSegmentListener} or {@link TrackpointListener} and
 * {@link ProcessorListenerQuantityProvider#hasQuantity()} will be used to determine if the cell
 * deserves a value.
 * 
 * @author aroller
 * 
 */
public class QuantityTableCellProvider<Q extends Quantity<Q>>
		extends QuantityTableCellProviderBase
		implements TrackpointListener {

	private ProcessorListenerQuantityProvider<Q> provider;

	/**
	 * Used to construct all instances of QuantityTableCellProvider.
	 */
	public static class Builder<Q extends Quantity<Q>>
			extends QuantityTableCellProviderBase.Builder<Builder<Q>, QuantityTableCellProvider<Q>> {

		public Builder() {
			super(new QuantityTableCellProvider<Q>());
		}

		public Builder<Q> provider(ProcessorListenerQuantityProvider<Q> provider) {
			building.provider = provider;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.track.process.calc.datatable.QuantityTableCellProviderBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("provider", building.provider);
		}

		@Override
		public QuantityTableCellProvider<Q> build() {
			QuantityTableCellProvider<Q> built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct QuantityTableCellProvider */
	private QuantityTableCellProvider() {
	}

	public static <Q extends Quantity<Q>> Builder<Q> create() {
		return new Builder<Q>();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		if (provider.hasQuantity()) {
			setCell(buildCell(provider.getQuantity()));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.provider == null) ? 0 : this.provider.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		QuantityTableCellProvider<Q> other = (QuantityTableCellProvider<Q>) obj;
		if (this.provider == null) {
			if (other.provider != null)
				return false;
		} else if (!this.provider.equals(other.provider))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return QuantityTableCellProvider.class.getSimpleName() + " (" + this.provider.toString() + ")";
	}

}
