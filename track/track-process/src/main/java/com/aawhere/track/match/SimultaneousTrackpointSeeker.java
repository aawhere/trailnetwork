/**
 * 
 */
package com.aawhere.track.match;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Track;
import com.aawhere.track.TrackTimestampIterator;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.seek.virtual.SequentialTimestampTrackpointSeeker;

/**
 * Extends the time synching capabilities of {@link SequentialTimestampTrackpointSeeker}, but
 * provides the matching trackpoint to the target timestamp.
 * 
 * The source trackpoint are provided in the {@link #desiredTrackpointFound(Trackpoint)} and the
 * target is available from {@link #getCurrentTargetTrackpoint()}.
 * 
 * @author roller
 * 
 * @deprecated this receives an entire track rather that allowing existing Processors to provide the
 *             trackpoint
 */
public class SimultaneousTrackpointSeeker
		extends SequentialTimestampTrackpointSeeker {

	public static class Builder
			extends SequentialTimestampTrackpointSeeker.BaseBuilder<SimultaneousTrackpointSeeker, Builder> {

		/**
		 * @param building
		 */
		public Builder() {
			super(new SimultaneousTrackpointSeeker());

		}

		/*
		 * (non-Javadoc)
		 * @seecom.aawhere.track.process.seek.virtual.
		 * SequentialTimestampTrackpointSeeker.BaseBuilder#validate()
		 */
		@Override
		protected void validate() {

			super.validate();
			Assertion.assertNotNull("target", building.targetTrackpointIterator);
		}

		/**
		 * The target track that has trackpoints that you desire to match up with trackpoints from
		 * the source.
		 * 
		 * @param target
		 * @return
		 */
		public Builder setTarget(Track target) {
			building.targetTrackpointIterator = new TrackTimestampIterator.Builder().setTrack(target).build();
			super.setTargetTimestamps(building.targetTrackpointIterator);
			return this;
		}

	}

	private TrackTimestampIterator targetTrackpointIterator;

	protected SimultaneousTrackpointSeeker() {
		super();
	}

	public Trackpoint getCurrentTargetTrackpoint() {
		return this.targetTrackpointIterator.getCurrentTrackpoint();
	}

}
