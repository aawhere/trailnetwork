/**
 * 
 */
package com.aawhere.track.process.calc.datatable;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

import com.google.visualization.datasource.datatable.TableCell;
import com.google.visualization.datasource.datatable.value.ValueType;

/**
 * @author aroller
 * 
 */
public class GeoCoordinateTableCellProvider

		extends TableCellProvider
		implements TrackpointListener {

	/**
	 * Used to construct all instances of GeoCoordinateTableCellProvider.
	 */
	public static class Builder
			extends TableCellProvider.Builder<Builder, GeoCoordinateTableCellProvider> {

		public Builder() {
			super(new GeoCoordinateTableCellProvider());
			columnType(ValueType.TEXT);
		}

		@Override
		public GeoCoordinateTableCellProvider build() {
			GeoCoordinateTableCellProvider built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct GeoCoordinateTableCellProvider */
	private GeoCoordinateTableCellProvider() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		TableCell cell = new TableCell(trackpoint.getLocation().getAsString());
		setCell(cell);
	}

}
