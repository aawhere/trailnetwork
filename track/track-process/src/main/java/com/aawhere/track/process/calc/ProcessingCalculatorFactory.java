/**
 *
 */
package com.aawhere.track.process.calc;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;

import org.apache.commons.math.stat.descriptive.StorelessUnivariateStatistic;
import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.summary.Sum;

import com.aawhere.math.stats.Gain;
import com.aawhere.math.stats.Loss;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.calc.QuantityUnivariateStatistic;
import com.aawhere.track.process.ProcessorListenerQuantityProvider;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.calc.TrackSignalQualityCalculator.Builder;
import com.aawhere.track.process.provider.AccelerationSegmentProvider;
import com.aawhere.track.process.provider.ElevationChangeSegmentProvider;
import com.aawhere.track.process.provider.TrackSegmentDistanceProvider;
import com.aawhere.track.process.provider.VelocitySegmentProvider;

/**
 * A factory that provides state by creating only a single instance of a specific type of calculator
 * or provider once per factory instance.
 * 
 * FIXME: this was made stateful after being a singleton. Convert existing cuatiously , but new
 * additions should use the stateful variables.
 * 
 * @author Aaron Roller
 * 
 */
public class ProcessingCalculatorFactory {

	private VelocitySegmentProvider velocityProvider;
	private AccelerationSegmentProvider accelerationProvider;
	private TrackProcessorBase.Builder<?> processorBuilder;
	private TrackHeadingConsitencyCalculator headingConsitencyCalculator;

	/**
	 * @deprecated use #create()
	 * 
	 */
	@Deprecated
	public static ProcessingCalculatorFactory getInstance() {
		return create(null);

	}// end Builder

	public static ProcessingCalculatorFactory create(TrackProcessorBase.Builder<?> processorBuilder) {
		return new ProcessingCalculatorFactory(processorBuilder);
	}

	/**
	 * Use {@link #getInstance()} to get ProcessingCalculatorFactory
	 * 
	 * @param processorBuilder
	 */
	private ProcessingCalculatorFactory(com.aawhere.track.process.TrackProcessorBase.Builder<?> processorBuilder) {
		this.processorBuilder = processorBuilder;
	}

	/** Calculates the total distance of a track. */
	public QuantityUnivariateStatisticProcessorCalculator<Length, Sum> newTotalDistanceCalculator(
			TrackProcessorBase.Builder<?> processorBuilder) {
		TrackSegmentDistanceProvider provider = new TrackSegmentDistanceProvider.Builder()
				.registerWithProcessor(processorBuilder).build();
		return newSumCalculator(processorBuilder, provider, Length.class);
	}

	/**
	 * Generic method to put together a sum processor for any {@link Quantity}.
	 * 
	 * 
	 * @param processorBuilder
	 * @param provider
	 * @return
	 */
	public <Q extends Quantity<Q>> QuantityUnivariateStatisticProcessorCalculator<Q, Sum> newSumCalculator(
			TrackProcessorBase.Builder<?> processorBuilder, ProcessorListenerQuantityProvider<Q> provider,
			Class<Q> quantityType) {
		QuantityUnivariateStatistic<Q, Sum> sumCalculator = QuantityUnivariateStatistic.newSum(quantityType);
		QuantityUnivariateStatisticProcessorCalculator<Q, Sum> sumProcessor = new QuantityUnivariateStatisticProcessorCalculator.Builder<Q, Sum>()
				.setCalculator(sumCalculator).setProvider(provider).setProvider(provider)
				.registerWithProcessor(processorBuilder).build();
		return sumProcessor;
	}

	/**
	 * Calculates the simple mean for all distances between trackpoints. This is useful to determine
	 * the quality of a recorded track.
	 * 
	 * @param processorBuilder
	 * @return
	 */
	public QuantityUnivariateStatisticProcessorCalculator<Length, Mean> newMeanDistanceBetweenTrackpointsCalculator(
			TrackProcessorBase.Builder<?> processorBuilder) {
		TrackSegmentDistanceProvider provider = new TrackSegmentDistanceProvider.Builder()
				.registerWithProcessor(processorBuilder).build();
		QuantityUnivariateStatistic<Length, Mean> meanCalculator = QuantityUnivariateStatistic.newMean(Length.class);
		QuantityUnivariateStatisticProcessorCalculator<Length, Mean> averageTrackpointDistanceCalculator = new QuantityUnivariateStatisticProcessorCalculator.Builder<Length, Mean>()
				.setCalculator(meanCalculator).setProvider(provider).registerWithProcessor(processorBuilder).build();
		return averageTrackpointDistanceCalculator;
	}

	public DurationUnivariateStatisticProcessorCalculator<Mean> newMeanDurationBetweenTrackpointsCalculator(
			TrackProcessorBase.Builder<?> processorBuilder) {
		return new DurationUnivariateStatisticProcessorCalculator.Builder<Mean>().setStatistic(new Mean())
				.registerWithProcessor(processorBuilder).build();
	}

	/**
	 * Calculates the sum of all Elevation changes between trackpoints. Should be the same as the
	 * track's end elevation minus the tracks start elevation.
	 * 
	 * @param processorBuilder
	 * @return
	 */
	public QuantityUnivariateStatisticProcessorCalculator<ElevationChange, Sum> newElevationChangeCalculator(
			TrackProcessorBase.Builder<?> processorBuilder) {
		QuantityUnivariateStatistic<ElevationChange, Sum> sum = QuantityUnivariateStatistic
				.newSum(ElevationChange.class);
		return elevationChangeCalculator(processorBuilder, sum);
	}

	/**
	 * @param processorBuilder
	 * @return
	 */
	private ElevationChangeSegmentProvider elevationChangeProvider(TrackProcessorBase.Builder<?> processorBuilder) {
		return new ElevationChangeSegmentProvider.Builder().registerWithProcessor(processorBuilder).build();
	}

	/**
	 * Calculates the Elevation gain for all elevation gains between trackpoints.
	 * 
	 * @param processorBuilder
	 * @return
	 */
	public QuantityUnivariateStatisticProcessorCalculator<ElevationChange, Gain> newElevationGainCalculator(
			TrackProcessorBase.Builder<?> processorBuilder) {

		final QuantityUnivariateStatistic<ElevationChange, Gain> gainCalculator = QuantityUnivariateStatistic
				.newGain(ElevationChange.class);
		return elevationChangeCalculator(processorBuilder, gainCalculator);
	}

	public AccelerationSegmentProvider accelerationProvider() {
		if (accelerationProvider == null) {
			accelerationProvider = AccelerationSegmentProvider.create().registerWithProcessor(this.processorBuilder)
					.build();
		}
		return accelerationProvider;
	}

	public VelocitySegmentProvider velocityProvider() {
		if (velocityProvider == null) {
			velocityProvider = VelocitySegmentProvider.create().registerWithProcessor(processorBuilder).build();
		}
		return velocityProvider;
	}

	public TrackSignalQualityCalculator signalQualityCalculator() {
		TrackSegmentCounter counter = TrackSegmentCounter.create().registerWithProcessor(processorBuilder).build();
		Builder builder = TrackSignalQualityCalculator.create();
		builder.velocityProvider(velocityProvider());
		builder.accelerationProvider(accelerationProvider());
		builder.counter(counter);
		builder.registerWithProcessor(processorBuilder);
		return builder.build();
	}

	/**
	 * @param processorBuilder
	 * @param calculator
	 * @return
	 */
	private <QU extends QuantityUnivariateStatistic<ElevationChange, U>, U extends StorelessUnivariateStatistic>
			QuantityUnivariateStatisticProcessorCalculator<ElevationChange, U> elevationChangeCalculator(
					TrackProcessorBase.Builder<?> processorBuilder, final QU calculator) {
		QuantityUnivariateStatisticProcessorCalculator<ElevationChange, U> elevationGainCalculator = new QuantityUnivariateStatisticProcessorCalculator.Builder<ElevationChange, U>()
				.setCalculator(calculator).setProvider(elevationChangeProvider(processorBuilder))
				.registerWithProcessor(processorBuilder).build();
		return elevationGainCalculator;
	}

	public QuantityUnivariateStatisticProcessorCalculator<ElevationChange, Loss> newElevationLossCalculator(
			TrackProcessorBase.Builder<?> processorBuilder) {

		final QuantityUnivariateStatistic<ElevationChange, Loss> loss = QuantityUnivariateStatistic
				.newLoss(ElevationChange.class);
		return elevationChangeCalculator(processorBuilder, loss);
	}

	/**
	 * @param processorBuilder
	 * @return
	 */
	public BoundingBoxCalculator newBoundingBoxCalculator(TrackProcessorBase.Builder<?> processorBuilder) {
		BoundingBoxCalculator calc = new BoundingBoxCalculator.Builder().registerWithProcessor(processorBuilder)
				.build();
		return calc;
	}

	public TrackHeadingConsitencyCalculator headingConsitencyCalculator() {
		if (this.headingConsitencyCalculator == null) {
			this.headingConsitencyCalculator = TrackHeadingConsitencyCalculator.create()
					.registerWithProcessor(processorBuilder).counter().build();
		}
		return this.headingConsitencyCalculator;
	}

}
