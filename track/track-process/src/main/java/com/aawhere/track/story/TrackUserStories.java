/**
 *
 */
package com.aawhere.track.story;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.track.Track;
import com.aawhere.track.TrackSummaryMessage;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.seek.TrackpointSeekerChainBuilder;
import com.aawhere.track.process.seek.filter.MissingElevationTrackpointFilter;
import com.aawhere.track.process.seek.filter.MissingLocationTrackpointFilter;
import com.aawhere.track.process.seek.filter.NonProgressingTimestampTrackpointFilter;
import com.aawhere.track.process.seek.filter.ProcessorFilter;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageStatus;
import com.aawhere.util.rb.StatusMessages;

/**
 * Assists in building {@link UserStory}s by providing the {@link AutomaticTrackProcessor}. Mainly a
 * convienence so you don't forget a step or step order.
 * 
 * @author Brian Chapman
 * 
 */
public class TrackUserStories {

	/**
	 * Used to construct all instances of TrackUserStoryOrganizer.
	 */
	public static class Builder
			extends ObjectBuilder<TrackUserStories> {

		/**
		 * Allows {@link TrackpointSeeker}s and {@link ProcessorFilter}s to be added to the chain.
		 * This supports the driver by itself and doesn't cause any performance issue by being
		 * available for those that need it.
		 * 
		 */
		private TrackpointSeekerChainBuilder<AutomaticTrackProcessor> seekerChainBuilder;
		private Iterable<Trackpoint> track;

		private AutomaticTrackProcessor.Builder processorBuilder;
		private TrackpointCounter counterOfUnfilteredPoints;
		private TrackpointCounter counterOfPointsWithBadTimestamps;
		private TrackpointCounter counterOfPointsWithLocation;
		TrackpointCounter counterOfPointsWithElevation;

		public Builder(Iterable<Trackpoint> track) {
			super(new TrackUserStories(track));
			this.processorBuilder = new AutomaticTrackProcessor.Builder(track);
			this.seekerChainBuilder = TrackpointSeekerChainBuilder.create(processorBuilder);
			counterOfUnfilteredPoints = seekerChainBuilder
					.registerWithCurrentProcessor(new TrackpointCounter.Builder());
			this.track = track;
		}

		public Builder addStandardFilters() {
			seekerChainBuilder.addSeeker(NonProgressingTimestampTrackpointFilter.create());
			counterOfPointsWithBadTimestamps = seekerChainBuilder
					.registerWithCurrentProcessor(new TrackpointCounter.Builder());
			seekerChainBuilder.addSeeker(MissingLocationTrackpointFilter.create());
			counterOfPointsWithLocation = seekerChainBuilder
					.registerWithCurrentProcessor(new TrackpointCounter.Builder());
			seekerChainBuilder.addSeeker(MissingElevationTrackpointFilter.create());
			counterOfPointsWithElevation = seekerChainBuilder
					.registerWithCurrentProcessor(new TrackpointCounter.Builder());

			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull("track", this.track);
			super.validate();
		}

		/**
		 * Add a {@link BaseTrackUserStory.Builder} to this builder. The {@link Track} and
		 * {@link TrackProcessorBase.Builder} are set for you.
		 * 
		 * @param builder
		 * @return the built {@link UserStory}
		 */
		public <StoryBuilderT extends BaseTrackUserStory.Builder<StoryT, ?>, StoryT extends BaseTrackUserStory<?>>
				StoryT setUserStory(StoryBuilderT builder) {
			builder.setProcessorBuilder(this.seekerChainBuilder.getCurrentProcessor());
			StoryT story = builder.build();
			building.stories.add(story);
			return story;
		}

		/**
		 * Used during building to configure the stories with Seekers and filters.
		 * 
		 * The order in which the Seekers are added affects downstream processors. Typically you
		 * would add seekers first.
		 * 
		 * @return
		 */
		public TrackpointSeekerChainBuilder<AutomaticTrackProcessor> getSeekerChainBuilder() {
			return this.seekerChainBuilder;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public TrackUserStories build() {
			TrackUserStories built = super.build();
			seekerChainBuilder.build();
			for (UserStory<?> story : built.stories) {
				story.process();
			}
			built.stories = Collections.unmodifiableSet(built.stories);

			Integer numberOfUnfilteredPoints = counterOfUnfilteredPoints.getCount();

			StatusMessages.Builder messagesBuilder = new StatusMessages.Builder();

			messagesBuilder.addMessage(	MessageStatus.OK,
										new CompleteMessage.Builder()
												.setMessage(TrackSummaryMessage.TRACK_PROCESSED)
												.addParameter(	TrackSummaryMessage.Param.TOTAL_COUNT,
																numberOfUnfilteredPoints).build());

			if (counterOfPointsWithBadTimestamps != null) {
				Integer numberOfPointsWithBadTimestamps = numberOfUnfilteredPoints
						- counterOfPointsWithBadTimestamps.getCount();
				// TODO:this is a little confusing since each number depends on the previous count.
				// it doesn't allow each counter/filter to be added individually.F
				if (numberOfPointsWithBadTimestamps > 0) {
					messagesBuilder
							.addMessage(MessageStatus.WARNING,
										new CompleteMessage.Builder()
												.setMessage(TrackSummaryMessage.NUMBER_OF_POINTS_BAD_TIMESTAMPS)
												.addParameter(	TrackSummaryMessage.Param.COUNT,
																numberOfPointsWithBadTimestamps)
												.addParameter(	TrackSummaryMessage.Param.TOTAL_COUNT,
																numberOfUnfilteredPoints).build());
				}

				if (counterOfPointsWithLocation != null) {
					Integer numberOfPointsMissingLocation = numberOfPointsWithBadTimestamps
							- counterOfPointsWithLocation.getCount();
					if (numberOfPointsMissingLocation > 0) {
						messagesBuilder
								.addMessage(MessageStatus.WARNING,
											new CompleteMessage.Builder()
													.setMessage(TrackSummaryMessage.NUMBER_OF_POINTS_MISSING_LOCATION)
													.addParameter(	TrackSummaryMessage.Param.COUNT,
																	numberOfPointsMissingLocation)
													.addParameter(	TrackSummaryMessage.Param.TOTAL_COUNT,
																	numberOfUnfilteredPoints).build());
					}
					if (counterOfPointsWithElevation != null) {
						Integer numberOfPointsMissingElevation = numberOfPointsMissingLocation
								- counterOfPointsWithElevation.getCount();
						if (numberOfPointsMissingElevation > 0) {
							messagesBuilder
									.addMessage(MessageStatus.WARNING,
												new CompleteMessage.Builder()
														.setMessage(TrackSummaryMessage.NUMBER_OF_POINTS_MISSING_LOCATION)
														.addParameter(	TrackSummaryMessage.Param.COUNT,
																		numberOfPointsMissingElevation)
														.addParameter(	TrackSummaryMessage.Param.TOTAL_COUNT,
																		numberOfUnfilteredPoints).build());
						}
					}
				}
			}

			built.messages = messagesBuilder.build();

			return built;
		}

	}// end Builder

	public static Builder create(Iterable<Trackpoint> track) {
		return new Builder(track);
	}

	private Set<UserStory<?>> stories = new HashSet<UserStory<?>>();
	private StatusMessages messages;

	private TrackUserStories(Iterable<Trackpoint> track) {

	}

	/**
	 * @return the stories
	 */
	protected Set<UserStory<?>> getStories() {
		return this.stories;
	}

	/**
	 * @return the messages
	 */
	public StatusMessages getMessages() {
		return this.messages;
	}

}
