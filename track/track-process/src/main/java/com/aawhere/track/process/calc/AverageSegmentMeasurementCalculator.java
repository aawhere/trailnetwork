/**
 * 
 */
package com.aawhere.track.process.calc;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;

import org.joda.time.Duration;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;

/**
 * For {@link Quantity}s that require a segment to calculate information about a segment (i.e.
 * segment {@link Length}) this will calculate the average values.
 * 
 * Since every {@link TrackSegment#getDuration()} has a {@link Duration}
 * 
 * @author roller
 * 
 */
public class AverageSegmentMeasurementCalculator
		implements TrackSegmentListener {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track .TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {

	}

}
