/**
 * 
 */
package com.aawhere.track.process;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;

/**
 * Used for parties interested when a new trackpoint is discovered during
 * {@link AutomaticTrackProcessor#inspect()} execution.
 * 
 * Register the listener with the {@link AutomaticTrackProcessor.Builder}
 * 
 * @author roller
 * 
 */
public interface TrackpointListener
		extends ProcessorListener {

	void handle(Trackpoint trackpoint, TrackProcessor processor);

}
