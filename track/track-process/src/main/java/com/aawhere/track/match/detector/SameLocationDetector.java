/**
 *
 */
package com.aawhere.track.match.detector;

import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

/**
 * Detects if the Start or Finish locations for both tracks are within the
 * distanceToBeConsideredSameLocation distance.
 * 
 * @author Brian Chapman
 * 
 */
public class SameLocationDetector {

	// TODO: TN-122 (System Configuration)
	public static final Length DISTANCE_TO_BE_CONSIDERED_SAME_LOCATION = MeasurementUtil.createLengthInMeters(600d);
	private Length distanceToBeConsideredSameLocation = DISTANCE_TO_BE_CONSIDERED_SAME_LOCATION;
	private Boolean hasSameStart = null;
	private Boolean hasSameFinish = null;
	private TrackSummary sourceTs;
	private TrackSummary targetTs;

	public Boolean hasSameStart() {
		if (hasSameStart == null) {
			hasSameStart = calculateSameStart();
		}
		return hasSameStart;
	}

	public Boolean hasSameFinish() {
		if (hasSameFinish == null) {
			hasSameFinish = calculateSameFinish();
		}
		return hasSameFinish;
	}

	private Boolean calculateSameStart() {
		return calculateSameLocation(sourceTs.getStart(), targetTs.getStart());
	}

	private Boolean calculateSameFinish() {
		return calculateSameLocation(sourceTs.getFinish(), targetTs.getFinish());
	}

	private Boolean calculateSameLocation(Trackpoint tp1, Trackpoint tp2) {
		Length distanceBetween = TrackUtil.distanceBetween(tp1, tp2);
		if (new QuantityMath<Length>(distanceBetween).lessThan(distanceToBeConsideredSameLocation)) {
			return true;
		}
		return false;
	}

	/**
	 * Used to construct all instances of FinishTrackpointListener.
	 */
	public static class Builder
			extends ObjectBuilder<SameLocationDetector> {

		public Builder() {
			super(new SameLocationDetector());
		}

		public Builder setDistanceToBeConsideredSameLocation(Length distanceToBeConsideredSameLocation) {
			building.distanceToBeConsideredSameLocation = distanceToBeConsideredSameLocation;
			return this;
		}

		public Builder setSourceTrackSummary(TrackSummary sourceTs) {
			building.sourceTs = sourceTs;
			return this;
		}

		public Builder setTargetTrackSummary(TrackSummary targetTs) {
			building.targetTs = targetTs;
			return this;
		}

		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.sourceTs);
			Assertion.assertNotNull(building.targetTs);
		}

	}// end Builder

	/** Use {@link Builder} to construct FinishTrackpointListener */
	private SameLocationDetector() {
	}

	public static Builder create() {
		return new Builder();
	}

}
