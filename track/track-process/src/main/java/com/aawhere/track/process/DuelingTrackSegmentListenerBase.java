/**
 * 
 */
package com.aawhere.track.process;

import com.aawhere.track.TrackSegment;

/**
 * @see DuelingProcessorListenerBase
 * 
 * @author Aaron Roller
 * 
 */
public abstract class DuelingTrackSegmentListenerBase
		extends DuelingProcessorListenerBase
		implements TrackSegmentListener {

	/**
	 * Used to construct all instances of DuelingTrackSegmentListenerBase.
	 */
	public static class Builder<BuilderResultT extends DuelingTrackSegmentListenerBase, BuilderT extends Builder<BuilderResultT, BuilderT>>
			extends DuelingProcessorListenerBase.Builder<BuilderResultT, BuilderT> {

		public Builder(BuilderResultT building) {
			super(building);
		}

	}// end Builder

	private TrackSegment sourceSegment;
	private TrackSegment targetSegment;

	/** Use {@link Builder} to construct DuelingTrackSegmentListenerBase */
	protected DuelingTrackSegmentListenerBase() {
	}

	@Override
	public final void handle(TrackSegment segment, TrackProcessor processor) {

		String sourceProcessorKey = getSourceProcessorKey();
		String targetProcessorKey = getTargetProcessorKey();

		String key = processor.getKey();
		if (key.equals(sourceProcessorKey)) {
			this.sourceSegment = segment;
		} else if (key.equals(targetProcessorKey)) {
			this.targetSegment = segment;
			handleTargetSegment(segment);
		} else {
			throw new IllegalArgumentException(key + " is not one of the participating processors "
					+ sourceProcessorKey + " or " + targetProcessorKey);
		}
	}

	/**
	 * @return the sourceSegment
	 */
	protected TrackSegment getSourceSegment() {
		return this.sourceSegment;
	}

	/**
	 * @return the targetSegment
	 */
	protected TrackSegment getTargetSegment() {
		return this.targetSegment;
	}

	/**
	 * @param segment
	 */
	abstract protected void handleTargetSegment(TrackSegment segment);

}
