package com.aawhere.track.process.calc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.measure.quantity.Acceleration;
import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.quantity.QuantityFactory;
import javax.measure.quantity.Velocity;
import javax.measure.unit.MetricSystem;

import org.apache.commons.collections4.CollectionUtils;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.reflect.ClassUtilsExtended;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.track.Track;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.provider.AccelerationSegmentProvider;
import com.aawhere.track.process.provider.VelocitySegmentProvider;
import com.google.api.client.util.Lists;
import com.google.appengine.repackaged.com.google.common.base.Supplier;
import com.google.common.collect.Range;

/**
 * Calculates the {@link SignalQuality} based on various analytics for the GPS {@link Track} to
 * provide some confidence to the user of the track to know the quality of the track regardless of
 * the gps device used.
 * 
 * TODO: Observations could indicate levels of quality if multiple ranges were used to indicate
 * problems. i.e. a speed of 100kph indidcates FAIR quality, but 1000kph indicates POOR. TN-934
 * 
 * TODO:customize ranges based on activity type
 * 
 * @author aroller
 * 
 */
public class TrackSignalQualityCalculator
		extends ProcessorListenerBase
		implements TrackSegmentListener, Supplier<SignalQuality> {

	/**
	 * Fast velocity, way outside the average, can indicate problems with gps signal.
	 */
	private VelocitySegmentProvider velocityProvider;

	private AccelerationSegmentProvider accelerationProvider;

	/**
	 * Speeding up or slowing down too quickly is a sign of changes happening that aren't realistic
	 * (i.e. too many G forces).
	 */
	private Range<Comparable<Acceleration>> accelerationRange;
	/**
	 * going too fast is a sign that an unrealistic jump to another location may have happened too
	 * quickly.
	 */
	private Range<Comparable<Velocity>> velocityRange;

	/** Keeps track of how many trackpoints there are so releative judgement can be made. */
	private TrackSegmentCounter counter;

	/**
	 * acceptable range for a segment distance. Segments that jump too far indicate a potential
	 * signal loss or other spike event.
	 */
	private Range<Comparable<Length>> distanceRange;
	/**
	 * Observations with {@link #OBSERVATION_BREECHED} {@link SignalQualityObservation#quality}.
	 * 
	 */
	private LinkedList<SignalQualityObservation> breeches = new LinkedList<>();

	/**
	 * Used to construct all instances of TrackSignalQualityCalculator.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackSignalQualityCalculator, Builder> {

		public Builder() {
			super(new TrackSignalQualityCalculator());
		}

		public Builder counter(TrackSegmentCounter counter) {
			building.counter = counter;
			return this;
		}

		public Builder velocityProvider(VelocitySegmentProvider velocityProvider) {
			building.velocityProvider = velocityProvider;
			return this;
		}

		public Builder accelerationProvider(AccelerationSegmentProvider accelerationProvider) {
			building.accelerationProvider = accelerationProvider;
			return this;
		}

		@Override
		protected void validate() {

			super.validate();
			Assertion.exceptions().notNull("velocitySegmentProvider", building.velocityProvider);
			Assertion.exceptions().notNull("accelerationProvider", building.accelerationProvider);
			Assertion.exceptions().notNull("counter", building.counter);
		}

		@Override
		public TrackSignalQualityCalculator build() {
			TrackSignalQualityCalculator built = super.build();
			QuantityFactory<Velocity> velocityFactory = MeasurementUtil.factory(Velocity.class);
			QuantityFactory<Acceleration> accelerationFactory = MeasurementUtil.factory(Acceleration.class);
			built.velocityRange = Range.closed(MeasurementUtil.comparable(velocityFactory
					.create(0, ExtraUnits.KILOMETERS_PER_HOUR)), MeasurementUtil.comparable(velocityFactory
					.create(100, ExtraUnits.KILOMETERS_PER_HOUR)));
			// assuming a runner can accelerate about the same as a bike
			// http://math.stackexchange.com/questions/486922/what-is-the-runners-average-acceleration-during-the-first-4-1s
			final int accelerationMpss = 5;
			built.accelerationRange = Range.closed(MeasurementUtil.comparable(accelerationFactory
					.create(-accelerationMpss, MetricSystem.METRES_PER_SQUARE_SECOND)), MeasurementUtil
					.comparable(accelerationFactory.create(accelerationMpss, MetricSystem.METRES_PER_SQUARE_SECOND)));
			built.distanceRange = Range.closed(	MeasurementUtil.comparable(MeasurementUtil.createLengthInMeters(0)),
												MeasurementUtil.comparable(MeasurementUtil.createLengthInMeters(100)));

			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrackSignalQualityCalculator */
	private TrackSignalQualityCalculator() {
	}

	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		SignalQualityObservation observation = new SignalQualityObservation();
		observation.segment = segment;
		if (velocityProvider.hasQuantity()) {
			observation.velocity = velocityProvider.getQuantity();
			if (!velocityRange.contains(MeasurementUtil.comparable(observation.velocity))) {
				observation.breeched(Velocity.class);
			}
		}
		if (accelerationProvider.hasQuantity()) {
			observation.acceleration = accelerationProvider.getQuantity();
			if (!accelerationRange.contains(MeasurementUtil.comparable(observation.acceleration))) {
				observation.breeched(Acceleration.class);
			}
		}
		observation.distance = segment.getDistance();
		if (observation.distance != null) {
			if (!distanceRange.contains(MeasurementUtil.comparable(observation.distance))) {
				observation.breeched(Length.class);
			}
		}
		if (CollectionUtils.isNotEmpty(observation.breeches)) {
			breeches.add(observation);
		}
	}

	/**
	 * Provides a single description of the signal quality known so far. It looks at
	 * {@link #breeches} and relative to the {@link #counter} it will determine quality. This
	 * calculates every time it is called so use efficiently.
	 * 
	 */
	@Override
	public SignalQuality get() {
		SignalQuality quality;
		int totalCount = this.counter.getCount();
		if (totalCount == 0) {
			quality = SignalQuality.UNKNOWN;
		} else {
			int breechCount = this.breeches.size();
			switch (breechCount) {
				case 0: {
					quality = SignalQuality.GREAT;
				}
					break;
				default: {
					Ratio ratio = MeasurementUtil.ratio(breechCount, totalCount);
					double percent = ratio.doubleValue(ExtraUnits.PERCENT);
					// these values can be changed after some feedback
					if (percent < 0.1) {
						quality = SignalQuality.GREAT;
					} else if (percent < 0.5) {
						quality = SignalQuality.GOOD;
					} else if (percent < 1) {
						quality = SignalQuality.FAIR;
					} else {
						quality = SignalQuality.POOR;
					}
				}

			}
		}
		return quality;
	}

	public List<SignalQualityObservation> breeches() {
		return breeches;
	}

	@Override
	public String toString() {
		return String.valueOf(get());
	}

	public static class SignalQualityObservation
			implements Serializable {

		private static final long serialVersionUID = -7683851197443414536L;
		TrackSegment segment;
		ArrayList<Class<? extends Quantity<?>>> breeches;
		Velocity velocity;
		Acceleration acceleration;
		Length distance;

		private SignalQualityObservation breeched(Class<? extends Quantity<?>> quantityType) {
			if (breeches == null) {
				breeches = Lists.newArrayList();
			}
			breeches.add(quantityType);
			return this;
		}

		@Override
		public String toString() {
			String separator = "||";
			return segment.getInterval() + separator + velocity + separator + acceleration + separator + distance
					+ separator + ClassUtilsExtended.simpleNames(breeches) + System.lineSeparator();
		}

	}

}
