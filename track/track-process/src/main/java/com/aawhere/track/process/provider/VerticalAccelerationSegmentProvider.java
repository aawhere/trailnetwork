/**
 * 
 */
package com.aawhere.track.process.provider;

import javax.measure.quantity.Acceleration;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;

/**
 * Looking at multiple {@link TrackSegment}s this will calculate the acceleration between the two
 * segments.
 * 
 * @author Aaron Roller on Apr 8, 2011
 * 
 */
public class VerticalAccelerationSegmentProvider
		implements TrackSegmentCalculatedQuantityProvider<Acceleration> {

	private TrackSegment previousSegment;
	private Acceleration acceleration;

	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		if (previousSegment != null) {

			acceleration = MeasurementUtil.acceleration(previousSegment.getVerticalVelocity(),
														segment.getVerticalVelocity(),
														TrackUtil.duration(previousSegment, segment));
			// NOTE:should we use duration from both segments?
		} else {
			acceleration = MeasurementUtil.createAccelerationInMpss(0);
		}
		previousSegment = segment;

	}

	@Override
	public Acceleration getQuantity() {
		return Assertion.assertNotNull(this.acceleration);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return this.acceleration != null;
	}
}
