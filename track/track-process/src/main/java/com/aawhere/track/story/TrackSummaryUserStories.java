/**
 *
 */
package com.aawhere.track.story;

import javax.measure.quantity.Length;

import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.Track;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.calc.GeoCellProvider;
import com.aawhere.track.process.calc.ProcessingCalculatorFactory;
import com.aawhere.track.process.calc.TrackSignalQualityProcessorUtil;
import com.aawhere.track.process.calc.TrackSummaryCreatorListener;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.listener.FinishTrackpointListener;
import com.aawhere.track.process.listener.StartTrackpointListener;

/**
 * Provides the implementation to satisfy the user stories related to Creating, displaying summary
 * information about a detailed {@link Track} resulting in a {@link TrackSummary}
 * 
 * Stories are important to have in the system to avoid writing this implementation in a
 * ServiceStandard which will subsequently use these stories to put the pieces together without the
 * requirement for storage,etc.
 * 
 * @see http://aawhere.jira.com/browse/TN-14
 * @see http://aawhere.jira.com/browse/TN-15
 * 
 * @author Aaron Roller
 * 
 */
public class TrackSummaryUserStories
		extends BaseTrackUserStory<TrackSummary> {

	public static final Length DEFAULT_BUFFER_WIDTH = MeasurementUtil.createLengthInMeters(20);

	TrackSummaryCreatorListener summaryCreator;

	@Override
	public void process() {
		// FIXME:the purpose of process is confusing. AR 2013-07-28
		this.setResult(summaryCreator.getTrackSummary());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.story.BaseTrackUserStory#getResult()
	 */
	@Override
	public TrackSummary getResult() {
		return summaryCreator.getTrackSummary();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.story.BaseTrackUserStory#hasResult()
	 */
	@Override
	public Boolean hasResult() {
		return summaryCreator.hasSummary();
	}

	/**
	 * Used to construct all instances of TrackSummaryUserStories.
	 * 
	 * TODO:consider accepting a {@link TrackProcessor.Builder} to piggyback TODO:consider
	 * generalizing all track stories into a base story that handles track/processing.
	 * 
	 */
	public static class Builder
			extends BaseTrackUserStory.Builder<TrackSummaryUserStories, Builder> {

		private ArchivedDocumentId documentId;
		private Spatial spatial = Spatial.BUILD_SPATIAL;

		public Builder() {
			super(new TrackSummaryUserStories());
		}

		/**
		 * @param docId
		 * @return
		 */
		public Builder setDocumentId(ArchivedDocumentId docId) {
			this.documentId = docId;
			return this;
		}

		public Builder withoutSpatial() {
			spatial = Spatial.DO_NOT_BUILD_SPATIAL;
			return this;
		}

		@Override
		protected void configureProcessorBuilder(TrackSummaryUserStories built) {
			configureProcessorBuilder(built, spatial);
		}

		private void configureProcessorBuilder(TrackSummaryUserStories built, Spatial spatial) {
			TrackSummaryCreatorListener.Builder tsBuilder = new TrackSummaryCreatorListener.Builder();

			final TrackProcessorBase.Builder<?> processorBuilder = getProcessorBuilder();
			ProcessingCalculatorFactory calcFactory = ProcessingCalculatorFactory.create(processorBuilder);
			tsBuilder
					.setDocumentId(this.documentId)

					.setTrackpointCounter(new TrackpointCounter.Builder().registerWithProcessor(getProcessorBuilder())
							.build())
					.setTotalDistanceCalculator(calcFactory.newTotalDistanceCalculator(processorBuilder))
					.setStartListener(new StartTrackpointListener.Builder().registerWithProcessor(processorBuilder)
							.build())
					.setFinishListener(new FinishTrackpointListener.Builder().registerWithProcessor(processorBuilder)
							.build())
					.setMeanDistanceBetweenTrackpointsCalculator(calcFactory.newMeanDistanceBetweenTrackpointsCalculator(processorBuilder))
					.setMeanDurationBetweenTrackpointsCalculator(calcFactory.newMeanDurationBetweenTrackpointsCalculator(processorBuilder))
					.setSignalQualitySupplier(TrackSignalQualityProcessorUtil.signalQualitySupplier(calcFactory))
					// .setElevationChangeCalculator(
					// calcFactory.newElevationChangeCalculator(processorBuilder))
					.setElevationGainCalculator(calcFactory.newElevationGainCalculator(processorBuilder));
			if (spatial == Spatial.BUILD_SPATIAL) {
				tsBuilder.setGeoCellProvider(new GeoCellProvider.Builder().registerWithProcessor(processorBuilder)
						.setBufferLength(DEFAULT_BUFFER_WIDTH).setTargetResolution(TrackSummary.MAX_BOUNDS_RESOLUTION)
						.build());
				tsBuilder.setBoundingBoxCalculator(calcFactory.newBoundingBoxCalculator(processorBuilder));
			}
			built.summaryCreator = tsBuilder.registerWithProcessor(processorBuilder).build();
		}
	}// end Builder

	/** Use {@link Builder} to construct TrackSummaryUserStories */
	private TrackSummaryUserStories() {
	}

	public static Builder create() {
		return new Builder();
	}

	private enum Spatial {
		BUILD_SPATIAL, DO_NOT_BUILD_SPATIAL;
	}
}
