/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import com.aawhere.measure.Elevation;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * @author Aaron Roller on Apr 20, 2011
 * 
 */
public class ElevationBetweenFilter
		extends BetweenFilter<Elevation>
		implements TrackpointListener {

	/**
	 * Used to construct all instances of ElevationChangeBetweenFilter.
	 */
	public static class Builder
			extends BetweenFilter.Builder<ElevationBetweenFilter, Elevation> {

		public Builder() {
			super(new ElevationBetweenFilter());
			super.setIncludeEndpoints(false);
		}

	}// end Builder

	/** Use {@link Builder} to construct ElevationChangeBetweenFilter */
	private ElevationBetweenFilter() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		super.handle(trackpoint.getElevation(), trackpoint);

	}

}
