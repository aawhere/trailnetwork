/**
 *
 */
package com.aawhere.track.process.listener;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * Super basic...is notified and reports the startpoint.
 * 
 * @author Aaron Roller
 * 
 */
public class StartTrackpointListener
		extends ProcessorListenerBase
		implements TrackpointListener {

	private Trackpoint start;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		if (this.start == null) {
			this.start = trackpoint;
		}

	}

	/**
	 * @return the start
	 */
	public Trackpoint getStart() {
		return this.start;
	}

	/**
	 * Used to construct all instances of StartTrackpointListener.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<StartTrackpointListener, Builder> {

		public Builder() {
			super(new StartTrackpointListener());
		}

	}// end Builder

	/** Use {@link Builder} to construct StartTrackpointListener */
	private StartTrackpointListener() {
	}

	public static Builder create() {
		return new Builder();
	}

}
