/**
 *
 */
package com.aawhere.track.process.processor;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.TrackpointListener;

/**
 * The main iterator that delegates calculations, formatting and any other actions to
 * {@link TrackpointListener}s.
 * 
 * By using the receiver, iterations can be more efficient since processing is de-coupled and
 * independent.
 * 
 * @author roller
 * 
 */
public class AutomaticTrackProcessor
		extends TrackProcessorBase {

	private Iterable<Trackpoint> track;

	AutomaticTrackProcessor(Iterable<Trackpoint> track) {
		this.track = track;
	}

	/**
	 * The primary iteration of the track who's primary responsibility is to notify listeners of
	 * events.
	 * 
	 */
	protected void inspect() {

		for (Trackpoint trackpoint : track) {
			notifyTrackpointListeners(trackpoint);
		}

		finish();
	}

	public static class Builder
			extends TrackProcessorBase.Builder<AutomaticTrackProcessor> {

		/**
		 *
		 */
		public Builder(Iterable<Trackpoint> track) {
			super(new AutomaticTrackProcessor(track));
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.TrackProcessorBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull("track", building.track);
			super.validate();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public AutomaticTrackProcessor build() {

			AutomaticTrackProcessor built = super.build();
			built.inspect();
			return built;
		}

	}

	public static Builder create(Iterable<Trackpoint> track) {
		return new AutomaticTrackProcessor.Builder(track);
	}
}
