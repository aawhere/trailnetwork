package com.aawhere.track.process.calc;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * Calculates the maximum and minimum Latitude a track covers.
 * 
 * @author Brian Chapman
 * 
 */
public final class BoundingBoxCalculator
		extends ProcessorListenerBase
		implements TrackpointListener {

	/**
	 * @return
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<BoundingBoxCalculator, Builder> {

		/**
		 * @param building
		 */
		public Builder() {
			super(new BoundingBoxCalculator());
		}

	}

	private BoundingBoxCalculator() {
	};

	private Latitude northLat;
	private Latitude southLat;
	private Longitude eastLon;
	private Longitude westLon;
	private Longitude previous;
	private boolean crossed180Meridian = false;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track .TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint point, TrackProcessor processor) {
		Latitude lat = point.getLocation().getLatitude();
		Longitude lon = point.getLocation().getLongitude();

		QuantityMath<Latitude> mathLat = QuantityMath.create(lat);
		QuantityMath<Longitude> mathLon = QuantityMath.create(lon);

		handleInitialTrackpoint(lat, lon);
		handleSegmentCrosses180Meridian(mathLon);
		handleMaxLat(mathLat);
		handleMinLat(mathLat);
		handleEastLon(mathLon);
		handleWestLon(mathLon);
		previous = lon;
	}

	/**
	 * returns the maxiumum {@link Latitude} received by this calculator
	 * 
	 * @return
	 */
	public Latitude getNorth() {
		return northLat;
	}

	/**
	 * returns the minimum {@link Latitude} received by this calculator
	 * 
	 * @return
	 */
	public Latitude getSouth() {
		return southLat;
	}

	/**
	 * returns the maxiumum {@link Longitude} received by this calculator
	 * 
	 * @return
	 */
	public Longitude getEast() {
		return eastLon;
	}

	/**
	 * returns the minimum {@link Longitude} received by this calculator
	 * 
	 * @return
	 */
	public Longitude getWest() {
		return westLon;
	}

	public GeoCoordinate getNorthEast() {
		return GeoCoordinate.create().setLatitude(northLat).setLongitude(eastLon).build();
	}

	public GeoCoordinate getSouthWest() {
		return GeoCoordinate.create().setLatitude(southLat).setLongitude(westLon).build();
	}

	public BoundingBox getBoundingBox() {
		return new BoundingBox.Builder().setNorthEast(getNorthEast()).setSouthWest(getSouthWest()).build();
	}

	private void handleMaxLat(QuantityMath<Latitude> math) {
		if (math.greaterThan(northLat)) {
			northLat = math.getQuantity();
		}
	}

	private void handleMinLat(QuantityMath<Latitude> math) {
		if (math.lessThan(southLat)) {
			southLat = math.getQuantity();
		}
	}

	/*
	 * The logic here compensates for a bounding box that crosses from + to - longitude. Which is
	 * the case when you are somewhere over the pacific ocean (and part of Russia) and the poles ...
	 * lots of trails there, I know, but possible none the less. If we find ourselves dealing with
	 * the 180 meridian and other issues like the north pole singleton a lot, consider
	 * http://en.wikipedia.org/wiki/N-vector calculations instead.
	 */
	private void handleEastLon(QuantityMath<Longitude> math) {
		if (crossed180Meridian) {
			if (math.negative() && QuantityMath.create(eastLon).positive()) {
				eastLon = math.getQuantity();
			} else if (math.negative() && math.greaterThan(eastLon)) {
				eastLon = math.getQuantity();
			}
		} else {
			if (math.greaterThan(eastLon)) {
				eastLon = math.getQuantity();
			}
		}
	}

	/*
	 * TODO:move the meridian logic into BoundingBoxUtil See #handleEastLon
	 * @param math
	 */
	private void handleWestLon(QuantityMath<Longitude> math) {
		if (crossed180Meridian) {
			if (math.positive() && QuantityMath.create(westLon).negative()) {
				westLon = math.getQuantity();
			} else if (math.positive() && math.lessThan(westLon)) {
				westLon = math.getQuantity();
			}
		} else {
			if (math.lessThan(westLon)) {
				westLon = math.getQuantity();
			}
		}
	}

	/*
	 * If the absolute value of the difference between the two points is greater than 180, then it
	 * crosses the 180 meridian. This flag is set once for a track.
	 */
	private void handleSegmentCrosses180Meridian(QuantityMath<Longitude> math) {
		final Double CROSSING_FACTOR = 180D;

		if (previous == null) {
			return;
		}

		if (Math.abs(math.minus(previous).getQuantity().getInDecimalDegrees()) > CROSSING_FACTOR) {
			crossed180Meridian = true;
		}
	}

	private void handleInitialTrackpoint(Latitude lat, Longitude lon) {
		if (southLat == null) {
			southLat = lat;
		}
		if (northLat == null) {
			northLat = lat;
		}
		if (westLon == null) {
			westLon = lon;
		}
		if (eastLon == null) {
			eastLon = lon;
		}
	}

}
