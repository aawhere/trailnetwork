/**
 *
 */
package com.aawhere.track.process.calc.datatable;

import com.aawhere.lang.Assertion;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.util.rb.Message;

import com.google.visualization.datasource.datatable.TableCell;
import com.google.visualization.datasource.datatable.value.ValueType;

/**
 * TODO: Move this and it's friends to and export project. Perhaps activity-export.
 * 
 * @author brian
 * 
 */
public abstract class TableCellProvider
		extends ProcessorListenerBase {

	private TableCell cell;
	private Message columnLabel;
	private String columnId;
	private ValueType columnType;

	/**
	 * Used to construct all instances of TableCellProvider.
	 */
	public static class Builder<BuilderT extends Builder<BuilderT, ProviderT>, ProviderT extends TableCellProvider>
			extends ProcessorListenerBase.Builder<ProviderT, BuilderT> {

		public Builder(ProviderT provider) {
			super(provider);
		}

		public BuilderT setColumnLabel(Message desc) {
			building().columnLabel = desc;
			return dis;
		}

		public BuilderT columnType(ValueType columnType) {
			building().columnType = columnType;
			return dis;
		}

		/**
		 * @return
		 */
		private TableCellProvider building() {
			return ((TableCellProvider) building);
		}

		public BuilderT columnId(String columnId) {
			building().columnId = columnId;
			return dis;
		}

		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(((TableCellProvider) building).columnLabel);
			Assertion.exceptions().notNull("columnType", building().columnType);
			Assertion.exceptions().notNull("columnId", building().columnId);

		}

	}// end Builder

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.datatable.TableCellProvider#getCell()
	 */
	public TableCell getCell() {
		return cell;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.TableCellProvider#hasCell()
	 */
	public Boolean hasCell() {
		return this.cell != null;
	}

	protected void setCell(TableCell cell) {
		this.cell = cell;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.TableCellProvider#getColumnLabel()
	 */
	public Message getColumnLabel() {
		return columnLabel;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.datatable.TableCellProvider#getColumnId()
	 */
	public String getColumnId() {
		return this.columnId;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.datatable.TableCellProvider#getColumnType()
	 */
	public ValueType getColumnType() {
		return this.columnType;
	}
}
