/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import com.aawhere.track.Trackpoint;

/**
 * Filters out trackpoints where the current timestamp is not after the previous including
 * timestamps that are equal.
 * 
 * @author roller
 * 
 */
public class NonSequentialTimestampFilter
		extends SequentialTrackpointFilterBase {

	/** A convience method for constructing a builder */
	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct NonSequentialTimestampFilter */
	private NonSequentialTimestampFilter() {
	}

	/**
	 * Used to construct all instances of NonSequentialTimestampFilter.
	 */
	public static class Builder
			extends SequentialTrackpointFilterBase.Builder<NonSequentialTimestampFilter, Builder> {

		public Builder() {
			super(new NonSequentialTimestampFilter());
		}

	}// end Builder

	/**
	 * @return true if the previousTrackpoint timestamp is before the current Trackpoint's timestamp
	 */
	@Override
	protected Boolean includeCurrentTrackpoint(Trackpoint previousTrackpoint, Trackpoint currentTrackpoint) {
		return previousTrackpoint.getTimestamp().isBefore(currentTrackpoint.getTimestamp());
	}

}
