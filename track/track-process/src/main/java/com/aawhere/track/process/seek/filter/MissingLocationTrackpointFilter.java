/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * A simple {@link ProcessorFilter} and {@link TrackpointListener} the removes any
 * {@link Trackpoint} that doesn't report location or erroneously just a latitude/longitude.
 * 
 * @author aroller
 * 
 */
public class MissingLocationTrackpointFilter
		extends TrackpointSeekerBase
		implements ProcessorFilter, TrackpointListener {

	/**
	 * Used to construct all instances of MissingLocationTrackpointFilter.
	 */
	public static class Builder
			extends TrackpointSeekerBase.Builder<MissingLocationTrackpointFilter, Builder> {

		public Builder() {
			super(new MissingLocationTrackpointFilter());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct MissingLocationTrackpointFilter */
	private MissingLocationTrackpointFilter() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		if (trackpoint.isLocationCapable()) {
			super.desiredTrackpointFound(trackpoint);
		}
	}

}
