/**
 *
 */
package com.aawhere.track.process;

import com.aawhere.track.match.processor.DuelingTrackProcessor;

/**
 * Used by {@link ProcessorListener}s that need to be told to reset their state. Useful when using a
 * {@link DuelingTrackProcessor} and you need to loop through the target track multiple times.
 * 
 * see {@link TrackProcessorBase#reset()}
 * 
 * @author Brian Chapman
 * 
 */
public interface ResettingProcessorListener {

	/** Completes whatever is needed by the implementor */
	void reset();
}
