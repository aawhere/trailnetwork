/**
 * 
 */
package com.aawhere.track.process.processor;

import java.util.Iterator;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListener;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * A semi-automatic {@link TrackProcessor} that will iterate through trackpoints until a limit is
 * reached as indicated by one or more participating {@link TrackpointSeeker}s. A traditional seeker
 * chain uses a processor to provide trackpoints to a seeker which notifies others downstream that a
 * trackpoint is found. Like a tradition processor this provides the trackpoints to the seeker, but
 * instead it receives notification when a trackpoint is found since this is the
 * {@link TrackpointSeeker#getReceiver()}. Once notification is received this will stop providing
 * trackpoints until some external control calls {@link #resume()}. .
 * 
 * @author aroller
 * 
 */
public class SemiAutomaticTrackProcessor
		extends TrackpointSeekingProcessor {

	/**
	 * Used to construct all instances of SemiAutomaticTrackProcessor.
	 */
	public static class Builder
			extends TrackpointSeekingProcessor.BaseBuilder<SemiAutomaticTrackProcessor, Builder> {
		private Iterable<Trackpoint> track;

		public Builder() {
			super(new SemiAutomaticTrackProcessor());
		}

		public Builder setTrack(Iterable<Trackpoint> track) {
			this.track = track;
			return this;
		}

		/**
		 * Registers the given seeker with the processor being built. It is registered as a
		 * {@link ProcessorListener}. The processor being built is set as it's receiver so it is
		 * both a listener of the processor and notifier of sought after points. It also builds the
		 * seeker ensuring it is ready for processing and returns it for client reference (instead
		 * of the standard Builder).
		 * 
		 * @param seekerBuilder
		 * @return
		 */
		public <S extends TrackpointSeekerBase> S registerLimitSeeker(TrackpointSeekerBase.Builder<S, ?> seekerBuilder) {
			seekerBuilder.setReceiver(building);
			seekerBuilder.registerWithProcessor(this);
			return seekerBuilder.build();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.TrackProcessorBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("track", this.track);
		}

		public SemiAutomaticTrackProcessor build() {
			SemiAutomaticTrackProcessor built = super.build();
			built.iterator = this.track.iterator();
			// kick off the processing
			built.resume();
			return built;
		}

	}// end Builder

	public static Builder createSemiAutomatic() {
		return new Builder();
	}

	private Iterator<Trackpoint> iterator;
	/**
	 * This is the indicator that a {@link #desiredTrackpointFound(Trackpoint)} has been found and
	 * iteration should be paused.
	 */
	private Boolean haltRequested = false;

	/** Use {@link Builder} to construct SemiAutomaticTrackProcessor */
	private SemiAutomaticTrackProcessor() {
	}

	/**
	 * called when iteration has stopped, but the external controller wishes for processing to
	 * continue. Returns true if there are more trackpoints to iterate and false when the end of
	 * iteration has been reached.
	 * 
	 * @return
	 */
	public Boolean resume() {

		// This is a synchronous loop that with each iteration a seeker may find the target and
		// notify us to stop
		while (this.iterator.hasNext() && !haltRequested) {
			super.notifyTrackpointListeners(iterator.next());
			// place this check here we ensure that this is the last trackpoint and finish is only
			// called once.
			if (!iterator.hasNext()) {
				super.finish();
			}
		}
		// reset the halt signal for the next resume call
		haltRequested = false;
		return this.iterator.hasNext();
	}

	/** Indicates if there are more trackpoints to iterate. */
	public Boolean hasMore() {
		return this.iterator.hasNext();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.process.processor.TrackpointSeekingProcessor#desiredTrackpointFound(com
	 * .aawhere.track.Trackpoint)
	 */
	@Override
	public Trackpoint desiredTrackpointFound(Trackpoint trackpoint) {
		this.haltRequested = true;
		return trackpoint;
	}

	/**
	 * Being a receiver finish being called here can cause an infinite recursive loop. This does
	 * nothing because finish is determined by the end of the track when called by resume.
	 * 
	 * @see com.aawhere.track.process.processor.TrackpointSeekingProcessor#finish()
	 */
	@Override
	public void finish() {
		// this line intentionally left blank
	}
}
