/**
 *
 */
package com.aawhere.track.process.calc.datatable;

import javax.measure.quantity.Quantity;

import com.aawhere.lang.Assertion;
import com.aawhere.personalize.QuantityDisplay;
import com.aawhere.personalize.QuantityPersonalizer;

import com.google.visualization.datasource.datatable.TableCell;
import com.google.visualization.datasource.datatable.value.ValueType;

/**
 * Base Marker Class for {@link TableCell} Providers.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class QuantityTableCellProviderBase
		extends TableCellProvider {

	/**
	 * Used to construct all instances.
	 */
	public static class Builder<BuilderT extends Builder<BuilderT, ProviderT>, ProviderT extends QuantityTableCellProviderBase>
			extends TableCellProvider.Builder<BuilderT, ProviderT> {

		public Builder(ProviderT provider) {
			super(provider);
			columnType(ValueType.NUMBER);
		}

		public BuilderT setPersonalizer(QuantityPersonalizer plizer) {
			((QuantityTableCellProviderBase) building).plizer = plizer;
			return dis();
		}

		@SuppressWarnings("unchecked")
		private BuilderT dis() {
			return (BuilderT) this;
		}

		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(((QuantityTableCellProviderBase) building).plizer);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.ProcessorListenerBase.Builder#build()
		 */
		@Override
		public ProviderT build() {

			return super.build();

		}
	}// end Builder

	private QuantityPersonalizer plizer;

	protected TableCell buildCell(Quantity<?> q) {
		plizer.personalize(q);
		QuantityDisplay plizedQuantity = plizer.getResult().getPersonalized();
		TableCell cell = new TableCell(plizedQuantity.getValue());
		cell.setFormattedValue(plizedQuantity.getFormatted());
		return cell;
	}

}
