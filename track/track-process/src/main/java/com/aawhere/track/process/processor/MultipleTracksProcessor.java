/**
 *
 */
package com.aawhere.track.process.processor;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessorBase;

/**
 * Track processor that combines all trackpoints from multiple tracks into a single processing
 * stream. This behaves similar to an {@link AutomaticTrackProcessor}, but continues for each track
 * provided.
 * 
 * 
 * @author Aaron Roller on Apr 6, 2011
 * 
 */
public class MultipleTracksProcessor
		extends TrackProcessorBase {

	/**
	 * Used to construct all instances of MultipleTracksProcessor.
	 */
	public static class Builder
			extends TrackProcessorBase.Builder<MultipleTracksProcessor> {

		public Builder() {
			super(new MultipleTracksProcessor());
		}

		public Builder setTracks(Iterable<Track> tracks) {
			building.tracks = tracks;
			return this;
		}

		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("tracks", building.tracks);
		}

		@Override
		public MultipleTracksProcessor build() {

			MultipleTracksProcessor result = super.build();
			result.process();
			return result;
		}
	}// end Builder

	/** Use {@link Builder} to construct MultipleTracksProcessor */
	private MultipleTracksProcessor() {
	}

	private Iterable<Track> tracks;

	/**
	 * The primary iteration of the track who's primary responsibility is to notify listeners of
	 * events.
	 * 
	 */
	protected void process() {

		for (Track track : tracks) {
			for (Trackpoint trackpoint : track) {
				notifyTrackpointListeners(trackpoint);
			}
		}
		finish();
	}

}
