/**
 * 
 */
package com.aawhere.track.match;

import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.DuelingTrackpointListenerBase;

/**
 * Simply answers the question if two tracks have met at the same place and time.
 * 
 * @author Aaron Roller
 * 
 */
public class TracksSimultaneousMetListener
		extends DuelingTrackpointListenerBase {

	/**
	 * Used to construct all instances of TrackMetDetector.
	 */
	public static class Builder
			extends DuelingTrackpointListenerBase.Builder<TracksSimultaneousMetListener, Builder> {

		public Builder() {
			super(new TracksSimultaneousMetListener());
		}

		public Builder setMaxDistanceToBeConsideredTogether(Length maxDistanceToBeConsideredTogether) {
			building.maxDistanceToBeConsideredTogether = maxDistanceToBeConsideredTogether;
			return this;
		}

		@Override
		protected void validate() {
			super.validate();

			Assertion.assertNotNull(building.maxDistanceToBeConsideredTogether);
		}

	}// end Builder

	private Length maxDistanceToBeConsideredTogether;

	/**
	 * Indicates if they have ever met. Assume not until proven otherwise.
	 * 
	 */
	private boolean met = false;

	/** Use {@link Builder} to construct TrackMetDetector */
	private TracksSimultaneousMetListener() {
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.process.DuelingTrackpointListenerBase#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public final void handleTargetTrackpoint(Trackpoint trackpoint) {

		Trackpoint sourceTrackpoint = getSourceTrackpoint();
		// compare the target to the source already received. Source must
		// always come first.
		if (sourceTrackpoint.getTimestamp().equals(trackpoint.getTimestamp())) {
			Length distanceBetween = TrackUtil.distanceBetween(sourceTrackpoint, trackpoint);
			if (new QuantityMath<Length>(distanceBetween).lessThan(this.maxDistanceToBeConsideredTogether)) {
				this.met = true;
			}
		} else {
			throw new IllegalArgumentException("Timestamps aren't equal...why are we trying to see if they met?"
					+ sourceTrackpoint + trackpoint);
		}
	}

	public Boolean isMet() {
		return met;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((this.maxDistanceToBeConsideredTogether == null) ? 0 : this.maxDistanceToBeConsideredTogether
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TracksSimultaneousMetListener other = (TracksSimultaneousMetListener) obj;
		if (this.maxDistanceToBeConsideredTogether == null) {
			if (other.maxDistanceToBeConsideredTogether != null)
				return false;
		} else if (!this.maxDistanceToBeConsideredTogether.equals(other.maxDistanceToBeConsideredTogether))
			return false;
		return true;
	}

}
