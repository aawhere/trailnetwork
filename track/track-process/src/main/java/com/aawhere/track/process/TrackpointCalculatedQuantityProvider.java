/**
 * 
 */
package com.aawhere.track.process;

import javax.measure.quantity.Quantity;

/**
 * Provides a {@link Quantity} retrieved during the
 * {@link TrackpointListener#handle(com.aawhere.track.Trackpoint, com.aawhere.track.process.TrackProcessor)}
 * execution during a {@link TrackProcessor} iteration.
 * 
 * 
 * @author roller
 * 
 */
public interface TrackpointCalculatedQuantityProvider<QuantityT extends Quantity<?>>
		extends TrackpointListener, ProcessorListenerQuantityProvider<QuantityT> {

	public QuantityT getQuantity();

}
