/**
 * 
 */
package com.aawhere.track.process;

import com.aawhere.lang.Assertion;
import com.aawhere.track.match.DistanceApartMeasurementProvider;

/**
 * A track processor that specializes in receiving notifications from two different tracks in two
 * different iterations.
 * 
 * Yeah, pretty crazy, but the one processor uses {@link TrackpointSeeker}s to throttle the
 * iteration so they remain related during processing.
 * 
 * The source track is provided through normal iteration as either a {@link TrackpointListener} or
 * {@link TrackSegmentListener}. The target Track
 * 
 * @author Aaron Roller
 * 
 */
public abstract class DuelingProcessorListenerBase
		extends ProcessorListenerBase {

	/**
	 * Used to construct all instances of DuelingTrackProcessorBase.
	 */
	public static class Builder<BuilderResultT extends DuelingProcessorListenerBase, BuilderT extends Builder<BuilderResultT, BuilderT>>
			extends ProcessorListenerBase.Builder<BuilderResultT, BuilderT> {

		public Builder(BuilderResultT processor) {
			super(processor);
		}

		/**
		 * Receives updates for the {@link DistanceApartMeasurementProvider#sourceSegment}. <>?
		 * 
		 * @param processorBuilder
		 */
		public BuilderT registerWithSourceProcessor(
				TrackProcessorBase.Builder<? extends TrackProcessorBase> processorBuilder) {
			((DuelingProcessorListenerBase) building).sourceProcessorKey = processorBuilder.getTrackProcessorKey();
			processorBuilder.register(building);
			return dis;
		}

		public BuilderT registerWithTargetProcessor(
				TrackProcessorBase.Builder<? extends TrackProcessorBase> processorBuilder) {
			super.registerWithProcessor(processorBuilder);
			((DuelingProcessorListenerBase) building).targetProcessorKey = processorBuilder.getTrackProcessorKey();
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.TrackProcessorBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(((DuelingProcessorListenerBase) building).sourceProcessorKey);
			Assertion.assertNotNull(((DuelingProcessorListenerBase) building).targetProcessorKey);
		}

	}// end Builder

	private String sourceProcessorKey;

	private String targetProcessorKey;

	/**
	 * @return the sourceProcessorKey
	 */
	String getSourceProcessorKey() {
		return this.sourceProcessorKey;
	}

	/**
	 * @return the targetProcessorKey
	 */
	String getTargetProcessorKey() {
		return this.targetProcessorKey;
	}
}
