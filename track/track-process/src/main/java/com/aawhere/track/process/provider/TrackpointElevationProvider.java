/**
 * 
 */
package com.aawhere.track.process.provider;

import com.aawhere.measure.Elevation;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointCalculatedQuantityProvider;

/**
 * @author roller
 * 
 */
public class TrackpointElevationProvider
		extends ProcessorListenerBase
		implements TrackpointCalculatedQuantityProvider<Elevation> {

	/**
	 * Used to construct all instances of TrackpointElevationProvider.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackpointElevationProvider, Builder> {

		public Builder() {
			super(new TrackpointElevationProvider());
		}

	}// end Builder

	/** Use {@link Builder} to construct TrackpointElevationProvider */
	private TrackpointElevationProvider() {
	}

	public static Builder create() {
		return new Builder();
	}

	private Elevation elevation;

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.process.calc.TrackpointCalculatedQuantityProvider#getTrackpointQuantity()
	 */
	@Override
	public Elevation getQuantity() {
		return elevation;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		elevation = trackpoint.getElevation();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return this.elevation != null;
	}

}
