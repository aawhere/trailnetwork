/**
 *
 */
package com.aawhere.track.match.processor;

import javax.annotation.processing.Processor;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Track;
import com.aawhere.track.process.DuelingProcessorListenerBase;
import com.aawhere.track.process.DuelingTrackpointSeekerBase;
import com.aawhere.track.process.ProcessorListener;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;

/**
 * A convenience {@link Processor} that takes two {@link Track}s, a {@link TrackpointSeeker} and any
 * number of {@link ProcessorListener} or {@link DuelingProcessorListenerBase}s and links them
 * together for you using a {@link TrackpointSeekingProcessor}.
 * 
 * @author Brian Chapman
 * 
 */
public class DuelingTrackProcessor
		extends TrackProcessorBase {

	@SuppressWarnings("unused")
	private Track sourceTrack;
	private Track targetTrack;

	/** use the Builder to create */
	private DuelingTrackProcessor(Track sourceTrack) {
		this.sourceTrack = sourceTrack;
	}

	public static class Builder
			extends TrackProcessorBase.Builder<DuelingTrackProcessor> {

		private AutomaticTrackProcessor.Builder sourceProcessorBuilder;
		private TrackpointSeekingProcessor.Builder targetProcessorBuilder;
		private DuelingTrackpointSeekerBase.Builder<? extends DuelingTrackpointSeekerBase, ? extends DuelingTrackpointSeekerBase.Builder<?, ?>> trackpointSeekerBuilder;

		/**
		 *
		 */
		public Builder(Track sourceTrack) {
			super(new DuelingTrackProcessor(sourceTrack));
			sourceProcessorBuilder = AutomaticTrackProcessor.create(sourceTrack);
			targetProcessorBuilder = new TrackpointSeekingProcessor.Builder();
		}

		public Builder setTarget(Track targetTrack) {
			building.targetTrack = targetTrack;
			return this;
		}

		public
				Builder
				setTrackpointSeeker(
						DuelingTrackpointSeekerBase.Builder<? extends DuelingTrackpointSeekerBase, ? extends DuelingTrackpointSeekerBase.Builder<?, ?>> trackpointSeekerBuilder) {
			this.trackpointSeekerBuilder = trackpointSeekerBuilder;
			return this;
		}

		public TrackProcessorBase.Builder<? extends TrackProcessorBase> getSourceProcessor() {
			return sourceProcessorBuilder;
		}

		public TrackProcessorBase.Builder<? extends TrackProcessorBase> getTargetProcessor() {
			return targetProcessorBuilder;
		}

		@Override
		public void validate() {
			Assertion.assertNotNull("trackpointSeekerBuilder", trackpointSeekerBuilder);
			Assertion.assertNotNull("targetTrack", building.targetTrack);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public DuelingTrackProcessor build() {
			DuelingTrackProcessor duelingProcessor = super.build();
			TrackpointSeekingProcessor targetProcessor = targetProcessorBuilder.build();

			trackpointSeekerBuilder.setReceiver(targetProcessor).setTargetTrack(duelingProcessor.targetTrack)
					.registerWithProcessor(sourceProcessorBuilder).build();

			sourceProcessorBuilder.build();

			return duelingProcessor;
		}
	}

	public static Builder create(Track sourceTrack) {
		return new Builder(sourceTrack);
	}

}
