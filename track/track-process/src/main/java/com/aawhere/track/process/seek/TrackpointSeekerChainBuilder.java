/**
 * 
 */
package com.aawhere.track.process.seek;

import java.util.ArrayList;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.collections.ListUtilsExtended;
import com.aawhere.track.process.ProcessorListener;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.TrackpointSeekerBase;
import com.aawhere.track.process.TrackpointSeekerBase.Builder;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor.BaseBuilder;
import com.aawhere.track.process.seek.filter.ProcessorFilter;

/**
 * Especially useful when linking together {@link ProcessorFilter}s. This abstracts the building
 * process which is quite confusing due to the order in which {@link ProcessorListener}s,
 * {@link TrackpointSeeker}s must be built.
 * 
 * This chain limits each {@link TrackpointSeekingProcessor} to be limited to a single
 * {@link TrackpointSeeker} that will drive it. The building therefore is done by calling
 * {@link #addSeeker(com.aawhere.track.process.TrackpointSeekerBase.Builder)} , then calling
 * {@link #registerWithCurrentProcessor(com.aawhere.track.process.ProcessorListenerBase.Builder)}
 * for any listener desired which will be registered with the same
 * {@link TrackpointSeekingProcessor} being managed until the next call to addSeeker.
 * 
 * <pre>
 * builder.addSeeker(seeker1);// creates the trackpoint seeking processor
 * builder.registerWithCurrentProcess(counterForSeeker1);// registers the listener with the created
 * // seeking processor
 * builder.addSeeker(seeker2);// creates another seeking processor
 * build.registerWithCurrentProcessor(counterforSeeker2);
 * build.registerWIthCurrentProcessor(calculatorForSeeker2);
 * build.buil(); // completes building all.
 * </pre>
 * 
 * @author aroller
 * 
 */
public class TrackpointSeekerChainBuilder<T extends TrackProcessorBase> {

	private TrackProcessorBase.Builder<?> driver;

	private ArrayList<Pair<TrackpointSeekerBase.Builder<?, ?>, TrackpointSeekingProcessor.BaseBuilder<?, ?>>> seekerChain = new ArrayList<Pair<TrackpointSeekerBase.Builder<?, ?>, TrackpointSeekingProcessor.BaseBuilder<?, ?>>>();

	private TrackpointSeekerChainBuilder(TrackProcessorBase.Builder<T> driver) {
		this.driver = driver;
	}

	public void addSeeker(TrackpointSeekerBase.Builder<?, ?> seeker) {
		addSeeker(seeker, new TrackpointSeekingProcessor.Builder());
	}

	/**
	 * Adds the seeker to be a listener of the {@link #getCurrentProcessor()}. The given receiver is
	 * registered with the given seeker so it may receive notifications when the trackpoint has been
	 * found. The given receiver now becomes the {@link #getCurrentProcessor()} for others to
	 * register.
	 * 
	 * @param seeker
	 */
	@SuppressWarnings("unchecked")
	public void addSeeker(TrackpointSeekerBase.Builder<?, ?> seeker,
			TrackpointSeekingProcessor.BaseBuilder<?, ?> receiverBuilder) {
		// this is the processor that will be providing trackpoints to the seeker
		TrackProcessorBase.Builder<?> providingProcessor = getCurrentProcessor();
		// registers to receive trackpoints from previous processor
		seeker.registerWithProcessor(providingProcessor);
		Pair<?, ?> seekerPair = Pair.of(seeker, receiverBuilder);
		// can't set receiver until receiver is built during #build
		seekerChain.add((Pair<Builder<?, ?>, BaseBuilder<?, ?>>) seekerPair);

	}

	/**
	 * Provides the most recently added processor for the seeker or the driver if none yet added.
	 * Consider using
	 * {@link #registerWithCurrentProcessor(com.aawhere.track.process.ProcessorListenerBase.Builder)}
	 * .
	 * 
	 * @return
	 */
	public TrackProcessorBase.Builder<?> getCurrentProcessor() {
		TrackProcessorBase.Builder<?> providingProcessor;
		if (seekerChain.isEmpty()) {
			// the driver is the first in the chain.
			// usually a Manual or Automatic processor
			providingProcessor = this.driver;
		} else {
			Pair<TrackpointSeekerBase.Builder<?, ?>, TrackpointSeekingProcessor.BaseBuilder<?, ?>> previousSeeker = ListUtilsExtended
					.lastOrNull(this.seekerChain);
			providingProcessor = previousSeeker.getRight();
		}
		return providingProcessor;
	}

	/**
	 * used in coordination with
	 * {@link #addSeeker(com.aawhere.track.process.TrackpointSeekerBase.Builder)} this will register
	 * any provided listener with the current process.
	 * 
	 * Ensure that you have done all you wish to do with the builder before passing since this will
	 * build for you so you don't forget.
	 * 
	 * 
	 * @param builder
	 *            the builder used to register with the {@link #getCurrentProcessor()}
	 * @return the listener built from the provided builder
	 */
	public <L extends ProcessorListenerBase> L
			registerWithCurrentProcessor(ProcessorListenerBase.Builder<L, ?> builder) {
		builder.registerWithProcessor(getCurrentProcessor());
		return builder.build();
	}

	/**
	 * Iterates through the chain of seekers and builds each of them finishing with building and
	 * return the driver. Each seeker and it's receiver are not available since you likely need
	 * reference to the listeners registered with each and not the seeker itself. should there be a
	 * case where you need them we could make them available, but you should really ask yourself
	 * "why" such is necessary. The driver you need of course since the trackpoints will be
	 * provided.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T build() {

		// build each of the pairs from the end of the queue first since all listeners must be built
		// before the seeker.
		for (int whichSeeker = seekerChain.size() - 1; whichSeeker >= 0; whichSeeker--) {
			Pair<TrackpointSeekerBase.Builder<?, ?>, TrackpointSeekingProcessor.BaseBuilder<?, ?>> pair = seekerChain
					.get(whichSeeker);
			pair.getLeft().setReceiver(pair.getRight().build()).build();

		}
		return (T) driver.build();
	}

	/**
	 * The only way to create a builder. A driver is always required and will be built by this
	 * during the {@link #build()} method.
	 * 
	 * @return
	 */
	public static <T extends TrackProcessorBase> TrackpointSeekerChainBuilder<T> create(
			TrackProcessorBase.Builder<T> driver) {

		return new TrackpointSeekerChainBuilder<T>(driver);
	}

}
