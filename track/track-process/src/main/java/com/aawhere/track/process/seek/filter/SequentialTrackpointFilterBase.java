/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * Base class for any Filter that determines if a trackpoint should be included based on criteria
 * from the previous trackpoint (i.e. sequential).
 * 
 * Useful for detecting duplicates or repeats.
 * 
 * @author roller
 * 
 */
abstract public class SequentialTrackpointFilterBase
		extends TrackpointSeekerBase
		implements ProcessorFilter, TrackpointListener {

	public static class Builder<BuilderResult extends SequentialTrackpointFilterBase, BuilderT extends Builder<BuilderResult, BuilderT>>
			extends TrackpointSeekerBase.Builder<BuilderResult, BuilderT> {

		/**
		 * @param building
		 */
		protected Builder(BuilderResult building) {
			super(building);
		}

	}

	protected SequentialTrackpointFilterBase() {
	}

	private Trackpoint previousTrackpoint;

	/**
	 * Extracts the current timestamp and asks implementing class if the trackpoint should be
	 * filtered.
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {

		if (previousTrackpoint == null || includeCurrentTrackpoint(previousTrackpoint, trackpoint)) {
			desiredTrackpointFound(trackpoint);
			previousTrackpoint = trackpoint;
		}

	}

	abstract protected Boolean includeCurrentTrackpoint(Trackpoint previousTrackpoint, Trackpoint currentTrackpoint);
}
