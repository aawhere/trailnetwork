/**
 *
 */
package com.aawhere.track.process.provider;

import com.aawhere.measure.Elevation;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;

/**
 * @author roller
 * 
 */
public class TrackSegmentElevationProvider
		implements TrackSegmentCalculatedQuantityProvider<Elevation> {

	private TrackSegment currentSegment;

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.process.calc.TrackSegmentCalculatedQuantityProvider#getSegmentQuantity()
	 */
	@Override
	public Elevation getQuantity() {
		return this.currentSegment.getAverageElevation();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track.TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		this.currentSegment = segment;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return this.currentSegment != null;
	}
}
