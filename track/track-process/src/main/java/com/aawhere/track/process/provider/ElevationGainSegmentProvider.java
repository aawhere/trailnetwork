/**
 * 
 */
package com.aawhere.track.process.provider;

import com.aawhere.measure.ElevationChange;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;

/**
 * @author Brian Chapman
 * 
 */
public class ElevationGainSegmentProvider
		extends ProcessorListenerBase
		implements TrackSegmentCalculatedQuantityProvider<ElevationChange> {

	/**
	 * Used to construct all instances of ElevationGainSegmentProvider.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<ElevationGainSegmentProvider, Builder> {

		public Builder() {
			super(new ElevationGainSegmentProvider());
		}

	}// end Builder

	private ElevationChange gain;

	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		gain = TrackUtil.elevationGain(segment);

	}

	@Override
	public ElevationChange getQuantity() {
		return gain;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return this.gain != null;
	}

}
