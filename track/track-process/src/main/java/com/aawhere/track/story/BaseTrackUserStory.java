/**
 *
 */
package com.aawhere.track.story;

import javax.annotation.processing.Processor;

import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessorBase;

/**
 * Provides the base methods for implementing {@link UserStory}.
 * 
 * Stories are important to have in the system to avoid writing this implementation in a
 * ServiceStandard which will subsequently use these stories to put the pieces together without the
 * requirement for storage,etc.
 * 
 * @see http://aawhere.jira.com/browse/TN-14
 * @see http://aawhere.jira.com/browse/TN-15
 * 
 * @author Brian Chapman
 * 
 */
public abstract class BaseTrackUserStory<ResultT>
		implements UserStory<ResultT> {

	private ResultT result;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.story.UserStory#getResult()
	 */
	@Override
	public ResultT getResult() {
		if (result == null) {
			throw new NullPointerException("result is null.  Call #hasResult to avoid this");
		}
		return result;
	}

	protected void setResult(ResultT result) {
		this.result = result;
	}

	@Override
	public Boolean hasResult() {
		return result != null;
	}

	@Override
	abstract public void process();

	/**
	 * Base Builder for {@link BaseTrackUserStory}.
	 * 
	 */
	public abstract static class Builder<StoryT extends BaseTrackUserStory<?>, BuilderT extends BaseTrackUserStory.Builder<StoryT, BuilderT>>
			extends AbstractObjectBuilder<StoryT, BuilderT> {

		/** Protected is dangerous since it isn't final. Use setter */
		@Deprecated
		protected Iterable<Trackpoint> track;
		private TrackProcessorBase.Builder<?> processorBuilder;

		public Builder(StoryT building) {
			super(building);
		}

		protected TrackProcessorBase.Builder<?> getProcessorBuilder() {
			return processorBuilder;
		}

		public BuilderT setProcessorBuilder(TrackProcessorBase.Builder<?> processorBuilder) {
			this.processorBuilder = processorBuilder;
			return dis;
		}

		@Override
		public void validate() {
			super.validate();
			Assertion.assertNotNull("ProcessorBuilder", processorBuilder);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public StoryT build() {
			StoryT built = super.build();
			configureProcessorBuilder(built);
			return built;
		}

		/**
		 * Setup the {@link Processor}'s Builder and configures the StoryT with the objects it needs
		 * to produce a result after the ProcessorBuilder has been built.
		 */
		abstract protected void configureProcessorBuilder(StoryT built);
	}

}
