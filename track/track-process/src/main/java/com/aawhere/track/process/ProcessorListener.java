/**
 *
 */
package com.aawhere.track.process;

/**
 * The base for all objects that are to be notified during processing.
 * 
 * <pre>
 * Although tempting to make this generic accepting TrackComponent, doing so
 * prohibits a Class from being both a TrackpointListener and
 * TrackSegmentListener.
 * 
 * The interface ProcessorListener cannot be implemented
 * more than once with different arguments: ProcessorListener<TrackSegment> and
 * ProcessorListener<Trackpoint>
 * </pre>
 * 
 * @see TrackpointListener
 * @see TrackSegmentListener
 * 
 * @author roller
 * 
 */
public abstract interface ProcessorListener {

	/**
	 * The main method used to process the {@link TrackComponent} provided in sequence from the
	 * Track.
	 * 
	 * Called when a new Trackpoint is discovered during {@link AutomaticTrackProcessor#inspect()}.
	 * 
	 * @param component
	 * @param processor
	 *            is the processor that is calling the handle method so it can be identified (useful
	 *            when a listener is registered with multiple processors)
	 */
	// public void handle(ComponentT component, TrackProcessor processor);
}
