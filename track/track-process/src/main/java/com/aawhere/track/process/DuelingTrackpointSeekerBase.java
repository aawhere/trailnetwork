/**
 *
 */
package com.aawhere.track.process;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Track;

/**
 * Supplies the common needs of a {@link TrackpointSeeker}.
 * 
 * @deprecated this exposes the track for others to do with it as they please instead of providing
 *             the iteration into another {@link TrackProcessor}.
 * 
 * @author roller
 * 
 */
@Deprecated
abstract public class DuelingTrackpointSeekerBase
		extends TrackpointSeekerBase {

	public static class Builder<BuilderResult extends DuelingTrackpointSeekerBase, BuilderT extends Builder<BuilderResult, BuilderT>>
			extends TrackpointSeekerBase.Builder<BuilderResult, BuilderT> {

		/**
		 * @param building
		 *            children are required to produce their own result
		 */
		protected Builder(BuilderResult building) {
			super(building);
		}

		public BuilderT setTargetTrack(Track targetTrack) {
			((DuelingTrackpointSeekerBase) building).targetTrack = targetTrack;
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("receiver", ((DuelingTrackpointSeekerBase) building).targetTrack);
		}

	}

	/**
	 * The recipient of the trackpoints being sought when they are discovered.
	 * 
	 */
	private Track targetTrack;

	/**
	 * @return the targetTrack
	 */
	public Track getTargetTrack() {
		return targetTrack;
	}

}
