/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import com.aawhere.measure.ElevationChange;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;

/**
 * @author Aaron Roller on Apr 20, 2011
 * 
 */
public class ElevationChangeBetweenFilter
		extends BetweenFilter<ElevationChange>
		implements TrackSegmentListener {

	/**
	 * Used to construct all instances of ElevationChangeBetweenFilter.
	 */
	public static class Builder
			extends BetweenFilter.Builder<ElevationChangeBetweenFilter, ElevationChange> {

		public Builder() {
			super(new ElevationChangeBetweenFilter());
		}

	}// end Builder

	/** Use {@link Builder} to construct ElevationChangeBetweenFilter */
	private ElevationChangeBetweenFilter() {
	}

	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		super.handle(segment.getElevationChange(), segment.getSecond());
	}
}
