/**
 * 
 */
package com.aawhere.track.process.seek.virtual;

import org.joda.time.DateTime;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.UnfinishedProcessorListener;

/**
 * A {@link VirtualTrackpointSeeker} that seeks for the {@link #nextTargetedTimestamp} and
 * interpolates a trackpoint matching the time if found in the current segment.
 * 
 * @author roller
 */
abstract public class TargetTimeVirtualTrackpointSeeker
		extends VirtualTrackpointSeeker
		implements TrackSegmentListener, UnfinishedProcessorListener {

	/**
	 * Used to construct all instances of TargetTimeVirtualTrackpointSeeker.
	 */
	public static class Builder<BuilderResultT extends TargetTimeVirtualTrackpointSeeker, BuilderT extends Builder<BuilderResultT, BuilderT>>
			extends VirtualTrackpointSeeker.Builder<BuilderResultT, BuilderT> {

		public Builder(BuilderResultT building) {
			super(building);
		}

	}// end Builder

	/** Use {@link Builder} to construct TargetTimeVirtualTrackpointSeeker */
	protected TargetTimeVirtualTrackpointSeeker() {
	}

	/**
	 * @return the finished
	 */
	protected boolean isFinished() {
		return finished;
	}

	/** The timestamp being sought after. */
	private DateTime nextTargetedTimestamp;
	private boolean finished = false;
	/**
	 * Used to delay processing of the last trackpoint until {@link #finished()}.
	 */
	private TrackSegment lastHandledSegment;

	/**
	 * @return the nextTargetedTimestamp
	 */
	protected DateTime getNextTargetedTimestamp() {
		return nextTargetedTimestamp;
	}

	/**
	 * @param nextTargetedTimestamp
	 *            the nextTargetedTimestamp to set
	 */
	protected void setNextTargetedTimestamp(DateTime nextTargetedTimestamp) {
		this.nextTargetedTimestamp = nextTargetedTimestamp;
	}

	/**
	 * Receives each segment looking to see if the time span encompasses the next targeted
	 * instant(s). If so it interpolates the targeted points and calls
	 * {@link #desiredTrackpointFound(com.aawhere.track.Trackpoint)}.
	 * 
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track.TrackSegment,
	 *      com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		// avoids nullpointer exeptions when no longer seeking
		if (!finished) {
			// look for containment. Interval contains is exclusive of end, but
			// we
			// need to check the end otherwise the last trackpoint won't be
			// included.
			while (segment.getInterval().contains(this.nextTargetedTimestamp)) {
				calculatePoint(segment);
				incrementTarget();
			}
			this.lastHandledSegment = segment;
		}
	}

	/**
	 * Assumes the {@link #nextTargetedTimestamp} is contained within
	 * {@link TrackSegment#getInterval()}, this will create a virtual point at the next targeted
	 * 
	 * @param segment
	 */
	protected void calculatePoint(TrackSegment segment) {
		final Trackpoint interpolated = TrackUtil.interpolate(segment, nextTargetedTimestamp);
		desiredTrackpointFound(interpolated);
	}

	/**
	 * Used by subclasses to assign a new value to the {@link #nextTargetedTimestamp}.
	 */
	protected abstract void incrementTarget();

	/**
	 * Used by subclasses to indicate there will be no more {@link #nextTargetedTimestamp}s.
	 */
	@Override
	public void finish() {
		// give a final test to make sure the last point is included if it is a match.
		if (this.lastHandledSegment != null
				&& this.lastHandledSegment.getSecond().getTimestamp().equals(nextTargetedTimestamp)) {
			calculatePoint(lastHandledSegment);
		}
		this.finished = true;
		setNextTargetedTimestamp(null);
	}
}
