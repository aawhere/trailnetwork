/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * For those times when Elevation is required for the processors this will remove trackpoints that
 * don't provide the optional measurement.
 * 
 * @author aroller
 * 
 */
public class MissingElevationTrackpointFilter
		extends TrackpointSeekerBase
		implements ProcessorFilter, TrackpointListener {

	/**
	 * Used to construct all instances of MissingElevationTrackpointFilter.
	 */
	public static class Builder
			extends TrackpointSeekerBase.Builder<MissingElevationTrackpointFilter, Builder> {

		public Builder() {
			super(new MissingElevationTrackpointFilter());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct MissingElevationTrackpointFilter */
	private MissingElevationTrackpointFilter() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		if (trackpoint.getElevation() != null) {
			super.desiredTrackpointFound(trackpoint);
		}

	}

}
