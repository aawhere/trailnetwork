/**
 *
 */
package com.aawhere.track.match;

import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.TrackpointSeekerBase;
import com.aawhere.track.process.calc.ObservedDurationCalculator;
import com.aawhere.track.process.calc.TrackElapsedDurationCalculator;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;

/**
 * A simple object that puts together a bunch of processor components to determine statistics
 * related to when two tracks were at the same place and the same time.
 * 
 * @author Aaron Roller
 */
public class TogetherCalculator {

	/**
	 * Used to construct all instances of TogetherCalculator.
	 */
	public static class Builder
			extends ObjectBuilder<TogetherCalculator> {

		private TrackProcessorBase.Builder<?> sourceProcessorBuilder;
		private TrackpointSeekerBase.Builder<? extends TrackpointSeekerBase, ? extends TrackpointSeekerBase.Builder<?, ?>> seekerBuilder;
		private Length maxDistanceToBeConsideredTogether;

		public Builder() {
			super(new TogetherCalculator());
		}

		public Builder setSourceProcessor(TrackProcessorBase.Builder<?> sourceProcessor) {
			this.sourceProcessorBuilder = sourceProcessor;
			return this;
		}

		public
				Builder
				setSeekerBuilder(
						TrackpointSeekerBase.Builder<? extends TrackpointSeekerBase, ? extends TrackpointSeekerBase.Builder<?, ?>> seekerBuilder) {
			this.seekerBuilder = seekerBuilder;
			return this;
		}

		public Builder setMaxDistanceToBeConsideredTogether(Length maxDistanceToBeConsideredTogether) {
			this.maxDistanceToBeConsideredTogether = maxDistanceToBeConsideredTogether;
			return this;
		}

		@Override
		public TogetherCalculator build() {

			TrackpointSeekingProcessor.Builder targetProcessorBuilder = new TrackpointSeekingProcessor.Builder();

			building.totalDurationCalculator = new TrackElapsedDurationCalculator.Builder()
					.registerWithProcessor(targetProcessorBuilder).build();
			DistanceApartMeasurementProvider distanceApartMeasurementProvider = new DistanceApartMeasurementProvider.Builder()
					.registerWithTargetProcessor(targetProcessorBuilder)
					.registerWithSourceProcessor(sourceProcessorBuilder).build();

			building.durationTogetherCalculator = new ObservedDurationCalculator.Builder()
					.registerWithProcessor(targetProcessorBuilder).build();
			// TODO: Check to see if this has any effect on the build. It appears that it is not
			// registered wtih a processor and thus will never receive a message to handle()
			// new
			// WeightedMeanQuantityCalculator.Builder<Length>().setProvider(distanceApartMeasurementProvider).build();

			building.metDetector = new TracksSimultaneousMetListener.Builder()
					.setMaxDistanceToBeConsideredTogether(maxDistanceToBeConsideredTogether)
					.registerWithSourceProcessor(sourceProcessorBuilder)
					.registerWithTargetProcessor(targetProcessorBuilder).build();

			TrackpointSeekingProcessor processor = targetProcessorBuilder.build();

			this.seekerBuilder.setReceiver(processor);
			return super.build();
		}

		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(maxDistanceToBeConsideredTogether);
		}
	}// end Builder

	/** Use {@link Builder} to construct TogetherCalculator */
	private TogetherCalculator() {
	}

	private TrackElapsedDurationCalculator totalDurationCalculator;
	private ObservedDurationCalculator durationTogetherCalculator;
	private TracksSimultaneousMetListener metDetector;

	public TrackElapsedDurationCalculator getTotalDurationCalculator() {
		return totalDurationCalculator;
	}

	public ObservedDurationCalculator getDurationTogetherCalculator() {
		return durationTogetherCalculator;
	}

	public TracksSimultaneousMetListener getMetDetector() {
		return metDetector;
	}

}
