/**
 * 
 */
package com.aawhere.track.process.seek.virtual;

import org.joda.time.Duration;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.seek.DurationTrackpointSeeker;

/**
 * Creates a track guaranteeing a trackpoint available at fixed duration. This means virtual
 * trackpoints may be created to satisfy the requirement. Use {@link DurationTrackpointSeeker} if a
 * minimum time separation is needed rather than fixed.
 * 
 * Multiple virtual trackpoints may need to be created to satisfy the fixed duration needs.
 * 
 * {@link TrackSegmentListener} is used since interpolation may be needed from
 * {@link TrackUtil#interpolate(TrackSegment, com.aawhere.measure.quantity.Ratio)} .
 * 
 * 
 * @author roller
 * 
 */
public class FixedDurationTrackpointSeeker
		extends TargetTimeVirtualTrackpointSeeker {

	public static class Builder
			extends TargetTimeVirtualTrackpointSeeker.Builder<FixedDurationTrackpointSeeker, Builder> {

		/**
		 * 
		 */
		public Builder() {
			super(new FixedDurationTrackpointSeeker());
		}

		public Builder setDurationBetweenTrackpoints(Duration duration) {
			building.durationBetweenTrackpoints = duration;
			return this;
		}
	}

	private Duration durationBetweenTrackpoints = new Duration(5000);
	private boolean initialized = false;

	/**
	 * 
	 */
	private FixedDurationTrackpointSeeker() {
		super();
	}

	@Override
	protected void incrementTarget() {
		setNextTargetedTimestamp(getNextTargetedTimestamp().plus(this.durationBetweenTrackpoints));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.seek.virtual.VirtualTrackpointSeeker#handle(com.aawhere.track.
	 * Trackpoint, com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {

		if (!initialized) {
			// this should ensure the first trackpoint will be kept.
			setNextTargetedTimestamp(trackpoint.getTimestamp());
			this.initialized = true;
		}
		super.handle(trackpoint, processor);
	}

}
