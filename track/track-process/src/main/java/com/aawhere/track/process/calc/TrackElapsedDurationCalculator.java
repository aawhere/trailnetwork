/**
 * 
 */
package com.aawhere.track.process.calc;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * Calculates the duration from the beginning to the current trackpoint inspected. This is the
 * Elapsed time of a track, not moving or recording duration. See {@link ObservedDurationCalculator}
 * if only those segments visited should count towards a total duration.
 * 
 * @author roller
 * 
 */
public class TrackElapsedDurationCalculator
		extends ProcessorListenerBase
		implements TrackpointListener {

	private DateTime startTime;
	private DateTime endTime;

	public static class Builder
			extends ProcessorListenerBase.Builder<TrackElapsedDurationCalculator, Builder> {

		/**
		 * @param building
		 */
		public Builder() {
			super(new TrackElapsedDurationCalculator());
		}

	}

	private TrackElapsedDurationCalculator() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.inspect.TrackpointListener#handle(com.aawhere.track .Trackpoint)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {

		if (this.startTime == null) {
			this.startTime = trackpoint.getTimestamp();
		}
		this.endTime = trackpoint.getTimestamp();

	}

	/**
	 * @return the startTime
	 */
	public DateTime getStartTime() {
		return startTime;
	}

	/**
	 * @return the duration
	 */
	public Duration getDuration() {
		return getInterval().toDuration();
	}

	/**
	 * @return the endTime
	 */
	public DateTime getEndTime() {
		return endTime;
	}

	public Interval getInterval() {

		return new Interval(getStartTime(), getEndTime());
	}

}
