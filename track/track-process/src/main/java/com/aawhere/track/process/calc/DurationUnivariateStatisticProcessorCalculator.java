/**
 * 
 */
package com.aawhere.track.process.calc;

import org.apache.commons.math.stat.descriptive.StorelessUnivariateStatistic;
import org.joda.time.Duration;

import com.aawhere.joda.time.calc.DurationUnivariateStatistic;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;

/**
 * Provides statistical calculations for Duration measurements.
 * 
 * @author Aaron Roller
 * 
 */
public class DurationUnivariateStatisticProcessorCalculator<S extends StorelessUnivariateStatistic>
		extends ProcessorListenerBase
		implements TrackSegmentListener {

	private DurationUnivariateStatistic<S> statistic;

	/**
	 * Used to construct all instances of DurationUnivariateStatisticProcessorCalculator.
	 */
	public static class Builder<S extends StorelessUnivariateStatistic>
			extends ProcessorListenerBase.Builder<DurationUnivariateStatisticProcessorCalculator<S>, Builder<S>> {

		public Builder() {
			super(new DurationUnivariateStatisticProcessorCalculator<S>());
		}

		public Builder<S> setStatistic(S statistic) {
			building.statistic = new DurationUnivariateStatistic.Builder<S>().setStatistic(statistic).build();
			return dis;
		}

	}// end Builder

	/** Use {@link Builder} to construct DurationUnivariateStatisticProcessorCalculator */
	private DurationUnivariateStatisticProcessorCalculator() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track.TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		statistic.increment(segment.getDuration());
	}

	/** Indicates if a calculation has occurred. */
	public Boolean hasDuration() {
		return statistic.hasResult();
	}

	/**
	 * 
	 * @return the calculated duration or throws an exception if hasDuration return false
	 */
	public Duration getDuration() {
		return statistic.getResult();
	}
}
