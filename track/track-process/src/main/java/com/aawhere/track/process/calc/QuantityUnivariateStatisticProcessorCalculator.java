/**
 * 
 */
package com.aawhere.track.process.calc;

import javax.measure.quantity.Quantity;

import org.apache.commons.math.stat.descriptive.StorelessUnivariateStatistic;
import org.apache.commons.math.stat.descriptive.UnivariateStatistic;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.QuantityUnivariateStatistic;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.ProcessorListenerQuantityProvider;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * Pretty much does any statistical calculation using the {@link UnivariateStatistic} library.
 * Specifically, the {@link QuantityUnivariateStatistic} is provided during building which will be
 * used during processing to calculate the {@link #getQuantity()}.
 * 
 * @author Aaron Roller
 * 
 */
public class QuantityUnivariateStatisticProcessorCalculator<Q extends Quantity<Q>, U extends StorelessUnivariateStatistic>
		extends ProcessorListenerBase
		implements TrackpointListener, ProcessorListenerQuantityProvider<Q> {

	/**
	 * Used to construct all instances of QuantityUnivariateStatisticProcessorCalculator.
	 */
	public static class Builder<Q extends Quantity<Q>, U extends StorelessUnivariateStatistic>
			extends ProcessorListenerBase.Builder<QuantityUnivariateStatisticProcessorCalculator<Q, U>, Builder<Q, U>> {

		public Builder() {
			super(new QuantityUnivariateStatisticProcessorCalculator<Q, U>());
		}

		public Builder<Q, U> setProvider(ProcessorListenerQuantityProvider<Q> provider) {
			building.provider = provider;
			return this;
		}

		public Builder<Q, U> setCalculator(QuantityUnivariateStatistic<Q, U> calculator) {
			building.calculator = calculator;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.ProcessorListenerBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("provider", building.provider);
			Assertion.exceptions().notNull("calculator", building.calculator);

		}

	}// end Builder

	/** provides the quantity to be calculated */
	private ProcessorListenerQuantityProvider<Q> provider;
	/** calculates the quanityties from the {@link #provider} */
	private QuantityUnivariateStatistic<Q, U> calculator;

	/** Use {@link Builder} to construct QuantityUnivariateStatisticProcessorCalculator */
	private QuantityUnivariateStatisticProcessorCalculator() {

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		if (provider.hasQuantity()) {
			calculator.increment(provider.getQuantity());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#getQuantity()
	 */
	@Override
	public Q getQuantity() {
		return calculator.getResult();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return calculator.hasResult();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.calculator.toString() + " of " + this.provider.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.calculator == null) ? 0 : this.calculator.hashCode());
		result = prime * result + ((this.provider == null) ? 0 : this.provider.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuantityUnivariateStatisticProcessorCalculator other = (QuantityUnivariateStatisticProcessorCalculator) obj;
		if (this.calculator == null) {
			if (other.calculator != null)
				return false;
		} else if (!this.calculator.equals(other.calculator))
			return false;
		if (this.provider == null) {
			if (other.provider != null)
				return false;
		} else if (!this.provider.equals(other.provider))
			return false;
		return true;
	}

}
