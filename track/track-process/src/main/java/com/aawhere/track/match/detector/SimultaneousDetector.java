/**
 *
 */
package com.aawhere.track.match.detector;

import org.joda.time.Interval;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.track.TrackSummary;

/**
 * 
 * Determines if the two tracks are Simultaneous
 * 
 * @author Brian Chapman
 * 
 */
public class SimultaneousDetector {

	public static final Double MIN_OVERLAP_TO_BE_CONSIDERED_TOGETHER = 0.8d;
	private TrackSummary sourceTs;
	private TrackSummary targetTs;

	public Boolean isSimultaneous() {
		Interval overlapInterval = sourceTs.getInterval().overlap(targetTs.getInterval());
		if (overlapInterval == null) {
			return false;
		}
		Interval sourceInterval = sourceTs.getInterval();
		Interval targetInterval = targetTs.getInterval();
		Interval longestInterval = sourceInterval.toDurationMillis() > targetInterval.toDurationMillis() ? sourceInterval
				: targetInterval;
		if ((((double) overlapInterval.toDurationMillis()) / ((double) longestInterval.toDurationMillis())) >= MIN_OVERLAP_TO_BE_CONSIDERED_TOGETHER) {
			return true;
		}
		return false;
	}

	/**
	 * Used to construct all instances of TogetherCalculator.
	 */
	public static class Builder
			extends ObjectBuilder<SimultaneousDetector> {

		public Builder() {
			super(new SimultaneousDetector());
		}

		public Builder setSourceTrackSummary(TrackSummary sourceTs) {
			building.sourceTs = sourceTs;
			return this;
		}

		public Builder setTargetTrackSummary(TrackSummary targetTs) {
			building.targetTs = targetTs;
			return this;
		}

		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.sourceTs);
			Assertion.assertNotNull(building.targetTs);
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}
}
