/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import org.joda.time.DateTime;

import com.aawhere.joda.time.InvalidIntervalRuntimeException;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * Avoids {@link InvalidIntervalRuntimeException} by filtering out trackpoints that have equal
 * {@link Trackpoint#getTimestamp()}s of the second is before the first.
 * 
 * @author aroller
 * 
 */
public class NonProgressingTimestampTrackpointFilter
		extends TrackpointSeekerBase
		implements ProcessorFilter, TrackpointListener {

	/**
	 * Used to construct all instances of RepeatTimestampTrackpointFilter.
	 */
	public static class Builder
			extends TrackpointSeekerBase.Builder<NonProgressingTimestampTrackpointFilter, Builder> {

		public Builder() {
			super(new NonProgressingTimestampTrackpointFilter());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** First trackpoint is a guarantee. */
	private DateTime previousTimestamp = new DateTime(Long.MIN_VALUE);

	/** Use {@link Builder} to construct RepeatTimestampTrackpointFilter */
	private NonProgressingTimestampTrackpointFilter() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {

		DateTime timestamp = trackpoint.getTimestamp();
		if (timestamp != null && timestamp.isAfter(previousTimestamp)) {
			super.desiredTrackpointFound(trackpoint);
			// only set previous if it is valid. anything else is a problem so just forget about it.
			this.previousTimestamp = timestamp;
		}
	}

}
