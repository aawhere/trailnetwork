/**
 * 
 */
package com.aawhere.track.process.calc;

import org.joda.time.Duration;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;

/**
 * Cumulates the duration of segments that are provided. This does not calculate the absolute
 * duration meaning only those segments provided will be considered, not the time in between
 * segments. See {@link TrackElapsedDurationCalculator} for elapsed time calculations.
 * 
 * @author roller
 * 
 */
public class ObservedDurationCalculator
		extends ProcessorListenerBase
		implements TrackSegmentListener {

	/**
	 * @return
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<ObservedDurationCalculator, Builder> {

		/**
		 * @param building
		 */
		public Builder() {
			super(new ObservedDurationCalculator());
		}

	}

	private Duration totalDuration = new Duration(0);

	/**
	 * 
	 */
	private ObservedDurationCalculator() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track .TrackSegment)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		totalDuration = totalDuration.plus(segment.getDuration());
	}

	/**
	 * 
	 */
	public Duration getTotalDuration() {
		return this.totalDuration;
	}

}
