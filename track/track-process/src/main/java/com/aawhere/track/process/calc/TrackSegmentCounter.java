/**
 *
 */
package com.aawhere.track.process.calc;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;

/**
 * Simply counts the number of {@link TrackSegment}s provided in the {@link #handle(TrackSegment)}
 * method.
 * 
 * @author roller
 * 
 */
public class TrackSegmentCounter
		extends ProcessorListenerBase
		implements TrackSegmentListener {

	/**
	 * Used to construct all instances of TrackSegmentCounter.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackSegmentCounter, Builder> {

		public Builder() {
			super(new TrackSegmentCounter());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrackSegmentCounter */
	private TrackSegmentCounter() {
	}

	private Integer count = 0;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track .TrackSegment)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		count++;
	}

	/**
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}

}
