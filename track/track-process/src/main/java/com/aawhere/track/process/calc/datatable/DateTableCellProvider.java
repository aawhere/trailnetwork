/**
 *
 */
package com.aawhere.track.process.calc.datatable;

import org.joda.time.DateTime;

import com.aawhere.lang.Assertion;
import com.aawhere.personalize.DateTimePersonalized;
import com.aawhere.personalize.DateTimePersonalizer;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

import com.google.visualization.datasource.datatable.TableCell;
import com.google.visualization.datasource.datatable.value.DateTimeValue;
import com.google.visualization.datasource.datatable.value.ValueType;
import com.ibm.icu.util.GregorianCalendar;
import com.ibm.icu.util.TimeZone;

/**
 * Provides {@link TableCell} for {@link Date}.
 * 
 * @author Brian Chapman
 * 
 */
public class DateTableCellProvider
		extends TableCellProvider
		implements TrackpointListener {

	/**
	 * Used to construct all instances.
	 */
	public static class Builder
			extends TableCellProvider.Builder<Builder, DateTableCellProvider> {

		public Builder() {
			super(new DateTableCellProvider());
			columnType(ValueType.DATETIME);
			columnId("date");
		}

		public Builder setPersonalizer(DateTimePersonalizer plizer) {
			building.plizer = plizer;
			return this;
		}

		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.plizer);
		}

	}// end Builder

	private DateTimePersonalizer plizer;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track.TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint tp, TrackProcessor processor) {
		setCell(buildCell(tp.getTimestamp()));
	}

	protected TableCell buildCell(DateTime d) {
		plizer.personalize(d);
		DateTimePersonalized plizedDateTime = plizer.getResult();
		com.ibm.icu.util.GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(d.toDate());
		// Satisfy DateValue's requirement that the calendar be in GMT timezone.
		calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
		TableCell cell = new TableCell(new DateTimeValue(calendar));
		cell.setFormattedValue(plizedDateTime.getPersonalized().getMediumTime());
		return cell;
	}

}
