/**
 *
 */
package com.aawhere.track.match;

import javax.measure.quantity.Length;

import com.aawhere.track.Track;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.DuelingTrackSegmentListenerBase;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;
import com.aawhere.track.process.TrackSegmentListener;

/**
 * Given the {@link Trackpoint} of another {@link Track}, this provides the distance between the two
 * {@link Trackpoint}s.
 * 
 * This {@link TrackSegmentListener} must be registered with the target processor that will be
 * providing synchronized track segments matching the changing source {@link TrackSegment}. The
 * source must be changed by an external source...likely by another {@link TrackSegmentListener},
 * but must provide the source segment by calling the mutator method
 * {@link #setSourceSegment(TrackSegment)}.
 * 
 * @author roller
 * 
 */
public class DistanceApartMeasurementProvider
		extends DuelingTrackSegmentListenerBase
		implements TrackSegmentCalculatedQuantityProvider<Length> {

	/**
	 * Used to construct all instances of DistanceApartMeasurementProvider.
	 */
	public static class Builder
			extends DuelingTrackSegmentListenerBase.Builder<DistanceApartMeasurementProvider, Builder> {

		public Builder() {
			super(new DistanceApartMeasurementProvider());
		}

	}// end Builder

	/** Use {@link Builder} to construct DistanceApartMeasurementProvider */
	private DistanceApartMeasurementProvider() {
	}

	/*
	 * (non-Javadoc)
	 * @seecom.aawhere.track.process.calc.TrackpointCalculatedQuantityProvider#
	 * getTrackpointQuantity()
	 */
	@Override
	public Length getQuantity() {
		return TrackUtil.averageDistanceApart(getSourceSegment(), getTargetSegment());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {

		return getSourceSegment() != null && getTargetSegment() != null;
	}

	/**
	 * Used by a client that wants to ensure the distances being calculated are matching times that
	 * match exactly.
	 * 
	 * @see SimultaneousTrackpointSeeker
	 * @see TrackUtil#timesAreSynched(TrackSegment, TrackSegment)
	 * 
	 * @return
	 */
	public Boolean isSegmentsSynched() {
		return TrackUtil.timesAreSynched(getSourceSegment(), getTargetSegment());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.track.process.DuelingTrackSegmentListenerBase#handleTargetSegment(com.aawhere
	 * .track.TrackSegment)
	 */
	@Override
	protected void handleTargetSegment(TrackSegment segment) {
		// not much to do since calculations are done on demand
	}

}
