/**
 *
 */
package com.aawhere.track.process.calc;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * Simply counts the number of {@link Trackpoint}s provided in the {@link #handle(Trackpoint)}
 * method.
 * 
 * @author roller
 * 
 */
public class TrackpointCounter
		extends ProcessorListenerBase
		implements TrackpointListener {

	/** Use {@link Builder} to construct TrackpointCounter */
	private TrackpointCounter() {
	}

	private Integer count = 0;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track .Trackpoint)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		count++;

	}

	/**
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * Used to construct all instances of TrackpointCounter.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackpointCounter, Builder> {

		public Builder() {
			super(new TrackpointCounter());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}
}
