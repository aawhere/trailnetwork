package com.aawhere.track.process.calc;

import com.aawhere.app.DeviceCapabilities;
import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.lang.enums.EnumUtil;
import com.google.common.base.Supplier;

public class TrackSignalQualityProcessorUtil {

	/**
	 * Extracts all it can from the factory to determine the best representation of a track Signal
	 * Quality.
	 * 
	 * @param factory
	 * @return
	 */
	public static Supplier<SignalQuality> signalQualitySupplier(ProcessingCalculatorFactory factory) {

		final TrackSignalQualityCalculator signalCalculator = factory.signalQualityCalculator();
		final TrackHeadingConsitencyCalculator headingConsitencyCalculator = factory.headingConsitencyCalculator();

		return new Supplier<DeviceCapabilities.SignalQuality>() {

			@Override
			public SignalQuality get() {
				return EnumUtil.min(signalCalculator.get(), headingConsitencyCalculator.summary());
			}
		};
	}
}
