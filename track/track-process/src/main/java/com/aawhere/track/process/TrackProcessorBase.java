/**
 *
 */
package com.aawhere.track.process;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.functors.InstanceofPredicate;

import com.aawhere.collections.apache.ListOrderedSetExtended;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;
import com.aawhere.track.SimpleTrackSegment;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.Trackpoint;

/**
 * Used to interact with a series of {@link Trackpoint}s by delegating work to be done by
 * {@link TrackpointListener}s.
 * 
 * Listeners should do small items of work and depend on other listeners to provide the results of
 * their specialty.
 * 
 * By using the receiver, iterations can be more efficient since processing is de-coupled and
 * independent.
 * 
 * @author roller
 * 
 */
abstract public class TrackProcessorBase
		implements TrackProcessor {

	public abstract static class Builder<BuilderResult extends TrackProcessorBase>
			extends ObjectBuilder<BuilderResult> {

		private ArrayList<ProcessorListener> preRegistered = new ArrayList<ProcessorListener>();
		private ListOrderedSetExtended.Builder<ProcessorListener> processorListeners = new ListOrderedSetExtended.Builder<>();

		/**
				 *
				 */
		protected Builder(BuilderResult result) {
			super(result);

		}

		/**
		 * Used during building to indicate the intention to register once the Listener is built.
		 * 
		 * This action will ensure the Listener is actually built, but will allow for it to be
		 * registered in the order it must be built to respect the dependency chain.
		 * 
		 * This is not a mandatory step, but using it will help keep integrity.
		 * 
		 * @param listener
		 * @return
		 */
		Builder<BuilderResult> preRegister(ProcessorListener listener) {
			preRegistered.add(listener);
			return this;
		}

		/**
		 * registers each listener by class so it may be retrieved by users of the processor.
		 * 
		 * This means only one listener per class is allowed for now. This is limiting and may need
		 * to be changed in the future, but for now we have not seen the need for multiple
		 * configurations of listeners running the same processor.
		 * 
		 * Forcing no duplicate registration will ensure the recipient is not holding onto a
		 * listener that will never get processed since we will only ever process one of each.
		 * 
		 * Duplicate listeners will not be registered and the previously registered will be returned
		 * so the client may receive the current.
		 * 
		 * @param listener
		 * @return a {@link ProcessorListener} if one of the same has already been registered.
		 * @throws IllegalArgumentException
		 *             when a class not yet handled is provided
		 * 
		 */
		ProcessorListener register(ProcessorListener listener) {

			if (building == null) {
				throw new UnsupportedOperationException(
						"trying to register with a processor that has already been built");
			}
			ProcessorListener previous = this.processorListeners.add(listener);
			if (previous != null) {

			}
			this.preRegistered.remove(listener);
			return previous;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {

			super.validate();
			// all preregistrations must be completed with a registration
			Assertion
					.exceptions()
					.assertEmpty(	"unbuilt pre-registrations remain. build them.  If using a seekerChainBuilder then you may have registered with processor directly instead of allowing the seeker",
									preRegistered);
		}

		/**
		 * A string that identifies the track processor available before it is built, but it won't
		 * change upon building.
		 * 
		 * @see TrackProcessorBase#getKey()
		 * @return
		 */
		public String getTrackProcessorKey() {
			return building.getKey();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public BuilderResult build() {
			((TrackProcessorBase) building).processorListeners = this.processorListeners.build().asList();
			return super.build();
		}
	}

	private List<ProcessorListener> processorListeners;

	private String key;

	private boolean finished = false;
	/** Used to create track segments. */
	private Trackpoint previousTrackpoint;

	protected TrackProcessorBase() {

	}

	/**
	 * alerts all TrackpointListners that a new Trackpoint was found.
	 * 
	 * @param trackpoint
	 */
	protected void notifyTrackpointListeners(Trackpoint trackpoint) {
		assertNotFinished();

		TrackSegment segment = null;
		if (this.previousTrackpoint != null) {
			segment = new SimpleTrackSegment(this.previousTrackpoint, trackpoint);
		}
		for (ProcessorListener listener : processorListeners) {

			try {
				if (listener instanceof TrackpointListener) {
					((TrackpointListener) listener).handle(trackpoint, this);
				}
				// not else because it may implement both
				if (previousTrackpoint != null && listener instanceof TrackSegmentListener) {
					((TrackSegmentListener) listener).handle(segment, this);
				}
			} catch (RuntimeException e) {
				StringBuilder message = new StringBuilder();
				if (trackpoint != null) {
					message.append(trackpoint.getTimestamp());
					message.append(" caused an unhandled exception: ");
					message.append(e.getMessage());
					message.append(Arrays.toString(e.getStackTrace()));
				}
				LoggerFactory.getLogger(getClass()).severe(message.toString());
				throw (e);
			}
		}
		this.previousTrackpoint = trackpoint;
	}

	/** Just to make sure when the child says we are finished we really are. */
	private void assertNotFinished() {
		if (finished) {
			throw new IllegalStateException("attempt to call notify when already finished.");
		}
	}

	/**
	 * Indicates processing is complete and there will be no more trackpoints to handle.
	 * 
	 */
	protected void finish() {
		for (ProcessorListener listener : processorListeners) {
			if (listener instanceof UnfinishedProcessorListener) {
				((UnfinishedProcessorListener) listener).finish();
			}
		}

		finished = true;
	}

	/**
	 * Indicates that all listeners implementing {@link ResettingProcessorListener} should reset to
	 * their initial state.
	 */
	protected void reset() {
		for (ProcessorListener listener : processorListeners) {
			if (listener instanceof ResettingProcessorListener) {
				((ResettingProcessorListener) listener).reset();
			}
		}
		this.previousTrackpoint = null;
		finished = false;
	}

	/**
	 * A string that uniquely identifies this {@link TrackProcessor} from others. Useful in {@link
	 * ProcessorListener#}
	 * 
	 * @return
	 */
	@Override
	public String getKey() {
		if (this.key == null) {
			this.key = super.toString();
		}
		return this.key;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackProcessor#getListener(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends ProcessorListener> T getListener(Class<T> listenerClass) {
		Predicate predicate = new InstanceofPredicate(listenerClass);

		T targetListener = (T) CollectionUtils.find(this.processorListeners, predicate);
		return targetListener;
	}

	/**
	 * Indicates if {@link #finish()} has already been called.
	 * 
	 * @return the finished
	 */
	public boolean isFinished() {
		return this.finished;
	}
}
