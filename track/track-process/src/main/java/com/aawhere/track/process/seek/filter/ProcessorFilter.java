/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.seek.virtual.VirtualTrackpointSeeker;

/**
 * Used to remove track elements from the processing due to any reason.
 * 
 * Simply an extension of seeker, filters remove trackpoints from the list for their specific
 * reason.
 * 
 * If a trackpoint is to be kept, but modified, then consider looking at
 * {@link VirtualTrackpointSeeker}s to provide a new trackpoint with the modified values.
 * 
 * TODO: Provide a way for a listener to be notified when a trackopint is filtered.
 * 
 * @author Aaron Roller on Apr 19, 2011
 * 
 */
public interface ProcessorFilter
		extends TrackpointSeeker {

}
