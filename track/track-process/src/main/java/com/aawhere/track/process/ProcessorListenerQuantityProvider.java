/**
 * 
 */
package com.aawhere.track.process;

import javax.measure.quantity.Quantity;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessorBase.Builder;

/**
 * Providers give access to {@link Trackpoint} and {@link TrackSegment} {@link Quantity} values.
 * They may be as simple as passing a existing value or executed complex algorithms that result in a
 * single value.
 * 
 * 
 * @author roller
 * 
 */
public interface ProcessorListenerQuantityProvider<QuantityT extends Quantity<?>> {

	/**
	 * provides the quantity or throws a {@link NullPointerException} if it is null.
	 * 
	 * @return
	 */
	public QuantityT getQuantity();

	/**
	 * It is very common that a quantity may not be available at a particular point in the
	 * processing (lost signnal, etc). This indicates the same as a null.
	 * 
	 * @return
	 */
	public Boolean hasQuantity();

	/**
	 * Override and provide a unique check for the quantity provider to avoid being minimized
	 * because it is of the same class.
	 * 
	 * @see Builder#register(ProcessorListener)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj);

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode();
}
