/**
 *
 */
package com.aawhere.track.process.calc;

import java.util.ArrayList;

import com.aawhere.track.SimpleTrack;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.util.ListUtilsExtended;

/**
 * Simply creates a {@link Track} from the {@link Trackpoint}s provided via the
 * {@link #handle(Trackpoint, TrackProcessor)} method.
 * 
 * @author roller
 * 
 */
public class TrackCreatorTrackpointCalculator
		extends ProcessorListenerBase
		implements TrackpointListener {
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackCreatorTrackpointCalculator, Builder> {

		public Builder() {
			super(new TrackCreatorTrackpointCalculator());
		}
	}

	private ArrayList<Trackpoint> trackpoints = new ArrayList<Trackpoint>();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track .Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		trackpoints.add(trackpoint);
	}

	/**
	 * Creates a new {@link Track} from the growing number of {@link Trackpoint} s added to
	 * {@link #trackpoints} during the {@link #handle(Trackpoint, TrackProcessor)}. That means don't
	 * call this method a lot without thinking about the consequences of building a new
	 * {@link Object} with every call.
	 * 
	 * @return
	 */
	public Track getTrack() {
		return new SimpleTrack.Builder(trackpoints).build();
	}

	/**
	 * Clears the track similar to {@link #reset()}, however, it keeps the last trackpoint added and
	 * makes it the first trackpoint of the new track.
	 * 
	 */
	public void resetKeepLastPoint() {
		Trackpoint last = ListUtilsExtended.lastOrNull(this.trackpoints);
		reset();
		this.trackpoints.add(last);
	}

	/**
	 * Clears out the list of {@link #trackpoints} allowing for separate tracks to be created.
	 * 
	 */
	public void reset() {
		this.trackpoints.clear();
	}

	/**
	 * 
	 */
	public static Builder create() {
		return new Builder();
	}
}
