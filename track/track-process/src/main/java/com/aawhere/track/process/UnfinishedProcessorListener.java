/**
 * 
 */
package com.aawhere.track.process;

/**
 * Used by {@link ProcessorListener}s that need to be told to finish their work once processing is
 * complete.
 * 
 * see {@link TrackProcessorBase#finish()}
 * 
 * @author Aaron Roller
 * 
 */
public interface UnfinishedProcessorListener {

	/** Completes whatever is needed by the implementor */
	void finish();
}
