/**
 *
 */
package com.aawhere.track.match;

import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

/**
 * A {@link SimultaneousTrackpointSeeker} that takes it one step further and makes sure the distance
 * is within {@link #maxDistanceToBeConsideredTogether}.
 * 
 * 
 * @author roller
 * 
 */
public class TogetherTrackpointSeeker
		extends SimultaneousTrackpointSeeker {

	static public class Builder
			extends ObjectBuilder<TogetherTrackpointSeeker> {

		/**
		 * @param building
		 */
		protected Builder() {
			super(new TogetherTrackpointSeeker());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull("maxDistanceToBeConsideredTogether", building.maxDistanceToBeConsideredTogether);
			super.validate();
		}
	}

	/**
	 *
	 */
	protected TogetherTrackpointSeeker() {

	}

	private Length maxDistanceToBeConsideredTogether;

	/**
	 * Finds only those trackpoints that close together at the same time.
	 * 
	 * @see #maxDistanceToBeConsideredTogether
	 * 
	 * @see com.aawhere.track.process.TrackpointSeekerBase#desiredTrackpointFound(com.aawhere.track.Trackpoint)
	 */
	@Override
	protected void desiredTrackpointFound(Trackpoint trackpoint) {
		// these trackpoints are synched by time...how close are they?
		Trackpoint target = getCurrentTargetTrackpoint();
		Length distanceBetween = TrackUtil.distanceBetween(trackpoint, target);
		if (new QuantityMath<Length>(distanceBetween).lessThanEqualTo(maxDistanceToBeConsideredTogether)) {

			super.desiredTrackpointFound(trackpoint);
		}
	}

}
