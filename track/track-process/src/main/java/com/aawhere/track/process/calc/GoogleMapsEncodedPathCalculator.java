package com.aawhere.track.process.calc;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * TODO: split this into a SegmentProvider that encodes each segment, and a Calculator that
 * concatnates the result of the Provider
 * 
 * Takes trackpoints and converts them into a Google Maps Encoded Path
 * 
 * @link http ://code.google.com/apis/maps/documentation/utilities/polylinealgorithm .html
 * 
 * @author Brian Chapman
 * 
 */
public final class GoogleMapsEncodedPathCalculator
		extends ProcessorListenerBase
		implements TrackpointListener {

	/**
	 * @return
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<GoogleMapsEncodedPathCalculator, Builder> {

		/**
		 * @param building
		 */
		public Builder() {
			super(new GoogleMapsEncodedPathCalculator());
		}

	}

	private GoogleMapsEncodedPathCalculator() {
	};

	// First trackpoint received
	private GeoCoordinate previous;
	private String encodedPath = "";

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track .TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint point, TrackProcessor processor) {
		handle(point.getLocation());
	}

	public void handle(GeoCoordinate location) {
		String encodedSegment;
		if (previous == null) {
			encodedSegment = encodeFirstPoint(location);
		} else {
			encodedSegment = encodeSegment(location);
		}
		encodedPath += encodedSegment;
		previous = location;
	}

	/**
	 * Returns a Google Maps V3 {@link EncodedPath} from the growing number of {@link Trackpoint}s
	 * added via {@link #handle(Trackpoint, TrackProcessor)} .
	 * 
	 * @return
	 */
	public String getEncodedPath() {
		return encodedPath;
	}

	/**
	 * @param location
	 * @return
	 */
	private String encodeFirstPoint(final GeoCoordinate location) {
		Integer translatedLat = translateCoordinate(location.getLatitude().getInDecimalDegrees());
		Integer translatedLon = translateCoordinate(location.getLongitude().getInDecimalDegrees());
		String encodedSegment = encode(translatedLat, translatedLon);
		return encodedSegment;
	}

	/*
	 * Encodes a track point into a {@link String} using the polyline algorithm {@link
	 * http://code.google.com/apis/maps/documentation/utilities/polylinealgorithm .html}
	 */
	private String encodeSegment(GeoCoordinate point) {
		Integer changeInLatitude = changeInLatitude(previous, point);
		Integer changeInLongitude = changeInLongitude(previous, point);
		String encodedSegment = encode(changeInLatitude, changeInLongitude);
		return encodedSegment;
	}

	/*
	 * Translates a {@see Double} angle (representing a Latitude or Longitude in degrees) into an
	 * integer by multiplying by 1e5 and rounding.
	 */
	private Integer translateCoordinate(Double coordinate) {
		return (int) Math.round(coordinate * 100000);
	}

	/*
	 * Calculates the change in Latitude by first converting the Latitude into an {@see Ingeger}
	 * using {@see #translateAnglularDistance} then calculating the difference.
	 */
	private Integer changeInLatitude(GeoCoordinate firstPoint, GeoCoordinate secondPoint) {
		Double first = firstPoint.getLatitude().getInDecimalDegrees();
		Double second = secondPoint.getLatitude().getInDecimalDegrees();

		return changeBetweenCoordinates(first, second);
	}

	private Integer changeInLongitude(GeoCoordinate firstPoint, GeoCoordinate secondPoint) {
		Double first = firstPoint.getLongitude().getInDecimalDegrees();
		Double second = secondPoint.getLongitude().getInDecimalDegrees();

		return changeBetweenCoordinates(first, second);
	}

	/*
	 * Takes a decimal representation of an angle in degrees and returns the difference between them
	 * in translated degrees;
	 */
	private Integer changeBetweenCoordinates(Double first, Double second) {
		Integer translatedFirst = translateCoordinate(first);
		Integer translatedSecond = translateCoordinate(second);

		Integer change = translatedSecond - translatedFirst;
		return change;
	}

	private String encode(Integer changeInLatitude, Integer changeInLongitude) {
		String encodedLat = encodeCoordinate(changeInLatitude);
		String encodedLon = encodeCoordinate(changeInLongitude);
		return encodedLat + encodedLon;
	}

	private String encodeCoordinate(Integer coordinate) {
		return encodeSignedNumber(coordinate);
	}

	/*
	 * Taken from Mark McClures Javascript PolylineEncoder rewriten by Mark Rambow
	 * @author Mark Rambow
	 * @e-mail markrambow[at]gmail[dot]com
	 */
	private static String encodeSignedNumber(int num) {
		int sgn_num = num << 1;
		if (num < 0) {
			sgn_num = ~(sgn_num);
		}
		return (encodeNumber(sgn_num));
	}

	/*
	 * Taken from Mark McClures Javascript PolylineEncoder rewriten by Mark Rambow
	 * @author Mark Rambow
	 * @e-mail markrambow[at]gmail[dot]com
	 */
	private static String encodeNumber(int num) {
		StringBuffer encodeString = new StringBuffer();

		while (num >= 0x20) {
			int nextValue = (0x20 | (num & 0x1f)) + 63;
			encodeString.append((char) (nextValue));
			num >>= 5;
		}

		num += 63;
		encodeString.append((char) (num));

		return encodeString.toString();
	}
}
