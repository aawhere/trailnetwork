package com.aawhere.track.process;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.Trackpoint;
import com.google.common.base.Supplier;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

/**
 * A stanard {@link ProcessorListenerBase} that will count the results from the given supplier when
 * invoked by the implementing {@link TrackSegmentListener} or {@link TrackpointListener}.
 * 
 * @author aroller
 * 
 */
public class SupplierMultisetProcessorListener<E>
		extends ProcessorListenerBase
		implements Supplier<Multiset<E>> {
	private Supplier<E> supplier;
	private Multiset<E> multiset;

	/**
	 * Used to construct all instances of SupplierMultisetProcessorListener.
	 */
	public static class Builder<E>
			extends ProcessorListenerBase.Builder<SupplierMultisetProcessorListener<E>, Builder<E>> {

		private Builder(SupplierMultisetProcessorListener<E> listener) {
			super(listener);
		}

		public Builder<E> supplier(Supplier<E> supplier) {
			building.supplier = supplier;
			return this;
		}

		@Override
		public SupplierMultisetProcessorListener<E> build() {
			SupplierMultisetProcessorListener<E> built = super.build();
			if (built.multiset == null) {
				built.multiset = HashMultiset.create();
			}
			return built;
		}

	}// end Builder

	public static <E> Builder<E> createForTrackpoints() {
		return new Builder<E>(new SupplierMultisetProcessorListener.ForTrackpoints<E>());
	}

	public static <E> Builder<E> createForSegments() {
		return new Builder<E>(new SupplierMultisetProcessorListener.ForSegments<E>());
	}

	/** Use {@link Builder} to construct SupplierMultisetProcessorListener */
	private SupplierMultisetProcessorListener() {
	}

	protected void handle() {
		multiset.add(supplier.get());
	}

	private static class ForTrackpoints<E>
			extends SupplierMultisetProcessorListener<E>
			implements TrackpointListener {
		@Override
		public void handle(Trackpoint trackpoint, TrackProcessor processor) {
			handle();
		}
	}

	private static class ForSegments<E>
			extends SupplierMultisetProcessorListener<E>
			implements TrackSegmentListener {

		@Override
		public void handle(TrackSegment segment, TrackProcessor processor) {
			handle();
		}
	}

	@Override
	public Multiset<E> get() {
		// left mutable for efficient?
		return multiset;
	}

}
