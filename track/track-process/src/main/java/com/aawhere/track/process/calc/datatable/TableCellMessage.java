/**
 *
 */
package com.aawhere.track.process.calc.datatable;

import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

import com.google.visualization.datasource.datatable.ColumnDescription;

/**
 * Messages for TrackTableCellProviders.
 * 
 * @author Brian Chapman
 * 
 */
public enum TableCellMessage implements Message {

	/** @see ColumnDescription#setLabel */
	DATE_LABEL("Time"),
	/** @see ColumnDescription#setLabel */
	DISTANCE_LABEL("Distance"),
	/** @see ColumnDescription#setLabel */
	ELEVATION_LABEL("Elevation"),
	/** @see ColumnDescription#setLabel */
	ELEVATION_GAIN_LABEL("Gain"),
	/** @see ColumnDescription#setLabel */
	ELEVATION_LOSS_LABEL("Loss"),
	/** @see ColumnDescription#setLabel */
	GEO_COORDINATE_LABEL("Coordinate"),
	/** @see ColumnDescription#setLabel */
	VELOCITY_LABEL("Speed");

	private ParamKey[] parameterKeys;
	private String message;

	private TableCellMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

}
