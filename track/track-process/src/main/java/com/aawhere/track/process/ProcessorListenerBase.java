/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Apr 12, 2011
main : com.aawhere.track.process.ProcessorListenerBase.java
 */
package com.aawhere.track.process;

import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * A {@link ProcessorListener} implementation providing the common features for the Listener and
 * it's {@link ObjectBuilder}.
 * 
 * @author roller
 * 
 */
abstract public class ProcessorListenerBase
		implements ProcessorListener {

	/**
	 * Used to construct all instances of ProcessorListenerBase.
	 */
	abstract public static class Builder<BuilderResult extends ProcessorListenerBase, BuilderT extends Builder<BuilderResult, BuilderT>>
			extends AbstractObjectBuilder<BuilderResult, BuilderT> {

		private TrackProcessorBase.Builder<? extends TrackProcessorBase> processorBuilder;

		public Builder(BuilderResult listener) {
			super(listener);

		}

		/**
		 * A requirement to be registered with a processor before the object can be built.
		 * 
		 * This is a necessary evil to make sure each listener is registered with it's processor in
		 * the correct order. We do this by registering during building rather than after making
		 * sure that listeners that depend upon other listeners to be notified first can rely that
		 * they will have been notified.
		 * 
		 * So you do the registration, but then you build this and pass it to another Listener
		 * during the building (which will subsequently get registered after this one).
		 * 
		 * @param processorBuilder
		 * @return
		 */

		public BuilderT
				registerWithProcessor(TrackProcessorBase.Builder<? extends TrackProcessorBase> processorBuilder) {

			this.processorBuilder = processorBuilder;
			this.processorBuilder.preRegister(building);
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("registerWithProcessor", processorBuilder);
		}

		@Override
		public BuilderResult build() {
			BuilderResult built = super.build();

			// registering during building ensures that other depending processors get registered
			// first

			@SuppressWarnings("unchecked")
			BuilderResult previousRegistration = (BuilderResult) processorBuilder.register(built);

			if (previousRegistration != null) {
				built = previousRegistration;
			}
			return built;
		}
	}// end Builder

	/** Use {@link Builder} to construct ProcessorListenerBase */
	protected ProcessorListenerBase() {

	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		return getClass().hashCode();
	}

	/**
	 * Equality is important since it is used to avoid duplication of execution during processing.
	 * The Processor will return the already registered duplicate in place of a new one during
	 * building so it is important that important configurations are included in the equality of a
	 * {@link ProcessorListener}.
	 * 
	 * The default assumes that all processors are equal if they are of the same class.
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		} else {
			return getClass().equals(obj.getClass());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
	}
}
