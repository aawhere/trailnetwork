/**
 *
 */
package com.aawhere.track.story;

import com.aawhere.track.Track;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.calc.GoogleMapsEncodedPathCalculator;

/**
 * Provides the implementation to satisfy the user stories related to Creating, displaying summary
 * information about a detailed {@link Track} resulting in a Google Maps encodedPath {@see String}
 * 
 * TODO:move this to activity-export
 * 
 * @author Brian Chapman
 * 
 */
public class GoogleMapEncodedPathUserStory
		extends BaseTrackUserStory<String> {

	private GoogleMapsEncodedPathCalculator calculator;

	@Override
	public void process() {
		setResult(calculator.getEncodedPath());
	}

	/**
	 * Used to construct all instances of GoogleMapEncodedPathUserStory.
	 * 
	 * TODO:consider accepting a {@link TrackProcessor.Builder} to piggyback TODO:consider
	 * generalizing all track stories into a base story that handles track/processing.
	 * 
	 */
	public static class Builder
			extends BaseTrackUserStory.Builder<GoogleMapEncodedPathUserStory, Builder> {

		public Builder() {
			super(new GoogleMapEncodedPathUserStory());
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.track.story.BaseTrackUserStory.Builder#configureProcessorBuilder(com.aawhere
		 * .track.story.BaseTrackUserStory)
		 */
		@Override
		protected void configureProcessorBuilder(GoogleMapEncodedPathUserStory built) {
			built.calculator = new GoogleMapsEncodedPathCalculator.Builder()
					.registerWithProcessor(getProcessorBuilder()).build();
		}

	}// end Builder

	/** Use {@link Builder} to construct GoogleMapEncodedPathUserStory */
	private GoogleMapEncodedPathUserStory() {
	}

	public static Builder create() {
		return new Builder();
	}

}
