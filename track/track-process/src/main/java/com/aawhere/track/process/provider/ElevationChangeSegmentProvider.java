/**
 * 
 */
package com.aawhere.track.process.provider;

import com.aawhere.measure.ElevationChange;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;

/**
 * @author Aaron Roller
 * 
 */
public class ElevationChangeSegmentProvider
		extends ProcessorListenerBase
		implements TrackSegmentCalculatedQuantityProvider<ElevationChange> {

	/**
	 * Used to construct all instances of ElevationChangeSegmentProvider.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<ElevationChangeSegmentProvider, Builder> {

		public Builder() {
			super(new ElevationChangeSegmentProvider());
		}

	}// end Builder

	private ElevationChange change;

	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		change = segment.getElevationChange();

	}

	@Override
	public ElevationChange getQuantity() {
		return change;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return this.change != null;
	}

}
