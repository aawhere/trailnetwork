/**
 * 
 */
package com.aawhere.track.process.seek;

import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * A trackpoint reducer that ignores any trackpoints within the frequency identified by
 * {@link #minDurationBetweenTrackpoints}.
 * 
 * This will not create a fixed frequency at the duration, but just ignore those that happen more
 * frequently.
 * 
 * @author roller
 * 
 */
public class DurationTrackpointSeeker
		extends TrackpointSeekerBase
		implements TrackpointListener {

	public static class Builder
			extends TrackpointSeekerBase.Builder<DurationTrackpointSeeker, Builder> {
		public Builder() {
			super(new DurationTrackpointSeeker());
			// set the default value
			building.minDurationBetweenTrackpoints = new Duration(5000);
		}

		public Builder setMinDurationBetweenTrackpoints(Duration duration) {
			building.minDurationBetweenTrackpoints = duration;
			return this;
		}
	}

	/** The inclusive duration between trackpoints */
	private Duration minDurationBetweenTrackpoints;

	/** The previous trackpoint that was passed through. */
	private Trackpoint previouslyKeptTrackpoint;

	private DurationTrackpointSeeker() {
	}

	/**
	 * Passes through any trackpoint that follows another by the
	 * {@link #minDurationBetweenTrackpoints}.
	 * 
	 * @see com.aawhere.track.process.TrackpointListener #handle(com.aawhere.track.Trackpoint,
	 *      com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		if (previouslyKeptTrackpoint == null || isDurationExceeded(trackpoint)) {
			desiredTrackpointFound(trackpoint);
			previouslyKeptTrackpoint = trackpoint;
		}
		// otherwise just ignore...
	}

	/**
	 * @param trackpoint
	 * @return
	 */
	private boolean isDurationExceeded(Trackpoint trackpoint) {
		Interval interval = new Interval(previouslyKeptTrackpoint.getTimestamp(), trackpoint.getTimestamp());
		final Duration duration = interval.toDuration();
		return duration.isEqual(minDurationBetweenTrackpoints) || duration.isLongerThan(minDurationBetweenTrackpoints);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((this.minDurationBetweenTrackpoints == null) ? 0 : this.minDurationBetweenTrackpoints.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof DurationTrackpointSeeker))
			return false;
		DurationTrackpointSeeker other = (DurationTrackpointSeeker) obj;
		if (this.minDurationBetweenTrackpoints == null) {
			if (other.minDurationBetweenTrackpoints != null)
				return false;
		} else if (!this.minDurationBetweenTrackpoints.equals(other.minDurationBetweenTrackpoints))
			return false;
		return true;
	}

}
