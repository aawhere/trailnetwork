/**
 * 
 */
package com.aawhere.track.process.seek.virtual;

import java.util.Iterator;

import org.joda.time.DateTime;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;

/**
 * Provides the function of {@link TargetTimeVirtualTrackpointSeeker}, but iterates a sequential
 * list of {@link DateTime}s which calls {@link #setNextTargetedTimestamp(DateTime)} with the next
 * timestamp in the iterator.
 * 
 * 
 * 
 * @author roller
 * 
 */
public class SequentialTimestampTrackpointSeeker
		extends TargetTimeVirtualTrackpointSeeker {

	public abstract static class BaseBuilder<BuilderResultT extends SequentialTimestampTrackpointSeeker, BuilderT extends BaseBuilder<BuilderResultT, BuilderT>>
			extends TargetTimeVirtualTrackpointSeeker.Builder<BuilderResultT, BuilderT> {

		/**
		 * @param seeker
		 */
		protected BaseBuilder(BuilderResultT seeker) {
			super(seeker);
		}

		public BuilderT setTargetTimestamps(Iterable<DateTime> targetTimestampsContainer) {
			building.setTargetTimestamps(targetTimestampsContainer.iterator());
			return dis;
		}

		public BuilderT setTargetTimestamps(Iterator<DateTime> targetTimestamps) {
			building.setTargetTimestamps(targetTimestamps);
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.seek.TrackpointSeekerBase.Builder#validate ()
		 */
		@Override
		protected void validate() {

			super.validate();
			Assertion.assertNotNull("targetTimestamps", building.getTargetTimestamps());

		}

	}

	public static class Builder
			extends BaseBuilder<SequentialTimestampTrackpointSeeker, Builder> {
		public Builder() {
			super(new SequentialTimestampTrackpointSeeker());
		}
	}

	private Iterator<DateTime> targetTimestamps;
	private boolean initialized = false;

	/**
	 * 
	 */
	protected SequentialTimestampTrackpointSeeker() {
	}

	void setTargetTimestamps(Iterator<DateTime> timestamps) {
		this.targetTimestamps = timestamps;
		incrementTarget();
	}

	public Iterator<DateTime> getTargetTimestamps() {
		return targetTimestamps;
	}

	/**
	 * Fast forwards through the timestamps until the {@link #getNextTargetedTimestamp()} is after
	 * the the trackpoint's timestamp.
	 * 
	 * @see com.aawhere.track.process.seek.virtual.VirtualTrackpointSeeker#handle
	 *      (com.aawhere.track.Trackpoint, com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {

		// only want to fast-forward once since regular processing should keep forwarding
		if (!initialized) {

			final DateTime trackpointTimestamp = trackpoint.getTimestamp();
			// call for the next timestamp every time because increment changes it
			while (!isFinished() && getNextTargetedTimestamp().isBefore(trackpointTimestamp)) {
				incrementTarget();
			}
			initialized = true;
		}
		super.handle(trackpoint, processor);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.seek.virtual.TargetTimeVirtualTrackpointSeeker
	 * #incrementTarget()
	 */
	@Override
	protected void incrementTarget() {
		if (this.targetTimestamps.hasNext()) {
			setNextTargetedTimestamp(this.targetTimestamps.next());
		} else {
			finish();
		}
	}

}
