/**
 * 
 */
package com.aawhere.track.process.seek.virtual;

import javax.measure.quantity.Quantity;

import org.apache.commons.math.stat.descriptive.summary.Sum;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.calc.QuantityUnivariateStatistic;
import com.aawhere.track.SimpleTrackSegment;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.UnfinishedProcessorListener;

/**
 * A {@link VirtualTrackpointSeeker} that creates virtual trackpoints when the targeted sum has been
 * met for any {@link Quantity}. The first and last point of the track is always included regardless
 * if the sum has been met.
 * 
 * @author aroller
 * 
 */
public class FixedQuantitySumVirtualTrackpointSeeker<Q extends Quantity<Q>>
		extends VirtualTrackpointSeeker
		implements TrackSegmentListener, UnfinishedProcessorListener {

	/** The fixed targetSum determining when a virtual trackpoint is required. */
	private Q targetSum;
	private TrackSegmentCalculatedQuantityProvider<Q> provider;
	private QuantityUnivariateStatistic<Q, Sum> sumCalculator;
	private QuantityMath<Q> targetMath;
	private boolean initialized = false;
	private Trackpoint potentialFinalTrackpoint;

	@Deprecated
	private Trackpoint first;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track.TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		// "sum" quantities must always include the first trackpoint
		if (!initialized) {
			desiredTrackpointFound(segment.getFirst());
			initialized = true;
			this.first = segment.getFirst();
		}
		final Trackpoint segmentEndpoint = segment.getSecond();
		this.potentialFinalTrackpoint = segmentEndpoint;
		Q segmentQuantity = provider.getQuantity();
		sumCalculator.increment(segmentQuantity);
		Q sum;

		// iterate through this (and virtual segments) creating points at every sum achieved
		// there often may be multiple per segment
		// |--|--|--|-|
		TrackSegment remainingSegment = segment;
		Q remainingQuantityOfSegment = segmentQuantity;
		// create a trackpoint for as many segments
		while (targetMath.lessThanEqualTo(sum = sumCalculator.getResult())) {

			// this segment's quantity put us over the edge of the total
			// interpolation is within the segment, but it may produce multiple segments
			// find the ratio within the segment that achieved the target
			Q extraFromSum = QuantityMath.create(sum).minus(targetSum).getQuantity();
			Q partOfSegment = QuantityMath.create(remainingQuantityOfSegment).minus(extraFromSum).getQuantity();

			Ratio ratioOfSegmentRemaining = QuantityMath.create(partOfSegment).over(remainingQuantityOfSegment);
			Trackpoint targetReached = targetReached(remainingSegment, ratioOfSegmentRemaining);
			// update to reflect the new "virtual segment"
			remainingSegment = new SimpleTrackSegment(targetReached, segmentEndpoint);
			// sum gets the extra for next segment or next portion of this segment
			sumCalculator.clear();
			sumCalculator.increment(extraFromSum);
			// in case there is another pass the segment length must be reduced
			remainingQuantityOfSegment = extraFromSum;
		}
	}

	/**
	 * When a target has been achieved within a segment, provide the ratio from the start of the
	 * segment and this will interpolate the virtual trackpoint.
	 * 
	 * @see TrackUtil#interpolate(TrackSegment, Ratio)
	 * @param segment
	 * @param ratioFromStart
	 * @return
	 */
	protected Trackpoint targetReached(TrackSegment segment, Ratio ratioFromStart) {
		Trackpoint point = TrackUtil.interpolate(segment, ratioFromStart);
		desiredTrackpointFound(point);
		// must keep track of the last point for #finish()
		if (potentialFinalTrackpoint != null && TrackUtil.equals(point, potentialFinalTrackpoint)) {
			// already notified last one so no need to keep around
			potentialFinalTrackpoint = null;
		}
		return point;
	}

	/**
	 * Used to construct all instances of FixedQuantitySumVirtualTrackpointSeeker.
	 */
	public static class Builder<Q extends Quantity<Q>>
			extends VirtualTrackpointSeeker.Builder<FixedQuantitySumVirtualTrackpointSeeker<Q>, Builder<Q>> {

		public Builder() {
			super(new FixedQuantitySumVirtualTrackpointSeeker<Q>());
		}

		public Builder<Q> targetSum(Q targetSum) {
			building.targetSum = targetSum;
			return this;
		}

		public Builder<Q> provider(TrackSegmentCalculatedQuantityProvider<Q> provider) {
			building.provider = provider;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.TrackpointSeekerBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("provider", building.provider);

		}

		@SuppressWarnings("unchecked")
		@Override
		public FixedQuantitySumVirtualTrackpointSeeker<Q> build() {
			FixedQuantitySumVirtualTrackpointSeeker<Q> built = super.build();
			built.targetMath = QuantityMath.create(built.targetSum);
			built.sumCalculator = QuantityUnivariateStatistic.newSum(built.targetSum.getClass());
			return built;
		}

	}// end Builder

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointSeekerBase#finish()
	 */
	@Override
	public void finish() {
		// if 0 or 1 trackpoint
		if (potentialFinalTrackpoint != null) {
			desiredTrackpointFound(potentialFinalTrackpoint);
		}
		super.finish();
	}

	/** Use {@link Builder} to construct FixedQuantitySumVirtualTrackpointSeeker */
	private FixedQuantitySumVirtualTrackpointSeeker() {
	}

	public static <Q extends Quantity<Q>> Builder<Q> create() {
		return new Builder<Q>();
	}

}
