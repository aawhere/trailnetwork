/**
 *
 */
package com.aawhere.track.process.processor;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.UnfinishedProcessorListener;

/**
 * Provides {@link TrackProcessorBase} iteration of trackpoints that are provided externally by some
 * provider.
 * 
 * @author roller
 * 
 */
public class ManualTrackProcessor
		extends TrackProcessorBase {

	/**
	 *
	 */
	private ManualTrackProcessor() {

	}

	/**
	 * Trackpoints are sent to all the listeners by the external provider.
	 * 
	 * @see com.aawhere.track.process.TrackProcessorBase#notifyTrackpointListeners
	 *      (com.aawhere.track.Trackpoint)
	 */
	@Override
	public void notifyTrackpointListeners(Trackpoint trackpoint) {
		super.notifyTrackpointListeners(trackpoint);
	}

	public static class Builder
			extends TrackProcessorBase.Builder<ManualTrackProcessor> {

		/**
		 *
		 */
		public Builder() {
			super(new ManualTrackProcessor());
		}

	}

	/**
	 * Since the trackpoints are being provided externally, whoever is providing them should call
	 * this to complete the {@link UnfinishedProcessorListener} s.
	 * 
	 * Visibility increased to provide access to client.
	 * 
	 */
	@Override
	public void finish() {
		super.finish();
	}

	/**
	 * 
	 */
	public static Builder create() {
		return new Builder();
	}
}
