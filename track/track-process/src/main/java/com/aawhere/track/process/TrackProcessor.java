/**
 * 
 */
package com.aawhere.track.process;

/**
 * A variety of processing classes that delegate calcuations and work to be done by
 * {@link TrackSegmentListener}s and {@link TrackpointListener}s by iterating through the track in
 * sequential order and notifying listeners.
 * 
 * @author roller
 * 
 */
public interface TrackProcessor {

	/**
	 * Given the class of processor listener, this will return the first (if more than one match) of
	 * the object that is an exact match of the type given.
	 * 
	 * @param listenerType
	 * @return
	 */
	public <T extends ProcessorListener> T getListener(Class<T> listenerType);

	/**
	 * A unique identifier that represents this particular processor. Useful when identifying a
	 * processor source during the handle method.
	 * 
	 * @return
	 */
	public String getKey();
}
