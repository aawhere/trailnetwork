/**
 * 
 */
package com.aawhere.track.process.provider;

import javax.measure.quantity.Acceleration;

import org.joda.time.Duration;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;

/**
 * Looking at multiple {@link TrackSegment}s this will calculate the acceleration between the two
 * segments.
 * 
 * @author Aaron Roller on Apr 8, 2011
 * 
 */
public class AccelerationSegmentProvider
		extends ProcessorListenerBase
		implements TrackSegmentCalculatedQuantityProvider<Acceleration> {

	/**
	 * Used to construct all instances of AccelerationSegmentProvider.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<AccelerationSegmentProvider, Builder> {

		public Builder() {
			super(new AccelerationSegmentProvider());
		}

		@Override
		public AccelerationSegmentProvider build() {
			AccelerationSegmentProvider built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct AccelerationSegmentProvider */
	private AccelerationSegmentProvider() {
	}

	private TrackSegment previousSegment;
	private Acceleration acceleration;

	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		if (previousSegment != null) {

			// Calculate the time between the half-way point of each segment.
			Duration accelDuration = new Duration((previousSegment.getDuration().getMillis() + segment.getDuration()
					.getMillis()) / 2);
			acceleration = MeasurementUtil.acceleration(previousSegment.getVelocity(),
														segment.getVelocity(),
														accelDuration);
			// Question: Should we use duration from both segments?
			// Answer: I don't think so. The change in velocity is relative only to the duration of
			// the second. Although the velocity is technically the average of each segment, the
			// change is relative to the second.
			// Re-Answer: I (Brian) disagree with the first answer. I think the acceleration should
			// be calculated using the time between the midpoint of each segment. So if the previous
			// segment was 10s and the current segment was 5s, the time interval used to calculate
			// the acceleration would be 7.5 seconds instead of the current 5s. This will best
			// average out the change in speed between the two segments since we do not know the
			// actual speed of travel at the end of each segment.
		} else {
			acceleration = MeasurementUtil.createAccelerationInMpss(0);
		}
		previousSegment = segment;

	}

	@Override
	public Acceleration getQuantity() {
		return Assertion.assertNotNull(this.acceleration);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return this.acceleration != null;
	}

}
