/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * Given a {@link Period} this will send only those trackpoints through that are within the time
 * period. This differs from the {@link Interval#contains(org.joda.time.ReadableInstant)} in that
 * trackpoints are included if they equal to the end (i.e. inclusive of the end).
 * 
 * NOTE: This would be fine to provide an option outside the time filter also.
 * 
 * @author aroller
 * 
 */
public class TimeIntervalTrackpointFilter
		extends TrackpointSeekerBase
		implements TrackpointListener {

	/**
	 * Used to construct all instances of TimePeriodTrackpointFilter.
	 */
	public static class Builder
			extends TrackpointSeekerBase.Builder<TimeIntervalTrackpointFilter, Builder> {

		private Interval interval;

		public Builder() {
			super(new TimeIntervalTrackpointFilter());
		}

		public Builder interval(Interval interval) {
			this.interval = interval;
			building.start = interval.getStartMillis();
			building.end = interval.getEndMillis();
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.TrackpointSeekerBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("interval", this.interval);

		}

		public TimeIntervalTrackpointFilter build() {
			TimeIntervalTrackpointFilter built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TimePeriodTrackpointFilter */
	private TimeIntervalTrackpointFilter() {
	}

	// used for efficiency since once the end is found
	private boolean keepLooking = true;
	private long start;
	private long end;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		DateTime timestamp = trackpoint.getTimestamp();

		// this is intentionally efficient to avoid checking when not necessary
		if (keepLooking) {
			if (timestamp.isBefore(start)) {
				keepLooking = true;
			} else if (timestamp.isAfter(end)) {
				keepLooking = false;
			} else {
				// must be between...notify
				desiredTrackpointFound(trackpoint);
			}
		}
	}

}
