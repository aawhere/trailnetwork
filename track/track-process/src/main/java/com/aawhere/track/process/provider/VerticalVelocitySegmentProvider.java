/**
 * 
 */
package com.aawhere.track.process.provider;

import javax.measure.quantity.Velocity;

import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;

/**
 * @author Aaron Roller on Apr 7, 2011
 * 
 */
public class VerticalVelocitySegmentProvider
		extends ProcessorListenerBase
		implements TrackSegmentCalculatedQuantityProvider<Velocity> {

	/**
	 * Used to construct all instances of VerticalVelocitySegmentProvider.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<VerticalVelocitySegmentProvider, Builder> {

		public Builder() {
			super(new VerticalVelocitySegmentProvider());
		}

	}// end Builder

	/** Use {@link Builder} to construct VerticalVelocitySegmentProvider */
	private VerticalVelocitySegmentProvider() {
	}

	private Velocity velocity;

	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {

		this.velocity = segment.getVerticalVelocity();
	}

	@Override
	public Velocity getQuantity() {

		return velocity;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {

		return this.velocity != null;
	}

}
