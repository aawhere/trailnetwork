/**
 *
 */
package com.aawhere.track.process.processor;

import com.aawhere.joda.time.InvalidIntervalRuntimeException;
import com.aawhere.track.SimpleTrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListener;
import com.aawhere.track.process.ResettingProcessorListener;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.UnfinishedProcessorListener;

/**
 * A {@link ManualTrackProcessor} that is driven by one or more {@link TrackpointSeeker}s that
 * provide trackpoints by calling back {@link #desiredTrackpointFound(Trackpoint)} when a qualifying
 * trackpoint is found.
 * 
 * Since {@link TrackpointSeeker}s are just {@link ProcessorListener}s, multiple seekers may be
 * registered. Sometimes these seekers will find the same point so then the
 * {@link DuplicationDetector} is used to ignore duplicates.
 * 
 * 
 * @author roller
 */
public class TrackpointSeekingProcessor
		extends TrackProcessorBase
		implements ResettingProcessorListener {

	public static class Builder
			extends BaseBuilder<TrackpointSeekingProcessor, Builder> {
		/**
		 * Used to construct all instances of TrackpointSeekingProcessor.
		 */
		public Builder() {
			super(new TrackpointSeekingProcessor());
		}

	}

	/**
	 * Abstract used by others that extend TrackpointSeekingProcessor
	 * 
	 * @author aroller
	 * 
	 * @param <P>
	 * @param <B>
	 */
	public static class BaseBuilder<P extends TrackpointSeekingProcessor, B extends BaseBuilder<P, B>>
			extends TrackProcessorBase.Builder<P> {

		public BaseBuilder(P processor) {
			super(processor);
		}

	}// end Builder

	/** Use {@link Builder} to construct TrackpointSeekingProcessor */

	private DuplicationDetector duplicatationDetector = new DuplicationDetector();

	/**
	 * Available for seekers to notify listeners that the trackpoint being sought after has been
	 * detected.
	 * 
	 * @param trackpoint
	 * @return
	 */
	public Trackpoint desiredTrackpointFound(Trackpoint trackpoint) {
		Trackpoint duplicate = duplicatationDetector.duplicateDetected(trackpoint);
		// notify listeners only if this trackpoint is original
		if (duplicate == null) {
			super.notifyTrackpointListeners(trackpoint);
		}
		return duplicate;
	}

	@Override
	public void reset() {
		super.reset();
	}

	/**
	 *
	 */
	protected TrackpointSeekingProcessor() {

	}

	/**
	 * Used to stop duplicate notification due to multiple Seekers reporting the same trackpoint.
	 * 
	 * Currently this will only detect duplicates within the relative zone and not absolutely
	 * guarentee duplication. This means if two seekers report the trackpoint with the same
	 * timestamp in sequence duplication will be detected. This pattern works well if the seekers
	 * are part of the same processor and being notified in sequence. This works by keeping only a
	 * single trackpoint around and when a trackpoint of new timestamp is found the
	 * {@link #previousTrackpoint} will be replaced.
	 * 
	 * 
	 * 
	 * @author roller
	 * 
	 */
	static class DuplicationDetector {

		private Trackpoint previousTrackpoint;

		/**
		 * returns the previous trackpoint if the equal is found or null if not.
		 * 
		 * @param currentTrackpoint
		 * @return
		 * @throws IllegalArgumentException
		 *             when a trackpoint with the same timestamp is found, but other values are not
		 *             equal.
		 */
		public Trackpoint duplicateDetected(Trackpoint currentTrackpoint) {
			Trackpoint result;

			if (previousTrackpoint != null
					&& previousTrackpoint.getTimestamp().equals(currentTrackpoint.getTimestamp())) {

				// fine, so timestamps are the same, but if not everything is
				// the same then let's tell the programmer
				if (!TrackUtil.equalsDeeply(previousTrackpoint, currentTrackpoint)) {
					throw new InvalidIntervalRuntimeException(previousTrackpoint.getTimestamp(),
							new SimpleTrackSegment(previousTrackpoint, currentTrackpoint));
				} else {
					result = previousTrackpoint;
				}

			} else {
				// move on to the next
				previousTrackpoint = currentTrackpoint;
				result = null;
			}
			return result;
		}

	}

	/**
	 * @return
	 */
	public static Builder create() {
		return new Builder();
	}

	/**
	 * Unlike other processors, this one doesn't know when it is finished unless it is told so it is
	 * an {@link UnfinishedProcessorListener}.
	 * 
	 * @see com.aawhere.track.process.TrackProcessorBase#finish()
	 */
	@Override
	public void finish() {

		super.finish();
	}
}
