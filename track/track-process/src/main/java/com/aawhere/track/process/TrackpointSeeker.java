/**
 * 
 */
package com.aawhere.track.process;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;

/**
 * Seekers find {@link Trackpoint}s of interest during processing of a {@link TrackProcessor}. A
 * seeker is a {@link ProcessorListener} that will report trackpoints matching their search to the
 * {@link #getReceiver()}.
 * 
 * 
 * @author roller
 * 
 */
public interface TrackpointSeeker {

	/**
	 * Each seeker must be registered with a {@link TrackProcessor} that will be the recipient of a
	 * found {@link Trackpoint}.
	 * 
	 * @return
	 */
	public TrackpointSeekingProcessor getReceiver();
}
