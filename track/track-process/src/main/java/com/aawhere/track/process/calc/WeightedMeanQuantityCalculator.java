/**
 * 
 */
package com.aawhere.track.process.calc;

import javax.measure.quantity.Quantity;

import com.aawhere.measure.calc.QuantityWeightedEvaluation;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;

/**
 * Calculates the weighted mean of any {@link Quantity} that is provided by a {@link TrackSegment}.
 * 
 * Segments are required to weight the Quantity against the duration of the current segment.
 * 
 * @author roller
 * 
 */
public class WeightedMeanQuantityCalculator<Q extends Quantity<Q>>
		extends ProcessorListenerBase
		implements TrackSegmentCalculatedQuantityProvider<Q> {

	/**
	 * Used to construct all instances of WeightedMeanQuantityCalculator.
	 */
	public static class Builder<Q extends Quantity<Q>>
			extends ProcessorListenerBase.Builder<WeightedMeanQuantityCalculator<Q>, Builder<Q>> {

		public Builder() {
			super(new WeightedMeanQuantityCalculator<Q>());
		}

		public Builder<Q> setProvider(TrackSegmentCalculatedQuantityProvider<Q> provider) {
			building.provider = provider;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct WeightedMeanQuantityCalculator */
	private WeightedMeanQuantityCalculator() {
	}

	private TrackSegmentCalculatedQuantityProvider<Q> provider;
	private QuantityWeightedEvaluation<Q> weightedMeanCalculator = QuantityWeightedEvaluation.newMean();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return this.weightedMeanCalculator.hasResult();
	}

	/*
	 * (non-Javadoc)
	 * @seecom.aawhere.track.process.calc.TrackSegmentMeasurementProvider# getSegmentQuantity()
	 */
	@Override
	public Q getQuantity() {
		return this.weightedMeanCalculator.getResult();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track .TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		final Q segmentQuantity = provider.getQuantity();

		weightedMeanCalculator.increment(segmentQuantity, segment.getDuration());

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.provider == null) ? 0 : this.provider.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof WeightedMeanQuantityCalculator))
			return false;
		WeightedMeanQuantityCalculator<?> other = (WeightedMeanQuantityCalculator<?>) obj;
		if (this.provider == null) {
			if (other.provider != null)
				return false;
		} else if (!this.provider.equals(other.provider))
			return false;
		return true;
	}

}
