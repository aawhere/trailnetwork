/**
 *
 */
package com.aawhere.track.match.calc;

import com.aawhere.track.TrackSegment;

/**
 * @author brian
 * 
 */
public interface LcsPredicate {

	public Boolean match(TrackSegment first, TrackSegment second);
}
