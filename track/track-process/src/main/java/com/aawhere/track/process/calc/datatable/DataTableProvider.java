/**
 *
 */
package com.aawhere.track.process.calc.datatable;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

import com.google.visualization.datasource.datatable.ColumnDescription;
import com.google.visualization.datasource.datatable.DataTable;
import com.google.visualization.datasource.datatable.TableCell;
import com.google.visualization.datasource.datatable.TableRow;

/**
 * Provides a {@link DataTable} for use in the Google Charts DataSource protocal.
 * 
 * @See TrackDataSourceServlet
 * 
 * @author Brian Chapman
 * 
 */
public class DataTableProvider
		extends ProcessorListenerBase
		implements TrackpointListener {

	/**
	 * Used to construct all instances of TrackChartDataSourceProvider.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<DataTableProvider, Builder> {

		DataTable dataTable = new DataTable();

		public Builder() {
			super(new DataTableProvider());
		}

		public Builder setProvider(TableCellProvider provider) {
			building.providers.add(provider);
			return this;
		}

		@Override
		public DataTableProvider build() {
			ArrayList<ColumnDescription> cd = new ArrayList<ColumnDescription>();
			for (TableCellProvider provider : building.providers) {
				cd.add(new ColumnDescription(provider.getColumnId(), provider.getColumnType(), provider
						.getColumnLabel().getValue()));
			}
			dataTable.addColumns(cd);
			building.dataTable = dataTable;
			return super.build();
		}

	}// end Builder

	/** Use {@link Builder} to construct TrackChartDataSourceProvider */
	private DataTableProvider() {
	}

	private List<TableCellProvider> providers = new ArrayList<TableCellProvider>();
	DataTable dataTable;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		TableRow row = new TableRow();
		for (TableCellProvider provider : providers) {
			if (provider.hasCell()) {
				final TableCell cell = provider.getCell();
				row.addCell(cell);
			}
		}
		if (!row.getCells().isEmpty()) {
			try {
				dataTable.addRow(row);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	public DataTable getDataTable() {
		return this.dataTable;
	}

	public Boolean hasDataTable() {
		return this.dataTable != null;
	}
}
