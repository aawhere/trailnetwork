package com.aawhere.track.process.calc;

import java.util.LinkedList;
import java.util.Map;

import javax.measure.quantity.Angle;
import javax.measure.unit.USCustomarySystem;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.AngleMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.SupplierMultisetProcessorListener;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackProcessorBase;
import com.aawhere.track.process.TrackSegmentListener;
import com.google.common.base.Supplier;
import com.google.common.collect.HashMultiset;

/**
 * In the effort to understand {@link SignalQuality} it is helpful to understand the heading
 * consistency of a track. Some poorly recorded GPS data will swirl around like a pig tail when
 * actually the person traveled straight. This is a signal of poor quality. In cases where the
 * person is actually going in circles then this works as a detector of a person that is tracking an
 * activity that is unlikely to be desirable to follow so reporting poor signal is fine in those
 * cases too.
 * 
 * @author aroller
 * 
 */
public class TrackHeadingConsitencyCalculator
		extends ProcessorListenerBase
		implements TrackSegmentListener, Supplier<SignalQuality> {

	private static final int SLIGHT_DEGREES = 45;
	private static final int TURNING_DEGREES = 90;
	static final int BUFFER_SIZE = 10;
	/** The number of observations to keep around. */
	private int bufferSizeLimit = BUFFER_SIZE;
	private LinkedList<TrackSegment> queue = new LinkedList<TrackSegment>();
	private LinkedList<Angle> headings = new LinkedList<Angle>();
	private LinkedList<DeviationAmount> deviations = new LinkedList<>();
	private SupplierMultisetProcessorListener<SignalQuality> counter;

	enum DeviationAmount {
		SLIGHT(SLIGHT_DEGREES), TURNING(TURNING_DEGREES), REVERSING(MeasurementUtil.HALF_CIRCLE
				.doubleValue(USCustomarySystem.DEGREE_ANGLE));
		final Angle tolerance;

		private DeviationAmount(double toleranceInDegrees) {
			this.tolerance = MeasurementUtil.createAngleInDegrees(toleranceInDegrees);
		}
	}

	/**
	 * The angle, in either direction from the ideal heading, that is allowed private Angle
	 * deviationTolerance;
	 * 
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		queue.add(segment);
		Angle heading = TrackUtil.heading(segment);
		Angle previous = headings.peekLast();
		headings.add(heading);

		if (previous != null) {

			QuantityMath<Angle> deltaMath = AngleMath.create(previous).delta(heading).absolute();
			DeviationAmount deviationAmount;

			if (deltaMath.lessThan(DeviationAmount.SLIGHT.tolerance)) {
				deviationAmount = DeviationAmount.SLIGHT;
			} else if (deltaMath.lessThan(DeviationAmount.TURNING.tolerance)) {
				deviationAmount = DeviationAmount.TURNING;
			} else {
				deviationAmount = DeviationAmount.REVERSING;
			}
			deviations.add(deviationAmount);
			if (queue.size() > bufferSizeLimit) {
				queue.removeFirst();
				headings.removeFirst();
				deviations.removeFirst();
			}
		} else {
			// this keeps all the queue sizes consistent and doesn't make any mess
			deviations.add(DeviationAmount.SLIGHT);
		}

	}

	/**
	 * 
	 * /** Indicates the signal quality for the current segments being considered...which may
	 * change. Since it is difficult to know if the current segment is going wacko, since perhaps it
	 * was a switchback turn-around, we have to report the segment quality a few segments later. So
	 * this shouldn't be used to nuke a particular segment, but rather to indicate a problem with an
	 * area of a track.
	 * 
	 * @return
	 */
	@Override
	public SignalQuality get() {

		HashMultiset<DeviationAmount> deviationCount = HashMultiset.create(this.deviations);
		// weights how much worse reversing is than turning
		int reversingCount = deviationCount.count(DeviationAmount.REVERSING);
		// one reverse allowed, but any more and we have a problem
		if (reversingCount > 1) {
			reversingCount *= 2;
		}
		int turningCount = deviationCount.count(DeviationAmount.TURNING);
		// cumultation of the two provides an idea of how crazy the line is...without caring which
		// is cuasing it
		int total = reversingCount + turningCount;
		// the higher number is worse.
		// these numbers are affected by buffer size
		switch (total) {
			case 0:
			case 1:
				// straight lines are lovely
				// 1 reverse or turn is fine
				return SignalQuality.GREAT;
			case 2:
			case 3:
				return SignalQuality.GOOD;
			case 4:
			case 5:
				return SignalQuality.FAIR;
			default:
				return SignalQuality.POOR;
		}

	}

	/**
	 * if registered during building, this will provide the summary count for each
	 * {@link #handle(TrackSegment, TrackProcessor)}called so far. It is a connected listener and
	 * will update.
	 * 
	 * @return
	 */
	public SupplierMultisetProcessorListener<SignalQuality> counter() {
		Assertion.exceptions().notNull("call Builder#counter()", counter);
		return counter;
	}

	/**
	 * Provides an analysis of all that is known so far by providing a single value to indicate the
	 * quality.
	 * 
	 * @return
	 */
	public SignalQuality summary() {
		Map<SignalQuality, Ratio> ratios = MeasurementUtil.ratios(this.counter().get());
		SignalQuality quality;
		Ratio greatRatio = ratios.get(SignalQuality.GREAT);
		Ratio goodRatio = ratios.get(SignalQuality.GOOD);
		Ratio poorRatio = ratios.get(SignalQuality.POOR);

		if (greatRatio != null && QuantityMath.create(greatRatio).greaterThan(MeasurementUtil.ratio(0.90))) {
			quality = SignalQuality.GREAT;
		} else if (poorRatio != null && QuantityMath.create(poorRatio).greaterThan(MeasurementUtil.ratio(0.05))) {
			quality = SignalQuality.POOR;

		} else if (goodRatio != null
				&& QuantityMath.create(greatRatio).plus(goodRatio).greaterThan(MeasurementUtil.ratio(0.90))) {
			// good and great together makes the majority then it's good
			quality = SignalQuality.GOOD;
		} else {
			quality = SignalQuality.FAIR;
		}
		return quality;
	}

	/**
	 * Used to construct all instances of TrackHeadingConsitencyCalculator.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackHeadingConsitencyCalculator, Builder> {

		private TrackProcessorBase.Builder<? extends TrackProcessorBase> processorBuilder;
		private boolean counter;

		public Builder() {
			super(new TrackHeadingConsitencyCalculator());
		}

		/**
		 * Optionally register the standard counter the will monitor the segments and provide the
		 * summary.
		 * 
		 * @param processorBuilder
		 * @return
		 */
		public Builder counter() {
			this.counter = true;
			return this;
		}

		@Override
		public Builder registerWithProcessor(
				com.aawhere.track.process.TrackProcessorBase.Builder<? extends TrackProcessorBase> processorBuilder) {
			this.processorBuilder = processorBuilder;
			return super.registerWithProcessor(processorBuilder);
		}

		@Override
		public TrackHeadingConsitencyCalculator build() {
			TrackHeadingConsitencyCalculator built = super.build();
			// delaying the counter until built ensures the proper order with registration
			if (counter) {
				// TODO:this could skip some trackpoints for efficiency without much loss of
				// accruacy
				// because the buffer is slow to change
				built.counter = SupplierMultisetProcessorListener.<SignalQuality> createForSegments()
						.registerWithProcessor(processorBuilder).supplier(built).build();
			}
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrackHeadingConsitencyCalculator */
	private TrackHeadingConsitencyCalculator() {
	}

}
