/**
 *
 */
package com.aawhere.track.process;

import com.aawhere.track.Trackpoint;

/**
 * @see DuelingProcessorListenerBase
 * 
 * @author Aaron Roller
 * 
 */
public abstract class DuelingTrackpointListenerBase
		extends DuelingProcessorListenerBase
		implements TrackpointListener {

	/**
	 * Used to construct all instances of DuelingTrackpointListenerBase.
	 */
	public static class Builder<BuilderResultT extends DuelingTrackpointListenerBase, BuilderT extends Builder<BuilderResultT, BuilderT>>
			extends DuelingProcessorListenerBase.Builder<BuilderResultT, BuilderT> {

		public Builder(BuilderResultT building) {
			super(building);
		}

	}// end Builder

	/** Use {@link Builder} to construct DuelingTrackProcessorBase */
	protected DuelingTrackpointListenerBase() {
	}

	private Trackpoint sourceTrackpoint;

	@Override
	public final void handle(Trackpoint trackpoint, TrackProcessor processor) {

		String sourceProcessorKey = getSourceProcessorKey();
		String targetProcessorKey = getTargetProcessorKey();

		String key = processor.getKey();
		if (key.equals(sourceProcessorKey)) {
			sourceTrackpoint = trackpoint;
		} else if (key.equals(targetProcessorKey)) {
			handleTargetTrackpoint(trackpoint);
		} else {
			throw new IllegalArgumentException(key + " not valid " + sourceProcessorKey + " or " + targetProcessorKey);
		}
	}

	/**
	 * @return the sourceTrackpoint
	 */
	protected Trackpoint getSourceTrackpoint() {
		return this.sourceTrackpoint;
	}

	protected abstract void handleTargetTrackpoint(Trackpoint targetTrackpoint);
}
