/**
 * 
 */
package com.aawhere.track.process;

import javax.measure.quantity.Length;

import org.apache.commons.math.stat.descriptive.summary.Sum;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.SimpleTrack;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.calc.ProcessingCalculatorFactory;
import com.aawhere.track.process.calc.QuantityUnivariateStatisticProcessorCalculator;
import com.aawhere.track.process.calc.TrackCreatorTrackpointCalculator;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.provider.TrackSegmentDistanceProvider;
import com.aawhere.track.story.GoogleMapEncodedPathUserStory;
import com.aawhere.track.story.TrackUserStories;
import com.aawhere.track.story.TrackUserStories.Builder;
import com.google.common.base.Function;

/**
 * Useful functions that use {@link TrackProcessor} to work with {@link Track}s.
 * 
 * @author aroller
 * 
 */
public class TrackProcessorUtil {

	/**
	 * Given a track this will convert it to a simple track.
	 * 
	 * Use with caution since tracks should just be iterated once per request and if you do much
	 * with the result then you are iterating twice.
	 * 
	 * @param track
	 * @return
	 */
	public static Track simplify(Track track) {
		if (track instanceof SimpleTrack) {
			return (SimpleTrack) track;
		} else {
			AutomaticTrackProcessor.Builder processorBuilder = new AutomaticTrackProcessor.Builder(track);
			TrackCreatorTrackpointCalculator calculator = new TrackCreatorTrackpointCalculator.Builder()
					.registerWithProcessor(processorBuilder).build();
			processorBuilder.build();
			return calculator.getTrack();

		}
	}

	/**
	 * Calculates the cumulative length of each segment for a given track.
	 * 
	 * Warning: This is NOT intended to be used to calculate the length of entire tracks
	 * haphazardly. You should always consider using {@link TrackSegmentDistanceProvider} in an
	 * iteration along with other {@link ProcessorListener}s for optimal performance.
	 * 
	 * @param track
	 *            the series of trackpoints desired
	 * @return
	 */
	public static Length length(Iterable<Trackpoint> track) {
		AutomaticTrackProcessor.Builder processorBuilder = AutomaticTrackProcessor.create(track);
		QuantityUnivariateStatisticProcessorCalculator<Length, Sum> distanceCalculator = ProcessingCalculatorFactory
				.getInstance().newTotalDistanceCalculator(processorBuilder);
		processorBuilder.build();
		if (distanceCalculator.hasQuantity()) {
			return distanceCalculator.getQuantity();
		} else {
			return MeasurementUtil.ZERO_LENGTH;
		}
	}

	/**
	 * Given multiple tracks this will iterate each of them calculating the length of each and
	 * summing them together into a single length.
	 * 
	 * This is not intended to be used on full length tracks.
	 * 
	 * @param tracks
	 * @return
	 */
	public static Length lengthOfTracks(Iterable<Track> tracks) {
		QuantityMath<Length> math = QuantityMath.create(MeasurementUtil.createLengthInMeters(0));
		for (Iterable<Trackpoint> iterable : tracks) {
			math = math.plus(length(iterable));
		}

		return math.getQuantity();
	}

	public static Function<Track, String> gpolylineFromTrackFunction() {
		return new Function<Track, String>() {

			@Override
			public String apply(Track track) {
				return gpolyline(track);
			}

		};
	}

	/**
	 * Given a track this will transform it into a polyline using
	 * {@link GoogleMapEncodedPathUserStory}.
	 * 
	 * @param track
	 * @return
	 */
	public static String gpolyline(Track track) {
		String gpolyline;
		Builder storiesBuilder = TrackUserStories.create(track);
		GoogleMapEncodedPathUserStory googleMapStories = storiesBuilder.addStandardFilters()
				.setUserStory(GoogleMapEncodedPathUserStory.create());
		storiesBuilder.build();
		gpolyline = googleMapStories.getResult();
		return gpolyline;
	}
}
