/**
 * 
 */
package com.aawhere.track.process.provider;

import javax.measure.quantity.Velocity;

import com.aawhere.lang.Assertion;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;

/**
 * A measurement provider that provides the speed as appropriate for processing. Most times this
 * will just pass through the velocity from the segment, but allows for customization in case there
 * should be filtering, etc.
 * 
 * @author Aaron Roller on Apr 4, 2011
 * 
 */
public class VelocitySegmentProvider
		extends ProcessorListenerBase
		implements TrackSegmentCalculatedQuantityProvider<Velocity> {

	/**
	 * Used to construct all instances of VelocitySegmentProvider.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<VelocitySegmentProvider, Builder> {

		public Builder() {
			super(new VelocitySegmentProvider());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct VelocitySegmentProvider */
	private VelocitySegmentProvider() {
	}

	private Velocity velocity;

	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		velocity = segment.getVelocity();
	}

	@Override
	public Velocity getQuantity() {

		return Assertion.assertNotNull(this.velocity);

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return this.velocity != null;
	}

}
