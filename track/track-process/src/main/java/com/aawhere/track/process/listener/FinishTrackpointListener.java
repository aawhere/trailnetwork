/**
 *
 */
package com.aawhere.track.process.listener;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * Really basic...always reports the last notified Trackpoint as the finish.
 * 
 * @author Aaron Roller
 * 
 */
public class FinishTrackpointListener
		extends ProcessorListenerBase
		implements TrackpointListener {

	private Trackpoint finish;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		this.finish = trackpoint;

	}

	/**
	 * @return the finish
	 */
	public Trackpoint getFinish() {
		return this.finish;
	}

	/**
	 * Used to construct all instances of FinishTrackpointListener.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<FinishTrackpointListener, Builder> {

		public Builder() {
			super(new FinishTrackpointListener());
		}

	}// end Builder

	/** Use {@link Builder} to construct FinishTrackpointListener */
	private FinishTrackpointListener() {
	}

	public static Builder create() {
		return new Builder();
	}

}
