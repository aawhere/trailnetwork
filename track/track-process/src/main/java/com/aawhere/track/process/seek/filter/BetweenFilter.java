/**
 * 
 */
package com.aawhere.track.process.seek.filter;

import javax.measure.quantity.Quantity;

import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * Inspecting any quantity, this will pass on only those items that are between the min and max.
 * 
 * Inclusion is governed by the rules of {@link QuantityMath#between(Quantity, Quantity)}.
 * 
 * @author Aaron Roller on Apr 20, 2011
 * 
 */
public abstract class BetweenFilter<QuantityT extends Quantity<QuantityT>>
		extends TrackpointSeekerBase
		implements ProcessorFilter {

	/**
	 * Used to construct all instances of BetweenFilter.
	 */
	public static class Builder<BuilderResult extends BetweenFilter<QuantityT>, QuantityT extends Quantity<QuantityT>>
			extends TrackpointSeekerBase.Builder<BuilderResult, Builder<BuilderResult, QuantityT>> {

		public Builder(BuilderResult building) {
			super(building);
		}

		public Builder<BuilderResult, QuantityT> setMin(QuantityT min) {
			building.setMin(min);
			return this;
		}

		public Builder<BuilderResult, QuantityT> setMax(QuantityT max) {
			building.setMax(max);
			return this;
		}

		/**
		 * @see QuantityMath#between(Quantity, Quantity, Boolean)
		 * 
		 * @param includeEndpoints
		 * @return
		 */
		public Builder<BuilderResult, QuantityT> setIncludeEndpoints(Boolean includeEndpoints) {
			building.setIncludeEndpoints(includeEndpoints);
			return this;
		}
	}// end Builder

	/** Use {@link Builder} to construct BetweenFilter */
	protected BetweenFilter() {
	}

	private QuantityT min;
	private QuantityT max;
	private Boolean includeEndpoints = true;

	protected void handle(QuantityT quantity, Trackpoint trackpoint) {
		if (new QuantityMath<QuantityT>(quantity).between(min, max, includeEndpoints)) {
			desiredTrackpointFound(trackpoint);
		}
	}

	void setMin(QuantityT min) {
		this.min = min;
	}

	public QuantityT getMin() {
		return this.min;
	}

	public QuantityT getMax() {
		return this.max;
	}

	void setMax(QuantityT max) {
		this.max = max;
	}

	/**
	 * @param includeEndpoints
	 *            the includeEndpoints to set
	 */
	void setIncludeEndpoints(Boolean includeEndpoints) {
		this.includeEndpoints = includeEndpoints;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.includeEndpoints == null) ? 0 : this.includeEndpoints.hashCode());
		result = prime * result + ((this.max == null) ? 0 : this.max.hashCode());
		result = prime * result + ((this.min == null) ? 0 : this.min.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof BetweenFilter))
			return false;
		BetweenFilter<?> other = (BetweenFilter<?>) obj;
		if (this.includeEndpoints == null) {
			if (other.includeEndpoints != null)
				return false;
		} else if (!this.includeEndpoints.equals(other.includeEndpoints))
			return false;
		if (this.max == null) {
			if (other.max != null)
				return false;
		} else if (!this.max.equals(other.max))
			return false;
		if (this.min == null) {
			if (other.min != null)
				return false;
		} else if (!this.min.equals(other.min))
			return false;
		return true;
	}

}
