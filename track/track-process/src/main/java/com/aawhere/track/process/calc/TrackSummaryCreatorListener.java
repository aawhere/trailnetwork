/**
 *
 */
package com.aawhere.track.process.calc;

import javax.measure.quantity.Length;

import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.summary.Sum;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.lang.Assertion;
import com.aawhere.math.stats.Gain;
import com.aawhere.measure.ElevationChange;
import com.aawhere.track.TrackIncapableDocumentException;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.listener.FinishTrackpointListener;
import com.aawhere.track.process.listener.StartTrackpointListener;
import com.google.common.base.Supplier;

/**
 * Participates in processing to produces a populated {@link TrackSummary}.
 * 
 * {@link #equals(Object)} is not overridden because it is very questionable that anyone would want
 * two of these participating in the same processing.
 * 
 * @author Aaron Roller
 * 
 */
public class TrackSummaryCreatorListener
		extends ProcessorListenerBase
		implements TrackSegmentListener {

	private DurationUnivariateStatisticProcessorCalculator<Mean> meanDurationBetweenTrackpointsCalculator;
	private ArchivedDocumentId documentId;
	private BoundingBoxCalculator boundingBoxCalculator;
	private TrackpointCounter counter;
	private FinishTrackpointListener finishListener;
	private StartTrackpointListener startListener;
	private QuantityUnivariateStatisticProcessorCalculator<Length, Sum> totalDistanceCalculator;
	private QuantityUnivariateStatisticProcessorCalculator<Length, Mean> meanDistanceBetweenTrackpointsCalculator;
	private QuantityUnivariateStatisticProcessorCalculator<ElevationChange, Gain> elevationGainCalculator;
	private GeoCellProvider geoCellProvider;
	private Supplier<SignalQuality> signalQualitySupplier;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track.TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		// although not doing anything, someone may want to have track summary objects during
		// procesing.
	}

	public TrackSummary getTrackSummary() {
		// TODO: add the hasQuantity tests to avoid null pointers during processing
		TrackSummary.Builder builder = new TrackSummary.Builder();
		builder.setDocumentId(documentId);
		Integer count = counter.getCount();
		if (count < 2) {
			throw new TrackIncapableDocumentException(documentId);
		}
		builder.setNumberOfTrackpoints(count);

		builder.populatePoints(this.startListener.getStart(), this.finishListener.getFinish());
		builder.setDistance(totalDistanceCalculator.getQuantity());
		builder.setMeanDistanceBetweenTrackpoints(this.meanDistanceBetweenTrackpointsCalculator.getQuantity());
		builder.setMeanDurationBetweenTrackpoints(this.meanDurationBetweenTrackpointsCalculator.getDuration());
		builder.setElevationGain(this.elevationGainCalculator.getQuantity());
		if (signalQualitySupplier != null) {
			SignalQuality signalQuality = signalQualitySupplier.get();
			builder.setSignalQuality(signalQuality);
		}
		if (geoCellProvider != null) {
			builder.setSpatialBuffer(geoCellProvider.getGeoCellsAtAllResolutions());
		}
		if (boundingBoxCalculator != null) {
			builder.setBoundingBox(this.boundingBoxCalculator.getBoundingBox());
		}
		return builder.build();
	}

	public Boolean hasSummary() {
		return counter.getCount() > 1;
	}

	/**
	 * Used to construct all instances of TrackSummaryCreatorListener.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackSummaryCreatorListener, Builder> {

		public Builder() {
			super(new TrackSummaryCreatorListener());
		}

		public Builder setTrackpointCounter(TrackpointCounter counter) {
			building.counter = counter;
			return this;
		}

		public Builder setStartListener(StartTrackpointListener startListener) {
			building.startListener = startListener;
			return this;
		}

		public Builder setFinishListener(FinishTrackpointListener finishListener) {
			building.finishListener = finishListener;
			return this;
		}

		public Builder setBoundingBoxCalculator(BoundingBoxCalculator boundingBoxCalculator) {
			building.boundingBoxCalculator = boundingBoxCalculator;
			return this;
		}

		public Builder setSignalQualitySupplier(Supplier<SignalQuality> supplier) {
			building.signalQualitySupplier = supplier;
			return this;
		}

		public Builder setTotalDistanceCalculator(
				QuantityUnivariateStatisticProcessorCalculator<Length, Sum> totalDistanceCalculator) {
			building.totalDistanceCalculator = totalDistanceCalculator;
			return this;
		}

		/**
		 * @param newMeanDistanceBetweenTrackpointsCalculator
		 * @return
		 */
		public
				Builder
				setMeanDistanceBetweenTrackpointsCalculator(
						QuantityUnivariateStatisticProcessorCalculator<Length, Mean> newMeanDistanceBetweenTrackpointsCalculator) {
			building.meanDistanceBetweenTrackpointsCalculator = newMeanDistanceBetweenTrackpointsCalculator;
			return this;
		}

		public Builder setMeanDurationBetweenTrackpointsCalculator(
				DurationUnivariateStatisticProcessorCalculator<Mean> calculator) {
			building.meanDurationBetweenTrackpointsCalculator = calculator;
			return this;
		}

		public Builder setElevationGainCalculator(
				QuantityUnivariateStatisticProcessorCalculator<ElevationChange, Gain> elevationGainCalculator) {
			building.elevationGainCalculator = elevationGainCalculator;
			return this;
		}

		public Builder setGeoCellProvider(GeoCellProvider provider) {
			building.geoCellProvider = provider;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.ProcessorListenerBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.counter);
			Assertion.assertNotNull(building.startListener);
			Assertion.assertNotNull(building.finishListener);
		}

		/**
		 * @param documentId
		 * @return
		 */
		public Builder setDocumentId(ArchivedDocumentId documentId) {
			building.documentId = documentId;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct TrackSummaryCreatorListener */
	private TrackSummaryCreatorListener() {
	}

}
