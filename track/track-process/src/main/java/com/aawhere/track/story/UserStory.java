/**
 *
 */
package com.aawhere.track.story;

/**
 * TODO:move this to a general project since track processing isn't the only story.
 * 
 * TODO:remove {@link #process()} and require processing to be completed by the builder.
 * 
 * @author brian
 * 
 */
public interface UserStory<ResultT> {

	ResultT getResult();

	/**
	 * Determines if a result is available. This method must be called before {@link #getResult()}.
	 * 
	 * @return
	 */
	Boolean hasResult();

	/**
	 * Does the processing to produce the result.
	 */
	void process();

}