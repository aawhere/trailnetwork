/**
 * 
 */
package com.aawhere.track.process.provider;

import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.track.GeoCalculator;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentCalculatedQuantityProvider;

/**
 * Uses the {@link GeoCalculator} to determine the distance of the given segment. The result is
 * provided via {@link #getQuantity()}.
 * 
 * @author roller
 * 
 */
public class TrackSegmentDistanceProvider
		extends ProcessorListenerBase
		implements TrackSegmentCalculatedQuantityProvider<Length> {

	/**
	 * Used to construct all instances of TrackSegmentDistanceProvider.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackSegmentDistanceProvider, Builder> {

		public Builder() {
			super(new TrackSegmentDistanceProvider());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrackSegmentDistanceProvider */
	private TrackSegmentDistanceProvider() {
	}

	/**
	 * Initialize to zero since it can be considered that the track has traveled no distance until a
	 * segment has been completed. Useful to avoid return null.
	 */
	private Length segmentLength;

	/*
	 * (non-Javadoc)
	 * @seecom.aawhere.track.process.calc.SegmentMeasurementCalculator# getSegmentQuantity()
	 */
	@Override
	public Length getQuantity() {
		return Assertion.assertNotNull(segmentLength);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.calc.ProcessorQuantityProvider#hasQuantity()
	 */
	@Override
	public Boolean hasQuantity() {
		return segmentLength != null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track .TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		this.segmentLength = segment.getDistance();
	}

}
