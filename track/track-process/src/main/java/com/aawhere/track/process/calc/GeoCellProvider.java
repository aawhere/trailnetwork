package com.aawhere.track.process.calc;

import java.util.TreeSet;

import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

/**
 * TODO: this needs to be split into a Provider and a calculator. Actually a generic collecting
 * calculator could be used to provide the arraylist
 * 
 * Provides a list of GeoCells the track goes through in order of processing. GeoCells are
 * calculated based on the TARGET_RESOLUTION variable.
 * 
 * One limitation is that this implementation does not interpolate between trackpoints. So if the
 * trackpoints are spread apart by a distance greater than 2*widthOfGeoCellAtTargetResolution, then
 * it could be possible that two subsequent geocells won't share an edge (forming gaps).
 * 
 * @author Brian Chapman
 * 
 */
public final class GeoCellProvider
		extends ProcessorListenerBase
		implements TrackpointListener {

	/**
	 * @return
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<GeoCellProvider, Builder> {

		/**
		 * @param building
		 */
		public Builder() {
			super(new GeoCellProvider());
		}

		public Builder setTargetResolution(Resolution targetResolution) {
			building.targetResolution = targetResolution;
			return this;
		}

		public Builder setBufferLength(Length bufferLength) {
			building.bufferLength = bufferLength;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.ProcessorListenerBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("bufferLength", building.bufferLength);

		}

	}

	private GeoCellProvider() {
	};

	private TreeSet<GeoCell> geoCells = new TreeSet<GeoCell>();
	private Length bufferLength;
	private Resolution targetResolution;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track .TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint point, TrackProcessor processor) {

		GeoCells geoCells = GeoCellUtil.geoCells(point.getLocation(), targetResolution, this.bufferLength);
		this.geoCells.addAll(geoCells.getAll());
	}

	/**
	 * Returns a list of Geocells at the target resolution in order of the track log. Does not
	 * provide {@link GeoCell}s for all resolutions.
	 * 
	 * @return
	 */
	public GeoCells getGeoCells() {
		return GeoCells.create().setAll(geoCells).build();
	}

	/**
	 * Returns a set of Geocells at the target resolution and below.
	 * 
	 * @return
	 */
	public GeoCells getGeoCellsAtAllResolutions() {
		return GeoCells.create().addAll(GeoCellUtil.translateToAllResolutions(geoCells)).build();
	}

}
