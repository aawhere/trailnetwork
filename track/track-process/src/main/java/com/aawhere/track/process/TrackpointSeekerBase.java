/**
 * 
 */
package com.aawhere.track.process;

import com.aawhere.lang.Assertion;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;

/**
 * Supplies the common needs of a {@link TrackpointSeeker}.
 * 
 * @author roller
 * 
 */
abstract public class TrackpointSeekerBase
		extends ProcessorListenerBase
		implements TrackpointSeeker, UnfinishedProcessorListener {

	public static class Builder<BuilderResult extends TrackpointSeekerBase, BuilderT extends Builder<BuilderResult, BuilderT>>
			extends ProcessorListenerBase.Builder<BuilderResult, BuilderT> {

		/**
		 * @param building
		 *            children are required to produce their own result
		 */
		protected Builder(BuilderResult building) {
			super(building);
		}

		public BuilderT setReceiver(TrackpointSeekingProcessor receiver) {
			building.setReceiver(receiver);
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("receiver", building.getReceiver());
		}

	}

	/**
	 * The recipient of the trackpoints being sought when they are discovered.
	 * 
	 */
	private TrackpointSeekingProcessor receiver;

	protected void desiredTrackpointFound(Trackpoint trackpoint) {
		receiver.desiredTrackpointFound(trackpoint);
	}

	/**
	 * @return the receiver
	 */
	public TrackpointSeekingProcessor getReceiver() {
		return receiver;
	}

	void setReceiver(TrackpointSeekingProcessor receiver) {
		this.receiver = receiver;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.receiver == null) ? 0 : this.receiver.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof TrackpointSeekerBase))
			return false;
		TrackpointSeekerBase other = (TrackpointSeekerBase) obj;
		if (this.receiver == null) {
			if (other.receiver != null)
				return false;
		} else if (!this.receiver.equals(other.receiver))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.UnfinishedProcessorListener#finish()
	 */
	@Override
	public void finish() {
		getReceiver().finish();
	}
}
