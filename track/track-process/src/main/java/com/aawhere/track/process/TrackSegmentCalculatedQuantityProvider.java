/**
 * 
 */
package com.aawhere.track.process;

import javax.measure.quantity.Quantity;

/**
 * Specifically calculates a {@link Measurement} or {@link Quantity} for a segment.
 * 
 * 
 * @author roller
 * 
 */
public interface TrackSegmentCalculatedQuantityProvider<QuantityT extends Quantity<QuantityT>>
		extends TrackSegmentListener, ProcessorListenerQuantityProvider<QuantityT> {

	public QuantityT getQuantity();
}
