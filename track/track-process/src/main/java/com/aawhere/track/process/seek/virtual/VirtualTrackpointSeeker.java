/**
 * 
 */
package com.aawhere.track.process.seek.virtual;

import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeekerBase;

/**
 * Common seeker that will produce virtual trackpoints when that which is sought does not land
 * exactly on existing points. A "virtual" point is created by interpolating from two closest actual
 * points.
 * 
 * @author roller
 * 
 */
public abstract class VirtualTrackpointSeeker
		extends TrackpointSeekerBase
		implements TrackpointListener {

	public static abstract class Builder<BuilderResult extends VirtualTrackpointSeeker, BuilderT extends Builder<BuilderResult, BuilderT>>
			extends TrackpointSeekerBase.Builder<BuilderResult, BuilderT> {
		protected Builder(BuilderResult seeker) {
			super(seeker);
		}

		/**
		 * @see #includeOriginals
		 * 
		 * @param includeOriginals
		 * @return
		 */
		public BuilderT setIncludeOriginals(Boolean matchedTimesOnly) {
			building.setIncludeOriginals(matchedTimesOnly);
			return dis;
		}
	}

	/**
	 * Mandates to provide virtual trackpoints matching only those of the source times or pass
	 * through original points in addition to the virtual points.
	 * 
	 * Setting to true will match the source times exactly, but will lose valuable granularity from
	 * the target track.
	 */
	private Boolean includeOriginals = false;

	/**
	 * 
	 */
	protected VirtualTrackpointSeeker() {
	}

	/**
	 * Processes trackpoints handling only those common items available in all virtual trackpoint
	 * seekers.
	 * 
	 * @see #includeOriginals
	 * 
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 *      com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {

		// pass through the original if so desired
		if (this.includeOriginals) {
			super.desiredTrackpointFound(trackpoint);
		}
	}

	void setIncludeOriginals(Boolean includeOriginals) {
		this.includeOriginals = includeOriginals;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.includeOriginals == null) ? 0 : this.includeOriginals.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof VirtualTrackpointSeeker))
			return false;
		VirtualTrackpointSeeker other = (VirtualTrackpointSeeker) obj;
		if (this.includeOriginals == null) {
			if (other.includeOriginals != null)
				return false;
		} else if (!this.includeOriginals.equals(other.includeOriginals))
			return false;
		return true;
	}

}
