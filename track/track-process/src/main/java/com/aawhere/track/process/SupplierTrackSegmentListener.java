package com.aawhere.track.process;

import java.util.LinkedList;
import java.util.List;

import com.aawhere.track.TrackSegment;
import com.google.common.base.Supplier;

/**
 * Records every value provided from a {@link Supplier}, presumably who is also a
 * {@link TrackpointListener} or {@link TrackSegmentListener}.
 * 
 * @author aroller
 * 
 */
public class SupplierTrackSegmentListener<E>
		extends ProcessorListenerBase
		implements TrackSegmentListener, Supplier<List<E>> {

	/**
	 * Used to construct all instances of SupplierTrackSegmentListener.
	 */
	public static class Builder<E>
			extends ProcessorListenerBase.Builder<SupplierTrackSegmentListener<E>, Builder<E>> {

		public Builder() {
			super(new SupplierTrackSegmentListener<E>());
		}

		public Builder<E> supplier(Supplier<E> supplier) {
			building.supplier = supplier;
			return this;
		}

		@Override
		public SupplierTrackSegmentListener<E> build() {
			SupplierTrackSegmentListener<E> built = super.build();
			return built;
		}

	}// end Builder

	public static <E> Builder<E> create() {
		return new Builder<E>();
	}

	/** Use {@link Builder} to construct SupplierTrackSegmentListener */
	private SupplierTrackSegmentListener() {
	}

	private LinkedList<E> values = new LinkedList<>();
	private Supplier<E> supplier;

	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		values.add(supplier.get());
	}

	@Override
	public List<E> get() {
		return values;
	}

}
