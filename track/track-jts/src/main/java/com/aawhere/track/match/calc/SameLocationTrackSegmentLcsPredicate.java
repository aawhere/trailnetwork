/**
 *
 */
package com.aawhere.track.match.calc;

import javax.measure.quantity.Length;
import javax.measure.unit.USCustomarySystem;

import com.aawhere.jts.geom.JtsUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.JtsMeasureUtil;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.TrackSegment;

import com.vividsolutions.jts.geom.Geometry;

/**
 * Determines if two {@link TrackSegment}s are at the same location. Two {@link TrackSegment}s are
 * considered in the same location of the shortest distance between the lines made from the two
 * segments is less than or equal to the distanceToBeConsideredTogether.
 * 
 * @author Brian Chapman
 * 
 */
public class SameLocationTrackSegmentLcsPredicate
		implements LcsPredicate {

	private Length distanceToBeConsideredTogether;

	private SameLocationTrackSegmentLcsPredicate() {
	}

	@Override
	public Boolean match(TrackSegment source, TrackSegment target) {
		double maxDistance = JtsMeasureUtil.angleFromLength(distanceToBeConsideredTogether,
															source.getFirst().getLocation().getLatitude())
				.doubleValue(USCustomarySystem.DEGREE_ANGLE);
		Geometry sourceGeo = JtsTrackGeomUtil.convertToMultiLineString(source);
		Geometry targetGeo = JtsTrackGeomUtil.convertToMultiLineString(target);
		double minDistance = JtsUtil.shortestDistanceBetween(sourceGeo, targetGeo);
		return (minDistance <= maxDistance);
	}

	public static class Builder
			extends ObjectBuilder<SameLocationTrackSegmentLcsPredicate> {
		public Builder() {
			super(new SameLocationTrackSegmentLcsPredicate());
		}

		public Builder setSameLocationDistance(Length distance) {
			building.distanceToBeConsideredTogether = distance;
			return this;
		}

		@Override
		public void validate() {
			Assertion.assertNotNull("distanceToBeConsideredTogether", building.distanceToBeConsideredTogether);
		}
	}

	public static Builder create() {
		return new Builder();
	}

}
