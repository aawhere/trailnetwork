/**
 * 
 */
package com.aawhere.track;

import java.io.Serializable;
import java.util.ArrayList;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.calc.TrackToMultiLineStringConverter;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LineString;

/**
 * Represents a continuous signal in a track log that JTS can understand. A break in the GPS signal
 * should create a new sequence since each trackpoint in a sequence must have x/y values.
 * 
 * Intended to be used to populate a {@link LineString} in the most efficient manner, this will
 * create a {@link CoordinateSequence} of {@link Trackpoint}s being used with a
 * {@link TrackpointListener} such as {@link TrackToMultiLineStringConverter}. The resulting seqence
 * must not have null location. In the case of a null location a new Sequence should be created when
 * the locations start again.
 * 
 * Other coordinate sequences don't support growing one coordinate at a time (using our builder) and
 * this also ensures non-mutable.
 * 
 * TODO: if we need more efficiency we can avoid converting to coordinates and leave as trackpoints
 * if only the getX and getY methods are called. If the getCoordinate method is called...well this
 * is most efficient
 * 
 * @author aroller
 * 
 */
public class TrackCoordinateSequence
		implements CoordinateSequence, Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	public static final int INITIAL_CAPACITY = 50;
	private ArrayList<Coordinate> trackpoints = new ArrayList<Coordinate>(INITIAL_CAPACITY);
	private Envelope envelope = new Envelope();

	/**
	 * Used to construct all instances of TrackCoordinateSequence.
	 */
	public static class Builder
			extends ObjectBuilder<TrackCoordinateSequence> {

		public Builder() {
			super(new TrackCoordinateSequence());
		}

		public Builder add(Trackpoint trackpoint) {
			Coordinate coord = JtsTrackGeomUtil.convert(trackpoint);
			building.trackpoints.add(coord);
			building.envelope.expandToInclude(coord);
			return this;
		}

		public Builder addAll(Iterable<Trackpoint> all) {
			for (Trackpoint trackpoint : all) {
				add(trackpoint);
			}
			return this;
		}

		private Builder clone(TrackCoordinateSequence other) {
			building.trackpoints = new ArrayList<Coordinate>(other.trackpoints);
			building.envelope = new Envelope(other.envelope);
			return this;
		}

		/**
		 * provided as efficient way of checking size without building. Small sequences may be
		 * aborted.
		 * 
		 * @return
		 */
		public int size() {
			return building.trackpoints.size();
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrackCoordinateSequence */
	private TrackCoordinateSequence() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#getCoordinate(int)
	 */
	@Override
	public Coordinate getCoordinate(int i) {

		return trackpoints.get(i);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#getCoordinateCopy(int)
	 */
	@Override
	public Coordinate getCoordinateCopy(int i) {
		return new Coordinate(trackpoints.get(i));
	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#getCoordinate(int,
	 * com.vividsolutions.jts.geom.Coordinate)
	 */
	@Override
	public void getCoordinate(int index, Coordinate theirs) {
		Coordinate ours = this.trackpoints.get(index);
		theirs.x = ours.x;
		theirs.y = ours.y;

	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#getX(int)
	 */
	@Override
	public double getX(int index) {

		return trackpoints.get(index).x;
	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#getY(int)
	 */
	@Override
	public double getY(int index) {

		return trackpoints.get(index).y;
	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#getOrdinate(int, int)
	 */
	@Override
	public double getOrdinate(int index, int ordinateIndex) {
		switch (ordinateIndex) {
			case CoordinateSequence.X:
				return this.trackpoints.get(index).x;
			case CoordinateSequence.Y:
				return this.trackpoints.get(index).y;
			case CoordinateSequence.Z:
				return this.trackpoints.get(index).z;
			default:
				throw new IllegalArgumentException(ordinateIndex + " is the ordinate you seek, but only 0-2 for x,y,z");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#size()
	 */
	@Override
	public int size() {
		return this.trackpoints.size();
	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#setOrdinate(int, int, double)
	 */
	@Override
	public void setOrdinate(int index, int ordinateIndex, double value) {
		throw new UnsupportedOperationException("we don't like mutable objects");

	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#toCoordinateArray()
	 */
	@Override
	public Coordinate[] toCoordinateArray() {
		return this.trackpoints.toArray(new Coordinate[this.trackpoints.size()]);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.vividsolutions.jts.geom.CoordinateSequence#expandEnvelope(com.vividsolutions.jts.geom
	 * .Envelope)
	 */
	@Override
	public Envelope expandEnvelope(Envelope env) {

		env.expandToInclude(envelope);
		return env;
	}

	/*
	 * (non-Javadoc)
	 * @see com.vividsolutions.jts.geom.CoordinateSequence#getDimension()
	 */
	@Override
	public int getDimension() {
		return 3;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		return create().clone(this).build();
	}

}
