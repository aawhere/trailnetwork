/**
 * 
 */
package com.aawhere.track.process.calc;

import java.util.ArrayList;

import javax.measure.quantity.Length;

import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.TrackCoordinateSequence;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.UnfinishedProcessorListener;

import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

/**
 * Iterates through each of the segments of a track producing a {@link LineString} for each segment.
 * 
 * Segments without location are not included, but will cause surrounding segment sequences with
 * location to be isolated in their own LineString.
 * 
 * If a {@link #minDistanceBetweenTrackpoints} is provided then the {@link DouglasPeuckerSimplifier}
 * will simplify each trackpoint.
 * 
 * @author aroller
 * 
 */
public class TrackToMultiLineStringConverter
		extends ProcessorListenerBase
		implements TrackpointListener, UnfinishedProcessorListener {

	private ArrayList<LineString> lineStrings = new ArrayList<LineString>(500);
	private TrackCoordinateSequence.Builder sequence = new TrackCoordinateSequence.Builder();
	// even though we are using our own sequences we don't need a custom sequence factory since we
	// don't want to create them, but just make LineStrings efficiently.
	private GeometryFactory factory = new GeometryFactory();
	private MultiLineString multiLineString;
	/** Used to simplify the linestring. If not provided there will be no simplification. */
	public Length minDistanceBetweenTrackpoints;
	private Double simplificationArcLength;

	/**
	 * Used to construct all instances of TrackToMultiLineStringConverter.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackToMultiLineStringConverter, Builder> {

		public Builder() {
			super(new TrackToMultiLineStringConverter());
		}

		public Builder setMinDistanceBetweenTrackpoints(Length length) {
			building.minDistanceBetweenTrackpoints = length;
			return this;
		}

		public Builder setDefaultMinDistanceBetweenTrackpoints() {
			setMinDistanceBetweenTrackpoints(JtsTrackGeomUtil.similificationDistanceTolorance);
			return this;
		}

		public Builder setGeometryFactory(GeometryFactory factory) {
			building.factory = factory;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrackToMultiLineStringConverter */
	private TrackToMultiLineStringConverter() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		if (trackpoint.isLocationCapable()) {
			sequence.add(trackpoint);
		} else {
			nextSequence();
		}
	}

	/**
	 * This will return null until {@link #finish()} is called.
	 * 
	 * @return
	 */
	public MultiLineString getMultiLineString() {

		return this.multiLineString;
	}

	/**
	 * 
	 */
	private void nextSequence() {
		if (sequence.size() > 1) {

			TrackCoordinateSequence builtSequence = sequence.build();

			LineString lineString = factory.createLineString(builtSequence);
			// Simplify if so chosen
			if (this.minDistanceBetweenTrackpoints != null) {
				if (this.simplificationArcLength == null) {
					this.simplificationArcLength = JtsTrackGeomUtil.arcLength(	minDistanceBetweenTrackpoints,
																				builtSequence.getCoordinate(0));
				}
				lineString = (LineString) DouglasPeuckerSimplifier.simplify(lineString, this.simplificationArcLength);
			}
			lineStrings.add(lineString);
		}
		sequence = TrackCoordinateSequence.create();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.UnfinishedProcessorListener#finish()
	 */
	@Override
	public void finish() {
		nextSequence();
		this.multiLineString = factory.createMultiLineString(this.lineStrings.toArray(new LineString[this.lineStrings
				.size()]));
		if (this.simplificationArcLength != null) {
			LineString simplify = (LineString) DouglasPeuckerSimplifier.simplify(	this.multiLineString,
																					this.simplificationArcLength);
			this.multiLineString = factory.createMultiLineString(new LineString[] { simplify });
		}
		this.sequence = null;
		this.lineStrings = null;
	}
}
