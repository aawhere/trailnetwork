/**
 * 
 */
package com.aawhere.track.match;

import java.util.ArrayList;
import java.util.List;

import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.Track;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.UnfinishedProcessorListener;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.story.TrackSummaryUserStories;
import com.aawhere.xml.XmlNamespace;

/**
 * Given the summary of another track with a high relation to the one being processed, this will
 * detect the start and finish of the track. Since an Attempt may complete a given course multiple
 * times this will keep record each {@link TimingResult}.
 * 
 * Using the CourseAttemptDeviationProcessor the course has a buffer that the attempt must fall
 * within. If the attempt deviates outside the buffer it is only allowed to do so for the max
 * allowed deviation before the attempt is considered a failure. The course and the attempt leap
 * frog iterating through both tracks until the end of the course is reached and all results are
 * logged.
 * 
 * 
 * DONE:support more than one timing relation per track. TN-248
 * 
 * DONE: now we detect the starting point to be interpolated and matching the course's starting
 * point exactly TN-163
 * 
 * 
 * @author aroller
 * 
 */
public class TrackTimingRelationCalculator
		extends ProcessorListenerBase
		implements TrackSegmentListener, UnfinishedProcessorListener {

	/**
	 * Indicates the reason a timing may or may not have been completed.
	 * 
	 * TODO:these should become Messages explainable to the API.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlEnum
	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static enum TimingCompletionCategory {
		/** The ideal for a successful race. */
		FINISH_LINE_REACHED,
		/** If a limit of deviation has been reached and processing aborted. */
		TOO_MUCH_DEVIATION,

		/**
		 * Indicates that the start line was never crossed so the course is not completed.
		 */
		DID_NOT_START,
		/**
		 * When there is no more attempt points coming, but the finish hasn't yet completed.
		 */
		DID_NOT_FINISH,
		/**
		 * The start had been reached, but a return to the start before the finish line means let's
		 * log another race.
		 */
		FALSE_START,
		/**
		 * Indicates that the course requested is too short to be considered for comparison against
		 * other activities. TN-278
		 */
		COURSE_TOO_SHORT,
		/**
		 * TN-476 Activities of significant length, but are in a single spot can be just noise,
		 * people working out indoors, track runs, etc. The granularity goes beyond the tolerance of
		 * the relation processor and will produce inaccurate results.
		 */
		COURSE_FOOTPRINT_TOO_SMALL, ATTEMPT_FOOTPRINT_TOO_SMALL,
		/**
		 * Indicator that the two tracks are the same duplicated so they should not compete against
		 * each other. It's pointless. TN-277
		 */
		IDENTICAL_TRACKS,
		/**
		 * NOT_SIMILAR_ENOUGH, but similarity found to be failing when checking the bounding boxes.
		 */
		OUT_OF_BOUNDS,
		/**
		 * Indicator that the attempt buffer does not fully contain the course buffer so it is
		 * unlikely to have enough similarity to avoid exceeding the deviation allowance.
		 */
		NOT_SIMILAR_ENOUGH,
		/**
		 * Indicates the attempt is not long enough to complete the course so no detailed analysis
		 * is necessary.
		 */
		ATTEMPT_TOO_SHORT,
		/**
		 * Indicates portion followed did not calculate enough distance to have completed the
		 * course. This is a useful late-term test if other tests are passing erroneously.
		 * 
		 */
		NOT_ENOUGH_COMPLETED,
		/**
		 * This indicates it is a failure, but the reason for failing is not known without
		 * re-processing.
		 * 
		 */
		EXISTING,
	};

	/**
	 * Used to construct all instances of TrackTimingRelationCalculator.
	 */
	@XmlTransient
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackTimingRelationCalculator, Builder> {

		public Builder() {
			super(new TrackTimingRelationCalculator());
		}

		protected Builder(TrackTimingRelationCalculator calculator) {
			super(calculator);
		}

		public Builder goalLineLength(Length goalLineLength) {
			building.goalLineLength = goalLineLength;
			return this;
		}

		public Builder setCourse(TrackSummary course) {
			building.course = course;
			return this;
		}

		public Builder setMaxDeviationLength(Length maxDeviationLength) {
			building.maxDeviationLength = maxDeviationLength;
			return this;
		}

		public Builder setBufferWidth(Length bufferWidth) {
			building.bufferWidth = bufferWidth;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull("course", building.course);
			Assertion.exceptions().notNull("bufferWidth", building.bufferWidth);
			Assertion.exceptions().notNull("maxDeviationLength", building.maxDeviationLength);
			Assertion.exceptions().notNull("goalLineLength", building.goalLineLength);

			super.validate();
		}

		/**
		
		 */
		@Override
		public TrackTimingRelationCalculator build() {

			TrackTimingRelationCalculator built = super.build();
			built.setupRaceProcessor();
			return built;
		}

		/**
		 * @param courseTrack
		 * @return
		 */
		public Builder setCourseTrack(Iterable<Trackpoint> courseTrack) {
			building.courseTrack = courseTrack;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private TrackSummary course;

	// an indicator if we have reached the start
	private boolean racing = false;
	/** The track used to verify the attempt track completed the course. */
	private Iterable<Trackpoint> courseTrack;
	// FIXME:this should be set

	private ManualTrackProcessor raceProcessor;
	private TrackSummaryUserStories trackSummaryUserStories;
	private Length bufferWidth;
	private Length maxDeviationLength;
	private Length goalLineLength;
	private ArrayList<TimingResult> results = new ArrayList<TimingResult>();
	private CourseAttemptDeviationProcessor deviationProcessor;
	private GoalPointSearcher startSearcher;
	private GoalPointSearcher falseStartSearcher;
	/**
	 * False starts happen when lingering around the starting line, but once on their way the false
	 * start must stop being sought after or else we have problems detecting the finish as seen in
	 * TN-283 and TN-284.
	 */
	private boolean startLocked = false;
	private GoalPointSearcher finishSearcher;

	private Trackpoint previousRecordedPoint;

	/** Use {@link Builder} to construct TrackTimingRelationCalculator */
	protected TrackTimingRelationCalculator() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.TrackSegmentListener#handle(com.aawhere.track.TrackSegment,
	 * com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		// put in some protection to stop processing if deviation length is exceeded.
		if (this.deviationProcessor.isDeviationLengthExceeded()) {
			racingFinished(false, TimingCompletionCategory.TOO_MUCH_DEVIATION);
		} else {

			// look for finish if start has been found
			if (racing) {
				// sometimes a attempt returns to the start. If so we must log and respect it as
				// the start of the race
				// a false start searcher is created once we leave the area after starting
				if (!startLocked) {

					if (this.falseStartSearcher == null) {
						this.startSearcher.search(segment);
						if (!this.startSearcher.isCurrentlyInArea()) {
							this.falseStartSearcher = createStartSearcher(1);
						}
					} else {
						if (this.falseStartSearcher.search(segment)) {
							racingFinished(false, TimingCompletionCategory.FALSE_START);
							// recursive call so the real start searcher will get a chance.
							handle(segment, processor);
						} else {
							// once we get far enough away from the start it's time to give up on
							// false start searched
							Length distanceFromGoal = TrackUtil.distanceBetween(this.startSearcher.getGoal(),
																				segment.getSecond());
							final int factorForFalseStartSearchArea = 3;
							this.startLocked = QuantityMath.create(goalLineLength).times(factorForFalseStartSearchArea)
									.lessThan(distanceFromGoal);
						}
					}

				} else

				// if near the finish detection is simplified if course buffer is forced to catch up
				// to deviation immediately. TN-289
				if (this.finishSearcher.isCurrentlyInArea()
						&& QuantityMath.create(this.deviationProcessor.getLengthOfCurrentDeviation()).positive()) {

					this.deviationProcessor.requestNextCourseBuffer();
				}
				if (!this.finishSearcher.hasGoalLine() && this.deviationProcessor.endOfCourseReached()) {
					SlidingBufferTrackpointSeeker courseBufferSeeker = this.deviationProcessor.getCourseBufferSeeker();

					// this will keep it's current state which is on purpose in case it has already
					// reached the area.
					this.finishSearcher = GoalPointSearcher.mutate(finishSearcher)
							.setSegmentArrivingAtFinish(courseBufferSeeker.getFinishSegment())
							.setGoalAreaRadius(goalLineLength).build();
				}

				// if the end is reached, mark the finish and stop the race
				// not finished if the course isn't completed.
				// some courses reach the finish line multiple times. Think race course loops
				if (finishSearcher.search(segment) && this.deviationProcessor.endOfCourseReached()) {
					finishPointFound();
				} else {
					// pass on the segment for the race
					recordRacingTrackpoint(segment.getSecond());
				}

			}
			// Only determine start closeness if the finish has not been met
			else if (this.startSearcher.search(segment)) {
				this.racing = true;

				// the race starts at the starting line...the start is the first point, not the
				// actual trackpoint that starts processing

				Trackpoint trackpoint = startSearcher.getBest();
				recordRacingTrackpoint(trackpoint);
			}

		}

	}

	/**
	 * @param trackpoint
	 */
	private void recordRacingTrackpoint(Trackpoint trackpoint) {
		// avoiding adding a zero duration segment
		if (this.previousRecordedPoint == null
				|| trackpoint.getTimestamp().isAfter(this.previousRecordedPoint.getTimestamp())) {
			this.raceProcessor.notifyTrackpointListeners(trackpoint);
			this.previousRecordedPoint = trackpoint;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.UnfinishedProcessorListener#finish()
	 */
	@Override
	public void finish() {
		if (racing) {
			this.finishSearcher.finish();
			if (this.finishSearcher.isGoalReached()) {
				finishPointFound();
			} else {
				racingFinished(false, TimingCompletionCategory.DID_NOT_FINISH);
			}
		} else {
			// technically not racing, but worth logging the final
			racingFinished(false, TimingCompletionCategory.DID_NOT_START);
		}
	}

	/**
	 * 
	 */
	private void finishPointFound() {
		Trackpoint finish = this.finishSearcher.getBest();
		recordRacingTrackpoint(finish);

		racingFinished(true, TimingCompletionCategory.FINISH_LINE_REACHED);
	}

	/**
	 * used to process race results after a start has been detected, but not necessarily a finish.
	 * This just means the current race is over due to a DNF, false start, or too much deviation.
	 * TODO:provide the reason for finish
	 * 
	 */
	private void racingFinished(boolean raceCompleted, TimingCompletionCategory reason) {
		this.raceProcessor.finish();
		// last check after the processor is finished
		if (this.deviationProcessor.isDeviationLengthExceeded()) {
			reason = TimingCompletionCategory.TOO_MUCH_DEVIATION;
			raceCompleted = false;
		}
		if (raceCompleted) {
			if (!this.deviationProcessor.endOfCourseReached()) {
				raceCompleted = false;
				reason = TimingCompletionCategory.DID_NOT_FINISH;
			}
		}

		TimingResult result = new TimingResult();
		// no track summary means not enough points to matter.
		if (this.trackSummaryUserStories.hasResult()) {
			result.summary = this.trackSummaryUserStories.getResult();

			// final check...was enough distance cumulated? TN-395
			if (raceCompleted) {
				final double courseLengthFudgeFactor = 0.9;
				if (QuantityMath.create(course.getDistance()).times(courseLengthFudgeFactor)
						.greaterThan(result.summary.getDistance())) {
					raceCompleted = false;
					reason = TimingCompletionCategory.NOT_ENOUGH_COMPLETED;
				}
			}
		} else if (raceCompleted) {
			// TN-440 caught this bad finish detection
			raceCompleted = false;
			reason = TimingCompletionCategory.NOT_ENOUGH_COMPLETED;
		}

		result.category = reason;
		result.startSearcher = this.startSearcher;
		result.finishSearcher = this.finishSearcher;
		result.raceCompleted = raceCompleted;
		result.deviations = this.deviationProcessor.getDeviations();
		addResult(result);
		setupRaceProcessor();
	}

	/**
	 * Simply adds a result to the list of results.
	 * 
	 * This is available to extensions and friends to allow notifications to this event.
	 * 
	 * @param result
	 */
	protected void addResult(TimingResult result) {
		this.results.add(result);
	}

	/**
	 * Prepares the Course Processor that will be used to verify the course is on track. This has a
	 * manual track processor that will receive the paritcipation points and compare it to the
	 * already build course processor.
	 * 
	 * This also builds the {@link #startLine} so start detection can be determined.
	 * 
	 */
	private void setupRaceProcessor() {
		ManualTrackProcessor.Builder builder = ManualTrackProcessor.create();
		this.deviationProcessor = CourseAttemptDeviationProcessor.create().setBufferWidth(bufferWidth)
				.setAllowedDeviationLength(maxDeviationLength).setCourse(courseTrack).registerWithProcessor(builder)
				.build();
		this.trackSummaryUserStories = TrackSummaryUserStories.create().setProcessorBuilder(builder).withoutSpatial()
				.build();
		configureRaceProcessor(builder, this.deviationProcessor);
		this.raceProcessor = builder.build();
		this.racing = false;
		this.startSearcher = createStartSearcher();
		// finish line segment not yet known
		this.finishSearcher = GoalPointSearcher.create().setGoal(this.course.getFinish())
				.setGoalAreaRadius(goalLineLength).build();
		this.falseStartSearcher = null;
		this.startLocked = false;
		this.previousRecordedPoint = null;
	}

	/**
	 * @return
	 */
	private GoalPointSearcher createStartSearcher() {
		return createStartSearcher(1);
	}

	private GoalPointSearcher createStartSearcher(double areaFactor) {
		Length areaRadius = QuantityMath.create(goalLineLength).times(areaFactor).getQuantity();
		TrackSegment goalSegment = this.deviationProcessor.getCourseBufferSeeker().getStartSegment();
		GoalPointSearcher searcher = GoalPointSearcher.create().setSegmentLeavingStart(goalSegment)
				.setGoalAreaRadius(areaRadius).build();
		return searcher;
	}

	/**
	 * Provides the ability for extensions to configure the {@link #raceProcessor} while building.
	 * 
	 * @param builder
	 * 
	 */
	protected void configureRaceProcessor(ManualTrackProcessor.Builder builder,
			CourseAttemptDeviationProcessor deviationProcessor) {
		// do nothing so extensions can't change this classes behavior
	}

	/**
	 * Racing happens after crossing the start, but before reaching the finish.
	 * 
	 * @return the racing
	 */
	Boolean racing() {
		return this.racing;
	}

	/** Indicates that a start/finish have been found. */
	public Boolean raceCompleted() {
		return !this.results.isEmpty();
	}

	public List<TimingResult> getRaceResults() {
		return this.results;
	}

	public static class TimingResult {

		/**
		 * Track log segments of the attempt track that were not within reasonable distance to the
		 * course.
		 */
		private List<Track> deviations;
		/** The totals and averages calculated of the attempt during the race. */
		private TrackSummary summary;
		/**
		 * Indicates the race start and finish have been crossed, but no determination regarding the
		 * deviations has been made yet.
		 */
		private Boolean raceCompleted;

		private TimingCompletionCategory category;

		private GoalPointSearcher startSearcher;
		private GoalPointSearcher finishSearcher;

		/**
		 * @return the summary
		 */
		public TrackSummary getSummary() {
			return this.summary;
		}

		public Boolean raceCompleted() {
			return this.raceCompleted;
		}

		/**
		 * @return the deviations
		 */
		public List<Track> getDeviations() {
			return this.deviations;
		}

		/**
		 * @return the reason
		 */
		public TimingCompletionCategory getReason() {
			return this.category;
		}

		/**
		 * @return the startSearcher
		 */
		public GoalPointSearcher getStartSearcher() {
			return this.startSearcher;
		}

		/**
		 * @return the finishSearcher
		 */
		public GoalPointSearcher getFinishSearcher() {
			return this.finishSearcher;
		}
	}

}
