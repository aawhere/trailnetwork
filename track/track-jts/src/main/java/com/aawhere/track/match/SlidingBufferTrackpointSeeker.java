/**
 * 
 */
package com.aawhere.track.match;

import java.util.ArrayList;

import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackSegmentListener;
import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.TrackpointSeekerBase;
import com.aawhere.track.process.UnfinishedProcessorListener;
import com.aawhere.track.process.processor.SemiAutomaticTrackProcessor;
import com.aawhere.track.process.processor.TrackpointSeekingProcessor;

import com.google.common.collect.Iterables;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

/**
 * Given {@link Trackpoint}s of the "source" track of interest, this will create a
 * {@link LineString} of approximately {@link #minLength}. When the length of the line has reached
 * {@link #bufferLengthReached()} the buffer is ready for a client to use. A comparison can be made
 * of a point to see if it is within the {@link #bufferWidth} away from the line.
 * 
 * Since it will likely take several segments to achieve the max length, this is a
 * {@link TrackpointSeeker} that receives iterating trackpoints until the buffer length is reached.
 * When the buffer is at max length, this will notify the {@link TrackpointSeekingProcessor} that
 * the buffer is ready.
 * 
 * When the buffer is already full and this receives another trackpoint this will
 * {@link #resetBuffer()} by emptying the entire buffer and add the newly provided trackpoints from
 * the given segment starting new buffer sequence. This results in a buffer that slides along the
 * given track incrementing approximately the desired length.
 * 
 * This seeker works best with the {@link SemiAutomaticTrackProcessor} which is the provider of the
 * trackpoints, but also the {@link #getReceiver()}. This notifies the processor that the buffer has
 * been reached which will pause the iteration until an external controller decides to
 * {@link SemiAutomaticTrackProcessor#resume()}.
 * 
 * 
 * @see SemiAutomaticTrackProcessor
 * 
 * @author aroller
 * 
 */
public class SlidingBufferTrackpointSeeker
		extends TrackpointSeekerBase
		implements TrackSegmentListener, TrackpointSeeker, UnfinishedProcessorListener {

	/**
	 * Used to construct all instances of SlidingBufferTrackpointSeeker.
	 */
	public static class Builder
			extends TrackpointSeekerBase.Builder<SlidingBufferTrackpointSeeker, Builder> {

		public Builder() {
			super(new SlidingBufferTrackpointSeeker());
		}

		/**
		 * Indicates how long the buffer length must be before notifying the receiver.
		 * 
		 * @param minBufferLength
		 * @return
		 */
		public Builder setMinBufferLength(Length minBufferLength) {
			building.minLength = minBufferLength;
			return this;
		}

		public Builder setBufferWidth(Length bufferWidth) {
			building.bufferWidth = bufferWidth;

			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.track.process.TrackpointSeekerBase.Builder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("BUFFER_WIDTH", building.bufferWidth);
			Assertion.exceptions().notNull("minLength", building.minLength);
			Assertion.exceptions().assertTrue(	"must be greater than 0, but was " + building.minLength,
												QuantityMath.create(building.minLength).positive());

		}

		public SlidingBufferTrackpointSeeker build() {
			SlidingBufferTrackpointSeeker built = super.build();
			built.resetBuffer();

			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** The trackpoints that generates the current buffer. */
	private ArrayList<Trackpoint> trackpoints;
	/**
	 * Used to determine if the length is exceeded by always having the latest length as calculated
	 * from all the receiving segments.
	 */
	private QuantityMath<Length> lengthOfBuffer;
	/** The minimum length of the buffer before notification */
	private Length minLength;
	/**
	 * The length of the JTS buffer. I think this is the total length the buffer will be with half
	 * of the length being on both sides of the line.
	 */
	private Length bufferWidth;
	private double bufferWidthArcLength;

	/** Provided to external users so they can understand the beginning of the track provided. */
	private TrackSegment startSegment;
	/**
	 * Provided to external users so they can understand where the track ends. (once the end is
	 * reached)
	 */
	private TrackSegment finishSegment;
	/** Internally available helpful for processing. */
	private TrackSegment previousSegment;
	private Geometry buffer;

	/** Use {@link Builder} to construct SlidingBufferTrackpointSeeker */
	private SlidingBufferTrackpointSeeker() {
	}

	/**
	 * Working with a fifo queue of buffers created for each segment that is received this will
	 * build a complete buffer from those segments. Once the width of the buffer reaches that of the
	 * desired {@link #minLength} this will call {@link #desiredTrackpointFound(Trackpoint)} to
	 * signal the buffer is ready.
	 */
	@Override
	public void handle(TrackSegment segment, TrackProcessor processor) {
		boolean segmentHasDistance = TrackUtil.isLocationCapable(segment)
				&& QuantityMath.create(segment.getDistance()).positive();
		// a segment without distance has no effect on the buffer since it is location-based.
		// this check especially avoids null pointers when location is missing.
		if (segmentHasDistance) {
			// give a start segment with distance for direction understanding
			if (this.startSegment == null) {
				this.startSegment = segment;
				// this only needs to be written once per processor based on the latitude.
				this.bufferWidthArcLength = JtsTrackGeomUtil.arcLength(this.bufferWidth, segment.getFirst());
			}
			// we check for this first since notification was done in a previous iteration
			// the details remained available for the client to use.
			if (bufferLengthReached()) {
				resetBuffer();
			}

			// if just starting out with a new buffer, the first point of the segment completes the
			// first segment
			if (trackpoints.isEmpty()) {
				trackpoints.add(segment.getFirst());
			}
			// always choose the second point. the first was retrieved by the previous segment (or
			// in
			// the preceding initialization statement block)
			this.trackpoints.add(segment.getSecond());
			this.lengthOfBuffer = this.lengthOfBuffer.plus(segment.getDistance());

			// notify receiver if the length is reached.
			if (bufferLengthReached()) {
				// prepare the buffer for retrieval
				updateBuffer();
				// don't reset yet. Allow clients to be notified and retrieve the current state.
				// length reached, time to signal the buffer is ready
				desiredTrackpointFound(segment.getSecond());
			}
			this.previousSegment = segment;
		}
	}

	/**
	 * Simple math check to see if the current known {@link #lengthOfBuffer} is greater than
	 * {@link #minLength}.
	 * 
	 * @return
	 */
	private Boolean bufferLengthReached() {
		return lengthOfBuffer.greaterThanEqualTo(minLength);
	}

	/**
	 * 
	 */
	private void resetBuffer() {
		this.trackpoints = new ArrayList<Trackpoint>();
		this.lengthOfBuffer = QuantityMath.create(MeasurementUtil.createLengthInMeters(0));
		this.buffer = null;
	}

	private void updateBuffer() {
		// this.buffer = JtsTrackGeomUtil.buffer(this.trackpoints, bufferWidth);

		// using multi line respects gaps in the GPS signal with worse performance that a simplified
		// string.
		this.buffer = JtsTrackGeomUtil.trackToMultiLineString(trackpoints, true);
	}

	/**
	 * Generates a JTS buffer representing the current set of trackpoints.
	 * 
	 * This is created with every call so repeat calls may cause performance issues.
	 * 
	 * @return the buffer
	 */
	public Geometry getBuffer() {

		return this.buffer;
	}

	/**
	 * Provides read-only access to the trackpoints currently in the queue that represent the
	 * buffer.
	 * 
	 * @return
	 */
	Iterable<Trackpoint> getBufferTrackpoints() {
		return Iterables.unmodifiableIterable(this.trackpoints);
	}

	/**
	 * Provides the test executed to see if a given {@link Trackpoint} is contained within the
	 * {@link SlidingBufferTrackpointSeeker#getBuffer()}.
	 * 
	 * @param trackpoint
	 * @return
	 */
	protected Boolean contains(Trackpoint trackpoint) {
		Point point = JtsTrackGeomUtil.convertToPoint(trackpoint);
		// boolean contains = buffer.contains(point);
		double distance = this.buffer.distance(point);

		boolean contains = distance < this.bufferWidthArcLength;
		return contains;
	}

	/**
	 * Useful to determine start lines of a course. Available only after the first segment has been
	 * received.
	 * 
	 * @return the startSegment
	 */
	@Nullable
	public TrackSegment getStartSegment() {
		return this.startSegment;
	}

	/**
	 * Useful to determine finish points of a course.
	 * 
	 * Available only after {@link #finish()} has been called by processer.
	 * 
	 * @return the finishSegment
	 */
	@Nullable
	public TrackSegment getFinishSegment() {
		return this.finishSegment;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.UnfinishedProcessorListener#finish()
	 */
	@Override
	public void finish() {
		updateBuffer();
		this.finishSegment = this.previousSegment;
	}

	/**
	 * Indicates to others that {@link #finish()} has been called and the end of the course has been
	 * reached.
	 * 
	 * @return
	 */
	public Boolean isFinished() {
		return this.finishSegment != null;
	}
}
