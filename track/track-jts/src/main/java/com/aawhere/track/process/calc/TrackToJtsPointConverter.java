/**
 *
 */
package com.aawhere.track.process.calc;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequenceFactory;

/**
 * Listens for a new trackpoint occurrence and builds the appropriate {@link CoordinateSequence} s.
 * 
 * @author roller
 * 
 */
public class TrackToJtsPointConverter
		extends ProcessorListenerBase
		implements TrackpointListener {

	/** Use {@link Builder} to construct TrackToJtsPointConverter */
	private TrackToJtsPointConverter() {
	}

	private ArrayList<Coordinate> coordinates = new ArrayList<Coordinate>();
	private ArrayList<CoordinateSequence> sequences = new ArrayList<CoordinateSequence>();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.inspect.TrackpointListener#handle(com.aawhere.track.Trackpoint)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		Coordinate coordinate = JtsTrackGeomUtil.convert(trackpoint);
		coordinates.add(coordinate);
	}

	/**
	 * Placeholder indicating this class should handle what to do when a signal outage occurs
	 * (example when new TrackSegment is found in GPX file).
	 */
	@SuppressWarnings("unused")
	private void handleSignalOutage() {
		forwardToNextSequence();
		coordinates = new ArrayList<Coordinate>();
	}

	public List<CoordinateSequence> getSequences() {
		// processing must be over so create the sequence and stop additions
		if (coordinates != null) {
			forwardToNextSequence();
			coordinates = null;
		}
		return sequences;
	}

	/**
	 * grabs the coordinates and creates a sequence.
	 * 
	 */
	private void forwardToNextSequence() {
		sequences.add(CoordinateArraySequenceFactory.instance().create(coordinates.toArray(new Coordinate[coordinates
				.size()])));
	}

	/**
	 * Used to construct all instances of TrackToJtsPointConverter.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<TrackToJtsPointConverter, Builder> {

		public Builder() {
			super(new TrackToJtsPointConverter());
		}

	}// end Builder
}
