/**
 *
 */
package com.aawhere.track.match;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.QuantityMath;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Using a buffer around the source LineString this will compare the ratio of the part of the source
 * line that is contained within the buffered target compared to the original length of the source
 * line.
 * 
 * https://aawhere.jira.com/wiki/display/TN/Activity+Spatial+Relationships
 * 
 * TN-181:This algorithm has a limitation for activities that overlap themselves. JTS returns a
 * single line to represent the two or more lines that repeat the same section. For most geometry
 * situations such a reduction is likely preferred, but for our track log human behaviors we don't
 * receive accurate numbers.
 * 
 * @author roller
 * 
 */
public class BufferedLineStringSimilarityCalculator {

	public static final Ratio RATIO_TOTAL_MATCH = MeasurementUtil.RATIO_ONE;
	public static final Ratio RATIO_NO_MATCH = MeasurementUtil.RATIO_ZERO;

	private Geometry sourceGeometry;
	private Geometry targetGeometry;
	private Geometry bufferedSource;
	private Geometry bufferedTarget;

	private Ratio sourceSimilarityRatio;

	/**
	 * Ratio of the length of similar track to the length of source track.
	 * 
	 * @see ActivitySpatialRelation
	 * 
	 */
	public Ratio getSourceRatio() {
		return sourceSimilarityRatio;
	}

	/**
	 * Use {@link Builder}
	 * 
	 */
	private BufferedLineStringSimilarityCalculator() {
	}

	/**
	 * https://aawhere.jira.com/wiki/display/TN/Activity+Spatial+Relationships
	 * 
	 * @param relationBuilder
	 * @param sourcePolyline
	 * @param targetPolyline
	 */
	private void determineOverlap() {
		// intersection and difference ignores repeats...so when looking at the total distance we
		// must too.
		Geometry sourceToSourceIntersection = this.bufferedSource.intersection(sourceGeometry);
		// the length of the line minus repeat sections
		double sourceLength = sourceToSourceIntersection.getLength();

		// the intersection will always be less than the total length (excluding buffer extra)
		// subtracting the difference allows for reporting repeat distance better.Intersection will
		// only report an out and back as half, where doing the difference will identify all points
		// are within
		Geometry difference = this.sourceGeometry.difference(this.bufferedTarget);
		// notice the length measurements are now compatible since they have both been minimized
		// through intersection/difference
		double sourceMatchLength = sourceLength - difference.getLength();

		double sourceRatio = sourceMatchLength / sourceLength;
		sourceSimilarityRatio = MeasurementUtil.createRatio(sourceRatio);
		// keep it below ratio 1 since it could be slightly bigger due to buffer length
		sourceSimilarityRatio = QuantityMath.create(sourceSimilarityRatio).min(RATIO_TOTAL_MATCH).getQuantity();

	}

	public static class Builder
			extends ObjectBuilder<BufferedLineStringSimilarityCalculator> {

		/**
		 * @param building
		 */
		public Builder() {
			super(new BufferedLineStringSimilarityCalculator());
		}

		public Builder setTargetGeometry(Geometry targetGeometry) {
			building.targetGeometry = targetGeometry;

			return this;
		}

		public Builder setSourceGeometry(Geometry sourceGeometry) {
			building.sourceGeometry = sourceGeometry;

			return this;
		}

		public Builder setSourceBufferedGeometry(Geometry sourceBufferedGeometry) {
			building.bufferedSource = sourceBufferedGeometry;
			return this;
		}

		public Builder setTargetBufferedGeometry(Geometry targetBufferedGeometry) {
			building.bufferedTarget = targetBufferedGeometry;
			return this;
		}

		@Override
		public void validate() {

			validateType(building.sourceGeometry);
			validateType(building.targetGeometry);
			Assertion.exceptions().notNull("bufferedSource", building.bufferedSource);
			Assertion.exceptions().notNull("bufferedTarget", building.bufferedTarget);

		}

		private void validateType(Geometry g) {
			Assertion.assertNotNull(g);
			if (!(g instanceof LineString || g instanceof MultiLineString)) {
				throw new IllegalArgumentException("LineString required, but provided " + g.getClass().getSimpleName());
			}

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public BufferedLineStringSimilarityCalculator build() {
			BufferedLineStringSimilarityCalculator built = super.build();
			built.determineOverlap();
			return built;
		}

	}

	public static Builder create() {
		return new BufferedLineStringSimilarityCalculator.Builder();
	}

}
