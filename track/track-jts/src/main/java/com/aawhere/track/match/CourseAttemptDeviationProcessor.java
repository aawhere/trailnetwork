/**
 * 
 */
package com.aawhere.track.match;

import java.util.List;

import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.ProcessorListenerBase;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeeker;
import com.aawhere.track.process.UnfinishedProcessorListener;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.SemiAutomaticTrackProcessor;
import com.aawhere.track.process.seek.TrackpointSeekerChainBuilder;

/**
 * Coordinates the {@link CourseAttemptDeviationSeeker} with the
 * {@link SlidingBufferTrackpointSeeker} by controlling the processing of each keeping them
 * coordinated.
 * 
 * Although not a proper {@link TrackProcessor}, this manages many processors,
 * {@link TrackpointSeeker}s and is also a {@link TrackpointListener} which should be part of the
 * attempt track processing.
 * 
 * @author aroller
 * 
 */
public class CourseAttemptDeviationProcessor
		extends ProcessorListenerBase
		implements TrackpointListener, UnfinishedProcessorListener {

	/**
	 * Used to construct all instances of CourseAttemptDeviationProcessor.
	 */
	public static class Builder
			extends ProcessorListenerBase.Builder<CourseAttemptDeviationProcessor, Builder> {
		private Iterable<Trackpoint> course;
		private Length bufferWidth;
		private Length allowedDeviationLength;

		public Builder() {
			super(new CourseAttemptDeviationProcessor());
		}

		public Builder setCourse(Iterable<Trackpoint> course) {
			this.course = course;
			return this;
		}

		public Builder setBufferWidth(Length bufferWidth) {
			this.bufferWidth = bufferWidth;
			return this;
		}

		/**
		 * This length is used for the buffer length for the course and the allowed deviation for
		 * the attempt.
		 * 
		 * @param allowedDeviationLength
		 * @return
		 */
		public Builder setAllowedDeviationLength(Length allowedDeviationLength) {
			this.allowedDeviationLength = allowedDeviationLength;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("course", course);
			Assertion.exceptions().notNull("bufferWidth", bufferWidth);
		}

		@Override
		public CourseAttemptDeviationProcessor build() {
			CourseAttemptDeviationProcessor built = super.build();

			SemiAutomaticTrackProcessor.Builder courseBuilder = SemiAutomaticTrackProcessor.createSemiAutomatic();
			courseBuilder.setTrack(this.course);

			// make the buffer length double of the deviant length
			Length bufferLength = QuantityMath.create(this.allowedDeviationLength).times(2).getQuantity();
			built.courseBufferSeeker = courseBuilder.registerLimitSeeker(SlidingBufferTrackpointSeeker.create()
					.setBufferWidth(this.bufferWidth).setMinBufferLength(bufferLength));

			built.courseBufferProcessor = courseBuilder.build();

			TrackpointSeekerChainBuilder<ManualTrackProcessor> deviationChainBuilder = TrackpointSeekerChainBuilder
					.create(new ManualTrackProcessor.Builder());
			CourseAttemptDeviationSeeker.Builder deviationSeekerBuilder = CourseAttemptDeviationSeeker.create()
					.setCourseBufferSeeker(built.courseBufferSeeker).setMaxDeviation(allowedDeviationLength);
			deviationChainBuilder.addSeeker(deviationSeekerBuilder);
			// this does the notification
			deviationChainBuilder.registerWithCurrentProcessor(built.new DeviationListenerBuilder());

			built.attemptProcessor = deviationChainBuilder.build();
			built.deviationSeeker = built.attemptProcessor.getListener(CourseAttemptDeviationSeeker.class);
			return built;
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Drives the iteration of the attempt that is provided from external sources
	 */
	private ManualTrackProcessor attemptProcessor;
	/** Searches for large deviations from the course and notifies this */
	private CourseAttemptDeviationSeeker deviationSeeker;
	private SlidingBufferTrackpointSeeker courseBufferSeeker;
	private SemiAutomaticTrackProcessor courseBufferProcessor;

	/** Use {@link Builder} to construct CourseAttemptDeviationProcessor */
	private CourseAttemptDeviationProcessor() {
	}

	public Boolean endOfCourseReached() {
		return !this.courseBufferProcessor.hasMore();
	}

	/** convenience for a common scenario where this may be driven external to standard processing. */
	public void handle(Trackpoint trackpoint) {
		handle(trackpoint, null);
	}

	/**
	 * The total deviations currently confirmed to be off-course. A deviation is a sequence of
	 * trackpoints that went off course, but the attempt has since returned to the course.
	 * 
	 * @return
	 */
	public List<Track> getDeviations() {
		return this.deviationSeeker.getDeviations();
	}

	/**
	 * @return the courseBufferSeeker
	 */
	SlidingBufferTrackpointSeeker getCourseBufferSeeker() {
		return this.courseBufferSeeker;
	}

	/**
	 * @return the deviationSeeker
	 */
	CourseAttemptDeviationSeeker getDeviationSeeker() {
		return this.deviationSeeker;
	}

	/**
	 * Provides the length indicating if the current deviation is building up.
	 * 
	 * @return
	 */
	public Length getLengthOfCurrentDeviation() {
		return this.deviationSeeker.lengthOfTrailingMisses();
	}

	/**
	 * Indicates that there is a deviation length that is beyond the desired allowed length and this
	 * processor will increasing perform slower and slower.
	 * 
	 * @return the deviationLengthExceeded
	 */
	public boolean isDeviationLengthExceeded() {
		return this.deviationSeeker.isTooMuchDeviationDetected();
	}

	/**
	 * Provided a trackpoint from the attempt track this will investigate if it is a deviation.
	 * Large deviations are inneficient to process so {@link #processingComplete()} should be called
	 * periodically to determine if processing should continue.
	 * 
	 * @param trackpoint
	 *            from the attempt track.
	 * @param processor
	 *            is not used so null may be passed
	 * 
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 *      com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		// insurance against a runaway process.
		if (!this.isDeviationLengthExceeded()) {
			attemptProcessor.notifyTrackpointListeners(trackpoint);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.process.UnfinishedProcessorListener#finish()
	 */
	@Override
	public void finish() {
		// make one final request to the buffer just in case it will help
		if (this.courseBufferProcessor.hasMore()) {
			requestNextCourseBuffer();
		}
		this.attemptProcessor.finish();
	}

	/**
	 * A common place for this to call when the buffer is to be resumed and the next length is
	 * available.
	 * 
	 */
	void requestNextCourseBuffer() {
		this.courseBufferProcessor.resume();
		this.courseBufferSlid();

	}

	/**
	 * A signal that a new course buffer is available. Process all that may be advantageous knowing
	 * the new course buffer may be matching current deviation points.
	 * 
	 */
	protected void courseBufferSlid() {
		this.deviationSeeker.nextCourseSectionAvailable();
	}

	/**
	 * Signaled by the deviationSeeker when it has deviated too far off course.
	 * 
	 * @author aroller
	 * 
	 */
	private class DeviationListener
			extends ProcessorListenerBase
			implements TrackpointListener {

		/**
		 * This is signaled by the devationSeeker that it has deviated off course too far and a new
		 * buffer should be tried.
		 * 
		 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
		 *      com.aawhere.track.process.TrackProcessor)
		 */
		@Override
		public void handle(Trackpoint trackpoint, TrackProcessor processor) {
			// the buffer will slide until it reaches it's new length
			requestNextCourseBuffer();

		}

	}

	/**
	 * Inner class used to build {@link DeviationListener}.
	 * 
	 * @author aroller
	 * 
	 */
	private class DeviationListenerBuilder
			extends ProcessorListenerBase.Builder<DeviationListener, DeviationListenerBuilder> {

		/**
		 * @param listener
		 */
		public DeviationListenerBuilder() {
			super(new DeviationListener());

		}

	}

}
