/**
 * 
 */
package com.aawhere.track.match;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.measure.quantity.Length;

import org.apache.commons.lang3.time.StopWatch;

import com.aawhere.lang.Assertion;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.SimpleTrack;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessor;
import com.aawhere.track.process.TrackProcessorUtil;
import com.aawhere.track.process.TrackpointListener;
import com.aawhere.track.process.TrackpointSeekerBase;
import com.aawhere.track.process.UnfinishedProcessorListener;

import com.vividsolutions.jts.geom.LineString;

/**
 * Verifies that a {@link Track} (known as the attempt) has completed another Track (known as the
 * course) within an acceptable deviation from the course. This produces {@link #deviations} which
 * are {@link Track}s that were deviations less than {@link #maxDeviationLength} where the attempt
 * went off course, but returned within a reasonable length.
 * 
 * 
 * Every point of the participating track must fall within the
 * {@link SlidingBufferTrackpointSeeker#getBuffer()} to be counted as completed (excluding the
 * allowed deviation). The limited length buffer progresses (or slides) when this signals that a
 * match is found (i.e. a trackpoint falls within the buffer) by calling
 * {@link #desiredTrackpointFound(Trackpoint)} to the processor that is managing the processing of
 * both the attempt and course tracks.
 * 
 * The buffer has a desired length which allows the participating trackpoints to fall within the
 * buffer. The points that fall within the buffer are verified and discarded. The points that fall
 * outside of the buffer are kept in sequence in the {@link #trailingMisses} queue. All points in
 * the {@link #trailingMisses} queue are always sequential with no gaps.
 * 
 * The {@link #trailingMisses} queue removes points for multiple reasons.
 * 
 * <pre>
 * 1. A new buffer is available and the misses fall within the new buffer.
 * 2. A subsequent trackpoint has been found to land within the buffer identifying the attempt is back on the course.  
 *    Missing Trackpoints preceding the match are then removed from the buffer 
 *    and a {@link Track} is added to {@link #deviations}.
 * 3. Processing has {@link #finish()} and any points still in the {@link #trailingMisses} are considered a non-match 
 *    and a deviation is created as in item 2.
 * </pre>
 * 
 * 
 * This seeker is intended to be used only with course/attempts that are likely matches. This also
 * requires both the course and attempt to be starting at the start of the course. The logic for
 * detecting this match is left up to the client creating this.
 * 
 * 
 * To avoid flopping issues (which may lead to incorrect mis-hits) this will notify only when it has
 * received enough {@link #trailingMisses} that don't land within the buffer to exceed the
 * {@link #maxDeviationLength} allowed. The resulting behavior between a course buffer and attempt
 * track that match is a line of {@link #trailingMisses} that extends away from the buffer until it
 * is too long, then the buffer increments and removes all those trailing misses. This repeats until
 * the end of the course or until attempt trackpoints are no longer provided (due to the end of the
 * attempt track or the external client determined processing is no longer a good idea).
 * 
 * If a trackpoint is added to the {@link #trailingMisses} when the {@link #maxDeviationLength} has
 * already been exceeded results in a runtime exception since the client is providing trackpoints
 * and not handling notifications properly.
 * 
 * @author aroller
 * 
 */
public class CourseAttemptDeviationSeeker
		extends TrackpointSeekerBase
		implements TrackpointListener, UnfinishedProcessorListener {

	/**
	 * Used to construct all instances of CourseAttemptDeviationSeeker.
	 */
	public static class Builder
			extends TrackpointSeekerBase.Builder<CourseAttemptDeviationSeeker, Builder> {

		public Builder() {
			super(new CourseAttemptDeviationSeeker());
		}

		public Builder setCourseBufferSeeker(SlidingBufferTrackpointSeeker courseBufferSeeker) {
			building.courseBufferSeeker = courseBufferSeeker;
			return this;
		}

		public Builder setMaxDeviation(Length maxDeviation) {
			building.maxDeviationLength = maxDeviation;
			return this;
		}

		@Override
		public CourseAttemptDeviationSeeker build() {
			CourseAttemptDeviationSeeker built = super.build();
			return built;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("maxDeviationLength", building.maxDeviationLength);
			Assertion.exceptions().notNull("courseBufferSeeker", building.courseBufferSeeker);

		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private Length maxDeviationLength;
	private SlidingBufferTrackpointSeeker courseBufferSeeker;

	private boolean tooMuchDeviationDetected = false;

	/**
	 * A collection of sequential trackpoints that all missed the buffer since the previous known
	 * match.
	 * 
	 */
	private ArrayList<Trackpoint> trailingMisses = new ArrayList<>();

	/**
	 * A list of trackpoint sequences that never hit the buffer indicated by subsequent trackpoints
	 * that did.
	 * 
	 */
	private ArrayList<Track> deviations = new ArrayList<Track>();
	/**
	 * Helps us understand where the last match was found. We keep record of the misses in
	 * {@link #trailingMisses}. This is used especially to calculate the length of the first
	 * deviated segment which is this point and the first from {@link #trailingMisses}.
	 */
	private Trackpoint previousMatchingTrackpoint;

	/** Use {@link Builder} to construct CourseAttemptDeviationSeeker */
	private CourseAttemptDeviationSeeker() {
	}

	/**
	 * Looks at the given trackpoint for match in the buffer. If not it is added to the trailing
	 * misses. IF the trailing misses is too long a new buffer will be requested. After a buffer
	 * request this will receive one more trackpoint in which it or any of the other misses must
	 * match, otherwise this will mark {@link #tooMuchDeviationDetected} to true and give up
	 * seeking.
	 * 
	 * @see com.aawhere.track.process.TrackpointListener#handle(com.aawhere.track.Trackpoint,
	 *      com.aawhere.track.process.TrackProcessor)
	 */
	@Override
	public void handle(Trackpoint trackpoint, TrackProcessor processor) {
		// protects wasted processing when deviation exceeded
		if (!this.tooMuchDeviationDetected) {
			Boolean contains = courseBufferContainsTrackpoint(trackpoint);
			if (contains) {
				// this means we found a match. The queue must be emptied since we will have a
				// positive sequence. no problem if already empty
				emptyBuffer(true, trackpoint);
				matchFound(trackpoint);
			} else {

				// not contained...we must log the miss into the buffer
				this.trailingMisses.add(trackpoint);

				// sliding the buffer is wasteful to often so the length check scales with
				// consecutiveSlideCount to avoid over-reaction
				if (missingBufferFull()) {
					// call for a course buffer slide if available
					if (this.courseBufferSeeker.isFinished()) {
						this.tooMuchDeviationDetected = true;
					} else {
						// this is the notification to slide the buffer forward
						desiredTrackpointFound(trackpoint);
						// see nextCourseSectionAvailable to see what happens after the buffer
						// increments
					}
				}

			}
		}
		// otherwise do nothing and iteration will send another trackpoint until one is found

	}

	/**
	 * @param trackpoint
	 */
	private void matchFound(Trackpoint trackpoint) {
		this.previousMatchingTrackpoint = trackpoint;
	}

	/**
	 * called by the processor at the end this will ensure all trailing misses are logged and
	 * accounted for.
	 * 
	 * @see com.aawhere.track.process.UnfinishedProcessorListener#finish()
	 */
	@Override
	public void finish() {

		emptyBuffer(true, null);
	}

	/**
	 * Since this is not a listener of the course buffer being increment, this allows the
	 * coordinator to send the signal so we can attempt to match the missmatches in our buffer
	 * against the new course buffer.
	 * 
	 */
	protected void nextCourseSectionAvailable() {
		// do not log the trailing misses because more buffers will come
		emptyBuffer(false, null);
		// if we are still full even after empying the buffer we must give up
		if (missingBufferFull()) {
			this.tooMuchDeviationDetected = true;
		}
	}

	/**
	 * processes the {@link #trailingMisses} knowing that each trackpoint must match the current
	 * buffer or be identified as part of a non-matching sequence. this does nothing if the buffer
	 * is empty
	 * 
	 * @param logTrailingDeviation
	 *            indicates the caller's desire to ensure the queue is emptied knowing a currently
	 *            received trackpoint has a match...hence all previous missmatches are permanent.
	 *            Providing false will only log mismatches that have a more recent match in which a
	 *            trailing deviation does not.
	 */
	private void emptyBuffer(Boolean logTrailingDeviation, Trackpoint mostRecentMatch) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		ArrayList<Trackpoint> deviation = new ArrayList<>();
		// first point of every deviation is the last known match
		// deviation.add(previousMatchingTrackpoint);
		int startedWith = this.trailingMisses.size();
		for (int i = 0; i < this.trailingMisses.size(); i++) {
			Trackpoint trackpoint = this.trailingMisses.get(i);
			if (courseBufferContainsTrackpoint(trackpoint)) {
				matchFound(trackpoint);
				// logs a deviation only if not empty..returns an empty list always
				deviation = logDeviation(deviation, trackpoint);
			} else {
				deviation.add(trackpoint);
			}
		}
		// the new set of trailing misses is the latest trailing deviation
		this.trailingMisses = deviation;

		// ensures any deviation is logged once the buffer is emptied if desired
		if (logTrailingDeviation) {
			// assumes the caller knows of a more recent match..or iteration has ended
			deviation = logDeviation(deviation, mostRecentMatch);
			this.trailingMisses = new ArrayList<>();
		}
		long time = stopWatch.getTime();
		if (time > 50) {
			LoggerFactory.getLogger(getClass()).fine(" emptying the buffer took " + time + " started with "
					+ startedWith + " finished with " + this.trailingMisses.size());

		}
	}

	/**
	 * Given a trackpoint builder this will log it as a missing sequence in the {@link #deviations}
	 * and returns an empty one ready for processing . If the deviation is empty there is no action
	 * is taken and the existing empty one is returned.
	 * 
	 * @param deviation
	 * @return
	 */
	private ArrayList<Trackpoint> logDeviation(ArrayList<Trackpoint> deviation, Trackpoint returnTrackpoint) {
		// previous were missing, but now one found so cut it off and store it
		if (!deviation.isEmpty()) {
			// avoid mutation of given deviation
			ArrayList<Trackpoint> loggedDeviation = new ArrayList<>();
			if (this.previousMatchingTrackpoint != null) {
				// add the exit to the beginning and return at the end
				loggedDeviation.add(this.previousMatchingTrackpoint);
			}
			loggedDeviation.addAll(deviation);
			if (returnTrackpoint != null) {
				loggedDeviation.add(returnTrackpoint);
			}
			this.deviations.add(new SimpleTrack.Builder(loggedDeviation).build());
			deviation = new ArrayList<>();
		}// else empty means all are being found...nothing to log
		return deviation;
	}

	/**
	 * Calculates the length of the recent trackpoints that have been added since the most recent
	 * match.
	 * 
	 * @return
	 */
	public Length lengthOfTrailingMisses() {
		return QuantityMath.create(TrackProcessorUtil.length(getTrailingMissesSegments())).getQuantity();
	}

	/**
	 * {@link #trailingMisses} holds the actual misses, but in order to represent the missing
	 * segments extra points are needed.
	 * 
	 * @return
	 */
	protected List<Trackpoint> getTrailingMissesSegments() {
		ArrayList<Trackpoint> completeMisses = new ArrayList<>();
		if (this.previousMatchingTrackpoint != null && !this.trailingMisses.isEmpty()) {
			completeMisses.add(previousMatchingTrackpoint);
		}
		completeMisses.addAll(trailingMisses);
		return completeMisses;
	}

	public LineString lineStringOfCurrentMisses() {

		return JtsTrackGeomUtil.trackToLineStringWithoutDuplicateTrackpoints(getTrailingMissesSegments());
	}

	/**
	 * Indicates that the current set of trackpoint misses has reached the
	 * {@link #maxDeviationLength} length.
	 * 
	 * @return
	 */
	private Boolean missingBufferFull() {

		// factoring in the slide count gives the buffer to fill up without taking action
		return QuantityMath.create(maxDeviationLength).lessThan(lengthOfTrailingMisses());
	}

	/**
	 * Provides the test executed to see if a given {@link Trackpoint} is contained within the
	 * {@link SlidingBufferTrackpointSeeker#getBuffer()}.
	 * 
	 * @param trackpoint
	 * @return
	 */
	private Boolean courseBufferContainsTrackpoint(Trackpoint trackpoint) {
		return this.courseBufferSeeker.contains(trackpoint);
	}

	public List<Track> getDeviations() {
		return Collections.unmodifiableList(this.deviations);
	}

	/**
	 * Indicates that this seeker has given up because the length of {@link #trailingMisses} has
	 * exceeded {@link #maxDeviationLength} even after another buffer request was made.
	 * 
	 * @return the tooMuchDeviationDetected
	 */
	public Boolean isTooMuchDeviationDetected() {
		return this.tooMuchDeviationDetected;
	}
}
