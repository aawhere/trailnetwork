/**
 * 
 */
package com.aawhere.track.match;

import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.Trackpoint;

import com.google.common.base.Function;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Converts the given track to the desired spatial buffer per the configurations given.
 * 
 * @author aroller
 * 
 */
public class TrackToGeometryFunction
		implements Function<Iterable<Trackpoint>, Pair<? extends Geometry, MultiLineString>> {

	private Length bufferWidth;

	/**
	 * Used to construct all instances of TrackToGeometryFunction.
	 */
	public static class Builder
			extends ObjectBuilder<TrackToGeometryFunction> {

		public Builder() {
			super(new TrackToGeometryFunction());
		}

		public Builder width(Length bufferWidth) {
			building.bufferWidth = bufferWidth;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("bufferWidth", building.bufferWidth);

		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct TrackToGeometryFunction */
	private TrackToGeometryFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public Pair<? extends Geometry, MultiLineString> apply(@Nullable Iterable<Trackpoint> input) {
		return JtsTrackGeomUtil.buffer(input, bufferWidth);
	}

}
