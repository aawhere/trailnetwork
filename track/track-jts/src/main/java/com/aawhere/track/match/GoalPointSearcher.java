/**
 * 
 */
package com.aawhere.track.match;

import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.JtsMeasureUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.operation.distance.DistanceOp;

/**
 * Specializes in searching for the best finish by looking at the area first and then looking at the
 * finish line once it can be known.
 * 
 * An optional goal line is ideally used to get an exact crossing. Such a line must be perpendicular
 * to the path of travel so the goal segment of the target track may be provided in such a case.
 * 
 * Some finishes don't cross the line because the finish line is funky. As long as they were in the
 * area the course is completed and the closes distance to the finish point will be used.
 * 
 * @author aroller
 * 
 */
public class GoalPointSearcher {

	/**
	 * Used to construct all instances of GoalSearcher.
	 */
	public static class Builder
			extends ObjectBuilder<GoalPointSearcher> {

		/**
		 * Optional final segment provided if the final goal segment is known. This is used to
		 * calculate the finish line.
		 */
		private TrackSegment goalSegment;
		private Length goalLineBufferLength = MeasurementUtil.createLengthInMeters(0.5);

		private Builder(GoalPointSearcher mutatee) {
			super(mutatee);
		}

		public Builder() {
			super(new GoalPointSearcher());
		}

		public Builder setGoal(Trackpoint goal) {
			building.goal = goal;
			building.goalPoint = JtsTrackGeomUtil.convertToPoint(goal);
			return this;
		}

		public Builder setGoalLineBufferLength(Length length) {
			this.goalLineBufferLength = length;
			return this;
		}

		public Builder setGoalAreaRadius(Length goalAreaRadius) {
			building.goalAreaRadius = goalAreaRadius;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("goalAreaRadius", building.goalAreaRadius);
			Assertion.exceptions().notNull("goal", building.goal);

		}

		public GoalPointSearcher build() {
			GoalPointSearcher built = super.build();
			built.areaDistance = JtsTrackGeomUtil.arcLength(built.goalAreaRadius, built.goal);
			// calculate the goal line if the goal line segment is provided.
			if (this.goalSegment != null) {
				// create a goal line with some buffer.
				built.goalLine = JtsTrackGeomUtil.perpendicularLine(built.goal,
																	TrackUtil.heading(goalSegment),
																	built.goalAreaRadius);
				// 1 meter buffer = 2m wide line
				// goal line can't be just a line because if the segment lands exactly on the line
				// it could be missed.
				built.goalPolygon = JtsTrackGeomUtil.buffer(built.goalLine, this.goalLineBufferLength);
				// Avoid exporting invalid KML due to an empty point set.
				if (built.goalPolygon.isEmpty()) {
					built.goalPolygon = null;
				}
			}
			return built;
		}

		/**
		 * This is the segment whose first point is the Goal Point and leads perpendicularly away
		 * from the finish line.
		 * 
		 * @param segment
		 * @return
		 */
		public Builder setSegmentLeavingStart(TrackSegment segment) {
			Assertion.exceptions().assertNull("segment already assigned", this.goalSegment);
			setGoal(segment.getFirst());
			this.goalSegment = segment;
			return this;
		}

		public Builder setSegmentArrivingAtFinish(TrackSegment segment) {
			Assertion.exceptions().assertNull("segment already assigned", this.goalSegment);
			setGoal(segment.getSecond());
			this.goalSegment = segment;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(GoalPointSearcher mutatee) {
		return new Builder(mutatee);
	}

	/** The point that represents the ideal spot to reach for {@link #goalReached} to be true. */
	private Trackpoint goal;
	private Point goalPoint;
	/** The goal line that is ideally crossed indicating the best point possible. */
	private LineString goalLine;
	/**
	 * A more forgiving {@link #goalLine} to accommodate minor misses due to geo math precision
	 * errors that can cause a segment to just miss the goal line on both sides.
	 * 
	 */
	private Geometry goalPolygon;
	/**
	 * represents the best trackpoint achieved so far. It is interpolated from either the spot
	 * crossing the line or the nearest point of a line segment that was close to the goal.
	 */
	private Trackpoint best;
	private Length goalAreaRadius;
	/**
	 * Indicates that the area circle has been reached already. This is useful to know to stop
	 * searching when leaving the area and also to enhance efficiency of looking for finish line
	 * crossing by doing a simple distance check.
	 * 
	 */
	private boolean areaReached = false;
	/** Identifies that the goal is reached and it is unlikely a better match will be found. */
	private boolean goalReached = false;
	/**
	 * the closest distance achieved to the goal point. This is used to log the best if the finish
	 * line was never crossed.
	 */
	private double bestDistance = Double.MAX_VALUE;
	/** A measurement in arclength for quick calculations of the distance to the goal. */
	private double areaDistance;
	private boolean currentlyInArea = false;

	/** Use {@link Builder} to construct GoalSearcher */
	private GoalPointSearcher() {
	}

	/**
	 * Searches the segment for the distance to the goal and even better crossing the goal line.
	 * 
	 * @param segment
	 * @return
	 */
	public Boolean search(TrackSegment segment) {
		// zero distance segments only cause problems and will never get closer to goal
		if (QuantityMath.create(segment.getDistance()).positive()) {
			LineString segmentLine = JtsTrackGeomUtil.trackSegmentToLineString(segment);
			// look for the closest distance of the segment to the goal
			// NOTE: If this is costly use a more tolerant point to point distance first
			double distance = segmentLine.distance(goalPoint);
			// within the area means we'll consider it a match, but keep trying to better it
			if (distance < areaDistance) {
				// mark the achievement, but look for better.
				areaReached = true;
				this.currentlyInArea = true;
				// goal line intersection is always preferred
				Geometry intersection;
				if (this.goalLine != null && !(intersection = goalLine.intersection(segmentLine)).isEmpty()) {
					// it's already likely a coordinate, but the centroid does the conversion
					Coordinate centerOfIntersection = intersection.getCentroid().getCoordinate();
					markBestSoFar(segment, centerOfIntersection, distance);
					this.goalReached = true;
				} else if (goalPolygon != null && !(intersection = goalPolygon.intersection(segmentLine)).isEmpty()) {
					// in this case the lines didn't intersect, but the line intersected the polygon
					// This must be one of the endpoints that landed in the outside of the buffer
					// figure out which endpoint and use it to avoid estimation errors that may make
					// the chosen point exist outside the segment
					Point crossingPoint;
					if (goalPolygon.contains(crossingPoint = JtsTrackGeomUtil.point(segment.getFirst()))) {
						// nothing to do since assignment was done in condition
					} else if (goalPolygon.contains(crossingPoint = JtsTrackGeomUtil.point(segment.getSecond()))) {
						// nothing to do since assignment was done in condition
					} else {
						// apparently it isn't one of the endpoints...so just use the point directly
						// and hope for the best.
						crossingPoint = intersection.getCentroid();
					}
					markBestSoFar(segment, crossingPoint.getCoordinate(), distance);
					this.goalReached = true;
				} else {
					// look for a closer point than previously logged
					if (distance < bestDistance) {
						// mark the best...that 1 indexes per the docs
						Coordinate nearestPoint = DistanceOp.nearestPoints(goalPoint, segmentLine)[1];
						markBestSoFar(segment, nearestPoint, distance);
					} else {
						// still within area, but going away from point. Assume line won't be
						// crossed.
						this.goalReached = true;
						// use the previous best
					}
				}
			} else {
				this.currentlyInArea = false;
				// if the area was reached and now it isn't, consider the search over since we are
				// moving away
				if (areaReached) {
					this.goalReached = true;
				}
				// otherwise do nothing and wait for something better
			}
		}
		return this.goalReached;
	}

	/**
	 * An indication there won't be any more segments received. This will finish up by marking
	 * goalReached if a visit to the area has been achieved.
	 * 
	 */
	public void finish() {
		this.goalReached = this.areaReached;
	}

	/**
	 * @param segment
	 * @param bestSoFar
	 */
	private void markBestSoFar(TrackSegment segment, Coordinate bestSoFar, double distanceAway) {
		GeoCoordinate cross = JtsMeasureUtil.convert(bestSoFar);
		this.best = TrackUtil.interpolate(segment, cross);
		// we are dealing with fat goal lines so interpolated points may hit outside the segment.
		// choose the bound of the segment if out of bounds!
		if (this.best.getTimestamp().isBefore(segment.getFirst().getTimestamp())) {
			this.best = segment.getFirst();
		} else if (this.best.getTimestamp().isAfter(segment.getSecond().getTimestamp())) {
			this.best = segment.getSecond();
		}
		this.bestDistance = distanceAway;
	}

	/**
	 * Indicates the closest point to the goal or the point that crossed the goal line.
	 * 
	 * @return the best
	 */
	public Trackpoint getBest() {
		return this.best;
	}

	/**
	 * @return the goalReached
	 */
	public Boolean isGoalReached() {
		return this.goalReached;
	}

	/**
	 * @return the areaReached
	 */
	public Boolean isAreaReached() {
		return this.areaReached;
	}

	public Boolean hasGoalLine() {
		return this.goalPolygon != null;
	}

	/**
	 * @return
	 */
	public Boolean isCurrentlyInArea() {
		return currentlyInArea;
	}

	/**
	 * @return the goal
	 */
	public Trackpoint getGoal() {
		return this.goal;
	}

	/**
	 * @return the goalPolygon
	 */
	public Geometry getGoalLine() {
		return this.goalPolygon;
	}

	/**
	 * @return the goalPoint
	 */
	public Point getGoalPoint() {
		return this.goalPoint;
	}

	/**
	 * @return the goalAreaRadius
	 */
	public Length getGoalAreaRadius() {
		return this.goalAreaRadius;
	}

	public Geometry getGoalArea() {
		return JtsTrackGeomUtil.buffer(goalPoint, goalAreaRadius);
	}
}
