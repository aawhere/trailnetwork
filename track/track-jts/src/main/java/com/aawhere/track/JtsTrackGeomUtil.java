/**
 *
 */
package com.aawhere.track;

import java.util.ArrayList;
import java.util.List;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;

import com.aawhere.jts.geom.JtsGeometryFactory;
import com.aawhere.jts.geom.JtsUtil;
import com.aawhere.jts.geom.match.JtsMatchUtil;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.JtsMeasureUtil;
import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.track.process.calc.TrackToMultiLineStringConverter;
import com.aawhere.track.story.TrackUserStories;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Lineal;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

/**
 * Simple static utility used to translate project domain objects to JTS specific objects.
 * 
 * @author roller
 * 
 */
public class JtsTrackGeomUtil {

	private static final Unit<Elevation> ELEVATION_UNIT = ExtraUnits.METER_MSL;
	// TODO: turn me into a configurable variable.
	public static final Length similificationDistanceTolorance = MeasurementUtil.createLengthInMeters(1d);
	// TODO: the factory and it's precision should be configurable. This util should turn into an
	// object
	private static final GeometryFactory factory = JtsGeometryFactory.getDefaultfactory();

	/**
	 * Converts the given trackpoint into a Coordinate focused on x,y.
	 * 
	 * @param trackpoint
	 * @return
	 */
	public static Coordinate convert(Trackpoint trackpoint) {
		GeoCoordinate location = trackpoint.getLocation();
		Coordinate coordinate;
		if (location != null) {
			double x = location.getLongitude().getInDecimalDegrees();
			double y = location.getLatitude().getInDecimalDegrees();
			Elevation elevation = trackpoint.getElevation();
			if (elevation != null) {
				double z = elevation.doubleValue(ELEVATION_UNIT);
				coordinate = new Coordinate(x, y, z);
			} else {
				coordinate = new Coordinate(x, y);
			}

		} else {
			coordinate = null;
		}
		return coordinate;
	}

	public static com.vividsolutions.jts.geom.Point convertToPoint(Trackpoint trackpoint) {
		return factory.createPoint(convert(trackpoint));
	}

	/**
	 * @deprecated use {@link #trackToLineStringWithoutDuplicateTrackpoints(Track)} since it doesn't
	 *             return multiLineString
	 * @param track
	 * @return
	 */
	@Deprecated
	public static LineString trackToMultiLineStringWithoutDuplicateTrackpoints(Track track) {
		return trackToLineStringWithoutDuplicateTrackpoints(track);
	}

	/**
	 * Converts a {@link Track} to a {@link MultiLineString}. Duplicate {@link Trackpoint}s are
	 * first filterd from the Track. If there aren't enough points then an empty {@link LineString}
	 * is returned.
	 * 
	 * @deprecated a track has missing segments and a {@link LineString} does not. use
	 *             {@link #trackToMultiLineString(Iterable, Boolean)} with true for simplify
	 * @param track
	 * @return
	 */
	@Deprecated
	public static LineString trackToLineStringWithoutDuplicateTrackpoints(Iterable<Trackpoint> track) {

		// one iteration
		MultiLineString mls = trackToMultiLineString(track);
		if (!JtsUtil.isEmpty(mls)) {
			Trackpoint trackpoint = track.iterator().next();
			Angle distanceTolerance = GeoMath.arcLength(similificationDistanceTolorance, trackpoint.getLocation());
			// second iteration
			// TODO:This might be too aggressive
			Geometry simplifiedMls = DouglasPeuckerSimplifier.simplify(mls, distanceTolerance
					.doubleValue(USCustomarySystem.DEGREE_ANGLE));
			return (LineString) simplifiedMls;
		} else {
			return JtsUtil.emptyLineString(factory);
		}
	}

	/**
	 * 
	 * @param track
	 * @return
	 */
	public static MultiLineString trackToMultiLineString(Iterable<Trackpoint> track) {
		return trackToMultiLineString(track, false);
	}

	/**
	 * @see #trackToMultiLineString(Iterable, Length)
	 * 
	 * @param track
	 * @param simplify
	 * @return
	 */
	public static MultiLineString trackToMultiLineString(Iterable<Trackpoint> track, Boolean simplify) {
		return trackToMultiLineString(track, (simplify) ? similificationDistanceTolorance : null);
	}

	/**
	 * Given a iteratble track this will convert the track into {@link LineString}s for each
	 * continuous segment without missing location. If a location is missing then a separate line
	 * string is created and each are added to a {@link MultiLineString}. The result is a line
	 * string with gaps where the GPS lost its signal.
	 * 
	 * @param track
	 * @param minDistanceBetweenTrackpoints
	 *            is the minimum distance between trackpoints or null if no simplification is
	 *            desired
	 * @return
	 */
	public static MultiLineString trackToMultiLineString(Iterable<Trackpoint> track,
			Length minDistanceBetweenTrackpoints) {

		TrackUserStories.Builder storiesBuilder = TrackUserStories.create(track);
		storiesBuilder.addStandardFilters();

		TrackToMultiLineStringConverter.Builder multiLineStringBuilder = new TrackToMultiLineStringConverter.Builder();
		multiLineStringBuilder.setGeometryFactory(factory);
		if (minDistanceBetweenTrackpoints != null) {
			multiLineStringBuilder.setMinDistanceBetweenTrackpoints(minDistanceBetweenTrackpoints);
		}
		TrackToMultiLineStringConverter converter = storiesBuilder.getSeekerChainBuilder()
				.registerWithCurrentProcessor(multiLineStringBuilder);
		storiesBuilder.build();
		return converter.getMultiLineString();

	}

	public static Track lineStringToTrack(LineString ls) {
		return toTrack(ls);

	}

	public static Track multiLineStringToTrack(MultiLineString mls) {
		return toTrack(mls);

	}

	/**
	 * provides a {@link Function} to transform a JTS {@link Coordinate} to the {@link Trackpoint}
	 * providing the date with the {@link Supplier} of the {@link DateTime}
	 * {@link Trackpoint#getTimestamp()} of choice.
	 * 
	 * @param timestampProvider
	 * @return
	 */
	public static Function<Coordinate, Trackpoint> trackpointFunction(final Supplier<DateTime> timestampProvider) {
		return new Function<Coordinate, Trackpoint>() {

			@Override
			public Trackpoint apply(Coordinate input) {
				return (input != null) ? trackpoint(input, timestampProvider) : null;
			}
		};
	}

	/**
	 * Creates a Track from a {@link Lineal} {@link Geometry}. Timestamps are generated on the fly
	 * to avoid NPE.
	 * 
	 * @param geom
	 * @return
	 */
	private static Track toTrack(Geometry geom) {
		if (!(geom instanceof LineString || geom instanceof MultiLineString)) {
			throw new IllegalArgumentException("I accept LineString or MultiLineStrings only");
		}

		Coordinate[] coords = geom.getCoordinates();
		List<Trackpoint> trackpoints = new ArrayList<Trackpoint>();
		for (Coordinate coord : coords) {
			Trackpoint tp = trackpoint(coord);
			trackpoints.add(tp);
		}
		Track track = new SimpleTrack.Builder(trackpoints).build();
		return track;

	}

	/**
	 * @param coord
	 * @return
	 */
	private static Trackpoint trackpoint(Coordinate coord) {
		Supplier<DateTime> timestampProvider = Suppliers.ofInstance(new DateTime());
		return trackpoint(coord, timestampProvider);
	}

	/**
	 * @param coord
	 * @param timestampProvider
	 * @return
	 */
	private static Trackpoint trackpoint(Coordinate coord, Supplier<DateTime> timestampProvider) {
		GeoCoordinate location = new GeoCoordinate(coord.y, coord.x);
		Elevation elevation = MeasurementQuantityFactory.getInstance(Elevation.class).create(	coord.z,
																								ExtraUnits.METER_MSL);
		Trackpoint tp = new SimpleTrackpoint.Builder().setLocation(location).setElevation(elevation)
				.setTimestamp(timestampProvider.get()).build();
		return tp;
	}

	/**
	 * @deprecated use {@link #trackSegmentToMultiLineString(TrackSegment)}
	 * @param ts
	 * @return
	 */
	@Deprecated
	public static MultiLineString convertToMultiLineString(TrackSegment ts) {
		return trackSegmentToMultiLineString(ts);
	}

	public static MultiLineString trackSegmentToMultiLineString(TrackSegment ts) {
		Coordinate[] coords = trackSegmentToCoordinates(ts);

		MultiLineString polyline = JtsUtil.multiLineStringFrom(coords, factory);
		return polyline;
	}

	public static Coordinate[] trackSegmentToCoordinates(TrackSegment ts) {
		final int lengthOfTrackSegment = 2;

		GeoCoordinate firstLoc = ts.getFirst().getLocation();
		GeoCoordinate secondLoc = ts.getSecond().getLocation();
		Coordinate[] coords = new Coordinate[lengthOfTrackSegment];
		coords[0] = new Coordinate(firstLoc.getLongitude().getInDecimalDegrees(), firstLoc.getLatitude()
				.getInDecimalDegrees());
		coords[1] = new Coordinate(secondLoc.getLongitude().getInDecimalDegrees(), secondLoc.getLatitude()
				.getInDecimalDegrees());
		return coords;
	}

	/**
	 * Given the segment this will return a {@link LineString} of the exact representation.
	 * 
	 * @param segment
	 */
	public static LineString trackSegmentToLineString(TrackSegment segment) {
		return factory.createLineString(trackSegmentToCoordinates(segment));
	}

	/**
	 * Builds a line perpendicular to the heading around the given length using the given point as
	 * the middle of the line.
	 * 
	 * @see JtsMeasureUtil#perpendicularLine(GeoCoordinate, Angle, Length)
	 * 
	 * @param midpoint
	 * @param heading
	 * @param lineLength
	 * @return
	 */
	public static LineString perpendicularLine(Trackpoint midpoint, Angle heading, Length lineLength) {
		return JtsMeasureUtil.perpendicularLine(midpoint.getLocation(), heading, lineLength);
	}

	/**
	 * Given a track segment this will create a perpdendicular line leading from it's end point. in
	 * the heading from first to second.
	 * 
	 * @param segment
	 * @param lineLength
	 * @return
	 */
	public static LineString perpendicularLine(TrackSegment segment, Length lineLength) {
		Angle heading = TrackUtil.heading(segment);
		return perpendicularLine(segment.getSecond(), heading, lineLength);
	}

	/**
	 * Given a {@link TrackSegment} this will convert into a two point {@link LineString} then
	 * buffer it into a {@link Polygon}.
	 * 
	 * @param segment
	 * @param BUFFER_WIDTH
	 * @return
	 */
	public static Polygon buffer(TrackSegment segment, Length bufferWidth) {
		double arcLength = arcLength(bufferWidth, segment.getFirst());
		// although the method does not guarantee Polygon, a Two Point Line Segment should produce
		// nothing else.
		return (Polygon) trackSegmentToLineString(segment).buffer(arcLength);
	}

	/**
	 * creates a buffer given the desired length of the width of the buffer and a collection of
	 * trackpoints that will create a linestring that will be buffered.
	 * 
	 * @param trackpoints
	 * @param BUFFER_WIDTH
	 * @return a pair of geometries where the buffer is the left and the created line string is the
	 *         right.
	 */

	public static Pair<? extends Geometry, MultiLineString>
			buffer(Iterable<Trackpoint> trackpoints, Length bufferWidth) {
		Pair<? extends Geometry, MultiLineString> result;
		Trackpoint trackpoint = TrackUtil.firstTrackpointWithLocation(trackpoints);
		if (trackpoint != null) {
			MultiLineString lineString = trackToMultiLineString(trackpoints, true);
			// match utils' buffer is avoiding an out of memory issue TN-206
			// it also appears to speed up processing by 5x
			Geometry buffer = buffer(lineString, bufferWidth);
			result = Pair.of(buffer, lineString);
		} else {
			Polygon buffer = emptyPolygon();
			result = Pair.of(buffer, emptyMultiLineString());
		}
		return result;
	}

	/**
	 * @return
	 */
	private static Polygon emptyPolygon() {
		return factory.createPolygon(new Coordinate[] {});
	}

	private static LineString emptyLineString() {
		return factory.createLineString(new Coordinate[] {});
	}

	private static MultiLineString emptyMultiLineString() {
		return factory.createMultiLineString(new LineString[] { emptyLineString() });
	}

	private static Geometry buffer(Geometry geometry, double arcLength) {
		return JtsMatchUtil.buffer(geometry, arcLength);
	}

	/**
	 * @param geometry
	 * @param arcLength
	 * @return
	 */
	public static Geometry buffer(Geometry geometry, Angle arcLength) {
		return buffer(geometry, arcLength(arcLength));
	}

	/**
	 * Given a geometry this will buffer it using the given width (the extra amount to buffer on the
	 * outside of the shape).
	 * 
	 * @param geometry
	 * @param width
	 * @return
	 */
	public static Geometry buffer(Geometry geometry, Length width) {
		if (geometry.isEmpty()) {
			return geometry;
		} else {
			Coordinate coordinate = geometry.getCoordinate();
			return buffer(geometry, arcLength(width, JtsMeasureUtil.convert(coordinate)));
		}
	}

	/**
	 * Provides the double that JTS uses that matches our unit used in the JTS Geometry:
	 * {@link #ARC_LENGTH_UNIT}.
	 * 
	 * @deprecated this is moved to {@link JtsMeasureUtil}
	 * 
	 * @param arcLength
	 * @return
	 */
	@Deprecated
	public static double arcLength(Angle arcLength) {
		return JtsMeasureUtil.arcLength(arcLength);
	}

	/**
	 * Given a {@link Length} and a location on earth this will calculate the arcLength using
	 * {@link GeoMath#arcLength(Length, GeoCoordinate)}.
	 * 
	 * @see GeoMath#arcLength(Length, GeoCoordinate)
	 * 
	 * @param length
	 * @param trackpoint
	 * @return
	 */
	public static double arcLength(Length length, Trackpoint trackpoint) {
		return arcLength(length, trackpoint.getLocation());
	}

	/**
	 * Given a {@link Length} and a location on earth this will calculate the arcLength using
	 * {@link GeoMath#arcLength(Length, GeoCoordinate)} returning the value in coordinates that
	 * match the standard JTS/Trackpoint conversions.
	 * 
	 * @see #ARC_LENGTH_UNIT
	 * 
	 * @deprecated this is moved to {@link JtsMeasureUtil}
	 * 
	 * @param length
	 * @param coordinate
	 * @return
	 */
	@Deprecated
	public static double arcLength(Length length, GeoCoordinate coordinate) {
		return JtsMeasureUtil.arcLength(length, coordinate);
	}

	/**
	 * Assumes the given coordinate was created using the {@link #ARC_LENGTH_UNIT}.
	 * 
	 * @deprecated this is moved to {@link JtsMeasureUtil}
	 * 
	 * @see #arcLength(Length, Coordinate)
	 * @param minDistanceBetweenTrackpoints
	 * @param coordinate
	 * @return
	 */
	@Deprecated
	public static double arcLength(Length minDistanceBetweenTrackpoints, Coordinate coordinate) {
		return JtsMeasureUtil.arcLength(minDistanceBetweenTrackpoints, coordinate);
	}

	/**
	 * Given a trackpoint this will return the JTS equivalent {@link Point}.
	 * 
	 * @param trackpoint
	 * @return
	 */
	public static Point point(Trackpoint trackpoint) {
		return JtsMeasureUtil.point(trackpoint.getLocation());
	}

}
