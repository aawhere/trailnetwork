/**
 *
 */
package com.aawhere.geo.map;

import java.text.NumberFormat;

import com.aawhere.geo.map.google.GoogleMapsBounds;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.TrackInfo;
import com.aawhere.track.TrackSummary;
import com.aawhere.util.rb.KeyedMessageFormat;

/**
 * Produces a static map using Scalable Vector Graphics to draw markers and polylines overlaid on a
 * static map image background.
 * 
 * FIXME: if this gets much more complex use an SVG library like Batik.
 * 
 * @author aroller
 * 
 */
public class StaticMapSvg {

	private Integer pixelWidth = 268;
	private Integer pixelHeight = 192;
	private String svg;

	/**
	 * Used to construct all instances of StaticMapSvg.
	 */
	public static class Builder
			extends ObjectBuilder<StaticMapSvg> {

		private final String GOOGLE_MAPS_KEY = "AIzaSyCyf5COPoHDzTihXktFZiRRCTee8gd7jYc";
		private TrackInfo trackInfo;

		public Builder() {
			super(new StaticMapSvg());
		}

		public Builder trackInfo(TrackInfo trackInfo) {
			this.trackInfo = trackInfo;
			return this;
		}

		@Override
		public StaticMapSvg build() {
			StaticMapSvg built = super.build();
			StringBuilder s = new StringBuilder();
			final KeyedMessageFormat.Builder p = KeyedMessageFormat.create();
			TrackSummary summary = this.trackInfo.summary();
			NumberFormat numberFormat = NumberFormat.getInstance();
			numberFormat.setGroupingUsed(false);
			numberFormat.setMinimumFractionDigits(GeoCoordinate.ONE_M_PRECISION);
			numberFormat.setMaximumFractionDigits(GeoCoordinate.ONE_M_PRECISION);
			GoogleMapsBounds googleMapsBounds = GoogleMapsBounds.create().bounds(summary.boundingBox())
					.pixelHeight(built.pixelHeight).pixelWidth(built.pixelWidth).build();
			final BoundingBox boundingBox = googleMapsBounds.bounds();
			GeoCoordinate center = boundingBox.getCenter();
			final GeoCoordinate min = boundingBox.getMin();
			final GeoCoordinate max = boundingBox.getMax();
			Double mapHeight = QuantityMath.create(max.getLatitude()).minus(min.getLatitude()).getQuantity()
					.getInDecimalDegrees();
			Double mapWidth = QuantityMath.create(max.getLongitude()).minus(min.getLongitude()).getQuantity()
					.getInDecimalDegrees();
			String pathWidth = numberFormat.format(Math.max(mapWidth, mapHeight) * 0.01);
			p.put("pathWidth", pathWidth);
			p.put("mapWidth", mapWidth);
			p.put("mapHeight", mapHeight);

			{
				// declare the image size and viewport
				s.append("<svg width=\"{pixelWidth}px\" height=\"{pixelHeight}px\" viewBox=\"{minLon} {inverseMaxLat} {mapWidth} {mapHeight}\" preserveAspectRatio=\"none\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
				p.put("pixelWidth", built.pixelWidth).put("pixelHeight", built.pixelHeight);
				p.put("minLon", min.getLongitude().getInDecimalDegrees());
				p.put("inverseMaxLat", convertToSvgCorrdinateSystem(max.getLatitude()).getInDecimalDegrees());
			}

			// include the map background
			{
				s.append("<image xlink:href=\"http://maps.googleapis.com/maps/api/staticmap?key={googleMapsKey}&amp;center={centerLatLon}&amp;zoom={zoom}&amp;size={pixelWidth}x{pixelHeight}&amp;maptype=roadmap\" x=\"{minLon}\" y=\"{inverseMaxLat}\" height=\"{mapHeight}\" width=\"{mapWidth}\" preserveAspectRatio=\"none\"/>");
				p.put("googleMapsKey", GOOGLE_MAPS_KEY);
				p.put("zoom", googleMapsBounds.zoom());
				p.put("centerLatLon", boundingBox.getCenter().getAsString());
			}

			// draw a circle at the center.
			// s.append("<circle cx=\"{centerLon}\" cy=\"{centerInverseLat}\" r=\"{circleRadius}\" stroke=\"black\" stroke-width=\"{pathWidth}\" fill=\"red\" />");
			// p.put("centerLon", center.getLongitude().getInDecimalDegrees());
			// p.put("centerInverseLat",
			// convertToSvgCorrdinateSystem(center.getLatitude()).getInDecimalDegrees());
			// p.put("circleRadius", pathWidth);

			// draw the path
			{
				String points = SvgTrackUtil.points(this.trackInfo.track());
				s.append("<polyline points=\"{pathPoints}\"\n"
						+ "  style=\"fill:none;stroke:black;stroke-width:{pathWidth};\" />");
				p.put("pathPoints", points);
			}

			s.append("</svg>");

			// relaying on runtime validation of message format to find misguided parameters.
			built.svg = p.message(s.toString()).build().getFormattedMessage();
			return built;
		}

		private Latitude convertToSvgCorrdinateSystem(Latitude lat) {
			return QuantityMath.create(lat).multipliedBy(-1).getQuantity();
		}
	}// end Builder

	/** Use {@link Builder} to construct StaticMapSvg */
	private StaticMapSvg() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return the svg
	 */
	public String svg() {
		return this.svg;
	}
}
