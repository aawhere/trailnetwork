/**
 *
 */
package com.aawhere.geo.map;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

/**
 * @author aroller
 * 
 */
public class SvgTrackUtil {

	/**
	 * Convert a track to a SVG path. Note that in SVG the corrdinate system starts at the upper
	 * left corner, so we invert the latitude values.
	 * 
	 * @param track
	 * @return
	 */
	public static String points(Iterable<Trackpoint> track) {
		Iterable<GeoCoordinate> geoCoordinates = Iterables.transform(track, TrackUtil.coordinateFunction());
		Iterable<GeoCoordinate> svgCoordinates = Iterables.transform(geoCoordinates, svgCorrdinateFunction());
		Iterable<String> svgCoords = Iterables.transform(svgCoordinates, MeasurementUtil.geoCoordinateXyFunction());
		svgCoords = Iterables.filter(svgCoords, Predicates.notNull());
		return StringUtils.join(svgCoords, " ");
	}

	public static Function<GeoCoordinate, GeoCoordinate> svgCorrdinateFunction() {
		return new Function<GeoCoordinate, GeoCoordinate>() {

			@Override
			public GeoCoordinate apply(GeoCoordinate input) {
				if (input == null) {
					return null;
				}
				return new GeoCoordinate.Builder().setLongitude(input.getLongitude())
						.setLatitude(QuantityMath.create(input.getLatitude()).multipliedBy(-1).getQuantity()).build();
			}
		};
	}
}
