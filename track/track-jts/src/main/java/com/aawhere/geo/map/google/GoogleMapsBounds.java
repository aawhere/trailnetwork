/**
 * 
 */
package com.aawhere.geo.map.google;

import javax.measure.quantity.Length;

import org.mapsforge.core.util.MercatorProjection;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.unit.ExtraUnits;

import com.bbn.openmap.proj.Mercator;
import com.google.common.collect.Ordering;

/**
 * Calculates the Zooms and bounds used for Google Maps since the bounds for the static maps is not
 * returned.
 * 
 * This code is derived from the following for zoom calculation:
 * http://stackoverflow.com/questions/6048975
 * /google-maps-v3-how-to-calculate-the-zoom-level-for-a-given-bounds?answertab=active#tab-top
 * 
 * For the bounds:
 * 
 * http://gis.stackexchange.com/questions/46729/corner-coordinates-of-google-static-map-tile
 * 
 * {@link Mercator} uses Point2D which is not on the appengine whitelist.
 * 
 * 
 * @author aroller
 * 
 */
public class GoogleMapsBounds {
	public static final Integer WORLD_PIXEL = 256;
	public static final Integer ZOOM_MAX = 21;
	/** The zoom level to be used for the map. */
	private Integer zoom;
	/**
	 * The bounding box representing the bounds that the map will cover given the pixel
	 * width/height.
	 */
	private BoundingBox bounds;

	/**
	 * Used to construct all instances of GoogleMapsBounds.
	 */
	public static class Builder
			extends ObjectBuilder<GoogleMapsBounds> {

		private Integer pixelHeight;
		private Integer pixelWidth;
		private BoundingBox bounds;

		@Override
		public GoogleMapsBounds build() {
			GoogleMapsBounds built = super.build();

			calculateZoom(built);
			calculateBounds(built);
			return built;
		}

		private void calculateBounds(GoogleMapsBounds built) {
			final GeoCoordinate center = bounds.getCenter();
			final Latitude latitude = center.getLatitude();
			double centerLat = latitude.getInDecimalDegrees();
			double centerLon = center.getLongitude().getInDecimalDegrees();
			double metersPerPixel = MercatorProjection.calculateGroundResolution(	centerLat,
																					built.zoom.byteValue(),
																					WORLD_PIXEL);
			// from the center, half the total width/height to the extremes
			Length width = MeasurementUtil.createLengthInMeters(metersPerPixel * pixelWidth / 2);
			Length height = MeasurementUtil.createLengthInMeters(metersPerPixel * pixelHeight / 2);

			built.bounds = BoundingBox.create()
					.setNorth(GeoMath.coordinateFrom(center, MeasurementUtil.NORTH, height).getLatitude())
					.setSouth(GeoMath.coordinateFrom(center, MeasurementUtil.SOUTH, height).getLatitude())
					.setEast(GeoMath.coordinateFrom(center, MeasurementUtil.EAST, width).getLongitude())
					.setWest(GeoMath.coordinateFrom(center, MeasurementUtil.WEST, width).getLongitude()).build();
		}

		/**
		 * @param built
		 */
		private void calculateZoom(GoogleMapsBounds built) {
			GeoCoordinate ne = bounds.getNorthEast();
			GeoCoordinate sw = bounds.getSouthWest();

			double latFraction = (ne.getLatitude().doubleValue(ExtraUnits.RADIAN_LATITUDE) - sw.getLatitude()
					.doubleValue(ExtraUnits.RADIAN_LATITUDE)) / Math.PI;

			double lngDiff = ne.getLongitude().getInDecimalDegrees() - sw.getLongitude().getInDecimalDegrees();
			double lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

			Integer latZoom = zoom(pixelHeight, WORLD_PIXEL, latFraction);
			Integer lngZoom = zoom(pixelWidth, WORLD_PIXEL, lngFraction);

			built.zoom = Ordering.natural().min(latZoom, lngZoom, ZOOM_MAX);
		}

		private Integer zoom(double mapPx, double worldPx, double fraction) {
			double worldFraction = mapPx / worldPx;
			final double log = Math.log(worldFraction / fraction);
			final double log2 = Math.log(2);
			return (int) Math.floor(log / log2);
		}

		public Builder() {
			super(new GoogleMapsBounds());
		}

		public Builder bounds(BoundingBox bounds) {
			this.bounds = bounds;
			return this;
		}

		public Builder pixelWidth(Integer pixelWidth) {
			this.pixelWidth = pixelWidth;
			return this;
		}

		public Builder pixelHeight(Integer pixelHeight) {
			this.pixelHeight = pixelHeight;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct GoogleMapsBounds */
	private GoogleMapsBounds() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return the zoom
	 */
	public Integer zoom() {
		return this.zoom;
	}

	/**
	 * @return
	 */
	public BoundingBox bounds() {
		return this.bounds;
	}

}
