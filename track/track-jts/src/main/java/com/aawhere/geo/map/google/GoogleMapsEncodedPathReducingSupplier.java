/**
 * 
 */
package com.aawhere.geo.map.google;

import javax.annotation.Nonnull;
import javax.measure.quantity.Length;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.joda.time.DateTime;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.TrackInfo;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.calc.TrackCreatorTrackpointCalculator;
import com.aawhere.track.process.calc.TrackToMultiLineStringConverter;
import com.aawhere.track.process.seek.filter.FrequencyTrackpointFilter;
import com.aawhere.track.process.seek.virtual.FixedQuantitySumVirtualTrackpointSeeker;
import com.aawhere.track.story.GoogleMapEncodedPathUserStory;
import com.aawhere.track.story.TrackUserStories;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * A supplier of GoogleMap's encoded encodedPath while reducing the resolution of the encodedPath
 * incrementally upon every request satisfying restrictions of URL length for static maps, but may
 * be useful in general for making smaller paths. This allows an optional first encodedPath that may
 * already have been produced (likely from a cache). For this reason the supporting information like
 * Track and {@link TrackSummary} are provided through a {@link Supplier} so they may lazy demand
 * the heavy resources.
 * 
 * Using {@link GoogleMapEncodedPathUserStory} to produce the path this will always use the
 * {@link FixedQuantitySumVirtualTrackpointSeeker} to provide a fixed distance between trackpoints
 * since providing multiple points too close to each other is not valuable for a spatial-only line
 * like the encoded path.
 * 
 * @author aroller
 * 
 */
public class GoogleMapsEncodedPathReducingSupplier
		implements Supplier<String> {

	private static final Integer DEFAULT_TRACKPOINT_REDUCTION_FACTOR = 2;
	private static final Length DEFAULT_SCALAR = MeasurementUtil.createLengthInMeters(10);
	private static final Length DEFAULT_MIN_SEGMENT_LENGTH = QuantityMath.create(DEFAULT_SCALAR)
			.dividedBy(DEFAULT_TRACKPOINT_REDUCTION_FACTOR).getQuantity();
	private static final int NO_GETS = 0;
	/** The goal of this to provide a scaled version of the path. */
	@XmlElement
	private String encodedPath;
	/** The number of trackpoints the current path has encoded. */
	@XmlAttribute
	private Integer numberOfTrackpoints;
	/** The segment length that the path is targeting. */
	@XmlElement
	private Length segmentLength;
	/**
	 * Provides the minimum segment length possible to ensure possible calculations don't go too
	 * micro.
	 */
	@XmlElement
	private Length minimumSegmentLength = DEFAULT_MIN_SEGMENT_LENGTH;
	/** Keeps track of how many attempts were made to know if the first default should be provided. */
	@XmlAttribute
	private Integer numberOfGets = NO_GETS;
	/** This is used to round the segment length to the scalar provided. */
	@XmlElement
	private Length fixedLengthScalar = DEFAULT_SCALAR;

	/**
	 * This is how much the numberOfTrackpoints is to be reduced with each call. The higher the
	 * number the bigger the reduction so the path will be smaller and less accurate quicker.
	 */
	private Integer trackpointReductionFactor = DEFAULT_TRACKPOINT_REDUCTION_FACTOR;

	/**
	 * Provides the track pair upon request. This should be lazy to avoid requesting resources until
	 * needed since the {@link #encodedPath} could also be supplied at build time and this may never
	 * be needed.
	 */
	private Supplier<TrackInfo> trackSupplier;
	private TrackInfo trackInfo;

	/**
	 * Used to construct all instances of GoogleMapsEncodedPathReducingSupplier.
	 */
	public static class Builder
			extends ObjectBuilder<GoogleMapsEncodedPathReducingSupplier> {

		public Builder() {
			super(new GoogleMapsEncodedPathReducingSupplier());
		}

		/**
		 * When an existing coded path is known, make this the first result which may satisfy the
		 * client.
		 */
		public Builder encodedPath(String encodedPath) {
			building.encodedPath = encodedPath;
			return this;
		}

		/**
		 * Provides a TrackSummary/Track pair that can provide the details needed to create an
		 * encoded path.
		 * 
		 * @param trackSupplier
		 * @return
		 */
		public Builder trackSupplier(Supplier<TrackInfo> trackSupplier) {
			building.trackSupplier = trackSupplier;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.e().assertMinimumNotNull(	"encodedPath,trackSupplier",
												1,
												building.trackSupplier,
												building.encodedPath);
		}

		@Override
		public GoogleMapsEncodedPathReducingSupplier build() {
			GoogleMapsEncodedPathReducingSupplier built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct GoogleMapsEncodedPathReducingSupplier */
	private GoogleMapsEncodedPathReducingSupplier() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * The first call will provide the given encoded path. Subsequent calls will reduce the
	 * resolution at a consistent interval (which could be assigned at build time).
	 */
	@Override
	@Nonnull
	public String get() {

		// the first get should return the provided if available.
		if (numberOfGets != NO_GETS || this.encodedPath == null) {
			if (this.isReductionCapable()) {

				if (this.trackInfo == null) {
					this.trackInfo = this.trackSupplier.get();
					this.numberOfTrackpoints = this.trackInfo.summary().getNumberOfTrackpoints();
					// target 1000 trackpoints as the ideal and reduce from there.
					this.trackpointReductionFactor = Math.max(1, this.numberOfTrackpoints / 1000);
				}

				// calculate the encoded path
				final Supplier<DateTime> timestampSupplier = Suppliers.ofInstance(null);
				Iterable<Trackpoint> track;

				// let the first attempt go all natural, otherwise simplify by frequency and length
				if (numberOfGets > NO_GETS) {

					TrackUserStories.Builder userStoriesBuilder = TrackUserStories.create(this.trackInfo.track());
					userStoriesBuilder.getSeekerChainBuilder().addSeeker(FrequencyTrackpointFilter.create()
							.setCountToSkip(this.trackpointReductionFactor));
					TrackToMultiLineStringConverter multiLineStringConverter = userStoriesBuilder
							.getSeekerChainBuilder().registerWithCurrentProcessor(TrackToMultiLineStringConverter
									.create().setMinDistanceBetweenTrackpoints(segmentLength));
					TrackCreatorTrackpointCalculator trackCreator = userStoriesBuilder.getSeekerChainBuilder()
							.registerWithCurrentProcessor(TrackCreatorTrackpointCalculator.create());
					userStoriesBuilder.build();
					MultiLineString simplifiedLineString = multiLineStringConverter.getMultiLineString();
					track = trackCreator.getTrack();
					track = Iterables.transform(Lists.newArrayList(simplifiedLineString.getCoordinates()),
												JtsTrackGeomUtil.trackpointFunction(timestampSupplier));
				} else {
					track = this.trackInfo.track();
				}

				TrackUserStories.Builder userStoriesBuilder = TrackUserStories.create(track);
				GoogleMapEncodedPathUserStory userStory = userStoriesBuilder.setUserStory(GoogleMapEncodedPathUserStory
						.create());
				userStoriesBuilder.build();
				this.encodedPath = userStory.getResult();
				// prepare for the next
				this.trackpointReductionFactor += this.trackpointReductionFactor;
			} else {
				throw new IllegalStateException(
						"calling get again invokes reduction, but no longer capable.  Call #isReductionCapable first to avoid this");
			}
		}
		this.numberOfGets++;
		return this.encodedPath;
	}

	/**
	 * Indicates if this is capable of reducing.
	 * 
	 * @return
	 */
	public Boolean isReductionCapable() {
		return this.trackSupplier != null

		&& (this.numberOfTrackpoints == null ||
		// basically this is saying that if we can't find a solution with 10 trackpoints then we
		// should give up
				(this.numberOfTrackpoints / this.trackpointReductionFactor) > 10);
	}
}
