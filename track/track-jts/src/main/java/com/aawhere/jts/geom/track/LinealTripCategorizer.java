/**
 * 
 */
package com.aawhere.jts.geom.track;

import com.aawhere.lang.ObjectBuilder;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Lineal;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.operation.distance.DistanceOp;

/**
 * A {@link LineString} or {@link MultiLineString} receiver that identifies the type of Trail the
 * path follows (Loop, Return, etc).
 * 
 * http://commontrack.jira.com/wiki/display/TRACKS/Track+Categorization
 * 
 * @author roller
 * 
 */
public class LinealTripCategorizer {

	public static enum Category {
		/** travel start/ending at the same point with very little overlap */
		COMPLETE_LOOP,
		/** travel starting/ending at the same point with significant overlap that leads to the loop */
		PARTIAL_LOOP,
		/**
		 * starting/ending at the same point while reaching a midpoint and returning back on the
		 * same path (aka out-and-back).
		 */
		RETURN_TRIP,
		/** starting/ending at different points without significant overlap */
		ONE_WAY
	};

	private Category category;

	private Geometry linealGeometry;

	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	private Double acceptableDistanceForReturnToStart = 1.0;

	private LinealTripCategorizer(Geometry linealGeometry) {
		this.linealGeometry = linealGeometry;
		if (!(linealGeometry instanceof Lineal)) {
			throw new IllegalArgumentException("This class requires a Lineal, but received "
					+ linealGeometry.getClass());
		}
	}

	private void process() {
		Coordinate[] coordinates = linealGeometry.getCoordinates();
		Coordinate firstCoordinate = coordinates[0];
		Coordinate lastCoordinate = coordinates[coordinates.length - 1];
		GeometryFactory factory = new GeometryFactory();
		Point firstPoint = factory.createPoint(firstCoordinate);
		Point lastPoint = factory.createPoint(lastCoordinate);

		boolean finishedAtStart = firstPoint.isWithinDistance(lastPoint, acceptableDistanceForReturnToStart);

		if (finishedAtStart) {
			Geometry unionedLineString = linealGeometry.union();
			int numberOfGeometries = unionedLineString.getNumGeometries();
			for (int whichSegment = 0; whichSegment < numberOfGeometries; whichSegment++) {
				LineString currentSegment = (LineString) unionedLineString.getGeometryN(whichSegment);

				for (int whichOppositeSegment = 0; whichOppositeSegment < numberOfGeometries; whichOppositeSegment++) {
					LineString oppositeSegment = (LineString) unionedLineString.getGeometryN(whichOppositeSegment);

					if (!oppositeSegment.equalsExact(currentSegment)) {
						new DistanceOp(currentSegment, oppositeSegment);
					}

				}

			}

		} else {
			this.category = Category.ONE_WAY;
		}

	}

	/**
	 * @return the acceptableDistanceForReturnToStart
	 */
	public Double getAcceptableDistanceForReturnToStart() {
		return acceptableDistanceForReturnToStart;
	}

	public static class Builder
			extends ObjectBuilder<LinealTripCategorizer> {

		/**
		 * @param building
		 */
		protected Builder(Geometry linealGeometry) {
			super(new LinealTripCategorizer(linealGeometry));

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public LinealTripCategorizer build() {
			building.process();
			return super.build();
		}

		/**
		 * @param acceptableDistanceForReturnToStart
		 *            the acceptableDistanceForReturnToStart to set
		 */
		public Builder setAcceptableDistanceForReturnToStart(Double acceptableDistanceForReturnToStart) {
			building.acceptableDistanceForReturnToStart = acceptableDistanceForReturnToStart;
			return this;
		}
	}

}
