/**
 * 
 */
package com.aawhere.jts.geom;

import javax.annotation.Nullable;

import com.vividsolutions.jts.geom.Geometry;

/**
 * Implemented by any class that provides a Java Topology Suite geometry. This is not idempotent so
 * that subsequent calls may produce different results based on the rules of the implementing class.
 * 
 * The type of geometry returned may also change, although unlikely in most cases.
 * 
 * @author aroller
 * 
 */
public interface JtsGeometryProvider {

	/**
	 * The geometry as provided by the implementing class.
	 * 
	 * @return The geometry or null if the geometry is not known (per the rules of the implementing
	 *         class).
	 */
	@Nullable
	public Geometry getGeometry();
}
