/**
 * 
 */
package com.aawhere.jts.geom.match;

import com.aawhere.jts.geom.JtsUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.track.JtsTrackGeomUtil;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * Common methods for determining track matching.
 * 
 * @author Brian Chapman
 * 
 */
public class JtsMatchUtil {

	/**
	 * Uses efficient methods to determine if there are any relationships at all between the source
	 * and the target.
	 * 
	 * The result is bidirectional so valid if the source and target are switched.
	 * 
	 * @return true if there is any touch, cross, intersection of the source with the target.
	 */
	public static boolean hasAnyRelationship(Geometry source, Geometry target) {
		return source.crosses(target) || source.touches(target) || source.intersects(target);
	}

	/**
	 * Are the two {@link Geometry}s identical?
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static boolean identical(Geometry source, Geometry target) {
		return source.equalsExact(target);
	}

	public static Geometry bufferedIntercetionLength(Geometry source, Geometry target, double bufferDistance) {
		Geometry bufferedSource = buffer(source, bufferDistance);
		return bufferedIntersectionLength(target, bufferedSource);
	}

	/**
	 * Special buffering for LineStrings. It will split the linestring into multiple line strings,
	 * buffer each and union them to create a similar, if not equal, buffer to what would be
	 * expected. This extra processing is required to get past memory overflow bug TN-206.
	 * 
	 * @deprecated use {@link JtsTrackGeomUtil#buffer(Iterable, javax.measure.quantity.Length)}
	 * 
	 * @param lineString
	 * @param bufferDistance
	 * @return
	 */
	public static Geometry buffer(Geometry source, double bufferDistance) {
		Geometry buffer = null;
		if (source instanceof LineString) {
			source = source.getFactory().createMultiLineString(new LineString[] { (LineString) source });
		}

		if (source instanceof MultiLineString) {

			MultiLineString sourceMultiLineString = (MultiLineString) source;

			for (int whichLineString = 0; whichLineString < sourceMultiLineString.getNumGeometries(); whichLineString++) {
				LineString lineString = (LineString) sourceMultiLineString.getGeometryN(whichLineString);

				// maximum is determined based on getting passed TN-206 and performance results
				// 500 causes overflow for test. 100 seems to perform quicker than 250
				MultiLineString multiLineString = JtsUtil.split(lineString, 100);
				for (int i = 0; i < multiLineString.getNumGeometries(); i++) {
					LineString lineStringSegment = (LineString) multiLineString.getGeometryN(i);
					Geometry segmentBuffer = lineStringSegment.buffer(bufferDistance);
					if (buffer == null) {
						buffer = segmentBuffer;
					} else {
						buffer = buffer.union(segmentBuffer);
					}
					Assertion.exceptions().assertTrue("valid segment buffer", buffer.isValid());
				}
			}
		} else {
			buffer = source.buffer(bufferDistance);
		}
		Assertion.exceptions().assertTrue("valid geometry", buffer.isValid());
		return buffer;
	}

	/**
	 * @param target
	 * @param bufferedSource
	 * @return
	 */
	public static Geometry bufferedIntersectionLength(Geometry bufferedSource, Geometry target) {
		Geometry targetMatch = bufferedSource.intersection(target);
		return targetMatch;
	}
}
