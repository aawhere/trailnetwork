/**
 *
 */
package com.aawhere.track.match.calc;

import static org.junit.Assert.*;

import javax.measure.quantity.Length;
import javax.measure.unit.USCustomarySystem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.JtsMeasureUtil;
import com.aawhere.measure.JtsMeasureUtilUnitTest;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.TrackSegment;

/**
 * @author Brian Chapman
 * 
 */
public class SameLocationTrackSegmentLcsPredicateUnitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 *             `
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMatch() {
		TrackSegment source = ExampleTracks.S1;
		TrackSegment target = ExampleTracks.S3;
		Length maxDistance = JtsMeasureUtilUnitTest.METERS_IN_ONE_DEG_LON;
		assertEquals(1d, JtsMeasureUtil.angleFromLength(maxDistance, source.getFirst().getLocation().getLatitude())
				.doubleValue(USCustomarySystem.DEGREE_ANGLE), 0.001);
		SameLocationTrackSegmentLcsPredicate predicate = SameLocationTrackSegmentLcsPredicate.create()
				.setSameLocationDistance(maxDistance).build();
		Boolean result = predicate.match(source, target);
		assertTrue("Segments are within tolarance", result);
	}

}
