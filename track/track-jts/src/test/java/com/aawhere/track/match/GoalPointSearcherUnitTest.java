/**
 * 
 */
package com.aawhere.track.match;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.SimpleTrackSegment;
import com.aawhere.track.TrackSegment;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.TrackpointTestUtil;

/**
 * @author aroller
 * 
 */
public class GoalPointSearcherUnitTest {

	/**
	 * 
	 */
	private static final SimpleTrackSegment LAND_IN_AREA_S2 = new SimpleTrackSegment(LOOP_2_RETURN_MIDDLE, P9);
	/**
	 * 
	 */
	private static final SimpleTrackSegment LAND_IN_AREA_S1 = new SimpleTrackSegment(P_MINUS_1, LOOP_2_RETURN_MIDDLE);

	/** A simple situation where the goal is the last point of the line. */
	@Test
	public void testExactMatchWithStartLine() {
		TrackSegment startSegment = S2;
		Trackpoint expectedGoal = startSegment.getFirst();
		GoalPointSearcher searcher = GoalPointSearcher.create().setGoalAreaRadius(S1_LENGTH)
				.setSegmentLeavingStart(startSegment).build();
		assertFalse("not even reaching the area", searcher.search(BEFORE_S1));
		assertTrue("segment ends at start line", searcher.search(S1));
		TrackpointTestUtil.assertTrackpointEquals("s1 ends at the start line", expectedGoal, searcher.getBest());

		assertTrue("already have a match, but just confirming this doesn't mess it up", searcher.search(startSegment));
		TrackpointTestUtil
				.assertTrackpointEquals("final point should match the goal", expectedGoal, searcher.getBest());

	}

	/** Ensures that a micro miss will be considered a crossing to avoid endpoint mismatches. */
	@Test
	public void testJustShortOfTheStartLine() {
		final TrackSegment segment = BEFORE_S1;
		GoalPointSearcher searcher = GoalPointSearcher.create().setGoalAreaRadius(S1_LENGTH)
				.setSegmentLeavingStart(S1_MICRO).build();
		assertTrue("segment stops at P1, but that is close enough for a match in the buffer.", searcher.search(segment));
		TrackpointTestUtil.assertTrackpointEquals(	"the last point of the segment should have matched",
													segment.getSecond(),
													searcher.getBest());
	}

	/** Matches a segment that starts just after the start line */
	@Test
	public void testJustPastTheFinishLine() {
		final TrackSegment segment = S1_MICRO;
		// use a big area to allow for the beyond segment to be within range still
		GoalPointSearcher searcher = GoalPointSearcher.create().setGoalAreaRadius(LINE_LENGTH)
		// .setGoalLineBufferLength(MeasurementUtil.createLengthInMeters(5))
				.setSegmentArrivingAtFinish(BEFORE_S1).build();
		assertTrue(	"segment starts at p1 micro missing the target segment, but should still qualify within the fat goal line",
					searcher.search(segment));
		TrackpointTestUtil.assertTrackpointEquals(	"the last point of the segment should have matched",
													segment.getFirst(),
													searcher.getBest());
	}

	@Test
	public void testExactmatchWithFinishLine() {
		TrackSegment finishSegment = S2;
		Trackpoint expectedGoal = finishSegment.getSecond();
		GoalPointSearcher searcher = GoalPointSearcher.create().setGoalAreaRadius(S1_LENGTH)
				.setSegmentArrivingAtFinish(finishSegment).build();
		assertFalse("not even reaching the area", searcher.search(BEFORE_S1));
		assertFalse("close, but finish not yet reached", searcher.search(S1));
		assertNull("s1 didn't make the area", searcher.getBest());

		assertTrue("land on the finish", searcher.search(finishSegment));
		TrackpointTestUtil
				.assertTrackpointEquals("final point should match the goal", expectedGoal, searcher.getBest());

	}

	/**
	 * This tests that being close enough to the goal provides a match when only reaching a point
	 * within the area and then going to a point outside the area. The length includes two segments
	 * ensuring that a point only 1 segment away will be in the area.
	 * 
	 * Since the entry angle is steeper than the exit the closest point will be on the entry.
	 * 
	 * <pre>
	 * 
	 *   P-1 (first point)    P0(boundary)    P1    P2(goal)   P3   P4(boundary)   ........     P9 (third point)
	 *                          
	 *     |--------------------------------------    LOOP_RETURN_MIDDLE(second point) ---------------------|
	 * 
	 * </pre>
	 * 
	 */
	@Test
	public void testLandInAreaThenExitNoLine() {
		Trackpoint goal = P2;
		GoalPointSearcher searcher = GoalPointSearcher.create().setGoalAreaRadius(LINE_LENGTH).setGoal(goal).build();

		assertFalse("although reached area, not close enough to be done", searcher.search(LAND_IN_AREA_S1));
		Trackpoint best = searcher.getBest();
		assertNotNull(best);
		assertTrue("exiting the area indicates the best was found", searcher.search(LAND_IN_AREA_S2));
		assertSame("the entry angle puts the line closer than the exit", best, searcher.getBest());
		Length bestDistance = TrackUtil.distanceBetween(best, goal);
		assertTrue("the closest point should be closer than a normal segment", QuantityMath.create(bestDistance)
				.lessThan(S1_LENGTH));
		assertTrue("the closest point should be along the line ", best.getLocation().getLongitude()
				.getInDecimalDegrees() < LOOP_2_RETURN_MIDDLE.getLocation().getLongitude().getInDecimalDegrees());
	}

	/**
	 * This is simlar path and scenario as {@link #testLandInAreaThenExitNoLine()}, but with a line
	 * the cross should be exactly on the axis between the goal and the middle loop return point.
	 * 
	 * To better challenge the interpolation the goal point has been moved to p3 instead of p2. This
	 * will make sure the second point is still in the area, but not exactly on the finish line. The
	 * chosen point should however be exactly on the finish line.
	 */
	@Test
	public void testLandInAreaThenExitWithLine() {
		GoalPointSearcher searcher = GoalPointSearcher.create().setGoalAreaRadius(LINE_LENGTH)
				.setSegmentArrivingAtFinish(S2).build();
		assertFalse("enter in to area, but not yet crossed the finish", searcher.search(LAND_IN_AREA_S1));
		Trackpoint bestAfterS1 = searcher.getBest();
		assertNotNull("in the area match", bestAfterS1);
		assertTrue("exit the area and cross the finish", searcher.search(LAND_IN_AREA_S2));
		Trackpoint bestAfterS2 = searcher.getBest();
		assertNotSame("a new best should have been found", bestAfterS1, bestAfterS2);
		MeasureTestUtil.assertEquals(P3.getLocation().getLatitude(), bestAfterS2.getLocation().getLatitude());
	}

	/**
	 * A segment that crosses the line, but a trackpoint doesn't land exactly on it. This uses a
	 * small area to ensure the span is detected.
	 * 
	 * 
	 */
	@Test
	public void testPanOverStartLine() {
		TrackSegment startSegment = S3;
		Trackpoint expectedGoal = startSegment.getFirst();
		GoalPointSearcher searcher = GoalPointSearcher.create()
				.setGoalAreaRadius(MeasurementUtil.createLengthInMeters(1)).setSegmentLeavingStart(startSegment)
				.build();
		assertFalse("s1 doesn't come close", searcher.search(S1));
		assertTrue("span over the goal", searcher.search(S2_S3_COMBINED));
		Trackpoint bestAfterS2 = searcher.getBest();
		TrackpointTestUtil.assertTrackpointEquals(	"should have interpolated the exact goal point",
													expectedGoal,
													bestAfterS2);
	}

	/**
	 * Tests only the area with no goal line provided.
	 * 
	 */
	@Test
	public void testExactMatchInAreaNoLine() {
		Trackpoint expectedGoal = P2;
		GoalPointSearcher searcher = GoalPointSearcher.create().setGoalAreaRadius(S1_LENGTH).setGoal(expectedGoal)
				.build();
		assertFalse("goal area not reached", searcher.search(BEFORE_S1));
		assertNull("p1 really close to the edge of the area", searcher.getBest());
		assertFalse("exactly close, but still not sure if we are done", searcher.search(S1));
		TrackpointTestUtil.assertTrackpointEquals(	"second segment ends with goal point",
													expectedGoal,
													searcher.getBest());
		assertTrue("we already matched exactly so this won't do better...mark it as best", searcher.search(S2));
		TrackpointTestUtil.assertTrackpointEquals("still super close", expectedGoal, searcher.getBest());
		assertTrue("leaving the area marks the best", searcher.search(S3));
		TrackpointTestUtil
				.assertTrackpointEquals("final point should match the goal", expectedGoal, searcher.getBest());
	}

}
