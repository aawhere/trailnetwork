/**
 *
 */
package com.aawhere.track.process.calc;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.measure.quantity.Length;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.JtsTrackGeomUtil;
import com.aawhere.track.Track;
import com.aawhere.track.TrackUnitTest;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.processor.AutomaticTrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * @author roller
 * @see TrackToJtsPointConverter
 * 
 */
public class TrackToJtsConverterUnitTest {

	@Test
	public void testInspection() {

		Track track = TrackUnitTest.getInstance().generateRandomTrack();
		AutomaticTrackProcessor.Builder builder = new AutomaticTrackProcessor.Builder(track);
		TrackToJtsPointConverter listener = new TrackToJtsPointConverter.Builder().registerWithProcessor(builder)
				.build();
		builder.build();
		List<CoordinateSequence> sequences = listener.getSequences();
		assertNotNull(sequences);
		assertEquals(1, sequences.size());
		CoordinateSequence sequence = sequences.get(0);
		int trackSize = CollectionUtils.size(track.iterator());
		assertEquals(trackSize, sequence.size());
	}

	@Test
	public void testMultiLineStringConverterMinimum() {

		assertMultiLineConverter(S1_POINTS, S1_POINTS.size(), new Boolean[] { true, true });
	}

	@Test
	public void testDuplicateLocationNoSimplification() {
		List<Trackpoint> lineWithRepeats = LINE_LINGER_AT_END_THEN_RETURN_POINTS;
		Boolean[] expectedIntersects = new Boolean[lineWithRepeats.size()];
		Arrays.fill(expectedIntersects, true);
		assertMultiLineConverter(lineWithRepeats, lineWithRepeats.size(), expectedIntersects);
	}

	@Test
	public void testDuplicateLocationWithSimplification() {
		// THIS IS the big test here. Notice We gave a bunch, but only received few points
		int numberOfExpectedPoints = LINE_POINTS.size();

		List<Trackpoint> lineWithRepeats = LINE_LINGER_AT_END_THEN_RETURN_POINTS;

		Boolean[] expectedIntersects = new Boolean[LINE_LINGER_AT_END_THEN_RETURN_POINTS.size()];
		Arrays.fill(expectedIntersects, true);
		assertMultiLineConverter(	lineWithRepeats,
									numberOfExpectedPoints,
									expectedIntersects,
									MeasurementUtil.createLengthInMeters(10));

	}

	/**
	 * Since multi line string is multiple line points without location will create gaps. Provided
	 * the expected results in boolean form so we can cofirm the multi-line will properly skip those
	 * missingpoints.
	 * 
	 * @param trackpoints
	 * @param intersects
	 *            an index matching the points explaining if the points is expected to intersect.
	 */
	private void assertMultiLineConverter(List<Trackpoint> trackpoints, Integer expectedCoordinateCount,
			Boolean[] intersects) {
		assertMultiLineConverter(trackpoints, expectedCoordinateCount, intersects, null);
	}

	private TrackToMultiLineStringConverter assertMultiLineConverter(List<Trackpoint> trackpoints,
			Integer expectedCoordinateCount, Boolean[] intersects, Length simplificationLength) {
		// this is junk...only built because required.Unit testing directly
		ManualTrackProcessor.Builder processorBuilder = new ManualTrackProcessor.Builder();
		TrackToMultiLineStringConverter converter = new TrackToMultiLineStringConverter.Builder()
				.registerWithProcessor(processorBuilder).setMinDistanceBetweenTrackpoints(simplificationLength).build();

		for (Trackpoint trackpoint : trackpoints) {
			converter.handle(trackpoint, null);

		}
		// finish is required to muilt the geometry
		converter.finish();

		MultiLineString multiLineString = converter.getMultiLineString();
		assertNotNull(multiLineString);
		assertTrue("line string is not valid", multiLineString.isValid());
		Coordinate[] coordinates = multiLineString.getCoordinates();
		assertEquals("incorrect coordinate count", expectedCoordinateCount.intValue(), coordinates.length);
		assertEquals(	"intersections should match trackpoints, not result coordinates",
						trackpoints.size(),
						intersects.length);
		int index = 0;
		for (Trackpoint trackpoint : trackpoints) {
			assertEquals(	trackpoint.toString() + " doesn't intersect for index = " + index,
							intersects[index],
							multiLineString.intersects(JtsTrackGeomUtil.convertToPoint(trackpoint)));
			index++;
		}
		return converter;
	}

	/**
	 * Two lines with a location gap in the middle.
	 * 
	 */
	@Test
	public void testMultLineStringConverterTwoLines() {
		// the middle point is missing location so this should create two segments. The intersection
		// will verify this.
		assertMultiLineConverter(LINE_BEFORE_AND_AFTER_MISSING_LOCATION_POINTS, 4, new Boolean[] { true, true, false,
				true, true });
	}
}
