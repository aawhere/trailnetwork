/**
 * 
 */
package com.aawhere.track.match;

import static com.aawhere.measure.MeasurementUtil.*;
import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.ExampleTracks.PointBuilder;
import com.aawhere.track.SimpleTrackSegment;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.match.CourseAttemptDeviationProcessor.Builder;
import com.aawhere.track.process.TrackProcessorUtil;
import com.aawhere.track.process.processor.ManualTrackProcessor;

/**
 * Tests {@link CourseAttemptDeviationProcessor} and it's interaction with
 * {@link CourseAttemptDeviationSeeker} and {@link SlidingBufferTrackpointSeeker} using simple,
 * short tracks to test the most basic situations.
 * 
 * @author aroller
 * 
 */
public class CourseAttemptDeviationProcessorUnitTest {

	private CourseAttemptDeviationProcessor deviationProcessor;
	private ManualTrackProcessor processor;
	private static final Length RECOMMENDED_BUFFER_LENGTH = QuantityMath.create(S1_LENGTH).dividedBy(4).getQuantity();

	/**
	 * Course and Attempt tracks are the same and the buffer is the length of the course. The
	 * easiest situation to prove a match.
	 * 
	 */
	@Test
	public void testSameAsCourseFullLengthBuffer() {
		Track course = LINE_BEFORE_AFTER;
		Track attempt = course;
		// buffer length is the full length of the course
		Length deviationLength = TrackProcessorUtil.length(course);
		setUpCourse(course, deviationLength);
		assertTrue(	"the buffer length is the whole course so it should have seeked to the end",
					deviationProcessor.endOfCourseReached());
		int numberOfDeviationsExpected = 0;
		Iterator<Trackpoint> attemptIterator = attempt.iterator();
		assertAll(numberOfDeviationsExpected, attemptIterator);
	}

	/**
	 * To prove the buffers grow properly we need two lines that are longer and a buffers that use
	 * multiple points.
	 * 
	 */
	@Test
	public void testSuperLongLineThreeLengthBuffer() {
		int lengthFactor = 3;
		assertSuperLong(lengthFactor);
	}

	/**
	 * @param bufferLengthFactor
	 */
	private void assertSuperLong(int bufferLengthFactor) {
		Track cours = LINE_SUPER_LONG;
		setUpCourse(cours, QuantityMath.create(RECOMMENDED_BUFFER_LENGTH).times(bufferLengthFactor).getQuantity());
		assertAll(0, cours.iterator());
	}

	/**
	 * @see #testSuperLongLineThreeLengthBuffer()
	 * 
	 */
	@Test
	public void testSuperLongLineFiveLengthBuffer() {
		assertSuperLong(5);
	}

	/**
	 * When the course and attempt are the same, but the but the buffer/deviation length is one
	 * segment long.
	 */
	@Test
	public void testSameAsCourseShortLength() {
		Track course = LINE_BEFORE_AFTER;
		assertSameTrackMatches(course);
	}

	/**
	 * Demonstrates where the course and attempt do double loops making a positive match.
	 * 
	 */
	@Test
	public void testSameDoubleLoopShortLength() {
		assertSameTrackMatches(DOUBLE_LOOP);
	}

	/**
	 * Shows that a attempt that only does one lap of a two lap course will fail.
	 * 
	 */
	@Test
	public void testDoubleLoopCourseSingleLoopAttempt() {
		Track course = DOUBLE_LOOP;
		setUpCourse(course, RECOMMENDED_BUFFER_LENGTH);
		assertLoop1();
		assertFalse("there should still be more buffer to process", deviationProcessor.endOfCourseReached());
		// at this point the attempt provider realizes there are no more points to provide.
		this.processor.finish();
		assertFalse("the buffer should never have reached the end.", deviationProcessor.endOfCourseReached());

		// knowing the buffer wasn't completed, but the attempt indicates a failed finish, but
		// that is left up to others.
		// This is only a deviation processor, complete matching is left up to others
		// we may need to provide a way for others to understand the distance remaining in the
		// buffer.
	}

	/**
	 * Attempt deviates and returns to course at a later spot. The buffer is the entire length so we
	 * can test the deviation is logged appropriately. A small buffer makes the seeker fail early so
	 * we use a long one. Other tests should verify sliding.
	 * 
	 * <pre>
	 *    _
	 * __| |___  = attempt
	 * (_________)  = course
	 * </pre>
	 */
	@Test
	public void testDeviationShortBuffer() {
		setUpCourse(LINE_SUPER_LONG, TrackProcessorUtil.length(LINE_SUPER_LONG));
		PointBuilder attempt = new ExampleTracks.PointBuilder();
		assertNext("first point on course", 0, ZERO_LENGTH, attempt);
		assertDeviationExceeded(false);
		assertNext("second point on course", 0, ZERO_LENGTH, attempt);
		assertDeviationExceeded(false);
		Trackpoint point3 = attempt.east();
		QuantityMath<Length> expectedDeviation = QuantityMath.create(new SimpleTrackSegment(P2, P3).getDistance());
		assertDeviation("third deviates from course", 0, expectedDeviation.getQuantity(), point3);
		assertDeviationExceeded(false);
		Trackpoint paralallel = attempt.next();
		expectedDeviation = expectedDeviation.plus(attempt.segmentLength());
		assertDeviation("travel in deviation, but not far enough to log",
						0,
						expectedDeviation.getQuantity(),
						paralallel);
		assertDeviationExceeded(false);

		Trackpoint returnToCourse = attempt.west();
		expectedDeviation = expectedDeviation.plus(attempt.segmentLength());
		assertDeviation("sixth returns so deviation expected", 1, ZERO_LENGTH, returnToCourse);
		assertDeviationLength(Index.FIRST, expectedDeviation.getQuantity());
		assertDeviationExceeded(false);

		assertNext("back to normal", 1, attempt);
		assertNext("back to normal", 1, attempt);
		assertNext("back to normal", 1, attempt);
		assertDeviationExceeded(false);
		MeasureTestUtil.assertEquals(	expectedDeviation.getQuantity(),
										TrackProcessorUtil.length(deviationProcessor.getDeviations().get(0)));
		this.processor.finish();
		assertDeviationExceeded(false);
		assertEquals("finish should not add anything", 1, this.deviationProcessor.getDeviations().size());
	}

	/**
	 * @param expectedDeviationExceeded
	 */
	private void assertDeviationExceeded(Boolean expectedDeviationExceeded) {
		assertEquals(	"deviation exceeded",
						expectedDeviationExceeded,
						this.deviationProcessor.isDeviationLengthExceeded());
	}

	/**
	 * Proves multiple deviations can be logged by deviating twice against a complete length buffer.
	 * 
	 * <pre>
	 *   _   _
	 * _| |_| |_  = attempt
	 * 
	 * (_________)  = course
	 * 
	 * </pre>
	 */
	@Test
	public void testDoubleDeviationFullBuffer() {
		setUpCourse(LINE_SUPER_LONG, TrackProcessorUtil.length(LINE_SUPER_LONG));
		PointBuilder attempt = new ExampleTracks.PointBuilder();
		assertNext("first point on course", 0, ZERO_LENGTH, attempt);
		assertNext("second point on course", 0, ZERO_LENGTH, attempt);
		Trackpoint startOfDeviation1 = attempt.east();
		assertDeviation("third point off course", 0, attempt.segmentLength(), startOfDeviation1);
		assertDeviationExceeded(false);
		QuantityMath<Length> expectedDeviation = QuantityMath.create(attempt.segmentLength());
		Trackpoint point4 = attempt.next();
		expectedDeviation = expectedDeviation.plus(attempt.segmentLength());
		assertDeviation("fourth point parallel", 0, expectedDeviation.getQuantity(), point4);
		assertDeviationExceeded(false);
		Trackpoint returnFromDeviation1 = attempt.west();
		expectedDeviation = expectedDeviation.plus(attempt.segmentLength());
		Length lengthOfFirstDeviation = expectedDeviation.getQuantity();
		assertDeviation("fifth returns to find a deviation occurred", 1, ZERO_LENGTH, returnFromDeviation1);
		assertDeviationExceeded(false);
		assertDeviationLength(Index.FIRST, lengthOfFirstDeviation);
		assertNext("sixth normal travel", 1, ZERO_LENGTH, attempt);
		assertDeviationExceeded(false);
		// DEVIATION 2
		Trackpoint startOfDeviation2 = attempt.east();
		expectedDeviation = QuantityMath.create(attempt.segmentLength());
		assertDeviation("second exit from course", 1, expectedDeviation.getQuantity(), startOfDeviation2);
		assertDeviationExceeded(false);
		Trackpoint devation2Segment = attempt.next();
		expectedDeviation = expectedDeviation.plus(attempt.segmentLength());
		assertDeviation("north and east segments are calulated", 1, expectedDeviation.getQuantity(), devation2Segment);
		Trackpoint devation2Return = attempt.west();
		expectedDeviation = expectedDeviation.plus(attempt.segmentLength());
		assertDeviationExceeded(false);
		int numberOfDeviationsExpected = 2;
		assertDeviation("return logs deviation", numberOfDeviationsExpected, ZERO_LENGTH, devation2Return);
		Index whichDeviation = Index.SECOND;
		Length expectedLengthForDeviation = expectedDeviation.getQuantity();
		assertDeviationLength(whichDeviation, expectedLengthForDeviation);
		this.processor.finish();
		assertEquals("finish shouldn't add anything", numberOfDeviationsExpected, this.deviationProcessor
				.getDeviations().size());
	}

	/**
	 * @param whichDeviation
	 * @param expectedLengthForDeviation
	 */
	private void assertDeviationLength(Index whichDeviation, Length expectedLengthForDeviation) {
		MeasureTestUtil.assertEquals(expectedLengthForDeviation, TrackProcessorUtil.length(this.deviationProcessor
				.getDeviations().get(whichDeviation.index)));
	}

	@Test
	public void testDeviationReturnToExitShortBuffer() {
		Length bufferLength = RECOMMENDED_BUFFER_LENGTH;
		assertDeviationSameEntryAsExit(bufferLength);
	}

	@Test
	public void testDeviationReturnToExitMediumBuffer() {
		Length bufferLength = LINE_LENGTH;
		assertDeviationSameEntryAsExit(bufferLength);
	}

	@Test
	public void testDeviationReturnToExitLongBuffer() {
		Length bufferLength = QuantityMath.create(LINE_LENGTH).times(2).getQuantity();
		assertDeviationSameEntryAsExit(bufferLength);
	}

	/**
	 * A deviation where the attempt exits and returns at the same point after doing a small loop
	 * deviation.
	 * 
	 * <pre>
	 * 
	 *   __
	 * __\/__  = attempt --> North
	 * 
	 * (_)________  = course
	 * 
	 * </pre>
	 */
	private void assertDeviationSameEntryAsExit(Length bufferLength) {
		setUpCourse(LINE_SUPER_LONG, bufferLength);
		PointBuilder attempt = new ExampleTracks.PointBuilder();
		assertNext("start point", 0, ZERO_LENGTH, attempt);
		assertNext("second point on course", 0, ZERO_LENGTH, attempt);
		assertNext("third point is the exit point", 0, ZERO_LENGTH, attempt);
		this.processor.notifyTrackpointListeners(attempt.southWest());
		this.processor.notifyTrackpointListeners(attempt.next());
		this.processor.notifyTrackpointListeners(attempt.next());
		this.processor.notifyTrackpointListeners(attempt.southEast());
		this.processor.notifyTrackpointListeners(attempt.next());
		this.processor.notifyTrackpointListeners(attempt.next());
		this.processor.finish();
		List<Track> deviations = this.deviationProcessor.getDeviations();
		assertEquals(	"at least the loop of should be logged as deviation, but perhaps even the track if the segment lagged behind the buffer "
								+ deviations,
						1,
						deviations.size());
		// the length is unpredictable right now, but it currently holds 3 of the 4 segments of
		// deviation
		// assertDeviationLength(Index.FIRST,
		// QuantityMath.create(S1_LENGTH).times(2).getQuantity());
	}

	/**
	 * 
	 */
	private void assertLoop1() {
		Iterator<Trackpoint> attemptIterator = LOOP.iterator();
		assertNext("start", 0, attemptIterator);
		MeasureTestUtil.assertEquals(ZERO_LENGTH, this.deviationProcessor.getLengthOfCurrentDeviation());
		assertNext("midpoint of line", 0, attemptIterator);
		MeasureTestUtil.assertEquals(ZERO_LENGTH, this.deviationProcessor.getLengthOfCurrentDeviation());
		assertNext("end of line", 0, attemptIterator);
		MeasureTestUtil.assertEquals(ZERO_LENGTH, this.deviationProcessor.getLengthOfCurrentDeviation());
		assertNext("midpoint of loop return", 0, attemptIterator);
		MeasureTestUtil.assertEquals(ZERO_LENGTH, this.deviationProcessor.getLengthOfCurrentDeviation());
		assertNext("back at beginning of Second loop", 0, attemptIterator);
		MeasureTestUtil.assertEquals(ZERO_LENGTH, this.deviationProcessor.getLengthOfCurrentDeviation());
	}

	/**
	 * A attempt that does two laps will successfully finish the course after one lap. Notice that
	 * the logic to stop providing attempts is not built into the
	 * {@link CourseAttemptDeviationProcessor} because it is receiving attempt trackpoints. That
	 * responsibility is delegated to {@link TrackTimingRelationCalculator} in coordination with
	 * Goal Line Detection. So the second loop will eventually exceed deviation length.
	 * 
	 */
	@Test
	public void testSingleLoopCourseDoubleLoopAttempt() {
		setUpCourse(LOOP, RECOMMENDED_BUFFER_LENGTH);
		assertLoop1();
		assertTrue("course is complete", deviationProcessor.endOfCourseReached());
		// first loop is an exact match,
		Length expectedLengthOfFinalMissingSegment = S1_LENGTH;
		assertDeviation("loop 2 middle is starting to go beyond course",
						0,
						expectedLengthOfFinalMissingSegment,
						LOOP_2_MIDDLE);
		assertDeviationExceeded(true);
		assertDeviation("another attempt to add does nothing since exceeded",
						0,
						expectedLengthOfFinalMissingSegment,
						LOOP_2_FINISH_LINE);
		// we can't return to the middle loop return because that's where the buffer is stopped.
		this.processor.finish();
		assertEquals("that last missing segment should be logged now after finish", 1, deviationProcessor
				.getDeviations().size());
		assertDeviationLength(Index.FIRST, expectedLengthOfFinalMissingSegment);
		// the trailing should have been emptied.
		MeasureTestUtil.assertEquals(ZERO_LENGTH, this.deviationProcessor.getLengthOfCurrentDeviation());

	}

	/**
	 * Given a course this creates a attempt of the exact same route. This uses a small buffer
	 * length (small being the length of each provided segment). This confirms no deviations are
	 * caused all the way through the finish.
	 * 
	 * @param course
	 */
	private void assertSameTrackMatches(Track course) {
		Iterator<Trackpoint> attemptIterator = course.iterator();
		setUpCourse(course, RECOMMENDED_BUFFER_LENGTH);
		assertFalse("the buffer should just be at the start", deviationProcessor.endOfCourseReached());
		assertAll(0, attemptIterator);
	}

	/**
	 * @param numberOfDeviationsExpected
	 * @param attemptIterator
	 */
	private void assertAll(int numberOfDeviationsExpected, Iterator<Trackpoint> attemptIterator) {
		int count = 0;
		while (attemptIterator.hasNext()) {
			assertNext(String.valueOf(count++), numberOfDeviationsExpected, attemptIterator);
		}
		// finish must be called to make sure any trailing deviations are recorded
		this.processor.finish();
		List<Track> deviations = deviationProcessor.getDeviations();
		assertEquals("deviation expected for finish " + deviations, numberOfDeviationsExpected, deviations.size());

	}

	private void assertNext(String context, int numberOfDeviationsExpected, Iterator<Trackpoint> attemptIterator) {
		assertNext(context, numberOfDeviationsExpected, null, attemptIterator);
	}

	/**
	 * @param numberOfDeviationsExpected
	 * @param attemptIterator
	 * @param deviations
	 */
	private void assertNext(String context, int numberOfDeviationsExpected, Length deviationLength,
			Iterator<Trackpoint> attemptIterator) {
		Trackpoint next = attemptIterator.next();
		assertDeviation(context, numberOfDeviationsExpected, deviationLength, next);
	}

	/**
	 * @param numberOfDeviationsExpected
	 * @param next
	 */
	private void
			assertDeviation(String context, int numberOfDeviationsExpected, Length deviationLength, Trackpoint next) {
		List<Track> deviations = deviationProcessor.getDeviations();
		processor.notifyTrackpointListeners(next);
		assertEquals(context + " deviation expected for " + next, numberOfDeviationsExpected, deviations.size());
		if (deviationLength != null) {
			MeasureTestUtil.assertEquals(deviationLength, this.deviationProcessor.getLengthOfCurrentDeviation());
		}

	}

	/**
	 * @param course
	 * @param deviationLength
	 * @return
	 */
	private CourseAttemptDeviationProcessor setUpCourse(Track course, Length deviationLength) {
		Builder builder = CourseAttemptDeviationProcessor.create().setAllowedDeviationLength(deviationLength)
				.setBufferWidth(CourseAttemptDeviationSeekerUnitTest.BUFFER_WIDTH).setCourse(course);

		ManualTrackProcessor.Builder driverBuilder = ManualTrackProcessor.create();
		builder.registerWithProcessor(driverBuilder);
		this.deviationProcessor = builder.build();
		setUpDriverProcessor(driverBuilder, this.deviationProcessor);
		this.processor = driverBuilder.build();
		return deviationProcessor;
	}

	/**
	 * Allows for extensions to add additional listeners to the main processor that handles
	 * trackpoints. The {@link #deviationProcessor} will always be notified before additional.
	 * 
	 * @param deviationProcessor2
	 * 
	 * @param course
	 * @param deviationLength
	 * @return
	 */
	protected void setUpDriverProcessor(ManualTrackProcessor.Builder driverBuilder,
			CourseAttemptDeviationProcessor deviationProcessor2) {
		// available for children.
	}

}
