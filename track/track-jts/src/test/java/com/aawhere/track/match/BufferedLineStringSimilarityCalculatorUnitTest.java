/**
 *
 */
package com.aawhere.track.match;

import static com.aawhere.jts.geom.JtsTestUtil.*;
import static org.junit.Assert.*;

import javax.measure.quantity.Length;
import javax.measure.unit.USCustomarySystem;

import org.junit.Test;

import com.aawhere.jts.geom.JtsTestUtil;
import com.aawhere.jts.geom.match.JtsMatchUtil;
import com.aawhere.measure.JtsMeasureUtil;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.ExtraUnits;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;

/**
 * See <a href="http://aawhere.jira.com/wiki/display/TRACKS/Track+Matching">wiki</a> for more
 * details.
 * 
 * @author roller
 * 
 */
public class BufferedLineStringSimilarityCalculatorUnitTest {

	/**
	 * The length used to determine the buffer size.
	 * 
	 */
	private static final Length TEST_BUFFER_LENGTH = MeasurementUtil.createLengthInMeters(20);

	public static final Latitude TEST_LATITUDE = MeasurementUtil.createLatitude((double) JtsTestUtil.CENTER_VALUE);

	final Boolean IDENTICAL = true;

	/**
	 * @see #test(Geometry, Geometry, Double, String)
	 * 
	 * @param source
	 * @param target
	 * @param expectedSimilarityRatio
	 * @param messageExplainingExpected
	 */
	protected void assertSimilarity(Geometry source, Geometry target, Ratio expectedSimilarityRatio,
			String messageExplainingExpected) {
		final Boolean NOT_IDENTICAL = false;
		assertSimilarity(source, target, expectedSimilarityRatio, NOT_IDENTICAL, messageExplainingExpected);

	}

	/**
	 * 
	 * @param source
	 * @param target
	 * @param expectedSimilarityRatio
	 * @param identical
	 * @param messageExplainingExpected
	 */
	public static void assertSimilarity(Geometry sourceGeo, Geometry targetGeo, Ratio expectedSimilarityRatio,
			Boolean identical, String messageExplainingExpected) {
		BufferedLineStringSimilarityCalculator calculator = new BufferedLineStringSimilarityCalculator.Builder()
				.setSourceGeometry(sourceGeo).setTargetGeometry(targetGeo).setSourceBufferedGeometry(buffer(sourceGeo))
				.setTargetBufferedGeometry(buffer(targetGeo)).build();
		Ratio similarityRatio = calculator.getSourceRatio();
		// five percent for buffer allowance given small distances
		final double delta = 0.05;
		assertEquals(	messageExplainingExpected,
						expectedSimilarityRatio.doubleValue(ExtraUnits.RATIO),
						similarityRatio.doubleValue(ExtraUnits.RATIO),
						delta);
	}

	/**
	 * Buffers the lines for the tests.
	 * 
	 * @param sourceGeo
	 * @return
	 */
	private static Geometry buffer(Geometry line) {
		double length = JtsMeasureUtil.angleFromLength(TEST_BUFFER_LENGTH, TEST_LATITUDE)
				.doubleValue(USCustomarySystem.DEGREE_ANGLE);
		return JtsMatchUtil.buffer(line, length);
	}

	@Test
	public void testSame() {
		assertSimilarity(	JtsTestUtil.getVerticalCenterLine(),
							JtsTestUtil.getVerticalCenterLine(),
							BufferedLineStringSimilarityCalculator.RATIO_TOTAL_MATCH,
							IDENTICAL,
							"these are the same");
	}

	@Test
	public void testCrossing() {
		// the distance is the amount it takes to cross the buffer itself.
		Ratio ratio = MeasurementUtil.createRatio(JtsTestUtil.BUFFER_DISTANCE.getValue());
		assertSimilarity(	JtsTestUtil.getVerticalCenterLine(),
							JtsTestUtil.getHorizontalCenterLine(),
							ratio,
							"lines cross, but that is all.");
	}

	@Test
	public void testHalfCovers() {
		Ratio HALF = MeasurementUtil.createRatio(0.50);
		assertSimilarity(	JtsTestUtil.getHorizontalCenterLine(),
							JtsTestUtil.getL(),
							HALF,
							"The L joins the horizontal source at the middle of the line");
		assertSimilarity(	JtsTestUtil.getL(),
							JtsTestUtil.getHorizontalCenterLine(),
							HALF,
							"The horizontal line joins the source L and it's corner which is half");
	}

	@Test
	public void testNoTouch() {
		assertSimilarity(	JtsTestUtil.getVerticalCenterLine(),
							JtsTestUtil.getVerticalEndLine(),
							BufferedLineStringSimilarityCalculator.RATIO_NO_MATCH,
							"these are parallel lines that never cross");
		assertSimilarity(	getHorizontalCenterLine(),
							getVerticalLineMissingCenter(),
							BufferedLineStringSimilarityCalculator.RATIO_NO_MATCH,
							"The horizontal line that crosses a multiline with a missing center has not match");
	}

	@Test
	public void testOverlapsAll() {
		assertSimilarity(	JtsTestUtil.getVerticalCenterLineExtended(),
							JtsTestUtil.getVerticalCenterLine(),
							MeasurementUtil.createRatio(LINE_LENGTH, VERTICAL_CENTER_LINE_EXTENDED_LENGTH),
							"2 segments are similar, but another segment goes beyond");
		assertSimilarity(	JtsTestUtil.getVerticalCenterLine(),
							JtsTestUtil.getVerticalCenterLineExtended(),
							BufferedLineStringSimilarityCalculator.RATIO_TOTAL_MATCH,
							"all of vertical line is overlapped by the same, but longer");
	}

	@Test
	public void testAllContained() {
		assertSimilarity(	JtsTestUtil.getVerticalCenterLine(),
							JtsTestUtil.getVerticalCenterLineExtended(),
							BufferedLineStringSimilarityCalculator.RATIO_TOTAL_MATCH,
							"all of the center line is contained within the extended");
	}

	/**
	 * 
	 * 
	 * __
	 * 
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testOutAndBack() throws ParseException {
		Geometry outAndBack = getOutAndBack();
		Geometry outAndBackExtended = getOutAndBackExtended();

		assertSimilarity(	outAndBack,
							outAndBackExtended,
							BufferedLineStringSimilarityCalculator.RATIO_TOTAL_MATCH,
							"the source that goes from bottom to center and returns to bottom is 100% contained inside one that does the same but goes to the top and back");

	}

	@Test
	public void testOutAndBackExtended() {
		assertSimilarity(	getOutAndBackExtended(),
							getOutAndBack(),
							MeasurementUtil.createRatio(0.5),
							"1 segment common, then one segment is not, returning on another non common segment and then another common segments results in half similar");

	}

	@Test
	public void testMissingCenterAgainstCenterLine() {
		assertSimilarity(	JtsTestUtil.getVerticalLineMissingCenter(),
							JtsTestUtil.getVerticalCenterLine(),
							BufferedLineStringSimilarityCalculator.RATIO_TOTAL_MATCH,
							"The lines are the same, except the source is missing a section.  It is completely overlapped by the regular");
	}

	@Test
	public void testMissingCenter() {
		// line length is two minus one segment that is missing from the center.
		assertSimilarity(	JtsTestUtil.getVerticalCenterLine(),
							JtsTestUtil.getVerticalLineMissingCenter(),
							MeasurementUtil.createRatio(0.5),
							"These two lines are near equal, but one has a whole section missing from the middle. 1/2 sections missing = one half ");

	}

	@Test
	public void testRightLoopStartingUpAgainstCenterLine() {
		assertSimilarity(	getRightLoopStartingUp(),
							getVerticalCenterLine(),
							MeasurementUtil.createRatio(LINE_LENGTH / LOOP_LENGTH),
							"Loop overlaps all of the center line, but 2/3 of loop is not common");

	}

	@Test
	public void testCenterLineStartingUpAgainstRightLoop() {
		assertSimilarity(	getVerticalCenterLine(),
							getRightLoopStartingUp(),
							BufferedLineStringSimilarityCalculator.RATIO_TOTAL_MATCH,
							"Center line is completely visited at the beginning and then the end of the loop");

	}

	/**
	 * each loop is 2 segments for each vertical line and 1 segment for each horizontal making each
	 * loop = 6. Two loops is a distance of 12.
	 * 
	 * The vertical center line is overlapped 100% twice making 4 segments overlapping and 8
	 * segments not. So 4/12 coverage
	 * 
	 * Single loop works fine since difference and intersection give credit for one line
	 * 
	 * @see #testRightLoopStartingUpAgainstCenterLine()
	 */
	@Test
	public void testRightLoopStartingUpDoubleAgainstCenterLine() {
		assertSimilarity(	getRightLoopStartingUpDouble(),
							getVerticalCenterLine(),
							MeasurementUtil.createRatio((4.0 / 12.0)),
							"double loop overlaps center line twice");
	}

	@Test
	public void testCenterLineAgainstRightLoopStartingUpDouble() {
		assertSimilarity(	getVerticalCenterLine(),
							getRightLoopStartingUpDouble(),
							BufferedLineStringSimilarityCalculator.RATIO_TOTAL_MATCH,
							"double loop overlaps centerline twice completely ");
	}

	@Test(expected = NullPointerException.class)
	public void testBuilderSourceCannotBeNull() {
		BufferedLineStringSimilarityCalculator.create().setTargetGeometry(JtsTestUtil.getVerticalCenterLine()).build();
	}

	@Test(expected = NullPointerException.class)
	public void testBuilderTargetCannotBeNull() {
		BufferedLineStringSimilarityCalculator.create().setSourceGeometry(JtsTestUtil.getVerticalCenterLine()).build();
	}

}
