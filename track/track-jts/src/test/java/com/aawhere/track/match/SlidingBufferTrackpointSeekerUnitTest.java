/**
 * 
 */
package com.aawhere.track.match;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.calc.TrackpointCounter;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.ManualTrackProcessor.Builder;
import com.aawhere.track.process.seek.TrackpointSeekerChainBuilder;

import com.vividsolutions.jts.geom.Geometry;

/**
 * @author aroller
 * 
 */
public class SlidingBufferTrackpointSeekerUnitTest {

	private TrackpointCounter lengthReachedCounter;
	private ManualTrackProcessor processor;
	private SlidingBufferTrackpointSeeker slidingBufferTrackpointSeeker;
	private Iterator<Trackpoint> iterator;
	private Trackpoint currentTrackpoint;
	private int slideTestCount = 0;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Builder driverBuilder = ManualTrackProcessor.create();
		TrackpointSeekerChainBuilder<ManualTrackProcessor> chainBuilder = TrackpointSeekerChainBuilder
				.create(driverBuilder);
		com.aawhere.track.match.SlidingBufferTrackpointSeeker.Builder seekerBuilder = SlidingBufferTrackpointSeeker
				.create();
		// the buffer width value doesn't matter so much for testing
		seekerBuilder.setBufferWidth(MeasurementUtil.createLengthInMeters(10));
		seekerBuilder.setMinBufferLength(LINE_LENGTH);
		chainBuilder.addSeeker(seekerBuilder);
		this.lengthReachedCounter = chainBuilder.registerWithCurrentProcessor(TrackpointCounter.create());
		this.processor = chainBuilder.build();
		this.slidingBufferTrackpointSeeker = processor.getListener(SlidingBufferTrackpointSeeker.class);
		assertNotNull(slidingBufferTrackpointSeeker);
	}

	protected Trackpoint nextPoint(Track track) {
		boolean firstPoint;

		if (this.iterator == null) {
			this.iterator = track.iterator();
			firstPoint = true;
		} else {
			firstPoint = false;
		}

		this.currentTrackpoint = this.iterator.next();
		this.processor.notifyTrackpointListeners(currentTrackpoint);
		Geometry buffer = slidingBufferTrackpointSeeker.getBuffer();
		if (lengthReachedCounter.getCount() > this.slideTestCount) {
			this.slideTestCount = lengthReachedCounter.getCount();
			assertNotNull("buffer slid...should be available", buffer);
			assertTrue("buffer must be valid", buffer.isValid());
			if (firstPoint) {
				assertTrue("1 trackpoint is not enough for a buffer", buffer.isEmpty());
			} else {
				assertTrue(	"current point should always be in buffer " + currentTrackpoint,
							this.slidingBufferTrackpointSeeker.contains(currentTrackpoint));

			}
		} else {
			assertNull("buffer isn't available until notification", buffer);
		}
		return this.currentTrackpoint;
	}

	/**
	 * Using the most basic line this ensures that each trackpoint is contained within the new
	 * buffer and that notification only occurs after the length has been reached after the final
	 * point is added since the target length is the line length.
	 */
	@Test
	public void testWholeTrackIsBuffer() {

		Track track = LINE;
		// first point
		Trackpoint t1 = nextPoint(track);
		assertEquals("length not reached yet", 0, lengthReachedCounter.getCount().intValue());
		// first segment
		Trackpoint t2 = nextPoint(track);
		assertEquals("length not reached yet", 0, lengthReachedCounter.getCount().intValue());
		// second segment, end of the line
		Trackpoint t3 = nextPoint(track);
		assertBufferContains(t1, true);
		assertBufferContains(t2, true);
		assertBufferContains(t3, true);

	}

	/**
	 * A less detailed test than {@link #testWholeTrackIsBuffer()} this makes sure the limit is
	 * reached and buffer is staying short. The line is 3 points so the buffer slides until 3 points
	 * are contained.
	 */
	@Test
	public void testBufferLengthReached() {
		Track track = LINE_SUPER_LONG;
		Trackpoint t1 = nextPoint(track);
		Trackpoint t2 = nextPoint(track);
		// length is reached during this call
		Trackpoint t3 = nextPoint(track);
		assertBufferContains(t1, true);
		assertBufferContains(t2, true);
		assertBufferContains(t3, true);
		Trackpoint t4 = nextPoint(track);
		Trackpoint t5 = nextPoint(track);
		// buffer slides, but keeps the last point
		assertBufferContains(t1, false);
		assertBufferContains(t2, false);
		assertBufferContains(t3, true);
		assertBufferContains(t4, true);
		assertBufferContains(t5, true);
		nextPoint(track);
		nextPoint(track);
		nextPoint(track);
		assertBufferContains(t3, false);
		assertBufferContains(t4, false);
		assertBufferContains(t5, true);

	}

	public void assertBufferContains(Trackpoint trackpoint, Boolean containedIsExpected) {
		assertEquals(	trackpoint + " not contained in " + slidingBufferTrackpointSeeker.getBuffer(),
						containedIsExpected,
						slidingBufferTrackpointSeeker.contains(trackpoint));
	}

}
