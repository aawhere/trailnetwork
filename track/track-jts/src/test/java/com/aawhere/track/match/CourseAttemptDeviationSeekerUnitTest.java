/**
 * 
 */
package com.aawhere.track.match;

import static com.aawhere.measure.MeasurementUtil.*;
import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.util.Iterator;

import javax.measure.quantity.Length;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.lang.If;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.test.TestUtil;
import com.aawhere.track.SimpleTrack;
import com.aawhere.track.Track;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.process.TrackProcessorUtil;
import com.aawhere.track.process.processor.ManualTrackProcessor;
import com.aawhere.track.process.processor.SemiAutomaticTrackProcessor;
import com.aawhere.track.process.seek.TrackpointSeekerChainBuilder;

/**
 * Simple verification that {@link CourseAttemptDeviationSeeker} is working in isolation. The buffer
 * doesn't move in these tests, but rather the attempt moves against a static buffer which is
 * simpler to understand.
 * 
 * @author aroller
 * 
 */
public class CourseAttemptDeviationSeekerUnitTest {

	Iterator<Trackpoint> attemptIterator;
	CourseAttemptDeviationSeeker deviationSeeker;
	ManualTrackProcessor deviationProcessor;
	static final Length BUFFER_WIDTH = MeasurementUtil.createLengthInMeters(10);
	SlidingBufferTrackpointSeeker courseBufferSeeker;
	Length bufferLength;
	private SemiAutomaticTrackProcessor courseProcessor;

	@Before
	public void setUp() {

	}

	/**
	 * The most basic test where the course and attempt are exactly the same track and the buffer is
	 * the entire length of the course.
	 * 
	 */
	@Test
	public void testPerfectMatchLongBuffer() {
		Track course = LINE_SUPER_LONG;
		setUpCourse(course, null);

		Track attempt = course;
		setUpAttempt(attempt);
		int expectedNumberOfDeviations = 0;
		Length expectedLengthOfTrailingMisses = MeasurementUtil.ZERO_LENGTH;
		Boolean expectedMissingBufferFull = false;

		assertAllTrackpoints(expectedNumberOfDeviations, expectedLengthOfTrailingMisses, expectedMissingBufferFull);
	}

	/**
	 * Tests paricipant that follows the exact same course, but the buffer is the length of each
	 * segment.
	 * 
	 * @see #testPerfectMatchLongBuffer()
	 */
	@Test
	public void testSameCourseShortBufferAtEndWithLeadingMisses() {
		Track course = LINE;
		// the setup iterates all so the buffer is at the end of the line
		setUpCourse(course, S1_LENGTH);

		Track attempt = LINE_BEFORE;
		setUpAttempt(attempt);
		assertNextTrackpoint("first point is not in buffer", 0, ZERO_LENGTH, false);
		assertNextTrackpoint(	"second point is not in buffer yet and length exceeds the allowed. The buffer is at the end so it flags to give up",
								0,
								S1_LENGTH,
								true);

	}

	/**
	 * This uses a segment long buffer that exists only at the beginning. The test demonstrates the
	 * points at the beginning of the course fall within the buffer, but the points at the end fall
	 * outside and the misses add up.
	 */
	@Test
	public void testSameCourseShortBufferAtBeginning() {
		// the course will only buffer to the length of the first segment.
		Track course = new SimpleTrack.TrackpointCollectionBuilder().addAll(S1_POINTS).getTrack();
		// the setup iterates all so the buffer is at the end of the line
		Length expectedDeviationLength = S1_LENGTH;
		setUpCourse(course, expectedDeviationLength);

		Track attempt = LINE_LONGER;
		setUpAttempt(attempt);
		assertNextTrackpoint("first point in the buffer", 0, MeasurementUtil.ZERO_LENGTH, false);
		assertNextTrackpoint("second point is within buffer", 0, ZERO_LENGTH, false);
		assertNextTrackpoint("third point is beyond the buffer", 0, expectedDeviationLength, true);
		assertNextTrackpoint(	"fourth point is beyond the buffer, but listener should be ignoring points since it has exceeded",
								0,
								expectedDeviationLength,
								true);
		deviationProcessor.finish();
		assertProcessing("the finish should have logged the trailing deviation", 1, ZERO_LENGTH, true);
		Track deviationTrack = this.deviationSeeker.getDeviations().get(Index.FIRST.index);
		Length deviationLength = TrackProcessorUtil.length(deviationTrack);
		MeasureTestUtil.assertEquals(	"Final deviation length should be one segment",
										expectedDeviationLength,
										deviationLength);

	}

	/**
	 * @param attempt
	 */
	private void setUpAttempt(Track attempt) {
		attemptIterator = attempt.iterator();
	}

	/**
	 * 
	 */
	private void setUpDeviationProcessor() {
		TrackpointSeekerChainBuilder<ManualTrackProcessor> deviationChainBuilder = TrackpointSeekerChainBuilder
				.create(new ManualTrackProcessor.Builder());
		deviationChainBuilder.addSeeker(CourseAttemptDeviationSeeker.create().setCourseBufferSeeker(courseBufferSeeker)
		// the deviation seeker works better when the buffer is longer.
				.setMaxDeviation(QuantityMath.create(this.bufferLength).dividedBy(2).getQuantity()));
		this.deviationProcessor = deviationChainBuilder.build();
		this.deviationSeeker = deviationProcessor.getListener(CourseAttemptDeviationSeeker.class);
		TestUtil.assertEmpty(deviationSeeker.getDeviations());
	}

	/**
	 * @param course
	 */
	private void setUpCourse(Track course, Length givenBufferedLength) {
		// this buffer width is sufficient for testing and is not relevant for these tests
		this.bufferLength = If.nil(givenBufferedLength).use(TrackProcessorUtil.length(course));

		// setup the course buffer first since it is required for deviation seeker
		TrackpointSeekerChainBuilder<SemiAutomaticTrackProcessor> courseChainBuilder = TrackpointSeekerChainBuilder
				.create(new SemiAutomaticTrackProcessor.Builder().setTrack(course));
		courseChainBuilder.addSeeker(SlidingBufferTrackpointSeeker.create().setMinBufferLength(this.bufferLength)
				.setBufferWidth(BUFFER_WIDTH));
		this.courseProcessor = courseChainBuilder.build();
		// the buffer is fully populated at this point
		this.courseBufferSeeker = courseProcessor.getListener(SlidingBufferTrackpointSeeker.class);
		setUpDeviationProcessor();
	}

	/**
	 * @param expectedNumberOfDeviations
	 * @param expectedLengthOfTrailingMisses
	 * @param tooMuchDeviationExpected
	 */
	private void assertAllTrackpoints(int expectedNumberOfDeviations, Length expectedLengthOfTrailingMisses,
			Boolean tooMuchDeviationExpected) {
		// all trackpoints match so all are the same
		Integer counter = 1;
		while (this.attemptIterator.hasNext()) {
			assertNextTrackpoint(	counter.toString(),
									expectedNumberOfDeviations,
									expectedLengthOfTrailingMisses,
									tooMuchDeviationExpected);
			counter++;
		}
	}

	/**
	 * Iterates the next trackpoint verifying the provided expectations.
	 * 
	 * @param expectedNumberOfDeviations
	 * @param expectedLengthOfTrailingMisses
	 * @param tooMuchDeviationExpected
	 */
	private void assertNextTrackpoint(String context, int expectedNumberOfDeviations,
			Length expectedLengthOfTrailingMisses, Boolean tooMuchDeviationExpected) {
		deviationProcessor.notifyTrackpointListeners(attemptIterator.next());
		assertProcessing(context, expectedNumberOfDeviations, expectedLengthOfTrailingMisses, tooMuchDeviationExpected);
	}

	/**
	 * Confirms the state of the processors are as expected with the given params.
	 * 
	 * @param context
	 * @param expectedNumberOfDeviations
	 * @param expectedLengthOfTrailingMisses
	 * @param tooMuchDeviationExpected
	 */
	private void assertProcessing(String context, int expectedNumberOfDeviations,
			Length expectedLengthOfTrailingMisses, Boolean tooMuchDeviationExpected) {
		assertEquals(context + " : deviation size", expectedNumberOfDeviations, deviationSeeker.getDeviations().size());
		MeasureTestUtil.assertEquals(	context + " : length of misses",
										expectedLengthOfTrailingMisses,
										deviationSeeker.lengthOfTrailingMisses());
		assertEquals(	context + " : too much devation",
						tooMuchDeviationExpected,
						deviationSeeker.isTooMuchDeviationDetected());
	}

}
