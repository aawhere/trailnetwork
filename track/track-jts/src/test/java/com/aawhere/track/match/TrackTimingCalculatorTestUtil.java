/**
 * 
 */
package com.aawhere.track.match;

import java.util.List;

import javax.measure.quantity.Length;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.track.ExampleRealisticTracks;
import com.aawhere.track.ExampleRealisticTracks.Builder.Instruction;
import com.aawhere.track.Trackpoint;

/**
 * Provides useful {@link ExampleRealisticTracks} to be used in comparing a course to an attempt.
 * This provides the standard situation allowing convention over configuration to provide
 * simplicity.
 * 
 * @see TrackTimingRelationCalculatorUnitTest
 * 
 * @author aroller
 * 
 */
public class TrackTimingCalculatorTestUtil {

	public static Length MAX_DEVIATION = MeasurementUtil.createLengthInMeters(100);
	public static Length COURSE_LENGTH = MeasurementUtil.createLengthInMeters(2000);
	private static List<Trackpoint> COURSE = ExampleRealisticTracks.create().atLeast(COURSE_LENGTH).build().track();

	public static List<Trackpoint> course() {
		return COURSE;
	}

	/**
	 * Where the track repeats back on tiself for a period long enough to be considered a deviation.
	 * 
	 * @return
	 */
	public static List<Trackpoint> significantReturnInTheMiddle() {

		ExampleRealisticTracks.Builder attemptBuilder = ExampleRealisticTracks.create();
		{
			Length lengthWhereReturnStarts = QuantityMath.create(COURSE_LENGTH).times(0.75).getQuantity();
			// FIXME: Notice the repeat length is 20x the allowed deviation. That's too much!!!!!!
			Length repeatLength = QuantityMath.create(MAX_DEVIATION).times(5).getQuantity();
			Length lengthToFinish = QuantityMath.create(COURSE_LENGTH).minus(repeatLength).getQuantity();
			Instruction startToReturn = attemptBuilder.current().atLeast(lengthWhereReturnStarts);
			Instruction shortReturn = attemptBuilder.instruct().atLeast(repeatLength).negateLatIncrement();
			Instruction finish = attemptBuilder.instruct().atLeast(lengthToFinish);
		}
		return attemptBuilder.build().track();

	}

}
