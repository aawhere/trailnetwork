/**
 * 
 */
package com.aawhere.track.match;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import java.util.List;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import org.apache.commons.collections.IteratorUtils;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.collections.ListUtilsExtended;
import com.aawhere.lang.If;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleRealisticTracks;
import com.aawhere.track.SimpleTrack;
import com.aawhere.track.Track;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.TrackUtil;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.TrackpointTestUtil;
import com.aawhere.track.match.TrackTimingRelationCalculator.Builder;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingCompletionCategory;
import com.aawhere.track.match.TrackTimingRelationCalculator.TimingResult;
import com.aawhere.track.process.TrackProcessorUtil;
import com.aawhere.track.process.processor.ManualTrackProcessor;

/**
 * Runs simple scenarios against the timing calculator to make sure the algorithms are properly
 * detecting timing starts/finishes and ensuring non-matches work too.
 * 
 * Actual track log testing should be done in a downstream integration test against the service.
 * 
 * @see TrackTimingRelationCalculator
 * @author aroller
 * 
 */
public class TrackTimingRelationCalculatorUnitTest {

	private Length maxDeviation = S1_LENGTH;

	/**
	 * Racing the same line is the simplest test that should result in the first and last points as
	 * matching.
	 */
	@Test
	public void testRaceAgainstSelf() {
		Tester tester = createTester(LINE, LINE_POINTS);
		assertInTheRace(tester);
		tester.finish();
		assertNormalRaceCompleted(tester);
	}

	@Test
	public void testDidNotStart() {
		Tester tester = createTester(LINE_BEFORE, LINE_POINTS);
		assertInTheRace(tester);
		tester.finish();
		tester.assertNumberOfResults("never started so only a single result should be provided", 1);
		Index index = Index.FIRST;
		assertDidNotStart(tester, index);
	}

	/**
	 * @param tester
	 * @param index
	 */
	private void assertDidNotStart(Tester tester, Index index) {
		TimingCompletionCategory status = TimingCompletionCategory.DID_NOT_START;
		assertDidNot(tester, index, status);
	}

	private void assertDidNotFinish(Tester tester, Index index) {
		TimingCompletionCategory status = TimingCompletionCategory.DID_NOT_FINISH;
		assertDidNot(tester, index, status);
	}

	/**
	 * @param tester
	 * @param index
	 * @param status
	 */
	private void assertDidNot(Tester tester, Index index, TimingCompletionCategory status) {
		tester.assertRaceResult("expected not to reach start", index, false, null, null, status);
	}

	/**
	 * @param tester
	 */
	private void assertNormalRaceCompleted(Tester tester) {
		tester.assertRaceResult("same as course",
								Index.FIRST,
								true,
								P1,
								P3,
								TimingCompletionCategory.FINISH_LINE_REACHED);
	}

	/**
	 * Racing the same line is the simplest test that should result in the first and last points as
	 * matching.
	 */
	@Test
	public void testRaceAbove() {
		Tester tester = createTester(LINE_ABOVE, LINE_ABOVE_POINTS);
		assertInTheRace(tester);
		tester.finish();
		tester.assertRaceResult("courses are raced in a plane",
								Index.FIRST,
								true,
								ListUtilsExtended.firstOrNull(LINE_ABOVE_POINTS),
								ListUtilsExtended.lastOrNull(LINE_ABOVE_POINTS),
								TimingCompletionCategory.FINISH_LINE_REACHED);

	}

	/**
	 * Runs standard tests for the usual simple scenarios. Other scenarios should call manually and
	 * test with each point.
	 * 
	 * @param tester
	 */
	private void assertInTheRace(Tester tester) {
		tester.assertPoint("start point matches, but segments need another point for notification", false, false, false);
		tester.assertPoint("somewhere in the middle", true, false, true);
		tester.assertPoint("finished at the finish", true, true, false);
	}

	/** When the racer rides to the course and finishes at the finish */
	@Test
	public void testRideBeyondFinish() {
		List<Trackpoint> attemptPoints = LINE_LONGER_POINTS;
		Tester tester = createTester(LINE, attemptPoints);
		assertInTheRace(tester);
		tester.assertPoint("rode beyond the finish", false, false, false);
		tester.finish();
		tester.assertRaceResult("race completed and more",
								Index.FIRST,
								true,
								ListUtilsExtended.firstOrNull(attemptPoints),
								ListUtilsExtended.lastOrNull(LINE_POINTS),
								TimingCompletionCategory.FINISH_LINE_REACHED);

	}

	@Test
	public void testRideBeforeStart() {
		Tester tester = createTester(LINE, LINE_BEFORE_POINTS);
		tester.assertPoint("haven't yet reached the start", false, false, false);
		assertInTheRaceRunningStart(tester);
		tester.finish();
		tester.assertRaceResult("the result should start and end with the course ignoring the lead",
								Index.FIRST,
								true,
								ListUtilsExtended.firstOrNull(LINE_POINTS),
								ListUtilsExtended.lastOrNull(LINE_POINTS),
								TimingCompletionCategory.FINISH_LINE_REACHED);
	}

	/**
	 * The standard race when a point has already been run before. Same as
	 * {@link #assertInTheRace(Tester)}, but racing begins and a start foudn with first point.
	 * 
	 * @see #assertInTheRace(Tester)
	 * @param tester
	 */
	private void assertInTheRaceRunningStart(Tester tester) {

		tester.assertPoint("reached start", true, false, true);
		tester.assertPoint("middle doesn't matter", true, false, true);
		tester.assertPoint("reached start", true, true, false);
	}

	/**
	 * A rider that hits the start more than once should be rewarded the second start.
	 * 
	 */
	@Test
	public void testRideWithTwoStarts() {
		Tester tester = createTester(LINE_BEFORE, LINE_START_TWICE_THEN_FINISH_POINTS);
		// the starting line is at P_MINUS_1
		tester.assertPoint("First point finds the start, but segment's need another point", false, false, false);
		tester.assertPoint("second point leaves the start and starts the race", true, false, true);
		tester.assertPoint("Return to the start...sill racing, but new start", P_MINUS_1_T2, false, true);
		tester.assertPoint("middle point of the race", true, false, true);
		tester.assertPoint("finish line crossed", true, true, false);
		tester.finish();
		// notice no false start because not enough points between starts?
		tester.assertRaceResult("a second visit to the start must reset the start",
								Index.FIRST,
								true,
								P_MINUS_1_T2,
								ListUtilsExtended.lastOrNull(LINE_BEFORE_POINTS),
								TimingCompletionCategory.FINISH_LINE_REACHED);
	}

	/** An out and back executes the race, but returning shouldn't mark another. */
	@Test
	public void testRaceBeyondAndReturn() {
		Tester tester = createTester(LINE, LINE_LONGER_RETURN_POINTS);
		assertInTheRace(tester);
		tester.assertPoint("rode beyond the finish", false, false, false);
		tester.assertPoint("returned to the finish", false, false, false);
		tester.assertPoint("back in the middle", false, false, false);
		tester.assertPoint("reached the start again, start another race until direction matters", P1_X2, false, true);
		tester.finish();
		assertNormalRaceCompleted(tester);
		tester.assertRaceResult("A normal race with a reverse return to start should still be one race",
								Index.FIRST,
								true,
								ListUtilsExtended.firstOrNull(LINE_POINTS),
								ListUtilsExtended.lastOrNull(LINE_POINTS),
								TimingCompletionCategory.FINISH_LINE_REACHED);

		String message = "that return to start is erroenously being reported";
		int expectedNumberOfResults = 2;
		tester.assertNumberOfResults(message, expectedNumberOfResults);
	}

	@Test
	public void testRaceLingerAndReturn() {
		Tester tester = createTester(LINE, LINE_LINGER_AT_END_THEN_RETURN_POINTS);
		assertInTheRace(tester);
		tester.assertPoint(	"linger at the finish, no finish triggered since race not started again",
							false,
							false,
							false);
		tester.assertPoint("back in the middle...finish triggered", false, false, false);
		tester.assertPoint("reached the start again", P1_X2, false, true);
		tester.finish();
		tester.assertNumberOfResults("lingering is causing more races", 2);
		assertNormalRaceCompleted(tester);

	}

	@Test
	public void testFinishNeverReached() {
		Tester tester = createTester(LINE_LONGER, LINE_POINTS);
		tester.assertPoint("started at start, not yet racing", false, false, false);
		tester.assertPoint("somewhere in the middle", true, false, true);
		tester.assertPoint("still in the middle", true, false, true);
		tester.finish();
		tester.assertNumberOfResults("one result should have been logged, but not finished", 1);
		tester.assertRaceResult("the result should start and end with the course ignoring the lead",
								Index.FIRST,
								false,
								ListUtilsExtended.firstOrNull(LINE_LONGER_POINTS),
								// the last point is not the finish line, but the last point
								// recorded
								ListUtilsExtended.lastOrNull(LINE_POINTS),
								TimingCompletionCategory.DID_NOT_FINISH);
	}

	/**
	 * This is the same path in the wrong direction. This should not cause a race because the
	 * segments are not followed in the correct order.
	 */

	@Test
	public void testReverse() {

		Tester tester = createTester(LINE, RETURN_POINTS);
		tester.assertPoint("starting at the finish", false, false, false);
		tester.assertPoint("somewhere in the middle", false, false, false);
		// start is found...we could avoid this detection inspecting direction.
		tester.assertPoint("finished at start", P1_X2, false, true);
		tester.finish();
		tester.assertNumberOfResults("the start wasn't even detected until the end resulting in no race", 1);
		assertDidNotFinish(tester, Index.FIRST);
	}

	/**
	 * Two laps should result in multiple races. The two loops results in two races and three
	 * starts, but the third start does not finish.
	 * 
	 */
	@Test
	public void testTwoLoops() {
		Tester tester = createTester(LINE, DOUBLE_LOOP_POINTS);
		assertInTheRace(tester);
		tester.assertPoint("heading back towards start", false, false, false);
		tester.assertPoint("back at start", LOOP_2_START, false, true);

		tester.assertPoint("in the middle again", LOOP_2_START, false, true);
		tester.assertPoint("at the finish again", LOOP_2_START, LOOP_2_FINISH_LINE, false);
		tester.assertPoint("second lap middle return", false, false, false);
		tester.assertPoint("back at start after second lap", LOOP_3_START, false, true);
		tester.finish();
		assertNormalRaceCompleted(tester);
		tester.assertNumberOfResults("two results is all that is expected", 3);
		tester.assertRaceResult("first should have been a race",
								Index.FIRST,
								true,
								P1,
								P3,
								TimingCompletionCategory.FINISH_LINE_REACHED);
		tester.assertRaceResult("just like Line, but later",
								Index.SECOND,
								true,
								LOOP_2_START,
								LOOP_2_FINISH_LINE,
								TimingCompletionCategory.FINISH_LINE_REACHED);
		assertDidNotFinish(tester, Index.THIRD);
	}

	@Test
	@Ignore("having difficulty with this one...keep trying")
	public void testDeviation() {
		Tester tester = createTester(LINE, LINE_DEVIATION_POINTS);
		tester.assertPoint("start like always", false, false, false);
		tester.assertPoint("We are way off...race should stop", true, false, false);
		tester.assertPoint("returned to line at finish", true, true, false);
		tester.finish();
		assertNormalRaceCompleted(tester);
		int expectedDeviationCount = 1;
		assertDeviations(tester, expectedDeviationCount);
	}

	/**
	 * @param tester
	 * @param expectedDeviationCount
	 */
	private void assertDeviations(Tester tester, int expectedDeviationCount) {
		TimingResult raceResult = tester.calculator.getRaceResults().get(0);
		List<Track> deviations = raceResult.getDeviations();
		assertEquals("unexpected deviation count " + deviations, expectedDeviationCount, deviations.size());
		Track deviatedTrack = deviations.get(0);
		Length deviationLength = TrackProcessorUtil.length(deviatedTrack);
		QuantityMath<Length> deviationMath = QuantityMath.create(deviationLength);
		assertFalse("what good is a deviation that is zero?", deviationMath.equalToZero());
	}

	/**
	 * Entire purpose of this test is to make sure processing will stop if deviation is too far off
	 * track.
	 * 
	 * We needed two long lines to produce enough points to discover the deviation length was
	 * exceeded.
	 * 
	 * @see CourseAttemptDeviationProcessor#isDeviationLengthExceeded()
	 */
	@Test
	public void testLoopNotFinished() {
		Tester tester = createTester(DOUBLE_LOOP, LINE_SUPER_LONG_POINTS);

		tester.assertPoint("starting is a match", true, false, true);
		tester.assertPoint("deviation begins", true, false, true);
		tester.assertPoint("more deviation", true, false, true);
		tester.assertPoint("more deviation", true, false, true);
		tester.assertPoint("more deviation", true, false, true);
		tester.assertPoint("more deviation", true, false, true);
		tester.assertPoint("more deviation", true, false, true);
		tester.assertPoint("more deviation", true, false, true);
		tester.assertPoint("more deviation", true, false, true);
		tester.finish();
		tester.assertRaceResult("deviation should have exceeded",
								Index.FIRST,
								false,
								ListUtilsExtended.firstOrNull(DOUBLE_LOOP_POINTS),
								null,
								TimingCompletionCategory.TOO_MUCH_DEVIATION);

	}

	/**
	 * This demonstrates the matching of the start and finish regardless of the actual course
	 * followed.
	 * 
	 */
	@Test
	public void testStartFinishFollowingAlternateCourse() {
		Tester tester = createTester(LINE_BEFORE_AFTER, LINE_BEFORE_AND_AFTER_ALTERNATE_ROUTE_POINTS);
		tester.assertPoint("starting together should start the race, but segment not yet found", false, false, false);
		tester.assertPoint("left start now racing", true, false, true);
		// this is where the calculator should disqualify this race, but currently does not.
		tester.assertPoint("way off route, but still racing", true, false, true);
		tester.assertPoint("way off route, but still racing", true, false, true);
		tester.assertPoint("came back and finished", true, true, false);
		tester.finish();
		tester.assertRaceResult("the result should start and end with the course ignoring the lead",
								Index.FIRST,
								false,
								ListUtilsExtended.firstOrNull(LINE_BEFORE_AND_AFTER_POINTS),
								ListUtilsExtended.lastOrNull(LINE_BEFORE_AND_AFTER_POINTS),
								TimingCompletionCategory.TOO_MUCH_DEVIATION);
		assertDeviations(tester, 1);
	}

	/**
	 * Tests the tolerance for detecting line crossing. see TN-175
	 */
	@Test
	public void testReallyCloseToStart() {
		Tester tester = createTester(LINE, new SimpleTrack.TrackpointCollectionBuilder().add(P1_MICRO).add(P2).add(P3)
				.getTrackpoints());
		assertInTheRace(tester);
		tester.finish();
		assertNormalRaceCompleted(tester);

	}

	/**
	 * Close enough to the line to be considered finished.
	 * 
	 */
	@Test
	public void testAlmostFinished() {
		Tester tester = createTester(	LINE,
										new SimpleTrack.TrackpointCollectionBuilder().add(P1).add(P2)
												.add(P3_MICRO_BEFORE).getTrackpoints());
		assertInTheRace(tester);
		tester.finish();
		assertNormalRaceCompleted(tester);
	}

	@Test
	public void testZeroDistanceSegment() {
		Tester tester = createTester(LINE, LINE_ZERO_DISTANCE_SEGMENT_POINTS);
		tester.assertPoint("starting points match, but no segment yet", false, false, false);
		tester.assertPoint("first segment is real should start", P1, false, true);
		tester.assertPoint("zero distance segment does nothing", P1, false, true);
		tester.assertPoint("Goes back to start.", P1_X2, false, true);
		tester.finish();
		tester.assertRaceResult("same as course",
								Index.FIRST,
								false,
								ListUtilsExtended.firstOrNull(LINE_ZERO_DISTANCE_SEGMENT_POINTS),
								ListUtilsExtended.secondToLastOrNull(LINE_ZERO_DISTANCE_SEGMENT_POINTS),
								TimingCompletionCategory.DID_NOT_FINISH);

	}

	/**
	 * For activities that to a return trip, but the start is late the return must go back through
	 * the start to get to the finish.
	 * 
	 * A false start could be triggered. TN-283
	 * 
	 */
	@Test
	public void testReturnTripOverStart() {
		final List<Trackpoint> testLine = LINE_RETURN_PAST_START;
		Tester tester = createTester(testLine, testLine);
		tester.assertPoint("race starts", true, false, true);
		tester.assertPoint("middle of race", true, false, true);
		tester.assertPoint("second middle of race", true, false, true);
		tester.assertPoint("turn-around point", true, false, true);
		tester.assertPoint("second middle again", true, false, true);
		tester.assertPoint("middle again", true, false, true);
		tester.assertPoint("start reached again", true, false, true);
		tester.assertPoint("finish reached ", true, true, false);
		tester.finish();
		tester.assertNumberOfResults("this is a perfect match so only finish is expected", 1);
		tester.assertRaceResult("finish should be reached after crossing start line",
								Index.FIRST,
								true,
								ListUtilsExtended.firstOrNull(testLine),
								ListUtilsExtended.lastOrNull(testLine),
								TimingCompletionCategory.FINISH_LINE_REACHED);
	}

	@Test
	public void testReturnTrip() {
		final List<Trackpoint> testLine = LINE_LONGER_RETURN_POINTS;
		Tester tester = createTester(testLine, testLine);
		tester.assertPoint("race starts", true, false, true);
		tester.assertPoint("middle of race", true, false, true);
		tester.assertPoint("middle of race", true, false, true);
		tester.assertPoint("turn-around point", true, false, true);
		tester.assertPoint("middle again", true, false, true);
		tester.assertPoint("middle again", true, false, true);
		tester.assertPoint("start/finish reached again", true, false, true);
		tester.finish();
		tester.assertNumberOfResults("this is a perfect match so only finish is expected", 1);
		tester.assertRaceResult("finish should be chosen rather than false start",
								Index.FIRST,
								true,
								ListUtilsExtended.firstOrNull(testLine),
								ListUtilsExtended.lastOrNull(testLine),
								TimingCompletionCategory.FINISH_LINE_REACHED);
	}

	@Test
	public void testRealisticSameTracks() {
		List<Trackpoint> track = ExampleRealisticTracks.create().build().track();
		Tester tester = createTester(track, track);
		tester.complete();

		tester.assertRaceResult("same track should finish",
								Index.FIRST,
								true,
								ListUtilsExtended.firstOrNull(track),
								ListUtilsExtended.lastOrNull(track),
								TimingCompletionCategory.FINISH_LINE_REACHED);
	}

	/**
	 * Simulates a common situation where someone will return along the same path for a small bit,
	 * but then return to finish the course as expected. if this is long enough then a
	 * disqualification should occur.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testRealTracksMatchingButAttemptRepeatsShortSegment() {
		List<Trackpoint> course = TrackTimingCalculatorTestUtil.course();
		this.maxDeviation = TrackTimingCalculatorTestUtil.MAX_DEVIATION;
		List<Trackpoint> attempt = TrackTimingCalculatorTestUtil.significantReturnInTheMiddle();
		Tester tester = createTester(course, attempt);
		tester.complete();
		tester.assertRaceResult("should fail due to return exceeding deviation",
								Index.FIRST,
								false,
								ListUtilsExtended.firstOrNull(course),
								ListUtilsExtended.lastOrNull(course),
								TimingCompletionCategory.TOO_MUCH_DEVIATION);
	}

	/**
	 * Demonstrates that two tracks of equal distance going in opposite directions end without a
	 * finish.
	 */
	@Test
	public void testRealTracksOppositeDirections() {
		List<Trackpoint> course = ExampleRealisticTracks.create().build().track();
		List<Trackpoint> attempt = ExampleRealisticTracks.create().negateLatIncrement().build().track();
		Tester tester = createTester(course, attempt);
		tester.complete();
		tester.assertRaceResult("same track should finish",
								Index.FIRST,
								false,
								null,
								null,
								TimingCompletionCategory.DID_NOT_FINISH);
	}

	/**
	 * Demonstrates that two return trips going in opposite directions and returning to the same
	 * start will not finish. TN-395
	 * 
	 * Although this identifies that the processing is not handled well when the course buffer ends
	 * immediately upon start, this does not happen often enough to handle better which could
	 * introduce more complexities into an already complex situation.
	 */
	@Test
	public void testRealTracksOppositeDirectionsReturnTrip() {
		Length atLeast = MeasurementUtil.createLengthInMeters(500);
		List<Trackpoint> course = ExampleRealisticTracks.create().atLeast(atLeast).returnTrip().build().track();
		List<Trackpoint> attempt = ExampleRealisticTracks.create().atLeast(atLeast).negateLatIncrement().returnTrip()
				.build().track();
		this.maxDeviation = MeasurementUtil.createLengthInMeters(500);
		Tester tester = createTester(course, attempt);
		tester.complete();

		tester.assertRaceResult("the calculator should be smart enough to not find a start, but short courses cause false finishes so distance check fails",
								Index.FIRST,
								false,
								null,
								null,
								TimingCompletionCategory.NOT_ENOUGH_COMPLETED);
	}

	/**
	 * creates the Tester allowing extensions to override.
	 * 
	 * @param courseTrack
	 * @param attemptTrackpoints
	 * @return
	 */
	public Tester createTester(Iterable<Trackpoint> courseTrack, List<Trackpoint> attemptTrackpoints) {
		return new Tester(courseTrack, attemptTrackpoints);
	}

	public class Tester {
		Iterable<Trackpoint> courseTrack;
		List<Trackpoint> trackpoints;
		ManualTrackProcessor processor;
		TrackTimingRelationCalculator calculator;
		TrackSummary course;
		Integer trackpointIndex = 0;
		private Trackpoint currentTrackpoint;
		// allows a test to override.
		private Length maxDeviation = TrackTimingRelationCalculatorUnitTest.this.maxDeviation;

		/** Used to retrieve the result for testing expected finishes */

		public Tester(Iterable<Trackpoint> courseTrack, List<Trackpoint> attemptTrackpoints) {

			this.courseTrack = courseTrack;
			@SuppressWarnings("unchecked")
			List<Trackpoint> coursePoints = IteratorUtils.toList(courseTrack.iterator());
			this.trackpoints = attemptTrackpoints;
			course = new TrackSummary.Builder().setStart(coursePoints.get(0))
					.setFinish(ListUtilsExtended.lastOrNull(coursePoints))
					.setDistance(TrackProcessorUtil.length(courseTrack)).build();
			Builder calculatorBuilder = createCalculatorBuilder().setCourse(course)
					.setBufferWidth(MeasurementUtil.createLengthInMeters(100)).setCourseTrack(this.courseTrack)

					.setMaxDeviationLength(maxDeviation);
			ManualTrackProcessor.Builder processorBuilder = new ManualTrackProcessor.Builder();
			calculatorBuilder.registerWithProcessor(processorBuilder);
			calculatorBuilder.goalLineLength(MeasurementUtil.createLengthInMeters(100));
			calculator = calculatorBuilder.build();
			processor = processorBuilder.build();
			assertFalse("race completed not inialized properly", calculator.raceCompleted());

		}

		/**
		 * In case an extension wants a different calculator (i.e. KML).
		 * 
		 * @return
		 */
		protected Builder createCalculatorBuilder() {
			return TrackTimingRelationCalculator.create();
		}

		public void complete() {
			for (Trackpoint trackpoint : this.trackpoints) {
				processor.notifyTrackpointListeners(trackpoint);
			}
			finish();
		}

		/**
		 * @param string
		 * @param first
		 * @param pMinus1
		 * @param object
		 * @param falseStart
		 */
		public void assertRaceResult(String string, Index index, Boolean raceCompleted, Trackpoint expectedStart,
				Trackpoint expectedFinish, TimingCompletionCategory reason) {
			List<TimingResult> raceResults = this.calculator.getRaceResults();
			TestUtil.assertGreaterThan(index.index, raceResults.size());
			TimingResult raceResult = raceResults.get(index.index);
			assertEquals(string + " reason", reason, raceResult.getReason());
			if (expectedStart != null) {
				TrackpointTestUtil.assertTrackpointEquals("start", expectedStart, raceResult.getSummary().getStart());
			}
			assertEquals("race completed", raceCompleted, raceResult.raceCompleted());
			if (raceCompleted) {
				Length distanceToExpectedFinish = TrackUtil.distanceBetween(expectedFinish, raceResult.getSummary()
						.getFinish());
				TestUtil.assertLessThan(100, distanceToExpectedFinish.doubleValue(MetricSystem.METRE));
			}
		}

		public void assertNumberOfResults(String message, Integer expectedNumberOfResults) {
			assertEquals(message, expectedNumberOfResults.intValue(), calculator.getRaceResults().size());
		}

		/**
		 * 
		 */
		public void finish() {
			this.processor.finish();
		}

		/**
		 * iterates the next trackpoint and runs the test against the expected parameters.
		 * 
		 * @param context
		 * @param startExpected
		 * @param finishExpected
		 * @param racing
		 */
		void assertPoint(String context, Boolean startExpected, Boolean finishExpected, Boolean racing) {
			assertPoint(context, (Trackpoint) If.tru(startExpected).use(course.getStart()), finishExpected, racing);
		}

		/**
		 * Allows overriding of expected points
		 * 
		 * @param context
		 * @param expectedStart
		 * @param finishExpected
		 * @param racing
		 */
		void assertPoint(String context, Trackpoint expectedStart, Boolean finishExpected, Boolean racing) {
			assertPoint(context, expectedStart, (Trackpoint) If.tru(finishExpected).use(course.getFinish()), racing);
		}

		/**
		 * NOTE:most of the per-step testing is disabled now. It is too granular to test what is
		 * happening at each step which was causing failures due to how it was being processed
		 * rather than the end result.
		 * 
		 * @param context
		 * @param expectedStart
		 * @param expectedFinish
		 * @param racing
		 */
		void assertPoint(String context, Trackpoint expectedStart, Trackpoint expectedFinish, Boolean racing) {
			Boolean startExpected = expectedStart != null;
			Boolean finishExpected = expectedFinish != null;
			context += ": ";
			this.currentTrackpoint = trackpoints.get(trackpointIndex);
			processor.notifyTrackpointListeners(currentTrackpoint);
			// FIXME:racing micro validation isn't accurate now that there is buffering
			// assertEquals(context + "racing", racing, calculator.racing());

			// if finish is expected, then check the race result
			// if (finishExpected) {
			// List<RaceResult> raceResults = calculator.getRaceResults();
			//
			// // zero-based index, 1-based size
			// TestUtil.assertSize(resultIndex + 1, raceResults);
			// // get the current race result and increment preparing for the next
			// RaceResult currentRaceResult = raceResults.get(resultIndex++);
			//
			// // the finish is provided by the race results now
			// TrackpointTestUtil.assertTrackpointEquals( context + "finish isn't correct for " +
			// trackpointIndex,
			// expectedFinish,
			// currentRaceResult.getSummary().getFinish());
			// TrackpointTestUtil.assertTrackpointEquals( context + "start is incorrect for " +
			// trackpointIndex,
			// expectedStart,
			// currentRaceResult.getSummary().getStart());
			//
			// } else {
			// assertEquals("results processed unexpected", this.resultIndex,
			// calculator.getRaceResults().size());
			//
			// // start is verified in race results now
			// }
			trackpointIndex++;
		}
	}

}
