/**
 *
 */
package com.aawhere.track;

import static com.aawhere.test.TestUtil.assertDoubleEquals;
import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.jts.geom.JtsTestUtil;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.test.TestUtil;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;

/**
 * @author roller
 * @seee {@link JtsTrackGeomUtil}
 */
public class JtsTrackGeomUtilUnitTest {

	/**
	 * Test method for
	 * {@link com.aawhere.track.JtsTrackGeomUtil#convert(com.aawhere.track.Trackpoint)}.
	 */
	@Test
	public void testConvertTrackPoint() {
		Trackpoint trackpoint = TrackpointTestUtil.generateRandomTrackpoint();
		Coordinate jtsPoint = JtsTrackGeomUtil.convert(trackpoint);
		assertEquals(jtsPoint.x, trackpoint.getLocation().getLongitude().getValue());
		assertEquals(jtsPoint.y, trackpoint.getLocation().getLatitude().getValue());
		assertDoubleEquals(jtsPoint.z, trackpoint.getElevation().doubleValue(ExtraUnits.METER_MSL));
	}

	@Test
	public void testConvertTrackPointMissingElevation() {
		Trackpoint trackpoint = TrackpointTestUtil.WITHOUT_ELEVATION;
		Coordinate jtsPoint = JtsTrackGeomUtil.convert(trackpoint);
		assertEquals(trackpoint.getLocation().getLongitude().getValue(), jtsPoint.x);
		assertEquals(trackpoint.getLocation().getLatitude().getValue(), jtsPoint.y);
		assertNull(trackpoint.getElevation());
		assertEquals(Double.NaN, jtsPoint.z, 0.0001);
	}

	/**
	 * @see #testConvertTrackRepeatTimestamp()
	 */
	@Test
	public void testConvertTrackPointMissingLocation() {
		Trackpoint trackpoint = TrackpointTestUtil.WITHOUT_LOCATION;
		Coordinate jtsPoint = JtsTrackGeomUtil.convert(trackpoint);

		assertNull(jtsPoint);
	}

	/**
	 * @see #testConvertTrackBackInTime()
	 * 
	 */
	@Test
	public void testConvertTrackRepeatTimestamp() {

		Track track = ExampleTracks.LINE_EQUAL_TIMESTAMPS;
		MultiLineString multiLineString = JtsTrackGeomUtil.trackToMultiLineString(track);
		assertEquals("one of two points should have been filtered out. ", 0, multiLineString.getNumPoints());
	}

	@Test
	public void testConvertTrackBackInTime() {

		Track track = ExampleTracks.LINE_PREVIOUS_TIMESTAMPS;
		MultiLineString multiLineString = JtsTrackGeomUtil.trackToMultiLineString(track);
		assertEquals("one of two points should have been filtered out. ", 0, multiLineString.getNumPoints());
	}

	@Test
	public void testTrackToMultiStringWithoutDuplicates() {
		Geometry lineString = JtsTrackGeomUtil
				.trackToMultiLineStringWithoutDuplicateTrackpoints(ExampleTracks.LINE_BEFORE_AND_AFTER_MISSING_LOCATION);
		assertNotNull(lineString);
		assertEquals("a straight line with many points is simplified to two endpoints", 2, lineString.getNumPoints());
	}

	@Test
	public void testMultiLineStringToTrack() {
		LineString ls1 = JtsTestUtil.getVerticalEndLine();
		LineString ls2 = JtsTestUtil.getL();
		LineString[] lineStrings = { ls1, ls2 };
		MultiLineString mls = new MultiLineString(lineStrings, JtsTestUtil.getGeometryFactory());

		Track result = JtsTrackGeomUtil.multiLineStringToTrack(mls);
		assertNotNull(result);
	}

	@Test
	public void testConvertTrackToMultiLineString() {
		MultiLineString multiLineString = JtsTrackGeomUtil.trackToMultiLineString(ExampleTracks.LINE_BEFORE_AFTER);
		assertEquals(	"all these points are good",
						ExampleTracks.LINE_BEFORE_AND_AFTER_POINTS.size(),
						multiLineString.getNumPoints());
	}

	@Test
	public void testConvertSegmentToMultiLineString() {
		TrackSegment ts = ExampleTracks.S1;
		MultiLineString mls = JtsTrackGeomUtil.convertToMultiLineString(ts);

		assertNotNull(mls);
		TestUtil.assertDoubleEquals(ts.getFirst().getLocation().getLatitude().getInDecimalDegrees(),
									mls.getCoordinate().y);
		TestUtil.assertDoubleEquals(ts.getSecond().getLocation().getLatitude().getInDecimalDegrees(),
									mls.getCoordinates()[1].y);

	}

	@Test
	public void testTrackSegmentToCoordinates() {
		TrackSegment ts = ExampleTracks.S1;
		Coordinate[] coords = JtsTrackGeomUtil.trackSegmentToCoordinates(ts);

		assertNotNull(coords);
		assertEquals(2, coords.length);
	}
}
