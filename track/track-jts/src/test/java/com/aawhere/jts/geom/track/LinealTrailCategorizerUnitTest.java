/**
 * 
 */
package com.aawhere.jts.geom.track;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.jts.geom.track.LinealTripCategorizer;
import com.aawhere.jts.geom.track.LinealTripCategorizer.Category;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

/**
 * Tests the various categorizers using simple lines and multi lines for testing.
 * 
 * @author roller
 * 
 */
@Ignore
public class LinealTrailCategorizerUnitTest {

	public static final TestGeometry SIMPLE_ONE_WAY = new TestGeometry("Real Simple one-way trip with no overlap",
			"LINESTRING ( 0 3, 0 -3 )", LinealTripCategorizer.Category.ONE_WAY);

	public static final TestGeometry RETURN_TRIP = new TestGeometry(
			"Slightly chaotic vertical line starting at 3 going to -3 and returning",
			"LINESTRING ( 0 3, .1 2.1, -.1 1, 0 -1.1, -.1 2, 0 3, -.1 2.1, -.1 1, 0 .1, 0 1.5, 0 3.1 )",
			LinealTripCategorizer.Category.RETURN_TRIP);

	public void testGeometry(TestGeometry testData) {
		LinealTripCategorizer categorizer = new LinealTripCategorizer.Builder(testData.geometry).build();
		assertEquals(testData.description, testData.category, categorizer.getCategory());
	}

	@Test
	public void testSimpleOneWay() {
		testGeometry(SIMPLE_ONE_WAY);
	}

	@Test
	public void testReturnTrip() {
		testGeometry(RETURN_TRIP);
	}

	private static class TestGeometry {

		private Category category;
		private Geometry geometry;
		private String description;

		public TestGeometry(String description, String wkt, LinealTripCategorizer.Category category) {
			this.description = description;
			try {
				this.geometry = new WKTReader().read(wkt);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
			this.category = category;
		}

	}
}
