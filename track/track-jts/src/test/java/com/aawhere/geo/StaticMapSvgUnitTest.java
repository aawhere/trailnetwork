/**
 *
 */
package com.aawhere.geo;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.geo.map.StaticMapSvg;
import com.aawhere.track.ExampleRealisticTracks;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.Track;
import com.aawhere.track.TrackInfo;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.story.TrackSummaryUserStoriesTestUtil;

/**
 * @author aroller
 * 
 */
public class StaticMapSvgUnitTest {

	@Test
	public void testLoop() {
		Track track = ExampleTracks.LOOP;
		test(track);
	}

	@Test
	public void testRealisticTrack() {
		test(ExampleRealisticTracks.create().build().track());
	}

	/**
	 * @param track
	 */
	private void test(Iterable<Trackpoint> track) {
		TrackSummary trackSummary = TrackSummaryUserStoriesTestUtil.create(track);
		StaticMapSvg staticMapSvg = StaticMapSvg.create().trackInfo(TrackInfo.build(trackSummary, track)).build();
		String svg = staticMapSvg.svg();
		assertNotNull(svg);
		// System.out.println(svg);
	}

}
