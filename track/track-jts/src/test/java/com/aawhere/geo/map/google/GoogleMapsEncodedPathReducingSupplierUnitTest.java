/**
 * 
 */
package com.aawhere.geo.map.google;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.track.ExampleRealisticTracks;
import com.aawhere.track.TrackInfo;
import com.aawhere.track.TrackSummary;
import com.aawhere.track.Trackpoint;
import com.aawhere.track.story.TrackSummaryUserStoriesTestUtil;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

/**
 * @author aroller
 * 
 */
public class GoogleMapsEncodedPathReducingSupplierUnitTest {

	@Test
	public void testSupplierOnly() {
		List<Trackpoint> track = ExampleRealisticTracks.create().build().track();
		TrackSummary trackSummary = TrackSummaryUserStoriesTestUtil.create(track);
		TrackInfo trackInfo = TrackInfo.build(trackSummary, track);
		Supplier<TrackInfo> trackSupplier = Suppliers.ofInstance(trackInfo);
		GoogleMapsEncodedPathReducingSupplier pathSupplier = GoogleMapsEncodedPathReducingSupplier.create()
				.trackSupplier(trackSupplier).build();
		// reduction should finish before
		String initialPath = null;
		String previousPath = initialPath;
		int numberOfTries = 0;
		// make sure we get the path
		do {
			final String currentPath = pathSupplier.get();
			assertNotNull("path is null", currentPath);
			TestUtil.assertNotEquals(	"paths should be reduced, not repeated after tries=" + numberOfTries,
										previousPath,
										currentPath);

			if (previousPath != null) {
				TestUtil.assertLessThan(numberOfTries + " tries shows previous length should be less ",
										previousPath.length(),
										currentPath.length());
			}
			previousPath = currentPath;
			numberOfTries++;
		} while (pathSupplier.isReductionCapable() && numberOfTries < 10);
		assertNotSame("path never provided", initialPath, previousPath);
		assertOneGetTooMany(pathSupplier);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNeitherProvided() {
		GoogleMapsEncodedPathReducingSupplier.create().build();
	}

	@Test
	public void testPathOnly() {
		String path = TestUtil.generateRandomAlphaNumeric();
		GoogleMapsEncodedPathReducingSupplier supplier = GoogleMapsEncodedPathReducingSupplier.create()
				.encodedPath(path).build();
		assertEquals("the first path should provide the given", path, supplier.get());
		assertFalse("reduction not capable because no supplier given", supplier.isReductionCapable());
		assertOneGetTooMany(supplier);
	}

	/**
	 * @param supplier
	 */
	private void assertOneGetTooMany(GoogleMapsEncodedPathReducingSupplier supplier) {
		try {
			supplier.get();
			fail("calling get should fail for the incapble supplier");
		} catch (IllegalStateException e) {
			// cool.
		}
	}

	@Test
	public void testPathAndSupplier() {

	}
}
