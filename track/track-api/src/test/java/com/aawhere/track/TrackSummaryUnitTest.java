/**
 *
 */
package com.aawhere.track;

import static org.junit.Assert.*;

import java.io.StringWriter;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Set;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.Unit;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.text.TextDocumentProducer;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.measure.BoundingBoxXmlAdapter;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsOptionalXmlAdapter;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.person.pref.PrefMeasureUtil;
import com.aawhere.person.pref.PrefTestUtil;
import com.aawhere.personalize.xml.XmlAdapterTestUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.track.TrackSummary.Builder;
import com.aawhere.xml.bind.JAXBTestUtil;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

/**
 * @author roller
 * 
 */
public class TrackSummaryUnitTest {

	/**
	 * TODO: TrackSummary was setup to be Mutatable, but we can't test it due to TN-88
	 */
	public TrackSummaryUnitTest() {
		super();
	}

	private TrackSummary sampleTrackSummary;

	@Before
	public void setUp() {
		DocumentFactory.getInstance().register(new TextDocumentProducer.Builder().build());
		sampleTrackSummary = TrackSummaryTestUtil.getSampleTrackSummary();
	}

	@Test
	public void testPopulateWithPoints() {
		// the getter does a bunch of simple assertions so that's really the
		// test.
		assertNotNull(sampleTrackSummary);
	}

	@Test
	public void testCreate() {
		final Builder builder = TrackSummary.create();
		creationPopulation(builder);
		assertCreation(builder.build());
	}

	@Test
	public void testSpatialBufferSimpleAssignment() {
		final int count = TrackSummary.MAX_GEO_CELLS_COUNT - 1;
		GeoCells cells = assertSpatialBuffer(count);
		SetView<GeoCell> difference = Sets.difference(cells.getAll(), sampleTrackSummary.getSpatialBuffer().getAll());
		TestUtil.assertEmpty(difference);

	}

	@Test
	public void testBoundingBoxProvidedIsPreserved() {
		BoundingBox box = BoundingBoxTestUtils.createRandomBoundingBox();
		TrackSummary mutated = TrackSummary.mutate(sampleTrackSummary).setBoundingBox(box)
				.setSpatialBuffer(sampleTrackSummary.getSpatialBuffer()).build();
		BoundingBoxTestUtils.assertEquals(box, mutated.boundingBox());
	}

	@Test
	public void testSpatialBufferSizeExceed() {
		final int count = TrackSummary.MAX_GEO_CELLS_COUNT + 1;
		GeoCells cells = assertSpatialBuffer(count);

		assertEquals("resolution should have been decreased", cells.getMaxResolution().lower(), sampleTrackSummary
				.getSpatialBuffer().getMaxResolution());
	}

	/**
	 * @param count
	 * @return
	 */
	public GeoCells assertSpatialBuffer(final int count) {
		GeoCells geoCells = GeoCellTestUtil.createRandomGeoCells(count, TrackSummary.MAX_BOUNDS_RESOLUTION);
		sampleTrackSummary = TrackSummary.mutate(sampleTrackSummary).setSpatialBuffer(geoCells).build();
		TestUtil.assertLessThan("the max count shouldn't be exceeded",
								TrackSummary.MAX_GEO_CELLS_COUNT + 1,
								sampleTrackSummary.getSpatialBuffer().getAll().size());
		return geoCells;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere
	 * .persist.BaseEntity.Builder)
	 */
	protected void creationPopulation(TrackSummary.Builder builder) {
		builder.setNumberOfTrackpoints(sampleTrackSummary.getNumberOfTrackpoints());
		builder.setDistance(sampleTrackSummary.getDistance());
		builder.populatePoints(sampleTrackSummary.getStart(), sampleTrackSummary.getFinish());
		builder.setElevationGain(sampleTrackSummary.getElevationGain());
		builder.setSpatialBuffer(sampleTrackSummary.getSpatialBuffer());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere
	 * .persist.BaseEntity)
	 */
	public void assertCreation(TrackSummary sample) {
		TrackpointTestUtil.assertTrackpointEquals(this.sampleTrackSummary.getStart(), sample.getStart());
		TrackpointTestUtil.assertTrackpointEquals(this.sampleTrackSummary.getFinish(), sample.getFinish());
		MeasureTestUtil.assertEquals(sampleTrackSummary.getDistance(), sample.getDistance());
		assertEquals(sampleTrackSummary.getElevationChange(), sample.getElevationChange());
		Unit<ElevationChange> elevationChangeUnit = ExtraUnits.METER_CHANGE;
		TestUtil.assertDoubleEquals(sampleTrackSummary.getElevationGain().doubleValue(elevationChangeUnit), sample
				.getElevationGain().doubleValue(elevationChangeUnit));
		assertEntityEquals(sampleTrackSummary, sample);
	}

	@Test
	public void testTrackpointBuildsAsSimpleTrackpoint() {
		TrackSummary trackSummary = TrackSummaryTestUtil.getMockTrackpointSampleTrackSummary();
		assertNotNull(trackSummary);
	}

	@Test
	public void testElevationGainAsLength() {
		Length elevationGain = MeasureTestUtil.createRandomLength();
		TrackSummary ts = TrackSummary.mutate(sampleTrackSummary).setElevationGain(elevationGain).build();
		assertNotNull(ts.getElevationGain());
		assertEquals(elevationGain, ts.getElevationGain());
	}

	@Test
	public void testTn554DontStoreInfinity() {
		Length length = MeasurementUtil.createLengthInMeters(0.01);
		org.joda.time.Duration time = new org.joda.time.Duration(5);
		TrackSummary ts = TrackSummary.mutate(sampleTrackSummary).setDistance(length).setElapsedDuration(time).build();
		assertNotNull(ts.getAverageSpeed());
		assertTrue(!Double.isInfinite(ts.getAverageSpeed().doubleValue(MetricSystem.METRES_PER_SECOND)));
	}

	@Test
	public void testXmlMarshal() throws JAXBException {
		TrackSummary expected = TrackSummaryTestUtil.getSampleTrackSummary();
		JAXBContext jc = JAXBContext.newInstance(TrackSummary.class);
		Marshaller marshaller = jc.createMarshaller();
		Set<XmlAdapter<?, ?>> adapters = getAdapters();
		for (XmlAdapter<?, ?> adapter : adapters) {
			marshaller.setAdapter(adapter);
		}
		StringWriter writer = new StringWriter();
		marshaller.marshal(expected, writer);
		String actual = writer.toString();
		TestUtil.assertContains(actual, TrackSummary.FIELD.DOMAIN);
		// The decision to have a display isn't up to the personalizer.
		// assertFalse("shouldn't contain a display element since locale was not set"
		// + actual,
		// actual.contains("display"));
	}

	@Test
	public void testXmlPersonalizedMarshalUs() throws JAXBException {
		Locale locale = Locale.US;
		final NumberFormat formatter = NumberFormat.getNumberInstance(locale);
		assertPersonalized(locale, formatter);
	}

	@Test
	public void testXmlPersonalizedMarshalFr() throws JAXBException {
		Locale locale = Locale.FRANCE;
		final NumberFormat formatter = NumberFormat.getNumberInstance(locale);
		assertPersonalized(locale, formatter);
	}

	@Test
	public void testJaxbRoundTrip() throws JAXBException {
		TrackSummary sample = this.sampleTrackSummary;
		Class<?>[] classesToBeBound = { BoundingBox.class, GeoCoordinate.class, TrackSummary.class };
		JAXBTestUtil<TrackSummary> util = JAXBTestUtil.create(sample).classesToBeBound(classesToBeBound)
				.adapter(getAdapters()).build();
		TrackSummary actual = util.getActual();

		// just re-use the existing method to ensure the sample was "re-created"
		// appropriately
		assertCreation(actual);
		// assert that the two are indeed equal.
		assertEntityEquals(sample, actual);
	}

	public void assertEntityEquals(TrackSummary expected, TrackSummary actual) {
		assertEquals(expected.getNumberOfTrackpoints(), actual.getNumberOfTrackpoints());
		TrackpointTestUtil.assertTrackpointEquals(expected.getStart(), actual.getStart());
		TrackpointTestUtil.assertTrackpointEquals(expected.getFinish(), actual.getFinish());
		TestUtil.assertDoubleEquals("distance gets only Length since it is using LengthXmlAdapter", expected
				.getDistance().getValue().doubleValue(), actual.getDistance().getValue().doubleValue());
		assertNotNull(actual.getDistance());
		assertNotNull(actual.getAverageSpeed());
		assertNotNull(actual.getBoundingBox());
		assertNotNull(actual.getBoundingBox().getNorthEast());
		assertNotNull(actual.getBoundingBox().getSouthWest());
		assertNotNull(actual.getBoundingBox().getNorthEast().getLatitude());
		assertNotNull(actual.getBoundingBox().getSouthWest().getLongitude());
		assertNotNull(actual.getBoundingBox().getNorthEast().getLatitude().getInDecimalDegrees());
		assertNotNull(actual.getBoundingBox().getSouthWest().getLongitude().getInDecimalDegrees());
	}

	/**
	 * @param locale
	 * @param formatter
	 * @throws JAXBException
	 */
	private void assertPersonalized(Locale locale, final NumberFormat formatter) throws JAXBException {
		TrackSummary expected = TrackSummaryTestUtil.getSampleTrackSummary();
		Configuration defaultPrefs = PrefTestUtil.createSiPreferences(locale);
		formatter.setMaximumFractionDigits(PrefMeasureUtil.getMaxDigitsFor(MeasurementUtil.classFor(expected
				.getDistance()), defaultPrefs));
		Double expectedValue = expected.getDistance()
				.doubleValue(PrefMeasureUtil.getUnitFor(Length.class, defaultPrefs));
		String expectedDistance = formatter.format(expectedValue);

		JAXBContext jc = JAXBContext.newInstance(TrackSummary.class);
		Marshaller marshaller = jc.createMarshaller();
		Set<XmlAdapter<?, ?>> adapters = getAdapters(formatter);
		for (XmlAdapter<?, ?> adapter : adapters) {
			marshaller.setAdapter(adapter);
		}
		StringWriter writer = new StringWriter();
		marshaller.marshal(expected, writer);
		String actual = writer.toString();
		TestUtil.assertContains(actual, TrackSummary.FIELD.DOMAIN);
		TestUtil.assertContains(actual, expectedDistance);
	}

	public static TrackSummaryUnitTest getInstance() {
		TrackSummaryUnitTest instance = new TrackSummaryUnitTest();
		instance.setUp();
		return instance;
	}

	public Set<XmlAdapter<?, ?>> getAdapters() {
		final NumberFormat formatter = NumberFormat.getNumberInstance(Locale.US);
		return getAdapters(formatter);
	}

	public Set<XmlAdapter<?, ?>> getAdapters(NumberFormat formatter) {
		Set<XmlAdapter<?, ?>> adapters = XmlAdapterTestUtils.getAdapters();
		adapters.add(new BoundingBoxXmlAdapter());
		adapters.add(new GeoCellsOptionalXmlAdapter(true));
		return adapters;
	}

}
