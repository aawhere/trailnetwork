/**
 * 
 */
package com.aawhere.track;

import static com.aawhere.track.ExampleTracks.*;
import static org.junit.Assert.*;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.test.TestUtil;

/**
 * Tests {@link TrackSegment}.
 * 
 * @author roller
 * 
 */
public class TrackSegmentUnitTest {

	public static final double S1_EXPECTED_ELEVATION = (E1 + E2) / 2;

	@Test
	public void testTime() {
		long t0 = 500000000;
		long t1 = 500001000;
		Trackpoint p0 = TrackUtil.createPoint(t0);
		Trackpoint p1 = TrackUtil.createPoint(t1);
		TrackSegment s = new SimpleTrackSegment(p0, p1);
		assertEquals(p0, s.getFirst());
		assertEquals(p1, s.getSecond());
		assertNull(s.getDistance());
		assertNotNull(s.getInterval());
		assertNotNull(s.getDuration());
		assertEquals(t1 - t0, s.getDuration().getMillis());
	}

	@Test
	public void testDistance() {
		TrackSegment segment = S1;
		Length expectedDistance = new GeoCalculator.Builder().build().distanceBetween(	segment.getFirst(),
																						segment.getSecond());
		assertEquals(expectedDistance, segment.getDistance());
	}

	/** Simple test for the basic elevations. */
	@Test
	public void testElevation() {
		TrackSegment segment = S1;
		TestUtil.assertDoubleEquals(S1_EXPECTED_ELEVATION,
									segment.getAverageElevation().doubleValue(segment.getFirst().getElevation()
											.getUnit()));

	}

	@Test
	public void testSegmentDistance() {
		MeasureTestUtil.assertEquals(S1_LENGTH, S1.getDistance());
	}

	@Test
	public void testElevationWithExtraTime() {
		TrackSegment segment = S3_RETURN;
		double expected = E3;
		TestUtil.assertDoubleEquals(expected,
									segment.getAverageElevation().doubleValue(segment.getAverageElevation().getUnit()));
	}

}
