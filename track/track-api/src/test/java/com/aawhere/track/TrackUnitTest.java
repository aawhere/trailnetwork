/**
 * 
 */
package com.aawhere.track;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;

/**
 * Tests for {@link SimpleTrack} and {@link SimpleTrackpoint} and also providing basic tracks for
 * testing other components.
 * 
 * @author roller
 * 
 */
public class TrackUnitTest {

	public static TrackUnitTest getInstance() {
		return new TrackUnitTest();
	}

	public static void assertEquals(Trackpoint expected, Trackpoint actual) {
		TrackpointTestUtil.assertTrackpointEquals(expected, actual);
	}

	@Test
	public void testSimpleTrackpoint() {
		Trackpoint trackpoint = TrackpointTestUtil.generateRandomTrackpoint();
		TrackpointTestUtil.assertTrackpointEquals(trackpoint, trackpoint);

	}

	@Test
	public void shouldSerialize() throws IOException, ClassNotFoundException {
		Trackpoint trackpoint = TrackpointTestUtil.generateRandomTrackpoint();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(trackpoint);
		byte[] ba = baos.toByteArray();
		// Should not throw an exception.
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(ba));
		Trackpoint deserialized = (Trackpoint) ois.readObject();
		assertEquals(trackpoint, deserialized);
	}

	@Test
	public void testTrackSerializableUsingCollectionConstructor() {
		SimpleTrack generateRandomTrack = TrackTestUtil.generateRandomTrack();
		SimpleTrack copy = new SimpleTrack.Builder(generateRandomTrack).build();
		byte[] serialize = SerializationUtils.serialize(copy);
		assertNotNull(serialize);
	}

	/**
	 * Seriously, if you thought {@link #generateRandomTrackpoint()} was random, this is even more
	 * random. The trackpoints have no relation to each other.
	 * 
	 * @return
	 */
	public Track generateRandomTrack() {
		return TrackTestUtil.generateRandomTrack();
	}

}
