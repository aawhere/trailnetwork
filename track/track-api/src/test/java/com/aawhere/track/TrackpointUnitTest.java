/**
 *
 */
package com.aawhere.track;

import static com.aawhere.track.ExampleTracks.*;
import static com.aawhere.track.TrackpointTestUtil.*;
import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Testing {@link SimpleTrackpoint} and {@link Trackpoint} interface.
 * 
 * @author roller
 * 
 */
public class TrackpointUnitTest {

	@Test
	public void testTrackpoint() {
		// generator does granular assertions.
		assertNotNull(generateRandomTrackpoint());
	}

	/**
	 * @return
	 */
	public static TrackpointUnitTest getInstance() {

		return new TrackpointUnitTest();
	}

	/**
	 * Tests the basics using {@link SimpleTrackpoint}.
	 * 
	 */
	@Test
	public void testBasics() {
		assertBasics(P1);
	}

	@Test
	public void testFromAnother() {
		SimpleTrackpoint expected = generateRandomSimpleTrackpoint();
		SimpleTrackpoint actual = SimpleTrackpoint.copy(expected).build();
		TrackpointTestUtil.assertTrackpointEquals(expected, actual);
	}

	@Test
	public void testEquals() {
		TrackpointTestUtil.assertTrackpointEquals(RANDOM_SIMPLE, RANDOM_SIMPLE);
		TrackpointTestUtil.assertTrackpointEquals(RANDOM_SIMPLE, RANDOM_MOCK);
		TrackpointTestUtil.assertTrackpointEquals(RANDOM_MOCK, RANDOM_MOCK);
		TrackpointTestUtil.assertTrackpointEquals(WITHOUT_LOCATION, WITHOUT_LOCATION);
		TrackpointTestUtil.assertTrackpointEquals(WITHOUT_ELEVATION, WITHOUT_ELEVATION);
		assertFalse(TrackUtil.equalsDeeply(WITHOUT_ELEVATION, RANDOM_SIMPLE));
		assertFalse(TrackUtil.equalsDeeply(WITHOUT_ELEVATION, WITHOUT_LOCATION));
		assertFalse(TrackUtil.equalsDeeply(RANDOM_SIMPLE, WITHOUT_LOCATION));
		assertFalse(TrackUtil.equalsDeeply(WITHOUT_LOCATION, RANDOM_SIMPLE));
		assertFalse(TrackUtil.equalsDeeply(WITHOUT_LOCATION, WITHOUT_ELEVATION));
		assertFalse(TrackUtil.equalsDeeply(RANDOM_SIMPLE, WITHOUT_ELEVATION));

	}
}
