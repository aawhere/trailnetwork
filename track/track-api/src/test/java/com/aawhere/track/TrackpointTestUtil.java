/**
 * 
 */
package com.aawhere.track;

import static com.aawhere.test.TestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;

import com.aawhere.measure.Elevation;
import com.aawhere.measure.ElevationUnitTest;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class TrackpointTestUtil {

	public static final SimpleTrackpoint RANDOM_SIMPLE = TrackpointTestUtil.generateRandomSimpleTrackpoint();
	/** Same as {@link #RANDOM_SIMPLE}, but in Mock form. */
	public static final MockTrackpoint RANDOM_MOCK = new MockTrackpoint(RANDOM_SIMPLE);
	public static final SimpleTrackpoint WITHOUT_ELEVATION = SimpleTrackpoint.copy(RANDOM_SIMPLE).setElevation(null)
			.build();
	public static final SimpleTrackpoint WITHOUT_LOCATION = SimpleTrackpoint.copy(RANDOM_SIMPLE).setLocation(null)
			.build();

	/**
	 * Seriously, this won't create a trackpoint that makes any sense, just one that is populated.
	 * 
	 * 
	 * @return
	 */
	static public Trackpoint generateRandomTrackpoint() {
		return generateRandomSimpleTrackpoint();
	}

	static public MockTrackpoint generateRandomMockTrackpoint() {
		return new MockTrackpoint(generateRandomTrackpoint());
	}

	static public SimpleTrackpoint generateRandomSimpleTrackpoint() {
		Elevation elevation = new ElevationUnitTest().generateRandomQuantity();
		Latitude latitude = MeasureTestUtil.createRandomLatitude();
		Longitude longitude = MeasureTestUtil.createRandomLongitude();
		GeoCoordinate location = new GeoCoordinate.Builder().setLatitude(latitude).setLongitude(longitude).build();
		DateTime timestamp = MeasureTestUtil.createRandomTimestamp();
		SimpleTrackpoint result = new SimpleTrackpoint.Builder().setElevation(elevation).setLocation(location)
				.setTimestamp(timestamp).build();
		assertEquals(elevation, result.getElevation());
		assertEquals(latitude, result.getLocation().getLatitude());
		assertEquals(longitude, result.getLocation().getLongitude());
		assertEquals(timestamp, result.getTimestamp());
		return result;

	}

	static public List<Trackpoint> generateOrderedTrackpoints() {
		List<Trackpoint> trackpoints = generateRandomTrackpoints();
		Collections.sort(trackpoints, TrackpointTimestampComparator.getInstance());
		return trackpoints;
	}

	/**
	 * Generates a List of trackpoints that are populated, but chaotic in nature.
	 * 
	 * @return
	 */
	static public List<Trackpoint> generateRandomTrackpoints() {
		int numberOfTrackpoints = TestUtil.generateRandomInt(5, 50);
		ArrayList<Trackpoint> trackpoints = new ArrayList<Trackpoint>(numberOfTrackpoints);
		for (int i = 0; i < numberOfTrackpoints; i++) {
			trackpoints.add(generateRandomTrackpoint());
		}
		assertEquals(numberOfTrackpoints, trackpoints.size());
		return trackpoints;
	}

	/**
	 * Useful tool for verifying implementation of {@link Trackpoint}.
	 * 
	 * @param trackpoint
	 */
	public static void assertBasics(Trackpoint trackpoint) {
		assertNotNull(trackpoint);
		assertNotNull(trackpoint.getTimestamp());
		assertNotNull(trackpoint.toString());
		assertEquality(trackpoint);
	}

	public static void assertEquality(Trackpoint trackpoint) {
		final SimpleTrackpoint copy = SimpleTrackpoint.copy(ExampleTracks.P1).build();
		assertEquals(trackpoint, copy);
		assertEquals(trackpoint.hashCode(), copy.hashCode());

		final SimpleTrackpoint different = TrackUtil.copy(trackpoint, TestUtil.generateRandomLong());
		assertNotEquals(trackpoint, different);
	}

	public static class MockTrackpoint
			implements Trackpoint {

		private static final long serialVersionUID = -7386360563648713885L;

		public MockTrackpoint(Trackpoint other) {
			elevation = other.getElevation();
			location = other.getLocation();
			timestamp = other.getTimestamp();
		}

		private Elevation elevation;
		private DateTime timestamp;
		private GeoCoordinate location;

		@Override
		public DateTime getTimestamp() {
			return timestamp;
		}

		@Override
		public Elevation getElevation() {
			return elevation;
		}

		@Override
		public GeoCoordinate getLocation() {
			return this.location;
		}

		@Override
		public Boolean isLocationCapable() {
			return this.location != null;
		}

		@Override
		public boolean equals(Trackpoint that) {
			return TrackUtil.equals(this, that);
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object that) {
			return TrackUtil.equals(this, that);
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return TrackUtil.hashCode(this);
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return TrackUtil.toString(this);
		}
	}

	/**
	 * @param trackpoint1
	 * @param trackpoint2
	 */
	public static void assertTrackpointEquals(Trackpoint trackpoint1, Trackpoint trackpoint2) {
		assertTrackpointEquals("trackpoints not equal", trackpoint1, trackpoint2);
	}

	public static void assertTrackpointEquals(String message, Trackpoint trackpoint1, Trackpoint trackpoint2) {
		assertTrue(	message + " -> " + trackpoint1 + " != " + trackpoint2,
					TrackUtil.equalsDeeply(trackpoint1, trackpoint2));
	}

	/**
	 * provides a trackpoint with the given timestamp and some random location
	 * 
	 * @param startTime
	 * @return
	 */
	public static SimpleTrackpoint trackpoint(DateTime timestamp) {
		return SimpleTrackpoint.copy(generateRandomTrackpoint()).setTimestamp(timestamp).build();
	}
}
