/**
 * 
 */
package com.aawhere.track;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.joda.time.DateTime;
import org.junit.Test;

/**
 * Tests the basic {@link TrackTimestampIterator} class.
 * 
 * @author Aaron Roller
 * 
 */
public class TrackTimestampIteratorUnitTest {

	@Test
	public void testBasics() {

		Track track = ExampleTracks.LINE;
		List<Trackpoint> trackpoints = ExampleTracks.LINE_POINTS;
		TrackTimestampIterator iterator = new TrackTimestampIterator.Builder().setTrack(track).build();
		int whichTrackpoint = 0;
		for (Iterator<Trackpoint> expectedIterator = trackpoints.iterator(); expectedIterator.hasNext();) {
			DateTime actual = iterator.next();
			DateTime expected = expectedIterator.next().getTimestamp();
			assertEquals(whichTrackpoint + "th trackpoint times don't match", expected, actual);
		}
	}

	@Test(expected = NoSuchElementException.class)
	public void testNextOverrun() {
		TrackTimestampIterator iterator = new TrackTimestampIterator.Builder().setTrack(ExampleTracks.LINE).build();
		iterator.next();
		iterator.next();
		iterator.next();

		iterator.next();
		fail("Should not have succeeded to get Next");
	}
}
