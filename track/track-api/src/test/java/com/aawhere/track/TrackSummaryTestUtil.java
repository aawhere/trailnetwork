/**
 *
 */
package com.aawhere.track;

import static org.junit.Assert.*;

import java.util.Set;

import javax.measure.quantity.Length;

import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.joda.time.DurationTestUtil;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.test.TestUtil;
import com.aawhere.track.TrackpointTestUtil.MockTrackpoint;

import com.google.common.collect.Sets;

/**
 * Helpful to generate {@link TrackSummary} and related items.
 * 
 * @author Aaron Roller
 * 
 */
public class TrackSummaryTestUtil {

	static int START_TIME_IN_MILLIS = 10000000;
	static int TIME_DELTA_IN_MILLIS = 100000;
	static int FINISH_TIME_IN_MILLIS = START_TIME_IN_MILLIS + TIME_DELTA_IN_MILLIS;
	public final static double LAT_N = 1;
	public final static double LAT_S = 0;
	public final static double LON_E = 1;
	public final static double LON_W = 0;
	static double ELV_START = 100;
	static double ELV_FINISH = 50;

	/*
	 * Useful for testing the non-SimpleTrackpoint to SimpleTrackpoint conversion in TrackSummary
	 */
	public static TrackSummary getMockTrackpointSampleTrackSummary() {
		Trackpoint start = new MockTrackpoint(TrackUtil.createPoint(START_TIME_IN_MILLIS, LAT_S, LON_W, ELV_START));
		Trackpoint finish = new MockTrackpoint(TrackUtil.createPoint(FINISH_TIME_IN_MILLIS, LAT_N, LON_E, ELV_FINISH));
		return getSample(start, finish);
	}

	/**
	 * @param box
	 * @return
	 */
	public static TrackSummary getSampleTrackSummary(GeoCellBoundingBox box) {
		return getSample(TrackUtil.createPoint(	START_TIME_IN_MILLIS,
												box.getMin().getLatitude().getInDecimalDegrees(),
												box.getMin().getLongitude().getInDecimalDegrees(),
												ELV_START), TrackUtil.createPoint(FINISH_TIME_IN_MILLIS, box.getMax()
				.getLatitude().getInDecimalDegrees(), box.getMax().getLongitude().getInDecimalDegrees(), ELV_FINISH));
	}

	/**
	 * @param start
	 * @param finish
	 * @return
	 */
	public static TrackSummary getSample(Trackpoint start, Trackpoint finish) {

		Set<GeoCell> geoCells = Sets.newHashSet();
		geoCells.addAll(GeoCellUtil.geoCells(start.getLocation(), TrackSummary.MAX_BOUNDS_RESOLUTION).getAll());
		geoCells.addAll(GeoCellUtil.geoCells(finish.getLocation(), TrackSummary.MAX_BOUNDS_RESOLUTION).getAll());

		GeoCellBoundingBox box = GeoCellBoundingBox.createGeoCell().setGeoCells(geoCells).build();

		geoCells = GeoCellUtil.withAncestors(geoCells).getAll();
		return trackSummary(start, finish, box, GeoCells.create().addAll(geoCells).build());
	}

	/**
	 * @param start
	 * @return
	 */
	public static TrackSummary getSample(SimpleTrackpoint start) {
		SimpleTrackpoint finish = TrackpointTestUtil.trackpoint(start.getTimestamp().plus(DurationTestUtil.duration()));
		return getSample(start, finish);
	}

	public static TrackSummary getSampleTrackSummary() {
		Trackpoint start = TrackUtil.createPoint(START_TIME_IN_MILLIS, LAT_S, LON_W, ELV_START);
		Trackpoint finish = TrackUtil.createPoint(FINISH_TIME_IN_MILLIS, LAT_N, LON_E, ELV_FINISH);
		return getSample(start, finish);
	}

	public static TrackSummary createAndTestTrackSummary(Trackpoint start, Trackpoint finish, BoundingBox box,
			GeoCells geoCells) {
		TrackSummary trackSummary = trackSummary(start, finish, box, geoCells);
		assertNotNull(trackSummary.getStart());
		// TODO: equals not yet defined for Trackpoint
		TrackpointTestUtil.assertTrackpointEquals(start, trackSummary.getStart());
		TrackpointTestUtil.assertTrackpointEquals(finish, trackSummary.getFinish());
		assertNotNull(trackSummary.getFinish());
		assertNotNull(trackSummary.getStartTime());
		assertEquals(START_TIME_IN_MILLIS, trackSummary.getStartTime().getMillis());

		assertNotNull(trackSummary.getFinishTime());
		assertEquals(FINISH_TIME_IN_MILLIS, trackSummary.getFinishTime().getMillis());

		assertNotNull(trackSummary.getInterval());

		assertNotNull(trackSummary.getElapsedDuration());
		assertEquals(TIME_DELTA_IN_MILLIS, trackSummary.getElapsedDuration().getMillis());

		assertNotNull(trackSummary.getElevationChange());
		BoundingBoxTestUtils.assertContains(trackSummary.getBoundingBox(), box);
		return trackSummary;
	}

	public static TrackSummary trackSummary(Trackpoint start, Trackpoint finish, BoundingBox box, GeoCells geoCells) {
		Length distanceBetweenTrackpoints = MeasurementUtil.createLengthInMeters(TestUtil.generateRandomDouble());
		Length distance = MeasurementUtil.createLengthInMeters(TestUtil.generateRandomDouble());
		ElevationChange elevationGain = MeasurementUtil.createElevationChange(	Math.abs(TestUtil
																						.generateRandomDouble()),
																				ExtraUnits.METER_CHANGE);

		final Integer numberOfTrackpoints = TestUtil.generateRandomInt();
		TrackSummary trackSummary = new TrackSummary.Builder()
				.populatePoints(start, finish)
				.setNumberOfTrackpoints(numberOfTrackpoints)
				.setDistance(distance)
				.setMeanDistanceBetweenTrackpoints(distanceBetweenTrackpoints)
				.setDocumentId(IdentifierTestUtil.generateRandomLongId(ArchivedDocument.class, ArchivedDocumentId.class))
				.setElevationGain(elevationGain).setSpatialBuffer(geoCells).build();

		return trackSummary;
	}

	/** Provides access to other tests not allowed in main code. */
	public static TrackSummary.Builder mutate(TrackSummary summary) {
		return TrackSummary.mutate(summary);
	}

}
