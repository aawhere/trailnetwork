/**
 *
 */
package com.aawhere.track;

import static com.aawhere.track.TrackUtil.*;
import static javax.measure.unit.MetricSystem.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.Unit;

import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.collections.ListUtilsExtended;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.unit.ExtraUnits;

/**
 * Static class useful for unit testing tracks.
 * 
 * @author roller
 * 
 */
public class ExampleTracks {

	/**
	 *
	 */
	private static final int LAT_OR_LON_WAY_OFF = 89;
	public static final int TIME_INCREMENT = 1000;
	/** the starting time */
	public static final int T1 = 100000;
	public static final int T_MINUS_1 = T1 - TIME_INCREMENT;;
	public static final int T1_5 = T1 + (TIME_INCREMENT / 2);
	public static final int T2 = T1 + TIME_INCREMENT;
	public static final int T3 = T2 + TIME_INCREMENT;
	public static final int T4 = T3 + TIME_INCREMENT;
	public static final int T5 = T4 + TIME_INCREMENT;
	public static final int T6 = T5 + TIME_INCREMENT;
	public static final int T7 = T6 + TIME_INCREMENT;
	public static final int T8 = T7 + TIME_INCREMENT;
	public static final int T9 = T8 + TIME_INCREMENT;

	public static final double LONGITUDE = 0;
	public static final double LAT_INCREMENT = 1;
	/**
	 * Tiny distance used to test closeness.
	 * 
	 * @see GeoCoordinate#TEN_CM_PRECISION
	 */
	public static final double LAT_MICRO_INCREMENT = 0.000001;
	public static final double LAT_1 = 0;
	public static final double LAT_MINUS_1 = LAT_1 - LAT_INCREMENT;
	public static final double LAT_1_5 = LAT_1 + (LAT_INCREMENT / 2);
	public static final double LAT_2 = LAT_1 + LAT_INCREMENT;
	public static final double LAT_3 = LAT_2 + LAT_INCREMENT;
	public static final double LAT_4 = LAT_3 + LAT_INCREMENT;
	public static final double LAT_5 = LAT_4 + LAT_INCREMENT;
	public static final double LAT_6 = LAT_5 + LAT_INCREMENT;
	public static final double LAT_7 = LAT_6 + LAT_INCREMENT;
	public static final double LAT_8 = LAT_7 + LAT_INCREMENT;
	public static final double LAT_9 = LAT_8 + LAT_INCREMENT;
	public static final double LON_2 = LONGITUDE + LAT_INCREMENT;
	public static final Unit<Length> UNIT_FOR_LENGTH = MetricSystem.METRE;
	public static final Unit<Elevation> UNIT_FOR_ELE = ExtraUnits.METER_MSL;
	public static final int ELEVATION_INCREMENT = 10;
	public static final int ELEVATION_ABOVE_OFFSET = 1000;
	public static final Length ELEVATION_INCREMENT_LENGTH = MeasurementQuantityFactory.getInstance(Length.class)
			.create(ELEVATION_INCREMENT, METRE);
	public static final int E1 = 0;
	public static final int E_MINUS_1 = E1 - ELEVATION_INCREMENT;
	public static final int E2 = E1 + ELEVATION_INCREMENT;
	public static final int E3 = E2 + ELEVATION_INCREMENT;
	public static final int E4 = E3 + ELEVATION_INCREMENT;
	public static final int E5 = E4 + ELEVATION_INCREMENT;
	public static final int E6 = E5 + ELEVATION_INCREMENT;
	public static final int E7 = E6 + ELEVATION_INCREMENT;
	public static final int E8 = E7 + ELEVATION_INCREMENT;
	public static final int E9 = E8 + ELEVATION_INCREMENT;

	public static final Trackpoint P_MINUS_1_T2 = createPoint(T2, LAT_MINUS_1, LONGITUDE, E1);
	public static final Trackpoint P_MINUS_1 = createPoint(T_MINUS_1, LAT_MINUS_1, LONGITUDE, E_MINUS_1);
	public static final Trackpoint P1 = createPoint(T1, LAT_1, LONGITUDE, E1);
	public static final Trackpoint P1_T2 = createPoint(T2, LAT_1, LONGITUDE, E1);
	/** Micro distance between P1 and this towards P2 */
	public static final Trackpoint P1_MICRO = createPoint(T1, LAT_1 + LAT_MICRO_INCREMENT, LONGITUDE, E1);
	public static final Trackpoint P1_MISSING_LOCATION = createPointAllowNulls(	Long.valueOf(T1),
																				null,
																				null,
																				Integer.valueOf(E1));

	public static final Trackpoint P2 = createPoint(T2, LAT_2, LONGITUDE, E2);
	/** When a point away from P1 has the same time as P1 to produce detectable errors. */
	public static final Trackpoint P2_T1 = createPoint(T1, LAT_2, LONGITUDE, E2);
	public static final Trackpoint P2_T3 = createPoint(T3, LAT_2, LONGITUDE, E2);
	public static final Trackpoint P2_OFF_MARK = createPoint(T2, LAT_2, LON_2, E2);

	public static final Trackpoint P3 = createPoint(T3, LAT_3, LONGITUDE, E3);
	public static final Trackpoint P3_T4 = createPoint(T4, LAT_3, LONGITUDE, E3);
	public static final Trackpoint P3_MICRO_BEFORE = createPoint(T3, LAT_3 - LAT_MICRO_INCREMENT, LONGITUDE, E3);
	public static final Trackpoint P4 = createPoint(T4, LAT_4, LONGITUDE, E4);
	public static final Trackpoint P3_X2 = createPoint(T5, LAT_3, LONGITUDE, E3);
	public static final Trackpoint P2_X2 = createPoint(T6, LAT_2, LONGITUDE, E2);
	public static final Trackpoint P1_X2 = createPoint(T7, LAT_1, LONGITUDE, E1);
	public static final Trackpoint P_MINUS_1_X2 = createPoint(T8, LAT_MINUS_1, LONGITUDE, E_MINUS_1);
	public static final Trackpoint P2_MISSING_LOCATION = createPointAllowNulls(	Long.valueOf(T2),
																				null,
																				null,
																				Integer.valueOf(E2));
	public static final Trackpoint P5 = createPoint(T5, LAT_5, LONGITUDE, E5);
	public static final Trackpoint P6 = createPoint(T6, LAT_6, LONGITUDE, E6);
	public static final Trackpoint P7 = createPoint(T7, LAT_7, LONGITUDE, E7);
	public static final Trackpoint P8 = createPoint(T8, LAT_8, LONGITUDE, E8);
	public static final Trackpoint P9 = createPoint(T9, LAT_9, LONGITUDE, E9);

	public static final Trackpoint LOOP_1_RETURN_MIDDLE = createPoint(T4, LAT_2, LON_2, E1);
	public static final Trackpoint LOOP_2_START = createPoint(T5, LAT_1, LONGITUDE, E1);
	public static final Trackpoint LOOP_2_MIDDLE = createPoint(T6, LAT_2, LONGITUDE, E1);
	public static final Trackpoint LOOP_2_FINISH_LINE = createPoint(T7, LAT_3, LONGITUDE, E1);
	public static final Trackpoint LOOP_2_RETURN_MIDDLE = createPoint(T8, LAT_2, LON_2, E1);
	public static final Trackpoint LOOP_3_START = createPoint(T9, LAT_1, LONGITUDE, E1);

	// for testing 3D
	public static final Trackpoint P1_ABOVE = createPoint(T1, LAT_1, LONGITUDE, E1 + ELEVATION_ABOVE_OFFSET);
	public static final Trackpoint P2_ABOVE = createPoint(T2, LAT_2, LONGITUDE, E2 + ELEVATION_ABOVE_OFFSET);
	public static final Trackpoint P3_ABOVE = createPoint(T3, LAT_3, LONGITUDE, E3 + ELEVATION_ABOVE_OFFSET);
	public static final Trackpoint P4_ABOVE = createPoint(T4, LAT_4, LONGITUDE, E4 + ELEVATION_ABOVE_OFFSET);

	public static final Trackpoint P3_MISSING_ELEVATION = createPointAllowNulls(Long.valueOf(T2),
																				LAT_3,
																				LONGITUDE,
																				null);
	public static final TrackSegment BEFORE_S1 = new SimpleTrackSegment(P_MINUS_1, P1);
	public static final TrackSegment S1 = new SimpleTrackSegment(P1, P2);
	public static final TrackSegment S1_MICRO = new SimpleTrackSegment(P1_MICRO, P2);
	public static final TrackSegment S2 = new SimpleTrackSegment(P2, P3);
	public static final TrackSegment S3 = new SimpleTrackSegment(P3, P4);
	/** Used when you need to skip P3 starting with P2 end with P4 */
	public static final TrackSegment S2_S3_COMBINED = new SimpleTrackSegment(P2, P4);
	/** Used to test when track functions when a segment has the same point with increasing times. */
	public static final TrackSegment ZERO_LENGTH_SEGMENT = new SimpleTrackSegment(P1, P1_X2);
	public static final TrackSegment ZERO_DURATION_SEGMENT = new SimpleTrackSegment(P1, P1);
	/** S3 segment and returns to the starting point. Useful for weighting */
	public static final TrackSegment S3_RETURN = new SimpleTrackSegment(P3, P3_X2);
	/** Measured on google maps */
	public static final Length S1_LENGTH = S1.getDistance();

	public static final Length S1_HALF_LENGTH = QuantityMath.create(S1_LENGTH).dividedBy(2).getQuantity();

	public static final Length LOOP_RETURN_MIDDLE_SEGMENT_LENGTH = TrackUtil.distanceBetween(P3, LOOP_1_RETURN_MIDDLE);
	public static final Length LOOP_RETURN_TO_START_SEGMENT_LENGTH = TrackUtil
			.distanceBetween(LOOP_1_RETURN_MIDDLE, P1);
	public static final List<Trackpoint> S1_POINTS = new SimpleTrack.TrackpointCollectionBuilder().add(P1).add(P2)
			.getTrackpoints();
	/** Three trackpoints that hover around the start */
	public static final List<Trackpoint> MICRO_POINTS = new SimpleTrack.TrackpointCollectionBuilder().add(P1_MICRO)
			.add(P1_T2).add(P1_X2).getTrackpoints();
	public static final Track MICRO = new SimpleTrack.Builder(MICRO_POINTS).build();
	/**
	 * distance between latitude markers are always equal. Distance between longitude markers change
	 */
	public static final Length S2_LENGTH = S1_LENGTH;
	public static final Length LINE_LENGTH = new QuantityMath<Length>(S1_LENGTH).plus(S2_LENGTH).getQuantity();
	public static final Length LINE_LONGER_LENGTH = new QuantityMath<Length>(S1_LENGTH).times(3).getQuantity();
	/** The time of the first point, but the location is way off */
	public static final Trackpoint DISTANT_LOCATION_T1 = createPoint(T1, LAT_OR_LON_WAY_OFF, LAT_OR_LON_WAY_OFF, E1);
	public static final Trackpoint DISTANT_LOCATION_T2 = createPoint(	T2,
																		LAT_OR_LON_WAY_OFF - LAT_INCREMENT,
																		LAT_OR_LON_WAY_OFF - LAT_INCREMENT,
																		E2);
	public static final Trackpoint DISTANT_LOCATION_T3 = createPoint(	T3,
																		LAT_OR_LON_WAY_OFF - 2 * LAT_INCREMENT,
																		LAT_OR_LON_WAY_OFF - 2 * LAT_INCREMENT,
																		E3);
	/** The location of the first point, but the time is way off. */
	public static final Trackpoint DISTANT_TIME = createPoint(9999999, LAT_1, LONGITUDE);

	public static final List<Trackpoint> LINE_POINTS = new SimpleTrack.TrackpointCollectionBuilder().add(P1).add(P2)
			.add(P3).getTrackpoints();
	/** Same as line, but elevated */
	public static final List<Trackpoint> LINE_ABOVE_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P1_ABOVE).add(P2_ABOVE).add(P3_ABOVE).getTrackpoints();
	/** The reverse location of Line with timestamps that are later than line. */
	public static final List<Trackpoint> RETURN_POINTS = new SimpleTrack.TrackpointCollectionBuilder().add(P3_X2)
			.add(P2_X2).add(P1_X2).getTrackpoints();

	/** Start and finish same as {@link #LINE}, but middle deviates away */
	public static final List<Trackpoint> LINE_DEVIATION_POINTS = new SimpleTrack.TrackpointCollectionBuilder().add(P1)
			.add(P2_OFF_MARK).add(P3).getTrackpoints();

	public static final Track LINE_TIME_SHIFT = new SimpleTrack.TrackpointCollectionBuilder().add(P1_T2).add(P2_T3)
			.add(P3_T4).getTrack();
	/**
	 * useful when handling a line with missing points. This will be one segment in the beginning
	 * and one in the end.
	 */
	public static final List<Trackpoint> LINE_BEFORE_AND_AFTER_MISSING_LOCATION_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P_MINUS_1).add(P1).add(P2_MISSING_LOCATION).add(P3).add(P4).getTrackpoints();

	/**
	 * Two points with no location.
	 * 
	 */
	public static final List<Trackpoint> MISSING_LOCATION_ONLY_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P1_MISSING_LOCATION).add(P2_MISSING_LOCATION).getTrackpoints();
	/**
	 * Starts with {@link #P1_MISSING_LOCATION} and {@link #P2_MISSING_LOCATION} then continues to
	 * P3 and P4.
	 * 
	 */
	public static final List<Trackpoint> MISSING_LOCATION_FIRST_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.addAll(MISSING_LOCATION_ONLY_POINTS).add(P3).add(P4).getTrackpoints();

	/**
	 * The third trackpoint is missing elevation. useful for testing the effects of calculations
	 * like 3D distance and Elevation gain.
	 * 
	 */
	public static final List<Trackpoint> LINE_LONGER_MISSING_ELEVATION_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P1).add(P2).add(P3_MISSING_ELEVATION).add(P4).getTrackpoints();

	/**
	 * A loop that follows line, returns away from line and returns to the start. The shape is a
	 * triangle since there is only one point away from the line during the return.
	 * */
	public static final List<Trackpoint> LOOP_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.addAll(LINE_POINTS).add(LOOP_1_RETURN_MIDDLE).add(LOOP_2_START).getTrackpoints();
	/**
	 * Follows the same path at {@link #LOOP_POINTS}, but with later times.
	 * 
	 */
	public static final List<Trackpoint> LOOP_2_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(LOOP_2_MIDDLE).add(LOOP_2_FINISH_LINE).add(LOOP_2_RETURN_MIDDLE).add(LOOP_3_START).getTrackpoints();

	public static final List<Trackpoint> DOUBLE_LOOP_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.addAll(LOOP_POINTS).addAll(LOOP_2_POINTS).getTrackpoints();

	/**
	 * This is where the LINE is performed and at the end it remains for one time interval and then
	 * returns the same path.
	 */
	public static final List<Trackpoint> LINE_LINGER_AT_END_THEN_RETURN_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.addAll(LINE_POINTS).addAll(RETURN_POINTS).getTrackpoints();
	public static final List<Trackpoint> LINE_BEFORE_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P_MINUS_1).addAll(LINE_POINTS).getTrackpoints();
	public static final List<Trackpoint> LINE_LONGER_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.addAll(LINE_POINTS).add(P4).getTrackpoints();
	public static final List<Trackpoint> LINE_BEFORE_AND_AFTER_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P_MINUS_1).addAll(LINE_LONGER_POINTS).getTrackpoints();
	public static final List<Trackpoint> LINE_SUPER_LONG_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.addAll(LINE_BEFORE_AND_AFTER_POINTS).add(P5).add(P6).add(P7).add(P8).add(P9).getTrackpoints();
	/** @see #LINE_BEFORE_AFTER_SKIP_LINE_ENDPOINTS */
	public static final List<Trackpoint> LINE_BEFORE_AND_AFTER_SKIP_LINE_ENPOINTS_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P_MINUS_1).add(P2).add(P4).getTrackpoints();

	public static final List<Trackpoint> LINE_LONGER_RETURN_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.addAll(LINE_LONGER_POINTS).addAll(RETURN_POINTS).getTrackpoints();

	public static final List<Trackpoint> LINE_START_TWICE_THEN_FINISH_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P1).add(P_MINUS_1_T2).add(P3).add(P4).add(P5).getTrackpoints();

	public static final List<Trackpoint> FIRST_SEGMENT_THEN_DISTANT_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P1).add(P2).add(DISTANT_LOCATION_T3).getTrackpoints();

	/**
	 * Totally avoids {@link #LINE}, but will intersect with those that extend Line.
	 * 
	 */
	public static final List<Trackpoint> DEVIATE_AROUND_LINE = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P_MINUS_1).add(DISTANT_LOCATION_T1).add(DISTANT_LOCATION_T2).add(DISTANT_LOCATION_T3).add(P4)
			.getTrackpoints();
	/** No Relation to {@link #LINE} or most any others besides {@link #DEVIATE_AROUND_LINE}. */
	public static final List<Trackpoint> DISTANT = new SimpleTrack.TrackpointCollectionBuilder()
			.add(DISTANT_LOCATION_T1).add(DISTANT_LOCATION_T2).add(DISTANT_LOCATION_T3).getTrackpoints();

	/**
	 * Runs the normal Line course, but returns past the start to the previous point before the
	 * start
	 * 
	 */
	public static final List<Trackpoint> LINE_RETURN_PAST_START = new SimpleTrack.TrackpointCollectionBuilder().add(P1)
			.add(P2).add(P3).add(P4).add(P3_X2).add(P2_X2).add(P1_X2).add(P_MINUS_1_X2).getTrackpoints();

	public static final List<Trackpoint> LINE_BEFORE_AND_AFTER_ALTERNATE_ROUTE_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P_MINUS_1).add(P1).add(DISTANT_LOCATION_T2).add(DISTANT_LOCATION_T3).add(P4).getTrackpoints();
	public static final List<Trackpoint> LINE_ZERO_DISTANCE_SEGMENT_POINTS = new SimpleTrack.TrackpointCollectionBuilder()
			.add(P1).add(P2).add(P2_X2).add(P1_X2).getTrackpoints();

	public static final Track LINE = new SimpleTrack.Builder(LINE_POINTS).build();
	public static final Track LINE_ABOVE = new SimpleTrack.Builder(LINE_ABOVE_POINTS).build();
	public static final Track LINE_LONGER = new SimpleTrack.Builder(LINE_LONGER_POINTS).build();
	public static final Track LINE_SUPER_LONG = new SimpleTrack.Builder(LINE_SUPER_LONG_POINTS).build();
	public static final Track LINE_LONGER_MISSING_ELEVATION = new SimpleTrack.Builder(
			LINE_LONGER_MISSING_ELEVATION_POINTS).build();
	public static final Track LINE_BEFORE = new SimpleTrack.Builder(LINE_BEFORE_POINTS).build();
	public static final Track LINE_BEFORE_AFTER = new SimpleTrack.Builder(LINE_BEFORE_AND_AFTER_POINTS).build();
	/** @see #LOOP_POINTS */
	public static final Track LOOP = new SimpleTrack.Builder(LOOP_POINTS).build();
	public static final Track DOUBLE_LOOP = new SimpleTrack.Builder(DOUBLE_LOOP_POINTS).build();
	public static final Track LINE_DEVIATION = new SimpleTrack.Builder(LINE_DEVIATION_POINTS).build();

	/**
	 * Starts with a normal trackpoint, but the second segment has zero distance since the points
	 * are the same.
	 */
	public static final Track LINE_ZERO_DISTANCE_SEGMENT = new SimpleTrack.Builder(LINE_ZERO_DISTANCE_SEGMENT_POINTS)
			.build();

	public static final Track LINE_EQUAL_TIMESTAMPS = new SimpleTrack.TrackpointCollectionBuilder().add(P1).add(P2_T1)
			.getTrack();
	public static final Track LINE_PREVIOUS_TIMESTAMPS = new SimpleTrack.TrackpointCollectionBuilder().add(P2).add(P1)
			.getTrack();
	/**
	 * Follows the line, but P1 and P3 of LINE do not have matching points in this line. Useful for
	 * testing interpolation.
	 */
	public static final Track LINE_BEFORE_AFTER_SKIP_LINE_ENDPOINTS = new SimpleTrack.Builder(
			LINE_BEFORE_AND_AFTER_SKIP_LINE_ENPOINTS_POINTS).build();
	public static final Track LINE_LONGER_RETURN = new SimpleTrack.Builder(LINE_LONGER_RETURN_POINTS).build();
	/**
	 * When someone starts at the beginning P-1, proceeds and then returns to the beginning again.
	 * This is to test start detection for timings.
	 */
	public static final Track LINE_START_TWICE_THEN_FINISH = new SimpleTrack.Builder(
			LINE_START_TWICE_THEN_FINISH_POINTS).build();
	public static final Track FIRST_SEGMENT_THEN_DISTANT = new SimpleTrack.Builder(FIRST_SEGMENT_THEN_DISTANT_POINTS)
			.build();
	public static final Track LINE_BEFORE_AND_AFTER_MISSING_LOCATION = new SimpleTrack.Builder(
			LINE_BEFORE_AND_AFTER_MISSING_LOCATION_POINTS).build();
	public static final String ENCODED_LINE = "??_ibE?_ibE?";

	public static final BoundingBox LINE_BOUNDING_BOX = new BoundingBox.Builder().setNorthEast(P3.getLocation())
			.setSouthWest(P1.getLocation()).build();

	/**
	 * Starts and finishes with Line Longer at the same point and time, but goes far away aff route.
	 * 
	 */
	public static final Track LINE_BEFORE_AND_AFTER_ALTERNATE_ROUTE = new SimpleTrack.Builder(
			LINE_BEFORE_AND_AFTER_ALTERNATE_ROUTE_POINTS).build();
	/**
	 * Together with {@link #LINE} at first, then breaks away, then back together again. This
	 * matches the {@link #LINE_LONGER_RETURN}.all except for 2 time segments (of a total of 6)
	 * 
	 */
	public static final Track LINE_TOGETHER_AWAY_BACK_AGAIN = new SimpleTrack.TrackpointCollectionBuilder().add(S1)
			.add(DISTANT_LOCATION_T3).add(P4).add(P3_X2).add(P2_X2).add(P1_X2).getTrack();
	public static final Duration DURATION_TOGETHER_AWAY_BACK_AGAIN_MATCHING_LINE_RETURN = new org.joda.time.Interval(
			T1, T7).toDuration().minus(new Interval(T2, T4).toDuration());
	public static final Ratio RATIO_FOR_TOGETHER_AWAY_BACK_AGAIN_MATCHING_LINE_RETURN = JodaTimeUtil
			.ratioFrom(DURATION_TOGETHER_AWAY_BACK_AGAIN_MATCHING_LINE_RETURN, new Interval(T1, T6).toDuration());

	/**
	 * Specialty point builder for testing tracks. Normal procedure is to call {@link #next()} which
	 * will produce points following a line from {@link ExampleTracks#P_MINUS_1} to
	 * {@link ExampleTracks#P9}.
	 * 
	 * 
	 * 
	 * @author aroller
	 * 
	 */
	public static class PointBuilder
			implements Iterator<Trackpoint> {
		private long t;
		private double lat;
		private double lon;
		private double e;
		private ArrayList<Trackpoint> track = new ArrayList<Trackpoint>();

		/**
		 * 
		 */
		public PointBuilder() {
			t = T_MINUS_1;
			lat = LAT_MINUS_1;
			lon = LONGITUDE;
			e = E_MINUS_1;
		}

		public Trackpoint next() {
			// first point is already loaded. subsequent increment
			if (!track.isEmpty()) {
				standardIncrement();
			}
			return createPoint();
		}

		/**
		 * @return
		 */
		private Trackpoint createPoint() {
			Trackpoint point = TrackUtil.createPoint(t, lat, lon, e);
			track.add(point);
			return point;
		}

		/**
		 * increments time,lat,ele but leaves lon the same
		 * 
		 */
		private void standardIncrement() {
			// prepare for next
			timeIncrement();
			latIncrement();
			// lon stays the same
			elevationIncrement();
		}

		/**
		 * 
		 */
		private void latIncrement() {
			lat += LAT_INCREMENT;
		}

		private void latDecrement() {
			lat -= LAT_INCREMENT;
		}

		/**
		 * 
		 */
		private void elevationIncrement() {
			e += ELEVATION_INCREMENT;
		}

		/**
		 * 
		 */
		private void timeIncrement() {
			t += TIME_INCREMENT;
		}

		private void lonIncrement() {
			lon += LAT_INCREMENT;
		}

		private void lonDecrement() {
			lon -= LAT_INCREMENT;
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return t <= T9;
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			throw new RuntimeException("TODO:Implement this");
		}

		/**
		 * To go away from the standard path by 1 lon with no latitude change.
		 * 
		 * @return
		 */
		public Trackpoint east() {
			lonIncrement();
			timeIncrement();
			elevationIncrement();
			return createPoint();
		}

		public Length segmentLength() {
			if (track.size() > 1) {
				return TrackUtil.distanceBetween(	ListUtilsExtended.secondToLastOrNull(this.track),
													ListUtilsExtended.lastOrNull(this.track));
			} else {
				return null;
			}
		}

		/**
		 * the opposite of {@link #east()}.
		 * 
		 * @return
		 */
		public Trackpoint west() {
			lonDecrement();
			timeIncrement();
			elevationIncrement();
			return createPoint();

		}

		public Trackpoint southWest() {
			lonDecrement();
			latDecrement();
			timeIncrement();
			elevationIncrement();
			return createPoint();
		}

		public Trackpoint southEast() {
			lonIncrement();
			latDecrement();
			timeIncrement();
			elevationIncrement();
			return createPoint();
		}
	}

}
