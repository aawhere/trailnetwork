/**
 * 
 */
package com.aawhere.track;

import static com.aawhere.track.ExampleTracks.*;
import static com.aawhere.track.TrackUtil.*;

import java.util.ArrayList;
import java.util.List;

import javax.measure.quantity.Length;

import com.aawhere.collections.ListUtilsExtended;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Similar to {@link ExampleTracks}, but provides points that are about 10 meters apart and viewable
 * in Google Earth in a place with releative imagery.
 * 
 * This is provided because the big jumps from 0,0 to 1,0 in ExampleTracks is good to make sure our
 * algorithms do not cater to the special situations of GPS, however, without having the smaller
 * realistic tracks we miss some scenarios that can only be introduced from realistic sized tracks
 * when related to processors normal settings. Basically, this is just easier to relate.
 * 
 * <b>Known Issues with ExampleTracks:</b>
 * <ul>
 * <li>Geo Cells calculated by GeoCellProvider give cells for points, not segments so the big jumps
 * in Example tracks produce fewer geocells than is realistic. This hasn't been fixed because in the
 * practical world the 8 resolution geocell is more than enough for quality gps tracks.</li>
 * </ul>
 * 
 * 
 * 
 * @author aroller
 * 
 */
public class ExampleRealisticTracks {

	public static final double LAT_REAL = 45;
	public static final double LAT_REAL_INC = 0.0001;
	public static final double LON_DEFAULT_INC = 0;
	public static final double LON_INC_EAST_WEST = 0.00015;
	/**
	 * approximate segment legnth when using the defaults. This can change as latitude changes, but
	 * you'd have to go far to notice. This value is observed...not calculated.
	 */
	public static final Length SEGMENT_LENGTH = MeasurementUtil.createLengthInMeters(11.5);
	private List<Trackpoint> track;

	/**
	 * Used to construct all instances of ExampleRealisticTracks.
	 */
	public static class Builder
			extends ObjectBuilder<ExampleRealisticTracks> {

		private List<Instruction> instructions = new ArrayList<>();

		public Builder() {
			super(new ExampleRealisticTracks());
			start();
		}

		public Instruction instruct() {
			Instruction instruction = new Instruction();

			instructions.add(instruction);
			return instruction;
		}

		/** An indication that an instruction has already been added encouraging to start here. */
		public Instruction start() {
			return current();
		}

		public Instruction current() {
			Instruction instruction;
			if (instructions.isEmpty()) {
				instruction = instruct();
			} else {
				instruction = Iterables.getLast(instructions);
			}
			return instruction;
		}

		public Builder lonIncrement(double lonIncrement) {
			current().lonIncrement = lonIncrement;
			return this;
		}

		public Builder elevationIncrement(Integer elevationIncrement) {
			current().elevationIncrement = elevationIncrement;
			return this;
		}

		public Builder latitude(Latitude latitude) {
			current().lat(latitude.getInDecimalDegrees());
			return this;
		}

		/**
		 * @param atLeast
		 * @return
		 */
		public Builder atLeast(Length atLeast) {
			current().atLeast(atLeast);
			return this;
		}

		@Override
		public ExampleRealisticTracks build() {
			ExampleRealisticTracks built = super.build();
			SimpleTrack.TrackpointCollectionBuilder builder = new SimpleTrack.TrackpointCollectionBuilder();
			track(builder);

			built.track = builder.getTrackpoints();
			return built;
		}

		/**
		 * @param start
		 * @return
		 */
		private SimpleTrack.TrackpointCollectionBuilder track(SimpleTrack.TrackpointCollectionBuilder builder) {

			Instruction cumulation = null;

			Trackpoint previous;

			for (Instruction instruction : this.instructions) {
				// each instruction provides it's own length the breech so reset
				QuantityMath<Length> math = QuantityMath.create(MeasurementUtil.createLengthInMeters(0));
				if (cumulation == null) {
					cumulation = instruction.copy();
					final Trackpoint start = createPoint(	cumulation.time,
															cumulation.lat,
															cumulation.lon,
															cumulation.ele);
					builder.add(start);
					previous = start;
				} else {
					previous = ListUtilsExtended.lastOrNull(builder.getTrackpoints());
				}

				do {
					final Trackpoint point = createPoint(	cumulation.time += instruction.timeIncrementMillis,
															cumulation.lat += instruction.latIncrement,
															cumulation.lon += instruction.lonIncrement,
															cumulation.ele += instruction.elevationIncrement);
					builder.add(point);
					math = math.plus(TrackUtil.distanceBetween(previous, point));
					previous = point;
				} while (math.lessThan(instruction.atLeast));
			}
			return builder;
		}

		/**
		 * @return
		 */
		public Builder negateLatIncrement() {
			current().negateLatIncrement();
			return this;
		}

		public Builder negateLonIncrement() {
			current().negateLonIncrement();
			return this;
		}

		/**
		 * GOes the length given, then turns around and returns the same distance back.
		 * 
		 * @return
		 */
		public Builder returnTrip() {
			instruct().negateLatIncrement().negateLonIncrement();
			return this;
		}

		public class Instruction
				implements Cloneable {
			private Length atLeast = MeasurementUtil.createLengthInMeters(1000);
			private double latIncrement = LAT_REAL_INC;
			private double lonIncrement = LON_DEFAULT_INC;
			private double lat = LAT_REAL;
			private double lon = LONGITUDE;
			private long time = T1;
			private double ele = E1;
			private int elevationIncrement = 0;
			private int timeIncrementMillis = TIME_INCREMENT;

			/**
			 * @param length
			 *            the length to set
			 */
			public Instruction atLeast(Length length) {
				this.atLeast = length;
				return this;
			}

			/**
			 * @param negateLatIncrement
			 *            the negateLatIncrement to set
			 */
			public Instruction negateLatIncrement() {
				this.latIncrement *= -1;
				return this;
			}

			/**
			 * @param negateLonIncrement
			 *            the negateLonIncrement to set
			 */
			public Instruction negateLonIncrement() {
				this.lonIncrement *= -1;
				return this;
			}

			/**
			 * @param latIncrement
			 *            the latIncrement to set
			 */
			public Instruction latIncrement(double latIncrement) {
				this.latIncrement = latIncrement;
				return this;
			}

			/**
			 * @param lonIncrement
			 *            the lonIncrement to set
			 */
			public Instruction lonIncrement(double lonIncrement) {
				this.lonIncrement = lonIncrement;
				return this;
			}

			/**
			 * @param lat
			 *            the lat to set
			 */
			public Instruction lat(double lat) {
				this.lat = lat;
				return this;
			}

			/**
			 * @param lon
			 *            the lon to set
			 */
			public Instruction lon(double lon) {
				this.lon = lon;
				return this;
			}

			/**
			 * @param time
			 *            the time to set
			 */
			public Instruction time(long time) {
				this.time = time;
				return this;
			}

			/**
			 * @param ele
			 *            the ele to set
			 */
			public Instruction ele(double ele) {
				this.ele = ele;
				return this;
			}

			/**
			 * @param elevationIncrement
			 *            the elevationIncrement to set
			 */
			public Instruction elevationIncrement(int elevationIncrement) {
				this.elevationIncrement = elevationIncrement;
				return this;
			}

			public Instruction timeIncrementMillis(int timeIncrement) {
				this.timeIncrementMillis = timeIncrement;
				return this;
			}

			/**
			 * Clones the intruction without adding to the list of instructions.
			 * 
			 * @see #repeat()
			 * @return
			 */
			public Instruction copy() {
				try {
					final Instruction instruction = (Instruction) clone();

					return instruction;
				} catch (CloneNotSupportedException e) {
					throw new RuntimeException(e);
				}
			}

			/**
			 * Creates a clean new instruction without inheriting anything that was previously done.
			 */
			public Instruction instruct() {
				return Builder.this.instruct();
			}

			/**
			 * Full revesal of progress on the current instruction... calls
			 * {@link #negateLatIncrement()} and {@link #negateLonIncrement()}.
			 * 
			 * @return
			 */
			public Instruction reverse() {
				negateLatIncrement();
				negateLonIncrement();
				return this;
			}

			/** forces travel of default distance due north. */
			public Instruction north() {
				latIncrement = LAT_REAL_INC;
				lonIncrement = 0;
				return this;
			}

			/** the opposite of {@link #north()} */
			public Instruction south() {
				north();
				negateLatIncrement();
				return this;
			}

			/** makes the current segment go east using standard increments() */
			public Instruction east() {
				latIncrement = 0;
				lonIncrement = LON_INC_EAST_WEST;
				return this;
			}

			/** the opposite of {@link #east()} */
			public Instruction west() {
				east();
				negateLonIncrement();
				return this;
			}

			/**
			 * Shorthand for going far enough to create at least the number of segments desired.
			 * Could be one more than requested. Overwrites {@link #atLeast(Length)}.
			 * 
			 * @see #atLeast(Length)
			 */
			public Instruction segments(int numberOfSegments) {
				atLeast(QuantityMath.create(SEGMENT_LENGTH).times(numberOfSegments).getQuantity());
				return this;
			}

			/**
			 * Repeats all instructions recorded so far...including this one. Useful for doing
			 * multiple loops and similar.
			 * 
			 * @param i
			 * @return
			 */
			public Instruction repeatAll(Integer times) {
				ArrayList<Instruction> repeats = Lists.newArrayList();
				for (Instruction instruction : instructions) {
					// call copy to avoid conccurrent modification
					repeats.add(instruction.copy());
				}
				instructions.addAll(repeats);
				return Iterables.getLast(instructions);
			}

			/**
			 * Calls {@link #copy()} and adds it to the instructions inheriting all of the
			 * properties of the current instruction.
			 * 
			 * @return
			 */
			public Instruction repeat() {
				Instruction instruction = copy();
				instructions.add(instruction);
				return instruction;
			}

			/**
			 * @return
			 */
			public List<Trackpoint> track() {
				return build().track();
			}

		}

	}// end Builder

	/**
	 * Provides the Builder with the default current instruction for you to modify, accept or
	 * continue adding more instructions.
	 * 
	 * @return
	 */
	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ExampleRealisticTracks */
	private ExampleRealisticTracks() {
	}

	/**
	 * @return the track
	 */
	public List<Trackpoint> track() {
		return this.track;
	}

}
