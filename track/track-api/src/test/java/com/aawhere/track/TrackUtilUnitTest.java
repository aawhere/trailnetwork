/**
 *
 */
package com.aawhere.track;

import static com.aawhere.measure.MeasurementUtil.*;
import static com.aawhere.measure.calc.GeoMathTestUtil.*;
import static com.aawhere.track.ExampleTracks.*;
import static com.aawhere.track.TrackTestUtil.*;
import static com.aawhere.track.TrackUtil.*;
import static com.aawhere.track.TrackpointTestUtil.*;
import static org.junit.Assert.*;

import javax.measure.quantity.Length;
import javax.measure.quantity.Velocity;
import javax.measure.unit.MetricSystem;

import org.junit.Test;

import com.aawhere.joda.time.InvalidIntervalRuntimeException;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.PythagoreanTheorum;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.test.TestUtil;

/**
 * Tests the simple static class {@link TrackUtil}.
 * 
 * #see {@link TrackpointUnitTest#assertEquality(Trackpoint)} for testing
 * {@link TrackUtil#equals(Trackpoint, Trackpoint)} and {@link TrackUtil#hashCode(Trackpoint)}
 * 
 * @author roller
 * 
 */
public class TrackUtilUnitTest {

	@Test
	public void testTrackpointInterpolation() {

		TrackSegment segment = new SimpleTrackSegment(P1, P3);
		Ratio ratio = MeasurementQuantityFactory.getInstance(Ratio.class).create(.5, ExtraUnits.RATIO);
		Trackpoint interpolated = interpolate(segment, ratio);
		TrackUnitTest.assertEquals(P2, interpolated);
	}

	@Test
	public void testFirstWithLocation() {
		assertEquals("regular line first piont has location", P1, firstTrackpointWithLocation(LINE));
		assertEquals("first point with location is 3", P3, firstTrackpointWithLocation(MISSING_LOCATION_FIRST_POINTS));
		assertNull("no points have location", firstTrackpointWithLocation(MISSING_LOCATION_ONLY_POINTS));
	}

	@Test(expected = InvalidIntervalRuntimeException.class)
	public void testTrackpointInterpolationSamePoint() {
		TrackSegment segment = new SimpleTrackSegment(P1, P1);
		Ratio ratio = MeasurementQuantityFactory.getInstance(Ratio.class).create(.5, ExtraUnits.RATIO);
		Trackpoint interpolated = interpolate(segment, ratio);
		TrackUnitTest.assertEquals(P1, interpolated);
	}

	@Test(expected = InvalidIntervalRuntimeException.class)
	public void testTrackpointInterpolationByTimestampSamePoint() {
		Trackpoint expected = P1;
		TrackSegment segment = new SimpleTrackSegment(expected, expected);
		Trackpoint interpolated = interpolate(segment, expected.getTimestamp());
		TrackUnitTest.assertEquals(expected, interpolated);
	}

	@Test
	public void testTrackpointInterpolationByTimestampMidpoint() {
		TrackSegment segment = new SimpleTrackSegment(P1, P3);
		Trackpoint expected = P2;
		Trackpoint interpolated = interpolate(segment, expected.getTimestamp());
		TrackUnitTest.assertEquals(expected, interpolated);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInterpolationByondSegment() {
		TrackSegment segment = ExampleTracks.S1;
		interpolate(segment, P3.getTimestamp());
	}

	/**
	 * exageration of realistic situations to make sure trackpoints being calculated are taking into
	 * consideration elevation changes in their length calculations.
	 * 
	 * terminology related to: http://en.wikipedia.org/wiki/Right_triangle
	 * 
	 * <pre>
	 * 
	 *         B2
	 *         /|
	 *    c2  / |
	 *       /  |
	 *    B /   |
	 *     /|   |a2
	 * c  / |a  |
	 *   /  |   |
	 * A/___|___|C2
	 *    b C
	 *      b2
	 * </pre>
	 */
	@Test
	public void testInterpolationWithElevation() {
		Length bLength = MeasurementUtil.createLengthInMeters(100);
		Elevation aLength = MeasurementUtil.createElevation(bLength);

		final GeoCoordinate aLocation = EARTH_ORIGIN;
		final GeoCoordinate cLocation = GeoMath.coordinateFrom(aLocation, EAST, bLength);
		SimpleTrackpoint A = SimpleTrackpoint.create().setTimestamp(P1.getTimestamp()).setLocation(aLocation)
				.setElevation(ZERO_ELEVATION).build();
		SimpleTrackpoint B = SimpleTrackpoint.create().setTimestamp(P2.getTimestamp()).setLocation(cLocation)
				.setElevation(aLength).build();
		SimpleTrackpoint C = SimpleTrackpoint.create().setTimestamp(P2.getTimestamp()).setLocation(cLocation)
				.setElevation(ZERO_ELEVATION).build();

		// now, when interpolating we request the hypotenuse length c
		SimpleTrackSegment c = new SimpleTrackSegment(A, B);
		SimpleTrackSegment b = new SimpleTrackSegment(A, C);

		Length cLength = TrackUtil.distanceBetween(A, B);
		Length expectedCLength = PythagoreanTheorum.create().setA(createLength(aLength)).setB(bLength).build().getC();
		MeasureTestUtil.assertEquals("c didn't equate", expectedCLength, cLength);

		// setup double length to test interpolating back to single length
		Elevation a2Length = QuantityMath.create(aLength).times(2).getQuantity();
		Length b2Length = QuantityMath.create(bLength).times(2).getQuantity();
		Length c2Length = QuantityMath.create(cLength).times(2).getQuantity();
		GeoCoordinate C2location = GeoMath.coordinateFrom(aLocation, EAST, b2Length);
		SimpleTrackpoint C2 = SimpleTrackpoint.create().setElevation(ZERO_ELEVATION).setLocation(C2location)
				.setTimestamp(P3.getTimestamp()).build();
		SimpleTrackSegment b2 = new SimpleTrackSegment(A, C2);
		final Ratio half = ratio(0.5);
		Trackpoint CCalculated = TrackUtil.interpolate(b2, half);
		TrackpointTestUtil.assertTrackpointEquals("interpolation along base is wrong", C, CCalculated);

		Trackpoint B2 = SimpleTrackpoint.create().setElevation(a2Length).setLocation(C2location)
				.setTimestamp(P3.getTimestamp()).build();

		MeasureTestUtil.assertEquals("base distance is wrong", b2Length, TrackUtil.distanceBetween(A, C2));
		SimpleTrackSegment c2 = new SimpleTrackSegment(A, B2);
		assertEquals("hypotenuse didn't go double", c2.getDistance(), c2Length);
		TrackpointTestUtil.assertTrackpointEquals(	"interpoltion along hypotenuse is wrong",
													B,
													TrackUtil.interpolate(c2, half));

	}

	/**
	 * tests {@link TrackUtil#extrapolateBeyond(TrackSegment, Length)} and
	 * {@link TrackUtil#extrapolate(TrackSegment, Ratio)} transitively
	 */
	@Test
	public void testExtrapolateBeyond() {
		TrackSegment segment = S1;
		Trackpoint actual = extrapolateBeyond(segment, S2_LENGTH);
		TrackUnitTest.assertEquals(P3, actual);
	}

	@Test
	public void testExtrapolateBeyondSegmentZeroLength() {
		// produces the same point, but different times...zero length
		TrackSegment segment = new SimpleTrackSegment(P1, P1_X2);
		TestUtil.assertDoubleEquals(0, segment.getDistance().doubleValue(MetricSystem.METRE));
		Trackpoint actual = extrapolateBeyond(segment, S2_LENGTH);

	}

	/**
	 * Using GeoMath validations this ensures the location of the interpolated trackpoint is being
	 * properly calculated
	 */
	@Test
	public void testGeoInterpolation() {

		final Length fullLength = createLengthInMeters(METERS_FROM_ORIGIN_TO_NEAR);
		MeasureTestUtil.assertEquals(	"the segment distance isn't correct",
										fullLength,
										EQUATOR_ORIGIN_TO_NEAR.getDistance());
		// a simple test, but this was failing when lat/long were interpolated as quantities instead
		// of GeoMath.
		Trackpoint closeEnough = TrackUtil.interpolate(EQUATOR_ORIGIN_TO_NEAR, ratio(0.999));
		MeasureTestUtil.assertEquals("the segment distance isn't correct", new SimpleTrackSegment(
				EQUATOR_ORIGIN_TRACKPOINT, closeEnough).getDistance(), fullLength);
	}

	@Test
	public void testExtrapolateBefore() {
		Trackpoint actual = extrapolateBefore(S2, S1_LENGTH);
		TrackpointTestUtil.assertTrackpointEquals(P1, actual);
	}

	/**
	 * @see TrackUtil#lengthen(TrackSegment, Length)
	 * 
	 */
	@Test
	public void testLengthen() {
		TrackSegment actual = TrackUtil.lengthen(S2, S1_LENGTH);
		Length expectedLength = QuantityMath.create(S1_LENGTH).times(3).getQuantity();
		MeasureTestUtil.assertEquals(expectedLength, actual.getDistance());
		TrackpointTestUtil.assertTrackpointEquals("first", P1, actual.getFirst());
		TrackpointTestUtil.assertTrackpointEquals("second", P4, actual.getSecond());
	}

	@Test
	public void testTrackpointInterpolationByLocationMidpoint() {
		TrackSegment segment = new SimpleTrackSegment(P1, P3);
		Trackpoint expected = P2;
		Trackpoint interpolated = interpolate(segment, expected.getLocation());
		TrackUnitTest.assertEquals(expected, interpolated);
	}

	@Test
	public void testTrackpointValuesEqual() {
		Trackpoint t1 = generateRandomTrackpoint();
		assertTrue("a trackpiont should equal itself", valuesEqual(t1, SimpleTrackpoint.copy(t1).build()));
		Trackpoint t2 = generateRandomTrackpoint();
		assertFalse("two randoms can't be the same", valuesEqual(t1, t2));
		Trackpoint t3 = createPoint(t1.getTimestamp());
		assertFalse("nulls are having problems", valuesEqual(t1, t3));
		assertFalse("nulls are having problems", valuesEqual(t3, t1));

	}

	@Test
	public void testTrackSegmentValuesEqual() {
		TrackSegment s1 = ExampleTracks.S1;
		TrackSegment s1Clone = new SimpleTrackSegment(s1.getFirst(), s1.getSecond());
		assertTrue("a TrackSegment should equal itself", valuesEqual(s1, s1Clone));
		TrackSegment s2 = ExampleTracks.S2;
		assertFalse("two randoms can't be the same", valuesEqual(s1, s2));
		TrackSegment s3 = new SimpleTrackSegment(createPoint(s1.getFirst().getTimestamp()), createPoint(s1.getSecond()
				.getTimestamp()));
		assertFalse("nulls are having problems", valuesEqual(s1, s3));
		assertFalse("nulls are having problems", valuesEqual(s3, s1));

	}

	@Test
	public void testAverageDistanceApartForSegments() {
		// a little unintuitive, but testing these two segments creates a
		// situation where one is ahead of the other by 1 segment.
		Length expectedDistance = new QuantityMath<Length>(S1.getDistance()).average(S2.getDistance()).getQuantity();
		// S2.second to S2.second is the same length as S2 because S1.second and
		// S2.first are the same.
		Length actualDistance = averageDistanceApart(S1, S2);
		assertEquals(expectedDistance, actualDistance);
	}

	@Test
	public void testTimesSynched() {
		assertTrue(timesAreSynched(S1, S1));
		assertFalse(timesAreSynched(S1, S2));
		assertFalse(timesAreSynched(S2, S1));
		assertFalse(timesAreSynched(S3, S2));
		assertFalse(timesAreSynched(S2, S3));

	}

	@Test
	public void testVerticalVelocity() {

		Velocity expected = MeasurementQuantityFactory.getInstance(Velocity.class)
				.create(10, MetricSystem.METRES_PER_SECOND);
		Velocity actual = TrackUtil.verticalVelocity(S1);
		assertEquals(expected, actual);
	}

	@Test
	public void testVelocity() {
		TrackSegment segment = S1;
		Length distance = new PythagoreanTheorum.Builder().setB(segment.getDistance())
				.setA(segment.getElevationChange()).build().getC();
		Velocity expected = MeasurementQuantityFactory.getInstance(Velocity.class)
				.create(distance.doubleValue(MetricSystem.METRE), MetricSystem.METRES_PER_SECOND);
		Velocity actual = TrackUtil.velocity(segment);
		assertEquals(expected.getValue().doubleValue(), actual.getValue().doubleValue(), 0.001);
	}

	@Test
	public void testElevationChange() {

		Elevation expected = new QuantityMath<Elevation>(P2.getElevation()).minus(P1.getElevation()).getQuantity();

		testElevationChange(P1, P2, expected);
	}

	@Test
	public void testElevationChangeNegitive() {
		Elevation expected = new QuantityMath<Elevation>(P1.getElevation()).minus(P2.getElevation()).getQuantity();
		testElevationChange(P2, P1, expected);
	}

	@Test
	public void testElevationChangeZero() {
		Elevation expected = new QuantityMath<Elevation>(P1.getElevation()).minus(P1.getElevation()).getQuantity();
		testElevationChange(P1, P1, expected);
	}

	@Test
	public void testElevationGain() {
		Elevation expected = new QuantityMath<Elevation>(P2.getElevation()).minus(P1.getElevation()).getQuantity();
		testElevationGain(P1, P2, expected);
	}

	@Test
	public void testElevationGainCantBeNegative() {
		Elevation expected = MeasurementUtil.createElevationInMeters(0);
		testElevationGain(P2, P1, expected);
	}

	@Test
	public void testElevationGainZero() {
		Elevation expected = MeasurementUtil.createElevationInMeters(0);
		testElevationGain(P1, P1, expected);
	}

	private void testElevationChange(Point3d origin, Point3d dest, Elevation expected) {
		ElevationChange actual = TrackUtil.elevationChange(origin, dest);
		assertEquals(expected, actual);
	}

	private void testElevationGain(Point3d origin, Point3d dest, Elevation expected) {
		ElevationChange actual = TrackUtil.elevationGain(origin, dest);
		assertEquals(expected, actual);
	}

	@Test
	public void testTrackEquality() {
		SimpleTrack track = TrackTestUtil.generateRandomTrack();
		TrackTestUtil.assertTrackEquals(track, track);
	}

}
