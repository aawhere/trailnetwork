/**
 * 
 */
package com.aawhere.track;

import static com.aawhere.track.TrackpointTestUtil.*;
import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import org.joda.time.Duration;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.calc.GeoMathTestUtil;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.Resolution;

import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class TrackTestUtil {

	public static Trackpoint EQUATOR_ORIGIN_TRACKPOINT = createTrackpoint(0, GeoMathTestUtil.EQUATOR_ORIGIN);
	public static Trackpoint EQUATOR_ORIGIN_NEAR_TRACKPOINT = createTrackpoint(10000, GeoMathTestUtil.EQUATOR_NEAR);
	public static TrackSegment EQUATOR_ORIGIN_TO_NEAR = new SimpleTrackSegment(EQUATOR_ORIGIN_TRACKPOINT,
			EQUATOR_ORIGIN_NEAR_TRACKPOINT);

	/**
	 * Depply asserts each trackpoint is equal according to the
	 * {@link TrackpointTestUtil#assertTrackpointEquals(Trackpoint, Trackpoint)}
	 * 
	 * @param expected
	 * @param actual
	 */
	public static void assertTrackEquals(Track expected, Track actual) {

		Iterator<Trackpoint> actualIterator = actual.iterator();
		int count = 0;
		for (Trackpoint expectedTrackpoint : expected) {
			assertTrue("actual track does not have enough trackpoints " + count, actualIterator.hasNext());
			TrackpointTestUtil
					.assertTrackpointEquals("trackpoint #" + count, expectedTrackpoint, actualIterator.next());
			count++;
		}
	}

	/**
	 * @param earthOrigin
	 * @return
	 */
	private static Trackpoint createTrackpoint(long millis, GeoCoordinate coordinate) {
		return TrackUtil.createPoint(millis, coordinate.getLatitude().getInDecimalDegrees(), coordinate.getLongitude()
				.getInDecimalDegrees());
	}

	/**
	 * @return
	 * 
	 */
	public static SimpleTrack generateRandomTrack() {
		List<Trackpoint> trackpoints = generateRandomTrackpoints();
		return new SimpleTrack.Builder(trackpoints).build();
	}

	public static GeoCells geoCells(Track track) {
		Resolution resolution = TrackSummary.MAX_BOUNDS_RESOLUTION;
		GeoCells.Builder builder = GeoCells.create().setMaxResolution(resolution);
		for (Trackpoint trackpoint : track) {
			builder.addAll(GeoCellUtil.geoCells(trackpoint.getLocation(), resolution).getAll());
		}

		return builder.build();
	}

	/**
	 * Returns a copy of the track with adjusted times given the duration to add to each time.
	 * 
	 * @param track
	 *            the original trackpoints to copy and return with modified times.
	 * @param additive
	 *            the amount to add to each timestamp
	 * @return
	 */
	public static List<Trackpoint> timesAdjusted(Iterable<Trackpoint> track, Duration additive) {
		List<Trackpoint> results = Lists.newArrayList();
		for (Trackpoint trackpoint : track) {
			results.add(SimpleTrackpoint.copy(trackpoint).setTimestamp(trackpoint.getTimestamp().plus(additive))
					.build());
		}

		return results;
	}

}
