/**
 * 
 */
package com.aawhere.track;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class TrackSummaryUtilUnitTest {

	@Test
	public void testSameSummary() {
		TrackSummary sampleTrackSummary = TrackSummaryTestUtil.getSampleTrackSummary();
		assertTrue("same must equal", TrackSummaryUtil.isLikelyFromTheSameTrack(sampleTrackSummary, sampleTrackSummary));
		List<Trackpoint> orderedTrackpoints = TrackpointTestUtil.generateOrderedTrackpoints();
		TrackSummary copy = TrackSummary.create()
				.populatePoints(sampleTrackSummary.getStart(), sampleTrackSummary.getFinish()).build();

		assertTrue("copy should still match", TrackSummaryUtil.isLikelyFromTheSameTrack(sampleTrackSummary, copy));

		TrackSummary different = TrackSummary.create()
				.populatePoints(orderedTrackpoints.get(0), orderedTrackpoints.get(1)).build();
		assertFalse("not the same", TrackSummaryUtil.isLikelyFromTheSameTrack(sampleTrackSummary, different));
	}

}
