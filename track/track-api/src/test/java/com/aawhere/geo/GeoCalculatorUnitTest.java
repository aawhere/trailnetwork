/**
 * 
 */
package com.aawhere.geo;

import static com.aawhere.measure.calc.GeoMathTestUtil.*;
import static com.aawhere.track.TrackUtil.createPoint;
import static org.junit.Assert.assertEquals;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import org.junit.Test;

import com.aawhere.measure.calc.GeoMathTestUtil;
import com.aawhere.measure.calc.GeoMathUnitTest;
import com.aawhere.track.ExampleTracks;
import com.aawhere.track.GeoCalculator;
import com.aawhere.track.Point;

/**
 * Tests {@link GeoCalculator}.
 * 
 * @see GeoMathTestUtil
 * @see GeoMathUnitTest
 * 
 * @author roller
 * 
 */
public class GeoCalculatorUnitTest {

	public static final double LOW = 0.0;
	public static final double HIGH = 1000.0;

	public static Point EQUATOR_ORIGIN = createPoint(	ORIGIN_LATITUDE_IN_DECIMAL_DEGREES,
														ORIGIN_LONGITUDE_IN_DECIMAL_DEGREES);
	public static Point EQUATOR_ORIGIN_HIGH = createPoint(	ORIGIN_LATITUDE_IN_DECIMAL_DEGREES,
															ORIGIN_LONGITUDE_IN_DECIMAL_DEGREES,
															HIGH);
	public static Point EQUATOR_ORIGIN_LOW = createPoint(	ORIGIN_LATITUDE_IN_DECIMAL_DEGREES,
															ORIGIN_LONGITUDE_IN_DECIMAL_DEGREES,
															LOW);

	@Test
	public void testOriginSamePoint() {
		assertDistance(METERS_FROM_SAME_POINT, ExampleTracks.P1, ExampleTracks.P1, NO_TOLERANCE);
	}

	@Test
	public void testStraightUpVertical() {

		assertDistance(HIGH - LOW, EQUATOR_ORIGIN_HIGH, EQUATOR_ORIGIN_LOW, NO_TOLERANCE);
		assertDistance(HIGH - LOW, EQUATOR_ORIGIN_LOW, EQUATOR_ORIGIN_HIGH, NO_TOLERANCE);
	}

	@Test
	public void testStraightUpVertical2DimensionOnly() {
		GeoCalculator calculator = new GeoCalculator.Builder().twoDimensionCalculationsOnly().build();
		assertDistance(METERS_FROM_SAME_POINT, EQUATOR_ORIGIN_HIGH, EQUATOR_ORIGIN_LOW, NO_TOLERANCE, calculator);
		assertDistance(METERS_FROM_SAME_POINT, EQUATOR_ORIGIN_LOW, EQUATOR_ORIGIN_HIGH, NO_TOLERANCE, calculator);
	}

	/**
	 * Helper method for others to use for testing of the distance.
	 * 
	 * @param expectedMeters
	 * @param origin
	 * @param destination
	 * @param tolerance
	 */
	private void assertDistance(final double expectedMeters, final Point origin, final Point destination,
			final double tolerance) {
		GeoCalculator calculator = new GeoCalculator.Builder().build();
		assertDistance(expectedMeters, origin, destination, tolerance, calculator);
	}

	/**
	 * @param expectedMeters
	 * @param origin
	 * @param destination
	 * @param tolerance
	 * @param calculator
	 */
	private void assertDistance(final double expectedMeters, final Point origin, final Point destination,
			final double tolerance, GeoCalculator calculator) {
		Length distance = calculator.distanceBetween(origin, destination);
		assertEquals(MetricSystem.METRE, distance.getUnit());
		assertEquals(expectedMeters, distance.doubleValue(MetricSystem.METRE), tolerance);
	}
}
