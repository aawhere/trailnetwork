/**
 * 
 */
package com.aawhere.track;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Converts any trackpoint hidden by the interface to a SimpleTrackpoint that is XML Capable.
 * 
 * If it is already a {@link SimpleTrackpoint} then it just casts and passes on.
 * 
 * @author aroller
 * 
 */
public class TrackpointXmlAdapter
		extends XmlAdapter<SimpleTrackpoint, Trackpoint> {

	/**
	 * 
	 */
	public TrackpointXmlAdapter() {
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public SimpleTrackpoint marshal(Trackpoint trackpoint) throws Exception {
		if (trackpoint == null) {
			return null;
		}
		if (trackpoint instanceof SimpleTrackpoint) {
			return (SimpleTrackpoint) trackpoint;
		} else {
			return SimpleTrackpoint.copy(trackpoint).build();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Trackpoint unmarshal(SimpleTrackpoint trackpoint) throws Exception {
		return trackpoint;
	}

}
