/**
 * 
 */
package com.aawhere.track;

import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.geocell.GeoCells;

import com.google.common.base.Function;

/**
 * Utilities to help with common tasks related to {@link TrackSummary}.
 * 
 * @see TrackUtil
 * 
 * @author aroller
 * 
 */
public class TrackSummaryUtil {

	/**
	 * Provides a thorough enough check to determine if the two tracks were identical by inspecting
	 * a few attributes to see if they are close enough...specifically start and finish.
	 * 
	 * This isn't called equals since it isn't thorough enough.
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static Boolean isLikelyFromTheSameTrack(TrackSummary first, TrackSummary second) {
		return TrackUtil.equalsDeeply(first.getStart(), second.getStart())
				&& TrackUtil.equalsDeeply(first.getFinish(), second.getFinish());

	}

	public static Function<TrackSummary, BoundingBox> boundingBoxFunction() {
		return new Function<TrackSummary, BoundingBox>() {

			@Override
			public BoundingBox apply(TrackSummary input) {
				return (input == null) ? null : input.getBoundingBox();
			}
		};
	}

	public static Function<TrackSummary, ArchivedDocumentId> archivedDocumentIdFunction() {
		return new Function<TrackSummary, ArchivedDocumentId>() {

			@Override
			public ArchivedDocumentId apply(TrackSummary input) {
				return (input == null) ? null : input.getDocumentId();
			}
		};
	}

	/**
	 * @return
	 */
	public static Function<TrackSummary, GeoCells> spatialBufferFunction() {
		return new Function<TrackSummary, GeoCells>() {

			@Override
			public GeoCells apply(TrackSummary input) {
				return (input != null) ? input.getSpatialBuffer() : null;
			}
		};
	}
}
