/**
 * 
 */
package com.aawhere.track;

import java.util.Set;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.doc.archive.ArchivedDocumentId;

import com.google.common.collect.Sets;

/**
 * Any implementation of {@link XmlAdapter} that will provide nothing, but allows the factory to
 * replace it with a specific format provided as an option.
 * 
 * @see
 * 
 * @author aroller
 * 
 */
public class TrackFormatXmlAdapter<F>
		extends XmlAdapter<F, ArchivedDocumentId> {

	public Set<MediaType> acceptsMediaType() {
		return Sets.newHashSet(MediaType.WILDCARD_TYPE);
	}

	/**
	 * Unmarshalling is not a requirement and in most cases no possible since the document id is not
	 * encoded int the path.
	 * 
	 * @param v
	 * @return
	 * @throws Exception
	 */
	@Override
	public ArchivedDocumentId unmarshal(F v) throws Exception {
		return null;

	}

	/**
	 * The method to override when wishing to provide a better format than nothing which is what
	 * this will provide.
	 */
	@Override
	public F marshal(ArchivedDocumentId id) throws Exception {
		return null;
	}
}
