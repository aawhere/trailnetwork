/**
 *
 */
package com.aawhere.track;

import javax.measure.quantity.Length;
import javax.measure.quantity.Velocity;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.joda.time.DurationUtil;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.ElevationChange;

/**
 * @see TrackSegment
 * @author roller
 * 
 */
public class SimpleTrackSegment
		implements TrackSegment {

	private static final long serialVersionUID = 5440885401282598872L;

	private Trackpoint first;
	private Trackpoint second;
	// cached distance
	private Length distanceBetween = null;
	// cached interval
	private Interval interval = null;

	/**
	 * Constructor provided because no builder is needed for an inflexible class that requires only
	 * two fields.
	 */
	public SimpleTrackSegment(Trackpoint first, Trackpoint second) {
		this.first = first;
		this.second = second;
	}

	@Override
	public Interval getInterval() {
		// this delayed calculation allows invalid trackpoints to be built and
		// filtered before failing.
		if (interval == null) {
			DateTime firstTime = first.getTimestamp();
			DateTime secondTime = second.getTimestamp();
			// This will throw an InvalidIntervalRuntimeException if first is after second.
			interval = DurationUtil.interval(firstTime, secondTime);
		}
		return interval;
	}

	@Override
	public Duration getDuration() {
		return getInterval().toDuration();
	}

	@Override
	public Length getDistance() {
		if (distanceBetween == null) {

			if (first.isLocationCapable() && second.isLocationCapable()) {
				distanceBetween = new GeoCalculator.Builder().build().distanceBetween(first, second);
			} else {
				distanceBetween = null;
			}
		}
		return distanceBetween;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.TrackSegment#getFirst()
	 */
	@Override
	public Trackpoint getFirst() {
		return first;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.TrackSegment#getSecond()
	 */
	@Override
	public Trackpoint getSecond() {
		return second;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.TrackSegment#getAverageElevation()
	 */
	@Override
	public Elevation getAverageElevation() {
		return TrackUtil.averageElevationFrom(this);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return TrackUtil.toString(this);
	}

	@Override
	public ElevationChange getElevationChange() {
		return TrackUtil.elevationChange(first, second);
	}

	@Override
	public Velocity getVerticalVelocity() {
		return TrackUtil.verticalVelocity(this);
	}

	@Override
	public Velocity getVelocity() {
		return TrackUtil.velocity(this);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.first == null) ? 0 : this.first.hashCode());
		result = prime * result + ((this.second == null) ? 0 : this.second.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SimpleTrackSegment)) {
			return false;
		}
		SimpleTrackSegment other = (SimpleTrackSegment) obj;
		if (this.first == null) {
			if (other.first != null) {
				return false;
			}
		} else if (!this.first.equals(other.first)) {
			return false;
		}
		if (this.second == null) {
			if (other.second != null) {
				return false;
			}
		} else if (!this.second.equals(other.second)) {
			return false;
		}
		return true;
	}

}
