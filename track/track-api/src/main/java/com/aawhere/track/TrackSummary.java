/**
 *
 */
package com.aawhere.track;

import java.io.Serializable;

import javax.annotation.Nullable;
import javax.measure.quantity.Length;
import javax.measure.quantity.Velocity;
import javax.measure.unit.MetricSystem;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.EmbeddedDictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.joda.time.xml.SimpleDateTimeXmlAdapter;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxProvider;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsOptionalXmlAdapter;
import com.aawhere.measure.geocell.GeoCellsReducer;
import com.aawhere.measure.geocell.Resolution;
import com.aawhere.persist.BaseEntity;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Predicate;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Index;

/**
 * The useful meta-data describing the details of the track without having the track {@link Track}.
 * 
 * Although many of the attributes can be calculated from the Trackpoints {@link #start} and
 * {@link #finish}, we leave this class to be a simple getter/setter for flexibility to allow for
 * partial filling and/or custom information which could allow for discrepancies. It is left up to
 * the builder to ensure accuracy and it is encouraged to use the
 * {@link Builder#populatePoints(Trackpoint, Trackpoint)} method to maintain a consistent level of
 * accuracy.
 * 
 * All attributes may be null which indicates it was not populated, not necessarily that it couldn't
 * have been populated with the other attributes.
 * 
 * @see package-info
 * 
 * @author roller
 * 
 */
@Cache
@XmlRootElement(name = TrackSummary.FIELD.DOMAIN)
@XmlType(name = TrackSummary.FIELD.DOMAIN, namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = TrackSummary.FIELD.DOMAIN, messages = TrackSummaryMessage.class)
public class TrackSummary
		implements Serializable, BoundingBoxProvider {

	private static final long serialVersionUID = -9071445404786111936L;
	public static final Resolution MAX_START_AREA_RESOLUTION = Resolution.ELEVEN;

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(TrackSummary ts) {
		return new Builder(ts);
	}

	/**
	 * Use @{link Builder} to create instances.
	 */
	private TrackSummary() {
	}

	/**
	 * This results in a bounding box about the size of a block. Tracks should be bigger than this.
	 * 
	 */
	public static final Resolution MAX_BOUNDS_RESOLUTION = Resolution.EIGHT;
	public static final Integer MAX_GEO_CELLS_COUNT = 300;

	/**
	 */
	@XmlElement(name = FIELD.ELAPSED_TIME)
	@com.aawhere.field.annotation.Field(key = FIELD.ELAPSED_TIME, dataType = FieldDataType.NUMBER)
	private Duration elapsedTime;

	@XmlElement
	@Field(key = FIELD.START)
	@EmbeddedDictionary
	private SimpleTrackpoint start;

	/** The start area of the course. TN-916 */
	@Field(key = FIELD.START_AREA, indexedFor = "TN-916")
	@XmlElement(name = FIELD.START_AREA)
	@Nullable
	@XmlJavaTypeAdapter(GeoCellsOptionalXmlAdapter.class)
	private GeoCells startArea;

	@XmlElement
	@Field
	@EmbeddedDictionary
	private SimpleTrackpoint finish;

	@XmlAttribute
	private Integer numberOfTrackpoints;

	@XmlElement
	@com.aawhere.field.annotation.Field(dataType = FieldDataType.NUMBER)
	private ElevationChange elevationGain;

	@XmlElement(name = FIELD.DISTANCE)
	@com.aawhere.field.annotation.Field(key = FIELD.DISTANCE, dataType = FieldDataType.NUMBER,
			indexedFor = "AAWHERE-368")
	@Index
	private Length distance;

	/**
	 * TN-298 stopped persisting this, but re-introduced in version 24 because of TN-623
	 * 
	 * @see #getBoundingBox()
	 */
	private BoundingBox boundingBox;

	@com.aawhere.field.annotation.Field(key = FIELD.SPATIAL_BUFFER)
	@XmlJavaTypeAdapter(GeoCellsOptionalXmlAdapter.class)
	private GeoCells spatialBuffer;

	private ArchivedDocumentId documentId;

	@XmlElement
	private Velocity averageSpeed;

	@XmlElement
	private Length meanDistanceBetweenTrackpoints;

	@XmlElement
	private Duration meanDurationBetweenTrackpoints;

	/**
	 * A category describing our analysis of the signal quality for a given track. Many factors can
	 * be involved which this does not intend to answer, but it is to be used to compare one
	 * activity to another allowing a consumer of the activity to make a choice between activities.
	 * TN-934
	 * 
	 */
	@XmlElement
	private SignalQuality signalQuality;

	/**
	 * @return the averageDistanceBetweenTrackpoints
	 */
	public Length getMeanDistanceBetweenTrackpoints() {
		return meanDistanceBetweenTrackpoints;
	}

	/**
	 * @return the averageDurationBetweenTrackpoints
	 */
	public Duration getMeanDurationBetweenTrackpoints() {
		return meanDurationBetweenTrackpoints;
	}

	/**
	 * /**
	 * 
	 * @return the distance
	 */
	public Length getDistance() {
		return distance;
	}

	/**
	 * @return the duration
	 */
	public Duration getElapsedDuration() {
		return elapsedTime;
	}

	/**
	 * @return the finish
	 */
	public Trackpoint getFinish() {
		return finish;
	}

	public SignalQuality getSignalQuality() {
		return signalQuality;
	}

	/**
	 * @return the finishTime
	 */
	@XmlTransient
	public DateTime getFinishTime() {
		return finish.getTimestamp();
	}

	/**
	 * @return the numberOfTrackpoints
	 */
	public Integer getNumberOfTrackpoints() {
		return numberOfTrackpoints;
	}

	/**
	 * @return the start
	 */
	public Trackpoint getStart() {
		return start;
	}

	/**
	 * @return the startTime
	 */
	@XmlJavaTypeAdapter(type = DateTime.class, value = SimpleDateTimeXmlAdapter.class)
	public DateTime getStartTime() {
		return start.getTimestamp();
	}

	public Interval getInterval() {
		return new Interval(getStartTime(), getFinishTime());
	}

	public ArchivedDocumentId getDocumentId() {
		return documentId;
	}

	/**
	 * The track details are always stored in an {@link ArchivedDocument} and can be provided here
	 * in multiple formats.
	 * 
	 * @return
	 */
	@XmlElement
	@XmlJavaTypeAdapter(value = TrackFormatXmlAdapter.class)
	public ArchivedDocumentId getPoints() {
		return documentId;
	}

	public Velocity getAverageSpeed() {
		return averageSpeed;
	}

	public ElevationChange getElevationGain() {
		return elevationGain;
	}

	public ElevationChange getElevationChange() {
		return MeasurementUtil.createElevationChange(getStart().getElevation(), getFinish().getElevation());
	}

	@XmlElement(name = BoundingBox.FIELD.NAME)
	public BoundingBox getBoundingBox() {
		return transitionToGeoCellBoundingBox();
	}

	/**
	 * The {@link #boundingBox()} is created by points, but the bounds for the spatial buffer may be
	 * handing at times too.
	 * 
	 * @return
	 */
	public GeoCellBoundingBox getBoundingBoxForSpatialBuffer() {
		return GeoCellBoundingBox.createGeoCell().setGeoCells(getSpatialBuffer()).build();
	}

	/**
	 * temporary transition from storing bounding box. TN-298
	 * 
	 * The geocell accuracy has been determined to be too sloppy so bounding box is being stored
	 * again. Objectify Translator is provided for efficient string-based storage.
	 * 
	 * @return
	 */
	private synchronized BoundingBox transitionToGeoCellBoundingBox() {

		// bounding box should be available for many since re-introduced in version 24
		if (this.boundingBox == null && this.spatialBuffer != null) {
			// purposely not regenerating cells. we assume they have already been prepared
			this.boundingBox = GeoCellUtil.boundingBoxFromGeoCells(GeoCellUtil.filterToMaxResolution(spatialBuffer));
		}
		return this.boundingBox;
	}

	/**
	 * The Geo-spatial buffer that encompasses all of the track. This is a variable resolution set
	 * of geocells to avoid having too many cells representing large activities. TN-334
	 * 
	 * The resolution is included in the GeoCells object.
	 * 
	 * @return
	 */
	public GeoCells getSpatialBuffer() {
		return this.spatialBuffer;
	}

	/**
	 * @return the startArea
	 */
	public GeoCells startArea() {
		return this.startArea;
	}

	@XmlTransient
	public static class Builder
			extends ObjectBuilder<TrackSummary> {

		private Resolution resolution = MAX_START_AREA_RESOLUTION;

		public Builder() {
			this(new TrackSummary());
		}

		/** Builder method available for mutation of an existing TrackSummary. */
		Builder(TrackSummary original) {
			super(original);
		}

		public Builder populatePoints(Trackpoint start, Trackpoint finish) {
			setStart(start);
			setFinish(finish);
			Interval interval = new Interval(start.getTimestamp(), finish.getTimestamp());
			Duration duration = interval.toDuration();
			setElapsedDuration(duration);

			return this;
		}

		/**
		 * @param meanDistanceBetweenTrackpoints
		 *            the averageDistanceBetweenTrackpoints to set
		 */
		public Builder setMeanDistanceBetweenTrackpoints(Length meanDistanceBetweenTrackpoints) {
			building.meanDistanceBetweenTrackpoints = meanDistanceBetweenTrackpoints;
			return this;
		}

		/**
		 * @param averageDurationBetweenTrackpoints
		 *            the averageDurationBetweenTrackpoints to set
		 */
		public Builder setMeanDurationBetweenTrackpoints(Duration averageDurationBetweenTrackpoints) {
			building.meanDurationBetweenTrackpoints = averageDurationBetweenTrackpoints;
			return this;
		}

		/**
		 * @param distance
		 *            the distance to set
		 */
		public Builder setDistance(Length distance) {
			building.distance = distance;
			return this;
		}

		/**
		 * @param duration
		 *            the duration to set
		 */
		public Builder setElapsedDuration(Duration duration) {
			building.elapsedTime = duration;
			return this;
		}

		/**
		 * @param finish
		 *            the finish to set
		 */
		public Builder setFinish(Trackpoint finish) {
			building.finish = TrackUtil.simpleTrackpoint(finish);
			return this;
		}

		/**
		 * @param numberOfTrackpoints
		 *            the numberOfTrackpoints to set
		 */
		public Builder setNumberOfTrackpoints(Integer numberOfTrackpoints) {
			building.numberOfTrackpoints = numberOfTrackpoints;
			return this;
		}

		/**
		 * @param start
		 *            the start to set
		 */
		public Builder setStart(Trackpoint start) {
			building.start = TrackUtil.simpleTrackpoint(start);
			return this;
		}

		public Builder setBoundingBox(BoundingBox boundingBox) {
			building.boundingBox = boundingBox;
			return this;
		}

		public Builder setDocumentId(ArchivedDocumentId documentId) {
			building.documentId = documentId;
			return this;
		}

		/**
		 * @param quantity
		 * @return
		 */
		public Builder setElevationGain(ElevationChange elevationGain) {
			building.elevationGain = elevationGain;
			return this;
		}

		/**
		 * TODO: consider pushing {@link ElevationChange} all the way through the providers,
		 * calculators, and user stories rather than converting it here.
		 * 
		 * @param quantity
		 * @return
		 * 
		 */
		public Builder setElevationGain(Length elevationGain) {
			return setElevationGain(MeasurementUtil.createElevationChange(elevationGain));
		}

		public Builder setSpatialBuffer(GeoCells geoCells) {
			building.spatialBuffer = geoCells;
			return this;
		}

		public Builder startArea(GeoCells area) {
			// make the cells searchable
			building.startArea = GeoCellUtil.withAncestors(area);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("start", building.start);
			Assertion.exceptions().notNull("finish", building.finish);
		}

		@Override
		public TrackSummary build() {
			if (building.startArea == null) {
				startArea(GeoCellUtil.boundingBox(building.start.getLocation(), MAX_START_AREA_RESOLUTION)
						.getGeoCells());
			}
			// Calculate the average speed after the distance is set.
			if (building.distance != null & building.elapsedTime != null) {
				Double averageSpeedInMetersPerSecond = building.distance.doubleValue(MetricSystem.METRE)
						/ (new Double(building.elapsedTime.getStandardSeconds()));
				// TN-554 our system does not understand infinite values. There is no point in
				// supporting infinite values either.
				if (Double.isInfinite(averageSpeedInMetersPerSecond)) {
					averageSpeedInMetersPerSecond = 0d;
				}
				building.averageSpeed = MeasurementUtil.createVelocityInMps(averageSpeedInMetersPerSecond);
			}

			final TrackSummary built = super.build();
			if (built.spatialBuffer != null) {
				if (built.spatialBuffer.getAll().size() > MAX_GEO_CELLS_COUNT) {
					GeoCellsReducer reducer = GeoCellsReducer.create()
							.singleResolutionIgnoringParents(MAX_GEO_CELLS_COUNT).setGeoCells(built.spatialBuffer)
							.build();
					built.spatialBuffer = reducer.getReduced();
				} else if (built.spatialBuffer.getMaxResolution() == null) {
					throw new IllegalStateException("please set max resolution");
				}
			}
			return built;
		}

		public Builder setSignalQuality(SignalQuality signalQuality) {
			building.signalQuality = signalQuality;
			return this;

		}

	} // End Builder

	@XmlTransient
	public static final class FIELD {
		public static final String DOMAIN = "track";
		public static final String DISTANCE = "distance";
		public static final String START = "start";
		public static final String ELAPSED_TIME = "elapsedTime";
		public final static String SPATIAL_BUFFER = "spatialBuffer";
		public final static String START_AREA = "startArea";

		public static final String FORMAT = "format";

		public static final class KEY {
			public static final FieldKey ID = new FieldKey(DOMAIN, BaseEntity.BASE_FIELD.ID);
			public static final FieldKey DISTANCE = new FieldKey(DOMAIN, FIELD.DISTANCE);
			public static final FieldKey START = new FieldKey(DOMAIN, FIELD.START);
			public static final FieldKey START_TIME = new FieldKey(START, Trackpoint.FIELD.TIME);
			public static final FieldKey SPATIAL_BUFFER = new FieldKey(DOMAIN, FIELD.SPATIAL_BUFFER);
			public static final FieldKey SPATIAL_BUFFER_CELLS = new FieldKey(FIELD.SPATIAL_BUFFER, GeoCells.FIELD.CELLS);
			public static final FieldKey SPATIAL_BUFFER_MAX_RESOLUTION = new FieldKey(FIELD.SPATIAL_BUFFER,
					GeoCells.FIELD.MAX_RESOLUTION);
			public static final FieldKey TRACK_SUMMARY_SPATIAL_BUFFER_CELLS = new FieldKey(DOMAIN,
					SPATIAL_BUFFER_CELLS.getAbsoluteKey());
			public static final FieldKey TRACK_SUMMARY_SPATIAL_BUFFER_MAX_RESOLUTION = new FieldKey(DOMAIN,
					SPATIAL_BUFFER_MAX_RESOLUTION.getAbsoluteKey());
			/**
			 * Use this as a request parameter to indicate the format desired for the track.
			 * Omission means no trackpoints will be produced.
			 */
			public static final FieldKey FORMAT = new FieldKey(DOMAIN, FIELD.FORMAT);
		}

		public static final class OPTION {
			public static final String SPATIAL_BUFFER = KEY.SPATIAL_BUFFER.getAbsoluteKey();
			public static final String GOOGLE_POLYLINE = new FieldKey(FIELD.DOMAIN, GeoWebApiUri.GPOLYLINE)
					.getAbsoluteKey();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.BoundingBoxProvider#bounds()
	 */
	@Override
	public BoundingBox boundingBox() {
		return getBoundingBox();
	}

	/**
	 * Provided for TN-666 this will indicate if this track summary has a null bounding box. Likely
	 * the client will wish to update.
	 * 
	 * This is on {@link TrackSummary} for access to a private variable that is hidden by
	 * {@link #boundingBox()} method populating with geo cells bounding box.
	 * 
	 * @return
	 */
	public static Predicate<TrackSummary> nullBoundingBoxPredicate() {
		return new Predicate<TrackSummary>() {

			@Override
			public boolean apply(TrackSummary input) {
				return input.boundingBox == null;
			}
		};
	}

	/**
	 * Since this is not ann entity directly it must be called by those entities that contain this.
	 * 
	 * Prepares indexes.
	 * 
	 */
	public void prePersist() {
		this.start = SimpleTrackpoint.mutate(this.start).startTimeIndexed().build();
	}
}
