/**
 *
 */
package com.aawhere.track;

import java.io.Serializable;

import com.aawhere.measure.GeoCoordinate;

/**
 * The most basic identification of a location on earth (assuming on the surface of the earth).
 * WGS84 Coordinates.
 * 
 * @author roller
 * 
 */
public interface Point
		extends Serializable {

	/** The optional geo coordinates of this point. If if not. */
	GeoCoordinate getLocation();

	/** Indicates if lat/lon is available (not null). */
	Boolean isLocationCapable();
}
