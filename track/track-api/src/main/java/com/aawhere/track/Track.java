/**
 *
 */
package com.aawhere.track;

import java.io.Serializable;
import java.util.ListIterator;

/**
 * The series of trackpoints organized in sequence of time or travel. The Track is the core of the
 * CommonTrack Application.
 * 
 * TODO: Consider making this provide a {@link ListIterator} if going forward and back is necessary.
 * 
 * @author roller
 * @since 0.1
 */
public interface Track
		extends Iterable<Trackpoint>, Serializable {

}
