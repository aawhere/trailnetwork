/**
 * 
 */
package com.aawhere.track;

import com.aawhere.doc.Document;

/**
 * Any document that contains a track accessible by {@link #getTrack()}.
 * 
 * @author Aaron Roller
 * 
 */
public interface TrackContainer
		extends Document {

	/** Provides the track available, the first if many or null if no track is found. */
	Track getTrack();
}
