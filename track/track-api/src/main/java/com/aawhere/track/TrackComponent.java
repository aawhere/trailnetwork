/**
 * 
 */
package com.aawhere.track;

import java.io.Serializable;

/**
 * A smaller part of a {@link Track}.
 * 
 * @author Aaron Roller on Apr 20, 2011
 * 
 */
public interface TrackComponent
		extends Serializable {

}
