/**
 *
 */
package com.aawhere.track;

import java.util.Set;

import javax.measure.quantity.Length;

import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldKey;
import com.aawhere.measure.geocell.GeoCell;
import com.aawhere.measure.geocell.GeoCellFilterBuilder;
import com.aawhere.measure.geocell.GeoCells;
import com.aawhere.measure.geocell.GeoCellsReducer;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;

/**
 * A convienance class for building {@link Filter}s.
 * 
 * @author Brian Chapman
 * 
 */
public class TrackSummaryFilterBuilder<F extends TrackSummaryFilterBuilder<F>>
		extends Filter.BaseFilterBuilder<F> {

	private FieldDictionary dictionary;
	private GeoCellFilterBuilder spatialBuffer;

	public TrackSummaryFilterBuilder(FieldDictionary dictionary) {
		this(new Filter(), dictionary);
	}

	protected TrackSummaryFilterBuilder(Filter mutatee, FieldDictionary dictionary) {
		super(mutatee);
		/** Use {@link Builder} to construct TrackSummaryFilterBuilder */
		this.dictionary = dictionary;
	}

	public GeoCellFilterBuilder spatialBuffer() {
		if (this.spatialBuffer == null) {
			final FieldKey key = this.dictionary.getKey(TrackSummary.FIELD.KEY.SPATIAL_BUFFER.getRelativeKey());
			this.spatialBuffer = GeoCellFilterBuilder.mutate(key, building);
		}
		return this.spatialBuffer;
	}

	public F contains(TrackSummary other) {
		spatialBuffer().containsAtResolution(other.getSpatialBuffer());
		return dis;
	}

	/**
	 * @param spatialBuffer
	 */
	private void equalsSpatialBufferResolution(final GeoCells spatialBuffer) {
		final FieldKey resolutionKey = dictionary.getKey(TrackSummary.FIELD.KEY.SPATIAL_BUFFER_MAX_RESOLUTION
				.getAbsoluteKey());
		addCondition(FilterCondition.create().field(resolutionKey).operator(FilterOperator.EQUALS)
				.value(spatialBuffer.getMaxResolution().resolution).build());
	}

	/**
	 * Using the buffer from the {@link TrackSummary} this will return any TrackSummary that has at
	 * least one geocell in common.
	 * 
	 * <pre>
	 * X = 1,12,123
	 * Y = 1,12,120
	 * 
	 * X.intersects(Y) = false since 120 and 123 are not the same.
	 * 
	 * X is stored and Y is the query.
	 * 
	 * If we keep the parents of Y in the query we get a false postive:
	 * X.intersects(Y) = true since 1 or 12 are matched
	 * 
	 * Removing the parents of Y yields:
	 * Y=120
	 * 
	 * A query now provides the correct negative.
	 * X.intersects(Y-without parents)
	 * 
	 * Since Y does not have parents we can benefit from minimization.  Let's say Y has all 3rd resolution:
	 * Y = 1,12,120-12e
	 * X.intersects(Y) = true
	 * 
	 * Y without parents = 120-12e
	 * Y without parents minimized = 12
	 * X.intersects(Y without parents minimized) because both have 12
	 * 
	 * </pre>
	 * 
	 * @param other
	 * @return
	 */
	public F intersects(TrackSummary other) {
		GeoCells geoCells = other.getSpatialBuffer();
		equalsSpatialBufferResolution(geoCells);

		final FieldKey cellsKey = dictionary.getKey(TrackSummary.FIELD.KEY.SPATIAL_BUFFER_CELLS.getAbsoluteKey());
		Set<GeoCell> reduced = GeoCellsReducer.create().setMaxSize(MAX_VALUES_FOR_IN_OPERATOR).setMinimize(true)
				.setRemoveParents(true).setGeoCells(geoCells).build().getGeoCells();
		FilterCondition<Set<GeoCell>> geoCellCondition = new FilterCondition.Builder<Set<GeoCell>>().field(cellsKey)
				.operator(FilterOperator.IN).value(reduced).build();
		addCondition(geoCellCondition);

		return dis;

	}

	/**
	 * Limits results to those with a length greater than the minimum given.
	 * 
	 * @param minLength
	 */
	public F distanceGreaterThan(Length minLength) {
		final FilterOperator operator = FilterOperator.GREATER_THAN;
		return distance(operator, minLength);
	}

	/**
	 * Limits results to those with a length greater than the minimum given.
	 * 
	 * @param minLength
	 */
	public F distanceLessThan(Length length) {
		final FilterOperator operator = FilterOperator.LESS_THAN;
		return distance(operator, length);
	}

	/**
	 * @param operator
	 * @param minLength
	 * @return
	 */
	public F distance(final FilterOperator operator, Length minLength) {
		return addCondition(FilterCondition.create().field(dictionary.getKey(TrackSummary.FIELD.DISTANCE))
				.operator(operator).value(minLength).build());
	}

}
