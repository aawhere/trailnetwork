/**
 *
 */
package com.aawhere.track;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.AnnotatedDictionary;
import com.aawhere.field.annotation.Dictionary;

/**
 * * The main sensory measurement identifying the location of a sensor at a particular time.
 * 
 * @author roller
 * @since 0.1
 */
@XmlJavaTypeAdapter(TrackpointXmlAdapter.class)
@Dictionary(domain = Trackpoint.FIELD.DOMAIN)
public interface Trackpoint
		extends Point3d, TrackComponent, AnnotatedDictionary {
	static final String DOMAIN_KEY = FIELD.DOMAIN;

	/**
	 * Required for all trackpoints so it returns the absolute time. Use {@link Point} if time is
	 * not needed.
	 * 
	 * @return the absolute time (never null).
	 */

	DateTime getTimestamp();

	/**
	 * A required equals method that children should call from their overridden
	 * {@link Object#equals(Object)} method.
	 * 
	 * Equality for a Trackpoint is determined only by the {@link #getTimestamp()}. A Trackpoint
	 * {@link #equals(Track)} another trackpoint if and only if their timestamps are equal to the
	 * granularity of a millisecond.
	 * 
	 * Equality is limited to timestamp for efficiency and usefulness when organizing Trackpoints in
	 * Hashed collections.
	 * 
	 * Therefore equality should be delegated to {@link DateTime#equals(Object)} method.
	 * 
	 * @see TrackUtil#equals(Trackpoint, Trackpoint)
	 * 
	 * @param that
	 * @return
	 */
	boolean equals(Trackpoint that);

	@Override
	boolean equals(Object that);

	/**
	 * The hash code must use the hashcode provided by {@link #getTimestamp()}'s
	 * {@link DateTime#hashCode()} to provide consistency with the {@link #equals(Object)} method.
	 * 
	 * @see TrackUtil#hashCode(Trackpoint)
	 * 
	 * @return
	 */
	@Override
	int hashCode();

	@XmlTransient
	public static final class FIELD {
		public static final String DOMAIN = "trackpoint";
		public static final String TIME = "time";

		public static final class KEY {
			public static final FieldKey TIME = new FieldKey(DOMAIN, FIELD.TIME);
		}
	}
}
