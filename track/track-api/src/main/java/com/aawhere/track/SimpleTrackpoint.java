/**
 *
 */
package com.aawhere.track;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.joda.time.DateTime;

import com.aawhere.field.annotation.EmbeddedDictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.xml.XmlNamespace;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.PojoIf;

/**
 * Basic implementation of a trackpoint for anyone needing a place to assign the values for simple
 * retrieval.
 * 
 * Use the {@link Builder} for construction.
 * 
 * @author roller
 * 
 */
@XmlType(name = Trackpoint.DOMAIN_KEY, namespace = XmlNamespace.API_VALUE)
@XmlRootElement(name = Trackpoint.DOMAIN_KEY)
@XmlAccessorType(XmlAccessType.NONE)
public class SimpleTrackpoint
		implements Trackpoint {

	private static final long serialVersionUID = 4183741204579673296L;

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Provides a standard place to copy all attributes from the given trackpoint into a new one.
	 * 
	 * @param trackpoint
	 * @return
	 */
	public static Builder copy(Trackpoint trackpoint) {
		return create().setTimestamp(trackpoint.getTimestamp()).setLocation(trackpoint.getLocation())
				.setElevation(trackpoint.getElevation());
	}

	static Builder mutate(SimpleTrackpoint mutant) {
		return new Builder(mutant);
	}

	@XmlTransient
	public static class Builder
			extends ObjectBuilder<SimpleTrackpoint> {

		/**
		 * @param building
		 */
		public Builder() {
			super(new SimpleTrackpoint());
		}

		private Builder(SimpleTrackpoint simpleTrackpoint) {
			super(simpleTrackpoint);
		}

		/**
		 * @param location
		 */
		public Builder setLocation(GeoCoordinate location) {

			building.location = location;
			return this;
		}

		/**
		 * @param elevation
		 *            the elevation to set
		 */
		public Builder setElevation(Elevation elevation) {
			building.elevation = elevation;
			return this;
		}

		/**
		 * @param timestamp
		 *            the timestamp to set
		 */
		public Builder setTimestamp(DateTime timestamp) {
			building.timestamp = timestamp;
			return this;
		}

		public Builder startTimeIndexed() {
			building.startTimeIndexed = true;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			// TODO:enforce trackpoints have timestamps
			// if (building.timestamp == null) {
			// throw new TimestampRequiredRuntimeException();
			// }
			super.validate();
		}

	}

	@Ignore
	@XmlTransient
	private boolean startTimeIndexed = false;

	@XmlElement
	private Elevation elevation;
	@Index(IfStartTimeIndexed.class)
	@Field(key = FIELD.TIME, dataType = FieldDataType.NUMBER)
	@XmlElement
	private DateTime timestamp;
	@Field
	@EmbeddedDictionary
	@XmlElement
	private GeoCoordinate location;

	protected SimpleTrackpoint() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Trackpoint#getTimestamp()
	 */
	@Override
	public DateTime getTimestamp() {

		return timestamp;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Point3d#getElevation()
	 */
	@Override
	public Elevation getElevation() {
		return elevation;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Point#getLocation()
	 */
	@Override
	public GeoCoordinate getLocation() {
		return this.location;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Point#hasLocation()
	 */
	@Override
	public Boolean isLocationCapable() {
		return this.location != null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.track.Trackpoint#equals(com.aawhere.track.Track)
	 */
	@Override
	public boolean equals(Trackpoint that) {
		return TrackUtil.equals(this, that);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		return TrackUtil.equals(this, obj);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return TrackUtil.hashCode(this);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "tpt: " + TrackUtil.toString(this);
	}

	public static class IfStartTimeIndexed
			extends PojoIf<SimpleTrackpoint> {

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.condition.If#matchesPojo(java.lang.Object)
		 */
		@Override
		public boolean matchesPojo(SimpleTrackpoint trackpoint) {
			return (trackpoint != null && trackpoint.startTimeIndexed);
		}

	}
}
