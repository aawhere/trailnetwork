/**
 * 
 */
package com.aawhere.track;

/**
 * Useful for tracks that are able to understand how many trackpoints they have Available. Not all
 * are capable since they may be streams of trackpoints not yet discovered.
 * 
 * @author roller
 * 
 */
public interface SizeAwareTrack {

	public Integer getNumberOfTrackpoints();
}
