/**
 *
 */
package com.aawhere.track;

import java.math.RoundingMode;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.quantity.Velocity;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.doc.Document;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.string.StringUtilsExtended;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.calc.AngleMath;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.PythagoreanTheorum;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.unit.ExtraUnits;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Simple static utility intended to reduce some of the syntax required to create {@link Point}s,
 * {@link Trackpoint}s and {@link Track}s.
 * 
 * @see TrackSummaryUtil
 * 
 * @author roller
 * 
 */
public class TrackUtil {

	/**
	 * Useful for testing for when a track has no points or to bypass the requirement of a track.
	 */
	public static final Track EMPTY_TRACK = new SimpleTrack.TrackpointCollectionBuilder().getTrack();

	/**
	 * @param simpleTrackSegment
	 * @return
	 */
	public static Elevation averageElevationFrom(TrackSegment segment) {
		Elevation start = segment.getFirst().getElevation();
		Elevation end = segment.getSecond().getElevation();
		QuantityMath<Elevation> math = new QuantityMath<Elevation>(start).average(end);
		return math.getQuantity();
	}

	/**
	 * Computes the change in elevation as destination - origin. Value may be negative
	 * 
	 * @param origin
	 * @param destination
	 * @return
	 */
	public static ElevationChange elevationChange(Point3d origin, Point3d destination) {
		Elevation start = origin.getElevation();
		Elevation end = destination.getElevation();
		QuantityMath<Elevation> math = new QuantityMath<Elevation>(end).minus(start);
		return MeasurementUtil.createElevationChange(math.getQuantity());
	}

	/**
	 * Computes the elevation gain as follows:
	 * 
	 * return (destination - origin unless) = value unless value is less than 0, then return 0.
	 * 
	 * @param origin
	 * @param destination
	 * @return the elevation gain (always a positive number)
	 */
	public static ElevationChange elevationGain(Point3d origin, Point3d destination) {
		ElevationChange elevationChange = elevationChange(origin, destination);
		if (QuantityMath.create(elevationChange).negative()) {
			return MeasurementUtil.createElevationChange(MeasurementUtil.ZERO_LENGTH);

		} else {
			return elevationChange;
		}
	}

	public static ElevationChange elevationLoss(Point3d origin, Point3d destination) {
		ElevationChange elevationChange = elevationChange(origin, destination);
		if (QuantityMath.create(elevationChange).negative()) {
			return elevationChange;
		} else {
			return MeasurementUtil.createElevationChange(MeasurementUtil.ZERO_LENGTH);
		}
	}

	/**
	 * @param segment
	 * @return
	 */
	public static ElevationChange elevationGain(TrackSegment segment) {
		return TrackUtil.elevationGain(segment.getFirst(), segment.getSecond());
	}

	public static ElevationChange elevationLoss(TrackSegment segment) {
		return TrackUtil.elevationLoss(segment.getFirst(), segment.getSecond());
	}

	public static Velocity verticalVelocity(TrackSegment segment) {
		Length gain = MeasurementUtil.createLength(segment.getElevationChange());
		Duration duration = segment.getDuration();
		return MeasurementUtil.velocity(gain, duration);

	}

	public static Velocity velocity(TrackSegment segment) {
		Length distance = segment.getDistance();
		Duration duration = segment.getDuration();
		return MeasurementUtil.velocity(distance, duration);

	}

	/**
	 * Provides the heading from {@link TrackSegment#getFirst()} to {@link TrackSegment#getSecond()}
	 * .
	 * 
	 * @see #headingReversed(TrackSegment)
	 * 
	 * @param segment
	 * @return
	 */
	public static Angle heading(TrackSegment segment) {
		return GeoCalculator.create().build().heading(segment);
	}

	/**
	 * Provides the heading from {@link TrackSegment#getSecond()} to {@link TrackSegment#getFirst()}
	 * .
	 * 
	 * @param segment
	 * @return
	 */
	public static Angle headingReversed(TrackSegment segment) {
		return AngleMath.create(heading(segment)).reverse().getQuantity();
	}

	public static Length distanceBetween(Trackpoint t1, Trackpoint t2) {
		return new GeoCalculator.Builder().build().distanceBetween(t1, t2);
	}

	/**
	 * Calculates the recommended length of a track segment when reducing to the given number of
	 * trackpoints when considering the total length of a track. The fixedDistanceScalar provides a
	 * minimum precision of the resulting length which can be desirable to have a length rounded to
	 * the nearest meter for example.
	 * 
	 * This method is useful with fixed distance track processors.
	 * 
	 * @param maxNumberOfTrackpoints
	 * @param trackLength
	 *            the distance from track summary
	 * @param fixedDistanceScalar
	 *            limits the precision to the given scalar so the results will be rounded to the
	 *            scalar. The larger this value the wider the span of numberOfTrackpoints can be to
	 *            match the goal.
	 * @return
	 */
	public static Length segmentLength(Integer maxNumberOfTrackpoints, Length trackLength, Length fixedDistanceScalar) {
		// rounds to the nearest 10 meters fixed distance keeping the number of points below
		// 1000.
		final QuantityMath<Length> math = QuantityMath.create(trackLength)
		// switch to scalar unit to scale in desired units
				.convert(fixedDistanceScalar.getUnit())
				// reduces precision ex 10km / 10000 = 1 5 km / 10000 = 0.5
				.dividedBy(QuantityMath.create(fixedDistanceScalar).times(maxNumberOfTrackpoints).getQuantity())
				// go up to the next integer in the prefered unit
				.setScale(0, RoundingMode.CEILING).
				// scale the integer back to the target scale
				times(fixedDistanceScalar);
		final Length fixedDistanceApart = math.getQuantity();
		return fixedDistanceApart;
	}

	/**
	 * Calculates the average distance between the corresponding trackpoints for
	 * {@link TrackSegment#getFirst()} and {@link TrackSegment#getSecond()}.
	 * 
	 * TODO: (from bjc) this implementation doesn't correctly calculate the average distance between
	 * two lines. Consider the lines (0,0 and 0,16) and (1,8 and 9,8). The average distance is 5
	 * (dist between 0,8 and 5,8), but this method calculates 12.
	 * 
	 * 
	 * @param segment1
	 * @param segment2
	 * @return the average distance the two track segments are away from each other
	 */
	public static Length averageDistanceApart(TrackSegment segment1, TrackSegment segment2) {
		Trackpoint first = segment1.getFirst();
		Trackpoint first2 = segment2.getFirst();
		Length distance1 = distanceBetween(first, first2);
		Trackpoint second = segment1.getSecond();
		Trackpoint second2 = segment2.getSecond();
		Length distance2 = distanceBetween(second, second2);
		return new QuantityMath<Length>(distance1).average(distance2).getQuantity();
	}

	/**
	 * Tests the timestamps of the corresponding {@link Trackpoint}s to ensure the timestamps of
	 * each are equal.
	 * 
	 * @param segment1
	 * @param segment2
	 * @return true iff s1.first.timestamp = s2.first.timestamp and s1.second.timestamp =
	 *         s2.second.timestamp
	 */
	public static boolean timesAreSynched(TrackSegment segment1, TrackSegment segment2) {
		return timesAreSynched(segment1.getFirst(), segment2.getFirst())
				&& timesAreSynched(segment1.getSecond(), segment2.getSecond());
	}

	public static boolean timesAreSynched(Trackpoint trackpoint1, Trackpoint trackpoint2) {
		return JodaTimeUtil.equalsWithinTolerance(trackpoint1.getTimestamp(), trackpoint2.getTimestamp());
	}

	/**
	 * Copies all fo the values in the Trackpoint into a new {@link SimpleTrackpoint}, but replaces
	 * the timestamp with that given.
	 * 
	 * @param trackpoint
	 * @param generateRandomLong
	 * @return
	 */
	public static SimpleTrackpoint copy(Trackpoint trackpoint, Long timestampInMillis) {
		return SimpleTrackpoint.copy(trackpoint).setTimestamp(new DateTime(timestampInMillis)).build();
	}

	/**
	 * Casts or copies the trackpoint making it simple.
	 * 
	 * @param trackpoint
	 * @return
	 */
	public static SimpleTrackpoint simpleTrackpoint(Trackpoint trackpoint) {
		SimpleTrackpoint simpleTrackpoint;
		if (trackpoint instanceof SimpleTrackpoint) {
			simpleTrackpoint = (SimpleTrackpoint) trackpoint;
		} else {
			simpleTrackpoint = SimpleTrackpoint.copy(trackpoint).build();
		}
		return simpleTrackpoint;
	}

	public static BoundingBox boundingBox(Iterable<Trackpoint> track) {
		return BoundingBoxUtil.boundingBox(coordinates(track));
	}

	public static Iterable<GeoCoordinate> coordinates(Iterable<Trackpoint> track) {
		return Iterables.transform(track, coordinateFunction());
	}

	public static Function<Trackpoint, GeoCoordinate> coordinateFunction() {
		return new Function<Trackpoint, GeoCoordinate>() {

			@Override
			public GeoCoordinate apply(Trackpoint input) {
				if (input == null) {
					return null;
				}
				return input.getLocation();
			}
		};
	}

	/**
	 * 
	 * 
	 * @param timestamp
	 * @return
	 */
	public static Trackpoint createPoint(DateTime timestamp) {

		return createPoint(timestamp.getMillis());
	}

	public static Point createPoint(double latitudeInDecimalDegrees, double longitudeInDecimalDegrees) {
		return setLocation(new SimpleTrackpoint.Builder(), latitudeInDecimalDegrees, longitudeInDecimalDegrees).build();
	}

	public static Point
			createPoint(double latitudeInDecimalDegrees, double longitudeInDecimalDegrees, double elevation) {
		return setLocation(	setElevation(new SimpleTrackpoint.Builder(), elevation),
							latitudeInDecimalDegrees,
							longitudeInDecimalDegrees).build();
	}

	public static Trackpoint createPoint(long timeInMillis) {
		return setTime(new SimpleTrackpoint.Builder(), timeInMillis).build();
	}

	public static Trackpoint createPoint(long timeInMillis, double latitudeInDecimalDegrees,
			double longitudeInDecimalDegrees) {
		return setTime(	setLocation(new SimpleTrackpoint.Builder(), latitudeInDecimalDegrees, longitudeInDecimalDegrees),
						timeInMillis).build();
	}

	public static Trackpoint createPoint(long timeInMillis, double latitudeInDecimalDegrees,
			double longitudeInDecimalDegrees, double elevation) {
		return setTime(	setLocation(setElevation(new SimpleTrackpoint.Builder(), elevation),
									latitudeInDecimalDegrees,
									longitudeInDecimalDegrees),
						timeInMillis).build();
	}

	public static Trackpoint createPoint(Long timeInMillis, Double latitudeInDecimalDegrees,
			Double longitudeInDecimalDegrees, Double elevation) {
		Trackpoint point;
		if (timeInMillis != null) {
			long timeValue = timeInMillis.longValue();
			if (latitudeInDecimalDegrees != null && longitudeInDecimalDegrees != null) {
				double latValue = latitudeInDecimalDegrees.doubleValue();
				double lonValue = longitudeInDecimalDegrees.doubleValue();

				if (elevation != null) {
					point = createPoint(timeValue, latValue, lonValue, elevation.doubleValue());
				} else {
					point = createPoint(timeValue, latValue, lonValue);
				}
			} else {
				point = createPoint(timeValue);
			}

		} else {
			point = null;
		}
		return point;
	}

	public static Point createPoint(GeoCoordinate coordinate) {
		return createPoint(coordinate.getLatitude().getInDecimalDegrees(), coordinate.getLongitude()
				.getInDecimalDegrees());
	}

	/**
	 * Allowing nulls for any value, however, if the timestamp is empty then a null is returned to
	 * indicate no valid trackpoint could be created.
	 * 
	 * @param timeInMillis
	 * @param latitudeInDecimalDegrees
	 * @param longitudeInDecimalDegrees
	 * @param elevation
	 * @return the populated {@link Trackpoint} or null if timestamp is null.
	 */
	public static @Nullable
	Trackpoint createPointAllowNulls(@Nullable Long timeInMillis, @Nullable Double latitudeInDecimalDegrees,
			@Nullable Double longitudeInDecimalDegrees, @Nullable Number elevation) {
		if (timeInMillis == null) {
			return null;
		}
		return setElevation(setLocation(setTime(timeInMillis), latitudeInDecimalDegrees, longitudeInDecimalDegrees),
							elevation).build();
	}

	/**
	 * * Given strings this will convert to numbers and then to measurements allowing nulls. If
	 * duration is null then the resulting {@link Trackpoint} will also be null.
	 * 
	 * 
	 * @param startTime
	 * @param durationSinceStartInSeconds
	 *            seconds after start time. must be a positive whole number...any decimal will be
	 *            truncated
	 * @param latitudeStringInDecimalDegrees
	 * @param longitudeStringInDecimalDegrees
	 * @param elevationStringInMeters
	 * @return
	 */

	public static Trackpoint createPointAllowNulls(@Nonnull DateTime startTime,
			@Nullable String durationSinceStartInSeconds, @Nullable String latitudeStringInDecimalDegrees,
			@Nullable String longitudeStringInDecimalDegrees, @Nullable String elevationStringInMeters) {
		Integer durationOffset = NumberUtils
				.createInteger(StringUtilsExtended.nullIfEmpty(durationSinceStartInSeconds));
		Trackpoint result = null;
		if (durationOffset != null) {

			DateTime timestamp = startTime.plusSeconds(durationOffset);
			result = setElevation(	setLocation(setTime(timestamp),
												NumberUtils.createDouble(StringUtilsExtended
														.nullIfEmpty(latitudeStringInDecimalDegrees)),
												NumberUtils.createDouble(StringUtilsExtended
														.nullIfEmpty(longitudeStringInDecimalDegrees))),
									NumberUtils.createNumber(StringUtilsExtended.nullIfEmpty(elevationStringInMeters)))
					.build();
		}

		return result;
	}

	public static Boolean equals(Trackpoint source, Object targetTrackpoint) {
		boolean result = false;

		if (targetTrackpoint instanceof Trackpoint) {
			result = equals(source, (Trackpoint) targetTrackpoint);
		}
		return result;
	}

	/**
	 * A method used by {@link Trackpoint} implementations to test for equality according to the
	 * contract.
	 * 
	 * This only checks for equal timestamps which is the closes thing a Trackpoint has to an id.
	 * 
	 * @see Trackpoint#equals(Trackpoint)
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static Boolean equals(Trackpoint source, Trackpoint target) {
		return JodaTimeUtil.equalsWithinTolerance(source.getTimestamp(), target.getTimestamp());
	}

	/**
	 * Checks all the values of the trackpoint for equal within tolerance as defined by
	 * {@link QuantityMath}.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static Boolean equalsDeeply(Trackpoint source, Trackpoint target) {
		// intentionally ignore class check since implementation doesn't matter
		Boolean result = true;
		if (source == null || target == null) {
			result = false;
		} else {
			DateTime sourceTimestamp = source.getTimestamp();
			DateTime targetTimestamp = target.getTimestamp();
			result &= sourceTimestamp.equals(targetTimestamp);
			if (!result) {
				// undo the negative
				result = JodaTimeUtil.equalsWithinTolerance(sourceTimestamp, targetTimestamp);
			}
			if (source.getElevation() != null) {
				result &= new QuantityMath<Elevation>(source.getElevation()).equals(target.getElevation());
			} else {
				// covers nulls and equality...if both are null then equals
				result &= source.getElevation() == target.getElevation();
			}
			if (source.getLocation() != null) {
				result &= source.getLocation().equals(target.getLocation());
			} else {
				// covers nulls and equality...if both are null then equals
				result &= source.getLocation() == target.getLocation();
			}
		}
		return result;

	}

	/**
	 * Iterates from the begining until a trackpoint is found that is
	 * {@link Trackpoint#isLocationCapable()}.
	 * 
	 * @param track
	 * @return the first trackpoint with location or null if none found
	 */
	public static Trackpoint firstTrackpointWithLocation(Iterable<Trackpoint> track) {
		Iterator<Trackpoint> iterator = track.iterator();
		while (iterator.hasNext()) {
			Trackpoint next = iterator.next();
			if (next.isLocationCapable()) {
				return next;
			}
		}

		return null;
	}

	/**
	 * @see Trackpoint#hashCode()
	 * 
	 * @param source
	 * @return
	 */
	public static int hashCode(Trackpoint source) {
		return source.getTimestamp().hashCode();
	}

	/**
	 * Interpolates a trackpoint between the segment at the given {@link DateTime}.
	 * 
	 * @see #interpolate(TrackSegment, Ratio)
	 * @param segment
	 * @param nextTargetedTimestamp
	 */
	public static Trackpoint interpolate(TrackSegment segment, DateTime targetTimestamp) {
		Interval totalInterval = segment.getInterval();
		Duration totalDuration = totalInterval.toDuration();
		Trackpoint virtualTrackpoint;
		// zero time causes problems. This is the same as a ratio of zero.
		// not entirely sure if this is practical, but we'll leave that up to
		// the client.
		if (totalDuration.getMillis() == 0) {
			virtualTrackpoint = segment.getFirst();
		} else {

			Interval targetInterval = new Interval(segment.getFirst().getTimestamp(), targetTimestamp);
			Ratio ratio = JodaTimeUtil.ratioFrom(targetInterval.toDuration(), totalDuration);
			virtualTrackpoint = TrackUtil.interpolate(segment, ratio);
		}
		return virtualTrackpoint;
	}

	/**
	 * Interpolates the point between the segment at the point where the ratio is in between the
	 * first and second point of the segment.
	 * 
	 * @param segment
	 * @param ratio
	 * @return
	 */
	public static Trackpoint interpolate(TrackSegment segment, Ratio ratio) {
		assertInterpolation(ratio);
		Trackpoint result;

		result = estimate(segment, ratio);

		return result;
	}

	/**
	 * General purpose calculator to estimate a trackpoint for
	 * {@link #interpolate(TrackSegment, Ratio)} or {@link #extrapolate(TrackSegment, Ratio)}.
	 * 
	 * Providing a negative ratio will create a trackpoint before the first (extrapolation).
	 * Providing a ratio above 1 will create a trackpoint beyond the second (extrapolation). Provide
	 * a ratio between 0-1 will provide a trackpoint between first and second (interpolation).
	 * 
	 * @see #interpolate(TrackSegment, Ratio)
	 * @see #extrapolate(TrackSegment, Ratio)
	 * 
	 * @param segment
	 * @param ratio
	 * @param math
	 * @return
	 */
	private static Trackpoint estimate(TrackSegment segment, Ratio ratio) {
		Trackpoint result;
		// only create a new point if we must...exact matches pass back the
		// original
		Ratio tolerance = MeasurementUtil.ratio(0.001);
		final QuantityMath<Ratio> ratioMath = QuantityMath.create(ratio);
		if (ratioMath.equals(MeasurementUtil.RATIO_ZERO, tolerance)) {
			result = segment.getFirst();
		} else if (ratioMath.equals(MeasurementUtil.RATIO_ONE, tolerance)) {
			result = segment.getSecond();
		} else {
			Length b = horizontalDistance(segment);
			Length adjustedB = QuantityMath.create(b).times(ratio.doubleValue(ExtraUnits.RATIO)).getQuantity();
			GeoCoordinate location = GeoMath.coordinateFrom(segment.getFirst().getLocation(),
															TrackUtil.heading(segment),
															adjustedB);

			result = new SimpleTrackpoint.Builder().setTimestamp(interpolateTimestamp(segment, ratio))
					.setElevation(interpolateElevation(segment, ratio)).setLocation(location).build();
		}

		return result;
	}

	/**
	 * Given the segment with will determine the horizontal distance or
	 * {@link PythagoreanTheorum#getB()}.
	 * 
	 * @param segment
	 * @return
	 */
	public static Length horizontalDistance(TrackSegment segment) {
		Length a = MeasurementUtil.createLength(QuantityMath.create(segment.getElevationChange()).absolute()
				.getQuantity());
		Length b = PythagoreanTheorum.create().setC(segment.getDistance()).setA(a).build().getB();
		return b;
	}

	/**
	 * @param ratio
	 * @return
	 */
	private static QuantityMath<Ratio> assertInterpolation(Ratio ratio) {
		QuantityMath<Ratio> math = new QuantityMath<Ratio>(ratio).setScale(4);

		if (math.lessThan(MeasurementUtil.RATIO_ZERO) || math.greaterThan(MeasurementUtil.RATIO_ONE)) {
			throw new IllegalArgumentException(ratio + " must be between " + MeasurementUtil.RATIO_ZERO + " and "
					+ MeasurementUtil.RATIO_ONE + " but was " + ratio.getValue());
		}
		return math;
	}

	/**
	 * Given a segment and a coordinate that must lie on the line between the two coordinates this
	 * returns a trackpoint with the given location and interpolated time and elevation if present.
	 * 
	 * Since locationBetween is a {@link GeoCoordinate} without elevation then only 2D calculations
	 * will be performed for the ratio, but the resulting point will have elevation interpolated
	 * properly.
	 * 
	 * @param segment
	 * @param locationBetween
	 * @return
	 */
	public static Trackpoint interpolate(TrackSegment segment, GeoCoordinate locationBetween) {
		Length distanceToLocation = GeoMath.length(segment.getFirst().getLocation(), locationBetween);
		Length segmentDistance = GeoMath.length(segment.getFirst().getLocation(), segment.getSecond().getLocation());

		Ratio ratio = MeasurementUtil.ratio(distanceToLocation, segmentDistance);
		return interpolate(segment, ratio);
	}

	public static Elevation interpolateElevation(TrackSegment segment, Ratio ratio) {
		Elevation result;
		final Elevation firstElevation = segment.getFirst().getElevation();
		final Elevation secondElevation = segment.getSecond().getElevation();
		if (firstElevation != null && secondElevation != null) {
			Length interpolatedLength = MeasurementUtil.createLength(new QuantityMath<Elevation>(firstElevation)
					.interpolate(secondElevation, ratio).getQuantity());
			result = MeasurementUtil.createElevation(interpolatedLength);
		} else {
			result = null;
		}

		return result;
	}

	public static DateTime interpolateTimestamp(TrackSegment segment, Ratio ratio) {
		long millisPortion = (long) (segment.getDuration().getMillis() * ratio.doubleValue(ExtraUnits.RATIO));
		return segment.getFirst().getTimestamp().plus(millisPortion);
	}

	/**
	 * Given a segment this will add the given length to each endpoint making the line longer in
	 * each direction.
	 * 
	 * @param segment
	 * @param addToEndpoints
	 * @return
	 */
	public static TrackSegment lengthen(TrackSegment segment, Length addToEndpoints) {
		// to get the first we must be looking in the reverse direction
		Trackpoint first = extrapolateBefore(segment, addToEndpoints);
		// to get the second we must look past the second
		Trackpoint second = extrapolateBeyond(segment, addToEndpoints);
		return new SimpleTrackSegment(first, second);
	}

	/**
	 * Estimates a point before the first point estimating a straight line from the segment.
	 * 
	 * @param segment
	 * @param addToEndpoints
	 * @return
	 */
	public static Trackpoint extrapolateBefore(TrackSegment segment, Length beforeFirstPoint) {
		Ratio ratio = MeasurementUtil.ratio(beforeFirstPoint, segment.getDistance());
		ratio = QuantityMath.create(ratio).negate().getQuantity();
		return extrapolate(segment, ratio);
	}

	/**
	 * Creates an estimated {@link Trackpoint} add the given distance beyond the second endpoint of
	 * the segment in the direction of the line from first to second.
	 * 
	 * @param segment
	 * @param beyondSecondPoint
	 * @return
	 */
	public static Trackpoint extrapolateBeyond(TrackSegment segment, Length beyondSecondPoint) {

		Ratio ratio;
		Length segmentDistance = segment.getDistance();
		// if segment distance is zero, division by zero will happen TN-185
		// if extrapolation is not possible by length if both points in same location
		if (QuantityMath.create(segmentDistance).equalToZero()) {
			// just given them the second point back...
			ratio = MeasurementUtil.RATIO_ONE;
		} else {
			Length targetLength = QuantityMath.create(segmentDistance).plus(beyondSecondPoint).getQuantity();
			ratio = MeasurementUtil.ratio(targetLength, segmentDistance);
		}
		return extrapolate(segment, ratio);
	}

	/**
	 * Estimates a trackpoint that lies outside the given segment endpoints beyond the second
	 * endpoint in the direction from first to second endpoint.
	 * 
	 * @see #estimate(TrackSegment, Ratio)
	 * 
	 * @param segment
	 * @param ratio
	 * @return
	 */
	public static Trackpoint extrapolate(TrackSegment segment, Ratio ratio) {
		return estimate(segment, ratio);
	}

	public static String toString(Trackpoint source) {
		StringBuffer buf = new StringBuffer();
		buf.append("\ntime: ");
		buf.append(source.getTimestamp());
		buf.append("\nlat: ");

		GeoCoordinate location = source.getLocation();
		if (location != null) {
			buf.append(location.getLatitude());
			buf.append("\nlon: ");
			buf.append(location.getLongitude());
		}
		buf.append("\nele: ");
		buf.append(source.getElevation());
		return buf.toString();
	}

	public static String toString(TrackSegment segment) {
		StringBuffer buf = new StringBuffer();
		buf.append(segment.getFirst());
		buf.append("\n");
		buf.append(segment.getSecond());
		return buf.toString();
	}

	/**
	 * @deprecated use {@link #equalsDeeply(Trackpoint, Trackpoint)} since it allows tolerance
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static Boolean valuesEqual(Trackpoint source, Trackpoint target) {
		// same comparisons handles null checks
		return source.getTimestamp().equals(target.getTimestamp())
				&& (source.getElevation() == target.getElevation() || (source.getElevation() != null && source
						.getElevation().equals(target.getElevation())))
				&& (source.getLocation().getLatitude() == target.getLocation().getLatitude() || (source.getLocation()
						.getLatitude() != null && source.getLocation().getLatitude()
						.equals(target.getLocation().getLatitude())))
				&& (source.getLocation().getLongitude() == target.getLocation().getLongitude() || (source.getLocation()
						.getLongitude() != null && source.getLocation().getLongitude()
						.equals(target.getLocation().getLongitude())));
	}

	public static Boolean valuesEqual(TrackSegment source, TrackSegment target) {
		Trackpoint firstSource = source.getFirst();
		Trackpoint secondSource = source.getSecond();
		Trackpoint firstTarget = target.getFirst();
		Trackpoint secondTarget = target.getSecond();

		return valuesEqual(firstSource, firstTarget) && valuesEqual(secondSource, secondTarget);
	}

	private static SimpleTrackpoint.Builder setElevation(SimpleTrackpoint.Builder builder, double elevation) {
		builder.setElevation(MeasurementUtil.createElevationInMeters(elevation));
		return builder;
	}

	private static SimpleTrackpoint.Builder setElevation(SimpleTrackpoint.Builder builder, @Nullable Number elevation) {
		// elevation is nullable
		builder.setElevation(MeasurementUtil.createElevationInMeters(elevation));
		return builder;
	}

	private static SimpleTrackpoint.Builder setLocation(SimpleTrackpoint.Builder builder,
			double latitudeInDecimalDegrees, double longitudeInDecimalDegrees) {

		return setLocation(builder, (Double) latitudeInDecimalDegrees, (Double) longitudeInDecimalDegrees);
	}

	private static SimpleTrackpoint.Builder setLocation(SimpleTrackpoint.Builder builder, @Nullable Double latitude,
			@Nullable Double longitude) {
		if (latitude != null && longitude != null) {
			builder.setLocation(new GeoCoordinate.Builder().setLatitude(latitude).setLongitude(longitude).build());
		}
		return builder;
	}

	private static SimpleTrackpoint.Builder setTime(long millis) {
		return setTime(new SimpleTrackpoint.Builder(), millis);
	}

	private static SimpleTrackpoint.Builder setTime(DateTime timestamp) {
		return new SimpleTrackpoint.Builder().setTimestamp(timestamp);
	}

	private static SimpleTrackpoint.Builder setTime(SimpleTrackpoint.Builder builder, long millis) {
		builder.setTimestamp(new DateTime(millis));
		return builder;
	}

	/** Calculates the duration cumulated from one or more segments. */

	public static Duration duration(TrackSegment... segments) {
		long cumulativeMillis = 0;

		for (int whichSegment = 0; whichSegment < segments.length; whichSegment++) {
			TrackSegment trackSegment = segments[whichSegment];
			cumulativeMillis += trackSegment.getDuration().getMillis();
		}
		return new Duration(cumulativeMillis);
	}

	@SuppressWarnings("unchecked")
	public static List<Trackpoint> trackpointsFrom(Track track) {
		return IteratorUtils.toList(track.iterator());
	}

	/**
	 * Given a segment with two trackpoints this will indicate if both return
	 * {@link Trackpoint#isLocationCapable()}.
	 * 
	 * @param segment
	 * @return
	 */
	public static boolean isLocationCapable(TrackSegment segment) {
		return segment.getFirst().isLocationCapable() && segment.getSecond().isLocationCapable();
	}

	/**
	 * Given any iterable of trackpoints this will convert it to a track or cast it if already a
	 * track.
	 * 
	 * @param track
	 * @return
	 */
	public static Track track(Iterable<Trackpoint> track) {
		if (track instanceof Track) {
			return (Track) track;
		} else {
			return new SimpleTrack.Builder(track).build();
		}
	}

	public static Track track(ArchivedDocument archived) throws TrackIncapableDocumentException {
		Document doc = archived.getDocument();
		if (doc instanceof TrackContainer) {
			TrackContainer container = (TrackContainer) doc;
			return container.getTrack();
		} else {
			throw new TrackIncapableDocumentException(archived.id());
		}

	}

	/**
	 * 
	 * @return the track or null if the document is not a valid track.
	 */
	public static Function<ArchivedDocument, Track> trackFromArchivedDocumentFunction() {
		return new Function<ArchivedDocument, Track>() {

			@Override
			public Track apply(ArchivedDocument input) {
				try {
					return track(input);
				} catch (TrackIncapableDocumentException e) {
					return null;
				}
			}
		};
	}

}
