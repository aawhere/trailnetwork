/**
 * 
 */
package com.aawhere.track;

import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * @author Aaron Roller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class TrackIncapableDocumentException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 4108185358842877681L;

	/**
	 * @param documentId
	 */
	public TrackIncapableDocumentException(ArchivedDocumentId documentId) {
		super(new CompleteMessage.Builder(TrackSummaryMessage.TRACK_INCAPABLE_DOCUMENT)
				.addParameter(TrackSummaryMessage.Param.DOC_ID, documentId).build());
	}
}
