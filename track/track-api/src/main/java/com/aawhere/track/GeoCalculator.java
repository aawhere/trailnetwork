/**
 *
 */
package com.aawhere.track;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.PythagoreanTheorum;
import com.aawhere.measure.calc.QuantityMath;

/**
 * A useful tool for calculating Distance, angles and points using only WGS84.
 * 
 * @author roller
 * 
 */
public class GeoCalculator {

	/**
	 * Used to construct all instances of GeoCalculator.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCalculator> {

		public Builder() {
			super(new GeoCalculator());
		}

		/**
		 * Regardless of the type of point given, this will ignore elevation for all calculations.
		 * 
		 */
		public Builder twoDimensionCalculationsOnly() {
			building.threeDimensionCalculations = false;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** disables 3D calculations regardless of the point given. */
	private boolean threeDimensionCalculations = true;

	/** Use {@link Builder} to construct GeoCalculator */
	private GeoCalculator() {
	}

	/**
	 * Determines the distance between two points. If the points are 3D and have elevations then
	 * this will calculate the distance between the two considering elevation change.
	 * 
	 * @see #distanceBetween(Point3d, Point3d)
	 * 
	 * @param origin
	 * @param destination
	 * @return
	 */
	public Length distanceBetween(Point origin, Point destination) {

		// horizontal length at a minimum...consider vertical effects and reassign later if
		// appropriate
		Length length = GeoMath.horizontalDistanceBetween(origin.getLocation(), destination.getLocation());
		// consider elevation in calculation?
		if (this.threeDimensionCalculations && origin instanceof Point3d && destination instanceof Point3d) {

			Point3d origin3d = (Point3d) origin;
			Point3d destination3d = (Point3d) destination;
			// elevation is required for both to be considered.
			if (origin3d.getElevation() != null && destination3d.getElevation() != null) {
				Length b = verticalDistanceBetween(origin3d, destination3d);
				length = new PythagoreanTheorum.Builder().setA(length).setB(b).build().getHypotenuse();
			}
		}
		return length;
	}

	/**
	 * Calculates the 3D distance between two points on the earth, but considers elevation as part
	 * of the distance.
	 */
	public Length distanceBetween(Point3d origin, Point3d destination) {

		return distanceBetween((Point) origin, (Point) destination);
	}

	private Length verticalDistanceBetween(Point3d origin, Point3d destination) {
		Elevation destEle = destination.getElevation();
		Elevation originEle = origin.getElevation();
		return MeasurementUtil.createLength(new QuantityMath<Elevation>(destEle).minus(originEle).getQuantity());
	}

	/**
	 * @param segment
	 * @return
	 */
	public Angle heading(TrackSegment segment) {
		return GeoMath.heading(segment.getFirst().getLocation(), segment.getSecond().getLocation());
	}
}
