/**
 *
 */
package com.aawhere.track;

import javax.annotation.Nonnull;
import javax.measure.quantity.Length;
import javax.measure.quantity.Velocity;

import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.measure.Elevation;
import com.aawhere.measure.ElevationChange;

/**
 * Two trackpoints providing a straight line from the first to the second (in that order).
 * 
 * @author roller
 * 
 */
public interface TrackSegment
		extends TrackComponent {

	/**
	 * The {@link Trackpoint} that occurs before {@link #getSecond()}.
	 * 
	 * @return
	 */
	@Nonnull
	Trackpoint getFirst();

	/**
	 * The Trackpoint that occurs after {@link #getFirst()}.
	 * 
	 * @return
	 */
	@Nonnull
	Trackpoint getSecond();

	Interval getInterval();

	Duration getDuration();

	/**
	 * Provides the 3D Distance between the {@link #getFirst()} and {@link #getSecond()} points.
	 */
	Length getDistance();

	/**
	 * @return
	 */
	Elevation getAverageElevation();

	/**
	 * Total elevation change over the segment. This is not the elevation gain, which would return a
	 * positive value or 0. Elevation change can be negative.
	 * 
	 * @return
	 */
	ElevationChange getElevationChange();

	Velocity getVerticalVelocity();

	Velocity getVelocity();

	@Override
	boolean equals(Object that);

	@Override
	int hashCode();
}
