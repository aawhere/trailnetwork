/**
 *
 */
package com.aawhere.track;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.EntityMessage;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Provides personalized messages for {@link TrackSummary}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlTransient
public enum TrackSummaryMessage implements Message {

	ID_NAME(EntityMessage.ID_NAME.getValue()),
	ID_DESCRIPTION(EntityMessage.ID_DESCRIPTION.getValue()),
	TRACK_NAME("Track"),
	TRACK_DESCRIPTION("The path recorded during an Activity."),
	DISTANCE_NAME("Distance"),
	DISTANCE_DESCRIPTION("The total distance of the track."),
	DISTANCE_GROUP_NAME("Distance Group"),
	DISTANCE_GROUP_DESCRIPTION("Distance group category for the track."),
	ELEVATION_GAIN_NAME("Elevation Gain"),
	ELEVATION_GAIN_DESCRIPTION("Cumulative positive change in elevation."),
	NUMBER_OF_POINTS_MISSING_LOCATION(
			"{COUNT} of {TOTAL_COUNT} trackpoints filtered since they had no location.",
			Param.COUNT,
			Param.TOTAL_COUNT),
	/** Indicates elevation was missing from trackpoints to avoid calculation issues */
	NUMBER_OF_POINTS_MISSING_ELEVATION(
			"{COUNT} of {TOTAL_COUNT} trackpoints filtered since they had no elevation.",
			Param.COUNT,
			Param.TOTAL_COUNT),
	/** When a document is attempting to be loaded, but it does not have a track inside. */

	TRACK_INCAPABLE_DOCUMENT("The document {DOC_ID} is not a valid track.", Param.DOC_ID),
	/** Explanation of the points filtered due to equal or lesser timestamps in sequence. */
	NUMBER_OF_POINTS_BAD_TIMESTAMPS(
			"{COUNT} of {TOTAL_COUNT} trackpoints filtered due to non-progressing timestamps.",
			Param.TOTAL_COUNT,
			Param.COUNT),
	/** Just a pro-active message explaining the track was processed. */
	TRACK_PROCESSED("{TOTAL_COUNT} trackpoints processed", Param.TOTAL_COUNT), /* */
	DATE_CREATED_NAME(EntityMessage.DATE_CREATED_NAME.getValue()), /* */
	DATE_CREATED_DESCRIPTION(EntityMessage.DATE_CREATED_DESCRIPTION.getValue()), /* */
	DATE_UPDATED_NAME(EntityMessage.DATE_UPDATED_NAME.getValue()), /* */
	DATE_UPDATED_DESCRIPTION(EntityMessage.DATE_UPDATED_DESCRIPTION.getValue()), /* */
	ELAPSED_TIME_NAME("Elapsed Time"), /* */
	ELAPSED_TIME_DESCRIPTION("Time difference from start of activity to end of activity."), /* */
	GEO_CELLS_NAME("Geo Cells"), /* */
	GEO_CELLS_DESCRIPTION("Collection of GeoCells following the path of the track.");/* */

	private ParamKey[] parameterKeys;
	private String message;

	private TrackSummaryMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** Indicates some number being counted. */
		COUNT,
		/** Always bigger than COUNT */
		TOTAL_COUNT,
		/** THe document id related to the track. */
		DOC_ID;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static final class Doc {
		public static final String DESCRIPTION = "The sums and average measurements calculated from the Track details.";
	}
}
