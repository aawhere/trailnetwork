/**
 * 
 */
package com.aawhere.track;

import java.util.Comparator;

/**
 * Compares the trackpoint's timestamps. Timestamps are required so any trackpoint without a
 * timestamp will throw a {@link NullPointerException} during
 * {@link #compare(Trackpoint, Trackpoint)}.
 * 
 * @author Aaron Roller
 * 
 */
public class TrackpointTimestampComparator
		implements Comparator<Trackpoint> {

	private static TrackpointTimestampComparator instance;

	/**
	 * Used to retrieve the only instance of TrackpointTimestampComparator.
	 */
	public static TrackpointTimestampComparator getInstance() {

		if (instance == null) {
			instance = new TrackpointTimestampComparator();
		}
		return instance;

	}// end Builder

	/** Use {@link #getInstance()} to get TrackpointTimestampComparator */
	private TrackpointTimestampComparator() {
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Trackpoint o1, Trackpoint o2) {
		return o1.getTimestamp().compareTo(o2.getTimestamp());
	}
}
