/**
 *
 */
package com.aawhere.track;

import java.util.Iterator;

import org.joda.time.DateTime;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * Simple iterator used to iterate trackpoints, but return {@link DateTime} timestamps.
 * 
 * @author roller
 * 
 */
public class TrackTimestampIterator
		implements Iterator<DateTime> {

	/**
	 * Used to construct all instances of TrackTimestampIterator.
	 */
	public static class Builder
			extends ObjectBuilder<TrackTimestampIterator> {

		public Builder() {
			super(new TrackTimestampIterator());
		}

		public Builder setTrack(Track track) {
			building.trackpoints = track.iterator();
			return this;
		}

		@Override
		protected void validate() {

			super.validate();
			Assertion.assertNotNull(building.trackpoints);
		}

	}// end Builder

	/** Use {@link Builder} to construct TrackTimestampIterator */
	private TrackTimestampIterator() {
	}

	private Iterator<Trackpoint> trackpoints;
	private Trackpoint currentTrackpoint;

	/*
	 * (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return trackpoints.hasNext();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public DateTime next() {
		this.currentTrackpoint = this.trackpoints.next();
		return this.currentTrackpoint.getTimestamp();
	}

	/**
	 * @return the currentTrackpoint
	 */
	public Trackpoint getCurrentTrackpoint() {
		return currentTrackpoint;
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		this.trackpoints.remove();

	}

}
