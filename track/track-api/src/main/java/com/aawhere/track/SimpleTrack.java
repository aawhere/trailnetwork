/**
 *
 */
package com.aawhere.track;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.collect.ImmutableList;

/**
 * The most basic implementation of a Track using only simple types to provide the function.
 * 
 * @author roller
 * 
 */
public class SimpleTrack
		implements Track {

	private static final long serialVersionUID = -5339767322213394604L;

	/**
	 * @author roller
	 * 
	 */
	public static class Builder
			extends ObjectBuilder<SimpleTrack> {

		/**
		 * @param building
		 */
		public Builder(Iterable<Trackpoint> collection) {
			super(new SimpleTrack(ImmutableList.copyOf(collection)));

		}

	}

	public static class TrackpointCollectionBuilder {
		private ArrayList<Trackpoint> trackpoints = new ArrayList<Trackpoint>();

		public TrackpointCollectionBuilder add(Trackpoint... trackpoint) {
			trackpoints.addAll(Arrays.asList(trackpoint));
			return this;
		}

		/**
		 * @return the trackpoints
		 */
		public java.util.List<Trackpoint> getTrackpoints() {
			return ImmutableList.copyOf(trackpoints);
		}

		public Track getTrack() {
			return new SimpleTrack.Builder(getTrackpoints()).build();
		}

		/**
		 * @param segment
		 * @return
		 */
		public TrackpointCollectionBuilder add(TrackSegment segment) {
			add(segment.getFirst());
			add(segment.getSecond());
			return this;
		}

		/**
		 * @param linePoints
		 * @return
		 */
		public TrackpointCollectionBuilder addAll(List<Trackpoint> linePoints) {

			this.trackpoints.addAll(linePoints);
			return this;
		}

		/**
		 * @return
		 */
		public Boolean isEmpty() {
			return this.trackpoints.isEmpty();
		}

	}

	private Iterable<Trackpoint> collection;

	private SimpleTrack(Iterable<Trackpoint> collection) {
		this.collection = collection;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Trackpoint> iterator() {
		return collection.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return this.collection.toString();
	}

}
