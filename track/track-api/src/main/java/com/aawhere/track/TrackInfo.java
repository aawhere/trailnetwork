/**
 * 
 */
package com.aawhere.track;

import org.apache.commons.lang3.tuple.Pair;

/**
 * A common way to package the {@link TrackSummary} and {@link Track} together.
 * 
 * @author aroller
 * 
 */
public class TrackInfo {

	private final Pair<TrackSummary, Iterable<Trackpoint>> pair;

	/**
	 * 
	 */
	public TrackInfo(TrackSummary summary, Iterable<Trackpoint> track) {
		this.pair = Pair.of(summary, track);
	}

	/**
	 * @return the pair
	 */
	public Pair<TrackSummary, ? extends Iterable<Trackpoint>> pair() {
		return this.pair;
	}

	public Iterable<Trackpoint> track() {
		return pair.getRight();
	}

	public TrackSummary summary() {
		return pair.getKey();
	}

	/**
	 * @param trackSummary
	 * @param track
	 * @return
	 */
	public static TrackInfo build(TrackSummary trackSummary, Iterable<Trackpoint> track) {
		return new TrackInfo(trackSummary, track);
	}

}
