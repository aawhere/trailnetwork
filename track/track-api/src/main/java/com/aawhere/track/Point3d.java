/**
 *
 */
package com.aawhere.track;

import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.measure.Elevation;

/**
 * A location and elevation identifying a position on earth that may be on or above the surface.
 * 
 * @author roller
 * @since 0.1
 */
public interface Point3d
		extends Point {

	@Field(dataType = FieldDataType.NUMBER)
	Elevation getElevation();
}
