/**
 * 
 */
package com.aawhere.test.category;


/**
 * Web Client tests indicate this test will hit a web api or website that must be available for the
 * test to succeed. These test may be fast or slow depending upon what they are doing and the speed
 * of the web resources.
 * 
 * 
 * @author Aaron Roller
 * 
 */
public interface WebClientTest {

}
