/**
 * 
 */
package com.aawhere.test.category;

/**
 * Indicates that the given test depends upon working task queues. These tests
 * often times introduce delays in addition to requiring special resources to
 * receive and process task queues.
 * 
 * 
 * @author aroller
 * 
 */
public interface TaskQueueTest {

}
