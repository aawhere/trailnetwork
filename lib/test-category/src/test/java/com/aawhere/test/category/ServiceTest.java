/**
 * 
 */
package com.aawhere.test.category;

/**
 * Tests against a Service class that may depend upon many different types of external services like
 * database, web applications, email servers, etc.
 * 
 * @author Aaron Roller
 * 
 */
public interface ServiceTest {

}
