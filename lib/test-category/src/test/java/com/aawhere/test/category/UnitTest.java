/**
 * 
 */
package com.aawhere.test.category;

/**
 * Unit tests are small, very fast and self-supported tests that do not require any system to be
 * available whatsoever.
 * 
 * The declaration of UnitTest is assumed so inclusion of it is optional as the test will run
 * without it. It is recommended best practice to include it for clarity.
 * 
 * 
 * @author Aaron Roller
 * 
 */
public interface UnitTest {

}
