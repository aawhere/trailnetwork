/**
 * 
 */
package com.aawhere.test.category;

/**
 * A Test that requires access to the System Reources to get a file for it's contents.
 * 
 * This does not exclude being a {@link UnitTest} as a test would likely declare to be both.
 * 
 * @author Aaron Roller
 * 
 */
public interface FileTest {

}
