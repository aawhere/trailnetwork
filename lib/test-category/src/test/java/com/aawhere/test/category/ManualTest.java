/**
 *
 */
package com.aawhere.test.category;

/**
 * Manually run tests. One use case is for seeding the database for UI Testing and development.
 *
 * @author Brian Chapman
 *
 */
public interface ManualTest {

}
