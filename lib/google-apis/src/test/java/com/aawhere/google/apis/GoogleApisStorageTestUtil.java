/**
 *
 */
package com.aawhere.google.apis;

import static org.mockito.Mockito.mock;

import com.google.api.services.storage.Storage;

/**
 * @author Brian Chapman
 * 
 */
public class GoogleApisStorageTestUtil {

	public static final String LIVE_BUCKET = "common-track";
	public static final String LIVE_FIXTURE_PREFIX = "common-track/fixture/base";
	public static final String LIVE_KEY = "common-track/fixture/base/calevaras_a_135741825.axm";

	protected GoogleApisStorageTestUtil() {
	}

	public Storage getStorageInstance() {
		Storage storage = mock(Storage.class);
		return storage;
	}

}
