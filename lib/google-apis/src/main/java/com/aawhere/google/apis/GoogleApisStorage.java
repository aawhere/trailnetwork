/**
 *
 */
package com.aawhere.google.apis;

import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.services.storage.Storage;
import com.google.inject.Singleton;

/**
 * Helper for using google-api-java-client's storage api
 * 
 * @see https://developers.google.com/storage/docs/developer-guide
 * @see http://code.google.com/p/google-api-java-client
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class GoogleApisStorage {

	private static GoogleApisStorage instance;
	private static GoogleApis googleApis = GoogleApis.getInstance();
	public final static String COMMON_TRACK_BUCKET = "common-track";
	public final static String COMMON_TRACK_FIXTURE_BASE_PREFIX = "common-track/fixture/base";

	/**
	 * Used to retrieve the only instance of GoogleApisStorage.
	 */
	public static GoogleApisStorage getInstance() {

		if (instance == null) {
			instance = new GoogleApisStorage();
		}
		return instance;

	}// end Builder

	/** Use {@link #getInstance()} to get GoogleApisStorage */
	private GoogleApisStorage() {
	}

	public Storage getStorageInstance(HttpRequestInitializer httpRequestInitializer) {
		Storage storage = new Storage.Builder(googleApis.getHttpTransport(), googleApis.getJsonFactory(),
				httpRequestInitializer).build();
		return storage;
	}

}
