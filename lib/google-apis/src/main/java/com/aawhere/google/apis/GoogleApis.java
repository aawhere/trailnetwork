/**
 *
 */
package com.aawhere.google.apis;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.inject.Singleton;

/**
 * @author brian
 * 
 */
@Singleton
public class GoogleApis {

	private static GoogleApis instance;
	// Set up the HTTP transport and JSON factory. These should be treated as global shared
	// instances.
	private final HttpTransport httpTransport = new NetHttpTransport();
	private final JsonFactory jsonFactory = new JacksonFactory();

	/**
	 * Used to retrieve the only instance of GoogleApis.
	 */
	public static GoogleApis getInstance() {

		if (instance == null) {
			instance = new GoogleApis();
		}
		return instance;

	}// end Builder

	/** Use {@link #getInstance()} to get GoogleApis */
	private GoogleApis() {
	}

	public HttpTransport getHttpTransport() {
		return httpTransport;
	}

	public JsonFactory getJsonFactory() {
		return jsonFactory;
	}

}
