/**
 *
 */
package com.aawhere.google.apis;

import com.aawhere.io.IoMessage;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * @author Brian Chapman
 * 
 */
public class ObjectNotFoundException
		extends BaseException {

	private static final long serialVersionUID = -3265400794157605568L;

	public ObjectNotFoundException(String path) {
		this(path, null);
	}

	/**
	 *
	 */
	public ObjectNotFoundException(String path, java.io.IOException e) {
		super(new CompleteMessage.Builder(IoMessage.FILE_NOT_FOUND).addParameter(IoMessage.Param.FILE_NAME, path)
				.build(), e);
	}

}
