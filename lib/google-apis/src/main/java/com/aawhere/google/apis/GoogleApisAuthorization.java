/**
 *
 */
package com.aawhere.google.apis;

import java.util.Arrays;
import java.util.List;

import com.google.api.client.googleapis.extensions.appengine.auth.oauth2.AppIdentityCredential;
import com.google.inject.Singleton;

/**
 * Provides helper methods for google-api-java-client
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class GoogleApisAuthorization {

	private static GoogleApisAuthorization instance;

	/**
	 * Used to retrieve the only instance of Authorization.
	 */
	public static GoogleApisAuthorization getInstance() {

		if (instance == null) {
			instance = new GoogleApisAuthorization();
		}
		return instance;

	}// end Builder

	/** Use {@link #getInstance()} to get Authorization */
	private GoogleApisAuthorization() {
	}

	public AppIdentityCredential getAppIdentityCredential(List<String> apis) {
		// Set up OAuth 2.0 access of protected resources
		AppIdentityCredential credential = new AppIdentityCredential(apis);
		return credential;
	}

	public AppIdentityCredential getAppIdentityCredential(String... apis) {
		return getAppIdentityCredential(Arrays.asList(apis));
	}

	public AppIdentityCredential getAppIdentityCredentialForStorage() {
		return getAppIdentityCredential("https://www.googleapis.com/auth/devstorage.read_write");
	}
}
