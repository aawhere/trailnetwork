/**
 * 
 */
package com.aawhere.gpx._1._1;

import org.junit.Ignore;

/**
 * @author aroller
 * 
 */
@Ignore("The unit test is expecting strict where this is happy with anything")
public class GpxRelaxedUnitTest
		extends GpxUnitTest {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxUnitTest#getGpxUtil()
	 */
	@Override
	protected GpxTestUtil getGpxUtil() {
		return new GpxRelaxedTestUtil();
	}

}
