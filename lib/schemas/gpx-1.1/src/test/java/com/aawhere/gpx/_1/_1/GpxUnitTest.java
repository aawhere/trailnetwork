package com.aawhere.gpx._1._1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.test.category.UnitTest;

/**
 * 
 */

/**
 * Tests the parsing of a GPX file.
 * 
 * @author roller
 * 
 */
@Category(UnitTest.class)
public abstract class GpxUnitTest {

	/** doesn't pass */
	public static final String MINIMUM = "<gpx><trk><trkseg><trkpt lon=\"1\" lat=\"5\"><ele>10</ele><time>2004-02-14T18:36:17Z</time></trkpt></trkseg></trk></gpx>";
	/** doesn't pass */
	public static final String MINIMUM_WITH_VERSION = "<gpx version=\"1.1\"><trk><trkseg><trkpt lon=\"1\" lat=\"5\"><ele>10</ele><time>2004-02-14T18:36:17Z</time></trkpt></trkseg></trk></gpx>";
	/** passes */
	public static final String MINIMUM_WITH_NAMESPACE = "<gpx creator=\"Garmin Connect\" xmlns=\"http://www.topografix.com/GPX/1/1\" version=\"1.1\"><trk><trkseg><trkpt lon=\"1\" lat=\"5\"><ele>10</ele><time>2004-02-14T18:36:17Z</time></trkpt></trkseg></trk></gpx>";
	/** passes */
	public static final String GARMIN_CONNECT_MIN = "<gpx version=\"1.1\" creator=\"Garmin Connect\" xmlns=\"http://www.topografix.com/GPX/1/1\"><trk><trkseg><trkpt lon=\"1\" lat=\"5\"><ele>10</ele><time>2004-02-14T18:36:17Z</time></trkpt></trkseg></trk></gpx>";
	/** passes */
	public static final String GARMIN_CONNECT_COMPLETE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><gpx version=\"1.1\" creator=\"Garmin Connect\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\" xmlns=\"http://www.topografix.com/GPX/1/1\" xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\" xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><metadata><link href=\"connect.garmin.com\"><text>Garmin Connect</text></link><time>2004-02-14T18:36:17.000Z</time></metadata><trk><name>Heart</name><desc>We love the TrailNetwork!</desc><trkseg><trkpt lon=\"-122.48309603892267\" lat=\"37.8623628616333\"><ele>-5.800000190734863</ele><time>2004-02-14T18:36:17.000Z</time></trkpt><trkpt lon=\"-122.48281717300415\" lat=\"37.86279193125665\"><ele>-5.800000190734863</ele><time>2004-02-14T18:36:48.000Z</time></trkpt></trkseg></trk></gpx>";

	protected GpxTestUtil gpxUtil;

	@Before
	public void setupGpxUnitTest() {
		this.gpxUtil = getGpxUtil();
	}

	protected abstract GpxTestUtil getGpxUtil();

	public void assertGpxString(String xml, Boolean expectedToPass) {
		try {
			gpxUtil.loadString(xml);
			assertTrue("Expected to pass: " + expectedToPass + "(" + xml + ")", expectedToPass);
		} catch (GpxReadException e) {
			assertFalse(e.getMessage(), expectedToPass);
		}
	}

	@Test
	public void testStrings() {
		assertGpxString(MINIMUM, false);
		assertGpxString(MINIMUM_WITH_VERSION, false);
		assertGpxString(MINIMUM_WITH_NAMESPACE, true);
		assertGpxString(GARMIN_CONNECT_MIN, true);
		assertGpxString(GARMIN_CONNECT_COMPLETE, true);
	}

}
