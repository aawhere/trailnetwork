/**
 * 
 */
package com.aawhere.gpx._1._1;

import org.junit.Test;

import com.aawhere.gpx._1._1.GpxTestUtil.GpxTestDocument;

/**
 * @author aroller
 * 
 */
public class GpxRelaxedFileTest
		extends GpxFileTest {

	private GpxRelaxedTestUtil util = new GpxRelaxedTestUtil();

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.gpx._1._1.GpxFileTest#getGpx(com.aawhere.gpx._1._1.GpxTestUtil.GpxTestDocument)
	 */
	@Override
	protected Gpx getGpx(GpxTestDocument gpx) throws GpxReadException {
		return util.loadResource(gpx.resourceName);

	}

	@Test
	public void testEveryTrailTripTrack() throws GpxReadException {
		GpxTestDocument testDocument = new GpxTestDocument("eTripTrackResponse-2735753.xml", 1, 1, 400);
		assertGpx(testDocument);
	}
}
