/**
 * 
 */
package com.aawhere.gpx._1._1;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.gpx._1._1.GpxTestUtil.GpxTestDocument;
import com.aawhere.test.category.FileTest;
import com.aawhere.xml.bind.JAXBReadException;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public abstract class GpxFileTest {

	@Test
	public void testGarminConnectActivity() throws GpxReadException {
		assertGpx(GpxTestUtil.GARMIN_CONNECT_ACTIVITY);
	}

	@Test
	public void testHeart() throws GpxReadException {
		assertGpx(GpxTestUtil.HEART_FILE);
	}

	@Test
	public void testHugeActivity() throws GpxReadException {
		assertGpx(GpxTestUtil.GARMIN_CONNECT_ACTIVITY_HUGE);
	}

	@Test
	public void testLikeBloatedActivity() throws GpxReadException {
		assertGpx(GpxTestUtil.GARMIN_CONNECT_ACTIVITY_LIKE_BLOATED);
	}

	@Test
	public void testBloatedActivity() throws GpxReadException {
		assertGpx(GpxTestUtil.GARMIN_CONNECT_ACTIVITY_BLOATED);
	}

	@Test
	public void testEveryTrail() throws GpxReadException {
		assertGpx(GpxTestUtil.EVERYTRAIL);
	}

	@Test
	public void testEveryTrailProblem() throws GpxReadException {
		assertGpx(GpxTestUtil.EVERY_TRAIL_1_1_PARSE_PROB);
	}

	@Test
	public void testEveryTrailGarminExtensions() throws GpxReadException {
		assertGpx(GpxTestUtil.EVERY_TRAIL_1_1_GARMIN_EXTENSIONS);
	}

	@Test
	public void testEveryTrailGpxTpx() throws GpxReadException {
		assertGpx(GpxTestUtil.EVERY_TRAIL_1_1_GPXTPX);
	}

	/**
	 * Mostly used to demonstrate how fast the huge file could be when read a different way.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testBloatedFileReading() throws IOException {
		InputStream is = ClassLoader
				.getSystemResourceAsStream(GpxTestUtil.GARMIN_CONNECT_ACTIVITY_BLOATED.resourceName);
		@SuppressWarnings("rawtypes")
		List lines = IOUtils.readLines(is);
		assertEquals(154528, lines.size());
	}

	/**
	 * @param gpx
	 * @throws JAXBReadException
	 */
	protected void assertGpx(GpxTestDocument gpxDocument) throws GpxReadException {
		Gpx gpx = getGpx(gpxDocument);
		GpxTestUtil.assertGpx(gpxDocument, gpx);
	}

	protected abstract Gpx getGpx(GpxTestDocument gpx) throws GpxReadException;

}
