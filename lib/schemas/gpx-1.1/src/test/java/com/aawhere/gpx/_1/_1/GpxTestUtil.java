/**
 * 
 */
package com.aawhere.gpx._1._1;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import com.aawhere.gpx._1._1.GpxTestUtil.GpxTestDocument;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBReadException;

/**
 * Used to assist the many dependencies on GPX files.
 * 
 * @author aroller
 * 
 */
public abstract class GpxTestUtil {

	public static final GpxTestDocument GARMIN_CONNECT_ACTIVITY = new GpxTestDocument("GarminConnectActivity.gpx", 1,
			1, 2240);
	/** Has times with millis */
	public static final GpxTestDocument EVERY_TRAIL_1_0 = new GpxTestDocument("et-514786-1_0.gpx", 1, 1, 307);
	/** Has times with no millis. */
	public static final GpxTestDocument EVERY_TRAIL_1_0_TIME_PROB = new GpxTestDocument("et-828094-1_0.gpx", 1, 1, 281);
	/** Has times with no millis. */
	public static final GpxTestDocument EVERY_TRAIL_1_1_PARSE_PROB = new GpxTestDocument("et-206-1_1.gpx", 1, 1, 283);
	/** Has times with no millis. */
	public static final GpxTestDocument EVERY_TRAIL_1_1_GARMIN_EXTENSIONS = new GpxTestDocument(
			"et-30422-1_1-GPXX_V3.gpx", 1, 1, 400);
	/** A gpx with an invalid schema since the extension is not declared in the gpx file. */
	public static final GpxTestDocument EVERY_TRAIL_1_1_GPXTPX = new GpxTestDocument("et-519103_gpxtpx.gpx", 1, 1, 909);

	/** inside passage boat ride. Really long and 13 hours. */
	public static final GpxTestDocument GARMIN_CONNECT_ACTIVITY_HUGE = new GpxTestDocument(
			"gc_huge_activity_7877284.gpx", 1, 1, 6418);

	/** 173 Km bike ride with temperature and HR and 15K trackpoints */
	public static final GpxTestDocument GARMIN_CONNECT_ACTIVITY_BLOATED = new GpxTestDocument(
			"gc_bloated_activity_reyes_188755294.gpx", 1, 1, 15451);
	public static final GpxTestDocument EVERYTRAIL = new GpxTestDocument("everytrail_2121681.gpx", 2, 1, 296);

	/** Large bike ride with a lot of overlap of bloated, but not nearly as enormous */
	public static final GpxTestDocument GARMIN_CONNECT_ACTIVITY_LIKE_BLOATED = new GpxTestDocument(
			"gc_huge_activity_reyes_310821468.gpx", 1, 1, 3510);

	public static final GpxTestDocument HEART_FILE = new GpxTestDocument("gc-3006961-aroller-heart.gpx", 1, 1, 68);

	public static BigDecimal generateRandomLat() {
		return TestUtil.generateRandomBigDecimal(-90, 90);
	}

	public static BigDecimal generateRandomLon() {
		return TestUtil.generateRandomBigDecimal(-180, 180);
	}

	public abstract Wpt generateRandomWpt();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxTestUtil#loadResource(java.lang.String)
	 */
	public Gpx loadResource(String resourcePath) throws GpxReadException {
		InputStream is = ClassLoader.getSystemResourceAsStream(resourcePath);
		assertNotNull(resourcePath + " not found", is);
		Gpx gpxType = getReader().read(is);
		assertNotNull(gpxType);
		return gpxType;
	}

	/**
	 * @return
	 */
	protected abstract GpxReader getReader();

	/**
	 * Given a string this will read it into a GPX if the format is correct.
	 * 
	 * @param xml
	 * @return
	 * @throws JAXBReadException
	 *             if the format is not valid
	 */
	public Gpx loadString(String xml) throws GpxReadException {
		ByteArrayInputStream is = new ByteArrayInputStream(xml.getBytes());
		return getReader().read(is);
	}

	/**
	 * Provides the recommended test activity when specifics are not a concern.
	 * 
	 * @param testDocument
	 *            Provide the test document you wish to use for your test.
	 * 
	 * @throws JAXBReadException
	 * 
	 */
	public Gpx getTestGpx(GpxTestDocument testDocument) throws GpxReadException {
		return loadResource(testDocument.resourceName);

	}

	/**
	 * Provides specifically a Garmin Connect activity. No particular activity type is indicated,
	 * but just the format produced by the latest API at Garmin Connect.
	 * 
	 * http://connectapi.garmin.com
	 * 
	 * @return
	 * @throws JAXBReadException
	 */
	public Gpx getGarminConnectActivity() throws GpxReadException {
		return getTestGpx(GARMIN_CONNECT_ACTIVITY);
	}

	public static class GpxTestDocument {
		public final String resourceName;
		public final int numberOfTracks;
		public final int numberOfTrackSegments;
		public final int numberOfTrackpoints;

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			boolean result;
			if (obj == null || !obj.getClass().equals(GpxTestDocument.class)) {
				result = false;
			} else {
				result = ((GpxTestDocument) obj).resourceName.equals(resourceName);
			}
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return this.resourceName.hashCode();
		}

		/**
		 * @param resourceName
		 * @param numberOfTracks
		 * @param numberOfTrackSegments
		 * @param numberOfTrackpoints
		 */
		public GpxTestDocument(String resourceName, int numberOfTracks, int numberOfTrackSegments,
				int numberOfTrackpoints) {
			super();
			this.resourceName = resourceName;
			this.numberOfTracks = numberOfTracks;
			this.numberOfTrackSegments = numberOfTrackSegments;
			this.numberOfTrackpoints = numberOfTrackpoints;
		}

	}

	/**
	 * @param gpxDocument
	 * @param gpx
	 */
	public static void assertGpx(GpxTestDocument gpxDocument, Gpx gpx) {

		List<Trk> tracks = gpx.getTracks();
		assertEquals("should only be one track", gpxDocument.numberOfTracks, tracks.size());
		Trk track = tracks.get(0);
		List<Trkseg> segments = track.getSegments();
		assertEquals("incorrect number of segments", gpxDocument.numberOfTrackSegments, segments.size());
		List<Wpt> trackpoints = segments.get(0).getTrackpoints();
		assertEquals("incorrect number of trackpoints found", gpxDocument.numberOfTrackpoints, trackpoints.size());
		if (!trackpoints.isEmpty()) {
			Wpt wpt = trackpoints.get(0);
			assertNotNull("time", wpt.getDateTime());
			assertNotNull("lat", wpt.getLatitude());
			assertNotNull("lon", wpt.getLongitude());
			assertNotNull("elevation", wpt.getElevation());
		}

	}
}
