/**
 * 
 */
package com.aawhere.gpx._1._1;

import static org.junit.Assert.*;

import java.io.InputStream;

import com.aawhere.gpx._1._1.GpxReader;
import com.aawhere.gpx._1._1.GpxRelaxedReader;
import com.aawhere.gpx._1._1.Wpt;

/**
 * @author aroller
 * 
 */
public class GpxRelaxedTestUtil
		extends GpxTestUtil {

	private GpxRelaxedReader reader;

	/**
 * 
 */
	public GpxRelaxedTestUtil() {
		this.reader = new GpxRelaxedReader();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxTestUtil#generateRandomWpt()
	 */
	@Override
	public Wpt generateRandomWpt() {
		return null;
	}

	/*
	 * /* (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxTestUtil#getReader()
	 */
	@Override
	protected GpxReader getReader() {
		return this.reader;
	}

}
