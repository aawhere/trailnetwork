/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;

import com.aawhere.lang.ObjectBuilder;

/**
 * A serializable GPX implementation allowing the transfer of DOM elements into a pojo so it may be
 * distributed through the system.
 * 
 * @author aroller
 * 
 */
public class SimpleGpx
		implements Gpx {

	private static final long serialVersionUID = 8602330409417225048L;

	/**
	 * Used to construct all instances of SimpleGpx.
	 */
	public static class Builder
			extends ObjectBuilder<SimpleGpx> {

		private SimpleTrkseg seg;
		private SimpleTrk track;

		public Builder() {
			super(new SimpleGpx());
		}

		public Builder trackpoint(DateTime timestamp, Double lat, Double lon, Double ele) {
			SimpleWpt wpt = new SimpleWpt();
			wpt.timestamp = timestamp;
			wpt.latitude = lat;
			wpt.longitude = lon;
			wpt.elevation = ele;
			seg.trackpoints.add(wpt);
			return this;
		}

		public Builder nextTrack() {
			track = new SimpleTrk();
			building.tracks.add(track);
			return this;
		}

		public Builder nextSegment() {
			seg = new SimpleTrkseg();
			track.segments.add(seg);
			return this;
		}

		@Override
		public SimpleGpx build() {
			SimpleGpx built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct SimpleGpx */
	private SimpleGpx() {
	}

	public static Builder create() {
		return new Builder();
	}

	private List<Trk> tracks = new LinkedList<>();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getWaypoints()
	 */
	@Override
	public List<Wpt> getWaypoints() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getTracks()
	 */
	@Override
	public List<Trk> getTracks() {
		return this.tracks;
	}

	public static class SimpleTrk
			implements Trk {

		private static final long serialVersionUID = 2493483168841783353L;
		LinkedList<Trkseg> segments = new LinkedList<>();

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.gpx._1._1.Trk#getSegments()
		 */
		@Override
		public List<Trkseg> getSegments() {
			return segments;
		}
	}

	public static class SimpleTrkseg
			implements Trkseg {

		private LinkedList<Wpt> trackpoints = new LinkedList<>();
		private static final long serialVersionUID = 1L;

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.gpx._1._1.Trkseg#getTrackpoints()
		 */
		@Override
		public List<Wpt> getTrackpoints() {
			return trackpoints;
		}

	}

	public static class SimpleWpt
			implements Wpt {

		private static final long serialVersionUID = 5799280189343969981L;
		private Double latitude;
		private Double longitude;
		private Double elevation;
		private DateTime timestamp;

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.gpx._1._1.Wpt#getLatitude()
		 */
		@Override
		public Double getLatitude() {
			return latitude;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.gpx._1._1.Wpt#getLongitude()
		 */
		@Override
		public Double getLongitude() {
			return longitude;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.gpx._1._1.Wpt#getDateTime()
		 */
		@Override
		public DateTime getDateTime() {
			return timestamp;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.gpx._1._1.Wpt#getElevation()
		 */
		@Override
		public Double getElevation() {
			return elevation;
		}
	}
}
