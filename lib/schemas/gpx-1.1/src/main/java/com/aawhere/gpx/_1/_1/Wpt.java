/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.io.Serializable;

import org.joda.time.DateTime;

/**
 * 
 * Common interface to Jaxb and Jibx Wpt objects. This interface contains the minimal methods
 * required for the application. Add methods as required.
 * 
 * @author Brian Chapman
 * 
 */
public interface Wpt
		extends Serializable {

	public Double getLatitude();

	public Double getLongitude();

	public DateTime getDateTime();

	public Double getElevation();
}
