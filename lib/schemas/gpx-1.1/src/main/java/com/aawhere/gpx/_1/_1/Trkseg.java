/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.io.Serializable;
import java.util.List;

/**
 * Common interface to Jaxb and Jibx Trkseg objects. This interface contains the minimal methods
 * required for the application. Add methods as required.
 * 
 * @author Brian Chapman
 * 
 */
public interface Trkseg
		extends Serializable {

	public List<Wpt> getTrackpoints();
}
