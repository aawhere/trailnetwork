/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.io.InputStream;

/**
 * Common reader for Gpx types.
 * 
 * @author Brian Chapman
 * 
 */
public interface GpxReader {

	public Gpx read(InputStream inputStream) throws GpxReadException;

	public byte[] write(Gpx gpx);
}
