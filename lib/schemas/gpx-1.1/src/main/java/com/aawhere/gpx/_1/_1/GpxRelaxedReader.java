/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * To support those "well formed" gpx with good intentions, but bad xml structure this will parse
 * the xml using element names and ignoring the rest.
 * 
 * @see GpxRelaxed
 * 
 * @author aroller
 * 
 */
public class GpxRelaxedReader
		implements GpxReader {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxReader#read(java.io.InputStream)
	 */
	@Override
	public Gpx read(InputStream inputStream) throws GpxReadException {
		try {
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
			Element root = document.getDocumentElement();
			return GpxRelaxed.create().root(root).build();
		} catch (SAXException | IOException | ParserConfigurationException e) {
			throw new GpxReadException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxReader#write(com.aawhere.gpx._1._1.Gpx)
	 */
	@Override
	public byte[] write(Gpx gpx) {
		throw new UnsupportedOperationException("do we need this?");
	}

}
