/**
 * 
 */
package com.aawhere.gpx._1._1;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * General exception that happens when unmarshalling xml into a Gpx object.
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(HttpStatusCode.UNPROCESSABLE_ENTITY)
public class GpxReadException
		extends BaseException {

	private static final long serialVersionUID = 5736328043599102486L;

	/**
	 * @param cause
	 */
	public GpxReadException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param e
	 */
	public GpxReadException(Message message, Exception e) {
		super(message, e);
	}

	/**
	 * @param message
	 * @param paramKey
	 * @param paramValue
	 */
	public GpxReadException(Message message, ParamKey paramKey, Object paramValue) {
		super(message, paramKey, paramValue);
	}

	/**
	 * @param message
	 */
	public GpxReadException(Message message) {
		super(message);
	}

}
