/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.util.List;

import org.joda.time.DateTime;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.aawhere.joda.time.JodaTimeUtil;
import com.aawhere.lang.ObjectBuilder;

/**
 * Provided a general DOM GPX Node this will do it's best to understand the GPX without strict
 * validation.
 * 
 * @see SimpleGpx
 * 
 * @author aroller
 * 
 */
public class GpxRelaxed
		implements Gpx {

	private static final long serialVersionUID = 7455736759664025776L;

	/**
	 * Used to construct all instances of GpxRelaxed.
	 */
	public static class Builder
			extends ObjectBuilder<GpxRelaxed> {

		private Element root;

		public Builder() {
			super(new GpxRelaxed());
		}

		public Builder root(Element root) {
			this.root = root;
			return this;
		}

		@Override
		public GpxRelaxed build() {
			GpxRelaxed built = super.build();
			SimpleGpx.Builder builder = SimpleGpx.create();

			NodeList trackElements = root.getElementsByTagName("trk");
			for (int i = 0; i < trackElements.getLength(); i++) {
				builder.nextTrack();
				final Element trackElement = (Element) trackElements.item(i);
				NodeList trackSegmentElements = trackElement.getElementsByTagName("trkseg");
				for (int j = 0; j < trackSegmentElements.getLength(); j++) {
					builder.nextSegment();
					final Element trackSegmentElement = (Element) trackSegmentElements.item(j);
					NodeList trackpointElements = trackSegmentElement.getElementsByTagName("trkpt");
					for (int k = 0; k < trackpointElements.getLength(); k++) {
						Element trackpointElement = (Element) trackpointElements.item(k);
						Double lon = Double.parseDouble(trackpointElement.getAttribute("lon"));
						Double lat = Double.parseDouble(trackpointElement.getAttribute("lat"));
						NodeList elevationElements = trackpointElement.getElementsByTagName("ele");
						Double elevation;
						if (elevationElements.getLength() > 0) {
							Node nodeValue = elevationElements.item(0).getFirstChild();
							if (nodeValue != null) {
								try {
									elevation = Double.parseDouble(nodeValue.getNodeValue());
								} catch (NumberFormatException e) {
									// for whatever reason if an elevation can't be read just
									// indicate with null
									elevation = null;
								}
							} else {
								elevation = null;
							}
						} else {
							elevation = null;
						}
						NodeList timeElements = trackpointElement.getElementsByTagName("time");
						DateTime timestamp;
						if (timeElements.getLength() > 0) {
							timestamp = JodaTimeUtil.parseXmlDate(timeElements.item(0).getFirstChild().getNodeValue());
						} else {
							timestamp = null;
						}
						builder.trackpoint(timestamp, lat, lon, elevation);
					}
				}
			}
			built.gpx = builder.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct GpxRelaxed */
	private GpxRelaxed() {
	}

	public static Builder create() {
		return new Builder();
	}

	private SimpleGpx gpx;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getWaypoints()
	 */
	@Override
	public List<Wpt> getWaypoints() {
		return gpx.getWaypoints();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getTracks()
	 */
	@Override
	public List<Trk> getTracks() {

		return gpx.getTracks();
	}

}
