/**
 * 
 */
package com.aawhere.gpx._1._0;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.joda.time.DateTime;
import org.joda.time.JodaTimePermission;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxReader;
import com.aawhere.gpx._1._1.GpxTestUtil;
import com.aawhere.gpx._1._1.Wpt;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBReadException;
import com.aawhere.xml.bind.JibxReader;

/**
 * @author aroller
 * 
 */
public class GpxJaxb10TestUtil
		extends GpxTestUtil {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxTestUtil#generateRandomWpt()
	 */
	@Override
	public Wpt generateRandomWpt() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxTestUtil#getReader()
	 */
	@Override
	protected GpxReader getReader() {
		return new Gpx10JaxbReader();
	}

}
