/**
 * 
 */
package com.aawhere.gpx._1._0;

import static com.aawhere.gpx._1._1.GpxTestUtil.*;
import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Test;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxTestUtil;
import com.aawhere.gpx._1._1.GpxTestUtil.GpxTestDocument;
import com.aawhere.gpx._1._1.Wpt;
import com.aawhere.joda.time.DateTimeConverter;
import com.aawhere.joda.time.JodaTimeUtil;

/**
 * @author aroller
 * 
 */
public class GpxJaxb10UnitTest {

	@Test
	public void testEveryTrail() throws GpxReadException {
		GpxTestDocument testDoc = EVERY_TRAIL_1_0;
		assertGpx(testDoc);
	}

	@Test
	public void testEveryTrailMissingTime() throws GpxReadException {
		assertGpx(EVERY_TRAIL_1_0_TIME_PROB);
	}

	/**
	 * @param testUtil
	 * @param testDoc
	 * @throws GpxReadException
	 */
	private void assertGpx(GpxTestDocument testDoc) throws GpxReadException {
		GpxJaxb10TestUtil testUtil = new GpxJaxb10TestUtil();
		Gpx gpx = testUtil.getTestGpx(testDoc);
		GpxTestUtil.assertGpx(testDoc, gpx);
		Wpt wpt = gpx.getTracks().get(0).getSegments().get(0).getTrackpoints().get(0);
		assertNotNull(wpt.getDateTime());
		assertNotNull(wpt.getLatitude());
		assertNotNull(wpt.getLongitude());
		assertNotNull(wpt.getElevation());
	}

}
