/**
 * 
 */
package com.aawhere.gpx._1._0;

import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.Trk;
import com.aawhere.gpx._1._1.Trkseg;
import com.aawhere.gpx._1._1.Wpt;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.topografix.gpx._1._0.Gpx.Trk.Trkseg.Trkpt;

/**
 * @author aroller
 * 
 */
@SuppressWarnings("serial")
public class JaxbGpx
		implements Gpx {

	/**
	 * Used to construct all instances of JaxbGpx.
	 */
	public static class Builder
			extends ObjectBuilder<JaxbGpx> {

		public Builder() {
			super(new JaxbGpx());
		}

		public Builder root(com.topografix.gpx._1._0.Gpx root) {
			building.root = root;
			return this;
		}

		@Override
		public JaxbGpx build() {
			JaxbGpx built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct JaxbGpx */
	private JaxbGpx() {
	}

	public static JaxbGpx build(com.topografix.gpx._1._0.Gpx root) {
		return create().root(root).build();
	}

	public static Builder create() {
		return new Builder();
	}

	private com.topografix.gpx._1._0.Gpx root;

	/**
	 * @return the root
	 */
	public com.topografix.gpx._1._0.Gpx getRoot() {
		return this.root;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getWaypoints()
	 */
	@Override
	public List<Wpt> getWaypoints() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getTracks()
	 */
	@Override
	public List<Trk> getTracks() {
		return Lists.transform(root.getTrk(), new Function<com.topografix.gpx._1._0.Gpx.Trk, Trk>() {

			// FIXME:These inner classes aren't Serializable and can't be cached
			@Override
			public Trk apply(final com.topografix.gpx._1._0.Gpx.Trk input) {
				return new Trk() {

					@Override
					public List<Trkseg> getSegments() {
						if (input == null || input.getTrkseg() == null) {
							return Collections.emptyList();
						}
						return trackSegments(input);
					}

				};
			}
		});
	}

	/**
	 * @param input
	 * @return
	 */
	public List<Trkseg> trackSegments(final com.topografix.gpx._1._0.Gpx.Trk input) {
		return Lists.transform(input.getTrkseg(), new Function<com.topografix.gpx._1._0.Gpx.Trk.Trkseg, Trkseg>() {

			@Override
			public Trkseg apply(final com.topografix.gpx._1._0.Gpx.Trk.Trkseg input) {
				if (input == null) {
					return null;
				}
				return new Trkseg() {

					@Override
					public List<Wpt> getTrackpoints() {
						if (input == null || input.getTrkpt() == null) {
							return Collections.emptyList();
						}
						return waypoints(input);
					}

				};
			}

		});
	}

	/**
	 * @param input
	 * @return
	 */
	public List<Wpt> waypoints(final com.topografix.gpx._1._0.Gpx.Trk.Trkseg input) {
		return Lists.transform(input.getTrkpt(), new Function<Trkpt, Wpt>() {

			@Override
			public Wpt apply(final Trkpt input) {
				if (input == null) {
					return null;
				}
				return new Wpt() {

					@Override
					public Double getLongitude() {
						return input.getLon().doubleValue();
					}

					@Override
					public Double getLatitude() {
						return input.getLat().doubleValue();
					}

					@Override
					public Double getElevation() {
						if (input.getEle() == null) {
							return null;
						}
						return input.getEle().doubleValue();
					}

					@Override
					public DateTime getDateTime() {
						DateTime time = input.getTime();
						if (time == null) {
							throw new NullPointerException("time can't be nulll");
						}
						return time;
					}
				};
			}
		});
	}
}
