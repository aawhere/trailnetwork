/**
 * 
 */
package com.aawhere.gpx._1._0;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxReader;
import com.aawhere.xml.bind.JAXBReadException;
import com.aawhere.xml.bind.JAXBReader;

import com.topografix.gpx._1._0.ObjectFactory;

/**
 * @author aroller
 * 
 */
public class Gpx10JaxbReader
		implements GpxReader {

	private JAXBReader<Object, ObjectFactory> reader;

	/**
 * 
 */
	public Gpx10JaxbReader() {
		this.reader = JAXBReader.getReader(ObjectFactory.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxReader#read(java.io.InputStream)
	 */
	@Override
	public Gpx read(InputStream inputStream) throws GpxReadException {
		com.topografix.gpx._1._0.Gpx root;
		try {
			root = (com.topografix.gpx._1._0.Gpx) this.reader.read(inputStream);
		} catch (JAXBReadException e) {
			throw new GpxReadException(e);
		}
		return JaxbGpx.build(root);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxReader#write(com.aawhere.gpx._1._1.Gpx)
	 */
	@Override
	public byte[] write(Gpx gpx) {
		try {
			JaxbGpx jaxbGpx = (JaxbGpx) gpx;
			JAXBContext jc = JAXBContext.newInstance(ObjectFactory.class);
			// Create marshaller
			Marshaller m = jc.createMarshaller();
			// Marshal object into file.
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			m.marshal(jaxbGpx.getRoot(), byteStream);
			return byteStream.toByteArray();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

}
