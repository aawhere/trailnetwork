/**
 * 
 */
package com.aawhere.doc.garmin.activity.json;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.aawhere.test.category.FileTest;
import com.aawhere.test.category.UnitTest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Tests parsing Garmin Connect JSON into the generated Activity objects.
 * 
 * @author aroller
 * 
 */
@Category(UnitTest.class)
public class GarminConnectActivityJsonUnitTest {

	private final String ID_VALUE = "45751001";

	@Test
	@Category(UnitTest.class)
	public void testBasicParsing() throws JsonParseException, IOException {
		final String noActivitiesJson = "{\"results\": {\"activities\": [{\"activity\": {\"activityId\": \"" + ID_VALUE
				+ "\"}}]}}";
		ObjectMapper mapper = new ObjectMapper();
		ActivitySearch search = mapper.readValue(noActivitiesJson, ActivitySearch.class);
		assertResults(search);
	}

	/**
	 * @param search
	 */
	private void assertResults(ActivitySearch search) {
		Results results = search.getResults();
		assertNotNull(results);
		List<Activitie> activites = results.getActivities();
		assertNotNull(activites);
		assertEquals(1, activites.size());
		Activitie activitie = activites.get(0);
		Activity activity = activitie.getActivity();
		assertNotNull(activity);
		assertEquals(ID_VALUE, activity.getActivityId());
	}

	@Test
	@Category(FileTest.class)
	public void testFileParsing() throws JsonParseException, JsonMappingException, IOException {
		Resource json = new ClassPathResource("json/ActivitySearch.json");
		ObjectMapper mapper = new ObjectMapper();
		ActivitySearch activitySearch = mapper.readValue(json.getInputStream(), ActivitySearch.class);
		assertResults(activitySearch);

	}
}
