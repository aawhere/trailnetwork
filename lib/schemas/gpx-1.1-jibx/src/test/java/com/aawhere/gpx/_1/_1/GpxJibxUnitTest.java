package com.aawhere.gpx._1._1;

import org.junit.experimental.categories.Category;

import com.aawhere.test.category.UnitTest;

/**
 * 
 */

/**
 * Tests the parsing of a GPX file.
 * 
 * @author roller
 * 
 */
@Category(UnitTest.class)
public class GpxJibxUnitTest
		extends GpxUnitTest {

	/*
	 * (non-Javadoc)
	 * @see com.topografix.gpx._1._1.GpxUnitTest#setGpxUtil()
	 */
	@Override
	protected GpxJibxTestUtil getGpxUtil() {
		return new GpxJibxTestUtil();
	}

}
