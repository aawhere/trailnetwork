/**
 * 
 */
package com.aawhere.gpx._1._1;

import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.InputStream;

import org.jibx.runtime.BindingDirectory;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.IUnmarshallingContext;
import org.jibx.runtime.JiBXException;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.category.FileTest;

import com.topografix.gpx._1._1.jibx.GpxType;

/**
 * Test the Performance of GPX unmarshalling.
 * 
 * @author Brian Chapman
 * 
 */
@Category(FileTest.class)
public class GpxPerformanceTest {

	@Test
	public void testJixBUnmarshallBloated() throws JiBXException, FileNotFoundException {
		// unmarshal customer information from file
		IBindingFactory bfact = BindingDirectory.getFactory(GpxType.class);
		IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
		InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("gc_bloated_activity_reyes_188755294.gpx");
		GpxType gpxType = (GpxType) uctx.unmarshalDocument(in, null);
		assertNotNull(gpxType);
		Gpx gpx = new JibxGpx.Builder().setGpxType(gpxType).build();
		assertNotNull(gpx.getTracks());
		Trk trk = gpx.getTracks().iterator().next();
	}

}
