/**
 * 
 */
package com.aawhere.gpx._1._1;

import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxTestUtil;
import com.aawhere.gpx._1._1.JibxGpx;
import com.aawhere.gpx._1._1.JibxWpt;
import com.aawhere.gpx._1._1.Wpt;
import com.aawhere.xml.bind.JAXBReadException;
import com.aawhere.xml.bind.JibxReader;

import com.topografix.gpx._1._1.jibx.GpxType;
import com.topografix.gpx._1._1.jibx.WptType;

/**
 * Used to assist the many dependencies on GPX files.
 * 
 * @author aroller
 * 
 */
public class GpxJibxTestUtil
		extends GpxTestUtil {

	public static GpxJibxTestUtil instance() {
		return new GpxJibxTestUtil();
	}

	public Gpx loadResource(String resourcePath) throws GpxReadException {

		InputStream is = ClassLoader.getSystemResourceAsStream(resourcePath);
		assertNotNull(resourcePath + " not found", is);

		return readInputStream(is);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxTestUtil#getReader()
	 */
	@Override
	protected GpxReader getReader() {
		return new com.aawhere.gpx._1._1.JibxReader();
	}

	/**
*/

	private Gpx readInputStream(InputStream is) throws GpxReadException {
		JibxReader reader = JibxReader.getReader();
		GpxType gpxType;
		try {
			gpxType = reader.read(is, GpxType.class);
		} catch (JAXBReadException e) {
			throw new GpxReadException(e);
		}
		return new JibxGpx.Builder().setGpxType(gpxType).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.topografix.gpx._1._1.GpxTestUtil#generateRandomWptType()
	 */
	@Override
	public Wpt generateRandomWpt() {
		WptType wptType = new WptType();
		// TODO:fill in timestamp and ele.
		wptType.setLat(generateRandomLat().doubleValue());
		assertNotNull(wptType.getLat());
		wptType.setLon(generateRandomLon().doubleValue());
		assertNotNull(wptType.getLon());
		return new JibxWpt.Builder().setWptType(wptType).build();
	}

}
