/**
 * 
 */
package com.aawhere.gpx._1._1;

import org.junit.Ignore;
import org.junit.experimental.categories.Category;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxTestUtil.GpxTestDocument;
import com.aawhere.test.category.FileTest;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class GpxJibxFileTest
		extends GpxFileTest {

	/*
	 * (non-Javadoc)
	 * @see
	 * com.topografix.gpx._1._1.GpxFileTest#getGpx(com.topografix.gpx._1._1.GpxTestUtil.GpxTestDocument
	 * )
	 */
	@Override
	protected Gpx getGpx(GpxTestDocument gpx) throws GpxReadException {
		return new GpxJibxTestUtil().getTestGpx(gpx);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxFileTest#testEveryTrailGarminExtensions()
	 */
	@Override
	@Ignore("This can' read the extensions")
	public void testEveryTrailGarminExtensions() throws GpxReadException {

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxFileTest#testEveryTrailGpxTpx()
	 */
	@Override
	@Ignore("this can't read extensions")
	public void testEveryTrailGpxTpx() throws GpxReadException {

	}
}
