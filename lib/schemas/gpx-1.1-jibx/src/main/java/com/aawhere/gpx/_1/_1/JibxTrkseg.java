/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.lang.ObjectBuilder;

import com.topografix.gpx._1._1.jibx.TrksegType;
import com.topografix.gpx._1._1.jibx.WptType;

/**
 * Implementation of {@link Trk} for Jibx generated types.
 * 
 * @author Brian Chapman
 * 
 */
public class JibxTrkseg
		implements Trkseg {

	private static final long serialVersionUID = -4693085572984706141L;

	/**
	 * Used to construct all instances of JaxbTrkseg.
	 */
	public static class Builder
			extends ObjectBuilder<JibxTrkseg> {

		public Builder() {
			super(new JibxTrkseg());
		}

		public Builder setTrksegType(TrksegType trksegType) {
			building.trksegType = trksegType;
			return this;
		}

	}// end Builder

	private TrksegType trksegType;
	private List<Wpt> waypoints;

	/** Use {@link Builder} to construct JaxbTrkseg */
	private JibxTrkseg() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Trkseg#getTrackpoints()
	 */
	@Override
	public List<Wpt> getTrackpoints() {
		if (waypoints == null) {
			waypoints = new ArrayList<Wpt>();
			for (WptType wptType : trksegType.getTrkptList()) {
				waypoints.add(new JibxWpt.Builder().setWptType(wptType).build());
			}
		}
		return waypoints;
	}

}
