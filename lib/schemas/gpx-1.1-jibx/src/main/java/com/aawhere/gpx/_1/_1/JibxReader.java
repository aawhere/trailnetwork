package com.aawhere.gpx._1._1;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.jibx.runtime.BindingDirectory;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.IMarshallingContext;
import org.jibx.runtime.JiBXException;

import com.aawhere.log.LoggerFactory;
import com.aawhere.xml.bind.JAXBReadException;

import com.topografix.gpx._1._1.jibx.GpxType;

/**
 * 
 */

/**
 * Implementation of {@link GpxReader} for reading Jibx types.
 * 
 * @author Brian Chapman
 * 
 */
public class JibxReader
		implements GpxReader {

	com.aawhere.xml.bind.JibxReader reader;

	public JibxReader() {
		this.reader = com.aawhere.xml.bind.JibxReader.getReader();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxReader#read(java.io.InputStream)
	 */
	@Override
	public Gpx read(InputStream inputStream) throws GpxReadException {
		try {

			GpxType gpxType = reader.read(inputStream, GpxType.class);

			return new JibxGpx.Builder().setGpxType(gpxType).build();
		} catch (JAXBReadException e) {
			throw new GpxReadException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxReader#write(com.aawhere.gpx._1._1.Gpx)
	 */
	@Override
	public byte[] write(Gpx gpx) {
		JibxGpx jibxGpx;
		if (gpx instanceof JibxGpx) {
			jibxGpx = (JibxGpx) gpx;
		} else {
			throw new RuntimeException("Hey, I expected a JibxGpx. You have me a " + gpx.getClass());
		}

		try {
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			IBindingFactory bfact = BindingDirectory.getFactory(GpxType.class);
			IMarshallingContext mctx = bfact.createMarshallingContext();
			mctx.marshalDocument(jibxGpx.getRoot(), "UTF-8", null, byteStream);
			return byteStream.toByteArray();
		} catch (JiBXException e) {
			throw new RuntimeException(e);
		}
	}
}
