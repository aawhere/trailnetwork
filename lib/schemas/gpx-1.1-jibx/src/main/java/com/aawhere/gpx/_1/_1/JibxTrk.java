/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.lang.ObjectBuilder;

import com.topografix.gpx._1._1.jibx.TrkType;
import com.topografix.gpx._1._1.jibx.TrksegType;

/**
 * Implementation of {@link Trk} for Jibx generated types.
 * 
 * @author Brian Chapman
 * 
 */
public class JibxTrk
		implements Trk {

	private static final long serialVersionUID = -8360486429778429363L;

	/**
	 * Used to construct all instances of JaxbTrk.
	 */
	public static class Builder
			extends ObjectBuilder<JibxTrk> {

		public Builder() {
			super(new JibxTrk());
		}

		public Builder setTrkType(TrkType type) {
			building.trkType = type;
			return this;
		}

	}// end Builder

	private TrkType trkType;
	private List<Trkseg> segments;

	/** Use {@link Builder} to construct JaxbTrk */
	private JibxTrk() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Trk#getSegments()
	 */
	@Override
	public List<Trkseg> getSegments() {
		if (segments == null) {
			segments = new ArrayList<Trkseg>();
			for (TrksegType trksegType : trkType.getTrksegList()) {
				segments.add(new JibxTrkseg.Builder().setTrksegType(trksegType).build());
			}
		}
		return segments;
	}

}
