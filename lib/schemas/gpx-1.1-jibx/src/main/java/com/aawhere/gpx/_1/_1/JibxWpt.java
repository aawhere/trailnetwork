/**
 * 
 */
package com.aawhere.gpx._1._1;

import org.joda.time.DateTime;

import com.aawhere.lang.ObjectBuilder;

import com.topografix.gpx._1._1.jibx.WptType;

/**
 * Implementation of {@link Wpt} for Jibx generated types.
 * 
 * @author Brian Chapman
 * 
 */
public class JibxWpt
		implements Wpt {

	private static final long serialVersionUID = -3777096658123199187L;

	/**
	 * Used to construct all instances of JaxbWpt.
	 */
	public static class Builder
			extends ObjectBuilder<JibxWpt> {

		public Builder() {
			super(new JibxWpt());
		}

		public Builder setWptType(WptType wptType) {
			building.wptType = wptType;
			return this;
		}

	}// end Builder

	private WptType wptType;

	/** Use {@link Builder} to construct JaxbWpt */
	private JibxWpt() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Wpt#getLatitude()
	 */
	@Override
	public Double getLatitude() {
		return wptType.getLat().doubleValue();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Wpt#getLongitude()
	 */
	@Override
	public Double getLongitude() {
		return wptType.getLon().doubleValue();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Wpt#getDateTime()
	 */
	@Override
	public DateTime getDateTime() {
		return wptType.getTime();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Wpt#getElevation()
	 */
	@Override
	public Double getElevation() {
		return wptType.getEle();
	}

}
