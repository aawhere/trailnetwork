/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.lang.ObjectBuilder;

import com.topografix.gpx._1._1.jibx.GpxType;
import com.topografix.gpx._1._1.jibx.TrkType;
import com.topografix.gpx._1._1.jibx.WptType;

/**
 * Implementation of {@link Gpx} for Jibx generated types.
 * 
 * @author Brian Chapman
 * 
 */
public class JibxGpx
		implements Gpx {

	private static final long serialVersionUID = -3000386222520256772L;

	/**
	 * Used to construct all instances of JaxbGpx.
	 */
	public static class Builder
			extends ObjectBuilder<JibxGpx> {

		public Builder() {
			super(new JibxGpx());
		}

		public Builder setGpxType(GpxType gpxType) {
			building.gpxType = gpxType;
			return this;
		}

	}// end Builder

	private GpxType gpxType;
	private List<Wpt> waypoints;
	private List<Trk> tracks;

	/** Use {@link Builder} to construct JaxbGpx */
	private JibxGpx() {
	}

	GpxType getRoot() {
		return gpxType;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getWaypoint()
	 */
	@Override
	public List<Wpt> getWaypoints() {
		if (waypoints == null) {
			waypoints = new ArrayList<Wpt>();
			for (WptType wptType : gpxType.getWptList()) {
				waypoints.add(new JibxWpt.Builder().setWptType(wptType).build());
			}
		}
		return waypoints;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getTracks()
	 */
	@Override
	public List<Trk> getTracks() {
		if (tracks == null) {
			tracks = new ArrayList<Trk>();
			for (TrkType trkType : gpxType.getTrkList()) {
				tracks.add(new JibxTrk.Builder().setTrkType(trkType).build());
			}
		}
		return tracks;
	}
}
