/**
 * 
 */
package com.garmin.developer.schemas.axm;

import static org.junit.Assert.assertNotNull;

import java.io.InputStream;

import org.junit.Test;

import com.aawhere.xml.bind.JAXBReadException;
import com.aawhere.xml.bind.JAXBReader;

/**
 * @author Aaron Roller
 * 
 */
public class AxmUnitTest {

	public static final String HEART_SUMMARY_FILENAME = "activity_3006961.axm";

	public static final String ACTIVITY_NAME = "Short Neighborhood Walk";
	public static final String ACTIVITY_DESCRIPTION = "Test";
	public static final String SUMMARY_FILENAME = "activity_147737455-summary.axm";
	public static final String FULL_FILENAME = "activity_147737455-full.axm";

	public static AxmAxm axmFrom(String resourcePath) throws JAXBReadException {
		InputStream is = ClassLoader.getSystemResourceAsStream(resourcePath);
		assertNotNull(resourcePath + " not found", is);

		JAXBReader<AxmAxm, ObjectFactory> reader = JAXBReader.getReader(ObjectFactory.class);
		AxmAxm axm = reader.read(is);
		assertNotNull(axm);
		return axm;
	}

	@Test
	public void testSummary() throws JAXBReadException {

		AxmAxm axm = axmFrom(SUMMARY_FILENAME);
		assertNotNull(axm);
		assertNotNull(axm.getActivities());

	}

	@Test
	public void testFull() throws JAXBReadException {

		AxmAxm axm = axmFrom(FULL_FILENAME);
		assertNotNull(axm);

	}

	@Test
	public void testHeartSummary() throws JAXBReadException {

		AxmAxm axm = axmFrom(HEART_SUMMARY_FILENAME);
		assertNotNull(axm);

	}
}
