/**
 * 
 */
package com.aawhere.gpx._1._1;

import org.junit.Ignore;
import org.junit.experimental.categories.Category;

import com.aawhere.gpx._1._1.Gpx;
import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.gpx._1._1.GpxTestUtil.GpxTestDocument;
import com.aawhere.test.category.FileTest;

/**
 * @author aroller
 * 
 */
@Category(FileTest.class)
public class GpxJaxbFileTest
		extends GpxFileTest {

	/*
	 * (non-Javadoc)
	 * @see
	 * com.topografix.gpx._1._1.GpxFileTest#getGpx(com.topografix.gpx._1._1.GpxTestUtil.GpxTestDocument
	 * )
	 */
	@Override
	protected Gpx getGpx(GpxTestDocument gpx) throws GpxReadException {
		return new GpxJaxbTestUtil().getTestGpx(gpx);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxFileTest#testEveryTrailGpxTpx()
	 */
	@Override
	@Ignore("this is a bad file that can't be parsed by strict JAXB")
	public void testEveryTrailGpxTpx() throws GpxReadException {

	}
}
