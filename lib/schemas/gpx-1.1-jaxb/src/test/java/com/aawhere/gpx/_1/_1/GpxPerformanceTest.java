/**
 * 
 */
package com.aawhere.gpx._1._1;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.gpx._1._1.GpxReadException;
import com.aawhere.test.category.FileTest;
import com.aawhere.xml.bind.JAXBReadException;

/**
 * Test the Performance of GPX unmarshalling.
 * 
 * @author Brian Chapman
 * 
 */
@Category(FileTest.class)
public class GpxPerformanceTest {

	/**
	 * This is a duplicate test that warms up the system to test the others since the first one
	 * seems to take a lot longer.
	 * 
	 * @throws JAXBReadException
	 * 
	 */
	@Test
	public void testUnmarshallBloated() throws GpxReadException {
		new GpxJaxbTestUtil().getTestGpx(GpxJaxbTestUtil.GARMIN_CONNECT_ACTIVITY_BLOATED);
	}

}
