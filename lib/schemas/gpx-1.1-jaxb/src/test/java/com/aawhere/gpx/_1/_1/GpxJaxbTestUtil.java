/**
 * 
 */
package com.aawhere.gpx._1._1;

import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.topografix.gpx._1._1.WptType;

/**
 * Used to assist the many dependencies on GPX files.
 * 
 * @author aroller
 * 
 */
public class GpxJaxbTestUtil
		extends GpxTestUtil {

	private JaxbReader reader;

	/**
	 * @param gpxFactory
	 */
	public GpxJaxbTestUtil() {
		this.reader = new JaxbReader();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxTestUtil#generateRandomWpt()
	 */
	@Override
	public Wpt generateRandomWpt() {
		WptType wptType = new WptType();
		// TODO:fill in timestamp and ele.
		wptType.setLat(generateRandomLat());
		assertNotNull(wptType.getLat());
		wptType.setLon(generateRandomLon());
		assertNotNull(wptType.getLon());
		return new JaxbWpt.Builder().setWptType(wptType).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxTestUtil#getReader()
	 */
	@Override
	protected GpxReader getReader() {
		return new JaxbReader();
	}

}
