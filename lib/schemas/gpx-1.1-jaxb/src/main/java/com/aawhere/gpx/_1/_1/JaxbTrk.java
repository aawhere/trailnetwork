/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.lang.ObjectBuilder;

import com.topografix.gpx._1._1.TrkType;
import com.topografix.gpx._1._1.TrksegType;

/**
 * Implementation of {@link Trk} for Jaxb generated types.
 * 
 * @author Brian Chapman
 * 
 */
public class JaxbTrk
		implements Trk {

	private static final long serialVersionUID = 7837107315281826280L;

	/**
	 * Used to construct all instances of JaxbTrk.
	 */
	public static class Builder
			extends ObjectBuilder<JaxbTrk> {

		public Builder() {
			super(new JaxbTrk());
		}

		public Builder setTrkType(TrkType type) {
			building.trkType = type;
			return this;
		}

	}// end Builder

	private TrkType trkType;
	private List<Trkseg> segments;

	/** Use {@link Builder} to construct JaxbTrk */
	private JaxbTrk() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Trk#getSegments()
	 */
	@Override
	public List<Trkseg> getSegments() {
		if (segments == null) {
			segments = new ArrayList<Trkseg>();
			for (TrksegType trksegType : trkType.getTrkseg()) {
				segments.add(new JaxbTrkseg.Builder().setTrksegType(trksegType).build());
			}
		}
		return segments;
	}

}
