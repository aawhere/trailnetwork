/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.aawhere.log.LoggerFactory;
import com.aawhere.xml.bind.JAXBReadException;
import com.aawhere.xml.bind.JAXBReader;

import com.topografix.gpx._1._1.GpxType;
import com.topografix.gpx._1._1.ObjectFactory;

/**
 * @author brian
 * 
 */
public class JaxbReader
		implements GpxReader {

	JAXBReader<Object, ObjectFactory> reader;

	public JaxbReader() {
		this.reader = JAXBReader.getReader(ObjectFactory.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxReader#read(java.io.InputStream)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Gpx read(InputStream inputStream) throws GpxReadException {
		try {
			long start = System.currentTimeMillis();

			GpxType gpxType = (GpxType) reader.read(inputStream);

			long duration = System.currentTimeMillis() - start;
			if (duration > 1000) {
				LoggerFactory.getLogger(getClass()).warning(duration + "ms to unmarshal xml using Jaxb");
			}

			return new JaxbGpx.Builder().setGpxType(gpxType).build();
		} catch (JAXBReadException e) {
			throw new GpxReadException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.GpxReader#write(com.aawhere.gpx._1._1.Gpx)
	 */
	@Override
	public byte[] write(Gpx gpx) {
		JaxbGpx jaxbGpx;
		if (gpx instanceof JaxbGpx) {
			jaxbGpx = (JaxbGpx) gpx;
		} else {
			throw new RuntimeException("Hey, I expected a JaxbGpx. You have me a " + gpx.getClass());
		}
		try {

			JAXBContext jc = JAXBContext.newInstance(ObjectFactory.class);
			// Create marshaller
			Marshaller m = jc.createMarshaller();
			// Marshal object into file.
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			m.marshal(jaxbGpx.getRoot(), byteStream);
			return byteStream.toByteArray();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

}
