/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.lang.ObjectBuilder;

import com.topografix.gpx._1._1.TrksegType;
import com.topografix.gpx._1._1.WptType;

/**
 * Implementation of {@link Trk} for Jaxb generated types.
 * 
 * @author Brian Chapman
 * 
 */
public class JaxbTrkseg
		implements Trkseg {

	private static final long serialVersionUID = 1017863115767062054L;

	/**
	 * Used to construct all instances of JaxbTrkseg.
	 */
	public static class Builder
			extends ObjectBuilder<JaxbTrkseg> {

		public Builder() {
			super(new JaxbTrkseg());
		}

		public Builder setTrksegType(TrksegType trksegType) {
			building.trksegType = trksegType;
			return this;
		}

	}// end Builder

	private TrksegType trksegType;
	private List<Wpt> waypoints;

	/** Use {@link Builder} to construct JaxbTrkseg */
	private JaxbTrkseg() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Trkseg#getTrackpoints()
	 */
	@Override
	public List<Wpt> getTrackpoints() {
		if (waypoints == null) {
			waypoints = new ArrayList<Wpt>();
			for (WptType wptType : trksegType.getTrkpt()) {
				waypoints.add(new JaxbWpt.Builder().setWptType(wptType).build());
			}
		}
		return waypoints;
	}

}
