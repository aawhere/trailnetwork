/**
 * 
 */
package com.aawhere.gpx._1._1;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.lang.ObjectBuilder;

import com.topografix.gpx._1._1.GpxType;
import com.topografix.gpx._1._1.TrkType;
import com.topografix.gpx._1._1.WptType;

/**
 * Implementation of {@link Gpx} for Jaxb generated types.
 * 
 * @author Brian Chapman
 * 
 */
public class JaxbGpx
		implements Gpx {

	private static final long serialVersionUID = -7925034531212134365L;

	/**
	 * Used to construct all instances of JaxbGpx.
	 */
	public static class Builder
			extends ObjectBuilder<JaxbGpx> {

		public Builder() {
			super(new JaxbGpx());
		}

		public Builder setGpxType(GpxType gpxType) {
			building.gpxType = gpxType;
			return this;
		}

	}// end Builder

	private GpxType gpxType;
	private List<Wpt> waypoints;
	private List<Trk> tracks;

	/** Use {@link Builder} to construct JaxbGpx */
	private JaxbGpx() {
	}

	GpxType getRoot() {
		return gpxType;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getWaypoint()
	 */
	@Override
	public List<Wpt> getWaypoints() {
		if (waypoints == null) {
			waypoints = new ArrayList<Wpt>();
			for (WptType wptType : gpxType.getWpt()) {
				waypoints.add(new JaxbWpt.Builder().setWptType(wptType).build());
			}
		}
		return waypoints;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.gpx._1._1.Gpx#getTracks()
	 */
	@Override
	public List<Trk> getTracks() {
		if (tracks == null) {
			tracks = new ArrayList<Trk>();
			for (TrkType trkType : gpxType.getTrk()) {
				tracks.add(new JaxbTrk.Builder().setTrkType(trkType).build());
			}
		}
		return tracks;
	}
}
