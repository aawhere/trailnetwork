/**
 *
 */
package com.aawhere.measure.geocell;

import java.io.Serializable;

import javax.inject.Inject;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ComparatorResult;
import com.aawhere.xml.XmlNamespace;

import com.beoui.geocell.GeocellManager;

/**
 * @see GeocellManager
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class GeoCell
		implements Comparable<GeoCell>, Serializable {

	private static final long serialVersionUID = 3858137512358979356L;

	@XmlValue
	@Inject
	private String cell;

	/** please JaxB */
	@SuppressWarnings("unused")
	private GeoCell() {
	}

	/**
	 * 
	 * @param cell
	 * @throws InvalidGeoCellException
	 *             if the given cell is not of the {@link #ALPHABET}.
	 * 
	 */
	public GeoCell(String cell) throws InvalidGeoCellException {
		Assertion.exceptions().assertNotEmpty("cell", cell);
		this.cell = cell;
		// validate cell using GeoCellUtil#validate only when necessary
	}

	public String getCellAsString() {
		return cell;
	}

	public Resolution getResolution() {
		return GeoCellUtil.getResolution(cell.length());
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}

		GeoCell otherCell;
		if (other instanceof GeoCell) {
			otherCell = (GeoCell) other;
		} else {
			return false;
		}

		if (otherCell.getCellAsString().equals(this.getCellAsString())) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getCellAsString().hashCode();
	}

	@Override
	public String toString() {
		return cell;
	}

	/**
	 * Does this {@link GeoCell} fully contain the provided {@link GeoCell}?
	 * 
	 * @param geoCell
	 * @return
	 */
	public boolean contains(GeoCell geoCell) {
		GeoCell parentGeoCell = GeoCellUtil.getParentGeoCell(geoCell);
		return (this.equals(parentGeoCell));
	}

	/**
	 * @See {@link GeoCellUtil#hasParentGeoCell(GeoCell)}
	 * @return
	 */
	public boolean hasParent() {
		return GeoCellUtil.hasParentGeoCell(this);
	}

	/**
	 * True if this {@link GeoCell} is the direct parent of the supplied {@link GeoCell}.
	 * 
	 * @param geoCell
	 * @return
	 */
	public boolean isParent(GeoCell geoCell) {
		boolean result = geoCell.hasParent();
		if (result) {
			GeoCell parentGeoCell = GeoCellUtil.getParentGeoCell(geoCell);
			result = this.equals(parentGeoCell);
		}
		return result;
	}

	/**
	 * True if this {@link GeoCell} is the direct parent or ancestor of the supplied {@link GeoCell}
	 * . For example c is the parent of c1 and the ancestor of c12
	 * 
	 * @param geoCell
	 * @return
	 */
	public boolean isAncestor(GeoCell geoCell) {
		GeoCell cell = geoCell;
		Boolean result = false;
		while (cell.hasParent()) {
			if (this.isParent(cell)) {
				result = true;
				break;
			} else {
				cell = GeoCellUtil.getParentGeoCell(cell);
			}
		}
		return result;
	}

	/**
	 * Natural sorting order is for organization purposes. This sorts alphabetically for convenience
	 * and does not consider the actual geographic size, which is better left to the
	 * {@link GeoCellBoundingBox}.
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(GeoCell o) {
		if (o != null) {
			return this.cell.compareTo(o.cell);
		} else {
			// push nulls to the end of the list
			return ComparatorResult.LESS_THAN.getValue();
		}
	}
}
