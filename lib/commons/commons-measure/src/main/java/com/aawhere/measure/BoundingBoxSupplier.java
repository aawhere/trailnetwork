/**
 * 
 */
package com.aawhere.measure;

import com.google.common.base.Supplier;

/**
 * @author aroller
 * 
 */
public interface BoundingBoxSupplier
		extends Supplier<BoundingBox> {

	Boolean isValid();

	BoundingBox get();
}
