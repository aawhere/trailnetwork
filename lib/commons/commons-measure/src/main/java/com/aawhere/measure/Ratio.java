/**
 * 
 */
package com.aawhere.measure;

import javax.measure.quantity.Quantity;

/**
 * The fractional representation of a whole. You know, percentage kindof sortof.
 * 
 * @author roller
 * 
 */
public interface Ratio
		extends Quantity<Ratio>, Comparable<Ratio> {

}
