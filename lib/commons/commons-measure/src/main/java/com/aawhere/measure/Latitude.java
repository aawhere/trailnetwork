/**
 *
 */
package com.aawhere.measure;

/**
 * The y for Earth. Supports only WGS84 projection.
 * 
 * @author roller
 * 
 */
public interface Latitude
		extends GeoQuantity<Latitude> {
	public static final Double MAX = 90.0;
	public static final Double MIN = -90.0;
}
