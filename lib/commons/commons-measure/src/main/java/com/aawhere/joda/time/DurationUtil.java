/**
 *
 */
package com.aawhere.joda.time;

import javax.annotation.Nullable;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.ReadableDuration;

import com.google.common.base.Function;

/**
 * Quick way to provide common functions to work with the JODA Time library.
 * 
 * @see Duration
 * 
 * @author Aaron Roller on Apr 8, 2011
 * 
 */
public class DurationUtil {

	public static final Duration DURATION_ZERO = new Duration(0);
	public static final Duration ONE_MINUTE = Duration.standardMinutes(1);
	public static final Duration ONE_HOUR = Duration.standardHours(1);

	/**
	 * Needed because {@link Duration#getStandardSeconds()} concatenates and this will maintain
	 * decimal seconds representing possible milliseconds.
	 * 
	 * @param duration
	 * @return
	 */
	public static double secondsFromDuration(Duration duration) {
		return duration.getMillis() / 1000.0;
	}

	/**
	 * Simple constructor with a validation check to report properly.
	 * 
	 * @param first
	 * @param second
	 * @return throws InvalidIntervalRuntimeException when a negative duration is found.
	 */
	public static Interval interval(DateTime first, DateTime second) {
		// ideally, a filter was run prior to here so we could avoid this
		// problem.
		if (first.isAfter(second) || first.equals(second)) {
			throw new InvalidIntervalRuntimeException(first, second);
		}
		return new Interval(first, second);
	}

	/**
	 * Given the numberOfMinutes this will return the appropriate duration so you don't have to do
	 * the math.
	 * 
	 * @param numberOfMinutes
	 * @return
	 */
	public static Duration durationFromMinutes(Integer numberOfMinutes) {
		return new Duration(numberOfMinutes * 60 * 1000);
	}

	/** Provides the millis from a duration. */
	public static Function<ReadableDuration, Long> millisFunction() {
		return new Function<ReadableDuration, Long>() {
			@Override
			public Long apply(ReadableDuration input) {
				return (input == null) ? null : input.getMillis();

			}
		};
	}

	/** creates a duration with the given millis */
	public static Function<Long, ? extends ReadableDuration> durationFromMillisFunction() {
		return new Function<Long, Duration>() {
			@Override
			public Duration apply(Long input) {
				return (input == null) ? null : new Duration(input);
			}
		};
	}

	public static <D extends ReadableDuration> D max(@Nullable D left, @Nullable D right) {
		return (left != null && left.isLongerThan(right)) ? left : right;
	}

	public static <D extends ReadableDuration> D min(@Nullable D left, @Nullable D right) {
		return (left != null && left.isShorterThan(right)) ? left : right;
	}

}
