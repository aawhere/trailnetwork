/**
 * 
 */
package com.aawhere.measure.calc;

import javax.measure.quantity.Length;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.MeasurementUtil;

/**
 * Determines the length of one unknown side of a right triangle when two others are known.
 * 
 * Terminology is dependent upon definitions and equations identified at <a
 * href="http://en.wikipedia.org/wiki/Pythagorean_theorem">wikipedia</a>.
 * 
 * @author Aaron Roller on Apr 6, 2011
 * 
 */
public class PythagoreanTheorum {

	/**
	 * Used to construct all instances of PythagoreanTheorum.
	 */
	public static class Builder
			extends ObjectBuilder<PythagoreanTheorum> {

		public Builder() {
			super(new PythagoreanTheorum());
		}

		public Builder setA(Length a) {
			building.a = a;
			return this;
		}

		public Builder setA(ElevationChange a) {
			building.a = MeasurementUtil.createLength(a);
			return this;
		}

		public Builder setB(Length b) {
			building.b = b;
			return this;
		}

		public Builder setC(Length c) {
			building.c = c;
			return this;
		}

		@Override
		protected void validate() {
			if (building.c == null) {
				Assertion.assertNotNull("a", building.a);
				Assertion.assertNotNull("b", building.b);
			} else {
				Assertion.exceptions().assertTrue("a or b must be provided", building.a != null || building.b != null);
			}

			super.validate();
		}

		@Override
		public PythagoreanTheorum build() {

			PythagoreanTheorum result = super.build();
			if (result.c == null) {
				result.c = new QuantityMath<Length>(result.a).squared()
						.plus(new QuantityMath<Length>(result.b).squared().getQuantity()).squareRoot().getQuantity();
			} else {
				Length known = (result.a == null) ? result.b : result.a;

				Length unknown = QuantityMath.create(result.c).squared()
						.minus(QuantityMath.create(known).squared().getQuantity()).squareRoot().getQuantity();
				if (result.b == null) {
					result.b = unknown;
				} else {
					result.a = unknown;
				}
			}
			return result;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct PythagoreanTheorum */
	private PythagoreanTheorum() {
	}

	private Length a;
	private Length b;
	private Length c;

	public Length getA() {
		return a;
	}

	public Length getB() {
		return b;
	}

	public Length getC() {
		return c;
	}

	/**
	 * The long side opposite the right angle. Analogous to {@link #getC()}.
	 * */
	public Length getHypotenuse() {
		return getC();
	}
}
