/**
 *
 */
package com.aawhere.measure;

import javax.measure.quantity.Acceleration;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.quantity.QuantityFactory;
import javax.measure.quantity.Velocity;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.Unit;

import com.aawhere.measure.unit.ExtraUnits;

/**
 * Provides access to custom {@link Quantity} interfaces and concrete implementations using
 * {@link Measurement}. This improves performance and portability since these are seriazable and do
 * not use dynamic strings to create stub objects.
 * 
 * @author Aaron Roller
 * 
 */
abstract public class MeasurementQuantityFactory<Q extends Quantity<Q>>
		extends QuantityFactory<Q> {

	static {
		setInstance(Elevation.class, new MeasurementQuantityFactory<Elevation>(Elevation.class, ExtraUnits.METER_MSL) {
			@Override
			public Elevation create(Number value, Unit<Elevation> unit) {

				return new DefaultElevation(value, unit);
			}
		});

		setInstance(ElevationChange.class, new MeasurementQuantityFactory<ElevationChange>(ElevationChange.class,
				ExtraUnits.METER_CHANGE) {
			@Override
			public ElevationChange create(Number value, Unit<ElevationChange> unit) {

				return new DefaultElevationChange(value, unit);
			}
		});

		setInstance(Length.class, new MeasurementQuantityFactory<Length>(Length.class, MetricSystem.METRE) {
			@Override
			public Length create(Number value, Unit<Length> unit) {

				return new DefaultLength(value, unit);
			}
		});
		setInstance(Velocity.class, new MeasurementQuantityFactory<Velocity>(Velocity.class,
				MetricSystem.METRES_PER_SECOND) {
			@Override
			public Velocity create(Number value, Unit<Velocity> unit) {

				return new DefaultVelocity(value, unit);
			}
		});
		setInstance(Acceleration.class, new MeasurementQuantityFactory<Acceleration>(Acceleration.class,
				MetricSystem.METRES_PER_SQUARE_SECOND) {
			@Override
			public Acceleration create(Number value, Unit<Acceleration> unit) {
				return new DefaultAcceleration(value, unit);
			}
		});

		setInstance(Angle.class, new MeasurementQuantityFactory<Angle>(Angle.class, MetricSystem.RADIAN) {
			@Override
			public Angle create(Number value, Unit<Angle> unit) {

				return new DefaultAngle(value, unit);
			}
		});
		setInstance(Latitude.class, new MeasurementQuantityFactory<Latitude>(Latitude.class,
				ExtraUnits.DECIMAL_DEGREES_LATITUDE) {
			@Override
			public Latitude create(Number value, Unit<Latitude> unit) {

				return new DefaultLatitude(value, unit);
			}
		});
		setInstance(Longitude.class, new MeasurementQuantityFactory<Longitude>(Longitude.class,
				ExtraUnits.DECIMAL_DEGREES_LONGITUDE) {
			@Override
			public Longitude create(Number value, Unit<Longitude> unit) {

				return new DefaultLongitude(value, unit);
			}
		});
		setInstance(Ratio.class, new MeasurementQuantityFactory<Ratio>(Ratio.class, ExtraUnits.RATIO) {
			@Override
			public Ratio create(Number value, Unit<Ratio> unit) {

				return new DefaultRatio(value, unit);
			}
		});
		setInstance(Count.class, new MeasurementQuantityFactory<Count>(Count.class, ExtraUnits.COUNT) {
			@Override
			public Count create(Number value, Unit<Count> unit) {

				return new DefaultCount(value, unit);
			}
		});
	}

	/**
	 * Used to ensure the measurements are registered with the general {@link QuantityFactory} so
	 * all will receive.
	 * 
	 * Failure to register results in simply receiving the poorly performing proxies while the
	 * function is essentially the same.
	 * 
	 * This actually does nothing, but just calling this ensures the static initializer is executed
	 * during the first Class loading.
	 * 
	 */
	public static void initialize() {
		// stub just to make sure static inializer is called.
		// TODO:consider Guice to help out.
	}

	public static <Q extends Quantity<Q>> QuantityFactory<Q> getInstance(Class<Q> type) {
		return QuantityFactory.getInstance(type);
	}

	private Class<Q> type;
	private Unit<Q> metricUnit;

	/**
	 * @param class1
	 * @param meterMsl
	 */
	public MeasurementQuantityFactory(Class<Q> class1, Unit<Q> metricUnit) {
		this.type = class1;
		this.metricUnit = metricUnit;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.measure.quantity.QuantityFactory#getMetricUnit()
	 */
	@Override
	public Unit<Q> getMetricUnit() {
		return this.metricUnit;
	}

	static class DefaultElevation
			extends AbstractQuantity<Elevation>
			implements Elevation {
		private static final long serialVersionUID = 6665535291193227291L;

		public DefaultElevation(Number value, Unit<Elevation> unit) {
			super(value, unit);
		}

	}

	static class DefaultElevationChange
			extends AbstractQuantity<ElevationChange>
			implements ElevationChange {
		private static final long serialVersionUID = 7614164989244163045L;

		public DefaultElevationChange(Number value, Unit<ElevationChange> unit) {
			super(value, unit);
		}

	}

	static class DefaultLength
			extends AbstractQuantity<Length>
			implements Length {
		private static final long serialVersionUID = 434058066104531320L;

		public DefaultLength(Number value, Unit<Length> unit) {
			super(value, unit);
		}

	}

	static class DefaultAcceleration
			extends AbstractQuantity<Acceleration>
			implements Acceleration {
		private static final long serialVersionUID = -2735021744599416217L;

		public DefaultAcceleration(Number value, Unit<Acceleration> unit) {
			super(value, unit);
		}
	}

	static class DefaultCount
			extends AbstractQuantity<Count>
			implements Count {

		/**
		 * @param value
		 * @param unit
		 */
		public DefaultCount(Number value, Unit<Count> unit) {
			super(value, unit);
		}

		private static final long serialVersionUID = -2305397653154752331L;

	}

	static class DefaultVelocity
			extends AbstractQuantity<Velocity>
			implements Velocity {
		private static final long serialVersionUID = 434058066104531320L;

		public DefaultVelocity(Number value, Unit<Velocity> unit) {
			super(value, unit);
		}

	}

	static class DefaultAngle
			extends AbstractQuantity<Angle>
			implements Angle {
		private static final long serialVersionUID = 43458066104531320L;

		public DefaultAngle(Number value, Unit<Angle> unit) {
			super(value, unit);
		}

	}

	static class DefaultLatitude
			extends AbstractQuantity<Latitude>
			implements Latitude {
		private static final long serialVersionUID = 4690159770202906642L;

		public DefaultLatitude(Number value, Unit<Latitude> unit) {
			super(value, unit);
		}

		@Override
		public double getInDecimalDegrees() {
			return doubleValue(ExtraUnits.DECIMAL_DEGREES_LATITUDE);
		}

		@Override
		protected int getToStringPrecision() {
			return GeoCoordinate.ONE_MM_PRECISION;
		}

	}

	static class DefaultLongitude
			extends AbstractQuantity<Longitude>
			implements Longitude {
		private static final long serialVersionUID = 2096206815718652962L;

		public DefaultLongitude(Number value, Unit<Longitude> unit) {
			super(value, unit);
		}

		@Override
		public double getInDecimalDegrees() {
			return doubleValue(ExtraUnits.DECIMAL_DEGREES_LONGITUDE);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.AbstractQuantity#getToStringPrecision()
		 */
		@Override
		protected int getToStringPrecision() {
			return GeoCoordinate.ONE_MM_PRECISION;
		}

	}

	static class DefaultRatio
			extends AbstractQuantity<Ratio>
			implements Ratio {
		private static final long serialVersionUID = 7562963099318330215L;

		public DefaultRatio(Number value, Unit<Ratio> unit) {
			super(value, unit);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.AbstractQuantity#getToStringPrecision()
		 */
		@Override
		protected int getToStringPrecision() {
			return 4;
		}
	}
}
