/**
 *
 */
package com.aawhere.measure;

import com.aawhere.field.type.DataTypeAdapter;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.exception.BaseException;

/**
 * {@link DataTypeAdapter} for {@link GeoCoordinate} to {@link FieldDataType#GEO}
 * 
 * @author Brian Chapman
 * 
 */
public class GeoCoordinateDataTypeGeoAdapter
		extends DataTypeAdapter<GeoCoordinate> {

	private static final String SEPERATOR = ",";
	private QuantityDataTypeNumberAdapter quantityAdapter = new QuantityDataTypeNumberAdapter();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldData marshal(Object adaptee) {
		if (adaptee instanceof GeoCoordinate) {
			GeoCoordinate geoCoordinate = (GeoCoordinate) adaptee;
			FieldData latitude = quantityAdapter.marshal(geoCoordinate.getLatitude());
			FieldData longitude = quantityAdapter.marshal(geoCoordinate.getLongitude());

			StringBuilder sb = new StringBuilder();
			sb.append(latitude.getValue());
			sb.append(SEPERATOR);
			sb.append(longitude.getValue());
			return new FieldData.Builder().setType(getFieldDataType()).setValue(sb.toString()).build();
		} else {
			throw new IllegalArgumentException("expected GeoCoordinate.class, got " + adaptee.getClass());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#unmarshal(java.lang.Object, java.lang.Class)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public <A extends GeoCoordinate> A unmarshal(FieldData adapted, Class<A> type) throws BaseException {
		if (adapted.getValue() instanceof String) {
			return (A) new GeoCoordinate.Builder().setAsString((String) adapted.getValue()).build();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@Override
	public Class<GeoCoordinate> getType() {
		return GeoCoordinate.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.DataTypeAdapter#getFieldDataType()
	 */
	@Override
	public FieldDataType getFieldDataType() {
		return FieldDataType.GEO;
	}

}
