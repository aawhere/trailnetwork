/**
 * 
 */
package com.aawhere.measure;

import javax.measure.quantity.Acceleration;
import javax.measure.quantity.Quantity;

/**
 * Static class providing commonly used constants in their appropriate {@link Quantity}.
 * 
 * @author Aaron Roller on Apr 8, 2011
 * 
 */
public class QuantityConstants {

	public static final Acceleration EARTH_GRAVITY_SEA_LEVEL = MeasurementUtil.createAccelerationInMpss(-9.80665);
}
