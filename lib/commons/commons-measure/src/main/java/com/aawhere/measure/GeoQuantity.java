/**
 *
 */
package com.aawhere.measure;

import javax.measure.quantity.Quantity;

/**
 * Base Quantity for measurements of the earth.
 * 
 * @author Aaron Roller
 * 
 */
public interface GeoQuantity<Q extends Quantity<Q>>
		extends Quantity<Q> {

	public static final Integer DD_MAX_FRACTION_DIGITS_1_MM = 8;
	public static final Integer DD_MAX_FRACTION_DIGITS_1_CM = 7;
	public static final Integer DD_MAX_FRACTION_DIGITS_1_DM = 6;
	/**
	 * This is declared here to help {@link GeoCoordinate}s from using useless information. See
	 * http://en.wikipedia.org/wiki/Decimal_degrees#Accuracy
	 * 
	 * A value of 7 makes it good to 1.11 cm at the equator.
	 * 
	 * Although GPS is accurate only to the meter at best, cumulative loss should be considered when
	 * choosing a value.
	 */
	public static final Integer DD_MAX_FRACTION_DIGITS_DEFAULT = DD_MAX_FRACTION_DIGITS_1_CM;

	/** A common method to retrieve the most used unit */
	public double getInDecimalDegrees();

}
