/**
 *
 */
package com.aawhere.measure.unit;

import static javax.measure.unit.MetricSystem.*;
import static javax.measure.unit.USCustomarySystem.*;

import java.util.HashSet;
import java.util.Set;

import javax.measure.quantity.Velocity;
import javax.measure.unit.BaseUnit;
import javax.measure.unit.SystemOfUnits;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;

import com.aawhere.measure.Count;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.GeoAngle;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.measure.Ratio;

/**
 * @author roller
 * 
 */
public class ExtraUnits
		extends SystemOfUnits {

	/**
	 *
	 */
	public static final String RATIO_SYMBOL = "ratio";
	public static final String PERCENT_SYMBOL = "%";
	public static final String COUNT_SYMBOL = "count";

	/**
	 *
	 */
	public static final String DEGREES_SYMBOL = "°";

	private static final Set<Unit<?>> units = new HashSet<Unit<?>>();

	/*
	 * (non-Javadoc)
	 * @see javax.measure.unit.SystemOfUnits#getUnits()
	 */
	@Override
	public Set<Unit<?>> getUnits() {

		return units;
	}

	/**
	 * Adds a new unit to the collection.
	 * 
	 * @param unit
	 *            the unit being added.
	 * @return <code>unit</code>.
	 */
	private static <U extends Unit<?>> U addUnit(U unit) {
		units.add(unit);
		return unit;
	}

	public static final Unit<Velocity> KILOMETERS_PER_HOUR = addUnit(KILO(METRE).divide(HOUR)).asType(Velocity.class);

	/**
	 * Meters above mean sea level http://en.wikipedia.org/wiki/Altitude
	 * */
	public static final Unit<Elevation> METER_MSL = addUnit(METRE).asType(Elevation.class);
	public static final Unit<Elevation> FOOT_MSL = addUnit(USCustomarySystem.FOOT).asType(Elevation.class);

	public static final Unit<ElevationChange> METER_CHANGE = addUnit(METRE).asType(ElevationChange.class);
	public static final Unit<ElevationChange> FOOT_CHANGE = addUnit(USCustomarySystem.FOOT)
			.asType(ElevationChange.class);

	public static final BaseUnit<GeoAngle> DECIMAL_DEGREES = addUnit(new BaseUnit<GeoAngle>(DEGREES_SYMBOL));

	/*
	 * This is not the same Radians found in {@link MetricSystem}, though it is the same radians in
	 * real life. Radians are not the base unit for {@link GeoAngle}s.
	 */
	public static final Unit<GeoAngle> GEO_RADIAN = addUnit(ExtraUnits.DECIMAL_DEGREES.multiply(180 / Math.PI));

	public static final Unit<Latitude> DECIMAL_DEGREES_LATITUDE = addUnit(ExtraUnits.DECIMAL_DEGREES)
			.asType(Latitude.class);
	public static final Unit<Longitude> DECIMAL_DEGREES_LONGITUDE = addUnit(ExtraUnits.DECIMAL_DEGREES)
			.asType(Longitude.class);

	public static final Unit<Latitude> RADIAN_LATITUDE = addUnit(ExtraUnits.GEO_RADIAN).asType(Latitude.class);
	public static final Unit<Longitude> RADIAN_LONGITUDE = addUnit(ExtraUnits.GEO_RADIAN).asType(Longitude.class);

	public static final Unit<Ratio> RATIO = addUnit(new BaseUnit<Ratio>(RATIO_SYMBOL)).asType(Ratio.class);

	public static final Unit<Ratio> PERCENT = RATIO.divide(100).asType(Ratio.class);

	/**
	 * The standard unit for {@link Count}. Size was mustered up from
	 * http://en.wikipedia.org/wiki/Counting_measure.
	 */
	public static final Unit<Count> COUNT = addUnit(new BaseUnit<Count>(COUNT_SYMBOL)).asType(Count.class);

	static {
		// THIS MUST REMAIN AFTER STATIC declarations
		// initializer to make sure factory is setup properly after Units are created.
		MeasurementQuantityFactory.initialize();
	}

}
