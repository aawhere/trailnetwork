/**
 * 
 */
package com.aawhere.measure.geocell;

import javax.annotation.Nullable;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class GeoCellToStringFunction
		implements Function<GeoCell, String> {

	/**
	 * Used to construct all instances of GeoCellToStringFunction.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCellToStringFunction> {

		public Builder() {
			super(new GeoCellToStringFunction());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct GeoCellToStringFunction */
	private GeoCellToStringFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public String apply(@Nullable GeoCell cell) {
		return cell.getCellAsString();
	}

}
