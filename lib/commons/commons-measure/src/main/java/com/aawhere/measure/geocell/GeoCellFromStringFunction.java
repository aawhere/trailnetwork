/**
 * 
 */
package com.aawhere.measure.geocell;

import javax.annotation.Nullable;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class GeoCellFromStringFunction
		implements Function<String, GeoCell> {

	/**
	 * Used to construct all instances of GeoCellFromStringFunction.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCellFromStringFunction> {

		public Builder() {
			super(new GeoCellFromStringFunction());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct GeoCellFromStringFunction */
	private GeoCellFromStringFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public GeoCell apply(@Nullable String string) {
		return new GeoCell(string);

	}

}
