/**
 * 
 */
package com.aawhere.measure.geocell;

import static com.aawhere.measure.geocell.GeoCellUtil.*;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;

import com.google.common.collect.Sets;

/**
 * A stateful utility specializing in reducing {@link GeoCells} to fit the desired state (size,
 * minimize, resolution, remove parents). This reduces size by generalizing more specific high
 * resolution cells with more general lower resolution cells (parents). The reducer can only ever
 * make cells more general and never more specific, but often it can stay at the given resolution.
 * 
 * Static functions in {@link GeoCellUtil} cause inefficiencies during processing so this is meant
 * to improve efficiency while utizling static methods when appropriate.
 * 
 * TODO: Support other useful reductions like resolution, minimize, etc.
 * 
 * @author aroller
 * 
 */
public class GeoCellsReducer {

	/**
	 * Used to construct all instances of GeoCellReducer.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCellsReducer> {

		private Integer maxSize;
		private Boolean minimize;
		private Boolean removeParents;
		private Boolean singleResolution;

		/**
		 * Provides a limit for the geocells not to exceed when reducing size and decreasing
		 * resolution. If this is exceeded then the geocells closest to the bounding box centerpoint
		 * will be returned up to the limit.
		 * 
		 * @param boundingBox
		 * @return
		 */
		private BoundingBox boundary;

		/**
		 * The restricted resolution. This is different than {@link GeoCellsReducer#maxResolution}
		 * since that is an observation of what is the current max and this is the restriction of
		 * what is allowed.
		 */
		private Resolution maxAllowedResolution;

		public Builder() {
			super(new GeoCellsReducer());
		}

		public Builder setGeoCells(GeoCells geoCells) {
			return setGeoCells(geoCells.getAll());
		}

		public Builder setGeoCells(Set<GeoCell> geoCells) {
			building.geoCells = geoCells;
			return this;
		}

		/**
		 * 
		 * Reduce the {@link GeoCells} to the minimum number of geocells that will still match the -
		 * * same area represented by the original GeoCells.
		 * 
		 * @param minimize
		 *            the minimize to set
		 * @return
		 */
		@Deprecated
		public Builder setMinimize(Boolean minimize) {
			this.minimize = minimize;
			return this;
		}

		/**
		 * Reduce the {@link GeoCells} to the minimum number of geocells that will still match the -
		 * same area represented by the original GeoCells.
		 * 
		 * @return
		 */
		public Builder minimize() {
			this.minimize = true;
			return this;

		}

		/**
		 * Remove parents from the result.
		 * 
		 * @param removeParents
		 * 
		 * @return
		 */
		public Builder removeParents() {
			this.removeParents = true;
			return this;
		}

		/**
		 * Indicates that the results should be of a single resolution (excluding parents) This
		 * avoids heterogenous mixture of resolutions introduced by the minimize function that will
		 * take 16 high resolution cells and replace with a single to represent the exact same area.
		 * 
		 * This currently only works with max size function which is why you must provide it. If you
		 * wish to have homogenous resolution without care of size then use the
		 * {@link GeoCellsExploder}.
		 * 
		 * @return
		 */
		public Builder singleResolutionIgnoringParents(Integer maxSize) {
			this.singleResolution = Boolean.TRUE;
			this.setMaxSize(maxSize);
			return this;
		}

		/**
		 * Remove parents from the result.
		 * 
		 * @param removeParents
		 *            the removeParents to set
		 * @return
		 */
		@Deprecated
		public Builder setRemoveParents(Boolean removeParents) {
			this.removeParents = removeParents;
			return this;
		}

		public Builder setMaxResolution(Resolution maxResolution) {
			this.maxAllowedResolution = maxResolution;
			return this;
		}

		/**
		 * @param maxSize
		 *            the maxSize to set
		 */
		public Builder setMaxSize(Integer maxSize) {
			this.maxSize = maxSize;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.exceptions().notNull("geoCells", building.geoCells);

		}

		@Override
		public GeoCellsReducer build() {
			GeoCellsReducer built = super.build();
			final Set<GeoCell> original = built.geoCells;

			reduce(built, original);

			return built;
		}

		/**
		 * @param built
		 * @param original
		 */
		private void reduce(GeoCellsReducer built, final Set<GeoCell> original) {
			// avoid infinite loop looking for an unsolvable situation.
			if (!original.isEmpty()) {

				// this will recursively repeat until the cells include no higher than the desired
				// resolution.
				if (this.maxAllowedResolution != null) {
					built.geoCells = removeCellsWithResolutionsHigherThan(original, this.maxAllowedResolution);
					// an empty set isn't desirable so add direct parents and try again
					if (built.geoCells.isEmpty()) {
						reduce(built, withParents(original).getAll());
					} else {
						// we've reached our max so let's not try any more
						this.maxAllowedResolution = null;
					}
				}

				if (BooleanUtils.isTrue(removeParents)) {
					built.geoCells = GeoCellUtil.withOutParents(built.geoCells).getAll();
				}

				if (BooleanUtils.isTrue(minimize)) {
					built.geoCells = minimize(built.geoCells);
				}
				if (maxSize != null) {
					if (built.geoCells.size() > maxSize) {
						if (BooleanUtils.isTrue(singleResolution)) {

							while (built.geoCells.size() > maxSize && !built.getMaxResolution().equals(Resolution.ONE)) {
								built.maxResolution = built.getMaxResolution().lower();
								// notice the original is used here since parents may have been
								// removed
								built.geoCells = removeCellsWithResolutionsHigherThan(	original,
																						built.getMaxResolution());
								// just reduced a resolution, but run through all filters again on
								// the new set.
								reduce(built, original);
							}
						} else {

							if (this.boundary != null) {
								List<GeoCell> closestToPoint = GeoCellUtil.closestToPoint(	this.boundary.getCenter(),
																							built.geoCells);
								// this will choose those that are closest.
								built.geoCells = Sets.newTreeSet(closestToPoint.subList(0, maxSize));
							} else {
								// This reduction will allow mixed resolutions, which is ideal for
								// area
								// queries.
								built.geoCells = reduceToSize(built.geoCells, maxSize);
							}
						}
					}
				}
			}
		}

		/**
		 * returns a new {@link Set} containing the {@link GeoCell}s from the input set minus those
		 * where the supplied {@link GeoCell} is a parent.
		 * 
		 * @param geoCell
		 * @param geoCells
		 * @return
		 */
		private Set<GeoCell> substituteGeoCell(GeoCell geoCell, Set<GeoCell> geoCells) {
			Set<GeoCell> result = new HashSet<GeoCell>();
			result.add(geoCell);
			for (GeoCell cell : geoCells) {
				if (!geoCell.isParent(cell)) {
					result.add(cell);
				}
			}
			return result;
		}

		private TreeMap<GeoCell, Integer> getGeoCellCount(Set<GeoCell> cells) {
			TreeMap<GeoCell, Integer> geoCellCount = new TreeMap<GeoCell, Integer>();
			for (GeoCell geoCell : cells) {
				if (geoCell.getResolution() != Resolution.ONE) {
					GeoCell parentGeoCell = GeoCellUtil.getParentGeoCell(geoCell);
					Integer count = geoCellCount.containsKey(parentGeoCell) ? geoCellCount.get(parentGeoCell) + 1 : 1;
					geoCellCount.put(parentGeoCell, count);
				}
			}
			return geoCellCount;
		}

		private Set<GeoCell> minimize(Set<GeoCell> geoCells) {
			Set<GeoCell> result = new HashSet<>(geoCells);
			Map<GeoCell, Integer> geoCellCount = getGeoCellCount(geoCells);

			for (Entry<GeoCell, Integer> entry : geoCellCount.entrySet()) {
				if (GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL == entry.getValue()) {
					result = substituteGeoCell(entry.getKey(), geoCells);
				}
			}

			// We may need to run through again since we only looked at immediate parents.
			if (result.size() != geoCells.size()) {
				result = minimize(result);
			}
			return result;
		}

		private Set<GeoCell> reduceToSize(Set<GeoCell> geoCells, Integer size) {
			Resolution maxResolution = GeoCellUtil.maxResolution(geoCells);

			while (maxSize < geoCells.size() && maxResolution != Resolution.ONE) {
				TreeMap<GeoCell, Integer> geoCellCount = getGeoCellCount(GeoCellUtil.withParents(geoCells).getAll());
				for (GeoCell cell : geoCellCount.descendingKeySet()) {
					if (maxSize < geoCells.size()) {
						geoCells = substituteGeoCell(cell, geoCells);
					} else {
						break;
					}
				}
				maxResolution = maxResolution.lower();
			}
			return geoCells;
		}

		/**
		 * @see #boundary
		 * @param boundingBox
		 * @return
		 */
		public Builder proximityBoundary(BoundingBox boundingBox) {
			if (boundingBox != null) {
				// bounding boxes must be near square to behave properly for this expansion to work
				// properly
				// if you don't like this then use geocells directly
				this.boundary = BoundingBoxUtil.expandSquare(boundingBox);
			}
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private Set<GeoCell> geoCells;
	private Resolution maxResolution;

	/** Use {@link Builder} to construct GeoCellReducer */
	private GeoCellsReducer() {
	}

	/**
	 * The maximum resolution provided in the {@link #geoCells}.
	 * 
	 * @return the maxResolution
	 */
	public Resolution getMaxResolution() {
		if (this.maxResolution == null) {
			this.maxResolution = maxResolution(geoCells);
		}
		return this.maxResolution;
	}

	/**
	 * @return the geoCells
	 */
	public Set<GeoCell> getGeoCells() {
		return this.geoCells;
	}

	/**
	 * @return
	 */
	public GeoCells getReduced() {
		return GeoCells.create().setAll(new TreeSet<GeoCell>(this.geoCells)).setMaxResolution(getMaxResolution())
				.build();
	}
}
