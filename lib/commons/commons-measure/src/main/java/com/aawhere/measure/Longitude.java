/**
 *
 */
package com.aawhere.measure;

/**
 * The x for earth.
 * 
 * Supports only WGS84 projection.
 * 
 * @author roller
 * 
 */
public interface Longitude
		extends GeoQuantity<Longitude> {
	public static final Double MIN = -180.0;
	public static final Double MAX = 180.0;
}
