/**
 *
 */
package com.aawhere.measure.geocell;

import javax.measure.quantity.Quantity;

import com.aawhere.field.type.DataTypeAdapter;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.exception.BaseException;

/**
 * {@link DataTypeAdapter} for {@link Quantity}
 * 
 * @author Brian Chapman
 * 
 */
public class GeoCellDataTypeTextAdapter
		extends DataTypeAdapter<GeoCell> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldData marshal(Object adaptee) {
		if (adaptee instanceof GeoCell) {
			GeoCell value = (GeoCell) adaptee;
			return new FieldData.Builder().setType(getFieldDataType()).setValue(value.toString()).build();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#unmarshal(java.lang.Object, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends GeoCell> A unmarshal(FieldData adapted, Class<A> type) throws BaseException {
		if (adapted.getValue() instanceof String) {
			String value = ((String) adapted.getValue());
			return (A) new GeoCell(value);
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@Override
	public Class<GeoCell> getType() {
		return GeoCell.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.DataTypeAdapter#getFieldDataType()
	 */
	@Override
	public FieldDataType getFieldDataType() {
		return FieldDataType.TEXT;
	}

}
