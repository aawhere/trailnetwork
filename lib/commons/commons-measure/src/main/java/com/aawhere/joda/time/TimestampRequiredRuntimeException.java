/**
 * 
 */
package com.aawhere.joda.time;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Used to indicate a timestamp is required. This is a runtime because it is typically considered an
 * unrecoverable error that simply needs to be send back to the user. Should you need to catch this
 * create a regular exception with the same message.
 * 
 * @author aroller
 * 
 */
public class TimestampRequiredRuntimeException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 7336333270454393472L;

	public TimestampRequiredRuntimeException() {
		this("");
	}

	/**
	 * 
	 */
	public TimestampRequiredRuntimeException(String context) {
		super(new CompleteMessage.Builder(JodaTimeMessage.TIMESTAMP_REQUIRED)
				.addParameter(JodaTimeMessage.Param.CONTEXT, context).build());
	}
}
