/**
 *
 */
package com.aawhere.measure.geocell;

import java.util.Collection;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxXmlAdapter;
import com.google.common.collect.Lists;

/**
 * An extension of {@link BoundingBox} to represent the box using {@link GeoCell}s.
 * 
 * This class is inconsistent depending upon how it is built. Providing a bounds this will provide
 * an approximate list of cells that represent that bounds.
 * 
 * When building by cells the bounds will match exactly those cells since the bounds is derived from
 * those cells.
 * 
 * The inconsistency is that if you construct with a bounds, then take those cells and construct
 * another box from those cells you will end up with different bounds.
 * 
 * @author Brian Chapman
 * 
 */

@XmlJavaTypeAdapter(BoundingBoxXmlAdapter.class)
public class GeoCellBoundingBox
		extends BoundingBox {

	private static final long serialVersionUID = -4502028202554866896L;
	private GeoCells geoCells;

	/**
	 * Used to construct all instances of GeoCellBoundingBox.
	 */
	public static class Builder
			extends BoundingBox.BaseBuilder<GeoCellBoundingBox, Builder> {

		private Boolean minimize;
		private Resolution resolution;
		private Integer maxGeoCells;

		public Builder() {
			super(new GeoCellBoundingBox());
		}

		public Builder setGeoCells(GeoCells geoCells) {
			Assertion.assertNotNull("geoCells", geoCells);
			building.geoCells = geoCells;
			BoundingBox boundingBox = GeoCellUtil.boundingBoxFromGeoCells(geoCells);
			super.setOther(boundingBox);
			return dis;
		}

		public Builder setGeoCells(GeoCell... geoCells) {
			return setGeoCells(new GeoCells.Builder().addAll(Lists.newArrayList(geoCells)).build());
		}

		/**
		 * @param bounds
		 * @return
		 */
		public Builder setGeoCells(Collection<GeoCell> bounds) {
			setGeoCells(new GeoCells.Builder().addAll(bounds).build());
			return dis;
		}

		/**
		 * @see GeoCellUtil#generateGeoCells(BoundingBox, Resolution, Boolean, Double)
		 * 
		 * @param minimize
		 *            defaults to {@link GeoCellUtil.DONT_MINIMIZE_RESULT};
		 */
		public Builder minimizeGeoCells(Boolean minimize) {
			this.minimize = minimize;
			return this;
		}

		/**
		 * @see GeoCellUtil#generateGeoCells(BoundingBox, Resolution, Boolean, Double)
		 * @param res
		 *            defaults to {@link GeoCellUtil#DEFAULT_GEOCELL_RESOLUTION}.
		 * @return
		 */
		public Builder maxGeoCellResolution(Resolution res) {
			this.resolution = res;
			return this;
		}

		/**
		 * @see GeoCellUtil#generateGeoCells(BoundingBox, Resolution, Boolean, Double)
		 * @param maxGeoCells
		 *            defaults to Double.MAX_VALUE.
		 */
		public Builder maxGeoCells(Integer maxGeoCells) {
			this.maxGeoCells = maxGeoCells;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.BoundingBox.BaseBuilder#build()
		 */
		@Override
		public GeoCellBoundingBox build() {
			// purposely preceding super.build so updated bounding box may be assigned
			prepareToBuild();
			// if no geocells, then a bounding box must have been provided
			if (building.geoCells == null || this.minimize != null || this.maxGeoCells != null
					|| this.resolution != null) {

				Set<GeoCell> cells = GeoCellUtil
						.generateGeoCells(building, resolution, this.minimize, this.maxGeoCells);
				// call the setter to update the bounds.
				setGeoCells(cells);
			}

			// make sure the boss is ready
			GeoCellBoundingBox box = super.build();

			return box;
		}

	}// end Builder

	/**
	 * Required for xml marshaling
	 * 
	 */
	private GeoCellBoundingBox() {
		super();
	}

	/**
	 * Get a list of {@link GeoCell} that best fit the bounding box. Uses default {@link Resolution}
	 * {@link #MAX_CELL_RESOLUTION}.
	 * 
	 * @param box
	 * @return a list of {@link GeoCell} fitting the {@link BoundingBox}
	 */
	public GeoCells getGeoCells() {
		return this.geoCells;
	}

	/**
	 * Constructs the {@link GeoCellBoundingBox} given the list of GeoCell values or the
	 * {@link BoundingBox#getBounds()} decimal degrees formatted string.
	 * 
	 * This will throw an exception if neither format is recognized.
	 * 
	 * @param boundingBoxAsString
	 * @return
	 */
	public static GeoCellBoundingBox valueOf(String boundingBoxAsString) {
		return GeoCellUtil.geoCellBoundingBox(GeoCellUtil.boundingBox(boundingBoxAsString));
	}

	/**
	 * @return
	 */
	public static Builder createGeoCell() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.geoCells == null) ? 0 : this.geoCells.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GeoCellBoundingBox other = (GeoCellBoundingBox) obj;
		if (this.geoCells == null) {
			if (other.geoCells != null) {
				return false;
			}
		} else if (!this.geoCells.equals(other.geoCells)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.BoundingBox#toString()
	 */
	@Override
	public String toString() {
		return new StringBuilder().append("(").append(this.geoCells).append(") = ").append(super.toString()).toString();

	}
}
