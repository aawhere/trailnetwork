/**
 * 
 */
package com.aawhere.measure;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Indicates the {@link BoundingBox} string format {@link BoundingBox#getBounds()} is an invalid
 * format.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class InvalidBoundsException
		extends BaseRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5585132182065260575L;

	/**
	 * @param bounds
	 *            is the failing value being used to attempt to create a bounding box using the
	 *            standard form.
	 */
	public InvalidBoundsException(String bounds) {
		super(CompleteMessage.create(MeasureMessage.INVALID_BOUNDS).addParameter(MeasureMessage.Param.BOUNDS, bounds)
				.build());
	}

}
