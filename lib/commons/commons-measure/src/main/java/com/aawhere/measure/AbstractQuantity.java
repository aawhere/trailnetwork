/**
 *
 */
package com.aawhere.measure;

import java.io.Serializable;
import java.text.NumberFormat;

import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;
import javax.xml.bind.annotation.XmlRootElement;

import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.QuantityMath;

/**
 * A simple implementation of Quantity to improve performance over the default proxy
 * implementations.
 * 
 * @author Aaron Roller
 * 
 */
@XmlRootElement
public class AbstractQuantity<Q extends Quantity<Q>>
		implements Quantity<Q>, Serializable, Comparable<Q> {

	private static final long serialVersionUID = 4661531110794635004L;
	private Unit<Q> unit;
	private Number value;

	// for Jaxb
	@SuppressWarnings("unused")
	private AbstractQuantity() {
	}

	/**
	 *
	 */
	public AbstractQuantity(Number value, Unit<Q> unit) {
		Assertion.assertNotNull(value);
		Assertion.assertNotNull(unit);
		this.unit = unit;
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.measure.quantity.Quantity#getValue()
	 */
	@Override
	public Number getValue() {

		return this.value;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.measure.quantity.Quantity#getUnit()
	 */
	@Override
	public Unit<Q> getUnit() {

		return this.unit;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.measure.quantity.Quantity#doubleValue(javax.measure.unit.Unit)
	 */
	@Override
	public double doubleValue(Unit<Q> unit) {
		return this.unit.getConverterTo(unit).convert(this.value.doubleValue());

	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof Quantity) {
			@SuppressWarnings("rawtypes")
			Quantity that = (Quantity) obj;
			if (this.unit.isCompatible(that.getUnit())) {
				result = this.doubleValue(this.unit) == that.doubleValue(this.unit);
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final NumberFormat format = NumberFormat.getInstance();
		format.setMaximumFractionDigits(getToStringPrecision());
		return format.format(getValue()) + " " + getUnit();
	}

	/**
	 * @return
	 */
	protected int getToStringPrecision() {
		return 2;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Q o) {
		return QuantityMath.create((Q) this).compareTo(o);
	}

}
