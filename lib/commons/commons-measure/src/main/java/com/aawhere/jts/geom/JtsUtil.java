/**
 * 
 */
package com.aawhere.jts.geom;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.collections.ArrayUtilsExtended;

import com.vividsolutions.jts.algorithm.CGAlgorithms;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.operation.distance.DistanceOp;

/**
 * Simple util to be used when only dealing with geometries. com.aawhere.measure.* types should not
 * be here. See MeasureJtsUtil for converting between JTS and com.aawhere.measure.* types.
 * 
 * @author roller
 * 
 */
public class JtsUtil {

	public static final GeometryFactory DEFAULT_GEOMETRY_FACTORY = JtsGeometryFactory.getDefaultfactory();

	static public MultiLineString multiLineStringFrom(LineString lineString, GeometryFactory factory) {
		LineString[] lineStrings = { lineString };
		return factory.createMultiLineString(lineStrings);

	}

	/**
	 * 
	 * @param sequences
	 * @param factory
	 * @return
	 */
	static public MultiLineString multiLineStringFrom(List<? extends CoordinateSequence> sequences,
			GeometryFactory factory) {

		LineString[] lineStrings = new LineString[sequences.size()];
		for (int whichSequnce = 0; whichSequnce < sequences.size(); whichSequnce++) {
			CoordinateSequence coordinateSequence = sequences.get(whichSequnce);
			LineString lineString = factory.createLineString(coordinateSequence);
			lineStrings[whichSequnce] = lineString;
		}
		return factory.createMultiLineString(lineStrings);
	}

	static public MultiLineString multiLineStringFrom(Coordinate[] corrdinates, GeometryFactory factory) {
		return multiLineStringFrom(factory.createLineString(corrdinates), factory);
	}

	/**
	 * Given the geometry this will provide the length as computed by
	 * {@link CGAlgorithms#length(com.vividsolutions.jts.geom.CoordinateSequence)}
	 * 
	 * TODO: Handle MultiLineString ignoring the gaps between line strings. This will currently
	 * calculate the length between the two.
	 * 
	 * @param target
	 * @return
	 */
	public static double length(Geometry target) {
		return CGAlgorithms.length(new CoordinateArraySequence(target.getCoordinates()));
	}

	public static double shortestDistanceBetween(Geometry geo1, Geometry geo2) {
		return DistanceOp.distance(geo1, geo2);
	}

	/**
	 * Just an alias using easier syntax for
	 * {@link GeometryFactory#createMultiLineString(LineString[])}
	 * 
	 * @param factory
	 * @param lineStrings
	 * @return
	 */
	public static MultiLineString multiLineString(GeometryFactory factory, LineString... lineStrings) {
		return factory.createMultiLineString(lineStrings);
	}

	/**
	 * Given a LineString, this will take it's coordinates and split the lineString into multiple
	 * LineStrings resulting in a MultiLineString...each line string with max length.
	 * 
	 * @param lineString
	 * @param maxCoordinatesPerLine
	 * @return
	 */
	public static MultiLineString split(LineString lineString, Integer maxCoordinatesPerLine) {
		if (maxCoordinatesPerLine < 2) {
			throw new IllegalArgumentException("can't make a line smaller than size two, but you gave "
					+ maxCoordinatesPerLine);
		}
		Coordinate[] allCoordinates = lineString.getCoordinates();
		List<Coordinate[]> split = ArrayUtilsExtended.split(allCoordinates, maxCoordinatesPerLine);
		GeometryFactory factory = lineString.getFactory();
		LineString[] lineStrings = new LineString[split.size()];
		int i = 0;
		for (Coordinate[] lineCoordinates : split) {
			// if only a single coordinate, append to the previous line
			if (lineCoordinates.length == 1) {
				int previousIndex = i - 1;
				LineString previous = lineStrings[previousIndex];
				Coordinate[] previousCoords = previous.getCoordinates();
				previousCoords = ArrayUtils.add(previousCoords, lineCoordinates[0]);
				// now, we must reproduce previous line string and shorten overall array
				lineStrings[previousIndex] = factory.createLineString(previousCoords);
				lineStrings = ArrayUtils.subarray(lineStrings, 0, lineStrings.length - 1);
			} else {
				lineStrings[i] = factory.createLineString(lineCoordinates);
			}
			i++;
		}

		return factory.createMultiLineString(lineStrings);

	}

	/**
	 * Returns true if {@link Geometry#isEmpty()} returns true of Geometry is null.
	 * 
	 * @param mls
	 * @return
	 */
	public static Boolean isEmpty(Geometry mls) {
		return mls == null || mls.isEmpty();
	}

	/**
	 * @return
	 */
	public static LineString emptyLineString(GeometryFactory factory) {
		return factory.createLineString(new Coordinate[0]);
	}

	public static Point point(Coordinate coord, GeometryFactory factory) {
		return factory.createPoint(coord);
	}

	public static MultiPoint[] toMultiPointArray(Collection<Point> points) {
		return GeometryFactory.toMultiPointArray(points);
	}

	public static MultiPoint toMultiPoint(Collection<Coordinate> coords) {
		return DEFAULT_GEOMETRY_FACTORY.createMultiPoint(coords.toArray(new Coordinate[coords.size()]));
	}

	/**
	 * @param centerOfIntersection
	 * @param tenCmPrecision
	 * @return
	 */
	public static Coordinate reducePrecision(Coordinate coordinate, Integer maxDecimalPlaces) {
		RoundingMode roundingMode = RoundingMode.HALF_UP;
		BigDecimal x = new BigDecimal(coordinate.x).setScale(maxDecimalPlaces, roundingMode);
		BigDecimal y = new BigDecimal(coordinate.y).setScale(maxDecimalPlaces, roundingMode);
		return new Coordinate(x.doubleValue(), y.doubleValue(), coordinate.z);
	}
}
