/**
 * 
 */
package com.aawhere.measure;

import javax.measure.quantity.Quantity;

/**
 * A measurement indicating some number of items the result being exclusively and {@link Integer}.
 * Useful for personalizing numbers using the standard measurement system rather than just Integer.
 * 
 * @author aroller
 * 
 */
public interface Count
		extends Quantity<Count> {

}
