/**
 *
 */
package com.aawhere.measure;

import java.io.Serializable;
import java.math.MathContext;
import java.text.NumberFormat;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.number.NumberUtilsExtended;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.xml.XmlNamespace;

/**
 * A point on earth.
 * 
 * A simple container for {@link Latitude} and {@link Longitude}.
 * 
 * @author Aaron Roller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@Dictionary(domain = "geoCoordinate")
public class GeoCoordinate
		implements Serializable {

	private static final long serialVersionUID = -8635396606598920069L;
	private static final NumberFormat DECIMAL_DEGREE_FORMAT;
	public static final Integer ONE_M_PRECISION = 5;
	public static final Integer TEN_CM_PRECISION = 6;
	public static final Integer ONE_CM_PRECISION = 7;
	public static final Integer ONE_MM_PRECISION = 8;

	static {
		DECIMAL_DEGREE_FORMAT = NumberFormat.getNumberInstance();
		DECIMAL_DEGREE_FORMAT.setGroupingUsed(false);
		DECIMAL_DEGREE_FORMAT.setMinimumFractionDigits(ONE_CM_PRECISION);
	}
	/**
	 * Precision constants offered to round to an appropriate precision
	 * 
	 * @see QuantityMath#setScale(MathContext)
	 */
	static final int STRING_LON_INDEX = 1;

	static final int STRING_LAT_INDEX = 0;

	static final String STRING_DILIMETER = NumberUtilsExtended.DEFAULT_DILIMETER;

	/**
	 * Used to construct all instances of GeoCoordinate.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<GeoCoordinate> {

		public Builder() {
			super(new GeoCoordinate());
		}

		public Builder setLatitude(Latitude latitude) {
			building.latitude = latitude;
			return this;
		}

		public Builder setLongitude(Longitude longitude) {
			building.longitude = longitude;
			return this;
		}

		public Builder setLongitude(Double inDecimalDegrees) {
			setLongitude(MeasurementUtil.createLongitude(inDecimalDegrees));
			return this;
		}

		public Builder setLatitude(Double inDecimalDegrees) {
			setLatitude(MeasurementUtil.createLatitude(inDecimalDegrees));
			return this;
		}

		/**
		 * Accepts the value as defined in {@link #getAsString()}
		 * 
		 * @see #getAsString()
		 */
		public Builder setAsString(String asString) {

			Double[] doubles = NumberUtilsExtended.split(asString);
			setLatitude(doubles[STRING_LAT_INDEX]);
			setLongitude(doubles[STRING_LON_INDEX]);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull(building.latitude);
			Assertion.assertNotNull(building.longitude);
			super.validate();
		}

	}// end Builder

	@XmlElement
	@Field
	private Latitude latitude;
	@XmlElement
	@Field
	private Longitude longitude;

	/**
	 * Required for xml marshaling
	 */
	protected GeoCoordinate() {
	}

	/**
	 * Constructor providing Latitude first because non-cartographers prefer to say it that way.
	 * 
	 * @deprecated use Builder
	 * @param latitude
	 * @param longitude
	 */
	@Deprecated
	public GeoCoordinate(Latitude latitude, Longitude longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		Assertion.assertNotNull(this.latitude);
		Assertion.assertNotNull(this.longitude);

	}

	/**
	 * Convenience constructor when providing {@link GeoQuantity} in decimal degrees unit.
	 * 
	 * @deprecated use {@link Builder}
	 * @param latitude
	 * @param longitude
	 */
	@Deprecated
	public GeoCoordinate(Double latitude, Double longitude) {
		this(MeasurementUtil.createLatitude(latitude), MeasurementUtil.createLongitude(longitude));
	}

	/**
	 * Duplicate constructor providing arguments with x first since some cartographers prefer it
	 * that way.
	 * 
	 * @deprecated use {@link Builder}
	 * @param longitude
	 * @param latitude
	 */
	@Deprecated
	public GeoCoordinate(Longitude longitude, Latitude latitude) {
		this(latitude, longitude);
	}

	public Latitude getLatitude() {
		return this.latitude;
	}

	public Longitude getLongitude() {
		return this.longitude;
	}

	/**
	 * Produces a string representation of this coordinate that may be parsed using
	 * {@link #GeoCoordinate(String)} or {@link #valueOf(String)}.
	 * 
	 * Format is latitude then longitude in decimal degrees separated by a comma.
	 * 
	 * latitude,longitude
	 * 
	 * @return
	 */
	@XmlAttribute(name = "value")
	public String getAsString() {
		String[] parts = stringParts();
		return StringUtils.join(parts, STRING_DILIMETER);
	}

	public String toXyString() {
		String[] parts = stringParts();
		ArrayUtils.reverse(parts);
		return StringUtils.join(parts, STRING_DILIMETER);
	}

	/**
	 * @return
	 */
	private String[] stringParts() {
		String[] parts = new String[2];
		parts[STRING_LAT_INDEX] = DECIMAL_DEGREE_FORMAT.format(this.latitude.getInDecimalDegrees());
		parts[STRING_LON_INDEX] = DECIMAL_DEGREE_FORMAT.format(this.longitude.getInDecimalDegrees());
		return parts;
	}

	public static GeoCoordinate valueOf(String coordinateAsString) {
		return new GeoCoordinate.Builder().setAsString(coordinateAsString).build();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object that) {

		boolean result = false;
		if (that != null && that instanceof GeoCoordinate) {
			GeoCoordinate dat = (GeoCoordinate) that;
			result = new QuantityMath<Latitude>(this.latitude).equals(dat.latitude)
					&& new QuantityMath<Longitude>(this.longitude).equals(dat.longitude);

		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		return this.latitude.hashCode() + this.longitude.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getAsString();
	}

	/**
	 * @return
	 * 
	 */
	public static Builder create() {
		return new Builder();
	}

}
