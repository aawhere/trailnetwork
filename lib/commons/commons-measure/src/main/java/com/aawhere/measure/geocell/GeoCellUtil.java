/**
 *
 */
package com.aawhere.measure.geocell;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.measure.quantity.Length;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.collections.FunctionsExtended;
import com.aawhere.jts.geom.JtsUtil;
import com.aawhere.lang.If;
import com.aawhere.lang.string.StringInvalidCharacterFinder;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.BoundingBoxes;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.InvalidBoundsException;
import com.aawhere.measure.JtsMeasureUtil;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCells.Builder;
import com.beoui.geocell.GeocellManager;
import com.beoui.geocell.GeocellUtils;
import com.beoui.geocell.model.CostFunction;
import com.beoui.geocell.model.Point;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import com.vividsolutions.jts.algorithm.MinimumBoundingCircle;
import com.vividsolutions.jts.geom.MultiPoint;

/**
 * Utility methods for accessing the GeoCell library.
 * 
 * @see http://code.google.com/p/javageomodel/ and
 *      http://code.google.com/p/geomodel/source/browse/trunk/geo/geocell.py for more details
 * @author brian
 * 
 */
public class GeoCellUtil {

	public static final int NUM_OF_CHILD_CELLS_IN_CELL = (int) Math.pow(GeocellUtils.GEOCELL_GRID_SIZE, 2);
	private static final Integer DEFAULT_MAX_NUMBER_CELLS = Integer.MAX_VALUE;
	public static final Boolean MINIMIZE_RESULTS = true;
	public static final Boolean DONT_MINIMIZE_RESULTS = false;
	public static final Resolution MAX_GEOCELL_RESOLUTION = Resolution.THIRTEEN;
	// Why 11? Well, it is about 10m, which is fine enough for most bounding box uses and we need a
	// reasonable default somewhere. The max resolution could result in way too many geocells for
	// any practical use, though this default could too...
	public static final Resolution DEFAULT_GEOCELL_RESOLUTION = Resolution.ELEVEN;

	// Duplication of Direction enumerations from GeocellUtils since they are not public.
	private static final int[] NORTHWEST = new int[] { -1, 1 };
	private static final int[] NORTH = new int[] { 0, 1 };
	private static final int[] NORTHEAST = new int[] { 1, 1 };
	private static final int[] EAST = new int[] { 1, 0 };
	private static final int[] SOUTHEAST = new int[] { 1, -1 };
	private static final int[] SOUTH = new int[] { 0, -1 };
	private static final int[] SOUTHWEST = new int[] { -1, -1 };
	private static final int[] WEST = new int[] { -1, 0 };

	/**
	 * Valid letters. This is available in {@link GeocellUtils}, but unfortunately only private.
	 */
	static final String ALPHABET = "01234567890abcdef";
	/** Provides all of the Resolution 1 cells which will cover the entire world. */
	public static final GeoCells WORLD = GeoCells
			.valueOf(StringUtils.join(ArrayUtils.toObject(ALPHABET.toCharArray()), GeoCells.SPACE));

	private static BoundingBox convertBoundingBox(com.beoui.geocell.model.BoundingBox boundingBox) {

		return new BoundingBox.Builder().setNorth(MeasurementUtil.createLatitude(boundingBox.getNorth()))
				.setSouth(MeasurementUtil.createLatitude(boundingBox.getSouth()))
				.setEast(MeasurementUtil.createLongitude(boundingBox.getEast()))
				.setWest(MeasurementUtil.createLongitude(boundingBox.getWest())).build();
	}

	/**
	 * Given all of the {@link GeoCells} this will return the bounding box that contains them all.
	 * 
	 * @see #boundingBoxFromGeoCell(GeoCell)
	 * @see #boundingBoxFromGeoCells(Set)
	 * @param geoCells
	 * @return
	 */
	public static BoundingBox boundingBoxFromGeoCells(GeoCells geoCells) {
		Set<GeoCell> all = geoCells.getAll();
		BoundingBox result = boundingBoxFromGeoCells(filterToMaxResolution(all));
		return result;
	}

	/**
	 * @param all
	 * @return
	 */
	public static BoundingBox boundingBoxFromGeoCells(Set<GeoCell> all) {
		BoundingBox result = null;
		for (GeoCell geoCell : all) {
			BoundingBox box = boundingBoxFromGeoCell(geoCell);
			if (result == null) {
				result = box;
			} else {
				result = result.combine(box);
			}
		}
		return result;
	}

	/**
	 * provides the actual {@link BoundingBox} given a single {@link GeoCell}.
	 * 
	 * @param geoCell
	 * @return
	 */
	public static BoundingBox boundingBoxFromGeoCell(GeoCell geoCell) {
		return convertBoundingBox(GeocellUtils.computeBox(geoCell.getCellAsString()));
	}

	/**
	 * Generate a list of geocells that best fit the supplied bounding box.
	 * 
	 * @param box
	 * @param maxResolution
	 *            maximum geocell resolution (number of charaters) to use when determining the best
	 *            fit.
	 * @param minimize
	 *            reduce the number of {@link GeoCell}s by combining them when possible.
	 * @param maxCells
	 *            the maximum number of cells to return. More {@link GeoCell}s will provide a better
	 *            match to the supplied {@link BoundingBox}.
	 * @return a list of geocells fitting the supplied {@link BoundingBox}
	 */
	public static Set<GeoCell> generateGeoCells(BoundingBox box, final Resolution maxResolution,
			final Boolean minimize, final Integer maxCells) {

		final Boolean minimizeChoice = If.nil(minimize).use(DONT_MINIMIZE_RESULTS);
		final Resolution maxResolutionChoice = If.nil(maxResolution).use(MAX_GEOCELL_RESOLUTION);
		final Integer maxCellsChoice = If.nil(maxCells).use(DEFAULT_MAX_NUMBER_CELLS);

		com.beoui.geocell.model.BoundingBox bb = convertToGeoCellBoundingBox(box);
		List<String> searchCells = GeocellManager.bestBboxSearchCells(bb, new CostFunction() {
			@Override
			public double defaultCostFunction(int numCells, int resolution) {
				Double cost = 0d;
				final boolean resolutionExceeded = resolution > maxResolutionChoice.asInteger();
				final boolean cellSizeExceeded = numCells > maxCellsChoice;
				cost = (resolutionExceeded || cellSizeExceeded) ? Double.MAX_VALUE : 0;
				return cost;
			}
		});
		Set<GeoCell> geoCells = convertToGeoCellsSet(searchCells);

		// the cost algorithm reduce resolution to the max when higher resolution cells are
		// provided.
		GeoCellsReducer.Builder reducerBuilder = GeoCellsReducer.create().setGeoCells(geoCells);
		if (maxResolution != null) {
			reducerBuilder.setMaxResolution(maxResolution);
		}
		if (minimizeChoice.equals(MINIMIZE_RESULTS)) {
			reducerBuilder.minimize();
		}
		if (maxResolution != null || minimizeChoice.equals(MINIMIZE_RESULTS)) {
			geoCells = reducerBuilder.build().getGeoCells();
		}
		return geoCells;
	}

	/**
	 * @See {@link #generateGeoCells(com.beoui.geocell.model.BoundingBox, Resolution, Boolean, Integer)}
	 * 
	 *      Defaults maxCells to {@link #NUM_OF_CHILD_CELLS_IN_CELL}
	 */
	public static Set<GeoCell> generateGeoCells(BoundingBox box, final Resolution maxResolution, Boolean minimize) {
		return generateGeoCells(box, maxResolution, minimize, null);
	}

	/**
	 * @See {@link #generateGeoCells(com.beoui.geocell.model.BoundingBox, Resolution, Boolean, Integer)}
	 * 
	 *      Defaults maxResolution to {@link GeoCellBoundingBox#DEFAULT_GEOCELL_RESOLUTION}.
	 */
	public static Set<GeoCell> generateGeoCells(BoundingBox box, Boolean minimize) {
		return generateGeoCells(box, null, minimize);
	}

	/**
	 * @See {@link #generateGeoCells(com.beoui.geocell.model.BoundingBox, Resolution, Boolean, Integer)}
	 */
	public static Set<GeoCell> generateGeoCells(GeoCoordinate ne, GeoCoordinate sw, Boolean minimize) {
		BoundingBox box = new BoundingBox.Builder().setNorthEast(ne).setSouthWest(sw).build();

		return generateGeoCells(box, minimize);
	}

	/**
	 * <p>
	 * Takes a {@link Collection} of {@link GeoCell}s and translates it to include all encompassing
	 * resolutions.
	 * </p>
	 * <p>
	 * Example: ["b54c", b56b"] becomes ["b", "b5", "b54", "b56", "b54c", "b56b"]
	 * </p>
	 * <p>
	 * No duplicates allowed, order is not preserved.
	 * 
	 * @deprecated use {@link #withAncestors(Iterable)}
	 * @param geoCells
	 * @return
	 */
	@Deprecated
	public static Set<GeoCell> translateToAllResolutions(Iterable<GeoCell> geoCells) {
		Set<GeoCell> transformedGeoCells = new HashSet<GeoCell>();
		for (GeoCell geoCell : geoCells) {
			int resolution = geoCell.getResolution().asInteger();
			while (resolution != 0) {
				String transformedCell = geoCell.getCellAsString().substring(0, resolution);
				transformedGeoCells.add(new GeoCell(transformedCell));
				resolution--;
			}
		}
		return transformedGeoCells;
	}

	/**
	 * Returns the given set of cells with all of it's parents and their parents up until
	 * {@link Resolution#ONE}. Synonym for {@link #translateToAllResolutions(Iterable)}.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static GeoCells withAncestors(Iterable<GeoCell> geoCells) {
		return GeoCells.create().addAll(translateToAllResolutions(geoCells)).build();
	}

	/**
	 * Adds the parents of the given geocells and returns a new set with the original cells and it's
	 * direct parents (not all ancestors).
	 * 
	 * @see #withAncestors(Iterable)
	 * @param geoCells
	 * @return
	 */
	public static GeoCells withParents(Iterable<GeoCell> geoCells) {
		Builder builder = GeoCells.create();
		for (GeoCell geoCell : geoCells) {
			builder.add(geoCell);
			if (geoCell.hasParent()) {
				builder.add(getParentGeoCell(geoCell));
			}
		}
		return builder.build();
	}

	/**
	 * Given a geoCell this will explode it to have all the children which won't change it's
	 * coverage, but useful for getting all children.
	 * 
	 * @param geoCell
	 * @return
	 */
	public static Set<GeoCell> explode(GeoCell geoCell) {
		char[] chars = GeoCellUtil.ALPHABET.toCharArray();
		HashSet<GeoCell> result = new HashSet<>();
		for (int i = 0; i < chars.length; i++) {
			char c = chars[i];
			result.add(new GeoCell(geoCell.getCellAsString() + c));
		}
		return result;
	}

	/**
	 * @param all
	 * @return
	 */
	public static Set<GeoCell> explodeLast(SortedSet<GeoCell> all) {
		Set<GeoCell> result = explode(all.last());
		result.addAll(all);
		return result;
	}

	/**
	 * This will return the parent of the given {@link GeoCell} or throw an exception if no parent
	 * exists. Use {@link #hasParentGeoCell(GeoCell)} to find out if a parent exists before calling
	 * this method.
	 * 
	 * @param geoCell
	 * @return the parent GeoCell (i.e. 1f45 parent is 1f4).
	 */
	public static GeoCell getParentGeoCell(GeoCell geoCell) {
		String geoString = geoCell.getCellAsString();
		String parentString = geoString.substring(0, geoString.length() - 1);
		return new GeoCell(parentString);
	}

	public static Boolean hasParentGeoCell(GeoCell geoCell) {
		return geoCell.getCellAsString().length() > 1;
	}

	/**
	 * The opposite of {@link #withParents(Iterable)}. This returns a new set with the original
	 * cells parents removed. Supports mixed resolution cells.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static GeoCells withOutParents(Iterable<GeoCell> geoCells) {
		Set<GeoCell> result = Sets.newHashSet(geoCells);
		for (GeoCell cell : geoCells) {
			for (GeoCell parent : geoCells) {
				if (parent.isAncestor(cell)) {
					result.remove(parent);
				}
			}
		}
		return GeoCells.create().addAll(result).build();
	}

	/**
	 * Ensure that the returned list contains only GeoCells of the maximum resolution contained in
	 * the set.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static Set<GeoCell> filterToMaxResolution(Set<GeoCell> geoCells) {
		Resolution maxResolution = maxResolution(geoCells);
		return filterToResolution(geoCells, maxResolution).getAll();
	}

	/**
	 * Ensure that the returned list contains only GeoCells of the maximum resolution contained in
	 * the set.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static GeoCells filterToMaxResolution(GeoCells geoCells) {
		Resolution maxResolution = maxResolution(geoCells.getAll());
		return filterToResolution(geoCells, maxResolution);
	}

	/**
	 * Filters out any GeoCells with a resolution higher than the given max. This is useful when
	 * geocells includes its parents and wishes to keep them.
	 * 
	 * @param geoCells
	 * @param resolution
	 * @return
	 */
	public static Set<GeoCell> removeCellsWithResolutionsHigherThan(Set<GeoCell> geoCells, Resolution resolution) {
		Range<Resolution> atMost = Range.atMost(resolution);
		Predicate<GeoCell> predicate = Predicates.compose(atMost, GeoCellResolutionFunction.create().build());
		final Set<GeoCell> all = geoCells;
		Set<GeoCell> filtered = Sets.filter(all, predicate);
		// no point in re-creating if nothing changed
		if (all.size() != filtered.size()) {
			geoCells = filtered;
		}
		return geoCells;

	}

	/**
	 * Reduces the given {@link GeoCells} to the given maxInclusive size by reducing the resolution
	 * incrementally until the size can be achieved. The minimum value of maxInclusive is
	 * NUM_OF_CHILD_CELLS_IN_CELL.
	 * 
	 * @param geoCells
	 * @param maxInclusive
	 * @return
	 */
	public static Set<GeoCell> filterToSizeReducingResolution(Set<GeoCell> geoCells, Integer maxInclusive) {
		if (maxInclusive < NUM_OF_CHILD_CELLS_IN_CELL) {
			throw new IllegalArgumentException("The minimum valid for maxInclusive is " + NUM_OF_CHILD_CELLS_IN_CELL);
		}
		Set<GeoCell> result = new HashSet<>();
		while (geoCells.size() > maxInclusive) {
			result = removeCellsWithResolutionsHigherThan(geoCells, maxResolution(geoCells).lower());
		}
		return result;
	}

	/**
	 * Ensure the returned list contains GeoCells only of the selected resolution.
	 * 
	 * @param geoCells
	 * @param resolution
	 * @return
	 */
	public static GeoCells filterToResolution(Set<GeoCell> geoCells, Resolution resolution) {
		return filterToResolution((Iterable<GeoCell>) geoCells, resolution);
	}

	public static GeoCells filterToResolution(Iterable<GeoCell> geoCells, Resolution resolution) {
		GeoCells.Builder result = new GeoCells.Builder();

		for (GeoCell cell : geoCells) {
			if (cell.getResolution().equals(resolution)) {
				result.add(cell);
			}
		}
		return result.build();
	}

	/**
	 * Return the maximum resolution contained in the supplied {@link GeoCell}s.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static Resolution maxResolution(Set<GeoCell> geoCells) {
		Resolution resolution = Resolution.ONE;
		for (GeoCell cell : geoCells) {
			Resolution cellRes = cell.getResolution();
			if (resolution.asInteger() < cellRes.asInteger()) {
				resolution = cellRes;
			}
		}
		return resolution;
	}

	/**
	 * Returns the lowest {@link Resolution} contained in the set.
	 * 
	 * @param cellsWithoutParents
	 * @return
	 */
	public static Resolution lowestResolution(Set<GeoCell> geoCells) {
		Resolution resolution = MAX_GEOCELL_RESOLUTION;
		for (GeoCell cell : geoCells) {
			Resolution cellRes = cell.getResolution();
			if (resolution.asInteger() > cellRes.asInteger()) {
				resolution = cellRes;
			}
		}
		return resolution;
	}

	public static Resolution getResolution(Integer res) {
		return Resolution.valueOf(res.byteValue());
	}

	/**
	 * Convert a {@link com.beoui.geocell.model.BoundingBox} to a {@link BoundingBox}
	 * 
	 * @param box
	 * @return
	 */
	static com.beoui.geocell.model.BoundingBox convertToGeoCellBoundingBox(BoundingBox box) {
		Double latN, lonE, latS, lonW;

		latN = box.getNorthEast().getLatitude().getInDecimalDegrees();
		lonE = box.getNorthEast().getLongitude().getInDecimalDegrees();
		latS = box.getSouthWest().getLatitude().getInDecimalDegrees();
		lonW = box.getSouthWest().getLongitude().getInDecimalDegrees();

		com.beoui.geocell.model.BoundingBox bb = new com.beoui.geocell.model.BoundingBox(latN, lonE, latS, lonW);
		return bb;
	}

	public static GeoCells convertToGeoCells(Iterable<String> geoCellsAsString) {
		return GeoCells.create().setAll(convertToGeoCellsSet(geoCellsAsString)).build();
	}

	public static SortedSet<GeoCell> convertToGeoCellsSet(Iterable<String> geoCellsAsString) {

		SortedSet<GeoCell> geoCells = new TreeSet<GeoCell>();
		for (String cell : Iterables.filter(geoCellsAsString, Predicates.notNull())) {
			geoCells.add(geoCellValidated(cell));
		}
		return geoCells;
	}

	/**
	 * Given the array of geocell values this will return the strongly typed set.
	 * 
	 * @see #convertToGeoCellsSet(Collection)
	 * @param geoCellValues
	 * @return
	 */
	public static Set<GeoCell> convertToGeoCellsSet(final String... geoCellValues) {
		return convertToGeoCellsSet(Arrays.asList(geoCellValues));
	}

	public static Set<String> convertGeoCellsToStringSet(Collection<GeoCell> geoCells) {

		Set<String> geoCellsAsString = new HashSet<String>();
		for (GeoCell cell : geoCells) {
			geoCellsAsString.add(cell.getCellAsString());
		}
		return geoCellsAsString;
	}

	public static Boolean crosses180Meridian(BoundingBox box) {
		return (box.getNorthEast().getLongitude().getInDecimalDegrees() < box.getSouthWest().getLongitude()
				.getInDecimalDegrees());
	}

	/**
	 * Generate a set of {@link GeoCell}s at all resolutions.
	 * 
	 * @param location
	 * @param resolution
	 * @return
	 */
	public static Set<GeoCell> createGeoCells(GeoCoordinate location) {
		Point point = point(location);
		List<String> geoCellStrings = GeocellManager.generateGeoCell(point);
		Set<GeoCell> geoCells = convertToGeoCellsSet(geoCellStrings);
		return geoCells;
	}

	/**
	 * @deprecated use {@link #geoCell(GeoCoordinate, Resolution)}
	 * @param location
	 * @param resolution
	 * @return
	 */
	@Deprecated
	public static GeoCell createGeoCell(GeoCoordinate location, Resolution resolution) {
		Point point = point(location);
		String geoCellString = GeocellUtils.compute(point, resolution.asInteger());
		return new GeoCell(geoCellString);
	}

	/**
	 * Generate a {@link GeoCell} for the supplied Resolution. This method is typically not
	 * recommended for use of storing and searching for a point. This may be useful in some
	 * circumstances when a single cell is required to describe a point.
	 * 
	 * WARNING: 0,0 lands on the intersection of 4 boxes (9,3,6,c), but only c is chosen. This will
	 * only end in the search for mixups. It's useful, but it's limitations should be considered for
	 * the related use.
	 * 
	 * 
	 * @see #geoCells(GeoCoordinate, Resolution)
	 * @param location
	 * @param resolution
	 * @return
	 */

	public static GeoCell geoCell(GeoCoordinate location, Resolution resolution) {
		return createGeoCell(location, resolution);
	}

	/**
	 * @param location
	 * @return
	 */
	private static Point point(GeoCoordinate location) {
		Point point = new Point(location.getLatitude().getInDecimalDegrees(), location.getLongitude()
				.getInDecimalDegrees());
		return point;
	}

	/**
	 * Given a point this will return at least one up to four boxes representing the area around the
	 * point. 4 boxes because a point can land exactly at the intersection of no more than 4 (0,0) =
	 * (9,3,6,c)
	 * 
	 * @param location
	 * @param resolution
	 * @return
	 */
	public static GeoCells geoCells(GeoCoordinate location, Resolution resolution, Length halfDiagonalLength) {
		/*
		 * General thought process for streamlining this method. 1. find box and bounds containing
		 * point. 2. determine if point within buffer distance from edge(s) 3. add 1 or 3 adjacent
		 * cells if point within buffer distance or near corner.
		 */
		GeoCell cell = createGeoCell(location, resolution);
		BoundingBox cellBox = boundingBoxFromGeoCell(cell);
		GeoCells.Builder geoCells = GeoCells.create();
		geoCells.add(cell);

		Boolean addedEast = false;
		Boolean addedNorth = false;
		Boolean addedSouth = false;

		GeoCoordinate northSide = GeoCoordinate.create().setLatitude(cellBox.getNorth())
				.setLongitude(location.getLongitude()).build();
		if (QuantityMath.create(GeoMath.length(location, northSide)).lessThanEqualTo(halfDiagonalLength)) {
			geoCells.add(adjacentNorthOf(cell));
			addedNorth = true;
		}

		GeoCoordinate eastSide = GeoCoordinate.create().setLatitude(location.getLatitude())
				.setLongitude(cellBox.getEast()).build();
		if (QuantityMath.create(GeoMath.length(location, eastSide)).lessThanEqualTo(halfDiagonalLength)) {
			GeoCell eastCell = adjacentEastOf(cell);
			geoCells.add(eastCell);
			if (addedNorth) {
				geoCells.add(adjacentNorthOf(eastCell));
			}
			addedEast = true;
		}

		GeoCoordinate southSide = GeoCoordinate.create().setLatitude(cellBox.getSouth())
				.setLongitude(location.getLongitude()).build();
		if (QuantityMath.create(GeoMath.length(location, southSide)).lessThanEqualTo(halfDiagonalLength)) {
			GeoCell southCell = adjacentSouthOf(cell);
			geoCells.add(southCell);
			if (addedEast) {
				geoCells.add(adjacentEastOf(southCell));
			}
			addedSouth = true;
		}

		GeoCoordinate westSide = GeoCoordinate.create().setLatitude(location.getLatitude())
				.setLongitude(cellBox.getWest()).build();
		if (QuantityMath.create(GeoMath.length(location, westSide)).lessThanEqualTo(halfDiagonalLength)) {
			GeoCell westCell = adjacentWestOf(cell);
			geoCells.add(westCell);
			if (addedSouth) {
				geoCells.add(adjacentSouthOf(westCell));
			}
			if (addedNorth) {
				geoCells.add(adjacentNorthOf(westCell));
			}
		}

		return geoCells.build();
	}

	/**
	 * - * Similar to {@link #geoCells(GeoCoordinate, Resolution, Length)} except provides a default
	 * - * length that works in most cases.
	 * 
	 * @param location
	 * @param resolution
	 * @return
	 */
	public static GeoCells geoCells(GeoCoordinate location, Resolution resolution) {
		// We need some small length to create a bounding box to work from. This avoids pitfalls
		// when the point falls between 4 geocells.
		Length halfDiagonalLength = MeasurementUtil.createLengthInMeters(1);
		return geoCells(location, resolution, halfDiagonalLength);
	}

	public static GeoCells geoCells(BoundingBox bounds) {
		return GeoCellBoundingBox.createGeoCell().setOther(bounds).build().getGeoCells();
	}

	/**
	 * Provides those geocells that are closest to the given center. Null geocells are filtered out
	 * so no nulls will be present in the results.
	 * 
	 * @param boundingBox
	 * @return
	 */
	public static List<GeoCell> closestToPoint(GeoCoordinate center, Iterable<GeoCell> geoCells) {
		Function<GeoCell, Length> proximityFunction = proximityFunction(center);
		final Function<Object, Comparable<Length>> castFunction = FunctionsExtended.castFunction();
		Function<GeoCell, Comparable<Length>> comparableProximityFunction = Functions.compose(	castFunction,
																								proximityFunction);
		return Ordering.natural().onResultOf(comparableProximityFunction)
				.sortedCopy(Iterables.filter(geoCells, Predicates.notNull()));
	}

	/**
	 * Provides the Length from the given target {@link GeoCoordinate} to the center of the
	 * {@link GeoCell} provided as input.
	 * 
	 * 
	 * @see BoundingBoxUtil#proximityFunction(GeoCoordinate)
	 * 
	 * @param target
	 *            the point being compared to the geocell bounds.
	 * @return
	 */
	private static Function<GeoCell, Length> proximityFunction(GeoCoordinate target) {
		Function<GeoCell, BoundingBox> boundingBoxFunction = boundingBoxFunction();
		Function<BoundingBox, Length> proximityFunction = BoundingBoxUtil.proximityFunction(target);
		Function<GeoCell, Length> geoCellProximityFunction = Functions.compose(proximityFunction, boundingBoxFunction);
		return geoCellProximityFunction;
	}

	/**
	 * @return
	 */
	private static Function<GeoCell, BoundingBox> boundingBoxFunction() {
		return new Function<GeoCell, BoundingBox>() {

			@Override
			public BoundingBox apply(GeoCell input) {
				return (input != null) ? boundingBoxFromGeoCell(input) : null;
			}
		};
	}

	/**
	 * Retrieve the geocell immediately east of the provided geocell (same resolution).
	 * 
	 * @param cell
	 * @return
	 */
	public static GeoCell adjacentEastOf(GeoCell cell) {
		return new GeoCell(GeocellUtils.adjacent(cell.getCellAsString(), EAST));
	}

	/**
	 * Retrieve the geocell immediately West of the provided geocell (same resolution).
	 * 
	 * @param cell
	 * @return
	 */
	public static GeoCell adjacentWestOf(GeoCell cell) {
		return new GeoCell(GeocellUtils.adjacent(cell.getCellAsString(), WEST));
	}

	/**
	 * Retrieve the geocell immediately North of the provided geocell (same resolution).
	 * 
	 * @param cell
	 * @return
	 */
	public static GeoCell adjacentNorthOf(GeoCell cell) {
		return new GeoCell(GeocellUtils.adjacent(cell.getCellAsString(), NORTH));
	}

	/**
	 * Retrieve the geocell immediately South of the provided geocell (same resolution).
	 * 
	 * @param cell
	 * @return
	 */
	public static GeoCell adjacentSouthOf(GeoCell cell) {
		return new GeoCell(GeocellUtils.adjacent(cell.getCellAsString(), SOUTH));
	}

	/**
	 * Given the location and a desired resolution this will return a box containing one to four
	 * cells that fully represent the desired point scaled to the desired resolution.
	 * 
	 * @param location
	 * @param resolution
	 * @return
	 */
	public static GeoCellBoundingBox boundingBox(GeoCoordinate location, Resolution resolution) {
		// perhaps not a good idea to have a default, but this is enough to pass those perfect
		// points (0,0)
		Length bufferWidth = MeasurementUtil.createLengthInMeters(1);
		return boundingBox(location, resolution, bufferWidth);
	}

	/**
	 * Provides the bounding box that represents the format...regular {@link BoundingBox} if the
	 * format is bounds, {@link GeoCellBoundingBox} if geocells are provided.
	 * 
	 * @param boundingBoxAsString
	 * @return
	 */
	public static BoundingBox boundingBox(String boundingBoxAsString) {
		BoundingBox result;
		try {
			if (BoundingBox.isValidBounds(boundingBoxAsString)) {
				result = BoundingBox.valueOf(boundingBoxAsString);

			} else {
				GeoCells geoCells = new GeoCells.Builder().setFromString(boundingBoxAsString).build();
				result = new GeoCellBoundingBox.Builder().setGeoCells(geoCells).build();
			}
		} catch (Exception e) {
			if (e instanceof InvalidBoundsException) {
				throw e;
			} else {
				throw new InvalidBoundsException(boundingBoxAsString);
			}
		}
		return result;
	}

	/**
	 * Casts or creates a {@link GeoCellBoundingBox} when given a regular {@link BoundingBox}.
	 * 
	 * @param boundingBox
	 * @return
	 */
	public static GeoCellBoundingBox geoCellBoundingBox(BoundingBox boundingBox) {
		if (boundingBox instanceof GeoCellBoundingBox) {
			return (GeoCellBoundingBox) boundingBox;
		} else {
			return GeoCellBoundingBox.createGeoCell().setOther(boundingBox).build();
		}
	}

	/**
	 * Given the location and a desired resolution this will return a box containing one to four
	 * cells that fully represent the desired point and the area around it indicated by the provided
	 * buffer.
	 * 
	 * @param location
	 * @param resolution
	 * @param halfDiagonalLength
	 *            the length from the point to the extreme points of the bounding box.
	 * @return
	 */
	public static GeoCellBoundingBox boundingBox(GeoCoordinate location, Resolution resolution,
			Length halfDiagonalLength) {
		GeoCellBoundingBox.Builder builder = GeoCellBoundingBox.createGeoCell();
		builder.maxGeoCellResolution(resolution);
		// using #createGeoCell doesn't return a fair representation
		builder.setNorthEast(GeoMath.coordinateFrom(location, MeasurementUtil.NORTH_EAST, halfDiagonalLength));
		builder.setSouthWest(GeoMath.coordinateFrom(location, MeasurementUtil.SOUTH_WEST, halfDiagonalLength));
		return builder.build();
	}

	/**
	 * Given a {@link GeoCell} value attempt, this will highlight those characters that are not
	 * allowed.
	 * 
	 * @param badCell
	 * @return
	 */
	public static String highlightInvalidCharacters(String badCell) {
		Character[] characters = ArrayUtils.toObject(ALPHABET.toCharArray());
		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(badCell)
				.addCharacterIsAllowed(characters).build();
		return finder.getHighlightedInvalidCharacters();
	}

	/**
	 * Given a set of GeoCells, this will return a set of single-celled bounding boxes representing
	 * each geoCell with it's appropriate boundaries.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static BoundingBoxes boundingBoxes(Set<GeoCell> geoCells) {
		HashSet<GeoCellBoundingBox> boundingBoxes = new HashSet<>();
		for (GeoCell geoCell : geoCells) {
			GeoCellBoundingBox box = GeoCellBoundingBox.createGeoCell().setGeoCells(geoCell).build();
			boundingBoxes.add(box);
		}
		return BoundingBoxes.create().setAll(boundingBoxes).build();
	}

	/**
	 * Given a bounding box, this will generate single-celled bounding boxes for each cell resulting
	 * from the highest resolution of geocells that produce no more than max boxes.
	 * 
	 * @see #generateGeoCells(BoundingBox, Resolution, Boolean, Integer)
	 * @param boundary
	 * @param maxBoxes
	 * @return
	 */
	public static BoundingBoxes boundingBoxes(BoundingBox boundary, Integer maxBoxes) {
		return boundingBoxes(generateGeoCells(boundary, MAX_GEOCELL_RESOLUTION, DONT_MINIMIZE_RESULTS, maxBoxes));
	}

	/**
	 * @see #boundingBoxes(BoundingBox, Integer)
	 * @param boundary
	 * @return
	 */
	public static BoundingBoxes boundingBoxes(BoundingBox boundary) {
		return boundingBoxes(boundary, NUM_OF_CHILD_CELLS_IN_CELL * NUM_OF_CHILD_CELLS_IN_CELL);
	}

	/**
	 * Given a bounding box this will return a {@link GeoCellBoundingBox} that contains this
	 * bounding box.
	 * 
	 * @see GeoCellBoundingBox.Builder#setOther(BoundingBox)
	 * 
	 * @param bounds
	 */
	public static GeoCellBoundingBox boundingBox(BoundingBox bounds) {
		GeoCellBoundingBox result;
		if (bounds instanceof GeoCellBoundingBox) {
			result = (GeoCellBoundingBox) bounds;
		} else {
			result = GeoCellBoundingBox.createGeoCell().setOther(bounds).build();
		}
		return result;
	}

	/**
	 * Computes the minimum circle that will contain the provided GeoCells. Uses the GeoCell's
	 * bounding box as the parameter. Provides the center as {@link GeoCoordinate} and the radius
	 * (calculated using latitudes only) as {@link Length}.
	 * 
	 * @param geocells
	 * @return a {@link Pair} with the center of the minimum circle and the radius.
	 */
	public static Pair<GeoCoordinate, Length> minimumCircle(GeoCells geocells) {
		if (geocells.isEmpty()) {
			throw new IllegalArgumentException("GeoCells may not be empty.");
		}
		MultiPoint coords = JtsUtil.toMultiPoint(JtsGeoCellUtil.boxCoordinates(geocells));
		MinimumBoundingCircle circle = new MinimumBoundingCircle(coords);
		GeoCoordinate center = getCenter(circle);
		GeoCoordinate pointOnEdge = getPointOnEdge(circle);
		Length radius = GeoMath.length(center, pointOnEdge);
		return Pair.of(center, radius);
	}

	private static GeoCoordinate getPointOnEdge(MinimumBoundingCircle circle) {
		GeoCoordinate center = getCenter(circle);
		double lat = center.getLatitude().getInDecimalDegrees();
		double edgeLat;
		if (lat + circle.getRadius() > Latitude.MAX) {
			edgeLat = lat - circle.getRadius();
		} else {
			edgeLat = lat + circle.getRadius();
		}
		return new GeoCoordinate.Builder().setLatitude(edgeLat)
				.setLongitude(center.getLongitude().getInDecimalDegrees()).build();
	}

	private static GeoCoordinate getCenter(MinimumBoundingCircle circle) {
		return JtsMeasureUtil.convert(circle.getCentre());
	}

	/**
	 * @param a
	 * @return
	 */
	public static GeoCells geoCells(GeoCell... geoCells) {
		return GeoCells.create().add(geoCells).build();
	}

	public static Function<String, GeoCell> geoCellFunction() {
		return new Function<String, GeoCell>() {

			@Override
			public GeoCell apply(String input) {
				return (input != null) ? new GeoCell(input) : null;
			}
		};
	}

	/**
	 * Create a GeoCell using this when you can't be sure of it's contents (i.e. input from API).
	 * 
	 * @param value
	 * @return
	 */
	public static GeoCell geoCellValidated(String value) {
		value = value.trim().toLowerCase();
		if (!GeocellUtils.isValid(value)) {
			throw new InvalidGeoCellException(value);
		}
		return new GeoCell(value);
	}

	/**
	 * @param spatialBuffer
	 */
	public static SortedSet<GeoCell> geoCellsSet(Iterable<String> cellsAsStrings) {
		return convertToGeoCellsSet(cellsAsStrings);
	}

}
