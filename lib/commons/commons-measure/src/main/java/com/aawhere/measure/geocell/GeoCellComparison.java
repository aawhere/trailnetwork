/**
 * 
 */
package com.aawhere.measure.geocell;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aawhere.lang.ComparatorResult;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.ExtraUnits;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

/**
 * Used to do comparison of two sets of {@link GeoCells}. These comparisons are typically for
 * spatial comparisons similar to <a href="http://www.vividsolutions.com/jts/tests/index.html">JTS
 * Multi-Polygon comparisons</a> (see Relate AA).
 * 
 * This is a stateful utility since there may need to be preparations/validations done to the
 * geocells prior to relation execution for performance and consistency sake. it also provides
 * multiple ways of providing GeoCells without having to provide duplicate methods for each.
 * 
 * This methods provided act as a typical mathematical equal and relational operator going from left
 * to right. http://en.wikipedia.org/wiki/Relational_operator
 * 
 * left.contains(right) left.equals(right) left.within(right)
 * 
 * 
 * This is best used as postfix notation:
 * 
 * GeoCellComparison.left(cells1).right(cells2).contains();
 * 
 * Notice build is called by the alias of the {@link #contains()} method in Builder. For situations
 * where multiple operations are needed then use this as a typical builder by calling
 * {@link Builder#build()}.
 * 
 * 
 * <pre>
 * Limitations:
 * - Only does  {@link Sets} comparisons and no other logic is applied. The meaning of the geocell is ignored
 * - Doesn't ensure consistent parent inclusion ([a,ab].contains([ab]) == false)
 * - Touches is not implemented.  
 *   This would go beyond just the cell letters and have to convert to Geo boundaries to look spatially
 * -Past Limitations:
 * - TN-465 fixes and testDifferentResolutions proves: Requires same resolution (i.e. minimize doesn't work). [a].contains[ab] == false)
 * </pre>
 * 
 * @author aroller
 * 
 */
public class GeoCellComparison {

	/**
	 * Used to construct all instances of GeoCellComparison.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCellComparison> {

		/**
		 * 
		 */
		private static final Logger LOGGER = LoggerFactory.getLogger(GeoCellComparison.class);
		private Ratio ratioOfCourseCellsTolerated;

		public Builder() {
			super(new GeoCellComparison());
		}

		public Builder left(Set<GeoCell> left) {
			left(GeoCells.create().addAll(left).build());
			return this;
		}

		public Builder left(GeoCells left) {
			building.left = left;
			return this;
		}

		public Builder right(Iterable<GeoCell> right) {
			right(GeoCells.create().addAll(right).build());
			return this;
		}

		public Builder right(GeoCells right) {
			building.right = right;
			return this;
		}

		public Builder right(GeoCell... right) {
			return right(new HashSet<GeoCell>(Arrays.asList(right)));
		}

		public Boolean contains() {
			return build().contains();
		}

		/**
		 * Allowance for the number of cells to not be matched. This is a direct number that has no
		 * relation to the number of cells being considered. Use {@link #tolerance(Ratio)} for that
		 * sliding ratio.
		 * 
		 * If this tolerance is used in coordination with {@link #tolerance(Ratio)} this will be
		 * added to the result of the value determined from the ratio thereby giving a relative
		 * tolerance with a minimum.
		 * 
		 * @param tolerance
		 * @return
		 */
		public Builder tolerance(Integer tolerance) {
			building.tolerance = tolerance;
			return this;
		}

		/**
		 * A more appropriate use of {@link #tolerance(Integer)}, this will assign the number of
		 * cells tolerated based on the size of the {@link #right(GeoCell...)}. Ratio of zero is no
		 * tolerance, ratio of one is complete tolerance.
		 * 
		 * @param ratioOfRightCellsTolerated
		 * @return
		 */
		public Builder tolerance(Ratio ratioOfRightCellsTolerated) {
			this.ratioOfCourseCellsTolerated = ratioOfRightCellsTolerated;
			return this;
		}

		@Override
		public GeoCellComparison build() {
			GeoCellComparison built = super.build();
			if (this.ratioOfCourseCellsTolerated != null) {
				// add the relative tolerance to the minimum allowed tolerance.
				built.tolerance += (int) Math.floor(ratioOfCourseCellsTolerated.doubleValue(ExtraUnits.RATIO)
						* built.right.size());
			}
			final Resolution leftResolution = built.left.getMaxResolution();
			final Resolution rightResolution = built.right.getMaxResolution();
			if (ComparatorResult.isLessThan(leftResolution.compareTo(rightResolution))) {
				if (LOGGER.isLoggable(Level.FINE)) {
					LOGGER.fine(rightResolution + " is right's resoluation being reduced to left's: " + leftResolution);
				}
				built.right = GeoCellsReducer.create().setGeoCells(built.right).setMaxResolution(leftResolution)
						.build().getReduced();
			}
			built.leftSet = built.left.getAll();
			built.rightSet = built.right.getAll();
			return built;
		}

		/**
		 * Shorthand for {@link #build()}.{@link #intersects()}.
		 * 
		 * @return
		 */
		public Boolean intersects() {
			return build().intersects();
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	public static Builder left(Set<GeoCell> left) {
		final Builder builder = new Builder();
		builder.left(left);
		return builder;
	}

	public static Builder left(GeoCells left) {
		return left(left.getAll());
	}

	/**
	 * @param a
	 * @return
	 */
	public static Builder left(GeoCell... left) {
		return left(new HashSet<GeoCell>(Arrays.asList(left)));
	}

	// the sets are handy for common fast access.
	private SortedSet<GeoCell> leftSet;
	private SortedSet<GeoCell> rightSet;
	private GeoCells left;
	private GeoCells right;
	/**
	 * Tolerance is a fudge factor since edge cases can cause slight differences. This is reasonable
	 * to provide since, let's be honest, geocell is not exactly a perfect representation of
	 * anything on earth.
	 * 
	 * TN-380
	 * 
	 */
	private Integer tolerance = 0;
	private Boolean within;
	private Boolean contains;
	private Boolean disjoint;

	/** Use {@link Builder} to construct GeoCellComparison */
	private GeoCellComparison() {
	}

	/**
	 * left contains right if all of right is within left or in other words right doesn't have any
	 * cells not found in left.
	 * 
	 * @see #within()
	 * @see Sets#difference(Set, Set)
	 * @return
	 */
	public Boolean contains() {
		if (this.contains == null) {
			this.contains = withinTolerance(Sets.difference(rightSet, leftSet));
		}
		return contains;
	}

	private Boolean withinTolerance(SetView<GeoCell> result) {
		return result.size() <= this.tolerance;
	}

	/**
	 * left is within right if all cells fall within right or if left doesn't have any cells that
	 * aren't in right.
	 * 
	 * @see Sets#difference(Set, Set)
	 * 
	 * @see #contains()
	 * @return
	 */
	public Boolean within() {
		if (this.within == null) {
			this.within = withinTolerance(Sets.difference(leftSet, rightSet));
		}
		return within;
	}

	public Boolean intersects() {
		return !disjoint();
	}

	public Boolean disjoint() {
		if (this.disjoint == null) {
			this.disjoint = withinTolerance(Sets.intersection(leftSet, rightSet));
		}
		return disjoint;
	}

	/**
	 * Everything must be the same. Tolerance is ignored.
	 * 
	 * @return
	 */
	public Boolean equals() {
		return rightSet.equals(leftSet);
	}

	/**
	 * All cells must be equal, contained in each other, within tolerance.
	 * 
	 * @see #contains()
	 * @see #within()
	 * @see #equals()
	 */
	public Boolean equalsWithinTolerance() {
		return within() && contains();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return leftSet.toString() + " -> " + rightSet.toString();
	}

}
