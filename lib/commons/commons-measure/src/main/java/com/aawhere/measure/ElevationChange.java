package com.aawhere.measure;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;

/**
 * Represents either the Elevaton Gain or Elevation Loss over a distance. Uses {@link Length} and
 * it's appropriate Units internally.
 * 
 * @author Brian Chapman
 * 
 */
public interface ElevationChange
		extends Quantity<ElevationChange> {

}
