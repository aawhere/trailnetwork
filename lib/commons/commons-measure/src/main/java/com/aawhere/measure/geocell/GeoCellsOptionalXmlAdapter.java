/**
 * 
 */
package com.aawhere.measure.geocell;

import com.aawhere.xml.bind.OptionalPassThroughXmlAdapter;
import com.aawhere.xml.bind.OptionalXmlAdapterFactory;

/**
 * Provides the option to show GeoCells if desired. All this does is tells JAXB what type to expect.
 * The configuration is done in {@link OptionalXmlAdapterFactory}.
 * 
 * @author aroller
 * 
 */
public class GeoCellsOptionalXmlAdapter
		extends OptionalPassThroughXmlAdapter<GeoCells> {

	/**
	 * 
	 */
	public GeoCellsOptionalXmlAdapter() {
		super();
	}

	/**
	 * @param show
	 */
	public GeoCellsOptionalXmlAdapter(Boolean show) {
		super(show);
	}

}
