/**
 * 
 */
package com.aawhere.joda.time;

import org.joda.time.Interval;

import com.google.common.base.Function;

/**
 * Expands all intervals into a single {@link Interval} that will encompass all given intervals. In
 * other words it finds the earliest min and the latest max of all intervals.
 * 
 * @author aroller
 * 
 */
public class IntervalExpandingFunction
		implements Function<Interval, Interval> {

	public static IntervalExpandingFunction build() {
		return new IntervalExpandingFunction();
	}

	private Interval expanded;

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Interval apply(Interval input) {
		if (input == null) {
			return null;
		}
		if (expanded == null) {
			expanded = input;
		} else {
			expanded = IntervalUtil.expanded(expanded, input);
		}
		return expanded;
	}

	/**
	 * @return the expanded
	 */
	public Interval expanded() {
		return this.expanded;
	}

}
