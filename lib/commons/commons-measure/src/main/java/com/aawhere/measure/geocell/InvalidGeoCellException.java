/**
 * 
 */
package com.aawhere.measure.geocell;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.measure.MeasureMessage;
import com.aawhere.measure.MeasureMessage.Param;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

import com.beoui.geocell.GeocellUtils;

/**
 * Indicates a {@link GeoCell} value returns false when tested against
 * {@link GeocellUtils#isValid(String)}.
 * 
 * TODO:Consider changing this to a checked exception, but {@link GeocellUtils#isValid(String)}
 * should be used instead of relying on catching this.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class InvalidGeoCellException
		extends BaseRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7612547935389136744L;

	/**
	 * 
	 */
	public InvalidGeoCellException(String badCell) {
		super(CompleteMessage.create(MeasureMessage.INVALID_GEO_CELL).addParameter(Param.GEO_CELL, badCell)
				.addParameter(Param.GEO_CELL_ALPHABET, GeoCellUtil.ALPHABET)
				.addParameter(Param.HIGHLIGHTED_INVALID_CHARACTERS, GeoCellUtil.highlightInvalidCharacters(badCell))
				.build());

	}
}
