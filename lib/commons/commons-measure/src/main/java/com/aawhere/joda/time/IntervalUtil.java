/**
 * 
 */
package com.aawhere.joda.time;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * @author aroller
 * 
 */
public class IntervalUtil {

	public static Interval findOverlap(Iterable<Interval> intervals, Interval target) {
		IntervalOverlapPredicate predicate = new IntervalOverlapPredicate(target);
		return Iterables.find(intervals, predicate);
	}

	/**
	 * Given a collection of intervals this will return an natural ordered list of intervals where
	 * the resulting intervals will contain all times from the provided intervals, however, none of
	 * the resulting intervals will overlap each other.
	 * 
	 * @param all
	 * @return
	 */
	public static List<Interval> minimizeOverlappingIntervals(Iterable<Interval> all) {
		IntervalStartComparator startComparator = new IntervalStartComparator();
		List<Interval> minimized = new ArrayList<>();
		for (Interval interval : all) {
			boolean overlapDetected = false;
			for (int i = 0; i < minimized.size() && !overlapDetected; i++) {
				Interval target = minimized.get(i);
				if (target.overlaps(interval)) {
					overlapDetected = true;
					Interval expandedTarget = expanded(interval, target);
					minimized.set(i, expandedTarget);
					// ensure newly expanded doesn't overlap existing
					minimized = minimizeOverlappingIntervals(minimized);
				}
			}
			// no overlaps so this interval is unique and added to the result list
			if (!overlapDetected) {
				minimized.add(interval);
				Collections.sort(minimized, startComparator);
			}
		}

		return minimized;
	}

	public static Interval expanded(Interval first, Interval second) {
		return new Interval(min(first.getStart(), second.getStart()), max(first.getEnd(), second.getEnd()));
	}

	/**
	 * Expands all given intervals into a single interval that encompasses all given intervals.
	 * Finds the earliest start and the latest end.
	 * 
	 * @see IntervalExpandingFunction
	 * @param intervals
	 * @return
	 */
	public static Interval expanded(Iterable<Interval> intervals) {
		IntervalExpandingFunction function = IntervalExpandingFunction.build();
		Iterables.transform(intervals, function);
		return function.expanded();
	}

	public static DateTime min(DateTime left, DateTime right) {
		return (left.isBefore(right)) ? left : right;
	}

	public static DateTime max(DateTime left, DateTime right) {
		return (left.isAfter(right)) ? left : right;
	}

	/**
	 * @return
	 */
	public static Function<Interval, DateTime> startTimeFuncton() {
		return new Function<Interval, DateTime>() {

			@Override
			public DateTime apply(Interval input) {
				return (input != null) ? input.getStart() : null;
			}
		};
	}

	/**
	 * Subtracts the given duration from both the start and finish times of the
	 * interval...essentially shifting it earlier.
	 * 
	 * @see #plus(Interval, Duration)
	 * @param interval
	 * @param slowerduration
	 * @return
	 */
	public static Interval minus(Interval interval, Duration duration) {
		return new Interval(interval.getStart().minus(duration), interval.getEnd().minus(duration));
	}

	/**
	 * Shifts the given interval the duration given into the future relative to the given interval.
	 * 
	 * @see #minus(Interval, Duration)
	 * @param interval
	 * @param duration
	 * @return
	 */
	public static Interval plus(Interval interval, Duration duration) {
		return new Interval(interval.getStart().plus(duration), interval.getEnd().plus(duration));
	}

}
