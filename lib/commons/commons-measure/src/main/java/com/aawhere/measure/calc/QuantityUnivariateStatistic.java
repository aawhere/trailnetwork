/**
 * 
 */
package com.aawhere.measure.calc;

import javax.measure.quantity.Quantity;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.Unit;

import org.apache.commons.math.stat.descriptive.StorelessUnivariateStatistic;
import org.apache.commons.math.stat.descriptive.UnivariateStatistic;
import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.rank.Max;
import org.apache.commons.math.stat.descriptive.rank.Min;
import org.apache.commons.math.stat.descriptive.summary.Sum;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.math.stats.Gain;
import com.aawhere.math.stats.Loss;

/**
 * Given one or more {@link Quantity} values this will determine the {@link UnivariateStatistic} (a
 * single valued statistic) based on the implementation provided.
 * 
 * The {@link #unit} is assigned by the first use of a quantity. It is best to keep the unit
 * homogeneous, but this should still work fine if it is heterogeneous. Add a mutator to the builder
 * if a Unit should be declarative.
 * 
 * @author Aaron Roller
 * 
 */
public class QuantityUnivariateStatistic<Q extends Quantity<Q>, U extends StorelessUnivariateStatistic> {

	/**
	 * Used to construct all instances of QuantityExtremeCalculator.
	 */
	public static class Builder<Q extends Quantity<Q>, U extends StorelessUnivariateStatistic>
			extends ObjectBuilder<QuantityUnivariateStatistic<Q, U>> {

		public Builder() {
			super(new QuantityUnivariateStatistic<Q, U>());
		}

		public Builder<Q, U> setUnivariateStatistic(U univariateStatistic) {
			building.univariateStatistic = univariateStatistic;
			return this;
		}

		public Builder<Q, U> setFactory(Class<Q> quantityType) {
			building.quantityFactory = QuantityFactory.getInstance(quantityType);
			return this;
		}

		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("quantityFactory", building.quantityFactory);
			Assertion.exceptions().notNull("univariateStatistic", building.univariateStatistic);

		}

	}// end Builder

	/**
	 * Uses {@link Min}.
	 * 
	 * @return
	 */
	public static <Q extends Quantity<Q>> QuantityUnivariateStatistic<Q, Min> newMin(Class<Q> quantityType) {

		return new Builder<Q, Min>().setUnivariateStatistic(new Min()).setFactory(quantityType).build();
	}

	/**
	 * Uses {@link Max}.
	 * 
	 * @return
	 */
	public static <Q extends Quantity<Q>> QuantityUnivariateStatistic<Q, Max> newMax(Class<Q> quantityType) {
		return new Builder<Q, Max>().setUnivariateStatistic(new Max()).setFactory(quantityType).build();
	}

	/**
	 * uses {@link Mean}
	 * 
	 * @return
	 */
	public static <Q extends Quantity<Q>> QuantityUnivariateStatistic<Q, Mean> newMean(Class<Q> quantityType) {
		return new Builder<Q, Mean>().setUnivariateStatistic(new Mean()).setFactory(quantityType).build();
	}

	private static Sum zeroSum() {
		Sum sum = new Sum();
		sum.increment(0);
		return sum;
	}

	/**
	 * uses {@link Sum}
	 * 
	 * @return
	 */
	public static <Q extends Quantity<Q>> QuantityUnivariateStatistic<Q, Sum> newSum(Class<Q> quantityType) {
		final Builder<Q, Sum> builder = new Builder<Q, Sum>();

		return builder.setUnivariateStatistic(zeroSum()).setFactory(quantityType).build();
	}

	public static <Q extends Quantity<Q>> QuantityUnivariateStatistic<Q, Gain> newGain(Class<Q> quantityType) {
		return new Builder<Q, Gain>().setUnivariateStatistic(new Gain(zeroSum())).setFactory(quantityType).build();
	}

	public static <Q extends Quantity<Q>> QuantityUnivariateStatistic<Q, Loss> newLoss(Class<Q> quantityType) {
		return new Builder<Q, Loss>().setUnivariateStatistic(new Loss(zeroSum())).setFactory(quantityType).build();
	}

	/** Use {@link Builder} to construct QuantityExtremeCalculator */
	private QuantityUnivariateStatistic() {
	}

	/** The calculator */
	private U univariateStatistic;

	/**
	 * The unit that will be used for statistical analysis and creating the result. Assigned by the
	 * first use of quantity.
	 */
	private Unit<Q> unit;
	private QuantityFactory<Q> quantityFactory;

	/**
	 * Used to add a quantity to the {@link #univariateStatistic}
	 * 
	 * @param quantity
	 * @return true if the given quantity is greater than the current maximum.
	 */
	public void increment(Q quantity) {
		if (getUnit() == null) {
			this.unit = quantity.getUnit();
		}
		this.univariateStatistic.increment(quantity.doubleValue(getUnit()));
	}

	/**
	 * Resets the value back to zero allowing this to be re-used to calculate a new sum. Useful
	 * during iterations that need to calculate new values when another has been achieved.
	 * 
	 * @see {@link StorelessUnivariateStatistic#clear()}
	 */
	public void clear() {
		this.univariateStatistic.clear();
	}

	/**
	 * Returns the resulting {@link UnivariateStatistic} as a {@link Quantity} with the {@link Unit}
	 * .
	 * 
	 * @see #hasResult()
	 * 
	 * @return the result. If throws exception
	 */
	public Q getResult() {
		return this.quantityFactory.create(univariateStatistic.getResult(), getUnit());
	}

	/**
	 * @return
	 */
	private Unit<Q> getUnit() {
		if (this.unit == null) {
			return this.quantityFactory.getMetricUnit();
		}
		return this.unit;
	}

	/**
	 * Used to check if there is a quantity ready. {@link #getResult()} will not return a null so
	 * this method should be used to avoid a {@link NullPointerException}.
	 * 
	 * @return
	 */
	public Boolean hasResult() {
		return !Double.isNaN(this.univariateStatistic.getResult());
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		Class<? extends StorelessUnivariateStatistic> statType = this.univariateStatistic.getClass();
		final int prime = 31;
		int result = 1;
		result = prime * result + ((statType == null) ? 0 : statType.hashCode());
		result = prime * result + ((getUnit() == null) ? 0 : getUnit().hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuantityUnivariateStatistic other = (QuantityUnivariateStatistic) obj;
		if (this.univariateStatistic == null) {
			if (other.univariateStatistic == null)
				return false;
		} else if (!this.univariateStatistic.getClass().equals(other.univariateStatistic.getClass()))
			return false;
		if (getUnit() == null) {
			if (other.unit != null)
				return false;
		} else if (!getUnit().equals(other.unit))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.univariateStatistic.getClass().getSimpleName() + " of " + String.valueOf(getUnit());
	}
}
