/**
 * 
 */
package com.aawhere.joda.time;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * @author aroller
 * 
 */
public enum JodaTimeMessage implements Message {

	/** We can't go back in time...at least not in our system. */
	NEGATIVE_DURATION("{FIRST} must not occur after {SECOND}.", Param.FIRST, Param.SECOND),
	/** Indicates that a positive duration is expected, but zero was found. */
	ZERO_DURATION(
			"Two timestamps of {FIRST} produce an unacceptable zero duration for {CONTEXT}.",
			Param.FIRST,
			Param.CONTEXT),
	/**
	 * Indicates that a timestamp is required, but may be used in many places since the message is
	 * general.
	 */
	TIMESTAMP_REQUIRED("The value you provided requires a timestamp, but has none. {CONTEXT}", Param.CONTEXT), ;

	private ParamKey[] parameterKeys;
	private String message;

	private JodaTimeMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** ORDERED_TIMES */
		FIRST, SECOND,
		/** Explains what the interval is for */
		CONTEXT;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
