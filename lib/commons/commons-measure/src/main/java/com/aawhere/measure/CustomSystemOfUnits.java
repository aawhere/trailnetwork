/**
 *
 */
package com.aawhere.measure;

import javax.measure.unit.MetricSystem;

import com.aawhere.measure.unit.ExtraUnits;

/**
 * Additional {@link MetricSystem} units.
 * 
 * @author Aaron Roller
 * @deprecated use {@link ExtraUnits}.
 */

@Deprecated
public class CustomSystemOfUnits
		extends ExtraUnits {

}
