/**
 *
 */
package com.aawhere.measure;

import javax.measure.quantity.Quantity;

import com.aawhere.field.type.DataTypeAdapter;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.exception.BaseException;

/**
 * {@link DataTypeAdapter} for {@link Quantity}
 * 
 * @author Brian Chapman
 * 
 */
@SuppressWarnings("rawtypes")
public class QuantityDataTypeNumberAdapter
		extends DataTypeAdapter<Quantity> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldData marshal(Object adaptee) {
		if (adaptee instanceof Quantity) {
			Quantity<?> quantity = (Quantity<?>) adaptee;
			@SuppressWarnings("unchecked")
			Double value = quantity.doubleValue(MeasurementUtil.getNormalizedUnit(quantity.getClass()));
			return new FieldData.Builder().setType(getFieldDataType()).setValue(value).build();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#unmarshal(java.lang.Object, java.lang.Class)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public <A extends Quantity> A unmarshal(FieldData adapted, Class<A> type) throws BaseException {
		if (adapted.getValue() instanceof Number) {
			Double value = ((Number) adapted.getValue()).doubleValue();
			return (A) MeasurementUtil.normalizedQuantityOrNull(value, type);
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@Override
	public Class<Quantity> getType() {
		return Quantity.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.DataTypeAdapter#getFieldDataType()
	 */
	@Override
	public FieldDataType getFieldDataType() {
		return FieldDataType.NUMBER;
	}

}
