/**
 * 
 */
package com.aawhere.measure.calc;

import static com.aawhere.math.PlaceValueNotation.TENTH;

import java.util.HashMap;

import javax.measure.quantity.Quantity;
import javax.measure.unit.Dimension;
import javax.measure.unit.Unit;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.math.PlaceValueNotation;
import com.aawhere.measure.MeasurementUtil;

/**
 * Provides desired precision based on the {@link Dimension} or {@link Quantity} type if more
 * specific.
 * 
 * A precision may be provided for a {@link Quantity} type which will be the precision used. If that
 * is not provided then it will look to see if a precision for the Dimension for a Unit has been
 * provided. If still no precision is found the the default for the base unit will be provided from
 * {@link #DEFAULT_PRECISION_FOR_BASE_UNIT}.
 * 
 * @author Aaron Roller
 * 
 *         TODO:Make this injectible with Guice
 * 
 */
public class QuantityPrecision {

	/**
	 * Used to construct all instances of QuantityPrecision.
	 */
	public static class Builder
			extends ObjectBuilder<QuantityPrecision> {

		public Builder() {
			super(new QuantityPrecision());
		}

		public Builder setPrecision(Dimension dimension, PlaceValueNotation place) {
			building.precisions.put(dimension, place);
			return this;
		}

		public Builder setPrecision(Class<? extends Quantity<?>> quantityType, PlaceValueNotation place) {
			building.precisions.put(quantityType, place);
			return this;
		}
	}// end Builder

	/** Use {@link Builder} to construct QuantityPrecision */
	private QuantityPrecision() {
	}

	/**
	 * 
	 */
	static final PlaceValueNotation DEFAULT_PRECISION_FOR_BASE_UNIT = TENTH;

	private HashMap<Object, PlaceValueNotation> precisions = new HashMap<Object, PlaceValueNotation>();

	/** Provides the precision based on the quantity given. */
	public <Q extends Quantity<Q>> Double precisionValueForQuantity(Q quantity) {
		PlaceValueNotation result = precisionPlaceFor(quantity);
		return result.getNumericRepresentation();
	}

	/**
	 * @param quantity
	 * @return
	 */
	public <Q extends Quantity<Q>> PlaceValueNotation precisionPlaceFor(Q quantity) {
		PlaceValueNotation result = precisions.get(MeasurementUtil.classFor(quantity));
		if (result == null) {
			result = precisions.get(quantity.getUnit().getDimension());
		}

		if (result == null) {
			result = DEFAULT_PRECISION_FOR_BASE_UNIT;
		}
		return result;
	}

	/**
	 * provides a Quantity of the same type, but with the precision value for the Quantity type
	 * based on the rules described by this class.
	 */
	public <Q extends Quantity<Q>> Q precisionFor(Q quantity) {

		double precisionValue = precisionValueForQuantity(quantity);
		Unit<Q> baseUnit = MeasurementUtil.getNormalizedQuantity(quantity).getUnit();
		return MeasurementUtil.getFactory(quantity).create(precisionValue, baseUnit);
	}

}
