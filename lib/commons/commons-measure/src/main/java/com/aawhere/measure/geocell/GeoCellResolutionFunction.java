/**
 * 
 */
package com.aawhere.measure.geocell;

import javax.annotation.Nullable;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class GeoCellResolutionFunction
		implements Function<GeoCell, Resolution> {

	/**
	 * Used to construct all instances of GeoCellResolutionFunction.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCellResolutionFunction> {

		public Builder() {
			super(new GeoCellResolutionFunction());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct GeoCellResolutionFunction */
	private GeoCellResolutionFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public Resolution apply(@Nullable GeoCell input) {
		return input.getResolution();
	}

}
