/**
 * Parts of this class were taken from Alexandre Gelliber's work in http://code.google.com/p/javageomodel/
 *
 * Copyright 2010 Alexandre Gellibert
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.aawhere.measure.geocell;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.beoui.geocell.GeocellUtils;
import com.beoui.geocell.model.Point;

/**
 * 
 * Represents a {@link GeoCoordinate} using {@link GeoCell}s.
 * 
 * A geocell is a hexadecimal string that defines a two dimensional rectangular region inside the
 * [-90,90] x [-180,180] latitude/longitude space. A geocell's 'resolution' is its length. For most
 * practical purposes, at high resolutions, geocells can be treated as single points.
 * 
 * Much like geohashes (see http://en.wikipedia.org/wiki/Geohash), geocells are hierarchical, in
 * that any prefix of a geocell is considered its ancestor, with geocell[:-1] being geocell's
 * immediate parent cell.
 * 
 * To calculate the rectangle of a given geocell string, first divide the [-90,90] x [-180,180]
 * latitude/longitude space evenly into a 4x4 grid like so:
 * 
 * {@code
 *            +---+---+---+---+ (90, 180)
 *            | a | b | e | f |
 *            +---+---+---+---+
 *            | 8 | 9 | c | d |
 *            +---+---+---+---+
 *            | 2 | 3 | 6 | 7 |
 *            +---+---+---+---+
 *            | 0 | 1 | 4 | 5 |
 * (-90,-180) +---+---+---+---+
 * }
 * 
 * 
 * NOTE: The point (0, 0) is at the intersection of grid cells 3, 6, 9 and c. And, for example, cell
 * 7 should be the sub-rectangle from (-45, 90) to (0, 180).
 * 
 * Calculate the sub-rectangle for the first character of the geocell string and re-divide this
 * sub-rectangle into another 4x4 grid. For example, if the geocell string is '78a', we will
 * re-divide the sub-rectangle like so:
 * 
 * {@code
 *              .                   .
 *              .                   .
 *          . . +----+----+----+----+ (0, 180)
 *              | 7a | 7b | 7e | 7f |
 *              +----+----+----+----+
 *              | 78 | 79 | 7c | 7d |
 *              +----+----+----+----+
 *              | 72 | 73 | 76 | 77 |
 *              +----+----+----+----+
 *              | 70 | 71 | 74 | 75 |
 * . . (-45,90) +----+----+----+----+ . .
 *             .                   .
 *             .                   .
 * }
 * 
 * Continue to re-divide into sub-rectangles and 4x4 grids until the entire geocell string has been
 * exhausted. The final sub-rectangle is the rectangular region for the geocell.
 * 
 * {@code
 * Resolution	Width of cell at equator(m)
 * 1	        10,018,790.00
 * 2	        2,504,697.50
 * 3	        626,174.38
 * 4	        156,543.59
 * 5	        39,135.90
 * 6	        9,783.97
 * 7	        2,445.99
 * 8	        611.50
 * 9	        152.87
 * 10	        38.22
 * 11	        9.55
 * 12	        2.39
 * 13	        0.60 (MAX_RESOLUTION)
 * 14	        0.15
 * 15	        0.04
 * }
 * 
 * The height and width of the cell depends on its location. This is because of the way latitude and
 * longitude values divide the geoid up. Generally speaking the height of a geocell will be greater
 * near the poles than near the equator and the width of a cell will be less near the poles than at
 * the equator, thus the geocell isn't a square or rectangle but rather a trapizoid.
 * 
 * @See python version of geocell:
 *      http://code.google.com/p/geomodel/source/browse/trunk/geo/geocell.py
 * 
 * @author api.roman.public@gmail.com (Roman Nurik)
 * @author (java portage) Alexandre Gellibert
 * 
 * 
 * @author Brian Chapman
 * 
 */
public class GeoCellCoordinate
		extends GeoCoordinate {

	private static final long serialVersionUID = -2854541670581692208L;

	// The maximum *practical* geocell resolution.
	public static final Resolution MAX_RESOLUTION = GeoCellUtil.MAX_GEOCELL_RESOLUTION;

	private List<GeoCell> geoCells = new ArrayList<GeoCell>();

	/**
	 * Required for xml marshaling
	 */
	@SuppressWarnings("unused")
	private GeoCellCoordinate() {
		super();
		calculateGeoCells();
	}

	/**
	 * Constructor providing Latitude first because non-cartographers prefer to say it that way.
	 * 
	 * @param latitude
	 * @param longitude
	 */
	public GeoCellCoordinate(Latitude latitude, Longitude longitude) {
		super(latitude, longitude);
		calculateGeoCells();

	}

	public GeoCellCoordinate(Double latitude, Double longitude) {
		super(latitude, longitude);
		calculateGeoCells();
	}

	/**
	 * Duplicate constructor providing arguments with x first since some cartographers prefer it
	 * that way.
	 * 
	 * @param longitude
	 * @param latitude
	 */
	public GeoCellCoordinate(Longitude longitude, Latitude latitude) {
		this(latitude, longitude);
	}

	public GeoCellCoordinate(GeoCoordinate coordinate) {
		this(coordinate.getLatitude(), coordinate.getLongitude());
	}

	/**
	 * Provides the geo cell for the supplied resolution.
	 * 
	 * @param resolution
	 *            the resolution of the {@link GeoCell} to retreive. Valid numbers are 1 to
	 *            MAX_RESOLUTION
	 * @return
	 */
	public GeoCell getGeoCell(Resolution resolution) {
		final int ARRAY_TO_RESOLUTION_OFFSET = 1;
		if (resolution.asInteger() > MAX_RESOLUTION.asInteger()) {
			throw new IllegalArgumentException("The maximum value of resolution is " + String.valueOf(MAX_RESOLUTION));
		}
		return geoCells.get(resolution.asInteger() - ARRAY_TO_RESOLUTION_OFFSET);
	}

	/**
	 * List of GeoCells ordered from resolution 1 = position 0 to MAX_RESOLUTION in the last
	 * position.
	 * 
	 * @return
	 */
	public List<GeoCell> getGeoCells() {
		return geoCells;
	}

	private void calculateGeoCells() {
		Point point = new Point(this.getLatitude().getInDecimalDegrees(), this.getLongitude().getInDecimalDegrees());
		for (int i = Resolution.ONE.asInteger(); i <= MAX_RESOLUTION.asInteger(); i++) {
			GeoCell cell = new GeoCell(GeocellUtils.compute(point, i));
			geoCells.add(cell);
		}
	}

}
