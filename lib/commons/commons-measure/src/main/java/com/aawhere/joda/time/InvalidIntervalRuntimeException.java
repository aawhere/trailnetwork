/**
 * 
 */
package com.aawhere.joda.time;

import org.joda.time.DateTime;

import com.aawhere.joda.time.JodaTimeMessage.Param;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Useful when reporting an interval that isn't possible to construct, typically due to the second
 * time being before or the same as the current.
 * 
 * This is runtime because it would typically be a developer error for providing these times in the
 * wrong order. Consider adding a pre-check to your times if you want to catch this OR create a
 * checked exception, but you typically do not catch runtimes.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.UNPROCESSABLE_ENTITY)
public class InvalidIntervalRuntimeException
		extends BaseRuntimeException {

	private static final long serialVersionUID = -2579840384116572633L;

	/**
	 * @param dynamicResourceBundleMessage
	 */
	public InvalidIntervalRuntimeException(DateTime youThoughtWasFirst, DateTime youThoughtWasSecond) {
		super(new CompleteMessage.Builder(JodaTimeMessage.NEGATIVE_DURATION)
				.addParameter(Param.FIRST, youThoughtWasFirst).addParameter(Param.SECOND, youThoughtWasSecond).build());
	}

	/**
	 * Used when the same timestamps produce an unnaceptable situation.
	 * 
	 * @param sameTimestamps
	 *            the first or second timestamp since they are the same
	 * @param context
	 *            an object that describes what the timestamps are being used for
	 */
	public InvalidIntervalRuntimeException(DateTime sameTimestamps, Object context) {
		super(new CompleteMessage.Builder(JodaTimeMessage.ZERO_DURATION).addParameter(Param.FIRST, sameTimestamps)
				.addParameter(Param.CONTEXT, context).build());
	}

}
