/**
 * 
 */
package com.aawhere.measure;

/**
 * Creates consistency and re-usability for those items that can provide a bounding box.
 * 
 * @author aroller
 * 
 */
public interface BoundingBoxProvider {

	BoundingBox boundingBox();
}
