/**
 * 
 */
package com.aawhere.joda.time;

import java.util.GregorianCalendar;

import javax.annotation.Nullable;
import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalDate;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aawhere.lang.ComparatorResult;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;

import com.google.common.base.Function;
import com.google.common.collect.Ordering;

/**
 * General helper utility to map system objects into Joda Time objects. Specific Utils may exist for
 * common Joda data structures like {@link DurationUtil}.
 * 
 * @author roller
 * 
 */
public class JodaTimeUtil {

	/**
	 * 
	 */
	private static final DateTimeFormatter ISO_DATE_PATTERN = DateTimeFormat.forPattern("yyyy-MM-dd");
	private static final int DEFAULT_ALLOWED_MILLIS = 1;
	private static final DateTimeFormatter RFC_1123_FORMATTER = rfc1123HeaderDateTimeFormatter();
	public static final Duration ZERO_DURATION = new Duration(0);

	/**
	 * @param gregorianCalendar
	 * @return the DateTime conversion or null if gregorianCalendar param is null
	 */
	public static DateTime createDateTime(GregorianCalendar gregorianCalendar) {
		if (gregorianCalendar == null) {
			return null;
		} else {
			return new DateTime(gregorianCalendar.getTimeInMillis());
		}

	}

	/**
	 * 
	 * @param gregorianCalendar
	 * @return the converted {@link DateTime} or null if gregorianCalendar param is null
	 */
	public static DateTime createDateTime(XMLGregorianCalendar gregorianCalendar) {
		DateTime result;
		if (gregorianCalendar == null) {
			result = null;
		} else {

			result = createDateTime(gregorianCalendar.toGregorianCalendar());
		}
		return result;

	}

	/**
	 * Given the timestamp this will return the standard fullly qualified version of the timestamp
	 * in String format matching the ISO format used in XML.
	 * 
	 * 
	 * @param timestamp
	 * @return
	 */
	public static String toIsoXmlFormat(DateTime timestamp) {
		return DatatypeConverter.printDateTime(timestamp.toGregorianCalendar());
	}

	public static Ratio ratioFrom(Duration portion, Duration total) {
		return MeasurementUtil.ratio(portion.getMillis(), total.getMillis());
	}

	/**
	 * Checks for equality within 1 millisecond tolerance.
	 * 
	 * @see #equalsWithinTolerance(DateTime, DateTime, int)
	 * 
	 * @param sourceTimestamp
	 * @param targetTimestamp
	 * @return
	 */
	public static boolean equalsWithinTolerance(DateTime sourceTimestamp, DateTime targetTimestamp) {
		return equalsWithinTolerance(sourceTimestamp, targetTimestamp, DEFAULT_ALLOWED_MILLIS);
	}

	/**
	 * @param sourceTimestamp
	 * @param targetTimestamp
	 * @param allowedMillis
	 * @return
	 */
	public static boolean equalsWithinTolerance(DateTime sourceTimestamp, DateTime targetTimestamp, int allowedMillis) {
		return Math.abs(sourceTimestamp.getMillis() - targetTimestamp.getMillis()) <= allowedMillis;
	}

	/**
	 * Given a valid formatted string this will parse it and return the {@link DateTime} with
	 * TimeZone information.
	 * 
	 * @param formattedTimestamp
	 */
	public static DateTime parseXmlDate(String formattedTimestamp) {
		return new DateTime(DatatypeConverter.parseDateTime(formattedTimestamp));
	}

	/**
	 * @see #compareToWithinTolerance(Duration, Duration, int)
	 * @param left
	 * @param right
	 * @return
	 */
	public static int compareToWithinTolerance(Duration left, Duration right) {
		return compareToWithinTolerance(left, right, DEFAULT_ALLOWED_MILLIS);
	}

	/**
	 * @param left
	 * @param right
	 * @param allowedMillis
	 *            how much tolerance would you like?
	 * @return
	 */
	public static int compareToWithinTolerance(Duration left, Duration right, int allowedMillis) {
		int result;
		if (equalsWithinTolerance(left, right, allowedMillis)) {
			result = ComparatorResult.EQUAL.value;
		} else {
			result = left.compareTo(right);
		}
		return result;
	}

	public static boolean equalsWithinTolerance(Duration left, Duration right, int allowedMillis) {
		return Math.abs(left.getMillis() - right.getMillis()) <= allowedMillis;
	}

	/**
	 * Some generics seem fussy so this just does a simple cast to get DateTime to be a comparable
	 * since DateTime Comparable<ReadableInstant> is the comparable, not Comparable<DateTime>.
	 * 
	 * @return
	 */
	public static Function<DateTime, ReadableInstant> dateTimeToReadableInstantFunction() {
		return new Function<DateTime, ReadableInstant>() {

			@Override
			public ReadableInstant apply(DateTime input) {
				return (input == null) ? null : (ReadableInstant) input;
			}
		};
	}

	/**
	 * Provides a parser for the standard rfc 1123 format
	 * http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3
	 * 
	 * @return
	 */
	public static DateTimeFormatter rfc1123HeaderDateTimeFormatter() {
		return DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss 'GMT'").withZoneUTC();
	}

	public static DateTime rfc1123HeaderDateTime(@Nullable String timeString) {
		return (timeString == null) ? null : RFC_1123_FORMATTER.parseDateTime(timeString);
	}

	public static String rfc1123HeaderDateTime(DateTime dateTime) {
		return (dateTime == null) ? null : RFC_1123_FORMATTER.print(dateTime);
	}

	/**
	 * A discovery method that will try our common formats.
	 * 
	 * @see #rfc1123HeaderDateTime(String)
	 * @see #parseXmlDate(String)
	 * 
	 * @param timeString
	 * @return the dateTime if recognized or null if not.
	 */
	public static DateTime dateTime(@Nullable String timeString) {
		if (timeString == null) {
			return null;
		}
		try {
			return parseXmlDate(timeString);
		} catch (IllegalArgumentException e) {
			try {
				return rfc1123HeaderDateTime(timeString);
			} catch (IllegalArgumentException e1) {
				// got others? try here.
				return null;
			}
		}
	}

	/**
	 * When the time is in unix epoch this returns the date time with the adjusted milliseconds used
	 * for the constructor.
	 * 
	 * @param secondsSinceEpoch
	 * @return
	 */
	public static DateTime dateTimeFromUnixEpoch(Long secondsSinceEpoch) {
		return new DateTime(secondsSinceEpoch * 1000);
	}

	/**
	 * Returns the longer of the two.
	 * 
	 * @deprecated use
	 *             {@link DurationUtil#max(org.joda.time.ReadableDuration, org.joda.time.ReadableDuration)}
	 * @param left
	 * @param right
	 * @return
	 */
	@Deprecated
	public static Duration max(Duration left, Duration right) {
		return DurationUtil.max(left, right);
	}

	/**
	 * Provides just the date part of the iso format, YYYY-MM-DD, set to UTC.
	 * 
	 * @param startTime
	 * @return
	 */
	public static String toIsoXmlUtcDateFormat(DateTime startTime) {
		return ISO_DATE_PATTERN.withZoneUTC().print(startTime);

	}

	/**
	 * @return
	 * 
	 */
	public static Function<? extends ReadableInstant, Long> millisFunction() {
		return new Function<ReadableInstant, Long>() {

			@Override
			public Long apply(ReadableInstant input) {
				return (input != null) ? input.getMillis() : null;
			}
		};
	}

	/**
	 * Returns the most recent of those given, nulls being equal and only returned if all given are
	 * null.
	 * 
	 * @see Ordering#max(Object, Object, Object, Object...)
	 * @param first
	 * @param second
	 * @return
	 */
	public static <P extends ReadablePartial> P moreRecent(P first, P second) {
		return Ordering.natural().nullsFirst().max(first, second);
	}

	public static <I extends ReadableInstant> I moreRecent(I first, I second) {
		return Ordering.natural().nullsFirst().max(first, second);
	}

	/**
	 * Given a {@link ReadablePartial}s like {@link LocalDate} this returns the most recent of the
	 * bunch.
	 * 
	 * @param partials
	 * @return
	 */
	public static <P extends ReadablePartial> P mostRecentPartial(Iterable<P> partials) {
		return Ordering.natural().nullsFirst().max(partials);
	}
}
