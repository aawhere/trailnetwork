/**
 *
 */
package com.aawhere.measure.geocell;

import javax.annotation.Nullable;

import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.util.rb.StringMessage;

import com.google.common.collect.Range;

/**
 * The resolution of the GeoCell.
 * 
 * {@literal
 * Resolution	Width
 * 				at equator(m)	feet
 * 1			10,018,790.00	32,870,046.98
 * 2			2,504,697.50	8,217,511.75
 * 3			626,174.38		2,054,377.94
 * 4			156,543.59		513,594.48
 * 5			39,135.90		128,398.62
 * 6			9,783.97		32,099.66
 * 7			2,445.99		8,024.91
 * 8			611.50			2,006.23
 * 9			152.87			501.56
 * 10			38.22			125.39
 * 11			9.55			31.35
 * 12			2.39			7.84
 * 13			0.60			1.96
 * 14			0.15			0.49
 * 15			0.04			0.12
 *}
 * 
 * @author Brian Chapman
 * 
 */
public enum Resolution {
	/** Order matters. don't change */
	ONE(1),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(VALUE.EIGHT),
	NINE(9),
	TEN(10),
	ELEVEN(11),
	TWELVE(12),
	THIRTEEN(13);

	public final Byte resolution;

	private Resolution(Integer resolution) {
		this.resolution = resolution.byteValue();
	}

	private static final Range<Byte> RANGE = Range.closed(lowest().resolution, highest().resolution);

	public Integer asInteger() {
		return resolution.intValue();
	}

	/**
	 * Retrieves the next lower resolution from this one. If this is {@link #FIVE} then
	 * {@link #FOUR} will be returned representing the resolution of the parent cell of a cell at
	 * this resolution.
	 * 
	 * @return
	 * @throws IndexOutOfBoundsException
	 *             when this is called on {@link #ONE}. Test before your call.
	 */
	public Resolution lower() {
		return valueOf((byte) (resolution - 1));
	}

	/**
	 * @param geoCellResolution
	 * @return
	 */
	public static Resolution valueOf(Byte resolution) {
		// resolution is one-based, ordinal is zero based.

		InvalidArgumentException.range(RANGE, resolution);
		return values()[resolution - 1];

	}

	/**
	 * Provides the next resolution representing the resolution of the children of a cell at this
	 * resolution.
	 * 
	 * @return
	 * @throws IndexOutOfBoundsException
	 *             when this is called on the minimum resolution. test your call against
	 *             {@link #THIRTEEN}.
	 */
	public Resolution higher() {
		return valueOf((byte) (resolution + 1));
	}

	/** Values that require static reference to be used in annoations. */
	public final static class VALUE {
		public static final Integer EIGHT = 8;

		public static final class STRING {
			public static final String EIGHT = "8";
		}
	}

	/**
	 * Converts any acceptable form of resolution given.
	 * 
	 * @see Resolution#valueOf(String)
	 * @see Resolution#valueOf(Byte)
	 * 
	 * 
	 * @param stringRepresentation
	 * @return the resolution if found
	 * @throws InvalidArgumentException
	 *             when the string is not understood.
	 */
	@Nullable
	public static final Resolution fromString(@Nullable String stringRepresentation) {
		if (stringRepresentation == null) {
			return null;
		}
		try {
			byte resolution = Byte.parseByte(stringRepresentation);
			return valueOf(resolution);
		} catch (NumberFormatException e) {
			try {
				return valueOf(stringRepresentation);
			} catch (Exception e1) {
				throw InvalidArgumentException.notUnderstood(new StringMessage(Resolution.ONE + "-"
						+ Resolution.THIRTEEN), stringRepresentation);
			}
		}
	}

	public static Resolution lowest() {
		return ONE;
	}

	/**
	 * @return
	 */
	public static Resolution highest() {
		return THIRTEEN;
	}
}
