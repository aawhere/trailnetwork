/**
 * 
 */
package com.aawhere.measure.zone;

import java.io.Serializable;

/**
 * A way to track one or more zones that will store a collection of measurements related to that
 * zone.
 * 
 * Zones are typically targeted by a set of threshold measurements (floor and ceiling) that
 * guarentee all values related will fall within the extreme thesholds.
 * 
 * Sequential means the ceiling for a previous zone is the floor of the next.
 * 
 * The ceiling is inclusive and the floor is exclusive so if the ceiling is 20 then a value of 20
 * will go into the lower zone rather than the next.
 * 
 * Zones are required to have a sequential order to them and zones should be all encompassing for
 * all possible values (so the top zone will have related values for all above the threshold).
 * 
 * @author roller
 * 
 */
public class MeasurementZones
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2684336088519975126L;

}
