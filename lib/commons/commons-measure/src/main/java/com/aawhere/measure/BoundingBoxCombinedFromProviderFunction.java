/**
 * 
 */
package com.aawhere.measure;

import com.google.common.base.Function;

/**
 * A pass through function for a given type that can produce a bounding box. This will calculate the
 * cumulative bounds.
 * 
 * @author aroller
 * 
 */
public class BoundingBoxCombinedFromProviderFunction<T>
		implements Function<T, T>, BoundingBoxSupplier {

	private Function<T, BoundingBox> provider;
	private BoundingBoxCombinedFunction combiner;

	/**
	 * 
	 */
	private BoundingBoxCombinedFromProviderFunction() {
	}

	public static <T> BoundingBoxCombinedFromProviderFunction<T> build(Function<T, BoundingBox> provider) {
		BoundingBoxCombinedFromProviderFunction<T> result = new BoundingBoxCombinedFromProviderFunction<>();
		result.combiner = BoundingBoxCombinedFunction.build();
		result.provider = provider;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public T apply(T input) {
		if (input == null) {
			return null;
		}
		combiner.apply(provider.apply(input));
		return input;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.BoundingBoxSupplier#isValid()
	 */
	@Override
	public Boolean isValid() {
		return combiner.isValid();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.BoundingBoxSupplier#get()
	 */
	@Override
	public BoundingBox get() {
		return combiner.get();
	}

}
