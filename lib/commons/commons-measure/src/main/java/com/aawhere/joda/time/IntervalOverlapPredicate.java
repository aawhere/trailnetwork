/**
 * 
 */
package com.aawhere.joda.time;

import org.joda.time.Interval;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

/**
 * Simple detection if two Intervals overlap. Used with
 * {@link Collections2#filter(java.util.Collection, Predicate)} this will return all of those that
 * overlap.
 * 
 * @author aroller
 * 
 */
public class IntervalOverlapPredicate
		implements Predicate<Interval> {

	private Interval interval;

	/**
	 * @param interval
	 */
	public IntervalOverlapPredicate(Interval interval) {
		this.interval = interval;
	}

	public static IntervalOverlapPredicate build(Interval interval) {
		return new IntervalOverlapPredicate(interval);
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(Interval input) {
		if (input == null) {
			return false;
		}
		return this.interval.overlaps(input);
	}

}
