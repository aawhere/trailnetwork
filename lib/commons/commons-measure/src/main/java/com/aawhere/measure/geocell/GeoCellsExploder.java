/**
 * 
 */
package com.aawhere.measure.geocell;

import java.util.Set;

import com.aawhere.lang.ComparatorResult;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.math.IntMath;

/**
 * Given a bunch of GeoCells, potentially at mixed resolutions but without parents, this will
 * explode each cell into it's 16 children and remove the parent itself. Options all shooting for a
 * specific resolution, but the default is to explode to the resolution beyond the max of the
 * geocells given. In any case all resulting cells will be of the same resolution as represented by
 * {@link #exploded()}'s {@link GeoCells#getMaxResolution()}.
 * 
 * @author aroller
 * 
 */
public class GeoCellsExploder {

	/**
	 * Used to construct all instances of GeoCellsExploder.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCellsExploder> {

		/**
		 * 
		 */
		private static final int CELL_COUNT_LIMIT_DEFAULT = IntMath.pow(GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL, 4);
		private Resolution to;
		private GeoCells these;
		private GeoCells.Builder exploded = GeoCells.create();
		private Integer cellCountLimit = CELL_COUNT_LIMIT_DEFAULT;

		public Builder() {
			super(new GeoCellsExploder());
		}

		public Builder to(Resolution to) {
			this.to = to;
			return this;
		}

		public Builder these(GeoCells cells) {
			this.these = cells;
			return this;
		}

		public Builder limitTo(Integer cellCountLimit) {
			this.cellCountLimit = cellCountLimit;
			return this;
		}

		/**
		 * Either explodes the geocell given if needed or adds it to the {@link #exploded} and
		 * completes the loop for a cell.
		 * 
		 * @param cell
		 */
		private void explode(GeoCell cell) {
			if (ComparatorResult.isLessThanOrEqualTo(to.compareTo(cell.getResolution()))) {
				exploded.add(cell);
			} else {
				Set<GeoCell> children = GeoCellUtil.explode(cell);
				// indirectly recursive...
				explode(children);
			}
		}

		/**
		 * @param children
		 */
		private void explode(Set<GeoCell> children) {
			InvalidArgumentException.limitExceeded(cellCountLimit, exploded.size());
			for (GeoCell geoCell : children) {
				explode(geoCell);
			}
		}

		/** Handy way to build and get what you are looking for. */
		public GeoCells exploded() {
			return build().exploded();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public GeoCellsExploder build() {
			GeoCellsExploder built = super.build();
			if (to == null) {
				// default is to explode one beyond that given
				to = this.these.getMaxResolution().higher();
			}
			explode(these.getAll());
			built.exploded = this.exploded.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct GeoCellsExploder */
	private GeoCellsExploder() {
	}

	private GeoCells exploded;

	public static Builder create() {
		return new Builder();
	}

	/** Convenience for readibility. */
	public static Builder explode() {
		return create();
	}

	public GeoCells exploded() {
		return exploded;
	}

}
