package com.aawhere.measure;

import java.io.Serializable;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;

/**
 * Distance above average sea level in meters. Uses {@link Length} and it's appropriate Units, but
 * implies Length above sea level as it's reference.
 * 
 * @author roller
 * 
 */
public interface Elevation
		extends Quantity<Elevation>, Serializable, Comparable<Elevation> {

	int MAX_METER_DIGITS = 0;

}
