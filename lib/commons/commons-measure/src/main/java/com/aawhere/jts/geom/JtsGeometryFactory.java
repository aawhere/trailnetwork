/**
 * 
 */
package com.aawhere.jts.geom;

import com.google.inject.Singleton;
import com.vividsolutions.jts.geom.GeometryFactory;

/**
 * 
 * Produces a JTS {@link com.vividsolutions.jts.geom.GeometryFactory}
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class JtsGeometryFactory {

	/**
	 * PrecisionModel uses Math.round which rounds to long, not a double. It can't work with
	 * decimals PrecisionModel precisionModel = new PrecisionModel(GeoCoordinate.TEN_CM_PRECISION);
	 * int maximumSignificantDigits = precisionModel.getMaximumSignificantDigits();
	 */
	public static final GeometryFactory defaultFactory;

	static {

		defaultFactory = new GeometryFactory();
	}

	/**
	 * @return the defaultfactory
	 */
	public static GeometryFactory getDefaultfactory() {
		return defaultFactory;
	}

}
