/**
 * 
 */
package com.aawhere.measure;

import com.aawhere.measure.calc.QuantityMath;

import com.google.common.base.Function;

/**
 * A stateful {@link Function} that combines all provided bounding boxes into a bounding box being
 * built until it's retrieval is called. The resulting bounding box is the same as that passed in.
 * 
 * @author aroller
 * 
 */
public class BoundingBoxCombinedFunction
		implements Function<BoundingBox, BoundingBox>, BoundingBoxSupplier {

	private QuantityMath<Latitude> minLat = QuantityMath.create(MeasurementUtil.MAX_LATITUDE);
	private QuantityMath<Longitude> minLon = QuantityMath.create(MeasurementUtil.MAX_LONGITUDE);
	private QuantityMath<Latitude> maxLat = QuantityMath.create(MeasurementUtil.MIN_LATITUDE);
	private QuantityMath<Longitude> maxLon = QuantityMath.create(MeasurementUtil.MIN_LONGITUDE);

	private boolean valid = false;

	public static BoundingBoxCombinedFunction build() {
		return new BoundingBoxCombinedFunction();
	}

	/**
	 * Will be applied to exand the bounding box. Returns true if it affected the existing bounds.
	 */
	@Override
	public BoundingBox apply(BoundingBox input) {
		if (input == null) {
			return null;
		}
		valid = true;
		GeoCoordinate min = input.getMin();
		minLat = minLat.min(min.getLatitude());
		minLon = minLon.min(min.getLongitude());
		GeoCoordinate max = input.getMax();
		maxLat = maxLat.max(max.getLatitude());
		maxLon = maxLon.max(max.getLongitude());
		return input;
	}

	/** Indicates if at least one bounding box was applied to the combined. */
	public Boolean isValid() {
		return valid;
	}

	/**
	 * Provides a view into the current state of the bounding box. Every call to this creates a new
	 * bounding box, but does not affect the ongoing calculations.
	 * 
	 * @see #isValid()
	 * 
	 * @return
	 * @throws IllegalStateException
	 *             when this is called and not valid. you call isValid first
	 */
	public BoundingBox get() {
		if (!isValid()) {
			throw new IllegalStateException("this isn't valid.  you should call is valid first.");
		}
		return BoundingBox.create().setNorth(maxLat.getQuantity()).setSouth(minLat.getQuantity())
				.setEast(maxLon.getQuantity()).setWest(minLon.getQuantity()).build();
	}
}
