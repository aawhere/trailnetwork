/**
 * 
 */
package com.aawhere.measure.geocell;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.Iterables;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfNotNull;
import com.googlecode.objectify.condition.IfNull;

/**
 * A simple container for multiple {@link GeoCell}s. This will remove duplicates and sort them by
 * the natural order of {@link GeoCell}.
 * 
 * 
 * Provides common functionality including toString conversion.
 * 
 * The string format for geocells is the geocells separated by a space. This is compatible with
 * {@link XmlList} annotation which is no coincidence.
 * 
 * @author aroller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlRootElement
@Dictionary(domain = GeoCells.FIELD.DOMAIN)
public class GeoCells
		implements Serializable, Iterable<GeoCell> {

	private static final long serialVersionUID = 4097881200045104597L;

	static final Character SPACE = ' ';
	static final Character COMMA = ',';

	@XmlValue
	@Field(key = FIELD.CELLS, parameterType = GeoCell.class, indexedFor = { "TN-360" })
	@Index
	private SortedSet<GeoCell> cells = new TreeSet<GeoCell>();
	/**
	 * This is better stored as a byte than an enum. It represents the resolution just the same
	 * since it is just the number spelled out.
	 */
	@XmlAttribute
	@Field(key = FIELD.MAX_RESOLUTION, indexedFor = { "TN-360" })
	@Index(IfNotNull.class)
	@IgnoreSave(IfNull.class)
	private Byte maxResolution;

	/**
	 * Used to construct all instances of GeoCells.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<GeoCells> {

		Builder(GeoCells geoCells) {
			super(geoCells);
		}

		public Builder() {
			this(new GeoCells());
		}

		public Builder add(GeoCell... geoCell) {
			building.cells.addAll(Arrays.asList(geoCell));
			return this;
		}

		public Integer size() {
			return building.cells.size();
		}

		public Builder addAll(Iterable<GeoCell> geoCells) {
			Iterables.addAll(building.cells, geoCells);
			return this;
		}

		/**
		 * Geocells separated by comma OR spaces, but not mixed. The string will be split and each
		 * cell will be an entry in the collection.
		 */
		public Builder setFromString(String geoCellsString) {
			List<String> asList = Arrays.asList(StringUtils.split(geoCellsString, COMMA));
			// maybe 1 geocell, or perhaps the recommended character wasn't the dilimeter.
			if (asList.size() == 1) {
				// moved space to be secondary since trim will clean it up if spaces and commas are
				// used.
				// let's be forgiving and try the standard split character.
				asList = Arrays.asList(StringUtils.split(geoCellsString, SPACE));
			}
			addAll(GeoCellUtil.convertToGeoCellsSet(asList));
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public GeoCells build() {
			building.cells = Collections.unmodifiableSortedSet(building.cells);

			return super.build();
		}

		/**
		 * Direct assignment of the given SortedSet to {@link GeoCells#cells}.
		 * 
		 * @param all
		 * @return
		 */
		public Builder setAll(SortedSet<GeoCell> all) {
			building.cells = all;
			return this;
		}

		/**
		 * @param geoCellsResolution
		 * @return
		 */
		public Builder setMaxResolution(Resolution maxResolution) {
			building.maxResolution = maxResolution.resolution;
			return this;
		}
	}// end Builder

	/** Use {@link Builder} to construct GeoCells */
	private GeoCells() {
	}

	/**
	 * FIXME: return a LinkedHashSet which will keep the order, but not go through the efforts of
	 * sorting every time.
	 * 
	 * http://java.dzone.com/articles/hashset-vs-treeset-vs
	 * 
	 * @return the all is unmodifiable sorted set.
	 */
	public SortedSet<GeoCell> getAll() {
		return this.cells;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return StringUtils.join(cells, COMMA);
	}

	/**
	 * @return
	 * 
	 */
	public static Builder create() {
		return new GeoCells.Builder();

	}

	/**
	 * @param cells
	 * @return
	 */
	public static Builder mutate(GeoCells cells) {
		return new Builder(cells);
	}

	/**
	 * Standard factory method producing {@link GeoCells} from the standard string.
	 * 
	 * @see Builder#setFromString(String)
	 * 
	 * @param cellList
	 * @return
	 */
	public static GeoCells valueOf(String cellList) {
		return new Builder().setFromString(cellList).build();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.cells == null) ? 0 : this.cells.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GeoCells other = (GeoCells) obj;
		if (this.cells == null) {
			if (other.cells != null) {
				return false;
			}
		} else if (!this.cells.equals(other.cells)) {
			return false;
		}
		return true;

	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<GeoCell> iterator() {
		return getAll().iterator();
	}

	/**
	 * Returns the max resolution if known (determined during building).
	 * 
	 * @return the maxResolution
	 */
	@Nullable
	public Resolution getMaxResolution() {
		if (this.maxResolution == null) {
			this.maxResolution = GeoCellUtil.maxResolution(cells).resolution;
		}
		return Resolution.valueOf(this.maxResolution);
	}

	public Integer size() {
		return cells.size();
	}

	public Boolean isEmpty() {
		return cells.isEmpty();
	}

	public static class FIELD {
		public static final String DOMAIN = "geoCells";
		public static final String CELLS = "cells";
		public static final String MAX_RESOLUTION = "maxResolution";
	}

	/**
	 * @return
	 */
	public Boolean hasMaxResolution() {
		return this.maxResolution != null;
	}

}
