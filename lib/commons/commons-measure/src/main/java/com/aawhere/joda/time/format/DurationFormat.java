/**
 *
 */
package com.aawhere.joda.time.format;

import java.util.Locale;

import org.joda.time.Duration;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormat;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import com.aawhere.text.format.custom.CustomFormat;

/**
 * Formats Joda Duration objects.
 * 
 * @author Brian Chapman
 * 
 */
public class DurationFormat
		implements CustomFormat<Duration> {

	private final String TIME_SEPERATOR = ":";
	// Note space afterwards. We cannot have 2 adjacent seperators.
	private final String HOUR_SYMBOL = "h ";
	private final String MINUTE_SYMBOL = "m";

	public DurationFormat() {
	}

	/**
	 * Formats the {@link Duration} as a {@link Format#DATE_LONG}
	 * 
	 */
	@Override
	public String format(Duration object, Locale locale) {
		return format(object, locale, Format.COMPACT);
	}

	public String format(Duration duration, Locale locale, Format format) {
		PeriodFormatter fmt = null;
		boolean sortable = false;
		switch (format) {
			case COMPACT:

				PeriodFormatterBuilder builder = new PeriodFormatterBuilder();
				builder.appendHours();
				builder.appendSeparator(TIME_SEPERATOR);
				builder.minimumPrintedDigits(2);
				builder.printZeroAlways();
				builder.appendMinutes();
				builder.appendSeparator(TIME_SEPERATOR);
				builder.minimumPrintedDigits(2);
				builder.appendSeconds();
				fmt = builder.toFormatter();
				break;
			case WRITTEN_OUT:
				fmt = PeriodFormat.wordBased(locale);
				break;
			case ISO:
				fmt = ISOPeriodFormat.standard();
				break;
			case SORTABLE:
				sortable = true;
				break;
			case SHORTHAND:
				PeriodFormatterBuilder shorthandBuilder = new PeriodFormatterBuilder();
				shorthandBuilder.appendHours();
				shorthandBuilder.appendSeparator(HOUR_SYMBOL);
				shorthandBuilder.printZeroAlways();
				shorthandBuilder.appendMinutes();
				shorthandBuilder.appendLiteral(MINUTE_SYMBOL);
				fmt = shorthandBuilder.toFormatter();
				break;
			default:
				fmt = ISOPeriodFormat.standard();
				break;
		}
		if (sortable) {
			return Long.valueOf(duration.getMillis()).toString();
		} else {
			PeriodFormatter formatter = fmt.withLocale(locale);
			return formatter.print(duration.toPeriod());
		}
	}

	@Override
	public Class<Duration> handlesType() {
		return Duration.class;
	}

	public enum Format {
		/* @formatter:off */
		WRITTEN_OUT,
		ISO,
		COMPACT,
		SORTABLE,
		SHORTHAND;
		/* @formatter:on */
	}

}
