/**
 * 
 */
package com.aawhere.measure.calc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

import javax.measure.quantity.Quantity;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.Unit;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.ExtraUnits;

/**
 * Non-mutable math class emulating state by passing new instances of modified math classes
 * specializing in {@link Quantity} manipulation.
 * 
 * @author roller
 * 
 */
public class QuantityMath<Q extends Quantity<Q>> {

	final private Q quantity;

	public QuantityMath(Q quantity) {
		this.quantity = quantity;
	}

	/**
	 * @deprecated use {@link #create(Quantity)}
	 * 
	 * @param quantity
	 * @return
	 */
	public static <Q extends Quantity<Q>> QuantityMath<Q> getInstance(Q quantity) {
		return new QuantityMath<Q>(quantity);
	}

	public static <Q extends Quantity<Q>> QuantityMath<Q> create(Q quantity) {
		return new QuantityMath<Q>(quantity);
	}

	public QuantityMath<Q> dividedBy(Q targetQuantity) {

		double result = doubleValueFrom(this.quantity) / doubleValueFrom(targetQuantity);
		return produceResult(result);
	}

	/**
	 * @return the quantity
	 */
	public Q getQuantity() {
		return quantity;
	}

	public Boolean greaterThan(Q targetQuantity) {
		double leftValue = doubleValueFrom(quantity);
		double rightValue = doubleValueFrom(targetQuantity);
		return leftValue > rightValue;
	}

	public Boolean greaterThanEqualTo(Q targetQuantity) {
		double leftValue = doubleValueFrom(quantity);
		double rightValue = doubleValueFrom(targetQuantity);
		return leftValue >= rightValue;
	}

	/**
	 * Adds the portion of the total between the {@link #quantity} and the to parameter by
	 * multiplying the ratio given and adding to the {@link #quantity}.
	 * 
	 * @param to
	 * @param ratio
	 * @return
	 */
	public QuantityMath<Q> interpolate(Q to, Ratio ratio) {
		Q total = new QuantityMath<Q>(to).minus(quantity).getQuantity();
		double portionValue = total.doubleValue(getUnit()) * ratio.doubleValue(ExtraUnits.RATIO);
		Q portion = produceQuantity(portionValue);
		return this.plus(portion);
	}

	public Boolean lessThan(Q targetQuantity) {
		double leftValue = doubleValueFrom(quantity);
		double rightValue = doubleValueFrom(targetQuantity);
		return leftValue < rightValue;
	}

	public Boolean lessThanEqualTo(Q targetQuantity) {
		double leftValue = doubleValueFrom(quantity);
		double rightValue = doubleValueFrom(targetQuantity);
		return leftValue <= rightValue;
	}

	/**
	 * Ensures that this value is {@link #greaterThan(Quantity)} min provided and
	 * {@link #lessThan(Quantity)} max provided.
	 * 
	 * The values are inclusive so equality is a true.
	 * 
	 * If either value is not provided it will be ignored. So if both values are not provided this
	 * always returns true satisfying the "no limit", bottom limit or top limit use cases.
	 * 
	 * @see http://en.wikipedia.org/wiki/Interval_(mathematics)
	 * 
	 * @param min
	 * @param max
	 * @param includeEndpoints
	 *            means this quantity will be between if it is equal to either the min or max
	 * @return
	 */
	public Boolean between(Q min, Q max, Boolean includeEndpoints) {
		Boolean result = true;
		if (min != null && max != null) {
			if (new QuantityMath<Q>(min).greaterThan(max)) {
				throw new IllegalArgumentException(min + " is greater than " + max);
			}
		}
		if (min != null) {
			if (includeEndpoints) {
				result = result && greaterThanEqualTo(min);
			} else {
				result = result && greaterThan(min);
			}
		}
		if (max != null) {
			if (includeEndpoints) {
				result = result && lessThanEqualTo(max);
			} else {
				result = result && lessThan(max);
			}
		}
		return result;
	}

	/**
	 * Between the min and max inclusive of endpoints.
	 * 
	 * @see #between(Quantity, Quantity, Boolean)
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public Boolean between(Q min, Q max) {
		return between(min, max, true);
	}

	/**
	 * Checks for equality of the target within the precision given. This means we check if the
	 * target is between {@link #quantity} - tolerance and quantity + tolerance.
	 * 
	 * @see #between(Quantity, Quantity)
	 * 
	 * @param targetQuantity
	 * @param tolerance
	 * @return true if this quantity is within the precision
	 */
	public Boolean equals(Q targetQuantity, Q tolerance) {
		return between(new QuantityMath<Q>(targetQuantity).minus(tolerance).getQuantity(), new QuantityMath<Q>(
				targetQuantity).plus(tolerance).getQuantity());
	}

	/**
	 * Checks for equality within the tolerance determined by the registered
	 * {@link QuantityPrecision}.
	 * 
	 * @see QuantityPrecision
	 * @see #equals(Quantity, Quantity)
	 * 
	 * @param targetQuantity
	 * @return
	 */
	public Boolean equals(Q targetQuantity) {
		if (targetQuantity == null) {
			return false;
		} else {
			Q tolerance = new QuantityPrecision.Builder().build().precisionFor(targetQuantity);
			return equals(targetQuantity, tolerance);
		}
	}

	/**
	 * Useful method to report if this is equal to zero within it's normal precison.
	 * 
	 * @return
	 */
	public boolean equalToZero() {
		Q zero = produceQuantity(0);
		return equals(zero);
	}

	/**
	 * @param quantity2
	 * @return
	 */
	public QuantityMath<Q> minus(Q targetQuantity) {

		double result = doubleValueFrom(this.quantity) - doubleValueFrom(targetQuantity);
		return produceResult(result);
	}

	public QuantityMath<Q> multipliedBy(double factor) {
		return produceResult(doubleValueFrom(this.quantity) * factor);
	}

	public QuantityMath<Q> multipliedBy(Q targetQuantity) {
		return multipliedBy(doubleValueFrom(targetQuantity));
	}

	/**
	 * Divides the quantity of this over the given total as the denominator.
	 * 
	 * @see #dividedBy(Quantity)
	 * 
	 * @param total
	 * @return
	 */
	public Ratio over(Q total) {
		return MeasurementUtil.createRatio(this.doubleValueFrom(this.quantity), this.doubleValueFrom(total));
	}

	/**
	 * alias for {@link #multipliedBy(double)}
	 * 
	 * @param factor
	 * @return
	 */
	public QuantityMath<Q> times(double factor) {
		return produceResult(doubleValueFrom(this.quantity) * factor);
	}

	/**
	 * alias for {@link #multipliedBy(Quantity)}
	 * 
	 * @param targetQuantity
	 * @return
	 */
	public QuantityMath<Q> times(Q targetQuantity) {
		return multipliedBy(doubleValueFrom(targetQuantity));
	}

	/**
	 * Adds the two together to produce the result.
	 * 
	 * @see #plus(Iterable)
	 * 
	 * */
	public QuantityMath<Q> plus(Q targetQuantity) {
		// although it pains me, I'm purposely not chaining to plus(Q[]) for efficiency's sake
		// plus is an import, common used method and creating an array and list would be a waste
		double result = doubleValueFrom(this.quantity) + doubleValueFrom(targetQuantity);
		return produceResult(result);
	}

	/**
	 * @see #plus(Iterable)
	 * 
	 * @param quantitiesToSum
	 * @return
	 */
	public QuantityMath<Q> plus(Q... quantitiesToSum) {
		return plus(Arrays.asList(quantitiesToSum));
	}

	/**
	 * Adds all the given quantities to this quantity.
	 * 
	 * @see #plus(Quantity)
	 * @param quantitiesToSum
	 * @return
	 */
	public QuantityMath<Q> plus(Iterable<Q> quantitiesToSum) {
		double result = doubleValueFrom(this.quantity);
		for (Q q : quantitiesToSum) {
			result += doubleValueFrom(q);
		}
		return produceResult(result);
	}

	/**
	 * Returns the lesser of the two
	 * 
	 * @param targetQuantity
	 * @return
	 */
	public QuantityMath<Q> min(Q targetQuantity) {
		if (lessThan(targetQuantity)) {
			return this;
		} else {
			return produceResult(targetQuantity);
		}
	}

	/**
	 * Retursn the greater of the two.
	 * 
	 * @param targetQuantity
	 * @return
	 */
	public QuantityMath<Q> max(Q targetQuantity) {
		if (greaterThan(targetQuantity)) {
			return this;
		} else {
			return produceResult(targetQuantity);
		}
	}

	public QuantityMath<Q> squared() {
		return new QuantityMath<Q>(this.quantity).multipliedBy(this.quantity);
	}

	public QuantityMath<Q> squareRoot() {
		double result = Math.sqrt(doubleValueFrom(this.quantity));
		return produceResult(result);
	}

	private double doubleValueFrom(Q targetQuantity) {
		return targetQuantity.doubleValue(getUnit());
	}

	/**
	 * @return
	 */
	private Unit<Q> getUnit() {
		return this.quantity.getUnit();
	}

	protected Q produceQuantity(double result) {
		QuantityFactory<Q> factory = factory();
		Unit<Q> unit = getUnit();
		return factory.create(result, unit);
	}

	/**
	 * @return
	 */
	private QuantityFactory<Q> factory() {
		return MeasurementUtil.getFactory(this.quantity);
	}

	/**
	 * Used by every method to produce another {@link QuantityMath} with the new value.
	 * 
	 * @param targetQuantity
	 * @param result
	 * @return
	 */
	protected QuantityMath<Q> produceResult(double result) {
		final Q resultQuantity = produceQuantity(result);
		return new QuantityMath<Q>(resultQuantity);
	}

	/**
	 * @param targetQuantity
	 * @return
	 */
	private QuantityMath<Q> produceResult(Q targetQuantity) {

		return new QuantityMath<Q>(targetQuantity);
	}

	/**
	 * @param end
	 * @return
	 */
	public QuantityMath<Q> average(Q end) {
		return this.plus(end).dividedBy(2);
	}

	/**
	 * Useful when dividing by a number that is not the quantity.
	 * 
	 * This should not be used to bypass the {@link #dividedBy(Quantity)} method when quantities are
	 * the subject.
	 * 
	 * @param i
	 * @return
	 */
	public QuantityMath<Q> dividedBy(double divisor) {
		// not sure if divisor is the right word. Denominator?
		double dividedValue = this.doubleValueFrom(quantity) / divisor;
		return new QuantityMath<Q>(MeasurementUtil.getFactory(quantity).create(dividedValue, getUnit()));
	}

	/**
	 * 
	 * @return true if the value is greater than zero, false otherwise
	 */
	public Boolean positive() {

		return this.quantity.doubleValue(getUnit()) > 0;
	}

	/**
	 * 
	 * @return true if the value is less than zero, false otherwise
	 */
	public Boolean negative() {
		return this.quantity.doubleValue(getUnit()) < 0;
	}

	@Override
	public String toString() {
		return this.quantity.toString();
	}

	/**
	 * Returns the negative of the given. If the given is negative then positive is returned.
	 * 
	 * @return
	 */
	public QuantityMath<Q> negate() {
		return this.times(-1);
	}

	public QuantityMath<Q> absolute() {
		if (negative()) {
			return negate();
		} else {
			return this;
		}
	}

	public QuantityMath<Q> setScale(int newScale, RoundingMode rounding) {
		return produceResult(new BigDecimal(doubleValueFrom(quantity)).setScale(newScale, rounding).doubleValue());
	}

	/**
	 * Simply rounds the number using {@link BigDecimal#setScale(int)}
	 * 
	 * @param mathContext
	 * @return
	 */
	public QuantityMath<Q> setScale(int newScale) {
		return setScale(newScale, RoundingMode.HALF_EVEN);
	}

	/**
	 * @return
	 */
	public QuantityMath<Q> inverse() {
		return produceResult(1 / doubleValueFrom(quantity));
	}

	public Integer compareTo(Q target) {
		return ((Double) doubleValueFrom(this.quantity)).compareTo(doubleValueFrom(target));
	}

	/**
	 * Provides the resulting Math in the new unit provided. Useful in cases when
	 * {@link #setScale(int)} is relative to some desired {@link Unit} or when the result
	 * {@link #getQuantity()} is to be in a specific unit.
	 * 
	 * If the same unit is provided as {@link #getUnit()} this math is returned unchanged.
	 * 
	 * @param unit
	 * @return
	 */
	public QuantityMath<Q> convert(Unit<Q> unit) {
		if (unit.equals(getUnit())) {
			return this;
		} else {
			return produceResult(factory().create(this.quantity.doubleValue(unit), unit));
		}
	}

}
