/**
 *
 */
package com.aawhere.joda.time.format;

import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aawhere.text.format.custom.CustomFormat;

/**
 * Formats Joda DateTime objects.
 * 
 * Note: class is named JodaDateTimeFormat, which deviates from our standard naming conventions.
 * However to avoid conflict with org.joda.DateTimeFormat, this name was chosen.
 * 
 * @author Brian Chapman
 * 
 */
public class JodaDateTimeFormat
		implements CustomFormat<DateTime> {

	public JodaDateTimeFormat() {
	}

	/**
	 * Formats the {@link DateTime} as a {@link Format#DATE_LONG}
	 * 
	 */
	@Override
	public String format(DateTime object, Locale locale) {
		return format(object, locale, Format.MEDIUM_DATETIME);
	}

	public String format(DateTime object, Locale locale, Format format) {
		DateTimeFormatter fmt;
		switch (format) {
			case SHORT_DATE:
				fmt = DateTimeFormat.shortDate();
				break;
			case FULL_TIME:
				fmt = DateTimeFormat.fullTime();
				break;
			case MEDIUM_DATE:
				fmt = DateTimeFormat.mediumDate();
				break;
			case MEDIUM_DATETIME:
				fmt = DateTimeFormat.mediumDateTime();
				break;
			case MEDIUM_TIME:
				fmt = DateTimeFormat.mediumTime();
				break;
			case SHORT_DATETIME:
				fmt = DateTimeFormat.shortDateTime();
				break;
			case SORTABLE_DATETIME:
				fmt = DateTimeFormat.forPattern("YYYY'-'MM'-'dd'T'HH':'mm':'ss.SSS");
				break;
			default:
				fmt = DateTimeFormat.mediumDateTime();
				break;
		}
		DateTimeFormatter formatter = fmt.withLocale(locale);
		return formatter.print(object);
	}

	@Override
	public Class<DateTime> handlesType() {
		return DateTime.class;
	}

	public enum Format {
		/* @formatter:off */
		SHORT_DATE,
		MEDIUM_DATE,

		MEDIUM_TIME,
		FULL_TIME,

		MEDIUM_DATETIME,
		SHORT_DATETIME,

		SORTABLE_DATETIME;
		/* @formatter:on */
	}

}
