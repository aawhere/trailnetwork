/**
 *
 */
package com.aawhere.joda.time.format;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringFormatException;

/**
 * @author aroller
 * 
 */
public class DateTimeStringAdapter
		implements StringAdapter<DateTime> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Object value) {
		if (value == null) {
			return null;
		}
		if (value instanceof DateTime) {
			return ISODateTimeFormat.dateTime().print((DateTime) value);
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#unmarshal(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <D extends DateTime> D unmarshal(String string, Class<D> type) throws StringFormatException {
		if (string == null) {
			return null;
		}
		return (D) ISODateTimeFormat.dateTimeParser().parseDateTime(string);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#getType()
	 */
	@Override
	public Class<DateTime> getType() {

		return DateTime.class;
	}

}
