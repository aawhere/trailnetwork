/**
 *
 */
package com.aawhere.measure;

import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Nullable;
import javax.measure.quantity.Acceleration;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.quantity.QuantityFactory;
import javax.measure.quantity.Velocity;
import javax.measure.unit.BaseUnit;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;

import org.apache.commons.math3.util.Precision;
import org.joda.time.Duration;

import com.aawhere.joda.time.DurationUtil;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.measure.calc.AngleMath;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.measure.unit.format.UnitFormatFactory;
import com.aawhere.number.format.custom.DoubleFormat;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.collect.Ordering;

/**
 * @author roller
 * 
 */
public class MeasurementUtil {

	static {
		MeasurementQuantityFactory.initialize();
	}

	private MeasurementUtil() {
	}

	public static final Ratio RATIO_ZERO = MeasurementQuantityFactory.getInstance(Ratio.class)
			.create(0, ExtraUnits.RATIO);
	public static final Ratio RATIO_ONE = MeasurementQuantityFactory.getInstance(Ratio.class)
			.create(1, ExtraUnits.RATIO);
	public static final Length ZERO_LENGTH = MeasurementQuantityFactory.getInstance(Length.class)
			.create(0, MetricSystem.METRE);
	public static final Elevation ZERO_ELEVATION = MeasurementQuantityFactory.getInstance(Elevation.class)
			.create(0, ExtraUnits.METER_MSL);

	public static final Angle ANGLE_MIN = MeasurementQuantityFactory.getInstance(Angle.class)
			.create(0.0, MetricSystem.RADIAN);
	public static final Angle ANGLE_MAX = MeasurementQuantityFactory.getInstance(Angle.class)
			.create(2 * Math.PI, MetricSystem.RADIAN);
	public static final Angle QUARTER_CIRCLE = createAngleInRadians(Math.PI / 2);
	public static final Angle HALF_CIRCLE = createAngleInRadians(Math.PI);

	public static final Longitude MIN_LONGITUDE = MeasurementUtil.createLongitude(Longitude.MIN);
	public static final Longitude MAX_LONGITUDE = MeasurementUtil.createLongitude(Longitude.MAX);
	public static final Latitude MIN_LATITUDE = MeasurementUtil.createLatitude(Latitude.MIN);
	public static final Latitude MAX_LATITUDE = MeasurementUtil.createLatitude(Latitude.MAX);
	public static final GeoCoordinate MIN_GEO_COORDINATE = GeoCoordinate.create().setLatitude(MIN_LATITUDE)
			.setLongitude(MIN_LONGITUDE).build();
	public static final GeoCoordinate MAX_GEO_COORDINATE = GeoCoordinate.create().setLatitude(MAX_LATITUDE)
			.setLongitude(MAX_LONGITUDE).build();
	public static final BoundingBox WORLD_BOUNDARY = new BoundingBox.Builder()
			.setNorthEast(MeasurementUtil.MAX_GEO_COORDINATE).setSouthWest(MeasurementUtil.MIN_GEO_COORDINATE).build();
	public static final Angle NORTH = createAngleInRadians(0.0);
	public static final Angle SOUTH = createAngleInRadians(Math.PI);
	public static final Angle EAST = QUARTER_CIRCLE;
	public static final Angle WEST = new AngleMath(QUARTER_CIRCLE).multipliedBy(3).getQuantity();
	public static final Angle NORTH_EAST = new AngleMath(QUARTER_CIRCLE).dividedBy(2).getQuantity();
	public static final Angle SOUTH_WEST = new AngleMath(NORTH_EAST).reverse().getQuantity();

	@SuppressWarnings("unchecked")
	public static <Q extends Quantity<Q>> QuantityFactory<Q> getFactory(Q quantity) {
		QuantityFactory<Q> factory = MeasurementQuantityFactory.getInstance(quantity.getClass());

		return factory;
	}

	public static <Q extends Quantity<Q>> QuantityFactory<Q> factory(Q quantity) {
		return getFactory(quantity);
	}

	public static <Q extends Quantity<Q>> QuantityFactory<Q> factory(Class<Q> type) {
		return MeasurementQuantityFactory.getInstance(type);
	}

	/**
	 * Provides the normalized {@link Quantity} given a value that must be of the Normalized unit as
	 * defined by {@link #getNormalizedUnit(Class)}.
	 * 
	 * Mostly a convenience for classes at the persistence layer since they are the only ones that
	 * get to assume the unit of measurement for a value is normalized.
	 * 
	 * @see #quantityOrNull(Double, Unit, Class)
	 * 
	 * @param value
	 * @param type
	 * @return
	 */
	public static <Q extends Quantity<Q>> Q normalizedQuantityOrNull(Double value, Class<Q> type) {

		return quantityOrNull(value, getNormalizedUnit(type), type);
	}

	/**
	 * Simply creates a quantity avoiding a null pointer expection if the value is null.
	 * 
	 * 
	 * @param value
	 *            the value in the unit provided
	 * @param unit
	 * @param type
	 *            the expected Quantity type.
	 * @return the created Quantity or null if value is null
	 */
	public static <Q extends Quantity<Q>> Q quantityOrNull(Double value, Unit<Q> unit, Class<Q> type) {
		Q result;
		if (value != null) {
			result = MeasurementQuantityFactory.getInstance(type).create(value, unit);
		} else {
			result = null;
		}
		return result;
	}

	/**
	 * @deprecated use {@link #ratio(Number, Number)}
	 * @param partial
	 * @param total
	 * @return
	 */
	@Deprecated
	public static Ratio createRatio(Number partial, Number total) {

		return ratio(partial, total);

	}

	/**
	 * Given the numerator and denominator this will return the ratio.
	 * 
	 * @see QuantityMath#over(Quantity)
	 * 
	 * @param distanceToLocation
	 * @param segmentDistance
	 * @return
	 */
	public static <Q extends Quantity<Q>> Ratio ratio(Q numerator, Q denominator) {
		return QuantityMath.create(numerator).over(denominator);
	}

	public static <N extends Number> Ratio ratio(N numerator, N denominator) {
		double value;
		// avoid division by zero
		if (denominator.doubleValue() == 0) {
			if (numerator.doubleValue() == 0) {
				// 0/0 is essentially zero...
				value = 0;
			} else {
				throw new IllegalArgumentException(
						" the denominator is zero so.  filter out if this is an expected value " + numerator + "/"
								+ denominator);
			}
		} else {
			value = numerator.doubleValue() / denominator.doubleValue();
		}
		return ratio(value);
	}

	/**
	 * converts the given multiset into a multimap of ratios mapping the value of the set as the key
	 * and the ratio being the value. the total size of the multiset is used as the denominator of
	 * the ratio.
	 */
	public static <E> Map<E, Ratio> ratios(Multiset<E> counts) {
		int size = counts.size();
		ImmutableMap.Builder<E, Ratio> builder = ImmutableMap.builder();
		for (Entry<E> e : counts.entrySet()) {
			builder.put(e.getElement(), ratio(e.getCount(), size));
		}
		return builder.build();

	}

	private static class RatioFunction
			implements Function<Double, Ratio> {
		final private Double denominator;

		public RatioFunction(Double denominator) {
			this.denominator = denominator;
		}

		@Override
		@Nullable
		public Ratio apply(@Nullable Double numerator) {
			return (numerator != null) ? ratio(numerator, denominator) : null;
		}

		@Override
		public String toString() {
			return "RatioFunction " + denominator;
		}
	}

	public static Function<Double, Ratio> ratioFunction(Double denominator) {
		return new RatioFunction(denominator);
	}

	/**
	 * @deprecated use #ratio(Number value)
	 * 
	 * @param value
	 * @return
	 */
	@Deprecated
	public static Ratio createRatio(Number value) {

		return ratio(value);
	}

	/**
	 * @param value
	 * @return
	 */
	public static Ratio ratio(Number value) {
		return MeasurementQuantityFactory.getInstance(Ratio.class).create(value, ExtraUnits.RATIO);
	}

	/**
	 * Converts a collection of Quantities to the corresponding Double values using the
	 * {@link Quantity#doubleValue(javax.measure.unit.Unit)} method.
	 * 
	 * @param <QuantityT>
	 * @param quantities
	 * @return
	 */
	public static <QuantityT extends Quantity<QuantityT>> Double[] toArray(Iterable<QuantityT> quantities) {

		ArrayList<Double> numbers = new ArrayList<Double>();
		for (QuantityT quantity : quantities) {
			numbers.add(quantity.doubleValue(quantity.getUnit()));
		}

		return numbers.toArray(new Double[numbers.size()]);
	}

	/**
	 * @see #toArray(Iterable)
	 * 
	 * @param <QuantityT>
	 * @param quantities
	 * @return
	 */
	public static <QuantityT extends Quantity<QuantityT>> Double[] toArray(Collection<QuantityT> quantities) {

		Double[] values = new Double[quantities.size()];
		int index = 0;
		for (QuantityT quantity : quantities) {
			values[index] = quantity.doubleValue(quantity.getUnit());
		}
		return values;
	}

	public static Length createLengthInMeters(double value) {
		return MeasurementQuantityFactory.getInstance(Length.class).create(value, MetricSystem.METRE);
	}

	public static Latitude createLatitude(Double decimalDegrees) {
		return MeasurementQuantityFactory.getInstance(Latitude.class).create(	decimalDegrees,
																				ExtraUnits.DECIMAL_DEGREES_LATITUDE);
	}

	public static Latitude createLatitudeRounded(Double decimalDegrees) {
		return createLatitude(Precision.round(decimalDegrees, GeoQuantity.DD_MAX_FRACTION_DIGITS_DEFAULT));
	}

	public static Longitude createLongitudeRounded(Double decimalDegrees) {
		return createLongitude(Precision.round(decimalDegrees, GeoQuantity.DD_MAX_FRACTION_DIGITS_DEFAULT));
	}

	public static Longitude createLongitude(Double decimalDegrees) {
		return MeasurementQuantityFactory.getInstance(Longitude.class).create(	decimalDegrees,
																				ExtraUnits.DECIMAL_DEGREES_LONGITUDE);
	}

	public static Longitude createLongitude(Angle angle) {
		return createLongitude(angle.doubleValue(USCustomarySystem.DEGREE_ANGLE));
	}

	public static Latitude createLatitude(Angle angle) {
		return createLatitude(angle.doubleValue(USCustomarySystem.DEGREE_ANGLE));
	}

	public static GeoCoordinate createGeoCoordinate(Latitude latitude, Longitude longitude) {
		return new GeoCoordinate.Builder().setLatitude(latitude).setLongitude(longitude).build();
	}

	public static GeoCoordinate createGeoCoordinate(Double latitudeDecimalDegrees, Double longitudeDecimalDegrees) {
		return new GeoCoordinate.Builder().setLatitude(latitudeDecimalDegrees).setLongitude(longitudeDecimalDegrees)
				.build();
	}

	/**
	 * Translates elevation units into length by making {@link MetricSystem#METRE} equivalent to
	 * {@link CustomSystemOfUnits#METER_MSL}.
	 * 
	 * @param elevation
	 * @return
	 */
	public static Length createLength(Elevation elevation) {
		return createLengthInMeters(elevation.doubleValue(ExtraUnits.METER_MSL));
	}

	public static Length createLength(ElevationChange elevationChange) {
		return createLengthInMeters(elevationChange.doubleValue(ExtraUnits.METER_CHANGE));
	}

	public static Length createLength(QuantityMath<Elevation> math) {
		return createLength(math.getQuantity());
	}

	public static Velocity createVelocityInMps(double metersPerSecond) {
		return MeasurementQuantityFactory.getInstance(Velocity.class).create(	metersPerSecond,
																				MetricSystem.METRES_PER_SECOND);
	}

	public static Acceleration createAccelerationInMpss(double metersPerSquareSecond) {
		return MeasurementQuantityFactory.getInstance(Acceleration.class)
				.create(metersPerSquareSecond, MetricSystem.METRES_PER_SQUARE_SECOND);
	}

	public static Angle createAngleInRadians(Double value) {
		return MeasurementQuantityFactory.getInstance(Angle.class).create(value, MetricSystem.RADIAN);
	}

	public static Angle createAngleInDegrees(Double value) {
		return MeasurementQuantityFactory.getInstance(Angle.class).create(value, USCustomarySystem.DEGREE_ANGLE);
	}

	public static Velocity velocity(Length distance, Duration duration) {

		double velocityInMps = distance.doubleValue(MetricSystem.METRE) / DurationUtil.secondsFromDuration(duration);

		QuantityFactory<Velocity> factory = MeasurementQuantityFactory.getInstance(Velocity.class);
		return factory.create(velocityInMps, MetricSystem.METRES_PER_SECOND);
	}

	/**
	 * Calcuates acceleration from velocity change and time. Assumes uniform acceleration.
	 * http://en.wikipedia.org/wiki/Acceleration
	 * 
	 * @param initialVelocity
	 * @param finalVelocity
	 * @param duration
	 * @return
	 */
	public static Acceleration acceleration(Velocity initialVelocity, Velocity finalVelocity, Duration duration) {

		Velocity delta = new QuantityMath<Velocity>(finalVelocity).minus(initialVelocity).getQuantity();
		double deltaInMps = delta.doubleValue(MetricSystem.METRES_PER_SECOND);
		double seconds = DurationUtil.secondsFromDuration(duration);
		double accelerationInMpss = deltaInMps / seconds;
		return MeasurementQuantityFactory.getInstance(Acceleration.class)
				.create(accelerationInMpss, MetricSystem.METRES_PER_SQUARE_SECOND);

	}

	/**
	 * Creates an Elevation
	 * 
	 * @param length
	 *            the elevation as a Length
	 * @return
	 */
	public static Elevation createElevation(Length length) {
		return MeasurementQuantityFactory.getInstance(Elevation.class).create(	length.doubleValue(MetricSystem.METRE),
																				ExtraUnits.METER_MSL);
	}

	public static Elevation createElevation(Number value, Unit<Elevation> unit) {
		return MeasurementQuantityFactory.getInstance(Elevation.class).create(value, unit);
	}

	/**
	 * Creates an {@link ElevationChange}
	 * 
	 * @param length
	 * @return
	 */
	public static ElevationChange createElevationChange(Length length) {
		return MeasurementQuantityFactory.getInstance(ElevationChange.class)
				.create(length.doubleValue(MetricSystem.METRE), ExtraUnits.METER_CHANGE);
	}

	public static ElevationChange createElevationChange(Number value, Unit<ElevationChange> unit) {
		return MeasurementQuantityFactory.getInstance(ElevationChange.class).create(value, unit);
	}

	public static ElevationChange createElevationChange(Elevation elevation) {
		return createElevationChange(createLength(elevation));
	}

	/**
	 * Creates an Elevation in meters or returns null if null is passed.
	 * 
	 * @param elevation
	 * @return
	 */
	public static Elevation createElevationInMeters(Number elevation) {

		if (elevation != null) {
			return MeasurementQuantityFactory.getInstance(Elevation.class).create(elevation, ExtraUnits.METER_MSL);

		} else {
			return null;
		}
	}

	public static Elevation createElevationInMetersRounded(Number elevation) {
		DoubleFormat doubleFormat = new DoubleFormat(Elevation.MAX_METER_DIGITS);
		Double formatted = doubleFormat.format(elevation.doubleValue());
		return createElevationInMeters(formatted);
	}

	/**
	 * getClass method on a Quantity can return the proxy rather than the expected Quantity
	 * interface. This method will always return that expected interface.
	 */
	@SuppressWarnings("unchecked")
	public static <Q extends Quantity<?>> Class<Q> classFor(Q quantity) {
		Class<Q> result;
		Class<Q>[] classes = (Class<Q>[]) quantity.getClass().getInterfaces();
		// check to see if it is a proxy or a concrete class
		if (classes.length == 0) {
			result = (Class<Q>) quantity.getClass();
		} else {
			result = classes[0];
		}
		return result;
	}

	/**
	 * Given a unit this will return the Normalized Unit across the system (typically the
	 * {@link BaseUnit} of the {@link MetricSystem}.
	 * 
	 * @param unit
	 * @return
	 */
	public static <Q extends Quantity<Q>> Unit<Q> getNormalizedUnit(Class<Q> quantityType) {
		Unit<Q> metricUnit = MeasurementQuantityFactory.getInstance(quantityType).getMetricUnit();
		return getNormalizedUnit(metricUnit);
	}

	/**
	 * Given a unit this will return the Normalized Unit across the system (typically the
	 * {@link BaseUnit} of the {@link MetricSystem}.
	 * 
	 * @param unit
	 * @return
	 */
	public static <Q extends Quantity<Q>> Unit<Q> getNormalizedUnit(Unit<Q> unit) {
		return unit.toMetric();
	}

	/**
	 * 
	 * Returns a normalized value using the {@link BaseUnit} from the {@link MetricSytem} unit
	 * class.
	 * 
	 * Length => METER Angle => RADIANS Other units havn't been tested, please write tests to
	 * verify.
	 * 
	 * @param quantity
	 * @return
	 */
	public static <Q extends Quantity<Q>> Q getNormalizedQuantity(Q quantity) {
		Unit<Q> unit = quantity.getUnit();
		Unit<Q> normalizedUnit = getNormalizedUnit(unit);
		double metricValue = quantity.doubleValue(normalizedUnit);

		@SuppressWarnings("unchecked")
		Q metricQuantity = (Q) MeasurementQuantityFactory.getInstance(quantity.getClass()).create(	metricValue,
																									normalizedUnit);

		return metricQuantity;
	}

	/**
	 * Get the symbol for the unit using the default Locale.
	 * 
	 * @param unit
	 * @return
	 */
	public static String getUnitSymbol(Unit<?> unit) {
		return UnitFormatFactory.getInstance().createDefaultUnitFormat().format(unit);
	}

	/**
	 * Get the localized symbol for the unit.
	 * 
	 * @param unit
	 * @return
	 */
	public static String getUnitSymbol(Unit<?> unit, Locale locale) {
		return UnitFormatFactory.getInstance().createUnitFormat(locale).format(unit);
	}

	public static Unit<? extends Quantity<?>> getUnitFromSymbol(String symbol) {
		Unit<? extends Quantity<?>> unit = UnitFormatFactory.getInstance().createDefaultUnitFormat()
				.parseObject(symbol, new ParsePosition(0));
		if (unit == null) {
			// TODO:add all unit symbols to provide possible choices.
			throw new InvalidChoiceException(symbol);
		} else {
			return unit;

		}
	}

	/**
	 * Normalizes the given quantity by always returning a positive angle beteween 0 and 2
	 * {@link Math#PI}.
	 * 
	 * 0 <= X < 2PI.
	 * 
	 * It is encouraged to use {@link AngleMath} in place of {@link QuantityMath} and this will
	 * automagically be called for you.
	 * 
	 * @see AngleMath
	 * @param original
	 *            the given angle that can be any angle
	 * @return the adjusted angle always greater than or equal to {@link #ANGLE_MIN} and less than
	 *         {@link #ANGLE_MAX}.
	 */
	public static Angle normalize(Angle original) {
		QuantityMath<Angle> adjusted = QuantityMath.create(original);
		if (adjusted.lessThan(ANGLE_MAX) && adjusted.greaterThanEqualTo(ANGLE_MIN)) {
			return original;
		}
		while (adjusted.lessThan(ANGLE_MIN)) {
			adjusted = adjusted.plus(ANGLE_MAX);
		}
		int newScale = 9;
		Angle reasonableMax = QuantityMath.create(ANGLE_MAX).setScale(newScale).getQuantity();
		while (adjusted.setScale(newScale).greaterThanEqualTo(reasonableMax)) {
			adjusted = adjusted.minus(ANGLE_MAX);
		}
		return adjusted.getQuantity();
	}

	/**
	 * Given a heading this will return a new heading that is 90 degrees to the right of the current
	 * heading.
	 * 
	 * 
	 * @param heading
	 * @return
	 */
	public static Angle perpendicularRight(Angle heading) {
		return AngleMath.create(heading).plus(QUARTER_CIRCLE).getQuantity();
	}

	/**
	 * Given a heading this will return a new heading that is 90 degrees to the left of the current
	 * heading.
	 * 
	 * @param heading
	 * @return
	 */
	public static Angle perpendicularLeft(Angle heading) {
		return AngleMath.create(heading).minus(QUARTER_CIRCLE).getQuantity();
	}

	/**
	 * Given two elevations this will calculate the difference and return the change.
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static ElevationChange createElevationChange(Elevation start, Elevation end) {
		return createElevationChange(QuantityMath.create(start).minus(end).getQuantity());
	}

	/**
	 * Given the target location this will return a function that will calculate the length from the
	 * input {@link GeoCoordinate}.
	 * 
	 * @param target
	 * @return
	 */
	public static Function<GeoCoordinate, Length> lengthAwayFunction(final GeoCoordinate target) {
		return new Function<GeoCoordinate, Length>() {

			@Override
			public Length apply(GeoCoordinate input) {
				if (input == null) {
					return null;
				}
				return GeoMath.length(target, input);
			}
		};
	}

	/**
	 * Provides an Ordering which is a {@link Comparator} for sorting any {@link Quantity}.
	 * 
	 * @return
	 */
	public static <Q extends Quantity<Q>> Ordering<Q> quantityOrdering() {
		return new Ordering<Q>() {

			@Override
			public int compare(Q left, Q right) {
				return QuantityMath.create(left).compareTo(right);
			}

		};

	}

	/**
	 * Since Quantity is not {@link Comparable} this will make it so.
	 * 
	 * @param quantity
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <Q extends Quantity<Q>> Comparable<Q> comparable(final Q quantity) {
		try {
			return (Comparable<Q>) quantity;
		} catch (ClassCastException e) {

			throw new ClassCastException(quantity.getClass() + " is not comparable.  Implement this quantity in "
					+ MeasurementQuantityFactory.class);

		}
	}

	/**
	 * Composes the given function which produces a quantity with the {@link #comparableFunction()}
	 * handling the generic stuff for you.
	 * 
	 * @param function
	 * @return
	 */
	public static <F, Q extends Quantity<Q>> Function<F, Comparable<Q>> comparableFunction(Function<F, Q> function) {
		final Function<Q, Comparable<Q>> comparableFunction = comparableFunction();
		Function<F, Comparable<Q>> composed = Functions.compose(comparableFunction, function);
		return composed;
	}

	public static <Q extends Quantity<Q>> Function<Q, Comparable<Q>> comparableFunction() {
		return new Function<Q, Comparable<Q>>() {

			@Override
			public Comparable<Q> apply(Q input) {
				if (input == null) {
					return null;
				}
				return comparable(input);
			}
		};
	}

	/**
	 * Given a ratio this will return an Integer rounded to the nearest 10s always up.
	 * 
	 * @param ratio
	 */
	public static Integer roundPercentToTens(Ratio ratio) {
		double percent = ratio.doubleValue(ExtraUnits.PERCENT);
		return ((int) (Math.round((percent + 4.9) / 10.0) * 10.0));
	}

	/**
	 * Provides the {@link Count} given then standard unit that is almost always used to represent
	 * count: {@link ExtraUnits#COUNT}.
	 * 
	 * null given returns null.
	 * 
	 * @param i
	 * @return
	 */
	@Nullable
	public static Count count(@Nullable Integer i) {
		return (i != null) ? QuantityFactory.getInstance(Count.class).create(i, ExtraUnits.COUNT) : null;
	}

	/**
	 * Convenience to provide {@link Integer} for a count. null if null is given.
	 * 
	 * @param count
	 * @return
	 */
	@Nullable
	public static Integer integer(@Nullable Count count) {
		return (count != null) ? (int) count.doubleValue(ExtraUnits.COUNT) : null;
	}

	/**
	 * Assuming a portion of 100% is given this will return the remaining portion required to reach
	 * 100%. If a ratio is given outside the bounds of 0-1 then the result will be returned to that
	 * boundary.
	 * 
	 * @param portion
	 * @return
	 */
	public static Ratio percentRemaining(Ratio portion) {
		return QuantityMath.create(RATIO_ONE).minus(portion).min(RATIO_ONE).max(RATIO_ZERO).getQuantity();
	}

	/**
	 * Produces the {@link GeoCoordinate#toXyString()}.
	 * 
	 * @return
	 */
	public static Function<GeoCoordinate, String> geoCoordinateXyFunction() {
		return new Function<GeoCoordinate, String>() {

			@Override
			public String apply(GeoCoordinate input) {
				return (input != null) ? input.toXyString() : null;
			}
		};
	}

	public static Function<Integer, Count> countFunction() {
		return new Function<Integer, Count>() {

			@Override
			public Count apply(Integer input) {
				return (input != null) ? count(input) : null;
			}
		};
	}
}
