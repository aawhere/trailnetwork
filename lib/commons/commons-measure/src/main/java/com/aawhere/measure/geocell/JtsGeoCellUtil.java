/**
 * 
 */
package com.aawhere.measure.geocell;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.JtsMeasureUtil;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * Utility methods for performing GeoSpatial calculations using {@link GeoCell}s.
 * 
 * @author Brian Chapman
 * 
 */
public class JtsGeoCellUtil {

	/**
	 * Converts {@link GeoCells} to a list of {@link Coordinate} representing the center of each
	 * {@link GeoCell}.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static List<Coordinate> centerCoordinates(GeoCells geoCells) {
		List<Coordinate> coords = new ArrayList<>();
		for (GeoCell geoCell : geoCells) {
			coords.add(centerCorrdinate(geoCell));
		}
		return coords;
	}

	/**
	 * Converts {@link GeoCells} to a list of {@link Coordinate} representing the four corners of
	 * each {@link GeoCell}.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static List<Coordinate> boxCoordinates(GeoCells geoCells) {
		List<Coordinate> coords = new ArrayList<>();
		for (GeoCell geoCell : geoCells) {
			BoundingBox box = GeoCellUtil.boundingBoxFromGeoCell(geoCell);
			coords.add(JtsMeasureUtil.convert(box.getNorthEast()));
			coords.add(JtsMeasureUtil.convert(box.getNorthWest()));
			coords.add(JtsMeasureUtil.convert(box.getSouthEast()));
			coords.add(JtsMeasureUtil.convert(box.getSouthWest()));
		}
		return coords;
	}

	/**
	 * Converts a {@link GeoCell} to a point representing the center of the {@link GeoCell}.
	 * 
	 * @param geocell
	 * @return
	 */
	public static Coordinate centerCorrdinate(GeoCell geocell) {
		BoundingBox box = GeoCellUtil.boundingBoxFromGeoCell(geocell);
		GeoCoordinate center = box.getCenter();
		return JtsMeasureUtil.convert(center);
	}

}
