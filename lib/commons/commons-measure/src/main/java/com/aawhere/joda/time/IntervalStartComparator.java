/**
 * 
 */
package com.aawhere.joda.time;

import java.util.Comparator;

import org.joda.time.Interval;

/**
 * Simple comparator used to sort intervals by start time.
 * 
 * @author aroller
 * 
 */
public class IntervalStartComparator
		implements Comparator<Interval> {

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Interval o1, Interval o2) {
		return o1.getStart().compareTo(o2.getStart());
	}

}
