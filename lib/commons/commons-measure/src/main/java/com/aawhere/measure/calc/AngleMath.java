/**
 * 
 */
package com.aawhere.measure.calc;

import javax.measure.quantity.Angle;

import com.aawhere.measure.MeasurementUtil;

/**
 * Extends {@link QuantityMath} to make angle math easier by always returning a positive angle
 * beteween 0 and 2 {@link Math#PI}.
 * 
 * @see MeasurementUtil#normalize(Angle)
 * @author aroller
 * 
 */
public class AngleMath
		extends QuantityMath<Angle> {

	/**
	 * @param quantity
	 */
	public AngleMath(Angle angle) {
		super(MeasurementUtil.normalize(angle));
	}

	public static AngleMath create(Angle angle) {
		return new AngleMath(angle);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.calc.QuantityMath#produceResult(double)
	 */
	@Override
	protected AngleMath produceResult(double result) {
		return new AngleMath(produceQuantity(result));
	}

	/**
	 * Provides the exact opposite angle of this one.
	 * 
	 * @return
	 */
	public AngleMath reverse() {
		return (AngleMath) plus(MeasurementUtil.HALF_CIRCLE);
	}

	/**
	 * Changes the behavior of min/max to automatically adjust the extremes to accommodate the
	 * crossing of angle=0. Assuming incoming is always between 0-2PI, this will adjust the min/max
	 * if they are out of order (min > max). The extreme that can make a difference will be
	 * adjusted, but only one extreme will be adjusted to avoid false positives.
	 * 
	 */
	@Override
	public Boolean between(Angle min, Angle max, Boolean includeEndpoints) {

		QuantityMath<Angle> minMath = QuantityMath.create(min);
		if (minMath.greaterThan(max)) {
			// min/max are mixed up, but give target a chance by modifying the extreme that can make
			// a difference
			if (minMath.greaterThan(getQuantity())) {
				// rewind a full circle...don't use angle math because we want negatives
				min = minMath.minus(MeasurementUtil.ANGLE_MAX).getQuantity();
			} else {
				max = QuantityMath.create(max).plus(MeasurementUtil.ANGLE_MAX).getQuantity();
			}
		}

		return super.between(min, max, includeEndpoints);
	}

	/**
	 * Provides the change in the angle from the previous to the next...ensuring if the angle
	 * crosses over 0 then adjustments will be made to report the smaller angle. It also ensures
	 * that the closest Angle to the new heading will be used so values will only ever be between
	 * -180 to 180.
	 * 
	 * Previous = {@link #getQuantity()}
	 * 
	 * <pre>
	 *  examples: 
	 *  
	 * previous = 0,   	next =10, 	delta =10
	 * previous = 350, 	next=0, 	delta =10
	 * previous = 0,   	next=350, 	delta =-10
	 * previous = 0,   	next=180, 	delta =180
	 * previous = 0,   	next=185, 	delta =-175
	 * 
	 * <pre>
	 * 
	 * use {@link #absolute()} if a direction of change does not matter.
	 * @param next
	 * @return
	 */
	public QuantityMath<Angle> delta(Angle next) {
		next = MeasurementUtil.normalize(next);
		Angle previous = getQuantity();
		QuantityMath<Angle> nextMath = QuantityMath.create(next);
		if (nextMath.lessThan(previous)) {
			nextMath = nextMath.plus(MeasurementUtil.ANGLE_MAX);
		}
		nextMath = nextMath.minus(previous);
		if (nextMath.greaterThan(MeasurementUtil.HALF_CIRCLE)) {
			nextMath = nextMath.minus(MeasurementUtil.ANGLE_MAX);
		}
		return nextMath;
	}
}
