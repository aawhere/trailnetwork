/**
 * 
 */
package com.aawhere.measure.calc;

import javax.measure.quantity.Quantity;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.Unit;

import org.apache.commons.math.stat.descriptive.WeightedEvaluation;
import org.joda.time.Duration;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.math.stats.StorelessWeightedEvaluation;
import com.aawhere.math.stats.WeightedEvaluationFactory;
import com.aawhere.measure.MeasurementUtil;

/**
 * Performs {@link WeightedEvaluation}s for {@link Quantity} values similar to
 * {@link QuantityUnivariateStatistic}.
 * 
 * @author Aaron Roller
 * 
 */
public class QuantityWeightedEvaluation<Q extends Quantity<Q>> {

	/**
	 * Used to construct all instances of QuantityWeightedEvaluation.
	 */
	public static class Builder<Q extends Quantity<Q>>
			extends ObjectBuilder<QuantityWeightedEvaluation<Q>> {

		public Builder() {
			super(new QuantityWeightedEvaluation<Q>());
		}

		public Builder<Q> setWeightedEvaluation(StorelessWeightedEvaluation weightedEvaluation) {
			building.weightedEvaluation = weightedEvaluation;
			return this;
		}

		@Override
		protected void validate() {

			super.validate();
			Assertion.assertNotNull("weightedEvaluation", building.weightedEvaluation);
		}

	}// end Builder

	public static <Q extends Quantity<Q>> QuantityWeightedEvaluation<Q> newMean() {
		return new Builder<Q>().setWeightedEvaluation(WeightedEvaluationFactory.newMean()).build();
	}

	/** Use {@link Builder} to construct QuantityWeightedEvaluation */
	private QuantityWeightedEvaluation() {
	}

	private StorelessWeightedEvaluation weightedEvaluation;
	private Unit<Q> unit;
	private QuantityFactory<Q> factory;

	public void increment(Q value, Double weight) {
		if (factory == null) {
			this.factory = MeasurementUtil.getFactory(value);
			this.unit = value.getUnit();
		}
		this.weightedEvaluation.increment(value.doubleValue(this.unit), weight);
	}

	public void increment(Q value, Duration weight) {
		this.increment(value, (double) weight.getMillis());
	}

	public Q getResult() {
		return this.factory.create(weightedEvaluation.getResult(), this.unit);
	}

	public Boolean hasResult() {
		return this.factory != null;
	}
}
