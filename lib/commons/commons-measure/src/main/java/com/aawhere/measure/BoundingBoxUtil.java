/**
 * 
 */
package com.aawhere.measure;

import java.util.ArrayList;
import java.util.List;

import javax.measure.quantity.Length;

import com.aawhere.collections.IterablesExtended;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Useful methods handling {@link BoundingBox} and friends.
 * 
 * TODO: Consider using GeoTools gt-main library to provide a lot of this geo functionality.
 * http://docs.geotools.org/latest/javadocs/org/opengis/geometry/BoundingBox.html
 * 
 * @author aroller
 * 
 */
public class BoundingBoxUtil {

	public static BoundingBox WORLD = MeasurementUtil.WORLD_BOUNDARY;

	/**
	 * Combines two bounding boxes returning the extremes resulting in a bounding box that could
	 * contain both.
	 * 
	 * TODO: Handle meridian case
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BoundingBox combine(BoundingBox first, BoundingBox second) {
		Latitude minLat = new QuantityMath<Latitude>(first.getMin().getLatitude()).min(second.getMin().getLatitude())
				.getQuantity();
		Longitude minLon = new QuantityMath<Longitude>(first.getMin().getLongitude()).min(second.getMin()
				.getLongitude()).getQuantity();
		Latitude maxLat = new QuantityMath<Latitude>(first.getMax().getLatitude()).max(second.getMax().getLatitude())
				.getQuantity();
		Longitude maxLon = new QuantityMath<Longitude>(first.getMax().getLongitude()).max(second.getMax()
				.getLongitude()).getQuantity();
		return new BoundingBox.Builder().setWest(minLon).setEast(maxLon).setNorth(maxLat).setSouth(minLat).build();
	}

	public static BoundingBox combinedProviders(Iterable<? extends BoundingBoxProvider> providers) {
		return combined(Iterables.transform(providers, boundingBoxFromProviderFunction()));

	}

	public static Function<BoundingBoxProvider, BoundingBox> boundingBoxFromProviderFunction() {
		return new Function<BoundingBoxProvider, BoundingBox>() {

			@Override
			public BoundingBox apply(BoundingBoxProvider input) {
				return (input == null) ? null : input.boundingBox();
			}
		};
	}

	/**
	 * Combines all the {@link BoundingBox} objects in {@link BoundingBoxes} using
	 * {@link #combine(BoundingBox, BoundingBox)}.
	 * 
	 * @param boundingBoxes
	 * @return
	 */
	public static BoundingBox combined(Iterable<BoundingBox> boundingBoxes) {
		BoundingBox result = null;
		for (BoundingBox boundingBox : boundingBoxes) {
			if (result == null) {
				result = boundingBox;
			} else {
				result = combine(result, boundingBox);
			}

		}
		return result;
	}

	/**
	 * Admittedly this could be provided by GeoTools or JTS. If geometry gets complicated, switch to
	 * using them.
	 * 
	 * @param boundingBox
	 * @param that
	 * @return
	 */
	public static Boolean contains(BoundingBox dis, BoundingBox that) {
		return new QuantityMath<Latitude>(dis.getNorth()).greaterThanEqualTo(that.getNorth())
				&& new QuantityMath<Latitude>(dis.getSouth()).lessThanEqualTo(that.getSouth())
				&& new QuantityMath<Longitude>(dis.getEast()).greaterThanEqualTo(that.getEast())
				&& new QuantityMath<Longitude>(dis.getWest()).lessThanEqualTo(that.getWest());
	}

	/**
	 * Provides true if an only if the given location falls within the given bounding box.
	 * 
	 * FIXME: This does not work across the international date line.
	 * 
	 * @param box
	 * @param location
	 * @return
	 */
	public static Boolean contains(BoundingBox box, GeoCoordinate location) {
		QuantityMath<Latitude> latitudeMath = QuantityMath.create(location.getLatitude());
		QuantityMath<Longitude> longitudeMath = QuantityMath.create(location.getLongitude());
		return latitudeMath.between(box.getSouth(), box.getNorth())
				&& longitudeMath.between(box.getWest(), box.getEast());
	}

	public static Boolean equals(BoundingBox left, BoundingBox right) {

		return QuantityMath.create(left.getNorth()).equals(right.getNorth())
				&& QuantityMath.create(left.getSouth()).equals(right.getSouth())
				&& QuantityMath.create(left.getEast()).equals(right.getEast())
				&& QuantityMath.create(left.getWest()).equals(right.getWest());
	}

	/**
	 * Converts the bounding box to a series of coordinates starting at the
	 * {@link BoundingBox#getSouthWest()} going clockwise all the way back to and including
	 * Southwest again. This is useful when creating Polygon or LineString since for a polygon the
	 * same point for start and finish must be provided.
	 * 
	 * @param box
	 * @return
	 */
	public static List<GeoCoordinate> coordinates(BoundingBox box) {
		ArrayList<GeoCoordinate> coordinates = new ArrayList<>();
		coordinates.add(box.getSouthWest());
		coordinates.add(box.getNorthWest());
		coordinates.add(box.getNorthEast());
		coordinates.add(box.getSouthEast());
		coordinates.add(box.getSouthWest());
		return coordinates;
	}

	public static BoundingBox boundingBox(GeoCoordinate... coordinates) {
		return boundingBox(Lists.newArrayList(coordinates));
	}

	/**
	 * Provided a series of {@link GeoCoordinate}s this will find the bounding box related to the
	 * coordinates.
	 * 
	 * WARNING: This does not handle the meridian case.
	 * 
	 * 
	 * @param coordinates
	 * @return
	 */
	public static BoundingBox boundingBox(Iterable<GeoCoordinate> coordinates) {
		double minLat = Latitude.MAX;
		double minLon = Longitude.MAX;
		double maxLat = Latitude.MIN;
		double maxLon = Longitude.MIN;
		;

		for (GeoCoordinate geoCoordinate : coordinates) {
			double lat = geoCoordinate.getLatitude().getInDecimalDegrees();
			double lon = geoCoordinate.getLongitude().getInDecimalDegrees();
			minLat = Math.min(minLat, lat);
			minLon = Math.min(minLon, lon);
			maxLat = Math.max(maxLat, lat);
			maxLon = Math.max(maxLon, lon);
		}
		return BoundingBox.create().setNorth(maxLat).setSouth(minLat).setEast(maxLon).setWest(minLon).build();
	}

	/**
	 * Provided the boundingbox and a point this will determine the farthest corner from the point.
	 * 
	 * @param box
	 * @param point
	 * @return
	 */
	public static GeoCoordinate farthestPoint(BoundingBox box, GeoCoordinate point) {
		Iterable<GeoCoordinate> extremes = box.getExtremes();
		return GeoMath.farthest(point, extremes);
	}

	/**
	 * Provides the closest point to the bounding box coordinates including the midpoint in case the
	 * point is within the bounds this may be more accurate if the point is within a large bounding
	 * box.
	 * 
	 * @param box
	 * @param point
	 * @return
	 */
	public static GeoCoordinate closestPoint(BoundingBox box, GeoCoordinate point) {
		return GeoMath.closest(point, IterablesExtended.concat(box.getExtremes(), box.getCenter()));
	}

	/**
	 * Adds padding to the bounding box in each direction equal to the lenght of the given padding.
	 * So the total extra length of the bounding box is 2 x padding length (horizontal and vertical
	 * each). The diagonal length will grow at the Pythagorean portion.
	 * 
	 * @param boundingBox
	 * @param defaultBufferWidth
	 * @return
	 */
	public static BoundingBox expand(BoundingBox boundingBox, Length padding) {
		GeoCoordinate north = GeoMath.coordinateFrom(boundingBox.getMax(), MeasurementUtil.NORTH, padding);
		GeoCoordinate south = GeoMath.coordinateFrom(boundingBox.getMin(), MeasurementUtil.SOUTH, padding);
		GeoCoordinate east = GeoMath.coordinateFrom(boundingBox.getMax(), MeasurementUtil.EAST, padding);
		GeoCoordinate west = GeoMath.coordinateFrom(boundingBox.getMin(), MeasurementUtil.WEST, padding);
		return boundingBox(north, south, east, west);
	}

	/**
	 * Expands the bounding box to be practically square making the shorter side equal to the
	 * longer.
	 */
	public static BoundingBox expandSquare(BoundingBox boundingBox) {
		Length width = boundingBox.getWidth();
		Length height = boundingBox.getHeight();
		Length half = QuantityMath.create(width).max(height).dividedBy(2).getQuantity();
		GeoCoordinate center = boundingBox.getCenter();
		GeoCoordinate north = GeoMath.coordinateFrom(center, MeasurementUtil.NORTH, half);
		GeoCoordinate south = GeoMath.coordinateFrom(center, MeasurementUtil.SOUTH, half);
		GeoCoordinate east = GeoMath.coordinateFrom(center, MeasurementUtil.EAST, half);
		GeoCoordinate west = GeoMath.coordinateFrom(center, MeasurementUtil.WEST, half);
		return boundingBox(north, south, east, west);
	}

	/**
	 * Provides the length from the given target to the CenterPoint of the bounding box.
	 * 
	 * * The centerpoint of the geocell (rather than the closest corner) is used for simplicity
	 * since it is a single point and when compared to all other center points should provide a
	 * reasonable comparison.
	 * 
	 * @param target
	 * @return
	 */
	public static Function<BoundingBox, Length> proximityFunction(final GeoCoordinate target) {
		return new Function<BoundingBox, Length>() {

			@Override
			public Length apply(BoundingBox input) {
				return (input != null) ? GeoMath.length(target, input.getCenter()) : null;
			}
		};
	}

	/**
	 * Set the bounds of the box to the box created by the center point and the distance between the
	 * center and a side . This is also a convenient approximation of a circle made from the center
	 * and the box width as the radius.
	 * 
	 * @param center
	 * @param halfDiagonal
	 * @return
	 */
	public static BoundingBox boundedBy(GeoCoordinate center, Length radius) {
		// using boundingBox(GeoCoordinates...) breakes meridian case.
		return BoundingBox.create()
				.setNorth(GeoMath.coordinateFrom(center, MeasurementUtil.NORTH, radius).getLatitude())
				.setEast(GeoMath.coordinateFrom(center, MeasurementUtil.EAST, radius).getLongitude())
				.setSouth(GeoMath.coordinateFrom(center, MeasurementUtil.SOUTH, radius).getLatitude())
				.setWest(GeoMath.coordinateFrom(center, MeasurementUtil.WEST, radius).getLongitude()).build();

	}

}
