/**
 *
 */
package com.aawhere.joda.time.calc;

import org.apache.commons.math.stat.descriptive.StorelessUnivariateStatistic;
import org.apache.commons.math.stat.descriptive.UnivariateStatistic;
import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.rank.Max;
import org.apache.commons.math.stat.descriptive.rank.Min;
import org.apache.commons.math.stat.descriptive.summary.Sum;
import org.joda.time.Duration;

import com.aawhere.lang.ObjectBuilder;

/**
 * Applies the {@link UnivariateStatistic} functions to {@link Duration} values.
 * 
 * @author Aaron Roller
 * 
 */
public class DurationUnivariateStatistic<U extends StorelessUnivariateStatistic> {

	/**
	 * Used to construct all instances of DurationUnivariateStatistic.
	 */
	public static class Builder<U extends StorelessUnivariateStatistic>
			extends ObjectBuilder<DurationUnivariateStatistic<U>> {

		public Builder() {
			super(new DurationUnivariateStatistic<U>());
		}

		public Builder<U> setStatistic(U statistic) {
			building.statistic = statistic;
			return this;
		}

	}// end Builder

	public static DurationUnivariateStatistic<Mean> newMean() {
		return new DurationUnivariateStatistic.Builder<Mean>().setStatistic(new Mean()).build();
	}

	public static DurationUnivariateStatistic<Min> newMin() {
		return new DurationUnivariateStatistic.Builder<Min>().setStatistic(new Min()).build();

	}

	public static DurationUnivariateStatistic<Max> newMax() {
		return new DurationUnivariateStatistic.Builder<Max>().setStatistic(new Max()).build();

	}

	public static DurationUnivariateStatistic<Sum> newSum() {
		return new DurationUnivariateStatistic.Builder<Sum>().setStatistic(new Sum()).build();

	}

	/** Use {@link Builder} to construct DurationUnivariateStatistic */
	private DurationUnivariateStatistic() {
	}

	private U statistic;

	public void increment(Duration duration) {
		statistic.increment(duration.getMillis());
	}

	/**
	 * Returns the result if an increment has been called or throws an exception otherwise. Use
	 * {@link #hasResult()} to check if a value is available.
	 * 
	 * @return the result. Never returns null.
	 */
	public Duration getResult() {
		if (!hasResult()) {
			throw new NullPointerException("use #hasResult to avoid this");
		}
		return new Duration((long) this.statistic.getResult());
	}

	public Boolean hasResult() {
		return this.statistic.getN() > 0;
	}
}
