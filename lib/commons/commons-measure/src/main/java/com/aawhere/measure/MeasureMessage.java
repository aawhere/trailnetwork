/**
 * 
 */
package com.aawhere.measure;

import javax.measure.quantity.Quantity;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Messages for {@link Quantity} and other measurement related items.
 * 
 * @author aroller
 * 
 */
public enum MeasureMessage implements Message {

	/**
	 * The bounds string has a specific format that must be followed to be understood. This is a
	 * general failure when parsing is not succeeding.
	 */
	INVALID_BOUNDS("{BOUNDS} does not match the required bounding box format: "
			+ MeasureMessage.Doc.BOUNDING_BOX_FORMAT_SNIPPET, Param.BOUNDS), INVALID_GEO_CELL(
			"{GEO_CELL} must contain only characters from '{GEO_CELL_ALPHABET}': {HIGHLIGHTED_INVALID_CHARACTERS} ",
			Param.GEO_CELL,
			Param.GEO_CELL_ALPHABET,
			Param.HIGHLIGHTED_INVALID_CHARACTERS),
	/** Used when translating a number string into a number. */
	INVALID_NUMBER_FORMAT("{NUMBER} is not a valid number.", Param.NUMBER);

	private ParamKey[] parameterKeys;
	private String message;

	private MeasureMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** The {@link BoundingBox} string format. */
		BOUNDS,
		/** A string highlighted the invalid characters presented. */
		HIGHLIGHTED_INVALID_CHARACTERS,
		/** The geocell value attempt. */
		GEO_CELL,
		/** Possible characters for Geocells */
		GEO_CELL_ALPHABET,
		/** Parameter to identity the number provided */
		NUMBER;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static class Doc {
		public static final String BOUNDING_BOX_FORMAT_SNIPPET = "minLat,minLon|maxLat,maxLon";

		public static class GeoCell {
			public static final String RESOLUTION_DATA_TYPE = "Resolution";
		}
	}
}
