/**
 *
 */
package com.aawhere.measure.unit.format;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import javax.measure.unit.UnitFormat;
import javax.measure.unit.format.LocalFormat;
import javax.measure.unit.format.LocalFormatFactory;

import com.aawhere.lang.ResourceUtils;

/**
 * Create instances of {@link UnitFormat} using our own {@link ResourceBundle}. UnitFormat objects
 * created are cached for performance reasons.
 * 
 * NOTE: This factory is located in the same package as the main libraries
 * 
 * @author Brian Chapman
 * 
 */
public class UnitFormatFactory {

	private static final String BUNDLE_NAME = UnitFormatFactory.class.getPackage().getName() + ".ExtraUnits";

	private static UnitFormatFactory instance;
	private Map<Locale, UnitFormat> unitFormatMap = new ConcurrentHashMap<Locale, UnitFormat>();

	/**
	 * Used to retrieve the only instance of UnitFormatFactory.
	 */
	public static UnitFormatFactory getInstance() {

		if (instance == null) {
			instance = new UnitFormatFactory();
		}
		return instance;
	}// end Builder

	/** Use {@link #getInstance()} to get UnitFormatFactory */
	private UnitFormatFactory() {
	}

	/**
	 * Returns the unit format for the default locale.
	 * 
	 * @return {@link LocalFormat}
	 */
	public UnitFormat createDefaultUnitFormat() {
		return getUnitFormat(Locale.getDefault());
	}

	/**
	 * Returns the unit format for the specified locale.
	 * 
	 * @param locale
	 *            the locale for which the format is returned.
	 * @return {@link LocalFormat#getInstance(java.util.Locale)}
	 */
	public UnitFormat createUnitFormat(Locale locale) {
		return getUnitFormat(locale);
	}

	private UnitFormat getUnitFormat(Locale locale) {
		if (unitFormatMap.containsKey(locale)) {
			return unitFormatMap.get(locale);
		} else {
			ResourceBundle rb = getResourceBundle(locale);
			UnitFormat unitFormat = LocalFormatFactory.getInstance(rb);
			unitFormatMap.put(locale, unitFormat);
			return unitFormat;
		}
	}

	private ResourceBundle getResourceBundle(Locale locale) {
		return ResourceUtils.getResourceBundle(BUNDLE_NAME, locale);
	}

}
