/**
 * 
 */
package com.aawhere.measure;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.number.NumberUtilsExtended;
import com.aawhere.lang.string.FromString;

/**
 * An alternate way of getting a bounding bound from a string. This is standard for Google earth
 * according to the KML docs.
 * 
 * https://developers.google.com/kml/documentation/kmlreference#viewformat
 * 
 * BBOX=[bboxWest],[bboxSouth],[bboxEast],[bboxNorth]
 * 
 * This information matches the Web Map Service (WMS) bounding box specification.
 * 
 * @author aroller
 * 
 */
public class WmsBoundingBoxFormat {

	/**
	 * Used to construct all instances of WmsBoundingBoxFormat.
	 */
	public static class Builder
			extends ObjectBuilder<WmsBoundingBoxFormat> {

		public Builder() {
			super(new WmsBoundingBoxFormat());
		}

		public Builder setString(String string) {
			building.string = string;
			return this;
		}

		public Builder setBoundingBox(BoundingBox boundingBox) {
			building.boundingBox = boundingBox;
			return this;
		}

		public WmsBoundingBoxFormat build() {
			WmsBoundingBoxFormat built = super.build();
			final char separatorChar = ',';
			if (built.string != null) {
				Double[] parts = NumberUtilsExtended.split(built.string, String.valueOf(separatorChar));
				BoundingBox.Builder boxBuilder = BoundingBox.create();
				boxBuilder.setWest(parts[0]);
				boxBuilder.setSouth(parts[1]);
				boxBuilder.setEast(parts[2]);
				boxBuilder.setNorth(parts[3]);
				built.boundingBox = boxBuilder.build();
			} else {
				if (built.boundingBox == null) {
					built.boundingBox = BoundingBox.create().setDefault().build();
				}

				final List<String> parts = Arrays.asList(	format(built.boundingBox.getWest()),
															format(built.boundingBox.getSouth()),
															format(built.boundingBox.getEast()),
															format(built.boundingBox.getNorth()));
				built.string = StringUtils.join(parts, separatorChar);
			}

			return built;
		}

		String format(GeoQuantity<?> quantity) {
			return String.valueOf(quantity.getInDecimalDegrees());
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private BoundingBox boundingBox;
	private String string;

	/** Use {@link Builder} to construct WmsBoundingBoxFormat */
	private WmsBoundingBoxFormat() {
	}

	/**
	 * @return the boundingBox
	 */
	public BoundingBox getBoundingBox() {
		return this.boundingBox;
	}

	/**
	 * @return the string
	 */
	public String getString() {
		return this.string;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getString();
	}

	/**
	 * Standard String setter conforming to {@link FromString}.
	 * 
	 * @param bbox
	 * @return
	 */
	public WmsBoundingBoxFormat valueOf(String bbox) {
		return create().setString(bbox).build();
	}
}
