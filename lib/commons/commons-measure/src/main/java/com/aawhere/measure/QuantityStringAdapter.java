/**
 *
 */
package com.aawhere.measure;

import java.text.NumberFormat;
import java.text.ParseException;

import javax.measure.quantity.Quantity;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.Unit;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringFormatException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Adapts a string into any {@link Quantity} that follows the required format:
 * 
 * {number}{space delimiter}{unit symbol}
 * 
 * The format could be expanded upon or multiple formats could be supported if the above is limited.
 * 
 * This represents all quantities and the concrete type of the Quantity must be provided during the
 * {@link #unmarshal(String, Class)} method invokation.
 * 
 * @author aroller
 * 
 */
public class QuantityStringAdapter<Q extends Quantity<Q>>
		implements StringAdapter<Q> {

	static final String SEPARATOR = " ";

	/**
	 * Used to construct all instances of QuantityStringAdapter.
	 */
	public static class Builder<Q extends Quantity<Q>>
			extends ObjectBuilder<QuantityStringAdapter<Q>> {

		public Builder() {
			super(new QuantityStringAdapter<Q>());
		}

	}// end Builder

	/** Use {@link Builder} to construct QuantityStringAdapter */
	private QuantityStringAdapter() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Object quantity) {
		if (quantity instanceof Quantity) {
			Quantity<?> q = (Quantity<?>) quantity;
			StringBuilder result = new StringBuilder();
			result.append(q.getValue());
			result.append(SEPARATOR);
			result.append(MeasurementUtil.getUnitSymbol(q.getUnit()));
			return result.toString();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#unmarshal(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <QuantityT extends Q> QuantityT unmarshal(String string, Class<QuantityT> type) throws StringFormatException {
		String[] parts = StringUtils.split(string, SEPARATOR);
		if (parts.length != 2) {
			// we should probably elaborate on the actual format, but this is a start
			throw new StringFormatException(string, type);
		} else {
			String numberPart = parts[0];
			Number number;
			try {
				number = NumberFormat.getNumberInstance().parse(numberPart);
			} catch (ParseException e) {
				throw new StringFormatException(new CompleteMessage.Builder(MeasureMessage.INVALID_NUMBER_FORMAT)
						.addParameter(MeasureMessage.Param.NUMBER, numberPart).build(), e);
			}
			String symbolPart = parts[1];
			// this throws a runtime rather than string format...which is not great.
			// call a method that throws a checked exception, catch it and convert to string format
			// if desired.
			Unit<Q> unit = (Unit<Q>) MeasurementUtil.getUnitFromSymbol(symbolPart);
			return (QuantityT) QuantityFactory.getInstance((Class<Q>) type).create(number, unit);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#getType()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Class<Q> getType() {
		return (Class<Q>) (Class<?>) Quantity.class;
	}

}
