/**
 * 
 */
package com.aawhere.measure;

import java.util.Collection;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

/**
 * Mostly needed for avoiding painful interactions with JAXB!
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class BoundingBoxes
		implements Iterable<BoundingBox> {

	/**
	 * Used to construct all instances of BoundingBoxes.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<BoundingBoxes> {

		public Builder() {
			super(new BoundingBoxes());
		}

		public Builder setAll(Collection<? extends BoundingBox> all) {
			building.boundingBoxes = all;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.exceptions().notNull("boundingBoxes", building.boundingBoxes);

		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct BoundingBoxes */
	private BoundingBoxes() {
	}

	@XmlElement(name = "boundingBox")
	private Collection<? extends BoundingBox> boundingBoxes;

	/**
	 * @return the boundingBoxes
	 */
	public Collection<? extends BoundingBox> getBoundingBoxes() {
		return this.boundingBoxes;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Iterator<BoundingBox> iterator() {
		return (Iterator<BoundingBox>) this.boundingBoxes.iterator();
	}
}
