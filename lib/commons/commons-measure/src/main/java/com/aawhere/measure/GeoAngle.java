/**
 *
 */
package com.aawhere.measure;

import javax.measure.quantity.Quantity;

/**
 * 
 * Base Quantity for measurements of the earth.
 * 
 * @author Brian Chapman
 * 
 */
public interface GeoAngle
		extends Quantity<GeoAngle> {

	/** A common method to retrieve the most used unit */
	public double getInDecimalDegrees();
}
