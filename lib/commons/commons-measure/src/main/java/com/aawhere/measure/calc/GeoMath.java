/**
 *
 */
package com.aawhere.measure.calc;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.GeoQuantity;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.unit.ExtraUnits;

import com.bbn.openmap.LatLonPoint;
import com.bbn.openmap.proj.GreatCircle;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;

/**
 * A tool for performing calculations on Geometric points.
 * 
 * @author Brian Chapman
 * 
 */
public class GeoMath {

	/**
	 * @deprecated use #length since we know it is horizontal because {@link GeoCoordinate} doesn't
	 *             have elevation
	 */
	@Deprecated
	public static Length horizontalDistanceBetween(GeoCoordinate origin, GeoCoordinate destination) {
		return length(origin, destination);
	}

	public static Length length(GeoCoordinate origin, GeoCoordinate destination) {
		return MeasurementUtil.createLengthInMeters((horizontalDistanceInMeters(origin, destination)));
	}

	public static Length halfLength(GeoCoordinate origin, GeoCoordinate destination) {
		return QuantityMath.create(length(origin, destination)).dividedBy(2).getQuantity();
	}

	private static double horizontalDistanceInMeters(GeoCoordinate origin, GeoCoordinate destination) {
		float originLat = openMapLatitude(origin);
		float originLon = openMapLongitude(origin);
		float destinationLat = openMapLatitude(destination);
		float destinationLon = openMapLongitude(destination);
		double distanceInRadians = GreatCircle.spherical_distance(originLat, originLon, destinationLat, destinationLon);

		float distanceInMeters = com.bbn.openmap.proj.Length.METER.fromRadians((float) distanceInRadians);

		return distanceInMeters;
	}

	/**
	 * Provides the heading, also known as course or direction from the origin to the destination.
	 * 
	 * http://en.wikipedia.org/wiki/Course_(navigation)
	 * 
	 * 0 degrees being true north and being measured clockwise to 360 degrees back to true north.
	 * 
	 * bearing is another term, however, that is the angle between our direction and another object
	 * so in such a case we would need a third coordinate.
	 * 
	 * @param origin
	 * @param destination
	 * @return
	 */
	public static Angle heading(GeoCoordinate origin, GeoCoordinate destination) {
		float azimuthBetweenPoints = GreatCircle.spherical_azimuth(	openMapLatitude(origin),
																	openMapLongitude(origin),
																	openMapLatitude(destination),
																	openMapLongitude(destination));
		// Anglemath normalizes automatically.
		return new AngleMath(MeasurementUtil.createAngleInRadians((double) azimuthBetweenPoints)).getQuantity();
	}

	/**
	 * @param origin
	 * @return
	 */
	private static float openMapLongitude(GeoCoordinate origin) {
		return (float) origin.getLongitude().doubleValue(ExtraUnits.RADIAN_LONGITUDE);
	}

	/**
	 * @param origin
	 * @return
	 */
	private static float openMapLatitude(GeoCoordinate origin) {
		return (float) origin.getLatitude().doubleValue(ExtraUnits.RADIAN_LATITUDE);
	}

	/**
	 * @param fromPoint
	 * @param bearing
	 * @param distanceToDesiredPoint
	 * @return
	 */
	public static GeoCoordinate coordinateFrom(GeoCoordinate fromPoint, Angle bearing, Length distanceToDesiredPoint) {
		float distanceInRadians = openMapDistance(distanceToDesiredPoint);

		LatLonPoint point = GreatCircle.spherical_between(	openMapLatitude(fromPoint),
															openMapLongitude(fromPoint),
															distanceInRadians,
															openMapBearing(bearing));
		return coordinateFrom(point);
	}

	/**
	 * The distance between two points along the curve of the earth. The coordinate is required
	 * (mostly for it's latitude) since the arc distance is greater at the equater than at the
	 * poles.
	 * 
	 * http://www.mathopenref.com/arclength.html
	 * http://en.wikipedia.org/wiki/Arc_length#Arcs_of_great_circles_on_the_Earth
	 * 
	 * @see GreatCircle#spherical_distance(float, float, float, float)
	 * 
	 * @param length
	 *            distance "horizontally" for which the arcLength is to be calculated.
	 * @param where
	 * @return
	 */
	public static Angle arcLength(Length length, GeoCoordinate where) {
		GeoCoordinate to = coordinateFrom(where, MeasurementUtil.EAST, length);
		Float radians = GreatCircle.spherical_distance(	openMapLatitude(where),
														openMapLongitude(where),
														openMapLatitude(to),
														openMapLongitude(to));
		return MeasurementUtil.createAngleInRadians(radians.doubleValue());
	}

	/**
	 * The distance between two points of the same latitude along the curve of the earth. Since
	 * {@link Latitude} is the only {@link GeoQuantity} that affects arcLength this allows just
	 * latitude to be known and will use a dummy longitude and simply call
	 * {@link #arcLength(Length, GeoCoordinate)}.
	 * 
	 * http://www.mathopenref.com/arclength.html - *
	 * http://en.wikipedia.org/wiki/Arc_length#Arcs_of_great_circles_on_the_Earth - * - *
	 * 
	 * @see GreatCircle#spherical_distance(float, float, float, float)
	 * 
	 * @param length
	 * @param latitude
	 * @return
	 */
	public static Angle arcLength(Length length, Latitude latitude) {
		return arcLength(length, GeoCoordinate.create().setLatitude(latitude).setLongitude(0.0).build());
	}

	/**
	 * Internal representation of meters in radians used only for openMap calculations which will
	 * subsequently consider adjustments based on location of the earth.
	 * 
	 * @see #arcLength(Length, GeoCoordinate)
	 * 
	 * @param distanceToDesiredPoint
	 * @return
	 */
	private static float openMapDistance(Length distanceToDesiredPoint) {
		return (float) com.bbn.openmap.proj.Length.METER.toRadians(distanceToDesiredPoint
				.doubleValue(MetricSystem.METRE));
	}

	/**
	 * Openmap requires bearing between -PI <= X < PI. Our headings are normalized from 0 <= X <
	 * 2PI.
	 * 
	 * @see MeasurementUtil#normalize(Angle)
	 * 
	 * @param bearing
	 * @return
	 */
	private static float openMapBearing(Angle bearing) {
		float value = (float) bearing.doubleValue(MetricSystem.RADIAN);
		double fullCircle = 2 * Math.PI;
		while (value >= Math.PI) {
			value -= fullCircle;
		}
		while (value < -Math.PI) {
			value += fullCircle;
		}
		return value;
	}

	/**
	 * @param coordinate
	 * @return
	 */
	@SuppressWarnings("unused")
	private static LatLonPoint coordToLatLonPoint(GeoCoordinate coordinate) {
		return new LatLonPoint(coordinate.getLatitude().getInDecimalDegrees(), coordinate.getLongitude()
				.getInDecimalDegrees());
	}

	/**
	 * @param point
	 * @return
	 */
	private static GeoCoordinate coordinateFrom(LatLonPoint point) {

		// upgrading from float to double adds erroneous digits. rouding removes them
		Integer precision = GeoCoordinate.ONE_CM_PRECISION;
		Latitude latitude = QuantityMath.create(MeasurementUtil.createLatitude((double) point.getLatitude()))
				.setScale(precision).getQuantity();
		Longitude longitude = QuantityMath.create(MeasurementUtil.createLongitude((double) point.getLongitude()))
				.setScale(precision).getQuantity();
		return new GeoCoordinate.Builder().setLatitude(latitude).setLongitude(longitude).build();
	}

	/**
	 * provides a new point in between the given.
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static GeoCoordinate midpoint(GeoCoordinate first, GeoCoordinate second) {
		Length halfLength = halfLength(first, second);
		return coordinateFrom(first, heading(first, second), halfLength);
	}

	/**
	 * Provides the farthest {@link GeoCoordinate} from the given extremes to the given point.
	 * 
	 * @param point
	 * @param extremes
	 * @return
	 */
	public static GeoCoordinate farthest(GeoCoordinate point, Iterable<GeoCoordinate> extremes) {
		GeoCoordinate farthestPoint = null;
		QuantityMath<Length> farthest = QuantityMath.create(MeasurementUtil.ZERO_LENGTH);
		for (GeoCoordinate geoCoordinate : extremes) {
			final Length length = length(point, geoCoordinate);
			if (farthest.lessThan(length)) {
				farthestPoint = geoCoordinate;
				farthest = QuantityMath.create(length);
			}
		}
		return farthestPoint;
	}

	/**
	 * Given all the points this will find the closest point
	 * 
	 * @param target
	 * @param extremes
	 * @return
	 */
	public static GeoCoordinate closest(GeoCoordinate target, Iterable<GeoCoordinate> extremes) {

		Function<GeoCoordinate, Comparable<Length>> lengthFunction = MeasurementUtil
				.comparableFunction(lengthFunction(target));
		return Ordering.natural().nullsLast().onResultOf(lengthFunction).min(extremes);
	}

	public static Function<GeoCoordinate, Length> lengthFunction(final GeoCoordinate from) {
		return new Function<GeoCoordinate, Length>() {

			@Override
			public Length apply(GeoCoordinate to) {
				return (to != null) ? length(from, to) : null;
			}
		};
	}
}
