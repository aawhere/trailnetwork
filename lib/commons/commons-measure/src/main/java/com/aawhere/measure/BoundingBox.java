/**
 *
 */
package com.aawhere.measure;

import java.io.Serializable;
import java.util.Collection;

import javax.measure.quantity.Length;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang.StringUtils;

import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;

import com.google.common.collect.Lists;

/**
 * A box created by two @{link GeoCoordinate}s with {@link #southWest} as the min and
 * {@link #northEast} as the max.
 * 
 * @see BoundingBoxTranslatorFactory for storage
 * @author Brian Chapman
 * 
 */
@XmlJavaTypeAdapter(BoundingBoxXmlAdapter.class)
public class BoundingBox
		implements Serializable {

	private static final long serialVersionUID = 8959739621576769316L;
	static final String STRING_POINT_DILIMETER = "|";
	static final int STRING_SW_INDEX = 0;
	static final int STRING_NE_INDEX = 1;

	private GeoCoordinate northEast;
	private GeoCoordinate southWest;

	/**
	 * Required for xml marshaling
	 * 
	 */
	protected BoundingBox() {
	}

	public GeoCoordinate getNorthEast() {
		return this.northEast;
	}

	public GeoCoordinate getSouthWest() {
		return this.southWest;
	}

	public GeoCoordinate getNorthWest() {
		return GeoCoordinate.create().setLatitude(getNorth()).setLongitude(getWest()).build();
	}

	public GeoCoordinate getSouthEast() {
		return GeoCoordinate.create().setLatitude(getSouth()).setLongitude(getEast()).build();
	}

	/** Provides northEast as a cartographer synonym */
	public GeoCoordinate getMax() {
		return getNorthEast();
	}

	/** Provides southWest as a cartographer synonym */
	public GeoCoordinate getMin() {
		return getSouthWest();
	}

	/**
	 * Provides the bounding box as a single string representing the southwest and northeast corners
	 * where the latitude and longitude are separated by commas and the points are separated by a
	 * vertical bar. All values are in decimal degrees.
	 * 
	 * S,W|N,E
	 * 
	 * This syntax was derived from the bounds parameter in the Google Maps Geocoding API.
	 * 
	 * <pre>
	 * http://maps.googleapis.com/maps/api/geocode/json?address=Winnetka&bounds=34.172684,-118.604794|34.236144,-118.500938&sensor=false
	 * </pre>
	 * 
	 * https://developers.google.com/maps/documentation/geocoding/#Viewports
	 * 
	 * 
	 * @return
	 */
	public String getBounds() {
		String[] pointStrings = new String[2];
		pointStrings[STRING_SW_INDEX] = getSouthWest().getAsString();
		pointStrings[STRING_NE_INDEX] = getNorthEast().getAsString();
		return StringUtils.join(pointStrings, STRING_POINT_DILIMETER);
	}

	public Longitude getWest() {
		return this.southWest.getLongitude();
	}

	public Longitude getEast() {
		return this.northEast.getLongitude();
	}

	public Latitude getNorth() {
		return this.northEast.getLatitude();
	}

	public Latitude getSouth() {
		return this.southWest.getLatitude();
	}

	/**
	 * A point representing the center of the BoundingBox.
	 * 
	 * Note: does not work across the 180 meridian and across the poles.
	 * 
	 * @return
	 */
	public GeoCoordinate getCenter() {
		double top = getNorth().getInDecimalDegrees();
		double bottom = getSouth().getInDecimalDegrees();
		double left = getWest().getInDecimalDegrees();
		double right = getEast().getInDecimalDegrees();

		double centerVertical = bottom + ((top - bottom) / 2);
		double centerHorizontal = left + ((right - left) / 2);

		return new GeoCoordinate.Builder().setLatitude(centerVertical).setLongitude(centerHorizontal).build();
	}

	/**
	 * The height of the bounding box (latitude direction)
	 * 
	 * @return
	 */
	public Length getHeight() {
		GeoCoordinate northWest = new GeoCoordinate.Builder().setLatitude(getNorth()).setLongitude(getWest()).build();
		return GeoMath.length(getSouthWest(), northWest);
	}

	/**
	 * The width of the bounding box (longitude) at the center (average width). Note that Bounding
	 * Boxes can be approximated as trapezoids near the poles and rectangles near the equator.
	 * 
	 * @return
	 */
	public Length getWidth() {
		GeoCoordinate northWest = new GeoCoordinate.Builder().setLatitude(getNorth()).setLongitude(getWest()).build();
		GeoCoordinate southEast = new GeoCoordinate.Builder().setLatitude(getSouth()).setLongitude(getEast()).build();
		Length topDistance = GeoMath.length(getSouthWest(), southEast);
		Length bottomDistance = GeoMath.length(getNorthEast(), northWest);
		Length averageDistance = QuantityMath.create(topDistance).average(bottomDistance).getQuantity();
		return averageDistance;
	}

	/**
	 * The length from the min to the max points.
	 * 
	 * @return
	 */
	public Length getDiagonal() {
		return GeoMath.length(getNorthEast(), getSouthWest());
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object that) {

		boolean result = false;
		if (that instanceof BoundingBox) {
			BoundingBox dat = (BoundingBox) that;
			result = this.northEast.equals(dat.northEast) && this.southWest.equals(dat.southWest);
		}
		return result;
	}

	public BoundingBox combine(BoundingBox that) {
		return BoundingBoxUtil.combine(this, that);
	}

	/**
	 * Returns true if this Box contains the other box completely (inclusively). So equal boxes will
	 * contain each other.
	 * 
	 * @param box
	 */
	public Boolean contains(BoundingBox that) {
		return BoundingBoxUtil.contains(this, that);

	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.northEast.hashCode() + this.southWest.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getBounds();
	}

	/**
	 * constructs given a string as produced by {@link #getBounds()}.
	 * 
	 * @see #getBounds()
	 * 
	 * @param bboxString
	 * @return
	 * @throws InvalidBoundsException
	 */
	public static BoundingBox valueOf(String bboxString) throws InvalidBoundsException {
		return new Builder().setBounds(bboxString).build();
	}

	public static class Builder
			extends BaseBuilder<BoundingBox, Builder> {
		public Builder() {
			super(new BoundingBox());
		}

	}

	/**
	 * Used to construct all instances of BoundingBox.
	 */
	public static class BaseBuilder<BB extends BoundingBox, B extends BaseBuilder<BB, B>>
			extends AbstractObjectBuilder<BB, B> {
		private Latitude south;
		private Longitude east;
		private Latitude north;
		private Longitude west;

		protected BaseBuilder(BB boundingBox) {
			super(boundingBox);
		}

		/**
		 * If you want to create abounding box from another bounding box.
		 * 
		 * @param createRandomBoundingBox
		 * @return
		 */
		public B setOther(BoundingBox boundingBox) {
			setNorthEast(boundingBox.getNorthEast());
			setSouthWest(boundingBox.getSouthWest());
			return dis;
		}

		public B setDefault() {
			// TODO:Consider a better default.
			setNorth(Latitude.MAX);
			setSouth(Latitude.MIN);
			setEast(Longitude.MAX);
			setWest(Longitude.MIN);
			return dis;
		}

		public B setSouth(Latitude south) {
			this.south = south;
			return dis;
		}

		/**
		 * @param east
		 *            the east to set
		 */
		public B setEast(Longitude east) {
			this.east = east;
			return dis;
		}

		/**
		 * @param north
		 *            the north to set
		 */
		public B setNorth(Latitude north) {
			this.north = north;
			return dis;
		}

		/**
		 * @param west
		 *            the west to set
		 */
		public B setWest(Longitude west) {
			this.west = west;
			return dis;
		}

		public B setSouth(Double inDecimalDegrees) {
			return setSouth(MeasurementUtil.createLatitude(inDecimalDegrees));
		}

		public B setWest(Double inDecimalDegrees) {
			return setWest(MeasurementUtil.createLongitude(inDecimalDegrees));
		}

		public B setNorth(Double inDecimalDegrees) {
			return setNorth(MeasurementUtil.createLatitude(inDecimalDegrees));
		}

		public B setEast(Double inDecimalDegrees) {
			return setEast(MeasurementUtil.createLongitude(inDecimalDegrees));
		}

		/**
		 * The reciprocal of {@link BoundingBox#getBounds()}
		 * 
		 * @param bboxString
		 * @return
		 * @throws InvalidBoundsException
		 */
		public B setBounds(String bboxString) throws InvalidBoundsException {
			String[] coordStrings = StringUtils.split(bboxString, STRING_POINT_DILIMETER);
			if (coordStrings.length < 2) {
				throw new InvalidBoundsException(bboxString);
			}
			setSouthWest(GeoCoordinate.valueOf(coordStrings[STRING_SW_INDEX]));
			setNorthEast(GeoCoordinate.valueOf(coordStrings[STRING_NE_INDEX]));
			return dis;
		}

		public B setSouthWest(GeoCoordinate southWest) {
			((BoundingBox) building).southWest = southWest;
			return dis;
		}

		public B setNorthEast(GeoCoordinate northEast) {
			((BoundingBox) building).northEast = northEast;
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public BB build() {
			prepareToBuild();
			return super.build();
		}

		/**
		 * Ensures the bounding box has all the necessary components.
		 * 
		 */
		protected void prepareToBuild() {
			if (((BoundingBox) building).northEast == null || ((BoundingBox) building).southWest == null) {
				Assertion.assertNotNull("north", this.north);
				Assertion.assertNotNull("south", this.south);
				Assertion.assertNotNull("east", this.east);
				Assertion.assertNotNull("west", this.west);
				((BoundingBox) building).northEast = new GeoCoordinate.Builder().setLatitude(this.north)
						.setLongitude(this.east).build();
				((BoundingBox) building).southWest = new GeoCoordinate.Builder().setLatitude(this.south)
						.setLongitude(this.west).build();
			}
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Let's you know if {@link #valueOf(String)} is likely to produce a bounding box.
	 * 
	 * @param boundingBoxAsString
	 */
	public static Boolean isValidBounds(String boundingBoxAsString) {
		// TODO:consider a regular expression to make this more accurate.
		return boundingBoxAsString.contains(STRING_POINT_DILIMETER);

	}

	/**
	 * Provides the four corners in a collection in no specified order.
	 * 
	 * @return
	 */
	public Collection<GeoCoordinate> getExtremes() {
		return Lists.newArrayList(getNorthEast(), getNorthWest(), getSouthEast(), getSouthWest());
	}

	public static class FIELD {
		public static final String NAME = "boundingBox";
		public static String BOUNDS = "bounds";
	}
}
