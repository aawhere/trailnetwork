/**
 *
 */
package com.aawhere.measure;

import javax.measure.unit.Unit;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;

import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.measure.geocell.GeoCellUtil;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.xml.XmlNamespace;
import com.aawhere.xml.bind.OptionalXmlAdapter;

/**
 * Used by JAXB Marshalling to transform BoundingBox into a string using BoundingBox#getAsString.
 * 
 * @author Brian Chapman
 * 
 */
public class BoundingBoxXmlAdapter
		extends OptionalXmlAdapter<BoundingBoxXmlAdapter.BoundingBoxXml, BoundingBox> {

	public BoundingBoxXmlAdapter() {
		// supports legacy behavior.
		super(Visibility.SHOW);
	}

	/**
	 * @param show
	 */
	public BoundingBoxXmlAdapter(Boolean show) {
		super(show);
	}

	/**
	 * @param hide
	 */
	public BoundingBoxXmlAdapter(com.aawhere.xml.bind.OptionalXmlAdapter.Hide hide) {
		super(hide);
	}

	/**
	 * @param show
	 */
	public BoundingBoxXmlAdapter(com.aawhere.xml.bind.OptionalXmlAdapter.Show show) {
		super(show);
	}

	/**
	 * @param visibility
	 */
	public BoundingBoxXmlAdapter(com.aawhere.xml.bind.OptionalXmlAdapter.Visibility visibility) {
		super(visibility);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public BoundingBox showUnmarshal(BoundingBoxXml formatted) throws Exception {
		BoundingBox result;
		// Note: nulls may be acceptable in some classes (aka filters)
		if (formatted == null) {
			return null;
		} else {
		}
		if (!StringUtils.isEmpty(formatted.bounds)) {
			result = GeoCellUtil.boundingBox(formatted.bounds);
		} else {
			result = new GeoCellBoundingBox.Builder()
					.setNorthEast(GeoCoordinate.create().setLatitude(formatted.north).setLongitude(formatted.east)
							.build())
					.setSouthWest(GeoCoordinate.create().setLatitude(formatted.south).setLongitude(formatted.west)
							.build()).build();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public BoundingBoxXml showMarshal(BoundingBox box) throws Exception {
		// Note: nulls may be acceptable in some classes (aka filters)
		if (box == null) {
			return null;
		} else {
			Unit<Latitude> latUnit = ExtraUnits.DECIMAL_DEGREES_LATITUDE;
			Unit<Longitude> lonUnit = ExtraUnits.DECIMAL_DEGREES_LONGITUDE;
			BoundingBoxXml xml = new BoundingBoxXml();
			xml.bounds = box.getBounds();
			xml.north = box.getNorthEast().getLatitude().doubleValue(latUnit);
			xml.south = box.getSouthWest().getLatitude().doubleValue(latUnit);
			xml.east = box.getNorthEast().getLongitude().doubleValue(lonUnit);
			xml.west = box.getSouthWest().getLongitude().doubleValue(lonUnit);
			xml.center = new BoundingBoxCenterXml();
			GeoCoordinate center = box.getCenter();
			xml.center.latitude = center.getLatitude().doubleValue(latUnit);
			xml.center.longitude = center.getLongitude().doubleValue(lonUnit);
			return xml;
		}
	}

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE, name = "BoundingBox")
	public static class BoundingBoxXml {
		@XmlAttribute
		private String bounds;
		@XmlAttribute
		private Double north;
		@XmlAttribute
		private Double east;
		@XmlAttribute
		private Double south;
		@XmlAttribute
		private Double west;
		@XmlElement
		private BoundingBoxCenterXml center;

	}

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE, name = "center")
	public static class BoundingBoxCenterXml {
		@XmlAttribute
		private Double latitude;
		@XmlAttribute
		private Double longitude;
	}

}
