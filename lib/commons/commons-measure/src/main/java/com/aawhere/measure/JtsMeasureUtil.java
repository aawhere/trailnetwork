/**
 *
 */
package com.aawhere.measure;

import javax.annotation.Nullable;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.collections.Index;
import com.aawhere.jts.geom.JtsGeometryFactory;
import com.aawhere.lang.Assertion;
import com.aawhere.math.GeomUtil;
import com.aawhere.measure.calc.AngleMath;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.QuantityMath;

import com.bbn.openmap.proj.Planet;
import com.google.common.base.Function;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Utility for converting between Measurements and {@link Geometry}.
 * 
 * @author Brian Chapman
 * 
 * 
 */
public class JtsMeasureUtil {

	// TODO:consider making the factory a singleton
	// NOTE: this factory is setup for GeoCoordinate
	// public static final GeometryFactory factory = new GeometryFactory();
	private static final Unit<Angle> ARC_LENGTH_UNIT = USCustomarySystem.DEGREE_ANGLE;

	private static final GeometryFactory factory = JtsGeometryFactory.getDefaultfactory();

	/**
	 * This helper method translates from length to approximate decimal degrees by considering what
	 * Latitude. Useful for approximating a buffer when determining intersection lengths.
	 * 
	 * Note that this method is *not* exact and was introduced in lieu of doing a full re-projection
	 * for WGS84 coordinates to local cartesian2D coordinates. There will be an error in that the
	 * latitude and longitude decimal degrees differ in representations when converted to meters.
	 * This error is expected to be on the order of 2x for Latitudes up to 60 degrees.
	 * 
	 * 
	 * 
	 * @deprecated use {@link GeoMath#arcLength(Length, GeoCoordinate)}
	 * 
	 * @param buffernLength
	 * @return
	 */
	@Deprecated
	public static Angle angleFromLength(Length bufferLength, Latitude latitude) {
		// If your displacements aren't too great (less than a few kilometers) and you're not right
		// at the poles, use the quick and dirty estimate that 111,111 meters in the y direction is
		// 1 degree (of latitude) and 111,111 * cos(latitude) meters in the x direction is 1 degree
		// (of longitude).

		// Position, decimal degrees
		double lat = latitude.getInDecimalDegrees();

		// Earth’s circumference, sphere
		double circumferenceOfEarth = Planet.wgs84_earthEquatorialCircumferenceMeters;
		double radiusOfEarth = GeomUtil.circumferenceToRadius(circumferenceOfEarth);

		// offsets in meters
		double offset = bufferLength.doubleValue(MetricSystem.METRE);

		// Coordinate offsets in radians
		double radLat = offset / radiusOfEarth;
		double radLon = offset / (radiusOfEarth * Math.cos(Math.toRadians(lat)));

		// OffsetPosition, decimal degrees
		double degLat = Math.toDegrees(radLat);
		double degLon = Math.toDegrees(radLon);
		double bufferLengthInDecimalDegrees = (degLat + degLon) / 2;
		return MeasurementUtil.createAngleInRadians(Math.toRadians(bufferLengthInDecimalDegrees));

	}

	/**
	 * Given a {@link GeoCoordinate} this will create {@link Coordinate} with x and y in decimal
	 * degrees.
	 * 
	 * @see JtsMeasureUtil#convert(GeoCoordinate)
	 * @param coordinate
	 * @return
	 */
	public static Coordinate convert(GeoCoordinate coordinate) {
		double x = factory.getPrecisionModel().makePrecise(coordinate.getLongitude().getInDecimalDegrees());
		double y = factory.getPrecisionModel().makePrecise(coordinate.getLatitude().getInDecimalDegrees());
		return new Coordinate(x, y);
	}

	/**
	 * Given a Coordinate created from the same standards as {@link #convert(GeoCoordinate)} this
	 * will return the {@link GeoCoordinate}.
	 * 
	 * the x and y must be in decimal degrees.
	 * 
	 * @see #convert(GeoCoordinate)
	 * 
	 * @param coordinate
	 * @return
	 */
	public static GeoCoordinate convert(Coordinate coordinate) {
		return MeasurementUtil.createGeoCoordinate(coordinate.y, coordinate.x);
	}

	/**
	 * Useful when converting from a JTS {@link Coordinate} to a {@link GeoCoordinate}.
	 * 
	 * @return
	 */
	public static Function<Coordinate, GeoCoordinate> geoCoordinateFunction() {
		return new Function<Coordinate, GeoCoordinate>() {

			@Override
			public GeoCoordinate apply(Coordinate input) {
				return (input != null) ? convert(input) : null;
			}
		};
	}

	/**
	 * @see #convert(Coordinate)
	 * @param coordinates
	 * @return
	 */
	public static GeoCoordinate[] convert(Coordinate... coordinates) {
		GeoCoordinate[] result = new GeoCoordinate[coordinates.length];
		for (int i = 0; i < coordinates.length; i++) {
			Coordinate coordinate = coordinates[i];
			result[i] = convert(coordinate);
		}
		return result;
	}

	/**
	 * @see #convert(GeoCoordinate)
	 * @param geoCoordinates
	 * @return
	 */
	public static Coordinate[] convert(GeoCoordinate... geoCoordinates) {
		Coordinate[] coordinates = new Coordinate[geoCoordinates.length];
		for (int i = 0; i < geoCoordinates.length; i++) {
			GeoCoordinate geoCoordinate = geoCoordinates[i];
			coordinates[i] = convert(geoCoordinate);
		}
		return coordinates;
	}

	/**
	 * Convenience for the common scenario of creating a LineString given only the endpoints.
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static LineString line(GeoCoordinate first, GeoCoordinate second) {

		return factory.createLineString(convert(first, second));
	}

	/**
	 * When creating a simpleLine given the two points, but the desired length is to be multiplied
	 * by the given lengthFactor. Greater than 1 and the line gets longer, less than one and the
	 * line gets shorter.
	 * 
	 * This is useful in a similar manner that buffer is useful in that a buffer provides some extra
	 * geometry around the line, but this remains a line which can provide an advantage in
	 * simplicity and performance.
	 * 
	 * WARNING: This will, however, scale the factor relative to the length between the coords.
	 * 
	 * 
	 * @see Geometry#buffer(double)
	 * 
	 * @param first
	 * @param second
	 * @param lengthFactor
	 * @return
	 */
	public static LineString line(GeoCoordinate first, GeoCoordinate second, Double lengthFactor) {
		Angle heading = GeoMath.heading(first, second);
		Length length = GeoMath.length(first, second);
		length = QuantityMath.create(length).multipliedBy(lengthFactor).getQuantity();
		GeoCoordinate midpoint = GeoMath.midpoint(first, second);
		return line(midpoint, heading, length);

	}

	/**
	 * Builds a line equidistant around the midpoint both in the direction of the given heading and
	 * opposite the heading.
	 * 
	 * @see #perpendicularLine(GeoCoordinate, Angle, Length)
	 * 
	 * @param midpoint
	 * @param heading
	 * @param lineLength
	 * @return
	 */
	public static LineString line(GeoCoordinate midpoint, Angle heading, Length lineLength) {
		Angle oppositeHeading = AngleMath.create(heading).reverse().getQuantity();
		Length halfLength = QuantityMath.create(lineLength).dividedBy(2).getQuantity();
		GeoCoordinate left = GeoMath.coordinateFrom(midpoint, heading, halfLength);
		GeoCoordinate right = GeoMath.coordinateFrom(midpoint, oppositeHeading, halfLength);
		Coordinate first = convert(left);
		Coordinate second = convert(right);
		return factory.createLineString(ArrayUtils.toArray(first, second));
	}

	/**
	 * Given any {@link Geometry} created with {@link GeoCoordinate} this will use it's
	 * {@link Geometry#getEnvelope()} to convert into a {@link BoundingBox}.
	 * 
	 * @param geometry
	 * @return the bounding box from the envelope of null if the geometry is null or empty.
	 */
	@Nullable
	public static BoundingBox boundingBox(@Nullable Geometry geometry) {
		if (geometry == null || geometry.isEmpty()) {
			return null;
		} else {

			Geometry envelope = geometry.getEnvelope();

			BoundingBox.Builder builder = BoundingBox.create();
			if (envelope instanceof Polygon) {
				// <code>Polygon</code> whose vertices are
				// (minx miny, maxx miny,maxx maxy, minx maxy, minx miny).
				Assertion.exceptions().assertTrue("must be a rectangle for this work", envelope.isRectangle());
				Coordinate[] coordinates = envelope.getCoordinates();
				GeoCoordinate min = convert(coordinates[Index.FIRST.index]);
				builder.setSouthWest(min);
				GeoCoordinate max = convert(coordinates[Index.THIRD.index]);
				builder.setNorthEast(max);

			} else if (envelope instanceof LineString) {
				Coordinate[] coords = envelope.getCoordinates();
				builder.setSouthWest(convert(coords[Index.FIRST.index]));
				builder.setNorthEast(convert(coords[Index.SECOND.index]));
			} else if (envelope instanceof Point) {

				GeoCoordinate geoCoordinate = convert((Point) envelope);
				builder.setNorthEast(geoCoordinate);
				builder.setSouthWest(geoCoordinate);
			} else {
				throw new IllegalArgumentException(envelope.getClass() + " is not a supported envelope type");
			}

			return builder.build();
		}
	}

	/**
	 * A single coordinate point is easily converted to it's matching {@link GeoCoordinate}.
	 * 
	 * @param point
	 * @return
	 */
	private static GeoCoordinate convert(Point point) {
		return convert(point.getCoordinate());
	}

	/**
	 * Builds a line perpendicular to the heading for the given length around the given midpoint.
	 * 
	 * <pre>
	 *  l-----m-----r
	 *  
	 *  l = left
	 *  r = right
	 *  m = midpoint
	 *  length is from l to r
	 * 
	 * </pre>
	 * 
	 * @param midpoint
	 * @param heading
	 * @param lineLength
	 * @return
	 */
	public static LineString perpendicularLine(GeoCoordinate midpoint, Angle heading, Length lineLength) {
		// from the midpoint find a right angle heading and build the line around that. Left or
		// right is indifferent
		Angle perpendicularLeft = MeasurementUtil.perpendicularLeft(heading);
		return line(midpoint, perpendicularLeft, lineLength);
	}

	/**
	 * Provides a JTS version {@link BoundingBox} to be used for geometric checks.
	 * 
	 * @param midpoint
	 * @param width
	 * @return
	 */
	public static Envelope envelope(GeoCoordinate midpoint, Length width) {
		LineString horizontal = perpendicularLine(midpoint, MeasurementUtil.NORTH, width);
		LineString vertical = perpendicularLine(midpoint, MeasurementUtil.EAST, width);
		MultiLineString multiLineString = factory.createMultiLineString(new LineString[] { horizontal, vertical });
		return multiLineString.getEnvelopeInternal();
	}

	/**
	 * Simply calls {@link Envelope#contains(Coordinate)} with using {@link #convert(GeoCoordinate)}
	 * to get the jts point.
	 * 
	 * @param point
	 * @param envelope
	 * @return
	 */
	public static Boolean contains(GeoCoordinate point, Envelope envelope) {
		return envelope.contains(convert(point));
	}

	/**
	 * Provides a {@link com.vividsolutions.jts.geom.Point} when given a {@link GeoCoordinate}.
	 * 
	 * @param earthOrigin
	 * @return
	 */
	public static Point point(GeoCoordinate geoCoordinate) {
		return factory.createPoint(convert(geoCoordinate));
	}

	/**
	 * Given the {@link GeoCoordinate} this will return a {@link Polygon} which is essentially a
	 * circle representing the point and the radius given.
	 * 
	 * @param location
	 * @return
	 */
	public static Geometry buffer(GeoCoordinate location, Length radius) {
		Point point = point(location);
		return point.buffer(arcLength(radius, location));
	}

	/**
	 * Given a {@link Length} and a location on earth this will calculate the arcLength using
	 * {@link GeoMath#arcLength(Length, GeoCoordinate)} returning the value in coordinates that
	 * match the standard JTS/Trackpoint conversions.
	 * 
	 * @see #ARC_LENGTH_UNIT
	 * 
	 * 
	 * @param length
	 * @param coordinate
	 * @return
	 */
	public static double arcLength(Length length, GeoCoordinate coordinate) {
		return GeoMath.arcLength(length, coordinate).doubleValue(ARC_LENGTH_UNIT);
	}

	/**
	 * Provides the double that JTS uses that matches our unit used in the JTS Geometry:
	 * {@link #ARC_LENGTH_UNIT}.
	 * 
	 * 
	 * @param arcLength
	 * @return
	 */
	public static double arcLength(Angle arcLength) {
		return arcLength.doubleValue(ARC_LENGTH_UNIT);
	}

	/**
	 * Assumes the given coordinate was created using the {@link #ARC_LENGTH_UNIT}.
	 * 
	 * 
	 * @see #arcLength(Length, Coordinate)
	 * @param minDistanceBetweenTrackpoints
	 * @param coordinate
	 * @return
	 */
	public static double arcLength(Length minDistanceBetweenTrackpoints, Coordinate coordinate) {
		return arcLength(minDistanceBetweenTrackpoints, JtsMeasureUtil.convert(coordinate));
	}

}
