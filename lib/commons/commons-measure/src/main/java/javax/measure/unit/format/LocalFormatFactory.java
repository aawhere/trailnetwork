/**
 * 
 */
package javax.measure.unit.format;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.measure.unit.UnitFormat;

/**
 * The {@link LocalFormat} doesn't like to share it's availability to
 * {@link LocalFormat#getInstance(SymbolMapImpl)}, but we need it to extend the units to support
 * ours. This is here to make the package accessible method available to our system.
 * 
 * @author aroller
 * 
 */
public class LocalFormatFactory {

	/**
	 * 
	 */
	public LocalFormatFactory() {
	}

	/**
	 * Given the resource bundle that is to extend the the symbols this will load all of those into
	 * the SymbolMap from {@link LocalFormat#getInstance(Locale)}.
	 * 
	 * The locale will be retrieved from the provided {@link ResourceBundle}.
	 * 
	 * @param rb
	 * @return
	 */
	public static UnitFormat getInstance(ResourceBundle rb) {

		LocalFormat format = LocalFormat.getInstance(rb.getLocale());
		return LocalFormat.getInstance(new CombinedSymbolMap(rb, format.getSymbolMap()).getMaster());
	}

}
