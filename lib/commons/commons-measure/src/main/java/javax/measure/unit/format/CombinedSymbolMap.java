/**
 * 
 */
package javax.measure.unit.format;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javax.measure.unit.Unit;
import javax.measure.unit.UnitConverter;
import javax.measure.unit.UnitFormat;
import javax.measure.unit.UnitFormat.SymbolMap;

/**
 * Provides the ability to extend a unit format by combing multiple {@link SymbolMap}s by delegating
 * all functionality from the "super" to the "master" that is provided during construction. This
 * approach is required since the parsing is done during construction in {@link SymbolMapImpl}.
 * 
 * This is located in the javax package because of the lack of access to extend the functionality of
 * {@link LocalFormat}.
 * 
 * Simply construct providing the master, then call {@link #getMaster()} after construction.
 * 
 * The correct way to extend would be to make our own {@link UnitFormat} and {@link SymbolMap}, but
 * that seems like too much of a waste to re-implement so we have inserted ourselves into their
 * package to get package access to protected methods.
 * 
 * @author aroller
 * 
 */
class CombinedSymbolMap
		extends SymbolMapImpl {

	private SymbolMapImpl master;
	private HashMap<Unit<?>, String> labels;
	private HashMap<Unit<?>, String> aliases;
	private HashMap<ParsePrefix, String> moreLabels;

	/**
	 * @param rb
	 */
	public CombinedSymbolMap(ResourceBundle rb, SymbolMapImpl master) {
		super(rb);
		// master isn't available during parsing during construction.
		this.master = master;
		if (labels != null) {
			for (Entry<Unit<?>, String> label : labels.entrySet()) {
				master.label(label.getKey(), label.getValue());
			}
		}
		if (aliases != null) {
			for (Entry<Unit<?>, String> alias : aliases.entrySet()) {
				master.alias(alias.getKey(), alias.getValue());
			}
		}
		if (moreLabels != null) {
			for (Entry<ParsePrefix, String> label : moreLabels.entrySet()) {
				master.label(label.getKey(), label.getValue());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see javax.measure.unit.UnitFormat.SymbolMap#label(javax.measure.unit.Unit, java.lang.String)
	 */
	@Override
	public void label(Unit<?> unit, String symbol) {
		if (this.labels == null) {
			this.labels = new HashMap<Unit<?>, String>();
		}
		labels.put(unit, symbol);

	}

	/*
	 * (non-Javadoc)
	 * @see javax.measure.unit.UnitFormat.SymbolMap#alias(javax.measure.unit.Unit, java.lang.String)
	 */
	@Override
	public void alias(Unit<?> unit, String symbol) {
		if (this.aliases == null) {
			this.aliases = new HashMap<Unit<?>, String>();
		}
		aliases.put(unit, symbol);

	}

	/*
	 * (non-Javadoc)
	 * @see javax.measure.unit.format.SymbolMapImpl#label(javax.measure.unit.format .ParsePrefix,
	 * java.lang.String)
	 */
	@Override
	void label(ParsePrefix prefix, String symbol) {
		if (this.moreLabels == null) {
			this.moreLabels = new HashMap<ParsePrefix, String>();
		}
		moreLabels.put(prefix, symbol);
	}

	/**
	 * @return the master
	 */
	public SymbolMapImpl getMaster() {
		return this.master;
	}
}
