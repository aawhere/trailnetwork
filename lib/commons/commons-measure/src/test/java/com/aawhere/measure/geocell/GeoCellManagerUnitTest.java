/**
 *
 */
package com.aawhere.measure.geocell;

import java.util.List;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.beoui.geocell.GeocellManager;
import com.beoui.geocell.annotations.Geocells;
import com.beoui.geocell.annotations.Latitude;
import com.beoui.geocell.annotations.Longitude;
import com.beoui.geocell.model.CostFunction;
import com.beoui.geocell.model.Point;

/**
 * Sandbox to Test/Learn GeoCell library (or play around with it rather).
 * 
 * @author brian
 * 
 */
// We don't need to be running this for every build!
@Ignore
// SandboxTest
public class GeoCellManagerUnitTest {

	@Test
	public void testGeoCellPoint() {
		double lat = 0;
		double lon = 0;

		Point p = new Point(lat, lon);

		List<String> cells = GeocellManager.generateGeoCell(p);

		GeoCellManagerUnitTest.ObjectToSave obj = new GeoCellManagerUnitTest.ObjectToSave();
		obj.setLatitude(lat);
		obj.setLongitude(lon);
		obj.setGeocells(cells);

		Assert.assertTrue(cells.size() > 0);
	}

	@Test
	public void testGeoCellBb() {
		double latN = 0.1;
		double lonE = 0.1;

		double latS = 0;
		double lonW = 0;

		com.beoui.geocell.model.BoundingBox bb = new com.beoui.geocell.model.BoundingBox(latN, lonE, latS, lonW);

		List<String> cells = GeocellManager.bestBboxSearchCells(bb, null);

		Assert.assertTrue(cells.size() > 0);
	}

	@Test
	public void testGeoCellBbWithCostFunction() {
		double latN = 0.1;
		double lonE = 0.1;

		double latS = 0;
		double lonW = 0;

		com.beoui.geocell.model.BoundingBox bb = new com.beoui.geocell.model.BoundingBox(latN, lonE, latS, lonW);

		List<String> cells = GeocellManager.bestBboxSearchCells(bb, new CostFunction() {
			public double defaultCostFunction(int numCells, int resolution) {
				return (resolution > 8) ? Double.MAX_VALUE : 0;
				// return Double.MAX_VALUE;
			}
		});

		Assert.assertTrue(cells.size() > 0);
	}

	@PersistenceCapable
	public class ObjectToSave {

		@PrimaryKey
		@Persistent
		private long id;

		@Persistent
		@Latitude
		private double latitude;

		@Persistent
		@Longitude
		private double longitude;

		@Persistent
		@Geocells
		private List<String> geocells;

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public double getLatitude() {
			return latitude;
		}

		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

		public List<String> getGeocells() {
			return geocells;
		}

		public void setGeocells(List<String> geocells) {
			this.geocells = geocells;
		}
	}
}
