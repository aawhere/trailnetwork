/**
 * 
 */
package com.aawhere.measure.calc;

import static org.junit.Assert.assertEquals;

import javax.measure.quantity.Length;
import javax.measure.unit.Dimension;

import org.junit.Test;

import com.aawhere.math.PlaceValueNotation;
import com.aawhere.measure.MeasurementUtil;

/**
 * @see QuantityPrecision
 * 
 * @author Aaron Roller
 * 
 */
public class QuantityPrecisionUnitTest {

	PlaceValueNotation WINNING_OVERRIDE = PlaceValueNotation.HUNDREDTH;
	PlaceValueNotation LOSING_OVERRIDE = PlaceValueNotation.THOUSANDTH;
	Length length = MeasurementUtil.createLengthInMeters(12345.67890);

	@Test
	public void testDefault() {
		assertPrecision(new QuantityPrecision.Builder().build(), QuantityPrecision.DEFAULT_PRECISION_FOR_BASE_UNIT);
	}

	@Test
	public void testQuantityOverridingDefault() {

		assertPrecision(new QuantityPrecision.Builder().setPrecision(Length.class, WINNING_OVERRIDE).build(),
						WINNING_OVERRIDE);

	}

	@Test
	public void testDimensionOverridingDefault() {

		assertPrecision(new QuantityPrecision.Builder().setPrecision(Dimension.LENGTH, WINNING_OVERRIDE).build(),
						WINNING_OVERRIDE);
	}

	@Test
	public void testQuantityOverridingDimension() {
		assertPrecision(new QuantityPrecision.Builder().setPrecision(Dimension.LENGTH, LOSING_OVERRIDE)
								.setPrecision(Length.class, WINNING_OVERRIDE).build(),
						WINNING_OVERRIDE);

	}

	void assertPrecision(QuantityPrecision precision, PlaceValueNotation expected) {

		PlaceValueNotation place = precision.precisionPlaceFor(length);
		assertEquals(expected, place);

	}

	@Test
	public void testPrecisionValue() {
		QuantityPrecision precision = new QuantityPrecision.Builder().build();
		Length seed = MeasurementUtil.createLengthInMeters(12345.667890);
		Length precisionLength = precision.precisionFor(seed);
		Double expected = QuantityPrecision.DEFAULT_PRECISION_FOR_BASE_UNIT.getNumericRepresentation();
		assertEquals(expected, precisionLength.getValue());
		assertEquals(expected, precision.precisionValueForQuantity(seed));
	}
}
