/**
 *
 */
package com.aawhere.measure;

import static org.junit.Assert.*;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import org.junit.Test;

import com.aawhere.field.type.FieldData;
import com.aawhere.lang.exception.BaseException;

/**
 * @author brian Chapman
 * 
 */
public class QuantityDataTypeNumberAdapterUnitTest {

	@Test
	public void testMarshall() throws BaseException {
		Length length = MeasureTestUtil.createRandomLength();
		QuantityDataTypeNumberAdapter adapter = new QuantityDataTypeNumberAdapter();
		FieldData marshalled = adapter.marshal(length);
		assertNotNull(marshalled);
		assertEquals(length.doubleValue(MetricSystem.METRE), marshalled.getValue());

		Length unmarshalled = adapter.unmarshal(marshalled, Length.class);
		assertEquals(length, unmarshalled);
	}
}
