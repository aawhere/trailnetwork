/**
 * 
 */
package com.aawhere.measure;

import java.util.Locale;

import javax.measure.quantity.Length;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageFormatter;

/**
 * Tests the inclusion of quantities in standard messages.
 * 
 * @author aroller
 * 
 */
public class QuantityMessageUnitTest {

	@Test
	public void testLength() {

		final int value = 12345;
		Length length = MeasurementUtil.createLengthInMeters(value);
		CompleteMessage completeMessage = CompleteMessage.create(MeasureMessage.INVALID_NUMBER_FORMAT)
				.param(MeasureMessage.Param.NUMBER, length).build();
		String formattedMessage = MessageFormatter.create().message(completeMessage).locale(Locale.US).build()
				.getFormattedMessage();
		TestUtil.assertContains(formattedMessage, "12,345");
	}
}
