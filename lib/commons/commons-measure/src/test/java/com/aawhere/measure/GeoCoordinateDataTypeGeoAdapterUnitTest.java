/**
 *
 */
package com.aawhere.measure;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.adapter.Adapter;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.exception.BaseException;

/**
 * @author Brian Chapman
 * 
 */
public class GeoCoordinateDataTypeGeoAdapterUnitTest {

	private GeoCoordinate sample = new GeoCoordinate.Builder().setAsString("1,2").build();
	private GeoCoordinateDataTypeGeoAdapter adapter;

	@Before
	public void setUp() {
		adapter = new GeoCoordinateDataTypeGeoAdapter();
	}

	@Test
	public void testMarshall() throws BaseException {
		assertAdapter(sample);
	}

	/** Ensures a single registration will satisfy providing adapters of multiple subtypes. */
	@Test
	public void testQuantityRegistration() {
		DataTypeAdapterFactory factory = new DataTypeAdapterFactory();
		GeoCoordinateDataTypeGeoAdapter geoCoordinateAdapter = new GeoCoordinateDataTypeGeoAdapter();
		factory.register(geoCoordinateAdapter, FieldDataType.GEO);
		Adapter<Object, FieldData> foundAdapter = factory.getAdapter(GeoCoordinate.class, FieldDataType.GEO);
		assertSame(geoCoordinateAdapter, foundAdapter);
	}

	/**
	 * @param expected
	 * @return
	 * @throws BaseException
	 */
	private GeoCoordinate assertAdapter(GeoCoordinate sample) throws BaseException {
		FieldData result = adapter.marshal(sample);
		assertNotNull(result);
		GeoCoordinate actual = adapter.unmarshal(result, GeoCoordinate.class);
		assertEquals(sample, actual);
		return actual;
	}
}
