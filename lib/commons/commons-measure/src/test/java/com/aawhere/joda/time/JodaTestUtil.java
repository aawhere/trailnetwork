/**
 * 
 */
package com.aawhere.joda.time;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.Interval;

import com.aawhere.test.TestUtil;

import com.google.common.collect.Range;

/**
 * Useful functions for the testing of Joda library dependencies.
 * 
 * @author Aaron Roller
 * 
 */
public class JodaTestUtil {

	static final DateTime zero = new DateTime(0);
	static final DateTime one = new DateTime(1);
	static final DateTime two = new DateTime(2);
	static final DateTime three = new DateTime(3);
	static final Interval zeroToOne = new Interval(zero, one);
	static final Interval zeroToTwo = new Interval(zero, two);
	static final Interval oneToTwo = new Interval(one, two);
	static final Interval oneToThree = new Interval(one, three);
	static final Interval twoToThree = new Interval(two, three);
	static final Interval zeroToThree = new Interval(zero, three);

	/** Useful for testing that EPOCH_TIMESTAMP_ZULU was formatted correctly. */
	public static final String EPOCH_ZULU_1_MILLIS_STRING = "1970-01-01T00:00:00.001Z";
	public static final DateTime EPOCH_ZULU_1_MILLIS_TIMESTAMP = new DateTime(1, DateTimeZone.UTC);
	public static final String EPOCH_ZULU_STRING = "1970-01-01T00:00:00Z";
	public static final DateTime EPOCH_ZULU_TIMESTAMP = new DateTime(0, DateTimeZone.UTC);

	public DateTime generateNow() {
		return DateTime.now();
	}

	/**
	 * @deprecated use {@link #timestamp()}
	 * @return
	 */
	@Deprecated
	public static DateTime createRandomTimestamp() {

		return new DateTime(TestUtil.generateRandomLong(Integer.MAX_VALUE));
	}

	/**
	 * like {@link #createRandomTimestamp()}, but with a better name and bigger range.
	 * 
	 * @return
	 */
	public static DateTime timestamp() {

		final long thirtyYears = 30l * 365l * 24l * 60l * 60l * 1000l;
		return new DateTime(TestUtil.generateRandomLong(thirtyYears));
	}

	/**
	 * Given a stirng this will attempt to parse or fail otherwise.
	 * 
	 * @param string
	 * @param begin
	 */
	public static void assertParseableXmlDate(String message, String formattedTimestamp) {
		DateTime result = JodaTimeUtil.parseXmlDate(formattedTimestamp);
		assertNotNull(message, result);
	}

	/**
	 * COmpares the expected to the actual using
	 * {@link JodaTimeUtil#equalsWithinTolerance(DateTime, DateTime)}.
	 * 
	 * @param message
	 * @param expected
	 * @param actual
	 */
	public static void assertEquals(String message, DateTime expected, DateTime actual) {
		assertTrue(	message + " expecting " + expected + ", but got " + actual,
					JodaTimeUtil.equalsWithinTolerance(expected, actual));
	}

	/**
	 * creates a random interval.
	 * 
	 * @return
	 */
	public static Interval interval() {
		return new Interval(timestamp(), duration(1, 10 * 60 * 1000));
	}

	public static Duration duration() {
		return new Duration((long) TestUtil.i());
	}

	/**
	 * @return
	 */
	public static Duration duration(int min, int max) {
		return new Duration(TestUtil.generateRandomInt(min, max).longValue());
	}

	/**
	 * @param timestamp
	 * @param timestamp2
	 */
	public static void assertBefore(DateTime expectedBefore, DateTime expectedAfter) {
		assertTrue(expectedBefore + " not before " + expectedAfter, expectedBefore.isBefore(expectedAfter));
	}

	/**
	 * @param expanded
	 * @return
	 */
	public static IntervalAssertion assertInterval(Interval target) {
		return new IntervalAssertion(target);
	}

	public static class IntervalAssertion {
		private Interval target;

		/**
		 * 
		 */
		public IntervalAssertion(Interval target) {
			this.target = target;
		}

		public IntervalAssertion contains(Interval toBeContained) {
			assertTrue(target + " does not contain " + toBeContained, target.contains(toBeContained));
			return this;
		}

		/**
		 * @param interval
		 */
		public void overlaps(Interval interval) {
			assertTrue(target + " does not overlap " + interval, target.overlaps(interval));
		}
	}

	/**
	 * asserts teh range with a good message.
	 * 
	 * @param range
	 * @param dateTime
	 */
	public static void assertRange(Range<DateTime> range, DateTime dateTime) {
		assertTrue(dateTime + " not within " + range, range.apply(dateTime));
	}
}
