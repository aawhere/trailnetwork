/**
 * 
 */
package com.aawhere.measure.geocell;

import static org.junit.Assert.*;

import java.util.SortedSet;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Basic tests for {@link GeoCells}.
 * 
 * @see GeoCellUnitTest
 * @see GeoCellTestUtil
 * 
 * @author aroller
 * 
 */
public class GeoCellsUnitTest {

	private GeoCell first;
	private GeoCell second;
	private GeoCell third;
	private GeoCells geoCells;

	@Before
	public void setUp() {
		first = new GeoCell("a");
		second = new GeoCell("aa");
		third = new GeoCell("b");
		geoCells = new GeoCells.Builder().add(first).add(second).add(third).add(first).build();
	}

	@Test
	public void testBasics() {

		SortedSet<GeoCell> all = geoCells.getAll();
		assertNotNull(all);
		assertEquals("size is wrong, equals/hash code must not be working", 3, all.size());
		assertEquals("sorting is off", first, all.first());
		assertEquals(third, all.last());
	}

	@Test
	public void testFromCsv() {
		final String separator = ",";
		assertFromCsv(separator);
	}

	/**
	 * @param separator
	 */
	private void assertFromCsv(final String separator) {
		String csv = StringUtils.join(geoCells.getAll(), separator);
		GeoCells parsed = GeoCells.valueOf(csv);
		assertEquals("csv didn't reproduce the same cells", geoCells, parsed);
	}

	@Test
	public void testFromCsvAndSpaces() {
		assertFromCsv(", ");
	}

	@Test
	public void testResolution() {
		Resolution expected = Resolution.EIGHT;
		assertEquals("the number should match the name", 8, (byte) expected.resolution);
		assertEquals("value of producing wrong result", expected, Resolution.valueOf(expected.resolution));
	}

	@Test
	public void testString() {
		String string = geoCells.toString();
		GeoCells actual = GeoCells.create().setFromString(string).build();
		assertEquals(geoCells, actual);
	}

	@Test(expected = InvalidGeoCellException.class)
	public void testInvalidString() {
		String invalid = "aa bcd 23d a3x";
		GeoCells.create().setFromString(invalid).build();
		fail("there was an invalid value in there");
	}

	@Test
	public void testJaxb() {
		JAXBTestUtil<GeoCells> util = JAXBTestUtil.create(geoCells).build();
		GeoCells actual = util.getActual();
		assertEquals(geoCells, actual);
	}
}
