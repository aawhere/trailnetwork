/**
 *
 */
package com.aawhere.measure.geocell;

import static org.junit.Assert.*;

import java.util.SortedSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.InvalidBoundsException;
import com.aawhere.test.TestUtil;

/**
 * @author brian
 * 
 */
public class GeoCellBoundingBoxUnitTest {

	private final Double NE_LAT = 2d;
	private final Double NE_LON = 2d;
	private final Double SW_LAT = -1d;
	private final Double SW_LON = -1d;
	private GeoCoordinate ne;
	private GeoCoordinate sw;
	private GeoCellBoundingBox b;
	GeoCell aa = new GeoCell("aa");
	GeoCell ab = new GeoCell("ab");
	GeoCell ac = new GeoCell("ac");
	GeoCells threeCells = new GeoCells.Builder().add(aa).add(ab).add(ac).build();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ne = new GeoCoordinate.Builder().setLatitude(SW_LAT).setLongitude(NE_LON).build();
		sw = new GeoCoordinate.Builder().setLatitude(NE_LAT).setLongitude(SW_LON).build();
		b = new GeoCellBoundingBox.Builder().setNorthEast(ne).setSouthWest(sw).build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.geocell.GeoCellBoundingBox#GeoCellBoundingBox(com.aawhere.measure.BoundingBox)}
	 * .
	 */
	@Test
	public void testGeoCellBoundingBoxBoundingBox() {

		assertGeoCellBoundingBox(b);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.geocell.GeoCellBoundingBox#GeoCellBoundingBox(com.aawhere.measure.GeoCoordinate, com.aawhere.measure.GeoCoordinate)}
	 * .
	 */
	@Test
	public void testGeoCellBoundingBoxGeoCoordinateGeoCoordinate() {
		GeoCellBoundingBox b = new GeoCellBoundingBox.Builder().setNorthEast(ne).setSouthWest(sw).build();
		assertGeoCellBoundingBox(b);
	}

	@Test
	public void testValueOf() {
		String expectedBounds = BoundingBoxTestUtils.LARGE_SQAURE_FROM_ORIGIN;
		GeoCellBoundingBox cellBox = new GeoCellBoundingBox.Builder().setBounds(expectedBounds).build();
		GeoCells actualGeoCells = cellBox.getGeoCells();
		assertNotNull(actualGeoCells);
		GeoCellBoundingBox boxByCells = GeoCellBoundingBox.valueOf(actualGeoCells.toString());
		// test contains when it is working
		// boxByCells.contains(box);
		assertEquals(cellBox.getGeoCells(), boxByCells.getGeoCells());
		// cell bounds always grow bigger than coordinate bounds
		BoundingBox box = BoundingBox.valueOf(expectedBounds);
		BoundingBoxTestUtils.assertContains(cellBox, box);
		BoundingBoxTestUtils.assertContains(boxByCells, box);
	}

	/**
	 * Simple test to show given a geocel bounding box this will generate the same geocell in
	 * return.
	 * 
	 */
	@Test
	public void testGenerateGeoCellsWithinMaxSize() {
		GeoCellBoundingBox input = GeoCellBoundingBox.createGeoCell().setGeoCells(threeCells).build();
		GeoCells geoCells = input.getGeoCells();
		assertEquals("nothing should have changed", threeCells, geoCells);
	}

	@Test
	public void testGenerateGeoCellsMaxSizeExceeded() {

		final int maxGeoCells = 2;
		GeoCellBoundingBox input = GeoCellBoundingBox.createGeoCell().maxGeoCells(maxGeoCells).setGeoCells(threeCells)
				.build();
		GeoCells minimized = input.getGeoCells();
		final SortedSet<GeoCell> all = minimized.getAll();
		assertEquals(" incorrect number of geocells: " + all, 1, all.size());
		GeoCell expected = new GeoCell("a");
		assertEquals(threeCells + " should have been reduced to " + expected, expected, all.first());

	}

	@Test
	public void testBadValueOf() {
		String invalid = GeoCellUnitTest.INVALID;
		try {
			GeoCellBoundingBox cell = GeoCellBoundingBox.valueOf(invalid);
			fail(cell + " shouldn't have been built");
		} catch (InvalidBoundsException e) {
			TestUtil.assertContains(e.getMessage(), invalid);
		}
	}

	@Test
	public void testMaxLimit() {
		Integer maxResultSet = 32;
		// This bounding box was producing > 32 cells at the WebApi level.
		String boundingBoxString = "37.66931138468582,-123.12190466015625|38.09474258680874,-121.80354528515625";
		GeoCellBoundingBox box = new GeoCellBoundingBox.Builder().setOther(BoundingBox.valueOf(boundingBoxString))
				.maxGeoCells(maxResultSet).build();
		Integer boxCellSize = box.getGeoCells().getAll().size();
		assertTrue(	"max expected result set of " + maxResultSet + " exceeded. Found " + boxCellSize,
					boxCellSize <= maxResultSet);

	}

	@Test
	public void testBuilderGeoCells() {
		GeoCell gc1 = new GeoCell("8");
		GeoCell gc2 = new GeoCell("4");

		GeoCellBoundingBox box = new GeoCellBoundingBox.Builder().setGeoCells(gc1, gc2).build();
		assertNotNull(box);
		final SortedSet<GeoCell> all = box.getGeoCells().getAll();
		TestUtil.assertContains(gc1, all);
		TestUtil.assertContains(gc2, all);

	}

	public void assertGeoCellBoundingBox(GeoCellBoundingBox b) {
		assertNotNull(b);
		assertNotNull(b.getGeoCells());
	}

}
