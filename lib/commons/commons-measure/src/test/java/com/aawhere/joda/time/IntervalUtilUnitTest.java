/**
 * 
 */
package com.aawhere.joda.time;

import static com.aawhere.joda.time.IntervalUtil.*;
import static com.aawhere.joda.time.JodaTestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;

import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class IntervalUtilUnitTest {

	@Test
	public void testExpandInterval() {

		assertEquals(zeroToOne, expanded(zeroToOne, zeroToOne));
		assertEquals(oneToThree, expanded(oneToTwo, twoToThree));
		assertEquals(zeroToThree, expanded(zeroToOne, twoToThree));
		// now the inverse to make sure order doesn't matter
		assertEquals(oneToThree, expanded(twoToThree, oneToTwo));
		assertEquals(zeroToThree, expanded(twoToThree, zeroToOne));
	}

	@Test
	public void testMinimizeIntervalSolo() {
		ArrayList<Interval> expected = Lists.newArrayList(zeroToThree);
		assertEquals(expected, minimizeOverlappingIntervals(expected));
	}

	@Test
	public void testMinimizeIntervalAdjacent() {
		ArrayList<Interval> input = Lists.newArrayList(zeroToOne, oneToTwo);
		assertEquals("overlap is exclusive of the end time", input, minimizeOverlappingIntervals(input));
	}

	@Test
	public void testMinimizeIntervalOverlapping() {
		ArrayList<Interval> input = Lists.newArrayList(zeroToTwo, oneToThree);
		ArrayList<Interval> expected = Lists.newArrayList(zeroToThree);
		assertEquals("times overlap so all should be combined", expected, minimizeOverlappingIntervals(input));
	}

	@Test
	public void testMinimizeIntervalManyRepeats() {
		ArrayList<Interval> input = Lists.newArrayList(	zeroToOne,
														zeroToOne,
														zeroToTwo,
														zeroToOne,
														oneToThree,
														zeroToOne,
														twoToThree);
		ArrayList<Interval> expected = Lists.newArrayList(zeroToThree);
		assertEquals("times overlap so all should be combined", expected, minimizeOverlappingIntervals(input));
	}

	@Test
	public void testMinimizeIntervalBiggestFirst() {
		ArrayList<Interval> input = Lists.newArrayList(twoToThree, zeroToOne);
		ArrayList<Interval> expected = Lists.newArrayList(zeroToOne, twoToThree);
		assertEquals("no overlapp, but order changed", expected, minimizeOverlappingIntervals(input));
	}

	@Test
	public void testMinimizeIntervalProgressivelySmaller() {
		ArrayList<Interval> input = Lists.newArrayList(twoToThree, oneToThree, zeroToThree);
		ArrayList<Interval> expected = Lists.newArrayList(zeroToThree);
		assertEquals("no overlapp, but order changed", expected, minimizeOverlappingIntervals(input));
	}

	@Test
	public void testMinimizeIntervalSpanTheGap() {
		ArrayList<Interval> input = Lists.newArrayList(twoToThree, zeroToTwo, oneToThree);
		ArrayList<Interval> expected = Lists.newArrayList(zeroToThree);
		assertEquals(	"the final one should combine the previous two into a single ",
						expected,
						minimizeOverlappingIntervals(input));
	}

	@Test
	public void testMinMaxDateTime() {
		DateTime small = new DateTime(1);
		DateTime big = new DateTime(2);
		assertEquals(small, min(small, big));
		assertEquals(small, min(big, small));
		assertEquals(small, min(small, small));
		assertEquals(big, max(small, big));
		assertEquals(big, max(big, small));
		assertEquals(big, max(big, big));
	}
}
