/**
 *
 */
package com.aawhere.measure.geocell;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.test.TestUtil;

import com.beoui.geocell.GeocellManager;
import com.beoui.geocell.model.Point;

/**
 * @author brian
 * 
 */
public class GeoCellCoordinateUnitTest {

	private final Double LAT = 1d;
	private final Double LON = 2d;
	private GeoCoordinate coord;
	private GeoCellCoordinate cellCoord;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		coord = new GeoCoordinate(LAT, LON);
		cellCoord = new GeoCellCoordinate(coord);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.geocell.GeoCellCoordinate#GeoCellCoordinate(com.aawhere.measure.Latitude, com.aawhere.measure.Longitude)}
	 * .
	 */
	@Test
	public void testGeoCellCoordinateLatitudeLongitude() {
		GeoCellCoordinate c = new GeoCellCoordinate(coord.getLatitude(), coord.getLongitude());
		assertGeoCellCoordinate(c);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.geocell.GeoCellCoordinate#GeoCellCoordinate(java.lang.Double, java.lang.Double)}
	 * .
	 */
	@Test
	public void testGeoCellCoordinateDoubleDouble() {
		GeoCellCoordinate c = new GeoCellCoordinate(LAT, LON);
		assertGeoCellCoordinate(c);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.geocell.GeoCellCoordinate#GeoCellCoordinate(com.aawhere.measure.Longitude, com.aawhere.measure.Latitude)}
	 * .
	 */
	@Test
	public void testGeoCellCoordinateLongitudeLatitude() {
		GeoCellCoordinate c = new GeoCellCoordinate(coord.getLongitude(), coord.getLatitude());
		assertGeoCellCoordinate(c);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.geocell.GeoCellCoordinate#GeoCellCoordinate(com.aawhere.measure.GeoCoordinate)}
	 * .
	 */
	@Test
	public void testGeoCellCoordinateGeoCoordinate() {
		GeoCellCoordinate c = new GeoCellCoordinate(coord);
		assertGeoCellCoordinate(c);
	}

	/**
	 * Test method for {@link com.aawhere.measure.geocell.GeoCellCoordinate#getGeoCell(int)}.
	 */
	@Test
	public void testGetGeoCell() {
		List<String> cells = GeocellManager.generateGeoCell(new Point(LAT, LON));
		for (int i = 1; i <= GeoCellUtil.MAX_GEOCELL_RESOLUTION.asInteger(); i++) {
			assertEquals(cells.get(i - 1), (cellCoord.getGeoCell(GeoCellUtil.getResolution(i))).getCellAsString());
		}
	}

	/**
	 * Test method for {@link com.aawhere.measure.geocell.GeoCellCoordinate#getGeoCells()}.
	 */
	@Test
	public void testGetGeoCells() {
		List<String> cells = GeocellManager.generateGeoCell(new Point(LAT, LON));
		assertEquals(cells.size(), cellCoord.getGeoCells().size());
	}

	private void assertGeoCellCoordinate(GeoCellCoordinate c) {
		assertNotNull(c);
		TestUtil.assertDoubleEquals(LAT, c.getLatitude().getInDecimalDegrees());
		TestUtil.assertDoubleEquals(LON, c.getLongitude().getInDecimalDegrees());
		for (String cellString : GeocellManager.generateGeoCell(new Point(LAT, LON))) {
			assertTrue(c.getGeoCells().contains(new GeoCell(cellString)));
		}
	}

}
