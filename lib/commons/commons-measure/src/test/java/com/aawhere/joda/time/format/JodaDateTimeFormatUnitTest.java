/**
 *
 */
package com.aawhere.joda.time.format;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;

/**
 * @author brian
 * 
 */
public class JodaDateTimeFormatUnitTest {

	DateTime date;
	Locale locale;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		date = new DateTime();
		locale = new Locale("en", "US");
	}

	/**
	 * Test method for
	 * {@link com.aawhere.joda.time.format.JodaDateTimeFormat#format(org.joda.time.DateTime, java.util.Locale)}
	 * .
	 */
	@Test
	public void testFormatDateTimeLocale() {
		String actual = new JodaDateTimeFormat().format(date, locale);
		String expected = DateTimeFormat.mediumDateTime().withLocale(locale).print(date);
		assertEquals(expected, actual);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.joda.time.format.JodaDateTimeFormat#format(org.joda.time.DateTime, java.util.Locale, com.aawhere.joda.time.format.JodaDateTimeFormat.Format)}
	 * .
	 */
	@Test
	public void testFormatDateTimeLocaleFormat() {
		String actual = new JodaDateTimeFormat().format(date, locale, JodaDateTimeFormat.Format.SHORT_DATE);
		String expected = DateTimeFormat.shortDate().withLocale(locale).print(date);
		assertEquals(expected, actual);
	}

	/**
	 * Test method for {@link com.aawhere.joda.time.format.JodaDateTimeFormat#handlesType()}.
	 */
	@Test
	public void testHandlesType() {
		Class<?> handles = new JodaDateTimeFormat().handlesType();
		assertEquals(DateTime.class, handles);
	}

}
