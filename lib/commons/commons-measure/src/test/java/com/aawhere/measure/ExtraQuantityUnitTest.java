/**
 *
 */
package com.aawhere.measure;

import static org.junit.Assert.*;

import javax.measure.quantity.Quantity;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.Unit;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * A base class for testing new Quantity types.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class ExtraQuantityUnitTest<QuantityT extends Quantity<QuantityT>> {

	protected static final double REASONABLE_PRECISION = 0.00001;

	/**
	 * The class represented by QuantityT.
	 */
	protected abstract Class<QuantityT> getQuantityClass();

	/**
	 * The normalized unit for QuantityT
	 * 
	 * @return
	 */
	protected abstract Unit<QuantityT> getNormalizedUnit();

	/**
	 * A unit for QuantityT to be used by tests.
	 * 
	 * @return
	 */
	protected abstract Unit<QuantityT> getUnit();

	public QuantityT generateRandomQuantity() {
		Double value = TestUtil.generateRandomDouble(10, 10000);
		QuantityT quantity = createQuantity(value, getUnit());
		assertNotNull(quantity);
		assertQuantity(quantity, value, getUnit());
		return quantity;
	}

	public QuantityT createQuantity(Double value, Unit<QuantityT> unit) {
		QuantityT quantity = MeasurementQuantityFactory.getInstance(getQuantityClass()).create(value, unit);
		assertQuantity(quantity, value, unit);
		return quantity;
	}

	private void assertQuantity(QuantityT quantity, Double expectedValue, Unit<QuantityT> unit) {
		TestUtil.assertDoubleEquals(expectedValue, quantity.doubleValue(unit));
	}

	@Test
	public void testConstruction() {
		// asserts found in generator
		generateRandomQuantity();

	}

	@Test
	public void testEquality() {
		final Double value = 3325d;
		QuantityT expected = createQuantity(value, getUnit());
		QuantityT actual = createQuantity(value, getUnit());
		assertEquals(expected, actual);
	}

	@Test
	public void testNegativeQuantity() {
		QuantityT quantity = createQuantity(-10d, getUnit());
		assertNotNull(quantity);
	}

	@Test
	public void testNormalizedUnit() {
		Unit<QuantityT> expected = getNormalizedUnit();
		Unit<QuantityT> actual = MeasurementUtil.getNormalizedUnit(getQuantityClass());
		assertEquals(expected, actual);
	}

	@Test
	public void testQuantityFactory() {
		QuantityFactory<QuantityT> f = MeasurementQuantityFactory.getInstance(getQuantityClass());
		final Integer expectedValue = 3;
		final Unit<QuantityT> expectedUnit = getUnit();
		QuantityT actual = f.create(expectedValue, expectedUnit);
		assertTrue(	"we should be getting the concrete implementation, not the proxy",
					AbstractQuantity.class.isAssignableFrom(actual.getClass()));
		assertNotNull(actual);
		assertEquals(expectedValue, actual.getValue());
		assertEquals(expectedUnit, actual.getUnit());
		TestUtil.assertDoubleEquals(expectedValue, actual.doubleValue(expectedUnit));
	}

}
