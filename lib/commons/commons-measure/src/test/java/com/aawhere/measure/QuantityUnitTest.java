package com.aawhere.measure;

import static javax.measure.unit.USCustomarySystem.DEGREE_ANGLE;
import static javax.measure.unit.USCustomarySystem.FOOT;
import static javax.measure.unit.USCustomarySystem.MINUTE_ANGLE;
import static javax.measure.unit.USCustomarySystem.YARD;
import static org.junit.Assert.assertEquals;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;
import javax.measure.unit.UnitConverter;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * Test to verify behavior of {@link Quantity} and {@link Unit} classes from javax.measure
 * 
 * @author brian
 * 
 */
public class QuantityUnitTest {

	private final double QTY_FOOT = 40;
	private final double QTY_YARD = 40 / 3;
	private final double QTY_METER = 12.19200;
	private final double QTY_DEGREE_ANGLE = 90;
	private final double QTY_MINUTE_ANGLE = QTY_DEGREE_ANGLE * 60;
	private final double QTY_RADIAN = 1.57079633;

	@Test
	public void testLengthIsNormalizedFoot() {
		Length length = MeasureTestUtil.createLength(QTY_FOOT, FOOT);
		// Verifies that we don't get a normalized value back from the Quantity
		// classes.
		assertEquals("", QTY_FOOT, length.getValue());
	}

	@Test
	public void testLengthIsNormalizedYard() {
		Length length = MeasureTestUtil.createLength(QTY_YARD, YARD);
		// Verifies that we don't get a normalized value back from the Quantity
		// classes.
		assertEquals("", QTY_YARD, length.getValue());
	}

	@Test
	public void testLengthToMetric() {
		Length length = MeasureTestUtil.createLength(QTY_FOOT, FOOT);
		// Verifies that we don't get a normalized value back from the Quantity
		// classes.

		Unit<Length> unit = length.getUnit();
		UnitConverter converter = unit.getConverterToMetric();
		double convertedValue = converter.convert(length.getValue().doubleValue());
		TestUtil.assertDoubleEquals(QTY_METER, convertedValue);

		Unit<Length> metricUnit = unit.toMetric();
		double metricValue = length.doubleValue(metricUnit);
		TestUtil.assertDoubleEquals(QTY_METER, metricValue);
	}

	@Test
	public void testAngleIsNormalized() {
		Angle angle = MeasureTestUtil.createAngle(QTY_MINUTE_ANGLE, MINUTE_ANGLE);
		// Verifies that we don't get a normalized value back from the Quantity
		// classes.
		assertEquals("", QTY_MINUTE_ANGLE, angle.getValue());
	}

	@Test
	public void testAngleToMetric() {
		final Angle angle = MeasureTestUtil.createAngle(QTY_DEGREE_ANGLE, DEGREE_ANGLE);
		// Verifies that we don't get a normalized value back from the Quantity
		// classes.

		Unit<Angle> unit = angle.getUnit();
		UnitConverter converter = unit.getConverterToMetric();
		double convertedValue = converter.convert(angle.getValue().doubleValue());
		TestUtil.assertDoubleEquals(QTY_RADIAN, convertedValue);

		Unit<Angle> metricUnit = unit.toMetric();
		double metricValue = angle.doubleValue(metricUnit);
		TestUtil.assertDoubleEquals(QTY_RADIAN, metricValue);
	}

	@Test
	public void testAngleConversion() {
		final Angle angle = MeasureTestUtil.createAngle(QTY_DEGREE_ANGLE, DEGREE_ANGLE);
		double minuteValue = angle.doubleValue(MINUTE_ANGLE);
		TestUtil.assertDoubleEquals(QTY_MINUTE_ANGLE, minuteValue);
	}

}
