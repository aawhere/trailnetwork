/**
 * 
 */
package com.aawhere.measure.geocell;

import static com.aawhere.measure.geocell.GeoCellTestUtil.*;
import static com.aawhere.measure.geocell.GeoCellUtil.*;
import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import com.aawhere.measure.BoundingBox;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class GeoCellReducerUnitTest {

	@Test
	public void testReduceSize() {
		final int expectedSize = 3;
		final GeoCellsReducer reducer = GeoCellsReducer.create().setGeoCells(A_THRU_F_WITH_PARENTS.getAll())
				.setMaxSize(expectedSize).build();
		TestUtil.assertNotEquals(expectedSize, A_THRU_F_WITH_PARENTS.getAll().size());
		Set<GeoCell> three = reducer.getGeoCells();
		TestUtil.assertSize(expectedSize, three);
		TestUtil.assertContains(A, three);
		TestUtil.assertContains(AB, three);
		TestUtil.assertContains(ABC, three);
		assertEquals("incorrect resolution in reducer", ABC.getResolution(), reducer.getMaxResolution());
	}

	@Test
	public void testRemoveParents() {
		GeoCellsReducer reducer = GeoCellsReducer.create().setGeoCells(A_THRU_F_WITH_PARENTS.getAll()).removeParents()
				.build();
		Set<GeoCell> geoCells = reducer.getGeoCells();
		TestUtil.assertSize(1, geoCells);
		assertEquals(ABCDEF.getResolution(), geoCells.iterator().next().getResolution());
	}

	@Test
	public void testMinimizeNothing() {
		GeoCellsReducer reducer = GeoCellsReducer.create().setGeoCells(A_THRU_F_WITH_PARENTS.getAll()).minimize()
				.build();
		TestUtil.assertSize(A_THRU_F_WITH_PARENTS.getAll().size(), reducer.getGeoCells());
		assertEquals(ABCDEF.getResolution(), reducer.getMaxResolution());
	}

	/**
	 * Inspired by TN-608 this ensures that when size reduction happens parents are included if
	 * desired.
	 * 
	 */
	@Test
	public void testSizeReductionIncludesParents() {
		final GeoCells tooManyCells = A_THRU_F_WITH_PARENTS;
		Integer maxSize = tooManyCells.size() - 1;
		GeoCellsReducer reducer = GeoCellsReducer.create().singleResolutionIgnoringParents(maxSize)
				.setGeoCells(tooManyCells).build();
		assertEquals(tooManyCells.getMaxResolution().lower(), reducer.getMaxResolution());
		final GeoCells reduced = reducer.getReduced();
		assertEquals("size was reduced too much", maxSize, reduced.size());
	}

	/**
	 * Takes the standard test set and adds another, more detailed row to it and minimizes it to
	 * make sure it goes away.
	 * 
	 */
	@Test
	public void testMinimizeOneResolution() {
		Set<GeoCell> cells = explode(ABCDEF);
		assertEquals(GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL, cells.size());
		cells.addAll(A_THRU_F_WITH_PARENTS.getAll());
		GeoCellsReducer reducer = GeoCellsReducer.create().setMinimize(true).setGeoCells(cells).build();
		Set<GeoCell> reduced = reducer.getGeoCells();
		assertEquals(A_THRU_F_WITH_PARENTS.getAll(), reduced);
	}

	@Test
	public void testRemoveParentsAndSizeNotExceeded() {
		int expectedSize = 3;
		GeoCellsReducer reducer = GeoCellsReducer.create().setGeoCells(A_THRU_F_WITH_PARENTS).setMaxSize(expectedSize)
				.setRemoveParents(true).build();
		// only one because there is only 1 in that resolution
		TestUtil.assertSize(1, reducer.getGeoCells());
		TestUtil.assertContains(ABCDEF, reducer.getGeoCells());
	}

	@Test
	public void testRemoveParentsAndSizeExceeded() {
		Set<GeoCell> explodeLast = explodeLast(A_THRU_F_WITH_PARENTS.getAll());
		int expectedSize = 3;

		// notice no reduction here, otherwise that would be what removes the last row.
		GeoCellsReducer reducer = GeoCellsReducer.create().setGeoCells(explodeLast).setMaxSize(expectedSize)
				.setRemoveParents(true).build();
		// only 1 because there is only one element in that resolution
		TestUtil.assertSize(1, reducer.getGeoCells());
		TestUtil.assertContains(ABCDEF, reducer.getGeoCells());
	}

	@Test
	public void testRemoveParentsAndSizeExceededAtMultipleResolutions() {

		Set<GeoCell> set = explode(ABCD);
		set.addAll(explode(ABCDE));
		set.addAll(A_THRU_F_WITH_PARENTS.getAll());
		GeoCellsReducer reducer = GeoCellsReducer.create().setGeoCells(set).setMaxSize(2).setRemoveParents(true)
				.build();
		TestUtil.assertSize(1, reducer.getGeoCells());
		// ABCD is chosen because it is the first resolution with only 1
		TestUtil.assertContains(ABCD, reducer.getGeoCells());
	}

	@Test
	public void testRemoveParentsMinimizedAndSizeExceededAtMultipleResolutions() {
		Set<GeoCell> set = explode(ABCDE);
		set.addAll(A_THRU_F_WITH_PARENTS.getAll());
		GeoCellsReducer reducer = GeoCellsReducer.create().setGeoCells(set).setMaxSize(2).setMinimize(true)
				.setRemoveParents(true).build();
		TestUtil.assertSize(1, reducer.getGeoCells());
		// since minimization is on the highest resolution was minimized and with no parents leaves
		// only 1
		TestUtil.assertContains(ABCDE, reducer.getGeoCells());
	}

	@Test
	public void testExceedingMaxResolutionWithoutParents() {
		final Resolution maxResolution = Resolution.THREE;
		final GeoCells original = GeoCells.create().add(ABCDE).build();
		final GeoCells expected = GeoCells.create().add(ABC).build();
		final Resolution expectedResolution = maxResolution;
		assertResolution(original, maxResolution, expected, expectedResolution);
	}

	@Test
	public void testNotExceedingMaxResolution() {
		assertResolution(A_THRU_F_WITH_PARENTS, Resolution.THIRTEEN, A_THRU_F_WITH_PARENTS, ABCDEF.getResolution());
	}

	@Test
	public void testExceedingMaxResolutionWithParents() {
		final Resolution resolution = Resolution.THREE;
		final GeoCells aThruCWithParents = GeoCells.create()
				.addAll(GeoCellUtil.removeCellsWithResolutionsHigherThan(A_THRU_F_WITH_PARENTS.getAll(), resolution))
				.build();
		assertResolution(A_THRU_F_WITH_PARENTS, resolution, aThruCWithParents, resolution);
	}

	@Test
	public void testMaxSizeMaxResolutionMinimizeRemoveParents() {

		int maxSize = 3;
		Resolution maxResolution = Resolution.FOUR;
		GeoCellsReducer reducer = GeoCellsReducer.create().setMaxSize(maxSize).minimize()
				.setMaxResolution(maxResolution).removeParents().setGeoCells(A_THRU_F_WITH_PARENTS).build();
		Set<GeoCell> cells = reducer.getGeoCells();
		TestUtil.assertSize(Range.atMost(maxSize), cells);
		assertEquals(maxResolution, reducer.getMaxResolution());
	}

	/**
	 * TN-748 describes how searching by bounds is resulting in geocells way out of bounds from the
	 * desired. This will test that when restricted by few numbers this will choose the best cells
	 * to represent the box without going too large.
	 * 
	 */
	@Test
	public void testBoundsToGeoCellBoundingBoxTn748() {
		String bounds = GeoCellTestUtil.MARIN_VIEW_BOUNDS;
		GeoCells expectedCells = GeoCellTestUtil.MARIN_VIEW_CELLS;
		BoundingBox boundingBox = BoundingBox.valueOf(bounds);
		GeoCellBoundingBox geoCellBoundingBox = GeoCellBoundingBox.createGeoCell().setOther(boundingBox).build();
		GeoCellsReducer geoCellsReducer = GeoCellsReducer.create().proximityBoundary(boundingBox).minimize()
				.removeParents().setMaxSize(expectedCells.size()).setGeoCells(geoCellBoundingBox.getGeoCells()).build();
		Set<GeoCell> actual = Sets.newTreeSet(geoCellsReducer.getGeoCells());
		TestUtil.assertCollectionEquals(expectedCells.getAll(), actual);
	}

	/**
	 * @param original
	 * @param maxResolution
	 * @param expected
	 * @param expectedResolution
	 */
	private void assertResolution(final GeoCells original, final Resolution maxResolution, final GeoCells expected,
			final Resolution expectedResolution) {
		GeoCellsReducer reducer = GeoCellsReducer.create().setMaxResolution(maxResolution).setGeoCells(original)
				.build();
		assertEquals(	reducer.getGeoCells().toString() + " exceeded resolution",
						expectedResolution,
						reducer.getMaxResolution());
		TestUtil.assertSize(expected.getAll().size(), reducer.getGeoCells());
		assertEquals(expected.getAll(), reducer.getGeoCells());
	}
}
