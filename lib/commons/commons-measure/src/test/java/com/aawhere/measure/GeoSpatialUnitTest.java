/**
 *
 */
package com.aawhere.measure;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.test.TestUtil;

/**
 * @author roller
 * 
 */
public class GeoSpatialUnitTest {

	/**
	 *
	 */
	// private static final double REASONABLE_PRECISION = 0.00001;

	@Test
	public void testLatitude() {
		final double value = 90;
		Latitude latitude = MeasurementUtil.createLatitude(value);
		assertNotNull(latitude);
		TestUtil.assertDoubleEquals(value, latitude.getInDecimalDegrees());
		TestUtil.assertDoubleEquals(value, latitude.doubleValue(ExtraUnits.DECIMAL_DEGREES_LATITUDE));

	}

	@Test
	public void testConversions() {
		final double decimalDegrees = 180 / Math.PI;
		Latitude expectedDegrees = MeasurementUtil.createLatitude(decimalDegrees);
		TestUtil.assertDoubleEquals(decimalDegrees, (Double) expectedDegrees.getValue());
		TestUtil.assertDoubleEquals(decimalDegrees,
									(Double) expectedDegrees.doubleValue(ExtraUnits.DECIMAL_DEGREES_LATITUDE));

		final int radians = 1;

		Latitude expectedRads = MeasurementQuantityFactory.getInstance(Latitude.class)
				.create(radians, ExtraUnits.RADIAN_LATITUDE);
		TestUtil.assertDoubleEquals(radians, expectedDegrees.doubleValue(ExtraUnits.RADIAN_LATITUDE));
		TestUtil.assertDoubleEquals(decimalDegrees, expectedRads.getInDecimalDegrees());
	}

	@Test
	public void testAsString() {
		GeoCoordinate expected = MeasureTestUtil.createRandomGeoCoordinate();
		String actualString = expected.getAsString();
		assertNotNull(actualString);
		TestUtil.assertContains(actualString, GeoCoordinate.STRING_DILIMETER);
		GeoCoordinate actual = GeoCoordinate.valueOf(actualString);
		assertEquals("valueOf is producing junk", expected, actual);
	}

	/**
	 * @return
	 */
	public static GeoSpatialUnitTest getInstance() {

		return new GeoSpatialUnitTest();
	}

}
