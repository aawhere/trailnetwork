/**
 * 
 */
package com.aawhere.measure.calc;

import static org.junit.Assert.*;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;

/**
 * * All points mapped out on Google Map with points of the same name. Measurements recorded using
 * the distance measure tool. Tolerances adjusted to allow for differences between the two measuring
 * tools. http://goo.gl/maps/KFI5
 * 
 * TODO:Move more from GeoCalculatorUnitTest to here.
 * 
 * @author aroller
 * 
 */
public class GeoMathTestUtil {

	private static final double EARTH_ORIGIN_LON = 0.0;
	private static final double EARTH_ORIGIN_LAT = 0.0;
	private static final double OFFSET = 1;
	private static final double EARTH_ORIGIN_LAT_NORTH = EARTH_ORIGIN_LAT + OFFSET;
	private static final double EARTH_ORIGIN_LAT_SOUTH = EARTH_ORIGIN_LAT - OFFSET;
	private static final double EARTH_ORIGIN_LON_EAST = EARTH_ORIGIN_LON + OFFSET;
	private static final double EARTH_ORIGIN_LON_WEST = EARTH_ORIGIN_LON - OFFSET;
	public static final double NO_TOLERANCE = 0.0;
	public static final double ORIGIN_LONGITUDE_IN_DECIMAL_DEGREES = -62.885399;
	public static final double ORIGIN_LATITUDE_IN_DECIMAL_DEGREES = -0.482878;
	public static GeoCoordinate EQUATOR_ORIGIN = MeasurementUtil
			.createGeoCoordinate(ORIGIN_LATITUDE_IN_DECIMAL_DEGREES, ORIGIN_LONGITUDE_IN_DECIMAL_DEGREES);
	public static GeoCoordinate EQUATOR_NEAR = MeasurementUtil.createGeoCoordinate(-0.482888, -62.886778);
	public static GeoCoordinate EQUATOR_FAR = MeasurementUtil.createGeoCoordinate(-0.527339, 11.250000);
	public static GeoCoordinate ARCTIC_ORIGIN = MeasurementUtil.createGeoCoordinate(83.0581868, -35.948261);
	public static GeoCoordinate ARCTIC_NEAR = MeasurementUtil.createGeoCoordinate(83.0581868, -35.948422);
	public static GeoCoordinate ARCTIC_FAR = MeasurementUtil.createGeoCoordinate(83.2777047, 90.351568);
	public static GeoCoordinate FAR_EAST = MeasurementUtil.createGeoCoordinate(-0.175124, 178.066409);
	public static GeoCoordinate FAR_WEST = MeasurementUtil.createGeoCoordinate(-0.351568, -177.714841);

	public static double METERS_FROM_ORIGIN_TO_NEAR = 153.5;
	public static final int METERS_FROM_SAME_POINT = 0;
	public static final int METERS_FROM_ORIGIN_TO_ARCTIC_ORIGIN = 9552860;
	public static final double METERS_FROM_ARCTIC_ORIGIN_TO_ARCTIC_NEAR = 2.16642;
	public static final int METERS_FROM_ARCTIC_ORIGIN_TO_ARCTIC_FAR = 1356480;
	public static final int METERS_FROM_ORIGIN_TO_FAR = 8252342;
	public static final int METERS_FROM_FAR_WEST_TO_FAR_EAST = 470032;

	public static final GeoCoordinate EARTH_ORIGIN = MeasurementUtil.createGeoCoordinate(	EARTH_ORIGIN_LAT,
																							EARTH_ORIGIN_LON);
	public static final GeoCoordinate EARTH_ORIGIN_NORTH = MeasurementUtil.createGeoCoordinate(	EARTH_ORIGIN_LAT_NORTH,
																								EARTH_ORIGIN_LON);
	public static final GeoCoordinate EARTH_ORIGIN_SOUTH = MeasurementUtil.createGeoCoordinate(	EARTH_ORIGIN_LAT_SOUTH,
																								EARTH_ORIGIN_LON);
	public static final GeoCoordinate EARTH_ORIGIN_EAST_HALFWAY = MeasurementUtil
			.createGeoCoordinate(EARTH_ORIGIN_LAT, EARTH_ORIGIN_LON_EAST / 2);
	public static final GeoCoordinate EARTH_ORIGIN_WEST_HALFWAY = MeasurementUtil
			.createGeoCoordinate(EARTH_ORIGIN_LAT, EARTH_ORIGIN_LON_WEST / 2);
	public static final GeoCoordinate EARTH_ORIGIN_SOUTH_HALFWAY = MeasurementUtil
			.createGeoCoordinate(EARTH_ORIGIN_LAT, EARTH_ORIGIN_LAT_SOUTH / 2);
	public static final GeoCoordinate EARTH_ORIGIN_NORTH_HALFWAY = MeasurementUtil
			.createGeoCoordinate(EARTH_ORIGIN_LAT, EARTH_ORIGIN_LAT_NORTH / 2);
	public static final GeoCoordinate EARTH_ORIGIN_EAST = MeasurementUtil.createGeoCoordinate(	EARTH_ORIGIN_LAT,
																								EARTH_ORIGIN_LON_EAST);
	public static final GeoCoordinate EARTH_ORIGIN_WEST = MeasurementUtil.createGeoCoordinate(	EARTH_ORIGIN_LAT,
																								EARTH_ORIGIN_LON_WEST);

	public static final GeoCoordinate EARTH_ORIGIN_SOUTH_WEST = MeasurementUtil
			.createGeoCoordinate(EARTH_ORIGIN_LAT_SOUTH, EARTH_ORIGIN_LON_WEST);
	public static final GeoCoordinate EARTH_ORIGIN_SOUTH_WEST_HALFWAY = MeasurementUtil
			.createGeoCoordinate(EARTH_ORIGIN_SOUTH_HALFWAY.getLatitude(), EARTH_ORIGIN_WEST_HALFWAY.getLongitude());
	public static final GeoCoordinate EARTH_ORIGIN_NORTH_EAST = MeasurementUtil
			.createGeoCoordinate(EARTH_ORIGIN_LAT_NORTH, EARTH_ORIGIN_LON_EAST);
	public static final GeoCoordinate EARTH_ORIGIN_NORTH_EAST_HALFWAY = MeasurementUtil
			.createGeoCoordinate(EARTH_ORIGIN_NORTH_HALFWAY.getLatitude(), EARTH_ORIGIN_EAST_HALFWAY.getLongitude());

	/**
	 * Given two points and the known distance this will calculate the distance between and confirm
	 * by then creating the geopoint given the distance and heading between the two points.
	 * 
	 * @see GeoMath#coordinateFrom(GeoCoordinate, Angle, Length)
	 * @see GeoMath#horizontalDistanceBetween(GeoCoordinate, GeoCoordinate) #see
	 *      {@link GeoMath#heading(GeoCoordinate, GeoCoordinate)}
	 * 
	 * @param expectedMeters
	 * @param origin
	 * @param destination
	 * @param tolerance
	 * @param calculator
	 */
	public static void assertDistance(final double expectedMeters, final GeoCoordinate origin,
			final GeoCoordinate destination, final double tolerance) {
		Length distance = GeoMath.horizontalDistanceBetween(origin, destination);
		assertEquals(MetricSystem.METRE, distance.getUnit());
		assertEquals(expectedMeters, distance.doubleValue(MetricSystem.METRE), tolerance);

		// now, re-calculate the destination using heading and distance
		Angle heading = GeoMath.heading(origin, destination);
		GeoCoordinate calculatedDestination = GeoMath.coordinateFrom(origin, heading, distance);
		MeasureTestUtil.assertEquals(destination, calculatedDestination);
	}
}
