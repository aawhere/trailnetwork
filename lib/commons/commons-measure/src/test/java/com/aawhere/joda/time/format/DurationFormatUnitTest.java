/**
 *
 */
package com.aawhere.joda.time.format;

import static org.junit.Assert.*;

import java.util.Locale;

import org.joda.time.Duration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.joda.time.format.DurationFormat.Format;

/**
 * @author brian
 * 
 */
public class DurationFormatUnitTest {

	Duration duration;
	Locale locale;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Long mills = 10000000L;
		duration = new Duration(mills);
		locale = new Locale("en", "US");
	}

	/**
	 * Test method for
	 * {@link com.aawhere.joda.time.format.DurationFormat#format(org.joda.time.Duration, java.util.Locale)}
	 * .
	 */
	@Test
	public void testFormatDurationLocale() {
		String actual = new DurationFormat().format(duration, locale);
		String expected = "2:46:40";
		assertEquals(expected, actual);
	}

	@Test
	public void testCompactFormat() {
		assertCompact(2, 46, 40, locale, "2:46:40");
		assertCompact(0, 0, 1, locale, "00:01");
		assertCompact(0, 1, 0, locale, "01:00");
		assertCompact(0, 1, 1, locale, "01:01");
		assertCompact(1, 1, 1, locale, "1:01:01");
	}

	@Test
	public void testShorthand() {
		assertShorthand(2, 31, 2, locale, "2h 31m");
		assertShorthand(0, 31, 2, locale, "31m");
		assertShorthand(2, 0, 2, locale, "2h 0m");
	}

	private String format(Integer h, Integer m, Integer s, Locale locale, Format format) {
		return new DurationFormat().format(	Duration.standardHours(h).plus(Duration.standardMinutes(m).plus(Duration
													.standardSeconds(s))),
											locale,
											format);
	}

	private void assertShorthand(Integer h, Integer m, Integer s, Locale locale, String expected) {
		String actual = format(h, m, s, locale, Format.SHORTHAND);
		assertEquals(expected, actual);
	}

	private void assertCompact(Integer h, Integer m, Integer s, Locale locale, String expected) {
		String actual = format(h, m, s, locale, Format.COMPACT);
		assertEquals(expected, actual);
	}

	/**
	 * Test method for {@link com.aawhere.joda.time.format.DurationFormat#handlesType()}.
	 */
	@Test
	public void testHandlesType() {
		Class<?> handles = new DurationFormat().handlesType();
		assertEquals(Duration.class, handles);
	}

}
