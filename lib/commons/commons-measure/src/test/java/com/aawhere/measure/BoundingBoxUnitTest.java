/**
 *
 */
package com.aawhere.measure;

import static com.aawhere.measure.BoundingBoxTestUtils.*;
import static org.junit.Assert.*;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @see BoundingBox JAXBTesting found in identity with other measurements since they currently are
 *      being handled by the personalizer.
 * 
 * @author Brian Chapman
 * 
 */
public class BoundingBoxUnitTest {

	private static final Double CONTAINING_CIRCLE_TOLORANCE = 0.01d;

	private BoundingBox box;
	private GeoCoordinate box2NorthEast;
	private GeoCoordinate box2SouthWest;
	private GeoCoordinate box2Center;
	private BoundingBox box2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		box = BoundingBoxTestUtils.createRandomBoundingBox();
		box2NorthEast = new GeoCoordinate.Builder().setAsString("89,10").build();
		box2SouthWest = new GeoCoordinate.Builder().setAsString("80,2").build();
		box2Center = new GeoCoordinate.Builder().setAsString("84.5,6").build();
		box2 = new BoundingBox.Builder().setNorthEast(box2NorthEast).setSouthWest(box2SouthWest).build();
	}

	/**
	 * Test method for {@link com.aawhere.measure.BoundingBox#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		int hashCode = box.hashCode();
		assertNotNull(hashCode);
	}

	@Test
	public void testBoundingBoxGeoCoordinateBuilder() {
		GeoCoordinate ne = MeasureTestUtil.createRandomGeoCoordinate();
		GeoCoordinate sw = MeasureTestUtil.createRandomGeoCoordinate();
		BoundingBox box = new BoundingBox.Builder().setNorthEast(ne).setSouthWest(sw).build();
		assertEquals(ne, box.getNorthEast());
		assertEquals(sw, box.getSouthWest());

	}

	@Test
	public void testFromOther() {
		BoundingBox expected = BoundingBoxTestUtils.createRandomBoundingBox();
		BoundingBox actual = BoundingBox.create().setOther(expected).build();
		assertEquals(expected, actual);
	}

	@Test(expected = InvalidBoundsException.class)
	public void testBadBoundsString() throws InvalidBoundsException {
		String bad = "xmlsd";
		BoundingBox boundingBox = BoundingBox.valueOf(bad);
		fail(boundingBox + " was produced from junk: " + bad);
	}

	@Test
	public void testBuilderWithIndividualDirections() {
		Latitude north = MeasureTestUtil.createRandomLatitude();
		Latitude south = MeasureTestUtil.createRandomLatitude();
		Longitude east = MeasureTestUtil.createRandomLongitude();
		Longitude west = MeasureTestUtil.createRandomLongitude();

		BoundingBox box = new BoundingBox.Builder().setEast(east).setNorth(north).setSouth(south).setWest(west).build();
		assertEquals(north, box.getNorth());
		assertEquals(west, box.getWest());
		assertEquals(south, box.getSouth());
		assertEquals(east, box.getEast());
		assertEquals(west, box.getSouthWest().getLongitude());
		assertEquals(south, box.getSouthWest().getLatitude());
		assertEquals(north, box.getNorthEast().getLatitude());
		assertEquals(east, box.getNorthEast().getLongitude());
	}

	/**
	 * Test method for {@link com.aawhere.measure.BoundingBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		GeoCoordinate ne = MeasureTestUtil.createRandomGeoCoordinate();
		GeoCoordinate sw = MeasureTestUtil.createRandomGeoCoordinate();
		BoundingBox that = new BoundingBox.Builder().setSouthWest(sw).setNorthEast(ne).build();
		assert (!box.equals(that));
		BoundingBox dis = new BoundingBox.Builder().setSouthWest(sw).setNorthEast(ne).build();
		assert (dis.equals(that));
	}

	@Test
	public void testAsString() throws InvalidBoundsException {
		BoundingBox expected = BoundingBoxTestUtils.createRandomBoundingBox();
		String actualString = expected.getBounds();
		assertNotNull(actualString);
		TestUtil.assertContains(actualString, BoundingBox.STRING_POINT_DILIMETER);
		BoundingBox actual = BoundingBox.valueOf(actualString);
		assertEquals(expected, actual);
	}

	/**
	 * Test the combine method for {@link BoundingBox#combine(BoundingBox)} and
	 * {@link BoundingBoxUtil#combine(BoundingBox, BoundingBox)} for a situation where one bounds
	 * completely contains another.
	 * 
	 * <pre>
	 * 
	 *  |------------|
	 *  |  |-----|   |
	 *  |  |-----|   |
	 *  |------------|
	 * 
	 * </pre>
	 */
	@Test
	public void testCombineBoundingBoxCompletelyContained() {
		BoundingBox small = new BoundingBox.Builder().setBounds(SMALL_SQUARE_AROUND_ORIGIN).build();
		BoundingBox medium = new BoundingBox.Builder().setBounds(MEDIUM_SQUARE_AROUND_ORIGIN).build();
		BoundingBox combined = medium.combine(small);
		assertEquals(medium, combined);
		combined = small.combine(medium);
		assertEquals(medium, combined);
	}

	@Test
	@Ignore("switch to using JTS by creating a commons-geo project")
	public void testContains() {
		BoundingBox small = new BoundingBox.Builder().setBounds(SMALL_SQUARE_AROUND_ORIGIN).build();
		BoundingBox medium = new BoundingBox.Builder().setBounds(MEDIUM_SQUARE_AROUND_ORIGIN).build();
		assertTrue("medium should contain small", medium.contains(small));
		assertFalse("small should be contained by medium", small.contains(medium));
		assertTrue("containment is inclusive", medium.contains(medium));
		BoundingBox fromOrigin = new BoundingBox.Builder().setBounds(MEDIUM_SQUARE_FROM_ORIGIN).build();
		assertFalse("these intersect, not contain", medium.contains(fromOrigin));
		assertFalse("these intersect, not contain", fromOrigin.contains(medium));
	}

	/**
	 * Tests bounding boxes that overlap.
	 * 
	 * <pre>
	 *   |---|
	 * |-+-| |
	 * | |-+-|
	 * |---|
	 * </pre>
	 */
	@Test
	public void testCombineBoundingBoxCrossingOver() {
		BoundingBox left = new BoundingBox.Builder().setBounds(MEDIUM_SQUARE_FROM_ORIGIN).build();
		BoundingBox right = new BoundingBox.Builder().setBounds(MEDIUM_SQAURE_NORTH_EAST_OF_ORIGIN).build();

		BoundingBox expected = new BoundingBox.Builder().setBounds(LARGE_SQAURE_FROM_ORIGIN).build();

		assertEquals(expected, left.combine(right));
		assertEquals(expected, right.combine(left));
	}

	/**
	 * Test method for {@link com.aawhere.measure.BoundingBox#toString()}.
	 */
	@Test
	public void testToString() {
		String str = box.toString();
		assertNotNull(str);
	}

	@Test
	public void testHeight() {
		assertTrue(box2.getHeight().doubleValue(MetricSystem.METRE) > 1000000
				&& box2.getHeight().doubleValue(MetricSystem.METRE) < 1010000);
	}

	@Test
	public void testWidth() {
		assertTrue(box2.getWidth().doubleValue(MetricSystem.METRE) > 85000
				&& box2.getWidth().doubleValue(MetricSystem.METRE) < 85600);
	}

	@Test
	public void testCenter() {
		assertEquals(box2Center, box2.getCenter());
	}

	/**
	 * Tests a round trip using the WMS formatted string of a bounding box.
	 * 
	 */
	@Test
	public void testWmsFormat() {
		BoundingBox expected = BoundingBoxTestUtils.createRandomBoundingBox();
		WmsBoundingBoxFormat fromBox = WmsBoundingBoxFormat.create().setBoundingBox(expected).build();
		assertSame("bounding box not same", expected, fromBox.getBoundingBox());
		String formatted = fromBox.getString();
		assertNotNull("string not set", formatted);
		WmsBoundingBoxFormat fromString = WmsBoundingBoxFormat.create().setString(formatted).build();
		assertSame("string not set properly", formatted, fromString.getString());
		assertEquals("bounding box not equal", expected, fromString.getBoundingBox());

	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.BoundingBox.BaseBuilder#boundedBy(com.aawhere.measure.GeoCoordinate, javax.measure.quantity.Length)}
	 * .
	 */
	@Test
	public void testBoundedBy() throws Exception {
		GeoCoordinate expectedNorthEast = GeoCoordinate.create().setLatitude(1d).setLongitude(1d).build();
		GeoCoordinate expectedSouthWest = GeoCoordinate.create().setLatitude(-1d).setLongitude(-1d).build();
		GeoCoordinate center = GeoCoordinate.create().setLatitude(0d).setLongitude(0d).build();
		// distance of 1 degree in latitude.
		Length radius = MeasurementUtil.createLengthInMeters(111111d);
		assertContainingCircle(expectedNorthEast, expectedSouthWest, center, radius);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.BoundingBox.BaseBuilder#boundedBy(com.aawhere.measure.GeoCoordinate, javax.measure.quantity.Length)}
	 * .
	 */
	@Test
	public void testBoundedByAtNorthernExtream() throws Exception {
		GeoCoordinate expectedNorthEast = GeoCoordinate.create().setLatitude(81d).setLongitude(5.729d).build();
		GeoCoordinate expectedSouthWest = GeoCoordinate.create().setLatitude(79d).setLongitude(-5.729d).build();
		GeoCoordinate center = GeoCoordinate.create().setLatitude(80d).setLongitude(0d).build();
		// distance of 1 degree in latitude.
		Length radius = MeasurementUtil.createLengthInMeters(111111d);
		assertContainingCircle(expectedNorthEast, expectedSouthWest, center, radius);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.BoundingBox.BaseBuilder#boundedBy(com.aawhere.measure.GeoCoordinate, javax.measure.quantity.Length)}
	 * .
	 */
	@Test
	public void testBoundedByAt180Meridian() throws Exception {
		GeoCoordinate expectedNorthEast = GeoCoordinate.create().setLatitude(1d).setLongitude(-179d).build();
		GeoCoordinate expectedSouthWest = GeoCoordinate.create().setLatitude(-1d).setLongitude(179d).build();
		GeoCoordinate center = GeoCoordinate.create().setLatitude(0d).setLongitude(180d).build();
		// distance of 1 degree in latitude.
		Length radius = MeasurementUtil.createLengthInMeters(111111d);
		assertContainingCircle(expectedNorthEast, expectedSouthWest, center, radius);
	}

	private void assertContainingCircle(GeoCoordinate expectedNorthEast, GeoCoordinate expectedSouthWest,
			GeoCoordinate center, Length radius) {
		double expectedEast = expectedNorthEast.getLongitude().getInDecimalDegrees();
		double expectedNorth = expectedNorthEast.getLatitude().getInDecimalDegrees();
		;
		double expectedSouth = expectedSouthWest.getLatitude().getInDecimalDegrees();
		double expectedWest = expectedSouthWest.getLongitude().getInDecimalDegrees();
		// distance of 1 degree in latitude.
		BoundingBox box = BoundingBoxUtil.boundedBy(center, radius);
		assertEquals("north", expectedNorth, box.getNorth().getInDecimalDegrees(), CONTAINING_CIRCLE_TOLORANCE);
		assertEquals("east", expectedEast, box.getEast().getInDecimalDegrees(), CONTAINING_CIRCLE_TOLORANCE);
		assertEquals("south", expectedSouth, box.getSouth().getInDecimalDegrees(), CONTAINING_CIRCLE_TOLORANCE);
		assertEquals("west", expectedWest, box.getWest().getInDecimalDegrees(), CONTAINING_CIRCLE_TOLORANCE);
	}

}
