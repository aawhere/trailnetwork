/**
 *
 */
package com.aawhere.measure.calc;

import static org.junit.Assert.*;

import javax.measure.quantity.Length;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.Unit;

import org.junit.Test;

import com.aawhere.measure.MeasurementQuantityFactory;

/**
 * Tests the geometric calculator {@link PythagoreanTheorum}.
 * 
 * @author Aaron Roller on Apr 6, 2011
 * 
 */
public class PythagoreanTheorumUnitTest {

	private static final Length A;
	private static final Length B;
	private static final Length C;

	static {

		QuantityFactory<Length> factory = MeasurementQuantityFactory.getInstance(Length.class);

		Unit<Length> unit = MetricSystem.METRE;
		A = factory.create(3, unit);
		B = factory.create(4, unit);
		C = factory.create(5, unit);

	}

	@Test
	public void testHypotenuseNotProvided() {
		Length c = new PythagoreanTheorum.Builder().setA(A).setB(B).build().getC();
		assertEquals(C, c);
	}

	@Test(expected = NullPointerException.class)
	public void testANotProvided() {
		new PythagoreanTheorum.Builder().setB(B).build();
	}

	@Test(expected = NullPointerException.class)
	public void testBNotProvided() {
		new PythagoreanTheorum.Builder().setA(A).build();
	}

	@Test
	public void testCandAProvided() {
		Length actualB = PythagoreanTheorum.create().setC(C).setA(A).build().getB();
		assertEquals(B, actualB);
	}

	@Test
	public void testCandBProvided() {
		Length actualA = PythagoreanTheorum.create().setC(C).setB(B).build().getA();
		assertEquals(A, actualA);
	}

}
