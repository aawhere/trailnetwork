/**
 *
 */
package com.aawhere.measure;

import static org.junit.Assert.*;

import javax.measure.quantity.Length;
import javax.measure.quantity.Velocity;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringAdapterFactory;
import com.aawhere.lang.string.StringAdapterUnitTest;
import com.aawhere.lang.string.StringFormatException;
import com.aawhere.test.TestUtil;

/**
 * Tests the basic String adaptation for {@link QuantityStringAdapter}.
 * 
 * @author aroller
 * 
 */
public class QuantityStringAdapterUnitTest {

	/**
	 *
	 */
	private static final Class<Length> TYPE = Length.class;
	public static final String LENGTH = "12345 m";
	private QuantityStringAdapter<Length> adapter;

	@Before
	public void setUp() {
		adapter = new QuantityStringAdapter.Builder<Length>().build();
	}

	@Test
	public void testLength() throws StringFormatException {

		String expected = LENGTH;

		String actual = assertLength(expected);
		assertEquals(expected, actual);
	}

	/**
	 * @param expected
	 * @return
	 * @throws StringFormatException
	 */
	private String assertLength(String expected) throws StringFormatException {
		Length length = adapter.unmarshal(expected, TYPE);
		String actual = adapter.marshal(length);
		return actual;
	}

	@Test
	public void testInvalidNumber() {
		QuantityStringAdapter<Length> adapter = new QuantityStringAdapter.Builder<Length>().build();
		try {
			adapter.unmarshal("knskne m", TYPE);
			fail("this shouldn't have worked");
		} catch (StringFormatException e) {
			// look for an explanation about the number
			TestUtil.assertContains(e.getMessage(), "number");
		}
	}

	@Test(expected = InvalidChoiceException.class)
	public void testInvalidUnit() throws StringFormatException {
		adapter.unmarshal(LENGTH + "bogus", TYPE);
	}

	@Test
	public void testLengthFromFactory() throws BaseException {
		StringAdapterFactory factory = new StringAdapterFactory();
		factory.register(new QuantityStringAdapter.Builder<Length>().build());
		StringAdapterUnitTest.assertAdapter(factory, adapter.unmarshal(LENGTH, TYPE));
	}

	/** Ensures a single registration will satisfy providing adapters of multiple subtypes. */
	@Test
	public void testQuantityRegistration() {
		StringAdapterFactory factory = new StringAdapterFactory();
		QuantityStringAdapter<?> quantityAdapter = (QuantityStringAdapter<?>) new QuantityStringAdapter.Builder()
				.build();
		factory.register(quantityAdapter);
		StringAdapter<Length> lengthAdapter = factory.getAdapter(Length.class);
		assertSame(quantityAdapter, lengthAdapter);

		StringAdapter<Velocity> velocityAdapter = factory.getAdapter(Velocity.class);
		assertSame(quantityAdapter, velocityAdapter);
	}
}
