/**
 * 
 */
package com.aawhere.measure;

import static com.aawhere.lang.number.NumberUtilsExtended.*;
import static org.junit.Assert.*;

import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.Unit;

import org.junit.Test;

import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class CountUnitTest {

	@Test
	public void testValue() {
		Integer expected = TestUtil.generateRandomInt();
		final Unit<Count> unit = ExtraUnits.COUNT;
		Count count = QuantityFactory.getInstance(Count.class).create(expected, unit);
		assertEquals("value is different", expected, count.getValue());
		assertEquals("value by unit is different", expected, I(count.doubleValue(unit)));
		TestUtil.assertInstanceOf("should be our implementation", AbstractQuantity.class, count);
	}

}
