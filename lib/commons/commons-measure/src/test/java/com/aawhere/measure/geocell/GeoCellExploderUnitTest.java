/**
 * 
 */
package com.aawhere.measure.geocell;

import static com.aawhere.measure.geocell.GeoCellTestUtil.*;
import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.measure.geocell.GeoCellsExploder.Builder;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * Stateful unit tests that setup, each tests modifies the setup and {@link #test()} executes and
 * compares to the expected results.
 * 
 * @author aroller
 * 
 */
public class GeoCellExploderUnitTest {

	private Builder explode;
	private GeoCells.Builder geoCellBuilder = GeoCells.create();
	private Set<GeoCell> expected = Sets.newHashSet();
	private Resolution expectedMaxResolution;
	private Boolean limitExceededExpected = false;

	@Before
	public void setUp() {
		this.explode = GeoCellsExploder.explode();
	}

	@After
	public void test() {
		assertNotNull("expectedMaxResolution", this.expectedMaxResolution);
		assertNotNull("expected", expected);
		GeoCells input;
		try {
			input = geoCellBuilder.build();
			TestUtil.assertSize(Range.atLeast(1), input.getAll());
			GeoCells exploded = explode.these(input).build().exploded();
			assertEquals("max resolution", expectedMaxResolution, exploded.getMaxResolution());
			assertEquals("cells", expected, exploded.getAll());
		} catch (InvalidArgumentException e) {
			assertTrue("limit exceeded", this.limitExceededExpected);
		}

	}

	@Test
	public void testAtResolution() {
		GeoCell cell = AB;
		this.expectedMaxResolution = cell.getResolution();
		explode.to(expectedMaxResolution);
		this.expected = Sets.newHashSet(cell);
		geoCellBuilder.add(cell);

	}

	@Test
	public void testBelowResolution() {

		explode.to(AB.getResolution());
		GeoCell cell = ABC;
		this.expectedMaxResolution = cell.getResolution();
		this.expected = Sets.newHashSet(cell);
		geoCellBuilder.add(cell);
	}

	@Test
	public void testOneResolution() {
		GeoCell cell = A;
		this.expectedMaxResolution = cell.getResolution().higher();
		explode.to(this.expectedMaxResolution);
		this.expected = GeoCellUtil.explode(cell);
		geoCellBuilder.add(cell);

	}

	@Test
	public void testTwoResolutions() {
		GeoCell cell = A;
		this.expectedMaxResolution = cell.getResolution().higher().higher();
		explode.to(this.expectedMaxResolution);
		HashSet<GeoCell> children = Sets.newHashSet(GeoCellUtil.explode(cell));

		expecting(children);

		geoCellBuilder.add(cell);

	}

	/**
	 * Give parents that expecting children.
	 * 
	 * @param parents
	 */
	private void expecting(Set<GeoCell> parents) {
		for (GeoCell geoCell : parents) {
			this.expected.addAll(GeoCellUtil.explode(geoCell));
		}
	}

	@Test
	public void testMixedResolutions() {
		// AB and B are given targeting a resolution below. both should explode to THREE
		this.expectedMaxResolution = Resolution.THREE;
		this.explode.to(expectedMaxResolution);
		GeoCell higher = AB;
		expecting(Sets.newHashSet(higher));
		GeoCell lower = B;
		expecting(GeoCellUtil.explode(lower));
		this.geoCellBuilder.add(higher, lower);
	}

	@Test
	public void testDefaultToResolution() {
		this.expectedMaxResolution = Resolution.TWO;
		GeoCell given = A;
		this.expected.addAll(GeoCellUtil.explode(given));
		this.geoCellBuilder.add(given);
	}

	@Test
	public void testLimitExceeded() {
		geoCellBuilder.add(A);
		explode.to(Resolution.THREE);
		this.expectedMaxResolution = Resolution.THREE;
		explode.limitTo(GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL);
		this.limitExceededExpected = true;
	}
}

/**
 * a non recursive implementation written, but never used put here in case if current recursion is a
 * problem
 */
// public static Set<GeoCell> explode(GeoCells cells, Resolution targetResolution) {
// HashSet<GeoCell> targetResolutionCells = new HashSet<GeoCell>(cells.size());
//
// LinkedList<GeoCell> lowResolutionQueue = Lists.newLinkedList(cells);
// while (!lowResolutionQueue.isEmpty()) {
// GeoCell geoCell = lowResolutionQueue.removeFirst();
// if (ComparatorResult.isLessThan(geoCell.getResolution().compareTo(targetResolution))) {
//
// Set<GeoCell> exploded = explode(geoCell);
// // if the exploded cells are of the target resolution then put them in the happy set
// if (geoCell.getResolution().higher().equals(targetResolution)) {
// targetResolutionCells.addAll(targetResolutionCells);
// } else {
// // add these back to the queue and explode them again
// lowResolutionQueue.addAll(exploded);
// }
// } else {
// // it's already good enough
// targetResolutionCells.add(geoCell);
// }
// }
// return targetResolutionCells;
// }
