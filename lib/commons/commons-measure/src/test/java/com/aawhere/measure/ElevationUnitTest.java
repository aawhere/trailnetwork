/**
 *
 */
package com.aawhere.measure;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;
import javax.measure.unit.UnitConverter;

import org.junit.Test;

import com.aawhere.measure.unit.ExtraUnits;

/**
 * @author roller
 * 
 */
public class ElevationUnitTest
		extends ExtraQuantityUnitTest<Elevation> {

	static Unit<Elevation> FOOT = ExtraUnits.FOOT_MSL;
	static Unit<Elevation> METRE = ExtraUnits.METER_MSL;

	@Test
	public void testLengthEquality() {
		final int value = 3325;
		Elevation expected = MeasurementUtil.createElevationInMeters(value);
		Length actual = MeasurementUtil.createLengthInMeters(value);
		assertEquals("even though they are different measurements, their units are compatible", expected, actual);

	}

	@Test
	public void testRandom() {
		Elevation randomQuantity = generateRandomQuantity();
		assertNotNull(randomQuantity.getUnit());
		assertNotNull(randomQuantity.getValue());
		assertEquals(getUnit(), randomQuantity.getUnit());
	}

	@Test
	public void testUnitConversion() {

		UnitConverter converterTo = METRE.getConverterTo(FOOT);
		assertNotNull(converterTo);
		Double inMeters = 3.0;
		Double inFeet = MetricSystem.METRE.getConverterTo(USCustomarySystem.FOOT).convert(inMeters);
		Double inFeetMsl = converterTo.convert(inMeters);
		assertEquals(inFeet, inFeetMsl);
	}

	@Test
	public void testElevationEvenMore() {

		double throwElevationMetersValue = 1700;
		double throwElevationKmValue = throwElevationMetersValue / 1000;
		double throwElevationFeetValue = 5577.427821;
		Elevation throwElevationMeters = MeasurementQuantityFactory.getInstance(Elevation.class)
				.create(throwElevationMetersValue, METRE);
		assertNotNull(throwElevationMeters);
		assertEquals(throwElevationMetersValue, throwElevationMeters.doubleValue(METRE), REASONABLE_PRECISION);

		UnitConverter converter = METRE.getConverterTo(MetricSystem.KILO(METRE));
		assertEquals(	throwElevationKmValue,
						converter.convert(throwElevationMeters.getValue().doubleValue()),
						REASONABLE_PRECISION);
		converter = METRE.getConverterToAny(FOOT);
		assertEquals(	throwElevationFeetValue,
						converter.convert(throwElevationMeters.getValue().doubleValue()),
						REASONABLE_PRECISION);
	}

	@Test
	public void shouldSerialize() throws IOException, ClassNotFoundException {
		Elevation randomQuantity = generateRandomQuantity();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(randomQuantity);
		byte[] ba = baos.toByteArray();
		// Should not throw an exception.
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(ba));
		Elevation deserialized = (Elevation) ois.readObject();
		assertEquals(randomQuantity, deserialized);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.ExtraQuantityUnitTest#getQuantityClass()
	 */
	@Override
	protected Class<Elevation> getQuantityClass() {
		return Elevation.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.ExtraQuantityUnitTest#getNormalizedUnit()
	 */
	@Override
	protected Unit<Elevation> getNormalizedUnit() {
		return METRE;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.ExtraQuantityUnitTest#getUnit()
	 */
	@Override
	protected Unit<Elevation> getUnit() {
		return METRE;
	}
}
