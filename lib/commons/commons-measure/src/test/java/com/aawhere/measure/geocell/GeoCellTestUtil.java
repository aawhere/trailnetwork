/**
 *
 */
package com.aawhere.measure.geocell;

import static com.aawhere.measure.geocell.GeoCellUtil.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxes;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.test.TestUtil;

/**
 * @author brian
 * 
 */
public class GeoCellTestUtil {

	public static final GeoCell A = new GeoCell("a");
	public static final GeoCell AB = new GeoCell("ab");
	public static final GeoCell ABC = new GeoCell("abc");
	public static final GeoCell ABCD = new GeoCell("abcd");
	public static final GeoCell ABCDE = new GeoCell("abcde");
	public static final GeoCell ABCDEF = new GeoCell("abcdef");
	public static final GeoCells A_THRU_F_WITH_PARENTS = new GeoCells.Builder().add(A).add(AB).add(ABC).add(ABCD)
			.add(ABCDE).add(ABCDEF).build();
	public static final GeoCell B = new GeoCell("b");
	public static final GeoCell C = new GeoCell("c");
	public static final GeoCell D = new GeoCell("d");
	public static final GeoCell EIGHT = new GeoCell("8");
	public static final GeoCell NINE = new GeoCell("9");
	public static final BoundingBox BOUNDS_OF_A = BoundingBox.valueOf("45.0001,-179.9999|89.9999,-90.0001");
	/**
	 * The bounds is slightly smaller than the cells to ensure boundaries aren't crossed resulting
	 * only in 9.
	 */
	public static final BoundingBox BOUNDS_OF_9 = BoundingBox.valueOf("0.0001,-89.9999|44.9999,-0.0001");

	/**
	 * see TN-746 to understand the view must focus on the center rather than all encompassing.
	 */
	public static final String MARIN_VIEW_BOUNDS = "37.783270595574734,-122.7989164166504|37.99840005186738,-122.31105478334962";
	/**
	 * @see #MARIN_VIEW_BOUNDS
	 */
	public static final GeoCells MARIN_VIEW_CELLS = GeoCells
			.valueOf("8e62da, 8e62db, 8e62de, 8e62e7, 8e62ed, 8e62f, 8e6850, 8e6851, 8e6854");
	/**
	 * If you are gonna get one thing right...it's that the center cell is included AND that it is
	 * minimized.
	 */
	public static final GeoCell MARIN_VIEW_CENTER_CELL = new GeoCell("8e62f");

	/**
	 * Gets around the protected constructor.
	 */
	public static GeoCell create(String location) {
		return new GeoCell(location);
	}

	/**
	 * Given a resolution this will createa completely random geo cell at that resolution.
	 * 
	 * @param resolution
	 * @return
	 */
	public static GeoCell createRandomGeoCell(Resolution resolution) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < resolution.resolution; i++) {
			Integer index = TestUtil.generateRandomInt(0, GeoCellUtil.ALPHABET.length() - 1);
			builder.append(ALPHABET.charAt(index));
		}
		return new GeoCell(builder.toString());
	}

	public static Set<GeoCell> getSampleGeoCells() {
		Set<GeoCell> geoCells = new HashSet<GeoCell>();
		geoCells.add(GeoCellTestUtil.create("11110"));
		geoCells.add(GeoCellTestUtil.create("11111"));
		geoCells.add(GeoCellTestUtil.create("11114"));
		return geoCells;
	}

	public static Set<GeoCell> getSampleGeoCellsAcrossFaultLine() {
		Set<GeoCell> geoCells = new HashSet<GeoCell>();
		geoCells.add(GeoCellTestUtil.create("11111"));
		geoCells.add(GeoCellTestUtil.create("11114"));
		geoCells.add(GeoCellTestUtil.create("11140"));
		return geoCells;
	}

	/**
	 * This will ensure the given cellsWithParents contains all the parents for the children in the
	 * given cells.
	 * 
	 * @param cells
	 * @param cellsWithParents
	 */
	public static void assertParentsIncluded(Set<GeoCell> cells, Set<GeoCell> cellsWithParents) {
		for (GeoCell geoCell : cells) {
			while (GeoCellUtil.hasParentGeoCell(geoCell)) {
				GeoCell parentGeoCell = GeoCellUtil.getParentGeoCell(geoCell);
				TestUtil.assertContains("parent missing", parentGeoCell, cellsWithParents);
				geoCell = parentGeoCell;
			}
		}
	}

	/**
	 * @return
	 */
	public static BoundingBoxes childrenBoundingBoxes() {
		final GeoCell parent = D;
		return childrenBoundingBoxes(parent);

	}

	/**
	 * @param parent
	 * @return
	 */
	private static BoundingBoxes childrenBoundingBoxes(final GeoCell parent) {
		Set<GeoCell> exploded = explode(parent);
		ArrayList<GeoCellBoundingBox> boxes = new ArrayList<>();
		for (GeoCell geoCell : exploded) {
			boxes.add(GeoCellBoundingBox.createGeoCell().setGeoCells(geoCell).build());
		}

		return new BoundingBoxes.Builder().setAll(boxes).build();
	}

	/**
	 * This will return a {@link GeoCells} of the count given completely unrelated {@link GeoCell}
	 * values all at the given resolution.
	 * 
	 * @param i
	 * @return
	 */
	public static GeoCells createRandomGeoCells(Integer count, Resolution resolution) {
		GeoCells.Builder builder = GeoCells.create().setMaxResolution(resolution);
		for (int i = 0; i < count; i++) {
			builder.add(createRandomGeoCell(resolution));
		}
		return builder.build();
	}

	/**
	 * Given some geocells this will return approximately the same cells on the opposite side of the
	 * earth. Useful for testing negative searches.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static GeoCells opposite(final GeoCells geoCells) {
		BoundingBox boxFromGeoCells = GeoCellUtil.boundingBoxFromGeoCells(geoCells);
		GeoCoordinate center = boxFromGeoCells.getCenter();
		GeoCoordinate opposite = MeasureTestUtil.opposite(center);
		GeoCells oppositeCells = GeoCellUtil.geoCells(opposite, geoCells.getMaxResolution());
		return oppositeCells;
	}

}
