/**
 *
 */
package com.aawhere.measure.quantity;

import static org.junit.Assert.assertEquals;

import javax.measure.quantity.QuantityFactory;

import org.junit.Test;

import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.test.TestUtil;

/**
 * Deals with Ratios and Percentages.
 * 
 * @author roller
 * 
 */
public class RatioUnitTest {

	public static Ratio generateRandomRatio() {
		final double min = 0;
		final double max = 1;
		Double value = TestUtil.generateRandomDouble(min, max);
		Ratio result = QuantityFactory.getInstance(Ratio.class).create(value, ExtraUnits.RATIO);
		TestUtil.assertDoubleEquals(value, result.doubleValue(ExtraUnits.RATIO));
		return result;
	}

	@Test
	public void testRatio() {
		generateRandomRatio();
	}

	@Test
	public void testEquals() {
		Ratio first = generateRandomRatio();
		Ratio second = MeasurementQuantityFactory.getInstance(Ratio.class).create(first.getValue(), first.getUnit());
		assertEquals(first, second);
	}
	// TODO:Test percentages and formatting?
}
