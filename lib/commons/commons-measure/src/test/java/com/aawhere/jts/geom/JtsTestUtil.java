/**
 *
 */
package com.aawhere.jts.geom;

import java.util.Collection;

import javax.measure.quantity.Length;

import com.aawhere.measure.MeasurementUtil;

import com.google.common.collect.Sets;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.WKTReader;

/**
 * Utilities to assist in writing unit tests using JTS components.
 * 
 * @author brian
 * 
 */
public class JtsTestUtil {

	private static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();
	private static final WKTReader WKT_READER = new WKTReader(GEOMETRY_FACTORY);

	public static final Length BUFFER_DISTANCE = MeasurementUtil.createLengthInMeters(0.00001d);

	static final int NEARBY_VALUE = 1;
	public static final int CENTER_VALUE = 0;
	static final double HALF_NEAR_CENTER = 0.5;
	static final int OPPOSITE_NEARBY_VALUE = -1;
	static final int FAR_VALUE = 2;

	public static final Coordinate LEFT = new Coordinate(OPPOSITE_NEARBY_VALUE, CENTER_VALUE);
	public static final Coordinate RIGHT = new Coordinate(NEARBY_VALUE, CENTER_VALUE);
	public static final Coordinate TOP = new Coordinate(CENTER_VALUE, NEARBY_VALUE);
	public static final Coordinate JUST_ABOVE_CENTER = new Coordinate(CENTER_VALUE, HALF_NEAR_CENTER);
	public static final Coordinate CENTER = new Coordinate(CENTER_VALUE, CENTER_VALUE);
	public static final Coordinate JUST_BELOW_CENTER = new Coordinate(CENTER_VALUE, -HALF_NEAR_CENTER);
	public static final Coordinate BOTTOM = new Coordinate(CENTER_VALUE, OPPOSITE_NEARBY_VALUE);
	public static final Coordinate TOP_LEFT = new Coordinate(OPPOSITE_NEARBY_VALUE, NEARBY_VALUE);
	public static final Coordinate TOP_RIGHT = new Coordinate(NEARBY_VALUE, NEARBY_VALUE);
	public static final Coordinate BOTTOM_LEFT = new Coordinate(OPPOSITE_NEARBY_VALUE, OPPOSITE_NEARBY_VALUE);
	public static final Coordinate BOTTOM_RIGHT = new Coordinate(NEARBY_VALUE, OPPOSITE_NEARBY_VALUE);
	public static final Coordinate ABOVE_TOP = new Coordinate(CENTER_VALUE, FAR_VALUE);

	/** One line length of 6. Extra point thrown in for fun. */
	public static final double LINE_LENGTH = 2.0;
	public static final double LOOP_LENGTH = 6.0;
	public static final double VERTICAL_CENTER_LINE_EXTENDED_LENGTH = 3.0;
	public static final double VERTICAL_CENTER_MULTI_LINE_EXTENDED_LENGTH = 5.0;

	public static LineString getHorizontalCenterLine() {

		Coordinate[] coordinates = { LEFT, RIGHT };
		return getLineForCoordinates(coordinates);
	}

	public static LineString getVerticalCenterLine() {
		Coordinate[] coordinates = { BOTTOM, TOP };
		return getLineForCoordinates(coordinates);
	}

	public static LineString getL() {
		return getLineForCoordinates(new Coordinate[] { TOP, CENTER, RIGHT });
	}

	public static LineString getVerticalEndLine() {
		return getLineForCoordinates(new Coordinate[] { TOP_RIGHT, BOTTOM_RIGHT });
	}

	public static MultiLineString getVerticalCenterLineExtended() {
		return JtsUtil
				.multiLineString(GEOMETRY_FACTORY, getVerticalCenterLine(), getLineForCoordinates(TOP, ABOVE_TOP));
	}

	public static LineString getLeftVerticalLine() {
		return getLineForCoordinates(TOP_LEFT, BOTTOM_LEFT);
	}

	public static LineString getRightVertivalLine() {
		Coordinate[] coordinates = { TOP_RIGHT, BOTTOM_RIGHT };
		return getLineForCoordinates(coordinates);
	}

	public static MultiLineString getVerticalLineMissingCenter() {
		return JtsUtil.multiLineString(	GEOMETRY_FACTORY,
										getLineForCoordinates(BOTTOM, JUST_BELOW_CENTER),
										getLineForCoordinates(JUST_ABOVE_CENTER, TOP));
	}

	/**
	 * @param coordinates
	 * @return
	 */
	private static LineString getLineForCoordinates(Coordinate... coordinates) {
		return GEOMETRY_FACTORY.createLineString(coordinates);
	}

	/**
	 * @return
	 */
	public static GeometryFactory getGeometryFactory() {
		return GEOMETRY_FACTORY;
	}

	public static LineString getShortVerticalLine() {
		return getLineForCoordinates(BOTTOM, CENTER);
	}

	/**
	 * @return
	 */
	public static MultiLineString getOutAndBack() {
		return JtsUtil.multiLineString(GEOMETRY_FACTORY, getShortVerticalLine(), (LineString) getShortVerticalLine()
				.reverse());
	}

	/**
	 * @return
	 */
	public static MultiLineString getOutAndBackExtended() {
		return JtsUtil.multiLineString(GEOMETRY_FACTORY, getVerticalCenterLine(), (LineString) getVerticalCenterLine()
				.reverse());
	}

	public static LineString getRightLoopStartingUp() {
		return getLineForCoordinates(CENTER, TOP, TOP_RIGHT, RIGHT, BOTTOM_RIGHT, BOTTOM, CENTER);
	}

	/**
	 * @same as {@link #getRightLoopStartingUp()}, but repeats for a second
	 * 
	 * @return
	 */
	public static MultiLineString getRightLoopStartingUpDouble() {
		return JtsUtil.multiLineString(GEOMETRY_FACTORY, getRightLoopStartingUp(), getRightLoopStartingUp());
	}

	public static LineString getLeftLoopStartingUp() {
		return getLineForCoordinates(CENTER, TOP, TOP_LEFT, LEFT, BOTTOM_LEFT, BOTTOM, CENTER);
	}

	/** creates a polygon matching {@link #getLeftLoopStartingUp()} */
	public static Polygon createLeftLoopPolygon() {
		return getGeometryFactory().createPolygon(getLeftLoopStartingUp().getCoordinates());
	}

	/**
	 * creates a polygon from {@link #getRightLoopStartingUp()}
	 * 
	 * @return
	 */
	public static Polygon createRightLoopPolygon() {
		return getGeometryFactory().createPolygon(getRightLoopStartingUp().getCoordinates());
	}

	/**
	 * creates a figure 8 from {@link #createLeftLoopPolygon()} and
	 * {@link #createRightLoopPolygon()}.
	 * 
	 * @return
	 */
	public static MultiPolygon createMultiPolygon() {
		return getGeometryFactory().createMultiPolygon(new Polygon[] { createLeftLoopPolygon(),
				createRightLoopPolygon() });
	}

	public static Collection<Coordinate> createCoordinateCollection() {
		return Sets.newHashSet(LEFT, RIGHT, CENTER, TOP, BOTTOM);

	}
}
