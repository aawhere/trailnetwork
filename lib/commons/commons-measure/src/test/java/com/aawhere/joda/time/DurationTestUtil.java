/**
 * 
 */
package com.aawhere.joda.time;

import static com.aawhere.test.TestUtil.*;

import org.joda.time.Duration;
import org.joda.time.ReadableDuration;

import com.aawhere.collections.RangeExtended;
import com.aawhere.test.TestUtil;

import com.google.common.base.Function;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class DurationTestUtil {

	/** Generates a random duration */
	public static Duration duration() {
		return new Duration(l());
	}

	public static Duration duration(Range<ReadableDuration> range) {
		Range<ReadableDuration> generalRange = (Range<ReadableDuration>) range;
		final Function<ReadableDuration, Long> millisFunction = DurationUtil.millisFunction();
		final Range<Long> millisRange = RangeExtended.compose(generalRange, millisFunction);
		Long millis = TestUtil.l(millisRange);
		return new Duration(millis);
	}

}
