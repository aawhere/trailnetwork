/**
 *
 */
package com.aawhere.jts.geom;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;

/**
 * @author Brian Chapman
 * 
 */
public class JtsUtilUnitTest {

	/**
	 * Test method for
	 * {@link com.aawhere.jts.geom.JtsUtil#multiLineStringFrom(com.vividsolutions.jts.geom.LineString, com.vividsolutions.jts.geom.GeometryFactory)}
	 * .
	 */
	@Test
	@Ignore("not implemented")
	public void testMultiLineStringFromLineStringGeometryFactory() {
		// TODO: implement me!
	}

	/**
	 * Test method for
	 * {@link com.aawhere.jts.geom.JtsUtil#multiLineStringFrom(java.util.List, com.vividsolutions.jts.geom.GeometryFactory)}
	 * .
	 */
	@Test
	@Ignore("not implemented")
	public void testMultiLineStringFromListOfQextendsCoordinateSequenceGeometryFactory() {
		// TODO: implement me!
	}

	/**
	 * Test method for
	 * {@link com.aawhere.jts.geom.JtsUtil#multiLineStringFrom(com.vividsolutions.jts.geom.Coordinate[], com.vividsolutions.jts.geom.GeometryFactory)}
	 * .
	 */
	@Test
	@Ignore("not implemented")
	public void testMultiLineStringFromCoordinateArrayGeometryFactory() {
		// TODO: implement me!
	}

	/**
	 * Test method for
	 * {@link com.aawhere.jts.geom.JtsUtil#length(com.vividsolutions.jts.geom.Geometry)}.
	 */
	@Test
	public void testLength() {
		Geometry leftVerticalLine = JtsTestUtil.getLeftVerticalLine();
		double distance = JtsUtil.length(leftVerticalLine);
		TestUtil.assertDoubleEquals(2, distance);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.jts.geom.JtsUtil#shortestDistanceBetween(com.vividsolutions.jts.geom.Geometry, com.vividsolutions.jts.geom.Geometry)}
	 * .
	 */
	@Test
	public void testShortestDistanceBetween() {
		Geometry leftVerticalLine = JtsTestUtil.getLeftVerticalLine();
		Geometry rightVerticalLine = JtsTestUtil.getRightVertivalLine();
		double distance = JtsUtil.shortestDistanceBetween(leftVerticalLine, rightVerticalLine);
		TestUtil.assertDoubleEquals(2, distance);
	}

	@Test
	public void testLineStringSplitSizeOfOriginal() {
		LineString line = JtsTestUtil.getVerticalCenterLine();
		Object expectedNumberOfLineStrings = 1;
		int maxLength = line.getNumPoints();
		assertSplitLineString(line, expectedNumberOfLineStrings, maxLength);
	}

	@Test
	public void testLineStringSplitIntoTwoRemainderOfOne() {
		LineString line = JtsTestUtil.getLeftLoopStartingUp();
		// special situation where a remainder of one causes appending to previous
		assertSplitLineString(line, 1, line.getNumPoints() - 1);
	}

	@Test
	public void testLineStringSplitIntoTwoRemainderOfTwo() {
		LineString line = JtsTestUtil.getLeftLoopStartingUp();
		// special situation where a remainder of one causes appending to previous
		assertSplitLineString(line, 2, line.getNumPoints() - 2);
	}

	@Test
	public void testLineStringSplitIntoTwoRemainderTwo() {
		LineString line = JtsTestUtil.getLeftLoopStartingUp();
		assertSplitLineString(line, 2, line.getNumPoints() - 2);
	}

	@Test
	public void testToMultiPoint() {
		Collection<Coordinate> coords = JtsTestUtil.createCoordinateCollection();
		MultiPoint mp = JtsUtil.toMultiPoint(coords);
		assertNotNull(mp);
	}

	/**
	 * @param line
	 * @param expectedNumberOfLineStrings
	 * @param maxLength
	 */
	private void assertSplitLineString(LineString line, Object expectedNumberOfLineStrings, int maxLength) {
		MultiLineString multiLineString = JtsUtil.split(line, maxLength);
		assertEquals(expectedNumberOfLineStrings, multiLineString.getNumGeometries());
	}

}
