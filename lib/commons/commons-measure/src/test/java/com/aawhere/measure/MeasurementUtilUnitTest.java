package com.aawhere.measure;

import static com.aawhere.measure.MeasurementUtil.*;
import static com.aawhere.measure.unit.ExtraUnits.*;
import static com.aawhere.test.TestUtil.*;
import static javax.measure.unit.MetricSystem.*;
import static javax.measure.unit.USCustomarySystem.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import javax.measure.quantity.Acceleration;
import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.quantity.Mass;
import javax.measure.quantity.Quantity;
import javax.measure.quantity.QuantityFactory;
import javax.measure.quantity.Velocity;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;

import org.joda.time.Duration;
import org.junit.Test;

import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.test.TestUtil;

/**
 * Tests the static class methods of {@link MeasurementUtil}.
 * 
 * @author roller
 * 
 */
public class MeasurementUtilUnitTest {

	/**
	 * 
	 */
	private static final String METERS_PER_SECOND_SYMBOL = "mps";
	/**
	 * 
	 */
	private static final String METERS_OVER_SECOND_SYMBOL = "m/s";
	private final double QTY_FOOT = 40;
	private final double QTY_METER = 12.19200;
	private final double QTY_DEGREE_ANGLE = 90;
	private final double QTY_RADIAN = 1.57079633;

	@Test
	public void testGetFactory() {
		// if we don't initialize the custom factory first, they will be different.
		MeasurementQuantityFactory.initialize();
		QuantityFactory<Length> expected = MeasurementQuantityFactory.getInstance(Length.class);
		Length length = expected.create(3, MetricSystem.METRE);
		QuantityFactory<Length> actual = MeasurementUtil.getFactory(length);
		assertEquals(expected, actual);
	}

	/**
	 * @see MeasurementUtil#quantityOrNull(Double, Unit, Class)
	 */
	@Test
	public void testQuantityReturningQuantity() {
		Double value = TestUtil.generateRandomDouble();
		Unit<Length> unit = METRE;
		Class<Length> type = Length.class;
		Length actual = MeasurementUtil.quantityOrNull(value, unit, type);
		assertNotNull(actual);
		assertEquals(value, actual.getValue());
		assertEquals(unit, actual.getUnit());
	}

	/**
	 * Ensures a quantity created with an assumed normalized value returns the normalized value.
	 * 
	 */
	@Test
	public void testNormalizedQuantity() {
		double value = TestUtil.generateRandomDouble();
		TestUtil.assertDoubleEquals(value, normalizedQuantityOrNull(value, Length.class).doubleValue(METER));
	}

	/**
	 * @see MeasurementUtil#quantityOrNull(Double, Unit, Class)
	 */
	@Test
	public void testQuantityReturningNull() {
		Class<Length> type = null;
		Unit<Length> unit = null;
		Double value = null;
		assertNull(MeasurementUtil.quantityOrNull(value, unit, type));
	}

	@Test
	public void testCreateNormalRatio() {
		double value = generateRandomDouble(0, 1);
		Ratio ratio = createRatio(value);
		assertDoubleEquals(value, ratio.doubleValue(RATIO));
	}

	@Test
	public void testCreateZeroValue() {
		final int value = 0;
		assertDoubleEquals(value, createRatio(value).doubleValue(RATIO));
	}

	@Test
	public void testCreateOneValue() {
		final int value = 1;
		assertDoubleEquals(value, createRatio(value).doubleValue(RATIO));
	}

	@Test
	public void testCreateRatioTwoValuesNormal() {
		final int min = 0;
		double totalValue = generateRandomDouble(min, 100000);
		double partialValue = generateRandomDouble(min, totalValue);
		Ratio ratio = createRatio(partialValue, totalValue);
		double expectedValue = partialValue / totalValue;
		assertDoubleEquals(expectedValue, ratio.doubleValue(RATIO));
	}

	@Test
	public void testCreateRatioTwoValuesZeroTotalZeroPartial() {
		// this may not make much sense, but it seems ratios may have a total
		// zero now and again.
		Ratio ratio = createRatio(0, 0.0);
		assertEquals(RATIO_ZERO, ratio);
	}

	/** Division by zero never ends well. Tell the developer to don't bother. */
	@Test(expected = IllegalArgumentException.class)
	public void testCreateRatioTwoValuesZeroTotalPositivePartial() {
		// perhaps controversial since this doesn't make sense, but for now just
		// return a stupid value instead of error out
		createRatio(generateRandomDouble(), 0.0);
	}

	@Test
	public void testAcceleration() {

		Velocity initialVelocity = createVelocityInMps(0);
		int deltaInMps = 10;
		Velocity finalVelocity = createVelocityInMps(deltaInMps);
		Duration duration = new Duration(1000);
		Acceleration expected = createAccelerationInMpss(deltaInMps);
		assertEquals(expected, acceleration(initialVelocity, finalVelocity, duration));

	}

	@Test
	public void testNormalizedUnitFromUnit() {
		assertEquals(METRE, MeasurementUtil.getNormalizedUnit(MetricSystem.KILO(METRE)));
		assertEquals(METRE, MeasurementUtil.getNormalizedUnit(MetricSystem.MILLI(METRE)));
		assertEquals(METRE, MeasurementUtil.getNormalizedUnit(USCustomarySystem.MILE));
		assertEquals(METRES_PER_SECOND, MeasurementUtil.getNormalizedUnit(USCustomarySystem.MILES_PER_HOUR));
		assertEquals(METRES_PER_SECOND, MeasurementUtil.getNormalizedUnit(KILOMETERS_PER_HOUR));
		assertEquals(KILO(GRAM), getNormalizedUnit(POUND));
		assertEquals(KILO(GRAM), getNormalizedUnit(TON));
	}

	@Test
	public void testNormalizedUnitFromQuantityType() {
		assertEquals(METRE, MeasurementUtil.getNormalizedUnit(Length.class));
		assertEquals(KILO(GRAM), MeasurementUtil.getNormalizedUnit(Mass.class));
		assertEquals(KILO(GRAM), MeasurementUtil.getNormalizedUnit(Mass.class));
		assertEquals(MetricSystem.METRES_PER_SECOND, MeasurementUtil.getNormalizedUnit(Velocity.class));
	}

	@Test
	public void testGetNormalizedQuantityLength() {
		Length length = MeasureTestUtil.createLength(QTY_FOOT, FOOT);
		Quantity<Length> normalized = MeasurementUtil.getNormalizedQuantity(length);
		TestUtil.assertDoubleEquals("", QTY_METER, normalized.getValue().doubleValue());
	}

	@Test
	public void testGetNormalizedQuantityAngle() {
		Angle angle = MeasureTestUtil.createAngle(QTY_DEGREE_ANGLE, DEGREE_ANGLE);
		Quantity<Angle> normalized = MeasurementUtil.getNormalizedQuantity(angle);
		TestUtil.assertDoubleEquals("", QTY_RADIAN, normalized.getValue().doubleValue());
	}

	@Test
	public void testCreateAngleFromRadians() {
		Double radians = new Double(1.5);
		Angle angle = MeasurementUtil.createAngleInRadians(radians);
		TestUtil.assertDoubleEquals(radians, angle.doubleValue(MetricSystem.RADIAN));
	}

	@Test
	public void testCreateAngleFromDegrees() {
		Double degrees = new Double(1.5);
		Angle angle = MeasurementUtil.createAngleInDegrees(degrees);
		TestUtil.assertDoubleEquals(degrees, angle.doubleValue(USCustomarySystem.DEGREE_ANGLE));
	}

	/**
	 * Proves that two measurements may be equal regardless of their underlying unit. the
	 * {@link MeasureTestUtil#assertEquals(Quantity, Quantity)} method compares equality based on a
	 * reasonable precision based on the Measurement.
	 */
	@Test
	public void testAngleEquals() {
		Angle angleRadians = MeasurementUtil.createAngleInRadians(1.5);
		Angle angleDegrees = MeasurementQuantityFactory.getInstance(Angle.class).create(85.9436693,
																						USCustomarySystem.DEGREE_ANGLE);
		MeasureTestUtil.assertEquals(angleRadians, angleDegrees);

	}

	/**
	 * Proves that Quantity values equal regardless of the unit. Measurement equals method is
	 * virtually useless because it doesn't provide precision.
	 * 
	 * @see QuantityMath#equals(Quantity, Quantity)
	 */
	@Test
	public void testEqualityOfDifferentUnits() {
		Length km = MeasurementQuantityFactory.getInstance(Length.class).create(1, KILO(METRE));
		Length m = MeasurementQuantityFactory.getInstance(Length.class).create(1000, METRE);
		assertEquals(m, km);
	}

	/**
	 * @see MeasurementUtil#classFor(Quantity)
	 * 
	 */
	@Test
	public void testClassFor() {
		Class<Length> expected = Length.class;
		Length seed = MeasurementUtil.createLengthInMeters(TestUtil.generateRandomDouble());
		Class<? extends Quantity<?>> actual = MeasurementUtil.classFor(seed);
		assertEquals(expected, actual);

	}

	/**
	 * Tests classFor given Measurement.
	 * 
	 * @see MeasurementUtil#classFor(Quantity)
	 * @see #testClassFor()
	 */
	@Test
	public void testClassForMeasurement() {
		Length length = MeasurementUtil.createLengthInMeters(55);
		Elevation ele = MeasurementUtil.createElevation(length);
		Class<Elevation> elevationClass = MeasurementUtil.classFor(ele);
		assertEquals(Elevation.class, elevationClass);

	}

	@Test
	public void testMeasurementOrNullLatitude() {
		double value = TestUtil.generateRandomDouble();
		Latitude measurement = MeasurementUtil.quantityOrNull(null, DECIMAL_DEGREES_LATITUDE, Latitude.class);
		assertNull(measurement);
		measurement = MeasurementUtil.quantityOrNull(value, DECIMAL_DEGREES_LATITUDE, Latitude.class);
		assertNotNull(measurement);
	}

	@Test
	public void testMeasurementOrNullElevation() {
		double value = TestUtil.generateRandomDouble();
		Elevation measurement = MeasurementUtil.quantityOrNull(null, FOOT_MSL, Elevation.class);
		assertNull(measurement);
		measurement = MeasurementUtil.quantityOrNull(value, FOOT_MSL, Elevation.class);
		assertNotNull(measurement);
		assertEquals(measurement, MeasurementUtil.createElevation(measurement.getValue(), measurement.getUnit()));
	}

	@Test
	public void testGetSymbolForUnit() throws IOException {
		assertEquals("radian", getUnitSymbol(ExtraUnits.RADIAN_LATITUDE));
		assertEquals("W", getUnitSymbol(WATT));
		assertEquals("in", getUnitSymbol(INCH));
		assertEquals("mi", getUnitSymbol(MILE));
		assertEquals("mph", getUnitSymbol(MILES_PER_HOUR));
		assertEquals(METERS_OVER_SECOND_SYMBOL, getUnitSymbol(METRES_PER_SECOND));

	}

	@Test
	public void testGetUnitFromSymbol() {
		assertEquals(METRES_PER_SECOND, MeasurementUtil.getUnitFromSymbol(METERS_OVER_SECOND_SYMBOL));
		assertEquals(METRES_PER_SECOND, MeasurementUtil.getUnitFromSymbol(METERS_PER_SECOND_SYMBOL));
		assertEquals(METRES_PER_SECOND, MeasurementUtil.getUnitFromSymbol(METERS_PER_SECOND_SYMBOL));
	}

	@Test(expected = InvalidChoiceException.class)
	public void testGetUnitForSymbolNotFound() {
		assertNotNull("this should throw an exception", MeasurementUtil.getUnitFromSymbol(";unnknwel"));
	}

	/**
	 * There is no resource bundle, but it looks like SI units are the same for france and english.
	 */
	@Test
	public void testGetSymbolForUnitLocale() throws IOException {
		Locale fr = Locale.FRANCE;
		assertEquals("radian", getUnitSymbol(ExtraUnits.RADIAN_LATITUDE, fr));
		assertEquals("W", getUnitSymbol(WATT, fr));
		assertEquals("in", getUnitSymbol(INCH, fr));
		assertEquals("mi", getUnitSymbol(MILE, fr));
		assertEquals("mph", getUnitSymbol(MILES_PER_HOUR, fr));
		assertEquals(METERS_OVER_SECOND_SYMBOL, getUnitSymbol(METRES_PER_SECOND, fr));
	}

	/**
	 * Here we can learn about using Units.
	 * 
	 */
	@Test
	public void testUnitConversions() {
		assertEquals(METER, USCustomarySystem.MILE.toMetric());

	}

	@Test
	public void testToArrayCollection() {
		Collection<Latitude> coll = new ArrayList<Latitude>();
		coll.add(MeasureTestUtil.createRandomLatitude());
		coll.add(MeasureTestUtil.createRandomLatitude());

		Double[] results = MeasurementUtil.toArray(coll);
		assertNotNull(results);
		assertEquals(coll.size(), results.length);
	}

	@Test
	public void testToArrayIterable() {
		Collection<Latitude> coll = new ArrayList<Latitude>();
		coll.add(MeasureTestUtil.createRandomLatitude());
		coll.add(MeasureTestUtil.createRandomLatitude());

		Double[] results = MeasurementUtil.toArray((Iterable<Latitude>) coll);
		assertNotNull(results);
		assertEquals(coll.size(), results.length);
	}

	@Test
	public void testCreateLongitude() {
		Longitude result = MeasurementUtil.createLongitude(QTY_DEGREE_ANGLE);
		assertNotNull(result);
		TestUtil.assertDoubleEquals(QTY_DEGREE_ANGLE, result.getInDecimalDegrees());
	}

	@Test
	public void testCreateLatitude() {
		Latitude result = MeasurementUtil.createLatitude(QTY_DEGREE_ANGLE);
		assertNotNull(result);
		TestUtil.assertDoubleEquals(QTY_DEGREE_ANGLE, result.getInDecimalDegrees());
	}

	@Test
	public void testCreateLength() {
		Length length = MeasurementUtil.createLengthInMeters(QTY_METER);
		Elevation elv = MeasurementUtil.createElevation(length);
		Length result = MeasurementUtil.createLength(elv);
		assertNotNull(result);
		assertEquals(length, result);
	}

	@Test
	public void testCreateLengthQuantityMath() {
		Length length = MeasurementUtil.createLengthInMeters(QTY_METER);
		Elevation elv = MeasurementUtil.createElevation(length);
		QuantityMath<Elevation> qMath = QuantityMath.create(elv);
		Length result = MeasurementUtil.createLength(qMath);
		assertNotNull(result);
		assertEquals(length, result);
	}

	@Test
	public void testCreateElevationInMetersRounded() {
		Number value = 12.3;
		Double expected = 12d;
		Elevation elevation = MeasurementUtil.createElevationInMetersRounded(value);
		TestUtil.assertDoubleEquals(expected, elevation.doubleValue(METER_MSL));
	}

	@Test
	public void testRoundPercentToTens() {
		final double numerator = 55.1;
		final int expected = 60;
		assertRound(numerator, expected);
		assertRound(50, 50);
		assertRound(0, 0);
		assertRound(100, 100);
		assertRound(99, 100);
		assertRound(91, 100);
	}

	/**
	 * @param numerator
	 * @param expected
	 */
	private void assertRound(final double numerator, final int expected) {
		Ratio ratio = MeasurementUtil.ratio(numerator, 100);
		Integer percentToTens = MeasurementUtil.roundPercentToTens(ratio);
		assertEquals("round " + numerator, expected, percentToTens.intValue());
	}

	@Test
	public void testNormalizeAngle() {
		MeasureTestUtil.assertEquals("edge case of zero shoudl remain zero", NORTH, normalize(NORTH));
		MeasureTestUtil.assertEquals(	"edge case of 2pi should adjust to zero",
										NORTH,
										normalize(createAngleInRadians(2 * Math.PI)));
		MeasureTestUtil.assertEquals(	"ensures negatives go to positive",
										WEST,
										normalize(QuantityMath.create(QUARTER_CIRCLE).negate().getQuantity()));
		MeasureTestUtil.assertEquals(	"ensures adjustment will go far",
										NORTH,
										normalize(createAngleInRadians(16 * 2 * Math.PI)));

	}

	@Test
	public void testPerpendicularRight() {
		MeasureTestUtil.assertEquals(EAST, perpendicularRight(NORTH));
		MeasureTestUtil.assertEquals(SOUTH, perpendicularRight(EAST));
		MeasureTestUtil.assertEquals(WEST, perpendicularRight(SOUTH));
		MeasureTestUtil.assertEquals(NORTH, perpendicularRight(WEST));
	}

	@Test
	public void testPerpendicularLeft() {
		MeasureTestUtil.assertEquals(EAST, perpendicularLeft(SOUTH));
		MeasureTestUtil.assertEquals(SOUTH, perpendicularLeft(WEST));
		MeasureTestUtil.assertEquals(WEST, perpendicularLeft(NORTH));
		MeasureTestUtil.assertEquals(NORTH, perpendicularLeft(EAST));
	}

	@Test
	public void testPercentRemaining() {
		Ratio ratio = ratio(0.75);
		Ratio percentRemaining = percentRemaining(ratio);
		assertEquals(ratio(0.25), percentRemaining);
	}

	@Test
	public void testPercentRemainingOver1() {
		assertEquals("over 100% means nothing is remaining", RATIO_ZERO, percentRemaining(ratio(1.1)));
	}

	@Test
	public void testPercentRemainingBelowZero() {
		assertEquals("negative percent means 100% remaining", RATIO_ONE, percentRemaining(ratio(-0.1)));
	}
}
