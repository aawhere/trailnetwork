/**
 *
 */
package com.aawhere.measure.geocell;

import static com.aawhere.measure.geocell.GeoCellTestUtil.*;
import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.GeoMathTestUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * @author Brian Chapman
 * 
 */
public class GeoCellUtilUnitTest {

	private static final GeoCoordinate LOWER_CORNER = GeoCoordinate.create().setLatitude(0d).setLongitude(0d).build();
	private static final GeoCoordinate UPPER_CORNER = GeoCoordinate.create().setLatitude(1d).setLongitude(1d).build();
	BoundingBox box;
	/** See TN-494 for a view of why these are chosen. */
	private static final GeoCells expectedCellsResolution5 = GeoCells
			.valueOf("c0000,c0001,c0004,c0002,c0003,c0006,c0008,c0009,c000c,c000a,c000b,c000e,c0020,c0021,c0024,c0022,c0023,c0026");

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		GeoCoordinate ne = UPPER_CORNER;
		GeoCoordinate sw = LOWER_CORNER;
		this.box = new BoundingBox.Builder().setNorthEast(ne).setSouthWest(sw).build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenerateGeoCells_BoundingBox() {
		Boolean minimize = minimize();
		Set<GeoCell> cells = GeoCellUtil.generateGeoCells(box, minimize);
		assertGenerateCells(cells, GeoCellUtil.MAX_GEOCELL_RESOLUTION, minimize);
	}

	@Test
	public void testGenerateGeoCellsBoundingBoxResolutionFiveNoMinimize() {
		Set<GeoCell> cells = GeoCellUtil.generateGeoCells(box, Resolution.FIVE, GeoCellUtil.DONT_MINIMIZE_RESULTS);
		TestUtil.assertCollectionEquals(expectedCellsResolution5.getAll(), cells);
	}

	@Test
	public void testGenerateGeoCellsBoundingBoxResolutionFiveMinimize() {
		Set<GeoCell> cells = GeoCellUtil.generateGeoCells(box, Resolution.FIVE, minimize());
		TestUtil.assertCollectionEquals(expectedCellsResolution5.getAll(), cells);
	}

	@Test
	public void testGeoCellBoundingBoxResolutionFiveSetOtherNoMinimize() {
		assertGeoCellBoundingBoxSetOtherResolution5(GeoCellUtil.DONT_MINIMIZE_RESULTS);
	}

	/**
	 * @param minimize
	 */
	private void assertGeoCellBoundingBoxSetOtherResolution5(final boolean minimize) {
		GeoCellBoundingBox geoCellBoundingBox = GeoCellBoundingBox.createGeoCell().setOther(box)
				.maxGeoCellResolution(Resolution.FIVE).minimizeGeoCells(minimize).build();
		TestUtil.assertCollectionEquals(expectedCellsResolution5.getAll(), geoCellBoundingBox.getGeoCells().getAll());
	}

	@Test
	public void testGeoCellBoundingBoxResolutionFiveSetOtherMinimize() {
		assertGeoCellBoundingBoxSetOtherResolution5(minimize());
	}

	@Test
	public void testGenerateGeoCellsBoundingBoxForA() {
		final BoundingBox boundingBox = GeoCellTestUtil.BOUNDS_OF_A;
		GeoCell a = GeoCellTestUtil.A;
		assertMaxResolutionBoundingBoxForGeoCell(boundingBox, a, Resolution.THIRTEEN);
		assertMaxResolutionBoundingBoxForGeoCell(boundingBox, a, Resolution.EIGHT);
		assertMaxResolutionBoundingBoxForGeoCell(boundingBox, a, Resolution.FIVE);
		assertMaxResolutionBoundingBoxForGeoCell(boundingBox, a, Resolution.ONE);
	}

	@Test
	public void testGenerateGeoCellsBoundingBoxMinimizeCell9FullBounds() {
		final GeoCell expectedGeoCell = NINE;
		BoundingBox fullBox = GeoCellUtil.boundingBoxFromGeoCell(expectedGeoCell);

		assertMaxResolutionBoundingBoxForGeoCell(fullBox, expectedGeoCell, Resolution.THIRTEEN);
		assertMaxResolutionBoundingBoxForGeoCell(fullBox, expectedGeoCell, Resolution.ONE);
		assertMaxResolutionBoundingBoxForGeoCell(	fullBox,
													expectedGeoCell,
													Resolution.THIRTEEN,
													GeoCellUtil.DONT_MINIMIZE_RESULTS);
		assertMaxResolutionBoundingBoxForGeoCell(	fullBox,
													expectedGeoCell,
													Resolution.ONE,
													GeoCellUtil.DONT_MINIMIZE_RESULTS);

	}

	/** see TN-494 */
	@Test
	public void testGenerateGeoCellsBoundingBoxMinimizeCell9() {
		final BoundingBox expectedBoundingBox = GeoCellTestUtil.BOUNDS_OF_9;
		final GeoCell expectedGeoCell = GeoCellTestUtil.NINE;
		assertMaxResolutionBoundingBoxForGeoCell(expectedBoundingBox, expectedGeoCell, Resolution.THIRTEEN);
		assertMaxResolutionBoundingBoxForGeoCell(expectedBoundingBox, expectedGeoCell, Resolution.EIGHT);
		assertMaxResolutionBoundingBoxForGeoCell(expectedBoundingBox, expectedGeoCell, Resolution.FIVE);
		assertMaxResolutionBoundingBoxForGeoCell(expectedBoundingBox, expectedGeoCell, Resolution.ONE);
	}

	public void testGenerateGeoCellsBoundingBoxMinimizeCell9NoMinimize() {
		final BoundingBox expectedBoundingBox = GeoCellTestUtil.BOUNDS_OF_9;
		final GeoCell expectedGeoCell = GeoCellTestUtil.NINE;
		final boolean minimize = GeoCellUtil.DONT_MINIMIZE_RESULTS;
		assertMaxResolutionBoundingBoxForGeoCell(expectedBoundingBox, expectedGeoCell, Resolution.THIRTEEN, minimize);
		assertMaxResolutionBoundingBoxForGeoCell(expectedBoundingBox, expectedGeoCell, Resolution.EIGHT, minimize);
		assertMaxResolutionBoundingBoxForGeoCell(expectedBoundingBox, expectedGeoCell, Resolution.FIVE, minimize);
		assertMaxResolutionBoundingBoxForGeoCell(expectedBoundingBox, expectedGeoCell, Resolution.ONE, minimize);
	}

	/**
	 * Given a cell this will create a bounding box, get the cells from the box and minmize the
	 * results at various resolutions. It will then create a bounding box from those resulting cells
	 * and ensure the new bounding box is within an acceptable range in size of the original
	 * bounding box.
	 * 
	 * @param expectedBoundingBox
	 * @param expectedGeoCell
	 * @param maxResolution
	 */
	private void assertMaxResolutionBoundingBoxForGeoCell(final BoundingBox expectedBoundingBox,
			final GeoCell expectedGeoCell, final Resolution maxResolution) {
		final Boolean minimize = GeoCellUtil.MINIMIZE_RESULTS;
		assertMaxResolutionBoundingBoxForGeoCell(expectedBoundingBox, expectedGeoCell, maxResolution, minimize);
	}

	/**
	 * @param expectedBoundingBox
	 * @param expectedGeoCell
	 * @param maxResolution
	 * @param minimize
	 */
	private void assertMaxResolutionBoundingBoxForGeoCell(final BoundingBox expectedBoundingBox,
			final GeoCell expectedGeoCell, final Resolution maxResolution, final Boolean minimize) {
		Set<GeoCell> cells = GeoCellUtil.generateGeoCells(expectedBoundingBox, maxResolution, minimize);
		// minimize forces the cell to be existent
		if (minimize) {
			TestUtil.assertContains(expectedGeoCell, cells);
		} else {
			// pretty lame, but just ensure it is in the string
			TestUtil.assertContains(cells.toString(), expectedGeoCell.toString());
		}
		Length expectedDiagonal = expectedBoundingBox.getDiagonal();
		final double ratioAllowed = 0.05;
		Length allowedDifference = QuantityMath.create(expectedDiagonal).times(ratioAllowed).getQuantity();
		Length min = QuantityMath.create(expectedDiagonal).minus(allowedDifference).getQuantity();
		Length max = QuantityMath.create(expectedDiagonal).plus(allowedDifference).getQuantity();
		Range<Comparable<Length>> range = Range
				.closed(MeasurementUtil.comparable(min), MeasurementUtil.comparable(max));

		BoundingBox actualBoundingBox = GeoCellUtil.boundingBoxFromGeoCells(cells);
		MeasureTestUtil.assertRange(range, actualBoundingBox.getDiagonal());
	}

	/**
	 * @return
	 */
	private Boolean minimize() {
		return GeoCellUtil.MINIMIZE_RESULTS;
	}

	/**
	 * challenging the the cell generation with a bounding box of smaller resolution than is
	 * desired.
	 * 
	 */
	@Test
	public void testGenerateCellsBoundingBoxControlResolution() {
		final Resolution resolution = Resolution.EIGHT;
		// this bounding box was observed to cause a problem.
		GeoCellBoundingBox boundingBox = GeoCellUtil.boundingBox(new GeoCoordinate.Builder().setLatitude(1.0)
				.setLongitude(1.0).build(), resolution);
		Set<GeoCell> cells = GeoCellUtil.generateGeoCells(boundingBox, resolution, GeoCellUtil.DONT_MINIMIZE_RESULTS);
		assertEquals("incorrect resolution calculated", resolution, GeoCellUtil.maxResolution(cells));
	}

	@Test
	public void testTranslateToAllCells() {
		GeoCells geoCells = GeoCells.create().setFromString("3fffffff 95555555 c0026db4 c0000000 6aaaaaaa").build();
		geoCells = GeoCellUtil.withAncestors(geoCells);
		Set<GeoCell> allResolutions = GeoCellUtil.translateToAllResolutions(geoCells);
		assertTrue("bad match for " + allResolutions, GeoCellComparison.left(allResolutions).right(geoCells).contains());
	}

	@Test
	public void testGenerateGeoCellsHighResolution() {
		GeoCellBoundingBox smallBox = GeoCellBoundingBox.createGeoCell().setGeoCells(new GeoCell("8123456789a"))
				.build();
		Set<GeoCell> cells = GeoCellUtil.generateGeoCells(smallBox, Resolution.THIRTEEN, false, 100);
		GeoCell next = cells.iterator().next();
		assertEquals(Resolution.TWELVE, next.getResolution());
	}

	@Test
	public void testGenerateGeoCells_BoundingBox_int() {
		Boolean minimize = minimize();
		Set<GeoCell> cells = GeoCellUtil.generateGeoCells(box, GeoCellUtil.MAX_GEOCELL_RESOLUTION, minimize);

		assertGenerateCells(cells, GeoCellUtil.MAX_GEOCELL_RESOLUTION, minimize);
	}

	private void assertGenerateCells(Set<GeoCell> geoCells, Resolution maxResolution, Boolean minimize) {
		assertNotNull(geoCells);

		for (GeoCell cell : geoCells) {
			assertTrue(maxResolution.asInteger() > cell.getResolution().asInteger());
			// TODO: test that the result is minimized.
		}
	}

	@Test
	public void testTranslateToAllResolutions_Collection_GeoCells() {
		Set<GeoCell> cells = GeoCellUtil.generateGeoCells(box, minimize());
		Set<GeoCell> cellsWithParents = GeoCellUtil.translateToAllResolutions(cells);
		TestUtil.assertGreaterThan(cells.size(), cellsWithParents.size());
		assertNotNull(cellsWithParents);
		// WTF does this test?
		assertTrue(cellsWithParents.contains(new GeoCell(cells.iterator().next().getCellAsString().substring(0, 2))));

		assertParentsIncluded(cells, cellsWithParents);
	}

	@Test
	public void testParentGeoCell_GeoCell() {
		String cellString = "12345";
		String parentString = "1234";
		GeoCell geoCell = GeoCellTestUtil.create(cellString);
		GeoCell parentCell = GeoCellUtil.getParentGeoCell(geoCell);
		assertTrue(GeoCellUtil.hasParentGeoCell(geoCell));
		assertTrue(GeoCellUtil.hasParentGeoCell(parentCell));
		assertTrue(parentCell.isParent(geoCell));
		assertEquals(parentString, parentCell.getCellAsString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGrandParentGeoCell_GeoCell() {
		String cellString = "1";
		GeoCell geoCell = GeoCellTestUtil.create(cellString);
		assertFalse(GeoCellUtil.hasParentGeoCell(geoCell));
		GeoCellUtil.getParentGeoCell(geoCell);
		fail("calling parent is not allowed when it has no parent.");
	}

	@Test
	public void testFilterToMaxResolution_and_FilterToResolution() {
		GeoCell cell4 = new GeoCell("1234");
		GeoCell cell5 = new GeoCell("12345");
		GeoCells cells = new GeoCells.Builder().add(cell4).add(cell5).build();
		GeoCells resultMax = GeoCellUtil.filterToMaxResolution(cells);
		assertEquals(1, resultMax.getAll().size());
		assertEquals(cell5, resultMax.iterator().next());

		GeoCells result = GeoCellUtil.filterToResolution(cells, Resolution.FIVE);
		assertEquals(1, result.getAll().size());
		assertEquals(cell5, result.iterator().next());
	}

	@Test
	public void testLowestResolution() {
		Set<GeoCell> cells = Sets.newHashSet(GeoCellTestUtil.ABCD, GeoCellTestUtil.ABCDE);
		Resolution minRes = GeoCellUtil.lowestResolution(cells);
		assertEquals(Resolution.FOUR, minRes);
	}

	@Test
	public void testGetResolution() {
		String cellString = "aaa";
		Resolution res = GeoCellUtil.getResolution(cellString.length());
		assertEquals(Resolution.THREE, res);
	}

	@Test
	public void testMaxResolution() {
		GeoCell cell4 = new GeoCell("1234");
		GeoCell cell5 = new GeoCell("12345");
		Set<GeoCell> cells = new HashSet<GeoCell>();
		cells.add(cell4);
		cells.add(cell5);
		Resolution maxRes = GeoCellUtil.maxResolution(cells);
		assertEquals(Resolution.FIVE, maxRes);
	}

	@Test
	public void testConvertToGeoCellBoundingBox_BoundingBox() {
		com.beoui.geocell.model.BoundingBox bb = GeoCellUtil.convertToGeoCellBoundingBox(box);

		TestUtil.assertDoubleEquals(box.getNorthEast().getLatitude().getInDecimalDegrees(), bb.getNorth());
		TestUtil.assertDoubleEquals(box.getNorthEast().getLongitude().getInDecimalDegrees(), bb.getEast());
		TestUtil.assertDoubleEquals(box.getSouthWest().getLatitude().getInDecimalDegrees(), bb.getSouth());
		TestUtil.assertDoubleEquals(box.getSouthWest().getLongitude().getInDecimalDegrees(), bb.getWest());
	}

	@Test
	public void testConvertToFromGeoCellsSetString() {
		Set<GeoCell> cells = GeoCellTestUtil.getSampleGeoCells();
		Set<String> cellsString = GeoCellUtil.convertGeoCellsToStringSet(cells);
		Set<GeoCell> cellsFromString = GeoCellUtil.convertToGeoCellsSet(cellsString);

		assertEquals(cells, cellsFromString);
	}

	@Test
	public void testSingleCellBoundingBoxes() {
		assertBoundingBoxes(30, A);
		assertBoundingBoxes(2, A);
		assertBoundingBoxes(100, ABCDEF, B, C);

	}

	/**
	 * @param expected
	 * @param maxSize
	 */
	private void assertBoundingBoxes(final int maxSize, GeoCell... expected) {
		GeoCellBoundingBox boundingBox = GeoCellBoundingBox.createGeoCell().setGeoCells(expected).build();
		Collection<? extends BoundingBox> boundingBoxes = GeoCellUtil.boundingBoxes(boundingBox, maxSize)
				.getBoundingBoxes();
		final String message = boundingBoxes.toString();
		TestUtil.assertGreaterThan(message, maxSize / 5, boundingBoxes.size());
		TestUtil.assertLessThan(message, maxSize + 1, boundingBoxes.size());
	}

	/**
	 * Shows how a single point can't be well represented by a single geocell.
	 * 
	 * @see #testGeoCoordinateOriginToGeoCells()
	 */
	@Test
	public void testGeoCoordinateOriginToGeoCell() {
		GeoCell cell = GeoCellUtil.createGeoCell(GeoMathTestUtil.EARTH_ORIGIN, Resolution.ONE);
		assertEquals(C, cell);
	}

	/**
	 * The origin lands on the intersection of 4 boxes so this confirms all four are returned.
	 * 
	 */
	@Test
	public void testGeoCoordinateOriginToGeoCells() {
		GeoCells bigCells = GeoCellUtil.geoCells(GeoMathTestUtil.EARTH_ORIGIN, Resolution.ONE);
		assertEquals(GeoCells.create().setFromString("9 3 6 c").build(), bigCells);
		// REsolution of thirteen fails. see #testGeoCellsFromBoundingBox
		GeoCells littleCells = GeoCellUtil.geoCells(GeoMathTestUtil.EARTH_ORIGIN, Resolution.TWELVE);
		assertEquals(	GeoCells.create().setFromString("955555555555 3fffffffffff 6aaaaaaaaaaa c00000000000").build(),
						littleCells);
	}

	@Test
	public void testGeoCoordinateHeadlands() {
		GeoCoordinate geoCoordinate = GeoCoordinate.create().setAsString("37.860385,-122.5357839").build();
		GeoCell geoCell = GeoCellUtil.geoCell(geoCoordinate, Resolution.FIVE);
		assertEquals("8e62f", geoCell.getCellAsString());
	}

	@Test
	public void testGeoCoordinateInsideACell() {
		final GeoCell cell = A;
		assertCellCenter(cell);
		assertCellCenter(ABCDEF);
		assertCellCenter(AB);
	}

	@Test
	public void testGeoCellsFromBoundingBox() {
		final GeoCell cell = new GeoCell("a6aaaaaaaaaaa8");
		BoundingBox cellBox = GeoCellUtil.boundingBoxFromGeoCell(cell);

		Set<GeoCell> cells = GeoCellUtil.generateGeoCells(cellBox, Resolution.ONE, GeoCellUtil.DONT_MINIMIZE_RESULTS);
		TestUtil.assertContains(A, cells);
		TestUtil.assertSize(1, cells);
	}

	/**
	 * Given a cell this will ensure the {@link GeoCellUtil#geoCells(GeoCoordinate, Resolution)}
	 * returns the cell back when calling it's midpoint and resolution.
	 * 
	 * @param cell
	 */
	private void assertCellCenter(final GeoCell cell) {
		BoundingBox cellBox = GeoCellUtil.boundingBoxFromGeoCell(cell);
		GeoCells cells = GeoCellUtil.geoCells(cellBox.getCenter(), cell.getResolution());
		TestUtil.assertContains(cell, cells.getAll());
		TestUtil.assertSize(1, cells.getAll());
	}

	@Test
	public void testMinimumCircle() {
		double expectedradius = 59510;
		GeoCells geocells = GeoCells.create().addAll(GeoCellTestUtil.getSampleGeoCells()).build();
		assertCircle(GeoCellUtil.minimumCircle(geocells), MeasurementUtil.createLengthInMeters(expectedradius), 4d);
	}

	@Test
	public void testMinimumCircleWithOneCell() {
		GeoCell cell = GeoCellTestUtil.EIGHT;
		BoundingBox box = GeoCellUtil.boundingBoxFromGeoCell(cell);
		Length expectedRadius = MeasurementUtil.createLengthInMeters(5600650);
		GeoCells geocells = GeoCells.create().add(cell).build();
		assertCircle(GeoCellUtil.minimumCircle(geocells), expectedRadius, 4d);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testMinimumCircleWithZeroCells() {
		Length expectedRadius = MeasurementUtil.createLengthInMeters(0);
		GeoCells geocells = GeoCells.create().build();
		assertCircle(GeoCellUtil.minimumCircle(geocells), expectedRadius, 1d);
	}

	/**
	 * 
	 * @param circle
	 * @param expectedRadius
	 * @param delta
	 *            the max allowed delta between expected and actual as decimal percent
	 */
	public void assertCircle(Pair<GeoCoordinate, Length> circle, Length expectedRadius, double delta) {
		assertNotNull(circle);
		GeoCoordinate center = circle.getLeft();
		Length radius = circle.getRight();
		assertNotNull(center);
		assertNotNull(radius);
		assertEquals(expectedRadius.doubleValue(MetricSystem.METRE), radius.doubleValue(MetricSystem.METRE), delta);
	}

	@Test
	public void testWithoutParents() {
		Set<GeoCell> set = GeoCellUtil.explode(ABCD);
		set.addAll(GeoCellUtil.explode(ABCDE));
		set.addAll(A_THRU_F_WITH_PARENTS.getAll());
		Set<GeoCell> result = GeoCellUtil.withOutParents(set).getAll();

		Set<GeoCell> expected = GeoCellUtil.explode(ABCD);
		expected.addAll(GeoCellUtil.explode(ABCDE));
		expected.remove(ABCDE);
		assertEquals(expected, result);
	}

	@Test
	public void testWithoutParentsOneInput() {
		Set<GeoCell> set = Sets.newHashSet(ABCD);
		Set<GeoCell> result = GeoCellUtil.withOutParents(set).getAll();

		assertEquals(1, result.size());
		TestUtil.assertContains(ABCD, result);
	}

	@Test
	public void testProximitySortFromLeastCorner() {
		final GeoCoordinate target = LOWER_CORNER;
		final GeoCell expectedClosestCell = new GeoCell("c0000");
		assertProximity(target, expectedClosestCell);
	}

	@Test
	public void testProximitySortFromUpperCorner() {
		assertProximity(UPPER_CORNER, new GeoCell("c0026"));
	}

	@Test
	public void testProximitySortMiddle() {
		assertProximity(GeoMath.midpoint(UPPER_CORNER, LOWER_CORNER), new GeoCell("c0009"));
	}

	@Test
	public void testGeoCellsAll() {
		TestUtil.assertSize(GeoCellUtil.NUM_OF_CHILD_CELLS_IN_CELL, GeoCellUtil.WORLD);
	}

	/**
	 * see TN-494 for visual details of this which picks the point and compares to the given
	 * {@link GeoCells} from {@link #expectedCellsResolution5}.
	 * 
	 * @param target
	 * @param expectedClosestCell
	 */
	private void assertProximity(final GeoCoordinate target, final GeoCell expectedClosestCell) {
		List<GeoCell> proximity = GeoCellUtil.closestToPoint(target, expectedCellsResolution5);
		assertEquals("the point is at the lowest corner", expectedClosestCell, proximity.get(0));
	}

}
