/**
 * 
 */
package com.aawhere.joda.time;

import static com.aawhere.joda.time.JodaTestUtil.*;
import static com.aawhere.joda.time.JodaTimeUtil.*;
import static org.junit.Assert.*;

import java.util.GregorianCalendar;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.UnitTest;

/**
 * Tests the static functions of {@link JodaTimeUtil}.
 * 
 * @author roller
 * 
 */
@Category(UnitTest.class)
public class JodaTimeUtilUnitTest {

	@Test
	public void testDurationRatioAll() {
		Duration duration = new Duration(TestUtil.generateRandomLong());
		Ratio result = ratioFrom(duration, duration);
		assertEquals(MeasurementUtil.RATIO_ONE, result);
	}

	@Test
	public void testDurationRatioHalf() {
		final int time = 1000;
		Duration duration = new Duration(time);
		Duration totalDuration = new Duration(2 * time);
		Ratio expected = MeasurementUtil.createRatio(duration.getMillis(), totalDuration.getMillis());
		Ratio result = ratioFrom(duration, totalDuration);
		assertEquals(expected, result);
	}

	@Test
	public void testRfc1123HeaderDateTime() {
		String expected = "Sun, 06 Nov 1994 08:49:37 GMT";
		final DateTimeFormatter formatter = JodaTimeUtil.rfc1123HeaderDateTimeFormatter();
		DateTime dateTime = formatter.parseDateTime(expected);
		assertEquals(	"formatter isn't producing the same date as it read",
						expected,
						JodaTimeUtil.rfc1123HeaderDateTime(dateTime));
		DateTime rfc1123HeaderDateTime = JodaTimeUtil.rfc1123HeaderDateTime(expected);
		assertEquals(dateTime, rfc1123HeaderDateTime);
		assertNull(JodaTimeUtil.rfc1123HeaderDateTime((String) null));
		assertNull(JodaTimeUtil.rfc1123HeaderDateTime((DateTime) null));

	}

	@Test
	public void testGregorianCalendar() {

		final int second = 33;
		final int minute = 27;
		final int hourOfDay = 15;
		final int dayOfMonth = 23;
		final int month = GregorianCalendar.JUNE;
		final int year = 2007;
		GregorianCalendar cal = new GregorianCalendar(year, month, dayOfMonth, hourOfDay, minute, second);
		DateTime dateTime = JodaTimeUtil.createDateTime(cal);
		assertEquals(second, dateTime.getSecondOfMinute());
		assertEquals(minute, dateTime.getMinuteOfHour());
		assertEquals(hourOfDay, dateTime.getHourOfDay());
		assertEquals(dayOfMonth, dateTime.getDayOfMonth());
		assertEquals(month + 1, dateTime.getMonthOfYear());
		assertEquals(year, dateTime.getYear());

	}

	@Test
	public void testIsoFormatEpoch() {
		String expected = EPOCH_ZULU_STRING;
		assertIsoFormat(expected, EPOCH_ZULU_TIMESTAMP);
	}

	/**
	 * "demonstrates the millis are used when necessary",
	 * 
	 */
	@Test
	public void testIsoFormatWithMillis() {
		assertIsoFormat(EPOCH_ZULU_1_MILLIS_STRING, EPOCH_ZULU_1_MILLIS_TIMESTAMP);
	}

	@Test
	public void testParseXmlDate() {

		JodaTestUtil.assertEquals(	"without millis is incorrect",
									EPOCH_ZULU_TIMESTAMP,
									JodaTimeUtil.parseXmlDate(EPOCH_ZULU_STRING));
		JodaTestUtil.assertEquals(	"with millis is incorrect",
									EPOCH_ZULU_1_MILLIS_TIMESTAMP,
									JodaTimeUtil.parseXmlDate(EPOCH_ZULU_1_MILLIS_STRING));
	}

	@Test
	public void testEquals() {
		JodaTestUtil.assertEquals("these are the same", EPOCH_ZULU_TIMESTAMP, EPOCH_ZULU_TIMESTAMP);
		JodaTestUtil.assertEquals("these are close enough", EPOCH_ZULU_TIMESTAMP, EPOCH_ZULU_1_MILLIS_TIMESTAMP);
	}

	/**
	 * @param expected
	 * @param millisPastEpoch
	 */
	private void assertIsoFormat(String expected, DateTime timestamp) {
		String actual = JodaTimeUtil.toIsoXmlFormat(timestamp);
		assertEquals("epoch doesn't match formatted", expected, actual);
	}

	@Test
	public void testEqualsWithinTolerance() {
		assertTrue(JodaTimeUtil.equalsWithinTolerance(zero, zero));
		assertTrue(JodaTimeUtil.equalsWithinTolerance(zero, one));
		assertTrue(JodaTimeUtil.equalsWithinTolerance(one, zero));
		assertTrue(JodaTimeUtil.equalsWithinTolerance(one, two));
		assertFalse(JodaTimeUtil.equalsWithinTolerance(zero, two));
		assertTrue(JodaTimeUtil.equalsWithinTolerance(zero, two, 2));
	}

	@Test
	public void testUtcDateFormat() {
		DateTime dateTime = new DateTime(0).plusDays(30);
		String date = JodaTimeUtil.toIsoXmlUtcDateFormat(dateTime);
		assertEquals("1970-01-31", date);
	}

	@Test
	public void testMoreRecentDateBothNull() {
		assertNull(JodaTimeUtil.moreRecent((LocalDate) null, null));
	}

	@Test
	public void testMoreRecentDateOneNull() {
		final LocalDate now = LocalDate.now();
		assertEquals(now, JodaTimeUtil.moreRecent(now, null));
		assertEquals(now, JodaTimeUtil.moreRecent(null, now));
	}

	@Test
	public void testMoreRecentDate() {
		LocalDate now = LocalDate.now();
		LocalDate minusDays = now.minusDays(1);
		assertEquals(now, JodaTimeUtil.moreRecent(now, minusDays));
	}
}
