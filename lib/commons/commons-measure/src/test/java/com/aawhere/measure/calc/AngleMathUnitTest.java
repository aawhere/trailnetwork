/**
 * 
 */
package com.aawhere.measure.calc;

import static org.junit.Assert.*;

import javax.measure.quantity.Angle;

import org.junit.Test;

import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;

/**
 * @author aroller
 * 
 */
public class AngleMathUnitTest {

	@Test
	public void testReverse() {
		MeasureTestUtil.assertEquals("east to west", MeasurementUtil.WEST, AngleMath.create(MeasurementUtil.EAST)
				.reverse().getQuantity());
		MeasureTestUtil.assertEquals("west to east", MeasurementUtil.EAST, AngleMath.create(MeasurementUtil.WEST)
				.reverse().getQuantity());
		MeasureTestUtil.assertEquals("north to south", MeasurementUtil.SOUTH, AngleMath.create(MeasurementUtil.NORTH)
				.reverse().getQuantity());
		MeasureTestUtil.assertEquals("south to north", MeasurementUtil.NORTH, AngleMath.create(MeasurementUtil.SOUTH)
				.reverse().getQuantity());
	}

	@Test
	public void testBetween() {
		Boolean expectedBetween = true;
		double targetDegrees = 180.0;
		double minDegrees = 179.9;
		double maxDegrees = 180.1;
		assertBetween(expectedBetween, targetDegrees, minDegrees, maxDegrees);
		assertBetween(false, targetDegrees, maxDegrees, maxDegrees);
		assertBetween(true, 0, 359, 1);
		assertBetween(false, 359.0, 0, 1);
		assertBetween(true, 270, 180, 360);
		assertBetween(true, 270, 180, 0);
	}

	private void assertBetween(Boolean expectedBetween, double targetDegrees, double minDegrees, double maxDegrees) {
		Angle target = MeasurementUtil.createAngleInDegrees(targetDegrees);
		Angle min = MeasurementUtil.createAngleInDegrees(minDegrees);
		Angle max = MeasurementUtil.createAngleInDegrees(maxDegrees);
		assertEquals("between", expectedBetween, AngleMath.create(target).between(min, max));
	}

	@Test
	public void testDelta() {
		double previousDegrees = 0;
		double nextDegrees = 10;
		double deltaDegreesExpected = 10;
		assertDelta(previousDegrees, nextDegrees, deltaDegreesExpected);
		assertDelta(350, 0, 10);
		assertDelta(0, 350, -10);
		assertDelta(0, 180, 180);
		assertDelta(180, 0, 180);
		assertDelta(0, 185, -175);
		assertDelta(180, 10, -170);
		assertDelta(270, 90, 180);
	}

	private void assertDelta(double previousDegrees, double nextDegrees, double deltaDegreesExpected) {
		Angle previous = MeasurementUtil.createAngleInDegrees(previousDegrees);
		Angle next = MeasurementUtil.createAngleInDegrees(nextDegrees);
		Angle delta = AngleMath.create(previous).delta(next).getQuantity();
		MeasureTestUtil.assertEquals(MeasurementUtil.createAngleInDegrees(deltaDegreesExpected), delta);
	}
}
