/**
 * 
 */
package com.aawhere.measure;

import static com.aawhere.measure.BoundingBoxTestUtils.*;
import static com.aawhere.measure.calc.GeoMathTestUtil.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.aawhere.measure.calc.GeoMathTestUtil;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class BoundingBoxUtilUnitTest {

	@Test
	public void testCoordinates() {
		List<GeoCoordinate> coordinates = BoundingBoxUtil.coordinates(BoundingBoxTestUtils.getMaxBounds());
		TestUtil.assertSize(5, coordinates);
		TestUtil.assertContains(MeasurementUtil.MAX_GEO_COORDINATE, coordinates);
		TestUtil.assertContains(MeasurementUtil.MIN_GEO_COORDINATE, coordinates);
	}

	@Test
	public void testSinglePointBox() {
		GeoCoordinate coordinate = MeasureTestUtil.createRandomGeoCoordinate();
		BoundingBox boundingBox = BoundingBoxUtil.boundingBox(Lists.newArrayList(coordinate));
		assertEquals(coordinate, boundingBox.getMax());
		assertEquals(coordinate, boundingBox.getMin());
	}

	@Test
	public void testExtremes() {
		BoundingBox boundingBox = extremes();
		assertEquals(MeasurementUtil.MIN_LATITUDE, boundingBox.getMin().getLatitude());
		assertEquals(MeasurementUtil.MAX_LATITUDE, boundingBox.getMax().getLatitude());
		assertEquals(MeasurementUtil.MIN_LONGITUDE, boundingBox.getMin().getLongitude());
		assertEquals(MeasurementUtil.MAX_LONGITUDE, boundingBox.getMax().getLongitude());
	}

	@Test
	public void testContainsPoint() {

		BoundingBox boundingBox = origin();
		final GeoCoordinate coordinate = EARTH_ORIGIN;
		assertContains(true, boundingBox, coordinate);
		assertContains(true, boundingBox, EARTH_ORIGIN_NORTH_EAST);
		assertContains(true, boundingBox, EARTH_ORIGIN_SOUTH_WEST);
		assertContains(true, boundingBox, EARTH_ORIGIN_SOUTH);
		assertContains(true, boundingBox, EARTH_ORIGIN_WEST);
		assertContains(true, boundingBox, EARTH_ORIGIN_NORTH_EAST);
		assertContains(true, boundingBox, EARTH_ORIGIN_EAST);
		assertContains(true, boundingBox, EARTH_ORIGIN_EAST_HALFWAY);
		assertContains(false, boundingBox, FAR_EAST);
		assertContains(false, boundingBox, FAR_WEST);

	}

	@Test
	public void testContainsSameBox() {
		final BoundingBox origin = origin();
		assertTrue("doesn't contain itself", origin.contains(origin));
	}

	@Test
	public void testExtremesContainsOrigin() {
		assertTrue("big box contains little box", extremes().contains(origin()));
	}

	@Test
	public void testOriginContainsExtremes() {
		assertFalse("little box contained by big box", origin().contains(extremes()));
	}

	@Test
	public void testWestBoundaryShared() {
		assertTrue("halfway to east makes the box smaller", originNorthEast().contains(originNorthEastHalfway()));
	}

	/**
	 * These two overlapp around the origin both containing the origin, but do not contain each
	 * other completely.
	 * 
	 */
	@Test
	public void testOverlappingNoContainment() {
		BoundingBox topHeavy = originOverlappedFullNorthEastHalfSouthWest();
		BoundingBox bottomHeavy = originOverlappedFullSouthWestHalfNorthEast();
		assertFalse("overlapp, but no containment", topHeavy.contains(bottomHeavy));
		assertFalse("overlapp, but no containment", bottomHeavy.contains(topHeavy));
	}

	/** Expands one box and confirms containment, but not in reverse. */
	@Test
	public void testExpand() {
		BoundingBox origin = origin();
		BoundingBox expand = BoundingBoxUtil.expand(origin, MeasurementUtil.createLengthInMeters(1));
		assertContains(origin, expand, false);
		assertContains(expand, origin, true);

	}

	@Test
	public void testExpandSquareVertical() {
		assertSqauare(GeoMathTestUtil.EARTH_ORIGIN, EARTH_ORIGIN_NORTH);

	}

	/**
	 * @param boundingBox
	 */
	private void assertSqauare(GeoCoordinate... coordinates) {
		BoundingBox boundingBox = BoundingBoxUtil.boundingBox(coordinates);
		BoundingBox expandSquare = BoundingBoxUtil.expandSquare(boundingBox);
		BoundingBoxTestUtils.assertContains(expandSquare, boundingBox);
		BoundingBoxTestUtils.assertContains(boundingBox, expandSquare, false);
	}

	@Test
	public void testExpandSqaureHorizontal() {
		assertSqauare(EARTH_ORIGIN, EARTH_ORIGIN_EAST);
	}

	@Test
	public void testExpandAlreadySquare() {
		final BoundingBox expected = BoundingBoxUtil.boundingBox(	EARTH_ORIGIN,
																	EARTH_ORIGIN_SOUTH_WEST,
																	EARTH_ORIGIN_NORTH_EAST);
		assertEquals(expected, BoundingBoxUtil.expandSquare(expected));
	}
}
