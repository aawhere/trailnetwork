/**
 *
 */
package com.aawhere.measure.calc;

import static org.junit.Assert.*;

import java.math.RoundingMode;

import javax.measure.quantity.Length;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.BaseUnit;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.Unit;

import org.junit.Test;

import com.aawhere.lang.ComparatorResult;
import com.aawhere.measure.Elevation;
import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.test.TestUtil;

/**
 * Unit tests for {@link QuantityMath}.
 * 
 * @author roller
 * 
 */
public class QuantityMathUnitTest {

	/**
	 *
	 */
	private static final double JUST_A_TINY_BIT_MORE = 0.000000001;
	/**
	 *
	 */
	private static final BaseUnit<Length> UNIT = MetricSystem.METRE;
	private static final double TOLERANCE_VALUE = 1;
	private static final double ABOVE_TOLERANCE_VALUE = TOLERANCE_VALUE + JUST_A_TINY_BIT_MORE;

	private static final double BETWEEN_VALUE = 4.0;
	private static final double MAX_VALUE = BETWEEN_VALUE + TOLERANCE_VALUE;
	private static final double MIN_VALUE = BETWEEN_VALUE - TOLERANCE_VALUE;

	private static Length MIN = MeasurementQuantityFactory.getInstance(Length.class).create(MIN_VALUE, UNIT);
	private static Length MAX = MeasurementQuantityFactory.getInstance(Length.class).create(MAX_VALUE, UNIT);
	private static Length BELOW_MIN = MeasurementQuantityFactory.getInstance(Length.class)
			.create(BETWEEN_VALUE - ABOVE_TOLERANCE_VALUE, UNIT);
	private static Length ABOVE_MAX = MeasurementQuantityFactory.getInstance(Length.class)
			.create(BETWEEN_VALUE + ABOVE_TOLERANCE_VALUE, UNIT);
	private static Length BETWEEN = MeasurementQuantityFactory.getInstance(Length.class).create(BETWEEN_VALUE, UNIT);
	private static Length TOLERANCE = MeasurementQuantityFactory.getInstance(Length.class)
			.create(TOLERANCE_VALUE, UNIT);
	private static Length JUST_A_TINY_BIT = MeasurementQuantityFactory.getInstance(Length.class)
			.create(JUST_A_TINY_BIT_MORE, UNIT);
	private static Length ZERO = MeasurementQuantityFactory.getInstance(Length.class).create(0, UNIT);

	@Test
	public void testLessThan() {
		assertTrue(new QuantityMath<Length>(MIN).lessThan(MAX));
		assertFalse(new QuantityMath<Length>(MAX).lessThan(MIN));
	}

	@Test
	public void testGreaterThan() {
		assertTrue(new QuantityMath<Length>(MAX).greaterThan(MIN));
		assertFalse(new QuantityMath<Length>(MIN).greaterThan(MAX));
	}

	@Test
	public void testLessThanEqualTo() {
		assertTrue(new QuantityMath<Length>(MIN).lessThanEqualTo(MAX));
		assertFalse(new QuantityMath<Length>(MAX).lessThanEqualTo(MIN));
		assertTrue(new QuantityMath<Length>(MIN).lessThanEqualTo(MIN));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBetween() {
		assertTrue(new QuantityMath<Length>(BETWEEN).between(MIN, MAX));
		assertTrue(new QuantityMath<Length>(BETWEEN).between(null, MAX));
		assertTrue(new QuantityMath<Length>(BETWEEN).between(MIN, null));
		assertTrue(new QuantityMath<Length>(BETWEEN).between(null, null));
		assertFalse(new QuantityMath<Length>(MIN).between(BETWEEN, MAX));
		assertFalse(new QuantityMath<Length>(MAX).between(MIN, BETWEEN));

		// check exlusive
		Boolean includeEnpoints = false;
		assertTrue(new QuantityMath<Length>(BETWEEN).between(MIN, MAX, includeEnpoints));
		assertFalse(new QuantityMath<Length>(MIN).between(MIN, MAX, includeEnpoints));
		assertFalse(new QuantityMath<Length>(MAX).between(MIN, MAX, includeEnpoints));

		// test a value just below the min
		assertFalse(new QuantityMath<Length>(BELOW_MIN).between(MIN, MAX));
		assertFalse(new QuantityMath<Length>(ABOVE_MAX).between(MIN, MAX));

		// can't provide a min greater than the max, throws exception
		assertFalse(new QuantityMath<Length>(BETWEEN).between(MAX, MIN));

	}

	@Test
	public void testEquals() {
		assertTrue(new QuantityMath<Length>(BETWEEN).equals(MIN, TOLERANCE));
		assertTrue(new QuantityMath<Length>(BETWEEN).equals(MAX, TOLERANCE));
		assertFalse(new QuantityMath<Length>(BETWEEN).equals(ABOVE_MAX, TOLERANCE));
		assertFalse(new QuantityMath<Length>(BETWEEN).equals(BELOW_MIN, TOLERANCE));

		// test default tolerance, but true testing will require injection once enabled.
		assertTrue(new QuantityMath<Length>(MIN).equals(BELOW_MIN));
		assertTrue(new QuantityMath<Length>(JUST_A_TINY_BIT).equalToZero());
		assertTrue(new QuantityMath<Length>(ZERO).equalToZero());
		assertFalse(new QuantityMath<Length>(BETWEEN).equalToZero());
	}

	@Test
	public void testMinus() {
		double expected = MAX_VALUE - MIN_VALUE;
		TestUtil.assertDoubleEquals(expected, new QuantityMath<Length>(MAX).minus(MIN).getQuantity().doubleValue(UNIT));
	}

	@Test
	public void testPlus() {
		double expected = MAX_VALUE + MIN_VALUE;
		TestUtil.assertDoubleEquals(expected, new QuantityMath<Length>(MAX).plus(MIN).getQuantity().doubleValue(UNIT));
	}

	@Test
	public void testPlusMultiple() {
		double expected = MAX_VALUE + BETWEEN_VALUE + MIN_VALUE;
		TestUtil.assertDoubleEquals(expected, new QuantityMath<Length>(MAX).plus(MIN, BETWEEN).getQuantity()
				.doubleValue(UNIT));
	}

	@Test
	public void testDivision() {
		double expected = MAX_VALUE / MIN_VALUE;
		TestUtil.assertDoubleEquals(expected,
									new QuantityMath<Length>(MAX).dividedBy(MIN).getQuantity().doubleValue(UNIT));
	}

	@Test
	public void testSquare() {
		double expected = MAX_VALUE * MAX_VALUE;
		TestUtil.assertDoubleEquals(expected, new QuantityMath<Length>(MAX).squared().getQuantity().doubleValue(UNIT));
	}

	@Test
	public void testSquareRoot() {
		double expected = Math.sqrt(MAX_VALUE);
		TestUtil.assertDoubleEquals(expected, new QuantityMath<Length>(MAX).squareRoot().getQuantity()
				.doubleValue(UNIT));
	}

	@Test
	public void testDivisionOfValue() {
		double expected = MAX_VALUE / MIN_VALUE;
		TestUtil.assertDoubleEquals(expected, new QuantityMath<Length>(MAX).dividedBy(MIN_VALUE).getQuantity()
				.doubleValue(UNIT));
	}

	@Test
	public void testAverage() {
		double averageValue = (MAX_VALUE + MIN_VALUE) / 2;
		TestUtil.assertDoubleEquals(averageValue,
									new QuantityMath<Length>(MAX).average(MIN).getQuantity().doubleValue(UNIT));
	}

	@Test
	public void testMultiplication() {
		double expected = MAX_VALUE * MIN_VALUE;
		TestUtil.assertDoubleEquals(expected, new QuantityMath<Length>(MAX).multipliedBy(MIN).getQuantity()
				.doubleValue(UNIT));
		TestUtil.assertDoubleEquals(expected, new QuantityMath<Length>(MAX).times(MIN).getQuantity().doubleValue(UNIT));

		TestUtil.assertDoubleEquals(0, new QuantityMath<Length>(MAX).multipliedBy(0).getQuantity().doubleValue(UNIT));
		TestUtil.assertDoubleEquals(0, new QuantityMath<Length>(MAX).times(0).getQuantity().doubleValue(UNIT));

	}

	@Test
	public void testNegate() {
		double expected = MAX_VALUE * -1;
		QuantityMath<Length> math = new QuantityMath<Length>(MAX).negate();
		TestUtil.assertDoubleEquals(expected, math.getQuantity().doubleValue(UNIT));
		assertTrue(math.negative());
		assertFalse(math.positive());

	}

	@Test
	public void testAbsolute() {
		Length expected = MAX;
		assertEquals("positive should be positive", expected, QuantityMath.create(expected).absolute().getQuantity());
		assertEquals("negative should be positive", expected, QuantityMath.create(expected).negate().absolute()
				.getQuantity());
	}

	@Test
	public void testInterpolationMiddle() {
		Ratio ratio = MeasurementQuantityFactory.getInstance(Ratio.class).create(0.5, ExtraUnits.RATIO);
		Length result = new QuantityMath<Length>(MIN).interpolate(MAX, ratio).getQuantity();
		TestUtil.assertDoubleEquals(4, result.doubleValue(UNIT));
	}

	@Test
	public void testInterpolationMin() {
		Ratio ratio = MeasurementQuantityFactory.getInstance(Ratio.class).create(0, ExtraUnits.RATIO);
		Length result = new QuantityMath<Length>(MIN).interpolate(MAX, ratio).getQuantity();
		assertEquals(MIN, result);
	}

	@Test
	public void testInterpolationMax() {
		Ratio ratio = MeasurementQuantityFactory.getInstance(Ratio.class).create(1, ExtraUnits.RATIO);
		Length result = new QuantityMath<Length>(MIN).interpolate(MAX, ratio).getQuantity();
		assertEquals(MAX, result);
	}

	@Test
	public void testElevation() {
		Double value = 3.3;
		Elevation expected = MeasurementUtil.createElevationInMeters(value);
		Elevation actual = new QuantityMath<Elevation>(expected).getQuantity();
		assertEquals(expected, actual);
	}

	@Test
	public void testRound() {

		final Length seed = MeasurementUtil.createLengthInMeters(0.123456789);
		Unit<Length> meters = MetricSystem.METRE;
		QuantityMath<Length> math = QuantityMath.create(seed);
		double delta = 0.0000000001;
		assertEquals(0.123, math.setScale(3).getQuantity().doubleValue(meters), delta);
		assertEquals(0.12346, math.setScale(5, RoundingMode.CEILING).getQuantity().doubleValue(meters), delta);
		assertEquals(0.12345, math.setScale(5, RoundingMode.FLOOR).getQuantity().doubleValue(meters), delta);
		assertEquals(0.123457, math.setScale(6).getQuantity().doubleValue(meters), delta);
		final Length adder = MeasurementUtil.createLengthInMeters(180);
		assertEquals(	"proves that only decimaldigits matter regardless of integer digits",
						adder.doubleValue(meters) + 0.123457,
						math.plus(adder).setScale(6).getQuantity().doubleValue(meters),
						delta);
	}

	@Test
	public void testCompareTo() {
		QuantityMath<Length> between = QuantityMath.create(BETWEEN);
		assertEquals("equal", ComparatorResult.EQUAL.getValue(), (int) between.compareTo(BETWEEN));
		assertEquals("less", ComparatorResult.LESS_THAN.getValue(), (int) between.compareTo(MAX));
		assertEquals("more", ComparatorResult.GREATER_THAN.getValue(), (int) between.compareTo(MIN));
	}

	@Test
	public void testConvert() {
		final Length input = MAX;

		final Unit<Length> km = MetricSystem.KILO(MetricSystem.METRE);
		Length expected = QuantityFactory.getInstance(Length.class).create(input.doubleValue(km), km);
		assertEquals(input.getValue().doubleValue() / 1000.0, expected.getValue().doubleValue(), 0);
		final QuantityMath<Length> converted = QuantityMath.create(input).convert(km);
		assertEquals(km, converted.getQuantity().getUnit());
		assertEquals(expected, converted.getQuantity());
		assertEquals(expected.getValue(), converted.getQuantity().getValue());

	}
}
