/**
 * 
 */
package com.aawhere.measure.calc;

import static com.aawhere.measure.MeasurementUtil.*;
import static com.aawhere.measure.calc.GeoMathTestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.USCustomarySystem;

import org.junit.Test;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Lists;

/**
 * Tests {@link GeoMath}.
 * 
 * Not ideal, but this is being tested downstream by GeoCalculatorUnitTest.
 * 
 * TODO: Move tests from GeoCalculatorUnitTest to here.
 * 
 * @author aroller
 * 
 */
public class GeoMathUnitTest {

	@Test
	public void testHeadingNorth() {
		GeoCoordinate origin = EARTH_ORIGIN;
		GeoCoordinate destination = EARTH_ORIGIN_NORTH;
		Angle expectedHeading = NORTH;

		assertHeading(origin, destination, expectedHeading);
	}

	@Test
	public void testHeadingSouth() {
		assertHeading(EARTH_ORIGIN, EARTH_ORIGIN_SOUTH, SOUTH);
	}

	@Test
	public void testHeadingEast() {
		assertHeading(EARTH_ORIGIN, EARTH_ORIGIN_EAST, EAST);
	}

	@Test
	public void testHeadingWest() {
		assertHeading(EARTH_ORIGIN, EARTH_ORIGIN_WEST, WEST);
		assertEquals(true, true);
	}

	@Test
	public void testHeadingCausingProblems() {
		GeoCoordinate first = GeoCoordinate.create().setLatitude(47.5638281).setLongitude(7.6354351).build();
		GeoCoordinate second = GeoCoordinate.create().setLatitude(47.5638279).setLongitude(7.6354356).build();
		Angle heading = GeoMath.heading(first, second);
		assertNotNull(heading);
	}

	/**
	 * @param origin
	 * @param destination
	 * @param expectedHeading
	 */
	private void assertHeading(GeoCoordinate origin, GeoCoordinate destination, Angle expectedHeading) {
		Angle actualHeading = GeoMath.heading(origin, destination);
		MeasureTestUtil.assertEquals(origin + " to " + destination, expectedHeading, actualHeading);

	}

	@Test
	public void testShortEquatorDistance() {
		final double expectedMeters = METERS_FROM_ORIGIN_TO_NEAR;

		GeoCoordinate origin = EQUATOR_ORIGIN;
		GeoCoordinate destination = EQUATOR_NEAR;
		double tolerance = .25;
		assertDistance(expectedMeters, origin, destination, tolerance);
	}

	@Test
	public void testLongEquatorDistance() {
		assertDistance(METERS_FROM_ORIGIN_TO_FAR, EQUATOR_ORIGIN, EQUATOR_FAR, 1);
	}

	@Test
	public void testShortArcticDistance() {
		assertDistance(METERS_FROM_ARCTIC_ORIGIN_TO_ARCTIC_NEAR, ARCTIC_ORIGIN, ARCTIC_NEAR, 0.1);
	}

	@Test
	public void testLongArcticDistance() {
		assertDistance(METERS_FROM_ARCTIC_ORIGIN_TO_ARCTIC_FAR, ARCTIC_ORIGIN, ARCTIC_FAR, 3);
	}

	@Test
	public void testLongEquatorToArcticDistance() {
		int tolerance = 5;
		assertDistance(METERS_FROM_ORIGIN_TO_ARCTIC_ORIGIN, ARCTIC_ORIGIN, EQUATOR_FAR, tolerance);
		assertDistance(METERS_FROM_ORIGIN_TO_ARCTIC_ORIGIN, EQUATOR_FAR, ARCTIC_ORIGIN, tolerance);
	}

	@Test
	public void testSamePoint() {
		assertDistance(METERS_FROM_SAME_POINT, ARCTIC_ORIGIN, ARCTIC_ORIGIN, NO_TOLERANCE);
	}

	@Test
	public void testMidpointSamePoint() {
		GeoCoordinate expected = ARCTIC_ORIGIN;
		GeoCoordinate actual = GeoMath.midpoint(expected, expected);
		MeasureTestUtil.assertEquals(expected, actual);
	}

	@Test
	public void testLengthLongRoundTrip() {
		final Length tolerance = MeasurementUtil.createLengthInMeters(1);
		Length expectedLength = MeasurementUtil.createLengthInMeters(TestUtil.generateRandomDouble(1000, 10000.0));
		assertLengthRoundTrip(expectedLength, tolerance);

	}

	@Test
	public void testLengthShortRoundTrip() {
		final Length tolerance = MeasurementUtil.createLengthInMeters(0.1);
		Length expectedLength = MeasurementUtil.createLengthInMeters(TestUtil.generateRandomDouble(1, 100.0));
		assertLengthRoundTrip(expectedLength, tolerance);

	}

	/**
	 * Crawling in a single direction this detects the amount of cumulative error that can be
	 * achieved when calculating points repeatedly from a point and length.
	 */
	@Test
	public void testLengthCumulativeErrorLongJumps() {
		// 15 meters of 10,000 is about 0.15% error
		Length tolerance = MeasurementUtil.createLengthInMeters(15);
		Length jumpLength = MeasurementUtil.createLengthInMeters(100);

		assertCumulativeLengthTolerance(GeoMathTestUtil.EQUATOR_ORIGIN, jumpLength, tolerance);
	}

	@Test
	public void testLengthCumulativeErrorLongJumpsArctic() {
		// 15 meters of 10,000 is about 0.15% error
		// TN-665 upgrade of openmap 5.0.3 reduces this to 16 meters
		Length tolerance = MeasurementUtil.createLengthInMeters(28);
		Length jumpLength = MeasurementUtil.createLengthInMeters(100);

		assertCumulativeLengthTolerance(GeoMathTestUtil.ARCTIC_FAR, jumpLength, tolerance);
	}

	@Test
	public void testLengthCumulativeErrorSmallJumps() {
		// 15 meters of 10,000 is about 0.15% error
		// TN-665 upgrade of openmap 5.0.3 reduces this to 1 meter
		Length tolerance = MeasurementUtil.createLengthInMeters(12);
		Length jumpLength = MeasurementUtil.createLengthInMeters(3);

		assertCumulativeLengthTolerance(GeoMathTestUtil.EQUATOR_ORIGIN, jumpLength, tolerance);
	}

	public void testLengthCumulativeErrorMicroJumps() {
		// 15 meters of 10,000 is about 0.15% error
		Length tolerance = MeasurementUtil.createLengthInMeters(0.1);
		Length jumpLength = MeasurementUtil.createLengthInMeters(0.3);

		assertCumulativeLengthTolerance(GeoMathTestUtil.EQUATOR_ORIGIN, jumpLength, tolerance);
	}

	/**
	 * @param jumpLength
	 * @param tolerance
	 */
	private void assertCumulativeLengthTolerance(GeoCoordinate origin, Length jumpLength, Length tolerance) {
		Angle heading = MeasurementUtil.SOUTH_WEST;
		int numberOfIterations = 100;
		GeoCoordinate from = origin;
		Length expectedTotalLength = QuantityMath.create(jumpLength).times(numberOfIterations).getQuantity();
		for (int i = 0; i < numberOfIterations; i++) {
			from = GeoMath.coordinateFrom(from, heading, jumpLength);
		}

		Length totalLength = GeoMath.length(origin, from);

		assertTrue(expectedTotalLength + " expected, but was " + totalLength, QuantityMath.create(expectedTotalLength)
				.equals(totalLength, tolerance));
	}

	/**
	 * @param expectedLength
	 * @param tolerance
	 */
	private void assertLengthRoundTrip(Length expectedLength, final Length tolerance) {
		GeoCoordinate originalLocation = MeasureTestUtil.createRandomGeoCoordinate();
		Angle originalAngle = MeasureTestUtil.createRandomAngle();
		GeoCoordinate calculatedLocation = GeoMath.coordinateFrom(originalLocation, originalAngle, expectedLength);
		Length actualLength = GeoMath.length(originalLocation, calculatedLocation);
		assertTrue(originalLocation + " to " + calculatedLocation + " for length from " + expectedLength + " to "
				+ actualLength, QuantityMath.create(expectedLength).equals(expectedLength, tolerance));
	}

	@Test
	public void testMidpointFromOrigin() {
		GeoCoordinate expected = EARTH_ORIGIN_EAST_HALFWAY;
		GeoCoordinate midpoint = GeoMath.midpoint(EARTH_ORIGIN, EARTH_ORIGIN_EAST);
		MeasureTestUtil.assertEquals(expected, midpoint);
	}

	/**
	 * 
	 * At the equator, an arc-second of longitude approximately equals an arc-second of latitude,
	 * which is 1/60th of a nautical mile (or 101.27 feet or 30.87 meters).
	 * 
	 * http://www.esri.com/news/arcuser/0400/wdside.html
	 */
	@Test
	public void testArcLengthAtEquator() {
		assertAngleFromLength(30.87, 1.0);
	}

	/**
	 * At 49 degrees north latitude, along the northern boundary of the Concrete sheet, an
	 * arc-second of longitude equals 30.87 meters * 0.6561 (cos 49°) or 20.250 meters.
	 * 
	 * http://www.esri.com/news/arcuser/0400/wdside.html
	 * 
	 * @see #assertAngleFromLength(double, double)
	 */
	@Test
	public void testArcLengthAt49North() {
		assertAngleFromLength(20.250, 1.0);
	}

	/**
	 * Ideally, a three arc-second grid cell on the north edge of the Concrete sheet measures 60.75
	 * meters along its north side http://www.esri.com/news/arcuser/0400/wdside.html
	 */
	@Test
	public void test3ArcLengthAt49North() {
		assertAngleFromLength(60.75, 3.0);
	}

	@Test
	public void testFarthest() {
		final ArrayList<GeoCoordinate> extremes = Lists.newArrayList(	EARTH_ORIGIN,
																		EARTH_ORIGIN_EAST,
																		ARCTIC_FAR,
																		EARTH_ORIGIN_NORTH,
																		EARTH_ORIGIN_WEST);
		GeoCoordinate farthest = GeoMath.farthest(EARTH_ORIGIN, extremes);
		assertEquals(ARCTIC_FAR, farthest);

		assertEquals("closest", EARTH_ORIGIN, GeoMath.closest(EARTH_ORIGIN, extremes));
		assertEquals("closest", EARTH_ORIGIN_NORTH, GeoMath.closest(EARTH_ORIGIN_NORTH, extremes));
	}

	/**
	 * Arc-seconds of latitude remain nearly constant, while arc-seconds of longitude decrease in a
	 * trigonometric cosine-based fashion as one moves toward the earth's poles.
	 * http://www.esri.com/news/arcuser/0400/wdside.html
	 * 
	 * @param lengthOfArc
	 * @param angleInSeconds
	 */
	private void assertAngleFromLength(double lengthOfArcInMeters, double angleInSeconds) {
		Length lengthOfArc = MeasurementUtil.createLengthInMeters(lengthOfArcInMeters);
		Angle expected = QuantityFactory.getInstance(Angle.class)
				.create(angleInSeconds, USCustomarySystem.SECOND_ANGLE);
		MeasureTestUtil.assertEquals("arc length at equator", expected, GeoMath.arcLength(lengthOfArc, EARTH_ORIGIN));
	}
}
