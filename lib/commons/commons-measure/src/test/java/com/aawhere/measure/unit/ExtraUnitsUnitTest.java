/**
 *
 */
package com.aawhere.measure.unit;

import static com.aawhere.measure.unit.ExtraUnits.KILOMETERS_PER_HOUR;
import static javax.measure.unit.MetricSystem.METRES_PER_SECOND;
import static org.junit.Assert.assertEquals;

import javax.measure.quantity.Velocity;

import org.junit.Test;

import com.aawhere.measure.Latitude;
import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.test.TestUtil;

/**
 * @author Aaron Roller
 * 
 */
public class ExtraUnitsUnitTest {

	@Test
	public void testKph() {

		double valueFromGoogle = 0.277777778;
		Velocity expectedKph = MeasurementQuantityFactory.getInstance(Velocity.class).create(	valueFromGoogle,
																								METRES_PER_SECOND);
		Velocity actualKph = MeasurementQuantityFactory.getInstance(Velocity.class).create(1, KILOMETERS_PER_HOUR);
		assertEquals(expectedKph.doubleValue(METRES_PER_SECOND), actualKph.doubleValue(METRES_PER_SECOND), 0.0000001);
	}

	@Test
	public void testRatio() {
		double value = 1;
		Ratio ratio = MeasurementQuantityFactory.getInstance(Ratio.class).create(value, ExtraUnits.RATIO);
		double expected = 100d;
		double actual = ratio.doubleValue(ExtraUnits.PERCENT);

		TestUtil.assertDoubleEquals(expected, actual);
	}

	/*
	 * A failed test would resort to radians (rad) instead of the overriden degrees (deg).
	 */
	@Test
	public void testBaseUnits() {
		assertEquals(ExtraUnits.DECIMAL_DEGREES, MeasurementUtil.getNormalizedUnit(Latitude.class));
		assertEquals(ExtraUnits.DECIMAL_DEGREES_LATITUDE, MeasurementUtil.getNormalizedUnit(Latitude.class));
	}
}
