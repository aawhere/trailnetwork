/**
 *
 */
package com.aawhere.measure;

import static org.junit.Assert.*;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.unit.USCustomarySystem;

import org.junit.Test;

import com.aawhere.jts.geom.JtsGeometryFactory;
import com.aawhere.measure.calc.GeoMath;
import com.aawhere.measure.calc.GeoMathTestUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.test.TestUtil;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

/**
 * Tests {@link JtsMeasureUtil}
 * 
 * @author Brian Chapman
 * 
 */
public class JtsMeasureUtilUnitTest {

	public static final Length METERS_IN_ONE_DEG_LON = MeasurementUtil.createLengthInMeters(111320d);
	private final Latitude lat0 = MeasurementUtil.createLatitude(0d);
	private final Latitude lat90 = MeasurementUtil.createLatitude(90d);
	private final Latitude lat70 = MeasurementUtil.createLatitude(70d);
	private final Latitude lat60 = MeasurementUtil.createLatitude(60d);

	@Test
	public void testBufferLengthForOneDegree() {
		assertBufferLengthInDecimalDegrees(METERS_IN_ONE_DEG_LON, lat0, MeasurementUtil.createAngleInDegrees(1d));
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.JtsMeasureUtil#bufferLengthInDecimalDegrees(javax.measure.quantity.Length, com.aawhere.measure.Latitude)}
	 * .
	 */
	@Test
	public void testBufferLengthInDecimalDegrees() {
		assertBufferLengthInDecimalDegrees(METERS_IN_ONE_DEG_LON, lat0, MeasurementUtil.createAngleInDegrees(1d));
		assertBufferLengthInDecimalDegrees(METERS_IN_ONE_DEG_LON, lat60, MeasurementUtil.createAngleInDegrees(1.5));
		assertBufferLengthInDecimalDegrees(	METERS_IN_ONE_DEG_LON,
											lat70,
											MeasurementUtil.createAngleInDegrees(1.961911d));
		// Moral of the story, don't use this near the poles!
		assertBufferLengthInDecimalDegrees(	METERS_IN_ONE_DEG_LON,
											lat90,
											MeasurementUtil.createAngleInDegrees(8.165657168136565e15d));
	}

	private void assertBufferLengthInDecimalDegrees(Length buffer, Latitude latitude, Angle expectedDegrees) {
		Angle actual = JtsMeasureUtil.angleFromLength(buffer, latitude);
		TestUtil.assertDoubleEquals(expectedDegrees.doubleValue(USCustomarySystem.DEGREE_ANGLE),
									actual.doubleValue(USCustomarySystem.DEGREE_ANGLE));
	}

	@Test
	public void testLine() {
		GeoCoordinate origin = GeoMathTestUtil.EARTH_ORIGIN;
		GeoCoordinate east = GeoMathTestUtil.EARTH_ORIGIN_EAST;
		LineString actual = JtsMeasureUtil.line(origin, east);
		assertEquatorLine(GeoMath.length(origin, east), actual);
	}

	@Test
	public void testLongerLine() {
		GeoCoordinate origin = GeoMathTestUtil.EARTH_ORIGIN;
		LineString actual = JtsMeasureUtil.line(origin, GeoMathTestUtil.EARTH_ORIGIN_EAST_HALFWAY, 2.0);
		assertEquatorLine(GeoMath.length(origin, GeoMathTestUtil.EARTH_ORIGIN_EAST), actual);
	}

	@Test
	public void testShorterLine() {
		GeoCoordinate origin = GeoMathTestUtil.EARTH_ORIGIN;
		LineString actual = JtsMeasureUtil.line(origin, GeoMathTestUtil.EARTH_ORIGIN_EAST, 0.5);
		assertEquatorLine(GeoMath.length(origin, GeoMathTestUtil.EARTH_ORIGIN_EAST_HALFWAY), actual);
	}

	/**
	 * @see JtsMeasureUtil#line(GeoCoordinate, Angle, Length)
	 * @see JtsMeasureUtil#perpendicularLine(GeoCoordinate, Angle, Length)
	 * 
	 *      Simple test around the origin of the earth where the line is perpendicular to north so
	 *      it is parallel to the equator.
	 * 
	 */
	@Test
	public void testLinesAroundMidpoint() {

		Length expectedLength = MeasurementUtil.createLengthInMeters(50);

		LineString line = JtsMeasureUtil.line(GeoMathTestUtil.EARTH_ORIGIN, MeasurementUtil.EAST, expectedLength);
		assertEquatorLine(expectedLength, line);

		LineString perpendicularLine = JtsMeasureUtil.perpendicularLine(GeoMathTestUtil.EARTH_ORIGIN,
																		MeasurementUtil.NORTH,
																		expectedLength);
		assertEquatorLine(expectedLength, perpendicularLine);

	}

	/**
	 * @see GeoMath#midpoint(GeoCoordinate, GeoCoordinate)
	 * 
	 * @param expectedLength
	 * @param line
	 */
	private void assertEquatorLine(Length expectedLength, LineString line) {
		assertNotNull(line);
		assertEquals(2, line.getNumPoints());
		Point startPoint = line.getStartPoint();
		double originLatitude = GeoMathTestUtil.EARTH_ORIGIN.getLatitude().getInDecimalDegrees();
		assertEquals("perpendicular to north remains on equator", originLatitude, startPoint.getY(), 0.0000001);
		Point endPoint = line.getEndPoint();
		assertEquals("perpendicular to north remains on equator", originLatitude, endPoint.getY(), 0.0000001);

		MeasureTestUtil.assertEquals("length", expectedLength, GeoMath.length(JtsMeasureUtil.convert(startPoint
				.getCoordinate()), JtsMeasureUtil.convert(endPoint.getCoordinate())));
	}

	@Test
	public void testGeoCoordinate() {
		double smallCoordinateValue = Math.pow(10, -GeoCoordinate.TEN_CM_PRECISION);
		double smallerCoordinateValue = smallCoordinateValue * 0.1;
		GeoCoordinate expected = MeasurementUtil.createGeoCoordinate(smallCoordinateValue, smallCoordinateValue);
		assertEquals(	"latitude",
						smallCoordinateValue,
						expected.getLatitude().getInDecimalDegrees(),
						smallerCoordinateValue);
		assertEquals(	"longitude",
						smallCoordinateValue,
						expected.getLongitude().getInDecimalDegrees(),
						smallerCoordinateValue);
		Coordinate convert = JtsMeasureUtil.convert(expected);
		assertEquals("x", smallCoordinateValue, convert.x, smallerCoordinateValue);
		assertEquals("y", smallCoordinateValue, convert.y, smallerCoordinateValue);
		GeoCoordinate actual = JtsMeasureUtil.convert(convert);
		MeasureTestUtil.assertEquals(expected, actual);
	}

	@Test
	public void testPointBoundingBox() {
		GeoCoordinate expected = GeoMathTestUtil.EARTH_ORIGIN;
		Point point = JtsMeasureUtil.point(expected);
		BoundingBox boundingBox = JtsMeasureUtil.boundingBox(point);
		MeasureTestUtil.assertEquals(expected, boundingBox.getNorthEast());
		MeasureTestUtil.assertEquals(expected, boundingBox.getSouthWest());
	}

	@Test
	public void testEmptyGeometryBoundingBox() {
		LineString lineString = JtsGeometryFactory.getDefaultfactory().createLineString(new Coordinate[] {});
		BoundingBox boundingBox = JtsMeasureUtil.boundingBox(lineString);
		assertNull("emtpy geometry can't produce a bounding box", boundingBox);
	}

	/**
	 * This tests the Polygon portion of the conversion.
	 * 
	 */
	@Test
	public void testLineBoundingBox() {
		GeoCoordinate start = GeoMathTestUtil.EARTH_ORIGIN_SOUTH_WEST;
		GeoCoordinate finish = GeoMathTestUtil.EARTH_ORIGIN_NORTH_EAST;
		assertBoundingBoxFromLine(start, finish);
	}

	@Test
	public void testEnvelope() {
		GeoCoordinate midpoint = GeoMathTestUtil.EARTH_ORIGIN;
		Length length = QuantityMath.create(GeoMath.length(midpoint, GeoMathTestUtil.EARTH_ORIGIN_EAST)).times(2)
				.getQuantity();
		Envelope envelope = JtsMeasureUtil.envelope(midpoint, length);
		assertTrue("midpoint", JtsMeasureUtil.contains(midpoint, envelope));
		assertTrue("east", JtsMeasureUtil.contains(GeoMathTestUtil.EARTH_ORIGIN_EAST, envelope));
		assertTrue("north", JtsMeasureUtil.contains(GeoMathTestUtil.EARTH_ORIGIN_NORTH, envelope));
		assertTrue("south", JtsMeasureUtil.contains(GeoMathTestUtil.EARTH_ORIGIN_SOUTH, envelope));
		assertTrue("west", JtsMeasureUtil.contains(GeoMathTestUtil.EARTH_ORIGIN_WEST, envelope));
		assertTrue("northeast", JtsMeasureUtil.contains(GeoMathTestUtil.EARTH_ORIGIN_NORTH_EAST, envelope));
		assertTrue("southwest", JtsMeasureUtil.contains(GeoMathTestUtil.EARTH_ORIGIN_SOUTH_WEST, envelope));
		assertFalse("way off", JtsMeasureUtil.contains(GeoMathTestUtil.ARCTIC_FAR, envelope));
	}

	/**
	 * @param start
	 * @param finish
	 */
	private void assertBoundingBoxFromLine(GeoCoordinate start, GeoCoordinate finish) {
		LineString line = JtsMeasureUtil.line(start, finish);
		assertBoundingBoxFromLine(start, finish, line);
		assertBoundingBoxFromLine(start, finish, (LineString) line.reverse());
	}

	/**
	 * @param min
	 * @param max
	 * @param line
	 */
	private void assertBoundingBoxFromLine(GeoCoordinate min, GeoCoordinate max, LineString line) {
		BoundingBox boundingBox = JtsMeasureUtil.boundingBox(line);
		MeasureTestUtil.assertEquals(min, boundingBox.getMin());
		MeasureTestUtil.assertEquals(max, boundingBox.getMax());
	}

	/** This tests the special bounding box that is just a line since there is no width. */
	@Test
	public void testHorizontalLineBoundingBox() {
		assertBoundingBoxFromLine(GeoMathTestUtil.EARTH_ORIGIN, GeoMathTestUtil.EARTH_ORIGIN_EAST);
	}

	/** This tests the special bounding box that is just a line since there is no width. */
	@Test
	public void testVerticalLineBoundingBox() {

		assertBoundingBoxFromLine(GeoMathTestUtil.EARTH_ORIGIN, GeoMathTestUtil.EARTH_ORIGIN_NORTH);
	}

}
