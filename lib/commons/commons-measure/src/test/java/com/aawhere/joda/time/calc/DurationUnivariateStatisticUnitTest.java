/**
 * 
 */
package com.aawhere.joda.time.calc;

import static org.junit.Assert.*;

import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.rank.Max;
import org.apache.commons.math.stat.descriptive.rank.Min;
import org.apache.commons.math.stat.descriptive.summary.Sum;
import org.joda.time.Duration;
import org.junit.Test;

/**
 * @see DurationUnivariateStatistic
 * @author Aaron Roller
 * 
 */
public class DurationUnivariateStatisticUnitTest {

	/**
	 * 
	 */
	private static final int BASE_MILLIS = 2500;
	private final Duration BASE = new Duration(BASE_MILLIS);

	/** The primary test making sure the pieces are put together. OThers just test the basics. */
	@Test
	public void testSum() {

		DurationUnivariateStatistic<Sum> calculator = DurationUnivariateStatistic.newSum();
		assertFalse(calculator.hasResult());
		calculator.increment(BASE);
		assertEquals(BASE_MILLIS, calculator.getResult().getMillis());
		calculator.increment(BASE);
		assertEquals(BASE_MILLIS * 2, calculator.getResult().getMillis());
	}

	@Test
	public void testMean() {
		DurationUnivariateStatistic<Mean> calculator = DurationUnivariateStatistic.newMean();
		assertNotNull(calculator);
		// assuming mean works
	}

	@Test
	public void testMin() {
		DurationUnivariateStatistic<Min> calculator = DurationUnivariateStatistic.newMin();
		assertNotNull(calculator);
	}

	@Test
	public void testMax() {
		DurationUnivariateStatistic<Max> calculator = DurationUnivariateStatistic.newMax();
		assertNotNull(calculator);
	}

}
