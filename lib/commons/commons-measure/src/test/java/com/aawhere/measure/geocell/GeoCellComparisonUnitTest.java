/**
 * 
 */
package com.aawhere.measure.geocell;

import static com.aawhere.measure.geocell.GeoCellTestUtil.*;
import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;

/**
 * Tests reference standard OGC test cases demonstrated well by JTS (Run 7: TestRelateAA).
 * http://www.vividsolutions.com/jts/tests/index.html
 * 
 * @author aroller
 * 
 */
public class GeoCellComparisonUnitTest {

	/**
	 * Case 1: A/A-1-1: same polygons [dim(2){A.A.Int = B.A.Int}, dim(1){A.A.Bdy.SP-EP =
	 * B.A.Bdy.SP-EP}]
	 */
	@Test
	public void testSameCells() {

		GeoCellComparison comparison = GeoCellComparison.left(A_THRU_F_WITH_PARENTS).right(A_THRU_F_WITH_PARENTS)
				.build();
		Boolean expectedEquals = true;
		Boolean expectedDisjoint = false;
		Boolean expectedIntersects = true;
		Boolean expectedWithin = true;
		Boolean expectedContains = true;
		assertComparison(	comparison,
							expectedEquals,
							expectedDisjoint,
							expectedIntersects,
							expectedWithin,
							expectedContains);
	}

	/**
	 * @param comparison
	 * @param expectedEquals
	 * @param expectedDisjoint
	 * @param expectedIntersects
	 * @param expectedWithin
	 * @param expectedContains
	 */
	private void assertComparison(GeoCellComparison comparison, Boolean expectedEquals, Boolean expectedDisjoint,
			Boolean expectedIntersects, Boolean expectedWithin, Boolean expectedContains) {

		assertEquals("equals " + comparison, expectedEquals, comparison.equals());
		assertEquals("disjoint " + comparison, expectedDisjoint, comparison.disjoint());
		assertEquals("intersects " + comparison, expectedIntersects, comparison.intersects());
		assertEquals("within " + comparison, expectedWithin, comparison.within());
		assertEquals("contains " + comparison, expectedContains, comparison.contains());
		assertEquals("equals within tolerance", expectedContains && expectedWithin, comparison.equalsWithinTolerance());
	}

	/**
	 * As in TN-465, activities of different resolutions cause a false positive. A longer activity
	 * has reduced resolutions to avoid too many cells. An attempt also must "contain" a course for
	 * it to pass. So a "contains" query must reduce resolutions to match the least common
	 * denominator.
	 * 
	 */
	@Test
	public void testDifferentResolutions() {
		assertTrue("a big cell should contain a small", GeoCellComparison.left(A).right(AB).contains());
	}

	/**
	 * Case 5: A/A-2: different polygons [dim(2){A.A.Int = B.A.Ext}]
	 */
	@Test
	public void testDifferentCells() {
		GeoCellComparison comparison = GeoCellComparison.left(A).right(B).build();
		assertComparison(comparison, false, true, false, false, false);
	}

	/**
	 * Case 33: A/A-5-2-1: a polygon containing another polygon [dim(2){A.A.Int = B.A.Int},
	 * dim(0){A.A.Bdy.CP = B.A.Bdy.CP}]
	 */
	@Test
	public void testContains() {
		GeoCellComparison comparison = GeoCellComparison.left(A, B, C).right(B, C).build();
		assertComparison(comparison, false, false, true, false, true);
	}

	@Test
	public void testContainsWithRatioCompleteTolerance() {
		GeoCellComparison comparison = GeoCellComparison.left(A).right(B, C, D).tolerance(MeasurementUtil.RATIO_ONE)
				.build();
		assertTrue("complete tolerance will forgive this completely wrong situation", comparison.contains());

	}

	@Test
	public void testContainsWithRatioNoTolerance() {
		GeoCellComparison comparison = GeoCellComparison.left(A, B, C).right(B, C, D)
				.tolerance(MeasurementUtil.RATIO_ZERO).build();
		assertFalse("must exactly contain", comparison.contains());

	}

	/** 30% tolerance is not enough for 33% so this should fail to pass */
	@Test
	public void testContainsWithRatioLowTolerance() {
		GeoCellComparison comparison = GeoCellComparison.left(A, B, C).right(B, C, D)
				.tolerance(MeasurementUtil.ratio(32, 100)).build();
		assertFalse("our tolerance level is lower than the 1/3 off", comparison.contains());

	}

	@Test
	public void testContainsWithRatioLowToleranceWithMinimum() {
		GeoCellComparison comparison = GeoCellComparison.left(A, B, C).right(B, C, D)
				.tolerance(MeasurementUtil.ratio(32, 100)).tolerance(1).build();
		assertTrue(	"our tolerance level is lower than the 1/3 off, but we give a min of 1 which is enough",
					comparison.contains());

	}

	/** 40% is more than 1/3rd tolerance so this should pass. when 1 is missing out of 3. */
	@Test
	public void testContainsWithRatioEnoughTolerance() {
		GeoCellComparison comparison = GeoCellComparison.left(A, B, C).right(B, C, D)
				.tolerance(MeasurementUtil.ratio(34, 100)).build();
		assertTrue("only 1/3rd missing so we have enough tolerance", comparison.contains());

	}

	/**
	 * The opposite situation as {@link #testContains()}
	 * 
	 */
	@Test
	public void testWithin() {
		GeoCellComparison comparison = GeoCellComparison.left(B, C).right(A, B, C).build();
		assertComparison(comparison, false, false, true, true, false);
	}

	/**
	 * Case 54: A/A-6-1: a polygon overlapping another polygon [dim(2){A.A.Int = B.A.Int}]
	 */
	@Test
	public void testOverlap() {
		GeoCellComparison comparison = GeoCellComparison.left(A, B, C).right(B, C, D).build();
		assertComparison(comparison, false, false, true, false, false);
	}

	/** Real application of mixed resolution comparison */
	@Test
	public void testTn465MixedResolution() {
		String activity2ps1451Res8 = "e e0 e01 e01b e01b3 e01b36 e01b36c e01b36cf e01b36d e01b36da e01b36db e01b36de e01b36df e01b36f e01b36f1 e01b36f4 e01b37 e01b378 e01b378a e01b378b";
		String activity2p230089405Res7 = "e e0 e01 e01b e01b3 e01b33 e01b33f e01b35 e01b35c e01b35e e01b35f e01b36 e01b368 e01b369 e01b36a e01b36c e01b36d e01b36f e01b37 e01b372 e01b373 e01b374 e01b376 e01b378 e01b39 e01b395 e01b397 e01b39d e01b39f e01b3b e01b3b4 e01b3b5 e01b3b6 e01b3bc e01b3bd e01b3e e01b3e2 e01b3e8 e01b3e9 e01b3eb e01b3ee e01b6 e01b60 e01b60a e01b62 e01b620 e01b621 e01b623 e01b629 e01b62a e01b62b e01b68 e01b680 e01b682 e01b688 e01b68a e01b68b e01b6a e01b6a1 e01b6a3 e01b6a8 e01b6a9 e01b6aa e01b9 e01b94 e01b941 e01b943 e01b944 e01b946 e01b94c e01b94e e01b95 e01b957 e01b95d e01b95f e01b96 e01b964 e01b966 e01b967 e01b96c e01b96d e01b97 e01b972 e01b973 e01b975 e01b976 e01b977 e01bc e01bc0 e01bc00 e01bc02 e01bc08";
		assertSmallerLarger(activity2ps1451Res8, activity2p230089405Res7);
	}

	@Test
	@Ignore("TN-608 This test proves the situation doesn't work, but it isn't supposed to. Parents were missing erroneously.")
	public
			void testTn608MixedResolutionNoParents() {
		String twoPs1451ResOther = "e e0 e01 e01b e01b3 e01b36 e01b36cf e01b36da e01b36db e01b36de  e01b36df  e01b36f1  e01b36f4 e01b37 e01b378a e01b378b";
		String twoPs1451Res7 = "                                                            e01b36c e01b36d e01b36f                                 e01b378  ";
		String twoP261526128Res7 = "e01b33f e01b35c e01b35e e01b35f e01b368 e01b369 e01b36a e01b36c e01b36d e01b36f e01b372 e01b373 e01b374 e01b376 e01b378 e01b395 e01b397 e01b39d e01b39f e01b3b4 e01b3b5 e01b3b6 e01b3bc e01b3bd e01b3e2 e01b3e8 e01b3e9 e01b3eb e01b3ee e01b60a e01b620 e01b621 e01b623 e01b629 e01b62a e01b62b e01b680 e01b682 e01b688 e01b68a e01b68b e01b6a1 e01b6a3 e01b6a8 e01b6a9 e01b6aa e01b941 e01b943 e01b944 e01b946 e01b94c e01b94e e01b957 e01b95d e01b95f e01b964 e01b966 e01b972 e01b973 e01b975 e01b976 e01b977 e01bc00 e01bc02 e01bc08";
		assertSmallerLarger(twoPs1451Res7 + twoPs1451ResOther, twoP261526128Res7);
	}

	/** Real application of tolerance. */
	@Test
	public void testGc3333992Gc101135585() {
		String gc190327477 = "8 8e 8e6 8e62 8e62f 8e62f3 8e62f35 8e62f357 8e62f35c 8e62f35d 8e62f35e 8e62f37 8e62f373 8e62f374 8e62f376 8e62f379 8e62f37b 8e62f37f 8e62f3d          8e62f3d1 8e62f3d3 8e62f3d5 8e62f3d6 8e62f3d7 8e62f3d9 8e62f3dc 8e62f4 8e62f4a 8e62f4aa 8e62f6 8e62f60 8e62f600 8e62f602 8e62f603 8e62f609 8e62f60b 8e62f60c 8e62f60e 8e62f62 8e62f624 8e62f626 8e62f628 8e62f629 8e62f62a 8e62f62c";
		String gc101135585 = "8 8e 8e6 8e62 8e62f 8e62f3 8e62f35 8e62f357 8e62f35c 8e62f35d 8e62f35e 8e62f37 8e62f373 8e62f374 8e62f376 8e62f379 8e62f37b 8e62f37f 8e62f3d 8e62f3d0 8e62f3d1 8e62f3d3 8e62f3d5 8e62f3d6 8e62f3d7 8e62f3d9 8e62f3dc 8e62f4 8e62f4a 8e62f4aa 8e62f6 8e62f60 8e62f600 8e62f602 8e62f603 8e62f609 8e62f60b 8e62f60c 8e62f60e 8e62f62 8e62f624 8e62f626 8e62f628 8e62f629 8e62f62a 8e62f62c";
		// the second one is larger technically, although they should be equal
		assertSmallerLarger(gc190327477, gc101135585);
		assertComparison(comparison(gc101135585, gc190327477), false, false, true, true, false);
		assertComparison(comparison(gc101135585, gc190327477, 1), false, false, true, true, true);
	}

	/**
	 * @param smaller
	 * @param larger
	 */
	private void assertSmallerLarger(String smaller, String larger) {
		final GeoCellComparison smallerToLargerComparison = comparison(smaller, larger);
		assertComparison(smallerToLargerComparison, false, false, true, false, true);
	}

	private GeoCellComparison comparison(String left, String right) {
		return comparison(left, right, null);
	}

	/**
	 * @param left
	 * @param right
	 * @return
	 */
	private GeoCellComparison comparison(String left, String right, Integer tolerance) {
		GeoCells smallerCells = GeoCells.valueOf(left);
		GeoCells largerCells = GeoCells.valueOf(right);
		final GeoCellComparison smallerToLargerComparison = GeoCellComparison.left(largerCells).right(smallerCells)
				.tolerance((tolerance == null) ? 0 : tolerance).build();
		return smallerToLargerComparison;
	}
}
