/**
 * 
 */
package com.aawhere.measure.geocell;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * @author brian
 * 
 */
public class JtsGeoCellUtilUnitTest {

	@Test
	public void testBoxCoordinates() {
		GeoCells geocells = GeoCells.create().addAll(GeoCellTestUtil.getSampleGeoCells()).build();
		List<Coordinate> coords = JtsGeoCellUtil.boxCoordinates(geocells);
		assertEquals(12, coords.size());
	}

	@Test
	public void testBoxCoordinatesEmpty() {
		GeoCells geocells = GeoCells.create().build();
		List<Coordinate> coords = JtsGeoCellUtil.boxCoordinates(geocells);
		assertTrue(coords.isEmpty());
	}

	@Test
	public void testCenterCorrdinates() {
		GeoCells geocells = GeoCells.create().addAll(GeoCellTestUtil.getSampleGeoCells()).build();
		List<Coordinate> coords = JtsGeoCellUtil.centerCoordinates(geocells);
		assertEquals(3, coords.size());
	}

	@Test
	public void testCenterCoordinatesEmpty() {
		GeoCells geocells = GeoCells.create().build();
		List<Coordinate> coords = JtsGeoCellUtil.centerCoordinates(geocells);
		assertTrue(coords.isEmpty());
	}
}
