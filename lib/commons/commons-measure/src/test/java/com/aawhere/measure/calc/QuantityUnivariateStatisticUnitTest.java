/**
 *
 */
package com.aawhere.measure.calc;

import static org.junit.Assert.*;

import javax.measure.quantity.Length;
import javax.measure.quantity.QuantityFactory;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.Unit;

import org.apache.commons.math.stat.descriptive.StorelessUnivariateStatistic;
import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.rank.Max;
import org.apache.commons.math.stat.descriptive.rank.Min;
import org.apache.commons.math.stat.descriptive.summary.Sum;
import org.junit.Test;

import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.test.TestUtil;

/**
 * Tests {@link QuantityUnivariateStatistic}s and it's implementations:
 * 
 * {@link MinQuantityCalculator} {@link MaxQuantityCalculator}
 * 
 * @author Aaron Roller
 * 
 */
public class QuantityUnivariateStatisticUnitTest {

	private static final int[] HIGH_FIRST = { 8, 3, 5 };
	private static final int[] LOW_FIRST = { 3, 5, 8 };

	@Test
	public void testMinCalculatorLowFirst() {

		QuantityUnivariateStatistic<Length, Min> calculator = QuantityUnivariateStatistic.newMin(Length.class);

		int expectedExtremeValue = 3;
		assertStatistic(calculator, LOW_FIRST, expectedExtremeValue);
	}

	@Test
	public void testMinCalculatorHighFirst() {

		QuantityUnivariateStatistic<Length, Min> calculator = QuantityUnivariateStatistic.newMin(Length.class);

		int expectedExtremeValue = 3;
		assertStatistic(calculator, HIGH_FIRST, expectedExtremeValue);
	}

	@Test
	public void testMaxCalculatorLowFirst() {

		QuantityUnivariateStatistic<Length, Max> calculator = QuantityUnivariateStatistic.newMax(Length.class);
		int expectedExtremeValue = 8;
		assertStatistic(calculator, LOW_FIRST, expectedExtremeValue);
	}

	@Test
	public void testMaxCalculatorHighFirst() {

		QuantityUnivariateStatistic<Length, Max> calculator = QuantityUnivariateStatistic.newMax(Length.class);
		int expectedExtremeValue = 8;
		assertStatistic(calculator, HIGH_FIRST, expectedExtremeValue);
	}

	@Test
	public void testSum() {

		QuantityUnivariateStatistic<Length, Sum> calculator = QuantityUnivariateStatistic.newSum(Length.class);
		int expectedExtremeValue = 16;
		assertStatistic(calculator, HIGH_FIRST, expectedExtremeValue);
		calculator = QuantityUnivariateStatistic.newSum(Length.class);
		assertStatistic(calculator, LOW_FIRST, expectedExtremeValue);
	}

	@Test
	public void testMean() {
		double expectedMean = 16.0 / 3.0;
		QuantityUnivariateStatistic<Length, Mean> calculator = QuantityUnivariateStatistic.newMean(Length.class);
		assertStatistic(calculator, HIGH_FIRST, expectedMean);
	}

	@Test
	public void testHasResult() {
		QuantityUnivariateStatistic<Length, Sum> calculator = QuantityUnivariateStatistic.newSum(Length.class);
		assertTrue("sum is zero by default", calculator.hasResult());
		Length length = MeasurementUtil.createLengthInMeters(TestUtil.generateRandomDouble());
		calculator.increment(length);
		assertTrue(calculator.hasResult());
		assertEquals(length, calculator.getResult());
	}

	private <U extends StorelessUnivariateStatistic> void assertStatistic(
			QuantityUnivariateStatistic<Length, U> calculator, int[] values, double expectedExtremeValue) {
		QuantityFactory<Length> factory = MeasurementQuantityFactory.getInstance(Length.class);
		Unit<Length> unit = MetricSystem.METRE;
		Length expectedExtreme = factory.create(expectedExtremeValue, unit);
		for (int i = 0; i < values.length; i++) {
			int j = values[i];
			calculator.increment(factory.create(j, unit));
		}

		assertEquals(expectedExtreme, calculator.getResult());
	}
}
