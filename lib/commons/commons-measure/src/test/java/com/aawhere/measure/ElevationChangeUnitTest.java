/**
 *
 */
package com.aawhere.measure;

import static org.junit.Assert.*;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;
import javax.measure.unit.UnitConverter;

import org.junit.Test;

import com.aawhere.measure.unit.ExtraUnits;

/**
 * @author roller
 * 
 */
public class ElevationChangeUnitTest
		extends ExtraQuantityUnitTest<ElevationChange> {

	static Unit<ElevationChange> FOOT = ExtraUnits.FOOT_CHANGE;
	static Unit<ElevationChange> METER = ExtraUnits.METER_CHANGE;
	/**
	 *
	 */
	private static final double REASONABLE_PRECISION = 0.00001;

	@Test
	public void testLengthEquality() {
		final Double value = 3325d;
		ElevationChange expected = createQuantity(value, getNormalizedUnit());
		Length actual = MeasurementUtil.createLengthInMeters(value);
		assertEquals("even though they are different measurements, their units are compatible", expected, actual);

	}

	@Test
	public void testUnitConversion() {

		UnitConverter converterTo = METER.getConverterTo(FOOT);
		assertNotNull(converterTo);
		Double inMeters = 3.0;
		Double inFeet = MetricSystem.METRE.getConverterTo(USCustomarySystem.FOOT).convert(inMeters);
		Double inFeetChange = converterTo.convert(inMeters);
		assertEquals(inFeet, inFeetChange);
	}

	@Test
	public void testElevationChangeEvenMore() {

		double throwElevationChangeMetersValue = 1700;
		double throwElevationChangeKmValue = throwElevationChangeMetersValue / 1000;
		double throwElevationChangeFeetValue = 5577.427821;
		ElevationChange throwElevationChangeMeters = MeasurementQuantityFactory.getInstance(ElevationChange.class)
				.create(throwElevationChangeMetersValue, METER);
		assertNotNull(throwElevationChangeMeters);
		assertEquals(	throwElevationChangeMetersValue,
						throwElevationChangeMeters.doubleValue(METER),
						REASONABLE_PRECISION);

		UnitConverter converter = METER.getConverterTo(MetricSystem.KILO(METER));
		assertEquals(	throwElevationChangeKmValue,
						converter.convert(throwElevationChangeMeters.getValue().doubleValue()),
						REASONABLE_PRECISION);
		converter = METER.getConverterToAny(FOOT);
		assertEquals(	throwElevationChangeFeetValue,
						converter.convert(throwElevationChangeMeters.getValue().doubleValue()),
						REASONABLE_PRECISION);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.ExtraQuantityUnitTest#getQuantityClass()
	 */
	@Override
	protected Class<ElevationChange> getQuantityClass() {
		return ElevationChange.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.ExtraQuantityUnitTest#getNormalizedUnit()
	 */
	@Override
	protected Unit<ElevationChange> getNormalizedUnit() {
		return METER;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.ExtraQuantityUnitTest#getUnit()
	 */
	@Override
	protected Unit<ElevationChange> getUnit() {
		return FOOT;
	}
}
