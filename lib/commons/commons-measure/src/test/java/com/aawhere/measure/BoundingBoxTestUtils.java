/**
 * 
 */
package com.aawhere.measure;

import static com.aawhere.measure.calc.GeoMathTestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Assert;

import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Lists;

/**
 * @author Brian Chapman
 * 
 */
public class BoundingBoxTestUtils {

	public static final String LARGE_SQAURE_FROM_ORIGIN = "0.0,0.0|3.0,3.0";
	public static final String MEDIUM_SQAURE_NORTH_EAST_OF_ORIGIN = "1.0,1.0|3.0,3.0";
	public static final String MEDIUM_SQUARE_FROM_ORIGIN = "0.0,0.0|2.0,2.0";
	public static final String MEDIUM_SQUARE_AROUND_ORIGIN = "-2.0,-2.0|2.0,2.0";
	public static final String SMALL_SQUARE_AROUND_ORIGIN = "-1.0,-1.0|1.0,1.0";

	public static BoundingBox getMaxBounds() {
		return BoundingBox.create().setNorth(Latitude.MAX).setSouth(Latitude.MIN).setWest(Longitude.MIN)
				.setEast(Longitude.MAX).build();
	}

	public static BoundingBox createRandomBoundingBox() {
		return createBoundingBox(	MeasureTestUtil.createRandomGeoCoordinate(),
									MeasureTestUtil.createRandomGeoCoordinate());
	}

	public static BoundingBox createBoundingBox(GeoCoordinate northEast, GeoCoordinate southWest) {
		return new BoundingBox.Builder().setNorthEast(northEast).setSouthWest(southWest).build();
	}

	public static GeoCellBoundingBox createRandomGeoCellBoundingBox() {
		return new GeoCellBoundingBox.Builder().setOther(createRandomBoundingBox()).build();
	}

	public static BoundingBox createBoundingBoxFromBounds(String bounds) {
		try {
			return GeoCellBoundingBox.valueOf(bounds);
		} catch (InvalidBoundsException e) {
			// most of the time during testing this is a coding error.
			throw new RuntimeException(e);
		}
	}

	/**
	 * Given the expected container this will assert it contains the containee.
	 * 
	 * @param container
	 * @param containee
	 */
	public static void assertContains(BoundingBox container, BoundingBox containee) {
		assertContains(container, containee, true);
	}

	/**
	 * 
	 * @param container
	 * @param containee
	 * @param expectedContainment
	 */
	public static void assertContains(BoundingBox container, BoundingBox containee, Boolean expectedContainment) {
		Assert.assertEquals(container + " does not contain " + containee,
							expectedContainment,
							BoundingBoxUtil.contains(container, containee));
	}

	public static BoundingBoxes createRandomBoundingBoxes() {
		int count = TestUtil.generateRandomInt(1, 10);
		ArrayList<BoundingBox> boxes = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			boxes.add(createRandomBoundingBox());
		}
		return BoundingBoxes.create().setAll(boxes).build();
	}

	/**
	 * @param expectedBoundingBox
	 * @param resultsBoundingBox
	 */
	public static void assertEquals(BoundingBox expectedBoundingBox, BoundingBox resultsBoundingBox) {
		assertTrue(	expectedBoundingBox + " != " + resultsBoundingBox,
					BoundingBoxUtil.equals(expectedBoundingBox, resultsBoundingBox));
	}

	/**
	 * @param boundingBox
	 * @param coordinate
	 */
	public static void
			assertContains(Boolean expectedContains, BoundingBox boundingBox, final GeoCoordinate coordinate) {
		Boolean contains = BoundingBoxUtil.contains(boundingBox, coordinate);
		Assert.assertEquals(boundingBox + " contains " + coordinate, expectedContains, contains);
	}

	/**
	 * @return
	 */
	public static BoundingBox extremes() {
		return BoundingBoxUtil.boundingBox(Lists.newArrayList(	MeasurementUtil.MAX_GEO_COORDINATE,
																MeasurementUtil.MIN_GEO_COORDINATE));
	}

	/**
	 * @return
	 */
	public static BoundingBox origin() {
		return BoundingBoxUtil.boundingBox(Lists.newArrayList(EARTH_ORIGIN_SOUTH_WEST, EARTH_ORIGIN_NORTH_EAST));
	}

	public static BoundingBox originNorthEast() {
		return BoundingBoxUtil.boundingBox(Lists.newArrayList(EARTH_ORIGIN, EARTH_ORIGIN_NORTH_EAST));
	}

	public static BoundingBox originSouthWest() {
		return BoundingBoxUtil.boundingBox(Lists.newArrayList(EARTH_ORIGIN, EARTH_ORIGIN_SOUTH_WEST));
	}

	public static BoundingBox originSouthWestHalfway() {
		return BoundingBoxUtil.boundingBox(Lists.newArrayList(EARTH_ORIGIN, EARTH_ORIGIN_SOUTH_WEST));
	}

	/**
	 * Similar to {@link #origin()}, but
	 * 
	 * @return
	 */
	public static BoundingBox originOverlappedFullSouthWestHalfNorthEast() {
		return BoundingBoxUtil
				.boundingBox(Lists.newArrayList(EARTH_ORIGIN_SOUTH_WEST, EARTH_ORIGIN_NORTH_EAST_HALFWAY));
	}

	public static BoundingBox originOverlappedFullNorthEastHalfSouthWest() {
		return BoundingBoxUtil
				.boundingBox(Lists.newArrayList(EARTH_ORIGIN_SOUTH_WEST_HALFWAY, EARTH_ORIGIN_NORTH_EAST));
	}

	public static BoundingBox originNorthEastHalfway() {
		return BoundingBoxUtil.boundingBox(Lists.newArrayList(EARTH_ORIGIN, EARTH_ORIGIN_EAST_HALFWAY));
	}
}
