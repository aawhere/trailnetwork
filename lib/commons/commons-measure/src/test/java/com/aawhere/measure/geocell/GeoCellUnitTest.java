/**
 *
 */
package com.aawhere.measure.geocell;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.lang.ComparatorResult;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.exception.ExceptionUtil;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author brian
 * 
 */
public class GeoCellUnitTest {

	public final String CELL_STRING = "a00b";
	private GeoCell cell;
	public static final String INVALID = "ab32y83o";
	public static final String INVALID_HIGHLIGHT = "ab32[y]83[o]";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		cell = new GeoCell(CELL_STRING);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.aawhere.measure.geocell.GeoCell#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		assertEquals(CELL_STRING.hashCode(), cell.hashCode());
	}

	@Test
	public void testResolutionDecrement() {
		assertEquals("decrement didn't work", Resolution.FOUR, Resolution.FIVE.lower());
	}

	@Test
	public void testResolutionValueOfByte() {
		Resolution expected = Resolution.ONE;
		assertResolutionValueOfByte(expected);
		assertResolutionValueOfByte(Resolution.THIRTEEN);
	}

	@Test
	public void testResolutionFromByteString() {
		final Resolution expected = Resolution.EIGHT;
		assertEquals(expected, Resolution.fromString(String.valueOf(expected.resolution)));
		assertEquals(expected, Resolution.fromString(expected.name()));
		assertNull(Resolution.fromString(null));
	}

	@Test(expected = InvalidArgumentException.class)
	public void testFromStringAboveRange() {
		Resolution.fromString(String.valueOf(Resolution.highest().higher().resolution));
	}

	@Test(expected = InvalidArgumentException.class)
	public void testFromStringBelowRange() {
		Resolution.fromString(String.valueOf(Resolution.lowest().lower().resolution));
	}

	/**
	 * @param expected
	 */
	private void assertResolutionValueOfByte(Resolution expected) {
		Resolution actual = Resolution.valueOf(expected.resolution);
		assertEquals("value of is not working", expected, actual);
	}

	@Test(expected = InvalidArgumentException.class)
	public void testResolutionDecrementZero() {
		Resolution.ONE.lower();
	}

	@Test
	public void testTrim() {
		GeoCell cell = GeoCellUtil.geoCellValidated(" abcd\t");
		assertEquals("abcd", cell.getCellAsString());
	}

	@Test
	public void testUpperCase() {
		GeoCell cell = GeoCellUtil.geoCellValidated("123ABC");
		assertEquals("123abc", cell.getCellAsString());
	}

	/**
	 * Test method for {@link com.aawhere.measure.geocell.GeoCell#getCellAsString()}.
	 */
	@Test
	public void testGetCellAsString() {
		assertEquals(CELL_STRING, cell.getCellAsString());
	}

	/**
	 * Test method for {@link com.aawhere.measure.geocell.GeoCell#resolution()}.
	 */
	@Test
	public void testGetResolution() {
		assertEquals(Integer.valueOf(CELL_STRING.length()), cell.getResolution().asInteger());
	}

	/**
	 * Test method for {@link com.aawhere.measure.geocell.GeoCell#equals(java.lang.Object)}.
	 * 
	 * @throws InvalidGeoCellException
	 */
	@Test
	public void testEqualsObject() throws InvalidGeoCellException {
		assertEquals(cell, new GeoCell(CELL_STRING));
	}

	@Test
	public void testJaxb() {
		JAXBTestUtil<GeoCell> util = JAXBTestUtil.create(cell).build();
		util.assertContains(CELL_STRING);
		assertEquals(cell, util.getActual());
	}

	@Test
	public void testCompoarison() {
		assertSort("a", "b");
		assertSort("a", "ab");
		assertSort("a", "aa");
	}

	public void assertSort(String expectedLessThan, String expectedGreaterThan) {
		GeoCell less = new GeoCell(expectedLessThan);
		GeoCell more = new GeoCell(expectedGreaterThan);
		String message = less + " vs. " + more;
		assertEquals(message, ComparatorResult.LESS_THAN.getValue(), less.compareTo(more));
		assertEquals(message, ComparatorResult.GREATER_THAN.getValue(), more.compareTo(less));

	}

	@Test
	public void testHighlightInvalidCharacters() {

		String actual = GeoCellUtil.highlightInvalidCharacters(INVALID);

		assertEquals(INVALID_HIGHLIGHT, actual);
	}

	@Test
	public void testInvalid() {
		try {
			GeoCellUtil.geoCellValidated(INVALID);
			fail("where's the exception?");
		} catch (InvalidGeoCellException e) {
			TestUtil.assertContains(e.getMessage(), INVALID);
		}
	}

	@Test
	public void testStatusCode() {
		InvalidGeoCellException invalidGeoCellException = new InvalidGeoCellException("junk");
		HttpStatusCode responseCode = ExceptionUtil.responseCode(invalidGeoCellException);
		assertEquals(HttpStatusCode.BAD_REQUEST, responseCode);
	}

}
