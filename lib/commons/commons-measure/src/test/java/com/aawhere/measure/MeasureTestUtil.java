package com.aawhere.measure;

import static org.junit.Assert.*;

import javax.measure.quantity.Angle;
import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.junit.Ignore;

import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.measure.calc.QuantityMath;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Range;

/**
 * This is a helper for tests, not to be tested itself.
 * 
 * @author Brian Chapman
 * 
 */
@Ignore
public class MeasureTestUtil {

	public static Length createLength(double value, Unit<Length> unit) {
		return (Length) createQuantity(value, unit, Length.class);
	}

	public static Length createRandomLength() {
		return createLength(TestUtil.generateRandomDouble(), USCustomarySystem.METER);
	}

	public static Angle createAngle(double value, Unit<Angle> unit) {
		return (Angle) createQuantity(value, unit, Angle.class);
	}

	public static Angle createRandomAngle() {
		return createAngle(TestUtil.generateRandomDouble(), USCustomarySystem.DEGREE_ANGLE);
	}

	private static <Q extends Quantity<Q>> Quantity<Q> createQuantity(double value, Unit<Q> unit, Class<Q> clazz) {
		return MeasurementQuantityFactory.getInstance(clazz).create(value, unit);
	}

	/**
	 * Asserts equality as defined by {@link QuantityMath#equals(Quantity)}
	 * 
	 * @param left
	 * @param right
	 */
	public static <Q extends Quantity<Q>> void assertEquals(Q left, Q right) {
		assertEquals("", left, right);
	}

	public static void assertEquals(GeoCoordinate left, GeoCoordinate right) {
		assertEquals("latitude", left.getLatitude(), right.getLatitude());
		assertEquals("longitude", left.getLongitude(), right.getLongitude());
	}

	public static <Q extends Quantity<Q>> void assertEquals(String messagePrefix, Q left, Q right) {
		@SuppressWarnings("unchecked")
		Unit<Q> unit = MeasurementUtil.getNormalizedUnit(left.getClass());
		assertTrue(messagePrefix + " normalized values: " + left.doubleValue(unit) + " != " + right.doubleValue(unit)
				+ " for " + left.toString() + " and " + right.toString(), new QuantityMath<Q>(left).equals(right));
	}

	public static Elevation createElevation(double value, Unit<Length> unit) {
		return MeasurementUtil.createElevation(createLength(value, unit));
	}

	@Deprecated
	public static BoundingBox createRandomBoundingBox() {
		return BoundingBoxTestUtils.createRandomBoundingBox();
	}

	@Deprecated
	public static GeoCellBoundingBox createRandomGeoCellBoundingBox() {
		return BoundingBoxTestUtils.createRandomGeoCellBoundingBox();
	}

	@Deprecated
	public static BoundingBox createBoundingBox(GeoCoordinate northEast, GeoCoordinate southWest) {
		return BoundingBoxTestUtils.createBoundingBox(northEast, southWest);
	}

	public static GeoCoordinate createRandomGeoCoordinate() {
		return createGeoCoordinate(createRandomLatitude(), createRandomLongitude());
	}

	public static GeoCoordinate createGeoCoordinate(Latitude lat, Longitude lon) {
		return new GeoCoordinate.Builder().setLatitude(lat).setLongitude(lon).build();
	}

	/**
	 * Given a coordinate this will return the negated providing the coordinate exactly opposite the
	 * earth.
	 * 
	 * @param location
	 * @return
	 */
	public static GeoCoordinate opposite(final GeoCoordinate location) {
		// anything close to zero deserves a jump across the globe
		double latitudeValue = location.getLatitude().getInDecimalDegrees();
		if (latitudeValue < 1) {
			latitudeValue = Latitude.MAX;
		}
		double longitudeValue = location.getLongitude().getInDecimalDegrees();
		if (longitudeValue < 1) {
			longitudeValue = Longitude.MAX;
		}
		GeoCoordinate opposite = GeoCoordinate.create().setLatitude(-latitudeValue).setLongitude(-longitudeValue)
				.build();

		return opposite;
	}

	public static Latitude createRandomLatitude() {
		// let's keep these in reasonable ranges away from the poles
		return MeasurementUtil.createLatitude(TestUtil.generateRandomDouble(-45, 45));
	}

	public static Latitude createLatitude(Angle value) {
		return MeasurementUtil.createLatitude(value.doubleValue(USCustomarySystem.DEGREE_ANGLE));
	}

	public static Longitude createRandomLongitude() {
		// keep the random within range to avoid the meridian
		return MeasurementUtil.createLongitude(TestUtil.generateRandomDouble(-90, 90));
	}

	public static Longitude createLongitude(Angle value) {
		return MeasurementUtil.createLongitude(value.doubleValue(USCustomarySystem.DEGREE_ANGLE));
	}

	/**
	 * @return
	 */
	public static DateTime createRandomTimestamp() {

		return JodaTestUtil.createRandomTimestamp();
	}

	/**
	 * Jaxb may push the GeoQuantities through a Formatter that reduces the precision of the value.
	 * If that behavior is correct, this method makes comparing {@link GeoCoordinate}s easier.
	 * 
	 * @param expected
	 * @param actual
	 */
	public static void
			assertGeoCoordinateEqualsAfterJaxB(GeoCoordinate expected, GeoCoordinate actual, double precision) {
		double expectedLat = expected.getLatitude().getInDecimalDegrees();
		double expectedLon = expected.getLongitude().getInDecimalDegrees();
		double actualLat = actual.getLatitude().getInDecimalDegrees();
		double actualLon = actual.getLongitude().getInDecimalDegrees();
		Assert.assertEquals(expectedLat, actualLat, precision);
		Assert.assertEquals(expectedLon, actualLon, precision);
	}

	/**
	 * @param lessThanAllowed
	 * @param diagonal
	 */
	public static <Q extends Quantity<Q>, C extends Comparable<Q>> void assertRange(Range<C> expectedRange, Q quantity) {
		@SuppressWarnings("unchecked")
		C comparable = (C) MeasurementUtil.comparable(quantity);
		assertTrue(quantity + " not in range of " + expectedRange, expectedRange.apply(comparable));
	}

}
