/**
 *
 */
package com.aawhere.measure.calc;

import static javax.measure.unit.MetricSystem.METRE;
import static org.junit.Assert.assertEquals;

import javax.measure.quantity.Length;
import javax.measure.quantity.QuantityFactory;

import org.junit.Test;

import com.aawhere.measure.MeasurementQuantityFactory;

/**
 * Tests {@link QuantityWeightedEvaluation}.
 * 
 * @author Aaron Roller
 * 
 * 
 * @see <A
 *      href="http://en.wikipedia.org/wiki/Weighted_mean">http://en.wikipedia.org/wiki/Weighted_mean</a>
 */
public class QuantityWeightedEvaluationUnitTest {

	@Test
	/**
	 * Values used for this test provided by http://en.wikipedia.org/wiki/Weighted_mean.
	 */
	public void testWeightedMean() {
		double expectedValue = 86.0;
		QuantityWeightedEvaluation<Length> eval = QuantityWeightedEvaluation.newMean();
		QuantityFactory<Length> factory = MeasurementQuantityFactory.getInstance(Length.class);
		eval.increment(factory.create(80, METRE), 20.0);
		eval.increment(factory.create(90, METRE), 30.0);
		assertEquals(expectedValue, eval.getResult().doubleValue(METRE), 0.0);
	}

}
