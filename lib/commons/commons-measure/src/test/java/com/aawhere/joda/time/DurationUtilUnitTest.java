/**
 * 
 */
package com.aawhere.joda.time;

import static org.junit.Assert.assertEquals;

import org.joda.time.Duration;
import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class DurationUtilUnitTest {

	@Test
	public void testDurationFromZeroMinutes() {

		Duration expected = new Duration(0);
		Duration actual = DurationUtil.durationFromMinutes(0);
		assertEquals(expected, actual);
	}

	@Test
	public void testDurationFrom1Minute() {

		Duration expected = new Duration(60000);
		Duration actual = DurationUtil.durationFromMinutes(1);
		assertEquals(expected, actual);
	}

}
