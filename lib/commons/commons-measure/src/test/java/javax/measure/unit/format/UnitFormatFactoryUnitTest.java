/**
 *
 */
package javax.measure.unit.format;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.unit.format.UnitFormatFactory;

/**
 * @author brian
 * 
 */
public class UnitFormatFactoryUnitTest {

	UnitFormatFactory uff;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		uff = UnitFormatFactory.getInstance();
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.unit.format.UnitFormatFactory#getInstance(java.util.Locale)}.
	 */
	@Test
	public void testGetInstance() {
		assertNotNull(UnitFormatFactory.getInstance());
		// Should always get the same object back.
		assertTrue(UnitFormatFactory.getInstance() == UnitFormatFactory.getInstance());
	}

	/**
	 * Test method for
	 * {@link com.aawhere.measure.unit.format.UnitFormatFactory#getDefaultInstance()}.
	 */
	@Test
	public void testGetDefaultInstance() {
		assertNotNull(uff.createDefaultUnitFormat());
		// Test that the results are cached.
		assertTrue(uff.createDefaultUnitFormat() == uff.createDefaultUnitFormat());
		assertTrue(uff.createUnitFormat(Locale.FRANCE) == uff.createUnitFormat(Locale.FRANCE));
	}

}
