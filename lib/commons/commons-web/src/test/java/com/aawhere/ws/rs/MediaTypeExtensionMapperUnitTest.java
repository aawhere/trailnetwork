/**
 * 
 */
package com.aawhere.ws.rs;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.common.net.MediaType;

/**
 * @author brian
 * 
 */
public class MediaTypeExtensionMapperUnitTest {

	private static final String XML = "xml";
	private static final String KML = "kml";

	@Test
	public void testGetExtensionForGoogleMediaType() {
		String actual = MediaTypeExtensionMapper.build().getExtensionFor(MediaType.KML);
		assertEquals(KML, actual);
	}

	@Test
	public void testGetExtensionFor() {
		String actual = MediaTypeExtensionMapper.build()
				.getExtensionFor(javax.ws.rs.core.MediaType.APPLICATION_XML_TYPE);
		assertEquals(XML, actual);
	}

	@Test
	public void testGetMediaTypeFor() {
		javax.ws.rs.core.MediaType actual = MediaTypeExtensionMapper.build().getMediaTypeFor(XML);
		assertEquals(javax.ws.rs.core.MediaType.APPLICATION_XML_TYPE, actual);
	}

}
