/**
 * Derived from MITRE's org.mitre.dsmiley.httpproxy.ProxyServletUnitTest class
 *
 * Copyright AAWHERE
 * Portions copyright MITRE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aawhere.http;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.util.Properties;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.RequestLine;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.localserver.LocalTestServer;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;

/**
 * @author David Smiley dsmiley@mitre.org>, Brian Chapman
 * 
 */
public class ProxyServletUnitTest {

	/**
	 * From Apache httpcomponents/httpclient. Note httpunit has a similar thing called PseudoServlet
	 * but it is not as good since you can't even make it echo the request back.
	 */
	private LocalTestServer localTestServer;

	/** From Meterware httpunit. */
	private ServletRunner servletRunner;
	private ServletUnitClient sc;

	private String targetBaseUri;
	private String sourceBaseUri;

	private static String TEST_HEADER_NAME = "X-TestHeader";
	private static String TEST_HEADER_VALUE = "Test Header Value";
	private static String TEST_CONTENT_TYPE = "application/json";

	@Before
	public void setUp() throws Exception {
		localTestServer = new LocalTestServer(null, null);
		localTestServer.start();
		localTestServer.register("/targetPath*", new RequestInfoHandler());// matches
																			// /targetPath
																			// and
																			// /targetPath/blahblah
		targetBaseUri = "http://localhost:" + localTestServer.getServiceAddress().getPort() + "/targetPath";

		servletRunner = new ServletRunner();
		Properties params = new Properties();
		params.setProperty("http.protocol.handle-redirects", "false");
		params.setProperty("targetUri", targetBaseUri);
		servletRunner.registerServlet("/proxyMe/*", ProxyServlet.class.getName(), params);// also
																							// matches
																							// /proxyMe
																							// (no
																							// path
																							// info)
		sourceBaseUri = "http://localhost/proxyMe";// localhost:0 is hard-coded
													// in
													// ServletUnitHttpRequest
		sc = servletRunner.newClient();
		sc.getClientProperties().setAutoRedirect(false);// don't want httpunit
														// itself to redirect
	}

	@After
	public void tearDown() throws Exception {
		servletRunner.shutDown();
		localTestServer.stop();
	}

	// Url Fragments are not supposed to be interperted by the server, so
	// testing them doesn't make
	// sense (Brian)
	// private static String[] testUrlSuffixes = new String[] { "", "/pathInfo",
	// "?q=v", "/p?q=v",
	// "/p?#f", "/p?#" };
	private static String[] testUrlSuffixes = new String[] { "", "/pathInfo", "?q=v", "/p?q=v" };

	@Test
	public void testGet() throws Exception {
		for (String urlSuffix : testUrlSuffixes) {
			execAssert(makeGetMethodRequest(sourceBaseUri + urlSuffix));
		}
	}

	@Test(expected = com.meterware.httpunit.HttpException.class)
	public void testServerNotAvailable() throws Exception {
		localTestServer.stop();
		execAssert(makeGetMethodRequest(sourceBaseUri + "/test"));
	}

	@Test
	public void testContentTypeSet() throws Exception {
		WebResponse rsp = execAssert(makeGetMethodRequest(sourceBaseUri + "/testContentType"));
		assertNotNull(rsp.getContentType());
		assertEquals(TEST_CONTENT_TYPE, rsp.getContentType());
	}

	// Per above, it might not make sense to test fragments. They are for client
	// use, not server
	// use.
	@Test
	@Ignore
	public void testOnlyFragment() throws Exception {
		// TODO These fail; should they? Do they fail because of the test
		// infrastructure? Maybe we
		// should switch to Jetty.
		execAssert(makeGetMethodRequest(sourceBaseUri + "/p#f"));
		execAssert(makeGetMethodRequest(sourceBaseUri + "#f"));
		execAssert(makeGetMethodRequest(sourceBaseUri + "/#f"));
	}

	@Test
	public void testPost() throws Exception {
		for (String urlSuffix : testUrlSuffixes) {
			execAndAssert(makePostMethodRequest(sourceBaseUri + urlSuffix));
		}
	}

	@Test
	public void testRedirect() throws IOException, SAXException {
		localTestServer.register("/targetPath*", new HttpRequestHandler() {
			@Override
			public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException,
					IOException {
				response.setHeader(HttpHeaders.LOCATION, request.getFirstHeader("xxTarget").getValue());
				response.setStatusCode(HttpStatus.SC_MOVED_TEMPORARILY);
			}
		});// matches /targetPath and /targetPath/blahblah
		GetMethodWebRequest request = makeGetMethodRequest(sourceBaseUri);
		assertRedirect(request, "/dummy", "/dummy");// TODO represents a bug to
													// fix
		assertRedirect(request, targetBaseUri + "/dummy?a=b", sourceBaseUri + "/dummy?a=b");
	}

	private void assertRedirect(GetMethodWebRequest request, String origRedirect, String resultRedirect)
			throws IOException, SAXException {
		request.setHeaderField("xxTarget", origRedirect);
		WebResponse rsp = sc.getResponse(request);

		assertEquals(HttpStatus.SC_MOVED_TEMPORARILY, rsp.getResponseCode());
		assertEquals("", rsp.getText());
		String gotLocation = rsp.getHeaderField(HttpHeaders.LOCATION);
		assertEquals(resultRedirect, gotLocation);
	}

	@Test
	public void testSendFile() throws Exception {
		final PostMethodWebRequest request = new PostMethodWebRequest("http://localhost/proxyMe", true);// true:
																										// mime
																										// encoded
		InputStream data = new ByteArrayInputStream("testFileData".getBytes("UTF-8"));
		request.selectFile("fileNameParam", "fileName", data, "text/plain");
		WebResponse rsp = execAndAssert(request);
		assertTrue(rsp.getText().contains("Content-Type: multipart/form-data; boundary="));
	}

	@Test
	public void testProxyWithUnescapedChars() throws Exception {
		execAssert(makeGetMethodRequest(sourceBaseUri + "?fq={!f=field}"), "?fq=%7B!f=field%7D");// has
																									// squiggly
																									// brackets
		execAssert(makeGetMethodRequest(sourceBaseUri + "?fq=%7B!f=field%7D"));// already
																				// escaped;
																				// don't
																				// escape
																				// twice
	}

	private WebResponse execAssert(GetMethodWebRequest request, String expectedUri) throws Exception {
		return execAndAssert(request, expectedUri);
	}

	private WebResponse execAssert(GetMethodWebRequest request) throws Exception {
		return execAndAssert(request, null);
	}

	private WebResponse execAndAssert(PostMethodWebRequest request) throws Exception {
		request.setParameter("abc", "ABC");

		WebResponse rsp = execAndAssert(request, null);

		assertTrue(rsp.getText().contains("ABC"));
		return rsp;
	}

	private WebResponse execAndAssert(WebRequest request, String expectedUri) throws Exception {
		WebResponse rsp = sc.getResponse(request);

		assertEquals(HttpStatus.SC_OK, rsp.getResponseCode());
		// HttpUnit doesn't pass the message; not a big deal
		// assertEquals("TESTREASON",rsp.getResponseMessage());
		final String text = rsp.getText();
		assertTrue(text.startsWith("REQUESTLINE:"));

		if (expectedUri == null) {
			expectedUri = request.getURL().toString().substring(sourceBaseUri.length());
		}

		String firstTextLine = text.substring(0, text.indexOf('\n'));

		String expectedTargetUri = new URI(this.targetBaseUri).getPath() + expectedUri;
		String expectedFirstLine = "REQUESTLINE: " + (request instanceof GetMethodWebRequest ? "GET" : "POST");
		expectedFirstLine += " " + expectedTargetUri + " HTTP/1.1";
		assertEquals(expectedFirstLine, firstTextLine);
		String testHeaderValue = rsp.getHeaderField(TEST_HEADER_NAME);
		assertNotNull(testHeaderValue);
		assertEquals(TEST_HEADER_VALUE, testHeaderValue);
		return rsp;
	}

	private GetMethodWebRequest makeGetMethodRequest(final String url) {
		GetMethodWebRequest request = makeMethodRequest(url, GetMethodWebRequest.class);
		return request;
	}

	private PostMethodWebRequest makePostMethodRequest(final String url) {
		return makeMethodRequest(url, PostMethodWebRequest.class);
	}

	// Fixes problems in HttpUnit in which I can't specify the query string via
	// the url. I don't
	// want to use
	// setParam on a get request.
	@SuppressWarnings({ "unchecked" })
	private static <M> M makeMethodRequest(final String url, Class<M> clazz) {
		String urlNoQuery;
		final String queryString;
		int qIdx = url.indexOf('?');
		if (qIdx == -1) {
			urlNoQuery = url;
			queryString = null;
		} else {
			urlNoQuery = url.substring(0, qIdx);
			queryString = url.substring(qIdx + 1);

		}

		if (clazz == PostMethodWebRequest.class) {
			PostMethodWebRequest request = new PostMethodWebRequest(urlNoQuery) {
				@Override
				public String getQueryString() {
					return queryString;
				}

				@Override
				protected String getURLString() {
					return url;
				}
			};
			return (M) request;
		} else if (clazz == GetMethodWebRequest.class) {
			GetMethodWebRequest request = new GetMethodWebRequest(urlNoQuery) {
				@Override
				public String getQueryString() {
					return queryString;
				}

				@Override
				protected String getURLString() {
					return url;
				}

			};
			return (M) request;
		}
		throw new IllegalArgumentException(clazz.toString());
	}

	/**
	 * Writes all information about the request back to the response.
	 */
	class RequestInfoHandler
			implements HttpRequestHandler {

		/**
		 *
		 */
		public RequestInfoHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(HttpRequest request, HttpResponse response, HttpContext context) throws HttpException,
				IOException {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintWriter pw = new PrintWriter(baos, false);
			final RequestLine rl = request.getRequestLine();
			pw.println("REQUESTLINE: " + rl);

			for (Header header : request.getAllHeaders()) {
				pw.println(header.getName() + ": " + header.getValue());
			}
			pw.println("BODY: (below)");
			pw.flush();// done with pw now

			if (request instanceof HttpEntityEnclosingRequest) {
				HttpEntityEnclosingRequest enclosingRequest = (HttpEntityEnclosingRequest) request;
				HttpEntity entity = enclosingRequest.getEntity();
				byte[] body = EntityUtils.toByteArray(entity);
				baos.write(body);
			}

			response.setStatusCode(200);
			response.setReasonPhrase("TESTREASON");
			response.setEntity(new ByteArrayEntity(baos.toByteArray()));
			response.setHeader(TEST_HEADER_NAME, TEST_HEADER_VALUE);
			response.setHeader(HttpHeaders.CONTENT_TYPE, TEST_CONTENT_TYPE);
		}

	}
}
