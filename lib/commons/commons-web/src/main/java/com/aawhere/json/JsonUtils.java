/**
 *
 */
package com.aawhere.json;

import javax.xml.bind.JAXBException;

import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.api.json.JSONJAXBContext;

/**
 * Default way of creating JSONJaxBContext so that all Json looks the same no matter what web
 * application it is comming from.
 * 
 * @author Brian Chapman
 * 
 */
public class JsonUtils {

	public static JSONJAXBContext getJSONJAXBContext(Class<?>[] jaxbClasses) throws JAXBException {
		JSONJAXBContext jc = new JSONJAXBContext(JSONConfiguration.natural().build(), jaxbClasses);
		return jc;
	}
}
