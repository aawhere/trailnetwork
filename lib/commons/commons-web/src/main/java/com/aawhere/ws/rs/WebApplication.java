/**
 *
 */
package com.aawhere.ws.rs;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import com.aawhere.log.LoggerFactory;

import com.sun.jersey.api.container.filter.RolesAllowedResourceFilterFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;

/**
 * Used to register parts of a JAX-RS application. @See Application, ResourceConfig,
 * PackagesResourceConfig
 * 
 * @author Brian Chapman
 * 
 */
public abstract class WebApplication
		extends PackagesResourceConfig {

	private Map<String, Boolean> features = new HashMap<>();
	private Map<String, Object> properties = new HashMap<>();
	private Logger log = LoggerFactory.getLogger(getClass());
	private MediaTypeExtensionMapper mediaTypeMapper = MediaTypeExtensionMapper.build();

	public WebApplication(String... packages) {
		super(packages);
		init();
		logFeaturesAndProperties();
	}

	/**
	 * Search for root resource classes declaring the packages as a property of
	 * {@link ResourceConfig}.
	 * 
	 * @param props
	 *            the property bag that contains the property
	 *            {@link PackagesResourceConfig#PROPERTY_PACKAGES}.
	 */
	public WebApplication(Map<String, Object> props) {
		super(props);
		init();
		logFeaturesAndProperties();
	}

	protected void init() {
		// AppEngine compatibility:
		// https://java.net/jira/browse/JERSEY-630?page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel
		addFeatureIfNull(ResourceConfig.FEATURE_DISABLE_WADL, Boolean.TRUE);

		// Useful for debugging with the container up.
		// addPropertyIfNull(ResourceConfig.PROPERTY_CONTAINER_REQUEST_FILTERS, new
		// LoggingFilter());
		// addPropertyIfNull(ResourceConfig.PROPERTY_CONTAINER_RESPONSE_FILTERS, new
		// LoggingFilter());
		addPropertyIfNull(ResourceConfig.PROPERTY_RESOURCE_FILTER_FACTORIES, new RolesAllowedResourceFilterFactory());
	}

	protected void addFeatureIfNull(String name, Boolean value) {
		if (!features.containsKey(name)) {
			features.put(name, value);
		}
	}

	protected void addPropertyIfNull(String name, Object value) {
		if (!features.containsKey(name)) {
			properties.put(name, value);
		}
	}

	/**
	 * Prints out the current config to the log to document how we have configured Jersey.
	 */
	private void logFeaturesAndProperties() {
		StringBuffer sb = new StringBuffer();
		sb.append("Jersey Initialized with the following Features\n");
		sb.append("\t Features: \n");
		for (Entry<String, Boolean> entry : features.entrySet()) {
			sb.append("\t\t " + entry.getKey() + " : " + entry.getValue().toString() + "\n");
		}
		sb.append("\t Properties: \n");
		for (Entry<String, Object> entry : properties.entrySet()) {
			sb.append("\t\t " + entry.getKey() + " : " + entry.getValue().getClass().toString() + "\n");
		}
		log.info(sb.toString());
	}

	@Override
	public Set<Class<?>> getClasses() {
		return super.getClasses();
		// If we get away from class scanning, this will be necessary
		// final Set<Class<?>> classes = new HashSet<Class<?>>();
		// classes.addAll(Arrays.asList(injectableClasses()));
		// classes.addAll(Arrays.asList(providerClasses()));
		// return classes;
	}

	/*
	 * (non-Javadoc)
	 * @see com.sun.jersey.api.core.ResourceConfig#getMediaTypeMappings()
	 */
	@Override
	public Map<String, MediaType> getMediaTypeMappings() {
		return mediaTypeMapper.getMapping();
	}

	/*
	 * (non-Javadoc)
	 * @see com.sun.jersey.api.core.ResourceConfig#getFeatures()
	 */
	@Override
	public Map<String, Boolean> getFeatures() {
		return features;
	}

	/*
	 * (non-Javadoc)
	 * @see com.sun.jersey.api.core.ResourceConfig#getFeature(java.lang.String)
	 */
	@Override
	public boolean getFeature(String featureName) {
		return features.get(featureName);
	}

	/*
	 * (non-Javadoc)
	 * @see com.sun.jersey.api.core.ResourceConfig#getProperties()
	 */
	@Override
	public Map<String, Object> getProperties() {
		return properties;
	}

	/*
	 * (non-Javadoc)
	 * @see com.sun.jersey.api.core.ResourceConfig#getProperty(java.lang.String)
	 */
	@Override
	public Object getProperty(String propertyName) {
		return properties.get(propertyName);
	}
}
