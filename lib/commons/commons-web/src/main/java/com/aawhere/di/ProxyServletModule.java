/**
 *
 */
package com.aawhere.di;

import java.util.HashMap;
import java.util.Map;

import com.aawhere.http.ProxyServlet;

import com.google.inject.Guice;
import com.google.inject.Singleton;
import com.google.inject.servlet.ServletModule;

/**
 * {@link Guice} configuration for {@link ProxyServlet}.
 * 
 * @author Brian Chapman
 * 
 */
public class ProxyServletModule
		extends ServletModule {

	public String proxyUrl;
	public String server;

	public ProxyServletModule(String proxyUrl, String server) {
		this.proxyUrl = proxyUrl;
		this.server = server;
	}

	@Override
	protected void configureServlets() {
		/*
		 * Proxy Servlet
		 * @see https://github.com/dsmiley/HTTP-Proxy-Servlet
		 */
		Map<String, String> proxyParams = new HashMap<String, String>();
		proxyParams.put("targetUri", server);
		bind(ProxyServlet.class).in(Singleton.class);
		serve(proxyUrl).with(ProxyServlet.class, proxyParams);
	}
}
