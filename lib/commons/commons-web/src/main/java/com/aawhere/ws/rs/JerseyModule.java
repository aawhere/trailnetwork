/**
 *
 */
package com.aawhere.ws.rs;

import java.util.HashMap;
import java.util.Map;

import com.aawhere.lang.ObjectBuilder;

import com.google.inject.Guice;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

/**
 * {@link Guice} configuration for Jersey. Configuration should happen in the {@link WebApplication}
 * or any extensions of it.
 * 
 * @author Brian Chapman
 * 
 */
public class JerseyModule
		extends JerseyServletModule {

	private Class<? extends WebApplication> applicationType;
	// The path to serve jersey from. May be any UrlPattern.
	private String path;

	/*
	 * Use Builder to construct.
	 */
	private JerseyModule() {
	}

	@Override
	protected void configureServlets() {
		/*
		 * Looking to configure Jersey? Stop and go to WebApplication and it's children.
		 */
		Map<String, String> jerseyParams = new HashMap<String, String>();

		// This is the only required configuration at this level.
		jerseyParams.put("javax.ws.rs.Application", applicationType.getName());

		serve(path).with(GuiceContainer.class, jerseyParams);
	}

	/**
	 * Used to construct all instances of BoundingBox.
	 */
	public static class Builder
			extends ObjectBuilder<JerseyModule> {

		public Builder() {
			super(new JerseyModule());
		}

		public Builder application(Class<? extends WebApplication> applicationType) {
			building.applicationType = applicationType;
			return this;
		}

		public Builder path(String path) {
			building.path = path;
			return this;
		}
	}

	public static Builder create() {
		return new Builder();
	}
}
