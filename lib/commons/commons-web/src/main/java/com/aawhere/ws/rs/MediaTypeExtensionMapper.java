/**
 * 
 */
package com.aawhere.ws.rs;

import javax.ws.rs.core.MediaType;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.inject.Singleton;

/**
 * Maps {@link MediaType} to a rest url extension and vice versa.
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class MediaTypeExtensionMapper {

	public static MediaTypeExtensionMapper build() {
		return new MediaTypeExtensionMapper();
	}

	private BiMap<String, MediaType> mapping = HashBiMap.create();

	private MediaTypeExtensionMapper() {
		mapping.put(MediaTypes.XML, MediaType.APPLICATION_XML_TYPE);
		mapping.put(MediaTypes.JSON, MediaType.APPLICATION_JSON_TYPE);
		mapping.put(MediaTypes.KML, MediaTypes.toJaxRs(com.google.common.net.MediaType.KML));
		mapping.put(MediaTypes.CSV, MediaTypes.toJaxRs(com.google.common.net.MediaType.CSV_UTF_8));
		mapping.put(MediaTypes.SVG, MediaTypes.toJaxRs(com.google.common.net.MediaType.SVG_UTF_8));
		mapping.put(MediaTypes.GPOLYLINE, MediaTypes.POLYLINE_ENCODED);
		mapping.put(MediaTypes.PNG, MediaTypes.toJaxRs(com.google.common.net.MediaType.PNG));
		mapping.put(MediaTypes.NONE, MediaTypes.create(MediaTypes.APPLICATION_NONE));
	}

	/**
	 * @return the mapping
	 */
	public BiMap<String, MediaType> getMapping() {
		return this.mapping;
	}

	public String getExtensionFor(MediaType mediaType) {
		return mapping.inverse().get(mediaType);

	}

	/**
	 * Returns the extension as a filename suffix starting with a period (.xml, .csv, etc).
	 * 
	 * @param mediaType
	 * @return
	 */
	public String getSuffixFor(MediaType mediaType) {
		return MediaTypes.EXTENSION_SEPARATOR + getExtensionFor(mediaType);
	}

	public String getSuffixFor(com.google.common.net.MediaType googleMediaType) {
		return getSuffixFor(MediaTypes.toJaxRs(googleMediaType));
	}

	public String getExtensionFor(com.google.common.net.MediaType googleMediaType) {
		MediaType mediaType = MediaTypes.toJaxRs(googleMediaType);
		return getExtensionFor(mediaType);
	}

	public MediaType getMediaTypeFor(String extension) {
		return mapping.get(extension);
	}

}
