package com.aawhere.http;

/**
 * Derived from MITRE's org.mitre.dsmiley.httpproxy.ProxyServlet class
 *
 * Copyright AAWHERE
 * Portions copyright MITRE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.BitSet;
import java.util.Enumeration;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.utils.URIUtils;

/**
 * An HTTP reverse proxy/gateway servlet. It is designed to be extended for customization if
 * desired. Most of the work is handled by <a
 * href="http://hc.apache.org/httpcomponents-client-ga/">Apache HttpClient</a>.
 * <p>
 * There are alternatives to a servlet based proxy such as Apache mod_proxy if that is available to
 * you. However this servlet is easily customizable by Java, secure-able by your web application's
 * security (e.g. spring-security), portable across servlet engines, and is embeddable into another
 * web application.
 * </p>
 * <p>
 * Inspiration: http://httpd.apache.org/docs/2.0/mod/mod_proxy.html<br/>
 * </p>
 * <p>
 * Adapted to use Google App Engine's URL Fetch. App Engine does not allow socket connections.
 * </p>
 * 
 * @author David Smiley dsmiley@mitre.org>, Brian Chapman <brian.chapman@aawhere.com>
 */
public class ProxyServlet
		extends HttpServlet {

	private static final long serialVersionUID = 6639246159644105897L;

	/* INIT PARAMETER NAME CONSTANTS */

	/**
	 * A boolean parameter then when enabled will log input and target URLs to the servlet log.
	 */
	public static final String P_LOG = "log";
	public static final String P_TIMEOUT = "timeout";
	public static final Boolean FOLLOW_REDIRECTS = false;

	/* MISC */

	protected boolean doLog = false;
	protected URI targetUri;
	protected Integer timeout = null;

	@Override
	public String getServletInfo() {
		return "A proxy servlet by David Smiley, dsmiley@mitre.org. Modified by AAWHERE";
	}

	@Override
	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);

		String doLogStr = servletConfig.getInitParameter(P_LOG);
		if (doLogStr != null) {
			this.doLog = Boolean.parseBoolean(doLogStr);
		}

		try {
			targetUri = new URI(servletConfig.getInitParameter("targetUri"));
		} catch (Exception e) {
			throw new RuntimeException("Trying to process targetUri init parameter: " + e, e);
		}

		String to = servletConfig.getInitParameter(P_TIMEOUT);
		if (to != null) {
			timeout = Integer.valueOf(to);
		}
	}

	@Override
	public void destroy() {
	}

	@Override
	protected void doGet(HttpServletRequest servletRequest, HttpServletResponse servletResponse)
			throws ServletException, IOException {
		OutputStream out;
		int httpStatus;

		HttpURLConnection connection = createConnection(servletRequest);

		try {
			connection.connect();
			httpStatus = connection.getResponseCode();

			if (doResponseRedirectOrNotModifiedLogic(servletRequest, servletResponse, connection, httpStatus)) {
				return;
			} else {
				copyProxyResponseToThisResponse(connection, httpStatus, servletResponse);
			}

		} catch (ConnectException e) {
			handleError(HttpStatus.SC_GATEWAY_TIMEOUT, e, servletResponse);
		} catch (IOException e) {
			handleError(HttpStatus.SC_INTERNAL_SERVER_ERROR, e, servletResponse);
		} finally {
			connection.disconnect();
		}

	}

	@Override
	protected void doPost(HttpServletRequest servletRequest, HttpServletResponse servletResponse)
			throws ServletException, IOException {
		OutputStream out;
		int httpStatus;

		HttpURLConnection connection = createConnection(servletRequest);
		connection.setDoOutput(true);

		try {
			connection.connect();
			out = connection.getOutputStream();
			copyRequestPayload(servletRequest, out);
			httpStatus = connection.getResponseCode();

			if (doResponseRedirectOrNotModifiedLogic(servletRequest, servletResponse, connection, httpStatus)) {
				return;
			} else {
				copyProxyResponseToThisResponse(connection, httpStatus, servletResponse);
			}

		} catch (IOException e) {
			handleError(HttpStatus.SC_INTERNAL_SERVER_ERROR, e, servletResponse);
		} finally {
			connection.disconnect();
		}
	}

	private HttpURLConnection createConnection(HttpServletRequest servletRequest) throws IOException {
		URL url = new URL(rewriteUrlFromRequest(servletRequest));
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod(servletRequest.getMethod());
		connection.setInstanceFollowRedirects(FOLLOW_REDIRECTS);
		if (timeout != null) {
			connection.setConnectTimeout(timeout);
		}
		copyRequestHeaders(servletRequest, connection);
		return connection;
	}

	/**
	 * @param connection
	 * @param httpStatus
	 * @param servletResponse
	 * @throws IOException
	 */
	private void copyProxyResponseToThisResponse(HttpURLConnection connection, int httpStatus,
			HttpServletResponse servletResponse) throws IOException {

		// Note: there is no way to set the status message.
		servletResponse.setStatus(httpStatus);

		copyResponseHeaders(connection, servletResponse);
		setResponseContentType(connection, servletResponse);

		// This must be last since this commits the response.
		InputStream in = connection.getInputStream();
		copyResponseEntity(in, servletResponse);
	}

	/**
	 * Handle an IO Error indicating we could not communicate with the server at this.targetUri.
	 * 
	 * @param httpStatus
	 */
	private void handleError(int status, Exception e, HttpServletResponse response) {
		response.setStatus(status);
		try {
			response.sendError(status, e.getMessage());
		} catch (IOException e1) {
			response.setStatus(status);
		}
	}

	private boolean doResponseRedirectOrNotModifiedLogic(HttpServletRequest servletRequest,
			HttpServletResponse servletResponse, HttpURLConnection connection, int statusCode) throws ServletException,
			IOException {
		// Check if the proxy response is a redirect
		// The following code is adapted from
		// org.tigris.noodle.filters.CheckForRedirect
		if (statusCode >= HttpServletResponse.SC_MULTIPLE_CHOICES /* 300 */
				&& statusCode < HttpServletResponse.SC_NOT_MODIFIED /* 304 */) {
			String locationHeader = connection.getHeaderField(HttpHeaders.LOCATION);
			if (locationHeader == null) {
				throw new ServletException("Recieved status code: " + statusCode + " but no " + HttpHeaders.LOCATION
						+ " header was found in the response");
			}
			// Modify the redirect to go to this proxy servlet rather that the
			// proxied host
			String locStr = rewriteUrlFromResponse(servletRequest, locationHeader);

			servletResponse.sendRedirect(locStr);
			return true;
		}
		// 304 needs special handling. See:
		// http://www.ics.uci.edu/pub/ietf/http/rfc1945.html#Code304
		// We get a 304 whenever passed an 'If-Modified-Since'
		// header and the data on disk has not changed; server
		// responds w/ a 304 saying I'm not going to send the
		// body because the file has not changed.
		if (statusCode == HttpServletResponse.SC_NOT_MODIFIED) {
			servletResponse.setIntHeader(HttpHeaders.CONTENT_LENGTH, 0);
			servletResponse.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			return true;
		}
		return false;
	}

	protected void closeQuietly(Closeable closeable) {
		try {
			closeable.close();
		} catch (IOException e) {
			log(e.getMessage(), e);
		}
	}

	/** Copy request headers from the servlet client to the proxy request. */
	protected void copyRequestHeaders(HttpServletRequest servletRequest, HttpURLConnection connection) {
		// Get an Enumeration of all of the header names sent by the client
		Enumeration enumerationOfHeaderNames = servletRequest.getHeaderNames();
		while (enumerationOfHeaderNames.hasMoreElements()) {
			String headerName = (String) enumerationOfHeaderNames.nextElement();
			if (headerName.equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH)) {
				continue;
			}
			// As per the Java Servlet API 2.5 documentation:
			// Some headers, such as Accept-Language can be sent by clients
			// as several headers each with a different value rather than
			// sending the header as a comma separated list.
			// Thus, we get an Enumeration of the header values sent by the
			// client
			Enumeration headers = servletRequest.getHeaders(headerName);
			while (headers.hasMoreElements()) {
				String headerValue = (String) headers.nextElement();
				// In case the proxy host is running multiple virtual servers,
				// rewrite the Host header to ensure that we get content from
				// the correct virtual server
				if (headerName.equalsIgnoreCase(HttpHeaders.HOST)) {
					HttpHost host = URIUtils.extractHost(this.targetUri);
					headerValue = host.getHostName();
					if (host.getPort() != -1) {
						headerValue += ":" + host.getPort();
					}
				}
				connection.setRequestProperty(headerName, headerValue);
			}
		}
	}

	/**
	 * @param connection
	 * @param servletResponse
	 */
	private void setResponseContentType(HttpURLConnection connection, HttpServletResponse servletResponse) {
		if (connection.getContentType() != null) {
			servletResponse.setContentType(connection.getContentType());
		}

		servletResponse.setContentLength(connection.getContentLength());

		if (connection.getContentEncoding() != null) {
			servletResponse.setCharacterEncoding(connection.getContentEncoding());
		}
	}

	/** Copy proxied response headers back to the servlet client. */
	protected void copyResponseHeaders(HttpURLConnection connection, HttpServletResponse servletResponse) {
		for (Entry<String, List<String>> entry : connection.getHeaderFields().entrySet()) {
			if (entry.getKey() != null) {
				StringBuffer value = new StringBuffer();
				Iterator<String> valueIt = entry.getValue().iterator();
				while (valueIt.hasNext()) {
					value.append(valueIt.next());
					if (valueIt.hasNext()) {
						value.append(",");
					}
				}
				servletResponse.addHeader(entry.getKey(), value.toString());
			}
		}
	}

	/**
	 * @param req
	 * @param out
	 * @throws IOException
	 */
	private void copyRequestPayload(HttpServletRequest servletResponse, OutputStream proxy) throws IOException {
		InputStream servletInputStream = servletResponse.getInputStream();
		try {
			IOUtils.copy(servletInputStream, proxy);
		} finally {
			closeQuietly(servletInputStream);
		}
	}

	/**
	 * Copy response body data (the entity) from the proxy to the servlet client.
	 */
	private void copyResponseEntity(InputStream proxy, HttpServletResponse servletResponse) throws IOException {
		OutputStream servletOutputStream = servletResponse.getOutputStream();
		try {
			IOUtils.copy(proxy, servletOutputStream);
		} finally {
			closeQuietly(servletOutputStream);
		}
	}

	private void copy(InputStream in, OutputStream out) {
	}

	private String rewriteUrlFromRequest(HttpServletRequest servletRequest) {
		StringBuilder uri = new StringBuilder(500);
		uri.append(this.targetUri.toString());
		// Handle the path given to the servlet
		if (servletRequest.getPathInfo() != null) {// ex: /my/path.html
			uri.append(servletRequest.getPathInfo());
		}
		// Handle the query string
		String queryString = servletRequest.getQueryString();// ex:(following
																// '?'):
																// name=value&foo=bar#fragment
		if (queryString != null && queryString.length() > 0) {
			uri.append('?');
			int fragIdx = queryString.indexOf('#');
			String queryNoFrag = (fragIdx < 0 ? queryString : queryString.substring(0, fragIdx));
			uri.append(encodeUriQuery(queryNoFrag));
			if (fragIdx >= 0) {
				uri.append('#');
				uri.append(encodeUriQuery(queryString.substring(fragIdx + 1)));
			}
		}
		return uri.toString();
	}

	private String rewriteUrlFromResponse(HttpServletRequest servletRequest, String theUrl) {
		// TODO document example paths
		if (theUrl.startsWith(this.targetUri.toString())) {
			String curUrl = servletRequest.getRequestURL().toString();// no
																		// query
			String pathInfo = servletRequest.getPathInfo();
			if (pathInfo != null) {
				assert curUrl.endsWith(pathInfo);
				curUrl = curUrl.substring(0, curUrl.length() - pathInfo.length());// take pathInfo
																					// off
			}
			theUrl = curUrl + theUrl.substring(this.targetUri.toString().length());
		}
		return theUrl;
	}

	/**
	 * <p>
	 * Encodes characters in the query or fragment part of the URI.
	 * 
	 * <p>
	 * Unfortunately, an incoming URI sometimes has characters disallowed by the spec. HttpClient
	 * insists that the outgoing proxied request has a valid URI because it uses Java's {@link URI}.
	 * To be more forgiving, we must escape the problematic characters. See the URI class for the
	 * spec.
	 * 
	 * @param in
	 *            example: name=value&foo=bar#fragment
	 */
	static CharSequence encodeUriQuery(CharSequence in) {
		// Note that I can't simply use URI.java to encode because it will
		// escape pre-existing
		// escaped things.
		StringBuilder outBuf = null;
		Formatter formatter = null;
		for (int i = 0; i < in.length(); i++) {
			char c = in.charAt(i);
			boolean escape = true;
			if (c < 128) {
				if (asciiQueryChars.get(c)) {
					escape = false;
				}
			} else if (!Character.isISOControl(c) && !Character.isSpaceChar(c)) {// not-ascii
				escape = false;
			}
			if (!escape) {
				if (outBuf != null) {
					outBuf.append(c);
				}
			} else {
				// escape
				if (outBuf == null) {
					outBuf = new StringBuilder(in.length() + 5 * 3);
					outBuf.append(in, 0, i);
					formatter = new Formatter(outBuf);
				}
				// leading %, 0 padded, width 2, capital hex
				formatter.format("%%%02X", (int) c);// TODO
			}
		}
		return outBuf != null ? outBuf : in;
	}

	static final BitSet asciiQueryChars;
	static {
		char[] c_unreserved = "_-!.~'()*".toCharArray();// plus alphanum
		char[] c_punct = ",;:$&+=".toCharArray();
		char[] c_reserved = "?/[]@".toCharArray();// plus punct

		asciiQueryChars = new BitSet(128);
		for (char c = 'a'; c <= 'z'; c++) {
			asciiQueryChars.set(c);
		}
		for (char c = 'A'; c <= 'Z'; c++) {
			asciiQueryChars.set(c);
		}
		for (char c = '0'; c <= '9'; c++) {
			asciiQueryChars.set(c);
		}
		for (char c : c_unreserved) {
			asciiQueryChars.set(c);
		}
		for (char c : c_punct) {
			asciiQueryChars.set(c);
		}
		for (char c : c_reserved) {
			asciiQueryChars.set(c);
		}

		asciiQueryChars.set('%');// leave existing percent escapes in place
	}

}
