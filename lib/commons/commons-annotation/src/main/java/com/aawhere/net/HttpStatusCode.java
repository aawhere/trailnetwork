/**
 *
 */
package com.aawhere.net;

import static java.net.HttpURLConnection.*;

import java.net.HttpURLConnection;
import java.util.HashMap;

/**
 * Provides typesafe {@link Enum} for {@link HttpURLConnection} status codes.
 * 
 * Spring provides an enum, but comes at a heavy price.
 * 
 * Apache HTTP client provides more status codes, but comes at a heavy price.
 * 
 * java.net uses ints
 * 
 * @see http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 * 
 * @author Aaron Roller
 * 
 */
public enum HttpStatusCode {

	/**
	 * HTTP Status-Code 202: Accepted.
	 */
	ACCEPTED(HTTP_ACCEPTED),

	/**
	 * HTTP Status-Code 502: Bad Gateway.
	 */
	BAD_GATEWAY(HTTP_BAD_GATEWAY),

	BAD_METHOD(HTTP_BAD_METHOD),

	BAD_REQUEST(HTTP_BAD_REQUEST),

	CLIENT_TIMEOUT(HTTP_CLIENT_TIMEOUT),

	CONFLICT(HTTP_CONFLICT),

	CREATED(HTTP_CREATED),

	ENTITY_TOO_LARGE(HTTP_ENTITY_TOO_LARGE),

	FORBIDDEN(HTTP_FORBIDDEN),

	/**
	 * HTTP Status-Code 504: Gateway Timeout.
	 */
	GATEWAY_TIMEOUT(HTTP_GATEWAY_TIMEOUT),

	GONE(HTTP_GONE),

	INTERNAL_ERROR(HTTP_INTERNAL_ERROR),

	LENGTH_REQUIRED(HTTP_LENGTH_REQUIRED),

	MOVED_PERM(HTTP_MOVED_PERM),

	MOVED_TEMP(HTTP_MOVED_TEMP),

	MULT_CHOICE(HTTP_MULT_CHOICE),

	NO_CONTENT(HTTP_NO_CONTENT),

	NOT_ACCEPTABLE(HTTP_NOT_ACCEPTABLE),

	NOT_AUTHORITATIVE(HTTP_NOT_AUTHORITATIVE),

	/** 404 */
	NOT_FOUND(HTTP_NOT_FOUND),

	NOT_IMPLEMENTED(HTTP_NOT_IMPLEMENTED),

	NOT_MODIFIED(HTTP_NOT_MODIFIED),

	OK(HTTP_OK),

	PARTIAL(HTTP_PARTIAL),

	PAYMENT_REQUIRED(HTTP_PAYMENT_REQUIRED),

	PRECON_FAILED(HTTP_PRECON_FAILED),

	PROXY_AUTH(HTTP_PROXY_AUTH),

	/**
	 * A custom HttpStatus code to communicate a retry is necessary, but to avoid logging it as a
	 * 500 error. TN-800
	 */
	TASK_RETRY(333),

	REQ_TOO_LONG(HTTP_REQ_TOO_LONG),

	RESET(HTTP_RESET),

	SEE_OTHER(HTTP_SEE_OTHER),

	UNAUTHORIZED(HTTP_UNAUTHORIZED),

	UNAVAILABLE(HTTP_UNAVAILABLE),
	/**
	 * The request was well-formed but was unable to be followed due to semantic errors.
	 * */
	UNPROCESSABLE_ENTITY(422),

	UNSUPPORTED_TYPE(HTTP_UNSUPPORTED_TYPE),

	/** 414 */
	URI_TOO_LONG(HTTP_REQ_TOO_LONG),

	USE_PROXY(HTTP_USE_PROXY),

	VERSION(HTTP_VERSION);

	/**
	 * The value as defined by the specs.
	 * 
	 * @see HttpURLConnection
	 */

	final public int value;

	/**
	 * 
	 * @param the
	 *            status code provided from {@link HttpStatusCode} constants
	 */
	private HttpStatusCode(int httpStatusCode) {
		this.value = httpStatusCode;
		Codes.put(this);
	}

	/**
	 * 
	 * @param httpStatusCode
	 *            the standard status code value
	 * @return the {@link HttpStatusCode} matching the given int or null if no match.
	 */
	public static HttpStatusCode valueOf(int httpStatusCode) {
		return Codes.all.get(Integer.valueOf(httpStatusCode));
	}

	/**
	 * Simple test to indicate the value is within the acceptable range for handling (200s).
	 * 
	 * @param statusCodeValue
	 * @return
	 */
	public static Boolean isOk(int statusCodeValue) {
		return statusCodeValue < MULT_CHOICE.value;
	}

	/**
	 * @see #isOk(int)
	 * 
	 * @param code
	 * @return
	 */
	public static Boolean isOk(HttpStatusCode code) {
		return isOk(code.value);
	}

	public Boolean isOk() {
		return isOk(this);
	}

	public static Boolean isServerError(int code) {
		return code >= INTERNAL_ERROR.value;
	}

	public static Boolean isServerError(HttpStatusCode code) {
		return isServerError(code.value);
	}

	public Boolean isServerError() {
		return isServerError(this);
	}

	private static class Codes {
		/**
		 * @see #valueOf(int)
		 * 
		 */
		private static final HashMap<Integer, HttpStatusCode> all = new HashMap<Integer, HttpStatusCode>();

		private static void put(HttpStatusCode code) {
			all.put(Integer.valueOf(code.value), code);
		}
	}

	/**
	 * Helper to indicate a standard request for Authorization is provided.
	 * 
	 * @param responseCode
	 * @return
	 */
	public static Boolean authorizationRequired(int responseCode) {
		return UNAUTHORIZED.value == responseCode;
	}
}
