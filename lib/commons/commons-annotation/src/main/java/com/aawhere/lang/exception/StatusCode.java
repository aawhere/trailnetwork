/**
 * 
 */
package com.aawhere.lang.exception;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.ExceptionMessageAnnotationProcessor;

/**
 * An annotation placed on a {@link BaseException} or {@link BaseRuntimeException} to indicate the
 * type of response code would be appropriate to communicate to a web enabled system (such as REST
 * Web Services or a web application).
 * 
 * @author Aaron Roller
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(ElementType.TYPE)
public @interface StatusCode {

	static final String NONE = "";

	/** The enumeration ensuring we have declared the proper status code. */
	HttpStatusCode value();

	/**
	 * The display message using syntax compatible with {@link MessageResourceBundle} and friends.
	 * Putting the message here will automatically generate necessary resource classes used for
	 * localization.
	 * 
	 * @see ExceptionMessageAnnotationProcessor
	 * @return
	 */
	String message() default NONE;
}
