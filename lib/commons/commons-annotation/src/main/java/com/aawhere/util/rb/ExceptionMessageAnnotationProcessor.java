/**
 * 
 */
package com.aawhere.util.rb;

import static com.aawhere.lang.annotation.AnnotationProcessorUtil.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.annotation.AnnotationProcessorUtil;
import com.aawhere.lang.exception.StatusCode;

/**
 * @author aroller
 * 
 */
@SupportedAnnotationTypes("com.aawhere.lang.exception.StatusCode")
@SupportedSourceVersion(SourceVersion.RELEASE_7)
public class ExceptionMessageAnnotationProcessor
		extends AbstractProcessor {

	public ExceptionMessageAnnotationProcessor() {
	}

	/*
	 * @see javax.annotation.processing.AbstractProcessor#process(java.util.Set,
	 * javax.annotation.processing.RoundEnvironment)
	 */
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		Messager messager = processingEnv.getMessager();
		for (TypeElement te : annotations) {
			for (Element exceptionElement : roundEnv.getElementsAnnotatedWith(te)) {

				if (exceptionElement.getKind() == ElementKind.CLASS) {
					TypeElement classElement = (TypeElement) exceptionElement;
					// messager.printMessage(Kind.NOTE, "Fields: " + e.getAnnotationMirrors());

					StatusCode statusCodeAnnotation = exceptionElement.getAnnotation(StatusCode.class);
					if (statusCodeAnnotation != null && StringUtils.isNotEmpty(statusCodeAnnotation.message())) {
						PackageElement packageElement = packageElement(classElement);

						try {

							// FIXME: A lot of this is copy paste. It should be put in a util, but
							// we should use templates instead
							String suffix = "Message";
							String className = classElement.getSimpleName() + suffix;
							Name dictionaryClassName = classElement.getQualifiedName();

							String qualifiedClassName = dictionaryClassName + suffix;
							messager.printMessage(Kind.NOTE, "Generating: " + qualifiedClassName);
							// Filenames must go the package and use the simple name because using
							// qualifiedClassName may include the enclosing class
							JavaFileObject jfo = processingEnv.getFiler()
									.createSourceFile(packageElement.getQualifiedName() + "." + className);

							BufferedWriter bw = new BufferedWriter(jfo.openWriter());
							// package
							{
								bw.append("package ");
								bw.append(packageElement.getQualifiedName());
								bw.append(";");
							}
							bw.newLine();
							bw.newLine();
							// imports
							{
								bw.append("import com.aawhere.util.rb.*;");
								bw.newLine();
								bw.append("import com.aawhere.text.format.custom.CustomFormat;");
								bw.newLine();

							}

							// javadoc
							{
								bw.append("/** Generated code.");
								bw.newLine();
								bw.append("* @see ");
								bw.append(getClass().toString());
								bw.newLine();
								bw.append("* @see ");
								bw.append(dictionaryClassName);
								bw.newLine();
								bw.append("*/");
								bw.newLine();
							}
							// body
							{
								bw.append("public enum ");
								bw.append(className);
								bw.append(" implements Message{");
								bw.newLine();

							}
							final String message = statusCodeAnnotation.message();
							String[] parameters = StringUtils.substringsBetween(message, "{", "}");
							if (parameters == null) {
								parameters = ArrayUtils.EMPTY_STRING_ARRAY;
							}
							// enum constant
							String constantName = AnnotationProcessorUtil.exceptionConstantName(className);
							{
								bw.write(constantName);
								bw.write("(");
								bw.write("\"");
								bw.write(message);
								bw.write("\"");
								for (String param : parameters) {
									bw.write(", Param.");
									bw.write(param);
								}
								bw.write(");");
								bw.newLine();
							}
							{
								bw.write("private ParamKey[] parameterKeys;\n");

								bw.write("private String message;\n");

								bw.write("private ");
								bw.write(className);
								bw.write("(String message, ParamKey... parameters) {\n");
								bw.write("this.message = message;\n");
								bw.write("this.parameterKeys = parameters;\n");
								bw.write("}//end constructor\n\n");

								bw.write("@Override\n");
								bw.write("public String getValue() {\n");
								bw.write("\treturn this.message;\n");
								bw.write("}//end getValue()\n");

								bw.write("@Override\n");
								bw.write("public ParamKey[] getParameterKeys() {\n");
								bw.write("\treturn this.parameterKeys;\n");
								bw.write("}//end getParameterKeys()\n\n");
							}
							// params
							{

								if (ArrayUtils.isNotEmpty(parameters)) {
									bw.write("public enum Param implements ParamKey {\n\n");
									for (String parameter : parameters) {
										bw.write(parameter);
										bw.write(",\n");
									}
									bw.write(";\n\n");
									bw.write("private CustomFormat<?> format;\n\n");

									bw.write("private Param() {}\n\n");

									bw.write("private Param(CustomFormat<?> format) {\n\n");
									bw.write("\tthis.format = format;\n\n");
									bw.write("}\n\n");

									bw.write("@Override\n\n");
									bw.write("public CustomFormat<?> getFormat() {\n\n");
									bw.write("\treturn this.format;\n");
									bw.write("}//end getFormat\n\n");
									bw.write("}//end Param\n\n");
								}
							}
							// factory method provides an easy way to construct complete message
							{

								bw.write("public static CompleteMessage message(");
								// write each method parameter
								for (int i = 0; i < parameters.length; i++) {
									String param = parameters[i];
									// comma separate parameters
									if (i > 0) {
										bw.write(",");
									}
									bw.write("Object ");
									// not proper naming, but the best I can do without our commons
									// library access
									bw.write(AnnotationProcessorUtil.variableName(param));
								}
								bw.write("){");
								bw.newLine();
								bw.write("return CompleteMessage.create(");
								bw.write(constantName);
								bw.write(")");
								for (String param : parameters) {
									bw.write(".param(Param.");
									bw.write(param);
									bw.write(",");
									bw.write(AnnotationProcessorUtil.variableName(param));
									bw.write(")");
								}
								bw.write(".build();");
								bw.newLine();
								bw.write("}//end message method");
								bw.newLine();
							}
							bw.append("}//end Message enum");
							bw.newLine();
							bw.close();
						} catch (IOException ioException) {
							throw new RuntimeException(ioException);
						}
					}
				}
			}
		}
		return true;
	}
}
