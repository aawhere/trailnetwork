/**
 * 
 */
package com.aawhere.lang.annotation;

import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;

import org.apache.commons.lang3.StringUtils;

/**
 * @author aroller
 * 
 */
public class AnnotationProcessorUtil {

	/**
	 * crawls up the chain of elements looking for the package. the class could be in another class.
	 * 
	 * @param classElement
	 * @return
	 */
	public static PackageElement packageElement(TypeElement classElement) {
		Element enclosingElement = classElement;
		do {
			enclosingElement = enclosingElement.getEnclosingElement();
		} while (!(enclosingElement instanceof PackageElement));
		return (PackageElement) enclosingElement;
	}

	/**
	 * Converts the given Exception class name with standard naming (ReasonForMyException) this will
	 * strip the Exception from the class name and upper case with underscore separators creating a
	 * constant name. This is useful as a unique error code to be used in the system.
	 * 
	 * @param className
	 * @return
	 */
	public static String exceptionConstantName(String className) {
		String constantName = StringUtils.substringBefore(className, Exception.class.getSimpleName());
		constantName = StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(constantName), "_").toUpperCase();
		return constantName;
	}

	/**
	 * @see #exceptionConstantName(String)
	 * @param exceptionType
	 * @return
	 */
	public static String exceptionConstantName(Class<? extends Exception> exceptionType) {
		return exceptionConstantName(exceptionType.getSimpleName());
	}

	/**
	 * @see #exceptionConstantName(String)
	 * @param exception
	 * @return
	 */
	public static String exceptionConstantName(Exception exception) {
		return exceptionConstantName(exception.getClass());
	}

	public static String variableName(String param) {
		return param.toLowerCase();
	}

}
