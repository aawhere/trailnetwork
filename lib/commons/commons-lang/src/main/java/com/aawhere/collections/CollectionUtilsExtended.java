/**
 * 
 */
package com.aawhere.collections;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicates;
import com.google.common.collect.BiMap;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

/**
 * Anything that {@link CollectionUtils} doesn't provide.
 * 
 * @author Brian Chapman
 * 
 */
public class CollectionUtilsExtended {

	/**
	 * Chooses the last element in the collection or returns null. If the collection is not a
	 * ordered type, returns null.
	 * 
	 * @param <T>
	 * @param list
	 * @return
	 */
	public static <T> T lastOrNull(Collection<T> collection) {
		if (collection instanceof List) {
			return ListUtilsExtended.lastOrNull((List<T>) collection);
		} else if (collection instanceof SortedSet) {
			try {
				return ((SortedSet<T>) collection).last();
			} catch (NoSuchElementException e) {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * provides the size of the iterable. I couldn't find this in existing {@link Iterables} or
	 * {@link Functions}.
	 * 
	 * @see Iterables#size(Iterable)
	 * @return
	 */
	public static Function<Iterable<?>, Integer> sizeFunction() {
		return new Function<Iterable<?>, Integer>() {

			@Override
			public Integer apply(Iterable<?> input) {
				return (input == null) ? null : Iterables.size(input);
			}
		};
	}

	/**
	 * Gets the first from a collection or null if there isn't any. Useful for non-indexed
	 * collections.
	 * 
	 * @param select
	 * @return
	 */
	public static <T> T firstOrNull(Collection<T> collection) {
		T result;
		Iterator<T> iterator = collection.iterator();
		if (iterator.hasNext()) {
			result = iterator.next();
		} else {
			result = null;
		}
		return result;
	}

	/**
	 * As it sounds...gets the last in the list.
	 * 
	 * @param geoCells
	 * @return
	 */
	public static <T> T getLast(List<T> list) {
		if (list.size() != 0) {
			return list.get(list.size() - 1);
		} else {
			throw new IllegalArgumentException("Cannot return last element of empty list");
		}
	}

	/**
	 * Does a null check and returns an empty. Useful for avoiding null pointer exceptions since it
	 * is good standards to return an empty collection instead of a null.
	 * 
	 * @param collection
	 * @return
	 */
	public static <T> Collection<T> emptyIfNull(Collection<T> collection) {
		if (collection == null) {
			collection = Collections.emptyList();
		}
		return collection;
	}

	/**
	 * Given a class that represents a collection this will create a suitable implementation if one
	 * is known.
	 * 
	 * @param <T>
	 *            the type of the collection itself
	 * @param <P>
	 *            the parameter type inside the collection
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Collection<P>, P> T newCollection(Class<T> collectionType) {
		T result;
		try {
			result = collectionType.newInstance();
		} catch (Exception e) {
			if (List.class.isAssignableFrom(collectionType)) {
				result = (T) Lists.newArrayList();
			} else if (Set.class.isAssignableFrom(collectionType)) {
				if (SortedSet.class.isAssignableFrom(collectionType)) {
					result = (T) Sets.newTreeSet();
				} else {
					result = (T) Sets.newHashSet();
				}
			} else {
				throw new RuntimeException(collectionType + " not yet handled...add it if it's general");
			}
		}
		return result;
	}

	/**
	 * Helper to add all from a collection that may be null. This doesn't exist in
	 * {@link Collections}, {@link CollectionUtils} or {@link Collections2}.
	 * 
	 * @param to
	 *            the recipient
	 * @param from
	 *            the giver. ignored if null
	 */
	@Nonnull
	public static <C extends Collection<O>, O> C addAll(@Nonnull C to, @Nullable Iterable<O> from) {
		if (to != null && from != null) {
			Iterables.addAll(to, from);
		}
		return to;
	}

	/**
	 * Iterates the iterables ensuring the "lazy call" is called. {@link Iterables} size method does
	 * will only iterate if not a collection which makes sense. This will iterate every time.
	 * Replace me if they have something like this.
	 * 
	 * @param item
	 * @return the count of those items iterated that are not null
	 */
	public static Integer flush(Iterable<?> item) {
		int count = 0;
		for (Object object : item) {
			if (object != null) {
				count++;
			}
		}
		return count;
	}

	/**
	 * When there is a transitive relationship this will match the keys with the values you care
	 * about glued together by the map of keys. useful when an entity has a 1:1 relationship
	 * identified by the key
	 * 
	 * @param keys
	 *            containing a direct map of one id to another
	 * @param the
	 *            key you don't care about mapped to the value you do care about that will also
	 *            relate to those given in the keys
	 * @return
	 */
	public static <A, B, C> Map<A, C> swapKeys(Map<A, B> keys, Map<B, C> orignalValues) {
		Function<B, C> valueFunction = Functions.forMap(orignalValues, null);
		return Maps.filterValues(Maps.transformValues(keys, valueFunction), Predicates.notNull());
	}

	/**
	 * When the map you have has alias keys for the values you can provide a {@link BiMap} that will
	 * translate the keys and provide the values from the map for those keys.
	 * 
	 * This is necessary since Guava feels using a function would break the contract.
	 * 
	 * http://stackoverflow.com/questions/5733142/how-to-convert-mapstring-string-to-maplong-string-
	 * using-guava
	 * 
	 * The contract is not broken since we use a bimap to ensure uniqueness has been enforced.
	 * 
	 * @param map
	 * @param keys
	 *            works as a "Function", but ensures uniqueness in both directions
	 * @return immutable map that will lazy access the given and transform from the key given to the
	 *         necessary key using the provided keys
	 */
	public static <K1, K2, V> Map<K2, V> transformKeys(final Map<K1, V> map, final BiMap<K1, K2> keys) {

		return new Map<K2, V>() {

			private K1 key1(Object key) {
				return keys.inverse().get(key);
			}

			@Override
			public Set<K2> keySet() {
				// must only provide those keys that are found in the map, not provided in the keys
				SetView<K1> intersection = Sets.intersection(map.keySet(), keys.keySet());
				Iterable<K2> k2s = Iterables.transform(intersection, Functions.forMap(keys));
				return Sets.newHashSet(k2s);
			}

			@Override
			public Collection<V> values() {
				return map.values();
			}

			@Override
			public Set<java.util.Map.Entry<K2, V>> entrySet() {
				HashSet<Map.Entry<K2, V>> entrySet = Sets.newHashSet();
				Set<K2> k2s = keys.values();
				for (K2 k2 : k2s) {
					final V value = get(k2);
					if (value != null) {
						entrySet.add(Pair.of(k2, value));
					}
				}
				return entrySet;
			}

			@Override
			public int size() {
				return map.size();
			}

			@Override
			public boolean isEmpty() {
				return map.isEmpty();
			}

			@Override
			public boolean containsKey(Object key) {
				return map.containsKey(key1(key));
			}

			@Override
			public boolean containsValue(Object value) {
				return map.containsValue(value);
			}

			@Override
			public V get(Object key) {
				return map.get(key1(key));
			}

			@Override
			public V put(K2 key, V value) {
				return immutable();
			}

			@Override
			public V remove(Object key) {
				return immutable();
			}

			@Override
			public void putAll(Map<? extends K2, ? extends V> m) {
				immutable();
			}

			@Override
			public void clear() {
				immutable();
			}

			/*
			 * (non-Javadoc)
			 * @see java.lang.Object#hashCode()
			 */
			@Override
			public int hashCode() {
				return keySet().hashCode();
			}

			/*
			 * (non-Javadoc)
			 * @see java.lang.Object#equals(java.lang.Object)
			 */
			@Override
			public boolean equals(Object obj) {
				if (obj instanceof Map) {
					Map<K2, V> other = (Map<K2, V>) obj;
					return CollectionUtils.isEqualCollection(entrySet(), other.entrySet());
				}
				return false;
			}

			/*
			 * (non-Javadoc)
			 * @see java.lang.Object#toString()
			 */
			@Override
			public String toString() {
				return entrySet().toString();
			}

			private V immutable() {
				throw new UnsupportedOperationException("immutable");
			}
		};
	}

	/**
	 * Analogy to {@link Maps#transformValues(Map, Function)}, but the result is a biMap.
	 * 
	 * @param map
	 * @param function
	 */
	public static <A, B, C> BiMap<A, C> transformValuesIntoBiMap(Map<A, B> map, Function<B, C> function) {

		final Map<A, C> aToC = Maps.transformValues(map, function);
		return HashBiMap.create(aToC);
	}

	/**
	 * Practically does nothing since it only casts to the type you wish, but this helps get through
	 * generics that aren't setup properly.
	 * 
	 * @deprecated use {@link FunctionsExtended}
	 * @return
	 */
	public static <T> Function<Object, T> castFunction() {
		return FunctionsExtended.castFunction();
	}

	/**
	 * Provides a {@link Collection} when given an {@link Iterable}. If the Iterable is an instance
	 * of Collection a cast is made and the iterable returned. If not then an {@link ImmutableList}
	 * is provided.
	 * 
	 * @param iterable
	 *            to be transformed to a Collection
	 * @return a Collection...either the orginal if the Iterable is a Collection or an
	 *         {@link ImmutableList} if not already a collection.
	 */
	public static <T> Collection<T> toCollection(Iterable<T> iterable) {
		if (iterable instanceof Collection) {
			return (Collection<T>) iterable;
		} else {
			return Lists.newArrayList(iterable);
		}
	}
}
