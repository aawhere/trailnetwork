/**
 *
 */
package com.aawhere.text.format.custom;

/**
 * @author roller
 * 
 */
public class DoubleFormat
		extends DecimalFormat<Double> {

	public DoubleFormat() {
		super();
	}

	public DoubleFormat(int maximumFractionDigits, int minimumFractionDigits, boolean isGroupingEnabled) {
		super(maximumFractionDigits, minimumFractionDigits, isGroupingEnabled);
	}

	@Override
	public Class<Double> handlesType() {
		return Double.class;
	}

}
