/**
 * 
 */
package com.aawhere.id;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.google.common.base.Function;

/**
 * Passes through the entity with a refresh of the entity if a provider is given. The original
 * entity will be returned if no provided is given..null or otherwise.
 * 
 * @author aroller
 * 
 */
public class IdentifiableXmlAdapter<E extends Identifiable<I>, I extends Identifier<?, E>>
		extends XmlAdapter<E, E> {

	final private Function<I, E> provider;

	public IdentifiableXmlAdapter() {
		super();
		this.provider = null;
	}

	public IdentifiableXmlAdapter(IdentifierToIdentifiableXmlAdapter<E, I> idAdapter) {
		this.provider = idAdapter.getProvider();
	}

	/** This accepts ids since the field it's associated with is the id to support bulk loading. */
	public IdentifiableXmlAdapter(Function<I, E> provider) {
		this.provider = provider;
	}

	@Override
	public E unmarshal(E route) throws Exception {
		return route;
	}

	@Override
	public E marshal(E entity) throws Exception {
		return (entity != null && this.provider != null) ? provider.apply(entity.getId()) : entity;
	}
}
