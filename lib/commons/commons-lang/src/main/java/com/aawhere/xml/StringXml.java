/**
 * 
 */
package com.aawhere.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.google.common.base.Function;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Since returning a null string, Integer or other simple types as a result from an adapter causes a
 * null pointer exception this is a general purpose wrapper around the value you provide allowing
 * you to assign a null.
 * 
 * http://stackoverflow.com/questions/11894193/jaxb-marshal-empty-string-to-null-globally
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(name = "result", namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@ApiModel(description = "General container wrapping a single value")
public class StringXml {

	@XmlElement
	@ApiModelProperty(value = "The only result in the format as documented by the method")
	final public String value;

	StringXml() {
		this.value = null;
	}

	public StringXml(String value) {
		this.value = value;
	}

	/**
	 * Does a {@link String#valueOf(Object)}
	 * 
	 * @param value
	 */
	public StringXml(Object value) {
		this(String.valueOf(value));
	}

	public static final Function<String, StringXml> function() {
		return new Function<String, StringXml>() {

			@Override
			public StringXml apply(String input) {
				return new StringXml(input);
			}
		};
	}
}
