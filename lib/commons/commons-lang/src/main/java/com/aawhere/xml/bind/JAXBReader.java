/**
 *
 */
package com.aawhere.xml.bind;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Encapsulates the details of unmarshalling xml strings into XJC generated objects.
 * 
 * @author roller
 * 
 */
public class JAXBReader<SchemaType extends Object, SchemaTypeObjectFactory extends Object> {

	private Unmarshaller unmarshaller;

	protected JAXBReader(Class<SchemaTypeObjectFactory> objectFactoryClass) {

		try {
			final JAXBContext ctx = JAXBContextFactory.instance().getContext(objectFactoryClass);
			this.unmarshaller = ctx.createUnmarshaller();
		} catch (JAXBException e) {
			throw new RuntimeException("Problem creating unmarshaller for " + objectFactoryClass, e);
		}
	}

	/**
	 * the main method used to get a reader that will unmarshall an xml doc and provide the XJC
	 * Generated Document Type.
	 * 
	 * @see JAXBElement
	 * 
	 * @param <SchemaType>
	 * @param <SchemaTypeObjectFactory>
	 * @param objectFactoryClass
	 * @return
	 */
	public static <SchemaType extends Object, SchemaTypeObjectFactory extends Object>
			JAXBReader<SchemaType, SchemaTypeObjectFactory>
			getReader(Class<SchemaTypeObjectFactory> objectFactoryClass) {
		JAXBReader<SchemaType, SchemaTypeObjectFactory> reader = null;
		if (reader == null) {
			reader = new JAXBReader<SchemaType, SchemaTypeObjectFactory>(objectFactoryClass);
			// TODO: TN-27
			// cache.put(objectFactoryClass, reader);
		}

		return reader;

	}

	@SuppressWarnings("unchecked")
	public SchemaType read(InputStream inputStream) throws JAXBReadException {
		try {
			final Object result = this.unmarshaller.unmarshal(inputStream);
			SchemaType root;
			if (result instanceof JAXBElement) {
				root = ((JAXBElement<SchemaType>) result).getValue();
			} else {
				root = (SchemaType) result;
			}
			return root;
		} catch (JAXBException e) {
			throw new JAXBReadException(e);
		}
	}
}
