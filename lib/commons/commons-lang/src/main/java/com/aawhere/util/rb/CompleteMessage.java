/**
 * 
 */
package com.aawhere.util.rb;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * Reports a {@link Message} along with a set of parameters that will be used to complete the
 * message.
 * 
 * @author roller
 * 
 */
@XmlJavaTypeAdapter(CompleteMessageXmlAdapter.class)
public class CompleteMessage
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6878947464992966054L;

	public static Builder create(Message message) {
		return new Builder(message);
	}

	/**
	 * Used to construct all instances of DynamicResourceBundleMessage.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<CompleteMessage> {

		public Builder() {
			super(new CompleteMessage());
		}

		public Builder(Message message) {
			this();
			setMessage(message);
		}

		public Builder setMessage(Message message) {
			building.message = message;
			return this;
		}

		/**
		 * Provids easy access to those that have an array of keys and an array of values. The key
		 * will correspond to the value by the same index.
		 * 
		 * @param message
		 * @param indexedKeys
		 * @param indexedParams
		 * @throws IllegalArgumentException
		 *             when keys length do not match indexed params
		 */
		@SuppressWarnings("unchecked")
		public <B extends Builder> B addParameters(ParamKey[] indexedKeys, Object[] indexedParams) {
			if (indexedKeys.length != indexedParams.length) {
				throw new IllegalArgumentException(indexedKeys.length
						+ " keys found not matching the number of params " + indexedParams.length);
			}
			int counter = 0;
			for (ParamKey key : indexedKeys) {
				this.addParameter(key, indexedParams[counter]);
				counter++;
			}
			return (B) this;
		}

		/**
		 * Associates the given key with the value that is to replace the key found inside the
		 * message.
		 * 
		 * @param paramKey
		 * @param paramValue
		 * @return
		 */
		@SuppressWarnings("unchecked")
		public <B extends Builder> B param(ParamKey paramKey, Object paramValue) {
			return (B) addParameter(paramKey, paramValue);
		}

		/**
		 * @see #param(ParamKey, Object)
		 * @param paramKey
		 * @param paramValue
		 * @return
		 */
		public Builder addParameter(ParamKey paramKey, Object paramValue) {
			Object pushed = building.parameters.put(paramKey, paramValue);
			if (pushed != null) {
				throw new IllegalArgumentException(paramKey + " already had a value " + pushed);
			}
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {

			super.validate();
			Assertion.assertNotNull("message", building.message);
			building.validateKeys();

		}
	}

	/** use {@link Builder} to construct DynamicResourceBundleMessage */
	private CompleteMessage() {
	}

	private Message message;
	private Map<ParamKey, Object> parameters = new HashMap<ParamKey, Object>();

	public Message getMessage() {
		return message;
	}

	public Map<ParamKey, Object> getParameters() {
		return parameters;
	}

	protected void validateKeys() {
		Set<ParamKey> keys = this.parameters.keySet();
		for (ParamKey paramKey : keys) {
			validate(paramKey);
		}
	}

	/**
	 * Given the key this will ensure the parameter is registered with the message.
	 * 
	 * @param key
	 */
	protected void validate(ParamKey key) {
		ParamKey[] keys = this.message.getParameterKeys();

		if (!ArrayUtils.contains(keys, key)) {
			String additionalMessage = "";
			if (!this.message.getClass().equals(key.getClass().getEnclosingClass())) {
				additionalMessage += key.getClass() + " is not from the same class as the message "
						+ this.message.getClass();
			}
			throw new IllegalArgumentException(
					key
							+ " from class "
							+ " is not a valid parameter for message "
							+ this.message.getValue()
							+ ".  You need to add the Param when declaring the enum which have these params already associated: "
							+ ArrayUtils.toString(keys) + ".  " + additionalMessage);
		}
	}

	/**
	 * Provides the formatted object without localization. Mostly for internal messaging, but really
	 * handy.
	 * 
	 */
	@Override
	public String toString() {

		return KeyedMessageFormat.create(this.message.getValue()).putAll(parameters).build().getFormattedMessage();
	}
}
