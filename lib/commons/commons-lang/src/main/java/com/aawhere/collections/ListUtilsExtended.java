/**
 *
 */
package com.aawhere.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.SortedSet;

import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections4.list.UnmodifiableList;

import com.aawhere.util.CollectionUtilsExtended;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Anything that {@link ListUtils} doesn't provide.
 * 
 * @author Brian Chapman
 * 
 */
public class ListUtilsExtended
		extends com.aawhere.util.ListUtilsExtended {

	public static Boolean hasIndex(List<?> list, int index) {
		return list.size() > index && index >= 0;
	}

	/**
	 * Constructs a new {@link ArrayList} if the one given is already null. This is a shortcut for
	 * lazy created lists which is why it doesn't expose the ArrayList allowing that which is passed
	 * in to be returned and either assigned to a generic List which is often used as a property
	 * declaration.
	 * 
	 * this.myList = newArrayListIfNull(this.myList);
	 * 
	 * @param possiblyExisting
	 * @return
	 */
	public static <T> List<T> newArrayListIfNull(List<T> possiblyExisting) {
		if (possiblyExisting == null) {
			possiblyExisting = Lists.newArrayList();
		}
		return possiblyExisting;
	}

	/**
	 * @see CollectionUtilsExtended#emptyIfNull(Collection)
	 * @param collection
	 * @return
	 */
	public static <T> List<T> emptyIfNull(List<T> collection) {
		if (collection == null) {
			collection = Collections.emptyList();
		}
		return collection;
	}

	/**
	 * Chooses the first in the list or returns null if list is empty.
	 * 
	 * @param <T>
	 * @param list
	 * @return
	 */
	public static <T> T firstOrNull(List<T> list) {
		return indexOrNull(list, 0);
	}

	/**
	 * Chooses the last in the list or returns null if list is empty.
	 * 
	 * @param <T>
	 * @param list
	 * @return
	 */
	public static <T> T lastOrNull(List<T> list) {
		return indexOrNull(list, list.size() - 1);
	}

	/**
	 * Chooses the last in the sorted set or returns null if set is empty.
	 * 
	 * @param <T>
	 * @param sortedSet
	 * @return
	 */
	public static <T> T lastOrNull(SortedSet<T> sortedSet) {
		try {
			return sortedSet.last();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	/**
	 * Returns the element at the index or null if the index does not match a spot in the list.
	 * 
	 * @param <T>
	 * @param list
	 * @param index
	 * @return
	 */
	public static <T> T indexOrNull(List<T> list, Integer index) {
		T result;
		if (index >= 0 && list.size() > index) {
			result = list.get(index);

		} else {
			result = null;
		}
		return result;
	}

	/**
	 * @see CollectionUtils#select(java.util.Collection, org.apache.commons.collections.Predicate)
	 * @see CollectionUtilsExtended#firstOrNull(Collection)
	 * 
	 * @param <T>
	 * @param list
	 * @param predicate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T selectFirstOrNull(List<T> list, Predicate predicate) {
		Collection<T> selected = CollectionUtils.select(list, predicate);
		T result = CollectionUtilsExtended.firstOrNull(selected);
		return result;
	}

	/**
	 * @param track
	 * @return
	 */
	public static <T> T secondToLastOrNull(List<T> list) {
		if (list.size() > 1) {
			return list.get(list.size() - 2);
		} else {
			return null;
		}
	}

	public static <A, B> List<B> transformWithoutNulls(Iterable<A> input, Function<A, B> function) {
		return Lists.newArrayList(Iterables.filter(Iterables.transform(input, function), Predicates.notNull()));
	}

	/**
	 * Same as {@link Collections#unmodifiableList(List)}, but if null returns null.
	 * 
	 * @param modifiableOrNull
	 * @return
	 */
	public static <T> List<T> immutable(@Nullable List<T> modifiableOrNull) {
		if (modifiableOrNull == null) {
			return null;
		}
		return ImmutableList.copyOf(modifiableOrNull);
	}

	/**
	 * Efficient way of checking if an iterable is an {@link ImmutableList} or
	 * {@link UnmodifiableList} list and just casts. If not then it creates an {@link ImmutableList}
	 * .
	 * 
	 * @param iterable
	 * @return
	 */
	@Nullable
	public static <T> List<T> immutable(@Nullable Iterable<T> iterable) {
		if (iterable == null) {
			return null;
		}
		if (iterable instanceof ImmutableList || iterable instanceof UnmodifiableList) {
			return (List<T>) iterable;
		}
		return ImmutableList.<T> builder().addAll(iterable).build();
	}

	/** Simple removal of the last element by index via the size. */
	public static <T> List<T> removeLast(List<T> list) {
		list.remove(list.size() - 1);
		return list;
	}

	public static <E> ArrayList<E> newArrayList(Iterable<E> iterable, E... elements) {
		ArrayList<E> newArrayList = Lists.newArrayList(elements);
		Iterables.addAll(newArrayList, iterable);
		return newArrayList;
	}
}
