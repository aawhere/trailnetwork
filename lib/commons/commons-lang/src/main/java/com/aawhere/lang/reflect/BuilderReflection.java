/**
 *
 */
package com.aawhere.lang.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.aawhere.lang.ObjectBuilder;

/**
 * Useful helpers for constructing objects that have an embedded builder that extends
 * {@link ObjectBuilder}.
 * 
 * @author Brian Chapman
 * 
 */
abstract public class BuilderReflection<ObjectT, BuilderT extends ObjectBuilder<ObjectT>> {

	protected BuilderReflection() {
	}

	public Boolean isMutable(Class<BuilderT> builderType, Class<ObjectT> objectType) {
		Boolean mutable;

		try {
			builderType.getDeclaredConstructor(objectType);
			mutable = true;
		} catch (Exception e) {
			mutable = false;
		}
		return mutable;

	}

	/**
	 * Provides the default Builder for an entity.
	 * 
	 * @param object
	 * @return
	 * @throws ClassNotFoundException
	 *             when there is no Builder
	 * @throws InstantiationException
	 *             when the constructor is not available.
	 */
	public BuilderT constructDefaultBuilder(ObjectT object) throws ClassNotFoundException, InstantiationException {
		Class<BuilderT> builderClass = (Class<BuilderT>) builderClassFrom(object);
		return constructDefaultBuilder(builderClass);
	}

	/**
	 * Provides the default builder for a entity builder's class.
	 * 
	 * @see Class#newInstance()
	 * 
	 * @param builderClass
	 * @return
	 * @throws InstantiationException
	 *             when the constructor doesn't exist or is not accessible.
	 */
	public BuilderT constructDefaultBuilder(Class<BuilderT> builderClass) throws InstantiationException {

		try {
			return builderClass.newInstance();
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public BuilderT constructMutationBuilder(ObjectT object) throws NoSuchMethodException, ClassNotFoundException {
		final Class<BuilderT> builderClass = builderClassFrom(object);
		return (BuilderT) constructMutationBuilder(builderClass, object);
	}

	public BuilderT constructMutationBuilder(Class<BuilderT> builderClass, ObjectT object) throws NoSuchMethodException {
		Constructor<BuilderT> mutationConstructor = builderClass.getDeclaredConstructor(object.getClass());
		mutationConstructor.setAccessible(true);
		try {
			return mutationConstructor.newInstance(object);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Class<BuilderT> builderClassFrom(ObjectT object) throws ClassNotFoundException {

		Class<?> builderClass = null;
		Class<?>[] innerClasses = object.getClass().getClasses();
		for (int whichClass = 0; whichClass < innerClasses.length && builderClass == null; whichClass++) {
			Class<?> innerClass = innerClasses[whichClass];
			if (ObjectBuilder.class.isAssignableFrom(innerClass)) {
				builderClass = innerClass;
			}
		}

		if (builderClass == null) {
			throw new ClassNotFoundException(object.getClass().getSimpleName() + " doesn't have a Builder");
		}
		return (Class<BuilderT>) builderClass;
	}
}
