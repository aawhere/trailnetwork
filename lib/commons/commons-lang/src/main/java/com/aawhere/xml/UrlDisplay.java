/**
 * 
 */
package com.aawhere.xml;

import java.net.URI;
import java.net.URL;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Function;

/**
 * // return string directly results in a null return causes null pointer exception //
 * http://stackoverflow.com/questions/11894193/jaxb-marshal-empty-string-to-null-globally
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE, name = "url")
@XmlAccessorType(XmlAccessType.NONE)
public class UrlDisplay {

	public UrlDisplay() {
		this.url = StringUtils.EMPTY;
	}

	/**
	 * 
	 */
	public UrlDisplay(String url) {
		this.url = url;
	}

	public UrlDisplay(URL url) {
		this(url.toString());
	}

	public UrlDisplay(URI uri) {
		this(uri.toString());
	}

	/**
	 * @param urlXml
	 */
	public UrlDisplay(UrlDisplay urlXml) {
		this(urlXml.url);
	}

	@Override
	public String toString() {
		return url;
	}

	// do not try @XmlValue...it blows up when the UrlDisplay is null
	@XmlAttribute
	final private String url;

	private enum UrlDisplayFunction implements Function<URL, UrlDisplay> {
		INSTANCE;
		@Override
		@Nullable
		public UrlDisplay apply(@Nullable URL url) {
			return (url != null) ? new UrlDisplay(url) : null;
		}

		@Override
		public String toString() {
			return "UrlDisplayFunction";
		}
	}

	public static Function<URL, UrlDisplay> urlFunction() {
		return UrlDisplayFunction.INSTANCE;
	}
}
