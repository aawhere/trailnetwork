/**
 * 
 */
package com.aawhere.id;

import java.util.Map;

/**
 * Provides an xml adapter loaded with the ids given.
 * 
 * @author aroller
 * 
 */
public interface IdentifiersToIdentifiablesXmlAdapterProvider<I extends Identifier<?, T>, T extends Identifiable<I>> {

	Map<I, T> xmlAdapterPreloaded(Iterable<I> ids);
}
