/**
 * 
 */
package com.aawhere.lang.string;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

import com.aawhere.log.LoggerFactory;

import com.google.common.base.Function;

/**
 * Assists in creating objects using the standard conventions of creating and serializing objects
 * using strings.
 * 
 * These conventions are to provide a constructor receiving a single string, a static "valueOf"
 * method with a single string. The resulting object should produce this String using the standard
 * {@link Object#toString()} method.
 * 
 * Use {@link #qualifies(Type)} to inspect if a class meets the necessary requirements.
 * 
 * The toString method doesn't necessarily have to return the exact same string, but an object
 * constructed from a string must create an "equivalent" object that is constructed from the
 * toString result.
 * 
 * Integer expected = new Integer("7"); assertEquals(expected,new Integer(expected.toString());
 * 
 * This supports the ability for an object to modify the string that is provided such as
 * toLowerCase, etc.
 * 
 * Note: Performance could be improved by providing pre-inspection methods rather than exceptions
 * being thrown.
 * 
 * @author aroller
 * 
 */
public class FromString {

	/**
	 * 
	 */
	private static final String VALUE_OF_METHOD_NAME = "valueOf";

	/**
	 * Given a string and the class of the desired result this will construct a new instance of the
	 * type if it contains a constructor with a single argument of type string that will
	 * successfully build the instance.
	 * 
	 * 
	 * @param type
	 * @param string
	 * @return
	 * @throws NoSuchMethodException
	 *             when the constructor does not exist
	 * @throws StringFormatException
	 */
	public static <T> T construct(Class<T> type, String string) throws NoSuchMethodException, StringFormatException {
		Constructor<T> constructor = type.getConstructor(String.class);
		constructor.setAccessible(true);
		try {
			T result = constructor.newInstance(string);
			return result;
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			LoggerFactory.getLogger(FromString.class).throwing(FromString.class.getName(), "valueOf", e);
			throw new StringFormatException(string, type);
		}
	}

	/**
	 * Given a string and a type this will construct a new instance using a standard convention of a
	 * method named valueOf that accepts the provided string and returns an instance of the type
	 * that the method is provided.
	 * 
	 * @param type
	 * @param string
	 * @return
	 * @throws NoSuchMethodException
	 * @throws StringFormatException
	 * @throws SecurityException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T valueOf(Class<T> type, String string) throws NoSuchMethodException, StringFormatException {
		try {
			Method valueOfMethod = getValueOfMethod(type);
			Object valueOfResult = valueOfMethod.invoke(type, string);
			if (type.isAssignableFrom(valueOfResult.getClass())) {
				return (T) valueOfResult;
			} else {
				throw new IllegalArgumentException(valueOfMethod + " does not return the required " + type);
			}
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			LoggerFactory.getLogger(FromString.class).throwing(FromString.class.getName(), "valueOf", e);
			throw new StringFormatException(string, type);
		}
	}

	/**
	 * @param type
	 * @return
	 * @throws NoSuchMethodException
	 */
	private static <T> Method getValueOfMethod(Class<T> type) throws NoSuchMethodException {
		return type.getMethod(VALUE_OF_METHOD_NAME, String.class);
	}

	/**
	 * Given a type and a string this will attempt to construct and instance of the given type from
	 * the string using known conventions.
	 * 
	 * @see #valueOf(Class, String)
	 * @see #construct(Class, String)
	 * 
	 * @param type
	 * @param string
	 * @return
	 * @throws NoSuchMethodException
	 * @throws StringFormatException
	 */
	public static <T> T newInstance(Class<T> type, String string) throws NoSuchMethodException, StringFormatException {
		try {
			return construct(type, string);
		} catch (NoSuchMethodException e) {
			return valueOf(type, string);
		}
	}

	/**
	 * Indicates weather or not the given type will meets the requirements to be used with
	 * FromString.
	 * 
	 * @param type
	 */
	public static <T> Boolean qualifies(Class<T> type) {

		try {
			return type.getDeclaredConstructor(String.class) != null;
		} catch (Exception e) {

			try {
				return getValueOfMethod(type) != null;
			} catch (NoSuchMethodException e1) {
				return false;
			}

		}
	}

	/**
	 * Provides the ability for any object that conforms to the {@link FromString} requirements to
	 * be built.
	 * 
	 * @param type
	 * @return
	 */
	public static <T> Function<String, T> function(final Class<T> type) {
		return new Function<String, T>() {
			@Override
			public T apply(String input) {
				try {
					return (input == null) ? null : newInstance(type, input);
				} catch (NoSuchMethodException | StringFormatException e) {
					return null;
				}
			}
		};
	}

}
