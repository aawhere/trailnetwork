/**
 * 
 */
package com.aawhere.collections;

import java.util.Map;

import com.google.common.collect.HashBiMap;

/**
 * @author aroller
 * 
 */
public class HashBiMapXmlAdapter
		extends MapXmlAdapter {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.collections.MapXmlAdapter#createMap()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected Map createMap() {

		return HashBiMap.create();
	}

}
