/**
 * 
 */
package com.aawhere.io.zip;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.IOUtils;

import com.aawhere.lang.ObjectBuilder;

/**
 * Does a little bit of the repetative work to compress and uncompress files using GZip format.
 * 
 * This compression doesn't really use IO, but rather bytes and compression so we avoid using
 * IOException or any exceptions to report much of anything. If there is confusion about whether
 * data is compressed this class just handles it without alerting. The client can always use the
 * {@link #isCompressed(byte[])} method to check themselves.
 * 
 * 
 * 
 * @see GZIPInputStream
 * @see GZIPOutputStream
 * @see IOUtils
 * 
 * @see http
 *      ://www.javadocexamples.com/java_source/org/mule/util/compression/GZipCompression.java.html
 * 
 * @author Aaron Roller
 * 
 */
public class GzipCompression {

	/**
	 * Used to construct all instances of GzipCompression.
	 */
	public static class Builder
			extends ObjectBuilder<GzipCompression> {

		public Builder() {
			super(new GzipCompression());
		}

	}// end Builder

	/** Use {@link Builder} to construct GzipCompression */
	private GzipCompression() {
	}

	/**
	 * Compresses uncompressed bytes using GZip format
	 * 
	 * If the uncompressedData is already compressed they will just be returned unchanged.
	 * 
	 * @param uncompressedData
	 *            the bytes to be compressed
	 * @return compressed data
	 */
	public byte[] compress(final byte[] uncompressedData) {

		byte[] result;

		if (isCompressed(uncompressedData)) {
			result = uncompressedData;
		} else {
			ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();

			GZIPOutputStream zipOut;
			try {
				zipOut = new GZIPOutputStream(bytesOut);

				try {
					IOUtils.write(uncompressedData, zipOut);
				} catch (IOException e) {
					// honestly, what is anyone gonna do about this? It's just bytes!
					throw new RuntimeException(e);
				} finally {
					// we want to know if this fails because the gzip will be invalid if not closed
					// properly
					// make a checked exception if it appears to be a case that should be handled.
					// consider throwing CorruptGzipException
					zipOut.close();
				}
			} catch (IOException e1) {
				throw new RuntimeException(e1);
			}
			result = bytesOut.toByteArray();
			// no need to close ..per docs has no affect
		}

		return result;

	}

	/**
	 * Uncompresses already compressed bytes.
	 * 
	 * If the data isn't compressed it is just returned without modification.
	 * 
	 * @param compressedData
	 * @return
	 * @throws CorruptGzipFormatException
	 */
	public byte[] uncompress(final byte[] compressedData) throws CorruptGzipFormatException {
		byte[] result;

		if (isCompressed(compressedData)) {
			ByteArrayInputStream bytesIn = new ByteArrayInputStream(compressedData);
			GZIPInputStream zipIn;
			try {
				zipIn = new GZIPInputStream(bytesIn);
				try {
					result = IOUtils.toByteArray(zipIn);
				} catch (EOFException eof) {
					throw new CorruptGzipFormatException();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			} catch (IOException e1) {
				throw new RuntimeException(e1);
			}
		} else {
			result = compressedData;
		}
		return result;
	}

	/**
	 * Determines if a byte array is compressed. The java.util.zip GZip implementaiton does not
	 * expose the GZip header so it is difficult to determine if a string is compressed.
	 * 
	 * @param bytes
	 *            an array of bytes
	 * @return true if the array is compressed or false otherwise
	 * @throws java.io.IOException
	 *             if the byte array couldn't be read
	 */
	public static boolean isCompressed(byte[] bytes) {
		if ((bytes == null) || (bytes.length < 2)) {
			return false;
		} else {
			return ((bytes[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (bytes[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8)));
		}
	}
}
