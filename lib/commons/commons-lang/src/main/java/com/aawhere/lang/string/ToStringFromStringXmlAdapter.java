/**
 * 
 */
package com.aawhere.lang.string;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Serializes the presumably simple obect into it's string and rebuilds the object from the string.
 * 
 * @author aroller
 * 
 * @see ToStringFromStringXmlAdapter
 */
abstract public class ToStringFromStringXmlAdapter<T>
		extends XmlAdapter<String, T> {

	private final Class<T> type;

	/**
	 * 
	 */
	public ToStringFromStringXmlAdapter(Class<T> type) {
		this.type = type;

	}

	/*
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public T unmarshal(String value) throws Exception {
		return FromString.newInstance(type, value);
	}

	/*
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(T object) throws Exception {
		// returning null causes problems with adapters
		return String.valueOf(object);
	}

}
