/**
 * 
 */
package com.aawhere.lang;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * @author aroller
 * 
 */
public class BuilderUtil {

	/**
	 * Give me the unbuilt builders and I will build them...plain and simple. This purposely doesn't
	 * use a {@link Function} since it is lazy and must be idempotent since it may be called
	 * multiple times, but build can only be called once.
	 * 
	 * @param builders
	 * @return
	 */
	public static <B extends ObjectBuilder<O>, O> List<O> buildAll(Iterable<B> builders) {
		ArrayList<O> builts = new ArrayList<>();
		for (B b : builders) {
			builts.add(b.build());
		}
		return builts;
	}

	/** provides an Iterable that will build when iterated. */
	public static <B extends ObjectBuilder<O>, O> Iterable<O> build(Iterable<B> builders) {
		final ObjectBuilderFunction<O> function = ObjectBuilderFunction.build();
		return Iterables.transform(builders, function);
	}

	/**
	 * Given any implementation of an ObjectBuilder this will provide the object being mutated,
	 * provided build has not yet been called. As this provides access to an otherwise protected
	 * item use with discretion.
	 * 
	 * @return
	 */
	public static <B extends ObjectBuilder<O>, O> Function<B, O> buildingFunction() {
		return new Function<B, O>() {
			@Override
			public O apply(B input) {
				return (input == null) ? null : input.building;
			}
		};
	}

}
