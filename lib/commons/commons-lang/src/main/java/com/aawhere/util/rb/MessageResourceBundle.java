/**
 *
 */
package com.aawhere.util.rb;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import com.aawhere.lang.If;
import com.aawhere.lang.enums.EnumUtil;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.text.format.custom.CustomFormatFactory;
import com.google.common.collect.Maps;
import com.ibm.icu.text.MessageFormat;
import com.ibm.icu.util.ULocale;

/**
 * A simple class that encourages good use of resource bundles and their associated enumerations.
 * 
 * 
 * Basically, if there is gonna be any reference to a resource bundle key, it must be done via an
 * enumeration that extends {@link Message} key.
 * 
 * This class will take the associated ResourceBundleKey enum class and look for an associated
 * properties file. If no properties file exists, then it will just use the message provided by
 * {@link Message#getValue()} (which is easier to maintain by coders).
 * 
 * Upon start-up, this ManagedResourceBundle will {@link #validate()} the provided {@link Message}
 * to make sure all parameters match the messages provided.
 * 
 * @see BaseException to view an example of how the messages should be handled.
 * 
 * 
 * @author roller
 * 
 */
public class MessageResourceBundle {

	private PropertyResourceBundle resourceBundle;
	private Class<? extends Enum<? extends Message>> keyClass;
	private String bundleBaseName;

	/**
	 * Given the Enumeration Class representing the keys in the bundle this will look for the
	 * resource bundle matches exactly the fully qualified name of the Enumeration provied.
	 * 
	 * @param keyClass
	 *            the enumeration matching the keys in the properties file.
	 */
	public MessageResourceBundle(Class<? extends Enum<? extends Message>> keyClass) {
		this(keyClass, keyClass.getName());

	}

	/**
	 * provides an alternate bundle name to be used if it doesn't match teh standard pattern as
	 * described in {@link #ManagedResourceBundle(Class)}.
	 * 
	 * @param keyClass
	 * @param baseName
	 */
	public MessageResourceBundle(Class<? extends Enum<? extends Message>> keyClass, String baseName) {
		super();
		this.keyClass = keyClass;
		this.bundleBaseName = baseName;
		try {
			ResourceBundle rb = ResourceBundle.getBundle(baseName);
			if (rb instanceof PropertyResourceBundle) {
				this.resourceBundle = (PropertyResourceBundle) rb;
			} else {
				throw new RuntimeException("unable to find resource bundle with name of " + baseName);
			}
		} catch (MissingResourceException e) {

			Logger logger = Logger.getLogger(MessageResourceBundle.class.getSimpleName());
			logger.fine("No properties file matching " + baseName
					+ ".  No localization will be supported, but messages will work per the rules of "
					+ Message.class.getSimpleName());
		}
		validate();
	}

	/**
	 * Answers the simple question if a key is in the resource bundle indicating this is a qualified
	 * supplier.
	 * 
	 * @param key
	 * @return
	 */
	public boolean containsKey(String key) {
		// check to see if it is in the properties file or constants
		return this.resourceBundle != null && this.resourceBundle.containsKey(key)
				|| containsResourceBundleMessage(key);
	}

	/**
	 * Just uses toString from the enumeration to search for the key.
	 * 
	 * @see #containsKey(String)
	 * 
	 * @param message
	 * @return
	 */
	public boolean containsKey(Message message) {
		return containsKey(message.toString());
	}

	public boolean containsResourceBundleMessage(String key) {
		try {
			return getResouceBundleMessage(key) != null;

		} catch (MissingResourceException e) {
			return false;
		}
	}

	public String getString(@Nonnull Message message, @Nonnull Locale locale) {

		final String messageFromEnum = message.getValue();
		String localizedMessage = null;

		// no resource bundle so this will be over quick...the value must come from the enum
		if (this.resourceBundle == null) {
			// property from file overrides the constant
			if (messageFromEnum != null) {
				localizedMessage = messageFromEnum;
			} else {
				throw new MissingResourceException(message.toString()
						+ " is enumerated, but provides no value and there is no matching properties file for "
						+ this.bundleBaseName, message.getClass().getName(), message.toString());
			}
		} else {

			PropertyResourceBundle preferredBundle;
			// only lookup non-default if locale is different than default
			if (this.resourceBundle.getLocale().equals(locale)) {
				// we are gonna use the default, but in rare cases where the value is not
				// provided...go
				// to the resource bundle
				if (messageFromEnum != null) {
					localizedMessage = messageFromEnum;
				} else {
					preferredBundle = this.resourceBundle;
				}
			}
			if (localizedMessage == null) {
				// If this shows up as a hotspot, we can easily manage these in a local Map, but it
				// appears they use a cache
				preferredBundle = (PropertyResourceBundle) ResourceBundle.getBundle(bundleBaseName, locale);
				// throws MissingResource if still not found
				try {
					localizedMessage = preferredBundle.getString(message.toString());
				} catch (MissingResourceException e) {
					// this exception is gonna get thrown a lot and could be a bottleneck
					// FIXME: An annotation processor can take all values in Enum and put it in the
					// default
					// file
					localizedMessage = messageFromEnum;
				}
			}
		}
		if (localizedMessage == null) {
			throw new MissingResourceException(message.toString()
					+ "must return something for Message#getValue() or have a matching property in "
					+ this.bundleBaseName, this.bundleBaseName, message.toString());
		}

		return localizedMessage;
	}

	/**
	 * Formats a message with the default locale.
	 * 
	 * @see #formatMessage(CompleteMessage, Locale)
	 * @param message
	 * @return
	 */
	public String formatMessage(CompleteMessage message) {
		return this.formatMessage(message, Locale.getDefault());

	}

	/**
	 * Given the dynamic message containing the defained message with dynamic parameters this will
	 * format the message for the locale provided replacing all targets in the message value with
	 * the parameter values.
	 * 
	 * @see KeyedMessageFormat
	 * 
	 * @param dynamicMessage
	 *            the message enumarations and the dynamic parameters
	 * @param locale
	 *            the desired language
	 * @return the completed message with parameters replaced
	 * @throws IllegalArgumentException
	 *             when parameters do not match the message value.
	 * 
	 */
	public String formatMessage(CompleteMessage dynamicMessage, Locale locale) {
		return formatMessage(dynamicMessage, locale, true);
	}

	/**
	 * Exactly as {@link #formatMessage(CompleteMessage, Locale)} explains, but won't format the
	 * parameters. Only provided for validation since validator can't provide type-safe parameter
	 * values.
	 * 
	 * @param dynamicMessage
	 * @param locale
	 * @param formatParameters
	 *            should we format the parameters or not
	 * @return
	 */
	private String formatMessage(CompleteMessage dynamicMessage, @Nonnull Locale locale, boolean formatParameters) {

		String formattedMessage;
		Map<ParamKey, Object> parameters = dynamicMessage.getParameters();

		// only
		String unformattedMessage = getString(dynamicMessage.getMessage(), locale);
		// only try to format if there are parameters.
		if (parameters == null || parameters.isEmpty()) {
			formattedMessage = unformattedMessage;
		} else {
			Map<String, Object> stringParams = Maps.newHashMap();

			if (formatParameters) {
				// Add pre-formatted values with the "_VALUE" automatically added to the key name
				// before we format params.
				// ICU requires the keys to be String, not ParamKey.
				for (Entry<ParamKey, Object> entry : parameters.entrySet()) {
					ParamKey paramKey = entry.getKey();
					// FIXME:We could use just _KEY so remove _VALUE to avoid confusion
					stringParams.put(paramKey.toString() + "_VALUE", entry.getValue());

					// allows the targeted selection of a message customized during declaration
					if (paramKey instanceof SelectParamKey) {
						String selectKey = ((SelectParamKey) paramKey).selectKey(entry.getValue());
						if (selectKey != null) {
							// transition to use KEY since it matches the purpose better
							stringParams.put(paramKey.toString() + "_KEY", selectKey);
						}

					}

				}

				parameters = formatParameters(locale, parameters);
			}

			// ICU requires the keys to be Strings, not ParamKey.
			for (Entry<ParamKey, Object> entry : parameters.entrySet()) {
				stringParams.put(entry.getKey().toString(), entry.getValue());
			}

			StringBuffer sb = new StringBuffer();
			MessageFormat msgFormatter = new MessageFormat(unformattedMessage, ULocale.forLocale(locale));
			msgFormatter.format(stringParams, sb, null);
			formattedMessage = sb.toString();

			// this would validate if there are leftover parameters. Especially useful during
			// development if it could selectively enabled
			// KeyedMessageFormat.create(formattedMessage).putAll(parameters).forgiveExtraParameters().build();
		}

		return formattedMessage;

	}

	/**
	 * Takes the given parameters and formats them either by the {@link CustomFormat} provided in
	 * the {@link ParamKey} itself or by using the default CustomFormats provided by the
	 * {@link CustomFormatFactory}.
	 * 
	 * @param locale
	 * @param parameters
	 * @return
	 */

	private Map<ParamKey, Object> formatParameters(Locale locale, Map<ParamKey, Object> parameters) {
		Set<Map.Entry<ParamKey, Object>> parameterEntries = parameters.entrySet();
		Map<ParamKey, Object> formattedParameters = new HashMap<ParamKey, Object>();
		// iterate each entry and provide the formatted value instead of the
		// object
		for (Map.Entry<ParamKey, Object> entry : parameterEntries) {
			ParamKey paramKey = entry.getKey();
			@SuppressWarnings("rawtypes")
			CustomFormat format = paramKey.getFormat();
			Object entryValue = entry.getValue();
			if (entryValue == null) {
				throw new RuntimeException(EnumUtil.qualifiedName(paramKey)
						+ " provided with a null value. Please build message providing a non null value.");
			}
			if (format == null) {
				format = CustomFormatFactory.getInstance().getFormat(entryValue.getClass());
			}
			@SuppressWarnings("unchecked")
			String formattedValue = format.format(entryValue, locale);
			formattedParameters.put(paramKey, formattedValue);
		}
		return formattedParameters;
	}

	@SuppressWarnings("unchecked")
	public Enum<? extends Message> getResouceBundleMessage(String key) throws MissingResourceException {

		// this doesn't make any sense, but couldn't get it to work with
		// this.keyClass
		Enum<? extends Message> enum1 = keyClass.getEnumConstants()[0];

		try {
			// throws illegal argument if not available as enum
			return Enum.valueOf(enum1.getClass(), key);
		} catch (IllegalArgumentException e) {
			throw new MissingResourceException("No message with key of " + key, this.keyClass.getSimpleName(), key);
		}
	}

	protected void validate() {
		enumsMatchKeysInBundle();
		keysInBundleMatchEnums();
	}

	/**
	 * Iterates all enums making sure there is a matching key.
	 * 
	 * @throws MissingResourceException
	 */
	protected void enumsMatchKeysInBundle() throws MissingResourceException {

		Enum<? extends Message>[] enums = keyClass.getEnumConstants();
		for (Enum<? extends Message> currentMessage : enums) {
			validateMessage(currentMessage);
		}

	}

	/**
	 * Given a message this will inspect it and throw an exception if any of the messages or keys
	 * don't match the message provided.
	 * 
	 * @param enum1
	 */
	protected void validateMessage(Enum<? extends Message> enumMessage) {
		// this cast seems to be necesary for Eclipse.
		Message message = (Message) enumMessage;
		ParamKey[] params = If.nil(message.getParameterKeys()).use(new ParamKey[] {});
		Number[] paramValues = new Number[params.length];

		// populate values with fake values.
		for (int index = 0; index < paramValues.length; index++) {
			paramValues[index] = index;
		}

		CompleteMessage dynamicMessage = new CompleteMessage.Builder().setMessage(message)
				.addParameters(params, paramValues).build();
		// validates parameters match given string...runtime exception otherwise
		boolean formatParameters = false;
		formatMessage(dynamicMessage, Locale.getDefault(), formatParameters);
	}

	/**
	 * Iterates all keys in bundle looking for matching enum.
	 * 
	 * @see #validateMessage(Message)
	 * @see #enumsMatchKeysInBundle()
	 * 
	 * @throws IllegalArgumentException
	 */

	protected void keysInBundleMatchEnums() throws IllegalArgumentException {
		// only validate if a resource bundle is provided. If none then the enum
		// validation will be enough.
		if (this.resourceBundle != null) {
			Enumeration<String> keys = this.resourceBundle.getKeys();

			while (keys.hasMoreElements()) {
				String key = keys.nextElement();

				validateMessage(getResouceBundleMessage(key));

			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MessageResourceBundle) {
			MessageResourceBundle that = (MessageResourceBundle) obj;
			return that.keyClass.equals(this.keyClass);
		}
		return false;
	}

	@Override
	public int hashCode() {

		return this.keyClass.hashCode();
	}

}
