/**
 *
 */
package com.aawhere.lang.string;

import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;

/**
 * Provides adaptation of objects to/from Strings if there is a corresponding adapter for it's
 * parameter type.
 * 
 * This only supports homogeneous collections of the same Class {@link #parameterType}, not even
 * assignable from the same class since there is a single adapter.
 * 
 * This is intended to be used in conjunction with the {@link StringAdapterFactory} and not
 * registered with the factory itself.
 * 
 * @author aroller
 * 
 */
public class CollectionStringAdapter<C extends Collection<T>, T>
		implements StringAdapter<C> {

	/**
	 * Used to construct all instances of CollectionStringAdapter.
	 */
	public static class Builder<C extends Collection<T>, T>
			extends ObjectBuilder<CollectionStringAdapter<C, T>> {

		public Builder() {
			super(new CollectionStringAdapter<C, T>());
		}

		public Builder<C, T> setParameterType(Class<T> parameterType) {
			building.parameterType = parameterType;
			return this;
		}

		public Builder<C, T> setAdapter(StringAdapter<T> adapter) {
			building.adapter = adapter;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull("adapter", building.adapter);
			Assertion.assertNotNull("parameterType", building.parameterType);
			super.validate();
		}
	}// end Builder

	/** Use {@link Builder} to construct CollectionStringAdapter */
	private CollectionStringAdapter() {
	}

	/**
	 * The separator used in the string to separate items from the collection. If the parameter type
	 * provides a string equal to this dilimter the unmarshal will not work properly.
	 * 
	 */
	private static final String DILIMETER = " ";
	private static final String DILIMETER_ALTERNATE = ",";
	private StringAdapter<T> adapter;
	private Class<T> parameterType;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Object value) {
		String marshalled;
		if (!(value instanceof Iterable)) {
			marshalled = StringUtils.join((Iterable<?>) value, DILIMETER);
		} else {
			StringBuilder result = new StringBuilder();

			for (@SuppressWarnings("unchecked")
			Iterator<T> iterator = ((Collection<T>) value).iterator(); iterator.hasNext();) {
				T t = iterator.next();
				result.append(adapter.marshal(t));
				if (iterator.hasNext()) {
					result.append(DILIMETER);
				}
			}
			marshalled = result.toString();
		}
		return marshalled;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#unmarshal(java.lang.String, java.lang.Class)
	 */
	@Override
	public <A extends C> A unmarshal(@Nonnull String string, @Nonnull Class<A> type) throws StringFormatException,
			BaseException {
		A collection = CollectionUtilsExtended.newCollection(type);
		String[] strings = StringUtils.split(string, DILIMETER);
		// well, maybe they used commas?
		if (strings != null && strings.length == 1) {
			strings = StringUtils.split(string, DILIMETER_ALTERNATE);
		}
		if (strings != null) {
			for (int i = 0; i < strings.length; i++) {
				String token = strings[i];
				collection.add(adapter.unmarshal(token, parameterType));
			}
		}
		return collection;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Class<C> getType() {
		return (Class<C>) (Class<?>) Collection.class;
	}

}
