/**
 * 
 */
package com.aawhere.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * Used to convert a {@link Multimap} to a collection of {@link Map.Entry}s since a multimap does
 * not have a default constructor.
 * 
 * TODO:Abstract this for common functions to support other google collections.
 * 
 * @author aroller
 * 
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class HashMultimapXmlAdapter
		extends XmlAdapter<HashMultimapXmlAdapter.XmlMultimap, Multimap> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Multimap unmarshal(XmlMultimap xmlMap) throws Exception {
		if (xmlMap == null) {
			return null;
		}

		HashMultimap<Object, Object> map = HashMultimap.create();
		for (XmlMultimapEntry entry : xmlMap.entries) {
			map.put(entry.key, entry.value);
		}
		return map;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public XmlMultimap marshal(Multimap map) throws Exception {
		if (map == null) {
			return null;
		}
		Collection<Map.Entry> entries = map.entries();
		XmlMultimap result = new XmlMultimap();
		for (Map.Entry entry : entries) {
			result.entries.add(new XmlMultimapEntry(entry));
		}
		return result;
	}

	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class XmlMultimap {
		@XmlElement
		ArrayList<XmlMultimapEntry> entries = new ArrayList<HashMultimapXmlAdapter.XmlMultimapEntry>();
	}

	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class XmlMultimapEntry {

		/**
		 * 
		 */
		public XmlMultimapEntry() {
			// TODO Auto-generated constructor stub
		}

		/**
		 * @param entry
		 */
		public XmlMultimapEntry(java.util.Map.Entry entry) {
			this.key = entry.getKey();
			this.value = entry.getValue();
		}

		@XmlElement
		private Object key;
		@XmlElement
		private Object value;
	}
}
