/**
 * 
 */
package com.aawhere.id;

import java.io.Serializable;

/**
 * Implemented by those entities that are identifiable by a business key in addition to their
 * datastore key.
 * 
 * @author roller
 * 
 */
public interface KeyedEntity<K extends Identifier<?, ?>>
		extends Serializable {

	K getKey();
}
