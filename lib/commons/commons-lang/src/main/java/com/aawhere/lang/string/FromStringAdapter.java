/**
 *
 */
package com.aawhere.lang.string;

import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.reflect.GenericUtils;

/**
 * Adapts any object that implements the default String construction capabilities:
 * 
 * <pre>
 * 1. Accepts a String in single valued constructor.
 * or
 * 2. publishes a static valueOf(String) method that produces the object given that string.
 * and
 * 3. implements an equals method that will verify equality after construction from the string.
 * </pre>
 * 
 * The object must also publish it's string for using the standard {@link Object#toString()} method.
 * 
 * 
 * 
 * @author aroller
 * 
 */
public class FromStringAdapter<T>
		implements StringAdapter<T> {

	/**
	 * Used to construct all instances of FromStringAdapter.
	 */
	public static class Builder<T>
			extends BaseBuilder<T, Builder<T>> {
		/**
		 *
		 */
		public Builder() {
			super(new FromStringAdapter<T>());
		}
	}

	public static class BaseBuilder<T, B extends BaseBuilder<T, B>>
			extends AbstractObjectBuilder<FromStringAdapter<T>, B> {

		public BaseBuilder(FromStringAdapter<T> adapter) {
			super(adapter);
		}

		public B setType(Class<T> type) {
			building.type = type;
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			if (building.type == null) {
				building.type = GenericUtils.getTypeArgument(getClass());
				Assertion
						.assertNotNull(	"type needs to be provided or may be available from an implementation that decalres a type",
										building.type);
			}
			super.validate();
		}
	}// end Builder

	/** Use {@link Builder} to construct FromStringAdapter */
	private FromStringAdapter() {
	}

	private Class<T> type;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Object value) {
		if (value == null) {
			return null;
		} else {
			return String.valueOf(value);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#unmarshal(java.lang.String)
	 */
	@Override
	public <A extends T> A unmarshal(String adapted, Class<A> type) throws BaseException {
		if (adapted == null) {
			return null;
		} else {
			try {
				return FromString.newInstance(type, adapted);
			} catch (NoSuchMethodException e) {
				// if this is unacceptable behavior we can provide an understands method or for
				// pre-inpection. we just assume this is a coding error
				throw new RuntimeException(e);
			}
		}

	}

	@Override
	public Class<T> getType() {
		return type;
	}

}
