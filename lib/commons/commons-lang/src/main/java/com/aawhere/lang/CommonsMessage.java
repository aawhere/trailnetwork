/**
 * 
 */
package com.aawhere.lang;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * A single place to land all messages that really belong in Commons Lang. If you find a situation
 * producing a lot of these make your own.
 * 
 * @author aroller
 * 
 */
public enum CommonsMessage implements Message {

	/**
	 * Anyone that needs to communicate that an actual value exceeded the given maximum
	 */
	MAXIMUM_EXCEEDED("{ACTUAL} exceeded the maximum {MAXIMUM}.", Param.ACTUAL, Param.MAXIMUM);

	private ParamKey[] parameterKeys;
	private String message;

	private CommonsMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/**
		 * An amount of time. This could be number of millis or any other amount of time that
		 */
		ACTUAL,
		/**
		 * General use for any item that may need a maximum as long as the type is handled in the
		 * general Formats.
		 */
		MAXIMUM;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
