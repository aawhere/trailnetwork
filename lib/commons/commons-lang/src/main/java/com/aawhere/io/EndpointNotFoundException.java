/**
 * 
 */
package com.aawhere.io;

import java.io.IOException;

import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * @author aroller
 * 
 */
@StatusCode(value = HttpStatusCode.NOT_FOUND, message = "The endpoint is not currently available: {URL}")
public class EndpointNotFoundException
		extends IoException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8270922436395097425L;

	/**
	 * @param other
	 */
	public EndpointNotFoundException(IOException other) {
		super(other);
	}

	/**
	 * @param message
	 * @param e
	 */
	public EndpointNotFoundException(CompleteMessage message, IOException e) {
		super(message, e);
	}

	/**
	 * @param message
	 * @param e
	 */
	public EndpointNotFoundException(String enpointUrl) {
		super(new CompleteMessage.Builder(EndpointNotFoundExceptionMessage.ENDPOINT_NOT_FOUND)
				.addParameter(EndpointNotFoundExceptionMessage.Param.URL, enpointUrl).build());

	}

}
