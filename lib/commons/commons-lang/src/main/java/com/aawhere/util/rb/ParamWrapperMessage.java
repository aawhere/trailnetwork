package com.aawhere.util.rb;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.text.format.custom.CustomFormat;

public enum ParamWrapperMessage implements Message {

	/** do not localize. Using Message for param substitution. */
	WRAPPER_OPEN_SPAN_TAG("<span class=\"{WRAPPER_KEY}\">", Param.WRAPPER_KEY),
	/** do not localize. Using Message for param substitution. */
	WRAPPER_CLOSE_SPAN_TAG("</span>");
	private ParamKey[] parameterKeys;
	private String message;

	private ParamWrapperMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		/**
		 * Closing wrapper
		 */
		XX,
		/** Opening wrapper */
		_XX,
		/** key to identify a wrapper */
		WRAPPER_KEY;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
