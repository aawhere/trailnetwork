/**
 * 
 */
package com.aawhere.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.lang.If;
import com.aawhere.xml.XmlNamespace;

/**
 * Converts a Set into a List since Jaxb apparently can't process Sets, but can process Lists.
 * 
 * @author aroller
 * 
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class SetXmlAdapter
		extends XmlAdapter<SetXmlAdapter.XmlSet, Set> {

	/**
	 * 
	 */
	public SetXmlAdapter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public XmlSet marshal(Set set) throws Exception {

		return (set == null) ? null : new XmlSet(set);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Set unmarshal(XmlSet xml) throws Exception {

		return (xml == null) ? null : new HashSet(xml.item);
	}

	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static class XmlSet {

		XmlSet() {
			// TODO Auto-generated constructor stub
		}

		/**
		 * 
		 */
		XmlSet(Set set) {
			this.item = new ArrayList(set);
		}

		@XmlElement
		ArrayList item;
	}
}
