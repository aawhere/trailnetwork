/**
 * 
 */
package com.aawhere.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;

/**
 * Given an inputstream that contains a String representation this will produce a String for the
 * entire contents.
 * 
 * @author roller
 * 
 */
public class InputStreamReader {
	private String string;

	public InputStreamReader(InputStream inputStream) throws IOException {

		java.io.InputStreamReader inputStreamReader = new java.io.InputStreamReader(inputStream);
		LineNumberReader reader = new LineNumberReader(inputStreamReader);
		StringBuffer text = new StringBuffer();

		String line;
		while ((line = reader.readLine()) != null) {
			text.append(line);
		}

		this.string = text.toString();
	}

	/**
	 * @return the string
	 */
	public String getString() {
		return string;
	}
}
