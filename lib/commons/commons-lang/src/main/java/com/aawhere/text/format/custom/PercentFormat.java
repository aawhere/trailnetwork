/**
 * 
 */
package com.aawhere.text.format.custom;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Provides the localized representation in percent when given a number.
 * 
 * Do not register this with the factory because it handles Double.class and will replace general
 * number formatting. This could be registered if a specific PercentNumber is provided.
 * 
 * @author roller
 * 
 */
public class PercentFormat
		extends DoubleFormat {

	private static PercentFormat instance;

	/**
	 * Used to retrieve the only instance of PercentFormat.
	 */
	public static PercentFormat getInstance() {

		if (instance == null) {
			instance = new PercentFormat();
		}
		return instance;

	}// end Builder

	/** Use {@link #getInstance()} to get PercentFormat */
	private PercentFormat() {
	}

	@Override
	protected NumberFormat getNumberFormat(Locale locale) {
		return NumberFormat.getPercentInstance(locale);
	}

}
