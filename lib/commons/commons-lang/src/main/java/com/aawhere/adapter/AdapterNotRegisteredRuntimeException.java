/**
 * 
 */
package com.aawhere.adapter;

import java.lang.reflect.Type;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Useful for reporting when an adapter isn't registered for the type given.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class AdapterNotRegisteredRuntimeException
		extends BaseRuntimeException {

	private static final long serialVersionUID = -2571767974911951752L;

	/**
	 * @param keyOrMessage
	 */
	public AdapterNotRegisteredRuntimeException(Type type) {
		super(new CompleteMessage.Builder(AdapterMessage.NOT_REGISTERED).addParameter(AdapterMessage.Param.TYPE, type)
				.build());
	}

}
