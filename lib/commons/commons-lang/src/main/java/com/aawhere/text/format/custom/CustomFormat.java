/**
 * 
 */
package com.aawhere.text.format.custom;

import java.util.Locale;

/**
 * Formats objects in the system based on the locale. indicates the type of formats each instance
 * handles.
 * 
 * @author roller
 * 
 */
public interface CustomFormat<T> {

	/**
	 * The method called to transform an object into the specific format.
	 * 
	 * @param object
	 * @return
	 */
	public String format(T object, Locale locale);

	Class<? super T> handlesType();
}
