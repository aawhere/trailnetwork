/**
 * 
 */
package com.aawhere.collections;

import java.util.Collection;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.string.ToStringXmlAdapter;

/**
 * Simply provides the string version of a collection or null if it is null or empty.
 * 
 * @see ToStringXmlAdapter
 * @author aroller
 * 
 */
public class CollectionToStringXmlAdapter
		extends XmlAdapter<String, Collection<?>> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Collection<?> unmarshal(String v) throws Exception {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Collection<?> v) throws Exception {
		return (CollectionUtils.isEmpty(v)) ? StringUtils.EMPTY : StringUtils.join(v, ',');
	}

}
