/**
 *
 */
package com.aawhere.id;

import java.util.Locale;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.text.format.custom.CustomFormatFactory;

/**
 * @author Aaron Roller
 * 
 */
public class IdentifierCustomFormat
		implements CustomFormat<Identifier<?, ?>> {

	static {
		register();
	}
	private static boolean registered = false;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#format(java.lang.Object, java.util.Locale)
	 */
	@Override
	public String format(Identifier<?, ?> object, Locale locale) {

		return object.getValue().toString();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#handlesType()
	 */
	@Override
	public Class<? super Identifier<?, ?>> handlesType() {
		return Identifier.class;
	}

	/**
	 *
	 */
	public static void register() {
		if (!registered) {
			CustomFormatFactory.getInstance().register(new IdentifierCustomFormat());
			registered = true;
		}

	}

}
