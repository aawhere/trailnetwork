/**
 * 
 */
package com.aawhere.id;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import com.aawhere.lang.ObjectBuilder;

/**
 * When a collection of {@link Identifiable}s is desired that can be accessed by their
 * {@link Identifier}.
 * 
 * @author aroller
 * 
 */
public class IdentifierToIdentifiableMapFunction<F extends Identifier<?, T>, T extends Identifiable<F>>
		implements IdentifierToIdentifiableFunction<F, T> {
	private HashMap<F, T> map = new HashMap<>();

	/**
	 * Used to construct all instances of IdentifierToIdentifiableMapFunction.
	 */
	public static class Builder<F extends Identifier<?, T>, T extends Identifiable<F>>
			extends ObjectBuilder<IdentifierToIdentifiableMapFunction<F, T>> {

		public Builder() {
			super(new IdentifierToIdentifiableMapFunction<F, T>());
		}

		public Builder<F, T> add(T... identifiables) {
			return add(Arrays.asList(identifiables));
		}

		public Builder<F, T> add(Iterable<T> identifiables) {
			for (T t : identifiables) {
				building.map.put(t.getId(), t);
			}
			return this;
		}

	}// end Builder

	public static <F extends Identifier<?, T>, T extends Identifiable<F>> Builder<F, T> create() {
		return new Builder<F, T>();
	}

	/** Use {@link Builder} to construct IdentifierToIdentifiableMapFunction */
	private IdentifierToIdentifiableMapFunction() {
	}

	public Map<F, T> all() {
		return this.map;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public T apply(@Nullable F id) {
		return map.get(id);
	}

}
