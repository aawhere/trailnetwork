/**
 * 
 */
package com.aawhere.id;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.collections.Index;
import com.aawhere.lang.reflect.GenericUtils;

/**
 * Common identifier to be used when combining two {@link Identifier}s together using standard
 * notation used in {@link IdentifierUtils#createCompositeIdValue(Identifier, Identifier)}
 * 
 * @author aroller
 * 
 */
abstract public class CompositeIdentifier<KindT, P extends Identifier<?, ?>, S extends Identifier<?, ?>>
		extends StringIdentifier<KindT> {

	protected CompositeIdentifier(Class<KindT> kind) {
		super(kind);
	}

	/**
	 * 
	 */
	public CompositeIdentifier(String value, Class<KindT> kind) {
		super(value, kind);
	}

	/**
	 * 
	 */
	protected CompositeIdentifier(P prefix, S suffix, Class<KindT> kind) {
		this(IdentifierUtils.createCompositeIdValue(prefix, suffix), kind);
	}

	public P prefix() {
		Class<P> prefixClass = GenericUtils.getTypeArgument(this, Index.SECOND);
		Pair<String, String> split = IdentifierUtils.splitCompositeId(this);
		try {
			return prefixClass.getConstructor(String.class).newInstance(split.getLeft());
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	public S suffix() {
		Class<S> suffixClass = GenericUtils.getTypeArgument(this, Index.THIRD);
		Pair<String, String> split = IdentifierUtils.splitCompositeId(this);
		try {
			return suffixClass.getConstructor(String.class).newInstance(split.getRight());
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	private static final long serialVersionUID = 2807940416994004500L;

}
