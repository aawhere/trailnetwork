/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 1, 2010
commons-lang : com.aawhere.lang.string.predicate.IsCapitalLetterPredicate.java
 */
package com.aawhere.lang.string.predicate;

import org.apache.commons.lang3.StringUtils;

/**
 * Allows capital letters.
 * 
 * @author roller
 * 
 */
public class CapitalLetterPredicate
		extends CharSequencePredicate {

	private static CapitalLetterPredicate instance;

	private CapitalLetterPredicate() {
	}

	public static CapitalLetterPredicate getInstance() {
		if (null == instance) {
			instance = new CapitalLetterPredicate();
		}
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.predicate.CharSequencePredicate#evaluate(java.lang.CharSequence)
	 */
	@Override
	public boolean evaluate(CharSequence target) {

		return StringUtils.isAllUpperCase(target);
	}

}
