/**
 * 
 */
package com.aawhere.util.rb;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.ExceptionLocalizer;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

/**
 * Given {@link StatusMessages} this will send them as payload to explain the errors that stopped
 * operations.
 * 
 * TODO: Figure out of this fits into {@link BaseRuntimeException} and especially
 * {@link ExceptionLocalizer} for i18n support.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class StatusMessagesRuntimeException
		extends RuntimeException {

	private static final long serialVersionUID = 3815171478047390482L;
	private StatusMessages messages;

	/**
	 * 
	 */
	public StatusMessagesRuntimeException(StatusMessages messages) {
		this.messages = messages;
	}

	public StatusMessagesRuntimeException(StatusMessages messages, HttpStatusCode statusCode) {
		this(messages);

	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return this.messages.toString();
	}

}
