/**
 * 
 */
package com.aawhere.id;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * A general purpose exception used to indicate an invalid key is provided. This is a runtime since
 * internal use should check the format without throwing exceptions.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class InvalidIdentifierException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 7907933532850106045L;

	/**
	 * 
	 */
	public InvalidIdentifierException(CompleteMessage message) {
		super(message);
	}

	/**
	 * Used when the format of a key does not support splitting or otherwise not appropriate for
	 * separating multiple keys.
	 * 
	 * @param id
	 * @param dilimeter
	 * @return
	 */
	public static InvalidIdentifierException compositeFormat(Object id, String dilimeter) {
		return new InvalidIdentifierException(CompleteMessage.create(IdentifierMessage.INVALID_COMPOSITE_FORMAT)
				.addParameter(IdentifierMessage.Param.IDENTIFIER, id)
				.addParameter(IdentifierMessage.Param.DILIMETER, dilimeter).build());
	}

}
