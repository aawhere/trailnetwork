/**
 * 
 */
package com.aawhere.text.format.custom;

import java.util.Locale;

/**
 * Reports the {@link Class#getSimpleName()} instead of the default {@link Class#toString()} result
 * of {@link Class#getName()}.
 * 
 * @author aroller
 * 
 */
public class ClassFormat
		implements CustomFormat<Class<?>> {

	/**
	 * 
	 */
	public ClassFormat() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#format(java.lang.Object, java.util.Locale)
	 */
	@Override
	public String format(Class<?> object, Locale locale) {
		return object.getSimpleName();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#handlesType()
	 */
	@Override
	public Class<? super Class<?>> handlesType() {
		return Class.class;
	}

}
