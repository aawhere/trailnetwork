/**
 * 
 */
package com.aawhere.xml.bind;

import java.io.File;
import java.io.OutputStream;
import java.io.Writer;

import javax.annotation.Nonnull;
import javax.ws.rs.ext.ContextResolver;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.attachment.AttachmentMarshaller;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;
import javax.xml.validation.Schema;

import org.w3c.dom.Node;
import org.xml.sax.ContentHandler;

/**
 * A specialty implementation of {@link Marshaller} that throws the given exception during
 * marshalling. This is done to avoid bad reporting of good messages as runtime exceptions when a
 * framework like Jersey is trying to get the context (which doesn't allow exception reporting). See
 * TN-841.
 * 
 * Every method that can throw a JAXBException throws the JAXBException wrapping the given
 * {@link #exception}. Others delegate to the given marshaller.
 * 
 * @see ContextResolver#getContext(Class)
 * 
 * @author aroller
 * 
 */
public class ExceptionThrowingMarshaller
		implements Marshaller {
	@Nonnull
	final private Marshaller marshaller;
	@Nonnull
	final private Exception exception;

	/**
	 * @param marshaller
	 * @param e
	 */
	public ExceptionThrowingMarshaller(Marshaller marshaller, Exception e) {
		this.marshaller = marshaller;
		this.exception = e;
	}

	private void throwJAXBException() throws JAXBException {
		throw new JAXBException(exception);
	}

	/*
	 * @see javax.xml.bind.Marshaller#marshal(java.lang.Object, javax.xml.transform.Result)
	 */
	@Override
	public void marshal(Object jaxbElement, Result result) throws JAXBException {
		throwJAXBException();
	}

	/*
	 * @see javax.xml.bind.Marshaller#marshal(java.lang.Object, java.io.OutputStream)
	 */
	@Override
	public void marshal(Object jaxbElement, OutputStream os) throws JAXBException {
		throwJAXBException();
	}

	/*
	 * @see javax.xml.bind.Marshaller#marshal(java.lang.Object, java.io.File)
	 */
	@Override
	public void marshal(Object jaxbElement, File output) throws JAXBException {
		throwJAXBException();
	}

	/*
	 * @see javax.xml.bind.Marshaller#marshal(java.lang.Object, java.io.Writer)
	 */
	@Override
	public void marshal(Object jaxbElement, Writer writer) throws JAXBException {
		throwJAXBException();
	}

	/*
	 * @see javax.xml.bind.Marshaller#marshal(java.lang.Object, org.xml.sax.ContentHandler)
	 */
	@Override
	public void marshal(Object jaxbElement, ContentHandler handler) throws JAXBException {
		throwJAXBException();
	}

	/*
	 * @see javax.xml.bind.Marshaller#marshal(java.lang.Object, org.w3c.dom.Node)
	 */
	@Override
	public void marshal(Object jaxbElement, Node node) throws JAXBException {
		throwJAXBException();
	}

	/*
	 * @see javax.xml.bind.Marshaller#marshal(java.lang.Object, javax.xml.stream.XMLStreamWriter)
	 */
	@Override
	public void marshal(Object jaxbElement, XMLStreamWriter writer) throws JAXBException {
		throwJAXBException();
	}

	/*
	 * @see javax.xml.bind.Marshaller#marshal(java.lang.Object, javax.xml.stream.XMLEventWriter)
	 */
	@Override
	public void marshal(Object jaxbElement, XMLEventWriter writer) throws JAXBException {
		throwJAXBException();
	}

	/*
	 * @see javax.xml.bind.Marshaller#getNode(java.lang.Object)
	 */
	@Override
	public Node getNode(Object contentTree) throws JAXBException {
		throwJAXBException();
		return null;
	}

	/*
	 * @see javax.xml.bind.Marshaller#setProperty(java.lang.String, java.lang.Object)
	 */
	@Override
	public void setProperty(String name, Object value) throws PropertyException {
		marshaller.setProperty(name, value);
	}

	/*
	 * @see javax.xml.bind.Marshaller#getProperty(java.lang.String)
	 */
	@Override
	public Object getProperty(String name) throws PropertyException {
		return marshaller.getProperty(name);
	}

	/*
	 * @see javax.xml.bind.Marshaller#setEventHandler(javax.xml.bind.ValidationEventHandler)
	 */
	@Override
	public void setEventHandler(ValidationEventHandler handler) throws JAXBException {
		throwJAXBException();
	}

	/*
	 * @see javax.xml.bind.Marshaller#getEventHandler()
	 */
	@Override
	public ValidationEventHandler getEventHandler() throws JAXBException {
		throwJAXBException();
		return null;
	}

	/*
	 * @see javax.xml.bind.Marshaller#setAdapter(javax.xml.bind.annotation.adapters.XmlAdapter)
	 */
	@Override
	public void setAdapter(XmlAdapter adapter) {
		marshaller.setAdapter(adapter);
	}

	/*
	 * @see javax.xml.bind.Marshaller#setAdapter(java.lang.Class,
	 * javax.xml.bind.annotation.adapters.XmlAdapter)
	 */
	@Override
	public <A extends XmlAdapter> void setAdapter(Class<A> type, A adapter) {
		marshaller.setAdapter(adapter);
	}

	/*
	 * @see javax.xml.bind.Marshaller#getAdapter(java.lang.Class)
	 */
	@Override
	public <A extends XmlAdapter> A getAdapter(Class<A> type) {
		return marshaller.getAdapter(type);
	}

	/*
	 * @see
	 * javax.xml.bind.Marshaller#setAttachmentMarshaller(javax.xml.bind.attachment.AttachmentMarshaller
	 * )
	 */
	@Override
	public void setAttachmentMarshaller(AttachmentMarshaller am) {
		marshaller.setAttachmentMarshaller(am);
	}

	/*
	 * @see javax.xml.bind.Marshaller#getAttachmentMarshaller()
	 */
	@Override
	public AttachmentMarshaller getAttachmentMarshaller() {
		return marshaller.getAttachmentMarshaller();
	}

	/*
	 * @see javax.xml.bind.Marshaller#setSchema(javax.xml.validation.Schema)
	 */
	@Override
	public void setSchema(Schema schema) {
		marshaller.setSchema(schema);
	}

	/*
	 * @see javax.xml.bind.Marshaller#getSchema()
	 */
	@Override
	public Schema getSchema() {
		return marshaller.getSchema();
	}

	/*
	 * @see javax.xml.bind.Marshaller#setListener(javax.xml.bind.Marshaller.Listener)
	 */
	@Override
	public void setListener(Listener listener) {
		marshaller.setListener(listener);
	}

	/*
	 * @see javax.xml.bind.Marshaller#getListener()
	 */
	@Override
	public Listener getListener() {
		return marshaller.getListener();
	}

}
