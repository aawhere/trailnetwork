/**
 * 
 */
package com.aawhere.lang;

import com.aawhere.id.Identifier;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates to the caller that there is nothing to update since the resources is not stale.
 * 
 * @author aroller
 * 
 *         Override default status code with {@link HttpStatusCode#ACCEPTED} if this is talking to
 *         the queue using {@link #forTheQueue(Identifier)}.
 */
@StatusCode(HttpStatusCode.PRECON_FAILED)
public class UpToDateException
		extends BaseException {

	private static final long serialVersionUID = 8174909948646100376L;

	public UpToDateException(Identifier<?, ?> id) {
		this(id, null);
	}

	public UpToDateException(Identifier<?, ?> id, HttpStatusCode statusCode) {
		super(CompleteMessage.create(LangMessage.UP_TO_DATE).addParameter(LangMessage.Param.ID, id).build());
		setStatusCode(statusCode);
	}

	/**
	 * When this is talking to the queue it must break the standards and go with a 202 to tell it to
	 * stop trying.
	 * 
	 * @param id
	 * @return
	 */
	public static UpToDateException forTheQueue(Identifier<?, ?> id) {
		return new UpToDateException(id, HttpStatusCode.ACCEPTED);
	}
}
