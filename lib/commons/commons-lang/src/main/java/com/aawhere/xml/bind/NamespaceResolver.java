/**
 * 
 */
package com.aawhere.xml.bind;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.springframework.core.annotation.AnnotationUtils;

import com.aawhere.lang.NotImplementedException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

/**
 * Searches the given class for the namespace annotation in {@link XmlType} or
 * {@link XmlRootElement}. If not found then it searches the package for a global {@link XmlNs}
 * declaration as part of {@link XmlSchema}.
 * 
 * @author Aaron Roller
 * 
 */
public class NamespaceResolver {

	/**
	 * Used to construct all instances of NamespaceResolver.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<NamespaceResolver> {

		private Class<?> targetClass;

		public Builder() {
			super(new NamespaceResolver());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public NamespaceResolver build() {
			XmlType xmlType = AnnotationUtils.findAnnotation(targetClass, XmlType.class);
			if (xmlType != null) {
				building.namespace = xmlType.namespace();

			}
			if (!building.isNamespaceAvailable()) {
				XmlRootElement root = AnnotationUtils.findAnnotation(targetClass, XmlRootElement.class);
				if (root != null) {
					building.namespace = root.namespace();
				}
				if (!building.isNamespaceAvailable()) {
					XmlSchema schema = targetClass.getPackage().getAnnotation(XmlSchema.class);
					if (schema != null) {
						building.namespace = schema.namespace();
						if (!building.isNamespaceAvailable()) {
							XmlNs[] xmlns = schema.xmlns();
							if (xmlns != null && xmlns.length > 0) {
								throw new NotImplementedException(
										"the namespace resolver does not handle the complex XmlNs annotation yet.  It shouldn't be difficult");
							}
						}
					}

				}
			}

			return super.build();
		}

		public Builder setTargetClass(Class<?> targetClass) {
			this.targetClass = targetClass;
			return this;
		}
	}// end Builder

	private String namespace;

	/** Use {@link Builder} to construct NamespaceResolver */
	private NamespaceResolver() {
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return this.namespace;
	}

	/**
	 * @return
	 */
	public Boolean isNamespaceAvailable() {

		return this.namespace != null && !this.namespace.equals(XmlNamespace.XML_TYPE_DEFAULT_VALUE);
	}

}
