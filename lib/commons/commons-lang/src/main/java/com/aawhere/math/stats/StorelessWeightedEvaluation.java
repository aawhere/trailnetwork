/**
 * 
 */
package com.aawhere.math.stats;

import org.apache.commons.math.stat.descriptive.StorelessUnivariateStatistic;
import org.apache.commons.math.stat.descriptive.UnivariateStatistic;
import org.apache.commons.math.stat.descriptive.WeightedEvaluation;

/**
 * Extends {@link WeightedEvaluation} with the increment methods synonymous to the
 * {@link UnivariateStatistic} and {@link StorelessUnivariateStatistic} relationship.
 * 
 * @author Aaron Roller
 * 
 */
public interface StorelessWeightedEvaluation
		extends WeightedEvaluation {

	public void increment(double value, double weight);

	public void incrementAll(double value[], double[] weights);

	/** Returns the current value of the statistic. */
	public double getResult();
}
