/**
 * 
 */
package com.aawhere.lang.exception;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import com.aawhere.lang.exception.ExceptionLocalizerFactory.VariableCriteria;

/**
 * Since we need both runtime and checked exceptions and we don't have dual inheritance (since they
 * must extend Exception and RuntimeException accordingly) to be localized, this helper class
 * provides the common functionality between both.
 * 
 * Localizable exceptions depend on the {@link MessageFormat} to do the work of localizing based on
 * the keys and indexed parameters provided.
 * 
 * @author roller
 * 
 */
public class ResourceBundleExceptionLocalizer
		implements ExceptionLocalizer {

	private static final long serialVersionUID = 5167774523914101554L;

	private Exception cause;

	private String resourceBundleBaseName;

	private ExceptionLocalizerFactory.VariableCriteria criteria;

	/**
	 * TODO:Provide ability for this to be assigned or injected. Injection is ideal because it is
	 * unlikely that locale will be known when an exception is thrown. That is up to the view, where
	 * this is part of the model.
	 */
	private Locale locale = Locale.getDefault();

	/**
	 * 
	 */
	public ResourceBundleExceptionLocalizer(String resourceBundleBaseName,
			ExceptionLocalizerFactory.VariableCriteria criteria, Exception cause) {
		this.criteria = criteria;
		this.resourceBundleBaseName = resourceBundleBaseName;
		this.cause = cause;
	}

	@Override
	public String getMessage() {

		ResourceBundle bundle = ResourceBundle.getBundle(resourceBundleBaseName, getLocale());
		String unformattedMessage = bundle.getString(this.criteria.getKey());
		Object[] variables = this.criteria.getVariables();
		String formattedMessage;
		if (variables != null && variables.length > 0) {
			formattedMessage = MessageFormat.format(unformattedMessage, variables);

		} else {
			// nothing to format 'cause no variables
			formattedMessage = unformattedMessage;
		}
		return formattedMessage;
	}

	private Locale getLocale() {

		return this.locale;
	}

	@Override
	public Exception getCause() {

		return this.cause;
	}

	protected static class Producer
			implements ExceptionLocalizerProducer {

		private String resourceBundleBaseName;

		public Producer(String resourceBundleBaseName) {
			this.resourceBundleBaseName = resourceBundleBaseName;
		}

		/**
		 * Default constructor used for registering the producer for default values.
		 * 
		 */
		Producer() {

		}

		@Override
		public ExceptionLocalizer understands(Object criteriaObj, Exception cause) {

			String messageKey;
			ResourceBundleExceptionLocalizer localizer;
			if (this.resourceBundleBaseName == null) {
				// this must be the default producer...don't understand anything
				// and just go with the default if it comes back around
				localizer = null;
			} else {
				VariableCriteria criteria;
				if (criteriaObj instanceof String) {
					messageKey = (String) criteriaObj;
					criteria = new VariableCriteria(messageKey);

				} else if (criteriaObj instanceof VariableCriteria) {
					criteria = (VariableCriteria) criteriaObj;
					messageKey = criteria.getKey();

				} else {
					// we just don't know what it is
					criteria = null;
					messageKey = null;
				}

				if (messageKey != null && getResourceBundle().containsKey(messageKey)) {
					localizer = new ResourceBundleExceptionLocalizer(this.resourceBundleBaseName, criteria, cause);
				} else {
					localizer = null;
				}
			}
			return localizer;
		}

		private ResourceBundle getResourceBundle() {

			return ResourceBundle.getBundle(resourceBundleBaseName);
		}

		@Override
		public ExceptionLocalizer getDefault(Object criteria, Exception cause) {
			if (criteria instanceof VariableCriteria) {
				// resource bundle not found so just display the key until they
				// register
				VariableCriteria variableCriteria = (VariableCriteria) criteria;
				return new DefaultExceptionLocalizer(variableCriteria.getKey(), cause);

			} else {
				return null;
			}
		}

	}

}
