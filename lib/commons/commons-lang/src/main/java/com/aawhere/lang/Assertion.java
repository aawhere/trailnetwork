/**
 * 
 */
package com.aawhere.lang;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Range;

/**
 * A simple class that Asserts a predicate is true or throws an exception otherwise.
 * 
 * Exceptions thrown are runtime and are intended to communicate with the developer that a situation
 * should not continue because a coding error took place.
 * 
 * 
 * Similar to test frameworks, this should be used in the main code rather than limited to testing
 * code like JUnit.
 * 
 * http://en.wikipedia.org/wiki/Assertion_(computing)
 * 
 * 
 * TODO: Convert all the static methods to instance methods similar to {@link #assertNull(Object)}
 * 
 * TODO:investigate using
 * http://static.springsource.org/spring/docs/1.2.x/api/org/springframework/util/Assert.html
 * 
 * @author Aaron Roller
 * 
 */
public class Assertion {

	private static final Boolean THROW_EXCEPTIONS = true;
	private static final Boolean RETURN_BOOLEANS = false;

	/** this one doesn't throw exceptions */
	private static Assertion exceptionInstance = new Assertion(THROW_EXCEPTIONS);
	/** This one throws exceptions */
	private static Assertion booleanInstance = new Assertion(RETURN_BOOLEANS);

	/**
	 * Used to retrieve the Assertion instance that will throw exceptions rather than return
	 * booleans.
	 * 
	 * Synonymous to getExceptionInstance(), but reduced for readability even though it goes against
	 * standard accessor naming.
	 */
	public static Assertion exceptions() {

		return exceptionInstance;

	}// end Builder

	/** Constructs and delivers the only instance that will return booleans rather than exceptions. */
	public static Assertion booleans() {
		return booleanInstance;
	}

	public static Assertion e() {
		return exceptionInstance;
	}

	public static Assertion b() {
		return booleanInstance;
	}

	private Boolean throwExceptions;

	/** Use {@link #exceptions()} to get Assertion */
	private Assertion(Boolean throwExceptions) {
		this.throwExceptions = throwExceptions;
	}

	private static String formatMessage(String propertyName, String message) {
		if (propertyName == null) {
			propertyName = "the object";
		}
		return propertyName + " " + message;
	}

	public void assertTrue(String propertyName, Boolean itIs) {
		determine(itIs, propertyName);
	}

	/**
	 * ensures the object is assignable from type. nulls will be ignored. Use
	 * {@link #notNull(String, Object)} in combination with this if nulls are not allowed.
	 * 
	 * @param propertyName
	 * @param object
	 * @param type
	 */
	public void assertInstanceOf(String propertyName, @Nullable Object object, @Nonnull Class<?> type) {
		if (object != null) {
			Class<?> objectClass = object.getClass();
			assertTrue(propertyName + " must be instance of " + type + ", but was " + objectClass, object != null
					&& type.isAssignableFrom(objectClass));
		}
	}

	public void assertTrue(Boolean itIs) {
		assertTrue(null, itIs);
	}

	/**
	 * @param preRegistered
	 */
	public void assertEmpty(Collection<?> collection) {

		assertEmpty(null, collection);
	}

	public void assertEmpty(String propertyName, Collection<?> collection) {

		boolean empty = collection.isEmpty();
		String message = "";
		if (!empty) {
			message = "must be empty, but had " + collection.toString();
		}
		determine(empty, formatMessage(propertyName, message));
	}

	public Boolean assertNotEmpty(Map<?, ?> map) {
		return determine(!map.isEmpty(), "map must not be empty");
	}

	public Boolean assertNotEmpty(String string) {
		return assertNotEmpty(null, string);
	}

	public Boolean assertNotEmpty(String property, String string) {
		return determine(!StringUtils.isEmpty(string), property);
	}

	public static void assertNotEmpty(Collection<?> collection) {
		assertNotEmpty(null, collection);
	}

	public static void assertNotEmpty(String propertyName, Collection<?> collection) {
		if (collection.isEmpty()) {
			throw new UnsupportedOperationException(formatMessage(propertyName, "must not be empty"));
		}
	}

	public static <T> T assertNotNull(T anythingButNull) {
		return assertNotNull(null, anythingButNull);
	}

	public static <T> T assertNotNull(String propertyName, T anythingButNull) {
		if (anythingButNull == null) {
			throw new NullPointerException(formatMessage(propertyName, "cannot be null"));
		} else {
			return anythingButNull;
		}
	}

	/** Performs a simple null check. */
	public Boolean notNull(String propertyName, Object anythingButNull) {
		return determine(anythingButNull != null, formatMessage(propertyName, "cannot be null"));
	}

	public Boolean assertMinimumNotNull(Integer minimum, Object... objects) {
		return assertMinimumNotNull("some properties", minimum, objects);
	}

	/**
	 * Asserts that the of the objects provided at least the minimum number are not null.
	 * 
	 * @param minimum
	 * @param objects
	 * @return
	 */
	public Boolean assertMinimumNotNull(String properties, Integer minimum, Object... objects) {
		Integer notNullCount = 0;
		for (int i = 0; i < objects.length; i++) {
			Object object = objects[i];
			if (object != null) {
				notNullCount++;
			}
		}
		return determine(notNullCount >= minimum, minimum.toString() + " required to be not null for " + properties
				+ ": " + Arrays.toString(objects));
	}

	/**
	 * @param currentFieldDefinitionBuilder
	 */
	public Boolean assertNull(Object onlyNull) {
		return assertNull(null, onlyNull);
	}

	public Boolean assertNull(String property, Object onlyNull) {
		return determine(onlyNull == null, If.nil(property).use("the property") + " must be null!");
	}

	/**
	 * @param failed
	 * @param string
	 */
	private Boolean determine(Boolean succeeded, String message) {
		if (!succeeded && this.throwExceptions) {
			throw new IllegalArgumentException(message);
		}
		// whatever has happened, if we made it to here just return what the method thinks happened.
		return succeeded;
	}

	public Boolean eq(Object left, Object right) {
		Boolean result;

		if (left != null && right != null) {
			result = left.equals(right);
		} else {
			// double null check...== operator works.
			result = left == right;
		}

		return determine(result, left + " doesn't equal " + right);
	}

	/**
	 * Calls the equals check for you, but checks for null first to avoid
	 * {@link NullPointerException}.
	 * 
	 * Two nulls returns equal
	 * 
	 * @param left
	 * @param right
	 */
	public static void equals(Object left, Object right) {
		exceptions().eq(left, right);
	}

	/**
	 * @param message
	 * @param omission
	 * @param remove
	 */
	public Boolean same(String message, Object first, Object second) {
		return determine(first == second, message);
	}

	/**
	 * Ensures the given value is withing the inclusive range of min-max.
	 * 
	 * @see Range#closed(Comparable, Comparable)
	 * 
	 * @param value
	 * @param min
	 * @param max
	 */
	public <C extends Comparable<?>> Boolean assertRange(String message, C value, C min, C max) {
		return assertRange(message, value, Range.closed(min, max));
	}

	/**
	 * Given a range this will assert the given value is within the bounds of the range using the
	 * {@link Range#apply(Comparable)} to enforce the desired rules defined of the {@link Range}.
	 * 
	 * @param message
	 * @param value
	 * @param range
	 * @return
	 */
	public <C extends Comparable<?>> Boolean assertRange(String message, C value, Range<C> range) {
		return determine(range.contains(value), message + " " + String.valueOf(value) + " not within range " + range);
	}

	/**
	 * Scans all the fields for the given object looking for null values. This has slower
	 * performance than explicitly declaring desired fields.
	 * 
	 * Previously in ObjectBuilder, this did not perform well for all objects in the system.
	 * 
	 * @param object
	 */
	public static void assertNonnullFields(Object object) {
		// validate nonnull fields
		Field[] fields = object.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			if (field.getAnnotation(Nonnull.class) != null) {
				try {
					field.setAccessible(true);
					Assertion.assertNotNull(field.getName(), field.get(object));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
}
