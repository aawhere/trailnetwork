/**
 * 
 */
package com.aawhere.collections;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class MultisetsExtended {

	/**
	 * Provides the count for an element of the {@link Multiset}.
	 * 
	 * @see #countFiltered(Multiset, Predicate)
	 * 
	 * @param set
	 * @return
	 */
	public static <E> Function<E, Integer> countFunction(final Multiset<E> set) {
		return new Function<E, Integer>() {
			@Override
			public Integer apply(E input) {
				return (input != null) ? set.count(input) : null;
			}
		};

	}

	/**
	 * Filters the {@link Multiset} returning only those matching the given count {@link Predicate}
	 * (likely a {@link Range}).
	 * 
	 * @param multiset
	 * @param countPredicate
	 * @return
	 */
	public static <E> Multiset<E> countFiltered(Multiset<E> multiset, Predicate<Integer> countPredicate) {
		Predicate<E> predicate = Predicates.compose(countPredicate, countFunction(multiset));
		return Multisets.filter(multiset, predicate);
	}

}
