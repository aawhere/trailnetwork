/**
 * 
 */
package com.aawhere.lang.number;

import java.util.Iterator;

import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.google.common.base.Function;
import com.google.common.primitives.Ints;

/**
 * Extensions to {@link Number}, {@link NumberUtils} and other useful modifiers for numbers.
 * 
 * @author aroller
 * 
 */
public class NumberUtilsExtended {

	public static String DEFAULT_DILIMETER = ",";

	/**
	 * @see #split(String,String) with {@link #DEFAULT_DILIMETER}
	 * 
	 * @see #join(Number[])
	 * @param separatoredNumbers
	 * @return
	 */
	public static Double[] split(String separatoredNumbers) {
		return split(separatoredNumbers, DEFAULT_DILIMETER);
	}

	/**
	 * Splits the given string that is numbers that is separated by the given separator that can be
	 * converted to the resulting array of doubles.
	 * 
	 * @see #join(Number[], String)
	 * 
	 * @param separatoredNumbers
	 * @param separator
	 * @return
	 */
	public static Double[] split(String separatoredNumbers, String separator) {

		String[] split = StringUtils.split(separatoredNumbers, separator);

		return toDoubles(split);
	}

	/**
	 * Joins the given numbers into a Dilimited string using {@link #DEFAULT_DILIMETER}
	 * 
	 * @param numbers
	 * @return
	 */
	public static String join(Number[] numbers) {
		return join(numbers, DEFAULT_DILIMETER);
	}

	/**
	 * Joins the numbers using the given dilimeter.
	 * 
	 * @see StringUtils#join(Object[], String)
	 * 
	 * @param numbers
	 * @param separator
	 * @return
	 */
	public static String join(Number[] numbers, String separator) {
		return StringUtils.join(numbers, separator);
	}

	/**
	 * Given an array of Strings that can be converted to Doubles...this will return that converted
	 * array of Doubles.
	 * 
	 * @param doublesAsStrings
	 * @return
	 */
	private static Double[] toDoubles(String[] doublesAsStrings) {
		Double[] result = new Double[doublesAsStrings.length];

		for (int i = 0; i < doublesAsStrings.length; i++) {
			result[i] = Double.valueOf(doublesAsStrings[i]);
		}
		return result;
	}

	/**
	 * Simple indicator if the number is an odd integer.
	 * 
	 * @see #isEven(Integer)
	 * */
	public static Boolean isOdd(Integer number) {
		return !isEven(number);
	}

	/**
	 * Simple indicate if the number is even divisible by 2.
	 * 
	 * @see #isOdd(Integer)
	 * @param number
	 * @return
	 */
	public static Boolean isEven(Integer number) {
		return number % 2 == 0;
	}

	public static Integer sum(Iterable<Integer> integers) {
		return sum(integers.iterator());
	}

	/**
	 * @param iterator
	 * @return
	 */
	public static Integer sum(Iterator<Integer> iterator) {
		int result = 0;
		while (iterator.hasNext()) {
			result += iterator.next();
		}
		return result;
	}

	/**
	 * Returns the string or null if null is provided or the string otherwise can't be interpreted.
	 * 
	 * @see Ints#tryParse(String)
	 * @param integerAsString
	 * @return
	 */
	public static Integer parseInteger(String integerAsString) {
		return (integerAsString == null) ? null : Ints.tryParse(integerAsString);
	}

	/**
	 * Shorthand to create a new Integer from an int.
	 * 
	 * @param value
	 * @return
	 */
	public static Integer I(int value) {
		return new Integer(value);
	}

	public static Integer I(double value) {
		return new Integer((int) value);
	}

	public static Function<Long, Integer> integerFromLongFunction() {
		return new Function<Long, Integer>() {

			@Override
			public Integer apply(Long input) {
				return (input == null) ? null : input.intValue();
			}
		};
	}

	/**
	 * Uses the {@link Long#parseLong(String)} to parse the given string.
	 * 
	 * @return
	 */
	public static Function<String, Long> longFromStringFunction() {
		return new Function<String, Long>() {

			@Override
			public Long apply(String input) {
				return (input != null) ? Long.parseLong(input) : null;
			}
		};
	}

	private enum DoubleFromIntegerFunction implements Function<Integer, Double> {
		INSTANCE;
		@Override
		@Nullable
		public Double apply(@Nullable Integer integer) {
			return (integer != null) ? integer.doubleValue() : null;
		}

		@Override
		public String toString() {
			return "DoubleFromIntegerFunction";
		}
	}

	public static Function<Integer, Double> doubleFromIntegerFunction() {
		return DoubleFromIntegerFunction.INSTANCE;
	}
}
