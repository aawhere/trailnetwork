/**
 *
 */
package com.aawhere.i18n;

import java.util.Locale;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Utility methods for Internationalization.
 * 
 * @author Brian Chapman
 * 
 */
public class LocaleUtil {
	/**
	 *
	 */
	private static final String ANY = "*";
	public static final String ENGLISH_US = "en-US";
	public static final String DEUTSCH_DE = "de-DE";
	public static final String DEUTSCH_CH = "de-CH";

	/**
	 * Determine if the locale is valid. In particular, the getCountry and getLanguage methods
	 * should return valid strings.
	 * 
	 * @param locale
	 * @return
	 */
	public static void validateLocale(Locale locale) {
		if (locale == null) {
			throw new NullPointerException("locale may not be null!");
		}
		if (isAny(locale)) {
			throw new IllegalArgumentException("unable to handle local with *");
		}
		// Will throw an illegal argument exception if Locale was created incorrectly i.e.
		// Locale.forLanguageTag("none");
		LocaleUtils.toLocale(locale.toString());
	}

	public static Locale fromWebString(String stringWithHyphens) {
		String[] strings = StringUtils.split(stringWithHyphens, "-");
		String lang = null;
		String country = null;
		if (strings.length >= 0) {
			lang = strings[0];
		}
		if (strings.length >= 1) {
			country = strings[1];
		}

		return localeFrom(lang, country);

	}

	/**
	 * If {@link #ANY} is provided or null is provided for language then the given default locale
	 * will be provided. If language is provide with any country, then a local with only language
	 * will be provided.
	 * 
	 * @param locale
	 * @param defaultLocale
	 * @return
	 */
	public static Locale convertFromAny(Locale locale, Locale defaultLocale) {
		if (isAny(locale)) {
			if (isAnyLanguage(locale)) {
				locale = defaultLocale;
			} else {
				// keep the language and remove the country
				locale = new Locale(locale.getLanguage());
			}
		}
		return locale;
	}

	/**
	 * Does null checking of values before creating the Locale which doesn't like null values.
	 * 
	 * @param lang
	 * @param country
	 * @return the matching Locale or null if only nulls provided.
	 */
	static public Locale localeFrom(String lang, String country) {
		Locale result;
		if (country == null) {
			if (lang != null) {
				result = new Locale(lang);
			} else {
				result = null;
			}
		} else {
			result = new Locale(lang, country);
		}
		return result;
	}

	/**
	 * @param locale
	 * @return true if language or country is using {@link #ANY}
	 */
	public static Boolean isAny(Locale locale) {
		return isAnyLanguage(locale) || isAnyCountry(locale);
	}

	/**
	 * @param locale
	 * @return
	 */
	private static boolean isAnyCountry(Locale locale) {
		return locale.getCountry() == null || ANY.equals(locale.getCountry());
	}

	/**
	 * @param locale
	 * @return
	 */
	private static boolean isAnyLanguage(Locale locale) {
		return locale.getLanguage() == null || ANY.equals(locale.getLanguage());
	}

	public static Locale any() {
		return new Locale(ANY);
	}
}
