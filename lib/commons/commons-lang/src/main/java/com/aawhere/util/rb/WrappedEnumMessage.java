package com.aawhere.util.rb;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.xml.XmlNamespace;

/**
 * When a enumerated message needs to be wrapped this provides all the behavior of the original, but
 * allows for replacement of the param keys.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class WrappedEnumMessage
		extends StringMessage {

	private static final long serialVersionUID = 3883739563658532721L;

	private Message message;
	private ParamKey[] paramKeys;

	WrappedEnumMessage() {
	}

	public WrappedEnumMessage(Message message, ParamKey[] paramKeys) {
		this(message);
		this.paramKeys = paramKeys;
	}

	public WrappedEnumMessage(Message message) {
		super(message.getValue(), message.getValue());
		this.message = message;
		this.paramKeys = message.getParameterKeys();
	}

	/** provides access to the original. */
	public Message wrapped() {
		return message;
	}

	@Override
	public String name() {
		return message.name();
	}

	@Override
	public String getValue() {
		return message.getValue();
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return paramKeys;
	}

	@Override
	public String toString() {
		return message.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(paramKeys);
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WrappedEnumMessage other = (WrappedEnumMessage) obj;
		if (!Arrays.equals(paramKeys, other.paramKeys))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}

}
