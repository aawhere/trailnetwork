/**
 * 
 */
package com.aawhere.lang.string;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Useful for displaying any object with a toString method implemented in XML. unmarshal always
 * returns null. Some objects are too complex for marshaling, but still useful for displaying to
 * human readers.
 * 
 * @author aroller
 * 
 */
public class ToStringXmlAdapter
		extends XmlAdapter<String, Object> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Object unmarshal(String v) throws Exception {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Object v) throws Exception {
		return String.valueOf(v);
	}

}
