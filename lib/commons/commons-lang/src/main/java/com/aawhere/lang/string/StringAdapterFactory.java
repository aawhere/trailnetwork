/**
 *
 */
package com.aawhere.lang.string;

import java.lang.reflect.Type;
import java.util.Collection;

import com.aawhere.adapter.AbstractAdapterFactory;
import com.aawhere.adapter.Adapter;
import com.google.inject.Singleton;

/**
 * A singleton that manages adapters mapped by their class or a super class.
 * 
 * Personalization would likely require individual instances of this, but it is encouraged to
 * provide a PersonalizedStringAdapterFactory for such a case.
 * 
 * @author aroller
 * 
 */
@Singleton
@SuppressWarnings("unchecked")
public class StringAdapterFactory
		extends AbstractAdapterFactory<String> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AbstractAdapterFactory#getAdapterOrNull(java.lang.reflect.Type,
	 * java.lang.Class)
	 */
	@Override
	public <P, T> StringAdapter<T> getAdapterOrNull(Type type, Class<P> parameterType) {
		StringAdapter<T> adapter = (StringAdapter<T>) super.getAdapterOrNull(type, parameterType);
		// create default registrations
		// located here to provide registered first...always, automatic second.
		if (adapter == null && (type instanceof Class)) {
			// add fromstring adapter for free without registration if compatible
			Class<P> typeAsClass = (Class<P>) type;
			if (FromString.qualifies(typeAsClass)) {
				adapter = (StringAdapter<T>) new FromStringAdapter.Builder<P>().setType(typeAsClass).build();
			}
		}
		return adapter;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AbstractAdapterFactory#getAdapterOrNull(java.lang.reflect.Type)
	 */
	@Override
	public <T> StringAdapter<T> getAdapterOrNull(Type type) {
		return (StringAdapter<T>) super.getAdapterOrNull(type);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AbstractAdapterFactory#getAdapter(java.lang.reflect.Type)
	 */
	@Override
	public <T> StringAdapter<T> getAdapter(Type type) {
		return (StringAdapter<T>) super.getAdapter(type);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AbstractAdapterFactory#getAdapter(java.lang.reflect.Type,
	 * java.lang.Class)
	 */
	@Override
	public <T> StringAdapter<T> getAdapter(Type type, Class<?> parameterType) {
		return (StringAdapter<T>) super.getAdapter(type, parameterType);
	}

	/**
	 * Registers all of the known java.lang and other standard adapters that adhere to the contract.
	 */
	public StringAdapterFactory registerStandardAdapters() {
		register(new FromStringAdapter.Builder<Boolean>().setType(Boolean.class).build());
		register(new FromStringAdapter.Builder<Byte>().setType(Byte.class).build());
		register(new FromStringAdapter.Builder<Short>().setType(Short.class).build());
		register(new FromStringAdapter.Builder<Integer>().setType(Integer.class).build());
		register(new FromStringAdapter.Builder<Long>().setType(Long.class).build());
		register(new FromStringAdapter.Builder<Float>().setType(Float.class).build());
		register(new FromStringAdapter.Builder<Double>().setType(Double.class).build());
		register(new FromStringAdapter.Builder<String>().setType(String.class).build());
		register(EnumStringAdapter.create().build());
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AbstractAdapterFactory#getCollectionAdapter(com.aawhere.lang.Adapter,
	 * java.lang.Class)
	 */
	@Override
	protected <C extends Collection<T>, T> Adapter<C, String> getCollectionAdapter(Adapter<T, String> adapter,
			Class<T> paramaterType) {
		return new CollectionStringAdapter.Builder<C, T>().setAdapter((StringAdapter<T>) adapter)
				.setParameterType(paramaterType).build();
	}

}
