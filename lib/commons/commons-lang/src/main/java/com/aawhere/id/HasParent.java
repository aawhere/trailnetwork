/**
 *
 */
package com.aawhere.id;

/**
 * @author Brian Chapman
 * 
 */
public interface HasParent<I extends Identifier<?, ?>> {

	I getParent();

	/**
	 * Indicates that this is the root node so even though it is of a type that can have a parent,
	 * this one does not.
	 * 
	 * @see IdentifierWithParent
	 * @return
	 */
	Boolean isRoot();
}
