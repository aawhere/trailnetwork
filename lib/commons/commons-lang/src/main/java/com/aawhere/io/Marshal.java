/**
 *
 */
package com.aawhere.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import org.apache.commons.io.output.ByteArrayOutputStream;

import com.aawhere.util.rb.CompleteMessage;

/**
 * Serialize objects.
 * 
 * @author Brian Chapman
 * 
 */
public class Marshal {

	public static <T extends OutputStream> T marshal(Object object, T out) {
		try {
			ObjectOutputStream marshaler = new ObjectOutputStream(out);
			marshaler.writeObject(object);
			marshaler.close();
			return out;
		} catch (IOException e) {
			// Want to provide an informative message to developer, but don't want a checked
			// exception since one shouldn't be sending us non-serializable classes.
			try {
				throw new IoException(new CompleteMessage.Builder(IoMessage.SERIALIZATION_PROBLEM)
						.addParameter(IoMessage.Param.CLASS_NAME, object.getClass().getSimpleName()).build(), e);
			} catch (IoException ee) {
				throw new RuntimeException(ee);
			}
		}
	}

	public static byte[] marshal(Object object) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		marshal(object, out);
		return out.toByteArray();
	}

	@SuppressWarnings("unchecked")
	public static <ObjectT> ObjectT unMarshal(InputStream marshaled) {
		ObjectT object = null;
		try {
			ObjectInputStream in = new ObjectInputStream(marshaled);
			object = (ObjectT) in.readObject();
			in.close();

		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		return object;
	}

	public static <ObjectT> ObjectT unMarshal(byte[] marshaled) {
		ByteArrayInputStream in = new ByteArrayInputStream(marshaled);
		return unMarshal(in);
	}
}
