/**
 *
 */
package com.aawhere.lang.string;

import java.io.Serializable;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.SetUtils;
import org.apache.commons.collections.functors.EqualPredicate;

import com.aawhere.lang.ObjectBuilder;

/**
 * A collection wrapper for {@link CharacterIndex} to provide useful summation of the contents
 * inside. A good use of this class would be to iterate all of the Character indexes and highlight
 * those values that are not valid.
 * 
 * Add indexes using the {@link Builder#add(Character, Integer)} and retrieve all using
 * {@link #getAll()}.
 * 
 * TODO:This doesn't do much, but should be extended to satisfy needs of reporting information like
 * a String representing all of the invalid characters, etc.
 * 
 * @author roller
 * 
 */
public class CharacterIndexes
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4243926385792984515L;

	public static class Builder
			extends ObjectBuilder<CharacterIndexes> {

		private TreeSet<CharacterIndex> indexes = new TreeSet<CharacterIndex>();

		public Builder(String representing) {
			super(new CharacterIndexes(representing));
		}

		/**
		 * Creates a {@link CharacterIndex} given the values and adds it to the list available in
		 * {@link CharacterIndexes#getAll()}.
		 * 
		 * @param character
		 * @param index
		 *            must be an index within the {@link CharacterIndexes#representingString}.
		 * @return
		 */
		public Builder add(Character character, Integer index) {
			if (index > building.getMaxIndex()) {
				throw new IllegalArgumentException(index + " does not match " + building.representingString);
			}
			CharacterIndex characterIndex = building.new CharacterIndex(character, index);
			boolean successfullyAdded = indexes.add(characterIndex);
			if (!successfullyAdded) {
				CharacterIndex existing = (CharacterIndex) CollectionUtils.find(indexes, new EqualPredicate(
						characterIndex));
				throw new IllegalArgumentException(characterIndex + " cannot be added because index already exists "
						+ existing);
			}
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@SuppressWarnings("unchecked")
		@Override
		public CharacterIndexes build() {
			building.all = SetUtils.unmodifiableSortedSet(indexes);
			return super.build();
		}
	}

	/**
	 * A super simple class that represents a character located at a particular index of a string.
	 * 
	 * @author roller
	 * 
	 */
	public class CharacterIndex
			implements Serializable, Comparable<CharacterIndex> {

		private static final long serialVersionUID = -6879390318220105622L;

		static final String TO_STRING_DILIMETER = "=";
		private Character character;
		private Integer index;

		/**
		 *
		 */
		private CharacterIndex(Character character, Integer index) {

			this.character = character;
			this.index = index;
		}

		/**
		 * @return the character
		 */
		public Character getCharacter() {
			return character;
		}

		/**
		 * @return the index
		 */
		public Integer getIndex() {
			return index;
		}

		/**
		 * @return the string
		 */
		public String getString() {
			return representingString;
		}

		public String toString() {
			return String.valueOf(this.index) + TO_STRING_DILIMETER + this.character;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CharacterIndex other = (CharacterIndex) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (index == null) {
				if (other.index != null)
					return false;
			} else if (!index.equals(other.index))
				return false;
			return true;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((index == null) ? 0 : index.hashCode());
			return result;
		}

		private CharacterIndexes getOuterType() {
			return CharacterIndexes.this;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(CharacterIndex that) {
			return this.getIndex().compareTo(that.getIndex());
		}

	}

	private SortedSet<CharacterIndex> all;

	private String representingString;

	/**
	 *
	 */
	private CharacterIndexes(String representing) {
		this.representingString = representing;
	}

	public SortedSet<CharacterIndex> getAll() {
		return all;
	}

	private Integer getMaxIndex() {
		return this.representingString.length() - 1;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return this.all.toString();
	}
}
