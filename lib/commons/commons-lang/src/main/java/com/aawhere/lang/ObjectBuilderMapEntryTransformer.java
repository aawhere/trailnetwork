/**
 * 
 */
package com.aawhere.lang;

import com.google.common.collect.Maps;
import com.google.common.collect.Maps.EntryTransformer;

/**
 * Use this to simply transform a map of builders to their corresponding result object using
 * {@link Maps#transformEntries(java.util.Map, EntryTransformer)} and the likes.
 * 
 * @author aroller
 * 
 */
public class ObjectBuilderMapEntryTransformer<K, B extends ObjectBuilder<O>, O>
		implements EntryTransformer<K, B, O> {

	/*
	 * (non-Javadoc)
	 * @see com.google.common.collect.Maps.EntryTransformer#transformEntry(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public O transformEntry(K key, B builder) {
		return (builder == null) ? null : builder.build();
	}

}
