/**
 * 
 */
package com.aawhere.util.rb;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

/**
 * @author aroller
 * 
 */
public class MessageUtil {

	public static Function<String, StringMessage> stringToMessageFunction() {
		return new Function<String, StringMessage>() {

			@Override
			public StringMessage apply(String input) {
				if (input == null) {
					return null;
				}
				return new StringMessage(input);
			}
		};
	}

	public static Function<Message, String> messageToStringFunction() {
		return new Function<Message, String>() {

			@Override
			public String apply(Message input) {
				return valueOf(input);
			}
		};
	}

	/**
	 * @param bestStartName
	 * @return
	 */
	public static String valueOf(Message message) {
		if (message == null) {
			return null;
		}
		return message.getValue();
	}

	/**
	 * Provides a Message for the given value or the default if value is null.
	 * 
	 * @param value
	 * @return
	 */
	public static Message message(String value, Message defaultValue) {
		return (value == null) ? defaultValue : new StringMessage(value);
	}

	public static Function<StatusMessage, Message> messageFromStatusMessageFunction() {
		return new Function<StatusMessage, Message>() {

			@Override
			public Message apply(StatusMessage input) {
				return (input == null) ? null : input.message.getMessage();
			}
		};
	}

	/**
	 * Provides a predicate to filter only those status messages with the one being sought after.
	 * 
	 * @param message
	 * @return
	 */
	public static Predicate<StatusMessage> statusMessagePredicate(Message message) {
		return Predicates.compose(Predicates.equalTo(message), messageFromStatusMessageFunction());
	}

	public static boolean isEnum(Message message) {
		return message != null && (message.getClass().isEnum() || message instanceof WrappedEnumMessage);
	}

	@SuppressWarnings("unchecked")
	public static Enum<? extends Message> messageAsEnum(Message key) {
		if (key instanceof WrappedEnumMessage) {
			key = ((WrappedEnumMessage) key).wrapped();
		}
		// class cast exception here means you should call #isEnum first
		return (Enum<? extends Message>) key;
	}

	/**
	 * Used with ICU Select format, this allows an enumeration to follow the CONSTANT_CASE naming
	 * standard while following their convention of using lower case. It's encouraged to use single
	 * words for simplicity.
	 * 
	 * @param value
	 * @return
	 */
	public static String selectKey(Object value) {
		return value.toString().toLowerCase();
	}

}
