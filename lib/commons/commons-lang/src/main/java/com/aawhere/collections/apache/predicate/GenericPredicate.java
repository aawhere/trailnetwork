/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 4, 2010
commons-lang : com.aawhere.collections.apache.functor.GenericFunctor.java
 */
package com.aawhere.collections.apache.predicate;

import org.apache.commons.collections.Predicate;

/**
 * Just a generic version of {@link Functor} helping with casting and type safety.
 * 
 * @author roller
 * 
 */
abstract public class GenericPredicate<ValueT>
		implements Predicate {

	/**
	 * 
	 */
	protected GenericPredicate() {
	}

	/**
	 * New method required for children to implement. {@link #evaluate(Object)} calls this method so
	 * all Predicate
	 * 
	 * @param object
	 * @return
	 */
	abstract boolean evaluateCustom(ValueT object);

	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean evaluate(Object object) {

		return evaluateCustom((ValueT) object);

	}

}
