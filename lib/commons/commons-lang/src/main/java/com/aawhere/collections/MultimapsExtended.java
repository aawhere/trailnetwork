/**
 * 
 */
package com.aawhere.collections;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Maps.EntryTransformer;
import com.google.common.collect.Multimap;

/**
 * @author aroller
 * 
 */
public class MultimapsExtended {

	/**
	 * Standard way to translate the given {@link Multimap} into a map which counts the number of
	 * elements for each key.
	 * 
	 * @param multimap
	 * @return
	 */
	public static <K> Map<K, Integer> counts(Multimap<K, Integer> multimap) {
		return Maps.transformEntries(multimap.asMap(), countEntryTransformer());
	}

	/**
	 * Provides the count for each collection. Useful when used with {@link Multimap#asMap()}.
	 * 
	 * @return
	 */
	public static <K> EntryTransformer<K, Collection<?>, Integer> countEntryTransformer() {
		return new EntryTransformer<K, Collection<?>, Integer>() {

			@Override
			public Integer transformEntry(K key, Collection<?> value) {
				return (value != null) ? value.size() : null;
			}
		};
	}

	/**
	 * When you have two lists that are of equal size and each index is to be paired with each other
	 * this will iterate the keys and put the corresponding value with the key.
	 * 
	 * @param keys
	 * @param values
	 */
	public static <K, V> ListMultimap<K, V> multimap(List<K> keys, List<V> values) {
		LinkedListMultimap<K, V> multimap = LinkedListMultimap.create();

		for (int i = 0; i < keys.size(); i++) {
			multimap.put(keys.get(i), values.get(i));
		}
		return multimap;
	}
}
