/**
 * 
 */
package com.aawhere.collections;

import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * Casts an existing set unchanged or returns a new HashSet of the given iterable.
 * 
 * @author aroller
 * 
 */
public class SetUtilsExtended {

	public static <T> Set<T> asSet(Iterable<T> iterable) {
		if (iterable instanceof Set) {
			return (Set<T>) iterable;
		}
		return Sets.newHashSet(iterable);
	}

	/**
	 * @param aliases
	 * @param expectedUsername
	 * @return
	 */
	@SafeVarargs
	public static <E> Iterable<E> newHashSet(Iterable<E> iterable, E... elements) {
		HashSet<E> newHashSet = Sets.newHashSet(elements);
		Iterables.addAll(newHashSet, iterable);
		return newHashSet;
	}

	/**
	 * Provides a function that converts the iterable into a set per the rules of
	 * {@link #asSet(Iterable)}.
	 * 
	 * @return
	 */
	public static <F> Function<Iterable<F>, Set<F>> setFunction() {
		return new Function<Iterable<F>, Set<F>>() {

			@Override
			public Set<F> apply(Iterable<F> input) {
				return asSet(input);
			}
		};
	}

	/**
	 * Similar to {@link Functions#constant(Object)}, but without the generic hell for this specific
	 * situation.
	 * 
	 * @param ids
	 * @return
	 */
	public static <F> Function<Set<F>, Iterable<F>> constant(final Set<F> ids) {

		// the difference between expected ids gives those that already exist
		Function<Set<F>, Iterable<F>> function = new Function<Set<F>, Iterable<F>>() {

			@Override
			public Iterable<F> apply(Set<F> input) {
				return ids;
			}
		};
		return function;
	}
}
