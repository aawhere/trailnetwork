/**
 * 
 */
package com.aawhere.id;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Converts any {@link Identifiable} to it's simple {@link Identifier}.
 * 
 * @author aroller
 * 
 */
public class IdentifierFromIdentifiableXmlAdapter<E extends Identifiable<I>, I extends Identifier<?, E>>
		extends XmlAdapter<I, E> {

	/**
	 * 
	 */
	public IdentifierFromIdentifiableXmlAdapter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public E unmarshal(I identifier) throws Exception {
		throw new UnsupportedOperationException("we'd  need a provider for this");
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public I marshal(E identifiable) throws Exception {
		return (identifiable == null) ? null : identifiable.getId();
	}

}
