/**
 * 
 */
package com.aawhere.id;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.google.common.base.Function;

/**
 * A delegate for StringIdentifierWithParent and LongIdentifierWithParent to
 * 
 * * equality is handled by {@link Identifier}
 * 
 * @author aroller
 * 
 */
public class IdentifierWithParent<V extends Comparable<V>, P extends Identifier<?, ?>>
		implements Serializable {

	private static final int PARENT_PART_INDEX = 0;
	private static final int CHILD_PART_INDEX = 1;
	private V value;
	/** The parent id, if provided, indicating the direct parent of this id. null indicates a root. */
	@Nullable
	private P parent;
	private Class<?> kind;
	private Boolean parentRequired = true;

	/**
	 * Used to construct all instances of IdentifierWithParent.
	 */
	public static class Builder<V extends Comparable<V>, P extends Identifier<?, ?>>
			extends ObjectBuilder<IdentifierWithParent<V, P>> {
		private String compositeValue;
		private Class<P> parentType;
		private Function<String, V> valueFunction;
		private Function<String, P> parentFunction;

		public Builder() {
			super(new IdentifierWithParent<V, P>());
		}

		public Builder<V, P> parent(P parent) {
			building.parent = parent;
			return this;
		}

		public Builder<V, P> kind(Class<?> kind) {
			building.kind = kind;
			return this;
		}

		public Builder<V, P> compositeValue(String compositeValue) {
			this.compositeValue = compositeValue;
			return this;
		}

		public Builder<V, P> parentType(Class<P> parentType) {
			this.parentType = parentType;
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();

		}

		@Override
		public IdentifierWithParent<V, P> build() {

			IdentifierWithParent<V, P> built = super.build();
			// build from the composite if not alread provided
			if (built.value == null) {
				Assertion.exceptions().notNull("compositeValue", this.compositeValue);
				Assertion.exceptions().notNull("parentType", this.parentType);

				if (built.parentRequired) {
					if (this.parentFunction == null) {
						String[] parts = IdentifierUtils.splitCompositeIdValueFirstOccurrence(compositeValue);
						if (parts.length == 2) {
							built.value = valueFunction.apply(parts[CHILD_PART_INDEX]);
							built.parent = IdentifierUtils.idFrom(parts[PARENT_PART_INDEX], parentType);

						} else {
							if (built.parentRequired) {
								throw IdentifierUtils.throwInvalid(compositeValue);
							}
						}
					} else {
						built.parent = parentFunction.apply(compositeValue);
						valueAssignedFromFunction(built);
					}
				} else {
					valueAssignedFromFunction(built);

				}
			}
			Assertion.exceptions().notNull("kind", built.kind);
			Assertion.exceptions().notNull("value", built.value);
			if (built.parentRequired) {
				Assertion.exceptions().notNull("parent", built.parent);
			}
			return built;
		}

		private void valueAssignedFromFunction(IdentifierWithParent<V, P> built) {
			built.value = valueFunction.apply(compositeValue);
		}

		/**
		 * @param value
		 * @return
		 */
		public Builder<V, P> value(V value) {
			building.value = value;
			return this;
		}

		/**
		 * @param valuefunction2
		 * @return
		 */
		public Builder<V, P> valueFunction(Function<String, V> valuefunction) {
			this.valueFunction = valuefunction;
			return this;
		}

		public Builder<V, P> parentFunction(Function<String, P> parentFunction) {
			this.parentFunction = parentFunction;
			return this;
		}

		/**
		 * @return
		 */
		public Builder<V, P> parentNotRequired() {
			building.parentRequired = false;
			return this;
		}
	}// end Builder

	public static <V extends Comparable<V>, P extends Identifier<?, ?>> Builder<V, P> create() {
		return new Builder<V, P>();
	}

	public Boolean isRoot() {
		return this.parent == null;
	}

	public P parent() {
		return this.parent;
	}

	public V value() {
		return this.value;
	}

	public Class<?> kind() {
		return this.kind;
	}

	private static final long serialVersionUID = -4677288946148713344L;
}
