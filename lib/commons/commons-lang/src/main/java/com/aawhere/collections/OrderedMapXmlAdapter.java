/**
 *
 */
package com.aawhere.collections;

import java.util.Map;

import org.apache.commons.collections.map.LinkedMap;

/**
 * @author Brian Chapman
 * 
 */
public class OrderedMapXmlAdapter
		extends MapXmlAdapter {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.collections.MapXmlAdapter#createMap()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected Map createMap() {
		return new LinkedMap();
	}
}
