/**
 * 
 */
package com.aawhere.io;

/**
 * A simple class to name constants because Java was too lame to provide the standard.
 * 
 * http://stackoverflow.com/questions/1684040/java-why-charset-names-are-not-constants
 * 
 * 
 * @author roller
 * 
 */
public class CharsetConstants {

	public static final String UTF8 = "UTF-8";
}
