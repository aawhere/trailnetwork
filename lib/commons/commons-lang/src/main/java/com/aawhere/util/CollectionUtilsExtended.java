/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 16, 2010
commons-lang : com.aawhere.util.CollectionUtilsExtended.java
 */
package com.aawhere.util;

import java.util.Collections;

import org.apache.commons.collections.CollectionUtils;

/**
 * Extends {@link Collections} and {@link CollectionUtils} to do things they do not.
 * 
 * @deprecated use {@link com.aawhere.collections.CollectionUtilsExtended} TODO: Move this to
 *             collections package to be with its friends.
 * @author roller
 * 
 */
@Deprecated
public class CollectionUtilsExtended
		extends com.aawhere.collections.CollectionUtilsExtended {

}
