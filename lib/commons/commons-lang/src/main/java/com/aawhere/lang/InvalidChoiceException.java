/**
 *
 */
package com.aawhere.lang;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Used when communicating that an incorrect choice was made.
 * 
 * Every effort should be made to explain which choices are available, but for enums this is done
 * automatically so just provide the enum chosen and this will do the rest.
 * 
 * Useful in the default statement to remind a developer that a situation is not handled.
 * 
 * Although {@link HttpStatusCode#BAD_REQUEST} may be appropriate when providing badness, the fact
 * that there are choices may be offered to the client for recovery.
 * 
 * @author Aaron Roller
 * 
 */
@StatusCode(HttpStatusCode.MULT_CHOICE)
public class InvalidChoiceException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 8940285753890301440L;

	public static final HttpStatusCode CODE = HttpStatusCode.MULT_CHOICE;

	/**
	 * When only the choices are known and nothing was picked.
	 * 
	 * @param choices
	 */
	public InvalidChoiceException(Object[] choices) {
		super(message(null, choices));
	}

	/**
	 * Displays the chosen value that is not of the available choices.
	 * 
	 * The available choices are optional so if they aren't known they don't need to be provided.
	 * 
	 * @param keyOrMessage
	 */
	public InvalidChoiceException(Object switchVariable, Object... choices) {
		super(message(switchVariable, choices));
	}

	private static CompleteMessage message(Object chosen, Object... choices) {
		return LangMessage.create(chosen, choices);
	}
}
