/**
 * 
 */
package com.aawhere.log;

import java.util.logging.Logger;

/**
 * A really simple class intended top serve up {@link Logger} in a standard way.
 * 
 * Also, consider adding a template to eclipse so you don't even need to use this factory.
 * 
 * Example found at <a href="http://eclipse.dzone.com/news/effective-eclipse-custom-templ">this
 * website</a>.
 * 
 * template name = logit
 * 
 * <pre>
 * ${:import(java.util.logging.Logger)} 
 * private static final Logger logger =Logger.getLogger(${enclosing_type}.class.getSimpleName());
 * </pre>
 * 
 * or for making a log entry:
 * 
 * <pre>
 * ${:import(java.util.logging.Level)}
 * if (logger.isLoggable(Level.INFO)) {
 * 	  logger.info("${cursor}");
 * }
 * </pre>
 * 
 * @author roller
 * 
 */
public class LoggerFactory {

	/**
	 * Retrieves a logger in a standard way. Consider using templates described in this class'
	 * documentation.
	 */
	public static Logger getLogger(Class<?> whereFrom) {

		return Logger.getLogger(whereFrom.getName());
	}

}
