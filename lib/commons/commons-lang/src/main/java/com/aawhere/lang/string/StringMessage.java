/**
 * 
 */
package com.aawhere.lang.string;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Useful message constants for the string package.
 * 
 * @author aroller
 * 
 */
public enum StringMessage implements Message {

	/** Explain_Entry_Here */
	INVALID_FORMAT(
			"Unable to decode {INVALID_STRING} into a {EXPECTED_CLASS}.",
			Param.INVALID_STRING,
			Param.EXPECTED_CLASS);

	private ParamKey[] parameterKeys;
	private String message;

	private StringMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** The string that is not able to be converted */
		INVALID_STRING,
		/** The class that is attempting to be built with a string. */
		EXPECTED_CLASS;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
