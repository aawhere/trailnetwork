/**
 * 
 */
package com.aawhere.lang;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

/**
 * Replacement for the apache removal of this from commons-lang3.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.INTERNAL_ERROR)
public class NotImplementedException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 440759640012519201L;

	/**
	 * 
	 */
	public NotImplementedException() {
		super("This portion of code has not been implemented.");
	}

	public NotImplementedException(String message) {
		super(message);
	}

	/**
	 * Bypasses compiler's restriction of throwing always before other code can execute.
	 * 
	 * @return
	 */
	public static NotImplementedException throwIt() {
		throw new NotImplementedException();
	}

	public static NotImplementedException throwIt(String messsage) {
		throw new NotImplementedException(messsage);
	}
}
