/*
 * www.akuacom.com - Automating Demand StatusCode
 * 
 * com.akuacom.common.exception.BaseRuntimeException.java - Copyright(c)1994 to 2010 by Akuacom . All rights reserved. 
 * Redistribution and use in source and binary forms, with or without modification, is prohibited.
 *
 */
package com.aawhere.lang.exception;

import com.aawhere.lang.If;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;

/**
 * All RuntimeExceptions should extend this exception to provide valuable information.
 */
abstract public class BaseRuntimeException
		extends RuntimeException
		implements MessageException {

	private static final long serialVersionUID = 2176551578167720L;
	private ExceptionLocalizer exceptionLocalizer;
	private HttpStatusCode statusCode;

	/**
	 * Instantiates a new base runtime exception.
	 * 
	 * @param key
	 *            the key
	 */
	protected BaseRuntimeException(String keyOrMessage) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(keyOrMessage));
	}

	/**
	 * Instantiates a new base runtime exception.
	 * 
	 * @param key
	 *            the key
	 * @param cause
	 *            the cause
	 */
	protected BaseRuntimeException(String key, Throwable cause) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(key, cause));
	}

	/**
	 * Instantiates a new base runtime exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	protected BaseRuntimeException(Throwable cause) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(cause));
	}

	protected BaseRuntimeException(String key, Object... params) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(key, params));
	}

	BaseRuntimeException(ExceptionLocalizer exceptionLocalizer) {
		super(exceptionLocalizer.getCause());
		// implements messageException so we must make the leap!
		this.exceptionLocalizer = exceptionLocalizer;
	}

	public BaseRuntimeException(CompleteMessage completeMessage) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(completeMessage));
	}

	public BaseRuntimeException(CompleteMessage completeMessage, Exception e) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(completeMessage, e));
	}

	public BaseRuntimeException(Message message) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(message));
	}

	public BaseRuntimeException(Message message, Exception e) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(message, e));
	}

	/**
	 * Useful when you have a {@link BaseException} with all the right stuff, but you need a
	 * runtime. This will grab all the right stuff.
	 * 
	 * @see ToRuntimeException
	 * 
	 * @param like
	 */
	public BaseRuntimeException(BaseException like) {
		super(like);
		this.exceptionLocalizer = like.getExceptionLocalizer();
	}

	@Override
	public String getLocalizedMessage() {
		return this.exceptionLocalizer.getMessage();
	}

	@Override
	public String getMessage() {

		return this.exceptionLocalizer.getMessage();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.exception.MessageException#getCompleteMessage()
	 */
	@Override
	public CompleteMessage getCompleteMessage() {
		if (this.exceptionLocalizer instanceof MessageResourceBundleExceptionLocalizer) {
			return ((MessageResourceBundleExceptionLocalizer) this.exceptionLocalizer).getDynamicMessage();
		} else {
			return null;
		}
	}

	@Override
	public HttpStatusCode getStatusCode() {
		// don't call ExceptionUtil.statusCode since that calls this.
		return If.nil(this.statusCode).use(ExceptionUtil.statusCodeFromAnnotation(this));
	}

	/**
	 * Sets the overriding status code available only to implementors.
	 * 
	 * This breaks the non-mutable intentions, but the construction madness has to stop.
	 * 
	 * @param statusCode
	 */
	protected void setStatusCode(HttpStatusCode statusCode) {
		this.statusCode = statusCode;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.exception.MessageException#getErrorCode()
	 */
	@Override
	public String getErrorCode() {
		return MessageResourceBundleExceptionLocalizer.getErrorCode(exceptionLocalizer);
	}
}
