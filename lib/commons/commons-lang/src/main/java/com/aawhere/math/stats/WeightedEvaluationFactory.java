/**
 * 
 */
package com.aawhere.math.stats;

import org.apache.commons.collections.primitives.ArrayDoubleList;
import org.apache.commons.math.stat.descriptive.WeightedEvaluation;
import org.apache.commons.math.stat.descriptive.moment.Mean;

/**
 * Creates {@link StorelessWeightedEvaluation}s using known implementations of
 * {@link WeightedEvaluation}.
 * 
 * @author Aaron Roller
 * 
 * @see Mean
 */
public class WeightedEvaluationFactory {

	public static StorelessWeightedEvaluation newMean() {
		return new DefaultStorelessWeightedEvaluation(new Mean());
	}

	private static class DefaultStorelessWeightedEvaluation
			implements StorelessWeightedEvaluation {

		private ArrayDoubleList values = new ArrayDoubleList();
		private ArrayDoubleList weights = new ArrayDoubleList();

		private WeightedEvaluation weightedEvaluation;

		private DefaultStorelessWeightedEvaluation(WeightedEvaluation weightedEvaluation) {
			this.weightedEvaluation = weightedEvaluation;
		}

		@Override
		public double evaluate(double[] values, double[] weights) {
			return this.weightedEvaluation.evaluate(values, weights);
		}

		@Override
		public double evaluate(double[] values, double[] weights, int begin, int length) {
			return this.weightedEvaluation.evaluate(values, weights, begin, length);
		}

		@Override
		public void increment(double value, double weight) {
			this.values.add(value);
			this.weights.add(weight);

		}

		@Override
		public void incrementAll(double[] values, double[] weights) {
			if (values.length != weights.length) {
				throw new IllegalArgumentException("Values array length does not equal weights array length "
						+ values.length + " : " + weights.length);
			}
			for (int i = 0; i < weights.length; i++) {
				double v = values[i];
				double w = weights[i];
				increment(v, w);

			}

		}

		@Override
		public double getResult() {

			return evaluate(this.values.toArray(), this.weights.toArray());
		}

	}
}
