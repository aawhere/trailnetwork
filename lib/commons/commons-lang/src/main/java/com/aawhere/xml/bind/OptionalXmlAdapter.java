/**
 * 
 */
package com.aawhere.xml.bind;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.lang.NotImplementedException;
import com.google.inject.Singleton;

/**
 * Generic class used to Adapt any {@link XmlType} to being shown or not based on the {@link #show}
 * setting. General use is to create a concrete implementation (so your XML doesn't have annoying
 * type indication syntax).
 * 
 * The concrete class would be declared as the {@link XmlAdapter} on a field to indicate it will
 * transform the field optionally. The default constructor would indicate it's default preference
 * passing it to {@link #OptionalXmlAdapter(Boolean)}. The concrete class should also be a
 * {@link Singleton} and have a constructor that accepts either {@link Show} or {@link Hide} so that
 * the overriding behavior happens when in an injectable container. The subclass needs to pass it on
 * to the appropriate constructor {@link #OptionalXmlAdapter(Show)} or
 * {@link #OptionalXmlAdapter(Hide)} which will do as asked.
 * 
 * @param <X>
 *            the jaxb capable object
 * @param <T>
 *            the original object to be translated into X
 * @author aroller
 * 
 */
abstract public class OptionalXmlAdapter<X, T>
		extends XmlAdapter<X, T> {
	final private boolean VISIBILITY_DEFAULT = false;
	final private boolean show;

	public enum Visibility {
		SHOW, HIDE
	};

	/**
	 * @param show
	 */
	public OptionalXmlAdapter(Boolean show) {
		this.show = show;
	}

	public OptionalXmlAdapter(@Nullable Visibility visibility) {
		if (visibility == null) {
			this.show = VISIBILITY_DEFAULT;
		} else {
			switch (visibility) {
				case SHOW:
					this.show = true;
					break;
				case HIDE:
					this.show = false;
					break;
				default:
					this.show = VISIBILITY_DEFAULT;
					break;
			}
		}
	}

	/**
	 * Any call to this constructor indicates true.
	 * */
	public OptionalXmlAdapter(Show show) {
		this(true);
	}

	public OptionalXmlAdapter(Hide hide) {
		this(false);
	}

	public OptionalXmlAdapter() {
		this(false);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	final public T unmarshal(X v) throws Exception {
		return (show) ? showUnmarshal(v) : null;
	}

	/**
	 * Optional implementation that converts the xml value into a pojo. Often not possible so null
	 * is implemented by default.
	 * 
	 * @param v
	 * @return the pojo converted from the xml value.
	 */
	protected T showUnmarshal(X v) throws Exception {
		throw new NotImplementedException("showUnmarshal must be overridden if it is to be supported for " + getClass());
	}

	/**
	 * Only called if show is true the implementor should convert the type into an xml ready pojo.
	 * 
	 * @param v
	 * @return
	 * @throws Exception
	 */
	abstract protected X showMarshal(T v) throws Exception;

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	final public X marshal(T v) throws Exception {
		return (show) ? showMarshal(v) : null;
	}

	/**
	 * A flag class that is a singleton to be used by implementations to indicate this adapter
	 * should be shown. The subclass should simply pass it to this class's constructor to indicate
	 * the preference to show.
	 * 
	 * @author aroller
	 * 
	 */
	@Singleton
	public static class Show {
	}

	/**
	 * 
	 * @author aroller
	 * 
	 */
	@Singleton
	public static class Hide {
	}
}
