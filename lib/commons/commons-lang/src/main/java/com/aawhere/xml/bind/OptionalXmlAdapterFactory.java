/**
 * 
 */
package com.aawhere.xml.bind;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.inject.Singleton;

/**
 * @author aroller
 * @deprecated use FieldXmlAdapterService pattern so registration won't be needed, field syntax is
 *             respected and batches can be loaded TN-730
 */
@Singleton
public class OptionalXmlAdapterFactory {

	public static final String ALWAYS = "always";

	private Multimap<String, XmlAdapter<?, ?>> adapters = HashMultimap.create();
	private HashMap<XmlAdapter<?, ?>, Class<? extends XmlAdapter>> types = new HashMap<>();

	/**
	 * Used to construct all instances of OptionalXmlAdapterFactory.
	 */
	public static class Builder
			extends ObjectBuilder<OptionalXmlAdapterFactory> {

		public Builder() {
			super(new OptionalXmlAdapterFactory());
		}

		public Builder register(String optionKey, XmlAdapter<?, ?> adapter, Class<? extends XmlAdapter> type) {
			building.types.put(adapter, type);
			put(optionKey, adapter);
			return this;
		}

		public Builder register(String optionKey, XmlAdapter<?, ?> adapter) {
			put(optionKey, adapter);
			final Class<? extends XmlAdapter> adapterType = adapter.getClass();
			building.types.put(adapter, adapterType);
			return this;
		}

		/**
		 * @param optionKey
		 * @param adapter
		 */
		private void put(String optionKey, XmlAdapter<?, ?> adapter) {
			building.adapters.put(optionKey, adapter);
		}

		@Override
		public OptionalXmlAdapterFactory build() {
			OptionalXmlAdapterFactory built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct OptionalXmlAdapterFactory */
	private OptionalXmlAdapterFactory() {
	}

	public static Builder create() {
		return new Builder();
	}

	public List<XmlAdapter<?, ?>> adapter(@Nullable Iterable<String> parameters) {
		HashSet<String> requestedOptions = Sets.newHashSet(ALWAYS);

		if (parameters != null) {
			Iterables.addAll(requestedOptions, parameters);
		}
		ArrayList<XmlAdapter<?, ?>> results = new ArrayList<>();
		for (String option : requestedOptions) {
			Collection<XmlAdapter<?, ?>> optionalXmlAdapter = adapters.get(option);
			if (optionalXmlAdapter != null) {
				results.addAll(optionalXmlAdapter);
			}
		}
		return results;
	}

	/**
	 * @param optionalXmlAdapter
	 * @return
	 */
	public <A extends XmlAdapter> Class<A> type(A optionalXmlAdapter) {
		return (Class<A>) types.get(optionalXmlAdapter);
	}

}
