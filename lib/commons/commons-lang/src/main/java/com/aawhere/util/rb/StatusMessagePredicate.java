/**
 * 
 */
package com.aawhere.util.rb;

import javax.annotation.Nullable;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Predicate;

/**
 * @author aroller
 * 
 */
public class StatusMessagePredicate
		implements Predicate<StatusMessage> {

	/**
	 * Used to construct all instances of StatusMessagePredicate.
	 */
	public static class Builder
			extends ObjectBuilder<StatusMessagePredicate> {

		public Builder() {
			super(new StatusMessagePredicate());
		}

		public Builder setMessageStatus(MessageStatus status) {
			building.status = status;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private MessageStatus status;

	/** Use {@link Builder} to construct StatusMessagePredicate */
	private StatusMessagePredicate() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(@Nullable StatusMessage input) {
		return this.status.equals(input.status);
	}

}
