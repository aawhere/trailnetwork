/**
 * 
 */
package com.aawhere.ws.rs;

/**
 * When building URIs for Web Resources and clients that use web resources the URI must be built
 * using these common, application independent characters. They must be String constants so that
 * annotations may build them at compile time so using a smart object to piece everything together
 * is not possible.
 * 
 * 
 * @author aroller
 * 
 */
public class UriConstants {

	public static final String SLASH = "/";
	public static final String PARAM_BEGIN = "{";
	public static final String PARAM_END = "}";
	/** used to separate the suffix of a filename. example something.txt */
	public static final String SUFFIX_SEPARATOR = ".";

}
