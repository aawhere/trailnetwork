package com.aawhere.collections;

import java.io.Serializable;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

public class PredicatesExtended {

	public static <T> Function<Iterable<T>, Iterable<T>> resultFilteringFunction(Predicate<T> filter) {
		return new ResultFilteringFunction<T>(filter);
	}

	/**
	 * Filters the results of the function allowing a composition of a predicate to a function when
	 * the function returns an iterable. Maybe this isn't necessary and concat or compose can do it.
	 * 
	 * @author aroller
	 * 
	 * @param <T>
	 */
	private static class ResultFilteringFunction<T>
			implements Function<Iterable<T>, Iterable<T>>, Serializable {

		private static final long serialVersionUID = 7802825263800069462L;

		final private Predicate<T> filter;

		public ResultFilteringFunction(Predicate<T> filter) {
			this.filter = filter;
		}

		@Override
		public Iterable<T> apply(Iterable<T> input) {
			return (input != null) ? Iterables.filter(input, filter) : null;
		}

	}
}
