/**
 *
 */
package com.aawhere.lang;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import com.googlecode.objectify.annotation.Ignore;

/**
 * Simple interface to indicate an object is not mutable. Of course an object may be mutable during
 * construction so this interface works with the ObjectBuilder to ensure mutation occurs only during
 * building.
 * 
 * Implementors should call the validate state at various accessors to ensure accessing is not
 * happening during mutation.
 * 
 * Although this should be an interface to support base objects we don't control, interfaces have
 * public methods which isn't desirable to offer the validate method beyond the implmentor. We can
 * create a composition interface should we find the need.
 * 
 * @author roller
 * 
 */
@XmlTransient
public class NonMutable {

	/**
	 * Used for state validation to ensure building is completed before accessing.
	 * 
	 * @see ObjectBuilder#validateState()
	 * */
	@XmlTransient
	@Transient
	@Ignore
	boolean mutating = false;

	/**
	 * Used to ensure mutation is not currently happening. Implementors should call this method at
	 * various accessor methods to ensure proper building.
	 */
	protected void validateNotMutable() {
		if (this.mutating) {
			throw new IllegalStateException("build() must be called before access");
		}
	}

	protected Boolean isMutating() {
		return mutating;
	}
}
