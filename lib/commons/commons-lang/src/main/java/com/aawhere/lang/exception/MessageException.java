/**
 * 
 */
package com.aawhere.lang.exception;

import com.aawhere.net.HttpStatusCode;
import com.aawhere.net.StatusCodeContainer;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;

/**
 * A top level indicator both {@link BaseException} and {@link BaseRuntimeException} to identify
 * themselves as system friendly providing common methods.
 * 
 * They can't inherit from each other because of dual inheritance or {@link Exception} and
 * {@link RuntimeException}.
 * 
 * @author aroller
 * 
 */
public interface MessageException
		extends StatusCodeContainer {

	/**
	 * The standard method on all exceptions, but this should provide messages capable of being
	 * delivered to the end user able to be localized.
	 * 
	 * @return
	 */
	public String getMessage();

	/**
	 * A Constant used to categorize the type of error this represents.
	 * 
	 * This is typically the name of the enum implementation of Message or simply the
	 * {@link Message#toString()} result.
	 * 
	 * @return
	 */
	public String getErrorCode();

	/**
	 * Provides the status code used for Web API reporting.
	 * 
	 * @return
	 */
	public HttpStatusCode getStatusCode();

	/**
	 * The Exception provides the Message with all of it's parameters ready to be provided to an end
	 * user.
	 * 
	 * @return
	 */
	public CompleteMessage getCompleteMessage();
}
