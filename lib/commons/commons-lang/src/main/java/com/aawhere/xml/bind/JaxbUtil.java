/**
 *
 */
package com.aawhere.xml.bind;

import java.util.Set;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.reflections.Reflections;

/**
 * @author brian
 * 
 */
public class JaxbUtil {

	/**
	 * Get all Jaxb classes on the build path (these are created at build time for each artifact.
	 * 
	 * @return
	 */
	public static Class<?>[] getJaxBClasses(Reflections reflections) {
		Set<Class<?>> classes = reflections.getTypesAnnotatedWith(XmlType.class, true);
		classes.addAll(reflections.getTypesAnnotatedWith(XmlRootElement.class, true));
		classes.addAll(reflections.getTypesAnnotatedWith(XmlAccessorType.class, true));
		classes.removeAll(reflections.getTypesAnnotatedWith(XmlTransient.class));
		return classes.toArray(new Class<?>[classes.size()]);
	}

	/**
	 * Given an object this will return the class that is to be bound. If it's a {@link JAXBElement}
	 * then {@link JAXBElement#getDeclaredType()} is returned.
	 * 
	 * @param element
	 * @return
	 */
	public static Class<?> boundTypeFrom(Object element) {
		Class<?> boundType;
		if (element instanceof JAXBElement) {
			boundType = ((JAXBElement<?>) element).getDeclaredType();

		} else {
			boundType = element.getClass();
		}
		return boundType;
	}

}
