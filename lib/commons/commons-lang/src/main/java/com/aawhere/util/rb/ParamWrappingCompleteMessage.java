package com.aawhere.util.rb;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.lang.string.StringUtilsExtended;
import com.aawhere.text.format.custom.CustomFormat;
import com.google.common.base.Function;
import com.google.common.collect.Sets;

/**
 * Builds a standard {@link CompleteMessage} by overriding the
 * {@link com.aawhere.util.rb.CompleteMessage.Builder} adding
 * {@link #paramWrapped(ParamKey, Object)} which will add the given parameter, but will handle the
 * open/close syntax that allows targeted replacment of a client.
 * 
 * Although this is actually a builder, naming it as CompleteMessage provides familar syntax of
 * {@link ParamWrappingCompleteMessage#create(Message)}{@link #param(ParamKey, Object)}
 * {@link #build()} so dropped Builder from the name.
 * 
 * {@link #param(ParamKey, Object)} has been overridden so you'll receive this builder instead of
 * the parent which would then make the {@link #paramWrapped(ParamKey, Object)} unavailable.
 * 
 * See unit tests for example.
 * 
 * @author aroller
 * 
 */
public class ParamWrappingCompleteMessage
		extends CompleteMessage.Builder {
	/**
	 * transforms the open tag into the desired format given the param key.
	 */
	private Function<ParamKey, String> openTagFunction = openTagFunction();
	/**
	 * Transforms the close tag into the desired format given the param key.
	 * 
	 */
	private Function<ParamKey, String> closeTagFunction = closeTagFunction();
	/**
	 * The original message which must be wrapped to provide the {@link #paramKeys}
	 * 
	 */
	final private Message givenMessage;
	/**
	 * These keep track of what has been used because we need to provide these keys in addition to
	 * those statically declared.
	 * 
	 */
	private HashSet<ParamKey> paramKeys = Sets.newHashSet();

	public ParamWrappingCompleteMessage(Message message) {
		super();
		this.givenMessage = message;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ParamWrappingCompleteMessage param(ParamKey paramKey, Object paramValue) {
		return super.param(paramKey, paramValue);
	}

	public ParamWrappingCompleteMessage paramWrapped(ParamKey paramKey, Object paramValue) {
		ParamKey openKey = open(paramKey);
		ParamKey closeKey = close();
		param(openKey, openTagFunction.apply(paramKey));

		if (!paramKeys.contains(closeKey)) {
			param(closeKey, closeTagFunction.apply(paramKey));
		}
		param(paramKey, paramValue);
		// we must add the dynamic keys to the static declared
		paramKeys.add(closeKey);
		paramKeys.add(openKey);
		return this;

	}

	@Override
	public CompleteMessage build() {

		ParamKey[] givenKeys = givenMessage.getParameterKeys();
		if (givenKeys != null) {
			paramKeys.addAll(Arrays.asList(givenKeys));
		}
		ParamKey[] combinedParamKeys = paramKeys.toArray(new ParamKey[paramKeys.size()]);
		// combine the given message param keys with those we generated
		Message dynamicMessage = new WrappedEnumMessage(givenMessage, combinedParamKeys);
		setMessage(dynamicMessage);
		return super.build();
	}

	private enum WrapperOpenTagFunction implements Function<ParamKey, String> {
		INSTANCE;
		@Override
		@Nonnull
		public String apply(@Nonnull ParamKey paramKey) {
			return formatWrapperOpenTag(paramKey);
		}

		@Override
		public String toString() {
			return "WrapperOpenTagFunction";
		}
	}

	static Function<ParamKey, String> openTagFunction() {
		return WrapperOpenTagFunction.INSTANCE;
	}

	static String formatWrapperOpenTag(ParamKey key) {
		String keyString = StringUtilsExtended.constantCaseToHyphenated(key.toString());
		CompleteMessage wrapper = CompleteMessage.create(ParamWrapperMessage.WRAPPER_OPEN_SPAN_TAG)
				.addParameter(ParamWrapperMessage.Param.WRAPPER_KEY, keyString).build();
		return MessageFormatter.create().message(wrapper).locale(Locale.getDefault()).build().getFormattedMessage();
	}

	/**
	 * @return
	 */
	static String formatWrapperCloseTag() {
		return MessageFormatter.create().message(ParamWrapperMessage.WRAPPER_CLOSE_SPAN_TAG)
				.locale(Locale.getDefault()).build().getFormattedMessage();
	}

	private enum CloseTagFunction implements Function<ParamKey, String> {
		INSTANCE;

		@Override
		@Nullable
		public String apply(@Nullable ParamKey paramKey) {
			return formatWrapperCloseTag();
		}

		@Override
		public String toString() {
			return "CloseTagFunction";
		}
	}

	static Function<ParamKey, String> closeTagFunction() {
		return CloseTagFunction.INSTANCE;
	}

	/**
	 * Provides a standardized key wrapping capabilities within the message.
	 * 
	 * @see CompleteMessage.Builder#paramWrapped(ParamKey, Object)
	 * 
	 * @param key
	 * @return
	 */
	static ParamKey open(ParamKey key) {
		return new StringParamKey(new StringBuilder().append(key.toString()).append(ParamWrapperMessage.Param._XX)
				.toString());
	}

	static ParamKey close() {
		return ParamWrapperMessage.Param.XX;
	}

	static class StringParamKey
			implements ParamKey {

		private String name;

		public StringParamKey(String name) {
			this.name = name;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return null;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			StringParamKey other = (StringParamKey) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return name;
		}

	}

	public static ParamWrappingCompleteMessage create(Message message) {
		return new ParamWrappingCompleteMessage(message);

	}
}
