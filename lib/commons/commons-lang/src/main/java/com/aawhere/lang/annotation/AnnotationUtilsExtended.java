/**
 *
 */
package com.aawhere.lang.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.ReflectionUtils;
import org.springframework.core.annotation.AnnotationUtils;

import com.google.common.base.Predicate;

/**
 * Utility providing common function for {@link Annotation}s.
 * 
 * @author Aaron Roller
 * 
 */
public class AnnotationUtilsExtended {

	private static AnnotationUtilsExtended instance;

	/**
	 * Used to retrieve the only instance of AnnotationUtilsExtended.
	 */
	public static AnnotationUtilsExtended getInstance() {

		if (instance == null) {
			instance = new AnnotationUtilsExtended();
		}
		return instance;

	}// end Builder

	/** Use {@link #getInstance()} to get AnnotationUtilsExtended */
	private AnnotationUtilsExtended() {
	}

	/**
	 * Finds all the methods with the given annotation and returns each method mapped to it's
	 * annotation.
	 * 
	 * NOTE:One limitation is that an overriding method will be given in duplicate. The probably
	 * ideal is to have only the overriding children's method.
	 */
	public <A extends Annotation> Map<Method, A> findMethods(Class<A> annotationClass, Class<?> annotatedClass) {

		HashMap<Method, A> result = new HashMap<Method, A>();
		final Predicate<AnnotatedElement> withAnnotation = ReflectionUtils.withAnnotation(annotationClass);
		@SuppressWarnings("unchecked")
		Set<Method> allMethods = ReflectionUtils.getAllMethods(annotatedClass, withAnnotation);

		for (Method method : allMethods) {

			result.put(method, AnnotationUtils.findAnnotation(method, annotationClass));
		}
		return result;
	}

	/**
	 * Finds all the fields in the class and super classes and returns the Field with the annotation
	 * instance as the key
	 */
	public <A extends Annotation> Map<Field, A> findFields(Class<A> annotationClass, Class<?> annotatedClass) {
		@SuppressWarnings("unchecked")
		Set<Field> allFields = ReflectionUtils.getAllFields(annotatedClass,
															ReflectionUtils.withAnnotation(annotationClass));
		Map<Field, A> results = new HashMap<Field, A>();
		for (Field field : allFields) {
			A annotation = field.getAnnotation(annotationClass);
			results.put(field, annotation);
		}
		return results;
	}

	public <A extends Annotation> Map<Member, A> findMembers(Class<A> annotationClass, Class<?> annotatedClass) {
		Map<Field, A> findFields = findFields(annotationClass, annotatedClass);
		Map<Method, A> findMethods = findMethods(annotationClass, annotatedClass);
		Map<Member, A> members = new HashMap<Member, A>();
		members.putAll(findFields);
		members.putAll(findMethods);
		return members;
	}

	public <A extends Annotation> Boolean hasAnnoation(Class<?> type, Class<A> annotationType) {
		A annotation = AnnotationUtils.findAnnotation(type, annotationType);
		return (annotation != null);
	}

	/**
	 * Determines if the {@link Member} is annotated with the {@link Annotation} provided.
	 * 
	 * @param member
	 * @param annotationType
	 * @return
	 */
	public static Boolean isAnnotatedWith(Member member, Class<? extends Annotation> annotationType) {
		return getAnnotation(member, annotationType) != null;
	}

	/**
	 * Provides the annotation of the given type if declared on the member. Null is returned if not.
	 * 
	 * @param member
	 * @param annotationType
	 * @return
	 */
	public static <A extends Annotation> A getAnnotation(Member member, Class<A> annotationType) {
		A result;
		// Too bad getAnnotation isn't in an interface that is common to Field and Method ...
		if (member instanceof java.lang.reflect.Field) {
			result = ((java.lang.reflect.Field) member).getAnnotation(annotationType);
		} else if (member instanceof java.lang.reflect.Method) {
			result = ((java.lang.reflect.Method) member).getAnnotation(annotationType);
		} else {
			result = null;
		}
		return result;
	}
}
