/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 4, 2010
commons-lang : com.aawhere.collections.apache.functor.BinaryNumberPredicate.java
 */
package com.aawhere.collections.apache.predicate;

/**
 * Handy for any binary predicates using numbers.
 * 
 * @author roller
 * 
 */
public abstract class BinaryNumberPredicate
		extends BinaryPredicate<Number> {

	/**
	 * @param right
	 */
	public BinaryNumberPredicate(Number right) {
		super(right);
	}

}
