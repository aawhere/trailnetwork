/**
 * 
 */
package com.aawhere.math.stats;

import org.apache.commons.math.stat.descriptive.summary.Sum;

/**
 * @author aroller
 * 
 */
public abstract class FilteredSum
		extends Sum {

	private static final long serialVersionUID = -498283487479537363L;

	/**
	 * 
	 */
	public FilteredSum() {
		// intialize the sum to be zero
		super.increment(0);
	}

	/**
	 * @param original
	 */
	public FilteredSum(Sum original) {
		super(original);
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.math.stat.descriptive.summary.Sum#increment(double)
	 */
	@Override
	public void increment(double d) {
		if (passedFilter(d)) {
			super.increment(d);
		}
	}

	/**
	 * @param d
	 * @return
	 */
	abstract protected boolean passedFilter(double d);

	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.math.stat.descriptive.summary.Sum#evaluate(double[], double[])
	 */
	@Override
	public double evaluate(double[] values, double[] weights) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.apache.commons.math.stat.descriptive.AbstractStorelessUnivariateStatistic#evaluate(double
	 * [])
	 */
	@Override
	public double evaluate(double[] values) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.math.stat.descriptive.summary.Sum#evaluate(double[], double[], int,
	 * int)
	 */
	@Override
	public double evaluate(double[] values, double[] weights, int begin, int length) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.math.stat.descriptive.summary.Sum#evaluate(double[], int, int)
	 */
	@Override
	public double evaluate(double[] values, int begin, int length) {
		throw new UnsupportedOperationException();
	}

}
