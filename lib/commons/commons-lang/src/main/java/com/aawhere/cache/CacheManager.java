/**
 *
 */
package com.aawhere.cache;

import java.util.HashMap;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;

/**
 * Provides similar function to {@link net.sf.jsr107cache.CacheManager}, but encapsulates the
 * details common to the system.
 * 
 * Two know providers of the Cache library:
 * 
 * http://ehcache.org/documentation/jsr107.html Just include
 * http://mvnrepository.com/artifact/net.sf.ehcache/ehcache-jcache
 * 
 * or if using the Google App Engine, Memcache will be provided.
 * 
 * http://code.google.com/appengine/docs/java/memcache/usingjcache.html
 * 
 * @author roller
 * 
 */
public class CacheManager {

	private static CacheManager instance;

	private CacheManager() {
	}

	public Cache getCache(Class<?> keeperOfCache) {
		try {
			String cacheName = keeperOfCache.getName();

			net.sf.jsr107cache.CacheManager manager = net.sf.jsr107cache.CacheManager.getInstance();
			Cache cache = manager.getCache(cacheName);
			if (cache == null) {
				CacheFactory cacheFactory = manager.getCacheFactory();
				HashMap<String, String> env = new HashMap<String, String>();

				// this covers the ehcache implementation
				env.put("name", "test");
				env.put("maxElementsInMemory", "10");
				env.put("memoryStoreEvictionPolicy", "LFU");
				env.put("overflowToDisk", "true");
				env.put("eternal", "false");
				env.put("timeToLiveSeconds", "5");
				env.put("timeToIdleSeconds", "5");
				env.put("diskPersistent", "false");
				env.put("diskExpiryThreadIntervalSeconds", "120");
				cache = cacheFactory.createCache(env);

				manager.registerCache(cacheName, cache);
			}
			return cache;
		} catch (CacheException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return
	 */
	public static CacheManager getInstance() {
		if (instance == null) { // avoid sync penalty if we can
			synchronized (CacheManager.class) {
				if (instance == null) {
					instance = new CacheManager();
				}
			}
		}
		return instance;
	}

}
