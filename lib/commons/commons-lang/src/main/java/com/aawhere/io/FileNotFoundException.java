/**
 * 
 */
package com.aawhere.io;

import java.io.File;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Our version of {@link java.io.FileNotFoundException}.
 * 
 * @author roller
 * 
 */
@StatusCode(HttpStatusCode.NOT_FOUND)
public class FileNotFoundException
		extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3487621299199129412L;

	public FileNotFoundException(String filePath) {
		this(filePath, null);
	}

	/**
	 * 
	 */
	public FileNotFoundException(String filePath, java.io.FileNotFoundException e) {
		super(new CompleteMessage.Builder(IoMessage.FILE_NOT_FOUND).addParameter(IoMessage.Param.FILE_NAME, filePath)
				.build(), e);
	}

	/**
	 * @param file
	 * @param e
	 */
	public FileNotFoundException(File file, java.io.FileNotFoundException e) {
		this(file.getName(), e);
	}

	/**
	 * @param file
	 */
	public FileNotFoundException(File file) {
		this(file, null);
	}

}
