/**
 * 
 */
package com.aawhere.i18n;

import java.util.Locale;

/**
 * A place to define Locales needed beyond those offered in {@link Locale} constants.
 * 
 * @see <a href="http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">language codes</a>
 * @see <a href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">Country Codes</a>
 * 
 * @author roller
 * 
 */
public enum SystemLocale {

	/**
	 * Officially the demonstration language for the system. Use this language to test difficult
	 * characters. Zulu is a South African language spoken by about 10 Million people...an unlikely
	 * target audience.
	 * 
	 * http://en.wikipedia.org/wiki/Zulu_language
	 * 
	 * Google Chrome offers it as Zulu/isiZulu, Safari/Mac as isiZulu, Firefox as Zulu
	 */
	ZULU("zu");

	public final Locale locale;

	/**
	 * 
	 */
	private SystemLocale(String languageCode) {
		this.locale = new Locale(languageCode);
	}
}
