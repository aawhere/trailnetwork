/**
 * 
 */
package com.aawhere.lang.enums;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * @author roller
 * 
 */
public enum EnumMessage implements Message {

	/**
	 * Used to explain a key is missing.
	 * 
	 * @see InvalidEnumKeyException
	 */
	INVALID_KEY("{KEY} is not a registered enum: {REGISTERED_ENUMS}", Param.KEY, Param.REGISTERED_ENUMS);

	private ParamKey[] parameterKeys;
	private String message;

	private EnumMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** a map containing the key/enum pairs that are registered. */
		REGISTERED_ENUMS,
		/** The string representing the enum */
		KEY;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
