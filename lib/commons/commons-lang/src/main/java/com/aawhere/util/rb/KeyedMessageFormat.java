package com.aawhere.util.rb;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * Formats a piece of parameterized text and inserts appropriate value associated with parameter
 * found on page. Replaces any escaped characters.
 * 
 * Useful for standard replacement of messages, but replacing by named key instead of indexed
 * parameter.
 * 
 * Instead of having mystery numbers:
 * 
 * "{0} already taken.  Try {1}?"
 * 
 * You'd have:
 * 
 * "{username} already taken.  Try {suggestedUsername}?"
 * 
 * Notice the keyed message is much more readable.
 * 
 */
public class KeyedMessageFormat {

	/**
	 * Used to construct all instances of KeyedMessageFormat.
	 */
	public static class Builder
			extends ObjectBuilder<KeyedMessageFormat> {

		public Builder() {
			super(new KeyedMessageFormat());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public KeyedMessageFormat build() {

			KeyedMessageFormat built = super.build();
			built.formattedMessage = built.format();
			return built;
		}

		public Builder message(String message) {
			building.message = message;
			return this;
		}

		/**
		 * When it is expected that you will be providing some extra parameters that may not be in
		 * the search string this will allow it without failing. The default is to fail explaining
		 * which parameter is not found.
		 * 
		 * @return
		 */
		public Builder forgiveExtraParameters() {
			building.forgiveExtraParameters = true;
			return this;
		}

		/**
		 * Adds a key/value param replacement for a targeted braces within the message.
		 * 
		 * @param key
		 * @param value
		 * @return
		 */
		public Builder put(Object key, Object value) {
			building.params.put(key, value);
			return this;
		}

		public Builder putAll(Map<?, ?> params) {
			building.params.putAll(params);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull(building.message);
			boolean matched = applyStandards(building.message);
			if (!matched) {

				// yeah, seems like more REGEX, but I can't understand that snizzle! AR
				if (building.message.contains("{") && !building.message.contains("'{")) {
					throw new IllegalArgumentException(building.message + " syntax is not correct.");
				}

			}
			super.validate();
		}
	}// end Builder

	private Boolean forgiveExtraParameters = false;

	/**
	 * Same as calling new Builder().message(message);
	 * 
	 * @param message
	 * @return
	 */
	public static Builder create(String message) {
		return new Builder().message(message);
	}

	public static Builder create() {
		return new Builder();
	}

	private static final String PARAM_FINDER_REGEX = "('{2}|(?<!'))\\{(.+?)\\}";

	/**
	 * Regex captures anything inside of two curly braces that follows either zero single quotes or
	 * two single quotes Checks for the following problems:
	 * 
	 * <pre>
	 * 1) braces are not balanced 
	 * 2) contents between braces doesn't exist (braces are empty)
	 * 
	 * </pre>
	 * 
	 * 
	 * @param message
	 *            The string retrieved from the resource bundle
	 * @throws IllegalArgumentException
	 *             if any of the above problems are found
	 */
	private static boolean applyStandards(String message) {
		Pattern parameterRegex = Pattern.compile(PARAM_FINDER_REGEX);
		Matcher parameterMatcher = parameterRegex.matcher(message);
		return parameterMatcher.find();

	}

	/**
	 * Replaces all escaped braces and escaped quotes with char that was being escaped. Called after
	 * the parameters are replaced.
	 */
	private static String replaceEscapedCharacters(String source) {

		String result;
		String replacerRegex = "('('|\\{|\\}))";
		Pattern singleQuoteRegex = Pattern.compile(replacerRegex);
		Matcher singleQuoteMatcher = singleQuoteRegex.matcher(source);
		if (singleQuoteMatcher.find()) {
			result = source.replaceAll(replacerRegex, "$2");
		} else {
			result = source;
		}
		return result;
	}

	private static void validateNoParametersRemain(String message) {
		// Now that all the params provided have been used there should be no more items to replace.
		Pattern parameterRegex = Pattern.compile(PARAM_FINDER_REGEX);
		Matcher parameterMatcher = parameterRegex.matcher(message);

		while (parameterMatcher.find()) {
			throw new IllegalArgumentException(
					"the message '"
							+ message
							+ "' has unmatched parameters.  Please provide a parameter to match any Constant inside {} curly braces.");
		}

	}

	private String message;
	private String formattedMessage;
	private HashMap<Object, Object> params = new HashMap<Object, Object>();

	/** Use {@link Builder} to construct KeyedMessageFormat */
	private KeyedMessageFormat() {
	}

	/**
	 * Formats message established in constructor by replacing all parameterized text in the
	 * resource bundle which looks like {key} and replaces it with that key's value from the
	 * {@link Map} passed in
	 * 
	 * @param keysAndParams
	 *            A {@link Map} of all the {@link ParamTag} attribute "key" as keys and the body of
	 *            the ParamTag as values
	 * @return String The formatted message
	 * @throws IllegalArgumentException
	 *             if the parameter specified in the resource bundle was not found on the page
	 */
	private String format() {
		Set<Map.Entry<Object, Object>> entries = params.entrySet();

		String result = this.message;
		StringBuffer paramKey;
		for (Map.Entry<Object, Object> entry : entries) {
			paramKey = new StringBuffer();
			paramKey.append("{");
			paramKey.append(entry.getKey());
			paramKey.append("}");
			if (!forgiveExtraParameters && result.indexOf(paramKey.toString()) == -1) {
				throw new IllegalArgumentException("Unable to find given parameter " + entry.getKey() + " in message "
						+ message);
			}
			result = StringUtils.replace(result, paramKey.toString(), entry.getValue().toString());

		}

		validateNoParametersRemain(result);
		result = replaceEscapedCharacters(result);
		return result;
	}

	/**
	 * @return the formattedMessage
	 */
	public String getFormattedMessage() {
		return this.formattedMessage;
	}

	public String toString() {
		return this.formattedMessage;
	};

}
