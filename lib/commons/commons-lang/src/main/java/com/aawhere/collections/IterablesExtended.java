/**
 * 
 */
package com.aawhere.collections;

import static com.aawhere.collections.FunctionsExtended.castFunction;
import static com.google.common.collect.Iterables.transform;

import java.util.Iterator;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class IterablesExtended {

	/**
	 * Helps you get out of generic hell and places you back in generic purgatory. Some believe
	 * there is a generic heaven, but I've yet to find it.
	 * 
	 * @author aroller
	 * @param looselyType
	 * @return
	 */
	public static <T> Iterable<T> cast(Iterable<?> looselyType) {
		Function<Object, T> castFunction = castFunction();
		return transform(looselyType, castFunction);
	}

	/**
	 * Same as {@link Iterables#isEmpty(Iterable)}, but returns true for null instead of exception.
	 * 
	 * @param personIds
	 * @return
	 */
	public static Boolean isEmpty(Iterable<?> iterable) {
		return (iterable == null) ? true : Iterables.isEmpty(iterable);
	}

	/**
	 * Given a normal funciton that transforms individuals this will transform iterables of the
	 * individuals.
	 * 
	 * @param from
	 * @return
	 */
	public static <F, T> Function<Iterable<F>, Iterable<T>> compose(final Function<F, T> from) {
		return new Function<Iterable<F>, Iterable<T>>() {

			@Override
			public Iterable<T> apply(Iterable<F> input) {
				return Iterables.transform(input, from);
			}
		};
	}

	/**
	 * Delays "getting" until the iterator is called to provide lazy support (useful with Future).
	 * 
	 * It seems this may be provided in Guava, but I couldn't find it.
	 * 
	 * @param supplier
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <I extends Iterable<T>, T> I supplier(final Supplier<I> supplier) {
		return (I) new Iterable<T>() {
			@Override
			public Iterator<T> iterator() {
				return supplier.get().iterator();
			}
		};
	}

	/**
	 * When you have a function that transforms singlular to pural and you wish for that to handle
	 * multiple inputs use this function
	 * 
	 * @param from
	 * @return
	 */
	public static <F, T> Function<Iterable<F>, Iterable<T>> composeSingleToPlurals(final Function<F, Iterable<T>> from) {
		return new Function<Iterable<F>, Iterable<T>>() {

			@Override
			public Iterable<T> apply(Iterable<F> input) {
				return Iterables.concat(Iterables.transform(input, from));
			}
		};
	}

	/**
	 * Concatenation of the iterable with one or more items of the same.
	 * 
	 * @param extremes
	 * @param point
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> Iterable<T> concat(Iterable<T> iterable, T... items) {
		return Iterables.concat(Iterables.concat(iterable, Lists.newArrayList(items)));
	}

}
