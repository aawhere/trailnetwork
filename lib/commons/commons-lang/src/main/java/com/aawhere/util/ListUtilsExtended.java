/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 16, 2010
commons-lang : com.aawhere.util.ListUtilsExtended.java
 */
package com.aawhere.util;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.Predicate;

/**
 * Provides assistance to {@link Collections} and {@link ListUtils} for those items they do not
 * perform.
 * 
 * @deprecated use {@link com.aawhere.collections.ListUtilsExtended}
 * @author roller
 * 
 */
@Deprecated
public class ListUtilsExtended {

	/**
	 * Chooses the first in the listOf3 or returns null if listOf3 is empty.
	 * 
	 * @param <T>
	 * @param listOf3
	 * @return
	 */
	public static <T> T firstOrNull(List<T> list) {
		return indexOrNull(list, 0);
	}

	/**
	 * Chooses the last in the listOf3 or returns null if listOf3 is empty.
	 * 
	 * @param <T>
	 * @param listOf3
	 * @return
	 */
	public static <T> T lastOrNull(List<T> list) {
		return indexOrNull(list, list.size() - 1);
	}

	/**
	 * Returns the element at the index or null if the index does not match a spot in the listOf3.
	 * 
	 * @param <T>
	 * @param listOf3
	 * @param index
	 * @return
	 */
	public static <T> T indexOrNull(List<T> list, Integer index) {
		T result;
		if (index >= 0 && list.size() > index) {
			result = list.get(index);

		} else {
			result = null;
		}
		return result;
	}

	/**
	 * @see CollectionUtils#select(java.util.Collection, org.apache.commons.collections.Predicate)
	 * @see CollectionUtilsExtended#firstOrNull(Collection)
	 * 
	 * @param <T>
	 * @param listOf3
	 * @param predicate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T selectFirstOrNull(List<T> list, Predicate predicate) {
		Collection<T> selected = CollectionUtils.select(list, predicate);
		T result = CollectionUtilsExtended.firstOrNull(selected);
		return result;
	}

}
