/**
 * 
 */
package com.aawhere.util.rb;

import javax.annotation.Nullable;

import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.enums.EnumFactory;
import com.aawhere.lang.enums.EnumUtil;
import com.aawhere.lang.enums.EnumWithDefault;

/**
 * A central place to be aware of all enumerated {@link Message}s participating in the system.
 * 
 * Messages that are not enumerated are essentially ignored by this factory.
 * 
 * This provides a key representation of a message that can be used to reconstruct an Enum Message
 * instance.
 * 
 * @author roller
 * 
 */
public class MessageFactory
		extends EnumFactory<Message> {

	private static MessageFactory instance = (MessageFactory) new Builder().setEnumInterfaceType(Message.class).build();

	/** Use {@link Builder} to construct MessageFactory */
	private MessageFactory() {
	}

	/**
	 * Used to construct all instances of MessageFactory.
	 */
	private static class Builder
			extends EnumFactory.Builder<Message> {

		private Builder() {
			super(new MessageFactory());
		}

	}// end Builder

	/**
	 * The single instance to be used system wide to manage Messages.
	 * 
	 * @return the instance
	 */
	public static MessageFactory getInstance() {
		return instance;
	}

	/**
	 * Given possibly a message key and a known value this will return either the enumeration
	 * matching the key or a {@link StringMessage} containing the known value or null if both are
	 * null;
	 * 
	 * @param key
	 *            the Enumeration key from {@link #keyForEnum(Enum)} or null
	 * @param knownValue
	 *            the actual message which will be used if the key is not provided
	 * @return The enumeration matching the key or {@link StringMessage} containing the knownvalue
	 *         or null if both params are null
	 */
	@Nullable
	public Message bestMessage(@Nullable String key, @Nullable String knownValue) {
		Message result;
		if (key == null) {
			if (knownValue == null) {
				result = null;
			} else {
				result = new StringMessage(knownValue);
			}
		} else {
			if (keyIsRecognized(key)) {
				result = (Message) enumFor(key);

			} else {
				if (knownValue != null) {
					result = new StringMessage(knownValue);
				} else {
					result = null;
				}
			}
		}
		return result;
	}

	/**
	 * Given the class of Enum that implements {@link Message} this will return the enum value
	 * matching the given String value or the default if it is an {@link EnumWithDefault} or an
	 * {@link InvalidChoiceException} otherwise.
	 * 
	 * @param enumClass
	 * @param enumValue
	 * @return
	 */
	public static <E extends Enum<E>> Message valueOf(Class<E> enumClass, String enumValue) {
		return (Message) EnumUtil.valueOfStrict(enumClass, enumValue);
	}
}
