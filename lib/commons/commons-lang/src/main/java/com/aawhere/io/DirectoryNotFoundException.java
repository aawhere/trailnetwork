/**
 * 
 */
package com.aawhere.io;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Thrown when a directory is provides that returns false for {@link Directory#exists()}.
 * 
 * @author roller
 * 
 */
@StatusCode(HttpStatusCode.NOT_FOUND)
public class DirectoryNotFoundException
		extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1492644772304135920L;

	public DirectoryNotFoundException(Directory directory) {
		super(new CompleteMessage.Builder().setMessage(IoMessage.DIRECTORY_NOT_FOUND)
				.addParameter(IoMessage.Param.FILE_NAME, directory.getAbsolutePath()).build());
	}

}
