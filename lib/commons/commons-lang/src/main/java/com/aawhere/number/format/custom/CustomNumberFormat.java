/**
 *
 */
package com.aawhere.number.format.custom;

/**
 * Formats objects in the system based on the locale. indicates the type of formats each instance
 * handles.
 * 
 * TODO: Why is this here? Why not {@link com.aawhere.text.format.custom.CustomNumberFormat}
 * 
 * @author roller
 * 
 */
public interface CustomNumberFormat<T extends Number> {

	/**
	 * The method called to transform an object into the specific format.
	 * 
	 * @param object
	 * @return
	 */
	public T format(T object);

	Class<? super T> handlesType();
}