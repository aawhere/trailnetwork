/**
 * 
 */
package com.aawhere.lang.string.predicate;

import org.apache.commons.lang3.StringUtils;

/**
 * @author roller
 * 
 */
final public class DigitPredicate
		extends CharSequencePredicate {

	private static DigitPredicate instance;

	/**
	 * 
	 */
	private DigitPredicate() {
	}

	public static DigitPredicate getInstance() {
		if (instance == null) {
			instance = new DigitPredicate();
		}
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.predicate.CharSequencePredicate#evaluate(java
	 * .lang.CharSequence)
	 */
	@Override
	public boolean evaluate(CharSequence target) {
		return StringUtils.isNumeric(target);
	}

}
