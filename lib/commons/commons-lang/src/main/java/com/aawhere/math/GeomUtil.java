/**
 *
 */
package com.aawhere.math;

/**
 * Utilities for doing simple geometric manipulations.
 * 
 * @author Brian Chapman
 * 
 */
public class GeomUtil {

	private static final double FULL_CIRCLE_RADIANS = 2 * Math.PI;

	public static double circumferenceToRadius(double circumference) {
		return circumference / (FULL_CIRCLE_RADIANS);
	}

}
