/**
 * 
 */
package com.aawhere.collections;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Super simple function transforms the given iterator into the first element or null if no element
 * is found.
 * 
 * @author aroller
 * 
 */
public class FirstFunction<T>
		implements Function<Iterable<T>, T> {

	public static <T> FirstFunction<T> build() {
		return new FirstFunction<>();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public T apply(Iterable<T> input) {
		if (input == null) {
			return null;
		}
		return Iterables.getFirst(input, null);
	}

}
