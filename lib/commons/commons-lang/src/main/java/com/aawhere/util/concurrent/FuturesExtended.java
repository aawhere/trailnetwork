package com.aawhere.util.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.google.common.base.Supplier;

public class FuturesExtended {

	public static <T> Supplier<T> supplier(final Future<T> future) {
		return new Supplier<T>() {

			@Override
			public T get() {
				try {
					return future.get();
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					throw new RuntimeException(e);
				}
			}
		};
	}
}
