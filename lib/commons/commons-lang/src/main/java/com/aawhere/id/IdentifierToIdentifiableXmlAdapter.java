/**
 * 
 */
package com.aawhere.id;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
abstract public class IdentifierToIdentifiableXmlAdapter<E extends Identifiable<I>, I extends Identifier<?, E>>
		extends XmlAdapter<E, I> {

	final private Function<I, E> provider;

	/**
	 * 
	 */
	public IdentifierToIdentifiableXmlAdapter() {
		this.provider = null;
	}

	public IdentifierToIdentifiableXmlAdapter(Function<I, E> provider) {
		this.provider = provider;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public I unmarshal(E entity) throws Exception {
		return (entity == null) ? null : entity.getId();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public E marshal(I id) throws Exception {
		return (getProvider() != null) ? getProvider().apply(id) : null;
	}

	/**
	 * @return the provider
	 */
	protected Function<I, E> getProvider() {
		return this.provider;
	}
}
