/**
 *
 */
package com.aawhere.ws;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.ExceptionUtil;
import com.aawhere.lang.exception.MessageException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.CompleteMessageXmlAdapter;
import com.aawhere.xml.XmlNamespace;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Simple value object to transport exceptions packaged for the Web API.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@ApiModel(description = "Reports error details when status is not 2XX")
public class ErrorResponse
		implements Serializable {

	@XmlAttribute
	@ApiModelProperty(value = "The HTTP Status Code: http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
			required = true)
	private Integer status;

	@XmlElement
	@ApiModelProperty(value = "Keyword describing the status", required = true)
	private HttpStatusCode statusCode;

	@XmlElement
	@ApiModelProperty(value = "Further explanation of the status code", required = true)
	private String statusExplanationUrl;

	@XmlElement
	@ApiModelProperty(value = "The formatted message ready to display", required = true)
	private String message;

	@XmlElement
	@ApiModelProperty(value = CompleteMessageXmlAdapter.Doc.DESCRIPTION, required = false,
			dataType = CompleteMessageXmlAdapter.Doc.DATA_TYPE)
	private CompleteMessage completeMessage;

	/**
	 * Used to construct all instances of ErrorResponse.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<ErrorResponse> {

		private Exception exception;
		private Logger logger = LoggerFactory.getLogger(getClass());

		public Builder() {
			super(new ErrorResponse());
		}

		public Builder exception(Exception exception) {
			this.exception = exception;
			return this;
		}

		public Builder statusCode(HttpStatusCode statusCode) {
			building.statusCode = statusCode;
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			if (building.statusCode == null) {
				Assertion.exceptions().notNull("exception", this.exception);
			}
		}

		@Override
		public ErrorResponse build() {
			ErrorResponse built = super.build();
			if (exception != null) {
				built.statusCode = ExceptionUtil.responseCode(exception);
				built.message = exception.getMessage();
				if (exception instanceof MessageException) {
					built.completeMessage = ((MessageException) exception).getCompleteMessage();
				}
			}
			built.status = built.statusCode.value;
			built.statusExplanationUrl = "http://httpstatusdogs.com/" + built.status;

			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct ErrorResponse */
	private ErrorResponse() {
	}

	/**
	 * @return the statusCode
	 */
	public HttpStatusCode statusCode() {
		return this.statusCode;
	}

	/**
	 * @return the message
	 */
	public String message() {
		return this.message;
	}

	/**
	 * @return the completeMessage
	 */
	public CompleteMessage completeMessage() {
		return this.completeMessage;
	}

	/**
	 * @return the statusExplanationUrl
	 */
	public String statusExplanationUrl() {
		return this.statusExplanationUrl;
	}

	private static final long serialVersionUID = -2562229949848771484L;
}
