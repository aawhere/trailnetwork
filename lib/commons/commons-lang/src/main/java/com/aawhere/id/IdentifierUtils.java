/**
 *
 */
package com.aawhere.id;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.collections.Index;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.number.NumberUtilsExtended;
import com.aawhere.lang.reflect.GenericUtils;
import com.aawhere.lang.string.FromString;
import com.aawhere.lang.string.StringFormatException;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;

/**
 * @author roller
 * 
 */
public class IdentifierUtils {

	/**
	 * The dilimeter that goes between ids. This is public to support annotation documentation.
	 * Otherwise you should use the construction methods available on this class, such as
	 * {@link #createCompositeIdValue(String...)}.
	 * 
	 */
	public static final char COMPOSITE_ID_SEPARATOR = '-';
	public static final String COMPOSITE_ID_SEPARATOR_STRING = String.valueOf(COMPOSITE_ID_SEPARATOR);
	private static final char MULTIPLE_IDS_DILIMETER = ',';

	/**
	 * Provides corresponding {@link Identifier}s from a {@link Collection} of {@link Identifiable}
	 * s.
	 * 
	 * @param identifiables
	 *            provides the id
	 * @return Set ids from given identifiables
	 */
	public static <T extends Identifiable<I>, I extends Identifier<?, ?>> Set<I> idsFrom(Collection<T> identifiables) {
		HashSet<I> ids = new HashSet<I>(identifiables.size());
		for (T identifiable : identifiables) {
			ids.add(identifiable.getId());
		}
		return ids;
	}

	/** Simple transformer for getting the key. */
	public static <K extends Identifier<?, E>, E extends KeyedEntity<K>> Function<E, K> keyFunction() {
		return new Function<E, K>() {
			@Override
			public K apply(E input) {
				return input.getKey();
			}
		};
	}

	public static <K extends Identifier<?, E>, E extends KeyedEntity<K>> Iterable<K> keys(Iterable<E> entities) {
		Function<E, K> keyFunction = keyFunction();
		return Iterables.transform(entities, keyFunction);
	}

	/**
	 * Provides an iterable of ids from the iterable of those things it identifies. Provides no
	 * guarantee of uniqueness.
	 * 
	 * @param identifiables
	 * @return
	 */
	@Nonnull
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> Iterable<I> ids(
			@Nullable Iterable<T> identifiables) {
		if (identifiables == null) {
			return null;
		}
		Function<T, I> function = idFunction();
		return Iterables.transform(identifiables, function);
	}

	@SafeVarargs
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> Iterable<I> ids(T... identifiables) {
		final ArrayList<T> list = Lists.newArrayList(identifiables);
		return ids(list);
	}

	/**
	 * Splits out the {@link Identifiable} into a map keyed with it's {@link Identifier}.
	 * 
	 * @param identifiables
	 * @return
	 */
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> Map<I, T> map(Iterable<T> identifiables) {
		Function<T, I> idFunction = idFunction();
		return Maps.uniqueIndex(identifiables, idFunction);
	}

	/**
	 * Given the collection of Identifiables this will provide a function that serves up submaps for
	 * the ids given if the ids are found in the map.
	 * http://stackoverflow.com/questions/5719833/get-
	 * all-values-from-a-map-for-some-keys-in-java-guava
	 * 
	 * @param identifiables
	 * @return
	 */
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> Function<Iterable<I>, Map<I, T>> mapFunction(
			final Iterable<T> identifiables) {
		Map<I, T> map = map(identifiables);
		final Function<I, T> forMap = Functions.forMap(map);
		final Predicate<I> in = Predicates.in(map.keySet());
		return new Function<Iterable<I>, Map<I, T>>() {

			@Override
			public Map<I, T> apply(Iterable<I> input) {
				return map(Iterables.transform(Iterables.filter(input, in), forMap));
			}
		};

	}

	/**
	 * Provide the choices of Identifiables and this will return the subset of those identifiables
	 * that match the identifiers given to the Function.
	 * 
	 * @param identifiables
	 * @return
	 */
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> Function<Iterable<I>, Iterable<T>>
			identifiablesFunction(final Iterable<T> identifiables) {
		final Function<Iterable<I>, Map<I, T>> mapFunction = mapFunction(identifiables);
		return new Function<Iterable<I>, Iterable<T>>() {

			@Override
			public Iterable<T> apply(Iterable<I> input) {
				return mapFunction.apply(input).values();
			}
		};
	}

	public static <T extends Identifiable<I>, I extends Identifier<?, T>> BiMap<T, I> biMap(Iterable<T> identifiables) {
		final Function<T, I> idFunction = idFunction();
		return HashBiMap.create(Maps.toMap(identifiables, idFunction));
	}

	private static class IdFunction<T extends Identifiable<I>, I extends Identifier<?, T>>
			implements Function<T, I>, Serializable {

		private static final long serialVersionUID = 1568939555453321550L;

		@Override
		@Nullable
		public I apply(@Nullable T entity) {
			return (entity != null) ? entity.getId() : null;
		}

		@Override
		public String toString() {
			return "IdFunction";
		}
	}

	public static <T extends Identifiable<I>, I extends Identifier<?, T>> Function<T, I> idFunction() {
		return new IdFunction<T, I>();
	}

	/**
	 * Provided a string with one or more comma separated id values as a string. Each string will be
	 * returned as its created id using the required {@link Constructor} with a single String
	 * parameter.
	 * 
	 * @param idsString
	 * @param idClass
	 * @return
	 */
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> Set<I> idsFrom(String idsString,
			Class<I> idClass) {
		String[] strings = StringUtils.split(idsString, ',');
		HashSet<I> ids = new HashSet<I>(strings.length);
		for (int i = 0; i < strings.length; i++) {
			String string = strings[i];
			I id = idFrom(string, idClass);
			ids.add(id);
		}
		return ids;
	}

	/**
	 * partner to {@link #idsFrom(String, Class)} this will return the ids in a string format.
	 * 
	 * @return
	 */
	public static String idsJoined(Iterable<? extends Identifier<?, ?>> ids) {
		return StringUtils.join(ids, MULTIPLE_IDS_DILIMETER);
	}

	/** Joins the prefix to the id in a standard way. */
	@Nonnull
	public static String join(@Nonnull String prefix, @Nonnull Identifier<?, ?> id) {
		return StringUtils.join(prefix, String.valueOf(COMPOSITE_ID_SEPARATOR), id.getValue());
	}

	/**
	 * Given a collection of {@link Identifiable}s and the {@link Identifier} in search of this will
	 * return the one or null . Multiple is not possible because that would break any rule of
	 * Identifiable/Identifier relationship.
	 * 
	 * @param identifiables
	 * @param id
	 * @return the matching identifable or null if none found
	 */
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> T filter(Iterable<T> identifiables, I id) {
		Iterable<T> filtered = Iterables.filter(identifiables, new IdentifiablePredicate<I, T>(id));
		return Iterables.getFirst(filtered, null);
	}

	/**
	 * Filters the identifiables keeping only those within the collection of identifiers to keep.
	 * Collection used instead of Iterable because of {@link Predicates#in(Collection)} requires
	 * such. Passing hashSet is best.
	 * 
	 * @param identifiables
	 * @param idsToKeep
	 * @return
	 */
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> Iterable<T> filter(Iterable<T> identifiables,
			Collection<I> idsToKeep) {
		Predicate<I> iPredicate = Predicates.in(idsToKeep);
		Function<T, I> idFunction = idFunction();
		Predicate<T> tPredicate = Predicates.compose(iPredicate, idFunction);
		return Iterables.filter(identifiables, tPredicate);

	}

	/**
	 * Inspects the type for the identifier and extracts the Kind the identifier represents.
	 */
	public static <K> Class<K> kindFrom(Class<? extends Identifier<?, K>> idType) {
		return GenericUtils.getTypeArgument(idType, Index.FIRST);

	}

	/**
	 * Retrieves the kind from the class representing a concrete type that identifies it's kind. use
	 * {@link #isKindKnown(Class)} to determine if such is even possible or you will get a
	 * RuntimeException.
	 * 
	 * @param idType
	 * @return
	 */
	public static Class<?> kindFromUnknown(Class<? extends Identifier<?, ?>> idType) {
		return GenericUtils.getTypeArgument(idType, Index.FIRST);
	}

	public static <K> Class<K> kindFromUnknown(Identifier<?, ?> id) {
		@SuppressWarnings("unchecked")
		Class<? extends Identifier<?, ?>> idType = (Class<? extends Identifier<?, ?>>) id.getClass();
		return GenericUtils.getTypeArgument(idType, Index.FIRST);
	}

	/**
	 * Provides the standard way to concanate two identifers into a single string.
	 * 
	 * @param prefix
	 * @param suffix
	 * @return
	 */
	public static String createCompositeIdValue(Identifier<?, ?> prefix, Identifier<?, ?> suffix) {
		return createCompositeIdValue(prefix.getValue(), suffix.getValue());
	}

	public static String createCompositeIdValue(Object prefix, Object suffix) {
		return new StringBuilder().append(prefix).append(COMPOSITE_ID_SEPARATOR).append(suffix).toString();
	}

	/**
	 * Given an identifier with a composite value this will split the id into two separate ids.
	 * 
	 * @param compositeId
	 * @return
	 */
	public static <I extends StringIdentifier<?>> Pair<String, String> splitCompositeId(I compositeId) {

		String[] split = splitCompositeIdAll(compositeId);
		// even count of pieces indicates odd number of characters.
		if (ArrayUtils.isEmpty(split) || NumberUtilsExtended.isOdd(split.length)) {
			throw InvalidIdentifierException.compositeFormat(compositeId, String.valueOf(COMPOSITE_ID_SEPARATOR));
		}
		// handle composite composite ids
		if (split.length > 2) {
			// rejoin the first half together and then the second half together.
			Integer middle = (split.length / 2);
			split = new String[] { StringUtils.join(ArrayUtils.subarray(split, 0, middle), COMPOSITE_ID_SEPARATOR),
					StringUtils.join(ArrayUtils.subarray(split, middle, split.length), COMPOSITE_ID_SEPARATOR) };

		}
		return Pair.of(split[0], split[1]);
	}

	/**
	 * Provides all unfiltered parts of an id split off the {@link #COMPOSITE_ID_SEPARATOR} without
	 * discretion.
	 * 
	 * @param compositeId
	 * @return
	 */
	public static <I extends StringIdentifier<?>> String[] splitCompositeIdAll(I compositeId) {
		String idValue = compositeId.getValue();
		String[] split = splitCompositeIdValue(idValue);

		return split;
	}

	/**
	 * @param idValue
	 * @return
	 */
	public static String[] splitCompositeIdValue(String idValue) {
		return StringUtils.split(idValue, COMPOSITE_ID_SEPARATOR);
	}

	/**
	 * Splits only the first composite value return two parts, the second which could have one or
	 * more occurrences {@link #COMPOSITE_ID_SEPARATOR}.
	 * 
	 * @param idValue
	 * @return
	 */
	public static String[] splitCompositeIdValueFirstOccurrence(String idValue) {
		return StringUtils.split(idValue, COMPOSITE_ID_SEPARATOR_STRING, 2);
	}

	/**
	 * Given a composite ID this will return a pair of IDs of the given type built from the
	 * composite. The left of the pair is the left of the composite separator.
	 * 
	 * @param compositeId
	 * @param idClassOfResults
	 * @return
	 */
	public static <I extends StringIdentifier<?>, I2 extends Identifier<?, ?>> Pair<I2, I2> splitCompositeId(
			I compositeId, Class<I2> idClassOfResults) {
		Pair<String, String> stringPair = splitCompositeId(compositeId);
		try {
			return Pair.of(	FromString.construct(idClassOfResults, stringPair.getLeft()),
							FromString.construct(idClassOfResults, stringPair.getRight()));
		} catch (StringFormatException e) {
			throw new ToRuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param value
	 * @param idClass
	 * @return
	 */
	public static <I extends Identifier<?, ?>> I idFrom(Object value, Class<I> idClass) {
		try {
			final Constructor<I> constructor = idClass.getConstructor(value.getClass());
			return constructor.newInstance(value);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException("The identifier " + idClass.getSimpleName() + " must accept it's own value "
					+ value.getClass(), e);
		}
	}

	/**
	 * 
	 * @deprecated taking the value of the parent key does not work. The parent key may have parents
	 *             too. use {@link #idWithParent(Object, Class, Identifier)}
	 * @param value
	 * @param idClass
	 * @return
	 */
	@Deprecated
	public static <I extends Identifier<?, ?>> I idWithParent(Object parentValue, Object value, Class<I> idClass) {
		final Class<? extends Object> parentType = parentValue.getClass();
		final Class<? extends Object> valueType = value.getClass();
		try {
			final Constructor<I> constructor = idClass.getDeclaredConstructor(parentType, valueType);
			constructor.setAccessible(true);
			return constructor.newInstance(parentValue, value);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException("You must provide a constructor with parameters (parent,value) of types ("
					+ parentType + ", " + valueType + ") for " + idClass, e);
		}
	}

	/**
	 * Constructs the identifier given the expected type of identifier and the value and parent to
	 * be passed into the constructor (value,parent).
	 * 
	 * @param value
	 * @param idType
	 * @param parent
	 * @return
	 */
	public static Identifier<?, ?> idWithParent(Object value, Class<? extends Identifier<?, ?>> idType,
			Identifier<?, ?> parent) {
		final Class<? extends Object> valueType = value.getClass();
		final Class<? extends Identifier<?, ?>> parentType = (Class<? extends Identifier<?, ?>>) parent.getClass();
		try {
			Constructor<? extends Identifier<?, ?>> constructor = idType.getConstructor(valueType, parentType);
			return constructor.newInstance(value, parent);
		} catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(idType.getSimpleName() + " must have a constructor with " + valueType + ", "
					+ parentType);
		}
	}

	/**
	 * @param key
	 * @return
	 */
	public static Boolean isCompositeKeyValue(String key) {
		return StringUtils.contains(key, COMPOSITE_ID_SEPARATOR);
	}

	/**
	 * @return
	 */
	public static String getCompositeSeparator() {
		return String.valueOf(COMPOSITE_ID_SEPARATOR);
	}

	/**
	 * Provides the value or null if the id given is null.
	 * 
	 * @param reciprocalGroupId
	 * @return
	 */
	@Nullable
	public static <V extends Comparable<V>> V valueOf(@Nullable Identifier<V, ?> id) {
		if (id != null) {
			return id.getValue();
		} else {
			return null;
		}
	}

	/**
	 * simply joins all the given parts using the {@link #COMPOSITE_ID_SEPARATOR}.
	 * 
	 * @param parts
	 * @return
	 */
	public static String createCompositeIdValue(String... parts) {
		return createCompositeIdValue(ImmutableList.<String> builder().add(parts).build());
	}

	public static String createCompositeIdValue(Iterable<String> parts) {
		return StringUtils.join(parts, getCompositeSeparator());
	}

	public static <C extends Comparable<C>, I extends Identifier<C, ?>> Function<I, C>
			valueFunction(Class<C> comparable) {
		return new Function<I, C>() {

			@Override
			public C apply(I input) {
				return (input == null) ? null : input.getValue();
			}
		};
	}

	/**
	 * Provides the ability to order by the Identifier since it's {@link Identifier#getValue()} is
	 * {@link Comparable}.
	 * 
	 * @return
	 */
	public static <I extends Identifier<?, ?>> List<I> idsOrdered(Iterable<I> ids) {
		return Ordering.natural().onResultOf(Functions.toStringFunction()).sortedCopy(ids);
	}

	/**
	 * Orders {@link Identifiable}s by their natural order of their {@link Identifier}s.
	 * 
	 * @see #valueFunction()
	 * @see #idFunction()
	 * 
	 * @return
	 */
	public static <E extends Identifiable<?>> Ordering<E> identifableOrdering() {
		// generic hell kept me from chaining from others,
		// adding I and C to the signature helps chaining, but ruins the client
		// use
		return Ordering.natural().onResultOf(new Function<E, Comparable<?>>() {

			@Override
			public Comparable<?> apply(E input) {
				return (input == null || input.getId() == null) ? null : input.getId().getValue();
			}
		});
	}

	/**
	 * provides the standard way to describe the domain for the {@link Identifier#getKind()}.
	 * 
	 * @param id
	 * @return
	 */
	@Nonnull
	public static String domain(@Nonnull Identifier<?, ?> id) {
		// this is a bit risky since it's not a guarantee to match
		// consider getting the Entity from the parameters
		// then AnnotatedDictionaryUtils#
		return StringUtils.uncapitalize(id.getKind().getSimpleName());
	}

	/**
	 * Throws InvalidIdentifierException with the standard {@link #COMPOSITE_ID_SEPARATOR}.
	 * 
	 * @param compositeValue
	 * @return
	 */
	public static InvalidIdentifierException throwInvalid(String compositeValue) throws InvalidIdentifierException {
		throw InvalidIdentifierException.compositeFormat(compositeValue, String.valueOf(COMPOSITE_ID_SEPARATOR));
	}

	/**
	 * The type may be a class of Identifier or a ParameterizedType of Identifier<?,?> itself. This
	 * inspects and returns the appropriate.
	 * 
	 * @param type
	 * @return
	 */
	public static Boolean isIdentifierType(final Type type) {
		Type rawType;
		if (type instanceof ParameterizedType) {
			rawType = ((ParameterizedType) type).getRawType();
		} else {
			rawType = type;
		}
		if (rawType instanceof Class) {
			Class<?> typeClass = (Class<?>) rawType;
			if (Identifier.class.isAssignableFrom(typeClass)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param type
	 */
	public static Class<? extends Identifier<?, ?>> typeToClass(Type type) {
		Type rawType;
		if (type instanceof ParameterizedType) {
			rawType = ((ParameterizedType) type).getRawType();
		} else {
			rawType = type;
		}

		return (Class<? extends Identifier<?, ?>>) rawType;
	}

	/**
	 * Extracts all parents from the identifiers until reaching the root. The order will be the leaf
	 * first and root last. At minimum it will return it will return the id as given...even if it is
	 * null.
	 * 
	 * @param id
	 * @return
	 */
	public static List<Identifier<?, ?>> ancestryRootLast(final Identifier<?, ?> id) {
		Builder<Identifier<?, ?>> ancestry = ImmutableList.builder();
		Identifier<?, ?> ancestor = id;
		// climb down the tree adding each ancestor to the list
		do {
			ancestry.add(ancestor);
			if (ancestor instanceof HasParent) {
				HasParent<Identifier<?, ?>> ancestorWithParent = (HasParent<Identifier<?, ?>>) ancestor;
				ancestor = ancestorWithParent.getParent();
			} else {
				ancestor = null;
			}
		} while (ancestor != null);
		return ancestry.build();
	}

	/**
	 * Provides the root identifier if the given id {@link HasParent} crawling the ancestry until
	 * {@link HasParent#isRoot()} is true.
	 * 
	 * @param id
	 * @return
	 */
	public static <I extends Identifier<?, ?>> I root(I id) {
		while (hasParent(id)) {
			id = ((HasParent<I>) id).getParent();
		}
		return id;
	}

	public static List<Identifier<?, ?>> ancestryRootFirst(final Identifier<?, ?> id) {
		return Lists.reverse(IdentifierUtils.ancestryRootLast(id));
	}

	/**
	 * indicates if the Identifier is a parent and if the parent is not null or better said
	 * {@link HasParent#isRoot()}.
	 * 
	 * @param id
	 * @return
	 */
	public static Boolean hasParent(Identifier<?, ?> id) {
		return (id instanceof HasParent && !((HasParent<?>) id).isRoot());
	}

	/**
	 * returns the parent from the id. It assumes you called {@link #hasParent(Identifier)} so it
	 * will throw null Pointer exception if no parent exists and throw an exception if this isn't
	 * even parent capable.
	 * 
	 * @see #hasParent(Identifier)
	 * @param id
	 * @return
	 * @throws {@link ClassCastException} when not implementing {@link HasParent}
	 * @throws NullPointerException
	 *             when parent is null.
	 */
	@Nonnull
	public static Identifier<?, ?> parent(Identifier<?, ?> id) {
		final Identifier<?, ?> parent = ((HasParent<?>) id).getParent();
		Assertion.assertNotNull(parent);
		return parent;
	}

	/**
	 * Indicates if this is of an identifier that {@link HasParent}.
	 * 
	 * @param idType
	 * @return
	 */
	public static Boolean hasParent(Class<? extends Identifier<?, ?>> idType) {
		return HasParent.class.isAssignableFrom(idType);
	}

	/**
	 * When you know a type has a parent this will provide it. This assumes you already called
	 * {@link #hasParent(Class)} so it will fail if the type is not parent capable.
	 * 
	 * @param idHasParentType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Class<? extends Identifier<?, ?>> parentType(Class<? extends Identifier<?, ?>> idHasParentType) {
		ParameterizedType genericSuperclass = (ParameterizedType) idHasParentType.getGenericSuperclass();

		// This is a very limited implementation that assumes this implements either of the
		// IdentifierWithParent classes
		final Type[] types = genericSuperclass.getActualTypeArguments();
		if (types.length < 2) {
			throw new IllegalArgumentException(idHasParentType
					+ " does not have the expected generic parameter types  (KindT,ParentIdType) "
					+ ArrayUtils.toString(types));
		}
		return (Class<? extends Identifier<?, ?>>) types[1];

	}
}