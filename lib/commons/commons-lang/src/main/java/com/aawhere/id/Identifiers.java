/**
 * 
 */
package com.aawhere.id;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;

import com.aawhere.collections.Index;
import com.aawhere.lang.reflect.GenericUtils;

/**
 * A simple wrapper that specializes in marshing ids to/from strings also providing typed security.
 * 
 * @author aroller
 * 
 */
abstract public class Identifiers<I extends Identifier<?, E>, E extends Identifiable<I>>
		implements Serializable, Iterable<I> {

	private final Iterable<I> ids;

	/**
	 * Necessary for JAXB.
	 * 
	 */
	protected Identifiers() {
		// whoever uses this one better inject
		this.ids = null;
	}

	public Identifiers(String identifierString) {

		if (identifierString != null) {
			Class<I> idClass = GenericUtils.getTypeArgument(this, Index.FIRST);
			this.ids = IdentifierUtils.idsFrom(identifierString, idClass);
		} else {
			this.ids = Collections.emptySet();
		}
	}

	public Identifiers(Iterable<I> ids) {
		this.ids = ids;

	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<I> iterator() {
		return this.ids.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return IdentifierUtils.idsJoined(ids);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.ids == null) ? 0 : this.ids.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Identifiers<I, E> other = (Identifiers<I, E>) obj;
		if (this.ids == null) {
			if (other.ids != null)
				return false;
		} else if (!this.ids.equals(other.ids))
			return false;
		return true;
	}

	private static final long serialVersionUID = 1167736932360217347L;

}
