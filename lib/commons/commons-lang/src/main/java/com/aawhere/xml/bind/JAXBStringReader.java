/**
 *
 */
package com.aawhere.xml.bind;

import java.io.StringReader;
import java.util.HashSet;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.lang.ObjectBuilder;

/**
 * @author Aaron Roller
 * 
 */
public class JAXBStringReader<T> {

	/**
	 * Used to construct all instances of JAXBStringReader.
	 */
	public static class Builder<T>
			extends ObjectBuilder<JAXBStringReader<T>> {

		private HashSet<XmlAdapter<?, ?>> adapters = new HashSet<XmlAdapter<?, ?>>();
		private Class<?>[] elementTypes = {};
		private String xml;

		private Builder() {
			super(new JAXBStringReader<T>());
		}

		public Builder<T> adapter(XmlAdapter<?, ?>... adapters) {
			CollectionUtils.addAll(this.adapters, adapters);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@SuppressWarnings("unchecked")
		@Override
		public JAXBStringReader<T> build() {
			StringReader reader = new StringReader(xml);
			try {
				// classCastException is a good way to explain the problem.
				final Unmarshaller unmarshaller = JAXBContext.newInstance(this.elementTypes).createUnmarshaller();
				for (XmlAdapter<?, ?> adapter : this.adapters) {
					unmarshaller.setAdapter(adapter);
				}
				building.element = (T) unmarshaller.unmarshal(reader);

			} catch (JAXBException e) {
				throw new RuntimeException(e);
			}

			return super.build();
		}

		public Builder<T> elementTypes(Class<?>... classesToBeBound) {

			this.elementTypes = ArrayUtils.addAll(this.elementTypes, classesToBeBound);
			return this;
		}

		public Builder<T> xml(String xml) {
			this.xml = xml;
			return this;
		}

	}// end Builder

	public static <T> Builder<T> create(String xml, Class<T> elementType) {
		Builder<T> builder = new Builder<T>().xml(xml);
		if (!JAXBElement.class.isAssignableFrom(elementType)) {
			builder.elementTypes(elementType);
		}
		return builder;
	}

	private T element;

	/** Use {@link Builder} to construct JAXBStringReader */
	private JAXBStringReader() {
	}

	/**
	 * @return the element
	 */
	public T getElement() {
		return this.element;
	}
}
