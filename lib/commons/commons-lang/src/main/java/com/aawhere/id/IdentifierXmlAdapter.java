/**
 *
 */
package com.aawhere.id;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.lang.NotImplementedException;

/**
 * @author Aaron Roller
 * 
 */
@Deprecated
public class IdentifierXmlAdapter
		extends XmlAdapter<Long, Identifier<Long, ?>> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Identifier<Long, ?> unmarshal(Long v) throws Exception {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Long marshal(Identifier<Long, ?> id) throws Exception {
		if (id != null) {
			return id.getValue();
		} else {
			return null;
		}

	}

}
