/**
 * 
 */
package com.aawhere.math.stats;

import org.apache.commons.math.stat.descriptive.summary.Sum;

/**
 * The {@link Sum} of all positive numbers.
 * 
 * @author aroller
 * 
 */
public class Gain
		extends FilteredSum {

	private static final long serialVersionUID = 8251151712464599299L;

	/**
	 * 
	 */
	public Gain() {
	}

	/**
	 * @param original
	 */
	public Gain(Sum original) {
		super(original);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.math.stats.FilteredSum#passedFilter(double)
	 */
	@Override
	protected boolean passedFilter(double d) {
		return d > 0;
	}

}
