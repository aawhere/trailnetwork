/**
 *
 */
package com.aawhere.lang.enums;

import java.util.HashSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.util.Assert;

import com.aawhere.lang.ObjectBuilder;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * A single place to register enumerations in the system. The typical use case is to convert an enum
 * to a string for storage or communication and eventually use that String to convert back into a
 * enum object (perhaps at a database or xml transformation). Such a use case is typically easy if
 * the enum is known, but if the enum is not known (hidden by an interface perhaps) then this
 * factory is useful.
 * 
 * I'm not able to find any references on the Internet for a similar need...likely because most
 * people don't hide enumerations behind interfaces losing a value of dealing generally with an
 * abstracted version.
 * 
 * Entire systems would likely use the {@link #register(Class)} method to provide Enums that are
 * participating in the system, but localized uses of this can simply call the
 * {@link #keyForObject(Object)} method which will register Enums and provide keys on the fly. In
 * such localized cases calling {@link #enumFor(String)} won't work since the enumeration has not
 * yet been registered.
 * 
 * @param <EnumInterfaceT>
 *            generic template allowing restriction to a specific type of enums that implement an
 *            interface. Use Object if no restriction is required.
 * @author roller
 * 
 */
public class EnumFactory<EnumInterfaceT> {

	/** A convenience method for constructing a builder */
	public static <EnumInterfaceT> Builder<EnumInterfaceT> create() {
		return new Builder<EnumInterfaceT>();
	}

	private HashBiMap<String, Enum<?>> keys = HashBiMap.create();
	private HashSet<Class<? extends Enum<?>>> registered = new HashSet<Class<? extends Enum<?>>>();
	private Class<EnumInterfaceT> interfaceType;

	/**
	 * Use {@link Builder} to construct EnumFactory. Protected to allow children to extend providing
	 * the template.
	 * */
	protected EnumFactory() {
	}

	/**
	 * Used when an enum may be hidden behind an Interface.
	 * 
	 * @see #keyForEnum(Enum)
	 * @see #enumFor(String)
	 * 
	 * @param object
	 *            usually an {@link Enum}. Essentially ignored if not an Enum and the key will be
	 *            null.
	 * @return String representation of the enum that may be used to retrieve the Enum at a later
	 *         time, or null if not an enum.
	 */
	public String keyForObject(EnumInterfaceT object) {
		String key;
		Class<? extends Object> objectClass = object.getClass();

		if (objectClass.isEnum()) {
			Enum<?> enumeration = (Enum<?>) object;
			key = keyForEnum(enumeration);
		} else {
			key = null;
		}
		return key;
	}

	public String keyForEnum(Enum<?> enumeration) {
		assertCorrectInterface(enumeration.getClass());
		final Class<? extends Enum<?>> declaringClass = enumeration.getDeclaringClass();

		String key = keys.inverse().get(enumeration);
		if (key == null) {
			register(declaringClass);
			key = keys.inverse().get(enumeration);
		}
		return key;

	}

	/**
	 * Provides a String key when given the enum class and the {@link Enum#name()}.
	 * 
	 * This does not validate that the name exist, but just builds the key.
	 * 
	 * This will also register any enumerations available on the class provdided so subsequent calls
	 * will already be registered.
	 * 
	 * @param declaringClass
	 * @param name
	 * @return
	 */
	public String keyForEnum(Class<? extends Enum<?>> declaringClass, String name) {
		assertCorrectInterface(declaringClass);
		register(declaringClass);
		return buildKey(declaringClass, name);
	}

	/**
	 * Provides the Enum matching the key. This will never return null, but will throw a runtime
	 * exception indicating that proper registration did not occur so this factory does not yet know
	 * about an existing Enum class OR the key is not valid. In either case it is likely a developer
	 * providing the wrong value so this exception will explain it in developer terms.
	 * 
	 * Should you need to know if a key exists you should call
	 * 
	 * @param key
	 * @return the enum.
	 * @throws InvalidEnumKeyException
	 *             when the key provided does match any registered enum
	 */
	@SuppressWarnings("unchecked")
	@Nonnull
	public <EnumT extends Enum<?>> EnumT enumFor(@Nullable String key) {
		EnumT foundEnum = (EnumT) this.keys.get(key);
		if (foundEnum == null) {
			throw new InvalidEnumKeyException(key, this.keys);
		}
		return foundEnum;
	}

	/**
	 * Simply identifies if the key exists. For use before calling {@link #enumFor(String)} to avoid
	 * runtime exception.
	 * 
	 * @param key
	 * @return
	 */
	public Boolean enumExists(@Nullable String key) {
		return this.keys.get(key) != null;
	}

	/**
	 * This registers the enum if it hadn't already been registered. This method is synchronized
	 * ensuring only one registration happens at a time. The retrieval is not synchronized to avoid
	 * a likely bottleneck. Registration happens in the beginning
	 * 
	 * @param enumClass
	 * @return true if the enumClass had not been registered previously, false otherwise.
	 */
	public synchronized Boolean register(Class<? extends Enum<?>> enumClass) {
		assertCorrectInterface(enumClass);

		Boolean registered;
		if (this.registered.contains(enumClass)) {
			registered = false;
		} else {
			registered = true;
			Enum<?>[] constants = enumClass.getEnumConstants();
			for (int whichEnum = 0; whichEnum < constants.length; whichEnum++) {
				Enum<?> currentEnum = constants[whichEnum];
				String key = buildKey(enumClass, currentEnum.name());
				Enum<?> previousRegistration = this.keys.put(key, currentEnum);
				if (previousRegistration != null) {
					throw new RuntimeException(
							previousRegistration.toString()
									+ " is already registered.  We likely have two Enumeration classes simple named the same in the system.  This is not handled so either we handle it or rename the Enum: "
									+ this.registered.toString() + " Keys: " + keys.toString());
				}

			}
			this.registered.add(enumClass);
		}
		return registered;
	}

	private String buildKey(Class<? extends Enum<?>> enumClass, String enumName) {
		assertCorrectInterface(enumClass);
		return enumClass.getSimpleName() + "." + enumName;
	}

	/**
	 * @param value
	 * @return
	 */
	@Nonnull
	public Boolean keyIsRecognized(String key) {
		return this.keys.containsKey(key);
	}

	/**
	 * Used to construct all instances of EnumFactory.
	 */
	public static class Builder<EnumInterfaceT>
			extends ObjectBuilder<EnumFactory<EnumInterfaceT>> {

		public Builder() {
			super(new EnumFactory<EnumInterfaceT>());
		}

		protected Builder(EnumFactory<EnumInterfaceT> child) {
			super(child);
		}

		public Builder<EnumInterfaceT> setEnumInterfaceType(Class<EnumInterfaceT> type) {
			building.interfaceType = type;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assert.notNull(building.interfaceType, "EnumInterfaceT is required");
			super.validate();
		}

	}// end Builder

	/**
	 * Provides read-only access to the map of enumerations being managed keyed by the string that
	 * represents each.
	 */
	public BiMap<String, Enum<?>> getAll() {
		return this.keys;
	}

	/**
	 * determines if the type has the correct interface (EnumInterfaceT). Using compile time
	 * checking was not possible due to the fact that one cannot do Enum<? extends EnumInterfaceT>
	 * in java 7 (works in Java 6)
	 * 
	 * @param type
	 */
	private void assertCorrectInterface(Class<?> type) {
		if (!interfaceType.isAssignableFrom(type)) {
			// Note that Java 7 made it difficult(impossible?) to use generics in this case to avoid
			// this call.
			// See Java 6 version in commit 5e52fadf8f78b02f381ebbe7184d87e2a71e3b4e or before.
			throw new IllegalArgumentException("Expecting an enumeration that extends EnumInterfaceT");
		}
	}

}
