/**
 *
 */
package com.aawhere.lang.reflect;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import com.aawhere.collections.Index;
import com.aawhere.log.LoggerFactory;
import com.google.common.collect.Iterators;

/**
 * Useful tools to help with relection against Generics.
 * 
 * 
 * @author Aaron Roller
 * 
 */
public class GenericUtils {

	/**
	 * Returns the class of the parameter of the super class. Assumes only 1 parameter so that's the
	 * only one returned.
	 * 
	 * @param objectWithParamaterizedTypes
	 * @return
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> getTypeArgument(@Nonnull Object objectWithParamaterizedTypes) {
		final Iterator<Class<?>> iterator = getTypeArguments(objectWithParamaterizedTypes).iterator();
		return (Class<T>) Iterators.getOnlyElement(iterator, null);
	}

	@SuppressWarnings("unchecked")
	public static <T> Class<T> getTypeArgument(@Nonnull Object objectWithParamaterizedTypes,
			@Nonnull Index parameterIndex) {

		List<Class<?>> typeArguments = getTypeArguments(objectWithParamaterizedTypes);
		if (typeArguments.size() <= parameterIndex.index) {
			throw new IllegalArgumentException(objectWithParamaterizedTypes + " looking for parameter "
					+ parameterIndex + ", but only has " + typeArguments);
		}
		return (Class<T>) typeArguments.get(parameterIndex.index);

	}

	/**
	 * Finds all of the declared classes declared as types of a generic superclass of the given
	 * object.
	 */
	public static List<Class<?>> getTypeArguments(@Nonnull Object objectWithParamaterizedTypes) {

		ArrayList<Class<?>> parameterClasses = new ArrayList<Class<?>>();

		final Class<? extends Object> targetClass;
		if (objectWithParamaterizedTypes instanceof Class<?>) {
			targetClass = (Class<?>) objectWithParamaterizedTypes;
		} else {
			targetClass = objectWithParamaterizedTypes.getClass();
		}
		Type genericSuperclass = targetClass.getGenericSuperclass();
		// Allow this class to be safely instantiated with or without a
		// parameterized type
		if (genericSuperclass instanceof ParameterizedType) {
			final Type[] actualTypeArguments = ((ParameterizedType) genericSuperclass).getActualTypeArguments();
			for (int i = 0; i < actualTypeArguments.length; i++) {
				Type type = actualTypeArguments[i];
				if (type instanceof Class) {
					Class<?> clazz = (Class<?>) type;
					parameterClasses.add(clazz);
				} else if (type instanceof ParameterizedType) {
					Type rawType = ((ParameterizedType) type).getRawType();
					if (rawType instanceof Class) {
						parameterClasses.add((Class<?>) rawType);
					}
				} else {
					LoggerFactory.getLogger(GenericUtils.class).fine("Cannot get class from parameter <" + type
							+ "> for " + targetClass);
					// add a placeholder because the client may not want this parameter, but should
					// still maintain order of parameters
					parameterClasses.add(null);
				}
			}
		}
		return parameterClasses;
	}

	/**
	 * http://www.artima.com/weblogs/viewpost.jsp?thread=208860 Currently failing a simple test, but
	 * left for future possibilities.
	 * 
	 * Get the underlying class for a type, or null if the type is a variable type.
	 * 
	 * @param type
	 *            the type
	 * @return the underlying class
	 */
	public static Class<?> getClass(Type type) {
		if (type instanceof Class) {
			return (Class<?>) type;
		} else if (type instanceof ParameterizedType) {
			return getClass(((ParameterizedType) type).getRawType());
		} else if (type instanceof GenericArrayType) {
			Type componentType = ((GenericArrayType) type).getGenericComponentType();
			Class<?> componentClass = getClass(componentType);
			if (componentClass != null) {
				return Array.newInstance(componentClass, 0).getClass();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * http://www.artima.com/weblogs/viewpost.jsp?thread=208860
	 * 
	 * 
	 * @param baseClass
	 *            the base class
	 * @param childClass
	 *            the child class
	 * @return a list of the raw classes for the actual type arguments.
	 */
	public static <T> List<Class<?>> getTypeArguments(Class<T> baseClass, Class<? extends T> childClass) {

		Map<Type, Type> resolvedTypes = new HashMap<Type, Type>();
		Type type = childClass;
		// start walking up the inheritance hierarchy until we hit baseClass
		while (!getClass(type).equals(baseClass)) {
			if (type instanceof Class) {
				// there is no useful information for us in raw types, so just
				// keep going.
				type = ((Class<?>) type).getGenericSuperclass();

			} else {
				ParameterizedType parameterizedType = (ParameterizedType) type;
				Class<?> rawType = (Class<?>) parameterizedType.getRawType();

				Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
				TypeVariable<?>[] typeParameters = rawType.getTypeParameters();
				for (int i = 0; i < actualTypeArguments.length; i++) {
					resolvedTypes.put(typeParameters[i], actualTypeArguments[i]);
				}

				if (!rawType.equals(baseClass)) {
					type = rawType.getGenericSuperclass();
				}
			}
		}

		// finally, for each actual type argument provided to baseClass,
		// determine (if possible)
		// the raw class for that type argument.
		Type[] actualTypeArguments;
		if (type instanceof Class) {
			actualTypeArguments = ((Class<?>) type).getTypeParameters();
		} else {
			actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
		}
		List<Class<?>> typeArgumentsAsClasses = new ArrayList<Class<?>>();
		// resolve types by chasing down type variables.
		for (Type baseType : actualTypeArguments) {
			while (resolvedTypes.containsKey(baseType)) {
				baseType = resolvedTypes.get(baseType);
			}
			typeArgumentsAsClasses.add(getClass(baseType));
		}
		return typeArgumentsAsClasses;
	}
}
