/**
 * 
 */
package com.aawhere.collections;

import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class RangeExtended {

	/**
	 * Provides a new range with the same bounds, but with the value returned from the given
	 * Function.
	 * 
	 * @param range
	 * @param function
	 * @return
	 */
	public static <F extends Comparable<F>, T extends Comparable<T>> Range<T> compose(Range<F> range,
			Function<F, T> function) {
		Range<T> result;
		if (range.hasUpperBound() && range.hasLowerBound()) {
			T upperEndpoint = function.apply(range.upperEndpoint());
			T lowerEndpoint = function.apply(range.lowerEndpoint());
			result = Range.range(lowerEndpoint, range.lowerBoundType(), upperEndpoint, range.upperBoundType());
		} else if (range.hasUpperBound()) {
			result = Range.upTo(function.apply(range.upperEndpoint()), range.upperBoundType());
		} else if (range.hasLowerBound()) {
			result = Range.downTo(function.apply(range.lowerEndpoint()), range.lowerBoundType());
		} else {
			result = Range.all();
		}
		return result;
	}

	/**
	 * Provides a standard way to search through a list of ranges and return the value mapped to
	 * that range.
	 * 
	 * @author aroller
	 * 
	 * @param <C>
	 *            any comparable that will be the value of the range
	 * @param <T>
	 *            any object you wish to have returned
	 */
	@SuppressWarnings("rawtypes")
	public static class RangeFunction<C extends Comparable, T>
			implements Function<C, T>, Serializable {
		private Map<Range<C>, T> ranges;
		private T defaultValue;

		public RangeFunction(@Nonnull Map<Range<C>, T> ranges, @Nullable T defaultValue) {
			this.ranges = ranges;
			this.defaultValue = defaultValue;
		}

		@Override
		public T apply(C input) {

			// breaks in the middle when range is matched
			Set<Entry<Range<C>, T>> entrySet = ranges.entrySet();
			for (Entry<Range<C>, T> entry : entrySet) {
				Range<C> range = entry.getKey();
				if (range.contains(input)) {
					return entry.getValue();
				}
			}
			// nothing found...indicate null.
			return (defaultValue != null) ? defaultValue : null;
		}

		private static final long serialVersionUID = 1170214429316651261L;
	}

}
