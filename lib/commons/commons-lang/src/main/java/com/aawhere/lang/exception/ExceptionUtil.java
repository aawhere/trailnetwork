/**
 *
 */
package com.aawhere.lang.exception;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.aawhere.net.HttpStatusCode;
import com.aawhere.net.StatusCodeContainer;

/**
 * Specific utilities that work with our {@link BaseException} and extends beyond
 * {@link ExceptionUtils}.
 * 
 * @author aroller
 * 
 */
public class ExceptionUtil {

	/**
	 *
	 */
	private static final HttpStatusCode DEFAULT_STATUS = HttpStatusCode.INTERNAL_ERROR;

	public static HttpStatusCode statusCodeFromAnnotation(Exception exception) {
		final Class<? extends Exception> exceptionClass = exception.getClass();
		return statusCodeFromAnnotation(exceptionClass);
	}

	public static HttpStatusCode statusCodeFromAnnotation(Class<? extends Exception> exceptionClass) {
		StatusCode annotation = exceptionClass.getAnnotation(StatusCode.class);
		if (annotation != null) {
			return annotation.value();
		} else {
			return null;
		}
	}

	/**
	 * Given the exception this will search for the {@link HttpStatusCode}. First if the Exception
	 * implements {@link StatusCodeContainer} then the response will come from
	 * {@link StatusCodeContainer#getStatusCode()}. If not then it will search for the
	 * {@link StatusCode} annotation and lastly just assume this must be an
	 * {@link HttpStatusCode#INTERNAL_ERROR} since nothing better was provided.
	 * 
	 * @param exception
	 * @return
	 */
	@Nonnull
	public static HttpStatusCode responseCode(@Nonnull Exception exception) {
		return responseCode(exception, null);
	}

	/**
	 * Provides the best response code when provided an exception and/or a status code..either may
	 * be null, but the response is guaranteed defaulting to general 500 error if nothing better is
	 * provided.
	 * 
	 * @param wrap
	 * @param status
	 * @return
	 */
	@Nonnull
	public static HttpStatusCode responseCode(@Nullable Exception exception, @Nullable HttpStatusCode status) {
		HttpStatusCode result = status;
		if (result == null) {
			if (exception instanceof StatusCodeContainer) {
				result = ((StatusCodeContainer) exception).getStatusCode();
			}
			if (result == null && exception != null) {
				result = statusCodeFromAnnotation(exception);
			}
		}
		if (result == null) {
			result = DEFAULT_STATUS;
		}
		return result;
	}
}
