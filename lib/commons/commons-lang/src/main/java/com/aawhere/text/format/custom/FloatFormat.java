/**
 * 
 */
package com.aawhere.text.format.custom;

/**
 * @author roller
 * 
 */
public class FloatFormat
		extends DecimalFormat<Float> {

	@Override
	public Class<Float> handlesType() {
		return Float.class;
	}

}
