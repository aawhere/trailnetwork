/**
 * 
 */
package com.aawhere.util.rb;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.collections4.MapUtils;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.text.format.custom.CustomFormatFactory;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * @author aroller
 * 
 */
public class CompleteMessageXmlAdapter
		extends XmlAdapter<CompleteMessageXmlAdapter.CompleteMessageXml, CompleteMessage> {

	// FIXME:Inject this
	private static final CustomFormatFactory customFormatFactory = CustomFormatFactory.getInstance();

	public CompleteMessageXmlAdapter() {
	}

	/*
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public CompleteMessage unmarshal(CompleteMessageXml xml) throws Exception {
		throw new UnsupportedOperationException("possible, not that useful");
	}

	/*
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public CompleteMessageXml marshal(CompleteMessage message) throws Exception {
		if (message == null) {
			return null;
		}

		return new CompleteMessageXml(message);
	}

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE, name = Doc.NAME)
	@XmlAccessorType(XmlAccessType.NONE)
	@ApiModel(value = Doc.NAME, description = Doc.DESCRIPTION)
	public static class CompleteMessageXml
			implements Serializable {

		@XmlElement
		@ApiModelProperty("The raw message with {ENCLOSED_PARAMS}.")
		public final String message;

		@XmlAttribute
		@ApiModelProperty("Unique key representing this type of message")
		public final String messageKey;
		@XmlElement(name = "param")
		@ApiModelProperty("All the Parameters associated with the message.")
		public final List<ParamXml> params;

		private static final long serialVersionUID = 3952287419815280185L;

		public CompleteMessageXml() {
			this(null);
		}

		/**
		 * @param message
		 * @param params
		 */
		public CompleteMessageXml(CompleteMessage message) {
			super();
			if (message == null) {
				this.message = null;
				this.params = null;
				this.messageKey = null;
			} else {
				if (MapUtils.isNotEmpty(message.getParameters())) {
					ImmutableList<ParamXml> params = ImmutableList.copyOf(Iterables.filter(Iterables.transform(message
							.getParameters().entrySet(), paramFunction()), Predicates.notNull()));
					this.params = params;
				} else {
					this.params = null;
				}
				this.message = message.getMessage().getValue();
				this.messageKey = message.getMessage().name();
			}

		}

	}

	@XmlType(namespace = XmlNamespace.API_VALUE, name = "CompleteMessageParam")
	@XmlAccessorType(XmlAccessType.NONE)
	@ApiModel(description = "Key/Value pair corresponding with enclosed parameters in the message.")
	public static class ParamXml
			implements Serializable {

		private static final long serialVersionUID = 5784496477983237140L;
		@XmlAttribute
		@ApiModelProperty(value = "Corresponds to the {ENCLOSED_PARAMS} in the message.", required = true)
		@Nonnull
		public final String key;

		@XmlElement
		@Nullable
		@ApiModelProperty(value = "The value to replace the key in the message.")
		public final String value;

		public ParamXml() {
			this(null, null);
		}

		/**
		 * @param key
		 * @param value
		 */
		public ParamXml(String key, Object value) {
			super();
			this.key = key;
			if (value == null) {
				this.value = null;
			} else {
				@SuppressWarnings("unchecked")
				final CustomFormat<Object> format = (CustomFormat<Object>) customFormatFactory.getFormat(value
						.getClass());
				// FIXME:Inject this for the session
				final Locale locale = Locale.US;
				this.value = format.format(value, locale);
			}
		}
	}

	public static final Function<Map.Entry<ParamKey, Object>, ParamXml> paramFunction() {
		return new Function<Map.Entry<ParamKey, Object>, CompleteMessageXmlAdapter.ParamXml>() {

			@Override
			public ParamXml apply(Entry<ParamKey, Object> input) {
				return (input != null) ? new ParamXml(input.getKey().toString(), input.getValue()) : null;
			}
		};
	}

	public static final class Doc {
		public static final String NAME = "CompleteMessage";
		public static final String DATA_TYPE = NAME;
		public static final String DESCRIPTION = "Programatic details about the message";
	}
}
