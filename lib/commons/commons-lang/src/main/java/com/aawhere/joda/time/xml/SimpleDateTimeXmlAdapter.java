/**
 *
 */
package com.aawhere.joda.time.xml;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;

/**
 * {@link XmlAdapter} for {@link DateTime}. The SimpleXmlAdapter simply outputs a string in a format
 * suitable for transporting in XML.
 * 
 * @author Aaron Roller
 * 
 */
public class SimpleDateTimeXmlAdapter
		extends XmlAdapter<String, DateTime> {

	@Override
	public DateTime unmarshal(String context) throws Exception {
		return new DateTime(context);
	}

	@Override
	public String marshal(DateTime context) {
		return context.toString();
	}
}
