/**
 *
 */
package com.aawhere.lang.bool;

/**
 * Utilities for workign with Boolean types
 * 
 * @author Brian Chapman
 * 
 */
public class BooleanUtil {

	public static final Integer TRUE = 1;
	public static final Integer FALSE = 0;

	public static Integer booleanToInteger(Boolean bool) {
		return bool ? TRUE : FALSE;
	}

	public static Boolean integerToBoolean(Integer i) {
		if (i.equals(TRUE)) {
			return true;
		} else if (i.equals(FALSE)) {
			return false;
		} else {
			throw new IllegalArgumentException("Invalid Integer. Acceptable values are 0 and 1");
		}
	}

	/**
	 * Returns true if the two provided are the same handling nulls. Two nulls returns true.
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static Boolean equals(Boolean left, Boolean right) {
		return left == right || (left != null && left.equals(right));
	}
}
