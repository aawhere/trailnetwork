/**
 * 
 */
package com.aawhere.util.rb;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.Assertion;
import com.aawhere.xml.XmlNamespace;

/**
 * A simple String implementation of a {@link Message} that doesn't provide i18n possibilities.
 * 
 * @author roller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
// @XmlTransient
public class StringMessage
		implements Message {

	private static final long serialVersionUID = 4013044594826677402L;
	@XmlAttribute
	private String value;
	@XmlAttribute
	private String name;

	protected StringMessage() {
	}

	/**
	 * @param value
	 */
	public StringMessage(@Nonnull String name, @Nonnull String value) {
		super();
		this.value = value;
		this.name = name;
		Assertion.assertNotNull("value", value);
	}

	/**
	 * @param value
	 */
	public StringMessage(@Nonnull String value) {
		this(null, value);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.util.rb.Message#getValue()
	 */
	@Override
	@Nonnull
	public String getValue() {

		return value;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.util.rb.Message#getParameterKeys()
	 */
	@Override
	@Nonnull
	public ParamKey[] getParameterKeys() {
		return EMPTY_PARAM_KEYS;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.value;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.value == null) ? 0 : this.value.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		StringMessage other = (StringMessage) obj;
		if (this.value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!this.value.equals(other.value)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.util.rb.Message#name()
	 */
	@Override
	public String name() {

		return this.name;
	}

}
