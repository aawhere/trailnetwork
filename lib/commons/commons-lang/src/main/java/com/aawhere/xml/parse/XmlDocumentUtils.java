/**
 *
 */
package com.aawhere.xml.parse;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Utilities for working with java.xml.* classes.
 * 
 * @author Brian Chapman
 * 
 */
public class XmlDocumentUtils {

	public static XmlDocumentUtils newInstance() {
		return new XmlDocumentUtils();
	}

	/** Use {@link #getInstance()} to get XmlDocumentUtils */
	private XmlDocumentUtils() {
	}

	public Document parseXml(String xml) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		Document doc = dBuilder.parse(is);
		doc.getDocumentElement().normalize();
		return doc;
	}

	public String getCharacterDataFromElement(Node e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		throw new RuntimeException("Failed to get Charater Data from element");
	}

}
