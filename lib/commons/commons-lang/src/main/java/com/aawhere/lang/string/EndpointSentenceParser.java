/**
 * 
 */
package com.aawhere.lang.string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

/**
 * Given a single sentence or search string this will parse it looking for the best candidate of
 * start and finish endpoints by looking for common keywords indicating such available from
 * {@link #to()} and {@link #from()}.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class EndpointSentenceParser {

	/**
	 * Used to construct all instances of EndpointSentenceParser.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<EndpointSentenceParser> {

		/**
		 * 
		 */
		private static final String WORD_DILIMETER = " ";
		private HashSet<String> toTokens = new HashSet<>();
		private HashSet<String> fromTokens = new HashSet<>();

		public Builder(String sentence) {
			super(new EndpointSentenceParser(sentence));
		}

		public void addEnglishTokens() {
			toTokens.add("to");
			toTokens.add("To");
			toTokens.add("TO");
			fromTokens.add("from");
			fromTokens.add("From");
			fromTokens.add("FROM");

		}

		public Set<String> trailingColonTokens(Set<String> tokens) {

			HashSet<String> colonTokens = new HashSet<>();

			for (String token : tokens) {
				colonTokens.add(token + ":");
			}
			return colonTokens;
		}

		@Override
		public EndpointSentenceParser build() {
			EndpointSentenceParser built = super.build();
			addEnglishTokens();
			this.toTokens.addAll(trailingColonTokens(this.toTokens));
			this.fromTokens.addAll(trailingColonTokens(fromTokens));

			SetView<String> toTokensFound = tokensMatched(built.sentence, toTokens);
			if (!toTokensFound.isEmpty()) {
				String toTokenFound = toTokensFound.iterator().next();
				String[] sentenceParts = built.sentence.split(keywordMidSentence(toTokenFound));
				if (sentenceParts.length > 1) {
					built.from = cleanUp(sentenceParts[0]);
					// always pick the last token after to as the final destination
					int indexOfLast = sentenceParts.length - 1;
					built.to = cleanUp(sentenceParts[indexOfLast]);
					if (sentenceParts.length > 2) {

						String[] intermediateStops = Arrays.copyOfRange(sentenceParts, 1, indexOfLast);
						Collections.addAll(built.leftovers, intermediateStops);
					}
				} else {
					// To must be the first word...
					String stillContainsTo = sentenceParts[0];
					if (stillContainsTo.startsWith(toTokenFound)) {
						String[] splitAgain = cleanUp(stillContainsTo).split(keywordAtStart(toTokenFound));
						if (splitAgain.length > 0) {
							// grab the last token because there might be other junk...
							built.to = cleanUp(splitAgain[splitAgain.length - 1]);

						}
					} else {
						// else the only word is to..leave as null and log leftovers
						built.leftovers.add(cleanUp(stillContainsTo));
					}
				}
			}
			// although "to" may have found a from, it may contain from keywords
			String remainingSentence;
			if (built.from == null) {
				remainingSentence = built.sentence;
			} else {
				remainingSentence = built.from;
			}

			SetView<String> fromTokensFound = tokensMatched(remainingSentence, this.fromTokens);
			if (!fromTokensFound.isEmpty()) {
				String fromTokenFound = fromTokensFound.iterator().next();

				String[] sentenceParts = remainingSentence.split(keywordMidSentence(fromTokenFound));
				if (sentenceParts.length > 1) {
					built.from = cleanUp(sentenceParts[1]);
					built.remainder = cleanUp(sentenceParts[0]);
					if (sentenceParts.length > 2) {
						built.leftovers.addAll(Arrays.asList(Arrays.copyOfRange(sentenceParts,
																				2,
																				sentenceParts.length - 1)));
					}
				} else {
					// maybe at the beginning of the sentence
					sentenceParts = remainingSentence.split(keywordAtStart(fromTokenFound));
					if (sentenceParts.length > 1) {
						built.remainder = cleanUp(sentenceParts[0]);
						built.from = cleanUp(sentenceParts[1]);
					}
				}
			}

			if (built.to == null && built.from == null) {
				built.remainder = built.sentence;
			}
			return built;
		}

		/**
		 * @param remainingSentence
		 * @param tokens
		 * @return
		 */
		private SetView<String> tokensMatched(String remainingSentence, HashSet<String> tokens) {
			HashSet<String> remainingWords = words(remainingSentence);
			SetView<String> fromTokensFound = Sets.intersection(remainingWords, tokens);
			return fromTokensFound;
		}

		/**
		 * Provides a token as if it were starting the sentence.
		 * 
		 * @param fromTokenFound
		 * @return
		 */
		private String keywordAtStart(String fromTokenFound) {
			return fromTokenFound + WORD_DILIMETER;
		}

		/**
		 * Adds spaces around the keyword as if it were between words.
		 * 
		 * @param string
		 * @return
		 */
		private String keywordMidSentence(String string) {
			return WORD_DILIMETER + string + WORD_DILIMETER;
		}

		/**
		 * @param string
		 * @return
		 */
		private HashSet<String> words(String string) {
			return Sets.newHashSet(string.split("\\s+"));
		}

		private String cleanUp(String in) {
			return in.trim();
		}
	}// end Builder

	@XmlElement
	private final String sentence;
	@XmlElement
	private String from;
	@XmlElement
	private String to;
	@XmlElement
	private String remainder = "";

	/** When we split things up and found more than desired report it here. */
	@XmlElement
	private ArrayList<String> leftovers = new ArrayList<>();

	/** Use {@link Builder} to construct EndpointSentenceParser */
	private EndpointSentenceParser() {
		this("Required by XML");
	}

	private EndpointSentenceParser(String sentence) {
		this.sentence = sentence;
	}

	public static Builder create(String sentence) {
		return new Builder(sentence);
	}

	public String from() {

		return from;
	}

	public Boolean hasFrom() {
		return !Strings.isNullOrEmpty(from);
	}

	public String to() {
		return to;
	}

	public Boolean hasTo() {
		return !Strings.isNullOrEmpty(to);
	}

	/**
	 * @return the remainder
	 */
	public String remainder() {
		return this.remainder;
	}

	public Boolean hasRemainder() {
		return !Strings.isNullOrEmpty(remainder);
	}

	/**
	 * @return the leftovers
	 */
	public List<String> leftovers() {
		return this.leftovers;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "To: " + to + " from " + from;
	}
}
