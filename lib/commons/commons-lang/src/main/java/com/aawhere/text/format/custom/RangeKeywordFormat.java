package com.aawhere.text.format.custom;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.aawhere.collections.MapsExtended;
import com.aawhere.collections.RangeExtended;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.number.NumberUtilsExtended;
import com.aawhere.util.rb.MessageResourceBundle;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.util.rb.SelectParamKey;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Range;
import com.ibm.icu.text.PluralFormat;
import com.ibm.icu.text.PluralRules;

/**
 * Provides a keyword description of a number provided (or any comparable). 3 is a few, 23 is many,
 * 55 is lots,whatever keywords you wish to include. These keywords are intended to be used with the
 * Select Format of ICU, although it could in theory be used as a replacement to a number directly
 * by providing the localized version.
 * 
 * TODO: provide localization capabilitiies by providing a function that handles locale too.
 * 
 * Replaces {@link PluralFormat} since that has varying behavior based on language and attempts to
 * use the custom {@link PluralFormat} with {@link PluralRules} failed. Using our custom format and
 * assigning to a {@link ParamKey#getFormat()} works better giving us more control.
 * 
 * Helpful documentation: https://www.npmjs.com/package/messageformat
 * 
 * NOTE: This is better used as a {@link SelectParamKey} since a custom format should be used to
 * format the value and the selection should be done with a separpate effort. See
 * {@link MessageResourceBundle#formatMessage(com.aawhere.util.rb.CompleteMessage, Locale)}.
 * 
 * @author aroller
 * 
 */
@SuppressWarnings("rawtypes")
public class RangeKeywordFormat<C extends Comparable>
		implements CustomFormat<C>, Serializable {

	private Function<C, String> keywordFunction;
	private Class<? super C> handlesType;

	/**
	 * Used to construct all instances of NumberRangeKeywordFormat.
	 */
	public static class Builder<C extends Comparable>
			extends ObjectBuilder<RangeKeywordFormat<C>> {

		public Builder() {
			super(new RangeKeywordFormat<C>());
		}

		private Builder<C> keywordFunction(Function<C, String> keywordFunction) {
			building.keywordFunction = keywordFunction;
			return this;
		}

		private Builder<C> handlesType(Class<? super C> handlesType) {
			building.handlesType = handlesType;
			return this;
		}

		@Override
		public RangeKeywordFormat<C> build() {
			RangeKeywordFormat<C> built = super.build();
			return built;
		}

	}// end Builder

	public static <C extends Comparable> Builder<C> create() {
		return new Builder<C>();
	}

	/** Use {@link Builder} to construct NumberRangeKeywordFormat */
	private RangeKeywordFormat() {
	}

	/**
	 * These match the standard names for ICU.
	 * 
	 * @author aroller
	 * 
	 */
	public enum PluralKeyword {
		ZERO, ONE, TWO, FEW, MANY, OTHER;

		public String toKey() {
			return formatKeyword(this);
		}
	}

	@Override
	public Class<? super C> handlesType() {
		return handlesType;
	}

	@Override
	public String format(C input, Locale locale) {
		return this.keywordFunction.apply(input);
	}

	/**
	 * This uses the ICU standard numbers that match {@link PluralKeyword}.
	 * 
	 * @return
	 */
	public static RangeKeywordFormat<Double> standardDouble() {
		Function<Double, String> rangeFunction = standardRangeFunction();
		return RangeKeywordFormat.<Double> create().keywordFunction(rangeFunction).handlesType(Double.class).build();
	}

	private static Function<Double, String> standardRangeFunction() {
		HashMap<Range<Double>, String> ranges = Maps.newHashMap();
		ranges.put(Range.lessThan(1.0), formatKeyword(PluralKeyword.ZERO));
		ranges.put(Range.closedOpen(1.0, 2.0), formatKeyword(PluralKeyword.ONE));
		ranges.put(Range.closedOpen(2.0, 3.0), formatKeyword(PluralKeyword.TWO));
		ranges.put(Range.closedOpen(3.0, 11.0), formatKeyword(PluralKeyword.FEW));
		ranges.put(Range.closedOpen(11.0, 100.0), formatKeyword(PluralKeyword.MANY));
		ranges.put(Range.atLeast(100.0), formatKeyword(PluralKeyword.OTHER));
		Function<Double, String> rangeFunction = rangeFunction(ranges);
		return rangeFunction;
	}

	@SafeVarargs
	public static <C extends Comparable> Function<C, String> rangeFunction(Map.Entry<Range<C>, String>... ranges) {
		ImmutableMap<Range<C>, String> map = MapsExtended.asMap(Arrays.asList(ranges));
		return rangeFunction(map);
	}

	@SafeVarargs
	public static <C extends Comparable> RangeKeywordFormat<C> rangeKeywordFormat(Class<? super C> handlesType,
			Map.Entry<Range<C>, String>... ranges) {
		Function<C, String> rangeFunction = rangeFunction(ranges);
		return RangeKeywordFormat.<C> create().keywordFunction(rangeFunction).handlesType(handlesType).build();
	}

	public static RangeKeywordFormat<Integer> standardInteger() {
		Function<Integer, String> function = standardIntegerFunction();
		return RangeKeywordFormat.<Integer> create().keywordFunction(function).handlesType(Integer.class).build();
	}

	/**
	 * Transforms the integers into {@link PluralKeyword}
	 * 
	 * @return
	 */
	public static Function<Integer, String> standardIntegerFunction() {
		return Functions.compose(standardRangeFunction(), NumberUtilsExtended.doubleFromIntegerFunction());
	}

	private static String formatKeyword(Object object) {
		return object.toString().toLowerCase();
	}

	/**
	 * The range function given the map of ranges (provides the default of
	 * {@link PluralKeyword#OTHER}.
	 * 
	 * @see #rangeFunction(java.util.Map.Entry...)
	 * @param ranges
	 * @return
	 */
	public static <C extends Comparable> Function<C, String> rangeFunction(final Map<Range<C>, String> ranges) {
		return new RangeExtended.RangeFunction<C, String>(ranges, formatKeyword(PluralKeyword.OTHER));
	}

	private static final long serialVersionUID = -8463522252683991019L;

}
