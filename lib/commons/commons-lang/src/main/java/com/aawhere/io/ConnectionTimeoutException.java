/**
 * 
 */
package com.aawhere.io;

import java.io.IOException;

import org.joda.time.Duration;

import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * A server (not necessarily a Web server) is acting as a gateway or proxy to fulfil the request by
 * the client (e.g. your Web browser or our CheckUpDown robot) to access the requested URL. This
 * server did not receive a timely response from an upstream server it accessed to deal with your
 * HTTP request.
 * 
 * This usually means that the upstream server is down (no response to the gateway/proxy), rather
 * than that the upstream server and the gateway/proxy do not agree on the protocol for exchanging
 * data.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.GATEWAY_TIMEOUT)
public class ConnectionTimeoutException
		extends IoException {

	private static final long serialVersionUID = -6671910593725627915L;

	/**
	 * @param message
	 * @param e
	 */
	public ConnectionTimeoutException(Duration maxTimeAllowed, Object item, IOException cause) {
		super(new CompleteMessage.Builder(IoMessage.TIMEOUT)
				.addParameter(IoMessage.Param.MAX_TIME_ALLOWED, maxTimeAllowed)
				.addParameter(IoMessage.Param.ITEM, item).build(), cause);
	}

}
