/**
 * 
 */
package com.aawhere.net;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Useful messages for the Net package.
 * 
 * @author Aaron Roller
 * 
 */
public enum NetMessage implements Message {

	/** Explain_Entry_Here */
	GENERAL_FAILURE("An undefined failure occurred: {STATUS_CODE}.", Param.STATUS_CODE), GENERAL_FAILURE_FOR_PATH(
			"An undefined network failure occurred: {STATUS_CODE} for {PATH}.",
			Param.STATUS_CODE,
			Param.PATH),

	// HTTP Status Code names must match enums exactly as HttpStatusCode
	/** @see HttpStatusCode#NOT_FOUND */
	NOT_FOUND("Not found");

	private ParamKey[] parameterKeys;
	private String message;

	private NetMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
		// sneaky way to make sure custom format is registered.
		HttpStatusCustomFormat.register();
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/**
		 * The actual {@link HttpStatusCode} object which may be further personalized.
		 */
		STATUS_CODE,
		/** The URI attempting to be reached */
		PATH;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
