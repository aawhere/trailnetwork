/**
 * 
 */
package com.aawhere.collections;

import com.google.common.base.Function;

/**
 * Provides the cumulation of all values provided in an {@link Iterable} of {@link Integer} values.
 * 
 * @author aroller
 * 
 */
public class CumulativeIntegerFunction
		implements Function<Iterable<Integer>, Integer> {

	public static CumulativeIntegerFunction build() {
		return new CumulativeIntegerFunction();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Integer apply(Iterable<Integer> input) {
		int cumulative = 0;
		for (Integer integer : input) {
			cumulative += integer;
		}
		return cumulative;
	}

}
