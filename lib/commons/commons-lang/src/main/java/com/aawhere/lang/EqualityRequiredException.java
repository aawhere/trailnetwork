/**
 * 
 */
package com.aawhere.lang;

import com.aawhere.lang.EqualityRequiredExceptionMessage.Param;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * General exception when equality is required.
 * 
 * @author aroller
 * 
 * 
 */
@StatusCode(value = HttpStatusCode.BAD_REQUEST,
		message = "{FIRST_CONTEXT} must equal {SECOND_CONTEXT}, but were {FIRST_VALUE} and {SECOND_VALUE}.")
public class EqualityRequiredException
		extends InvalidArgumentException {

	private static final long serialVersionUID = 4812517968383262364L;

	/**
	 * 
	 * @param first
	 *            the arbitrary first argument of equality
	 * @param second
	 *            the arbitrary second
	 * @param context
	 */
	public EqualityRequiredException(Object first, Object second, Object firstContext, Object secondContext) {
		super(CompleteMessage.create(EqualityRequiredExceptionMessage.EQUALITY_REQUIRED)
				.param(Param.FIRST_CONTEXT, firstContext).param(Param.SECOND_CONTEXT, secondContext)
				.param(Param.FIRST_VALUE, first).param(Param.SECOND_VALUE, second).build());
	}

}
