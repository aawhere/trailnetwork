/**
 * 
 */
package com.aawhere.lang.enums;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class Enums<E extends Enum<E>> {

	@XmlAnyElement(lax = true)
	private ArrayList<E> all;

	/**
	 * Used to construct all instances of Enums.
	 */
	@XmlTransient
	public static class Builder<E extends Enum<E>>
			extends ObjectBuilder<Enums<E>> {

		private Builder(Enums<E> enums) {
			super(enums);
		}

		@Override
		public Enums<E> build() {
			Enums<E> built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct Enums */
	private Enums() {
	}

	public static <E extends Enum<E>> Builder<E> create(Class<E> enumClass) {
		Enums<E> enums = new Enums<E>();
		enums.all = Lists.newArrayList(enumClass.getEnumConstants());
		return new Builder<E>(enums);
	}

	public List<E> all() {
		return all;
	}
}
