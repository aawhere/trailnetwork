/**
 * 
 */
package com.aawhere.util.rb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;

/**
 * A collection of {@link CompleteMessage}s used to report {@link MessageStatus} to actors.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class StatusMessages
		implements Iterable<StatusMessage> {

	/**
	 * Used to construct all instances of StatusMessages.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<StatusMessages> {

		public Builder() {
			super(new StatusMessages());
		}

		public Builder addMessage(MessageStatus status, CompleteMessage message) {
			building.messages.add(new StatusMessage(status, message));
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public StatusMessages build() {
			building.messages = Collections.unmodifiableList(building.messages);
			return super.build();
		}

		public void addMessage(StatusMessage verbStatusMessage) {
			building.messages.add(verbStatusMessage);
		}

	}// end Builder

	/** Use {@link Builder} to construct StatusMessages */
	private StatusMessages() {
	}

	@XmlElement
	private List<StatusMessage> messages = new ArrayList<StatusMessage>();

	/**
	 * @return the messages as an unmodifiable {@link Map} where the status correlates to one or
	 *         more messages.
	 */
	@Nonnull
	public List<StatusMessage> getMessages() {
		return this.messages;
	}

	public Boolean hasError() {
		return has(MessageStatus.ERROR) || has(MessageStatus.FAILURE);
	}

	public Boolean has(MessageStatus status) {
		return !getMessagesByStatus(status).isEmpty();
	}

	/**
	 * 
	 * @param status
	 * @return the list of messages in the order added or an empty list if no messages assigned for
	 *         the given status.
	 */
	@Nullable
	public Collection<StatusMessage> getMessagesByStatus(MessageStatus status) {

		return Collections2
				.filter(this.messages, new StatusMessagePredicate.Builder().setMessageStatus(status).build());
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<StatusMessage> iterator() {
		return this.messages.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return this.messages.toString();
	}

	/**
	 * @return
	 */
	public static Builder create() {
		return new Builder();
	}

	/**
	 * @param messageKey
	 * @return the status message or null if not found
	 */
	public StatusMessage statusMessage(Message message) {
		return Iterables.getFirst(Iterables.filter(messages, MessageUtil.statusMessagePredicate(message)), null);
	}

}
