/**
 * 
 */
package com.aawhere.net;

import java.util.Locale;

import com.aawhere.lang.enums.EnumUtil;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.text.format.custom.CustomFormatFactory;
import com.aawhere.util.rb.MessageFormatter;

/**
 * A {@link CustomFormat} for {@link HttpStatusCode} to be prettified in messages.
 * 
 * @author Aaron Roller
 * 
 */
public class HttpStatusCustomFormat
		implements CustomFormat<HttpStatusCode> {

	private static boolean registered = false;

	/**
	 * Use {@link #register()} with {@link CustomFormatFactory}.
	 * 
	 */
	HttpStatusCustomFormat() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#format(java.lang.Object, java.util.Locale)
	 */
	@Override
	public String format(HttpStatusCode status, Locale locale) {

		NetMessage message = EnumUtil.valueOf(NetMessage.class, status.name());

		String result;
		if (message != null) {
			MessageFormatter formatter = new MessageFormatter.Builder().message(message).locale(locale).build();
			result = formatter.getFormattedMessage();
		} else {
			result = status.name();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#handlesType()
	 */
	@Override
	public Class<? super HttpStatusCode> handlesType() {
		return HttpStatusCode.class;
	}

	public static void register() {
		if (!registered) {
			CustomFormatFactory.getInstance().register(new HttpStatusCustomFormat());
			registered = true;
		}
	}
}
