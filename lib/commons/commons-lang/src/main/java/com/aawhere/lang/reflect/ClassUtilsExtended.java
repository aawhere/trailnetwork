/**
 *
 */
package com.aawhere.lang.reflect;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.lang3.ClassUtils;
import org.reflections.ReflectionUtils;

import com.aawhere.lang.ObjectBuilder;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Class reflection helpers not found in {@link Class}, {@link org.apache.commons.lang3.ClassUtils}
 * or {@link ReflectionUtils}.
 * 
 * @author Aaron Roller
 * 
 */
public class ClassUtilsExtended {

	private static ClassUtilsExtended instance;

	/**
	 * Used to retrieve the only instance of ClassUtilsExtended.
	 */
	public static ClassUtilsExtended getInstance() {

		if (instance == null) {
			instance = new ClassUtilsExtended();
		}
		return instance;

	}// end Builder

	/** Use {@link #getInstance()} to get ClassUtilsExtended */
	private ClassUtilsExtended() {
	}

	/**
	 * returns all super classes including itself.
	 * 
	 * @param youngestAncestor
	 * @return
	 */
	public List<Class<?>> getClassHeirarchy(Class<?> youngestAncestor) {
		List<Class<?>> all = ClassUtils.getAllSuperclasses(youngestAncestor);
		all.add(0, youngestAncestor);
		return all;
	}

	/**
	 * Looking for a child class, but only know the parent and a younger ancestor? This will crawl
	 * the heirachy returning the subclass of the targetParent.
	 * 
	 * @param youngestAncestor
	 * @param targetSubclass
	 * @return
	 */
	public Class<?> climbSupersForSubclass(Class<?> youngestAncestor, Class<?> targetParent) {

		List<Class<?>> classes = getClassHeirarchy(youngestAncestor);
		int indexOfTargetSubclass = classes.indexOf(targetParent);
		return classes.get(indexOfTargetSubclass - 1);
	}

	public Class<?> getBuilderClass(Class<?> type) throws ClassNotFoundException {
		Class<?> builderClass = null;
		Class<?>[] innerClasses = type.getClasses();
		for (int whichClass = 0; whichClass < innerClasses.length && builderClass == null; whichClass++) {
			Class<?> innerClass = innerClasses[whichClass];
			if (ObjectBuilder.class.isAssignableFrom(innerClass)) {
				builderClass = innerClass;
			}
		}
		return builderClass;
	}

	/**
	 * Searches the given class with fields for a field matching the given type.
	 * 
	 * @param classWithField
	 * @param fieldType
	 * @return
	 */
	public Set<Field> findFieldByType(Class<?> classWithField, Class<?> fieldType) {
		// replaced some code that ReflectionUtils can do.
		return ReflectionUtils.getAllFields(classWithField, ReflectionUtils.withType(fieldType));

	}

	/**
	 * A simple utility that makes a string out of all the constant String fields showing the name =
	 * value.
	 * 
	 * @param classWithStringConstants
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public List<String> fieldConstantsToString(Class<?> classWithStringConstants) throws IllegalArgumentException,
			IllegalAccessException {
		List<String> results = new ArrayList<String>();
		Set<Field> stringFields = ReflectionUtils.getAllFields(	classWithStringConstants,
																ReflectionUtils.withType(String.class));

		for (Field field : stringFields) {
			field.setAccessible(true);
			results.add(field.getName() + " = " + field.get(""));
		}
		return results;
	}

	/**
	 * finds the packages from the classes.
	 * 
	 * @param classes
	 * @return
	 */
	public static String[] getPackages(Class<?>[] classes) {

		String[] packages = new String[classes.length];
		for (int i = 0; i < classes.length; i++) {
			Class<?> class1 = classes[i];
			packages[i] = ClassUtils.getPackageName(class1);
		}
		return packages;
	}

	private enum SimpleNameFunction implements Function<Class<?>, String> {
		INSTANCE;
		@Override
		@Nullable
		public String apply(@Nullable Class<?> clazz) {
			return (clazz != null) ? clazz.getSimpleName() : null;
		}

		@Override
		public String toString() {
			return "SimpleNameFunction";
		}
	}

	public static Function<Class<?>, String> simpleNameFunction() {
		return SimpleNameFunction.INSTANCE;
	}

	public static Iterable<String> simpleNames(Iterable<? extends Class<?>> classes) {
		return Iterables.transform(classes, simpleNameFunction());
	}
}
