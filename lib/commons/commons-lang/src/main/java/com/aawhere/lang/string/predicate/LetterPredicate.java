/**
 * 
 */
package com.aawhere.lang.string.predicate;

import org.apache.commons.lang3.StringUtils;

/**
 * Inspects the characters and returns true if the characters are letters.
 * 
 * @author roller
 * 
 */
public class LetterPredicate
		extends CharSequencePredicate {

	private static LetterPredicate instance;

	/**
	 * 
	 */
	private LetterPredicate() {

	}

	public static LetterPredicate getInstance() {
		if (instance == null) {
			instance = new LetterPredicate();
		}
		return instance;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.aawhere.lang.string.predicate.CharSequencePredicate#evaluate(java
	 *      .lang.CharSequence)
	 */
	@Override
	public boolean evaluate(CharSequence target) {
		return StringUtils.isAlpha(target);
	}

}
