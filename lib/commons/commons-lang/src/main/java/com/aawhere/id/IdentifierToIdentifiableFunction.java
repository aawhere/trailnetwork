/**
 * 
 */
package com.aawhere.id;

import com.google.common.base.Function;

/**
 * Shortcut for using a common function of retrieving an {@link Identifiable} from an
 * {@link Identifier}. The implementation would need to use some sort of repository to look up the
 * {@link Identifiable}.
 * 
 * @author aroller
 * 
 */
public interface IdentifierToIdentifiableFunction<F extends Identifier<?, T>, T extends Identifiable<F>>
		extends Function<F, T> {

}
