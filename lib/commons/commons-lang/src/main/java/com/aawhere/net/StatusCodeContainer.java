/**
 * 
 */
package com.aawhere.net;

/**
 * A general way to access any object that may provide a {@link HttpStatusCode} dynamically.
 * 
 * Annotations are related directly to the class, but this approach is more flexible at runtime.
 * 
 * @author Aaron Roller
 * 
 */
public interface StatusCodeContainer {

	/**
	 * Provides the {@link HttpStatusCode} for a response from a request.
	 * 
	 * @return
	 */
	HttpStatusCode getStatusCode();
}
