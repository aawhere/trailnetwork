/**
 * 
 */
package com.aawhere.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Anything that {@link Arrays} and {@link ArrayUtils} doesn't already provide.
 * 
 * @author roller
 * 
 */
public class ArrayUtilsExtended {

	/**
	 * 
	 * @param array
	 *            the array of interest
	 * @return the last element of the array. Only returns null if the last element is set to null.
	 * @throws IndexOutOfBoundsException
	 *             when the array is empty. use {@link ArrayUtils#isEmpty(boolean[])} to avoid this
	 */
	@Nonnull
	public static <T> T last(T[] array) {
		return array[array.length - 1];
	}

	/**
	 * @param array
	 * @return
	 */
	public static Integer indexOfLast(Object[] array) {

		return array.length - 1;
	}

	/**
	 * Given the array this will return a List of subarrays limited to the maxLength. The final
	 * array will be the appropriate size to ensure to total number of elements will remain the same
	 * as the original.
	 * 
	 * If an empty array is given, this will return an empty list.
	 * 
	 * @param array
	 *            array of any length
	 * @param maxLength
	 *            the maximum number of elements for any given array
	 * @return a List of arrays at the max length (a matrix of sorts).
	 * 
	 * @param <T>
	 *            any object is fine
	 */
	public static <T> List<T[]> split(T[] array, Integer maxLength) {

		Integer numberOfSplits = (array.length / maxLength);
		Integer finalSplitLength = array.length % maxLength;
		ArrayList<T[]> segments = new ArrayList<T[]>(numberOfSplits + 1);
		for (int i = 0; i < numberOfSplits; i++) {
			int begin = i * maxLength;
			int end = begin + maxLength;
			T[] segment = Arrays.copyOfRange(array, begin, end);
			segments.add(segment);
		}
		if (finalSplitLength > 0) {
			segments.add(Arrays.copyOfRange(array, numberOfSplits * maxLength, array.length));
		}
		return segments;
	}
}
