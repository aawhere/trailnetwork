/**
 * 
 */
package com.aawhere.io.zip;

import com.aawhere.io.IoMessage;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

/**
 * Indicates the data stream claims to be a GZIP file (based on
 * {@link GzipCompression#isCompressed(byte[])}, but uncompressions causes an error.
 * 
 * @author Aaron Roller
 * 
 */
@StatusCode(HttpStatusCode.UNPROCESSABLE_ENTITY)
public class CorruptGzipFormatException
		extends BaseException {

	private static final long serialVersionUID = -6846747904161438954L;

	/**
	 * Default constructor providing the default message with no parameters because there is no
	 * obvious parameters needed.
	 * 
	 */
	public CorruptGzipFormatException() {
		super(IoMessage.GZIP_CORRUPTION);
	}
}
