/**
 *
 */
package com.aawhere.lang.enums;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.lang.InvalidChoiceException;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;

/**
 * Useful methods against {@link Enum}s.
 * 
 * @author Aaron Roller
 * 
 */
public class EnumUtil {

	/**
	 * Returns the Enum represented by given name for the given Enum class. This is exactly the same
	 * as {@link Enum#valueOf(Class, String)}, however, instead of throwing the non-obvious runtime
	 * this returns a null.
	 * 
	 * 
	 * @param enumType
	 * @param name
	 * @return
	 */
	@Nullable
	public static <E extends Enum<E>> E valueOf(@Nonnull Class<E> enumType, @Nonnull String name) {
		E result;
		try {
			result = Enum.valueOf(enumType, name);

		} catch (IllegalArgumentException e) {
			if (EnumWithDefault.class.isAssignableFrom(enumType)) {
				@SuppressWarnings("unchecked")
				EnumWithDefault<E> any = (EnumWithDefault<E>) enumType.getEnumConstants()[0];
				result = any.getDefault();
			} else {
				// NOTE: Catching Runtimes is bad...isolate this common scenario here.
				result = null;
			}
		}
		return result;
	}

	public static <E extends Enum<E>> E valueOfStrict(Class<E> enumType, String name) {
		E valueOf = valueOf(enumType, name);
		if (valueOf == null) {
			E[] enumConstants = enumType.getEnumConstants();
			throw new InvalidChoiceException(valueOf, (Object[]) enumConstants);
		}
		return valueOf;
	}

	/**
	 * Given two similar enumerations that have the same {@link Enum#name()} this will return an the
	 * instance of the second that matches the name of the first using the
	 * {@link Enum#valueOf(Class, String)}. A runtime will be thrown if no name match is made. Null
	 * will be returned if e1 is null.
	 * 
	 * @param e1
	 * @param e2
	 * @return
	 */
	public static <E1 extends Enum<E1>, E2 extends Enum<E2>> E2 convert(E1 e1, Class<E2> e2Type) {
		if (e1 == null) {
			return null;
		} else {
			return Enum.valueOf(e2Type, e1.name());
		}

	}

	/**
	 * Used to return the value of a sibling of the given class of an enum. This is a simple
	 * convenience method providing casting.
	 * 
	 * If Junk is the enum, then enumType is Junk.A.
	 * 
	 * @see EnumUtil#valueOf(Class, String)
	 * 
	 * @param enumType
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <E extends Enum<E>> E valueOfEnum(Class<? extends E> enumType, String name) {
		return valueOf((Class<E>) enumType, name);
	}

	public static <E extends Enum<E>> E enumFrom(Class<E> enumClass) {
		return enumClass.getEnumConstants()[0];
	}

	public static String qualifiedName(Object o) {
		Class<? extends Object> clazz = o.getClass();
		String result;
		if (clazz.isEnum()) {
			result = qualifiedName((Enum) o);
		} else {
			result = clazz.getName();
		}
		return result;
	}

	public static <E extends Enum<E>> String qualifiedName(E e) {

		if (e == null) {
			return null;
		}
		StringBuilder result = new StringBuilder().append(e.getClass().getEnclosingClass().getName());
		final String separator = ".";
		result.append(separator);
		result.append(e.getClass().getSimpleName());
		result.append(separator);
		result.append(e.name());
		return result.toString();
	}

	/**
	 * Return the enum logically before the one provided or null if it is the first enum.
	 * 
	 * @param enumeration
	 */
	public static <E extends Enum<E>> E getPrevious(E e) {
		E[] values = getValues(e);
		return getPrevious(e, values);
	}

	/**
	 * Return the enum logically after the one provided or null if it is the last enum.
	 * 
	 * @param enumeration
	 */
	public static <E extends Enum<E>> E getNext(E e) {
		E[] values = getValues(e);
		ArrayUtils.reverse(values);
		return getPrevious(e, values);
	}

	/**
	 * get enum previous in the ordered list of values.
	 * 
	 * @param e
	 * @param values
	 * @return
	 */
	private static <E extends Enum<E>> E getPrevious(E e, E[] values) {
		E previous = null;
		for (E i : values) {
			if (i.equals(e)) {
				break;
			} else {
				previous = i;
			}
		}
		return previous;
	}

	@SuppressWarnings("unchecked")
	public static <E extends Enum<E>> E[] getValues(E e) {
		return (E[]) getValues(e.getClass());
	}

	@SuppressWarnings("unchecked")
	public static <E extends Enum<E>> E[] getValues(Class<E> e) {
		Method m;
		try {
			m = e.getMethod("values");

			return (E[]) m.invoke(null);
		} catch (SecurityException e1) {
			throw new RuntimeException(e1);
		} catch (NoSuchMethodException e1) {
			throw new RuntimeException(e1);
		} catch (IllegalArgumentException e1) {
			throw new RuntimeException(e1);
		} catch (IllegalAccessException e1) {
			throw new RuntimeException(e1);
		} catch (InvocationTargetException e1) {
			throw new RuntimeException(e1);
		}
	}

	/**
	 * Provides the smaller of the two. See {@link Ordering#min(Object, Object)}
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static <E extends Enum<E>> E min(E left, E right) {
		return Ordering.natural().min(left, right);
	}

	public static <E extends Enum<E>> Function<E, String> nameFunction() {
		return new Function<E, String>() {

			@Override
			public String apply(E input) {
				return (input != null) ? input.name() : null;
			}
		};
	}

	public static <E extends Enum<E>> List<String> names(Class<? extends E> enumClass) {
		E[] enumConstants = enumClass.getEnumConstants();
		List<E> list = Arrays.asList(enumConstants);
		final Function<E, String> nameFunction = nameFunction();
		return Lists.transform(list, nameFunction);
	}
}
