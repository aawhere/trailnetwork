/**
 * 
 */
package com.aawhere.collections;

import java.util.Map;

import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Function;
import com.google.common.base.Functions;

/**
 * @author aroller
 * 
 */
public class FunctionsExtended {

	/**
	 * Practically does nothing since it only casts to the type you wish, but this helps get through
	 * generics that aren't setup properly.
	 * 
	 * <code>
	 * Function<A, B> firstFunction = ...;
		final Function<Object, C> castFunction = FunctionsExtended.castFunction();
		Function<A, C> castedFunction = Functions.compose(castFunction,firstFunction);
		
		where B can be cast into C.
																								
	 * </code>
	 * 
	 * @return
	 */
	public static <T> Function<Object, T> castFunction() {
		return new Function<Object, T>() {

			@SuppressWarnings("unchecked")
			@Override
			public T apply(Object input) {
				return (T) input;
			}
		};
	}

	/**
	 * Just like {@link Functions#forMap(Map)}, but if the value doesn't exist it just passes it
	 * through since the key and value must be the same type. Null is just passed through.
	 * 
	 * @param map
	 * @return
	 */
	public static <T> Function<T, T> forMap(final Map<T, T> map) {
		return new Function<T, T>() {

			@Override
			public T apply(T input) {
				if (input == null) {
					return null;
				}
				T result = map.get(input);
				return (result == null) ? input : result;
			}
		};
	}

	/**
	 * returns the constant given in the method parameter. {@link Functions#constant(Object)} 
	 * doesn't declare generics resulting in problems with certain functions
	 * 
	 * @param to
	 * @return
	 */
	public static <F, T> Function<F, T> constant(final T to) {
		return new Function<F, T>() {

			@Override
			public T apply(F input) {
				return to;
			}
		};

	}

	private enum StringEmptyFunction implements Function<String, String> {
		INSTANCE;
		@Override
		@Nullable
		public String apply(@Nullable String input) {
			return (input != null) ? input : StringUtils.EMPTY;
		}

		@Override
		public String toString() {
			return "StringEmptyFunction";
		}
	}

	/**
	 * Never returns null. If the value is null then an empty string is returned.
	 * 
	 * @return
	 */
	public static Function<String, String> stringEmptyFunction() {
		return StringEmptyFunction.INSTANCE;
	}

	/**
	 * Composes the given string producing function with {@link #stringEmptyFunction()} to ensure
	 * null is never returned. Usseful for presentation layer.
	 * 
	 * @param nullableStringFunction
	 * @return
	 */
	public static <F> Function<F, String> stringEmptyFunction(Function<F, String> nullableStringFunction) {
		return Functions.compose(stringEmptyFunction(), nullableStringFunction);
	}
}
