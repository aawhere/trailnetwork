/**
 *
 */
package com.aawhere.lang;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.annotation.Nonnull;

import org.springframework.util.ClassUtils;

import com.aawhere.io.FileNotFoundException;
import com.aawhere.lang.exception.ToRuntimeException;

/**
 * Useful methods to find resources for loading files in the classpath.
 * 
 * @see ClassLoader#getResource(String)
 * 
 * @author roller
 * 
 */
public class ResourceUtils {

	/**
	 * Encourages standard configuration when a resource is one to one with a Class.
	 * 
	 * Example: targetClass = ResourceUtils and filenameSuffix = xml
	 * 
	 * URL = .../com/aawhere/lang/ResourceUtils.xml
	 * 
	 * @param targetClass
	 *            provides the simpleName that will be used for the resource.
	 * @param filenameSuffix
	 *            appended to resource name with a period separator.
	 * @return the URL to the resource.
	 */
	@Nonnull
	public static URL resourceFromClass(Class<?> targetClass, String filenameSuffix) {
		return resourceFromPackage(targetClass, targetClass.getSimpleName() + "." + filenameSuffix);
	}

	/**
	 * 
	 * @param classWithTargetPackage
	 * @param filename
	 * @return
	 */
	@Nonnull
	public static URL resourceFromPackage(Class<?> classWithTargetPackage, String filename) {
		return getResource(ClassUtils.addResourcePathToPackagePath(classWithTargetPackage, filename));
	}

	/**
	 * 
	 * @param resourceName
	 * @return
	 * 
	 */
	@Nonnull
	public static URL getResource(String resourceName) {
		URL result = Thread.currentThread().getContextClassLoader().getResource(resourceName);
		if (result == null) {
			throw new MissingResourceException(resourceName, resourceName, resourceName);
		}
		return result;
	}

	/**
	 * 
	 * @param resourceName
	 * @return
	 * 
	 */
	@Nonnull
	public static ResourceBundle getResourceBundle(String baseName, Locale locale) {
		ResourceBundle result = ResourceBundle.getBundle(baseName, locale, Thread.currentThread()
				.getContextClassLoader());
		if (result == null) {
			throw new MissingResourceException("Could not find resource " + baseName + " for Locale "
					+ locale.toString(), baseName, baseName);
		}
		return result;
	}

	/**
	 * Helper method to use standard java to find a file stream while encouraging a naming standard.
	 * 
	 * targetClass.getSimpleName() + resourceNameSuffix
	 * 
	 * So given the class IoUtil.class for targetClass and "Constants.properties" it would look for
	 * a resource
	 * 
	 * com/aawhere/io/IoUtilConstants.properties
	 * 
	 * Unlike the {@link Class#getResourceAsStream(String)} who returns a non-descriptive null when
	 * not found, this will explain which file is not found so you may have an easier time looking
	 * for the missing file. It is a runtime since resources are not good dynamic resources.
	 * 
	 * @param targetClass
	 * @param resourceNameSuffix
	 * @return the {@link InputStream}
	 * @throws ToRuntimeException
	 *             of a {@link FileNotFoundException} when the file is not found.
	 */
	public static InputStream getResourceStream(Class<?> targetClass, String resourceNameSuffix) {
		String resourceName = targetClass.getSimpleName() + resourceNameSuffix;
		InputStream stream = targetClass.getResourceAsStream(resourceName);
		if (stream == null) {
			throw new ToRuntimeException(new com.aawhere.io.FileNotFoundException(resourceName));
		}
		return stream;
	}

	/**
	 * Loads {@link Properties} from a resource matching the class name.
	 * 
	 * Given IoUtils.class this will look for a resource in the same package name IoUtils.properties
	 * 
	 * @see #getResource(Class, String)
	 * 
	 * @param targetClass
	 * @return the Properties
	 * @throws RuntimeException
	 *             when the resource is not found or not able to be read.
	 */
	public static Properties loadProperties(Class<?> targetClass) {
		Properties properties = new Properties();
		try {
			properties.load(getResourceStream(targetClass, ".properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return properties;
	}
}
