/**
 * 
 */
package com.aawhere.lang;

import java.util.Comparator;

import javax.annotation.Nullable;

import org.apache.commons.lang3.BooleanUtils;

/**
 * A glaring whole in the NO MAGIC VARIABLE rule in java for {@link Comparator} results.
 * 
 * This defines the three possible return values instead of having to read the docs every time!
 * 
 * @author Aaron Roller
 * 
 */
public enum ComparatorResult {
	/** Standard use in standard terms */
	LESS_THAN(-1), EQUAL(0),

	GREATER_THAN(1),
	/** More readable to choose the left value when natural ordering. */
	LEFT(LESS_THAN.getValue()),
	/** When the right operand is to be chosen during natural ordering. */
	RIGHT(GREATER_THAN.getValue());

	public final int value;

	/**
	 * 
	 */
	private ComparatorResult(int result) {
		this.value = result;
	}

	/**
	 * @return the result
	 */
	public int getValue() {
		return this.value;
	}

	public static Boolean isLessThan(int result) {
		return result <= LESS_THAN.value;
	}

	public static Boolean isGreaterThan(int result) {
		return result >= GREATER_THAN.value;
	}

	public static Boolean isEqualTo(int result) {
		return result == EQUAL.value;
	}

	public static Boolean isLessThanOrEqualTo(int result) {
		return isLessThan(result) || isEqualTo(result);
	}

	public static Boolean isGreaterThanOrEqualTo(int result) {
		return isGreaterThan(result) || isEqualTo(result);
	}

	/**
	 * Useful when you wish for the opposite result reversing the natural order.
	 * 
	 * @param compareTo
	 * @return
	 */
	public static int reverse(int compareTo) {
		return -compareTo;
	}

	/**
	 * Often times preference can be given by a true or false. This centralizes that common logic by
	 * providing the left preferred if the left is preferred and the right is not, vise versa or
	 * equal if both are preferred or not preferred. Null values are treated equally with false,
	 * although it may be useful to provide other methods that provide a preference in the null
	 * cases.
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static int binary(@Nullable Boolean left, @Nullable Boolean right) {
		boolean leftPreferred = BooleanUtils.isTrue(left);
		boolean rightPreferred = BooleanUtils.isTrue(right);

		if (leftPreferred && !rightPreferred) {
			return ComparatorResult.LEFT.value;
		} else if (rightPreferred && !leftPreferred) {
			return ComparatorResult.RIGHT.value;
		} else {
			return ComparatorResult.EQUAL.value;
		}
	}
}
