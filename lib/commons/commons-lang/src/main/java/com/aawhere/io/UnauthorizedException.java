/**
 * 
 */
package com.aawhere.io;

import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Simply communicates the standard {@link HttpStatusCode#UNAUTHORIZED} details.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.UNAUTHORIZED)
public class UnauthorizedException
		extends IoException {

	private static final long serialVersionUID = 3464473631487188223L;

	/**
	 * Explains that the url cannot be accessed.
	 * 
	 */
	public UnauthorizedException(String url) {
		super(new CompleteMessage.Builder(IoMessage.UNAUTHORIZED).addParameter(IoMessage.Param.URL, url).build());
	}

}
