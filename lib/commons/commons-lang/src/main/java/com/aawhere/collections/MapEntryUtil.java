/**
 * 
 */
package com.aawhere.collections;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multiset;

/**
 * Also works with {@link Pair}. Some of this may be duplicated from {@link Maps}, but it's not
 * obvious. Redirect if found.
 * 
 * @author aroller
 * 
 */
public class MapEntryUtil {

	/**
	 * Given a pair of the same type this returns a list contianing the left,Vight in that order.
	 * 
	 * @param pair
	 * @return
	 */
	public static <E> List<E> list(Entry<E, E> pair) {

		return Arrays.asList(pair.getKey(), pair.getValue());
	}

	/**
	 * Returns all values retrieved from the iterable of entries.
	 * 
	 * @param allPairs
	 */
	public static <K, V> Iterable<V> values(Iterable<? extends Entry<K, V>> allPairs) {
		return Iterables.transform(allPairs, MapEntryUtil.<K, V> valueFunction());
	}

	public static <K, V> Function<Entry<K, V>, K> keyFunction() {
		return new Function<Entry<K, V>, K>() {

			@Override
			public K apply(Entry<K, V> input) {
				return (input != null) ? input.getKey() : null;
			}
		};
	}

	public static <K, V> Function<Entry<K, V>, V> valueFunction() {
		return new Function<Entry<K, V>, V>() {

			@Override
			public V apply(Entry<K, V> input) {
				return (input != null) ? input.getValue() : null;
			}
		};
	}

	public static <K, V> Iterable<K> keys(Iterable<Pair<K, V>> allPairs) {
		return Iterables.transform(allPairs, MapEntryUtil.<K, V> keyFunction());
	}

	/**
	 * 
	 * @param allPairs
	 */
	public static <K, V> KeySetPredicate<K, V> keySetPredicate() {
		return new KeySetPredicate<K, V>();
	}

	/**
	 * * A stateful predicate that when used with filtering it will report the keyset after
	 * iteration. All entries return true;
	 * 
	 * @author aroller
	 * 
	 * @param <K>
	 * @param <V>
	 */
	public static class KeySetPredicate<K, V>
			implements Predicate<Map.Entry<K, V>> {
		final Multiset<K> keys = HashMultiset.create();

		@Override
		public boolean apply(Entry<K, V> input) {
			keys.add(input.getKey());
			return true;
		}

		public Set<K> keySet() {
			return keys.elementSet();
		}

		/**
		 * Returns a live copy of the {@link #keys} {@link Multiset} which will provide updates
		 * during iteration, but also allow for mutation by the client which should be avoided.
		 * 
		 * @return
		 */
		public Multiset<K> keys() {
			return keys;
		}
	}
}
