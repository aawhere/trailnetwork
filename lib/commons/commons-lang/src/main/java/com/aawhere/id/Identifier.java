/**
 *
 */
package com.aawhere.id;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.xml.XmlNamespace;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Identifier is used to uniquely identify a Data Entity within the system. This is done by
 * containing a unique value such as a number or string in addition to the Class or Kind the
 * Identifier represents.
 * 
 * TODO:Make this abstract...it will make everything a lot easier because of JAXB and other
 * reflective frameworks.
 * 
 * @see http://aawhere.jira.com/wiki/display/AAWHERE/Identifiers
 * 
 *      NOTE: It would seem that @XmlValue would be perfect on the {@link #value} field, but it has
 *      restrictions that make it not useful. @XmlValue does not work with a generic value. You must
 *      use a concrete extenions of one of the XML compatible classes like {@link StringIdentifier}
 *      or {@link LongIdentifier}.
 * 
 * 
 *      Making each class in the heiarchy transient causes a problem.
 * 
 * @param <ValueT>
 *            the type that the value will be (Long, String, etc)
 * @param <KindT>
 *            the entity this identifier represents. Person, Place, Thing, etc.
 * @author roller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
abstract public class Identifier<ValueT extends Comparable<ValueT>, KindT>
		implements Serializable, Comparable<Identifier<ValueT, KindT>> {

	private static final long serialVersionUID = -8213581043023690251L;

	/** The actual value of the id */
	/** Provided for type safety, this is the class that this id represents. */
	@ApiModelProperty(hidden = true, notes = "this isn't useful outside of java")
	private Class<KindT> kind;

	/**
	 *
	 */
	public Identifier(@Nonnull ValueT value, @Nonnull Class<KindT> kind) {
		this(kind);
		InvalidArgumentException.notNull(value, new StringMessage("value"));
		InvalidArgumentException.notNull(kind, new StringMessage("key"));
	}

	public Identifier(@Nonnull Identifier<ValueT, KindT> other) {
		this(other.getValue(), other.getKind());
	}

	// Required by persistance engine.
	protected Identifier() {
	};

	/**
	 * @param kind2
	 */
	public Identifier(Class<KindT> kind) {
		this.kind = kind;
		Assertion.assertNotNull("kind", kind);

	}

	/**
	 * The single value that represents the identifier.
	 * 
	 * @return the value
	 */
	abstract public ValueT getValue();

	/** Identifies the Identifiable Class this Identifier represents. */
	public Class<KindT> getKind() {
		return this.kind;
	}

	void setKind(Class<KindT> kind) {
		this.kind = kind;
	}

	/*
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.kind == null) ? 0 : this.kind.hashCode());
		result = prime * result + ((this.getValue() == null) ? 0 : this.getValue().hashCode());
		if (IdentifierUtils.hasParent(this)) {
			result = prime * result + ((HasParent<?>) this).getParent().hashCode();
		}
		return result;
	}

	/*
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identifier<?, ?> other = (Identifier<?, ?>) obj;
		if (this.kind == null) {
			if (other.kind != null)
				return false;
		} else if (!this.kind.equals(other.kind))
			return false;
		if (this.getValue() == null) {
			if (other.getValue() != null)
				return false;
		} else if (!this.getValue().equals(other.getValue())) {
			return false;
		} else if (!IdentifierUtils.hasParent(other).equals(IdentifierUtils.hasParent(this))) {
			// both must agree about having parents or not
			return false;
		} else if (IdentifierUtils.hasParent(this)) {
			// already did agreement check so other has parent too
			Identifier<?, ?> myParent = ((HasParent<Identifier<?, ?>>) this).getParent();
			Identifier<?, ?> theirParent = ((HasParent<Identifier<?, ?>>) other).getParent();
			if (!myParent.equals(theirParent)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Provides a string representation that identifies the kind with the value. Includes a parent
	 * if this implements {@link HasParent}.
	 * 
	 * @return
	 */
	@ApiModelProperty(hidden = true)
	public String getQualifiedString() {
		String parentString;
		// only add the parent string if there is a parent
		if (this instanceof HasParent && !((HasParent<?>) this).isRoot()) {
			parentString = ((HasParent<?>) this).getParent().getQualifiedString() + ",";
		} else {
			parentString = StringUtils.EMPTY;
		}
		return new StringBuilder().append(parentString).append(getKind().getSimpleName()).append("(")
				.append(getValue()).append(")").toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = String.valueOf(getValue());
		if (IdentifierUtils.hasParent(this)) {
			Identifier<?, ?> parent = ((HasParent<?>) this).getParent();
			if (parent != null) {
				result = IdentifierUtils.createCompositeIdValue(parent, result);
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Identifier<ValueT, KindT> that) {
		return this.getValue().compareTo(that.getValue());
	}
}
