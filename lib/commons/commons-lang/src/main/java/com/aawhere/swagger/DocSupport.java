/**
 * 
 */
package com.aawhere.swagger;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.enums.EnumUtil;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Static class intended for static import to assist in providing constants that satisfy the
 * requirements of Swagger annoations.
 * 
 * @see Api
 * @author aroller
 * 
 */
public class DocSupport {

	/**
	 * @see ApiOperation#responseContainer()
	 * @author aroller
	 * 
	 */
	public static final class ResponseContainer {
		public static final String LIST = "List";
		public static final String ARRAY = "Array";
		public static final String SET = "Set";
	}

	/**
	 * @see ApiModelProperty#allowableValues()
	 * @author aroller
	 * 
	 */
	public static final class Allowable {
		public static final String OPEN = "[";
		public static final String CLOSE = "]";
		public static final String NEXT = ",";
	}

	public static final class Required {
		public static final Boolean TRUE = Boolean.TRUE;
		public static final Boolean FALSE = Boolean.FALSE;
	}

	public static final class AllowMultiple {
		public static final Boolean TRUE = Boolean.TRUE;
		public static final Boolean FALSE = Boolean.FALSE;
	}

	/**
	 * @see ApiImplicitParam#paramType()
	 * @author aroller
	 * 
	 */
	public static final class ParamType {
		public static final String PATH = "path";
		public static final String QUERY = "query";
		public static final String HEADER = "header";
	}

	public static final class ParamAccess {
		/** Put this in the access attribute of Param fields to hide from everyone */
		public static final String HIDDEN = "hidden";
		/** Intended for developers */
		public static final String INTERNAL = "internal";
	}

	/** https://github.com/swagger-api/swagger-core/wiki/Datatypes */
	public static final class DataType {
		public static final String STRING = "string";
		public static final String INTEGER = "integer";
		public static final String BOOLEAN = "boolean";
		/** Not really necessary because AllowMultiple is what determines if it is an array or not */
		public static final String STRING_ARRAY = STRING;
	}

	/**
	 * Provides the allowable string syntax for enums by returning the names of each enum.
	 * 
	 * This can be extended to receive a function and transform the enumerations into any string.
	 * 
	 * @param enumClass
	 * @return
	 */
	public static <E extends Enum<E>> String allowable(Class<E> enumClass) {
		return StringUtils.join(EnumUtil.names(enumClass), Allowable.NEXT);
	}

	/**
	 * Creates the allowable list when given a collection of any object. It will toString the
	 * object.
	 */
	public static String allowable(Iterable<?> items) {
		return StringUtils.join(items, Allowable.NEXT);
	}
}
