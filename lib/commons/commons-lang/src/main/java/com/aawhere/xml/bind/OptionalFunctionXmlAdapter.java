/**
 * 
 */
package com.aawhere.xml.bind;

import com.google.common.base.Function;

/**
 * An {@link OptionalXmlAdapter} that delegates marshaling to the given {@link Function}.
 * 
 * @author aroller
 * 
 */
public class OptionalFunctionXmlAdapter<X, T>
		extends OptionalXmlAdapter<X, T> {

	final private Function<T, X> function;

	/**
	 * @param show
	 */
	public OptionalFunctionXmlAdapter(Function<T, X> function, Boolean show) {
		super(show);
		this.function = function;
	}

	/**
	 * @param visibility
	 */
	public OptionalFunctionXmlAdapter(Function<T, X> function, OptionalXmlAdapter.Visibility visibility) {
		super(visibility);
		this.function = function;
	}

	/**
	 * @param show
	 */
	public OptionalFunctionXmlAdapter(Function<T, X> function, OptionalXmlAdapter.Show show) {
		super(show);
		this.function = function;
	}

	/**
	 * @param hide
	 */
	public OptionalFunctionXmlAdapter(Function<T, X> function, OptionalXmlAdapter.Hide hide) {
		super(hide);
		this.function = function;
	}

	/**
	 * 
	 */
	public OptionalFunctionXmlAdapter() {
		this.function = null;
	}

	/*
	 * @see com.aawhere.xml.bind.OptionalXmlAdapter#showMarshal(java.lang.Object)
	 */
	@Override
	protected X showMarshal(T v) throws Exception {
		// let the function determine what to do with nulls
		return (this.function != null) ? this.function.apply(v) : null;
	}

}
