/**
 *
 */
package com.aawhere.collections.apache;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.set.ListOrderedSet;

import com.aawhere.collections.IndexResult;
import com.aawhere.lang.ObjectBuilder;

/**
 * Provides missing methods for the {@link ListOrderedSet}. It isn't a list so we can't use general
 * List Utils. Helps with generics too.
 * 
 * Since OrderedSet isn't generic we put the SuppressWarnings("unchecked") on the whole class since
 * it will be everywhere.
 * 
 * Using Builder style modifers are available during building and accessors available after.
 * 
 * @author Aaron Roller
 * 
 */

@SuppressWarnings("unchecked")
public class ListOrderedSetExtended<E> {

	/**
	 * Used to construct all instances of ListOrderedSetUtils.
	 */
	public static class Builder<E>
			extends ObjectBuilder<ListOrderedSetExtended<E>> {

		public Builder() {
			super(new ListOrderedSetExtended<E>());
		}

		/**
		 * Adds the element or returns the existing element if already found in the list.
		 * 
		 * @param e
		 * @return the found element or null if not already in the list
		 */
		public E add(E e) {
			E result;
			if (!building.set.add(e)) {
				result = building.get(e);
			} else {
				result = null;
			}
			return result;
		}
	}// end Builder

	/** Use {@link Builder} to construct ListOrderedSetUtils */
	private ListOrderedSetExtended() {
	}

	private ListOrderedSet set = new ListOrderedSet();

	/**
	 * Gets the element that equals the element given. Remeber, they may not be the "same", but they
	 * are "equals" according to the rules of a set.
	 * 
	 * @param e
	 * @return
	 */
	public E get(E e) {
		E result;
		int index = this.set.indexOf(e);
		if (index != IndexResult.NOT_FOUND.getIndex()) {
			result = (E) this.set.get(index);
		} else {
			result = null;
		}
		return result;
	}

	/** Unmodifiable list */
	public List<E> asList() {
		return this.set.asList();
	}

	/** Unmodifiable set */
	public Set<E> asSet() {
		return Collections.unmodifiableSet(this.set);
	}

}
