/**
 *
 */
package com.aawhere.lang.reflect;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Set;

import org.reflections.ReflectionUtils;

import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.annotation.AnnotationUtilsExtended;

/**
 * Does the dirty work dealing with reflection.
 * 
 * @see ReflectionUtils
 * @see ClassUtilsExtended
 * @see AnnotationUtilsExtended
 * 
 * @author aroller
 * 
 */
public class ReflectionUtilsExtended {

	/**
	 * Get's the only field available on a class. If it isn't found or more than one is found ends
	 * in a runtime exception.
	 * 
	 * TODO: Test me!!!!
	 * 
	 * @param type
	 * @param fieldName
	 * @return
	 */
	public static Field getField(Class<?> type, String fieldName) {
		Set<Field> allFields = ReflectionUtils.getAllFields(type, ReflectionUtils.withName(fieldName));
		if (allFields.size() != 1) {
			throw new IllegalArgumentException(fieldName + " is found " + allFields.size() + " times on " + type
					+ " : " + allFields);
		}
		return allFields.iterator().next();
	}

	/**
	 * Given a member this will return the type that member declares.
	 * 
	 * 
	 * @param key
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> typeFor(Member member) {
		Class<T> result;
		if (member instanceof Field) {
			result = (Class<T>) ((Field) member).getType();
		} else if (member instanceof Method) {
			result = (Class<T>) ((Method) member).getReturnType();
		} else {
			// currently we aren't handling constructor.make it happen if you like.
			throw new InvalidChoiceException(member);
		}
		return result;
	}

	/**
	 * Inspects the {@link AccessibleObject} for it's declaring class, assuming it is a member and
	 * can provide one.
	 * 
	 * @param accessibleObject
	 * @return
	 */
	public static Class<?> getDeclaringClass(AccessibleObject accessibleObject) {
		return ((Member) accessibleObject).getDeclaringClass();
	}
}
