/**
 *
 */
package com.aawhere.id;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.number.NumberUtilsExtended;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Function;

/**
 * An portable base class of {@link Identifier} for {@link Long} values and a parent.
 * 
 * 
 * @author Brian Chapman FIXME: use IdentifierWithParentDelegate to do the common work since
 *         inheritance is not possible.
 */
// @XmlTransient
@XmlType(namespace = XmlNamespace.API_VALUE)
// @XmlAccessorType(XmlAccessType.NONE)
abstract public class LongIdentifierWithParent<KindT, ParentT extends Identifier<?, ?>>
		extends LongIdentifier<KindT>
		implements HasParent<ParentT> {

	private static final Function<String, Long> valueFunction = NumberUtilsExtended.longFromStringFunction();
	private final IdentifierWithParent<Long, ParentT> delegate;

	/**
	 * Provided for XML and persistence libraries only.
	 * 
	 */
	protected LongIdentifierWithParent() {
		super();
		this.delegate = null;
	}

	/**
	 * Used by concrete class default constructor to provide the Kind that they always know.
	 * 
	 * @param kind
	 */
	protected LongIdentifierWithParent(Class<KindT> kind) {
		super(kind);
		this.delegate = null;
	}

	@SuppressWarnings("unchecked")
	protected LongIdentifierWithParent(IdentifierWithParent<Long, ParentT> delegate) {
		super(delegate.value(), (Class<KindT>) delegate.kind());
		this.delegate = delegate;
	}

	/**
	 * The typical method used by the caller of the child id.
	 * 
	 * @param value
	 * @param kind
	 */
	protected LongIdentifierWithParent(Long value, ParentT parent, Class<KindT> kind) {
		this(IdentifierWithParent.<Long, ParentT> create().value(value).parent(parent).kind(kind).build());
	}

	/**
	 * 
	 */
	protected LongIdentifierWithParent(String compositeValue, Class<KindT> kind, Class<ParentT> parentType) {
		this(IdentifierWithParent.<Long, ParentT> create().compositeValue(compositeValue).parentType(parentType)
				.kind(kind).valueFunction(valueFunction).build());
	}

	/**
	 *
	 */
	@Override
	@XmlElement
	public ParentT getParent() {
		return this.delegate.parent();
	}

	/*
	 * @see com.aawhere.id.HasParent#isRoot()
	 */
	@Override
	public Boolean isRoot() {
		return delegate.isRoot();
	}

	private static final long serialVersionUID = -3588241792554962664L;
}
