/**
 * 
 */
package com.aawhere.util.rb;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.xml.XmlNamespace;

/**
 * Simple way to express a {@link #status} indicating success or failure with an explanation of
 * {@link #message}.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusMessage {

	public final CompleteMessage message;
	@Nullable
	public final MessageStatus status;

	/**
	 * 
	 */
	public StatusMessage(@Nullable MessageStatus status, CompleteMessage message) {
		this.message = message;
		this.status = status;
	}

	public StatusMessage(CompleteMessage message) {
		this(null, message);
	}

	StatusMessage() {
		this.message = null;
		this.status = null;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.status.toString() + ":" + this.message.toString();
	}
}
