/**
 * 
 */
package com.aawhere.xml.bind;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * A simple Adapter used to map interfaces in conjunction with the {@link XmlSeeAlso} annotation.
 * 
 * http://www.attivio.com/blog/56-java-development/636-configuring-java-interfaces-to-work-with-jaxb
 * -and-web-services.html
 * 
 * @author aroller
 * 
 */
public class XmlAnyTypeAdapter
		extends XmlAdapter<Object, Object> {

	/**
	 * Noop. Just returns the object given as the argument.
	 */
	public Object unmarshal(Object v) {
		return v;
	}

	/**
	 * Noop. Just returns the object given as the argument.
	 */
	public Object marshal(Object v) {
		return v;
	}

}
