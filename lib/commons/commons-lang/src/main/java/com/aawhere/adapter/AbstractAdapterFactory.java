/**
 *
 */
package com.aawhere.adapter;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.google.inject.Singleton;

/**
 * For creating adapter factories that can translate one type into another type
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public abstract class AbstractAdapterFactory<AdaptedT>
		implements AdapterFactory<AdaptedT> {

	private HashMap<Type, Adapter<?, AdaptedT>> adapters = new HashMap<Type, Adapter<?, AdaptedT>>();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AdapterFactory#register(com.aawhere.lang.Adapter)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> Adapter<T, AdaptedT> register(Adapter<T, AdaptedT> adapter) {
		return (Adapter<T, AdaptedT>) adapters.put(adapter.getType(), adapter);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AdapterFactory#handles(java.lang.reflect.Type)
	 */
	@Override
	public Boolean handles(Type type) {
		return getAdapterOrNull(type) != null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AdapterFactory#handles(java.lang.reflect.Type, java.lang.Class)
	 */
	@Override
	public Boolean handles(Type type, Class<?> parameterType) {
		return getAdapterOrNull(type, parameterType) != null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AdapterFactory#getAdapterOrNull(java.lang.reflect.Type,
	 * java.lang.Class)
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <P, T> Adapter<T, AdaptedT> getAdapterOrNull(Type type, Class<P> parameterType) {
		Adapter<T, AdaptedT> adapter = (Adapter<T, AdaptedT>) adapters.get(type);
		if (adapter == null) {
			if (type instanceof Class) {
				Class<T> typeAsClass = (Class<T>) type;
				// if it's a collection, let's handle it.
				if (Collection.class.isAssignableFrom(typeAsClass)) {
					if (parameterType == null) {
						throw new UnsupportedOperationException(type
								+ " is a collection so a parameter type must be provided");
					} else {
						// get the type that will go inside the collection
						Adapter<P, AdaptedT> parameterTypeAdapter = getAdapter(parameterType);
						adapter = (Adapter<T, AdaptedT>) getCollectionAdapter(parameterTypeAdapter, parameterType);
					}
				}
				// maybe it was registered by a super type
				if (adapter == null) {
					Set<Type> keys = adapters.keySet();
					for (Iterator<Type> iterator = keys.iterator(); iterator.hasNext() && adapter == null;) {
						Type superType = iterator.next();
						if (superType instanceof Class) {
							Class superClass = (Class) superType;
							if (superClass.isAssignableFrom(typeAsClass)) {
								// recursive call using the known key
								adapter = getAdapterOrNull(superType, parameterType);

							}
						}
					}
				}
			}
		}
		return adapter;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AdapterFactory#getAdapterOrNull(java.lang.reflect.Type)
	 */
	@Override
	public <T> Adapter<T, AdaptedT> getAdapterOrNull(Type type) {
		return getAdapterOrNull(type, null);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AdapterFactory#getAdapter(java.lang.reflect.Type)
	 */
	@Override
	public <T> Adapter<T, AdaptedT> getAdapter(Type type) {
		return getAdapter(type, null);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.AdapterFactory#getAdapter(java.lang.reflect.Type, java.lang.Class)
	 */
	@Override
	public <T> Adapter<T, AdaptedT> getAdapter(Type type, Class<?> parameterType) {

		Adapter<T, AdaptedT> adapter = getAdapterOrNull(type, parameterType);

		if (adapter == null) {

			throw new IllegalStateException(type
					+ " is does not have an adapter registered...Call #handles method to avoid this exception");
		}

		return adapter;
	}

	/**
	 * Provides a CollectionAdapter that will translate a Collection of objects into an object.
	 * 
	 * @return
	 */
	protected abstract <C extends Collection<T>, T> Adapter<C, AdaptedT> getCollectionAdapter(
			Adapter<T, AdaptedT> adapter, Class<T> paramaterType);

}
