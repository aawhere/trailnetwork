package com.aawhere.lang.string;

import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.enums.EnumUtil;
import com.aawhere.lang.exception.BaseException;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class EnumStringAdapter
		implements StringAdapter<Enum<?>> {

	/**
	 * Used to construct all instances of EnumStringAdapter.
	 */
	public static class Builder
			extends ObjectBuilder<EnumStringAdapter> {

		public Builder() {
			super(new EnumStringAdapter());
		}

		@Override
		public EnumStringAdapter build() {
			EnumStringAdapter built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct EnumStringAdapter */
	private EnumStringAdapter() {
	}

	@Override
	public String marshal(Object adaptee) {
		if (adaptee == null) {
			return null;
		}
		if (adaptee.getClass().isEnum()) {
			return ((Enum) adaptee).name();
		}
		if (adaptee instanceof String) {
			return (String) adaptee;
		} else {
			throw InvalidArgumentException.notUnderstood("enum", adaptee.getClass().getSimpleName() + " " + adaptee);
		}
	}

	@Override
	public Class getType() {
		return Enum.class;
	}

	@Override
	public <A extends Enum<?>> A unmarshal(String adapted, Class<A> type) throws BaseException {
		if (adapted == null) {
			return null;
		}
		Class genericBs = type;
		return (A) EnumUtil.valueOfStrict(genericBs, adapted);
	}

}
