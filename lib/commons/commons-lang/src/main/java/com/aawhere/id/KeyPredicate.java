/**
 * 
 */
package com.aawhere.id;

import java.util.Collection;

import com.aawhere.lang.Assertion;

import com.google.common.base.Predicate;

/**
 * used to scan {@link Collection}s for matching Keys.
 * 
 * @author roller
 * 
 */
public class KeyPredicate<K extends Identifier<?, ?>>
		implements Predicate<KeyedEntity<K>> {

	private K lookingForKey;

	/**
	 * 
	 */
	public KeyPredicate(K lookingForKey) {
		this.lookingForKey = lookingForKey;
		Assertion.assertNotNull(lookingForKey);
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(KeyedEntity<K> input) {
		return lookingForKey.equals(input.getKey());
	}

	public boolean equals(Object obj) {
		if (obj instanceof KeyedEntity<?>) {
			return lookingForKey.equals(((KeyedEntity<?>) obj).getKey());
		}
		return false;
	};

}
