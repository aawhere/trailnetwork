/**
 * 
 */
package com.aawhere.net;

import java.net.HttpURLConnection;
import java.net.URLConnection;

import org.joda.time.DateTime;

import com.google.common.net.HttpHeaders;

/**
 * Useful stateless utility methods to work with IO and Network interactions
 * 
 * @author aroller
 * 
 */
public class NetUtils {

	/**
	 * Indicates if the connection has a {@link HttpHeaders#LAST_MODIFIED}.
	 * 
	 * @param connection
	 * @return
	 */
	public static Boolean lastModifiedAvailable(URLConnection connection) {
		// per the documentation of the method, 0 indicates not available. Old school!
		return connection.getLastModified() != 0;

	}

	/**
	 * Simple getter that knows how to translate the mystery long into a type {@link DateTime}
	 * 
	 * @param connection
	 * @return
	 */
	public static DateTime lastModified(HttpURLConnection connection) {
		if (!lastModifiedAvailable(connection)) {
			throw new IllegalStateException("call #lastModifiedAVailable first to avoid this");
		}
		return new DateTime(connection.getLastModified());
	}

	public static void setIfModifiedSince(HttpURLConnection connection, DateTime dateTime) {
		connection.setIfModifiedSince(dateTime.getMillis());
	}
}
