/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 4, 2010
commons-lang : com.aawhere.collections.apache.functor.GreaterThanEqualToPredicate.java
 */
package com.aawhere.collections.apache.predicate;

/**
 * @author roller
 * 
 */
public class GreaterThanEqualToPredicate
		extends BinaryNumberPredicate {

	/**
	 * @param min
	 */
	public GreaterThanEqualToPredicate(Number min) {
		super(min);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.collections.apache.functor.BinaryPredicate#evaluateCustom(java.lang.Object)
	 */
	@Override
	public boolean evaluateCustom(Number leftOperand) {

		return leftOperand.doubleValue() >= getRightOperand().doubleValue();
	}

}
