/**
 * 
 */
package com.aawhere.lang.enums;

import javax.annotation.Nonnull;

/**
 * A standard way to indicate the implementing {@link Enum} has a default that can be provided when
 * the value provided is not known. This happens often in a multi-version deployment where a new
 * enum value is provided. The value is inserted into the datastore, but historic versions do not
 * know how to interpret the new value so the newly introduced value causes an exception...hence
 * failing regression.
 * 
 * Implementing this will avoid such situations. Of course you must have this implemented on your
 * enum in the historical version. Adding this to the same version of the app as the new enum value
 * will provide no backwards compatibility of course.
 * 
 * TN-245
 * 
 * @author aroller
 * 
 */
public interface EnumWithDefault<E extends Enum<E>> {

	/**
	 * Provides the default enumeration when the value one has does not match any of the known
	 * enumerations.
	 * 
	 * @return
	 */
	@Nonnull
	E getDefault();

}
