/**
 * 
 */
package com.aawhere.ws.rs;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;

/**
 * A helper for creating {@link MediaType}s. It's quite a shame that we can't reference
 * {@link MediaType} which declares annotation worthy constants, but is incomplete and
 * {@link com.google.common.net.MediaType} which is complete, but doesn't declare annotation
 * friendly media type constants.
 * 
 * @author aroller
 * 
 */
public class MediaTypes {

	/**
	 * 
	 */
	public static final String SUBTYPE_SEPARATOR = "/";
	private static final char VND_SEPARATOR = '.';
	public static final char EXTENSION_SEPARATOR = '.';
	public static final String PLUS = "+";
	public static final String APPLICATION = "application";
	public static final String GOOGLE = "google";

	// types
	public static final String TEXT = "text";
	public static final String IMAGE = "image";

	// subtypes and extensions
	public static final String XML = "xml";
	public static final String JSON = "json";
	public static final String CSV = "csv";
	public static final String KML = "kml";
	public static final String NONE = "none";
	public static final String PNG = "png";
	public static final String SVG = "svg";
	public static final String SVG_PLUS_XML = SVG + PLUS + XML;
	/**
	 * Notice http://stackoverflow.com/a/11930514/721000 says {@link MediaType#APPLICATION_SVG_XML}
	 * is not correct. Constant for {@link com.google.common.net.MediaType#SVG_UTF_8}.
	 */
	public static final String SVG_IMAGE = IMAGE + SUBTYPE_SEPARATOR + SVG_PLUS_XML;
	public static final String PNG_IMAGE = IMAGE + SUBTYPE_SEPARATOR + PNG;
	public static final String GPOLYLINE = "gpolyline";
	public static final String POLYLINE_ENCODED_MEDIA_SUBTYPE = "vnd.google-maps.polyline";
	public static final String POLYLINE_ENCODED_MEDIA_TYPE = "application/" + POLYLINE_ENCODED_MEDIA_SUBTYPE;
	public static final MediaType POLYLINE_ENCODED = new MediaType(MediaTypes.APPLICATION,
			POLYLINE_ENCODED_MEDIA_SUBTYPE);
	/** Made up media type indicating no content returned is desired. */
	public static final String APPLICATION_NONE = APPLICATION + SUBTYPE_SEPARATOR + NONE;
	public static final String TEXT_CSV = TEXT + SUBTYPE_SEPARATOR + CSV;

	public static MediaType application(String subtype) {
		return new MediaType(APPLICATION, subtype);
	}

	/**
	 * Provides a quick way to create a vender subtype given the parts typically starting with who
	 * and moving to more specific parts of the application and document type.
	 */
	public static String vnd(String... parts) {
		return StringUtils.join(parts, VND_SEPARATOR);
	}

	/**
	 * @param wellKnownTextMediaType
	 * @return
	 */
	public static javax.ws.rs.core.MediaType create(String asString) {
		return toJaxRs(com.google.common.net.MediaType.parse(asString));
	}

	public static MediaType toJaxRs(com.google.common.net.MediaType googleMediaType) {
		return new MediaType(googleMediaType.type(), googleMediaType.subtype());
	}
}
