/**
 * 
 */
package com.aawhere.id;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * @author aroller
 * 
 */
public enum IdentifierMessage implements Message {

	/** @see SameIdentifierException */
	SAME_ID("{IDENTIFIER} was provided for both, but this feature requires different identifiers.", Param.IDENTIFIER),
	INVALID_COMPOSITE_FORMAT(
			"{IDENTIFIER} components must be split using only {DILIMETER}.",
			Param.IDENTIFIER,
			Param.DILIMETER);

	private ParamKey[] parameterKeys;
	private String message;

	private IdentifierMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** The identifier object is fine. */
		IDENTIFIER, DILIMETER;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
