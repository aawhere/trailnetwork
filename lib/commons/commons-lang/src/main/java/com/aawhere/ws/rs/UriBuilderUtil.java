/**
 * 
 */
package com.aawhere.ws.rs;

import java.util.Map;

import javax.ws.rs.core.UriBuilder;

import com.aawhere.util.rb.KeyedMessageFormat;

/**
 * Simple helper methods for {@link UriBuilder}.
 * 
 * @author aroller
 * 
 */
public class UriBuilderUtil {

	public static UriBuilder appendQueryParamIfNotNull(UriBuilder builder, String param, Object value) {
		if (value != null) {
			builder.queryParam(param, value);
		}
		return builder;
	}

	/**
	 * Provided the complete url and a map of the parameters this will replace all parameters in the
	 * url string.
	 * 
	 * This is used because URIBuilder encodes the question mark using fromPath, but won't allow
	 * template parameters when using fromUri.
	 * 
	 * @see KeyedMessageFormat
	 * @param url
	 * @param templateParameters
	 * @return
	 */
	public static String build(String url, Map<String, String> templateParameters) {
		return KeyedMessageFormat.create(url).forgiveExtraParameters().putAll(templateParameters).build()
				.getFormattedMessage();
	}
}
