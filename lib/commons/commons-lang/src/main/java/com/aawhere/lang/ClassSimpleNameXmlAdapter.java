/**
 * 
 */
package com.aawhere.lang;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Provides the SimpleName for a class removing the package prefix. Only works in one direction
 * 
 * @author aroller
 * 
 */
public class ClassSimpleNameXmlAdapter
		extends XmlAdapter<String, Class<?>> {
	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public String marshal(Class<?> v) throws Exception {
		return (v != null) ? v.getSimpleName() : null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Class<?> unmarshal(String v) throws Exception {
		// unable to find a class without Package
		return null;
	}

}
