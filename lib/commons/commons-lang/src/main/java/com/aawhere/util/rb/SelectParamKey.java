package com.aawhere.util.rb;

/**
 * Indicates this key may provide an custom transforer that will transform the value the key
 * represents into a value to can be used in a SelectFormat
 * https://www.npmjs.com/package/messageformat#selectformat
 * 
 * This allows a custom selection to be declared in the {@link Message#getParameterKeys()} that will
 * target the message specifically.
 * 
 * @author aroller
 * 
 */
public interface SelectParamKey
		extends ParamKey {

	/**
	 * Transforms the value into a key that can target a message selection. Null indicates it
	 * doesn't understand so there may not be a selection or it may not understand.
	 * 
	 * @param value
	 * @return
	 */
	public String selectKey(Object value);
}
