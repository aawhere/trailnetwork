/**
 * 
 */
package com.aawhere.lang.enums;

import java.util.Map;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates an invlaid key was provided matching no currently registered enums.
 * 
 * @see EnumFactory
 * 
 * @author roller
 * 
 */
@StatusCode(HttpStatusCode.NOT_FOUND)
public class InvalidEnumKeyException
		extends BaseRuntimeException {

	private static final long serialVersionUID = -8268891233619965693L;

	/**
	 * @param key
	 *            the invalid key being searched for and not found
	 * @param registeredEnums
	 *            the possible keys mapped to their
	 */
	public InvalidEnumKeyException(String key, Map<?, ?> registeredEnums) {
		super(new CompleteMessage.Builder(EnumMessage.INVALID_KEY).addParameter(EnumMessage.Param.KEY, key)
				.addParameter(EnumMessage.Param.REGISTERED_ENUMS, registeredEnums).build());
	}

}
