/**
 *
 */
package com.aawhere.joda.time;

import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Convienance class for parsing dates from strings. Useful for controlling XJB class generation to
 * avoid the non-serializable {@link XMLGregorianCalendar} type.
 * 
 * This can be used with an xjb binding file like this.
 * 
 * <pre>
 * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
 * 
 * <jxb:bindings version="2.1" xmlns:jxb="http://java.sun.com/xml/ns/jaxb"
 *         xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xjc="http://java.sun.com/xml/ns/jaxb/xjc">
 * 
 *         <jxb:globalBindings>
 *                 <jxb:serializable/>
 *                 <jxb:javaType name="org.joda.time.DateTime" xmlType="xsd:dateTime"
 *                         printMethod="jxbDateConverter.printDateIso"
 *                         parseMethod="jxbDateConverter.parseDateIso"/>
 *                 <jxb:javaType name="org.joda.time.DateTime" xmlType="xs:date"
 *                         printMethod="jxbDateConverter.printDateYYYYDashMMDashDD"
 *                         parseMethod="jxbDateConverter.parseDateYYYYDashMMDashDD"/>
 *         </jxb:globalBindings>
 * 
 * </jxb:bindings>
 * 
 * </pre>
 * 
 * @author Brian Chapman
 * @deprecated duplicate methods with JodaTimeUtil. This is not tested and xml date time is not
 *             working when millis are not provided.
 */
public class DateTimeConverter {

	private static DateTimeFormatter isoFormat = ISODateTimeFormat.dateTime();
	private static DateTimeFormatter YYYYDashMMDashDDFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
	private static DateTimeFormatter YYYYMMDDFormat = DateTimeFormat.forPattern("yyyyMMdd");
	private static DateTimeFormatter YYYYFormat = DateTimeFormat.forPattern("yyyy");

	public static String printDateIso(DateTime value) {
		return printDate(value, isoFormat);
	}

	public static DateTime parseDateIso(String value) {
		return parseDate(value, isoFormat);
	}

	public static String printDateYYYYDashMMDashDD(DateTime value) {
		return printDate(value, YYYYDashMMDashDDFormat);
	}

	public static DateTime parseDateYYYYDashMMDashDD(String value) {
		return parseDate(value, YYYYDashMMDashDDFormat);
	}

	public static String printDateYYYYMMDD(DateTime value) {
		return printDate(value, YYYYMMDDFormat);
	}

	public static DateTime parseDateYYYYMMDD(String value) {
		return parseDate(value, YYYYMMDDFormat);
	}

	public static String printDateYYYY(DateTime value) {
		return printDate(value, YYYYFormat);
	}

	public static DateTime parseDateYYYY(String value) {
		return parseDate(value, YYYYFormat);
	}

	private static String printDate(DateTime value, DateTimeFormatter format) {

		String dateAsStr;

		if (value != null) {
			dateAsStr = value.toString(format);
		} else {
			dateAsStr = null;
		}

		return dateAsStr;
	}

	private static DateTime parseDate(String value, DateTimeFormatter format) {

		DateTime strAsDate;

		if (value != null) {
			strAsDate = format.parseDateTime(value);
		} else {
			strAsDate = null;
		}
		return strAsDate;
	}
}
