/**
 *
 */
package com.aawhere.lang.string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import com.aawhere.collections.FunctionsExtended;
import com.aawhere.lang.If;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Seriously, please exhaust yourself looking into the many libraries that already provide these
 * utilities.
 * 
 * If you can't find it anywhere and it is completely generic then add it.
 * 
 * @author roller
 * 
 */
public class StringUtilsExtended {

	/**
	 * Defined here for annotations to use since Boolean.toString doesn't reference constants and
	 * annotations require such..
	 */
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String SPACE = " ";
	public static final String NO_SPACE = "";
	public static final String CONSTANT_CASE_DELIMINATOR = "_";
	/**
	 * Mixed Hyphens and camel case can create duplicate words. Double underscore indicates hyphen
	 * to avoid duplicates and make more clear the separation of the camel vs hyphen.
	 */
	public static final String CONSTANT_CASE_WORD_DELIMINATOR = CONSTANT_CASE_DELIMINATOR + CONSTANT_CASE_DELIMINATOR;

	/**
	 * Takes a TitleCase word or camelCase word and converts it into a CONSTANT_CASE word.
	 * 
	 * @param camelCase
	 * @return
	 */
	public static String camelCaseToConstant(String camelCase) {
		String[] split = StringUtils.splitByCharacterTypeCamelCase(camelCase);
		return toConstantCase(split);
	}

	/**
	 * Provides an underscore between all words given.
	 * 
	 * so {"my","Words","areVery","STANDARD"} -> MY_WORDS_AREVERY_STANDARD
	 * 
	 * 
	 * 
	 * @param words
	 * @return
	 */
	public static String toConstantCase(String... words) {
		return toConstantCase(CONSTANT_CASE_DELIMINATOR, words);
	}

	/**
	 * Given one or more words this will uppercase each letter of each word and separate each word
	 * by an underscore making a constant case to match the standard convention. Improves upon
	 * {@link #toConstantCase(String...)} by providing a double separator between words and single
	 * between camelCaseWords.
	 * 
	 * so {"my","Words","areVery","STANDARD"} -> MY__WORDS__ARE_VERY__STANDARD
	 * 
	 * @param words
	 * @return
	 */
	public static String toConstantCaseExtraWordSeparation(String... words) {
		return toConstantCase(CONSTANT_CASE_WORD_DELIMINATOR, words);
	}

	/**
	 * Separates hyphens into words and adds them to those words given. Those words will be joined
	 * using the word dilimeter given allowing for differentiation between words given (or hyphen)
	 * and camelcase. camelCase is also separated into words, but will use the
	 * {@link #CONSTANT_CASE_DELIMINATOR} always.
	 * 
	 * @param wordDilimeter
	 * @param words
	 * @return
	 */
	private static String toConstantCase(String wordDilimeter, String... words) {

		// first, each word must have hyphens separated
		ArrayList<String> wordsFromHyphens = Lists.newArrayList();
		for (String word : words) {
			wordsFromHyphens.addAll(Arrays.asList(StringUtils.split(word, '-')));
		}

		// then, change camel case into separate words
		List<String> camelCaseWords = Lists.newArrayList();
		for (String word : wordsFromHyphens) {
			List<String> wordsFromCamelCase = Arrays.asList(StringUtils.splitByCharacterTypeCamelCase(word));
			camelCaseWords.add(StringUtils.join(wordsFromCamelCase, CONSTANT_CASE_DELIMINATOR));
		}
		String joined = StringUtils.join(camelCaseWords, wordDilimeter);
		return joined.toUpperCase();
	}

	/**
	 * Given one or more words this will create aCamelCaseWord from them.
	 * 
	 * @param words
	 * @return
	 */
	public static String toCamelCase(String... words) {
		return toCamelCase(toTitleCase(words));

	}

	/**
	 * formats a string where the first letter of each word is uppercase. Words are given and the
	 * resulting words are separated by a single space.
	 * 
	 * @param words
	 * @return
	 */
	public static String toTitleCase(String... words) {
		Iterable<String> lowercase = Iterables.transform(Lists.newArrayList(words), lowerCaseFunction());
		Iterable<String> capitolized = Iterables.transform(lowercase, capitolizeFunction());
		String joined = StringUtils.join(capitolized, NO_SPACE);
		return joined;
	}

	public static String toCamelCase(String titleCase) {
		return StringUtils.uncapitalize(titleCase);
	}

	/**
	 * Converts a camelCase word to a hyphenated-word with all lower case.
	 * 
	 * @param camelCase
	 * @return
	 */
	public static String camelCaseToHyphenated(String camelCase) {
		String[] split = StringUtils.splitByCharacterTypeCamelCase(camelCase);
		String joined = StringUtils.join(split, "-");
		return joined.toLowerCase();

	}

	public static String constantCaseToHyphenated(String constantCase) {
		return camelCaseToHyphenated(constantCaseToCamelCase(constantCase));
	}

	public static String constantCaseToCamelCase(String constantCase) {
		String lowerCase = StringUtils.lowerCase(constantCase);
		String[] split = StringUtils.split(lowerCase, CONSTANT_CASE_DELIMINATOR);
		return toCamelCase(split);
	}

	/**
	 * Strips get or is from the beginning of a method name and reduces the capitalization to match
	 * the property naming scheme.
	 * 
	 * Yes, it seems like this should exist somewhere in Apache BeanUtils, but I couldn't find it
	 * easily. If it does then pass through once found.
	 * 
	 * @param methodName
	 * @return
	 */
	public static String accessorToProperty(String methodName) {
		// return the method name if no modification
		String property = methodName;

		String[] accessorPrefixes = { "get", "is" };
		String[] parts = StringUtils.splitByCharacterTypeCamelCase(methodName);
		boolean replaced = false;
		if (parts.length > 0) {
			String firstWord = parts[0];
			for (int whichPrefix = 0; whichPrefix < accessorPrefixes.length && !replaced; whichPrefix++) {
				String prefix = accessorPrefixes[whichPrefix];
				if (firstWord.equals(prefix)) {
					property = StringUtils.replace(methodName, prefix, "", 1);
					property = StringUtils.uncapitalize(property);
					replaced = true;
				}
			}

		}

		return property;
	}

	/** Makes sure nulls, empty strings and strings with only quotes are considered to be empty. */
	public static Boolean isEmpty(String string) {
		boolean empty = StringUtils.isEmpty(string);
		if (!empty) {
			string = StringUtils.strip(string, "\"\"");
		}
		return StringUtils.isEmpty(string);
	}

	/**
	 * Simple helper that returns null if null or empty string, otherwise returns the string if not
	 * empty. This also removes empty quotes since that is a common result of an "empty" value.
	 * 
	 * @param latitudeStringInDecimalDegrees
	 * @return
	 */
	public static String nullIfEmpty(String string) {
		return If.tru(isEmpty(string), string).use(null);
	}

	/** Returns the provided string as a lower case version of given or null if null is given. */
	public static Function<String, String> lowerCaseFunction() {
		return new Function<String, String>() {

			@Override
			public String apply(String input) {
				if (input == null) {
					return null;
				}
				return input.toLowerCase();
			}
		};
	}

	/**
	 * Returns the provided string as a capitolized version of given or null if null is given.
	 * 
	 * Examples:
	 * 
	 * captitol => Capitol
	 * 
	 * cAPItol => CAPItol
	 * 
	 * CAPITOL => CAPITOL
	 * 
	 * */
	public static Function<String, String> capitolizeFunction() {
		return new Function<String, String>() {

			@Override
			public String apply(String input) {
				if (input == null) {
					return null;
				}
				return StringUtils.capitalize(input);
			}
		};
	}

	/**
	 * @see WordUtils#capitalizeFully(String)
	 * @return
	 */
	public static Function<String, String> capitalizeFullyFunction() {
		return new Function<String, String>() {

			@Override
			public String apply(String input) {
				return (input == null) ? null : WordUtils.capitalizeFully(input);
			}
		};
	}

	public static Function<String, String> replaceFunction(final String searchString, final String replacement) {
		return new Function<String, String>() {

			@Override
			public String apply(String input) {
				return (input == null) ? null : StringUtils.replace(input, searchString, replacement);
			}
		};
	}

	/**
	 * @see StringUtils#join(Object[], String)
	 * @param separator
	 * @return
	 */
	public static Function<String[], String> joinFunction(final String separator) {
		return new Function<String[], String>() {

			@Override
			public String apply(String[] input) {
				return (input == null) ? null : StringUtils.join(input, separator);
			}
		};
	}

	public static Function<String, String[]> splitByCharacterTypeCamcelCaseFunction() {
		return new Function<String, String[]>() {

			@Override
			public String[] apply(String input) {
				return (input == null) ? null : StringUtils.splitByCharacterTypeCamelCase(input);
			}
		};
	}

	/**
	 * IF multiple spaces are together then remove the extra leaving only 1.
	 * 
	 * @param input
	 * @return
	 */
	public static String spacesCondensedIntoOne(String input) {
		return (input == null) ? null : input.trim().replaceAll(" +", SPACE);
	}

	/**
	 * Replaces all spaces with a single space removing with a {@link String#trim()} to remove
	 * spaces on the end.
	 * 
	 * @return
	 */
	public static Function<String, String> sentenceSpacingFunction() {
		return new Function<String, String>() {

			@Override
			public String apply(String input) {
				return spacesCondensedIntoOne(input);
			}

		};
	}

	/**
	 * @see String#replaceAll(String, String)
	 * @param regex
	 * @param replacement
	 * @return
	 */
	public static Function<String, String> replaceAllFunction(final String regex, final String replacement) {
		return new Function<String, String>() {

			@Override
			public String apply(String input) {
				return (input == null) ? null : input.replaceAll(regex, replacement);
			}
		};
	}

	public static <F> Function<F, String> emptyStringFunction(Function<F, String> nullableFunction) {
		return FunctionsExtended.stringEmptyFunction(nullableFunction);
	}
}
