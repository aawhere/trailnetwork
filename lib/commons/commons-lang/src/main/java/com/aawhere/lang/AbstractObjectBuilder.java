/**
 * 
 */
package com.aawhere.lang;

import javax.xml.bind.annotation.XmlTransient;

/**
 * Used by classes that remain abstract. This requests the builder template to use so the builder
 * values returned will be of the specific builder type and reduce the need for casting.
 * 
 * essentially, any base class should consider using this instead of {@link ObjectBuilder} directly
 * to ensure builders of useful types will be returned using dis instead of this.
 * 
 * @author Aaron Roller
 * 
 * @param <R>
 *            the result that is being built.
 * @param <B>
 *            the specific builder type that is doing the building
 */
@XmlTransient
public abstract class AbstractObjectBuilder<R, B extends AbstractObjectBuilder<R, B>>
		extends ObjectBuilder<R> {

	protected final B dis;

	/**
	 * @param building
	 */
	protected AbstractObjectBuilder(R building) {
		super(building);

		@SuppressWarnings("unchecked")
		B dis = (B) this;
		this.dis = dis;
	}
}
