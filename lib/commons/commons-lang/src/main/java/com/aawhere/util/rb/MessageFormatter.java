/**
 *
 */
package com.aawhere.util.rb;

import java.util.Locale;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * Simple utility to assist in formatting any message using the locale. If the given Message is an
 * enumeration this will use the {@link MessageResourceBundleFactory} to retrieve a
 * {@link MessageResourceBundle} to do the formatting.
 * 
 * If its anything besides an enumeration this will just grab the value of the Message.
 * 
 * @author roller
 * 
 */
public class MessageFormatter {

	/** A convience method for constructing a builder */
	public static Builder create() {
		return new Builder();
	}

	/** The result of formatting the message during building. */
	private String formattedMessage;

	/** Use {@link Builder} to construct MessageFormatter */
	private MessageFormatter() {
	}

	/**
	 * This is what you came here for.
	 * 
	 * @return the formattedMessage
	 */
	public String getFormattedMessage() {
		return this.formattedMessage;
	}

	/**
	 * Used to construct all instances of MessageFormatter.
	 */
	public static class Builder
			extends ObjectBuilder<MessageFormatter> {

		private Locale locale;
		private CompleteMessage completeMessage;

		public Builder() {
			super(new MessageFormatter());
		}

		public Builder message(CompleteMessage message) {
			this.completeMessage = message;
			return this;
		}

		public Builder message(Message message) {
			this.completeMessage = new CompleteMessage.Builder(message).build();
			return this;
		}

		public Builder locale(Locale locale) {
			this.locale = locale;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull("Locale", this.locale);
			Assertion.assertNotNull("Message", this.completeMessage);
			super.validate();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public MessageFormatter build() {
			MessageFormatter built = super.build();
			final Message message = this.completeMessage.getMessage();
			if (MessageUtil.isEnum(message)) {
				MessageResourceBundle rb = MessageResourceBundleFactory.getInstance()
						.getManagedResourceBundleFromMessage(message);
				built.formattedMessage = rb.formatMessage(this.completeMessage, locale);
			} else {
				built.formattedMessage = message.getValue();
			}
			return built;
		}
	}// end Builder

	@Override
	public String toString() {
		return this.formattedMessage;
	}
}
