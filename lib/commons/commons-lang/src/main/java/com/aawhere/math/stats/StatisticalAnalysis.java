package com.aawhere.math.stats;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math.stat.descriptive.StatisticalSummary;
import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.moment.StandardDeviation;

import com.aawhere.lang.ObjectBuilder;

/**
 * 
 */

/**
 * Receives a series of numbers and determines the confidence for each based on the statistical
 * analysis.
 * 
 * TODO: Finish adding criteria and tests to make sure this will select the anomolies.
 * 
 * @see StatisticalSummary for a potential replacement
 * 
 * @author Aaron Roller on Apr 22, 2011
 * 
 */
public class StatisticalAnalysis<NumberT extends Number & Comparable<NumberT>, IdentifierT> {

	/**
	 * Used to construct all instances of StatisticalAnalysis.
	 */
	public static class Builder<NumberT extends Number & Comparable<NumberT>, IdentifierT>
			extends ObjectBuilder<StatisticalAnalysis<NumberT, IdentifierT>> {

		public Builder() {
			super(new StatisticalAnalysis<NumberT, IdentifierT>());
		}

		public void add(NumberT value, IdentifierT id) {
			StatisticalAnalysis<NumberT, IdentifierT>.Entry entry = building.new Entry();
			entry.value = value;
			entry.id = id;
			entry.index = building.entries.size();
			building.entries.add(entry);
		}

		@Override
		public StatisticalAnalysis<NumberT, IdentifierT> build() {
			building.evaluate();
			return super.build();
		}

	}// end Builder

	/** Use {@link Builder} to construct StatisticalAnalysis */
	private StatisticalAnalysis() {
	}

	private ArrayList<Entry> entries = new ArrayList<Entry>();
	private double[] values;
	private Double mean;
	private Double standardDeviation;
	private ArrayList<Entry> outliers = new ArrayList<Entry>();

	private void evaluate() {
		values = new double[entries.size()];
		int index = 0;
		for (Entry entry : entries) {
			values[index] = entry.value.doubleValue();
			index++;
		}

		this.mean = new Mean().evaluate(values);
		this.standardDeviation = new StandardDeviation().evaluate(values, this.mean);

		for (Entry entry : entries) {
			entry.deviationFromMean = entry.value.doubleValue() - this.mean;

			if (Math.abs(entry.deviationFromMean) > standardDeviation) {
				changeToOutlier(entry);
			}

		}
	}

	private void changeToOutlier(Entry entry) {
		entry.outlier = true;
		outliers.add(entry);
	}

	public Double getMean() {
		return mean;
	}

	public Double getStandardDeviation() {
		return standardDeviation;
	}

	public List<Entry> getEntries() {
		return entries;
	}

	public List<Entry> getOutliers() {
		return outliers;
	}

	public class Entry {
		private Number value;
		private IdentifierT id;
		private Integer index;
		private Boolean outlier;
		private double deviationFromMean;

		public double getDeviationFromMean() {
			return deviationFromMean;
		}

		public IdentifierT getId() {
			return id;
		}

		public Integer getIndex() {
			return index;
		}

		public Number getValue() {
			return value;
		}

		public Boolean isOutlier() {
			return outlier;
		}
	}
}
