/**
 * 
 */
package com.aawhere.collections;

import java.util.Map;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.Maps.EntryTransformer;

/**
 * @author aroller
 * @see CollectionUtilsExtended since some maps functions were provided from there first
 */
public class MapsExtended {

	/**
	 * Provides an {@link EntryTransformer} that transforms the values of entries by delgating to
	 * the provided valueFunction.
	 * 
	 * @param valueFunction
	 * @return
	 */
	public static <K, V1, V2> EntryTransformer<K, V1, V2> compose(final Function<V1, V2> valueFunction) {
		return new EntryTransformer<K, V1, V2>() {
			@Override
			public V2 transformEntry(K key, V1 value) {
				return valueFunction.apply(value);
			}
		};
	}

	/**
	 * Given entries this returns a map. https://github.com/google/guava/issues/320 This should be
	 * available in v19.
	 * 
	 * @param entries
	 * @return
	 */
	public static <K, V> ImmutableMap<K, V> asMap(Iterable<? extends Map.Entry<K, V>> entries) {
		Builder<K, V> builder = ImmutableMap.builder();
		for (Map.Entry<K, V> entry : entries) {
			builder.put(entry);
		}
		return builder.build();
	}
}
