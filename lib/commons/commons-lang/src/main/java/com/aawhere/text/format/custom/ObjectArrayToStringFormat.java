/**
 * 
 */
package com.aawhere.text.format.custom;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Aaron Roller
 * 
 */
public class ObjectArrayToStringFormat
		implements CustomFormat<Object[]> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#format(java.lang.Object, java.util.Locale)
	 */
	@Override
	public String format(Object[] objects, Locale locale) {
		return StringUtils.join(objects, ',');
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#handlesType()
	 */
	@Override
	public Class<? super Object[]> handlesType() {

		return Object[].class;
	}

}
