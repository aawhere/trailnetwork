/**
 * 
 */
package com.aawhere.io;

import com.aawhere.lang.exception.BaseException;

/**
 * Indicates the
 * 
 * @author roller
 * 
 */
public class NotADirectoryException
		extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3529564855528324602L;

	public NotADirectoryException(Directory directory) {

	}

}
