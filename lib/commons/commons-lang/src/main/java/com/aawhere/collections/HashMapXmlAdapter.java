/**
 * 
 */
package com.aawhere.collections;

import java.util.HashMap;
import java.util.Map;

/**
 * @author aroller
 * 
 */
public class HashMapXmlAdapter
		extends MapXmlAdapter {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.collections.MapXmlAdapter#createMap()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected Map createMap() {

		return new HashMap();
	}

}
