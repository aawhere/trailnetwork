/**
 *
 */
package com.aawhere.lang.string;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates the string provided is not able to be converted into a desired object of the given
 * class.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class StringFormatException
		extends BaseException {

	private static final long serialVersionUID = 3298275282038715619L;

	public StringFormatException(String invalidString, Class<?> typeNotAbleToParse) {
		super(new CompleteMessage.Builder(StringMessage.INVALID_FORMAT)
				.addParameter(StringMessage.Param.INVALID_STRING, invalidString)
				.addParameter(StringMessage.Param.EXPECTED_CLASS, typeNotAbleToParse).build());
	}

	/**
	 * Allows subclasses to customize their own message.
	 * 
	 * @param message
	 * @param cause
	 */
	public StringFormatException(CompleteMessage message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public StringFormatException(CompleteMessage message) {
		super(message);
	}

}
