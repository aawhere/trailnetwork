/**
 *
 */
package com.aawhere.lang.string;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.util.CollectionUtilsExtended;

/**
 * Creates collections of objects when given a collection of strings. It uses the adapter factory to
 * adapt strings into a collection that is determined by the given Type.
 * 
 * The type must be a parameterized type that indicates both the expected collection and the objects
 * contained within the collection. The objects within the collection must be homogeneous concrete
 * classes and not a super interface.
 * 
 * @see StringAdapter
 * @see StringAdapterFactory
 * 
 * @author aroller
 * 
 */
public class CollectionStringsAdapter<C extends Collection<T>, T> {

	/**
	 * Used to construct all instances of CollectionStringsAdapter.
	 */
	public static class Builder<C extends Collection<T>, T>
			extends ObjectBuilder<CollectionStringsAdapter<C, T>> {

		private Type type;
		private StringAdapterFactory adapterFactory;

		public Builder() {
			super(new CollectionStringsAdapter<C, T>());
		}

		public Builder<C, T> setType(Type type) {
			this.type = type;
			return this;
		}

		/**
		 * @param langFactory
		 * @return
		 */
		public Builder<C, T> setFactory(StringAdapterFactory adapterFactory) {
			this.adapterFactory = adapterFactory;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("type", type);
			Assertion.assertNotNull("factory", adapterFactory);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@SuppressWarnings("unchecked")
		@Override
		public CollectionStringsAdapter<C, T> build() {
			// currently only parameterized type is supported...maybe more?
			if (type instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) type;
				Type rawType = parameterizedType.getRawType();
				if (rawType instanceof Class) {
					Class<?> collectionClass = (Class<?>) rawType;
					if (Collection.class.isAssignableFrom(collectionClass)) {
						Type actualType = parameterizedType.getActualTypeArguments()[0];
						try {
							building.typeInsideCollection = (Class<T>) actualType;
						} catch (ClassCastException e) {
							throw new ClassCastException(actualType + " must be a class.  It was provided from "
									+ collectionClass);
						}
						building.collectionClass = (Class<C>) collectionClass;
						if (adapterFactory.handles(building.typeInsideCollection)) {
							// make sure a collection can be created.
							Assertion.assertNotNull("collection", building.newCollection());
							building.adapter = (StringAdapter<T>) adapterFactory
									.getAdapter(building.typeInsideCollection);
						}
					}
				}
			}
			return super.build();
		}

	}// end Builder

	/** Use {@link Builder} to construct CollectionStringsAdapter */
	private CollectionStringsAdapter() {
	}

	/** the type of collection to be created. */
	private Class<C> collectionClass;
	private StringAdapter<T> adapter;
	private Class<T> typeInsideCollection;

	private C newCollection() {
		return CollectionUtilsExtended.newCollection(this.collectionClass);
	}

	public C marshal(Iterable<String> strings) throws BaseException {

		C collection = newCollection();

		for (String string : strings) {
			collection.add(adapter.unmarshal(string, typeInsideCollection));
		}
		return collection;
	}

	public Boolean handles() {
		return this.adapter != null;
	}

}
