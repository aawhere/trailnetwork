/**
 * 
 */
package com.aawhere.util.rb;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Default conversion for {@link Message}.
 * 
 * This does not handle personalization and is typically used for internal processing and data
 * transport.
 * 
 * @author aroller
 * 
 */
public class MessageXmlAdapter
		extends XmlAdapter<StringMessage, Message> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public StringMessage marshal(Message message) throws Exception {
		if (message == null) {
			return null;
		} else {
			return new StringMessage(message.name(), message.getValue());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Message unmarshal(StringMessage message) throws Exception {
		return message;
	}

}
