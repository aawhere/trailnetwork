/**
 * 
 */
package com.aawhere.lang;

import javax.annotation.Nullable;

import com.google.common.base.Function;

/**
 * Executes {@link ObjectBuilder#build()} providing the built object.
 * 
 * @author aroller
 * 
 */
public class ObjectBuilderFunction<O>
		implements Function<ObjectBuilder<O>, O> {

	public static <O> ObjectBuilderFunction<O> build() {
		return new ObjectBuilderFunction<>();
	}

	/** Use {@link Builder} to construct ObjectBuilderFunction */
	private ObjectBuilderFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public O apply(@Nullable ObjectBuilder<O> input) {
		return input.build();
	}

}
