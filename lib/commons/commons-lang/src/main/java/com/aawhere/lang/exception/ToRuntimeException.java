/**
 * 
 */
package com.aawhere.lang.exception;

import com.aawhere.net.HttpStatusCode;

/**
 * When you have a {@link BaseException} that explains it all, but you want to pass it on without
 * being checked use this exception and it will inherit everything valuable.
 * 
 * @author aroller
 * 
 */
// @StatusCode specifically not provided since it will inherit from given exception or use the
// default
public class ToRuntimeException
		extends BaseRuntimeException {

	private static final long serialVersionUID = -6528064191197344172L;

	public ToRuntimeException(Exception wrap, HttpStatusCode status) {
		super(wrap);
		setStatusCode(status);
	}

	/**
	 * Handles all exceptions finding the best status code from the exception and assigning it to
	 * the status code for this.
	 */
	public ToRuntimeException(Exception wrap) {
		this(wrap, ExceptionUtil.responseCode(wrap));
	}

	/**
	 * @param e
	 * @return
	 */
	public static ToRuntimeException wrapAndThrow(BaseException like) {
		throw new ToRuntimeException(like);
	}

	/**
	 * Indicates that the exception provided is not something the client caused nor can repair, that
	 * the system has invoked repair operations and that this request should work at a later time
	 * when the operations are complete.
	 * 
	 * TODO: the message could be better.
	 * 
	 * @param cause
	 * @return
	 */
	public static ToRuntimeException systemBeingRepaired(BaseException cause) {
		throw new ToRuntimeException(cause, HttpStatusCode.UNAVAILABLE);
	}

	@SuppressWarnings("unchecked")
	public static <E extends BaseException> E unwrap(ToRuntimeException wrapper, Class<E> expecting) throws E {
		return (E) wrapper.getCause();
	}
}
