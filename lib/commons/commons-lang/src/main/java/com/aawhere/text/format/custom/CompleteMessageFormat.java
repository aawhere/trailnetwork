package com.aawhere.text.format.custom;

import java.util.Locale;

import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageFormatter;

public class CompleteMessageFormat
		implements CustomFormat<CompleteMessage> {

	@Override
	public String format(CompleteMessage message, Locale locale) {
		if (message == null) {
			return null;
		}
		return MessageFormatter.create().locale(locale).message(message).build().getFormattedMessage();
	}

	@Override
	public Class<? super CompleteMessage> handlesType() {
		return CompleteMessage.class;
	}

}
