/**
 * 
 */
package com.aawhere.xml.bind;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 * {@link JAXBContext} is expensive to initialize. It is intended to be cached locally. This class
 * caches JAXBContext by type. Note that JaxBContextes can be created with multiple types. Consider
 * support for that situation a TOOD item.
 * 
 * @author Brian Chapman
 * 
 */
public class JAXBContextFactory {

	/**
	 * Threadsafe way of doing singleton pattern.
	 * 
	 * @author brian
	 * 
	 */
	private static class JAXBContextFactoryInstanceProducer {
		protected static JAXBContextFactory instance = new JAXBContextFactory();
	}

	public static JAXBContextFactory instance() {
		return JAXBContextFactoryInstanceProducer.instance;
	}

	/**
	 * Use {@link #instance()} to get singleton.
	 */
	private JAXBContextFactory() {
	}

	private Map<Class<?>, JAXBContext> contextMap = new HashMap<Class<?>, JAXBContext>();

	public JAXBContext getContext(Class<?> type) throws JAXBException {
		JAXBContext context;
		if (contextMap.containsKey(type)) {
			context = contextMap.get(type);
		} else {
			context = JAXBContext.newInstance(type);
			contextMap.put(type, context);
		}
		return context;
	}
}
