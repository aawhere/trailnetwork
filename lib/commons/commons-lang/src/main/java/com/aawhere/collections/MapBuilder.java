/**
 * 
 */
package com.aawhere.collections;

import java.util.HashMap;
import java.util.Map;

/**
 * Makes it easier to chain map parameters.
 * 
 * @author aroller
 * 
 */
public class MapBuilder<K, V> {

	private Map<K, V> map;

	/**
	 * 
	 */
	private MapBuilder(Map<K, V> map) {
		this.map = map;
	}

	/**
	 * 
	 */
	public MapBuilder() {
		this(new HashMap<K, V>());
	}

	public MapBuilder<K, V> put(K key, V value) {
		map.put(key, value);
		return this;
	}

	public Map<K, V> build() {
		Map<K, V> finishedMap = this.map;
		this.map = null;
		return finishedMap;
	}

	public static <K, V> MapBuilder<K, V> hashMap() {
		return new MapBuilder<>(new HashMap<K, V>());
	}
}
