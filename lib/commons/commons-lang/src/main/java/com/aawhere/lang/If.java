/**
 * 
 */
package com.aawhere.lang;

import java.util.List;

/**
 * Does a null check so you don't have to.
 * 
 * @author Aaron Roller
 * 
 */
public class If {

	/**
	 * returns the value or null if the value is null. Shorthand for a very common scenario.
	 * 
	 * @param value
	 * @return
	 */
	public static <T> T nul(T value) {
		if (value == null) {
			return null;
		} else {
			return value;
		}
	}

	/**
	 * retruns the value or the default value if the value is null.
	 * 
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	public static <T> T nul(T value, T defaultValue) {
		return If.nil(value).use(defaultValue);
	}

	/**
	 * Returns the value given or the default value if the value given is null.
	 * 
	 * A more readable version of null check with default:
	 * 
	 * 
	 * 
	 * If.nil(myValue).use(defaultValue);
	 * 
	 * Used nil to avoid duplicate erasure with nul
	 * 
	 * @param value
	 * @return
	 */
	public static <T> Default<T> nil(T value) {
		return new Default<T>(value, value == null);
	}

	/**
	 * Useful when unhappy if there is a result (like when a collection returns a previous entry).
	 * 
	 * @param put
	 * @return
	 */
	public static <T> Default<T> notNull(T value) {
		return new Default<T>(value, value != null);
	}

	/**
	 * More redable way to provide a default value.
	 * 
	 * @see If#nil(Object)
	 * */
	public static class Default<T> {
		private T value;
		private Boolean useDefault;

		public Default(T value, Boolean useDefault) {
			this.value = value;
			this.useDefault = useDefault;
		}

		public T use(T defaultValue) {
			return (useDefault) ? defaultValue : this.value;
		}

		/**
		 * Throws the given exception. Always be mindful in regards to performance that you are
		 * building an exception that you are hoping not to use.
		 * 
		 * @param e
		 * @throws E
		 */
		public <E extends Throwable> T raise(E e) throws E {
			if (useDefault) {
				throw (e);
			} else {
				return this.value;
			}
		}
	}

	/**
	 * @param acceptableLanguages
	 * @param us
	 * @return
	 */
	public static <T> T first(List<T> list, T defaultValue) {

		return (list == null || list.isEmpty()) ? defaultValue : list.get(0);
	}

	/**
	 * Given the value, this will return the {@link Default#use(Object)} result if value is equal to
	 * comparedTo parameter.
	 * 
	 * If they are not equal the value will be returned.
	 * 
	 * <pre>
	 * If.equal(1,1).use(2) == 2;
	 * If.equal(1,2).use(2) == 1;
	 * </pre>
	 * 
	 * @param unknown
	 * @return
	 */
	public static <T> Default<T> equal(T value, T comparedTo) {
		return new Default<T>(value, value != null && value.equals(comparedTo));
	}

	/**
	 * Used when a default or exception should be taken if true. If false, null is return and no
	 * exception thrown. If.tru(true).use("something");
	 * 
	 * 
	 * If.tru(true).raise(e);
	 * 
	 * If.tru(true).use("default") == "default";
	 * 
	 * If.tru(false).use("default") == null;
	 * 
	 * @see #tru(Boolean, String)
	 * @param value
	 * @return
	 */

	public static <T> Default<T> tru(Boolean useDefault) {
		return tru(useDefault, null);
	}

	/**
	 * If.tru(true,"value").use("default") == "default";
	 * 
	 * If.tru(false,"value").use("default") == "value";
	 * 
	 * @param empty
	 * @param string
	 * @return
	 */
	public static <T> Default<T> tru(Boolean useDefault, T value) {
		return new Default<T>(value, useDefault);
	}

	/**
	 * The opposite of {@link #equal(Object, Object)}.
	 * 
	 * If value is not equal to comparedTo, the {@link Default#use(Object)} will be returned.
	 * 
	 * The value will be returned if they are equal.
	 * 
	 * <pre>
	 * If.notEqual(1,1).use(2) == 1; 
	 * If.notEqual(1,2).use(2) == 2;
	 * </pre>
	 * 
	 * @param value
	 * @param comparedTo
	 * @return
	 */
	public static <T> Default<T> notEqual(T value, T comparedTo) {
		return new Default<T>(value, value != null && !value.equals(comparedTo));
	}

}
