/*
 * www.akuacom.com - Automating Demand StatusCode
 *
 * com.ct.exception.BaseException.java - Copyright(c)1994 to 2010 by Akuacom . All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, is prohibited.
 *
 */
package com.aawhere.lang.exception;

import com.aawhere.lang.If;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * The Base Exception to be used for all Exceptions in the system.
 * 
 * @see BaseRuntimeException for the runtime version of this.
 */
abstract public class BaseException
		extends Exception
		implements MessageException {

	private static final long serialVersionUID = 5549692483155308199L;

	private ExceptionLocalizer exceptionLocalizer;

	private HttpStatusCode statusCode;

	/**
	 * Strictly a convenience constructor to allow a developer to create exceptions and deal with
	 * messages later once the complete use case is solidified.
	 * 
	 */
	protected BaseException() {

	}

	/**
	 * @param message
	 * @param cause
	 */
	BaseException(String message, Throwable cause) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(message, cause));

	}

	/**
	 * @param message
	 */
	BaseException(String message) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(message));

	}

	/**
	 * @param cause
	 */
	protected BaseException(Throwable cause) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(cause));

	}

	protected BaseException(BaseException other) {
		this(other.exceptionLocalizer);
	}

	/** THis is the primary method that implementors will typicall call. */
	protected BaseException(CompleteMessage message) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(message));
	}

	/** This is a primary method that implementors will typically call. */
	protected BaseException(CompleteMessage message, Throwable cause) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(message, cause));
	}

	protected BaseException(Message message) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(message));
	}

	/**
	 * A helper constructor when a single message and single paramater is provided.
	 */
	protected BaseException(Message message, ParamKey paramKey, Object paramValue) {
		this(new CompleteMessage.Builder().setMessage(message).addParameter(paramKey, paramValue).build());
	}

	protected BaseException(Message message, Throwable e) {
		this(ExceptionLocalizerFactory.getInstance().getExceptionLocalizer(message, e));
	}

	private BaseException(ExceptionLocalizer exceptionLocalizer) {
		// message is not ready yet since locale is not known
		super(exceptionLocalizer.getCause());

		this.exceptionLocalizer = exceptionLocalizer;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Throwable#getLocalizedMessage()
	 */
	@Override
	public String getLocalizedMessage() {
		return getMessageFromLocalizer();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {

		String message = getMessageFromLocalizer();

		return message;

	}

	/**
	 * @return
	 */
	private String getMessageFromLocalizer() {
		String message;
		if (this.exceptionLocalizer == null) {
			message = null;
		} else {
			message = this.exceptionLocalizer.getMessage();
		}
		return message;
	}

	public ExceptionLocalizer getExceptionLocalizer() {
		return this.exceptionLocalizer;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.exception.MessageException#getCompleteMessage()
	 */
	@Override
	public CompleteMessage getCompleteMessage() {
		if (this.exceptionLocalizer instanceof MessageResourceBundleExceptionLocalizer) {
			return ((MessageResourceBundleExceptionLocalizer) this.exceptionLocalizer).getDynamicMessage();
		} else {
			return null;
		}
	}

	/**
	 * Inspects the exception for the {@link StatusCode} annotation and returns the
	 * {@link HttpStatusCode}.
	 * 
	 * @return
	 */
	public HttpStatusCode getStatusCode() {
		// don't call ExceptionUtil.statusCode since that calls this.
		return If.nil(this.statusCode).use(ExceptionUtil.statusCodeFromAnnotation(this));
	}

	/**
	 * Sets the overriding status code available only to implementors.
	 * 
	 * This breaks the non-mutable intentions, but the construction madness has to stop. Nulls are
	 * ignored so the status code remains as is.
	 * 
	 * @param statusCode
	 */
	protected void setStatusCode(HttpStatusCode statusCode) {
		if (statusCode != null) {
			this.statusCode = statusCode;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.exception.MessageException#getErrorCode()
	 */
	@Override
	public String getErrorCode() {
		return MessageResourceBundleExceptionLocalizer.getErrorCode(this.exceptionLocalizer);

	}
}
