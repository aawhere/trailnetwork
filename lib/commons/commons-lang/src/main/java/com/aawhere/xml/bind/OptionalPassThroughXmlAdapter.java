/**
 * 
 */
package com.aawhere.xml.bind;

/**
 * An {@link OptionalXmlAdapter} for special situations where there is no conversion happening, only
 * pass through.
 * 
 * @author aroller
 * 
 * @param <T>
 */
public class OptionalPassThroughXmlAdapter<T>
		extends OptionalXmlAdapter<T, T> {

	/**
	 * 
	 */
	public OptionalPassThroughXmlAdapter() {
		super();
	}

	/**
	 * @param show
	 */
	public OptionalPassThroughXmlAdapter(Boolean show) {
		super(show);
	}

	/**
	 * @param hide
	 */
	public OptionalPassThroughXmlAdapter(com.aawhere.xml.bind.OptionalXmlAdapter.Hide hide) {
		super(hide);
	}

	/**
	 * @param show
	 */
	public OptionalPassThroughXmlAdapter(com.aawhere.xml.bind.OptionalXmlAdapter.Show show) {
		super(show);
	}

	/**
	 * @param visibility
	 */
	public OptionalPassThroughXmlAdapter(com.aawhere.xml.bind.OptionalXmlAdapter.Visibility visibility) {
		super(visibility);
	}

	public static <T> OptionalPassThroughXmlAdapter<T> build(Boolean show) {
		return new OptionalPassThroughXmlAdapter<T>(show);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.xml.bind.OptionalXmlAdapter#showMarshal(java.lang.Object)
	 */
	@Override
	protected T showMarshal(T v) throws Exception {
		return v;
	}

	/**
	 * Since these are the same objects then we can convert in both directions.
	 * 
	 * @see com.aawhere.xml.bind.OptionalXmlAdapter#showUnmarshal(java.lang.Object)
	 */
	@Override
	protected T showUnmarshal(T v) throws Exception {
		return v;
	}

}
