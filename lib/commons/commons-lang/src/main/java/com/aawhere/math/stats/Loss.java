/**
 * 
 */
package com.aawhere.math.stats;

import org.apache.commons.math.stat.descriptive.summary.Sum;

/**
 * The {@link Sum} of all negative numbers, however, the {@link #getResult()} produces a postive
 * number since loss is typically expressed in terms of a postive number.
 * 
 * @author aroller
 * 
 */
public class Loss
		extends FilteredSum {

	private static final long serialVersionUID = 8622404978600873441L;

	/**
	 * 
	 */
	public Loss() {
	}

	/**
	 * @param original
	 */
	public Loss(Sum original) {
		super(original);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.math.stats.FilteredSum#passedFilter(double)
	 */
	@Override
	protected boolean passedFilter(double d) {
		return d < 0;
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.math.stat.descriptive.summary.Sum#getResult()
	 */
	@Override
	public double getResult() {
		double result = super.getResult();
		if (result != 0) {
			result = -result;
		}

		return result;

	}

}
