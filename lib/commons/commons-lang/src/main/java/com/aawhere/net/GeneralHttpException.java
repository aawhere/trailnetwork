/**
 * 
 */
package com.aawhere.net;

import com.aawhere.io.IoException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * A useful way to handle HTTP exceptions that can't provide a more specific message.
 * 
 * @deprecated use {@link IoException}
 * 
 * @author Aaron Roller
 * 
 */
public class GeneralHttpException
		extends BaseException
		implements StatusCodeContainer {

	private static final long serialVersionUID = 1171229963357034564L;
	private HttpStatusCode status;
	private String path;

	/**
	 * @param message
	 */
	public GeneralHttpException(HttpStatusCode status) {
		super(new CompleteMessage.Builder(NetMessage.GENERAL_FAILURE)
				.addParameter(NetMessage.Param.STATUS_CODE, status).build());
		this.status = status;
	}

	public GeneralHttpException(HttpStatusCode status, String path) {
		super(new CompleteMessage.Builder(NetMessage.GENERAL_FAILURE_FOR_PATH)
				.addParameter(NetMessage.Param.STATUS_CODE, status).addParameter(NetMessage.Param.PATH, path).build());
		this.status = status;
		this.path = path;

	}

	/**
	 * @return the status
	 */
	@Override
	public HttpStatusCode getStatusCode() {
		return this.status;
	}

}
