/**
 * 
 */
package com.aawhere.id;

import com.aawhere.lang.If;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Used to stop operations when the same identifier is provided when two different identifiers are
 * expected.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class SameIdentifierException
		extends BaseRuntimeException {

	private static final long serialVersionUID = -8103315890951125087L;

	public SameIdentifierException(Identifier<?, ?> id) {
		super(CompleteMessage.create(IdentifierMessage.SAME_ID).addParameter(IdentifierMessage.Param.IDENTIFIER, id)
				.build());
	}

	/**
	 * Useful method for providing the equality check and throwing if needed.
	 * 
	 * @param left
	 * @param right
	 * @throws SameIdentifierException
	 */
	public static void throwIfEquals(Identifier<?, ?> left, Identifier<?, ?> right) throws SameIdentifierException {
		If.equal(left, right).raise(new SameIdentifierException(left));
	}
}
