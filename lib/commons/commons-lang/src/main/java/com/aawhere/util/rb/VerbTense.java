package com.aawhere.util.rb;

import com.aawhere.text.format.custom.CustomFormat;

/**
 * Used in message select lists to offer different tense forms for verbs
 * 
 * <pre>
 * past = wrote
 * present = is writing
 * future = will write
 * </pre>
 * 
 * You must use the {@link VerbTense} with the {@link Param#TENSE} to get the lowercase standard of
 * the select list: {TENSE_KEY, select, past{wrote} present{is writing} future{will write}}
 * 
 * @author aroller
 * 
 */
public enum VerbTense {
	PAST, PRESENT, FUTURE;

	/**
	 * not intended for user display, this formats the key to be used with select lists.
	 * 
	 * @author aroller
	 * 
	 */
	public enum Param implements SelectParamKey {
		TENSE;

		@Override
		public CustomFormat<?> getFormat() {
			return null;
		}

		@Override
		public String selectKey(Object value) {
			return MessageUtil.selectKey(value);
		}

	}

	private static final ParamKey[] keys = { Param.TENSE };

	/** handy for those items that only need tense parameters. */
	public static ParamKey[] paramKeys() {
		return keys;
	}

	public static CompleteMessage completeMessage(Message message, VerbTense verbTense) {
		return CompleteMessage.create(message).param(VerbTense.Param.TENSE, verbTense).build();
	}
}
