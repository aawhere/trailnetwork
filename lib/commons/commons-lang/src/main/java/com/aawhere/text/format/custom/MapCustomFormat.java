/**
 * 
 */
package com.aawhere.text.format.custom;

import java.util.Locale;
import java.util.Map;

/**
 * @author roller
 * 
 */
public class MapCustomFormat
		implements CustomFormat<Map<?, ?>> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#format(java.lang.Object, java.util.Locale)
	 */
	@Override
	public String format(Map<?, ?> object, Locale locale) {
		return object.toString().replace('{', '[').replace('}', ']');
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#handlesType()
	 */
	@Override
	public Class<? super Map<?, ?>> handlesType() {

		return Map.class;
	}

}
