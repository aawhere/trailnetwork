/**
 * 
 */
package com.aawhere.math;

/**
 * Positional notation or place-value notation is a method of representing or encoding numbers.
 * Positional notation is distinguished from other notations (such as Roman numerals) for its use of
 * the same symbol for the different orders of magnitude (for example, the "ones place",
 * "tens place", "hundreds place").
 * 
 * @see http://en.wikipedia.org/wiki/Positional_notation
 * 
 * @author Aaron Roller
 * 
 */
public enum PlaceValueNotation {
	/** 3rd digit to the left of the decimal. */
	HUNDREDS(100.0),
	/** 2nd digit to the left of the decimal */
	TENS(10.0),
	/** 1st digit to the left of the decimal */
	ONES(1.0),
	/** 1st digit to the right of the decimal */
	TENTH(0.1),
	/** 2nd digit to the right of the decimal */
	HUNDREDTH(0.01),
	/** 3rd digit to the right of the decimal */
	THOUSANDTH(0.001),
	/** 4th digit to the right of the decimal */
	TEN_THOUSANDTH(0.0001),
	/** 5th digit */
	ONE_HUNDRED_THOUSANDTH(0.00001),
	/** 6th digit */
	MILLIONTH(0.000001);

	private Double numericRepresentation;

	/**
	 * @param numericRepresentation
	 */
	private PlaceValueNotation(Double numericRepresentation) {
		this.numericRepresentation = numericRepresentation;
	}

	/**
	 * @return the numericRepresentation
	 */
	public Double getNumericRepresentation() {
		return this.numericRepresentation;
	}

}
