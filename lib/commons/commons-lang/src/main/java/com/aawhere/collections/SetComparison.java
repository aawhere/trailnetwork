/**
 * 
 */
package com.aawhere.collections;

import java.io.Serializable;
import java.util.Set;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

/**
 * Uses {@link Sets#difference(java.util.Set, java.util.Set)} and such to look for equality within a
 * certain tolerance when another set is given during an equals call. Useful as a delegate when
 * perfect equality is not required.
 * 
 * @author aroller
 * 
 */
public class SetComparison<T>
		implements Serializable {

	private static final long serialVersionUID = -8972416544426322598L;

	private Integer differenceAllowed = 0;
	private Set<T> set;
	/**
	 * HashCode must be specifically assigned to a general group that will group similar sets in the
	 * same hash.
	 * 
	 */
	private Integer hashCode;

	/**
	 * Used to construct all instances of SetComparison.
	 */
	public static class Builder<T>
			extends ObjectBuilder<SetComparison<T>> {

		public Builder() {
			super(new SetComparison<T>());
		}

		@Override
		public SetComparison<T> build() {
			SetComparison<T> built = super.build();
			if (built.hashCode == null) {
				built.hashCode = built.set.hashCode();
			}
			return built;
		}

		public Builder<T> hashCode(Integer hashCode) {
			building.hashCode = hashCode;
			return this;
		}

		public Builder<T> differenceAllowed(Integer differenceAllowed) {
			building.differenceAllowed = differenceAllowed;
			return this;
		}

		/**
		 * @param set
		 * @return
		 */
		public Builder<T> set(Set<T> set) {
			building.set = set;

			return this;
		}

	}// end Builder

	public static <T> Builder<T> create() {
		return new Builder<T>();
	}

	private boolean equalsWithinTolerance(Set<T> other) {
		boolean result = false;
		SetView<T> difference = Sets.difference(this.set, other);
		if (difference.size() <= this.differenceAllowed) {
			SetView<T> difference2 = Sets.difference(other, this.set);
			if (difference2.size() <= this.differenceAllowed) {
				result = true;
			}
		}
		return result;
	}

	/*
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return hashCode;
	}

	/*
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		SetComparison<T> other = (SetComparison<T>) obj;
		if (this.set == null) {
			if (other.set != null)
				return false;
		} else if (!equalsWithinTolerance(other.set))
			return false;
		return true;
	}

	/** Use {@link Builder} to construct SetComparison */
	private SetComparison() {
	}

}
