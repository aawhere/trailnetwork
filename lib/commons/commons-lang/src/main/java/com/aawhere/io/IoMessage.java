/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Oct 30, 2010
commons-lang : com.aawhere.io.IoMessages.java
 */
package com.aawhere.io;

import static com.aawhere.io.IoMessage.Param.*;

import com.aawhere.io.zip.CorruptGzipFormatException;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;

/**
 * Messages for the entire com.aawhere.io package.
 * 
 * @author roller
 * 
 */
public enum IoMessage implements Message {

	/** @see IoException **/
	IO_EXCEPTION("There was an I/O problem: {MESSAGE}", Param.MESSAGE),
	/** a 401 status */
	UNAUTHORIZED("Access denied to {URL}", Param.URL),
	/** When the request takes too long */
	TIMEOUT("It took longer than {MAX_TIME_ALLOWED} to download {ITEM}.", Param.MAX_TIME_ALLOWED, Param.ITEM),
	/** @see CorruptGzipFormatException */
	GZIP_CORRUPTION("The gzipped data is corrupt."),
	/** @see FileNotFoundException */
	FILE_NOT_FOUND("The file {FILE_NAME} is not found or is unavailable.", FILE_NAME),
	/** @see DirectoryNotFoundException */
	DIRECTORY_NOT_FOUND("The directory {FILE_NAME} is not found.", FILE_NAME),
	/** When an attempt to serialize is not succeeding. */
	SERIALIZATION_PROBLEM("Could not serialize object {CLASS_NAME}.", Param.CLASS_NAME),

	GONE("{URL} has been removed from the system.", Param.URL);

	private com.aawhere.util.rb.ParamKey[] parameterKeys;
	private String message;

	private IoMessage(String message, com.aawhere.util.rb.ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public com.aawhere.util.rb.ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements com.aawhere.util.rb.ParamKey {
		/** the file name as represented by {@link java.io.File#getName()}. */
		FILE_NAME,
		/** Indicates the class name of the object with problems. */
		CLASS_NAME,
		/** A rare allowance of displaying the message from the exception itself. */
		MESSAGE,
		/** The maximimum time allowed before the enforced failure. */
		MAX_TIME_ALLOWED,
		/** the web address */
		URL,
		/** Identifies the url or id or entity being retrieved or sent. */
		ITEM;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
