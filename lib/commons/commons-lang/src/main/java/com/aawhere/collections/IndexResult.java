/**
 * 
 */
package com.aawhere.collections;

/**
 * Avoiding a magic variable. I can't believe I am the first to declare this constant!
 * 
 * @author Aaron Roller
 * 
 */
public enum IndexResult {

	NOT_FOUND(-1);

	private int index;

	/**
	 * 
	 */
	private IndexResult(int index) {
		this.index = index;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return this.index;
	}

	public static boolean notFound(int indexResult) {
		return indexResult == NOT_FOUND.index;
	}
}
