/**
 *
 */
package com.aawhere.io;

import java.io.IOException;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.GeneralHttpException;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * General exception working with {@link IoMessage}.
 * 
 * Notice this is a 500 error. You may override by providing a more appropriate status code during
 * construction.
 * 
 * 
 * @author brian
 * 
 */
@StatusCode(HttpStatusCode.INTERNAL_ERROR)
public class IoException
		extends BaseException {

	private static final long serialVersionUID = -3159809811253511293L;

	/**
	 * 
	 */
	public IoException(HttpStatusCode statusCode, String explanation) {
		this(new CompleteMessage.Builder(IoMessage.IO_EXCEPTION).addParameter(	IoMessage.Param.MESSAGE,
																				explanation + "(" + statusCode + ")")
				.build());
		super.setStatusCode(statusCode);
	}

	/**
	 * 
	 */
	public IoException(CompleteMessage completeMessage) {
		super(completeMessage);
	}

	/**
	 * When something bad happens and you don't have much to say about it beyond what Java will say.
	 */
	public IoException(IOException other) {
		super(new CompleteMessage.Builder(IoMessage.IO_EXCEPTION).addParameter(	IoMessage.Param.MESSAGE,
																				other.getMessage()).build(), other);
	}

	/**
	 *
	 */
	public IoException(CompleteMessage message, java.io.IOException e) {
		super(message, e);
	}

	/**
	 * Provides the best Exception for the given status code or {@link GeneralHttpException} if
	 * nothing better is known.
	 * 
	 * @param status
	 * @param path
	 * @return
	 * @throws BaseException
	 */
	public static IoException best(HttpStatusCode status, String path) {
		IoException exception;
		switch (status) {
			case UNAUTHORIZED:
				exception = new UnauthorizedException(path);
				break;
			case NOT_FOUND:
				exception = new EndpointNotFoundException(path);
				break;
			case GONE:
				exception = new ResourceGoneException(path);
				break;
			default:
				exception = new IoException(status, path);
		}
		return exception;
	}

}
