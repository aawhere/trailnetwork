/**
 * 
 */
package com.aawhere.adapter;

import java.lang.reflect.Type;

import com.aawhere.lang.string.FromStringAdapter;

/**
 * @author brian
 * 
 * @param <AdaptedT>
 */
public interface AdapterFactory<AdaptedT> {

	/**
	 * Registers the given adapter to handle the class. Multiple adapters of the same type may be
	 * provided as long as the class they are keyed by is unique. This will return the previously
	 * registered class if duplicate registration is found.
	 * 
	 * @param type
	 * @param adapter
	 * @return
	 */
	public abstract <T> Adapter<T, AdaptedT> register(Adapter<T, AdaptedT> adapter);

	/**
	 * a pre-investigation to avoid the runtime exception when retrieving an adapter.
	 * 
	 * @param type
	 * @return
	 */
	public abstract Boolean handles(Type type);

	/**
	 * For situations where a parameterized type is required to provide complete adaptation, this is
	 * used to indicate if such a situation is possible.
	 * 
	 * @param type
	 * @param parameterType
	 * @return
	 */
	public abstract Boolean handles(Type type, Class<?> parameterType);

	/**
	 * Provides a null if adapter is not found for those that prefer.
	 * 
	 * This will investigate if there is a super class registered that apparently can provide
	 * adaptation for it's child class.
	 * 
	 * Default implementations provided to handle common classes and patterns that use
	 * {@link FromStringAdapter}.
	 * 
	 * @param type
	 *            the type you need to adapt
	 * @param parameterType
	 *            the type contained inside the type. Typically used for collections.
	 * @return
	 */
	public abstract <P, T> Adapter<T, AdaptedT> getAdapterOrNull(Type type, Class<P> parameterType);

	/**
	 * @see #getAdapterOrNull(Type, Class)
	 * 
	 * @param type
	 * @return
	 */
	public abstract <T> Adapter<T, AdaptedT> getAdapterOrNull(Type type);

	/**
	 * @see #getAdapterOrNull(Type)
	 * 
	 * @param type
	 * @return
	 */
	public abstract <T> Adapter<T, AdaptedT> getAdapter(Type type);

	/**
	 * @see #getAdapterOrNull(Type, Class)
	 * 
	 * @param type
	 * @param parameterType
	 * @return
	 */
	public abstract <T> Adapter<T, AdaptedT> getAdapter(Type type, Class<?> parameterType);

}