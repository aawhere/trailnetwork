/**
 * 
 */
package com.aawhere.collections;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

/**
 * Provides the ability to count the number of items in an iterable.
 * {@link Iterables#size(Iterable)} works, but this can be inserted between two iterables to find
 * out if one is filtering out items.
 * 
 * @author aroller
 * 
 */
public class CountingPredicate<T>
		implements Predicate<T> {

	private int count;

	/**
	 * 
	 */
	public CountingPredicate() {

	}

	public static <T> CountingPredicate<T> build() {
		return new CountingPredicate<T>();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(T input) {
		this.count++;
		return true;
	}

	/**
	 * @return
	 */
	public Integer count() {
		return this.count;
	}

}
