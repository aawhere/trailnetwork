/**
 * 
 */
package com.aawhere.collections;

import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.xml.XmlNamespace;

/**
 * Simple adapter to handle any implementation of {@link Entry}.
 * 
 * @author aroller
 * 
 */
public class MapEntryXmlAdapter<K, V>
		extends XmlAdapter<MapEntryXmlAdapter.XmlMapEntry<K, V>, Map.Entry<K, V>> {

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.NONE)
	public static class XmlMapEntry<K, V>
			implements Map.Entry<K, V> {

		public XmlMapEntry() {
		}

		/**
		 * @param entry
		 */
		public XmlMapEntry(java.util.Map.Entry<K, V> entry) {
			this.key = entry.getKey();
			this.value = entry.getValue();
		}

		@XmlElement
		private K key;
		@XmlElement
		private V value;

		/*
		 * (non-Javadoc)
		 * @see java.util.Map.Entry#getKey()
		 */
		@Override
		public K getKey() {
			return this.key;
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Map.Entry#getValue()
		 */
		@Override
		public V getValue() {
			return this.value;
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Map.Entry#setValue(java.lang.Object)
		 */
		@Override
		public V setValue(V value) {
			V old = this.value;
			this.value = value;
			return old;
		}

		public final boolean equals(Object o) {
			if (!(o instanceof Map.Entry))
				return false;
			Map.Entry e = (Map.Entry) o;
			Object k1 = getKey();
			Object k2 = e.getKey();
			if (k1 == k2 || (k1 != null && k1.equals(k2))) {
				Object v1 = getValue();
				Object v2 = e.getValue();
				if (v1 == v2 || (v1 != null && v1.equals(v2)))
					return true;
			}
			return false;
		}

		public final int hashCode() {
			return (key == null ? 0 : key.hashCode()) ^ (value == null ? 0 : value.hashCode());
		}

		public final String toString() {
			return getKey() + "=" + getValue();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Entry<K, V> unmarshal(XmlMapEntry<K, V> xml) throws Exception {
		return xml;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public XmlMapEntry<K, V> marshal(Entry<K, V> entry) throws Exception {
		if (entry == null) {
			return null;
		}
		return new XmlMapEntry<>(entry);
	}
}
