/**
 *
 */
package com.aawhere.lang.string;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.adapter.Adapter;

/**
 * Adapts a String into the object represented by the template and produces a string that represents
 * an object. This type of adapter does not encrypting or decrypting of localization or
 * personalization, but indicates a universal translation such analogous to
 * {@link Integer#valueOf(String)}, etc.
 * 
 * This is based on the {@link XmlAdapter} design.
 * 
 * @author aroller
 * 
 */
public interface StringAdapter<T>
		extends Adapter<T, String> {

}
