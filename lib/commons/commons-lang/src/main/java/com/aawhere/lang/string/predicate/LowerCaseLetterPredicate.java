/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 1, 2010
commons-lang : com.aawhere.lang.string.predicate.LowerCaseLetterPredicate.java
 */
package com.aawhere.lang.string.predicate;

import org.apache.commons.lang3.StringUtils;

/**
 * Requires all are lower case.
 * 
 * @see StringUtils#isAllLowerCase(CharSequence)
 * @author roller
 * 
 */
public class LowerCaseLetterPredicate
		extends CharSequencePredicate {

	private static LowerCaseLetterPredicate instance;

	private LowerCaseLetterPredicate() {
	}

	public static LowerCaseLetterPredicate getInstance() {
		if (null == instance) {
			instance = new LowerCaseLetterPredicate();
		}
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.predicate.CharSequencePredicate#evaluate(java.lang.CharSequence)
	 */
	@Override
	public boolean evaluate(CharSequence target) {

		return StringUtils.isAllLowerCase(target);
	}

}
