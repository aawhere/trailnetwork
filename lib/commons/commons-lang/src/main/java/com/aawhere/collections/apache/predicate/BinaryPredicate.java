/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 4, 2010
commons-lang : com.aawhere.collections.apache.functor.BinaryPredicate.java
 */
package com.aawhere.collections.apache.predicate;

/**
 * Value provided during construction is used for evaluation when compared to the object passed
 * during {@link #evaluate(Object) evaluation}.
 * 
 * The constructor value is considered the right operand and the one provided during evaluation is
 * considered the left operand. Hence:
 * 
 * <pre>
 * leftOperand < rightOperand = result
 * </pre>
 * 
 * normally written as ...
 * 
 * <pre>
 * result = leftOperand.lessThan(rightOperand)
 * </pre>
 * 
 * actually written
 * 
 * <pre>
 * max = new BinaryPredicate(rightOperand);
 * max.evaluate(leftOperand);
 * </pre>
 * 
 * 
 * 
 * @author roller
 * 
 */
public abstract class BinaryPredicate<ValueT>
		extends GenericPredicate<ValueT> {

	private ValueT rightOperand;

	/**
	 * 
	 */
	public BinaryPredicate(ValueT right) {
		this.rightOperand = right;
	}

	/**
	 * @return the rightOperand
	 */
	public ValueT getRightOperand() {
		return rightOperand;
	}

	@Override
	abstract public boolean evaluateCustom(ValueT leftOperand);
}
