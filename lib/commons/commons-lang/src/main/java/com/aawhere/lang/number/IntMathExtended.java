/**
 * 
 */
package com.aawhere.lang.number;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.base.Function;
import com.google.common.math.IntMath;
import com.google.common.primitives.Primitives;

/**
 * Similar to {@link IntMath} and {@link Math} which it delegates most of it's work, however, does
 * null checks of {@link Primitives}.
 * 
 * The general rules apply to each unless specified otherwise.
 * 
 * <ul>
 * <li>If both are null an null is returned</li>
 * <li>If one is null and the other is not, the nonnull is returned.</li>
 * <li>If both are not null, the operation requested is performend and a nonnull value is returned.</li>
 * </ul>
 * 
 * @see #apply(Integer, Integer, Function)
 * @author aroller
 * 
 */
public class IntMathExtended {

	/**
	 * 
	 * @param left
	 * @param right
	 * @return the max of the two integers or null if both are null.
	 */
	@Nullable
	public static Integer max(@Nullable Integer left, @Nullable Integer right) {
		return apply(left, right, maxFunction());
	}

	@Nullable
	public static Integer min(@Nullable Integer left, @Nullable Integer right) {
		return apply(left, right, minFunction());
	}

	@Nullable
	public static Integer add(@Nullable Integer left, @Nullable Integer right) {
		return apply(left, right, additionFunction());
	}

	@Nullable
	public static Integer subtract(@Nullable Integer left, @Nullable Integer right) {
		return apply(left, right, subtractionFunction());
	}

	/**
	 * The internal worker that mostly does null checks and then delegates to the given function,
	 * which that function should subsequently delegate.
	 * 
	 * @param left
	 * @param right
	 * @param function
	 * @return
	 */
	private static Integer apply(Integer left, Integer right, Function<Pair<Integer, Integer>, Integer> function) {
		Integer result;
		if (left == null) {
			if (right == null) {
				result = null;
			} else {
				result = right;
			}
		} else {
			if (right == null) {
				return left;
			} else {
				result = function.apply(Pair.of(left, right));
			}
		}
		return result;
	}

	private static Function<Pair<Integer, Integer>, Integer> additionFunction() {
		return new Function<Pair<Integer, Integer>, Integer>() {

			@Override
			public Integer apply(Pair<Integer, Integer> input) {
				return (input == null) ? null : input.getLeft() + input.getRight();
			}
		};
	}

	private static Function<Pair<Integer, Integer>, Integer> subtractionFunction() {
		return new Function<Pair<Integer, Integer>, Integer>() {

			@Override
			public Integer apply(Pair<Integer, Integer> input) {
				return (input == null) ? null : input.getLeft() - input.getRight();
			}
		};
	}

	private static Function<Pair<Integer, Integer>, Integer> minFunction() {
		return new Function<Pair<Integer, Integer>, Integer>() {

			@Override
			public Integer apply(Pair<Integer, Integer> input) {
				return (input == null) ? null : Math.min(input.getLeft(), input.getRight());
			}
		};
	}

	/** make this less private if you find a need to share it. */
	private static Function<Pair<Integer, Integer>, Integer> maxFunction() {
		return new Function<Pair<Integer, Integer>, Integer>() {

			@Override
			public Integer apply(Pair<Integer, Integer> input) {
				return (input == null) ? null : Math.max(input.getLeft(), input.getRight());
			}
		};
	}

}
