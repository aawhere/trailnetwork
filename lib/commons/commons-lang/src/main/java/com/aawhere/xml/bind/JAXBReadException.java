/**
 * 
 */
package com.aawhere.xml.bind;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * General exception that happens when unmarshalling xml into a JAXB object.
 * 
 * @author roller
 * 
 */
@StatusCode(HttpStatusCode.UNPROCESSABLE_ENTITY)
public class JAXBReadException
		extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7783433114974290119L;

	/**
	 * @param cause
	 */
	public JAXBReadException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param e
	 */
	public JAXBReadException(Message message, Exception e) {
		super(message, e);
	}

	/**
	 * @param message
	 * @param paramKey
	 * @param paramValue
	 */
	public JAXBReadException(Message message, ParamKey paramKey, Object paramValue) {
		super(message, paramKey, paramValue);
	}

	/**
	 * @param message
	 */
	public JAXBReadException(Message message) {
		super(message);
	}

}
