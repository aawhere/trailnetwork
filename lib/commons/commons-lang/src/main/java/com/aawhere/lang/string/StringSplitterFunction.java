/**
 * 
 */
package com.aawhere.lang.string;

import java.util.regex.Pattern;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;
import com.google.common.base.Splitter;

/**
 * Splits the given String into the multiple strings.
 * 
 * @author aroller
 * 
 */
public class StringSplitterFunction
		implements Function<String, Iterable<String>> {

	/**
	 * Used to construct all instances of StringSplitterFunction.
	 */
	public static class Builder
			extends ObjectBuilder<StringSplitterFunction> {

		public Builder() {
			super(new StringSplitterFunction());
		}

		public Builder splitter(Splitter splitter) {
			building.splitter = splitter;
			return this;
		}

		@Override
		public StringSplitterFunction build() {
			StringSplitterFunction built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct StringSplitterFunction */
	private StringSplitterFunction() {
	}

	public static Builder create() {
		return new Builder();
	}

	private Splitter splitter;

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Iterable<String> apply(String input) {
		if (input == null) {
			return null;
		}
		return splitter.split(input);
	}

}
