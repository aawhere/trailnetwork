/**
 *
 */
package com.aawhere.util.rb;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A singleton that provides managed resources available in the system.
 * 
 * ManagedResources must be registered to the singleton so they are available for inspection when
 * the factory methods are queried.
 * 
 * Typically, registration is done at system initialization providing all managed resources that
 * should be used in the system.
 * 
 * 
 * @author roller
 * 
 */
public class MessageResourceBundleFactory {

	private static MessageResourceBundleFactory instance;

	private Set<MessageResourceBundle> bundles = new HashSet<MessageResourceBundle>();

	public static MessageResourceBundleFactory getInstance() {
		if (instance == null) { // avoid sync penalty if we can
			synchronized (MessageResourceBundleFactory.class) {
				if (instance == null) {
					instance = new MessageResourceBundleFactory();
				}
			}
		}
		return instance;
	}

	/**
	 * Inspects all the registerd managed bundles to see if they have a matching key. Returns the
	 * FIRST BUNDLE FOUND so keys not unique will be error-proned.
	 * 
	 * @param key
	 * @return
	 */
	public MessageResourceBundle getManagedResourceBundle(String key) {

		Iterator<MessageResourceBundle> bundleIterator = this.bundles.iterator();
		MessageResourceBundle result = null;
		while (bundleIterator.hasNext() && result == null) {
			MessageResourceBundle bundle = bundleIterator.next();
			if (bundle.containsKey(key)) {
				result = bundle;
			}
		}
		return result;
	}

	public MessageResourceBundle getManagedResourceBundleFromMessage(Message key) {
		if (MessageUtil.isEnum(key)) {

			return getManagedResourceBundle(MessageUtil.messageAsEnum(key));

		} else {
			throw new RuntimeException("Sorry, only enums that extend message are supported at this time."
					+ key.getClass());
		}
	}

	public MessageResourceBundle getManagedResourceBundle(Enum<? extends Message> key) {

		MessageResourceBundle result = getManagedResourceBundle(key.toString());
		if (result == null) {
			result = register(new MessageResourceBundle(key.getDeclaringClass()));
		}
		return result;
	}

	/**
	 * Registered the bundle making it available for messages and localization.
	 * 
	 * @param bundle
	 * @return the same bundle passed in for convenience
	 */
	public MessageResourceBundle register(MessageResourceBundle bundle) {
		bundles.add(bundle);
		return bundle;
	}

}
