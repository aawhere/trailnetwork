/**
 * 
 */
package com.aawhere.io;

import java.io.File;

/**
 * An extension of File providing integrity to ensure the file is a directory and other useful
 * methods for Directories only.
 * 
 * @see File#isDirectory()
 * 
 * @author roller
 * 
 */
public class Directory
		extends File {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8161025852684293134L;

	/**
	 * Given a file that is a directory this will abosorb it into a type-safe file.
	 * 
	 * @param directoryAsFile
	 *            the existing file.
	 * @throws DirectoryNotFoundException
	 * @throws NotADirectoryException
	 */
	public Directory(File directoryAsFile) throws NotADirectoryException {
		this(directoryAsFile.getAbsolutePath());
	}

	/**
	 * Given the parent directory and a name for the new directory.
	 * 
	 * @see File#File(File, String)
	 * 
	 * @param parent
	 * @param newDirectoryName
	 * @throws DirectoryNotFoundException
	 * @throws NotADirectoryException
	 */
	public Directory(File parent, String newDirectoryName) throws NotADirectoryException {
		super(parent, newDirectoryName);
		initialize();

	}

	public Directory(String pathname) throws NotADirectoryException {
		super(pathname);
		initialize();
	}

	private void initialize() throws NotADirectoryException {
		if (!exists()) {
			boolean directoriesMade = this.mkdirs();
			if (!directoriesMade) {
				throw new RuntimeException("Unable to create directory " + getAbsolutePath());
			}
		}
		if (!isDirectory()) {
			throw new NotADirectoryException(this);
		}
	}

}
