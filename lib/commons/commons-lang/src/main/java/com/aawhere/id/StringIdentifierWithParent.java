/**
 * 
 */
package com.aawhere.id;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.google.common.base.Function;
import com.google.common.base.Functions;

/**
 * Provides a base for {@link StringIdentifier} that {@link HasParent}. The parent id is not
 * embedded in the {@link #getValue()}, but is available from {@link #getParent()}.
 * {@link #toString()} produces a composite id with both parent and value so it can fully represent
 * this as a string and be reconstructed {@link #StringIdentifierWithParent(String, Class, Class)}.
 * 
 * 
 * @author aroller
 * 
 */
@XmlTransient
@XmlAccessorType(XmlAccessType.NONE)
abstract public class StringIdentifierWithParent<KindT, ParentT extends Identifier<?, ?>>
		extends StringIdentifier<KindT>
		implements HasParent<ParentT> {

	private ParentT parent;

	private static final Function<String, String> valueFunction = Functions.identity();

	/**
	 * 
	 */
	protected StringIdentifierWithParent(Class<KindT> kind) {
		super(kind);
		this.parent = null;
	}

	@SuppressWarnings("unchecked")
	protected StringIdentifierWithParent(IdentifierWithParent<String, ParentT> delegate) {
		super(delegate.value(), (Class<KindT>) delegate.kind());
		this.parent = delegate.parent();
	}

	/**
	 * 
	 */
	public StringIdentifierWithParent(String value, Class<ParentT> parentType, Class<KindT> kind) {
		this(IdentifierWithParent.<String, ParentT> create().compositeValue(value).parentType(parentType).kind(kind)
				.valueFunction(valueFunction).build());
	}

	/**
	 * @param value
	 * @param kind
	 */
	public StringIdentifierWithParent(String value, ParentT parent, Class<KindT> kind) {
		this(IdentifierWithParent.<String, ParentT> create().value(value).parent(parent).kind(kind).build());
	}

	/** Provides the parent-value combination. */
	@XmlAttribute
	public String getCompositeValue() {
		return toString();
	}

	/**
	 * Assists childrent that choose to pass the delegate to the constructor.
	 * 
	 * @param parentType
	 * @return
	 */
	protected static <ParentT extends Identifier<?, ?>> IdentifierWithParent.Builder<String, ParentT> delegateBuilder(
			Class<ParentT> parentType) {
		return IdentifierWithParent.<String, ParentT> create().valueFunction(valueFunction).parentType(parentType);
	}

	public ParentT parent() {
		return this.parent;
	}

	/**
	 *
	 */
	@Override
	@XmlElement
	public ParentT getParent() {
		return this.parent;
	}

	/**
	 * Unfortunately required for RoundTrip xml adaptation. The default constructor is invoked by
	 * the unmarshaler, then the parent id must be assigned which can happen if the parent is a
	 * variable, however, the resulting xml is cryptic with xsi:type instead of a concrete xml
	 * element (like RouteId).
	 * 
	 * @param parent
	 */
	protected void setParent(ParentT parent) {
		if (this.parent != null) {
			if (!this.parent.equals(parent)) {
				throw new UnsupportedOperationException(" this mutator is available only for JAXB during unmarshaling.");
			}
		}
		this.parent = parent;
	}

	/*
	 * @see com.aawhere.id.HasParent#isRoot()
	 */
	@Override
	public Boolean isRoot() {
		return this.parent == null;
	}

	private static final long serialVersionUID = 8958963678496211574L;
}
