/**
 * 
 */
package com.aawhere.util.rb;

/**
 * An indicator of success, failure or other status levels used by {@link StatusMessages} to
 * organize {@link CompleteMessage}s by level.
 * 
 * Typically an enum implementing this interface.
 * 
 * It is a requirement that the {@link #equals(Object)} and {@link #hashCode()} methods work
 * properly since these are used as keys in maps.
 * 
 * @see Standard for the typical message status.
 * 
 * @author aroller
 * 
 */
public enum MessageStatus implements Message {

	/** General case positive */
	OK("OK"),
	/** When something unusual happened, but still OK or partially OK. */
	WARNING("Warning"),
	/** Not OK */
	ERROR("Error"),
	/** Synonym to OK */
	SUCCESS("Success"),
	/** Synonym to Error */
	FAILURE("Failure");

	private ParamKey[] parameterKeys;
	private String message;

	private MessageStatus(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

}
