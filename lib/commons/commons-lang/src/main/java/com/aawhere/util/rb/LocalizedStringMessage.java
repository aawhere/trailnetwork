/**
 * 
 */
package com.aawhere.util.rb;

import java.util.Locale;
import java.util.ResourceBundle;

import com.aawhere.i18n.Localizable;

/**
 * A message, not from a {@link ResourceBundle}, that provides the locale indicating the i18n of the
 * value.
 * 
 * @author roller
 * 
 */
public class LocalizedStringMessage
		extends StringMessage
		implements Localizable {

	private static final long serialVersionUID = 2597854179969020286L;
	final private Locale locale;

	/**
	 * @param value
	 */
	public LocalizedStringMessage(String value, Locale locale) {
		super(value);
		this.locale = locale;
	}

	/**
	 * @return the locale
	 */
	@Override
	public Locale getLocale() {
		return this.locale;
	}

}
