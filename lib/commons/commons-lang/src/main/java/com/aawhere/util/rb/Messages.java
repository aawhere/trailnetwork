package com.aawhere.util.rb;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.xml.XmlNamespace;

/**
 * An ordered collection of messages
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class Messages {

	@XmlElement
	private List<Message> messages;

	@SuppressWarnings("unchecked")
	public Messages(List<? extends Message> messages) {
		this.messages = (List<Message>) messages;
	}

	// for jaxb
	@SuppressWarnings("unused")
	private Messages() {
	}

	/**
	 * @return the messages
	 */
	public List<Message> messages() {
		return this.messages;
	}
}
