/**
 *
 */
package com.aawhere.id;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.xml.XmlNamespace;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * An portable base class of {@link Identifier} for {@link Long} values.
 * 
 * 
 * @author Aaron Roller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
// @XmlTransient
// @XmlAccessorType(XmlAccessType.NONE)
abstract public class LongIdentifier<KindT>
		extends Identifier<Long, KindT> {

	@XmlAttribute
	@ApiModelProperty(required = true, value = "The primitive id value.")
	private Long value;

	/**
	 * @param other
	 */
	public LongIdentifier(Identifier<Long, KindT> other) {
		super(other);
	}

	private static final long serialVersionUID = 981561178463874915L;

	/**
	 * Provided for XML and persistence libraries only.
	 * 
	 */
	protected LongIdentifier() {
		super();
	}

	/**
	 * Used by concrete class default constructor to provide the Kind that they always know.
	 * 
	 * @param kind
	 */
	protected LongIdentifier(Class<KindT> kind) {
		super(kind);
	}

	/**
	 * The typical method used by the caller of the child id.
	 * 
	 * @param value
	 * @param kind
	 */
	protected LongIdentifier(Long value, Class<KindT> kind) {
		super(value, kind);
		this.value = value;
	}

	/**
	 * A convenience method to be used by any client that has a string version of a long.
	 * 
	 * TODO: throw a {@link BaseRuntimeException} so the message will be clear to the api user.
	 * 
	 * @param value
	 * @param kind
	 * @throws NumberFormatException
	 *             when the value provided is not a long
	 */
	protected LongIdentifier(String value, Class<KindT> kind) {
		this(Long.parseLong(value), kind);
	}

	/*
	 * Override to allow JaxB to see the @XmlValue annotation. You can't place this on the super
	 * class due to the presence of the comparable interface. (non-Javadoc).
	 * @see com.aawhere.id.Identifier#getValue()
	 */
	@Override
	public Long getValue() {
		return this.value;
	}

}
