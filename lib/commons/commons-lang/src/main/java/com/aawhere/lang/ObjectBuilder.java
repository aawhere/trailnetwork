/**
 * 
 */
package com.aawhere.lang;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.reflect.BuilderReflection;

/**
 * Used by all objects in the system to provide read-only objects, but simpler ways to build an
 * object beyond many constructors.
 * 
 * @see <a
 *      href="http://aawhere.jira.com/wiki/display/AAWHERE/Read-Only+Objects+-+Builder+Pattern">documentation</a>
 * 
 * @see BuilderReflection
 * 
 * @author roller
 * 
 */
@XmlTransient
public abstract class ObjectBuilder<BuilderResult> {

	/**
	 * The Object being built.
	 * 
	 */
	protected BuilderResult building;
	private Boolean nonMutable;

	protected ObjectBuilder(BuilderResult building) {
		Assertion.assertNotNull("building", building);
		this.building = building;
		this.nonMutable = building instanceof NonMutable;
		if (this.nonMutable) {
			((NonMutable) building).mutating = true;
		}

	}

	/**
	 * Optionally implemented by children to call various validate methods or provide validation
	 * themselves to ensure proper construction (such as not null, correct values, etc.). This takes
	 * place of constructors requiring assignment during compile time and instead corrects
	 * situations during runtime.
	 */
	protected void validate() {
		if (this.building == null) {
			throw new UnsupportedOperationException("You can't build more than once");
		}
	}

	public BuilderResult build() {
		validate();
		BuilderResult result = building;
		if (this.nonMutable) {
			((NonMutable) building).mutating = false;
		}
		building = null;
		return result;
	}
}
