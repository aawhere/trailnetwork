/**
 *
 */
package com.aawhere.number.format.custom;

import java.text.DecimalFormat;

/**
 * TODO: Is this duplicating {@link com.aawhere.text.format.custom.DoubleFormat}?
 * 
 * @author Brian Chapman
 * 
 */
public class DoubleFormat
		implements CustomNumberFormat<Double> {

	int numDigits;

	public DoubleFormat(int numDigits) {
		this.numDigits = numDigits;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.number.format.custom.CustomFormat#format(java.lang.Number)
	 */
	@Override
	public Double format(Double value) {
		DecimalFormat twoDForm = getDecimalFormat();
		return Double.valueOf(twoDForm.format(value));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.number.format.custom.CustomFormat#handlesType()
	 */
	@Override
	public Class<Double> handlesType() {
		return Double.class;
	}

	private DecimalFormat getDecimalFormat() {
		StringBuilder str = new StringBuilder();
		str.append("#.");
		for (int i = 0; i < numDigits; i++) {
			str.append("#");
		}
		return new DecimalFormat(str.toString());
	}
}
