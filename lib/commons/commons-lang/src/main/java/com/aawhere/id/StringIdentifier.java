/**
 *
 */
package com.aawhere.id;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

/**
 * An XML compatible verion of Identifier, this must be extended to be used for it's XML access
 * properties.
 * 
 * NOTE: {@link XmlValue} won't work in the generic class because, it's generic and is identified
 * only by an interface.
 * 
 * @author Aaron Roller
 * 
 */
@XmlTransient
@XmlAccessorType(XmlAccessType.NONE)
abstract public class StringIdentifier<KindT>
		extends Identifier<String, KindT> {
	private static final long serialVersionUID = -132013302028718366L;

	/**
	 * duplicating the value attribute here for better JAXB usage.
	 * 
	 */
	@XmlAttribute
	private String value;

	/**
	 * Default constructor for persistence and xml libraries.
	 * 
	 */
	protected StringIdentifier() {
		super();
	}

	/**
	 * Default constructor to be used by children's default constructor when they already know the
	 * Kind.
	 * 
	 * @param kind
	 */
	protected StringIdentifier(Class<KindT> kind) {
		super(kind);
	}

	/**
	 * The typical constructor used in a general case.
	 * 
	 * @param value
	 *            the value of this identifier.
	 * @param kind
	 *            provides type safety
	 */
	protected StringIdentifier(String value, Class<KindT> kind) {
		super(value, kind);
		this.value = value;
	}

	/*
	 * Override to allow JaxB to see the @XmlValue annotation. You can't place this on the super
	 * class due to the presence of the comparable interface. (non-Javadoc). (non-Javadoc)
	 * @see com.aawhere.id.Identifier#getValue()
	 */
	@Override
	public String getValue() {
		return this.value;
	}

}
