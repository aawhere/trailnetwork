/**
 *
 */
package com.aawhere.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.StringFormatException;

/**
 * Adapts a object of type AdapteeT into an object of type AdaptedT.
 * 
 * This is based on the {@link XmlAdapter} design.
 * 
 * @author Brian Chapman
 * 
 */
public interface Adapter<AdapteeT, AdaptedT> {

	/**
	 * Given a value of type AdapteeT, this will provide an object of type AdaptedT that is capable
	 * of being {@link #unmarshal(AdaptedT)} ed back into this object.
	 * 
	 * @param adaptee
	 * @return
	 */
	public AdaptedT marshal(Object adaptee);

	/**
	 * Given an object of type AdaptedT, this will provide an object of type AdapteeT that is
	 * capable of being {@link #marshal(AdapteeT)}ed back into this object.
	 * 
	 * @param adapted
	 * @param type
	 *            is the concrete type that is expected. This may often be the same as
	 *            {@link #getType()}, but supports the situation when {@link #getType()} provides a
	 *            super type of this type since it may be able handle a whole hierarchy of
	 *            adaptations.
	 * @return
	 * @throws StringFormatException
	 *             when the given string is not able to be marshaled into the desired value.
	 */
	public <A extends AdapteeT> A unmarshal(AdaptedT adapted, Class<A> type) throws BaseException;

	/**
	 * The type that this adapter will adapt. This may be the actual type provided in
	 * {@link #marshal(AdapteeT)} or a super type if the adapter can handle a hierarchy.
	 */
	public Class<AdapteeT> getType();
}
