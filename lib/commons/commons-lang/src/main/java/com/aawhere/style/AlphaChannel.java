/**
 * 
 */
package com.aawhere.style;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * A simple class used with {@link Color} to provide the alpha component for trasnparency/opacity.
 * 
 * 
 * http://en.wikipedia.org/wiki/Alpha_compositing
 * 
 * 
 * @author aroller
 * 
 */
public class AlphaChannel {

	public static int MIN = 0;
	public static int MAX = 255;
	public static AlphaChannel OPAQUE = create().setZeroTo255(MAX).build();
	public static AlphaChannel TRANSPARENT = create().setZeroTo255(MIN).build();
	public static AlphaChannel HALF = OPAQUE.half();
	public static AlphaChannel ALMOST_OPAQUE = OPAQUE.threeQuarters();
	public static AlphaChannel ALMOST_TRANSPARENT = OPAQUE.quarter();

	/**
	 * Used to construct all instances of AlphaChannel.
	 */
	public static class Builder
			extends ObjectBuilder<AlphaChannel> {

		public Builder() {
			super(new AlphaChannel());
		}

		public Builder setZeroTo255(int alpha) {
			building.alpha = alpha;
			return this;
		}

		public Builder setZeroTo255(Number alpha) {
			setZeroTo255(alpha.intValue());
			return this;
		}

		public Builder setZeroToOne(Float zeroToOne) {
			setZeroTo255(zeroToOne * MAX);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().assertRange("alpha", building.alpha, MIN, MAX);
		}

		public AlphaChannel build() {
			AlphaChannel built = super.build();
			return built;
		}

	}// end Builder

	private int alpha;

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct AlphaChannel */
	private AlphaChannel() {
	}

	/**
	 * Given an existing color this will apply this {@link AlphaChannel} to the alpha component of
	 * the color provided and return the new Color representing the choice.
	 * 
	 * @param color
	 * @return
	 */
	public Color apply(Color color) {
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), this.alpha);
	}

	/** Converts the alpha to a float ranging between 0 (transparent) to 1 (opaque). */
	public Float zeroToOne() {
		return (float) this.alpha / MAX;
	}

	/**
	 * Reduces the {@link #zeroTo255()} to {@link Byte#MIN_VALUE} to {@link Byte#MAX_VALUE}.
	 * 
	 * @return
	 */
	public byte byteValue() {
		return (byte) this.alpha;
	}

	public Integer zeroTo255() {
		return this.alpha;
	}

	/** reduction to 3/4 of the current value */
	public AlphaChannel threeQuarters() {
		return create().setZeroTo255(3 * alpha / 4).build();
	}

	/** reduction to 1/2 of the current value */
	public AlphaChannel half() {
		return create().setZeroTo255(alpha / 2).build();
	}

	/** reduction to 1/4 of the current value */
	public AlphaChannel quarter() {
		return create().setZeroTo255(alpha / 4).build();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.alpha;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlphaChannel other = (AlphaChannel) obj;
		if (this.alpha != other.alpha)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.valueOf(this.alpha);
	}

	/**
	 * Given a color this will extract the alpha channel from the color and provide it as the
	 * assigned value so you won't have to.
	 * 
	 * @param color
	 */
	public static Builder create(Color color) {
		return create().setZeroTo255(color.getAlpha());
	}

}
