/**
 * 
 */
package com.aawhere.lang.string.predicate;

import org.apache.commons.collections.Predicate;

/**
 * Returns true if the
 * 
 * @author Brian Chapman
 * 
 */
public class StringNotEmptyPredicate
		extends CharSequencePredicate {

	/** Singleton predicate instance */
	public static final Predicate INSTANCE = new StringNotEmptyPredicate();

	/**
	 * Factory returning the singleton instance.
	 * 
	 * @return the singleton instance
	 */
	public static Predicate getInstance() {
		return INSTANCE;
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(CharSequence charSequence) {
		return charSequence.length() != 0;
	}

}
