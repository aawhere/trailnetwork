/**
 * 
 */
package com.aawhere.lang.exception;

import java.util.Locale;

import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageResourceBundle;
import com.aawhere.util.rb.MessageResourceBundleFactory;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Given the strongly type-safe {@link MessageResourceBundle} and it's associated {@link Message}
 * and {@link ParamKey}s this will produce the localized message from the Managed resource.
 * 
 * @author roller
 * 
 */
public class MessageResourceBundleExceptionLocalizer
		implements ExceptionLocalizer {

	private static final long serialVersionUID = -8069383932438546417L;
	private Exception cause;
	private CompleteMessage dynamicMessage;
	// TODO:Make this actually localizable...by injection?
	private Locale locale = Locale.getDefault();
	private MessageResourceBundle resourceBundle;

	MessageResourceBundleExceptionLocalizer(MessageResourceBundle resourceBundle, CompleteMessage dynamicMessage,
			Exception cause) {
		this.cause = cause;
		this.dynamicMessage = dynamicMessage;
		this.resourceBundle = resourceBundle;
	}

	/*
	 * (non-Javadoc)
	 * @see com.akuacom.common.exception.ExceptionLocalizer#getCause()
	 */
	@Override
	public Exception getCause() {
		return this.cause;
	}

	/**
	 * @return the dynamicMessage
	 */
	public CompleteMessage getDynamicMessage() {
		return dynamicMessage;
	}

	/*
	 * (non-Javadoc)
	 * @see com.akuacom.common.exception.ExceptionLocalizer#getMessage()
	 */
	@Override
	public String getMessage() {
		return this.resourceBundle.formatMessage(this.dynamicMessage, this.locale);
	}

	/**
	 * Used to produce instances of the Localizer given the {@link MessageResourceBundle} and the
	 * {@link CompleteMessage} indicating the message key and it's parameters.
	 * 
	 * @author roller
	 * 
	 */
	static class Producer
			implements ExceptionLocalizerProducer {

		/**
		 * Default reserved for Factory loading for defaults.
		 * 
		 */
		Producer() {
		}

		public Producer(Class<? extends Enum<? extends Message>> keyClass) {
		}

		@Override
		public ExceptionLocalizer getDefault(Object criteria, Exception cause) {

			if (criteria instanceof Message) {
				criteria = new CompleteMessage.Builder().setMessage((Message) criteria);
			}
			if (criteria instanceof CompleteMessage) {

				Message message = ((CompleteMessage) criteria).getMessage();
				String exceptionMessage;
				if (message.getValue() == null) {
					exceptionMessage = message.toString();
				} else {
					// in this case, the resource bundle is not registered, but
					// the value of the message is provided in the Enum
					exceptionMessage = message.getValue();
				}

				return new DefaultExceptionLocalizer(exceptionMessage, cause);
			} else {
				return null;
			}
		}

		/**
		 * Looks to see if the criteria is a {@link Message} or {@link CompleteMessage} and returns
		 * a {@link MessageResourceBundleExceptionLocalizer} in such cases. .
		 */
		@Override
		public ExceptionLocalizer understands(Object criteria, Exception cause) {

			// convert a plain message into a complete message for easier
			// processing.
			if (criteria instanceof Message) {
				// re-assign the param variable using a complete
				criteria = new CompleteMessage.Builder().setMessage((Message) criteria).build();
			}
			if (criteria instanceof CompleteMessage) {
				CompleteMessage completeMessage = (CompleteMessage) criteria;
				MessageResourceBundle resourceBundle = MessageResourceBundleFactory.getInstance()
						.getManagedResourceBundleFromMessage(completeMessage.getMessage());
				return new MessageResourceBundleExceptionLocalizer(resourceBundle, completeMessage, cause);
			} else {
				return null;
			}
		}

	}

	/**
	 * A commmon way for {@link MessageException} to satisfy it's
	 * {@link MessageException#getErrorCode()} requirements.
	 * 
	 * @param baseException
	 * @return
	 */
	public static String getErrorCode(ExceptionLocalizer localizer) {
		String result;
		if (localizer instanceof MessageResourceBundleExceptionLocalizer) {
			CompleteMessage completeMessage = ((MessageResourceBundleExceptionLocalizer) localizer).getDynamicMessage();
			result = completeMessage.getMessage().name();
		} else {
			result = null;
		}
		return result;
	}

}
