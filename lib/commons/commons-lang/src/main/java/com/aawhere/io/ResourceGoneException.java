/**
 * 
 */
package com.aawhere.io;

import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.GONE)
public class ResourceGoneException
		extends IoException {

	private static final long serialVersionUID = 1929808841553335051L;

	/**
	 * 
	 */
	public ResourceGoneException(String resourcePath) {
		super(CompleteMessage.create(IoMessage.GONE).param(IoMessage.Param.URL, resourcePath).build());
	}
}
