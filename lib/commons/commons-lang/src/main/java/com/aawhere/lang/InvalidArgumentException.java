/**
 * 
 */
package com.aawhere.lang;

import java.util.Collection;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;

import com.google.common.collect.Range;

/**
 * A general exception equivalent to {@link IllegalArgumentException}, but provides useful message
 * to the user and a status code to the web developer.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class InvalidArgumentException
		extends BaseRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 125875039160795820L;

	/**
	 * @param dynamicResourceBundleMessage
	 */
	public InvalidArgumentException(CompleteMessage dynamicResourceBundleMessage) {
		super(dynamicResourceBundleMessage);
	}

	public InvalidArgumentException(BaseException like) {
		super(like);
	}

	/**
	 * Expresses that a limit has been reached as a runtime. This same message may be used to
	 * express an {@link BaseException} if a need for a checked exception arises.
	 * 
	 * @param limit
	 * @return
	 */
	public static InvalidArgumentException limitExceeded(Number limit) {
		return new InvalidArgumentException(new CompleteMessage.Builder(LangMessage.LIMIT_EXCEEDED)
				.addParameter(LangMessage.Param.LIMIT, limit).build());
	}

	/** when you have a limit and a number that exceeded it use this. */
	public static void limitExceeded(Number limit, Number value) throws InvalidArgumentException {
		if (value.doubleValue() > limit.doubleValue()) {
			throw new InvalidArgumentException(new CompleteMessage.Builder(LangMessage.LIMIT_EXCEEDED_WITH_ACTUAL)
					.param(LangMessage.Param.LIMIT, limit).param(LangMessage.Param.VALUE, value).build());
		}
	}

	/** applies the range and throws an exception if out of range. */
	public static <C extends Comparable<C>> void range(Range<C> range, C value) throws InvalidArgumentException {
		if (!range.contains(value)) {
			throw new InvalidArgumentException(new CompleteMessage.Builder(LangMessage.OUT_OF_RANGE)
					.param(LangMessage.Param.LIMIT, range).param(LangMessage.Param.VALUE, value).build());
		}
	}

	/**
	 * 
	 * @param notNullObect
	 * @param requiredItemName
	 * @throws InvalidArgumentException
	 *             when given is null. otherwise does nothing.
	 */
	public static void notNull(Object notNullObect, Object requiredItemName) {
		if (notNullObect == null) {
			throw required(requiredItemName);
		}
	}

	/**
	 * Use this when a null is provided. It's better to use {@link #required(Message, Collection)}
	 * providing what is to be provided.
	 * 
	 * @see #required(Message, Collection)
	 * @param requiredItemName
	 * @return
	 */
	public static InvalidArgumentException required(Object requiredItemName) {
		return new InvalidArgumentException(new CompleteMessage.Builder(LangMessage.REQUIRED)
				.addParameter(LangMessage.Param.ITEM_NAME, requiredItemName).build());
	}

	/**
	 * Given the required object, this does a null check and throws {@link #required(Message)} if
	 * the object is null.
	 * 
	 * @param notNull
	 * @param requiredItemName
	 */
	public static void required(Object notNull, Object requiredItemName) {
		if (notNull == null) {
			throw required(requiredItemName);
		}
	}

	public static InvalidArgumentException required(Message requiredItemName, Collection<Message> choices) {
		return new InvalidArgumentException(new CompleteMessage.Builder(LangMessage.REQUIRED_WITH_EXAMPLE)
				.addParameter(LangMessage.Param.ITEM_NAME, requiredItemName)
				.addParameter(LangMessage.Param.CHOICES, choices).build());
	}

	/**
	 * General message to be used when something can't be understood. Identify what the value
	 * represents and give the value that is not understood.
	 * 
	 * @return
	 */
	public static InvalidArgumentException notUnderstood(Object itemName, Object value) {
		return new InvalidArgumentException(new CompleteMessage.Builder(LangMessage.MISUNDERSTOOD)
				.addParameter(LangMessage.Param.ITEM_NAME, itemName).addParameter(LangMessage.Param.VALUE, value)
				.build());
	}
}
