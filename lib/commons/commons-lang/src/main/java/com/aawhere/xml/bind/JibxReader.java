/**
 * 
 */
package com.aawhere.xml.bind;

import java.io.InputStream;

import javax.xml.bind.JAXBElement;

import org.jibx.runtime.BindingDirectory;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.IUnmarshallingContext;
import org.jibx.runtime.JiBXException;

/**
 * Reader for unmarshalling via the jibx library.
 * 
 * @author Brian Chapman
 * 
 */
public class JibxReader {

	protected JibxReader() {
	}

	/**
	 * the main method used to get a reader that will unmarshall an xml doc and provide the XJC
	 * Generated Document Type.
	 * 
	 * @see JAXBElement
	 * 
	 * @param <SchemaType>
	 * @param <SchemaTypeObjectFactory>
	 * @param objectFactoryClass
	 * @return
	 */
	public static JibxReader getReader() {
		return new JibxReader();
	}

	@SuppressWarnings("unchecked")
	public <SchemaT> SchemaT read(InputStream inputStream, Class<SchemaT> schemaType) throws JAXBReadException {

		IBindingFactory bfact;
		try {
			bfact = BindingDirectory.getFactory(schemaType);
			IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
			return (SchemaT) uctx.unmarshalDocument(inputStream, null);
		} catch (JiBXException e) {
			// TODO: hmmm, this really isn't a JAXB function, but it is related to what
			// JAXBReadException represents. Maybe JAXBReadException can be more generic?
			throw new JAXBReadException(e);
		}

	}
}
