/**
 * 
 */
package com.aawhere.collections;

/**
 * A nice name put to the zero-based indexes of Java.
 * 
 * @author Aaron Roller
 * 
 */
public enum Index {

	FIRST(0), SECOND(1), THIRD(2), FOURTH(3), FIFTH(4), SIXTH(5), SEVENTH(6), EIGHTH(7), NINTH(8), TENTH(9),
	/** The indicator used by Collections to indicate nothing is found. */
	NONE(-1);

	public final Integer index;

	private Index(Integer index) {
		this.index = index;
	}

}
