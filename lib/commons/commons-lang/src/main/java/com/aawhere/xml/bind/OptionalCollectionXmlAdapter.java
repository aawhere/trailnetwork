package com.aawhere.xml.bind;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.xml.XmlNamespace;

/**
 * A list (nor any interface) can be returned from an adapter...even if JAXB normally handles the
 * interface (like List). This wraps any collection using the advantages of
 * {@link OptionalXmlAdapter} to show the collection selectively.
 * 
 * @author aroller
 * 
 * @param <T>
 */
public class OptionalCollectionXmlAdapter<T>
		extends OptionalXmlAdapter<OptionalCollectionXmlAdapter.Xml<T>, Iterable<T>> {

	public OptionalCollectionXmlAdapter() {
		super();
	}

	public OptionalCollectionXmlAdapter(Visibility visibility) {
		super(visibility);
	}

	public OptionalCollectionXmlAdapter(Show show) {
		super(show);
	}

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static class Xml<T> {
		@XmlAnyElement
		Collection<T> items;
	}

	@Override
	protected Xml<T> showMarshal(Iterable<T> v) throws Exception {
		if (v != null) {
			Collection<T> collection = CollectionUtilsExtended.toCollection(v);
			if (!collection.isEmpty()) {
				Xml<T> xml = new Xml<T>();
				xml.items = collection;
				return xml;
			}
		}
		return null;
	}
}
