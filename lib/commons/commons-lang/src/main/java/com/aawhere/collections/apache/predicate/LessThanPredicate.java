/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 1, 2010
commons-lang : com.aawhere.collections.predicate.LessThanPredicate.java
 */
package com.aawhere.collections.apache.predicate;

/**
 * Ensures that the number evaluated is less than the number provided during construction.
 * 
 * @author roller
 * 
 */
public class LessThanPredicate
		extends BinaryNumberPredicate {

	/**
	 * 
	 * @param max
	 *            less than or equal to is allowed
	 */
	public LessThanPredicate(Number max) {
		super(max);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.collections.apache.functor.BinaryPredicate#evaluateCustom(java.lang.Object)
	 */
	@Override
	public boolean evaluateCustom(Number leftOperand) {

		return leftOperand.doubleValue() < getRightOperand().doubleValue();
	}

}
