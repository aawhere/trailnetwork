/**
 * 
 */
package com.aawhere.lang;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Specifically handling messages general to choice and perhaps making a wrong choice.
 * 
 * This will display the chosen and the choices available. It works well with enum so if an enum is
 * chosen you may provide only the enum and the choices will be displayed automatically.
 * 
 * @author Aaron Roller
 * 
 */
public enum LangMessage implements Message {

	/** Message explaining an incorrect choice was made offering the options. */
	CHOSEN_FROM_CHOICES("{CHOSEN} isn't a valid choice of: {CHOICES}", Param.CHOSEN, Param.CHOICES),

	/** Choices given, but null provided instead of a real option */
	NOTHING_CHOSEN_FROM_CHOICES("Please choose one of these: {CHOICES}", Param.CHOICES),

	/** Something chosen, but no description of choices available. */
	CHOSEN_FROM_NO_CHOICES("{CHOSEN} is not a valid option.", Param.CHOSEN),

	/** Likely when the choice is null and no chioces are known. */
	NOTHING_CHOSEN_FROM_NO_CHOICES("You didn't provide anything.  Perhaps you have issues with committment?"),
	/**
	 * Mostly used when an argument is null and it is required.
	 */
	REQUIRED("{ITEM_NAME} is required.", Param.ITEM_NAME),
	/** Same as required, but provides examples of what may be used. */
	REQUIRED_WITH_EXAMPLE("{ITEM_NAME} requires {CHOICES}. ", Param.ITEM_NAME, Param.CHOICES),
	/**
	 * General message when something is not understood.
	 */
	MISUNDERSTOOD("Unable to understand {VALUE} for {ITEM_NAME}.", Param.VALUE, Param.ITEM_NAME),
	/**
	 * Usually a side effect of a switch statement that doesn't handle every case.
	 */
	UNHANDLED_CHOICE("{CHOSEN} is not handled. Try another: {CHOICES}", Param.CHOSEN, Param.CHOICES),
	/** @see UpToDateException */
	UP_TO_DATE("{ID} is already up to date.", Param.ID),

	/**
	 * Expresses a max limit of something.
	 * 
	 * @see InvalidArgumentException#limitExceeded(Integer)
	 */
	LIMIT_EXCEEDED("Limit of {LIMIT} exceeded.", Param.LIMIT),
	/** expresses the limit and what exceeded the limit. */
	LIMIT_EXCEEDED_WITH_ACTUAL("{VALUE} exceeds a limit of {LIMIT}.", Param.LIMIT, Param.VALUE),
	/**
	 * Works with Range.
	 * 
	 * @see InvalidArgumentException#range(com.google.common.collect.Range, Comparable)
	 */
	OUT_OF_RANGE("{VALUE} is not within {LIMIT}.", Param.LIMIT, Param.VALUE), ;
	private ParamKey[] parameterKeys;
	private String message;

	private LangMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** The item chosen when there are choices. */
		CHOSEN,
		/**
		 * Represents the possible choices. Feel free to pass an array of objects or a more advance
		 * object like a range description,etc.
		 * */
		CHOICES,
		/** Identifies to the client the name of what is required. */
		ITEM_NAME,
		/** identifies the item */
		ID,
		/** The number use to express some value such as for the limit */
		VALUE,
		/** A number representing a general limit being expressed. */
		LIMIT;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	/**
	 * Provides a compelete message when given standard parameters.
	 * 
	 * @param chosen
	 *            any object chosen
	 * @param choices
	 *            any objects providing the choices available
	 * @return the correct message complete with parameters.
	 */
	public static CompleteMessage create(Object chosen, Object... choices) {
		CompleteMessage.Builder messageBuilder = new CompleteMessage.Builder();

		final boolean noChoices = ArrayUtils.isEmpty(choices);

		if (chosen == null) {
			if (noChoices) {
				messageBuilder.setMessage(NOTHING_CHOSEN_FROM_NO_CHOICES);
			} else {
				messageBuilder.setMessage(NOTHING_CHOSEN_FROM_CHOICES);
				messageBuilder.addParameter(Param.CHOICES, choices);
			}
		} else {

			// do another empty check since choices may be enums
			if (noChoices) {
				// enums carry their own choices...make this association
				// automatically.
				if (chosen.getClass().isEnum()) {
					messageBuilder.setMessage(UNHANDLED_CHOICE);
					messageBuilder.addParameter(Param.CHOICES, chosen.getClass().getEnumConstants());
				} else {
					messageBuilder.setMessage(CHOSEN_FROM_NO_CHOICES);
				}
			} else {
				messageBuilder.setMessage(CHOSEN_FROM_CHOICES);
				messageBuilder.addParameter(Param.CHOICES, choices);
			}
			messageBuilder.addParameter(Param.CHOSEN, chosen);
		}
		return messageBuilder.build();
	}
}
