/**
 *
 */
package com.aawhere.id;

/**
 * A common interface to be used on all classes to provide a concrete method for all to use that
 * returns the typed concrete version of the getId method.
 * 
 * 
 * @author roller
 * 
 */
public interface Identifiable<IdentifierT extends Identifier<?, ?>>

{

	public IdentifierT getId();

	/**
	 * Identifiables are equals if and onlyl if their Ids are equal. Should the need arise where a
	 * deep equality check is needed it is encouraged to provide another method for such a check or
	 * a Utility class that will inspect equality.
	 * 
	 * @param that
	 * @return true if and only if the Ids of each Identifiable are equal.
	 */
	@Override
	public boolean equals(Object that);
}
