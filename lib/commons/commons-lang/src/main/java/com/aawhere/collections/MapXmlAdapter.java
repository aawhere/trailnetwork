/**
 *
 */
package com.aawhere.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.xml.XmlNamespace;

/**
 * Round trip conversion from any {@link Map} to/from Xml.
 * 
 * @author aroller
 * 
 */
public class MapXmlAdapter<K, V>
		extends XmlAdapter<MapXmlAdapter.XmlMap<K, V>, Map<K, V>> {

	/**
	 * Why not let anyone use this. Useful for services wanting to return their result as the Root
	 * which automatic adapter execution doesn't work.
	 * 
	 */
	public MapXmlAdapter() {
	}

	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class XmlMap<K, V> {
		@XmlElement
		ArrayList<MapEntryXmlAdapter.XmlMapEntry<K, V>> entries = new ArrayList<MapEntryXmlAdapter.XmlMapEntry<K, V>>();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */

	@Override
	public Map<K, V> unmarshal(XmlMap<K, V> xmlMap) throws Exception {
		if (xmlMap == null) {
			return null;
		}
		Map<K, V> map = createMap();
		for (MapEntryXmlAdapter.XmlMapEntry<K, V> entry : xmlMap.entries) {
			map.put(entry.getKey(), entry.getValue());
		}
		return map;
	}

	/**
	 * Children that want a differnt type of map should override.
	 * 
	 * @return
	 */
	protected Map<K, V> createMap() {
		return new HashMap<K, V>();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public XmlMap<K, V> marshal(Map<K, V> map) throws Exception {
		if (map == null) {
			return null;
		}
		Set<Map.Entry<K, V>> entries = map.entrySet();
		XmlMap<K, V> result = new XmlMap<K, V>();
		for (Map.Entry<K, V> entry : entries) {
			result.entries.add(new MapEntryXmlAdapter.XmlMapEntry<K, V>(entry));
		}
		return result;
	}
}
