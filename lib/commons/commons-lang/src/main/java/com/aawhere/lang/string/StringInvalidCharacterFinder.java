/**
 * 
 */
package com.aawhere.lang.string;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.SortedSet;

import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.functors.AnyPredicate;
import org.apache.commons.collections.functors.EqualPredicate;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.CharacterIndexes.CharacterIndex;
import com.aawhere.lang.string.predicate.CharSequencePredicate;
import com.aawhere.lang.string.predicate.DigitPredicate;
import com.aawhere.lang.string.predicate.LetterPredicate;

/**
 * Used to find characters that match a set of criteria (either positive or negative).
 * 
 * By default nothing is allowed. All allowances are assigned during {@link Builder} construction.
 * 
 * TODO: consider providing this generically for any array (since a string is really a character
 * array).
 * 
 * @see CharSequencePredicate for more options
 * 
 * @author roller
 * 
 */
public class StringInvalidCharacterFinder {

	public static class Builder
			extends ObjectBuilder<StringInvalidCharacterFinder> {

		public Builder(String string) {
			super(new StringInvalidCharacterFinder(string));

		}

		public Builder setLettersAreAllowed(Boolean allowed) {
			return addPredicate(LetterPredicate.getInstance());

		}

		public Builder setDigitsAreAllowed(Boolean allowed) {
			return addPredicate(DigitPredicate.getInstance());

		}

		public Builder addCharacterIsAllowed(Character... allowed) {
			for (int i = 0; i < allowed.length; i++) {
				Character character = allowed[i];
				building.predicates.add(new EqualPredicate(character));

			}
			return this;
		}

		/**
		 * You tell us what is o.k. by providing any predicate.
		 * 
		 * @see CharSequencePredicate
		 */
		public Builder addPredicate(Predicate predicate) {
			building.predicates.add(predicate);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("string", building.string);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public StringInvalidCharacterFinder build() {
			building.process();
			return super.build();
		}
	}// end Builder

	private String string;
	private CharacterIndexes indexes;
	/**
	 * {@link Predicate}s that determine if each character of the string qualifies for any of the
	 * allowed predicates.
	 * 
	 */
	private LinkedList<Predicate> predicates = new LinkedList<Predicate>();

	/**
	 * @see Builder#Builder()
	 */
	private StringInvalidCharacterFinder(String string) {
		this.string = string;
	}

	/**
	 * Searches the string for invalid characters and adds to {@link #indexes}.
	 * 
	 */
	private void process() {
		if (this.predicates.isEmpty()) {
			throw new RuntimeException("No positive criteria provided");
		}

		char[] chars = this.string.toCharArray();
		CharacterIndexes.Builder indexesBuilder = new CharacterIndexes.Builder(this.string);
		for (int whichChar = 0; whichChar < chars.length; whichChar++) {
			char c = chars[whichChar];

			Predicate any = AnyPredicate.getInstance(predicates);

			if (!any.evaluate(c)) {
				indexesBuilder.add(c, whichChar);
			}

		}
		indexes = indexesBuilder.build();
	}

	/**
	 * @return the string
	 */
	public String getString() {
		return string;
	}

	/**
	 * @return the indexes
	 */
	public CharacterIndexes getIndexes() {
		return indexes;
	}

	public String getHighlightedInvalidCharacters() {
		return this.toString();
	}

	@Override
	public String toString() {
		SortedSet<CharacterIndex> all = this.indexes.getAll();
		StringBuffer result = new StringBuffer();
		int previousIndex = 0;
		for (Iterator<CharacterIndexes.CharacterIndex> iterator = all.iterator(); iterator.hasNext();) {
			CharacterIndex characterIndex = (CharacterIndex) iterator.next();
			Integer currentIndex = characterIndex.getIndex();
			// append all the valid characters between the current and the
			// previous invalid index
			if (currentIndex != previousIndex) {
				result.append(this.string.substring(previousIndex, currentIndex));
			}
			char invalidCharacter = this.string.charAt(currentIndex);
			appendInvalidIdentifier(result, invalidCharacter);
			previousIndex = currentIndex + 1;
		}

		// now append the remaining valid characters
		int maxIndex = this.string.length();
		if (previousIndex <= maxIndex) {

			result.append(this.string.substring(previousIndex, maxIndex));
		}
		return result.toString();
	}

	static void appendInvalidIdentifier(StringBuffer string, Character invalidCharacter) {
		string.append('[');
		string.append(invalidCharacter);
		string.append(']');

	}
}
