/**
 * 
 */
package com.aawhere.style;

import java.text.NumberFormat;

/**
 * @author aroller
 * 
 */
public class ColorUtil {
	private static final NumberFormat FORMAT;
	static {
		FORMAT = NumberFormat.getIntegerInstance();
		FORMAT.setMaximumIntegerDigits(2);
		FORMAT.setMinimumIntegerDigits(2);
	}

	/** Provides the RGB value in a standard 0xRRGGBB string format. */
	public static String rgbString(Color color) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("0x");
		stringBuilder.append(FORMAT.format(color.getRed()));
		stringBuilder.append(FORMAT.format(color.getGreen()));
		stringBuilder.append(FORMAT.format(color.getBlue()));
		return stringBuilder.toString();
	}

	public static Color colorFromString(String rawValue) {
		final int rgb = Integer.parseInt(rawValue.split("[x|X]")[1], 16);
		return new Color(rgb);
	}
}
