/**
 *
 */
package com.aawhere.xml.bind;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.lang.ObjectBuilder;

/**
 * @author Aaron Roller
 * 
 */
public class JAXBStringWriter {

	/**
	 * Used to construct all instances of JAXBStringWriter.
	 */
	public static class Builder
			extends ObjectBuilder<JAXBStringWriter> {

		private Class<?>[] classesToBeBound = {};
		private Object element;
		private HashMap<XmlAdapter<?, ?>, Class<? extends XmlAdapter<?, ?>>> adapters = new HashMap<>();

		public Builder() {
			super(new JAXBStringWriter());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public JAXBStringWriter build() {
			StringWriter writer = new StringWriter();
			try {
				final Marshaller marshaller = JAXBContext.newInstance(classesToBeBound).createMarshaller();
				for (Entry<XmlAdapter<?, ?>, Class<? extends XmlAdapter<?, ?>>> entry : this.adapters.entrySet()) {

					XmlAdapter<?, ?> adapter = entry.getKey();
					@SuppressWarnings("unchecked")
					Class<XmlAdapter<?, ?>> replace = (Class<XmlAdapter<?, ?>>) entry.getValue();
					if (replace == null) {
						marshaller.setAdapter(adapter);
					} else {
						marshaller.setAdapter(replace, adapter);
					}
				}
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				marshaller.marshal(element, writer);

			} catch (JAXBException e) {
				throw new RuntimeException(e);
			}
			building.xml = writer.toString();
			return super.build();
		}

		public Builder adapter(XmlAdapter<?, ?>... adapters) {
			for (XmlAdapter<?, ?> xmlAdapter : adapters) {
				this.adapters.put(xmlAdapter, null);
			}
			return this;
		}

		public Builder adapter(XmlAdapter<?, ?> adapter, Class<XmlAdapter<?, ?>> replace) {
			adapters.put(adapter, replace);
			return this;
		}

		public Builder classesBound(Class<?>... classesToBeBound) {
			this.classesToBeBound = classesToBeBound;
			return this;
		}

		/**
		 * @param element
		 * @return
		 */
		public Builder element(Object element) {
			this.element = element;
			Class<?> boundType = JaxbUtil.boundTypeFrom(element);
			classesBound(boundType);
			return this;
		}

		/**
		 * @param replacementAdapters
		 * @return
		 */
		public Builder adapters(Map<XmlAdapter<?, ?>, Class<? extends XmlAdapter<?, ?>>> replacementAdapters) {
			this.adapters.putAll(replacementAdapters);
			return this;
		}

	}// end Builder

	public static Builder create(Object obj) {
		return new Builder().element(obj);
	}

	private String xml;

	/** Use {@link Builder} to construct JAXBStringWriter */
	private JAXBStringWriter() {

	}

	/**
	 * @return the xml
	 */
	public String getXml() {
		return this.xml;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return xml;
	}
}
