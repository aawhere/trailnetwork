/**
 * 
 */
package com.aawhere.adapter;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * @author aroller
 * 
 */
public enum AdapterMessage implements Message {

	/** @see AdapterNotRegisteredRuntimeException */
	NOT_REGISTERED("No adapter found for {TYPE}.", Param.TYPE);

	private ParamKey[] parameterKeys;
	private String message;

	private AdapterMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** Explains the Type expected for the adapter. */
		TYPE;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
