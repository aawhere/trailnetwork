/**
 * 
 */
package com.aawhere.xml;

import javax.xml.XMLConstants;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

/**
 * A place to declare common namespaces in our system and standard.
 * 
 * @author roller
 * 
 */
public enum XmlNamespace {
	W3C_XML_1998(XmlNamespace.W3C_XML_1998_VALUE),
	/** for use in the generated clients. */
	TRAIL_NETWORK_API(XmlNamespace.API_VALUE);

	public static final String W3C_XML_1998_VALUE = XMLConstants.XML_NS_URI;
	public static final String API_VALUE = "http://api.trailnetwork.com";
	public static final String API_PREFIX = "tna";
	public static final String XML_PREFIX = XMLConstants.XML_NS_PREFIX;
	/**
	 * The default, as provided by the {@link XmlType} annotation.
	 * 
	 * @see XmlType
	 */
	public static final String XML_TYPE_DEFAULT_VALUE = "##default";

	/** the URL namespace value */
	public final String value;

	private XmlNamespace(String value) {
		this.value = value;
	}

}
