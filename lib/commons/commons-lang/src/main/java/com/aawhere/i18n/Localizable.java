/**
 * 
 */
package com.aawhere.i18n;

import java.util.Locale;

/**
 * Implemented by any class that may provide a Locale.
 * 
 * @author roller
 * 
 */
public interface Localizable {

	/** The standard way to retrieve Locale */
	Locale getLocale();
}
