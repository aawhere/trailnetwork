/**
 * 
 */
package com.aawhere.id;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;

/**
 * An easy way to search for any {@link Identifiable} in a collection by it's {@link Identifier}.
 * 
 * @author aroller
 * 
 */
public class IdentifiablePredicate<ID extends Identifier<?, I>, I extends Identifiable<ID>>
		implements Predicate<I> {

	private ID lookingFor;

	/**
	 * 
	 */
	public IdentifiablePredicate(ID lookingFor) {
		this.lookingFor = lookingFor;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(@Nullable I input) {
		return this.lookingFor.equals(input.getId());
	}

}
