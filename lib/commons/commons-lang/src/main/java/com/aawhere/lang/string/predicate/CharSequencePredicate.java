/**
 * 
 */
package com.aawhere.lang.string.predicate;

import org.apache.commons.collections.Predicate;

/**
 * A Base Predicate that handles Character Sequences and Characters that specific predicates will
 * extend and be damanded to answer only a single question regardless of what is passed in.
 * 
 * @author roller
 * 
 */
public abstract class CharSequencePredicate
		implements Predicate {

	/**
	 * 
	 */
	public CharSequencePredicate() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * The method children must evaluate true if the sequence answers their specific questions.
	 * 
	 * @param target
	 *            a CharacterSequence that may contain a single character, string, stringBuffer,
	 *            etc.
	 * @return
	 */
	abstract public boolean evaluate(CharSequence target);

	/**
	 * Evaluates a {@link Character} or any {@link CharSequence}.
	 * 
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		if (object instanceof Character) {
			return evaluate(String.valueOf((Character) object));
		} else if (object instanceof CharSequence) {
			return evaluate((CharSequence) object);
		} else {
			return false;
		}
	}

}
