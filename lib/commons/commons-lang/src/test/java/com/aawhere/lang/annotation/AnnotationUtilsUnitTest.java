package com.aawhere.lang.annotation;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

import org.junit.Ignore;
import org.junit.Test;

/**
 * 
 */

/**
 * Tests {@link AnnotationUtilsExtended}.
 * 
 * @author Aaron Roller
 * 
 */
public class AnnotationUtilsUnitTest {

	/**
	 * 
	 */
	private static final Class<XmlElement> TARGET_ANNOTATION = XmlElement.class;
	private static final int EXPECTED_METHOD_SIZE = 7;
	private static final int EXPECTED_FIELD_SIZE = 2;

	@Test
	public void testFindMethods() {
		Map<Method, XmlElement> methodMap = AnnotationUtilsExtended.getInstance()
				.findMethods(TARGET_ANNOTATION, AnnotationExample.class);
		assertMembers(methodMap, EXPECTED_METHOD_SIZE);
	}

	public static void assertMembers(Map<? extends Member, XmlElement> memberMap, int expectedSize) {
		for (Map.Entry<? extends Member, XmlElement> entry : memberMap.entrySet()) {

			assertEquals("this test is supposed to provide the same name attribute as the member name itself", entry
					.getValue().name(), entry.getKey().getName());

		}
		assertEquals(expectedSize, memberMap.size());
	}

	@Test
	public void testMembers() {
		assertMembers(	AnnotationUtilsExtended.getInstance().findMembers(TARGET_ANNOTATION, AnnotationExample.class),
						EXPECTED_FIELD_SIZE + EXPECTED_METHOD_SIZE);
	}

	@Test
	public void testFindFields() {
		Map<Field, XmlElement> allFields = AnnotationUtilsExtended.getInstance().findFields(TARGET_ANNOTATION,
																							AnnotationExample.class);
		assertMembers(allFields, 2);
	}

	static class SuperExample {
		@XmlElement(name = "superField")
		private String superField;

		@XmlElement(name = "superMethod")
		public void superMethod() {
		}

		/**
		 * Intended to show that overriding will ignore this incorrect name curently not implemented
		 * due to limitation
		 * */
		// @XmlElement(name = "superMethod1")
		// public void method1() {
		// }
		/** Intended to show that overriding will include this regardless of the override. */
		@XmlElement(name = "method1")
		public void method1() {
		}

		@XmlElement(name = "superOnly")
		public void superOnly() {

		}
	}

	static class AnnotationExample
			extends SuperExample {

		/**
		 * 
		 */
		private static final String DUPLICATE = "duplicate";
		@XmlElement(name = "field1")
		private String field1;

		@XmlElement(name = "method1")
		public void method1() {

		}

		@XmlElement(name = "method2")
		public void method2() {

		}

		public void method3() {

		}

		@XmlElement(name = DUPLICATE)
		public void duplicate(String bs) {

		}

		/** Duplicates have the exact same annotation */
		@XmlElement(name = DUPLICATE)
		public void duplicate() {
		}

		@Ignore
		public void nonParticipant() {
		}
	}
}
