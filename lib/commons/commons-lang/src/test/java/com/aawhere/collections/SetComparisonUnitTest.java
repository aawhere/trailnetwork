/**
 * 
 */
package com.aawhere.collections;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
@Ignore
public class SetComparisonUnitTest {

	private static final String A = "A";
	private static final String B = "B";
	private static final String C = "C";
	private HashSet<String> set;
	private SetComparison.Builder<String> comparisonBuilder;
	private Integer hashCode;

	@Before
	public void setUp() {
		this.set = Sets.newHashSet(A, B);
		this.comparisonBuilder = SetComparison.<String> create().set(set);
	}

	public void test(Set<String> to, Integer allowableDifference, boolean expectingEquality) {
		SetComparison<String> compareFrom = comparisonBuilder.hashCode(hashCode).differenceAllowed(allowableDifference)
				.build();
		SetComparison<String> compareTo = SetComparison.<String> create().differenceAllowed(allowableDifference)
				.hashCode(hashCode).set(to).build();
		assertEquals(expectingEquality, compareFrom.equals(compareTo));
		assertEquals(expectingEquality, compareFrom.hashCode() == compareTo.hashCode());

		// now test as a key
		ImmutableMultiset<SetComparison<String>> sets = ImmutableMultiset.of(compareTo, compareFrom);
		int expectedCountForKey = (expectingEquality) ? 2 : 1;
		assertEquals(expectedCountForKey, sets.count(compareTo));
		assertEquals(expectedCountForKey, sets.count(compareFrom));
	}

	@Test
	public void testSameEquals() {
		test(this.set, 0, true);
	}

	@Test
	public void testOneDifferenceInequal() {
		final HashSet<String> to = Sets.newHashSet(A, B, C);
		test(to, 0, false);
	}

	@Test
	public void testOneMoreEqual() {
		final HashSet<String> to = Sets.newHashSet(A, B, C);
		this.hashCode = 234;
		test(to, 1, true);
	}

	@Test
	public void testOneLessEqual() {
		final HashSet<String> to = Sets.newHashSet(B);
		this.hashCode = 3;
		test(to, 1, true);
	}

}
