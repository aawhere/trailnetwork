/**
 * 
 */
package com.aawhere.xml.bind;

import static org.junit.Assert.assertNotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.junit.Test;

/**
 * Tests for {@link JAXBContextFactory}
 * 
 * @author Brian Chapman
 * 
 */
public class JAXBContextFactoryUnitTest {

	@Test
	public void testJaxBContextFactory() throws JAXBException {
		JAXBContextFactory factory = JAXBContextFactory.instance();

		JAXBContext context = factory.getContext(String.class);

		assertNotNull(context);
	}
}
