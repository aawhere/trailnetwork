/**
 * 
 */
package com.aawhere.lang;

import static org.junit.Assert.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * Tests the basic functions of {@link ObjectBuilder}.
 * 
 * @author roller
 * 
 */
public class ObjectBuilderUnitTest {

	@Test
	public void testConstruction() {

		TestBuilder builder = new TestBuilder();
		assertNotNull(builder.building);
		ObjectBuilderUnitTest result = builder.build();
		assertNotNull(result);
		assertTrue(builder.validated);

	}

	@Test(expected = UnsupportedOperationException.class)
	public void testBuildingTwice() {
		TestBuilder builder = new TestBuilder();
		builder.build();

		builder.build();
		fail("Should not allow double building");
	}

	@Test(expected = NullPointerException.class)
	public void testNullBuilding() {
		new TestBuilder(null);

	}

	@Test(expected = IllegalStateException.class)
	public void testNonMutable() {
		NonMutableChild child = new NonMutableChild();
		assertFalse("defalut should be false if not built by builder", child.mutating);
		NonMutableChild.Builder builder = new NonMutableChild.Builder(child);
		assertSame(child, builder.building);
		assertTrue(child.mutating);
		try {
			child.doNothing();
		} catch (IllegalStateException e) {
			// good. let's make sure building changes state before we exit non gracefully
			builder.build();
			assertFalse("building should make this ready for access", child.mutating);
			throw (e);
		}
		fail("should have stopped me from accessing");
	}

	/**
	 * Demonstrates use of {@link AbstractObjectBuilder}.
	 * 
	 */
	@Test
	public void testChildBuilder() {
		Integer variable = 1;
		Integer childVariable = 2;
		TestChildBuilder childBuilder = TestChildBuilder.createChild().setVariable(variable)
				.setChildVariable(childVariable).build();
		assertEquals("variable", variable, childBuilder.getVariable());
		assertEquals("childVariable", childVariable, childBuilder.getChildVariable());
	}

	@Test
	@Ignore("validating all fields is too slow for every object being built")
	public void testNullValidation() {
		try {
			new NonMutableChild.Builder().invalidNull().build();
		} catch (Exception e) {
			TestUtil.assertContains(e.getMessage(), "mustNotBeNull");
		}
	}

	static class NonMutableChild
			extends NonMutable {

		@Nonnull
		private Integer mustNotBeNull = 3;

		@Nullable
		private Integer canBeNull;

		private Integer nothingDeclared;

		/**
		 * Used to construct all instances of ObjectBuilderUnitTest.NonMutableChild.
		 */
		public static class Builder
				extends ObjectBuilder<ObjectBuilderUnitTest.NonMutableChild> {

			public Builder(NonMutableChild mutatee) {
				super(mutatee);
			}

			public Builder() {
				super(new NonMutableChild());
			}

			public Builder invalidNull() {
				building.mustNotBeNull = null;
				return this;
			}

		}// end Builder

		public void doNothing() {
			super.validateNotMutable();
		}
	}

	static class TestBuilder
			extends ObjectBuilder<ObjectBuilderUnitTest> {

		private boolean validated;

		/**
		 * @param building
		 */
		public TestBuilder() {
			super(new ObjectBuilderUnitTest());

		}

		public TestBuilder(ObjectBuilderUnitTest mutatee) {
			super(mutatee);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			this.validated = true;
		}

	}
}
