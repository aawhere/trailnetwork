/**
 *
 */
package com.aawhere.lang.string;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.Nullable;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.test.TestUtil;
import com.google.common.base.Functions;
import com.google.common.collect.Lists;
import com.google.inject.internal.MoreTypes.ParameterizedTypeImpl;

/**
 * @author aroller
 * 
 */
@SuppressWarnings("rawtypes")
public class StringAdapterUnitTest {

	public static enum TestEnum {
		TEST_1, TEST_2, TEST_3
	}

	@Test
	public void testIntegerAdaptation() throws BaseException {
		FromStringAdapter<Integer> adapter = getIntegerAdapter();
		assertIntegerAdapter(adapter);
	}

	/**
	 * @param adapter
	 * @throws BaseException
	 */
	private void assertIntegerAdapter(StringAdapter<Integer> adapter) throws BaseException {
		Integer expected = TestUtil.generateRandomInt();
		String string = adapter.marshal(expected);
		Integer actual = adapter.unmarshal(string, Integer.class);
		assertEquals(expected, actual);
	}

	@SuppressWarnings("unchecked")
	public static void assertAdapter(StringAdapterFactory factory, Object expected) throws BaseException {
		StringAdapter<Object> adapter = factory.getAdapter(expected.getClass());
		assertNotNull(adapter);
		String string = adapter.marshal(expected);
		Object actual = adapter.unmarshal(string, (Class<Object>) expected.getClass());
		assertEquals(expected, actual);
	}

	/**
	 * @param type
	 * @return
	 */
	private FromStringAdapter<Integer> getIntegerAdapter() {
		return new FromStringAdapter.Builder<Integer>().setType(Integer.class).build();
	}

	@Test
	public void testIntegerAdapterFromFactory() throws BaseException {
		StringAdapterFactory factory = new StringAdapterFactory();
		StringAdapter<Integer> adapter = (StringAdapter<Integer>) factory.register(getIntegerAdapter());
		assertNull("nobody should be registered before thsi one", adapter);
		StringAdapter<Integer> foundAdapter = factory.getAdapter(Integer.class);
		assertIntegerAdapter(foundAdapter);
	}

	/**
	 * Tests that the factory will find a suitable adapter if a super class is registered even if
	 * the concrete class is not.
	 * 
	 * @throws BaseException
	 * 
	 */
	@Test
	public void testSuperInterfaceRegistered() throws BaseException {
		StringAdapterFactory factory = new StringAdapterFactory();
		FromStringAdapter<Number> expected = new FromStringAdapter.Builder<Number>().setType(Number.class).build();
		assertNull("should be only registration", factory.register(expected));
		StringAdapter<Integer> actual = factory.getAdapter(Integer.class);
		assertTrue(factory.handles(Integer.class));
		assertSame(expected, actual);
		// the adapter actually doesn't work, we are just testing the factory.
		// assertAdapter(factory, Integer.MAX_VALUE);
		actual = factory.getAdapter(Double.class);
		assertTrue(factory.handles(Double.class));
		assertEquals(expected, actual);

	}

	@Test
	public void testInteger() throws BaseException {
		assertAdapter(getLangFactory(), Integer.MAX_VALUE);
	}

	@Test
	public void testByte() throws BaseException {
		assertAdapter(getLangFactory(), Byte.MAX_VALUE);
	}

	@Test
	public void testShort() throws BaseException {
		assertAdapter(getLangFactory(), Short.MAX_VALUE);
	}

	@Test
	public void testLong() throws BaseException {
		assertAdapter(getLangFactory(), Long.MAX_VALUE);
	}

	@Test
	public void testDouble() throws BaseException {
		assertAdapter(getLangFactory(), Double.MAX_VALUE);
	}

	@Test
	public void testFloat() throws BaseException {
		assertAdapter(getLangFactory(), Float.MAX_VALUE);
	}

	@Test
	public void testTrue() throws BaseException {
		assertAdapter(getLangFactory(), Boolean.TRUE);
	}

	@Test
	public void tesFalse() throws BaseException {
		assertAdapter(getLangFactory(), Boolean.FALSE);
	}

	@Test
	public void testEnum() throws BaseException {
		assertAdapter(getLangFactory(), TestEnum.TEST_1);
	}

	@Test(expected = InvalidChoiceException.class)
	public void testEnumBadString() throws BaseException {
		Class<TestEnum> type = TestEnum.class;
		StringAdapter<Object> adapter = getLangFactory().getAdapter(type);
		adapter.unmarshal("Junk", type);
	}

	@Test
	public void testFactoryFromStringNoRegistration() {
		StringAdapterFactory factory = new StringAdapterFactory();
		String message = "Strings are FromString compatible";
		assertTrue(message, factory.handles(String.class));
		assertNotNull(message, factory.getAdapter(String.class));
	}

	@Test
	public void tesCharacterNotHandled() throws BaseException {
		Character notHandled = Character.MAX_HIGH_SURROGATE;
		StringAdapterFactory langFactory = getLangFactory();
		assertFalse(langFactory.handles(notHandled.getClass()));
		// assertAdapter(langFactory, notHandled);
	}

	@Test
	public void testIntegerCollectionHandled() throws BaseException {

		ArrayList<Integer> expected = Lists.newArrayList(1, 3, 6, 7, 9);
		assertCollection(expected, Integer.class, null);

	}

	@Test
	public void testCollectionOfEnums() throws BaseException {

		final List<TestEnum> expected = Arrays.asList(TestEnum.values());
		final Class<TestEnum> typeInsideCollection = TestEnum.class;
		assertCollection(expected, typeInsideCollection, TestEnum.class);

	}

	/**
	 * @param expected
	 * @param typeInsideCollection
	 * @throws BaseException
	 */
	private <T> void assertCollection(final List<T> expected, final Class<T> typeInsideCollection,
			@Nullable Class<?> ownerType) throws BaseException {
		List<String> strings = Lists.transform(expected, Functions.toStringFunction());
		ParameterizedTypeImpl type = new ParameterizedTypeImpl(ownerType, expected.getClass(), typeInsideCollection);

		CollectionStringsAdapter adapter = new CollectionStringsAdapter.Builder().setType(type)
				.setFactory(getLangFactory()).build();
		@SuppressWarnings("unchecked")
		Collection<?> actual = adapter.marshal(strings);
		assertNotNull(actual);
		assertEquals("collection is scrambled", expected, actual);
	}

	@Test
	public void testCollectionContainedTypeNotHandled() {
		ArrayList<StringAdapterUnitTest> expected = new ArrayList<StringAdapterUnitTest>();
		ParameterizedTypeImpl type = new ParameterizedTypeImpl(null, expected.getClass(), StringAdapterUnitTest.class);
		CollectionStringsAdapter adapter = new CollectionStringsAdapter.Builder().setType(type)
				.setFactory(getLangFactory()).build();
		assertFalse(adapter.handles());
	}

	@Test
	@Ignore("this doesn't do anything.  is it supposed to?")
	public void testParameterizedType() {

	}

	@Test
	public void testFromStringAutoRegistration() {
		StringAdapterFactory factory = new StringAdapterFactory();
		// don't register anything
		StringAdapter<Integer> adapter = factory.getAdapter(Integer.class);
		assertNotNull("Integer qualifies for FromString adapter.", adapter);
	}

	/**
	 * @return
	 */
	public StringAdapterFactory getLangFactory() {
		return new StringAdapterFactory().registerStandardAdapters();
	}

	@Test
	public void testCollectionUnmarshallNull() throws StringFormatException, BaseException {
		assertCollectionOfNothing(null);
	}

	@Test
	public void testCollectionUnmarshallEmptyQuote() throws StringFormatException, BaseException {
		assertCollectionOfNothing("");
	}

	/**
	 * @param expected
	 * @throws StringFormatException
	 * @throws BaseException
	 */
	private void assertCollectionOfNothing(String expected) throws StringFormatException, BaseException {
		CollectionStringAdapter<SortedSet<Integer>, Integer> adapter = getIntegerCollectionAdapter();
		@SuppressWarnings("unchecked")
		SortedSet<Integer> result = adapter.unmarshal(expected, SortedSet.class);
		TestUtil.assertEmpty(result);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testIntegerCollection() throws BaseException {
		SortedSet<Integer> expected = new TreeSet<Integer>();
		Integer FIRST = 3;
		expected.add(FIRST);
		Integer SECOND = 6;
		expected.add(SECOND);
		Integer THIRD = 15;
		expected.add(THIRD);

		CollectionStringAdapter<SortedSet<Integer>, Integer> adapter = getIntegerCollectionAdapter();
		String string = adapter.marshal(expected);
		TestUtil.assertContains(string, FIRST.toString());
		TestUtil.assertContains(string, SECOND.toString());
		TestUtil.assertContains(string, THIRD.toString());
		Class<SortedSet<Integer>> collectionClass = (Class<SortedSet<Integer>>) expected.getClass();
		SortedSet<Integer> actual = adapter.unmarshal(string, collectionClass);
		assertEquals("adapter isn't able to translate collection String " + string, expected, actual);

		StringAdapterFactory factory = new StringAdapterFactory();
		StringAdapter<SortedSet<Integer>> adapterFromFactory = factory.getAdapter(SortedSet.class, Integer.class);
		assertEquals(adapter.getClass(), adapterFromFactory.getClass());
		SortedSet<Integer> actualFromFactory = adapterFromFactory.unmarshal(string, collectionClass);
		assertEquals("factory isn't able to translate collection string " + string, expected, actualFromFactory);
		assertTrue(factory.handles(collectionClass, Integer.class));
	}

	@Test
	public void testInvalidType() {
		StringAdapterFactory factory = new StringAdapterFactory();
		ArrayList<Integer> expected = Lists.newArrayList(1, 3, 6, 7, 9);
		ParameterizedTypeImpl type = new ParameterizedTypeImpl(null, expected.getClass(), StringAdapterUnitTest.class);
		Boolean handles = factory.handles(type);
		assertFalse(handles);
	}

	/**
	 * @return
	 */
	private CollectionStringAdapter<SortedSet<Integer>, Integer> getIntegerCollectionAdapter() {
		return new CollectionStringAdapter.Builder<SortedSet<Integer>, Integer>().setAdapter(getIntegerAdapter())
				.setParameterType(Integer.class).build();
	}
}
