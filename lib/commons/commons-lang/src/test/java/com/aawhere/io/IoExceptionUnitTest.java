/**
 * 
 */
package com.aawhere.io;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.net.HttpStatusCode;

/**
 * @author aroller
 * 
 */
public class IoExceptionUnitTest {

	@Test
	public void testStatusCode() {
		String explanation = "explanation";
		HttpStatusCode expectedStatus = HttpStatusCode.CLIENT_TIMEOUT;
		IoException ioException = new IoException(expectedStatus, explanation);
		assertEquals(expectedStatus, ioException.getStatusCode());
	}

}
