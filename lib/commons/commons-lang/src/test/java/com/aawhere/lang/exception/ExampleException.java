/**
 * 
 */
package com.aawhere.lang.exception;

import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;

/**
 * Used to test {@link BaseException} in {@link ExceptionLocalizerTest} and
 * {@link BaseExceptionUnitTest}
 * 
 * @author roller
 * 
 */
@StatusCode(HttpStatusCode.UNPROCESSABLE_ENTITY)
public class ExampleException
		extends BaseException {

	/**
	 * Use this for reference in testing. The annotation won't exception so if you change this
	 * change it in the annotaiton.
	 */

	static final HttpStatusCode STATUS_CODE = HttpStatusCode.UNPROCESSABLE_ENTITY;

	/**
	 * 
	 */
	private static final long serialVersionUID = 5662424087464713983L;

	/**
	 * 
	 */
	public ExampleException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ExampleException(CompleteMessage message, Exception cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ExampleException(CompleteMessage message) {
		super(message);
	}

	/**
	 * @param message
	 * @param e
	 */
	public ExampleException(Message message, Exception e) {
		super(message, e);
	}

	/**
	 * @param message
	 */
	public ExampleException(Message message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ExampleException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ExampleException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ExampleException(Throwable cause) {
		super(cause);
	}

}
