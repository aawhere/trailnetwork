/**
 * 
 */
package com.aawhere.lang;

/**
 * Demonstrates how to use builders as nested class rather than static inner class.
 * 
 * 
 * @author aroller
 * 
 */
public class TestInnerBuilder {

	/**
	 * Used to construct all instances of TestInnerBuilder.
	 */
	public class AbstractBuilder<T extends TestInnerBuilder, B extends AbstractBuilder<T, B>>
			extends AbstractObjectBuilder<T, B> {

		/** Notice this is private. */
		protected AbstractBuilder(T building) {
			super(building);
		}

		public B setVariable(Integer variable) {
			// use the traditional "building"
			// notice for java 6 this is NOT a problem.
			// for Java 7 this is a problem.
			((TestInnerBuilder) building).variable = variable;

			// notice this works for Java 7
			((TestInnerBuilder) building).variable = variable;

			// also, you can reference, however, this would allow someone that retains a reference
			// to builder to continue mutating after building. Our build method nulls out "building"
			// so once it is built the mutation is over
			// THIS IS NOT A PROBLEM for Java 7
			TestInnerBuilder.this.variable = variable;
			return dis;
		}

	}// end Builder

	public class Builder
			extends AbstractBuilder<TestInnerBuilder, Builder> {

		protected Builder() {
			super(TestInnerBuilder.this);
		}

	}

	/**
	 * The only way to create a Builder that is the only way to create a {@link TestInnerBuilder}.
	 * 
	 * @return
	 */
	public static Builder create() {
		TestInnerBuilder result = new TestInnerBuilder();
		return result.new Builder();
	}

	private Integer variable;

	/** Use {@link Builder} to construct TestInnerBuilder */
	private TestInnerBuilder() {
	}

}
