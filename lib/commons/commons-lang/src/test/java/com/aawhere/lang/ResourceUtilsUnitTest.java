/**
 *
 */
package com.aawhere.lang;

import static org.junit.Assert.*;

import java.net.URL;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.MessageResourceBundleUnitTest;

/**
 * @see ResourceUtils
 * 
 * @author roller
 * 
 */
public class ResourceUtilsUnitTest {
	static final String RESOURCE_NAME = "testResource.txt";

	@Test
	public void testGetResource() throws Exception {
		URL actual = ResourceUtils.resourceFromPackage(getClass(), RESOURCE_NAME);
		TestUtil.assertContains(actual.getPath(), "com/aawhere/lang/testResource.txt");
	}

	@Test
	public void testGetResoureByClass() {
		URL actual = ResourceUtils.resourceFromClass(getClass(), "txt");

		TestUtil.assertContains(actual.getPath(), "com/aawhere/lang/ResourceUtilsUnitTest.txt");
	}

	@Test(expected = MissingResourceException.class)
	public void testGetMissing() {
		assertNull(ResourceUtils.resourceFromClass(ResourceUtils.class, "junk"));
	}

	@Test
	public void testGetResoureBundle() {
		String baseName = MessageResourceBundleUnitTest.EXAMPLE_PROPERTIES_BASE_NAME;
		ResourceBundle actual = ResourceUtils.getResourceBundle(baseName, Locale.US);

		assertNotNull(actual);
	}

	@Test
	public void testProperties() {
		Properties properties = ResourceUtils.loadProperties(getClass());
		TestUtil.assertContains(properties.getProperty("test"), "Success");
	}
}
