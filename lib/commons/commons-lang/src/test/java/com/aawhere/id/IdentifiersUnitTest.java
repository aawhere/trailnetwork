/**
 * 
 */
package com.aawhere.id;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Test;

import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.id.IdentifierTestUtil.ChildIds;
import com.aawhere.lang.string.FromStringUnitTest;
import com.aawhere.lang.string.StringFormatException;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class IdentifiersUnitTest {

	private ChildIds actualIds;

	@Test
	public void testSingleId() {
		ChildId childId = IdentifierTestUtil.generateRandomChildId();
		this.actualIds = new ChildIds(childId.toString());
		TestUtil.assertContainsOnly(childId, actualIds);
	}

	@Test
	public void testMultiple() {
		ChildId childId = IdentifierTestUtil.generateRandomChildId();
		ChildId childId2 = IdentifierTestUtil.generateRandomChildId();
		ArrayList<ChildId> expectedIds = Lists.newArrayList(childId, childId2);
		actualIds = new ChildIds(IdentifierUtils.idsJoined(expectedIds));
		TestUtil.assertIterablesEquals("ids don't match", expectedIds, actualIds);

	}

	@Test
	public void testEmptyString() {
		this.actualIds = new ChildIds(StringUtils.EMPTY);
		TestUtil.assertEmpty(actualIds);
	}

	@Test
	public void testNull() {
		this.actualIds = new ChildIds(null);
		TestUtil.assertEmpty(this.actualIds);
	}

	@After
	public void assertActualIds() {
		assertIds(this.actualIds);
	}

	public static <I extends Identifier<?, E>, E extends Identifiable<I>> void assertIds(Identifiers<I, E> ids) {
		try {
			FromStringUnitTest.assertFromString(ids);
		} catch (StringFormatException | NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}
}
