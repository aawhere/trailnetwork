/**
 * 
 */
package com.aawhere.lang.string;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @see EndpointSentenceParser
 * @author aroller
 * 
 */
public class EndpointSentenceParserUnitTest {

	/**
	 * 
	 */
	private static final String HOME_TO_WORK = "Home to Work";
	private static final String WORK = "Work";
	private static final String HOME = "Home";

	@Test
	public void testNothing() {
		assertEndpoints("This has nothing in it", null, null);
	}

	@Test
	public void testHomeToWork() {
		assertEndpoints(HOME_TO_WORK, HOME, WORK);
	}

	/** Home and back again makes the endpoints both home. */
	@Test
	public void testHomeToWorkToHome() {
		assertEndpoints(HOME_TO_WORK + " to Home", HOME, HOME, "", 1);
	}

	@Test
	public void testHomeToWorkCapitalized() {
		assertEndpoints("Home To Work", HOME, WORK);
	}

	@Test
	public void testHomeToWorkAllCaps() {
		assertEndpoints("Home TO Work", HOME, WORK);
	}

	@Test
	public void testToWork() {
		assertEndpoints("To Work", null, WORK);
	}

	@Test
	@Ignore("this one isn't worth fixing. it's an unusual case")
	public void testTo() {
		assertEndpoints("To", null, null, "To", 1);
	}

	@Test
	public void testWorkToHome() {
		assertEndpoints("Work to Home", WORK, HOME);
	}

	@Test
	public void testFromHomeToWork() {
		assertEndpoints("From Home to Work", HOME, WORK);
	}

	@Test
	public void testFromHome() {
		assertEndpoints("From Home", HOME, null);
	}

	@Test
	public void testFromHomeSuffix() {
		assertEndpoints("Did something From Home", HOME, null, "Did something", 0);
	}

	@Test
	@Ignore("it's doable, but nice to have if it's not too ugly and needed")
	public void testInverseToHomeFromWork() {
		assertEndpoints("To Home from Work", HOME, WORK);
	}

	@Test
	public void testFromHomeWithsSomeone() {
		// removing with and other keywords is currently out of score
		assertEndpoints("From Home with Bob", "Home with Bob", null);
	}

	@Test
	public void testColonSuffix() {
		assertEndpoints("from: Home to: Work", HOME, WORK);
	}

	@Test
	public void testToInMiddleOfWord() {
		assertEndpoints("Today is great!", null, null);
	}

	@Test
	public void testDanglingPreposition() {
		assertEndpoints("Where are you off to?", null, null);

	}

	private static EndpointSentenceParser assertEndpoints(String sentence, String expectedFrom, String expectedTo) {
		return assertEndpoints(sentence, expectedFrom, expectedTo, "", 0);
	}

	/**
	 * @param string
	 * @param object
	 * @param object2
	 * @return
	 */
	private static EndpointSentenceParser assertEndpoints(String sentence, String expectedFrom, String expectedTo,
			String expectedRemainder, Integer expectedLeftoverCount) {

		EndpointSentenceParser parser = EndpointSentenceParser.create(sentence).build();
		assertEquals("to", expectedTo, parser.to());
		assertEquals("from", expectedFrom, parser.from());
		if (expectedFrom == null && expectedTo == null) {
			expectedRemainder = sentence;
		}
		assertEquals("remainder", expectedRemainder, parser.remainder());
		TestUtil.assertSize(expectedLeftoverCount, parser.leftovers());
		return parser;
	}

	@Test
	public void testXml() {
		EndpointSentenceParser parser = assertEndpoints(HOME_TO_WORK, HOME, WORK);
		JAXBTestUtil<EndpointSentenceParser> util = JAXBTestUtil.create(parser).build();
		util.assertContains(HOME);
		util.assertContains(WORK);
		util.assertContains(HOME_TO_WORK);
	}
}
