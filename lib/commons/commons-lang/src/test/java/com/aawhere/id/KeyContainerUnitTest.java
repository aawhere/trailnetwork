/**
 * 
 */
package com.aawhere.id;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Collections2;

/**
 * @author roller
 * 
 */
public class KeyContainerUnitTest {

	@Test
	public void testPredicate() throws Exception {
		ChildId expectedKey;
		ChildId notExpectedKey;
		do {
			expectedKey = new ChildId(TestUtil.generateRandomString());
			notExpectedKey = new ChildId(TestUtil.generateRandomString());
		} while (expectedKey.equals(notExpectedKey));

		TestUtil.assertNotEquals("this shouldn't be possible", expectedKey, notExpectedKey);

		ArrayList<ExampleKeyContainer> list = new ArrayList<KeyContainerUnitTest.ExampleKeyContainer>();
		final ExampleKeyContainer expected = new ExampleKeyContainer(expectedKey);
		list.add(expected);
		list.add(new ExampleKeyContainer(notExpectedKey));
		Collection<ExampleKeyContainer> filtered = Collections2.filter(list, new KeyPredicate<ChildId>(expectedKey));
		assertEquals("only expected should remain" + filtered, 1, filtered.size());
		assertTrue(filtered.contains(expected));
	}

	@Test(expected = RuntimeException.class)
	public void testPredicateNull() {
		new KeyPredicate<ChildId>(null);
	}

	public static class ExampleKeyContainer
			implements KeyedEntity<ChildId> {

		private static final long serialVersionUID = -3912232540811486238L;
		ChildId key;

		/**
		 * 
		 */
		public ExampleKeyContainer(ChildId key) {
			this.key = key;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.id.KeyContainer#getKey()
		 */
		@Override
		public ChildId getKey() {
			return key;
		}

	}
}
