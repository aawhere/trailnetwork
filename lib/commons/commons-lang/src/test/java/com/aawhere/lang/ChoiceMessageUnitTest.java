/**
 * 
 */
package com.aawhere.lang;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Tests {@link InvalidChoiceException} and {@link LangMessage}.
 * 
 * @author Aaron Roller
 * 
 */
public class ChoiceMessageUnitTest {

	public static final String CHOSEN = "fubar";
	public static final String[] CHOICES = { "foo", "bar" };

	static enum Choices {
		A, B, C
	}

	@Test
	public void testNothingChosenNoChoices() {
		assertChoiceMessage(null, null, LangMessage.NOTHING_CHOSEN_FROM_NO_CHOICES, "");
	}

	public static void assertChoiceMessage(Object chosen, Object[] choices, LangMessage expected,
			String expectedToContain) {
		CompleteMessage completeMessage = LangMessage.create(chosen, choices);
		assertEquals(expected, completeMessage.getMessage());
		TestUtil.assertContains(completeMessage.toString(), expectedToContain);

	}

	@Test
	public void testNothingChosenFromChoices() {
		assertChoiceMessage(null, CHOICES, LangMessage.NOTHING_CHOSEN_FROM_CHOICES, "");
	}

	@Test
	public void testChosenNoChoices() {
		assertChoiceMessage(CHOSEN, null, LangMessage.CHOSEN_FROM_NO_CHOICES, CHOSEN);
	}

	@Test
	public void testWrongChoiceFromChoices() {
		assertChoiceMessage(CHOSEN, CHOICES, LangMessage.CHOSEN_FROM_CHOICES, CHOSEN);
	}

	@Test
	public void testEnum() {
		assertChoiceMessage(Choices.A, null, LangMessage.UNHANDLED_CHOICE, Choices.A.name());
	}

	@Test
	public void testChoiceException() {
		TestUtil.assertContains(new InvalidChoiceException(CHOSEN).getMessage(), CHOSEN);
	}

	@Test
	public void testExceptionWithChoices() {
		TestUtil.assertContains(new InvalidChoiceException(CHOSEN, (Object[]) CHOICES).getMessage(), CHOSEN);
	}

	@Test
	public void testExceptionWithEnum() {
		TestUtil.assertContains(new InvalidChoiceException(Choices.C).getMessage(), Choices.B.name());
	}
}
