/**
 * 
 */
package com.aawhere.math.stats;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;

/**
 * @author Aaron Roller on Apr 22, 2011
 * @see StatisticalAnalysis
 */
public class StatisticalAnalysisUnitTest {

	public static Integer BASELINE = 10;
	public static Integer BASELINE_UP = BASELINE + 1;
	public static Integer BASELINE_DOWN = BASELINE - 1;

	public static Integer BELOW = BASELINE / 2;

	@Test
	public void testNoDeviation() {
		StatisticalAnalysis.Builder<Integer, Integer> statsBuilder = new StatisticalAnalysis.Builder<Integer, Integer>();

		final int NUMBER_OF_ENTRIES = 10;
		int tolerance = 0;
		for (int i = tolerance; i <= NUMBER_OF_ENTRIES; i++) {
			statsBuilder.add(BASELINE, i);
		}

		StatisticalAnalysis<Integer, Integer> stats = statsBuilder.build();

		assertEquals(BASELINE.doubleValue(), stats.getMean().doubleValue(), tolerance);
		assertEquals(tolerance, stats.getStandardDeviation(), tolerance);
	}

	@Test
	public void oneAnomoly() {
		Integer[] values = { BASELINE, BASELINE, BELOW, BASELINE, BASELINE };
		double expectedMean = 9;
		assertStats(values, expectedMean, 2.23, new Integer[] { 2 });

	}

	@Test
	public void jumpyWithOneOutlier() {
		Integer[] values = { BASELINE, BASELINE_UP, BELOW, BASELINE_DOWN, BASELINE, BASELINE, BASELINE_UP,
				BASELINE_DOWN };
		assertStats(values, 9.375, 1.9, new Integer[] { 2 });

	}

	private StatisticalAnalysis<Integer, Integer> assertStats(Integer[] values, double expectedMean,
			double expectedDeviationFromMean, Integer[] outlierIndeces) {
		StatisticalAnalysis.Builder<Integer, Integer> statsBuilder;
		statsBuilder = new StatisticalAnalysis.Builder<Integer, Integer>();

		for (int i = 0; i < values.length; i++) {
			Integer integer = values[i];
			statsBuilder.add(integer, i);
		}
		StatisticalAnalysis<Integer, Integer> stats = statsBuilder.build();
		double tolerance = 0.1;
		assertEquals(expectedMean, stats.getMean().doubleValue(), tolerance);
		assertEquals(expectedDeviationFromMean, stats.getStandardDeviation(), 0.1);
		List<StatisticalAnalysis<Integer, Integer>.Entry> outliers = stats.getOutliers();
		assertEquals(outlierIndeces.length, outliers.size());

		for (StatisticalAnalysis<Integer, Integer>.Entry entry : outliers) {
			ArrayUtils.contains(outlierIndeces, entry.getIndex());
		}
		return stats;
	}
}
