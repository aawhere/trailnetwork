/**
 *
 */
package com.aawhere.math.stats;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Tests {@link StorelessWeightedEvaluation} and {@link WeightedEvaluationFactory}.
 * 
 * @author Aaron Roller
 * 
 * @see <a
 *      href="http://en.wikipedia.org/wiki/Weighted_mean">http://en.wikipedia.org/wiki/Weighted_mean</a>
 * 
 */
public class WeightedEvaluationUnitTest {

	/**
	 * This test is based on <a href=
	 * "http://en.wikipedia.org/w/index.php?title=Weighted_mean&oldid=456261630" >this example</a>.
	 * 
	 */
	@Test
	public void testWeightedMean() {

		double[] values = { 80, 90 };
		double[] weights = { 20, 30 };
		double expectedValue = 86.0;

		StorelessWeightedEvaluation evaluation = WeightedEvaluationFactory.newMean();
		evaluation.incrementAll(values, weights);
		assertEquals(expectedValue, evaluation.getResult(), 0.0);
	}
}
