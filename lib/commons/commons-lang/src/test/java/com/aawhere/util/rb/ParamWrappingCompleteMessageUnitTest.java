package com.aawhere.util.rb;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.text.format.custom.CustomFormat;

/**
 * Tests and demonstrates param wrapping using {@link ParamWrappingCompleteMessage}.
 * 
 * @author aroller
 * 
 */
public class ParamWrappingCompleteMessageUnitTest {

	private static final String BEFORE = "before";
	private static final String AFTER = "after";
	private static final String BETWEWEEN = "between";
	private static final String BULLSEYE = "bullseye";
	private static final String SINGLE_VALUE = BEFORE + " {TARGET_XX}{TARGET}{XX} " + AFTER;
	private static final String FORMATTED_SINGLE = "before <span class=\"target\">between</span> after";
	private static final String FORMATTED_DOUBLE = FORMATTED_SINGLE + " <span class=\"found\">-bullseye-</span>";

	@Test
	public void testSingleParameter() {
		CompleteMessage completeMessage = ParamWrappingCompleteMessage.create(WrappingTestMessage.SINGLE)
				.paramWrapped(WrappingTestMessage.Param.TARGET, BETWEWEEN).build();
		Map<ParamKey, Object> parameters = completeMessage.getParameters();
		TestUtil.assertSize("one for target, open and close", 3, parameters.entrySet());
		ParamKey closeKey = ParamWrappingCompleteMessage.close();
		assertNotNull(parameters.get(closeKey));
		assertEquals(FORMATTED_SINGLE, completeMessage.toString());
	}

	@Test
	public void testDouble() {
		CompleteMessage completeMessage = ParamWrappingCompleteMessage.create(WrappingTestMessage.DOUBLE)
				.paramWrapped(WrappingTestMessage.Param.TARGET, BETWEWEEN)
				.paramWrapped(WrappingTestMessage.Param.FOUND, BULLSEYE).build();
		assertEquals(FORMATTED_DOUBLE, completeMessage.toString());
	}

	public enum WrappingTestMessage implements Message {
		/** demonstrates a single replacement. */
		SINGLE(SINGLE_VALUE, Param.TARGET),
		/** demonstrates a double replacment */
		DOUBLE(SINGLE_VALUE + " {FOUND_XX}-{FOUND}-{XX}", Param.TARGET, Param.FOUND),

		;

		final private ParamKey[] parmKeys;
		private String value;

		private WrappingTestMessage(String value, ParamKey... paramKeys) {
			this.parmKeys = paramKeys;
			this.value = value;
		}

		@Override
		public String getValue() {
			return value;
		}

		@Override
		public ParamKey[] getParameterKeys() {
			return this.parmKeys;
		}

		public enum Param implements ParamKey {
			TARGET, FOUND;

			@Override
			public CustomFormat<?> getFormat() {

				return null;
			}

		}

	}
}
