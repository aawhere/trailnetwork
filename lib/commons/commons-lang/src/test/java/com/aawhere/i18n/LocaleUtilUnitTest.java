package com.aawhere.i18n;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.Test;

import com.aawhere.i18n.LocaleUtil;

/**
 * 
 */

/**
 * Tests {@link LocaleUtil}.
 * 
 * @author roller
 * 
 */
public class LocaleUtilUnitTest {

	@Test
	public void testLocalFromLang() {
		Locale expected = Locale.ENGLISH;
		Locale actual = LocaleUtil.localeFrom(expected.getLanguage(), null);
		assertEquals(expected, actual);
	}

	@Test
	public void testLocalFromLangAndCountry() {
		Locale expected = Locale.US;
		assertFromLocale(expected);
	}

	/**
	 * @param expected
	 */
	private void assertFromLocale(Locale expected) {
		Locale actual = LocaleUtil.localeFrom(expected.getLanguage(), expected.getCountry());
		assertEquals(expected, actual);
	}
}
