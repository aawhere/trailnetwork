/**
 *
 */
package com.aawhere.util.rb;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.text.format.custom.PercentFormat;
import com.aawhere.text.format.custom.RangeKeywordFormat;
import com.google.common.base.Function;
import com.google.common.collect.Range;

/**
 * Strictly used for testing {@link MessageResourceBundle}.
 * 
 * The top level enum is the message that can be present only as a enum or declared in a properites
 * file.
 * 
 * The nested Enum is the list of parameters that are related to the Message and passed to the
 * message during construction.
 * 
 * @see MessageResourceBundleUnitTest
 * 
 * @author roller
 * 
 */
@XmlRootElement
public enum ExampleMessage implements Message {
	/** The message is in the file with no parameters. */
	MESSAGE_IN_FILE,
	/** The message is in the file, but specific params are provided */
	MESSAGE_IN_FILE_WITH_PARAMS(Param.FIRST_NAME, Param.USERNAME),
	/**
	 * The message is in the file, but only indexed parameters are declared (legacy support).
	 */
	MESSAGE_IN_FILE_WITH_INDEXED_PARAMS,
	/** The message is not in the file so this only this message will be used. */
	MESSAGE_PROVIDED_NOT_IN_FILE("Message not in file"),
	/**
	 * The message in java time is different than in the properties file...warning is provided.
	 */
	MESSAGE_PROVIDED_DIFFERENT_THAN_IN_FILE("Message different in file"),
	/** Message is in java time with parameters provided */
	MESSAGE_PROVIDED_WITH_PARAMS("{FIRST_NAME} created new user {USERNAME}.", Param.FIRST_NAME, Param.USERNAME),
	/**
	 * Ensures the proper formatting of each of the types per the parameter given.
	 */
	MESSAGE_TESTING_INTEGER_FORMAT("{INTEGER} is an int", Param.INTEGER),
	/** test formatting a long */
	MESSAGE_TESTING_LONG_FORMAT("{LONG} is a long", Param.LONG),
	/** tests formatting a double */
	MESSAGE_TESTING_DOUBLE_FORMAT("{DOUBLE} is a double", Param.DOUBLE),
	/** tests formatting a float */
	MESSAGE_TESTING_FLOAT_FORMAT("{FLOAT} is a float", Param.FLOAT),
	/** Test formatting a percent */
	MESSAGE_TESTING_PERCENT_FORMAT("{PERCENT} is a percent", Param.PERCENT),
	/** the value should be provided only in the the zulu file, not even here */
	ZULU_OVERRIDES_ENUM("This is from the enum!"),
	/**  */
	PLURAL_SUBSITUTION(
			"There were {CAT_COUNT_VALUE, plural, =0{no cats} one{one cat} other{# cats}}, that crossed the road on {DATE_VALUE, date, short}. The cat was the {CAT_COUNT_VALUE, ordinal} to cross the road. This is the ParamKey formatted cat count followed by the ICU formatted count: {CAT_COUNT}, {CAT_COUNT_VALUE}",
			Param.CAT_COUNT,
			Param.DATE),
	EMBEDDED_MESSAGE("The other guy says \"{MESSAGE}\" so it must be true.", Param.MESSAGE),
	VERB_TENSE("{TENSE_KEY,select," //
			+ "past{I tested the verb tense.} " //
			+ "present{I'm testing the verb tense.} "//
			+ "future{I will test the verb tense.}"//
			+ "other{I have no plans to test.}"//
			+ "}"//
	, VerbTense.Param.TENSE),

	/**
	 * @see MessageResourceBundleUnitTest#testPluralRules()
	 */
	PLURAL_RULES("{TEST_FAILURE_COUNT_KEY, select," //
			+ "zero{No tests failed. Super! } "//
			+ "one{One test failed. Almost there...} "//
			+ "two{A couple of tests failed. Achtung!} "//
			+ "few{A few tests failed. You can do better.} "//
			+ "many{Many tests failed. Maybe you need to rethink your strategy.} "//
			+ "other{# tests failed! Find a new job!}}", Param.TEST_FAILURE_COUNT),
	/**
	 * @see MessageResourceBundleUnitTest#testKeywordRange
	 */
	KEYWORD_RANGE("{LETTER} is {LETTER_KEY,select,"//
			+ "early{towards the beginning}" //
			+ "middle{in the middle}"//
			+ "other{near the end}}.", Param.LETTER)

	;

	private ParamKey[] parameterKeys;
	private String message;

	private ExampleMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	private ExampleMessage(ParamKey... parameterKeys) {
		this(null, parameterKeys);
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements SelectParamKey {
		/** Simplest example of a parameter that provides a readable key */
		FIRST_NAME,
		/** Simple username with a custom format provided */
		USERNAME(new ExampleUsernameCustomFormat()),
		/** demonstrates how to pass a percent as a double */
		PERCENT(PercentFormat.getInstance()),
		/** demonstrates how to pass a float */
		FLOAT,
		/** demonstrates how to pass an Integer */
		INTEGER,
		/** demonstrates how to pass a double */
		DOUBLE,
		/** demonstrates how to pass a long */
		LONG,
		/** Number of cats for pluralization example. */
		CAT_COUNT,
		/** A complete message that is to be embedded in another message */
		MESSAGE,
		/**
		 * A number used for rule testing
		 * 
		 * @see ExampleMessage#PLURAL_RULES
		 */
		TEST_FAILURE_COUNT(null, RangeKeywordFormat.standardIntegerFunction()),
		/**
		 * Used to test comparisons...purposely leaving out end to test the other default
		 * 
		 * @see MessageResourceBundleUnitTest#testKeywordRange()
		 * */
		LETTER(MessageTestUtil.toUpperCaseFormat(), RangeKeywordFormat.<Character> rangeFunction(Pair.of(Range
				.closed('a', 'j'), "early"), Pair.of(Range.closed('k', 'p'), "middle"))),
		/** Date handling using ICU */
		DATE;
		CustomFormat<?> format;
		private Function<Object, String> selectKeyFunction;

		private Param() {
		}

		@SuppressWarnings("unchecked")
		private Param(CustomFormat<?> customFormat, Function<?, String> selectKeyFunction) {
			this.selectKeyFunction = (Function<Object, String>) selectKeyFunction;
			this.format = customFormat;
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}

		@Override
		public String selectKey(Object value) {
			if (this.selectKeyFunction == null) {
				return null;
			}
			return selectKeyFunction.apply(value);
		}
	}

}
