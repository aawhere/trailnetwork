/**
 *
 */
package com.aawhere.adapter;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.lang.NotImplementedException;
import com.aawhere.lang.exception.BaseException;

/**
 * @author Brian Chapman
 * 
 */
public class AdapterFactoryUnitTest {

	MockAdapterFactory adapterFactory;
	MockNumberAdapter mockAdapter;
	MockEnumAdapter enumAdapter;

	@Before
	public void setUp() {
		this.adapterFactory = new MockAdapterFactory();
		this.mockAdapter = new MockNumberAdapter();
		this.enumAdapter = new MockEnumAdapter();
		this.adapterFactory.register(mockAdapter);
		this.adapterFactory.register(enumAdapter);
	}

	@Test
	public void testHandlesType() {
		MockAdapterFactory myAdapterFactory = new MockAdapterFactory();
		assertTrue(!myAdapterFactory.handles(mockAdapter.getType()));
		myAdapterFactory.register(mockAdapter);
		myAdapterFactory.register(enumAdapter);
		assertTrue(myAdapterFactory.handles(mockAdapter.getType()));
		assertTrue(myAdapterFactory.handles(Integer.class));
		assertTrue(myAdapterFactory.handles(Double.class));
		assertTrue(!myAdapterFactory.handles(String.class));
		assertTrue(myAdapterFactory.handles(Enum.class));
	}

	/*
	 * Just testing that it succeeds for now TODO: actually test this against collections, since
	 * that is what it is currently for.
	 */
	@Test
	public void testHandlesTypeParamaterizedType() {
		assertTrue(adapterFactory.handles(mockAdapter.getType(), Object.class));
	}

	@Test
	@Ignore("nothing in here, but someone found it worth writing")
	public void testGetAdapterOrNullTypeParamaterizedType() {

	}

	@Test
	public void testGetAdapterOrNullType() {
		Adapter<?, String> adapter = adapterFactory.getAdapterOrNull(mockAdapter.getType());
		assertEquals(mockAdapter, adapter);
		Adapter<?, String> expectedToBeNull = adapterFactory.getAdapterOrNull(String.class);
		assertTrue(expectedToBeNull == null);
	}

	@Test
	public void testGetAdapterTypeParamaterizedType() {
		// TODO: ...
	}

	@Test
	public void testGetAdapterType() {
		Adapter<?, ?> adapterNumber = adapterFactory.getAdapter(mockAdapter.getType());
		assertEquals(mockAdapter, adapterNumber);
		Adapter<?, ?> adapterInteger = adapterFactory.getAdapter(Integer.class);
		assertEquals(mockAdapter, adapterInteger);
		Adapter<?, ?> adapterDouble = adapterFactory.getAdapter(Double.class);
		assertEquals(mockAdapter, adapterDouble);
	}

	@Test(expected = IllegalStateException.class)
	public void testGetAdapterTypeException() {
		adapterFactory.getAdapter(String.class);
	}

	/**
	 * An adapter factory For testing purposes.
	 * 
	 * @author brian
	 * 
	 */
	public static class MockAdapterFactory
			extends AbstractAdapterFactory<String> {

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.lang.AbstractAdapterFactory#getCollectionAdapter(com.aawhere.lang.Adapter,
		 * java.lang.Class)
		 */
		@Override
		protected <C extends Collection<T>, T> Adapter<C, String> getCollectionAdapter(Adapter<T, String> adapter,
				Class<T> paramaterType) {
			throw new NotImplementedException();
		}

	}

	public static class MockNumberAdapter
			implements Adapter<Number, String> {

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.Adapter#marshal(java.lang.Object)
		 */
		@Override
		public String marshal(Object adaptee) {
			return adaptee.toString();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.Adapter#unmarshal(java.lang.Object, java.lang.Class)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public <N extends Number> N unmarshal(String adapted, Class<N> type) throws BaseException {
			if (type.equals(Integer.class)) {
				return (N) Integer.valueOf(adapted);
			} else if (type.equals(Double.class)) {
				return (N) Double.valueOf(adapted);
			} else {
				throw new UnsupportedOperationException("I don't know that number type.");
			}
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.Adapter#getType()
		 */
		@Override
		public Class<Number> getType() {
			return Number.class;
		}

	}

	public static class MockEnumAdapter
			implements Adapter<Enum<?>, String> {

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.Adapter#marshal(java.lang.Object)
		 */
		@Override
		public String marshal(Object adaptee) {
			return adaptee.toString();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.Adapter#unmarshal(java.lang.Object, java.lang.Class)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public <E extends Enum<?>> E unmarshal(String adapted, Class<E> type) throws BaseException {
			try {
				return (E) type.getDeclaredMethod("valueOf").invoke(adapted);
			} catch (IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (SecurityException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			} catch (InvocationTargetException e) {
				throw new RuntimeException(e);
			} catch (NoSuchMethodException e) {
				throw new RuntimeException(e);
			}
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.adapter.Adapter#getType()
		 */
		@SuppressWarnings("unchecked")
		@Override
		public Class<Enum<?>> getType() {
			return (Class<Enum<?>>) (Class<?>) Enum.class;
		}

	}

}
