/**
 * 
 */
package com.aawhere.xml.bind;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil.Junk;
import com.aawhere.xml.bind.JAXBTestUtil.JunkXmlAdapter;
import com.aawhere.xml.bind.JAXBTestUtil.NoRoot;
import com.aawhere.xml.bind.JAXBTestUtil.Parameterized;
import com.aawhere.xml.bind.JAXBTestUtil.SingleValue;

/**
 * Tests classes found in the com.aawhere.xml.bind package.
 * 
 * @author Aaron Roller
 * 
 */
public class JAXBUnitTest {

	/**
	 * 
	 */
	private static final QName Q_NAME = new QName("test");
	Junk expected;

	@Before
	public void setUp() {
		expected = new Junk();
		expected.field1 = TestUtil.generateRandomString();
		expected.adapted = TestUtil.generateRandomString();
	}

	@Test
	public void testStringWriter() {
		JAXBStringWriter writer = JAXBStringWriter.create(expected).build();
		TestUtil.assertContains(writer.getXml(), Junk.class.getSimpleName().toLowerCase());
		TestUtil.assertContains(writer.getXml(), expected.field1);
		TestUtil.assertContains(writer.getXml(), JunkXmlAdapter.DEFAULT);
	}

	@Test
	public void testRoundTrip() {
		JAXBTestUtil<Junk> util = JAXBTestUtil.create(this.expected).build();
		assertEquals(expected, util.getExpected());
		assertEquals(expected.field1, util.getActual().field1);
		TestUtil.assertContains(util.getXml(), JunkXmlAdapter.DEFAULT);
		assertEquals(expected.adapted, util.getActual().adapted);

	}

	@Test
	public void testCustomAdapter() {

		JunkXmlAdapter adapter = new JunkXmlAdapter();
		adapter.prefix = "adapted";
		JAXBTestUtil<Junk> util = JAXBTestUtil.create(this.expected).adapter(adapter).build();
		util.assertContains(adapter.prefix);
	}

	@Test
	public void testSingleValue() {
		SingleValue expected = new SingleValue();
		expected.value = TestUtil.generateRandomString();
		JAXBTestUtil<SingleValue> util = JAXBTestUtil.create(expected).build();
		SingleValue actual = util.getActual();
		assertEquals(expected.value, actual.value);
	}

	@Test(expected = RuntimeException.class)
	public void testNoRoot() {
		JAXBTestUtil<NoRoot> util = JAXBTestUtil.create(new NoRoot()).build();
		fail(util.getXml() + " has no root?");
	}

	@Test
	@Ignore("reader doesn't like JAXBElement as much as writer does")
	public void testUtilNoRootJaxbElementWrapper() {
		JAXBElement<NoRoot> jaxbElement = new JAXBElement<NoRoot>(Q_NAME, NoRoot.class, new NoRoot());
		JAXBTestUtil<JAXBElement<NoRoot>> util = JAXBTestUtil.create(jaxbElement).classesToBeBound(NoRoot.class)
				.build();
		util.assertContains(NoRoot.ELEMENT_NAME);
	}

	@Test
	public void testWriterNoRootJaxbElementWrapper() {
		JAXBElement<NoRoot> jaxbElement = new JAXBElement<NoRoot>(Q_NAME, NoRoot.class, new NoRoot());
		JAXBStringWriter writer = new JAXBStringWriter.Builder().element(jaxbElement).build();
		TestUtil.assertContains(writer.getXml(), Q_NAME.getLocalPart());
	}

	@Test
	public void testParameterizedType() {
		Parameterized<Junk> element = new Parameterized<Junk>();
		element.element = this.expected;
		JAXBTestUtil<Parameterized<Junk>> util = JAXBTestUtil.create(element).classesToBeBound(Junk.class).build();
		util.assertContains(this.expected.field1);
	}

}
