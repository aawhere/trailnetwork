/**
 * 
 */
package com.aawhere.collections;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.aawhere.xml.bind.JAXBTestUtil.Builder;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;

/**
 * @author aroller
 * 
 */
public class CollectionXmlAdapterUnitTest {

	public static Integer FIRST_KEY = 1;
	public static Integer SECOND_KEY = 2;
	public static String FIRST_VALUE = "first";
	public static String FIRST_SECOND_VALUE = "second of first";
	public static String SECOND_VALUE = "second";
	private Container container;

	@XmlType
	public enum Letter {
		@XmlElement
		A, @XmlElement
		B, @XmlElement
		C
	};

	@Before
	public void setUp() {
		container = new Container();
	}

	@Test
	public void testHashMap() {
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		populateTestMap(map);
		Container mc = new Container();
		mc.map = map;
		// complex values in enums make the adapter fail
		// mc.enumMap = new HashMap<Integer, CollectionXmlAdapterUnitTest.Letter>();
		// mc.enumMap.put(FIRST_KEY, Letter.A);
		// mc.enumMap.put(SECOND_KEY, Letter.C);
		JAXBTestUtil<Container> util = JAXBTestUtil.create(mc).build();
		assertMap(util.getActual().map);
	}

	@Test
	@Ignore("HashMap.Entry is not being recognized in the package-info adapter declaration of Map.Entry ")
	public void testMapEntryAsRoot() {
		Map<Integer, String> map = populateTestMap(new HashMap<Integer, String>());
		Entry<Integer, String> expected = map.entrySet().iterator().next();
		JAXBTestUtil<Map.Entry<Integer, String>> util = JAXBTestUtil.create(expected)
				.adapter(new MapEntryXmlAdapter<>()).build();
		Entry<Integer, String> actual = util.getActual();
		assertEquals(expected, actual);
	}

	@Test
	public void testCollectionOfEntries() {
		Map<Integer, String> map = populateTestMap(new HashMap<Integer, String>());
		container.entries = map.entrySet();
		JAXBTestUtil<Container> util = JAXBTestUtil.create(container).build();
		Container actual = util.getActual();
		assertNotNull(actual.entries);
		assertEquals(container.entries, actual.entries);
	}

	@Test
	public void testPair() {
		Map<Integer, String> map = populateTestMap(new HashMap<Integer, String>());
		Entry<Integer, String> expected = map.entrySet().iterator().next();
		assertEntry(Pair.of(expected.getKey(), expected.getValue()));
	}

	@Test
	public void testEntry() {
		Map<Integer, String> map = populateTestMap(new HashMap<Integer, String>());
		Entry<Integer, String> expected = map.entrySet().iterator().next();
		assertEntry(expected);
	}

	/**
	 * @param expected
	 */
	private void assertEntry(Entry<Integer, String> expected) {
		container.entry = expected;
		JAXBTestUtil<Container> util = JAXBTestUtil.create(container).build();
		Entry<Integer, String> actual = util.getActual().entry;
		util.assertContains(expected.getKey().toString());
		util.assertContains(expected.getValue());
		assertEquals(expected, actual);
	}

	@Test
	public void testHashBiMap() {
		HashBiMap<Integer, String> map = HashBiMap.create();
		container.biMap = populateTestMap(map);
		JAXBTestUtil<Container> util = JAXBTestUtil.create(container).build();
		assertMap(util.getActual().biMap);
	}

	/**
	 * @param map
	 */
	private void assertMap(Map<Integer, String> map) {
		assertEquals(FIRST_VALUE, map.get(FIRST_KEY));
		assertEquals(SECOND_VALUE, map.get(SECOND_KEY));
	}

	@Test
	public void testMultimap() {

		Container it = getTestMultiMap();
		JAXBTestUtil<Container> util = JAXBTestUtil.create(it).build();
		Container actual = util.getActual();
		Set<String> first = actual.multiMap.get(FIRST_KEY);
		Iterator<String> firstValues = first.iterator();
		assertTrue(util.getXml(), firstValues.hasNext());
		String firstActual = firstValues.next();
		assertEquals(FIRST_VALUE, firstActual);
		util.assertContains(FIRST_VALUE);
		String firstSecondActual = firstValues.next();
		assertEquals(FIRST_SECOND_VALUE, firstSecondActual);
	}

	@Test
	public void testSet() {
		SetContainer expected = getTestSet();
		JAXBTestUtil<SetContainer> util = JAXBTestUtil.create(expected).build();
		SetContainer actual = util.getActual();
		assertNotNull(actual);
		TestUtil.assertContains(FIRST_VALUE, actual.set);
		TestUtil.assertContains(SECOND_VALUE, actual.set);
		TestUtil.assertContains(FIRST_SECOND_VALUE, actual.set);

	}

	public static Map<Integer, String> populateTestMap(Map<Integer, String> map) {
		map.put(FIRST_KEY, FIRST_VALUE);
		map.put(SECOND_KEY, SECOND_VALUE);
		return map;
	}

	public static Container getTestMultiMap() {
		Container mc = new Container();
		mc.multiMap = HashMultimap.create();
		mc.multiMap.put(FIRST_KEY, FIRST_VALUE);
		mc.multiMap.put(FIRST_KEY, FIRST_SECOND_VALUE);
		mc.multiMap.put(SECOND_KEY, SECOND_VALUE);
		return mc;
	}

	public static SetContainer getTestSet() {
		SetContainer sc = new SetContainer();
		sc.set.add(FIRST_VALUE);
		sc.set.add(SECOND_VALUE);
		sc.set.add(FIRST_SECOND_VALUE);
		return sc;
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Container {

		@XmlJavaTypeAdapter(HashMultimapXmlAdapter.class)
		HashMultimap<Integer, String> multiMap;

		@XmlJavaTypeAdapter(HashMapXmlAdapter.class)
		Map<Integer, String> map;

		@XmlJavaTypeAdapter(HashBiMapXmlAdapter.class)
		Map<Integer, String> biMap;

		@XmlJavaTypeAdapter(HashMapXmlAdapter.class)
		Map<Integer, Letter> enumMap;

		// declared in package-info
		Entry<Integer, String> entry;

		Set<Entry<Integer, String>> entries;

		/**
		 * 
		 */
		public Container() {

		}
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class SetContainer {

		@XmlJavaTypeAdapter(SetXmlAdapter.class)
		@XmlElement
		Set<String> set = new HashSet<String>();
	}
}
