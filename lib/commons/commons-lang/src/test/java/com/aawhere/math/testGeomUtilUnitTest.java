/**
 *
 */
package com.aawhere.math;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author brian
 * 
 */
public class testGeomUtilUnitTest {

	@Test
	public void testCircumferenceToRadius() {
		double circumference = 2 * Math.PI;
		double radius = GeomUtil.circumferenceToRadius(circumference);
		TestUtil.assertDoubleEquals(1, radius);
	}
}
