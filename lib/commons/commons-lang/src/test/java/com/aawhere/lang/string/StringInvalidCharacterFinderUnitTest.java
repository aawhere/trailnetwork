package com.aawhere.lang.string;

import static org.junit.Assert.*;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;

/**
 * 
 */

/**
 * Tests {@link StringInvalidCharacterFinder}.
 * 
 * @author roller
 * 
 */
public class StringInvalidCharacterFinderUnitTest {

	public static final Character VALID_LETTER1 = 'a';
	public static final Character VALID_LETTER2 = 'b';
	public static final Character VALID_LETTER3 = 'c';
	public static final Character INVALID_LETTER1 = 'd';
	public static final Character INVALID_LETTER2 = 'e';

	public static final Character VALID_DIGIT1 = '1';
	public static final Character VALID_DIGIT2 = '2';
	public static final Character VALID_DIGIT3 = '3';
	public static final Character INVALID_DIGIT1 = '4';
	public static final Character INVALID_DIGIT2 = '5';

	public static final Character VALID_CHARACTER1 = '@';
	public static final Character VALID_CHARACTER2 = '#';
	public static final Character VALID_CHARACTER3 = '$';
	public static final Character INVALID_CHARACTER1 = '%';
	public static final Character INVALID_CHARACTER2 = '^';

	public static final String VALID_LETTERS = String.valueOf(VALID_LETTER1) + VALID_LETTER2 + VALID_LETTER3;
	public static final String INVALID_LETTERS = String.valueOf(INVALID_LETTER1) + INVALID_LETTER2;
	public static final String VALID_DIGITS = String.valueOf(VALID_DIGIT1) + VALID_DIGIT2 + VALID_DIGIT3;
	public static final String INVALID_DIGITS = String.valueOf(INVALID_DIGIT1) + INVALID_DIGIT2;
	public static final String VALID_CHARACTERS = String.valueOf(VALID_CHARACTER1) + VALID_CHARACTER2
			+ VALID_CHARACTER3;
	public static final String INVALID_CHARACTERS = String.valueOf(INVALID_CHARACTER1) + INVALID_CHARACTER2;

	/**
	 * @param expectedString
	 * @param expectedInvalidCount
	 * @param finder
	 */
	private void assertFinder(String expectedString, int expectedInvalidCount, StringInvalidCharacterFinder finder) {
		String view = finder.toString();
		assertEquals(view, expectedInvalidCount, finder.getIndexes().getAll().size());
		assertEquals(expectedString, view);
	}

	@Test(expected = RuntimeException.class)
	public void testNothingAllowed() {
		String string = INVALID_CHARACTERS;
		String expectedString = String.valueOf('[') + INVALID_CHARACTER1 + ']' + '[' + INVALID_CHARACTER2 + ']';
		int expectedInvalidCount = INVALID_CHARACTERS.length();
		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(string).build();
		assertFinder(expectedString, expectedInvalidCount, finder);
	}

	@Test
	public void testLettersOnly() {
		String string = VALID_LETTERS;
		String expectedString = string;
		int expectedInvalidCount = 0;
		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(string)
				.setLettersAreAllowed(true).build();
		assertFinder(expectedString, expectedInvalidCount, finder);
	}

	@Test
	public void testDigitsOnly() {
		String string = VALID_DIGITS;
		String expectedString = string;
		int expectedInvalidCount = 0;
		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(string)
				.setDigitsAreAllowed(true).build();
		assertFinder(expectedString, expectedInvalidCount, finder);
	}

	@Test
	public void testLettersAndDigits() {
		String string = VALID_DIGITS + VALID_LETTERS;
		String expectedString = string;
		int expectedInvalidCount = 0;
		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(string)
				.setDigitsAreAllowed(true).setLettersAreAllowed(true).build();
		assertFinder(expectedString, expectedInvalidCount, finder);
	}

	@Test
	public void testCharactersOnly() {
		String string = VALID_CHARACTERS;
		String expectedString = string;
		int expectedInvalidCount = 0;
		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(string)
				.addCharacterIsAllowed(ArrayUtils.toObject(VALID_CHARACTERS.toCharArray())).build();
		assertFinder(expectedString, expectedInvalidCount, finder);
	}

	static String invalidLetters(Character[] chars) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			Character character = chars[i];
			StringInvalidCharacterFinder.appendInvalidIdentifier(buffer, character);
		}
		return buffer.toString();
	}

	@Test
	public void testLettersOnlyProvidingDigits() {
		String string = VALID_DIGITS;
		String expectedString = invalidLetters(new Character[] { VALID_DIGIT1, VALID_DIGIT2, VALID_DIGIT3 });
		int expectedInvalidCount = VALID_DIGITS.length();
		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(string)
				.setLettersAreAllowed(true).build();
		assertFinder(expectedString, expectedInvalidCount, finder);
	}

	@Test
	public void testNotLetters() {
		String string = VALID_CHARACTERS + VALID_DIGITS;
		String expectedString = invalidLetters(ArrayUtils.toObject(string.toCharArray()));
		int expectedInvalidCount = string.length();
		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(string)
				.setLettersAreAllowed(true).build();
		assertFinder(expectedString, expectedInvalidCount, finder);
	}

	@Test
	public void testInvalidCharacters() {
		String string = INVALID_CHARACTERS;
		String expectedString = invalidLetters(ArrayUtils.toObject(string.toCharArray()));
		int expectedInvalidCount = string.length();
		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(string)
				.addCharacterIsAllowed(ArrayUtils.toObject(VALID_CHARACTERS.toCharArray())).build();
		assertFinder(expectedString, expectedInvalidCount, finder);
	}
}
