/**
 *
 */
package com.aawhere.util.rb;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.i18n.SystemLocale;
import com.aawhere.lang.exception.ExampleException;
import com.aawhere.test.TestUtil;
import com.aawhere.text.format.custom.CustomFormatUnitTest;
import com.aawhere.util.rb.ExampleMessage.Param;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Tests the basic funcionality of {@link MessageResourceBundle}.
 * 
 * @author roller
 * 
 */
public class MessageResourceBundleUnitTest {

	public static final String EXAMPLE_PROPERTIES_BASE_NAME = ExampleMessage.class.getName();

	private static final String EXTRA_KEY_BASE_NAME = EXAMPLE_PROPERTIES_BASE_NAME + "ExtraKey";

	public static final String FIRST_NAME_VALUE = "Dan";

	/**
	 * Corresponds to the {@link ExampleMessage#MESSAGE_IN_FILE} key in the example properties file.
	 * 
	 */
	public static final String MESSAGE_IN_FILE_VALUE = "Some irrelevant message.";

	public static final String MESSAGE_IN_FILE_VALUE_ZU = "汉语/漢語 لعربية/عربي/عربى Àà junk ἄλφα твёрдый знак";
	public static final String ONLY_IN_ZULU = "This is in Zulu.";
	/** Used to test the indexed values. */
	public static final String USERNAME_VALUE = "danno";
	public static final String[] MESSAGE_IN_FILE_WITH_PARAMS_PARAMS = { FIRST_NAME_VALUE, USERNAME_VALUE };
	/**
	 * Used to test the formatter of a username works.
	 * 
	 */
	public static final String[] MESSAGE_IN_FILE_WITH_PARAMS_PARAMS_UPPER = new String[] { FIRST_NAME_VALUE,
			USERNAME_VALUE.toUpperCase() };
	public static final String MESSAGE_IN_FILE_WITH_PARAMS_VALUE_FORMATTED = "Dan created a new user danno.";
	private static final String MISSING_KEY_BASE_NAME = EXAMPLE_PROPERTIES_BASE_NAME + "MissingKey";

	/**
	 * All language testing will use this locale. Reference here so we may change it easily.
	 */
	public static final Locale TEST_LOCALE = SystemLocale.ZULU.locale;

	/**
	 * @return
	 */
	public static CompleteMessage getExampleDoubleMessage() {
		return new CompleteMessage.Builder().setMessage(ExampleMessage.MESSAGE_TESTING_DOUBLE_FORMAT)
				.addParameter(ExampleMessage.Param.DOUBLE, CustomFormatUnitTest.DECIMAL_NUMBER).build();
	}

	/**
	 * @return
	 */
	public static CompleteMessage getExampleFloatMessage() {
		return new CompleteMessage.Builder().setMessage(ExampleMessage.MESSAGE_TESTING_FLOAT_FORMAT)
				.addParameter(ExampleMessage.Param.FLOAT, CustomFormatUnitTest.DECIMAL_NUMBER).build();
	}

	/**
	 * @return
	 */
	public static CompleteMessage getExampleIntegerMessage() {
		return new CompleteMessage.Builder().setMessage(ExampleMessage.MESSAGE_TESTING_INTEGER_FORMAT)
				.addParameter(ExampleMessage.Param.INTEGER, CustomFormatUnitTest.WHOLE_NUMBER).build();
	}

	/**
	 * @return
	 */
	public static CompleteMessage getExampleLongMessage() {
		return new CompleteMessage.Builder().setMessage(ExampleMessage.MESSAGE_TESTING_LONG_FORMAT)
				.addParameter(ExampleMessage.Param.LONG, CustomFormatUnitTest.WHOLE_NUMBER).build();
	}

	/**
	 * @return
	 */
	public static CompleteMessage getExamplePercentMessage() {
		return new CompleteMessage.Builder().setMessage(ExampleMessage.MESSAGE_TESTING_PERCENT_FORMAT)
				.addParameter(ExampleMessage.Param.PERCENT, CustomFormatUnitTest.DECIMAL_NUMBER).build();
	}

	/**
	 * Provides access to the ExampleResourceBundle used for testing.
	 * 
	 * @see ExampleMessage
	 * @return
	 */
	public static ResourceBundle getExampleResourceBundle() {
		return ResourceBundle.getBundle(EXAMPLE_PROPERTIES_BASE_NAME);
	}

	public static MessageResourceBundle getValidBundle() {
		return new MessageResourceBundle(ExampleMessage.class);
	}

	/**
	 * Run this in a unit test against any Message enum to make sure it is valid.
	 */
	public static void validateManagedResourceBundle(Class<? extends Enum<? extends Message>> keyClass) {
		MessageTestUtil.assertMessages(keyClass);
	}

	/**
	 * Tests the message that uses many numbers to be formatted by the automatic formatting or
	 * provided formatters.
	 * 
	 * @see ExampleMessage#MESSAGE_TESTING_FORMATS
	 * 
	 */
	public void assertFormattedMessage(String expected, CompleteMessage message) {

		String formattedMessage = getValidBundle().formatMessage(message);
		assertEquals(expected, formattedMessage);
	}

	/**
	 * @return
	 */
	public CompleteMessage getExampleInFileMessage() {
		CompleteMessage message = new CompleteMessage.Builder().setMessage(ExampleMessage.MESSAGE_IN_FILE).build();
		return message;
	}

	/**
	 * @return
	 */
	public CompleteMessage getExampleInFileWithParamsMessage() {
		CompleteMessage message = new CompleteMessage.Builder()
				.setMessage(ExampleMessage.MESSAGE_IN_FILE_WITH_PARAMS)
				.addParameters(	ExampleMessage.MESSAGE_IN_FILE_WITH_PARAMS.getParameterKeys(),
								MESSAGE_IN_FILE_WITH_PARAMS_PARAMS_UPPER).build();
		return message;
	}

	/**
	 *
	 */
	@Test
	public void testAllConstantsExist() throws Exception {

		MessageResourceBundle example = getValidBundle();
		assertNotNull(example);

		// throws missing resource exception if not o.k
		example.enumsMatchKeysInBundle();
		assertTrue(example.containsKey(ExampleMessage.MESSAGE_IN_FILE));
		assertTrue(example.containsKey(ExampleMessage.MESSAGE_IN_FILE_WITH_PARAMS));
		assertEquals(MESSAGE_IN_FILE_VALUE, example.getString(ExampleMessage.MESSAGE_IN_FILE, Locale.getDefault()));

	}

	@Test(expected = MissingResourceException.class)
	public void testExtraKeysInFile() {
		MessageResourceBundle example = new MessageResourceBundle(ExampleMessage.class, EXTRA_KEY_BASE_NAME);
		example.keysInBundleMatchEnums();
	}

	@Test
	public void testFactory() {
		MessageResourceBundleFactory factory = MessageResourceBundleFactory.getInstance();
		factory.register(getValidBundle());
		MessageResourceBundle bundle = factory.getManagedResourceBundle(ExampleMessage.MESSAGE_IN_FILE);
		assertNotNull(bundle);
		bundle = factory.getManagedResourceBundle(ExampleMessage.MESSAGE_IN_FILE);
		assertNotNull(bundle);
		bundle = factory.getManagedResourceBundle(TestUtil.generateRandomString());
		assertNull(bundle);

	}

	@Test
	public void testFactoryFromMessageWithNoRegistration() {

		Message message = ExampleMessage.MESSAGE_IN_FILE;
		MessageResourceBundleFactory factory = MessageResourceBundleFactory.getInstance();
		MessageResourceBundle bundle = factory.getManagedResourceBundleFromMessage(message);
		assertNotNull(bundle);
		MessageResourceBundle bundleFromEnum = factory.getManagedResourceBundle(ExampleMessage.MESSAGE_IN_FILE);
		assertSame("these came from the same message so they should be the exact same RB", bundle, bundleFromEnum);
	}

	@Test
	public void testFactoryWithNoRegistration() {
		MessageResourceBundleFactory factory = MessageResourceBundleFactory.getInstance();
		MessageResourceBundle bundle = factory.getManagedResourceBundle(ExampleMessage.MESSAGE_IN_FILE);
		assertNotNull(bundle);
	}

	@Test
	public void testFormatSimpleMessage() {
		CompleteMessage message = getExampleInFileMessage();
		MessageResourceBundle example = getValidBundle();
		String formattedMessage = example.formatMessage(message);
		assertEquals(MESSAGE_IN_FILE_VALUE, formattedMessage);
	}

	@Test
	public void testMessageComesFromFileRatherThanEnumIfProvided() {
		MessageResourceBundle resourceBundle = getValidBundle();
		String string = resourceBundle.getString(ExampleMessage.ZULU_OVERRIDES_ENUM, TEST_LOCALE);
		assertEquals("the message in the properties file should override the enumeration", ONLY_IN_ZULU, string);
	}

	@Test(expected = MissingResourceException.class)
	public void testGetStringMessageMissing() {
		MessageResourceBundle resourceBundle = getValidBundle();
		Message nullMessage = new Message() {

			@Override
			public String name() {
				return "this_should_be_so_ridiculous_it_will_never_match";
			}

			@Override
			public String getValue() {
				return null;
			}

			@Override
			public ParamKey[] getParameterKeys() {
				return null;
			}
		};
		resourceBundle.getString(nullMessage, TEST_LOCALE);
	}

	@Test
	public void testPluralizatonMessage() {
		MessageResourceBundleFactory factory = MessageResourceBundleFactory.getInstance();
		MessageResourceBundle bundle = factory.getManagedResourceBundle(ExampleMessage.PLURAL_SUBSITUTION);

		CompleteMessage zero = CompleteMessage.create(ExampleMessage.PLURAL_SUBSITUTION)
				.addParameter(ExampleMessage.Param.CAT_COUNT, 0).addParameter(ExampleMessage.Param.DATE, new Date())
				.build();
		String formattedZero = bundle.formatMessage(zero, Locale.FRANCE);
		assertPluralization(formattedZero, "no cats");

		CompleteMessage one = CompleteMessage.create(ExampleMessage.PLURAL_SUBSITUTION)
				.addParameter(ExampleMessage.Param.CAT_COUNT, 1).addParameter(ExampleMessage.Param.DATE, new Date())
				.build();
		String formattedOne = bundle.formatMessage(one, Locale.FRANCE);
		assertPluralization(formattedOne, "one cat,");

		CompleteMessage other = CompleteMessage.create(ExampleMessage.PLURAL_SUBSITUTION)
				.addParameter(ExampleMessage.Param.CAT_COUNT, 3200).addParameter(ExampleMessage.Param.DATE, new Date())
				.build();
		String formattedOther = bundle.formatMessage(other, Locale.FRANCE);
		assertPluralization(formattedOther, "3 200 cats");

	}

	/**
	 * Plural Rules allow custom ranges for the predfined plural categories (zero,one,few, many).
	 * This demonstrates how to provide custom PluralRules for your message.
	 * 
	 * http://icu-project.org/apiref/icu4j/com/ibm/icu/text/PluralRules.html
	 */
	@Test
	public void testPluralRulesZero() {
		int count = 0;
		String expectedQualifierWord = "No";
		assertPluralRules(count, expectedQualifierWord);

	}

	@Test
	public void testPluralRulesOne() {
		assertPluralRulesOne();

	}

	/**
	 * handy for re-use when testing in others
	 * 
	 * @return
	 */
	private Pair<CompleteMessage, String> assertPluralRulesOne() {
		return assertPluralRules(1, "One");
	}

	@Test
	public void testPluralRulesCouple() {
		assertPluralRules(2, "couple");
	}

	@Test
	public void testPluralRulesFew() {
		String expected = "few";

		assertPluralRules(3, expected);
		assertPluralRules(4, expected);
		assertPluralRules(5, expected);
		assertPluralRules(6, expected);
		assertPluralRules(7, expected);
		assertPluralRules(8, expected);
		assertPluralRules(9, expected);
		assertPluralRules(10, expected);
	}

	@Test
	public void testPluralRulesMany() {
		String expected = "Many";
		assertPluralRules(11, expected);
	}

	@Test
	public void testPluralRulesOther() {
		assertPluralRules(999, "job");
	}

	@Test
	@Ignore("demonstrates the standard # notation does not work using the select list.  You must follow the patter in #assertSelectForLetter")
	public
			void testPluralRulesOtherWithSubstitute() {
		int tooMany = 999;
		String expected = String.valueOf(tooMany);
		assertPluralRules(tooMany, expected);
	}

	/**
	 * @see #assertSelectForLetter(char, String)
	 */
	@Test
	public void testKeywordRange() {
		char smallLetter = 'd';
		String expectedPartOfMessage = "begin";
		assertSelectForLetter(smallLetter, expectedPartOfMessage);

	}

	@Test
	public void testKeywordRangeMiddle() {
		assertSelectForLetter('m', "middle");
	}

	/**
	 * Notice no end range is declared. this proves other will capture all
	 */
	@Test
	public void testKeywordRangeEnd() {
		assertSelectForLetter('y', "end");
	}

	/**
	 * Tests a letter comparison that transforms the value into a selection that matches a range. It
	 * also formats that value showing it can do both.
	 * 
	 * @param smallLetter
	 * @param expectedPartOfMessage
	 */
	private void assertSelectForLetter(char smallLetter, String expectedPartOfMessage) {
		CompleteMessage message = CompleteMessage.create(ExampleMessage.KEYWORD_RANGE).param(Param.LETTER, smallLetter)
				.build();
		assertMessage(expectedPartOfMessage, message);
		assertMessage(String.valueOf(smallLetter).toUpperCase(), message);
	}

	private Pair<CompleteMessage, String> assertPluralRules(int count, String expectedQualifierWord) {

		CompleteMessage message = CompleteMessage.create(ExampleMessage.PLURAL_RULES)
				.param(Param.TEST_FAILURE_COUNT, count).build();

		return Pair.of(message, assertMessage(expectedQualifierWord, message));
	}

	public static String assertMessage(String expectedQualifierWord, CompleteMessage message) {
		MessageResourceBundleFactory factory = MessageResourceBundleFactory.getInstance();
		MessageResourceBundle bundle = factory.getManagedResourceBundleFromMessage(message.getMessage());
		String formatted = bundle.formatMessage(message, Locale.US);
		TestUtil.assertContains(formatted, expectedQualifierWord);
		return formatted;
	}

	private void assertPluralization(String formatted, String expectedToContain) {
		// System.out.println(formatted);
		assertNotNull(formatted);
		assertTrue(formatted.contains(expectedToContain));
	}

	@Test
	public void testFormatSimpleMessageZulu() {
		CompleteMessage message = getExampleInFileMessage();
		MessageResourceBundle example = getValidBundle();
		String formattedMessage = example.formatMessage(message, SystemLocale.ZULU.locale);
		assertEquals(MESSAGE_IN_FILE_VALUE_ZU, formattedMessage);
	}

	@Test
	public void testFormattedDouble() {
		assertFormattedMessage(CustomFormatUnitTest.DECIMAL_FORMATTED + " is a double", getExampleDoubleMessage());
	}

	@Test
	public void testFormattedFloat() {
		assertFormattedMessage(CustomFormatUnitTest.DECIMAL_FORMATTED + " is a float", getExampleFloatMessage());
	}

	@Test
	public void testFormattedInt() {
		assertFormattedMessage(CustomFormatUnitTest.WHOLE_NUMBER_FORMATTED + " is an int", getExampleIntegerMessage());
	}

	@Test
	public void testFormattedLong() {
		assertFormattedMessage(CustomFormatUnitTest.WHOLE_NUMBER_FORMATTED + " is a long", getExampleLongMessage());
	}

	@Test
	public void testFormattedPercent() {
		assertFormattedMessage(CustomFormatUnitTest.PERCENT_FORMATTED + " is a percent", getExamplePercentMessage());
	}

	/**
	 * @see ExampleMessage#MESSAGE_IN_FILE_WITH_PARAMS
	 */
	@Test
	public void testFormatWithParams() {
		CompleteMessage message = getExampleInFileWithParamsMessage();
		MessageResourceBundle example = getValidBundle();
		String formattedMessage = example.formatMessage(message);
		assertEquals(MessageResourceBundleUnitTest.MESSAGE_IN_FILE_WITH_PARAMS_VALUE_FORMATTED, formattedMessage);
	}

	@Test
	public void testMessageWithoutPropertiesFile() {

		MessageResourceBundle rb = new MessageResourceBundle(ExampleMessageNoPropertiesFile.class);
		String paramValue = TestUtil.generateRandomString();
		CompleteMessage message = new CompleteMessage.Builder().setMessage(ExampleMessageNoPropertiesFile.ENTRY1)
				.addParameter(ExampleMessageNoPropertiesFile.Param.FIRST_PARAM, paramValue).build();

		String formattedMessage = rb.formatMessage(message);
		assertEquals(MESSAGE_IN_FILE_VALUE + paramValue, formattedMessage);
	}

	@Test(expected = MissingResourceException.class)
	public void testMissingFile() {
		new MessageResourceBundle(ExampleMessage.class, TestUtil.generateRandomString());
	}

	@Test(expected = MissingResourceException.class)
	public void testMissingKeysInFile() {
		MessageResourceBundle example = new MessageResourceBundle(ExampleMessage.class, MISSING_KEY_BASE_NAME);
		example.enumsMatchKeysInBundle();
	}

	@Test
	public void testStringMessage() {

		String value = TestUtil.generateRandomString();
		StringMessage message = new StringMessage(value);
		assertEquals(value, message.getValue());

	}

	@Test
	public void testErrorCode() {
		ExampleMessage expected = ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE;
		ExampleException exception = new ExampleException(expected);
		assertEquals(expected.toString(), exception.getErrorCode());
	}

	@Test
	public void testValidExample() {
		validateManagedResourceBundle(ExampleMessage.class);
	}

	@Test
	public void testWithMissingFile() {
		try {
			new MessageResourceBundle(ExampleMessage.class, TestUtil.generateRandomString());
			fail(ExampleMessage.class.getSimpleName() + " requires a properties file");
		} catch (MissingResourceException e) {
			assertEquals(	"The failure should have been caused by the enum providing the meesage in the file",
							ExampleMessage.MESSAGE_IN_FILE.toString(),
							e.getKey());
		}

	}

	@Test
	public void testStringMessageJaxb() {
		Message expected = new StringMessage(TestUtil.generateRandomString(), TestUtil.generateRandomString());
		assertJaxb(expected);
	}

	/**
	 * @param expected
	 */
	private void assertJaxb(Message expected) {
		JAXBTestUtil<Message> util = JAXBTestUtil.create(expected).build();
		Message actual = util.getActual();
		assertMessageEquals(expected, actual);
	}

	/**
	 * @param expected
	 * @param actual
	 */
	private void assertMessageEquals(Message expected, Message actual) {
		Class<? extends Message> expectedClass = expected.getClass();
		assertMessageEquals(expected, actual, expectedClass);
	}

	/**
	 * @param expected
	 * @param actual
	 * @param expectedClass
	 */
	private void assertMessageEquals(Message expected, Message actual, Class<? extends Message> expectedClass) {
		assertEquals(expected.name(), actual.name());
		assertEquals(expected.getValue(), actual.getValue());
		assertEquals(expectedClass, actual.getClass());
	}

	@Test
	public void testMessageJaxb() {
		Message expected = ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE;
		assertJaxb(expected);
	}

	@Test
	public void testMessageContainerJaxb() {
		Message expected = ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE;
		MessageContainer container = new MessageContainer();
		container.message = expected;
		JAXBTestUtil<MessageContainer> util = JAXBTestUtil.create(container).build();
		assertMessageEquals(expected, util.getActual().message, StringMessage.class);
	}

	@XmlRootElement
	@XmlType
	public static class MessageContainer {
		@XmlElement
		private Message message;
	}

	/**
	 * Havinga complete message within another is common. 12 mountain bike rides start from this
	 * trailhead where mountain bike rides is the activity type that has a singular and plural form.
	 * 
	 */
	@Test
	public void testEmbeddedCompleteMessage() {
		// provides the complete message to embedd and also the expected value from the other test
		// so we can use it to validate the embedded worked
		Pair<CompleteMessage, String> onePair = assertPluralRulesOne();

		CompleteMessage message = CompleteMessage.create(ExampleMessage.EMBEDDED_MESSAGE)
				.param(ExampleMessage.Param.MESSAGE, onePair.getKey()).build();
		assertMessage(onePair.getValue(), message);

	}

	@Test
	public void testVerbTense() {
		assertMessage(	"tested",
						CompleteMessage.create(ExampleMessage.VERB_TENSE).param(VerbTense.Param.TENSE, VerbTense.PAST)
								.build());
		assertMessage(	"testing",
						CompleteMessage.create(ExampleMessage.VERB_TENSE)
								.param(VerbTense.Param.TENSE, VerbTense.PRESENT).build());
		assertMessage(	"will test",
						CompleteMessage.create(ExampleMessage.VERB_TENSE)
								.param(VerbTense.Param.TENSE, VerbTense.FUTURE).build());
		// tests the required default by passing in junk
		assertMessage("plans", CompleteMessage.create(ExampleMessage.VERB_TENSE).param(VerbTense.Param.TENSE, "Junk")
				.build());

	}
}
