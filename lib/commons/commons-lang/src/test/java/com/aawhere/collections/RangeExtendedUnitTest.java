/**
 * 
 */
package com.aawhere.collections;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.lang.number.NumberUtilsExtended;

import com.google.common.base.Function;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class RangeExtendedUnitTest {

	@Test
	public void testLongRangeToInteger() {
		Integer inRange = 6;
		Integer outOfRange = 3;
		Range<Long> longRange = Range.atLeast(5l);
		assertTrue(longRange.apply(inRange.longValue()));
		assertFalse(longRange.apply(outOfRange.longValue()));

		Function<Long, Integer> function = NumberUtilsExtended.integerFromLongFunction();
		Range<Integer> intRange = RangeExtended.compose(longRange, function);
		assertTrue(intRange.apply(inRange));
		assertFalse(intRange.apply(outOfRange));
	}

}
