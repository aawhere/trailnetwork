/**
 * 
 */
package com.aawhere.lang;

/**
 * A Test class to demonstrate/test the use of {@link Builder} and {@link AbstractBuilder}.
 * 
 * The {@link TestBuilder} is an immutable object that is produced only by it's {@link Builder}. To
 * allow for TestBuilder to be extended this provides an {@link AbstractBuilder} that children of
 * TestBuilder will extend from in their own Builder.
 * 
 * 
 * 
 * @author aroller
 * 
 */
public class TestBuilder {

	/**
	 * Used to construct all instances of TestBuilder.
	 */
	public static class AbstractBuilder<T extends TestBuilder, BuilderT extends AbstractBuilder<T, BuilderT>>
			extends AbstractObjectBuilder<T, BuilderT> {

		public AbstractBuilder(T building) {
			super(building);
		}

		/**
		 * Public mutator only available during building.
		 * 
		 * 
		 * @param variable
		 * @return
		 */
		public BuilderT setVariable(Integer variable) {
			// This cast is required in Java 7. building.variable = variable was allowed in Java 6.
			((TestBuilder) building).variable = variable;
			return dis;
		}

		public T build() {
			T built = super.build();
			return built;
		}

	}// end Builder

	/**
	 * Used to construct all instances of TestBuilder.
	 */
	public static class Builder
			extends AbstractBuilder<TestBuilder, Builder> {

		public Builder() {
			super(new TestBuilder());
		}

		public TestBuilder build() {
			TestBuilder built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private Integer variable;

	/**
	 * Use {@link Builder} to construct TestBuilder . Protected to allow extension.
	 * */
	protected TestBuilder() {
	}

	/**
	 * Public accessor available for all to use.
	 * 
	 * @return the variable
	 */
	public Integer getVariable() {
		return this.variable;
	}
}
