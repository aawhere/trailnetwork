/**
 * 
 */
package com.aawhere.xml.bind;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.junit.Before;
import org.junit.Test;

/**
 * @see JAXBElementFactory
 * 
 * @author Aaron Roller
 * 
 */
public class JAXBElementFactoryUnitTest {

	private static final String ELEMENT_NAME = "junk-trash";

	JAXBElementFactory factory;

	@Before
	public void setUp() {
		factory = new JAXBElementFactory.Builder().setNamespace("http://api.only-testing.com").build();
	}

	@Test
	public void testXmlRootElement() {
		WithRootElement element = new WithRootElement();
		assertSame(element, factory.createElement(element));
		assertWrapWorthy(element.getClass(), false);
	}

	public static void assertWrapWorthy(Class<?> type, boolean expected) {
		assertEquals(expected, JAXBElementFactory.isWrapWorthy(type));
	}

	@Test
	public void testXmlTypeElement() throws Exception {
		WithXmlType element = new WithXmlType();
		final Object actual = factory.createElement(element);
		assertNotSame(element, actual);
		assertEquals(JAXBElement.class, actual.getClass());
		assertWrapWorthy(element.getClass(), true);
	}

	@Test
	public void testNoAnnotation() throws Exception {
		NoAnnotation element = new NoAnnotation();
		assertEquals(element, factory.createElement(element));
		assertWrapWorthy(element.getClass(), false);
	}

	static class NoAnnotation {

	}

	@XmlRootElement(name = ELEMENT_NAME)
	static class WithRootElement {

	}

	@XmlType(name = ELEMENT_NAME)
	static class WithXmlType {

	}
}
