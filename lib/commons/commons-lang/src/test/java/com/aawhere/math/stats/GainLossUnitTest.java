/**
 * 
 */
package com.aawhere.math.stats;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class GainLossUnitTest {

	public static int positive = 1;
	public static int negative = -1;

	@Test
	public void testGain() {
		Gain gain = new Gain();
		gain.increment(positive);
		assertEquals(1.0, gain.getResult(), 0.1);
		gain.increment(positive);
		assertEquals(2.0, gain.getResult(), 0.1);
		gain.increment(negative);
		assertEquals(2.0, gain.getResult(), 0.1);
	}

	@Test
	public void testLoss() {
		Loss loss = new Loss();
		loss.increment(negative);
		assertEquals(1.0, loss.getResult(), 0.1);
		loss.increment(negative);
		assertEquals(2.0, loss.getResult(), 0.1);
		loss.increment(positive);
		assertEquals(2.0, loss.getResult(), 0.1);
	}
}
