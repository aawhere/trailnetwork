/**
 * 
 */
package com.aawhere.lang.enums;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessageXmlAdapter;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.xml.XmlNamespace;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author aroller
 * 
 */
@Ignore("enums are lame with JAXB..only the name is shown or one alternate string as XmlEnumValue")
public class EnumJaxbUnitTest {

	private static final Example EXPECTED = Example.FIRST;

	@Test
	public void testJaxbAsRoot() {

		final Example expected = EXPECTED;
		assertJaxb(expected);
	}

	/**
	 * @param expected
	 */
	private void assertJaxb(final Object expected) {
		JAXBTestUtil<Object> util = JAXBTestUtil.create(expected).adapter(new MessageXmlAdapter()).build();
		util.assertContains(EXPECTED.name());
		util.assertContains(EXPECTED.display);
		System.out.println(util.getXml());
	}

	@Test
	public void testJaxbElementContained() {

		assertJaxb(new ElementContainter());
	}

	@Test
	public void testJaxbAttributeContained() {

		assertJaxb(new AttributeContainter());
	}

	@Test
	public void testMessage() {
		assertJaxb(MessgaeExample.FIRST);
	}

	@XmlRootElement
	public static class ElementContainter {
		@XmlElement
		Example element = EXPECTED;

	}

	@XmlRootElement
	public static class AttributeContainter {
		@XmlAttribute
		Example element = EXPECTED;

	}

	public static interface Displayable {
		String getDisplay();
	}

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static enum Example implements Displayable {
		FIRST("First"), SECOND("Second");

		@XmlElement
		public final String display;

		private Example(String display) {
			this.display = display;
		}

		@Override
		public String getDisplay() {
			return display;
		}
	}

	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static enum MessgaeExample implements Message {
		FIRST(Example.FIRST.display);

		@XmlElement
		private String display;

		private MessgaeExample(String first) {
			this.display = first;
		}

		@Override
		public String getValue() {
			return display;
		}

		@Override
		public ParamKey[] getParameterKeys() {
			return null;
		}

	}
}
