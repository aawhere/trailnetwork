/**
 * 
 */
package com.aawhere.lang.bool;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class BooleanUtilUnitTest {

	@Test
	public void testEquals() {

		assertTrue("both null", BooleanUtil.equals(null, null));
		assertTrue("both false", BooleanUtil.equals(Boolean.FALSE, Boolean.FALSE));
		assertTrue("both true", BooleanUtil.equals(Boolean.TRUE, Boolean.TRUE));
		assertFalse("left null right True", BooleanUtil.equals(null, Boolean.TRUE));
		assertFalse("left null right false", BooleanUtil.equals(null, Boolean.FALSE));
		assertFalse("left true right null", BooleanUtil.equals(Boolean.TRUE, null));
		assertFalse("left false right null", BooleanUtil.equals(Boolean.FALSE, null));
		assertFalse("left true right false", BooleanUtil.equals(Boolean.TRUE, Boolean.FALSE));
		assertFalse("left false right true", BooleanUtil.equals(Boolean.FALSE, Boolean.TRUE));
	}
}
