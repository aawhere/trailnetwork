/**
 * 
 */
package com.aawhere.collections;

import org.junit.Test;

import com.aawhere.test.TestUtil;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class MultisetsExtendedUnitTest {

	@Test
	public void testCountFiltered() {
		HashMultiset<String> multiset = HashMultiset.create();
		final String one = "one";
		final String two = "two";
		final String three = "three";
		final String four = "four";
		multiset.add(one);
		multiset.add(two);
		multiset.add(two);
		multiset.add(three);
		multiset.add(three);
		multiset.add(three);
		multiset.add(four);
		multiset.add(four);
		multiset.add(four);
		multiset.add(four);
		TestUtil.assertContainsOnly(one, MultisetsExtended.countFiltered(multiset, Range.singleton(1)).elementSet());
		TestUtil.assertContainsOnly(two, MultisetsExtended.countFiltered(multiset, Range.singleton(2)).elementSet());
		TestUtil.assertContainsOnly(three, MultisetsExtended.countFiltered(multiset, Range.singleton(3)).elementSet());
		TestUtil.assertContainsOnly(four, MultisetsExtended.countFiltered(multiset, Range.singleton(4)).elementSet());
		TestUtil.assertContainsOnly(four,
									MultisetsExtended.countFiltered(multiset, Range.closed(2, 3)).elementSet(),
									two,
									three);
	}
}
