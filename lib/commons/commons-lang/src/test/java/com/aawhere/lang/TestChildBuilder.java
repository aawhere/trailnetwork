/**
 * 
 */
package com.aawhere.lang;

/**
 * Demonstrates a concrete extension of an object that uses the {@link AbstractObjectBuilder} to
 * produce it's instance.
 * 
 * @author aroller
 * 
 */
public class TestChildBuilder
		extends TestBuilder {

	/**
	 * Used to construct all instances of TestChildBuilder.
	 */
	public static class Builder
			extends TestBuilder.AbstractBuilder<TestChildBuilder, Builder> {

		public Builder() {
			super(new TestChildBuilder());
		}

		public TestChildBuilder build() {
			TestChildBuilder built = super.build();
			return built;
		}

		public Builder setChildVariable(Integer childVariable) {
			// notice there is no problem accessing the outer class variable from a static inner
			// class
			building.childVariable = childVariable;
			return this;
		}

	}// end Builder

	public static Builder createChild() {
		return new Builder();
	}

	private Integer childVariable;

	/** Use {@link Builder} to construct TestChildBuilder */
	private TestChildBuilder() {
	}

	/**
	 * @return the childVariable
	 */
	public Integer getChildVariable() {
		return this.childVariable;
	}
}
