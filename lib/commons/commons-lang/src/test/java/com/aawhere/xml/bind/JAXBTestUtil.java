/**
 *
 */
package com.aawhere.xml.bind;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.test.TestUtil;

/**
 * Used to help tests validate JAXBObject ability to be marshalled and unmarshalled by completing
 * the round trip from a populated annotated element object into xml back into the actual element.
 * 
 * @author Aaron Roller
 * 
 */
public class JAXBTestUtil<T> {

	/**
	 * Used to construct all instances of JAXBTestUtil.
	 */
	@XmlTransient
	public static class Builder<T>
			extends ObjectBuilder<JAXBTestUtil<T>> {

		Set<XmlAdapter<?, ?>> adapters = new HashSet<XmlAdapter<?, ?>>();
		Map<XmlAdapter<?, ?>, Class<? extends XmlAdapter<?, ?>>> replacementAdapters = new HashMap<>();
		private Class<?>[] classesToBeBound = {};
		/**
		 * True writes, then reads. False just writes.
		 */
		private boolean roundTrip = true;

		public Builder() {
			super(new JAXBTestUtil<T>());
		}

		public Builder<T> adapter(XmlAdapter<?, ?>... adapters) {
			CollectionUtils.addAll(this.adapters, adapters);
			return this;
		}

		public Builder<T> adapter(Collection<XmlAdapter<?, ?>> adapters) {
			this.adapters.addAll(adapters);
			return this;
		}

		public Builder<T> adapter(XmlAdapter<?, ?> adapter, Class<? extends XmlAdapter<?, ?>> replacing) {
			replacementAdapters.put(adapter, replacing);
			return this;
		}

		public Builder<T> classesToBeBound(Class<?>... classes) {
			this.classesToBeBound = ArrayUtils.addAll(this.classesToBeBound, classes);
			return this;
		}

		public Builder<T> writeOnly() {
			this.roundTrip = false;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@SuppressWarnings("unchecked")
		@Override
		public JAXBTestUtil<T> build() {
			final XmlAdapter<?, ?>[] adapterArray = adapters.toArray(new XmlAdapter<?, ?>[] {});
			// marshal
			JAXBStringWriter.Builder writerBuilder = JAXBStringWriter.create(building.expected).adapter(adapterArray)
					.adapters(this.replacementAdapters);
			if (ArrayUtils.isNotEmpty(this.classesToBeBound)) {
				writerBuilder.classesBound(classesToBeBound);
			}
			building.xml = writerBuilder.build().toString();
			if (this.roundTrip) {
				// unmarshal
				JAXBStringReader.Builder<T> readerBuilder = (JAXBStringReader.Builder<T>) JAXBStringReader
						.create(building.xml, building.expected.getClass()).adapter(adapterArray);
				if (classesToBeBound != null) {
					readerBuilder.elementTypes(classesToBeBound);
				}
				building.actual = readerBuilder.build().getElement();
				assertNotNull(building.actual);
				assertEquals(building.expected.getClass(), building.actual.getClass());
			}
			return super.build();
		}

		public Builder<T> element(T element) {
			building.expected = element;
			classesToBeBound(JaxbUtil.boundTypeFrom(element));
			return this;
		}

	}// end Builder

	public static <T> Builder<T> create(T element) {
		return new Builder<T>().element(element);
	}

	private T actual;

	private T expected;

	private String xml;

	/** Use {@link Builder} to construct JAXBTestUtil */
	private JAXBTestUtil() {
	}

	public void assertContains(String target) {
		TestUtil.assertContains(xml, target);
	}

	/**
	 * @return the actual
	 */
	public T getActual() {
		return this.actual;
	}

	/**
	 * @return the expected
	 */
	public T getExpected() {
		return this.expected;
	}

	/**
	 * @return the xml
	 */
	public String getXml() {
		return this.xml;
	}

	@XmlRootElement(name = "simple")
	static class Simple
			extends OtherClass {
		@XmlValue
		public String contents;

	}

	@XmlTransient
	@XmlAccessorType(XmlAccessType.NONE)
	static public class OtherClass {

		public String other;

	}

	@XmlRootElement
	@XmlType
	public static class Junk {

		@XmlAttribute
		String field1;

		@XmlAttribute
		@XmlJavaTypeAdapter(JunkXmlAdapter.class)
		String adapted;

	}

	/**
	 * An adapter that demonstrates adaptation without complication.
	 * 
	 * Prepends a prefix during marshalling and removes it during unmarshalling.
	 * 
	 * @author Aaron Roller
	 * 
	 */
	public static class JunkXmlAdapter
			extends XmlAdapter<String, String> {

		static final String DEFAULT = "trash";
		String prefix = DEFAULT;

		/*
		 * (non-Javadoc)
		 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
		 */
		@Override
		public String unmarshal(String trash) throws Exception {
			TestUtil.assertContains(trash, prefix);
			return trash.replace(prefix, "");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
		 */
		@Override
		public String marshal(String original) throws Exception {

			return prefix + original;
		}

	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.NONE)
	public static class SingleValue {

		@XmlValue
		String value;

		/**
		 * @return the value
		 */
		public String getValue() {
			return this.value;
		}
	}

	public static class NoRoot {
		static final String ELEMENT_NAME = NoRoot.class.getSimpleName().toLowerCase();
	}

	@XmlRootElement
	public static class Parameterized<T> {
		@XmlElement
		T element;
	}
}
