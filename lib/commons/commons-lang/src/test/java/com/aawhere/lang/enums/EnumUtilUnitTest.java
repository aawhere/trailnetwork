/**
 *
 */
package com.aawhere.lang.enums;

import static com.aawhere.lang.enums.EnumUtil.*;
import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @see EnumUtil
 * @author Aaron Roller
 * 
 */
public class EnumUtilUnitTest {

	public enum Junk {
		A, B, C
	};

	/** Used for convert testing. */
	public enum LikeJunk {
		A, B;
	}

	@Test
	public void testValueOfFound() {
		Junk expected = Junk.A;
		assertEquals(expected, EnumUtil.valueOf(Junk.class, expected.name()));
	}

	@Test
	public void testValueOfNotFound() {
		assertNull(EnumUtil.valueOf(Junk.class, "you aint gonna find this"));
	}

	@Test
	public void testQualifiedName() {

		String actual = EnumUtil.qualifiedName(Junk.A);
		assertQualifiedNameForA(actual);
	}

	@Test
	public void enumClassFromContainer() {
		Class<? extends Enum> containerClass = Junk.class;
		Enum<?> enumType = Junk.A;

		Class<? extends Enum> enumClass = enumType.getClass();
		assertEquals(enumType, EnumUtil.valueOf(containerClass, enumType.name()));
		assertEquals(enumType, EnumUtil.valueOfEnum(enumClass, enumType.name()));
	}

	@Test
	public void testQualfiedNameForObject() {
		String actual = EnumUtil.qualifiedName("junk");
		assertEquals(String.class.getName(), actual);
	}

	@Test
	public void testQualifiedNameForEnumAsObject() {
		Object value = Junk.A;
		assertQualifiedNameForA(EnumUtil.qualifiedName(value));
	}

	@Test
	public void testGetPrevious() {
		Junk value = Junk.C;
		Junk previous = EnumUtil.getPrevious(value);
		assertEquals(Junk.B, previous);
	}

	/**
	 * What happens if we give the first enumeration in the list of values?
	 */
	@Test
	public void testGetPreviousFirst() {
		Junk value = Junk.A;
		Junk previous = EnumUtil.getPrevious(value);
		assertNull(previous);
	}

	@Test
	public void testGetNext() {
		Junk value = Junk.B;
		Junk next = EnumUtil.getNext(value);
		assertEquals(Junk.C, next);
	}

	/**
	 * What happens if we give the first enumeration in the list of values?
	 */
	@Test
	public void testGetNextLast() {
		Junk value = Junk.C;
		Junk next = EnumUtil.getNext(value);
		assertNull(next);
	}

	@Test
	public void testGetValues() {
		Junk[] actual = Junk.values();
		Junk[] values = EnumUtil.getValues(Junk.A);
		assertArrayEquals(values, actual);
	}

	/**
	 * @param actual
	 */
	private void assertQualifiedNameForA(String actual) {
		TestUtil.assertContains(actual, EnumUtilUnitTest.class.getName());
		TestUtil.assertContains(actual, Junk.class.getSimpleName());
		TestUtil.assertContains(actual, Junk.A.name());
	}

	@Test
	public void testConvert() {
		LikeJunk expected = LikeJunk.A;
		assertEquals("I want to be LikeJunk", expected, convert(Junk.A, LikeJunk.class));
	}

	@Test
	public void testConvertNull() {
		Junk junk = null;
		LikeJunk convert = convert(junk, LikeJunk.class);
		assertNull(convert);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConvertMissingName() {
		convert(Junk.C, LikeJunk.class);
		fail("nothing should have been created and expection thrown.");
	}
}
