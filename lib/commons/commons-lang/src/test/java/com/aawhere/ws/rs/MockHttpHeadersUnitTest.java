/**
 *
 */
package com.aawhere.ws.rs;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.collections.Index;

/**
 * @author Brian Chapman
 * 
 */
public class MockHttpHeadersUnitTest {

	@Test
	public void testAddHeader() {
		String headerName = "testHeader";
		String headerValue = "testHeaderValue";

		MockHttpHeaders headers = new MockHttpHeaders();
		headers.addHeader(headerName, headerValue);

		assertEquals(headerValue, headers.getRequestHeader(headerName).get(Index.FIRST.ordinal()));
	}

}
