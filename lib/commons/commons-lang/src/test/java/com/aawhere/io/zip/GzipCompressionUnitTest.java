/**
 * 
 */
package com.aawhere.io.zip;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @see GzipCompression
 * @author Aaron Roller
 * 
 */
public class GzipCompressionUnitTest {

	private String original;
	private byte[] originalBytes;
	private GzipCompression util;
	private byte[] compressed;
	private byte[] COMPRESSED_CORRUPT = { 31, -117, 8, 0, 0, 0, 0, 0, 0, 0 };

	@Before
	public void setUp() {
		original = RandomStringUtils.random(50, true, true);
		originalBytes = original.getBytes();
		util = new GzipCompression.Builder().build();
		compressed = util.compress(originalBytes);
	}

	@Test
	public void testCompressUncompress() throws CorruptGzipFormatException {

		// compress
		assertFalse(Arrays.equals(originalBytes, compressed));
		String compressString = new String(compressed);
		TestUtil.assertNotEquals(original, compressString);
		assertTrue(compressed.length > 0);

		// uncompress
		byte[] uncompressed = util.uncompress(compressed);
		assertTrue(Arrays.equals(originalBytes, uncompressed));
		String uncompressedString = new String(uncompressed);
		assertEquals(original, uncompressedString);

	}

	@Test
	public void testIsCompressed() {
		assertFalse(GzipCompression.isCompressed(originalBytes));
		assertTrue(GzipCompression.isCompressed(compressed));
	}

	/** compresing already compressed should be avoided. */
	@Test
	public void testCompressingCompressed() {
		byte[] doubleCompressed = util.compress(compressed);
		assertTrue(Arrays.equals(compressed, doubleCompressed));
	}

	@Test(expected = CorruptGzipFormatException.class)
	public void testUncompressingCorruptData() throws CorruptGzipFormatException {
		assertTrue(GzipCompression.isCompressed(COMPRESSED_CORRUPT));
		util.uncompress(COMPRESSED_CORRUPT);
	}
}
