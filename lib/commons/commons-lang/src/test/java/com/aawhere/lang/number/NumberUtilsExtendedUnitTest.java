package com.aawhere.lang.number;

import static com.aawhere.lang.number.NumberUtilsExtended.*;
import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 */

/**
 * Tests {@link NumberUtilsExtended}.
 * 
 * @author aroller
 * 
 */
public class NumberUtilsExtendedUnitTest {

	@Test
	public void testSplitWithOne() {
		assertJoinSplit(new Double[] { 3848.9892 });
	}

	@Test
	public void testSplitWithNone() {
		assertJoinSplit(new Double[] {});
	}

	@Test
	public void testSplitWithTwo() {
		Double[] expected = { 12345.789, 678.832 };

		assertJoinSplit(expected);
	}

	/**
	 * @param expected
	 */
	private void assertJoinSplit(Double[] expected) {
		Double[] actual = NumberUtilsExtended.split(NumberUtilsExtended.join(expected));
		assertEquals(expected.length, actual.length);
		for (int i = 0; i < actual.length; i++) {
			Double actualDouble = actual[i];
			Double expectedDouble = expected[i];
			assertEquals(expectedDouble, actualDouble);
		}
	}

	@Test
	public void testOddEven() {
		Integer odd = 3;
		Integer even = 4;
		assertTrue(isOdd(odd));
		assertTrue(isEven(even));
		assertFalse(isEven(odd));
		assertFalse(isOdd(even));
	}

}
