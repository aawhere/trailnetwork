/**
 * 
 */
package com.aawhere.collections;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.junit.BeforeClass;
import org.junit.Test;

import com.aawhere.util.CollectionUtilsExtended;

/**
 * @author aroller
 * 
 */
public class CollectionsUnitTest {

	static final List<String> singleEntryList = new LinkedList<String>();;

	@BeforeClass
	public static void setUpClass() {
		singleEntryList.add("bogus");

	}

	@Test
	public void testNonEmptyCollection() {

		assertEquals(singleEntryList.size(), CollectionUtilsExtended.emptyIfNull(singleEntryList).size());
	}

	@Test
	public void testNullCollection() {
		assertEquals(0, CollectionUtilsExtended.emptyIfNull(null).size());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testNewArrayList() {
		assertNewCollection(ArrayList.class);
	}

	@Test
	public void testNewList() {
		assertNewCollection(List.class);

	}

	/**
	 * @param expected
	 * @return
	 */
	private static <T extends Collection<O>, O> T assertNewCollection(Class<T> expected) {
		T actual = CollectionUtilsExtended.newCollection(expected);
		assertNotNull(actual);
		return actual;
	}

	@Test
	public void testNewSet() {
		assertNewCollection(Set.class);
	}

	@Test
	public void testNewSortedSet() {
		assertNewCollection(SortedSet.class);
	}

	@Test
	public void testNewHashSet() {
		assertNewCollection(HashSet.class);
	}

}
