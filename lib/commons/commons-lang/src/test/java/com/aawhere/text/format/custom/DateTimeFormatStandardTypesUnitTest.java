/**
 *
 */
package com.aawhere.text.format.custom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Ignore;
import org.junit.Test;

/**
 * 
 * @author Brian Chapman
 * 
 */
/* This is for information only, no need to include in maven's build. */
@Ignore
public class DateTimeFormatStandardTypesUnitTest {

	/*
	 * You'd think this would be out on the web. Perhaps I should blog it so it is.
	 */
	@Test
	public void testISODateTimeFormat() {
		Map<DateTimeFormatter, String> formatters = new HashMap<DateTimeFormatter, String>();
		List<String> implementedFormatter = new ArrayList<String>();
		implementedFormatter.add("sortableDateTime");
		implementedFormatter.add("mediumDate");
		implementedFormatter.add("shortDate");
		implementedFormatter.add("fullTime");
		implementedFormatter.add("mediumTime");
		implementedFormatter.add("mediumDateTime");
		implementedFormatter.add("shortDateTime");

		formatters.put(DateTimeFormat.fullDate(), "fullDate");
		formatters.put(DateTimeFormat.fullDateTime(), "fullDateTime");
		formatters.put(DateTimeFormat.fullTime(), "fullTime");
		formatters.put(DateTimeFormat.longDate(), "longDate");
		formatters.put(DateTimeFormat.longDateTime(), "longDateTime");
		formatters.put(DateTimeFormat.longTime(), "longTime");
		formatters.put(DateTimeFormat.mediumDate(), "mediumDate");
		formatters.put(DateTimeFormat.mediumDateTime(), "mediumDateTime");
		formatters.put(DateTimeFormat.mediumTime(), "mediumTime");
		formatters.put(DateTimeFormat.shortDate(), "shortDate");
		formatters.put(DateTimeFormat.shortDateTime(), "shortDateTime");
		formatters.put(DateTimeFormat.shortTime(), "shortTime");
		formatters.put(DateTimeFormat.forPattern("YYYY'-'MM'-'dd'T'HH':'mm':'ss.SSS"), "sortableDateTime");

		List<Locale> locals = new ArrayList<Locale>();
		locals.add(new Locale("en", "US"));
		locals.add(Locale.FRANCE);
		locals.add(new Locale("de", "DE"));
		locals.add(new Locale("el", "GR"));

		print(formatters, locals, implementedFormatter);
		print(" ");
		print("Confluence Output:");
		printConfluence(formatters, locals, implementedFormatter);
		print(" ");
		print("HTML Output:");
		printHtml(formatters, locals, implementedFormatter);
	}

	public void
			print(Map<DateTimeFormatter, String> formatters, List<Locale> locals, List<String> implementedFormatter) {

		for (Map.Entry<DateTimeFormatter, String> entry : formatters.entrySet()) {
			boolean implemented = false;
			for (Locale locale : locals) {
				if (implementedFormatter.contains(entry.getValue())) {
					implemented = true;
				}
				print(entry.getKey(), entry.getValue(), locale, implemented);
				implemented = false;
			}
			print(" ");
		}
	}

	public void printConfluence(Map<DateTimeFormatter, String> formatters, List<Locale> locals,
			List<String> implementedFormatter) {

		for (Map.Entry<DateTimeFormatter, String> entry : formatters.entrySet()) {
			boolean implemented = false;
			for (Locale locale : locals) {
				if (implementedFormatter.contains(entry.getValue())) {
					implemented = true;
				}
				printConfluenceTable(entry.getKey(), entry.getValue(), locale, implemented);
				implemented = false;
			}
			print("| | | |");
		}
	}

	public void printHtml(Map<DateTimeFormatter, String> formatters, List<Locale> locals,
			List<String> implementedFormatter) {

		for (Map.Entry<DateTimeFormatter, String> entry : formatters.entrySet()) {
			boolean implemented = false;
			for (Locale locale : locals) {
				if (implementedFormatter.contains(entry.getValue())) {
					implemented = true;
				}
				printHtmlTable(entry.getKey(), entry.getValue(), locale, implemented);
				implemented = false;
			}
		}
	}

	public void print(DateTimeFormatter formatter, String name, Locale locale, boolean implemented) {
		DateTime date = new DateTime();
		DateTimeFormatter fmt = formatter.withLocale(locale);
		StringBuilder str = new StringBuilder();
		str.append(name);
		str.append("(").append(fmt.getLocale().toString()).append(")");
		str.append(":\t");
		str.append(fmt.print(date));
		print(str.toString());
	}

	public void printConfluenceTable(DateTimeFormatter fmt, String name, Locale locale, boolean implemented) {
		DateTime date = new DateTime();
		fmt = fmt.withLocale(locale);
		StringBuilder str = new StringBuilder();
		str.append("|");
		if (implemented) {
			str.append("{color:#ff0000}");
		}
		str.append(name);
		if (implemented) {
			str.append("{color}");
		}
		str.append("|");
		str.append(locale.toString()).append("|");
		str.append(fmt.print(date)).append("|");
		print(str.toString());
	}

	public void printHtmlTable(DateTimeFormatter fmt, String name, Locale locale, boolean implemented) {
		DateTime date = new DateTime();
		fmt = fmt.withLocale(locale);
		StringBuilder str = new StringBuilder();
		str.append("<tr>");
		str.append("<td>").append(name).append("</td>");
		str.append("<td>").append(locale.toString()).append("</td>");
		str.append("<td>").append(fmt.print(date)).append("</td>");
		str.append("</tr>");
		print(str.toString());
	}

	public void print(String str) {
		System.out.println(str);
	}
}
