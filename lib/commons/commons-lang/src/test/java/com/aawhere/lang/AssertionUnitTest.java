/**
 * 
 */
package com.aawhere.lang;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * Tests {@link Assertion}.
 * 
 * @author Aaron Roller
 * 
 */
public class AssertionUnitTest {

	private static final String RIGHT = "right";
	private static final String LEFT = "left";
	private static final List<String> NOT_EMPTY = Arrays.asList("a", "b", "c");
	private static final List<String> EMPTY = Arrays.asList();

	@Test
	public void testEquals() {
		String test = "test";
		assertTrue(Assertion.b().eq(test, test));
	}

	public void testEqualsException() {

	}

	@Test
	public void testEqualsLeftNull() {
		assertFalse(Assertion.b().eq(null, RIGHT));
	}

	@Test
	public void testEqualsNulls() {
		assertTrue(Assertion.b().eq(null, null));
	}

	/**
	 * tests both the convenience method and subsequently the
	 * {@link Assertion#assertRange(String, Comparable, com.google.common.collect.Range)}
	 */
	@Test
	public void testRange() {
		int min = 0;
		int max = 1;
		assertFalse("below", Assertion.booleans().assertRange("below", -max, min, max));
		assertFalse("above", Assertion.booleans().assertRange("above", 2 * max, min, max));
		assertTrue("min", Assertion.booleans().assertRange("min", min, min, max));
		assertTrue("max", Assertion.booleans().assertRange("max", max, min, max));
	}

	@Test
	public void testSame() {
		assertTrue(Assertion.booleans().same("same", RIGHT, RIGHT));
		assertFalse(Assertion.booleans().same("not the same", RIGHT, LEFT));
	}

	@Test
	public void testEqualsNotNull() {
		assertTrue(Assertion.booleans().notNull(LEFT, LEFT));
		assertFalse(Assertion.booleans().notNull(LEFT, null));
		assertTrue(Assertion.exceptions().notNull(LEFT, LEFT));
		try {
			assertFalse(Assertion.exceptions().notNull(LEFT, null));
			fail("should have been null hard stop");
		} catch (Exception e) {
			// good
		}
	}

	@Test
	public void testEqualsRightNull() {
		assertFalse(Assertion.b().eq(LEFT, null));
	}

	@Test
	public void testNotEmptyWithNotEmpty() {
		Assertion.assertNotEmpty(NOT_EMPTY);
		// making it here is a happy thing
	}

	@Test(expected = RuntimeException.class)
	public void testNotEmptyWithEmpty() {
		Assertion.assertNotEmpty(EMPTY);
		fail("empty should throw exception");
	}

	@Test(expected = RuntimeException.class)
	public void testEmptyWithNotEmpty() {
		Assertion.exceptions().assertEmpty(NOT_EMPTY);
	}

	@Test
	public void testEmptyWithEmpty() {
		Assertion.exceptions().assertEmpty(EMPTY);
	}

	@Test
	public void testNotEquals() {
		assertFalse(Assertion.b().eq(LEFT, RIGHT));
	}

	@Test(expected = NullPointerException.class)
	public void testNullForNotNull() {
		// first, make sure not null is fine
		Class<AssertionUnitTest> expected = AssertionUnitTest.class;
		Class<AssertionUnitTest> actual = Assertion.assertNotNull("class", expected);
		assertEquals(expected, actual);
		// second, make sure null is not fine...throw expected
		Assertion.assertNotNull("supposed to be null", null);
	}

	@Test
	public void testNullForNull() {
		Assertion.exceptions().assertNull(null);

	}

	@Test
	public void testMinimumNotNull() {
		String one = "one";
		String two = null;
		String three = null;

		Assertion assertion = Assertion.booleans();
		assertTrue(assertion.assertMinimumNotNull(1, one, two, three));
		assertFalse(assertion.assertMinimumNotNull(1, two, three));
		assertFalse(assertion.assertMinimumNotNull(2, one, two, three));
		assertTrue(assertion.assertMinimumNotNull(1, one));
		assertFalse(assertion.assertMinimumNotNull(2, one));
		assertFalse(assertion.assertMinimumNotNull(3, one, two, three));
	}

	@Test(expected = RuntimeException.class)
	public void testNotNullForNull() {
		String expected = "junk";
		Assertion.exceptions().assertNull(expected);

	}

	@Test
	public void testTrue() {
		Assertion.exceptions().assertTrue(true);
		// good.
	}

	@Test(expected = RuntimeException.class)
	public void testFalse() {
		Assertion.exceptions().assertTrue(false);
	}

	@Test
	public void testBoolean() {
		assertTrue(Assertion.booleans().assertNull(null));
		assertFalse(Assertion.booleans().assertNull(getClass()));
	}

	@Test
	public void testEmptyString() {
		assertFalse(Assertion.booleans().assertNotEmpty(""));
		assertTrue(Assertion.booleans().assertNotEmpty("junk"));
		assertFalse(Assertion.booleans().assertNotEmpty((String) null));
	}
}
