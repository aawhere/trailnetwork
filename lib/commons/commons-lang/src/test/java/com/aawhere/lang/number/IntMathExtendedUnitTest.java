/**
 * 
 */
package com.aawhere.lang.number;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class IntMathExtendedUnitTest {

	/**
	 * 
	 */
	public static final Integer BIGGER = 2;
	public static final Integer SMALLER = 1;

	public static final Integer left = BIGGER;
	public static final Integer right = 1;
	public static final Integer ADDED = left + right;
	public static final Integer SUBTRACTED = left - right;

	@Test
	public void testBothNull() {
		assertNull(IntMathExtended.max(null, null));
	}

	@Test
	public void testLeftNull() {
		assertEquals(left, IntMathExtended.max(null, left));
	}

	@Test
	public void testRightNull() {
		assertEquals(right, IntMathExtended.max(right, null));
	}

	@Test
	public void testMax() {
		assertEquals(BIGGER, IntMathExtended.max(BIGGER, SMALLER));
		assertEquals(BIGGER, IntMathExtended.max(SMALLER, BIGGER));
	}

	@Test
	public void testMin() {
		assertEquals(SMALLER, IntMathExtended.min(BIGGER, SMALLER));
		assertEquals(SMALLER, IntMathExtended.min(SMALLER, BIGGER));
	}

	@Test
	public void testAdd() {
		assertEquals(ADDED, IntMathExtended.add(left, right));
		assertEquals(ADDED, IntMathExtended.add(right, left));
	}

	@Test
	public void testSubtracted() {
		assertEquals(SUBTRACTED, IntMathExtended.subtract(left, right));
	}
}
