/**
 * 
 */
package com.aawhere.collections;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class ArrayUtilsExtendedUnitTest {

	@Test
	public void testSplitEmptyArray() {
		Character[] array = ArrayUtils.EMPTY_CHARACTER_OBJECT_ARRAY;
		int maxLength = 10;
		int expectedNumberOfSegments = 0;
		int expectedSizeOfLast = 0;
		assertSplit(array, maxLength, expectedNumberOfSegments, expectedSizeOfLast);

	}

	/**
	 * @param array
	 * @param maxLength
	 * @param expectedNumberOfSegments
	 * @param expectedSizeOfLast
	 */
	private void assertSplit(Character[] array, int maxLength, int expectedNumberOfSegments, int expectedSizeOfLast) {
		List<Character[]> segments = ArrayUtilsExtended.split(array, maxLength);
		int numberOfSegments = segments.size();
		assertEquals("number of segments", expectedNumberOfSegments, segments.size());
		for (int i = 0; i < numberOfSegments; i++) {
			Character[] characters = segments.get(i);
			if (i < numberOfSegments - 1) {
				assertEquals("length of segment " + i, maxLength, characters.length);
			} else {
				assertEquals("length of last segment " + i, expectedSizeOfLast, characters.length);
			}
		}
	}

	@Test
	public void testSplitOneArrayOneElement() {
		assertSplit(new Character[1], 10, 1, 1);
	}

	@Test
	public void testSplitOneArrayFull() {
		assertSplit(new Character[10], 10, 1, 10);
	}

	@Test
	public void testSplitTwoArrayOneInSecond() {
		assertSplit(new Character[11], 10, 2, 1);
	}

	@Test
	public void testSplitTwoArraysFullSecond() {
		assertSplit(new Character[20], 10, 2, 10);
	}

	@Test
	public void testThreeArraysOneInSecond() {
		assertSplit(new Character[21], 10, 3, 1);
	}

	@Test
	public void testThreeArraysFullSecond() {
		assertSplit(new Character[30], 10, 3, 10);
	}

}
