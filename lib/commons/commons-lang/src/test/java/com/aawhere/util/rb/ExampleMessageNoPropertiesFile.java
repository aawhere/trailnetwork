/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Oct 31, 2010
commons-lang : com.aawhere.util.rb.ExampleMessageNoPropertiesFile.java
 */
package com.aawhere.util.rb;

import com.aawhere.text.format.custom.CustomFormat;

/**
 * @author roller
 * 
 */
public enum ExampleMessageNoPropertiesFile implements Message {

	/** Explain_Entry_Here */
	ENTRY1(MessageResourceBundleUnitTest.MESSAGE_IN_FILE_VALUE + "{FIRST_PARAM}", Param.FIRST_PARAM);

	private ParamKey[] parameterKeys;
	private String message;

	private ExampleMessageNoPropertiesFile(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** EXPLAIN_PARAM_HERE */
		FIRST_PARAM;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
