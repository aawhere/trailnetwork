/**
 *
 */
package com.aawhere.collections;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * @author Brian Chapman
 * 
 */
public class ListUtilsExtendedUnitTest {
	static final String FIRST = "a";
	static final String SECOND = "b";
	static final String THIRD = "c";

	List<String> listOf1 = Lists.newArrayList(FIRST);
	List<String> listOf2 = Lists.newArrayList(FIRST, SECOND);
	List<String> listOf3 = Lists.newArrayList(FIRST, SECOND, THIRD);

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testHasIndex() {
		Boolean hasIndex = ListUtilsExtended.hasIndex(listOf3, Index.FIRST.index);
		assertTrue(hasIndex);
		Boolean hasThirdIndex = ListUtilsExtended.hasIndex(listOf3, Index.THIRD.index);
		assertTrue(hasThirdIndex);
		Boolean doesntHaveIndex = ListUtilsExtended.hasIndex(listOf3, Index.FOURTH.index);
		assertFalse(doesntHaveIndex);
		Boolean hasNegativeIndex = ListUtilsExtended.hasIndex(listOf3, Index.NONE.index);
		assertFalse(hasNegativeIndex);
	}

	@Test
	public void testFullList() {
		List<String> list = CollectionsUnitTest.singleEntryList;
		assertEquals(list.size(), ListUtilsExtended.emptyIfNull(list).size());
	}

	@Test
	public void testNullList() {
		assertEquals(0, ListUtilsExtended.emptyIfNull(null).size());
	}

	@Test
	public void testSecondToLast() {
		assertEquals(null, ListUtilsExtended.secondToLastOrNull(listOf1));
		assertEquals(FIRST, ListUtilsExtended.secondToLastOrNull(listOf2));
		assertEquals(SECOND, ListUtilsExtended.secondToLastOrNull(listOf3));
	}

	public void testTransformWithoutNulls() {
		List<String> input = Lists.newArrayList("a", "b", null);
		List<String> transformed = ListUtilsExtended.transformWithoutNulls(input, new Function<String, String>() {

			@Override
			public String apply(String input) {
				return input;
			}

		});

		for (String item : transformed) {
			assertNotNull(item);
		}
	}
}
