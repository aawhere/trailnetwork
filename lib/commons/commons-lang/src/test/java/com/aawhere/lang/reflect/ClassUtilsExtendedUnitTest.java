/**
 *
 */
package com.aawhere.lang.reflect;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;

import org.junit.Test;
import org.reflections.ReflectionUtils;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.test.TestUtil;

/**
 * @author Aaron Roller
 * 
 */
public class ClassUtilsExtendedUnitTest {

	/**
	 * 
	 */
	private static final String INCEST_FIELD = "incestField";
	/**
	 * 
	 */
	private static final String GRAND_PARENT_FIELD = "grandParentField";
	/**
	 * 
	 */
	private static final String PARENT_FIELD = "parentField";
	/**
	 * 
	 */
	private static final String INTEGER_FIELD = "integerField";
	/**
	 * 
	 */
	private static final String STRING_FIELD = "stringField";

	@Test
	public void testSubclassSearchFromChild() {
		Class<?> subclass = ClassUtilsExtended.getInstance().climbSupersForSubclass(Child.class, GrandParent.class);
		assertEquals(Parent.class, subclass);
	}

	@Test
	public void testSubclassSearchFromParent() {

		Class<?> subclass = ClassUtilsExtended.getInstance().climbSupersForSubclass(Parent.class, GrandParent.class);
		assertEquals(Parent.class, subclass);
	}

	@Test(expected = RuntimeException.class)
	public void testSubclassSearchFromGrandParent() {
		Class<?> subclass = ClassUtilsExtended.getInstance().climbSupersForSubclass(GrandParent.class,
																					GrandParent.class);
		assertEquals(Parent.class, subclass);

	}

	@Test
	public void testGetBuilder() throws ClassNotFoundException {
		Class<?> builderType = ClassUtilsExtended.getInstance().getBuilderClass(BuilderParent.class);
		assertEquals(BuilderParent.Builder.class, builderType);
	}

	@Test
	public void testFindFieldWithOne() {
		Set<Field> stringFields = ClassUtilsExtended.getInstance().findFieldByType(Child.class, String.class);
		assertEquals(stringFields.toString(), 1, stringFields.size());
		assertEquals(STRING_FIELD, stringFields.iterator().next().getName());

	}

	@Test
	public void testFindFieldWithTwo() {
		Set<Field> fields = ClassUtilsExtended.getInstance().findFieldByType(Child.class, Integer.class);
		assertEquals(fields.toString(), 2, fields.size());
		TestUtil.assertStartsWith(fields.iterator().next().getName(), INTEGER_FIELD);

	}

	@Test
	public void testGetFieldInChild() {
		Field field = ReflectionUtilsExtended.getField(Child.class, STRING_FIELD);
		assertEquals(STRING_FIELD, field.getName());
	}

	@Test
	public void testGetFieldFromGrandParent() {
		String fieldName = GRAND_PARENT_FIELD;
		Field field = ReflectionUtilsExtended.getField(Child.class, fieldName);
		assertEquals(fieldName, field.getName());
	}

	@Test
	public void testGetFieldFromParent() {
		String fieldName = PARENT_FIELD;
		Field field = ReflectionUtilsExtended.getField(Child.class, fieldName);
		assertEquals(fieldName, field.getName());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetIncestField() {
		String fieldName = INCEST_FIELD;
		Field field = ReflectionUtilsExtended.getField(Child.class, fieldName);
		assertEquals(fieldName, field.getName());
	}

	@Test
	public void testTypeForMethod() {
		assertEquals(Long.class, typeForMethod("longMethod"));
	}

	@Test
	public void testTypeForVoidMethod() throws ClassNotFoundException {
		String methodName = "voidMethod";
		Class<?> type = typeForMethod(methodName);
		assertEquals(Void.TYPE, type);
	}

	/**
	 * @param methodName
	 * @return
	 */
	private Class<?> typeForMethod(String methodName) {
		Set<Method> allMethods = ReflectionUtils.getAllMethods(Child.class, ReflectionUtils.withName(methodName));
		assertEquals(1, allMethods.size());
		Class<?> type = ReflectionUtilsExtended.typeFor(allMethods.iterator().next());
		return type;
	}

	@Test
	public void testTypeForField() throws SecurityException, NoSuchFieldException {
		Field field = ReflectionUtilsExtended.getField(GrandParent.class, GRAND_PARENT_FIELD);
		Class<Long> type = ReflectionUtilsExtended.typeFor(field);
		assertEquals(Long.class, type);
	}

	@Test
	public void testGetPackages() {
		Class<?>[] classes = { String.class, ClassUtilsExtended.class };
		String[] expected = { String.class.getPackage().getName(), ClassUtilsExtended.class.getPackage().getName() };
		String[] actual = ClassUtilsExtended.getPackages(classes);
		assertNotNull(actual);
		assertEquals(expected, actual);
	}

	public static class GrandParent {
		private Long grandParentField;
		private Long incestField;
	}

	public static class Parent
			extends GrandParent {
		private Long parentField;
		private Long incestField;
	}

	public static class Child
			extends Parent {
		private String stringField;
		private Integer integerField1;
		private Integer integerField2;
		private Long incestField;

		public void voidMethod() {
		}

		public Long longMethod() {
			return 3l;
		}
	}

	public static class BuilderParent {
		public static class Builder
				extends ObjectBuilder<BuilderParent> {

			/**
			 * @param building
			 */
			protected Builder(BuilderParent building) {
				super(building);
				// TODO Auto-generated constructor stub
			}
		}
	}

}
