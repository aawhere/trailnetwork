/**
 * 
 */
package com.aawhere.lang.exception;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.net.HttpStatusCode;

/**
 * @author aroller
 * 
 */
public class ExceptionUtilUnitTest {

	@Test
	public void testStatusCodeFromAnnotation() {
		HttpStatusCode actual = ExceptionUtil.statusCodeFromAnnotation(new ExampleException());
		assertEquals(ExampleException.STATUS_CODE, actual);
	}

	@Test
	public void testStatusCodeNoAnnotation() {
		assertNull(ExceptionUtil.statusCodeFromAnnotation(new RuntimeException()));
	}

	@Test
	public void testStatusCodeFromResponse() {
		ExampleException example = new ExampleException();

		HttpStatusCode expected = HttpStatusCode.CONFLICT;
		example.setStatusCode(expected);
		HttpStatusCode actual = ExceptionUtil.responseCode(example);
		assertEquals("provided status is not being returned", expected, actual);

	}

	@Test
	public void testNullStatusCodeFromResponse() {
		ExampleException example = new ExampleException();
		example.setStatusCode(null);
		assertNotNull("Example has an annotation", example.getStatusCode());

	}

	@Test
	public void testNullStatusCodeNoAnnotation() {
		BaseException exception = new BaseException() {
			private static final long serialVersionUID = 1L;
		};
		assertNull("no status set, no annotation, no code", exception.getStatusCode());
		assertNotNull(	"the util guarantees no null...should provide default internal",
						ExceptionUtil.responseCode(exception));
	}

}
