/**
 * 
 */
package com.aawhere.net;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.i18n.LocaleTestUtil;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.test.TestUtil;

/**
 * @see HttpStatusCode
 * @see GeneralHttpException
 * @see NetMessage
 * 
 * @author Aaron Roller
 * 
 */
public class HttpStatusCodeUnitTest {

	/**
	 * 
	 */
	private static final String EXPECTED_MESSAGE_FOR_NOT_FOUND = "found";

	@Test
	public void testValueOf() {
		HttpStatusCode expected = HttpStatusCode.CLIENT_TIMEOUT;
		assertEquals(expected, HttpStatusCode.valueOf(expected.value));
		assertNull(HttpStatusCode.valueOf(-1));
	}

	@Test
	public void testGeneralExceptionNotTranslated() {
		HttpStatusCode status = HttpStatusCode.BAD_GATEWAY;
		GeneralHttpException exception = new GeneralHttpException(status);
		TestUtil.assertContains(exception.getMessage(), String.valueOf(status.name()));
	}

	@Test
	public void testGeneralExceptionTranslated() {
		HttpStatusCode status = HttpStatusCode.NOT_FOUND;
		GeneralHttpException exception = new GeneralHttpException(status);

		final String partOfExpectedMessageInEnglish = EXPECTED_MESSAGE_FOR_NOT_FOUND;
		TestUtil.assertContains(exception.getMessage(), partOfExpectedMessageInEnglish);
	}

	@Test
	public void testFormatMessageProvided() {
		HttpStatusCustomFormat format = new HttpStatusCustomFormat();
		final HttpStatusCode status = HttpStatusCode.NOT_FOUND;
		String formattedMessage = format.format(status, LocaleTestUtil.DEFAULT_LOCALE);
		TestUtil.assertContains(formattedMessage, EXPECTED_MESSAGE_FOR_NOT_FOUND);
		assertMessageProvided(formattedMessage, status);

	}

	/**
	 * Makes sure the formatted message doesn't contain the name of the enum since the message
	 * should be more natural language appropriate.
	 */
	public static void assertMessageProvided(String formattedMessage, HttpStatusCode status) {
		TestUtil.assertDoesntContain(formattedMessage, status.name());
	}

	public static void assertStatus(BaseException exception) {

	}

	@Test
	public void testFormatMessageNotProvided() {
		HttpStatusCustomFormat format = new HttpStatusCustomFormat();
		final HttpStatusCode status = HttpStatusCode.BAD_GATEWAY;
		String formattedMessage = format.format(status, LocaleTestUtil.DEFAULT_LOCALE);
		TestUtil.assertContains(formattedMessage, status.name());

	}
}
