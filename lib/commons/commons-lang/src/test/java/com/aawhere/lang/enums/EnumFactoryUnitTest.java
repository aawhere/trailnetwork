/**
 * 
 */
package com.aawhere.lang.enums;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author roller
 * 
 */
public class EnumFactoryUnitTest {

	private static final String TEST_ENUM_A = "TestEnum.A";

	static enum TestEnum {
		A, B, C
	};

	static interface Example {
	}

	static enum ExampleEnum implements Example {
		D, E, F
	};

	EnumFactory<Object> factory;

	@Before
	public void setUp() {
		factory = EnumFactory.create().setEnumInterfaceType(Object.class).build();
	}

	@Test
	public void testKeyForEnum() throws Exception {
		String expected = TEST_ENUM_A;
		TestEnum expectedEnum = TestEnum.A;
		String actual = factory.keyForEnum(expectedEnum);
		assertEquals(expected, actual);
		assertTrue(factory.enumExists(expected));
		TestEnum actualEnum = factory.enumFor(expected);
		assertEquals(expectedEnum, actualEnum);
	}

	@Test
	public void testKeyForObject() {
		assertNull(factory.keyForObject("junk"));
	}

	@Test(expected = InvalidEnumKeyException.class)
	public void testKeyForUnregistered() throws Exception {
		factory.register(TestEnum.class);
		final String key = "junk";
		assertFalse(factory.enumExists(key));
		try {
			factory.enumFor(key);
		} catch (InvalidEnumKeyException e) {
			TestUtil.assertContains(e.getMessage(), TEST_ENUM_A);
			TestUtil.assertContains(e.getMessage(), key);
			throw (e);
		}
	}

	@Test
	public void testRegistration() {
		assertTrue("supposed to indicate that registration occurred", factory.register(TestEnum.class));
		assertFalse("registration shouldn't happen twice", factory.register(TestEnum.class));
		assertEquals(TestEnum.C, factory.enumFor(factory.keyForEnum(TestEnum.C)));
	}

	@Test
	public void testEnumWithInterface() {
		EnumFactory<Example> exampleFactory = new EnumFactory.Builder<Example>().setEnumInterfaceType(Example.class)
				.build();
		ExampleEnum exampleEnum = ExampleEnum.D;
		String key = exampleFactory.keyForEnum(exampleEnum);
		ExampleEnum actualEnum = exampleFactory.enumFor(key);
		assertEquals(exampleEnum, actualEnum);

	}

	@Test
	public void testKeyIsRecognized() {
		String key = factory.keyForEnum(TestEnum.A);
		assertTrue(key + " should be recognized", factory.keyIsRecognized(key));
		assertFalse(factory.keyIsRecognized("blah"));
	}

}
