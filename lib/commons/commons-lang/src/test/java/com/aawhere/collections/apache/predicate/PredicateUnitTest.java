/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 4, 2010
commons-lang : com.aawhere.collections.apache.functor.PredicateUnitTest.java
 */
package com.aawhere.collections.apache.predicate;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author roller
 * 
 */
public class PredicateUnitTest {

	private static final Integer MIN = 1;
	private static final Integer MIDDLE = 5;

	private static final Integer MAX = 10;

	@Test
	public void testLessThan() {
		LessThanPredicate predicate = new LessThanPredicate(MIDDLE);
		assertTrue(predicate.evaluate(MIN));
		assertFalse(predicate.evaluate(MIDDLE));
		assertFalse(predicate.evaluate(MAX));
	}

	@Test
	public void testLessThanEqualTo() {
		LessThanEqualToPredicate predicate = new LessThanEqualToPredicate(MIDDLE);
		assertTrue(predicate.evaluate(MIN));
		assertTrue(predicate.evaluate(MIDDLE));
		assertFalse(predicate.evaluate(MAX));
	}

	@Test
	public void testGreaterThan() {
		GreaterThanPredicate predicate = new GreaterThanPredicate(MIDDLE);
		assertFalse(predicate.evaluate(MIN));
		assertFalse(predicate.evaluate(MIDDLE));
		assertTrue(predicate.evaluate(MAX));
	}

	@Test
	public void testGreaterThanEqualTo() {
		GreaterThanEqualToPredicate predicate = new GreaterThanEqualToPredicate(MIDDLE);
		assertFalse(predicate.evaluate(MIN));
		assertTrue(predicate.evaluate(MIDDLE));
		assertTrue(predicate.evaluate(MAX));
	}

}
