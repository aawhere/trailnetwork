/**
 * 
 */
package com.aawhere.xml.bind;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.test.TestUtil;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author aroller
 * 
 */
public class OptionalXmlAdapterFactoryUnitTest {
	/**
	 * 
	 */
	public static final String OPTION1 = "option1";
	public static final String OPTION2 = "option2";
	static XmlAdapter<String, byte[]> adapter1 = new HexBinaryAdapter();
	static XmlAdapter<String, String> adapter2 = new NormalizedStringAdapter();
	private static final Class<NormalizedStringAdapter> ADAPTER2_TYPE = NormalizedStringAdapter.class;
	static XmlAdapter<String, String> adapter2Replacement = new JAXBTestUtil.JunkXmlAdapter();
	OptionalXmlAdapterFactory.Builder builder;
	final ArrayList<String> registeredOptions = Lists.newArrayList();
	final List<XmlAdapter<?, ?>> registeredAdapters = new ArrayList<XmlAdapter<?, ?>>();
	OptionalXmlAdapterFactory factory;
	HashMap<String, XmlAdapter<?, ?>> notExpected = Maps.newHashMap();
	HashMap<XmlAdapter<?, ?>, XmlAdapter<?, ?>> expectedReplacements = Maps.newHashMap();

	@Before
	public void setUp() {
		builder = OptionalXmlAdapterFactory.create();
	}

	@After
	public void test() {
		factory = builder.build();
		List<XmlAdapter<?, ?>> adapters = factory.adapter(registeredOptions);
		TestUtil.assertIterablesEquals("adapter not available after registration", this.registeredAdapters, adapters);

		// when the test is expecting not to receive an adapter
		if (!notExpected.keySet().isEmpty()) {
			List<XmlAdapter<?, ?>> adaptersNotExpected = this.factory.adapter(Lists.newArrayList(notExpected.keySet()));
			TestUtil.assertEmpty("these aren't expected to be found", adaptersNotExpected);
		}

		Set<Entry<XmlAdapter<?, ?>, XmlAdapter<?, ?>>> entrySet = this.expectedReplacements.entrySet();
		for (Entry<XmlAdapter<?, ?>, XmlAdapter<?, ?>> entry : entrySet) {
			Class<?> type = factory.type(entry.getValue());
			assertEquals("replacement didn't happen", type, entry.getKey().getClass());
		}
	}

	@Test
	public void testOneRegistered() {
		register(OPTION1, adapter1);

	}

	@Test
	public void testTwoRegistered() {
		register(OPTION1, adapter1);
		register(OPTION2, adapter2);
	}

	@Test
	public void testAlways() {
		// purposely avoiding registered option indicating an expection that the adapter should be
		// available regardless of the options requested
		builder.register(OptionalXmlAdapterFactory.ALWAYS, adapter1);
		this.registeredAdapters.add(adapter1);
	}

	/**
	 * a keyword option is nice if we can provide a single "option" to represent a common request in
	 * the api.
	 * 
	 * option=account provides AccountIdXmlAdapter and AccountIdsXmlAdapter for person to display
	 * all accounts
	 */
	@Test
	public void testRegisterTwoForSameOption() {
		register(OPTION2, adapter2);
		register(OPTION2, adapter2Replacement);

	}

	@Test
	public void testReplacement() {
		replacement(OPTION2, adapter2Replacement, ADAPTER2_TYPE);
		this.expectedReplacements.put(adapter2, adapter2Replacement);
	}

	@Test
	public void testRequestOfNonRegistered() {
		this.notExpected.put(OPTION1, adapter1);
	}

	private void replacement(String option, XmlAdapter<?, ?> replacement, Class<? extends XmlAdapter<?, ?>> type) {
		builder.register(option, replacement, type);
		expected(option, replacement);
	}

	/**
	 * @param option
	 * @param replacement
	 */
	private void expected(String option, XmlAdapter<?, ?> replacement) {
		this.registeredOptions.add(option);
		this.registeredAdapters.add(replacement);
	}

	/**
	 * 
	 */
	private void register(String option, XmlAdapter<?, ?> adapter) {
		builder.register(option, adapter);
		expected(option, adapter);
	}
}
