/**
 * 
 */
package com.aawhere.collections.apache;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Aaron Roller
 * 
 */
public class ListOrderedSetExtendedUnitTest {

	private String first;
	private String second;
	private ListOrderedSetExtended.Builder<String> setBuilder;

	@Before
	public void setUp() {
		first = new String("junk");
		second = new String(first.toString());
		setBuilder = new ListOrderedSetExtended.Builder<String>();
	}

	@Test
	public void testGet() {
		assertNull(setBuilder.add(first));
		assertEquals(first, setBuilder.build().get(first));
	}

	@Test
	public void testAdd() {

		String firstAdd = setBuilder.add(first);
		assertNull(firstAdd);
		String secondAdd = setBuilder.add(second);
		assertNotNull(secondAdd);
		assertSame(first, secondAdd);
	}

	@Test
	public void testDefault() {
		ListOrderedSetExtended<String> los = new ListOrderedSetExtended.Builder<String>().build();
		Set<String> set = los.asSet();

		assertNotNull(set);
		assertTrue(set.isEmpty());

		List<String> list = los.asList();
		assertNotNull(list);
		assertTrue(list.isEmpty());
	}
}
