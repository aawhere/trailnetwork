package com.aawhere.test;

import static com.aawhere.test.TestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.lang.enums.EnumUtilUnitTest;
import com.aawhere.lang.enums.EnumUtilUnitTest.Junk;
import com.aawhere.test.category.ManualTest;

import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * Verifies the {@link TestUtil} is generating random values as expected.
 * 
 * @author roller
 * 
 */
public class TestUtilUnitTest {

	@Test
	public void testRandomDouble() {
		Double value = generateRandomDouble();
		assertNotNull(value);
	}

	@Test
	public void testRandomDoubleRange() {
		// half avoids overflow
		final double half = Double.MAX_VALUE / 2;
		double min = generateRandomDouble(0, half);
		double max = min + generateRandomDouble(0, half);
		double value = generateRandomDouble(min, max);
		assertTrue(value + " < " + min, value >= min);
		assertTrue(value + " > " + max, value <= max);
	}

	@Test
	public void testRandomDoubleRangeAtLeast() {
		final Range<Double> range = Range.atLeast(3.0);
		assertDouble(range);
	}

	/**
	 * @param range
	 */
	private void assertDouble(final Range<Double> range) {
		Double d = d(range);
		assertRange(range, d);
	}

	@Test
	public void testRandomDoubleRangeAtMost() {
		final Range<Double> range = Range.atMost(4.0);
		assertDouble(range);
	}

	@Test
	public void testRandomDoubleRangeBetween() {
		final Range<Double> range = Range.closed(0.0, 5.0);
		assertDouble(range);
	}

	@Test
	public void testRandomNegativeDoubleRangeBetween() {
		final Range<Double> range = Range.closed(-5.0, -0.0);
		assertDouble(range);
	}

	@Test
	public void testRandomLetter() {
		Character letter = generateRandomLetter();
		assertTrue(letter.toString(), Character.isLetter(letter));

	}

	@Test
	public void testRandomCharacter() {
		assertNotNull(generateRandomCharacter());
	}

	/** makes sure min is less than max */
	@Test(expected = RuntimeException.class)
	public void testRandomDoubleRangeMismatch() {
		double min = 1;
		double max = .5;
		generateRandomDouble(min, max);
	}

	@Test
	public void testRandomInt() {
		int max = 50;
		int result = TestUtil.generateRandomInt(max);
		assertTrue(result + " greater than " + max, result <= max);
	}

	@Test
	public void testRandomString() {
		int length = 33;
		String result = TestUtil.generateRandomStringOfLength(length);
		assertEquals(length, result.length());

	}

	@Test
	public void testRandomStringArray() {
		String[] result = TestUtil.generateRandomStringArray();
		assertTrue(result.length > 0);
		for (String string : result) {
			assertNotNull(string);

		}
	}

	/**
	 * Basically tests {@link TestUtil#generateRandomBoolean()} with enough attempts to ensure a
	 * true.
	 * 
	 */
	@Test
	public void testRandomBoolean() {

		// one flaw in this...doesn't ensure there are many falses, only trues.
		int howManyTries = 10;
		boolean result = false;
		for (int i = 0; i < howManyTries; i++) {
			result |= TestUtil.generateRandomBoolean();
		}
		assertTrue("Seriously, out of " + howManyTries
				+ " not a single true?  That's not random enough, but statistically possible.", result);
	}

	@Test
	public void testContains() {
		assertContains("you betcha", "betcha");
		// can't test the sad case because the test will fail
		// assertContains("you betcha", "doghnut");
	}

	@Test
	public void testContainsCollection() {
		String expected = "blah";
		assertContains(expected, Lists.newArrayList("yup", expected, "doh"));
		// assertContains(expected,Lists.newArrayList());
	}

	@Test
	public void testCollectionEquals() {
		String first = "first";
		String second = "second";
		String third = "third";

		assertCollectionEquals(Lists.newArrayList(first, second, third), Sets.newHashSet(third, first, second));
	}

	@Test
	public void testEndsWith() {
		assertEndsWith("you betcha", "betcha");
		// assertEndsWith("you betcha", "doghnut");
	}

	@Test
	public void testStartsWith() {
		assertStartsWith("you betcha", "you");
		// assertStartsWith("you betcha", "doghnut");
	}

	@Test
	public void testDoesntContain() {
		assertDoesntContain("you betcha", "doghnut");
		// can't test the sad case because the test will fail
		// assertDoesntContain("you betcha", "you");
	}

	@Test
	public void testEquality() {
		final String expected = new String("expected");
		assertEquality(expected, new String(expected), new String("notEqual"));
		// enable to test same check assertEquality(expected, expected, new String("notEqual"));
	}

	@Test
	public void testGreaterThan() {
		assertGreaterThan(null, 6, 7);
		// assertGreaterThan(8, 8);
	}

	@Test
	public void testLessThan() {
		assertLessThan(null, 5, 4);
		// assertLessThan(null, 5, 5);
		// assertLessThan(null, 5, 6);
	}

	@Test
	public void testDateTimeEquals() {
		DateTime expected = new DateTime();
		DateTime actual = new DateTime(expected.getMillis() + 10L);
		assertDateTimeEquals(expected, actual, 11L);
		assertDateTimeEquals(expected, actual, 10L);
		try {
			assertDateTimeEquals(expected, actual, 1L);
			fail("times were outside the tolorance.");
		} catch (AssertionError e) {
			// Expected outcome.
		}

	}

	@Test
	public void testInstanceOf() {
		Integer actual = new Integer(3);
		TestUtil.assertInstanceOf(Number.class, actual);
		TestUtil.assertInstanceOf(Integer.class, actual);
		// TestUtil.assertInstanceOf(Long.class, actual);
	}

	@Test
	public void testSizeRange() {
		ArrayList<String> list = Lists.newArrayList("a", "b", "c");
		assertSize(Range.atLeast(3), list);
		assertSize(Range.closed(0, 3), list);
		assertSize(Range.open(0, 4), list);
	}

	@Test
	public void testOrdered() {
		ArrayList<Integer> ordered = Lists.newArrayList(1, 2, 3);
		TestUtil.assertOrdered(ordered);
	}

	@Test
	public void testReverseOrdered() {
		ArrayList<Integer> ordered = Lists.newArrayList(3, 2, 1);
		final Ordering<Integer> ordering = Ordering.natural().reverse();
		TestUtil.assertOrdered(ordered, ordering);
	}

	/** It's expected to fail to confirm assert works. */
	@Test
	@Category(ManualTest.class)
	@Ignore("run this manually and remove ignore to do so")
	public void testIcorrectOrdered() {
		ArrayList<Integer> ordered = Lists.newArrayList(1, 3, 2);
		TestUtil.assertOrdered(ordered);
	}

	@Test
	public void testRandomEnumRange() {
		Class<Junk> type = EnumUtilUnitTest.Junk.class;
		Range<Integer> lessThan1 = Range.lessThan(1);
		TestUtil.assertSize(0, TestUtil.generateRandomEnums(type, lessThan1));
		Range<Integer> few = Range.open(1, 10);
		TestUtil.assertSize(few, generateRandomEnums(type, few));
	}
}
