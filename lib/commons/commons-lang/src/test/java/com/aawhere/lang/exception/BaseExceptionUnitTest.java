/**
 * 
 */
package com.aawhere.lang.exception;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.net.HttpStatusCode;
import com.aawhere.test.TestUtil;

/**
 * 
 * @author roller
 * 
 */
public class BaseExceptionUnitTest {

	@Test
	public void testEmptyConstructor() {
		ExampleException exception = new ExampleException();
		assertNull(exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testMessageConstructor() {
		String message = TestUtil.generateRandomString();
		ExampleException exception = new ExampleException(message);
		assertEquals(message, exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testExceptionConstructor() {
		String causeMessage = "Cause";
		ExampleException cause = new ExampleException(causeMessage);
		ExampleException exception = new ExampleException(cause);
		assertEquals(cause, exception.getCause());
		assertEquals(causeMessage, exception.getMessage());
	}

	@Test
	public void testStatusCode() {
		assertEquals(ExampleException.STATUS_CODE, new ExampleException().getStatusCode());
	}

	@Test
	public void testToRuntimeExceptionStatusCodeFromAnnotation() {
		final ExampleException exampleException = new ExampleException();
		ToRuntimeException toRuntimeException = new ToRuntimeException(exampleException);
		final HttpStatusCode expectedStatusCode = ExampleException.STATUS_CODE;
		assertEquals(expectedStatusCode, toRuntimeException.getStatusCode());
		assertEquals(exampleException.getStatusCode(), toRuntimeException.getStatusCode());

	}

	@Test
	public void testToRuntimeExceptionStatusCodeFromMethodOverride() {
		ExampleException exampleException = new ExampleException();
		final HttpStatusCode override = HttpStatusCode.ACCEPTED;
		exampleException.setStatusCode(override);
		ToRuntimeException toRuntimeException = new ToRuntimeException(exampleException);
		assertEquals(override, toRuntimeException.getStatusCode());
	}
}
