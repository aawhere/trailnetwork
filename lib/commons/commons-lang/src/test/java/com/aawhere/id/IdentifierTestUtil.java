/**
 *
 */
package com.aawhere.id;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.test.TestUtil;
import com.aawhere.xml.XmlNamespace;

/**
 * Useful methods for Identifiers.
 * 
 * @author Aaron Roller
 * 
 */
public class IdentifierTestUtil {

	public static <KindT, I extends Identifier<?, KindT>> I generateRandomLongId(Class<KindT> kindType,
			Class<I> identifierType) {
		return IdentifierUtils.idFrom(TestUtil.generateRandomLong(), identifierType);
	}

	public static ChildId generateRandomChildId() {
		return new ChildId(TestUtil.generateRandomAlphaNumeric());
	}

	public static List<ChildId> generateRandomChilIds() {

		int total = TestUtil.generateRandomInt(1, 10);
		ArrayList<ChildId> list = new ArrayList<IdentifierTestUtil.ChildId>(total);
		for (int i = 0; i < total; i++) {
			list.add(generateRandomChildId());
		}
		return list;
	}

	/**
	 * Creates a single id that is built from two others using standard
	 * {@link IdentifierUtils#createCompositeIdValue(Identifier, Identifier)}.
	 * 
	 * @return
	 */
	public static ChildId generateRandomCompositeId() {
		return new ChildId(IdentifierUtils.createCompositeIdValue(generateRandomChildId(), generateRandomChildId()));
	}

	public static Child generateRandomChild() {
		return new Child(generateRandomChildId());
	}

	public static List<Child> generateRandomChildren() {
		List<ChildId> ids = generateRandomChilIds();
		ArrayList<Child> children = new ArrayList<Child>(ids.size());
		for (ChildId childId : ids) {
			children.add(new Child(childId));
		}
		return children;
	}

	/**
	 * Demonstrates how a base class can take out the value requirements from a child, but allow a
	 * child to declare it's own.
	 * 
	 * @author Aaron Roller
	 * 
	 * @param <KindT>
	 *            The identifiable that a child will provide. The Child should declare itself.
	 */
	static abstract public class Base<KindT, IdValueT extends Comparable<IdValueT>, I extends Identifier<IdValueT, KindT>>
			implements Identifiable<I> {

		@SuppressWarnings("unchecked")
		public Class<KindT> getKind() {
			return (Class<KindT>) getClass();
		}

	}

	/**
	 * Demonstrates the implementation of a Base providing itself as the KindT
	 * 
	 * @author Aaron Roller
	 * 
	 */
	@XmlRootElement
	static public class Child
			extends Base<Child, String, ChildId> {

		/**
		 *
		 */
		public Child(ChildId id) {
			this.childId = id;
			this.xmlId = id;
		}

		public Child() {
		}

		@XmlElement
		ChildId childId;

		@XmlElement(name = "id", namespace = XmlNamespace.W3C_XML_1998_VALUE)
		ChildId xmlId;

		public void validate() {
			ChildId id = getId();
			assertNotNull(id);
			String idValue = getId().getValue();
			assertNotNull(idValue);
			Class<Child> kind = getId().getKind();
			assertNotNull(kind);

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.id.IdentifierTestUtil.Base#getId()
		 */
		@Override
		public ChildId getId() {
			return this.childId;
		}
	}

	/**
	 * Shows an optional way of providing Typed Identifiers by creating an actual class. This will
	 * equal a generic provided Identifier so it is considered only a convenience, not a
	 * requirement.
	 * 
	 * @see IdentifierUnitTest#testEqualityOfConvenience()
	 */
	@XmlType(name = ChildId.DOMAIN)
	@XmlRootElement
	static public class ChildId
			extends StringIdentifier<Child> {
		static public final String DOMAIN = "childId";
		private static final long serialVersionUID = 2215046361899323036L;

		/**
		 * Shows an optional way of providing Typed Identifiers by creating an actual class.
		 * 
		 * @param value
		 * @param kind
		 */
		public ChildId(String value) {
			super(value, Child.class);

		}

		/**
		 *
		 */
		public ChildId() {
			super(Child.class);
		}

	}

	public static class ChildIdWithParent
			extends StringIdentifierWithParent<Child, ChildIdWithParent> {

		private static final long serialVersionUID = 80159306418182732L;

		public ChildIdWithParent(String value, ChildIdWithParent parent) {
			super(delegateBuilder(ChildIdWithParent.class).kind(Child.class).parentNotRequired().value(value)
					.parent(parent).build());
		}

		/** no parent with given value */
		public ChildIdWithParent(String value) {
			this(value, null);
		}

		/** random id with the parent given. */
		public ChildIdWithParent(ChildIdWithParent parent) {
			this(TestUtil.generateRandomAlphaNumeric(), parent);
		}

		/** Random id value. no parent. */
		public ChildIdWithParent() {
			this((ChildIdWithParent) null);
		}

	}

	static public class LongChild
			extends Base<LongChild, Long, LongChildId> {
		private LongChildId id;

		public LongChild(LongChildId id) {
			this.id = id;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.id.Identifiable#getId()
		 */
		@Override
		public LongChildId getId() {
			return this.id;
		}
	}

	static public class LongChildId
			extends LongIdentifier<LongChild> {

		private static final long serialVersionUID = 1L;

		/**
		 * 
		 */
		LongChildId() {
			super(LongChild.class);

		}

		public LongChildId(Integer value) {
			this((long) value);
		}

		/**
		 * @param value
		 * @param kind
		 */
		public LongChildId(Long value) {
			super(value, LongChild.class);
		}

		/**
		 * @param value
		 * @param kind
		 */
		public LongChildId(String value) {
			super(value, LongChild.class);
		}

	}

	/**
	 * Iterates each identifable given to make sure it's corresponding id is available in the ids
	 * list.
	 * 
	 * @param idsFrom
	 * @param children
	 */
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> void assertIds(Collection<I> ids,
			Collection<T> identifiables) {
		for (T identifiable : identifiables) {
			final I id = identifiable.getId();
			assertTrue(id.toString() + " not found", ids.contains(id));
		}

	}

	/**
	 * @param expectedIds
	 */
	public static <C extends Comparable<C>> List<C> values(Identifier<C, ?>[] expectedIds) {
		ArrayList<C> values = new ArrayList<>();
		for (int i = 0; i < expectedIds.length; i++) {
			Identifier<C, ?> identifier = expectedIds[i];
			values.add(identifier.getValue());
		}
		return values;
	}

	/**
	 * @param identifiable
	 * @param identifier
	 * @return
	 */
	public static <T extends Identifiable<I>, I extends Identifier<?, T>> Set<I> generateRandomLongIds(
			Class<T> identifiable, Class<I> identifier) {
		int count = TestUtil.generateRandomInt(0, 10);
		HashSet<I> ids = new HashSet<>();
		for (int i = 0; i < count; i++) {
			ids.add(generateRandomLongId(identifiable, identifier));

		}
		return ids;
	}

	public static class ChildIds
			extends Identifiers<ChildId, Child> {

		private static final long serialVersionUID = -8653290955504152536L;

		protected ChildIds() {
			super();
		}

		/**
		 * @param identifierString
		 */
		public ChildIds(String identifierString) {
			super(identifierString);
		}

	}
}
