/**
 * 
 */
package com.aawhere.lang.string;

import static com.aawhere.test.TestUtil.*;
import static org.junit.Assert.*;

import java.util.SortedSet;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.aawhere.lang.string.CharacterIndexes.CharacterIndex;

/**
 * Tests {@link CharacterIndexes}.
 * 
 * @author roller
 * 
 */
public class CharacterIndexUnitTest {

	@Test
	public void testBasics() {

		String string = generateRandomStringOfLength(10);

		int max = string.length() - 1;
		int index1Max = max / 2;
		Integer index1 = generateRandomInt(index1Max);
		Integer index2 = generateRandomInt(index1Max, max);
		Character char1 = string.charAt(index1);
		Character char2 = string.charAt(index2);
		CharacterIndexes indexes = new CharacterIndexes.Builder(string).add(char1, index1).add(char2, index2).build();
		assertNotNull(indexes);
		SortedSet<CharacterIndex> all = indexes.getAll();
		assertNotNull(all);
		assertEquals(2, all.size());

		String toString = indexes.toString();
		// only test toString is working if therew were any chars
		// just wanna make sure we are getting the dilimeter
		assertTrue(toString, string.isEmpty() || toString.contains(CharacterIndexes.CharacterIndex.TO_STRING_DILIMETER));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIllegalIndex() {
		String string = generateRandomString();
		new CharacterIndexes.Builder(string).add('c', string.length()).build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDuplicate() {
		int max = 10;
		String string = generateRandomStringOfLength(max);
		Integer index = generateRandomInt(max);
		char char1 = '@';
		char char2 = '$';
		try {

			new CharacterIndexes.Builder(string).add(char1, index).add(char2, index).build();
		} catch (IllegalArgumentException e) {
			String message = e.getMessage();
			assertTrue(message, StringUtils.contains(message, char1));
			assertTrue(message, StringUtils.contains(message, char2));
			assertTrue(message, StringUtils.contains(message, String.valueOf(index)));
			throw (e);
		}
	}

}
