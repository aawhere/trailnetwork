/**
 *
 */
package com.aawhere.lang.string;

import static com.aawhere.lang.string.StringUtilsExtended.*;
import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author Aaron Roller
 * 
 */
public class StringUtilsExtendedUnitTest {

	@Test
	public void testCamelCaseToConstant() {
		String camelCase = "thisIsMyWordAndTLA";
		String result = StringUtilsExtended.camelCaseToConstant(camelCase);
		assertEquals("THIS_IS_MY_WORD_AND_TLA", result);
	}

	@Test
	public void testToConstant() {
		String result = StringUtilsExtended.toConstantCase("HOW", "About", "this", "tlaExample");
		assertEquals("HOW_ABOUT_THIS_TLA_EXAMPLE", result);
	}

	@Test
	public void testToConstantCaseDuelingHyphensAndCamelCase() {
		String hyphens = "first-second-word";
		String camelCase = "firstSecond-word";
		String fromCamelCase = StringUtilsExtended.toConstantCaseExtraWordSeparation(camelCase);
		String fromHyphens = StringUtilsExtended.toConstantCaseExtraWordSeparation(hyphens);
		TestUtil.assertNotEquals(fromCamelCase, fromHyphens);
		assertEquals("FIRST__SECOND__WORD", fromHyphens);
		assertEquals("FIRST_SECOND__WORD", fromCamelCase);

	}

	@Test
	public void testToConstantFromHyphens() {
		String result = StringUtilsExtended.toConstantCase("activitiesImport-processorVersion");
		assertEquals("ACTIVITIES_IMPORT_PROCESSOR_VERSION", result);
	}

	@Test
	public void testCamelCaseToHyphenated() {
		String camelCase = "ThisIsMyWordAndTLA";
		String result = StringUtilsExtended.camelCaseToHyphenated(camelCase);
		assertEquals("this-is-my-word-and-tla", result);
	}

	@Test
	public void testAccessor() {
		String methodName = "getForgetThis";
		String expected = "forgetThis";
		assertAccessor(methodName, expected);
		assertAccessor("isForgetThis", expected);
		assertAccessor("isIsForgetThis", "isForgetThis");
	}

	@Test
	public void testNonAccessor() {
		assertNonAccessor("forgetThis");
		assertNonAccessor("ishmael");
		assertNonAccessor("getty");

	}

	@Test
	public void testStripMultipleSpaces() {
		String actual = "Hey   you ".trim().replaceAll(" +", " ");
		assertEquals("Hey you", actual);
	}

	@Test
	public void testQuotesOnlyEmpty() {
		assertTrue(StringUtilsExtended.isEmpty("\"\""));
	}

	@Test
	public void testCondenseSpaces() {
		assertEquals("Spaces in the middle", spacesCondensedIntoOne(" Spaces in  the   middle "));
	}

	@Test
	public void testNullEmpty() {
		assertTrue(StringUtilsExtended.isEmpty(null));
	}

	@Test
	public void testEmptyQuotes() {
		assertTrue(StringUtilsExtended.isEmpty(""));
	}

	@Test
	public void testToTitleCaseWords() {
		String[] words = { "camel", "Hippo", "ELEPHANT" };
		String titleCase = StringUtilsExtended.toTitleCase(words);
		assertEquals("CamelHippoElephant", titleCase);
	}

	@Test
	public void testToCamelCaseWords() {
		String[] words = { "camel", "Hippo", "ELEPHANT" };
		String titleCase = StringUtilsExtended.toCamelCase(words);
		assertEquals("camelHippoElephant", titleCase);
	}

	@Test
	public void testConstantCaseToCamelCase() {
		String input = "A_BIG_HIPPO";
		String expected = "aBigHippo";
		assertEquals(expected, StringUtilsExtended.constantCaseToCamelCase(input));
	}

	@Test
	public void testConstantCaseToHyphenated() {
		String input = "A_BIG_HIPPO";
		String expected = "a-big-hippo";
		assertEquals(expected, StringUtilsExtended.constantCaseToHyphenated(input));
	}

	private void assertNonAccessor(String methodName) {
		assertAccessor(methodName, methodName);
	}

	/**
	 * @param methodName
	 */
	private void assertAccessor(String methodName, String expected) {
		String actual = StringUtilsExtended.accessorToProperty(methodName);
		assertEquals(expected, actual);
	}
}
