/**
 * 
 */
package com.aawhere.util.rb;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * Tests {@link MessageFormatter}.
 * 
 * @author roller
 * 
 */
public class MessageFormatterUnitTest {

	/**
	 * @param message
	 * @param locale
	 * @param expectedFormattedValue
	 */
	public static void assertMessageFormatter(final Message message, final Locale locale,
			final String expectedFormattedValue) {
		MessageFormatter formatter = new MessageFormatter.Builder().message(message).locale(locale).build();
		assertEquals(expectedFormattedValue, formatter.getFormattedMessage());
	}

	@Test
	public void testEnum() throws Exception {
		final ExampleMessage message = ExampleMessage.MESSAGE_IN_FILE;
		final Locale locale = Locale.US;
		final String expectedFormattedValue = MessageResourceBundleUnitTest.MESSAGE_IN_FILE_VALUE;
		assertMessageFormatter(message, locale, expectedFormattedValue);

	}

	@Test
	public void testEnumLanguage() {
		assertMessageFormatter(	ExampleMessage.MESSAGE_IN_FILE,
								MessageResourceBundleUnitTest.TEST_LOCALE,
								MessageResourceBundleUnitTest.MESSAGE_IN_FILE_VALUE_ZU);
	}

	/**
	 * Tests if a value will be taken from the properties file from an alternate language that does
	 * not have the property in the file.
	 * 
	 */

	@Test
	public void testEnumLanguageProvidedInFileForZuluAndEnumNotDefaultFile() {
		assertMessageFormatter(	ExampleMessage.ZULU_OVERRIDES_ENUM,
								MessageResourceBundleUnitTest.TEST_LOCALE,
								MessageResourceBundleUnitTest.ONLY_IN_ZULU);
	}

	@Test
	public void testString() throws Exception {
		String messageValue = TestUtil.generateRandomString();
		Message expected = new StringMessage(messageValue);
		assertMessageFormatter(expected, Locale.US, messageValue);
	}
}
