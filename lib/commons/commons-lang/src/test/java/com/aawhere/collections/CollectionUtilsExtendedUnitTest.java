/**
 * 
 */
package com.aawhere.collections;

import static com.aawhere.collections.CollectionUtilsExtended.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;

import com.aawhere.test.TestUtil;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author Brian Chapman
 * 
 */
public class CollectionUtilsExtendedUnitTest {

	private final String FIRST = "a";
	private final String MIDDLE = "b";
	private final String LAST = "c";
	private static final Integer ONE = 1;
	private static final Integer TWO = 2;
	private static final Integer THREE = 3;

	@Test
	public void testLastOrNull() {
		List<String> list = new ArrayList<String>();
		list.add(FIRST);
		list.add(MIDDLE);
		list.add(LAST);

		TreeSet<String> sorted = new TreeSet<String>();
		sorted.add(LAST);
		sorted.add(FIRST);
		sorted.add(MIDDLE);

		assertEquals(LAST, CollectionUtilsExtended.lastOrNull(list));
		assertEquals(LAST, CollectionUtilsExtended.lastOrNull(sorted));

	}

	@Test
	public void testSwapKeys() {
		HashMap<Integer, String> keyKeeper = Maps.newHashMap();
		keyKeeper.put(ONE, FIRST);
		HashMap<String, Integer> valueProvider = Maps.newHashMap();
		valueProvider.put(FIRST, THREE);

		Map<Integer, Integer> actual = CollectionUtilsExtended.swapKeys(keyKeeper, valueProvider);
		assertEquals(THREE, actual.get(ONE));
	}

	/**
	 * The same bimap should produce equal values of the keys. A simple test, but should be thorough
	 * enough.
	 */
	@Test
	public void testTransformKeysForSameBiMap() {
		HashBiMap<String, Integer> map = HashBiMap.create();
		map.put(FIRST, ONE);
		map.put(MIDDLE, TWO);
		map.put(LAST, THREE);

		Map<Integer, Integer> transformed = CollectionUtilsExtended.transformKeys(map, map);
		Set<Entry<Integer, Integer>> entrySet = transformed.entrySet();
		for (Entry<Integer, Integer> entry : entrySet) {
			final String message = String.valueOf(entry);
			assertEquals(message, entry.getKey(), entry.getValue());
			assertEquals(message, entry.getValue(), transformed.get(entry.getKey()));
			assertTrue(message, transformed.containsValue(entry.getValue()));
			assertTrue(message, transformed.containsKey(entry.getKey()));
		}

	}

	/** Proves that the map will ignore when extra keys are given that don't match any values. */
	@Test
	public void testTransformKeysBadKeyMap() {
		HashBiMap<String, Integer> keys = HashBiMap.create();
		keys.put(FIRST, ONE);
		final HashMap<String, String> map = Maps.newHashMap();
		Map<Integer, String> transformKeys = CollectionUtilsExtended.transformKeys(map, keys);
		assertTrue(transformKeys.isEmpty());
		assertTrue(transformKeys.entrySet().isEmpty());
		TestUtil.assertEmpty(transformKeys.keySet());
	}

	@Test
	public void testToCollectionAlreadyCollection() {
		ArrayList<String> list = Lists.newArrayList(FIRST, MIDDLE, LAST);
		assertSame("no need to change, just cast", list, toCollection(list));
	}

	@Test
	public void testIterableToCollection() {
		final ArrayList<String> expected = Lists.newArrayList(FIRST, null, MIDDLE, LAST);

		Collection<String> collection = toCollection(new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				return expected.iterator();
			}
		});
		assertNotSame("a new list must have been created", expected, collection);
		assertTrue(expected + " != " + collection, CollectionUtils.isEqualCollection(expected, collection));
	}
}
