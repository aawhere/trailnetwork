/**
 * 
 */
package com.aawhere.style;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class AlphaChannelUnitTest {

	@Test
	public void testFloatConversion() {
		assertFloatConversion(AlphaChannel.OPAQUE);
		assertFloatConversion(AlphaChannel.ALMOST_OPAQUE);
		assertFloatConversion(AlphaChannel.HALF);
		assertFloatConversion(AlphaChannel.ALMOST_TRANSPARENT);
		assertFloatConversion(AlphaChannel.TRANSPARENT);
	}

	@Test
	public void testZeroTo255Conversion() {
		assertZeroTo255Conversion(AlphaChannel.OPAQUE);
		assertZeroTo255Conversion(AlphaChannel.ALMOST_OPAQUE);
		assertZeroTo255Conversion(AlphaChannel.HALF);
		assertZeroTo255Conversion(AlphaChannel.ALMOST_TRANSPARENT);
		assertZeroTo255Conversion(AlphaChannel.TRANSPARENT);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIntegerOutOfRangeBelow() {
		AlphaChannel.create().setZeroTo255(AlphaChannel.MIN - 1).build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIntegerOutOfRangeAbove() {
		AlphaChannel.create().setZeroTo255(AlphaChannel.MAX + 1).build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFloatOutOfRangeBelow() {
		// something about the math rounds micro numbers to zero, but that's probably a good thing.
		AlphaChannel.create().setZeroToOne(AlphaChannel.TRANSPARENT.zeroTo255() - 0.01f).build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFloatOutOfRangeAbove() {
		AlphaChannel.create().setZeroToOne(AlphaChannel.OPAQUE.zeroTo255() + Float.MIN_VALUE).build();
	}

	@Test
	public void testColorAlphaAssignment() {
		Color color = Color.white;
		AlphaChannel expected = AlphaChannel.HALF;
		AlphaChannel original = AlphaChannel.create(color).build();
		TestUtil.assertNotEquals("proving they are orignally different", original, expected);
		Color modifiedColor = expected.apply(color);
		AlphaChannel actual = AlphaChannel.create(modifiedColor).build();
		assertEquals("color didn't receive proper alpha", expected, actual);
	}

	/**
	 * @param expected
	 */
	private void assertFloatConversion(AlphaChannel expected) {
		AlphaChannel actual = AlphaChannel.create().setZeroToOne(expected.zeroToOne()).build();
		assertEquals("float conversion didn't work", expected, actual);
	}

	private void assertZeroTo255Conversion(AlphaChannel expected) {
		AlphaChannel actual = AlphaChannel.create().setZeroTo255(expected.zeroTo255()).build();
		assertEquals("int conversion didn't work", expected, actual);
	}
}
