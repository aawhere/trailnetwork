/**
 *
 */
package com.aawhere.id;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.i18n.LocaleTestUtil;
import com.aawhere.id.IdentifierTestUtil.Child;
import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.id.IdentifierTestUtil.ChildIdWithParent;
import com.aawhere.id.IdentifierTestUtil.LongChildId;
import com.aawhere.test.TestUtil;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.text.format.custom.CustomFormatFactory;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Tests the basic class {@link Identifier} and it's simple implementations {@link BaseIdentifier}
 * and it's common children.
 * 
 * @author roller
 * 
 */
public class IdentifierUnitTest {

	public static <ValueT extends Comparable<ValueT>, KindT> void assertIdentifier(Identifier<ValueT, KindT> id,
			ValueT value) {
		assertNotNull(id.getValue());
		assertEquals(id, id);
		assertEquals(id.hashCode(), id.hashCode());
		assertEquals(value, id.getValue());
		String string = id.toString();
		assertNotNull(string);
		assertTrue(string, string.contains(id.getClass().getSimpleName()));
	}

	@Test
	public void testEquality() {
		Integer value = TestUtil.generateRandomInt();
		LongChildId left = new LongChildId(value);
		LongChildId right = new LongChildId(value);
		assertEquals(left, right);
	}

	@Test
	public void testValueInequality() {
		Integer value = TestUtil.generateRandomInt();
		LongChildId left = new LongChildId(value);
		LongChildId right = new LongChildId(value + 1);
		TestUtil.assertNotEquals(left, right);
	}

	@Test
	public void testKindInequality() {
		Long value = TestUtil.generateRandomLong();
		LongChildId left = new LongChildId(value);
		LongIdentifier<IdentifierUnitTest> right = new LongIdentifier<IdentifierUnitTest>(value,
				IdentifierUnitTest.class) {
			private static final long serialVersionUID = 1L;
		};
		TestUtil.assertNotEquals(left, right);
	}

	@Test
	public void testEqualityOfConvenience() {
		String value = TestUtil.generateRandomString();
		Identifier<String, Child> generic = new ChildId(value);
		ChildId childId = new ChildId(value);
		assertEquals(generic, childId);
		assertEquals(childId, childId);
	}

	@Test
	public void testComparable() {
		Integer leftValue = TestUtil.generateRandomInt();
		LongChildId left = new LongChildId(leftValue);
		Integer rightValue = leftValue + 1;
		LongChildId right = new LongChildId(rightValue);
		assertEquals(leftValue.compareTo(rightValue), left.compareTo(right));
		assertEquals(rightValue.compareTo(leftValue), right.compareTo(left));

	}

	@Test
	public void testChildIdJaxb() {

		String value = "test";
		ChildId id = new ChildId(value);
		JAXBTestUtil<ChildId> util = JAXBTestUtil.create(id).build();
		util.assertContains(id.getValue().toString());
		util.assertContains("childId");
	}

	@Test
	public void testChildJaxb() {

		String value = "test";
		ChildId id = new ChildId(value);
		Child child = new Child();
		child.childId = id;
		child.xmlId = id;
		JAXBTestUtil<Child> util = JAXBTestUtil.create(child).build();
		util.assertContains(id.getValue().toString());
		util.assertContains("childId");
		util.assertContains("xml:id");
		Child actual = util.getActual();
		assertEquals(child.childId, actual.childId);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCustomFormat() {
		IdentifierCustomFormat.register();
		final String expected = "boo";
		ChildId id = new ChildId(expected);

		@SuppressWarnings("rawtypes")
		CustomFormat format = CustomFormatFactory.getInstance().getFormat(ChildId.class);
		assertEquals(IdentifierCustomFormat.class, format.getClass());
		assertEquals(expected, format.format(id, LocaleTestUtil.DEFAULT_LOCALE));
	}

	@Test
	public void testParentEquals() {
		ChildIdWithParent parent = new ChildIdWithParent();
		ChildIdWithParent child1 = new ChildIdWithParent(parent);
		ChildIdWithParent child2 = new ChildIdWithParent(child1.getValue(), parent);
		assertEquals(child1, child2);
	}

	@Test
	public void testParentEqualsValuesDont() {
		ChildIdWithParent parent = new ChildIdWithParent();
		ChildIdWithParent child1 = new ChildIdWithParent(parent);
		ChildIdWithParent child2 = new ChildIdWithParent(parent);
		TestUtil.assertNotEquals("different values", child1, child2);
	}

	@Test
	public void testParentNotEqualValuesEqual() {
		ChildIdWithParent child1 = new ChildIdWithParent();
		ChildIdWithParent child2 = new ChildIdWithParent(child1.getValue(), new ChildIdWithParent());
		TestUtil.assertNotEquals("same values, different parents", child1, child2);
	}

	@Test
	public void testValuesEqualOtherParentNull() {
		ChildIdWithParent child1 = new ChildIdWithParent(new ChildIdWithParent());
		ChildIdWithParent child2 = new ChildIdWithParent(child1.getValue());
		TestUtil.assertNotEquals("same values, child1 has parent, child 2 does not", child1, child2);
	}

	@Test
	public void testValuesEqualThisParentNull() {
		ChildIdWithParent child1 = new ChildIdWithParent();
		ChildIdWithParent child2 = new ChildIdWithParent(child1.getValue(), new ChildIdWithParent());
		TestUtil.assertNotEquals("same values, child1 has no parent, child2 does", child1, child2);
	}

	@Test
	public void testToStringWithAncestors() {
		final String rootValue = "root";
		final String middleValue1 = "middle1";
		final String middleValue2 = "middle2";
		final String middleValue3 = "middle3";
		final String leafValue = "leaf";
		ChildIdWithParent root = new ChildIdWithParent(rootValue);
		ChildIdWithParent middle1 = new ChildIdWithParent(middleValue1, root);
		ChildIdWithParent middle2 = new ChildIdWithParent(middleValue2, middle1);
		ChildIdWithParent middle3 = new ChildIdWithParent(middleValue3, middle2);
		ChildIdWithParent leaf = new ChildIdWithParent(leafValue, middle3);
		String expected = IdentifierUtils.createCompositeIdValue(	rootValue,
																	middleValue1,
																	middleValue2,
																	middleValue3,
																	leafValue);
		assertEquals("incorrect toString()", expected, leaf.toString());
	}
}
