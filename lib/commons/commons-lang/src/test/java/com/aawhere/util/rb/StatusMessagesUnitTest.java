/**
 * 
 */
package com.aawhere.util.rb;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Tests the functionality of {@link StatusMessages}.
 * 
 * @author aroller
 * 
 */
public class StatusMessagesUnitTest {

	CompleteMessage message1;
	CompleteMessage message2;
	StatusMessages statusMessages;

	@Before
	public void setUp() {

		message1 = MessageResourceBundleUnitTest.getExampleFloatMessage();
		message2 = MessageResourceBundleUnitTest.getExampleDoubleMessage();
		statusMessages = new StatusMessages.Builder().addMessage(MessageStatus.OK, message1)
				.addMessage(MessageStatus.ERROR, message2).build();
	}

	@Test
	public void testSingleStatus() {
		StatusMessages.Builder builder = new StatusMessages.Builder();
		builder.addMessage(MessageStatus.OK, message1);
		builder.addMessage(MessageStatus.OK, message2);
		StatusMessages statusMessages = builder.build();
		List<StatusMessage> messages = statusMessages.getMessages();
		assertEquals("Should only be OK messages", 2, messages.size());
		Collection<StatusMessage> okMessages = statusMessages.getMessagesByStatus(MessageStatus.OK);
		assertEquals(2, okMessages.size());
		assertTrue(statusMessages.has(MessageStatus.OK));
		assertFalse(statusMessages.hasError());

	}

	public void testDoesntHave() {
		StatusMessages messages = new StatusMessages.Builder().build();
		assertFalse(messages.has(MessageStatus.OK));
		assertFalse(messages.hasError());
	}

	@Test
	public void testMultipleStatus() {
		Collection<StatusMessage> okMessages = statusMessages.getMessagesByStatus(MessageStatus.OK);
		assertEquals(1, okMessages.size());
		assertEquals(message1.toString(), okMessages.iterator().next().message.toString());
		Collection<StatusMessage> errorMessages = statusMessages.getMessagesByStatus(MessageStatus.ERROR);
		assertEquals(1, errorMessages.size());
		assertEquals(message2.toString(), errorMessages.iterator().next().message.toString());
		TestUtil.assertEmpty(statusMessages.getMessagesByStatus(MessageStatus.WARNING));
		assertTrue(statusMessages.has(MessageStatus.ERROR));
		assertTrue(statusMessages.hasError());

	}

	@Test
	public void testJaxb() {
		JAXBTestUtil<StatusMessages> util = JAXBTestUtil.create(statusMessages).adapter().build();
		util.assertContains(MessageStatus.OK.getValue());
		util.assertContains(MessageStatus.ERROR.getValue());
	}

	@Test
	@Ignore("No longer supporting complete message as a root since it is being adapted.")
	public void testCompleteMessageJaxb() {
		ExampleMessage message = ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE;
		JAXBTestUtil<CompleteMessage> util = JAXBTestUtil.create(new CompleteMessage.Builder(message).build()).build();
		util.assertContains(message.getValue());
	}

}
