package com.aawhere.test;

import static org.junit.Assert.*;

import java.io.File;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Ignore;

import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

/**
 * Used for helping tests do the basic stuff.
 * 
 * @author roller
 * 
 * 
 */
@Ignore("This is just a utility to be used by tests and should not be tested itself.")
public class TestUtil {

	private static final Double REASONABLE_PRECISION = 0.00001;
	/**
	 * If files are to be written out during testing them must go to this directory or one of it's
	 * sub-directories.
	 * 
	 */
	public static final File TEMP_DIR = new File("target");

	public static Double generateRandomDouble() {
		return Math.random();
	}

	/**
	 * Given the min and the max the double value will be generated within the range.
	 * 
	 * @param min
	 * @param max
	 */
	public static Double generateRandomDouble(double min, double max) {
		if (min >= max) {
			throw new RuntimeException(min + " must be less than " + max);
		}
		// if dealing with negatives then grow in the direction of the min
		double multiplier;
		if (max < 0) {
			multiplier = min;
		} else {
			multiplier = max;
		}
		double value;

		do {
			double seed = Math.random();
			value = seed * multiplier;
		} while (value < min);

		return value;
	}

	public static <C extends Comparable<C>> void assertRange(Range<C> expected, C actual) {
		assertRange("", expected, actual);
	}

	/** Simple assertion to apply the expected range to the actual value. */
	public static <C extends Comparable<C>> void assertRange(String message, Range<C> expected, C actual) {
		assertTrue(actual + " not within " + expected + " " + message, expected.apply(actual));
	}

	public static Integer generateRandomInt(int max) {
		return generateRandomInt(Integer.MIN_VALUE, max);
	}

	/** Give me the type of enum and i'll give you back one of them at random */
	public static <E extends Enum<E>> E generateRandomEnum(Class<E> type) {
		E[] enumConstants = type.getEnumConstants();
		int index = generateRandomInt(0, enumConstants.length - 1);
		return enumConstants[index];
	}

	/**
	 * Produces random enumerations until the number of enumerations to be returned satisfies the
	 * range.
	 * 
	 * FIXME: make the count returned random within the range. It's currently the same as passing an
	 * integer
	 */
	public static <E extends Enum<E>> List<E> generateRandomEnums(Class<E> type, Range<Integer> range) {
		LinkedList<E> list = new LinkedList<E>();
		while (!range.apply(list.size())) {
			list.add(generateRandomEnum(type));
		}
		return list;
	}

	public static Integer generateRandomInt(int min, int max) {

		return generateRandomDouble(min, max).intValue();
	}

	/**
	 * Generate a valid Id for use with Entities identifier
	 * 
	 * @param max
	 * @return
	 */
	public static Long generateRandomId(int max) {
		// Note that 0 is an invalid Id.
		return (long) generateRandomInt(max) + 1;
	}

	/**
	 * Generate a valid Id for use with Entities identifier
	 * 
	 * @return
	 */
	public static Long generateRandomId() {
		// Note that some Ids are created as a composite of two, so Long.MAX_VALUE/2 is the largest
		// theoretical value we can have for those entities.
		return generateRandomId(Integer.MAX_VALUE / 4);
	}

	/**
	 * Generate a valid Id for use with Entities identifier
	 * 
	 * @return
	 */
	public static String generateRandomIdAsString() {
		return String.valueOf(generateRandomId());
	}

	/**
	 * @param max
	 * @return
	 */
	public static Long generateRandomLong(long max) {

		return (long) (generateRandomDouble() * max);
	}

	public static Long generateRandomLong() {
		return generateRandomLong(Long.MAX_VALUE / 2);
	}

	public static String generateRandomString() {
		return generateRandomString(false);
	}

	public static String generateRandomAlphaNumeric() {
		return generateRandomAlphaNumeric(10);
	}

	public static String generateRandomAlphaNumeric(int length) {
		return RandomStringUtils.randomAlphanumeric(length);
	}

	/**
	 * just a place holder to encourage the use of a standard library.
	 * 
	 * @return
	 */
	public static RandomStringUtils getRandomStringGenerator() {
		return new RandomStringUtils();
	}

	public static Character generateRandomLetter() {
		char result = (char) ('a' + generateRandomInt(26));
		return new Character(result);
	}

	/**
	 * @return
	 */
	public static Character generateRandomCharacter() {

		return new Character((char) (Character.MIN_VALUE + generateRandomInt(Byte.MAX_VALUE)));
	}

	public static String generateRandomString(boolean allowEmptyString) {
		int max = 50;
		String result = generateRandomStringOfLength(generateRandomInt(max));

		if (!allowEmptyString) {
			while (result.isEmpty()) {
				result = generateRandomStringOfLength(generateRandomInt(max));
			}
		}
		return result;
	}

	public static String generateRandomStringOfLength(int targetLength) {
		StringBuffer stringBuilder = new StringBuffer();
		while (stringBuilder.length() < targetLength) {
			stringBuilder.append(String.valueOf(generateRandomDouble()));
		}
		return stringBuilder.substring(0, targetLength);
	}

	public static Integer generateRandomInt() {
		return generateRandomInt(Integer.MAX_VALUE);
	}

	public static Byte generateRandomByte() {
		return generateRandomInt(Byte.MAX_VALUE).byteValue();
	}

	public static byte[] generateRandomBytes() {
		return generateRandomString().getBytes();
	}

	public static Date generateRandomDate() {

		return new Date(generateRandomLong());
	}

	public static String[] generateRandomStringArray() {
		return generateRandomStringArray(true);
	}

	public static String[] generateRandomStringArray(boolean allowEmtpyStrings) {
		// ensure at least 1 element in the random array
		int size = Math.max(1, generateRandomInt(20));
		String[] result = new String[size];
		for (int i = 0; i < size; i++) {
			result[i] = generateRandomString(allowEmtpyStrings);
		}

		return result;
	}

	public static Boolean generateRandomBoolean() {
		return generateRandomInt(10) % 2 == 0;
	}

	public static void assertNotEquals(Object expected, Object actual) {
		assertNotEquals(null, expected, actual);
	}

	/**
	 * @param string
	 * @param expected
	 * @param notExpected
	 */
	public static void assertNotEquals(String message, Object expected, Object actual) {
		assertFalse(expected + " is not expected to equal " + actual + "  " + StringUtils.defaultString(message),
					ObjectUtils.equals(expected, actual));

	}

	/**
	 * The same as {@link Assert#assertEquals(double, double, double)} providing
	 * {@value #REASONABLE_PRECISION} as the acceptable delta.
	 * 
	 * @param message
	 * @param expected
	 * @param actual
	 */
	public static void assertDoubleEquals(String message, double expected, double actual) {
		assertEquals(message, expected, actual, REASONABLE_PRECISION);
	}

	/**
	 * @see #assertDoubleEquals(String, Double, Double) where the message provided is null
	 * @param expected
	 * @param actual
	 */
	public static void assertDoubleEquals(double expected, double actual) {
		assertDoubleEquals(null, expected, actual);
	}

	/**
	 * 
	 * @return
	 */
	public static BigDecimal generateRandomBigDecimal() {
		return new BigDecimal(generateRandomDouble());
	}

	public static BigDecimal generateRandomBigDecimal(double min, double max) {
		return new BigDecimal(generateRandomDouble(min, max));
	}

	/**
	 * Uses {@link String#contains(CharSequence)} to look for a match. Displays the sentence if no
	 * match is found.
	 * 
	 * @see Assert#assertTrue(boolean)
	 * @param sentence
	 * @param searchItem
	 */
	public static void assertContains(String sentence, String searchItem) {
		assertTrue(sentence + "\ndoesn't contain " + searchItem, sentence.contains(searchItem));
	}

	public static void assertContains(String message, String expectedContainer, String actualWithin) {
		assertTrue(	message + " " + actualWithin + " not found in " + expectedContainer,
					expectedContainer.contains(actualWithin));
	}

	public static void assertEndsWith(String sentence, String searchItem) {
		assertTrue(sentence + " doesn't end with " + searchItem, sentence.endsWith(searchItem));
	}

	public static void assertStartsWith(String sentence, String searchItem) {
		assertTrue(sentence + " doesn't start with " + searchItem, sentence.startsWith(searchItem));
	}

	/**
	 * Uses {@link String#contains(CharSequence)} to ensure the searchItem is not found in the
	 * sentence.
	 * 
	 * @see Assert#assertFalse(boolean)
	 * @see #assertDoesntContain(String, String, String)
	 * @param sentence
	 * @param searchItem
	 */
	public static void assertDoesntContain(String sentence, String searchItem) {
		assertDoesntContain("Failed", sentence, searchItem);
	}

	public static void assertDoesntContain(String message, String sentence, String searchItem) {
		assertFalse(message + " since " + sentence + " shouldn't contain " + searchItem, sentence.contains(searchItem));
	}

	/**
	 * Simply ensures there is no $ contained inside the property.
	 * 
	 * @see #assertDoesntContain(String, String, String)
	 * @param propertyValue
	 */
	public static void assertMavenFilteredProperty(String propertyValue) {
		assertNotNull("propertyValue is null", propertyValue);
		TestUtil.assertDoesntContain("You must run maven to filter the names", propertyValue, "$");
	}

	/**
	 * Runs test against all methods used for java equality.
	 * 
	 * @param expected
	 *            the baseline
	 * @param equal
	 *            equal to the baseline
	 * @param notEqual
	 */
	public static void assertEquality(Object expected, Object equal, Object notEqual) {
		assertEquals(expected, equal);
		assertNotSame(	"this test doesn't prove anything if you provide the same objects to test equality",
						expected,
						equal);
		assertEquals(expected.hashCode(), equal.hashCode());
		assertNotEquals(expected, notEqual);

		HashSet<Object> set = new HashSet<Object>();
		assertTrue("haven't added anything yet", set.add(expected));
		assertFalse("shouldn't be able to add the same thing to a set", set.add(expected));
		assertFalse("should be the same as expected already added", set.add(equal));
		assertTrue(set.add(notEqual));
		assertFalse(set.add(notEqual));
		assertTrue(set.contains(expected));
		assertTrue(set.contains(equal));
		assertTrue(set.contains(notEqual));

	}

	/**
	 */
	public static void assertLessThan(Number expectedMaximum, Number actual) {
		assertLessThan("expected ", expectedMaximum, actual);

	}

	/**
	 */
	public static void assertLessThan(String message, Number expectedMaximum, Number actual) {
		assertTrue(	message + actual + " is not less than " + expectedMaximum,
					expectedMaximum.doubleValue() > actual.doubleValue());
	}

	/**
	 */
	public static void assertLessThanEqualTo(String message, Number expectedMaximum, Number actual) {
		assertTrue(	message + actual + " is not less than " + expectedMaximum,
					expectedMaximum.doubleValue() >= actual.doubleValue());
	}

	public static <T> void assertGreaterThan(Number expectedMinimum, Number actual) {
		assertGreaterThan(null, expectedMinimum, actual);
	}

	public static <T> void assertGreaterThan(String message, Number expectedMinimum, Number actual) {
		assertTrue(	StringUtils.defaultString(message, " failed comparison:") + " " + expectedMinimum + " <= " + actual,
					expectedMinimum.doubleValue() < actual.doubleValue());
	}

	public static void assertDateTimeEquals(DateTime expected, DateTime actual, Long tolorance) {
		long diff = expected.getMillis() - actual.getMillis();

		assertTrue(expected + " is " + diff + " millis apart, but needs to be less than " + tolorance
				+ " millis since " + actual, Math.abs(diff) <= tolorance);
	}

	public static void assertEmpty(Iterable<?> coll) {
		// null is empty too
		if (coll != null) {
			assertSize("supposed to be empty", 0, coll);
		}
	}

	public static void assertEmpty(String message, Iterable<?> coll) {
		if (coll != null) {
			assertTrue(message + String.valueOf(coll) + " is not empty", Iterables.isEmpty(coll));
		}
	}

	public static void assertNotEmpty(Iterable<?> coll) {
		assertNotNull("can't be null", coll);
		assertTrue(String.valueOf(coll) + " is empty", !Iterables.isEmpty(coll));
	}

	public static void assertInstanceOf(Class<?> expected, Object actual) {
		if (actual == null) {
			fail("actual is null");
		}
		assertInstanceOf("your property", expected, actual);
	}

	public static void assertInstanceOf(String property, Class<?> expected, Object actual) {
		assertTrue(	property + " is " + actual.getClass().getSimpleName() + ", not instanceof "
							+ expected.getSimpleName(),
					expected.isAssignableFrom(actual.getClass()));
	}

	/**
	 * Searches the collection to see if the expected is contained within using the standard equals,
	 * hashcode.
	 * 
	 * @param id
	 * @param activities
	 */
	public static void assertContains(Object expected, Iterable<?> actuals) {
		assertContains("failed", expected, actuals);
	}

	public static <T> void assertContainsOnly(T expected, Iterable<T> actuals) {
		assertContainsOnly("", expected, actuals);
	}

	public static <T> void assertContainsOnly(String message, T expected, Iterable<T> actuals) {
		assertContains(message, expected, actuals);
		assertSize(message, 1, actuals);
	}

	public static <T> void assertContainsOnly(String message, Iterable<T> actuals, T... expecteds) {
		for (T expected : expecteds) {
			assertContains(message, expected, actuals);
		}
		TestUtil.assertSize(message, expecteds.length, actuals);
	}

	/**
	 * Confirms that all the items in the expected are in the actuals.
	 * 
	 * @see #assertCollectionEquals(Collection, Collection)
	 * @param expected
	 * @param actuals
	 */
	public static void assertContainsAll(Iterable<?> expected, Iterable<?> actuals) {
		for (Object t : expected) {
			assertContains(t, actuals);
		}
	}

	/**
	 * synonym for {@link #assertContainsAny(Iterable, Iterable)}
	 * 
	 * @param expected
	 * @param actuals
	 */
	public static void assertIntersetction(Iterable<?> expected, Iterable<?> actuals) {
		assertContainsAny(expected, actuals);
	}

	public static void assertContainsAny(Iterable<?> expected, Iterable<?> actuals) {
		ImmutableSet<?> expectedSet = ImmutableSet.copyOf(expected);
		ImmutableSet<?> actualSet = ImmutableSet.copyOf(actuals);
		boolean containsAny = !Sets.intersection(expectedSet, actualSet).isEmpty();
		if (!containsAny) {
			fail(actualSet + " doesn't match any " + expectedSet);
		}
	}

	public static void assertContains(String message, Object expected, Iterable<?> actuals) {
		assertTrue(message + " since " + expected + " not found in " + actuals, Iterables.contains(actuals, expected));
	}

	/**
	 * Uses {@link String#contains(CharSequence)} to ensure the searchItem is not found in the
	 * sentence.
	 * 
	 * @see Assert#assertFalse(boolean)
	 * @see #assertDoesntContain(String, String, String)
	 * @param alternate
	 * @param collection
	 */
	public static <T> void assertDoesntContain(T notExpected, Iterable<T> collection) {
		assertFalse(notExpected + " found in " + Iterables.toString(collection),
					Iterables.contains(collection, notExpected));
	}

	/**
	 * @param i
	 * @param knownQueueNames
	 */
	public static void assertSize(Integer size, Iterable<?> collection) {
		assertSize(null, size, collection);
	}

	public static void assertSize(String message, Integer size, Iterable<?> collection) {
		assertNotNull("collection can't be null", collection);
		if (message == null) {
			message = "";
		} else {
			message += ": ";
		}
		assertEquals(message + collection.toString(), size.intValue(), Iterables.size(collection));

	}

	/**
	 * Asserts that all the items in the first collection are contained in the second...and vise
	 * versa.
	 * 
	 * @param fields
	 * @param fields2
	 */
	public static <T> void assertCollectionEquals(Collection<T> first, Collection<T> second) {
		assertCollectionEquals(StringUtils.EMPTY, first, second);
	}

	/** allows either collection to be null..and if so then the other must be empty or null. */
	public static <T> void assertCollectionEquals(String message, Collection<T> first, Collection<T> second) {
		if (first instanceof Set && second instanceof Set) {
			Set<T> firstSet = (Set<T>) first;
			Set<T> secondSet = (Set<T>) second;
			SetView<T> difference1 = Sets.symmetricDifference(firstSet, secondSet);
			if (!difference1.isEmpty()) {
				fail(difference1 + "\n\nremains when comparing:\n\n " + firstSet + "\n\nto:\n\n " + secondSet);
			}

		} else {

			if (first != second) {
				if (CollectionUtils.isEmpty(first)) {
					assertEmpty(message + " -> first is empty and second is not  " + second, second);
				} else if (CollectionUtils.isEmpty(second)) {
					assertEmpty(message + " -> second is empty and first is not " + first, first);
				} else {
					assertTrue(	first + " not equal to " + second + " " + message,
								CollectionUtils.isEqualCollection(first, second));
				}
			}
		}
	}

	/**
	 * Converts each iterable into a List and calls
	 * {@link #assertCollectionEquals(Collection, Collection)}. Call that directly if you have
	 * something more specific than {@link Iterable} so it won't get converted.
	 * 
	 * @param message
	 * @param first
	 * @param second
	 */
	public static <T> void assertIterablesEquals(String message, Iterable<T> first, Iterable<T> second) {
		ArrayList<T> firstList = Lists.newArrayList(first);
		ArrayList<T> secondList = Lists.newArrayList(second);
		assertCollectionEquals(message, firstList, secondList);
	}

	/**
	 * Confirms the size is within the range given.
	 * 
	 * @param range
	 * @param all
	 *            is any collection that can give size
	 */
	public static void assertSize(Range<Integer> range, Iterable<?> all) {
		int size = Iterables.size(all);
		assertTrue(size + " not in range " + range, range.apply(size));
	}

	/**
	 * @see #assertOrdered(Iterable, Ordering)
	 * 
	 */
	public static <C extends Comparable<C>> void assertOrdered(Iterable<C> actual) {
		final Ordering<C> ordering = Ordering.natural();
		assertOrdered(actual, ordering);
	}

	/**
	 * Given a list or sorted collection that you expect to be in the correct order this will
	 * re-order based on the given {@link Ordering} and confirm the result is the same as the actual
	 * given.
	 * 
	 * @see #assertOrdered(Iterable)
	 * @param actual
	 * @param ordering
	 */
	public static <T extends Comparable<?>> void assertOrdered(Iterable<T> actual, final Ordering<T> ordering) {
		List<T> sortedCopy = ordering.sortedCopy(actual);
		assertEquals("given isn't sorted properly", sortedCopy, actual);
	}

	/**
	 * Like the standard assertNull, but the message explains what was given if it isn't null.
	 * 
	 * @param message
	 * @param supposedToBeNull
	 */
	public static void assertNullExplanation(String message, Object supposedToBeNull) {
		assertEquals(message, null, supposedToBeNull);
	}

	/**
	 * Provides random numbers when provided anything. If the input is null then so is the result to
	 * simulate likely results in a connected system. Use {@link Predicates#notNull()}.
	 * 
	 * @return
	 */
	public static <T> Function<T, Integer> randomIntegerFunction() {
		return new Function<T, Integer>() {

			@Override
			public Integer apply(T input) {
				return (input == null) ? null : generateRandomInt();
			}
		};
	}

	/**
	 * Generates a random double in the given range.
	 * 
	 * @see #generateRandomDouble()
	 * @param range
	 * @return
	 */
	public static Double d(Range<? extends Number> range) {
		double min;
		if (range.hasLowerBound()) {
			min = range.lowerEndpoint().doubleValue();
		} else {
			min = Double.MIN_VALUE;
		}
		double max;
		if (range.hasUpperBound()) {
			max = range.upperEndpoint().doubleValue();
		} else {
			max = Double.MAX_VALUE;
		}
		return generateRandomDouble(min, max);
	}

	/**
	 * @see #d(Range)
	 * @param range
	 * @return
	 */
	public static Long l(Range<Long> range) {
		return d(range).longValue();
	}

	public static Long l() {
		return generateRandomLong();
	}

	public static String s() {
		return generateRandomAlphaNumeric();
	}

	public static Integer i(Range<Integer> range) {
		return d(range).intValue();
	}

	public static Integer i() {
		return generateRandomInt();
	}

	/**
	 * @return
	 */
	public static Message message() {
		return new StringMessage(s());
	}

	public static <T> List<T> supply(Supplier<T> supplier, Integer count) {
		ArrayList<Supplier<T>> suppliers = new ArrayList<Supplier<T>>(count);
		Collections.fill(suppliers, supplier);
		return Lists.transform(suppliers, Suppliers.<T> supplierFunction());

	}

	/**
	 * Provides an example url guaranteed to be unique by adding a unique path to example.com.
	 * 
	 * @return
	 */
	public static URL url() {
		try {
			return new URL("http://example.com/" + TestUtil.generateRandomAlphaNumeric());
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}
}
