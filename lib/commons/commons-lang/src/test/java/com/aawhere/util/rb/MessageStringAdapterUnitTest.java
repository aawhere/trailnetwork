/**
 *
 */
package com.aawhere.util.rb;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.FromString;
import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringAdapterFactory;
import com.aawhere.lang.string.StringFormatException;

/**
 * @author aroller
 * 
 */
public class MessageStringAdapterUnitTest {

	/**
	 * Demonstrates adaptation works for a specific Message by using the standard valueOf method
	 * provided on enum.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testSpecificEnumMessageAdaptation() throws BaseException {
		StringAdapterFactory factory = new StringAdapterFactory();
		StringAdapter<ExampleMessage> adapter = factory.getAdapter(ExampleMessage.class);
		ExampleMessage expected = ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE;
		String marshalled = adapter.marshal(expected);
		Message actual = adapter.unmarshal(marshalled, ExampleMessage.class);
		assertEquals(expected, actual);
		assertSame("enums are singletons", expected, actual);
	}

	/**
	 * Verifies the {@link StringMessage} implementation of Message works when it is declared as a
	 * StringMessage using the Standard {@link FromString} adaptation.
	 * 
	 * @throws StringFormatException
	 * @throws BaseException
	 */
	@Test
	public void testStringMessageAdaptation() throws StringFormatException, BaseException {
		StringAdapterFactory factory = new StringAdapterFactory();
		StringAdapter<StringMessage> adapter = factory.getAdapter(StringMessage.class);
		StringMessage expected = new StringMessage("junk");
		StringMessage actual = adapter.unmarshal(adapter.marshal(expected), StringMessage.class);
		assertEquals(expected.toString(), actual.toString());
		assertNotSame("a new StringMessage should be created", expected, actual);
	}

}
