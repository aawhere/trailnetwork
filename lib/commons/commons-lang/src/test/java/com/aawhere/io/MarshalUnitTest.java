/**
 *
 */
package com.aawhere.io;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author brian
 * 
 */
public class MarshalUnitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMarshal() throws IOException {
		String expected = "TEST";
		MarshalTestUtil.testMarshalUnmarshal(expected);
	}

	@Test
	public void testMarshalConvientMethods() {
		String expected = "TEST";

		byte[] marshaled = Marshal.marshal(expected);
		assertNotNull(marshaled);

		Object result = Marshal.unMarshal(marshaled);
		assertNotNull(result);
		assertEquals(expected, result);
	}
}
