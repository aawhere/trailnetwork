/**
 * 
 */
package com.aawhere.id;

import static org.junit.Assert.*;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.junit.Test;

import com.aawhere.id.IdentifierTestUtil.Child;
import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.id.IdentifierTestUtil.LongChildId;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author aroller
 * 
 */
public class CompositeIdentifierUnitTest {

	@Test
	public void testStringString() {
		ChildId prefix = IdentifierTestUtil.generateRandomChildId();
		ChildId suffix = IdentifierTestUtil.generateRandomChildId();
		StringStringComposite composite = new StringStringComposite(prefix, suffix);
		assertEquals(prefix, composite.prefix());
		assertEquals(suffix, composite.suffix());
	}

	@Test
	public void testStringLong() {
		ChildId prefix = IdentifierTestUtil.generateRandomChildId();
		LongChildId suffix = new LongChildId(TestUtil.generateRandomId());
		StringLongComposite composite = new StringLongComposite(prefix, suffix);
		assertEquals(prefix, composite.prefix());
		assertEquals(suffix, composite.suffix());
	}

	@Test
	public void testJaxb() {
		StringStringComposite composite = new StringStringComposite(IdentifierTestUtil.generateRandomChildId(),
				IdentifierTestUtil.generateRandomChildId());
		JAXBTestUtil<StringStringComposite> util = JAXBTestUtil.create(composite).build();
		util.assertContains(composite.getValue());
	}

	@SuppressWarnings("serial")
	@XmlType(name = ChildId.DOMAIN)
	@XmlRootElement
	public static class StringStringComposite
			extends CompositeIdentifier<Child, ChildId, ChildId> {

		/**
		 * 
		 */
		public StringStringComposite() {
			super(Child.class);
		}

		/**
		 * @param prefix
		 * @param suffix
		 * @param kind
		 */
		protected StringStringComposite(ChildId prefix, ChildId suffix) {
			super(prefix, suffix, Child.class);
		}

	}

	@SuppressWarnings("serial")
	@XmlType(name = ChildId.DOMAIN)
	@XmlRootElement
	public static class StringLongComposite
			extends CompositeIdentifier<Child, ChildId, LongChildId> {

		/**
		 * 
		 */
		public StringLongComposite() {
			super(Child.class);
		}

		/**
		 * @param prefix
		 * @param suffix
		 * @param kind
		 */
		protected StringLongComposite(ChildId prefix, LongChildId suffix) {
			super(prefix, suffix, Child.class);
		}

	}

}
