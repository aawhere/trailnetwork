package com.aawhere.io;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.BeforeClass;
import org.junit.Test;

public class DirectoryUnitTest {

	private static File tempDirFile;

	@BeforeClass
	static public void setUpTempDir() throws IOException {
		File systemTemp = getSystemTempDir();
		tempDirFile = new File(systemTemp, "AAWhere");
		tempDirFile.mkdirs();
		assertTrue("can't make temp directory " + tempDirFile.getAbsolutePath(), tempDirFile.exists());
	}

	public static Directory creatTempDir() {

		try {
			return createTempDir(5);
		} catch (Exception e) {

			e.printStackTrace();
			fail(e.getMessage());
			return null;// unreachable
		}
	}

	public static String generateRandomFilename() {
		int filenameLength = 10;
		return RandomStringUtils.random(filenameLength, true, true);
	}

	public static File getSystemTempDir() throws IOException {
		return File.createTempFile(generateRandomFilename(), null).getParentFile();
	}

	public static Directory createTempDir(int numberOfFiles) throws NotADirectoryException, IOException {

		String path = generateRandomFilename();
		setUpTempDir();
		Directory tempDir = new Directory(tempDirFile);
		Directory testDir = new Directory(tempDir, path);
		for (int i = 0; i < numberOfFiles; i++) {
			File tempFile = File.createTempFile("test", null, testDir);
			FileWriter writer = new FileWriter(tempFile);
			writer.write(tempFile.getName() + " -> " + i);
			writer.close();
		}
		return testDir;
	}

	public static String findExistingFile() {
		File file = creatTempDir();

		File[] files = file.listFiles();
		return files[0].getAbsolutePath();
	}

	@Test
	public void testTempCreation() throws NotADirectoryException, DirectoryNotFoundException, IOException {
		int numberOfFiles = 6;
		Directory directory = createTempDir(numberOfFiles);
		assertTrue(directory.exists());
		File[] files = directory.listFiles();
		assertEquals(numberOfFiles, files.length);
	}

	@Test
	public void testStringConstructor() throws NotADirectoryException, DirectoryNotFoundException {

		Directory directory = creatTempDir();
		assertTrue(directory.isDirectory());

	}

	@Test
	public void testFileConstructor() throws NotADirectoryException {
		Directory existingDir = creatTempDir();
		Directory directory = new Directory(existingDir);
		assertNotNull(directory);
		assertEquals(existingDir, directory);
	}

	@Test(expected = NotADirectoryException.class)
	public void testFile() throws NotADirectoryException, DirectoryNotFoundException {
		new Directory(findExistingFile());
		fail("should have thrown an exception");
	}

}
