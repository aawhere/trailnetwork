/**
 * 
 */
package com.aawhere.net;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Pretty handy test service available for testing our response handling.
 * 
 * http://httpstat.us/
 * 
 * @author aroller
 * 
 */
public class HttpStatusCodeTestEndpoints {

	public static URL testUrl(HttpStatusCode code) {
		try {
			return new URL("http://httpstat.us/" + code.value);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}
}
