/**
 * 
 */
package com.aawhere.i18n;

import java.util.Locale;

/**
 * Specifically for testing locale. Try to isolate and reuse Locale specific resources for testing
 * here.
 * 
 * For instance, instead of {@link Locale#getDefault()}, use {@link #DEFAULT_LOCALE}
 * 
 * @author Aaron Roller
 * 
 */
public class LocaleTestUtil {

	public static Locale DEFAULT_LOCALE = Locale.US;
	public static final Locale TEST_LOCALE = SystemLocale.ZULU.locale;
}
