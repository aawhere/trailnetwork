/**
 * 
 */
package com.aawhere.util.rb;

import static org.junit.Assert.*;

import java.util.Locale;

import com.aawhere.test.TestUtil;
import com.aawhere.text.format.custom.CustomFormat;

/**
 * @author aroller
 * 
 */
public class MessageTestUtil {

	public static <M extends Enum<? extends Message>> void assertMessages(Class<M> messageClass) {
		assertNotNull(messageClass);
		assertTrue(	"Message class must be enum.  See " + ExampleMessage.class.getName(),
					Enum.class.isAssignableFrom(messageClass));
		// autoamtically validates upon construction
		new MessageResourceBundle(messageClass);
	}

	/**
	 * @return
	 */
	public static Message generateRandomMessage() {

		return new StringMessage(TestUtil.generateRandomString());
	}

	/**
	 * Just for testing it upper cases any non null object.
	 * 
	 * @return
	 */
	public static CustomFormat<Object> toUpperCaseFormat() {
		return new CustomFormat<Object>() {

			@Override
			public Class<? super Object> handlesType() {
				return Object.class;
			}

			@Override
			public String format(Object object, Locale locale) {
				return object.toString().toUpperCase();
			}
		};
	}
}
