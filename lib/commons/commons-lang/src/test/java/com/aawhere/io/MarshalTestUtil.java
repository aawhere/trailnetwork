/**
 *
 */
package com.aawhere.io;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Test Utilities for Marshalling and unMarshalling objects using Java Serialization.
 * 
 * @author Brian Chapman
 * 
 */
public class MarshalTestUtil {

	public static void testMarshalUnmarshal(Object expected) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Marshal.marshal(expected, out);
		byte[] marshaled = out.toByteArray();
		out.close();
		assertNotNull(marshaled);

		ByteArrayInputStream in = new ByteArrayInputStream(marshaled);
		Object result = Marshal.unMarshal(in);
		in.close();
		assertNotNull(result);
		assertEquals(expected, result);
	}
}
