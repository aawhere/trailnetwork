/**
 * 
 */
package com.aawhere.test;

import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.Timeout;

import com.aawhere.test.category.WebClientTest;

/**
 * Provides the common configurations for {@link WebClientTest} JUnit Categories.
 * 
 * The @Category(WebClientTest.class) annotation must be added to each class in order for filtering
 * to work properly. It's only included here for example.
 * 
 * TODO: THis timeout functionality could be moved to another base class or possibly even a
 * delegate.
 * 
 * @author Aaron Roller
 * 
 */
@Category(WebClientTest.class)
abstract public class WebClientTestBase {

	@Rule
	public final Timeout globalTimeout;

	/**
	 * 
	 */
	public WebClientTestBase() {
		this.globalTimeout = new Timeout(2000);
	}

	/**
	 * @param timeoutMillis
	 */
	protected WebClientTestBase(int timeoutMillis) {
		super();
		this.globalTimeout = new Timeout(timeoutMillis);
	}

}
