/**
 * 
 */
package com.aawhere.style;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class ColorUtilUnitTest {

	@Test
	public void testRgbString() {
		int r = 0;
		int g = 0;
		int b = 0;
		final AlphaChannel alpha = AlphaChannel.TRANSPARENT;
		assertColor(r, g, b, alpha);
	}

	/**
	 * @param r
	 * @param g
	 * @param b
	 * @param alpha
	 */
	private void assertColor(int r, int g, int b, final AlphaChannel alpha) {
		Color color = new Color(r, g, b);
		color = alpha.apply(color);
		String expected = "0x000000";
		final String rgbString = ColorUtil.rgbString(color);
		assertEquals(expected, rgbString);
		assertEquals(expected, ColorUtil.rgbString(ColorUtil.colorFromString(rgbString)));
	}

}
