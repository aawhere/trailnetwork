/**
 * 
 */
package com.aawhere.lang;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

/**
 * Tests {@link If} conditional utility.
 * 
 * @author roller
 * 
 */
public class IfUnitTest {
	final static String EXPECTED = "expected";
	final static String NOT_EXPECTED = "not";

	@Test
	public void testNilDefault() throws Exception {
		assertEquals(EXPECTED, If.nil(null).use(EXPECTED));
	}

	@Test
	public void testNotNullDefault() throws Exception {
		assertEquals(EXPECTED, If.notNull("junk").use(EXPECTED));
	}

	@Test
	public void testNotNullNull() throws Exception {

		assertEquals(null, If.notNull(null).use(EXPECTED));
	}

	@Test
	public void testNilNotNull() {
		assertEquals(EXPECTED, If.nil(EXPECTED).use(NOT_EXPECTED));
	}

	@Test
	public void testIfTrue() {
		assertEquals(EXPECTED, If.tru(true).use(EXPECTED));
	}

	@Test
	public void testIfNotTrue() {
		assertEquals(null, If.tru(false).use(NOT_EXPECTED));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIfTrueRaise() {
		If.tru(true).raise(new IllegalArgumentException(EXPECTED));
	}

	@Test
	public void testUseFirst() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(EXPECTED);
		assertEquals(EXPECTED, If.first(list, NOT_EXPECTED));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testFirstUseDefault() {
		assertEquals(EXPECTED, If.first(Collections.EMPTY_LIST, EXPECTED));
	}

	@Test
	public void testEqualNotEqual() {
		assertEquals(EXPECTED, If.equal(EXPECTED, NOT_EXPECTED).use(NOT_EXPECTED));
	}

	@Test
	public void testEqualEqual() {
		assertEquals(EXPECTED, If.equal(NOT_EXPECTED, NOT_EXPECTED).use(EXPECTED));
	}

	@Test
	public void testNotEqualNotEqual() {
		assertEquals(EXPECTED, If.notEqual(NOT_EXPECTED, EXPECTED).use(EXPECTED));
	}

	@Test
	public void testNotEqualEqual() {
		assertEquals(EXPECTED, If.notEqual(EXPECTED, EXPECTED).use(NOT_EXPECTED));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRaiseRuntime() {
		If.nil(null).raise(new IllegalArgumentException(EXPECTED));
	}

	@Test(expected = IOException.class)
	public void testRaiseChecked() throws IOException {
		// demonstrates the need for handling
		If.nil(null).raise(new IOException());
	}

	@Test
	public void testNoRaise() {
		assertEquals(EXPECTED, If.nil(EXPECTED).raise(new IllegalArgumentException()));
	}
}
