package com.aawhere.lang.reflect;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.aawhere.collections.Index;

/**
 * @see GenericUtils
 * 
 * @author Aaron Roller
 * 
 */
public class GenericUtilsUnitTest {

	@Test
	public void testReturnedClass() {
		Single junk = new Single();
		Class<?> parameterClass = GenericUtils.getTypeArgument(junk);
		assertEquals(String.class, parameterClass);
	}

	@Test
	public void testSingle() {
		Class<?> paramaterClass = GenericUtils.getTypeArguments(Parent.class, Single.class).get(0);
		assertEquals(String.class, paramaterClass);
	}

	@Test
	public void testNone() {
		GenericUtils.getTypeArgument(new String("junk"));
	}

	@Test
	public void testInterfaceParameter() {
		Class<Map.Entry<String, String>> typeArgument = GenericUtils.getTypeArgument(	HasInterfaceParam.class,
																						Index.FIRST);

	}

	@Test
	public void testMultipleAll() {
		List<Class<?>> all = GenericUtils.getTypeArguments(new Multiple());
		assertNotNull(all);

		assertEquals(3, all.size());
		assertTrue(all.contains(Long.class));
		assertTrue(all.contains(String.class));
		assertTrue(all.contains(Integer.class));
		assertEquals(0, all.indexOf(String.class));
		assertEquals(1, all.indexOf(Integer.class));
		assertEquals(2, all.indexOf(Long.class));
	}

	@Test
	public void testMultipleIndex() {
		assertEquals(String.class, GenericUtils.getTypeArgument(new Multiple(), Index.FIRST));
		assertEquals(Integer.class, GenericUtils.getTypeArgument(new Multiple(), Index.SECOND));
		assertEquals(Long.class, GenericUtils.getTypeArgument(new Multiple(), Index.THIRD));
	}

	abstract public static class ParentMultiple<A, B, C> {

	}

	public static class Multiple
			extends ParentMultiple<String, Integer, Long> {

	}

	abstract public static class Parent<T> {
	}

	public static class Single
			extends Parent<String> {

	}

	public static class HasInterfaceParam
			extends Parent<Map.Entry<String, String>> {

	}
}
