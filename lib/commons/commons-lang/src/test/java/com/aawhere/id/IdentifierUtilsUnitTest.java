/**
 * 
 */
package com.aawhere.id;

import static org.junit.Assert.*;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.aawhere.id.IdentifierTestUtil.Child;
import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.id.IdentifierTestUtil.ChildIdWithParent;
import com.aawhere.id.IdentifierTestUtil.LongChildId;
import com.aawhere.test.TestUtil;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author roller
 * @see IdentifierUtils
 */
public class IdentifierUtilsUnitTest {

	@Test
	public void testIdsFromCollection() throws Exception {
		List<Child> children = IdentifierTestUtil.generateRandomChildren();
		Set<ChildId> idsFrom = IdentifierUtils.idsFrom(children);
		IdentifierTestUtil.assertIds(idsFrom, children);
	}

	@Test
	public void testKind() {
		Class<Child> expected = Child.class;
		Class<Child> actual = IdentifierUtils.kindFrom(ChildId.class);
		assertEquals(expected, actual);
	}

	@Test
	public void testFromString() {
		String expected = "abc1123";
		ChildId actual = IdentifierUtils.idFrom(expected, ChildId.class);
		assertEquals(expected, actual.getValue());

	}

	/** Joins ids together and splits them back into ids. */
	@Test
	public void testFromStringArray() {
		ChildId one = IdentifierTestUtil.generateRandomChildId();
		ChildId two = IdentifierTestUtil.generateRandomChildId();
		ChildId three = IdentifierTestUtil.generateRandomChildId();
		ArrayList<ChildId> list = Lists.newArrayList(one, two, three);
		String join = IdentifierUtils.idsJoined(list);
		Set<ChildId> ids = IdentifierUtils.idsFrom(join, ChildId.class);
		TestUtil.assertIterablesEquals(join, list, ids);
	}

	@Test
	public void testFromLong() {
		Long expected = 123L;
		LongChildId actual = IdentifierUtils.idFrom(expected, LongChildId.class);
		assertEquals(expected, actual.getValue());
	}

	@Test
	public void testCompositeSplitStrings() {
		ChildId expectedLeft = IdentifierTestUtil.generateRandomChildId();
		ChildId expectedRight = IdentifierTestUtil.generateRandomChildId();
		assertComposite(expectedLeft, expectedRight);
	}

	@Test
	public void testCompositeSplitStringFirstOccurrence() {
		String a = "a";
		String b = "b";
		String c = "c";
		String compositeIdValue = IdentifierUtils.createCompositeIdValue(a, b, c);
		String[] split = IdentifierUtils.splitCompositeIdValueFirstOccurrence(compositeIdValue);
		TestUtil.assertSize(2, Lists.newArrayList(split));
		assertEquals(a, split[0]);
		String secondPart = split[1];
		TestUtil.assertContains(secondPart, b);
		TestUtil.assertContains(secondPart, c);
		TestUtil.assertContains(secondPart, IdentifierUtils.COMPOSITE_ID_SEPARATOR_STRING);
	}

	@Test
	public void testCompositeCompositeSplitStrings() {
		ChildId expectedLeft = IdentifierTestUtil.generateRandomCompositeId();
		ChildId expectedRight = IdentifierTestUtil.generateRandomCompositeId();
		assertComposite(expectedLeft, expectedRight);
	}

	@Test(expected = InvalidIdentifierException.class)
	public void testCompositeSplitNoDilimeter() {
		final String invalidValue = "sdflkn";
		assertInvalid(invalidValue);
	}

	/**
	 * @param invalidValue
	 */
	private void assertInvalid(final String invalidValue) {
		ChildId compositeId = new ChildId(invalidValue);
		IdentifierUtils.splitCompositeId(compositeId, ChildId.class);
	}

	@Test(expected = InvalidIdentifierException.class)
	public void testCompositeSplitEmtpyLead() {
		assertInvalid("-adb");
	}

	@Test(expected = InvalidIdentifierException.class)
	public void testCompositeSplitEmptySuffix() {
		assertInvalid("adb-");
	}

	@Test
	public void testFilterIds() {
		Child first = IdentifierTestUtil.generateRandomChild();
		Child second = IdentifierTestUtil.generateRandomChild();
		Child third = IdentifierTestUtil.generateRandomChild();
		HashSet<Child> children = Sets.newHashSet(first, second, third);
		final Set<ChildId> firstSecond = Sets.newHashSet(first.getId(), second.getId());
		final Set<ChildId> secondThird = Sets.newHashSet(second.getId(), third.getId());
		final Set<ChildId> firstThird = Sets.newHashSet(first.getId(), third.getId());
		final Set<ChildId> all = Sets.newHashSet(first.getId(), second.getId(), third.getId());
		final Set<ChildId> none = Sets.newHashSet();

		assertFilterByIds(children, firstSecond);
		assertFilterByIds(children, secondThird);
		assertFilterByIds(children, firstThird);
		assertFilterByIds(children, all);
		assertFilterByIds(children, none);

	}

	/**
	 * @see IdentifierUtils#isIdentifierType(Type)
	 */
	@Test
	public void testIdentifierType() {
		assertTrue("class directly is type identifier", IdentifierUtils.isIdentifierType(Identifier.class));
		assertTrue("class directly is type identifier", IdentifierUtils.isIdentifierType(ChildId.class));
		assertTrue("class directly is type identifier", IdentifierUtils.isIdentifierType(StringIdentifier.class));
		Type type = new ParameterizedType() {

			@Override
			public Type getRawType() {
				return Identifier.class;
			}

			@Override
			public Type getOwnerType() {
				throw new UnsupportedOperationException();
			}

			@Override
			public Type[] getActualTypeArguments() {
				throw new UnsupportedOperationException();
			}
		};

		assertTrue("this is how we test for type", IdentifierUtils.isIdentifierType(type));

	}

	/**
	 * @param children
	 * @param keepers
	 */
	private void assertFilterByIds(HashSet<Child> children, final Set<ChildId> keepers) {
		TestUtil.assertIterablesEquals(	"expecting filter",
										keepers,
										IdentifierUtils.ids(IdentifierUtils.filter(children, keepers)));
	}

	@Test(expected = InvalidIdentifierException.class)
	public void testCompositeSplitDoubleDash() {
		assertInvalid("--");
	}

	/**
	 * @param expectedLeft
	 * @param expectedRight
	 */
	private void assertComposite(ChildId expectedLeft, ChildId expectedRight) {
		String createCompositeIdValue = IdentifierUtils.createCompositeIdValue(expectedLeft, expectedRight);
		ChildId compositeId = new ChildId(createCompositeIdValue);
		Pair<ChildId, ChildId> pair = IdentifierUtils.splitCompositeId(compositeId, ChildId.class);
		assertEquals("left", expectedLeft, pair.getLeft());
		assertEquals("right", expectedRight, pair.getRight());
	}

	@Test
	public void testAncestry() {
		ChildIdWithParent root = new ChildIdWithParent();
		ChildIdWithParent middle = new ChildIdWithParent(root);
		ChildIdWithParent leaf = new ChildIdWithParent(middle);
		List<Identifier<?, ?>> ancestry = IdentifierUtils.ancestryRootLast(leaf);
		assertEquals(ImmutableList.of(leaf, middle, root), ancestry);
	}

	@Test
	public void testHasParent() {
		assertFalse(IdentifierUtils.hasParent(new ChildIdWithParent()));
		assertTrue(IdentifierUtils.hasParent(new ChildIdWithParent(new ChildIdWithParent())));
	}

	@Test
	public void testHasParentType() {
		assertFalse("child id", IdentifierUtils.hasParent(ChildId.class));
		assertTrue("child id with parent", IdentifierUtils.hasParent(ChildIdWithParent.class));
	}

	@Test
	public void testParentType() {
		assertEquals(ChildIdWithParent.class, IdentifierUtils.parentType(ChildIdWithParent.class));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParetTypeInvalid() {
		IdentifierUtils.parentType(ChildId.class);
	}
}
