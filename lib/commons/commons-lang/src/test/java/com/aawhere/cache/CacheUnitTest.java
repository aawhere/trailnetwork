/**
 * 
 */
package com.aawhere.cache;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.cache.CacheManager;
import com.aawhere.test.TestUtil;

import net.sf.jsr107cache.Cache;

/**
 * Tests the {@link CacheManager} and cache implementation provided.
 * 
 * This uses EHCache's JCache implementation.
 * 
 * @author roller
 * 
 */
public class CacheUnitTest {

	@Test
	public void testBasicCache() {
		CacheManager manager = CacheManager.getInstance();
		Class<CacheUnitTest> keeperOfCache = CacheUnitTest.class;
		Cache cache = manager.getCache(keeperOfCache);
		assertNotNull(cache);
		Cache cache2 = manager.getCache(keeperOfCache);
		assertSame(cache, cache2);

		String key = TestUtil.generateRandomString();
		String value = TestUtil.generateRandomString();
		Object previous = cache.put(key, value);
		assertNull(previous);
		Object foundValue = cache.get(key);
		assertEquals(value, foundValue);

		String nextValue = TestUtil.generateRandomString();
		previous = cache.put(key, nextValue);
		assertEquals(value, previous);
		Object foundNextValue = cache.get(key);
		assertEquals(nextValue, foundNextValue);

	}
}
