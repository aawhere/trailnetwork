/**
 * 
 */
package com.aawhere.ws.rs;

import java.util.Map;

import org.junit.Test;

import com.aawhere.collections.MapBuilder;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class UriBuilderUtilUnitTest {

	@Test
	public void testBuildWithParamaters() {
		String valueKey = "KEY";
		String subKey = "SUB";
		String value = "value";
		String sub = "test";
		String url = "http://{" + subKey + "}.example.com?something={" + valueKey + "}";
		Map<String, String> map = new MapBuilder<String, String>().put(valueKey, value).put(subKey, sub).build();
		String replaced = UriBuilderUtil.build(url, map);
		TestUtil.assertContains(replaced, value);
		TestUtil.assertContains(replaced, sub);
	}

}
