/**
 * 
 */
package com.aawhere.xml.bind;

import static org.junit.Assert.*;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.junit.Test;

import com.aawhere.xml.XmlNamespace;

/**
 * @see NamespaceResolver
 * @author Aaron Roller
 * 
 */
public class NamespaceResolverUnitTest {

	static final String NAMESPACE = "http://www.test.org";

	@Test
	public void testXmlType() {
		final Class<?> targetClass = TestXmlType.class;
		assertNamespace(targetClass);
	}

	/**
	 * @param targetClass
	 */
	private void assertNamespace(final Class<?> targetClass) {
		NamespaceResolver resolver = new NamespaceResolver.Builder().setTargetClass(targetClass).build();
		assertEquals(NAMESPACE, resolver.getNamespace());
		assertTrue(resolver.isNamespaceAvailable());
	}

	@Test
	public void testXmlRoot() {
		assertNamespace(TestXmlRoot.class);
	}

	@Test
	public void testPackage() {
		assertNamespace(TestPackage.class);
	}

	@Test
	public void testXmlTypeDefault() {
		assertNamespace(TestXmlTypeDefault.class);
	}

	@Test
	public void testNone() {
		NamespaceResolver resolver = new NamespaceResolver.Builder().setTargetClass(String.class).build();
		assertNull(resolver.getNamespace());
		assertFalse(resolver.isNamespaceAvailable());
	}

	@XmlType(namespace = NAMESPACE)
	static class TestXmlType {

	}

	@XmlRootElement(namespace = NAMESPACE)
	static class TestXmlRoot {

	}

	@XmlType(namespace = XmlNamespace.XML_TYPE_DEFAULT_VALUE)
	static class TestXmlTypeDefault {
	}

	// namespace declare in package-info.java
	static class TestPackage {

	}
}
