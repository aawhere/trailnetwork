/**
 * 
 */
package com.aawhere.util.rb;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.enums.EnumWithDefault;

/**
 * Tests {@link MessageFactory}
 * 
 * @author roller
 * 
 */
public class MessageFactoryUnitTest {

	final MessageFactory factory = MessageFactory.getInstance();
	final ExampleMessage expected = ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE;
	String value = "junk";

	@Test
	public void testMatchingEnum() throws Exception {
		String key = factory.keyForEnum(expected);
		Message actual = factory.bestMessage(key, value);
		assertEquals(expected, actual);

	}

	@Test
	public void testNullKey() throws Exception {

		Message actual = factory.bestMessage(null, value);
		assertEquals(value, actual.getValue());
	}

	@Test
	public void testBothAreNull() throws Exception {
		assertNull(factory.bestMessage(null, null));
	}

	@Test
	public void testBadKeyUseValue() {
		Message result = factory.bestMessage(value, value);
		assertTrue("should be a StringMessage " + result.toString(), result instanceof StringMessage);
		assertEquals(value, result.getValue());
	}

	@Test
	public void testBadKeyNoValue() {
		assertNull(factory.bestMessage(value, null));
	}

	@Test
	public void testValueOf() {
		assertEquals(expected, MessageFactory.valueOf(ExampleMessage.class, expected.name()));
	}

	@Test(expected = InvalidChoiceException.class)
	public void testBadValueOf() {
		assertEquals(expected, MessageFactory.valueOf(ExampleMessage.class, value));
	}

	@Test
	public void testBadValueWithDefault() {
		assertEquals(ExampleMessageWithDefault.B, MessageFactory.valueOf(ExampleMessageWithDefault.class, value));
	}

	public static enum ExampleMessageWithDefault implements Message, EnumWithDefault<ExampleMessageWithDefault> {
		A, B;

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.enums.EnumWithDefault#getDefault()
		 */
		@Override
		public ExampleMessageWithDefault getDefault() {
			return B;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.util.rb.Message#getValue()
		 */
		@Override
		public String getValue() {
			return name();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.util.rb.Message#getParameterKeys()
		 */
		@Override
		public ParamKey[] getParameterKeys() {
			return new ParamKey[] {};
		}

	}
}
