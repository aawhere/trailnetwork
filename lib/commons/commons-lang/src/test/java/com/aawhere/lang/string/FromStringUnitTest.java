/**
 * 
 */
package com.aawhere.lang.string;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * tests {@link FromString}.
 * 
 * @author aroller
 * 
 */
public class FromStringUnitTest {

	@Test
	public void testValueOf() throws NoSuchMethodException, StringFormatException {
		Integer expected = TestUtil.generateRandomInt();
		Integer actual = FromString.valueOf(Integer.class, expected.toString());
		assertEquals(expected, actual);
		assertFromString(Integer.class, expected.toString());
	}

	@Test
	public void testConstructor() throws NoSuchMethodException, StringFormatException {
		String expected = TestUtil.generateRandomAlphaNumeric();
		String actual = FromString.construct(String.class, expected);
		assertEquals(expected, actual);
		assertFromString(String.class, expected);
	}

	@Test(expected = NoSuchMethodException.class)
	public void testNoConstructor() throws NoSuchMethodException, StringFormatException {
		FromString.construct(FromStringUnitTest.class, "junk");
		fail("should have thrown an exception");
	}

	@Test(expected = NoSuchMethodException.class)
	public void testNoValueOf() throws NoSuchMethodException, StringFormatException {
		FromString.valueOf(FromStringUnitTest.class, "trash");
		fail("should have thrown an exception");
	}

	@Test(expected = StringFormatException.class)
	public void testBadValue() throws NoSuchMethodException, StringFormatException {
		Integer result = FromString.valueOf(Integer.class, "abc");
		fail("a bad value passed and FromString found: " + result);
	}

	@Test
	public void testQualifies() {
		assertTrue(FromString.qualifies(ConstructorOnly.class));
		assertTrue(FromString.qualifies(ValueOfOnly.class));
		assertFalse(FromString.qualifies(FromStringUnitTest.class));
	}

	/**
	 * A test incapable of being run regularly since it's intention is to make failure.
	 * 
	 * @throws NoSuchMethodException
	 * @throws StringFormatException
	 */
	// @Test
	public void testInvalidToString() throws NoSuchMethodException, StringFormatException {
		assertFromString(NoToString.class, "whatever");
	}

	public static <T> T assertFromString(T instance) throws StringFormatException, NoSuchMethodException {
		return assertFromString((Class<T>) instance.getClass(), instance.toString());
	}

	public static <T> T assertFromString(Class<T> type, String expected) throws NoSuchMethodException,
			StringFormatException {
		T instance = FromString.newInstance(type, expected);
		T secondInstance = FromString.newInstance(type, instance.toString());
		assertEquals(instance, secondInstance);
		return instance;

	}

	/** Class breaking the contract of not having a toString() method */
	public static class NoToString {
		public NoToString(String string) {
		}
	}

	public static class ConstructorOnly {
		String string;

		private ConstructorOnly(String string) {
		}

		public ConstructorOnly() {
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {

			return string;
		}
	}

	public static class ValueOfOnly
			extends ConstructorOnly {

		/**
		 * @param string
		 */
		private ValueOfOnly() {

		}

		public static ValueOfOnly valueOf(String string) {
			ValueOfOnly result = new ValueOfOnly();
			result.string = string;
			return result;
		}
	}

}
