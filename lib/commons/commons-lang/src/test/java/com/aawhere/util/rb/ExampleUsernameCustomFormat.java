/**
 * 
 */
package com.aawhere.util.rb;

import java.util.Locale;

import com.aawhere.text.format.custom.CustomFormat;

/**
 * Simply demonstrates how to use a formatter to format your parameters.
 * 
 * @author roller
 * 
 */
public class ExampleUsernameCustomFormat
		implements CustomFormat<String> {

	/*
	 * (non-Javadoc)
	 * @see com.akuacom.common.resource.ResourceBundleMessageParamFormat#format(java .lang.Object,
	 * java.util.Locale)
	 */
	@Override
	public String format(String object, Locale locale) {
		String username = object.toString();
		return username.toLowerCase();
	}

	@Override
	public Class<String> handlesType() {

		return String.class;
	}

}
