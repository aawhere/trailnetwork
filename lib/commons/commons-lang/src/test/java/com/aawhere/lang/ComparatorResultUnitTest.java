/**
 * 
 */
package com.aawhere.lang;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @see ComparatorResult
 * 
 * @author aroller
 * 
 */
public class ComparatorResultUnitTest {

	private Integer one = 1;
	private Integer two = 2;
	private Integer three = 3;

	@Test
	public void testEqualTo() {
		assertTrue(ComparatorResult.isEqualTo(one.compareTo(one)));
		assertFalse(ComparatorResult.isEqualTo(one.compareTo(two)));
	}

	@Test
	public void testLessThan() {
		assertFalse(ComparatorResult.isLessThan(one.compareTo(one)));
		assertFalse(ComparatorResult.isLessThan(two.compareTo(one)));
		assertTrue(ComparatorResult.isLessThan(one.compareTo(two)));
		assertTrue(ComparatorResult.isLessThan(one.compareTo(three)));
	}

	@Test
	public void testLessThanEqualTo() {
		assertTrue(ComparatorResult.isLessThanOrEqualTo(one.compareTo(one)));
		assertFalse(ComparatorResult.isLessThanOrEqualTo(two.compareTo(one)));
		assertTrue(ComparatorResult.isLessThanOrEqualTo(one.compareTo(two)));
	}

	@Test
	public void testGreaterThan() {
		assertFalse(ComparatorResult.isGreaterThan(one.compareTo(one)));
		assertFalse(ComparatorResult.isGreaterThan(one.compareTo(two)));
		assertTrue(ComparatorResult.isGreaterThan(two.compareTo(one)));
		assertTrue(ComparatorResult.isGreaterThan(three.compareTo(one)));
	}

	@Test
	public void testGreaterThanEqualtTo() {
		assertTrue(ComparatorResult.isGreaterThanOrEqualTo(one.compareTo(one)));
		assertFalse(ComparatorResult.isGreaterThanOrEqualTo(one.compareTo(two)));
		assertTrue(ComparatorResult.isGreaterThanOrEqualTo(two.compareTo(one)));
	}

	@Test
	public void testBinary() {
		assertEquals("right null", ComparatorResult.LEFT.value, ComparatorResult.binary(true, null));
		assertEquals("right false", ComparatorResult.LEFT.value, ComparatorResult.binary(true, false));
		assertEquals("left null", ComparatorResult.RIGHT.value, ComparatorResult.binary(null, true));
		assertEquals("left false", ComparatorResult.RIGHT.value, ComparatorResult.binary(false, true));
		assertEquals("both true", ComparatorResult.EQUAL.value, ComparatorResult.binary(true, true));
		assertEquals("both false", ComparatorResult.EQUAL.value, ComparatorResult.binary(false, false));
		assertEquals("both null", ComparatorResult.EQUAL.value, ComparatorResult.binary(null, null));
	}
}
