/**
 *
 */
package com.aawhere.ws.rs;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.aawhere.lang.NotImplementedException;

import com.google.common.collect.Lists;

/**
 * Provides an easy way to mock out HttpHeaders.
 * 
 * @author Brian Chapman
 * 
 */
public class MockHttpHeaders
		implements HttpHeaders {

	private Map<String, List<String>> headers = new HashMap<String, List<String>>();

	public void addHeader(String name, String value) {
		List<String> headerValueList = Lists.newArrayList(value);
		headers.put(name, headerValueList);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.core.HttpHeaders#getRequestHeader(java.lang.String)
	 */
	@Override
	public List<String> getRequestHeader(String name) {
		return headers.get(name);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.core.HttpHeaders#getRequestHeaders()
	 */
	@Override
	public MultivaluedMap<String, String> getRequestHeaders() {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.core.HttpHeaders#getAcceptableMediaTypes()
	 */
	@Override
	public List<MediaType> getAcceptableMediaTypes() {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.core.HttpHeaders#getAcceptableLanguages()
	 */
	@Override
	public List<Locale> getAcceptableLanguages() {
		return Lists.newArrayList(Locale.getDefault());
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.core.HttpHeaders#getMediaType()
	 */
	@Override
	public MediaType getMediaType() {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.core.HttpHeaders#getLanguage()
	 */
	@Override
	public Locale getLanguage() {
		return Locale.getDefault();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.core.HttpHeaders#getCookies()
	 */
	@Override
	public Map<String, Cookie> getCookies() {
		throw new NotImplementedException();
	}

}