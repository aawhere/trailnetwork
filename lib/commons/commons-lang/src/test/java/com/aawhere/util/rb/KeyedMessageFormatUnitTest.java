package com.aawhere.util.rb;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * Tests the strings for parameter replacement.
 * 
 * Strings are written without variable replacement for readibility since this is a unit test and
 * these values don't need to change.
 * 
 * @author roller
 * 
 * @see KeyedMessageFormat
 */
public class KeyedMessageFormatUnitTest {
	private final String ZERO = "zero", ONE = "one", TWO = "two", THREE = "three";
	private String multipleParamString = "one {zero} two {one} three {two}";

	private HashMap<String, String> hashMapBuilder(String[] keys, String[] values) {
		HashMap<String, String> keysAndParams = new HashMap<String, String>();
		for (int i = 0; i < keys.length; i++) {
			keysAndParams.put(keys[i], values[i]);
		}
		return keysAndParams;
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUnmatchingBraces() {
		KeyedMessageFormat.create("one { has no match").build();

	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyBraces() {
		KeyedMessageFormat.create("two {}").build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyBracesWithSpace() {
		KeyedMessageFormat.create("two {  }").build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testMisMatchedBraces() {
		KeyedMessageFormat.create("}two{ ").build();
	}

	@Test
	public void testNoBraces() {
		KeyedMessageFormat.create("one two").build();
	}

	@Test
	public void testEscapeNoParams() {
		KeyedMessageFormat.create("one '{ two").build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testParamCount() {
		HashMap<String, String> keysAndParams = (HashMap<String, String>) hashMapBuilder(	new String[] { ZERO },
																							new String[] { ONE });
		KeyedMessageFormat.create(multipleParamString).putAll(keysAndParams).build();

	}

	@Test(expected = IllegalArgumentException.class)
	public void testParamNotFound() {
		HashMap<String, String> keysAndParams = hashMapBuilder(new String[] { ZERO }, new String[] { ONE });
		KeyedMessageFormat.create("one {one}").putAll(keysAndParams).build();

	}

	@Test
	public void testSingleParam() {
		HashMap<String, String> keysAndParams = (HashMap<String, String>) hashMapBuilder(	new String[] { ZERO },
																							new String[] { ONE });
		KeyedMessageFormat formatMessage = KeyedMessageFormat.create("one {zero}").putAll(keysAndParams).build();
		String cleanedMessage = formatMessage.getFormattedMessage();
		String string = "one one";
		assertEquals(string, cleanedMessage);

	}

	@Test
	public void testParamInUrl() {
		String urlPath = "http://bogus/{zero}";
		KeyedMessageFormat format = KeyedMessageFormat.create(urlPath).put(ZERO, ONE).build();
		String formattedMessage = format.getFormattedMessage();
		TestUtil.assertContains(formattedMessage, ONE);
	}

	@Test
	public void testMultipleParams() {
		String[] keys = { ZERO, ONE, TWO };
		String[] values = { ONE, TWO, THREE };
		HashMap<String, String> keysAndParams = (HashMap<String, String>) hashMapBuilder(keys, values);
		KeyedMessageFormat formatMessage = KeyedMessageFormat.create(multipleParamString).putAll(keysAndParams).build();
		String cleanedMessage = formatMessage.getFormattedMessage();
		assertEquals("one one two two three three", cleanedMessage);

	}

	@Test
	public void testEscapedBrace() {
		HashMap<String, String> keysAndParams = (HashMap<String, String>) hashMapBuilder(	new String[] { TWO },
																							new String[] { ONE });
		KeyedMessageFormat formatMessage = KeyedMessageFormat.create("one '{ is a brace {two} is not")
				.putAll(keysAndParams).build();
		String cleanedMessage = formatMessage.getFormattedMessage();
		assertEquals("one { is a brace one is not", cleanedMessage);

	}

	@Test
	public void testEscapedQuote() {
		HashMap<String, String> keysAndParams = (HashMap<String, String>) hashMapBuilder(	new String[] { TWO },
																							new String[] { ONE });
		KeyedMessageFormat formatMessage = KeyedMessageFormat.create("one ''{two}").putAll(keysAndParams).build();
		String cleanedMessage = formatMessage.getFormattedMessage();
		assertEquals("one 'one", cleanedMessage);

	}

}
