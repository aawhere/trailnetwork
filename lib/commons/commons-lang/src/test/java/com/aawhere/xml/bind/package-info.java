@XmlSchema(namespace = com.aawhere.xml.bind.NamespaceResolverUnitTest.NAMESPACE)
/**Used to provide testing of namespaces.  
 * 
 * @see com.aawhere.xml.bind.NamespaceResolverUnitTest
 * 
 */
package com.aawhere.xml.bind;

import javax.xml.bind.annotation.XmlSchema;

