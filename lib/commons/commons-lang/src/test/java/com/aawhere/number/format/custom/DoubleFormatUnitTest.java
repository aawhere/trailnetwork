/**
 *
 */
package com.aawhere.number.format.custom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author brian
 * 
 */
public class DoubleFormatUnitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link com.aawhere.number.format.custom.DoubleFormat#format(java.lang.Double)}.
	 */
	@Test
	public void testFormat() {
		Double value = 10.123456789D;
		DoubleFormat format = new DoubleFormat(4);
		Double result = format.format(value);
		TestUtil.assertDoubleEquals(10.1235D, result);
	}

}
