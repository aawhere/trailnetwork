/**
 * 
 */
package com.aawhere.field;

import com.aawhere.field.FieldMessage.Param;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * When searching for a specific instance of a dictionary (or {@link Report}) this will indicate no
 * such instance is found for the given key.
 * 
 * @author aroller
 * 
 */
public class DictionaryInstanceNotFoundException
		extends BaseException {

	private static final long serialVersionUID = -8287340638779615291L;

	public DictionaryInstanceNotFoundException(Object dictionaryInstanceKey, String domain) {
		super(CompleteMessage.create(FieldMessage.DICTIONARY_INSTANCE_NOT_FOUND).param(Param.DOMAIN, domain)
				.param(Param.OBJECT, dictionaryInstanceKey).build());
	}

}
