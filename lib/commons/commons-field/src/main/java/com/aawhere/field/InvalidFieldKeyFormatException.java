/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Oct 31, 2010
commons-lang : com.aawhere.field.InvalidFieldKeyFormatException.java
 */
package com.aawhere.field;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates the field key provided simply is not of the correct format.
 * 
 * NOTE: Until it is determined that our system will be dynamically registering dictionaries (via
 * web servies, etc), we don't need this to be an ApplicationException.
 * 
 * @author roller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class InvalidFieldKeyFormatException
		extends BaseRuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = -2713077300638050231L;

	/**
	 *
	 */
	public InvalidFieldKeyFormatException(String fieldKeyDesiredValue, String fieldKeyHiglightedInvalidCharacters) {
		super(new CompleteMessage.Builder().setMessage(FieldMessage.INVALID_KEY_FORMAT)
				.addParameter(FieldMessage.Param.FIELD_KEY, fieldKeyDesiredValue)
				.addParameter(FieldMessage.Param.FIELD_KEY_INVALID_CHARACTERS, fieldKeyHiglightedInvalidCharacters)
				.build());
	}

	/**
	 * @param build
	 */
	protected InvalidFieldKeyFormatException(CompleteMessage message) {
		super(message);
	}

}
