/**
 *
 */
package com.aawhere.field;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.collections.ImmutableCollection;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

/**
 * Represents a set of Field Definitions. Duplictes are not allowed. Order is preserved based on the
 * order added.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class FieldDefinitions
		extends ImmutableCollection<FieldDefinition<?>>
		implements Iterable<FieldDefinition<?>> {

	/**
	 * Used to construct all instances of FieldDefinitions.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<FieldDefinitions> {

		public Builder() {
			super(new FieldDefinitions());
		}

		public Builder add(FieldDefinition<?> definition) {
			building.definitions.add(definition);
			return this;
		}

		public Builder addAll(Set<FieldDefinition<?>> definitions) {
			building.definitions.addAll(definitions);
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct FieldDefinitions */
	private FieldDefinitions() {
	}

	@XmlElement(name = "field")
	private Set<FieldDefinition<?>> definitions = new HashSet<FieldDefinition<?>>();

	/**
	 * @return the all
	 */
	public Set<FieldDefinition<?>> getAll() {
		return this.definitions;
	};

	/*
	 * Collection implementation below.
	 */

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object arg0) {
		return definitions.contains(arg0);
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#containsAll(java.util.Collection)
	 */
	@Override
	public boolean containsAll(Collection<?> arg0) {
		return definitions.containsAll(arg0);
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return definitions.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#iterator()
	 */
	@Override
	public Iterator<FieldDefinition<?>> iterator() {
		return definitions.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#size()
	 */
	@Override
	public int size() {
		return definitions.size();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#toArray()
	 */
	@Override
	public Object[] toArray() {
		return definitions.toArray();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#toArray(T[])
	 */
	@Override
	public <T> T[] toArray(T[] arg0) {
		return definitions.toArray(arg0);
	}
}
