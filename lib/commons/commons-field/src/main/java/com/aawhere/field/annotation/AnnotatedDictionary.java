/**
 * 
 */
package com.aawhere.field.annotation;

/**
 * A marker required to be placed on a class or interface that uses {@link Dictionary} and
 * {@link Field} annotations.
 * 
 * @author Aaron Roller
 * 
 */
public interface AnnotatedDictionary {

}
