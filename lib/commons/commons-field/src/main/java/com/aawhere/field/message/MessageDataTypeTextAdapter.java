/**
 *
 */
package com.aawhere.field.message;

import com.aawhere.field.type.DataTypeAdapter;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.NotImplementedException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;

/**
 * Translates from an Enumeration to a type supported by {@link FieldDataType#ATOM}
 * 
 * @author Brian Chapman
 * 
 */
public class MessageDataTypeTextAdapter
		extends DataTypeAdapter<Message> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldData marshal(Object adaptee) {
		if (adaptee instanceof Message) {
			return new FieldData.Builder().setValue(((Message) adaptee).getValue()).setType(getFieldDataType()).build();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.EnumDataTypeAdapter#unmarshal(com.aawhere.field.type.FieldData,
	 * java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <E extends Message> E unmarshal(FieldData adapted, Class<E> type) throws BaseException {
		// There isn't a way to unmarshal this sine the value is the localized message. We could
		// return a StringMessage, but no one is expecting that. They are expecting E.

		Object fieldDataValue = adapted.getValue();
		if (fieldDataValue instanceof String) {
			return (E) new StringMessage((String) fieldDataValue);
		}
		// Nothing matched.
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@Override
	public Class<Message> getType() {
		return Message.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.DataTypeAdapter#getFieldDataType()
	 */
	@Override
	public FieldDataType getFieldDataType() {
		return FieldDataType.TEXT;
	}

}
