/**
 *
 */
package com.aawhere.field.message;

import com.aawhere.field.type.DataTypeAdapter;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.NotImplementedException;
import com.aawhere.lang.enums.EnumUtil;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.FromString;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessageFactory;

/**
 * Translates from an Enumeration to a type supported by {@link FieldDataType#ENUM}
 * 
 * @author Brian Chapman
 * 
 */
public class EnumDataTypeAtomAdapter
		extends DataTypeAdapter<Enum<?>> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldData marshal(Object adaptee) {
		if (adaptee instanceof Enum) {
			return new FieldData.Builder().setValue(((Enum<?>) adaptee).toString()).setType(getFieldDataType()).build();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.EnumDataTypeAdapter#unmarshal(com.aawhere.field.type.FieldData,
	 * java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <E extends Enum<?>> E unmarshal(FieldData adapted, Class<E> type) throws BaseException {
		Object fieldDataValue = adapted.getValue();
		if (fieldDataValue instanceof String) {
			String value = (String) fieldDataValue;
			if (Message.class.isAssignableFrom(type) && Enum.class.isAssignableFrom(type)) {
				MessageFactory messageFactory = MessageFactory.getInstance();
				return (E) messageFactory.enumFor(messageFactory
						.keyForEnum((Class<? extends Enum<? extends Message>>) type, value));
			} else if (Enum.class.isAssignableFrom(type)) {
				try {
					return FromString.valueOf(type, value);
				} catch (NoSuchMethodException e) {
					throw new RuntimeException(e);
				}

			}
		}
		// Nothing matched.
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Class<Enum<?>> getType() {
		return (Class<Enum<?>>) (Class<?>) Enum.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.DataTypeAdapter#getFieldDataType()
	 */
	@Override
	public FieldDataType getFieldDataType() {
		return FieldDataType.ATOM;
	}

}
