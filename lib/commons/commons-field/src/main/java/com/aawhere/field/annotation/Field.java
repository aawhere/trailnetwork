/**
 *
 */
package com.aawhere.field.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Dictionary;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.field.type.FieldDataType;

/**
 * A way to declare {@link com.aawhere.field.Field} and registering it in a {@link Dictionary} using
 * only annotations on a class.
 * 
 * TODO: Add ability to accept Message for name and description. Although Messages are enumerations,
 * all we can declare here is Message which is not an acceptable parameter for an annotation. It
 * must be explicity enum.
 * 
 * 
 * @author Aaron Roller
 * 
 */
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Field {

	static final String UNDEFINED = "UNDEFINED";

	static class None
			extends XmlAdapter<Object, Object> {
		private None() {
		}

		@Override
		public Object unmarshal(Object v) throws Exception {
			return null;
		}

		@Override
		public Object marshal(Object v) throws Exception {
			return null;
		}
	}

	/**
	 * @see com.aawhere.field.FieldDefinition
	 * @return
	 */
	String key() default UNDEFINED;

	/**
	 * Optional datatype to indicate to external systems that support only primitive data types what
	 * datatype this field should be mapped to. If not present, the datatype should be mapped by the
	 * implementing system automatically. If present, this datatype should override the automatic
	 * mapping provided by the implementing system.
	 * 
	 * @return
	 */
	FieldDataType dataType() default FieldDataType.NONE;

	/**
	 * A flag to indicate that this field should be indexed in a search engine (such as appengine's
	 * search api).
	 * 
	 * @return
	 */
	boolean searchable() default false;

	/**
	 * Used to identity the parameter type declared in the Field's type since, due to Type Erasure,
	 * reflection will not provide it at runtime. This is required for Collections and other types
	 * where the Field is not able to be created without knowing the parameter.
	 * 
	 * @return the type identified in the parameter for the type of this field.
	 */
	Class<?> parameterType() default None.class;

	/**
	 * References the issue(s) that require this field to be indexed so it may be queried.
	 * 
	 * @return one or more strings explaining why the index is needed
	 * 
	 * */
	String[] indexedFor() default {};

	/**
	 * Identifies the xml adapter that is to be replaced if custom adaptation is needed (i.e. for
	 * optional showing of fields, batch loading of included fields, etc). Since JAXB Marshaling
	 * uses adapter types for identification it is recommended to create a unique class for each
	 * class that may use an adapter that may need to be replaced.
	 */
	Class<? extends XmlAdapter<?, ?>> xmlAdapter() default None.class;

}
