/**
 *
 */
package com.aawhere.field;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import javax.annotation.Nonnull;

import com.aawhere.lang.If;
import com.aawhere.lang.exception.ToRuntimeException;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Produces fields and keys based on the registration of such fields in the system.
 * 
 * These fields can be used to identify Class attributes as business items important to an external
 * client through web services and data formats.
 * 
 * Each FieldDictionary has a "domain" which guarantees uniqueness within the system. So a domain
 * can only be registered once and a key may only be registered once within each domain. The static
 * portion of this class maintains all instances of {@link FieldDictionaryFactory}s known within the
 * system and are available be {@link #getInstance(String)} and built by the {@link Builder}.
 * 
 * @author roller
 * 
 */
@Singleton
public class FieldDictionaryFactory {

	/** constructor available for Guice or local utils to produce the singleton instance */
	@Inject
	public FieldDictionaryFactory() {
	}

	/** The factories referenced by their domain key */
	private HashMap<String, FieldDictionary> dictionaries = new HashMap<String, FieldDictionary>();
	/** A global registration of absolute keys and their corresponding definitions for easy access. */
	private HashMap<FieldKey, FieldDefinition<?>> definitions = new HashMap<FieldKey, FieldDefinition<?>>();

	/**
	 * get's the instance keyed by the domainKey.
	 * 
	 * @param domainKey
	 * @return the {@link FieldDictionaryFactory}
	 * @throws UnsupportedOperationException
	 *             when an instance doesn't already exist. Use {@link #instanceExists(String)} to
	 *             check first.
	 */
	public @Nonnull
	FieldDictionary getDictionary(@Nonnull String domainKey) throws DictionaryNotRegisteredException {
		FieldDictionary dictionary = dictionaries.get(domainKey);

		if (dictionary == null) {
			throw new DictionaryNotRegisteredException(domainKey);
		}
		return dictionary;
	}

	/**
	 * Registers
	 * 
	 * @param domainKey
	 */
	public synchronized FieldDictionaryFactory register(@Nonnull FieldDictionary dictionary) {
		String domainKey = dictionary.getDomainKey();
		// disabled to allow nested dictionary keys (i.e. top-nested)
		// FieldUtils.validate(domainKey);

		FieldDictionary instance = dictionaries.get(domainKey);
		if (instance == null) {
			dictionaries.put(domainKey, dictionary);
			definitions.putAll(dictionary.getFieldMap());
		} else {
			If.notEqual(instance.getFields(), dictionary.getFields())
					.raise(new IllegalArgumentException(domainKey + " already exists with different fields"
							+ dictionary.getFields() + " vs. " + instance.getFields()));
		}
		return this;
	}

	/**
	 * Provides all registered dictionaries.
	 * 
	 * @return
	 */
	public FieldDictionaries getAllDictionaries() {
		// this could easily be cached during register if there is a performance issue.
		return new FieldDictionaries.Builder().addAll(this.dictionaries.values()).build();
	}

	/**
	 * @param key
	 * @return the field {@link #register(FieldDefinition) registered} with the matching key. Never
	 *         returns null
	 * @throws FieldNotRegisteredException
	 *             when no matching definition is found.
	 */
	@SuppressWarnings("unchecked")
	public @Nonnull
	<T> FieldDefinition<T> findDefinition(@Nonnull FieldKey key) throws FieldNotRegisteredException {
		FieldDefinition<?> def = definitions.get(key);
		if (def == null) {
			throw new FieldNotRegisteredException(key, definitions.keySet());
		} else {
			return (FieldDefinition<T>) def;
		}
	}

	/**
	 * Provides all definitions registered within the system.
	 * 
	 * Use {@link #getDictionary()} to get definitions for an instance.
	 * 
	 * @return
	 */
	public Collection<FieldDefinition<?>> getDefinitions() {

		return Collections.unmodifiableCollection(definitions.values());
	}

	/**
	 * Used to check if a domain exists. Useful when lazy creating.
	 * 
	 * @param domain
	 * @return
	 */
	public boolean instanceExists(String domain) {

		return dictionaries.containsKey(domain);
	}

	/**
	 * Given the absolute key of a {@link Field} this will return the registered FieldDefinition or
	 * throw {@link FieldNotRegisteredException} if it is not found. This method is available for
	 * external clients to use as a string-based query.
	 * 
	 * Those users of this Factory that are within the system should declare a Field Key that is
	 * know and call {@link #getDefinition(FieldKey)} which will throw a runtime.
	 * 
	 * @param absoluteKey
	 * @return
	 * @throws FieldNotRegisteredException
	 */
	@SuppressWarnings("unchecked")
	public <ValueT> FieldDefinition<ValueT> findDefinition(String absoluteKey) throws FieldNotRegisteredException {
		return (FieldDefinition<ValueT>) findDefinition(new FieldKey(absoluteKey));
	}

	/**
	 * Synonymous to {@link #findDefinition(FieldKey)}, but a runtime exception is thrown for use
	 * when a developer knows this field exists or coding error.
	 * 
	 * @param key
	 * @return
	 */
	public <ValueT> FieldDefinition<ValueT> getDefinition(FieldKey key) {
		try {
			return findDefinition(key);
		} catch (FieldNotRegisteredException e) {
			throw ToRuntimeException.wrapAndThrow(e);
		}
	}

	public <ValueT> FieldDefinition<ValueT> getDefinition(String absoluteKey) {
		try {
			return findDefinition(absoluteKey);
		} catch (FieldNotRegisteredException e) {
			throw ToRuntimeException.wrapAndThrow(e);
		}
	}

	/**
	 * Allows for proactive checking of a string as an existing definition avoiding exceptions.
	 * 
	 * @param key
	 * @return
	 */
	public boolean hasDefinition(String key) {
		try {
			FieldKey fieldKey = FieldUtils.key(key);
			return this.definitions.containsKey(fieldKey);
		} catch (Exception e) {
			return false;
		}

	}

}
