/**
 *
 */
package com.aawhere.field;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * {@link XmlAdapter} for Record.
 * 
 * @author brian
 * 
 */
public class RecordXmlAdapter
		extends XmlAdapter<XmlRecord, Record> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public XmlRecord marshal(Record record) throws Exception {
		XmlRecord xmlRecord = new XmlRecord();
		for (Field<?> field : record) {
			xmlRecord.fields.add(field);
		}
		return xmlRecord;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Record unmarshal(XmlRecord xmlRecord) throws Exception {
		Record.Builder builder = new Record.Builder();
		for (Field<?> field : xmlRecord.fields) {
			builder.addField(field);
		}
		return builder.build();
	}

}
