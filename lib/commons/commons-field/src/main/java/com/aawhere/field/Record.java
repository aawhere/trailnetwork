/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 4, 2010
commons-lang : com.aawhere.field.Record.java
 */
package com.aawhere.field;

import java.io.Serializable;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections.OrderedMap;
import org.apache.commons.collections.map.LinkedMap;

import com.aawhere.lang.ObjectBuilder;

/**
 * Made up of many related {@link Field}s (aka row).
 * 
 * Contained within a {@link Report} (composite relationship) containing many related {@link Field}s
 * (composite relationship) that provide values for the assigned {@link RecordSchema} (column).
 * 
 * This is also known as a Row in the Table analogy.
 * 
 * @author roller
 * 
 */
@XmlJavaTypeAdapter(RecordXmlAdapter.class)
@XmlAccessorType(XmlAccessType.NONE)
public class Record
		implements Serializable, Iterable<Field<?>> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8429249158910806679L;

	/**
	 * Used to construct all instances of Record.
	 */
	public static class Builder
			extends ObjectBuilder<Record> {

		/**
		 * @see Report.Builder#nextRecord()
		 * 
		 */
		public Builder() {
			super(new Record());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {

			super.validate();
			// some reports exclude fields and may have not fields like if values are null
			// assertNotEmpty("fields", building.fields.values());
			// assertNotNull("schema", building.schema);
		}

		/**
		 * Add a field to the record. Fields should be added in the exact order as the
		 * {@link RecordSchema}.
		 * 
		 * @return
		 * @throws ClassCastException
		 *             when a value is provided that is not compatible with the next definition in
		 *             the schema.
		 * 
		 *             TODO: Add schema safety into building this so field can be used twice or a
		 *             definition not found can't be added
		 */
		@SuppressWarnings("unchecked")
		public <ValueT> Builder addField(Field<ValueT> field) {
			building.fields.put(field.getDefinition().getKey(), field);
			return this;
		}

		/**
		 * @see #addField(Field)
		 * 
		 * @param value
		 * @param definition
		 * @return
		 */
		public <ValueT> Builder addField(FieldDefinition<ValueT> definition, ValueT value) {
			Field<ValueT> field = new Field.Builder<ValueT>().setDefinition(definition).setValue(value).build();
			return addField(field);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public Record build() {
			return super.build();
		}

	}// end Builder

	private OrderedMap fields = new LinkedMap();

	/** Use {@link Builder} to construct Record */
	private Record() {
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return this.fields.values().toString();
	}

	@SuppressWarnings("unchecked")
	public <ValueT> Field<ValueT> getField(FieldKey key) {
		return (Field<ValueT>) this.fields.get(key);
	}

	/**
	 * @param definition
	 * @return
	 */
	public <ValueT> Field<ValueT> getField(FieldDefinition<ValueT> definition) {
		return getField(definition.getKey());
	}

	/**
	 * @param key
	 * @return
	 */
	public boolean hasField(FieldKey key) {
		return this.fields.containsKey(key);
	}

	public boolean hasField(FieldDefinition<?> definition) {
		return hasField(definition.getKey());
	}

	public Integer getFieldCount() {
		return this.fields.size();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Iterator<Field<?>> iterator() {

		return this.fields.values().iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.fields == null) ? 0 : this.fields.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Record other = (Record) obj;
		if (this.fields == null) {
			if (other.fields != null) {
				return false;
			}
		} else if (!this.fields.equals(other.fields)) {
			return false;
		}
		return true;
	}

}
