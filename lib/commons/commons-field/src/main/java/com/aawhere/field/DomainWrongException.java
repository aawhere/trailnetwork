package com.aawhere.field;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

@StatusCode(value = HttpStatusCode.BAD_REQUEST, message = "{EXPECTED} domain was expected, but was {ACTUAL}")
public class DomainWrongException
		extends BaseRuntimeException {

	private static final long serialVersionUID = -8582939229345269149L;

	public DomainWrongException(String expectedDomain, String actualDomain) {
		super(DomainWrongExceptionMessage.message(expectedDomain, actualDomain));
	}

}
