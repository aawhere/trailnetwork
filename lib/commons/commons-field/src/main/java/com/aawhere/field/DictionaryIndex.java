/**
 * 
 */
package com.aawhere.field;

import java.io.Serializable;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

import com.aawhere.field.annotation.Field;

import com.google.common.collect.ImmutableSet;

/**
 * used with {@link Field} to report a combination index or an index on a single property that
 * cannot be declared at the property level. The implementation must provide a public default
 * constructor and implement the required methods.
 * 
 * * TN-352 TN-182
 * 
 * @author aroller
 * @since 11
 * 
 */
// @XmlType(namespace = XmlNamespace.API_VALUE)
// @XmlAccessorType(XmlAccessType.NONE)
// @XmlJavaTypeAdapter(DictionaryIndexXmlAdapter.class)
abstract public class DictionaryIndex
		implements Serializable {

	private static final long serialVersionUID = 6608816681685002304L;
	private final Set<FieldKey> keys;

	/**
	 * 
	 */
	protected DictionaryIndex(FieldKey... fieldKeys) {
		this.keys = ImmutableSet.copyOf(fieldKeys);
	}

	/**
	 * @param keys
	 */
	public DictionaryIndex(Set<FieldKey> keys) {
		this.keys = ImmutableSet.copyOf(keys);
	}

	/**
	 * Provides all the keys in the combination query. This must match a declaration in the
	 * datastore-indexes.xml
	 * 
	 * @return
	 */
	@XmlElement
	public final Set<FieldKey> keys() {
		return keys;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.keys == null) ? 0 : this.keys.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DictionaryIndex other = (DictionaryIndex) obj;
		if (this.keys == null) {
			if (other.keys != null)
				return false;
		} else if (!this.keys.equals(other.keys))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.valueOf(this.keys);
	}

}
