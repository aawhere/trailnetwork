/**
 * 
 */
package com.aawhere.field;

import java.util.List;
import java.util.Set;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Communicates that one or more fields are not indexed, but a query has been requested.
 * 
 * @author aroller
 * @since 11
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class FieldNotIndexedException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 7437521223034760116L;

	/**
	 * 
	 */
	public FieldNotIndexedException(FieldKey field) {
		super(CompleteMessage.create(FieldMessage.FIELD_NOT_INDEXED).addParameter(FieldMessage.Param.FIELD_KEY, field)
				.build());
	}

	/**
	 * Indicates a combination index is required.
	 * 
	 * @param keysRequiredIndexes
	 * @param indexes
	 */
	public FieldNotIndexedException(Set<FieldKey> keysRequiredIndexes, String domain, List<DictionaryIndex> indexes) {
		super(CompleteMessage.create(FieldMessage.FIELDS_NOT_INDEXED)
				.addParameter(FieldMessage.Param.FIELD_KEYS, keysRequiredIndexes)
				.addParameter(FieldMessage.Param.DOMAIN, domain).addParameter(FieldMessage.Param.INDEXES, indexes)
				.build());
	}
}
