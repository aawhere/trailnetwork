/**
 *
 */
package com.aawhere.field.type;

import com.aawhere.field.Field;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * Holds the value of a {@link Field} after it has been converted to a {@link FieldDataType} along
 * with the {@link FieldDataType}
 * 
 * @author Brian Chapman
 * 
 */
public class FieldData {

	private Object data;
	private FieldDataType type;

	@SuppressWarnings("unchecked")
	public <T> T getValue() {
		return (T) data;
	}

	public FieldDataType getType() {
		return type;
	}

	/**
	 * Used to construct all instances of FieldData.
	 */
	public static class Builder
			extends ObjectBuilder<FieldData> {

		public Builder() {
			super(new FieldData());
		}

		public Builder setType(FieldDataType type) {
			building.type = type;
			return this;
		}

		public Builder setValue(Object value) {
			building.data = value;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			// previously validated that type is assignable from the object, but that logic doesn't
			// work for string conversions to other objects during unmarshalling
		}
	}// end Builder

	/** Use {@link Builder} to construct FieldData */
	private FieldData() {
	}

	public static Builder create() {
		return new Builder();
	}

}
