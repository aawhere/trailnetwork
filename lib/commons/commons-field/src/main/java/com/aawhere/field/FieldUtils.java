/**
 *
 */
package com.aawhere.field;

import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.field.type.DataTypeAdapter;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.string.StringInvalidCharacterFinder;
import com.aawhere.lang.string.predicate.LowerCaseLetterPredicate;
import com.aawhere.util.rb.CompleteMessage;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;

/**
 * Field utility methods.
 * 
 * @author Brian Chapman
 * 
 */
public class FieldUtils {

	// FIXME: TN-779 can make this protected again
	public static final String KEY_DILIM = "-";
	static final int MAX_LENGTH = 50;

	public static FieldData adaptToDataType(Field<?> field, DataTypeAdapterFactory factory) {
		return adaptToDataType(field.getDefinition().getType(), field.getDefinition().getParameterType(), field
				.getDefinition().getDataType(), field.getValue(), factory);
	}

	public static FieldData adaptToDataType(Class<?> type, Class<?> paramaterType, FieldDataType dataType,
			Object value, DataTypeAdapterFactory factory) {
		DataTypeAdapter<?> adapter = (DataTypeAdapter<?>) factory.getAdapter(type, dataType, paramaterType);
		return adapter.marshal(value);
	}

	public static RecordSchema createRecordSchema(Record record) {
		RecordSchema.Builder schemaBuidler = new RecordSchema.Builder();
		for (Field<?> field : record) {
			schemaBuidler.addDefinition(field.getDefinition());
		}
		RecordSchema schema = schemaBuidler.build();
		return schema;
	}

	public static FieldKey key(String absoluteKey) {
		return new FieldKey(absoluteKey);
	}

	/**
	 * This method should be on dictionary itself.
	 * 
	 * @param fullKey
	 * @return
	 */
	static String[] parse(String fullKey) throws InvalidFieldKeyFormatException {
		String[] strings = fullKey.split(KEY_DILIM.toString());
		if (strings.length < 2) {
			throw new InvalidFieldKeyFormatException(new CompleteMessage.Builder(FieldMessage.MISSING_DOMAIN)
					.addParameter(FieldMessage.Param.FIELD_KEY, fullKey)
					.addParameter(FieldMessage.Param.DILIMETER, KEY_DILIM).build());
		}
		// anything else is a nested key.
		// we must get the key from the dictionary
		// The domain of the key must be available from the main field's definition.

		return strings;
	}

	/**
	 * @param definition
	 */
	public static Boolean definesComparableType(final FieldDefinition<?> definition) {
		Class<?> type = definition.getType();
		return Comparable.class.isAssignableFrom(type);
	}

	/**
	 * True if the key is from the given domain.
	 * 
	 * @param domain
	 * @param key
	 * @return
	 */
	public static Boolean isFromDomain(String domain, FieldKey key) {
		// using starts with because sometimes embedded keys have additional subdomains.
		return (domain == null || key == null) ? false : key.getDomainKey().startsWith(domain);
	}

	/**
	 * Builds the key parts into a single string. domainKey-keyValue
	 * 
	 * @param domainKey
	 * @param keyValue
	 * @return
	 */
	static String join(String domainKey, String keyValue) {
		return join(domainKey, keyValue, null);
	}

	/**
	 * Builds the key parts into a singleString. domainKey-keyValue-nested1-nested2
	 * 
	 * @param domainKey
	 * @param keyValue
	 * @param nested
	 *            this key represents an embedded object.
	 * 
	 * @return
	 */
	static String join(String domainKey, String keyValue, FieldKey nested) {
		StringBuilder compoundKeyValue = new StringBuilder(domainKey);
		compoundKeyValue.append(KEY_DILIM);
		compoundKeyValue.append(keyValue);
		join(compoundKeyValue, nested);
		return compoundKeyValue.toString();
	}

	/**
	 * Recursively joins nested keys onto the compoundKeyValue using standard notation.
	 * 
	 * @param compoundKeyValue
	 * @param nested
	 * @return
	 */
	static StringBuilder join(StringBuilder compoundKeyValue, FieldKey nested) {
		if (nested != null) {
			compoundKeyValue.append(KEY_DILIM);
			// releative key provides all the nested fields within the nested key
			compoundKeyValue.append(nested.getRelativeKey());
			FieldKey nestedNested = nested.getNested();
			if (nestedNested != null) {
				join(compoundKeyValue, nestedNested);
			}
		}
		return compoundKeyValue;
	}

	/**
	 * Used by the constructor of this class and available for anyone to discover values that are
	 * not valid for a key.
	 * 
	 * @param key
	 * @return
	 * @throws InvalidFieldKeyFormatException
	 *             , InvalidFieldKeyLengthException
	 */
	static void validate(String key) throws InvalidFieldKeyFormatException, InvalidFieldKeyLengthException {
		Integer maxLength = MAX_LENGTH;
		if (key.length() > maxLength) {
			throw new InvalidFieldKeyLengthException(key, maxLength);
		}

		StringInvalidCharacterFinder finder = new StringInvalidCharacterFinder.Builder(key).setDigitsAreAllowed(true)
				.setLettersAreAllowed(true).addPredicate(LowerCaseLetterPredicate.getInstance()).build();

		if (!finder.getIndexes().getAll().isEmpty()) {
			throw new InvalidFieldKeyFormatException(key, finder.getHighlightedInvalidCharacters());
		}

	}

	/**
	 * Indicates if the string given could be a key since it matches the format of a key.
	 * 
	 * @param key
	 *            any string that you are interested in knowing is a key. Null returns false.
	 */
	@Nonnull
	public static Boolean matchesKeyFormat(@Nullable String key) {
		return (key != null) ? key.contains(KEY_DILIM) : false;
	}

	/**
	 * @param expandableDefinitions
	 * @return
	 */
	public static Iterable<FieldKey> keys(Iterable<FieldDefinition<?>> defs) {
		return Iterables.transform(defs, keysFromDefinitionFunction());
	}

	public static Function<FieldDefinition<?>, FieldKey> keysFromDefinitionFunction() {
		return new Function<FieldDefinition<?>, FieldKey>() {

			@Override
			public FieldKey apply(FieldDefinition<?> input) {
				return (input != null) ? input.getKey() : null;
			}
		};
	}

	/**
	 * @param indexedDefinitions
	 * @return
	 */
	public static List<FieldDefinition<?>> sortedByKey(Iterable<FieldDefinition<?>> definitions) {
		return Ordering.natural().nullsLast().onResultOf(FieldUtils.keysFromDefinitionFunction())
				.sortedCopy(definitions);
	}

	public static Iterable<FieldKey> keysFromAbsolute(Iterable<String> absoluteFieldValues) {
		return Iterables.transform(absoluteFieldValues, absoluteToKeyFunction());
	}

	public static Function<String, FieldKey> absoluteToKeyFunction() {
		return new Function<String, FieldKey>() {

			@Override
			public FieldKey apply(String input) {
				return (input != null) ? key(input) : null;
			}
		};
	}

	/**
	 * Given the domain and stream of relative keys this will form field keys.
	 * 
	 * @param domain
	 * @param fieldRelativeKeys
	 * @return
	 */
	public static Iterable<FieldKey> fieldKeys(String domain, Iterable<String> fieldRelativeKeys) {
		return Iterables.transform(fieldRelativeKeys, fieldKeyFunction(domain));
	}

	/**
	 * Given the domain upfront this will transform the stream of relative keys into a full field
	 * key.
	 * 
	 * @param domain
	 * @return
	 */
	public static Function<String, FieldKey> fieldKeyFunction(final String domain) {
		return new Function<String, FieldKey>() {

			@Override
			public FieldKey apply(String relativeKey) {
				return (relativeKey != null) ? new FieldKey(domain, relativeKey) : null;
			}
		};
	}
}
