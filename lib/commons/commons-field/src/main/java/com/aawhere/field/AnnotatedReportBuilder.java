/**
 *
 */
package com.aawhere.field;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.aawhere.field.AnnotatedRecordBuilder.AnnotatedRecordBuilderFlags;
import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.lang.Assertion;
import com.aawhere.util.rb.StringMessage;

/**
 * This report is generated using annotations on an entity. It will recursively walk through fields
 * on an entity and see if their types have field annotations on them as well.
 * 
 * @author Brian Chapman
 * 
 */
// Note: this doesn't use ObjectBuilder or extend Report.builder as a composite relationship seemed
// more appropriate.
public class AnnotatedReportBuilder {

	private List<Object> objects = new ArrayList<Object>();
	private FieldAnnotationDictionaryFactory fieldFactory;
	private Set<AnnotatedRecordBuilderFlags> flags = new HashSet<>();

	public AnnotatedReportBuilder(FieldAnnotationDictionaryFactory fieldFactory) {
		this.fieldFactory = fieldFactory;
	}

	public AnnotatedReportBuilder addObject(Object object) {
		objects.add(object);
		return this;
	}

	public AnnotatedReportBuilder addFlag(AnnotatedRecordBuilderFlags flag) {
		this.flags.add(flag);
		return this;
	}

	public Report build() {
		Assertion.assertNotEmpty(this.objects);

		Report.Builder reportBuilder = new Report.Builder();

		String domain = null;
		Boolean hasSchema = false;
		for (Object object : objects) {
			domain = AnnotatedDictionaryUtils.domainFrom(object.getClass());
			Record record = new AnnotatedRecordBuilder(fieldFactory).setFlag(AnnotatedRecordBuilderFlags.FLATTEN)
					.addFlags(flags).setAnnotatedObject(object).build();
			if (!hasSchema) {
				RecordSchema schema = FieldUtils.createRecordSchema(record);
				reportBuilder.setSchema(schema);
				hasSchema = true;
			}
			reportBuilder.addRecord(record);
		}
		reportBuilder.setName(new StringMessage(domain));

		return reportBuilder.build();
	}

}
