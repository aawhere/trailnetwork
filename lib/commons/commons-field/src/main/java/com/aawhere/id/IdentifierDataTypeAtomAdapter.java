/**
 *
 */
package com.aawhere.id;

import com.aawhere.field.type.DataTypeAdapter;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.NotImplementedException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.FromStringAdapter;

/**
 * 
 * 
 * @author Brian Chapman
 * 
 */
public class IdentifierDataTypeAtomAdapter
		extends DataTypeAdapter<Identifier<?, ?>> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldData marshal(Object adaptee) {
		if (adaptee instanceof Identifier) {
			Identifier<?, ?> id = (Identifier<?, ?>) adaptee;
			return new FieldData.Builder().setValue(id.getValue().toString()).setType(getFieldDataType()).build();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#unmarshal(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <A extends Identifier<?, ?>> A unmarshal(FieldData adapted, Class<A> type) throws BaseException {
		Object fieldDataValue = adapted.getValue();
		if (fieldDataValue instanceof String) {
			String value = (String) fieldDataValue;
			return (new FromStringAdapter.Builder<A>()).setType(type).build().unmarshal(value, type);
		} else {
			throw new NotImplementedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Class<Identifier<?, ?>> getType() {
		return (Class<Identifier<?, ?>>) (Class<?>) Identifier.class;
	}

	@Override
	public FieldDataType getFieldDataType() {
		return FieldDataType.ATOM;
	}

}
