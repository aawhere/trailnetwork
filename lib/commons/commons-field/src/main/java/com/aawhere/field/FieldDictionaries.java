/**
 * 
 */
package com.aawhere.field;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

import com.wordnik.swagger.annotations.ApiModel;

/**
 * Simple wrapper for a collection of {@link FieldDictionary}.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@ApiModel(description = "Detailed FieldDictionary information provided for each domain.")
public class FieldDictionaries {

	@XmlElement(name = "dictionary")
	private Set<FieldDictionary> all = new HashSet<FieldDictionary>();

	/**
	 * Used to construct all instances of FieldDictionaries.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<FieldDictionaries> {

		public Builder() {
			super(new FieldDictionaries());
		}

		public Builder add(FieldDictionary dictionary) {
			building.all.add(dictionary);
			return this;
		}

		/**
		 * @param dictionaries
		 * @return
		 */
		public Builder addAll(Collection<FieldDictionary> dictionaries) {
			building.all.addAll(dictionaries);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public FieldDictionaries build() {
			building.all = Collections.unmodifiableSet(building.all);
			return super.build();
		}

	}// end Builder

	/** Use {@link Builder} to construct FieldDictionaries */
	private FieldDictionaries() {
	}

	/**
	 * @return the all
	 */
	public Set<FieldDictionary> getAll() {
		return this.all;
	}

}
