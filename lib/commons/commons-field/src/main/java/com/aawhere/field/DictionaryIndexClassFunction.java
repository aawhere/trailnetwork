/**
 * 
 */
package com.aawhere.field;

import javax.annotation.Nullable;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;

/**
 * Translates the common situation of Classes declared in the {@link Dictionary} annotation into the
 * corresponding instance for dictionary assignment.
 * 
 * @author aroller
 * 
 */
public class DictionaryIndexClassFunction
		implements Function<Class<? extends DictionaryIndex>, DictionaryIndex> {

	/**
	 * Used to construct all instances of CombinationIndexClassFunction.
	 */
	public static class Builder
			extends ObjectBuilder<DictionaryIndexClassFunction> {

		public Builder() {
			super(new DictionaryIndexClassFunction());
		}

		public DictionaryIndexClassFunction build() {
			DictionaryIndexClassFunction built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct CombinationIndexClassFunction */
	private DictionaryIndexClassFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public DictionaryIndex apply(@Nullable Class<? extends DictionaryIndex> input) {
		try {
			return input.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return
	 */
	public static Function<? super Class<? extends DictionaryIndex>, ? extends DictionaryIndex> build() {
		return create().build();
	}

}
