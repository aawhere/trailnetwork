/**
 *
 */
package com.aawhere.field;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang3.ArrayUtils;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.util.rb.Message;
import com.aawhere.xml.XmlNamespace;

/**
 * Provides the definition for a single data that can be used by external systems to represent
 * uniquely an item within the current system. These fields will often be persisted to a database
 * and would related to a Column of a table.
 * 
 * TODO: Provides the restrictions and formats available for a value that can be used to validate
 * against the field value in a generic way.
 * 
 * 
 * The definition is first identified by the ValueT template which requires a declaration of the
 * object type this definition represents.
 * 
 * @author roller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class FieldDefinition<ValueT>
		implements Serializable, Cloneable {

	private static final long serialVersionUID = -8337617389887175406L;

	/**
	 * Used to construct all instances of FieldDefinition.
	 */
	@XmlTransient
	public static class Builder<ValueT>
			extends ObjectBuilder<FieldDefinition<ValueT>> {

		/**
		 * Available for Dictionaries to create.
		 * 
		 * @param stringFieldDefinition
		 */
		public Builder() {
			this(new FieldDefinition<ValueT>());

		}

		/**
		 * @param clone
		 */
		Builder(FieldDefinition<ValueT> mutable) {
			super(mutable);
		}

		public Builder<ValueT> setParameterType(Class<?> type) {
			building.parameterType = type;
			return this;
		}

		/**
		 * Called by {@link RecordSchema} builder Use {@link #getKey()} to retrieve the built key.
		 * 
		 * @param key
		 * @return
		 * @throws DuplicateFieldRegistrationException
		 */
		public Builder<ValueT> setKey(FieldKey key) {
			building.key = key;

			return this;
		}

		/**
		 * Optional columnName provided if it differs from the key.
		 * 
		 * @param columnName
		 * @return
		 */
		public Builder<ValueT> setColumnName(String columnName) {
			building.columnName = columnName;
			return this;
		}

		public Builder<ValueT> setXmlAdapterType(Class<? extends XmlAdapter<?, ?>> xmlAdapter) {
			building.xmlAdapterType = xmlAdapter;
			return this;
		}

		/**
		 * @param description
		 *            the description to set
		 * @return
		 */
		public Builder<ValueT> setDescription(Message description) {
			building.description = description;
			return this;
		}

		/**
		 * @param name
		 *            the name to set
		 * @return
		 */
		public Builder<ValueT> setName(Message name) {
			building.name = name;
			return this;
		}

		public Builder<ValueT> setType(Class<ValueT> type) {
			building.type = type;
			return this;
		}

		public Builder<ValueT> setIndexedFor(String[] indexedFor) {
			building.indexedFor = indexedFor;
			return this;
		}

		public Builder<ValueT> setDataType(FieldDataType dataType) {
			building.dataType = dataType;
			return this;
		}

		public Builder<ValueT> setSearchable(Boolean searchable) {
			building.searchable = searchable;
			return this;
		}

		public Builder<ValueT> setNestedDictionary(FieldDictionary nested) {
			building.nested = nested;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("key", building.key);
			Assertion.assertNotNull("type", building.type);
		}

		/**
		 * Creates a Mutable builder for a COPY of the provided definition and assigns the given
		 * key.
		 * 
		 * @param nestedKey
		 * @param fieldDefinition
		 */
		@SuppressWarnings("unchecked")
		public static <ValueT> Builder<ValueT> create(FieldKey alternateKey, FieldDefinition<ValueT> fieldDefinition) {
			try {
				FieldDefinition<ValueT> clone = (FieldDefinition<ValueT>) fieldDefinition.clone();
				Builder<ValueT> builder = new Builder<ValueT>(clone);
				builder.setKey(alternateKey);
				return builder;
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}

		/**
		 * @param flatten
		 * @return
		 */
		public Builder<ValueT> flatten(Boolean flatten) {
			building.flattened = flatten;
			return this;
		}

		/**
		 * The enclosing dictionary for this field.
		 * 
		 * @param building
		 */
		public void setDictionary(FieldDictionary dictionary) {
			building.dictionary = dictionary;
		}
	}

	/** user {@link Builder} to construct FieldDefinition */
	protected FieldDefinition() {
	}

	@XmlTransient
	public FieldDictionary dictionary;

	/**
	 * The business key that may be used to represent this field definition. This must be unique
	 * within the entire system.
	 */
	@XmlElement
	private FieldKey key;

	/**
	 * Optional datastore column name if it differs from the key.
	 */
	@XmlElement
	private String columnName;
	/**
	 * Short and sweet, but with enough explanation to make it unique and relevant. Should relate to
	 * key.
	 */
	@XmlElement
	private Message name;
	/** Describes the purpose of the field. */

	@XmlElement
	private Message description;

	@XmlAttribute
	private Boolean flattened;

	/**
	 * Describes the type of object this definition handles. This is used to properly translate
	 * strings to objects.
	 */
	@XmlAttribute
	private Class<ValueT> type;

	/**
	 * References to why this field is indexed. Typically the strings are JIRA Issues.
	 * 
	 */
	@XmlList
	private String[] indexedFor;

	/**
	 * Indicates the type contained within the {@link #type}. This is required for Collections or
	 * other generic objects that don't identify what concrete type is contained in the field.
	 * 
	 * This is necessary due to Type Erasure.
	 */
	@XmlAttribute
	private Class<?> parameterType;

	/**
	 * @see FieldDataType
	 */
	@XmlElement
	private FieldDataType dataType;

	/**
	 * Flag that indicates that this field should be searchable via a search system.
	 */
	@XmlAttribute
	private Boolean searchable;

	/**
	 * When a field type is nested {@link Dictionary} (embedded) then this will provide that
	 * {@link FieldDictionary} so the fields will be available relative to this dictionary.
	 */
	@XmlElement
	private FieldDictionary nested;

	@XmlElement
	private Class<? extends XmlAdapter<?, ?>> xmlAdapterType;

	/**
	 * @return the key
	 */
	@Nonnull
	public FieldKey getKey() {
		return key;
	}

	/**
	 * The declaring dictionary where the field can be found.
	 * 
	 * @return the dictionary
	 */
	@Nonnull
	public FieldDictionary getDictionary() {
		return this.dictionary;
	}

	/**
	 * @return the name
	 */
	public Message getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public Message getDescription() {
		return description;
	}

	/**
	 * @return the type
	 */
	@Nonnull
	public Class<ValueT> getType() {
		return this.type;
	}

	/**
	 * @see #parameterType
	 * 
	 * @return the parameterType
	 */
	@Nullable
	public Class<?> getParameterType() {
		return this.parameterType;
	}

	public Boolean hasParameterType() {
		return this.parameterType != null;
	}

	public FieldDataType getDataType() {
		return this.dataType;
	}

	/**
	 * @return the searchable
	 */
	public Boolean getSearchable() {
		return this.searchable;
	}

	/**
	 * @see #nested
	 * @return the nested
	 */
	@Nullable
	public FieldDictionary getNested() {
		return this.nested;
	}

	/**
	 * @return the indexedFor
	 */
	public String[] getIndexedFor() {
		return this.indexedFor;
	}

	/**
	 * @return the xmlAdapterType
	 */
	public Class<? extends XmlAdapter<?, ?>> getXmlAdapterType() {
		return this.xmlAdapterType;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		if (this.columnName == null) {
			return getKey().getRelativeKey();
		}
		return this.columnName;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FieldDefinition<ValueT> other = (FieldDefinition<ValueT>) obj;
		if (key == null) {
			if (other.key != null) {
				return false;
			}
		} else if (!key.equals(other.key)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.key.getValue();
	}

	/**
	 * @return
	 */
	public boolean hasXmlAdapterType() {
		return this.xmlAdapterType != null;
	}

	public boolean hasIndexedFor() {
		return ArrayUtils.isNotEmpty(this.indexedFor);
	}
}
