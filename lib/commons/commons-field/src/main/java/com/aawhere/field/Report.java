/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 4, 2010
commons-lang : com.aawhere.field.Fields.java
 */
package com.aawhere.field;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.util.rb.Message;
import com.aawhere.xml.XmlNamespace;

/**
 * Organizes a bunch of {@link Field}s into {@link Record}s in the same fashion a database table has
 * rows.
 * 
 * Has an ordered set of {@link FieldDefinition}s that acts as the columns or the "schema".
 * 
 * Unlike a table, there is not necessarily a requirement to have an "empty cell" where no field is
 * present for a particular column. For that reason, this acts differently than a table because each
 * field defines itself via {@link FieldDefinition}.
 * 
 * @author roller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class Report
		implements Serializable, Iterable<Record> {

	private static final long serialVersionUID = 6949972757222207513L;

	/**
	 * Used to construct all instances of Fields.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<Report> {

		public Builder() {
			super(new Report());
		}

		/**
		 * @param description
		 *            the description to set
		 * @return
		 */
		public Builder setDescription(Message description) {
			building.description = description;
			return this;
		}

		/**
		 * @param name
		 *            the name to set
		 * @return
		 */
		public Builder setName(Message name) {
			building.name = name;
			return this;
		}

		public Builder setSchema(RecordSchema schema) {
			building.schema = schema;
			return this;
		}

		public Builder addRecord(Record record) {
			building.records.add(record);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			// assertNotNull("name", building.name);
			// assertNotNull("description", building.description);
			Assertion.assertNotNull("schema", building.schema);
			for (Record record : building.records) {
				Iterator<Field<?>> recordIt = record.iterator();
				Iterator<FieldDefinition<?>> schemaIt = building.getSchema().iterator();
				while (schemaIt.hasNext() != false && recordIt.hasNext() != false) {
					FieldDefinition<?> schemaDefinition = schemaIt.next();
					FieldDefinition<?> recordDefinition = recordIt.next().getDefinition();
					Assertion.exceptions().eq(schemaDefinition, recordDefinition);
				}
			}
			super.validate();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public Report build() {
			building.records = Collections.unmodifiableList(building.records);
			Report report = super.build();

			return report;
		}

	}// end Builder

	/** Use {@link Builder} to construct Fields */
	private Report() {
	}

	@XmlElement
	private Message name;
	@XmlElement
	private Message description;
	@XmlElement
	private RecordSchema schema;

	@XmlElement(name = "record")
	@XmlElementWrapper
	private List<Record> records = new LinkedList<Record>();

	/**
	 * @return the name
	 */
	public Message getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public Message getDescription() {
		return description;
	}

	public List<Record> getRecords() {
		return this.records;
	}

	/**
	 * @return the schema
	 */
	public RecordSchema getSchema() {
		return schema;
	}

	@XmlAttribute(name = "count")
	public Integer getNumberOfRecords() {
		return this.records.size();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer string = new StringBuffer();
		if (this.name != null) {
			string.append("-------- ");
			string.append(this.name.getValue());
			string.append(" --------\n");
			string.append(this.description.getValue());
			string.append("\n");
		}
		if (this.schema != null) {
			string.append(this.schema);
			string.append("\n");
		}
		for (Record record : records) {
			string.append(record);
			string.append("\n");
		}
		return string.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Record> iterator() {

		return this.records.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.description == null) ? 0 : this.description.hashCode());
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		result = prime * result + ((this.records == null) ? 0 : this.records.hashCode());
		result = prime * result + ((this.schema == null) ? 0 : this.schema.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Report other = (Report) obj;
		if (this.description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!this.description.equals(other.description)) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		if (this.records == null) {
			if (other.records != null) {
				return false;
			}
		} else if (!this.records.equals(other.records)) {
			return false;
		}
		if (this.schema == null) {
			if (other.schema != null) {
				return false;
			}
		} else if (!this.schema.equals(other.schema)) {
			return false;
		}
		return true;
	}

}
