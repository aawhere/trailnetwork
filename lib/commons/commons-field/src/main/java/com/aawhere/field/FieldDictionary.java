/**
 * 
 */
package com.aawhere.field;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.util.rb.Message;
import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;

/**
 * Describes a "domain" within the system and it's associated {@link Fields}s.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class FieldDictionary
		implements Serializable, Iterable<FieldDefinition<?>> {

	private static final long serialVersionUID = -5900583365598204076L;

	public Class<?> type;

	/**
	 * Used to construct all instances of DomainDictionary.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<FieldDictionary> {

		public Builder() {
			super(new FieldDictionary());
		}

		/**
		 * @param domainKey
		 *            the domainKey to set
		 */
		public Builder setDomainKey(String domainKey) {
			// FieldUtils.validate(domainKey);
			building.domainKey = domainKey;
			return this;
		}

		/**
		 * @param domain
		 *            the domain to set
		 */
		public Builder setDomain(Message domain) {
			building.domain = domain;
			return this;
		}

		/**
		 * @param description
		 *            the description to set
		 */
		public Builder setDescription(Message description) {
			building.description = description;
			return this;
		}

		public Builder setIndexes(List<DictionaryIndex> indexes) {
			building.indexes = indexes;
			return this;
		}

		/** The class this dictionary defines. */
		public Builder setType(Class<?> type) {
			building.type = type;
			return this;
		}

		/**
		 * @param fields
		 *            the fields to set
		 */
		public Builder addFields(Collection<FieldDefinition<?>> fields) {

			for (FieldDefinition<?> fieldDefinition : fields) {
				addField(fieldDefinition);
			}
			return this;
		}

		private Builder addField(FieldDefinition<?> field) {
			If.notNull(building.fields.put(field.getKey(), field)).raise(new DuplicateFieldRegistrationException(
					field.getKey()));
			return this;
		}

		public Builder addField(FieldDefinition.Builder<?> fieldBuilder) {
			fieldBuilder.setDictionary(building);
			addField(fieldBuilder.build());
			return this;
		}

		public boolean hasField(FieldKey key) {
			return building.fields.containsKey(key);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			// allow null, but provide a warning?
			Assertion.assertNotNull(Fields.DOMAIN_KEY, building.domainKey);
			// Assertion.assertNotNull(Fields.DOMAIN, building.domain);
			Assertion.assertNotEmpty(	Fields.FIELDS + " for " + Fields.DOMAIN_KEY + " = " + building.domainKey,
										building.fields.keySet());
			// must run through each field now since domainKey is confirmed to be set.
			for (FieldKey key : building.fields.keySet()) {
				String keyDomain = key.getDomainKey();
				If.notEqual(keyDomain, building.domainKey).raise(new IllegalArgumentException(key
						+ " is attempting to be set to dictionary with this different domain: " + building.domainKey));
			}
			super.validate();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public FieldDictionary build() {
			building.fields = Maps.unmodifiableBiMap(building.fields);
			return super.build();
		}
	}// end Builder

	/** Use {@link Builder} to construct DomainDictionary */
	private FieldDictionary() {

	}

	@XmlAttribute(name = Fields.DOMAIN_KEY)
	private String domainKey;

	@XmlElement(name = Fields.DOMAIN)
	private Message domain;

	@XmlElement(name = Fields.DESCRIPTION)
	private Message description;

	// @XmlElement
	private List<DictionaryIndex> indexes;

	@XmlElement(name = Fields.FIELDS)
	@XmlJavaTypeAdapter(FieldDefinitionsMapXmlAdapter.class)
	private BiMap<FieldKey, FieldDefinition<?>> fields = HashBiMap.create();

	/**
	 * @return the domainKey
	 */
	public String getDomainKey() {
		return this.domainKey;

	}

	/**
	 * @return the domain
	 */
	public Message getDomain() {
		return this.domain;
	}

	/**
	 * @return the description
	 */
	public Message getDescription() {
		return this.description;
	}

	/**
	 * Provides read-only access to all fields.
	 * 
	 * @return the fields
	 */
	public Set<FieldDefinition<?>> getFields() {
		return this.fields.values();
	}

	/**
	 * @return the indexes
	 */
	public List<DictionaryIndex> getIndexes() {
		return this.indexes;
	}

	/**
	 * @return the type
	 */
	public Class<?> getType() {
		return this.type;
	}

	/**
	 * Provides read-only access to the definitions referenced by their key.
	 * 
	 * @return
	 */
	BiMap<FieldKey, FieldDefinition<?>> getFieldMap() {
		return this.fields;
	}

	/**
	 * @param relativeKey
	 * @return
	 * @throws FieldNotRegisteredException
	 */
	public <ValueT> FieldDefinition<ValueT> findDefinition(String relativeKey) throws FieldNotRegisteredException {

		return findDefinition(createKey(relativeKey));
	}

	/**
	 * Simply indicates if a definition exists matching the given relative key.
	 * 
	 * @param relativeKey
	 *            the key value without the domain prefix.
	 * @return
	 */
	public Boolean containsKey(String relativeKey) {
		return this.fields.containsKey(createKey(relativeKey));
	}

	/**
	 * Given a key, this will return the type-safe key that can be used to identify the field it is
	 * representing.
	 * 
	 * 
	 * 
	 * @param key
	 *            the value part of the key without the domain represented by this dictionary.
	 * @return the key registered with {@link #definitions}
	 * @throws FieldNotRegisteredException
	 *             when there is no matching keyValue forcing the programmer to deal with this
	 *             alternative
	 */
	public FieldKey findKey(String relativeKey) throws FieldNotRegisteredException {
		FieldKey key = createKey(relativeKey);
		return findDefinition(key).getKey();
	}

	/**
	 * Same as {@link #findKey(String)}, but throws a runtime when the key is being provided from
	 * within.
	 * 
	 * @param relativeKey
	 * @return
	 */
	public FieldKey getKey(String relativeKey) {
		try {
			return findKey(relativeKey);
		} catch (FieldNotRegisteredException e) {
			throw ToRuntimeException.wrapAndThrow(e);
		}
	}

	/**
	 * This produces a simply constructed key that is useful for internal queries, but not complete
	 * for external use.
	 * 
	 * @param relativeKey
	 * @return
	 */
	private FieldKey createKey(String relativeKey) {
		return new FieldKey(this.domainKey, relativeKey);

	}

	@SuppressWarnings("unchecked")
	public @Nonnull
	<T> FieldDefinition<T> findDefinition(@Nonnull FieldKey key) throws FieldNotRegisteredException {
		FieldDefinition<?> def = fields.get(key);
		if (def == null) {
			throw new FieldNotRegisteredException(key, this.fields.keySet());
		} else {
			return (FieldDefinition<T>) def;
		}
	}

	/**
	 * Same as find definition, but doesn't throw an ApplicationException assuming the develooper
	 * knows it is available for sure or its a coding error.
	 * 
	 * @see #findDefinition(String)
	 * @param typeKeyValue
	 * @return
	 */
	public <ValueT> FieldDefinition<ValueT> getDefinition(String typeKeyValue) {
		try {
			return findDefinition(typeKeyValue);
		} catch (FieldNotRegisteredException e) {
			throw new ToRuntimeException(e);
		}
	}

	public <ValueT> FieldDefinition<ValueT> getDefinition(FieldKey fieldKey) {
		try {
			return findDefinition(fieldKey);
		} catch (FieldNotRegisteredException e) {
			throw new ToRuntimeException(e);
		}
	}

	@XmlTransient
	public static final class Fields {
		public static final String FIELD = "field";
		public static final String DOMAIN_KEY = "domainKey";
		public static final String DOMAIN = "domain";
		public static final String DESCRIPTION = "description";
		public static final String FIELDS = "fields";
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<FieldDefinition<?>> iterator() {

		return this.fields.values().iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.domainKey == null) ? 0 : this.domainKey.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FieldDictionary other = (FieldDictionary) obj;
		if (this.domainKey == null) {
			if (other.domainKey != null)
				return false;
		} else if (!this.domainKey.equals(other.domainKey))
			return false;
		return true;
	}

}
