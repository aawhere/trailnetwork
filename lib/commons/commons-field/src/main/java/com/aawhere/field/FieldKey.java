/**
 *
 */
package com.aawhere.field;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.id.StringIdentifier;
import com.aawhere.xml.XmlNamespace;

/**
 * A simple wrapper for String that identifies a valid business key used to communicate a unique
 * {@link Field} within the system independent of it's value.
 * 
 * {@link FieldDefinition} provides the details of what the field represents where this simply
 * identifies the field for easy identification and transportation.
 * 
 * @author roller
 * 
 */
@SuppressWarnings("rawtypes")
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class FieldKey
		extends StringIdentifier<FieldDefinition> {
	/**
	 *
	 */
	private static final long serialVersionUID = 8888298776141596884L;
	@XmlAttribute
	private String domainKey;
	@XmlAttribute
	private String keyValue;
	/**
	 * A nested key represents a property of a {@link Dictionary} enabled object
	 * 
	 */
	private FieldKey nested;

	@SuppressWarnings("unused")
	private FieldKey() {
		super(FieldDefinition.class);
	}

	/**
	 * Available only for the factory to easily look up FeildKeys by their value. This will not
	 * fully build a FieldKey since it will not handle nested keys.
	 * 
	 * 
	 * @param fullKey
	 */
	FieldKey(String fullKey) throws InvalidFieldKeyFormatException {
		super(fullKey, FieldDefinition.class);

		String[] strings = FieldUtils.parse(fullKey);
		this.domainKey = strings[0];
		this.keyValue = strings[1];
	}

	public FieldKey(String domainKey, String keyValue) {
		this(domainKey, keyValue, null);
	}

	/**
	 * builds this key and adds the given nested key
	 * 
	 * @param domainKey
	 * @param keyValue
	 * @param nested
	 */
	public FieldKey(@Nonnull String domainKey, @Nonnull String keyValue, @Nullable FieldKey nested) {
		super(FieldUtils.join(domainKey, keyValue, nested), FieldDefinition.class);
		this.domainKey = domainKey;
		this.keyValue = keyValue;
		this.nested = nested;
	}

	public FieldKey(FieldKey key, String nested) {
		this(key, new FieldKey(key.getAbsoluteKey(), nested));
	}

	/**
	 * @param key
	 *            references the property that is nesting the nested
	 * @param key
	 *            that represents nested property of interest
	 */
	public FieldKey(FieldKey key, FieldKey nested) {
		this(key.domainKey, key.keyValue, nested);
	}

	/**
	 * The subject that makes this key unique within the system. All {@link #keyValue} within this
	 * {@link #domainKey} must be unqiue accessed by {@link #getValue()} which is the qualified key.
	 * 
	 * @return
	 */
	public String getDomainKey() {
		return this.domainKey;
	}

	/**
	 * The local value for a key within the {@link #domainKey}. This allows for abbreviated
	 * reference when a client is working only within a single domain.
	 * 
	 * @return
	 */
	public String getRelativeKey() {
		return this.keyValue;
	}

	/**
	 * Synonym for {@link #getValue()}, but more clear about what is represented.
	 * 
	 * @return
	 */
	public String getAbsoluteKey() {
		return getValue();
	}

	/**
	 * @return the nested
	 */
	public FieldKey getNested() {
		return this.nested;
	}

	/**
	 * @return
	 */
	public boolean hasNested() {
		return this.nested != null;
	}
}
