/**
 * 
 */
package com.aawhere.field.type;

import java.lang.reflect.InvocationTargetException;

import org.springframework.util.NumberUtils;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.FromString;

/**
 * @author Brian Chapman
 * 
 */
public class NumberDataTypeAdapter
		extends DataTypeAdapter<Number> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldData marshal(Object adaptee) {
		return new FieldData.Builder().setType(FieldDataType.NUMBER).setValue((Number) adaptee).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#unmarshal(java.lang.Object, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends Number> A unmarshal(FieldData adapted, Class<A> type) throws BaseException {
		// tried using apache NumberUtils.getNumber(String) without success and proper feedback
		try {
			Double doubleValue = FromString.newInstance(Double.class, adapted.getValue().toString());
			return NumberUtils.convertNumberToTargetClass(doubleValue, type);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@Override
	public Class<Number> getType() {
		return Number.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.DataTypeAdapter#getFieldDataType()
	 */
	@Override
	public FieldDataType getFieldDataType() {
		return FieldDataType.NUMBER;
	}
}
