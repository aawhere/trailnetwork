/**
 *
 */
package com.aawhere.field.annotation;

import static com.aawhere.lang.annotation.AnnotationProcessorUtil.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

import org.apache.commons.lang3.StringEscapeUtils;

import com.aawhere.field.FieldKey;
import com.aawhere.lang.string.StringUtilsExtended;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * An Annotation Processor generating a class for every {@link Dictionary} annotated class that
 * makes constants for every {@link Field} annotation it encloses. TN-779 Discusses the need for
 * this, however, it falls short because we are not able to use the dictionary factory since the
 * Dictionary classes are not yet available..only the source.
 * 
 * FIXME: This should use a templating mechanism like velocity. It's pretty simple so far.
 * 
 * TODO: Parent annotaitons aren't being generated. Only those declared on the class
 * directly..apparently for those not in the same project. The constants could be generated for
 * items in the parent project and these could inherit...using classElement.getSuperclass().
 * 
 * @author aroller
 * 
 */
@SupportedAnnotationTypes("com.aawhere.field.annotation.Dictionary")
@SupportedSourceVersion(SourceVersion.RELEASE_7)
public class FieldDictionaryConstantsAnnotationProcessor
		extends AbstractProcessor {

	/*
	 * (non-Javadoc)
	 * @see javax.annotation.processing.AbstractProcessor#process(java.util.Set,
	 * javax.annotation.processing.RoundEnvironment)
	 */
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		Messager messager = processingEnv.getMessager();
		for (TypeElement te : annotations) {
			for (Element dictionaryElement : roundEnv.getElementsAnnotatedWith(te)) {

				if (dictionaryElement.getKind() == ElementKind.CLASS) {
					TypeElement classElement = (TypeElement) dictionaryElement;
					// messager.printMessage(Kind.NOTE, "Fields: " + e.getAnnotationMirrors());

					PackageElement packageElement = packageElement(classElement);

					try {
						String suffix = "Field";
						String className = classElement.getSimpleName() + suffix;
						Name dictionaryClassName = classElement.getQualifiedName();

						String qualifiedClassName = dictionaryClassName + suffix;
						messager.printMessage(Kind.NOTE, "Generating: " + qualifiedClassName);
						// Filenames must go the package and use the simple name because using
						// qualifiedClassName may include the enclosing class
						JavaFileObject jfo = processingEnv.getFiler()
								.createSourceFile(packageElement.getQualifiedName() + "." + className);

						BufferedWriter bw = new BufferedWriter(jfo.openWriter());
						// package
						{
							bw.append("package ");
							bw.append(packageElement.getQualifiedName());
							bw.append(";");
						}
						bw.newLine();
						bw.newLine();
						// imports
						{
							bw.append("import ");
							bw.append(FieldKey.class.getName());
							bw.append(";");
							bw.newLine();
						}
						String dictionaryJavadoc = processingEnv.getElementUtils().getDocComment(dictionaryElement);
						// javadoc
						{
							bw.append("/** Generated code.");
							bw.newLine();
							bw.append("* @see ");
							bw.append(getClass().toString());
							bw.newLine();
							if (dictionaryJavadoc != null) {
								bw.append(dictionaryJavadoc);
								bw.newLine();
							}
							bw.append("* @see ");
							bw.append(dictionaryClassName);
							bw.newLine();
							bw.append("*/");
							bw.newLine();
						}
						// body
						{
							bw.append("public class ");
							bw.append(className);
							bw.append("{");
							bw.newLine();
							// constants
							{

								// can't use the dictionary factory because the dictionary class
								// doesn't yet exist.
								// this is after source scanning, but before compilation
								bw.newLine();

								Map<String, FieldKey> keys = new HashMap<>();
								Map<String, ApiModelProperty> apiModelProperties = new HashMap<>();
								Map<String, String> javadocs = new HashMap<>();
								// constant strings
								{
									Dictionary dictionary = dictionaryElement.getAnnotation(Dictionary.class);
									// dicationary first
									{
										bw.append("public static final String DOMAIN = \"");
										bw.append(dictionary.domain());
										bw.append("\";");
										bw.newLine();
									}

									ApiModel apiModel = dictionaryElement.getAnnotation(ApiModel.class);
									if (apiModel != null) {
										bw.append("public static final String DOMAIN_DESCRIPTION = \"");
										bw.append(apiModel.description());
										bw.append("\";");
										bw.newLine();
									}

									if (dictionaryJavadoc != null) {
										bw.append("public static final String DOMAIN_JAVADOC = \"");
										bw.append(StringEscapeUtils.escapeJava(dictionaryJavadoc));
										bw.append("\";");
										bw.newLine();
									}
									// FIELDS
									List<? extends Element> enclosedElements = dictionaryElement.getEnclosedElements();
									for (Element element : enclosedElements) {
										Field field = element.getAnnotation(Field.class);
										if (field != null) {
											final String domain = dictionary.domain();
											String key;
											try {
												key = field.key();
											} catch (Exception e) {
												throw new RuntimeException("error retrieving key for " + element
														+ " on " + dictionaryElement + " with field type: "
														+ field.getClass(), e);
											}

											if (key.equals(Field.UNDEFINED)) {
												key = element.getSimpleName().toString();
											}
											String constantName = StringUtilsExtended.camelCaseToConstant(key);

											String javadoc = processingEnv.getElementUtils().getDocComment(element);
											if (javadoc != null) {
												bw.append("/**");
												bw.append(javadoc);
												bw.append("*/");
												bw.newLine();
												javadocs.put(constantName, javadoc);
											}

											bw.append("public static final String ");
											keys.put(constantName, new FieldKey(domain, key));
											bw.append(constantName);
											bw.append(" = ");
											bw.append("\"");
											bw.append(key);
											bw.append("\"");

											bw.append(";");
											bw.newLine();

											// now look for associated documentation
											ApiModelProperty apiModelProperty = element
													.getAnnotation(ApiModelProperty.class);
											if (apiModelProperty != null) {
												apiModelProperties.put(constantName, apiModelProperty);
											}
										}
									}
								}
								// FieldKeys
								{
									bw.newLine();
									bw.newLine();
									bw.append("public static class Key {");
									bw.newLine();
									for (String constant : keys.keySet()) {
										bw.append("\tpublic static final FieldKey ");
										bw.append(constant);
										bw.append(" = ");
										bw.append("new FieldKey(DOMAIN,");
										bw.append(className);
										bw.append(".");
										bw.append(constant);
										bw.append(");");
										bw.newLine();
									}
									bw.append("}//end Key");
									bw.newLine();
									bw.newLine();
								}
								// Absolute
								{
									bw.newLine();
									bw.newLine();
									bw.append("/**The domain-key combination for this only.  Does not support nested*/");
									bw.newLine();
									bw.append("public static class Absolute {");
									bw.newLine();
									for (String constant : keys.keySet()) {
										bw.append("\tpublic static final String ");
										bw.append(constant);
										bw.append(" = \"");
										bw.append(keys.get(constant).getAbsoluteKey());
										bw.append("\";");
										bw.newLine();
									}
									bw.append("}//end Absolute");
									bw.newLine();
									bw.newLine();
								}
								// Documentation
								{
									bw.newLine();
									bw.newLine();
									bw.append("/**Associated documentation from ApiModelProperty annoations.*/");
									bw.newLine();
									bw.append("public static class Doc {");
									bw.newLine();
									for (String constant : apiModelProperties.keySet()) {
										final ApiModelProperty apiModelProperty = apiModelProperties.get(constant);
										final String description = apiModelProperty.value();

										bw.append("\tpublic static final String ");
										bw.append(constant);
										bw.append("_DESCRIPTION");
										bw.append(" = \"");
										bw.append(StringEscapeUtils.escapeJava(description));
										bw.append("\";");
										bw.newLine();
									}
									for (Map.Entry<String, String> javadocEntry : javadocs.entrySet()) {

										bw.append("\tpublic static final String ");
										bw.append(javadocEntry.getKey());
										bw.append("_JAVADOC");
										bw.append(" = \"");
										bw.append(StringEscapeUtils.escapeJava(javadocEntry.getValue()));
										bw.append("\";");
										bw.newLine();
									}
									bw.append("}//end Doc");
									bw.newLine();
									bw.newLine();
								}

							}

							bw.append("}//end class");
							bw.newLine();
						}
						bw.close();
					} catch (IOException e1) {
						throw new RuntimeException(e1);
					}
					// rest of generated class contents
				}
			}
		}

		return true;
	}

}
