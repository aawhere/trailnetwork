/**
 *
 */
package com.aawhere.field;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

/**
 * A key/value pair. Stored in database this would be a "cell" of a table describing both the column
 * ( {@link #definition}) and the row ({@link #value} ).
 * 
 * @see FieldDefinition
 * @see Report
 * @see Record
 * @author roller
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class Field<ValueT>
		implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -596010111070557929L;

	/**
	 * All else that is necessary to understand information about what this field represents.
	 */
	@XmlElement
	private FieldDefinition<ValueT> definition;
	@XmlElement
	private ValueT value;

	/**
	 * @return the definition
	 */
	public FieldDefinition<ValueT> getDefinition() {
		return definition;
	}

	/**
	 * @return the value
	 */
	public ValueT getValue() {
		return value;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Field<ValueT> other = (Field<ValueT>) obj;
		if (definition == null) {
			if (other.definition != null) {
				return false;
			}
		} else if (!definition.equals(other.definition)) {
			return false;
		}
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return definition.toString() + " : " + getValue();
	}

	/**
	 * Used to construct all instances of Field.
	 */
	@XmlTransient
	public static class Builder<ValueT>
			extends ObjectBuilder<Field<ValueT>> {
		/**
		 * @see Record.Builder#nextField();
		 */
		public Builder() {
			super(new Field<ValueT>());
		}

		/**
		 * @param definition
		 *            the definition to set
		 */
		public Builder<ValueT> setDefinition(FieldDefinition<ValueT> definition) {
			building.definition = definition;
			return this;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public Builder<ValueT> setValue(ValueT value) {
			building.value = value;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			// allowing nulls since it may be required to have a field reporting nulls.
			// handlers should display nulls appropriately
			// Assertion.assertNotNull("value", building.value);
			Assertion.assertNotNull("definition", building.definition);
		}
	}

	/** user {@link Builder} to construct Field */
	private Field() {
	}
}
