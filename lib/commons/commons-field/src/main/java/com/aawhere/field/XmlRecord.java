/**
 *
 */
package com.aawhere.field;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * The result of the {@link RecordXmlAdapter}.
 * 
 * @author Brian Chapman
 * 
 */
public class XmlRecord {

	@XmlElement(name = "field")
	public List<Field<?>> fields = new ArrayList<Field<?>>();
}
