/**
 * 
 */
package com.aawhere.field.message;

import java.util.Locale;

import com.aawhere.field.FieldDefinition;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;

/**
 * Simple class that formats a {@link FieldDefinition} when used with a {@link Message}.
 * 
 * @author Aaron Roller
 * 
 */
public class FieldDefinitionCustomFormat
		implements CustomFormat<FieldDefinition<?>> {

	private static FieldDefinitionCustomFormat instance;

	/**
	 * Used to retrieve the only instance of FieldDefinitionCustomFormat.
	 */
	public static FieldDefinitionCustomFormat getInstance() {

		if (instance == null) {
			instance = new FieldDefinitionCustomFormat();
		}
		return instance;

	}// end Builder

	/** Use {@link #getInstance()} to get FieldDefinitionCustomFormat */
	private FieldDefinitionCustomFormat() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#format(java.lang.Object, java.util.Locale)
	 */
	@Override
	public String format(FieldDefinition<?> def, Locale locale) {

		return def.getName().toString();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#handlesType()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Class<FieldDefinition> handlesType() {
		return FieldDefinition.class;
	}

}
