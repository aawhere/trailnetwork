/**
 * 
 */
package com.aawhere.field;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.reflections.Reflections;

import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldDictionaryConstantsAnnotationProcessor;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.string.StringUtilsExtended;
import com.google.common.collect.Sets;

/**
 * A class generator that outputs the entire dictionary constants. This relies on classes already
 * loaded so it only finds those classes available prior in the reactor. For this reason it is
 * written once and intended to be in the war where the fields are delivered.
 * 
 * The result is a big class with a list of constants with really long names!
 * 
 * @see FieldDictionaryConstantsAnnotationProcessor
 * @author aroller
 * 
 */
public class FieldDictionaryConstantWriter {

	public static void main(String[] args) {

		Reflections reflections = Reflections.collect();
		Set<Class<?>> types = reflections.getTypesAnnotatedWith(Dictionary.class);
		String directory = args[0];
		// FIXME:This should be arg2.
		String projectName = args[1];
		String[] projectNameParts = StringUtils.split(projectName, "-");
		String className = StringUtilsExtended.toTitleCase(projectNameParts) + "NestedField";
		String folderPath = "/com/aawhere/all/";
		String fileName = directory + folderPath + className + ".java";
		File file = new File(fileName);
		file.getParentFile().mkdirs();

		FileWriter fileWriter = null;
		BufferedWriter writer = null;
		try {
			fileWriter = new FileWriter(fileName);
			writer = new BufferedWriter(fileWriter);
			FieldAnnotationDictionaryFactory annotationDictionaryFactory = new FieldAnnotationDictionaryFactory();

			writer.write("package com.aawhere.all; ");
			writer.newLine();
			// imports
			writer.write("import com.aawhere.field.*;");
			writer.newLine();
			// header
			writer.newLine();
			writer.write("/**  ");
			writer.write("* Generated constants for all keys in ");
			writer.write(FieldDictionaryFactory.class.getName());
			writer.newLine();
			writer.write("* @see ");
			writer.write(FieldDictionaryConstantWriter.class.getName());
			writer.newLine();
			writer.write("*/");
			// class begin
			writer.write("public class ");
			writer.write(className);
			writer.write("{\n\n");

			// load all the dictionaries.
			for (Class<?> class1 : types) {
				if (AnnotatedDictionaryUtils.isAnnotatedDictionary(class1)) {
					annotationDictionaryFactory.getDictionary(class1);
				}
			}
			TreeSet<FieldKey> keys = Sets.newTreeSet(FieldUtils.keys(annotationDictionaryFactory.getFactory()
					.getDefinitions()));
			ArrayList<String> constants = new ArrayList<String>();
			for (FieldKey fieldKey : keys) {
				String absoluteKey = fieldKey.getAbsoluteKey();
				// using extra word separation allows routeCompletion-route-id and
				// routeCompletion-routeId not to clobber
				String constant = StringUtilsExtended.toConstantCaseExtraWordSeparation(absoluteKey);
				constants.add(constant);
				writer.write("public static final String ");
				writer.write(constant);
				writer.write(" = \"");
				writer.write(absoluteKey);
				writer.write("\";");
				writer.newLine();
			}

			writer.write("public static final class Key{ ");
			writer.newLine();
			for (String constant : constants) {
				writer.write("public static final FieldKey ");
				writer.write(constant);
				writer.write(" = FieldUtils.key(");
				writer.write(className);
				writer.write(".");
				writer.write(constant);
				writer.write(");");
				writer.newLine();
			}
			writer.write("}//end Key");
			writer.newLine();
			writer.write("}//end class");
			writer.newLine();
		} catch (IOException e) {
			throw new RuntimeException("error writing field dictionary constants ", e);
		} finally {
			IOUtils.closeQuietly(writer);
			IOUtils.closeQuietly(fileWriter);
		}
	}
}
