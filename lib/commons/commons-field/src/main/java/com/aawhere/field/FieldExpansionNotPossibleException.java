/**
 * 
 */
package com.aawhere.field;

import static com.aawhere.field.FieldExpansionNotPossibleExceptionMessage.*;
import static com.aawhere.field.FieldExpansionNotPossibleExceptionMessage.Param.*;

import java.util.Collection;

import com.aawhere.field.FieldExpansionNotPossibleExceptionMessage.Param;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

import com.wordnik.swagger.annotations.ApiModel;

/**
 * 
 * @author aroller
 * 
 */
@StatusCode(
		value = HttpStatusCode.BAD_REQUEST,
		message = "{DEFINITIONS} requested, but not fullfilled for {DICTIONARY_TYPE}.  All fields must be a direct member or the results of another field requested. ")
@ApiModel(
		description = "Requesting a field on a dictionary to be expanded that can not be found or is not an expandable field.")
public class FieldExpansionNotPossibleException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 1273332461763727038L;

	public FieldExpansionNotPossibleException(Collection<FieldDefinition<?>> definitionsRequested,
			Class<?> dictionaryType) {
		super(CompleteMessage.create(FIELD_EXPANSION_NOT_POSSIBLE).param(Param.DEFINITIONS, definitionsRequested)
				.param(DICTIONARY_TYPE, dictionaryType).build());
	}

}
