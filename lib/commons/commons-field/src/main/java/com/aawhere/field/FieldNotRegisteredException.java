/**
 *
 */
package com.aawhere.field;

import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Identifies that the given field has not been registered. This may be useful for developers during
 * system coding, but also useful for web services responding to requests by keys.
 * 
 * @author roller
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class FieldNotRegisteredException
		extends BaseException {

	/**
	 *
	 */
	private static final long serialVersionUID = 6350486489231228831L;

	/**
	 * @deprecated
	 * @param field
	 */
	public FieldNotRegisteredException(String absoluteKey) {
		super(FieldMessage.FIELD_NOT_REGISTERED, FieldMessage.Param.FIELD_KEY, absoluteKey);
	}

	/**
	 * @param key
	 * @param keySet
	 */
	public FieldNotRegisteredException(FieldKey key, Set<FieldKey> keySet) {
		super(CompleteMessage
				.create(FieldMessage.FIELD_NOT_REGISTERED)
				.addParameter(FieldMessage.Param.FIELD_KEY, key)
				.addParameter(	FieldMessage.Param.FIELD_KEYS,
								StringUtils.join(new TreeSet(keySet), System.lineSeparator())).build());
	}

}
