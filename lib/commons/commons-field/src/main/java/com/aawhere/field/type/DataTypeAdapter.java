/**
 *
 */
package com.aawhere.field.type;

import com.aawhere.adapter.Adapter;
import com.aawhere.field.Field;

/**
 * Adapters to translate from {@link Object} annotated with {@link Field} with paramater
 * {@link FieldDataType} to the type represented by the {@link FieldDataType}
 * 
 * @author Brian Chapman
 * 
 */
public abstract class DataTypeAdapter<AdapteeT>
		implements Adapter<AdapteeT, FieldData> {

	public abstract FieldDataType getFieldDataType();
}
