package com.aawhere.field;

import com.aawhere.field.DictionaryNotRegisteredExceptionMessage.Param;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

@StatusCode(value = HttpStatusCode.BAD_REQUEST, message = "{DOMAIN} is not a registered dictionary.")
public class DictionaryNotRegisteredException
		extends BaseRuntimeException {

	private static final long serialVersionUID = -5202401680584033402L;

	public DictionaryNotRegisteredException(String domain) {
		super(CompleteMessage.create(DictionaryNotRegisteredExceptionMessage.DICTIONARY_NOT_REGISTERED)
				.param(Param.DOMAIN, domain).build());
	}
}
