/**
 * 
 */
package com.aawhere.field;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Function;

/**
 * Provides the {@link Field#getValue()} for a {@link FieldDefinition} when given an identifying key
 * for the {@link FieldDictionary}.
 * 
 * @author aroller
 * 
 */
public interface FieldValueProvider {
	/**
	 * Provides the persistent value for the property of interest on the dictionary matching the
	 * dictionary key.
	 * 
	 * @param fieldKey
	 *            the property that holds the value of interest
	 * @param dictionaryInstanceKey
	 *            the key that represents the dictionary instance that will provide the value for
	 *            the property.
	 * @return the value for the field or null if its not available
	 */
	@Nullable
	public <K, V> V value(@Nonnull FieldKey fieldKey, @Nonnull K dictionaryInstanceKey)
			throws DictionaryInstanceNotFoundException;

	public static class Util {
		/**
		 * Convenience method for {@link #function(FieldDefinition)}
		 * 
		 * @param fieldKey
		 * @param factory
		 * @return
		 */
		@SuppressWarnings("unchecked")
		public static <K, V> Function<K, V> function(final FieldKey fieldKey, final FieldValueProvider provider) {
			return new Function<K, V>() {

				@Override
				public V apply(K input) {
					try {
						return (V) ((input != null) ? provider.value(fieldKey, input) : null);
					} catch (DictionaryInstanceNotFoundException e) {
						return null;
					}
				}
			};
		}
	}
}
