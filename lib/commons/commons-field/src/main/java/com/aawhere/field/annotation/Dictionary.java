/**
 * 
 */
package com.aawhere.field.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.aawhere.field.DictionaryIndex;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Annotation used to declare a {@link Dictionary} domain relating to a class which contains one or
 * more {@link Field}.
 * 
 * @author Aaron Roller
 * 
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Dictionary {

	public enum None implements Message {
		;
		@Override
		public String getValue() {
			return null;
		}

		@Override
		public ParamKey[] getParameterKeys() {
			return null;
		}
	}

	/**
	 * The domain is the "subject" that this dictionary represents.
	 */
	String domain();

	/**
	 * The class of the Enumeration that provides the appropriate messages to describe the fields
	 * registered in the dictionary.
	 * 
	 * @return
	 */
	Class<? extends Enum<? extends Message>> messages() default None.class;

	/**
	 * Provides the combination indexes required for this Dictionary to satisfy queries. Each
	 * {@link DictionaryIndex} must provide the keys that are part of the index.
	 * 
	 * @since 11
	 * @return
	 */
	Class<? extends DictionaryIndex>[] indexes() default {};
}
