/**
 *
 */
package com.aawhere.field.type;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.field.Field;
import com.aawhere.field.annotation.Dictionary.None;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.xml.XmlNamespace;

/**
 * The type of data a {@link Field} represents. This is different than the object type the Field
 * contains, which may be any Type. These types are used by the FieldDataTypeAdapters to translate a
 * field into a base (or primitive) type for use in other systems.
 * 
 * Optional
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@XmlEnum
public enum FieldDataType implements Message {

	/* java.util.Date */
	DATE("Date", Date.class),
	/* Atomic unit, String */
	ATOM("Atom", String.class),
	/*
	 * Geographic point @See GeoCoordinate#getAsString() Should format as latitude,longitude Also
	 * @see STRING_LON_INDEX and STRING_LAT_INDEX
	 */
	GEO("Geopoint", String.class),
	/* java.lang.Number */
	NUMBER("Number", Number.class),
	/* java.lang.String */
	TEXT("Text", String.class),
	/**
	 * Used as a default indicating that the datatype was not specified.
	 */
	NONE("None", None.class);

	public static final int STRING_LON_INDEX = 1;
	public static final int STRING_LAT_INDEX = 0;
	private String name;

	private Class<?> dataType;

	private FieldDataType(String name, Class<?> dataType) {
		this.dataType = dataType;
		this.name = name;
	}

	public Class<?> dataType() {
		return dataType;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.util.rb.Message#getValue()
	 */
	@Override
	public String getValue() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.util.rb.Message#getParameterKeys()
	 */
	@Override
	public ParamKey[] getParameterKeys() {
		// TODO Auto-generated method stub
		return null;
	}

	public static boolean isNone(FieldDataType dataType) {
		return dataType == null || dataType.equals(NONE);
	}
}
