/**
 *
 */
package com.aawhere.field.annotation;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import org.springframework.core.annotation.AnnotationUtils;

import com.aawhere.field.DictionaryIndex;
import com.aawhere.field.DictionaryIndexClassFunction;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldKey;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.lang.annotation.AnnotationUtilsExtended;
import com.aawhere.lang.reflect.ReflectionUtilsExtended;
import com.aawhere.lang.string.StringUtilsExtended;
import com.aawhere.log.LoggerFactory;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessageFactory;
import com.aawhere.util.rb.StringMessage;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Used to initialize {@link Dictionary}s automatically via the annotations provided on classes
 * defining {@link com.aawhere.field.Field}s.
 * 
 * Multiple dictionaries may be defined based on the Dictionary annotations provided at classes in a
 * heirarchy. To satisfy the likely requirement that only a single dictionary is desired, this will
 * search from the bottom up looking for a Dictionary entry. It will choose the lowest defined
 * Dictionary element among the class Heirarchy. If the annotation is provided on an two Interfaces
 * that are implemented by the same class the Dictionary elements would be considered equal in the
 * heirarchy and one will be chosen without predictability so avoid this situation if possible.
 * 
 * TODO: make the heirarchy search more predictable if needed
 * 
 * @author Aaron Roller
 * 
 */
@Singleton
public class FieldAnnotationDictionaryFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(FieldAnnotationDictionaryFactory.class);
	private static final String DESCRIPTION_SUFFIX = "description";
	private static final String NAME_SUFFIX = "name";

	private FieldDictionaryFactory factory;

	@Inject
	public FieldAnnotationDictionaryFactory(FieldDictionaryFactory factory) {
		this.factory = factory;
		Assertion.assertNotNull("factory", factory);
	}

	public FieldAnnotationDictionaryFactory() {
		this(new FieldDictionaryFactory());
	}

	/**
	 * @return the factory
	 */
	public FieldDictionaryFactory getFactory() {
		return this.factory;
	}

	public static Boolean hasParameterType(Field field) {
		return !field.parameterType().equals(Field.None.class);
	}

	private static Logger getLogger() {
		return LOGGER;
	}

	/**
	 * @param member
	 * @param fieldAnnotation
	 * @return
	 */
	static String keyFrom(java.lang.reflect.Member member, Field fieldAnnotation) {
		return AnnotatedDictionaryUtils.keyFrom(member, fieldAnnotation);
	}

	/**
	 * finds the registered dictionary or registers the dictionary based off the annotations.
	 * 
	 * This is intentionally not synchronized because the factory allows for duplicate registration
	 * of equal dictionaries should there be a race to produce the first.
	 * 
	 * @param annotatedDictionaryClass
	 * @return
	 */
	public FieldDictionary getDictionary(Class<?> annotatedDictionaryClass) {
		return getDictionary(annotatedDictionaryClass, null, null);
	}

	private FieldDictionary getDictionary(Class<?> annotatedDictionaryClass, String domain, String parentColumnName) {
		Dictionary annotation = AnnotatedDictionaryUtils.dictionaryFrom(annotatedDictionaryClass);
		if (annotation == null) {
			throw new UnsupportedOperationException(annotatedDictionaryClass.getSimpleName()
					+ " does not have a Dictionary annotation and cannot be registered here without it");
		}
		domain = If.nil(domain).use(annotation.domain());
		FieldDictionary dictionary;
		if (this.factory.instanceExists(domain)) {
			dictionary = this.factory.getDictionary(domain);
		} else {
			AnnotationExtractor messageExtractor = new AnnotationExtractor(annotatedDictionaryClass, domain,
					parentColumnName);
			dictionary = messageExtractor.dictionaryBuilder.build();
			this.factory.register(dictionary);
		}
		return dictionary;

	}

	/**
	 * @param dictionaryMutator
	 * @return
	 */
	public static DictionaryMutator dicionaryAnnotationFrom(MutableAnnotatedDictionary dictionaryMutator) {

		DictionaryMutator dictionaryAnnotation = AnnotationUtils.findAnnotation(dictionaryMutator.getClass(),
																				DictionaryMutator.class);
		return dictionaryAnnotation;
	}

	/**
	 * Messages are automatically extracted based on the fields provided using a common naming
	 * standard
	 * 
	 * <pre>
	 * {DOMAIN}_NAME - for properties relating to the dictionary itself
	 * {DOMAIN}_{FIELD}_NAME - for properties relating to a field within the dictionary
	 * </pre>
	 * 
	 * @author aroller
	 * 
	 */
	private class AnnotationExtractor {
		String domain;
		FieldDictionary.Builder dictionaryBuilder;
		MessageFactory messageFactory;
		Class<? extends Enum<? extends Message>> messageEnumClass;

		/**
		 * @param factory
		 * 
		 */
		public AnnotationExtractor(Class<?> annotatedClass, String domain, String parentColumnName) {
			Dictionary dictionaryAnnotation = AnnotatedDictionaryUtils.dictionaryFrom(annotatedClass);
			this.domain = domain;
			dictionaryBuilder = new FieldDictionary.Builder().setDomainKey(domain).setType(annotatedClass);

			// assigned indexes if any
			{

				final List<Class<? extends DictionaryIndex>> indexClasses = Arrays.asList(dictionaryAnnotation
						.indexes());
				// applying copy to eager load the indexes so the function does not stick around
				// during serialization
				final List<DictionaryIndex> indexes = ImmutableList.copyOf(Lists.transform(	indexClasses,
																							DictionaryIndexClassFunction
																									.build()));
				dictionaryBuilder.setIndexes(indexes);
			}
			populateDomain(dictionaryAnnotation);
			populateFields(annotatedClass, parentColumnName);
		}

		@SuppressWarnings("unchecked")
		private void populateFields(Class<?> annotatedClass, String parentColumnName) {
			Class<Field> annotationClass = Field.class;
			Map<Member, Field> members = AnnotationUtilsExtended.getInstance().findMembers(	annotationClass,
																							annotatedClass);
			for (Map.Entry<Member, Field> memberEntry : members.entrySet()) {
				Member member = memberEntry.getKey();
				String key = keyFrom(member, memberEntry.getValue());

				// swagger annotations provide valuable information
				ApiModelProperty apiModelProperty = null;
				if (member instanceof AnnotatedElement) {
					apiModelProperty = ((AnnotatedElement) member).getAnnotation(ApiModelProperty.class);
				}
				FieldDefinition.Builder<Object> definition = populateDefinition(key, apiModelProperty);
				Class<?> type = ReflectionUtilsExtended.typeFor(member);
				definition.setType((Class<Object>) type);
				definition.setXmlAdapterType(If.equal(memberEntry.getValue().xmlAdapter(), Field.None.class).use(null));
				// the type that the member represents of the parameter type if provided
				// this is needed for checking for nested dictionaries
				Class<?> representedType = type;

				Field field = memberEntry.getValue();
				// for now the column name must always match the name of the java field
				// this can be overridden by providing a method in the Field annotation.
				String columnName = null;
				if (member instanceof java.lang.reflect.Field) {
					columnName = member.getName();
				} else {
					columnName = key;
				}
				if (parentColumnName != null) {
					// column name must inherit the parent's column name and append either the
					// FIXME:this is datastore specific and should be in persist, not field TN-224
					columnName = parentColumnName + "." + ((columnName == null) ? key : columnName);
				}
				// setting null is o.k.
				definition.setColumnName(columnName);
				// add a parameter type if provided
				if (hasParameterType(field)) {
					definition.setParameterType(field.parameterType());
					representedType = field.parameterType();
				} else {
					if (Collection.class.isAssignableFrom(type)) {
						throw new IllegalStateException(key + " for " + domain + " is of type " + type
								+ " which is a collection and reqiues a parameter type");
					}
				}

				// get the dictionary for the nested if the type is a dictionary
				// avoid crawling into thyself to avoid infinite loop
				if (AnnotatedDictionaryUtils.isAnnotatedDictionary(representedType)
						&& !representedType.equals(annotatedClass)) {
					EmbeddedDictionary embeddedDictionaryAnnotation = AnnotationUtilsExtended
							.getAnnotation(member, EmbeddedDictionary.class);
					boolean flatten = false;
					// this will get if already created or create recursively if not yet created
					// RECURSIVE CALL HERE
					FieldDictionary nestedDictionary = getDictionary(	representedType,
																		new FieldKey(domain, key).getAbsoluteKey(),
																		columnName);
					definition.setNestedDictionary(nestedDictionary);

					// we have nested field definitions, but they need to be registered with the new
					// key that is relative to the property
					for (FieldDefinition<?> fieldDefinition : nestedDictionary) {

						FieldKey nestedKey = new FieldKey(domain, key, fieldDefinition.getKey());
						// we have a new key, we must make a new definition and register it
						FieldDefinition.Builder<?> propertyDefinition = FieldDefinition.Builder
								.create(nestedKey, fieldDefinition);
						dictionaryBuilder.addField(propertyDefinition);
						if (embeddedDictionaryAnnotation != null) {
							flatten = embeddedDictionaryAnnotation.flatten();
						}
						// if flatten then add a duplicate definition as an alias
						if (flatten) {
							FieldKey flattenedKey = new FieldKey(domain, fieldDefinition.getKey().getRelativeKey());
							FieldDefinition.Builder<?> flattenedDefinition = FieldDefinition.Builder
									.create(flattenedKey, fieldDefinition).flatten(true);
							dictionaryBuilder.addField(flattenedDefinition);
						}
					}
				}
				definition.setDataType(memberEntry.getValue().dataType());
				definition.setSearchable(memberEntry.getValue().searchable());
				definition.setIndexedFor(memberEntry.getValue().indexedFor());

				dictionaryBuilder.addField(definition);
			}
		}

		/**
		 * Given the field key, this will create the definition and populate it with standard
		 * messages returning the builder for continued use.
		 * 
		 * @param key
		 * @param apiModelProperty
		 * @return
		 */
		public com.aawhere.field.FieldDefinition.Builder<Object> populateDefinition(String key,
				ApiModelProperty apiModelProperty) {
			FieldDefinition.Builder<Object> nextDefinition = new FieldDefinition.Builder<Object>().setKey(new FieldKey(
					domain, key));
			if (messageEnumClass != null) {

				nextDefinition.setName(messageFromEnum(buildNameKey(key)));
				final Message messageFromEnum = messageFromEnum(buildDescriptionKey(key));
				if (messageFromEnum != null) {
					nextDefinition.setDescription(messageFromEnum);
				} else {
					if (apiModelProperty != null) {
						final String description = apiModelProperty.value();
						if (description != null) {
							nextDefinition.setDescription(new StringMessage(description));
						}
					}
				}

			}
			return nextDefinition;
		}

		/**
		 * Retrieves relevant information from the {@link Dictionary} annotation and populates the
		 * provided builder.
		 * 
		 * @param dictionaryAnnotation
		 * @param dictionaryBuilder
		 */
		private void populateDomain(Dictionary dictionaryAnnotation) {
			Class<? extends Enum<? extends Message>> messagesClass = dictionaryAnnotation.messages();

			if (!messagesClass.equals(Dictionary.None.class)) {

				this.messageEnumClass = messagesClass;
				// make sure all keys are registered
				messageFactory = MessageFactory.getInstance();
				messageFactory.register(messageEnumClass);
				String nameMessageKey = buildNameKey(domain);
				dictionaryBuilder.setDomain(messageFromEnum(nameMessageKey));
				String descriptionMessageKey = buildDescriptionKey(domain);
				dictionaryBuilder.setDescription(messageFromEnum(descriptionMessageKey));

			} else {
				// re-enable if/when localization get's serious
				// LOGGER.finer(domain + " did not provide a messages class for i18n of fields.");
			}

		}

		private String buildNameKey(String key) {
			return buildRelativeKey(key, NAME_SUFFIX);
		}

		private String buildDescriptionKey(String key) {
			return buildRelativeKey(key, DESCRIPTION_SUFFIX);
		}

		private String buildRelativeKey(String key, String suffix) {
			key = StringUtilsExtended.camelCaseToConstant(key);
			return StringUtilsExtended.toConstantCase(key, suffix);
		}

		/**
		 * Retrieves a message from an enumeration or null if not found.
		 * 
		 * @param messagesClass
		 * @param messageFactory
		 * @param domain
		 * @param relativeMessageKey
		 */
		private Message messageFromEnum(String relativeMessageKey) {
			String absoluteKey = messageFactory.keyForEnum(messageEnumClass, relativeMessageKey);
			if (messageFactory.enumExists(absoluteKey)) {
				return (Message) messageFactory.enumFor(absoluteKey);
			} else {
				// this should remain as a passive nudge to any developer that not defining this
				// will
				// result in insufficient documentation
				getLogger().fine(absoluteKey + " is not declared in " + messageEnumClass + " for " + domain);
				return null;
			}
		}
	}

	/**
	 * Given a class with a {@link Dictionary} annotation and a relative field this will create the
	 * key that will absolutely identify the field.
	 * 
	 * @param dictionaryClass
	 * @param relativeField
	 */
	@Nonnull
	public static FieldKey keyFor(@Nonnull Class<?> dictionaryClass, @Nonnull String relativeField) {
		return new FieldKey(AnnotatedDictionaryUtils.dictionaryFrom(dictionaryClass).domain(), relativeField);

	}

	/**
	 * Registers the annotated dictionary class using {@link #getDictionary(Class)}, but is useful
	 * for chaining registrations.
	 * 
	 * @param type
	 * @return
	 */
	public FieldAnnotationDictionaryFactory register(Class<?> type) {
		getDictionary(type);
		return this;
	}
}
