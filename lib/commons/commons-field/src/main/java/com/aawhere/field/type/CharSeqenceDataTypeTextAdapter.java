/**
 *
 */
package com.aawhere.field.type;

import com.aawhere.lang.exception.BaseException;

/**
 * @author brian
 * 
 */
public class CharSeqenceDataTypeTextAdapter
		extends DataTypeAdapter<CharSequence> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldData marshal(Object adaptee) {
		if (adaptee instanceof CharSequence) {
			return new FieldData.Builder().setType(getFieldDataType()).setValue(adaptee).build();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#unmarshal(java.lang.Object, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends CharSequence> A unmarshal(FieldData adapted, Class<A> type) throws BaseException {
		return (A) adapted.getValue().toString();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@Override
	public Class<CharSequence> getType() {
		return CharSequence.class;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.DataTypeAdapter#getFieldDataType()
	 */
	@Override
	public FieldDataType getFieldDataType() {
		return FieldDataType.TEXT;
	}

}
