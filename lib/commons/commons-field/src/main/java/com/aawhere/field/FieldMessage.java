/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Oct 30, 2010
commons-lang : com.aawhere.field.FieldMessage.java
 */
package com.aawhere.field;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * {@link Message}s for {@link Field}s and friends.
 * 
 * @author roller
 * 
 */
public enum FieldMessage implements Message {

	/** @see FieldNotRegisteredException */
	FIELD_NOT_REGISTERED(
			"The field {FIELD_KEY} is not registered in the system: {FIELD_KEYS}",
			Param.FIELD_KEY,
			Param.FIELD_KEYS),
	FIELD_NOT_INDEXED("The field {FIELD_KEY} is not indexed and cannot be queried against.", Param.FIELD_KEY),
	FIELDS_NOT_INDEXED(
			"There is no index for {FIELD_KEYS} in {DOMAIN} so the query cannot be completed.  Available: {INDEXES}",
			Param.FIELD_KEYS,
			Param.DOMAIN,
			Param.INDEXES),
	/** @see InvalidFieldKeyFormatException */
	INVALID_KEY_FORMAT(
			"The key {FIELD_KEY} must be alpha-numeric camelCase words, but was {FIELD_KEY_INVALID_CHARACTERS}.",
			Param.FIELD_KEY,
			Param.FIELD_KEY_INVALID_CHARACTERS),
	/** @see InvalidFieldKeyLengthException */
	INVALID_KEY_LENGTH("The key {FIELD_KEY} must be less than {MAX_LENGTH}.", Param.FIELD_KEY, Param.MAX_LENGTH),
	/** @see DuplicateFieldRegistrationException */
	DUPLICATE_REGISTRATION("The key {FIELD_KEY} is already registered in the system.", Param.FIELD_KEY),
	MISSING_DOMAIN(
			"The field '{FIELD_KEY}' must be in the format of [domain]{DILIMETER}[relative key].",
			Param.FIELD_KEY,
			Param.DILIMETER),
	/*  */
	NOT_A_DICTIONARY("{OBJECT} is not a Dictionary.", Param.OBJECT),

	DICTIONARY_INSTANCE_NOT_FOUND(
			"No Dictionary instance for domain {DOMAIN} found matching {OBJECT}.",
			Param.DOMAIN,
			Param.OBJECT);

	private ParamKey[] parameterKeys;
	private String message;

	private FieldMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** @see FieldKey#getValue() */
		FIELD_KEY, DOMAIN,
		/** @see FieldKeyValidator#invalidKeyCharacters(String) */
		FIELD_KEY_INVALID_CHARACTERS,
		/** @see FieldKeyValidator#getMaxLength() */
		MAX_LENGTH,
		/** The separator between the domain and key value */
		DILIMETER,
		/* The object that we thought was a dictionary, but it was not */
		OBJECT, FIELD_KEYS,
		/** multiple indexes available. */
		INDEXES;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	/** Support for API documentation. */
	public static final class Doc {
		public static final String FIELD_KEY_DATA_TYPE = "FieldKey";
	}
}
