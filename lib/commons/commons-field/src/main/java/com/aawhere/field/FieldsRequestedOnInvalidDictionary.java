/**
 * 
 */
package com.aawhere.field;

import java.util.List;

import com.aawhere.field.FieldsRequestedOnInvalidDictionaryMessage.Param;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * @author aroller
 * 
 */
@StatusCode(value = HttpStatusCode.BAD_REQUEST,
		message = "{INVALID_CLASS} is not a dictionary, but fields were requested: {FIELDS}")
public class FieldsRequestedOnInvalidDictionary
		extends BaseRuntimeException {

	/**
	 * @param fieldsRequested
	 * @param typeBeingMarshalled
	 */
	public FieldsRequestedOnInvalidDictionary(List<String> fieldsRequested, Class<?> typeBeingMarshalled) {
		super(CompleteMessage
				.create(FieldsRequestedOnInvalidDictionaryMessage.FIELDS_REQUESTED_ON_INVALID_DICTIONARY_MESSAGE)
				.param(Param.INVALID_CLASS, typeBeingMarshalled).param(Param.FIELDS, fieldsRequested).build());
	}

	private static final long serialVersionUID = 1900872996366708660L;

}
