/**
 *
 */
package com.aawhere.field.type;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import com.aawhere.adapter.AbstractAdapterFactory;
import com.aawhere.adapter.Adapter;
import com.aawhere.adapter.AdapterFactory;
import com.aawhere.field.message.EnumDataTypeAtomAdapter;
import com.aawhere.field.message.MessageDataTypeTextAdapter;
import com.aawhere.id.IdentifierDataTypeAtomAdapter;
import com.google.inject.Singleton;

/**
 * For creating adapter factories that can translate one type into a {@link FieldData} given the
 * Object and {@link FieldDataType} to translate to.
 * 
 * This does not implement {@link AdapterFactory} since it requires a second paramater, the
 * {@link FieldDataType}.
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class DataTypeAdapterFactory {

	private Map<FieldDataType, FieldDataAdapterFactory> adapterFactories = new HashMap<FieldDataType, FieldDataAdapterFactory>();

	public DataTypeAdapterFactory() {
		for (FieldDataType type : FieldDataType.values()) {
			adapterFactories.put(type, new FieldDataAdapterFactory());
		}
		// Register types in theis maven module.
		// Types from commons-lang must be defined here since commons-field depends on commons-lang
		getFactory(FieldDataType.TEXT).register(new MessageDataTypeTextAdapter());
		getFactory(FieldDataType.ATOM).register(new EnumDataTypeAtomAdapter());
		getFactory(FieldDataType.ATOM).register(new IdentifierDataTypeAtomAdapter());
		// TODO: opps, this breaks the contract a bit, the return result is a DataType with
		// the adapted value and data type, which is always ATOM for the
		// IdentifierDataTypeAtomAdapter
		// The contract should be maintained and the adapter built to handle different types.
		getFactory(FieldDataType.TEXT).register(new IdentifierDataTypeAtomAdapter());
		getFactory(FieldDataType.ATOM).register(new CharSeqenceDataTypeTextAdapter());
		getFactory(FieldDataType.TEXT).register(new CharSeqenceDataTypeTextAdapter());
		getFactory(FieldDataType.NUMBER).register(new NumberDataTypeAdapter());
	}

	public <T> Adapter<T, FieldData> register(Adapter<T, FieldData> adapter, FieldDataType dataType) {
		return getFactory(dataType).register(adapter);
	}

	public Boolean handles(Type type, FieldDataType dataType) {
		return getAdapterOrNull(type, dataType) != null;
	}

	public Boolean handles(Type type, FieldDataType dataType, Class<?> parameterType) {
		return getAdapterOrNull(type, dataType, parameterType) != null;
	}

	@SuppressWarnings({ "unchecked" })
	@Nullable
	public <P, T> Adapter<T, FieldData> getAdapterOrNull(Type type, FieldDataType dataType, Class<P> parameterType) {
		FieldDataAdapterFactory factory = getFactory(dataType);
		if (factory != null) {
			return (Adapter<T, FieldData>) factory.getAdapterOrNull(type, parameterType);
		} else {
			return null;
		}
	}

	public <T> Adapter<T, FieldData> getAdapterOrNull(Type type, FieldDataType dataType) {
		return getAdapterOrNull(type, dataType, null);
	}

	public <T> Adapter<T, FieldData> getAdapter(Type type, FieldDataType dataType) {
		return getAdapter(type, dataType, null);
	}

	public <T> Adapter<T, FieldData> getAdapter(Type type, FieldDataType dataType, Class<?> parameterType) {
		FieldDataAdapterFactory factory = getFactory(dataType);
		if (factory.handles(type, parameterType)) {
			return getFactory(dataType).getAdapter(type, parameterType);
		} else {
			throw new IllegalArgumentException("No adapter found for " + type.toString());
		}
	}

	private FieldDataAdapterFactory getFactory(FieldDataType dataType) {
		return adapterFactories.get(dataType);
	}

	class FieldDataAdapterFactory
			extends AbstractAdapterFactory<FieldData> {

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.adapter.AbstractAdapterFactory#getCollectionAdapter(com.aawhere.adapter.Adapter
		 * , java.lang.Class)
		 */
		@Override
		protected <C extends Collection<T>, T> Adapter<C, FieldData> getCollectionAdapter(
				Adapter<T, FieldData> adapter, Class<T> paramaterType) {
			if (adapter instanceof DataTypeAdapter) {
				DataTypeAdapter<T> dataTypeAdapter = (DataTypeAdapter<T>) adapter;
				return new CollectionDataTypeAdapter.Builder<C, T>().setAdapter(dataTypeAdapter)
						.setParamaterType(paramaterType).build();
			} else {
				throw new IllegalArgumentException();
			}
		}
	}
}
