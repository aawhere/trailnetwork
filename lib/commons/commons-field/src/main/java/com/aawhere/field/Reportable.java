/**
 *
 */
package com.aawhere.field;

/**
 * Any object that can provide a {@link Report} containing {@link Record}s and {@link Field}s.
 * 
 * @author Aaron Roller
 * 
 */
public interface Reportable {

	/**
	 * Similar to {@link #toString()}, this will return a {@link Report} containing the information
	 * about this object.
	 * 
	 * @return
	 */
	Report toReport();
}
