/**
 *
 */
package com.aawhere.field.type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Adapter for Collections of {@link FieldData}
 * 
 * @author Brian Chapman
 * 
 */
public class CollectionDataTypeAdapter<C extends Collection<T>, T>
		extends DataTypeAdapter<C> {

	private final String SEPERATOR = ",";
	private DataTypeAdapter<T> adapter;
	private Class<T> paramaterType;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldData marshal(Object adaptee) {
		if (adaptee instanceof Collection) {
			Collection<String> fieldDataItems = new ArrayList<String>();
			@SuppressWarnings("unchecked")
			Collection<T> collection = (Collection<T>) adaptee;
			for (Object ob : collection) {
				FieldData fieldData = adapter.marshal(ob);
				fieldDataItems.add((String) fieldData.getValue());
			}
			String text = StringUtils.join(fieldDataItems, SEPERATOR);
			return new FieldData.Builder().setType(FieldDataType.TEXT).setValue(text).build();
		} else {
			throw new IllegalArgumentException("Expected a collection of items, got " + adaptee.getClass());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#unmarshal(java.lang.Object, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends C> A unmarshal(FieldData adapted, Class<A> type) throws BaseException {
		// TODO: WW-237 add better or automatic support here.
		Collection<T> items;
		if (type.equals(List.class)) {
			items = Lists.newArrayList();
		} else if (type.equals(Set.class)) {
			items = Sets.newHashSet();
		} else {
			try {
				items = type.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				items = Lists.newArrayList();
			}
		}
		String text = adapted.getValue();
		String[] values = StringUtils.split(text, SEPERATOR);
		for (String value : values) {
			FieldData fieldData = FieldData.create().setType(FieldDataType.TEXT).setValue(value).build();
			items.add(adapter.unmarshal(fieldData, paramaterType));
		}
		return (A) items;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.adapter.Adapter#getType()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Class<C> getType() {
		return (Class<C>) (Class<?>) Collection.class;
	}

	/**
	 * Used to construct all instances of CollectionDataTypeAdapter.
	 */
	public static class Builder<C extends Collection<T>, T>
			extends ObjectBuilder<CollectionDataTypeAdapter<C, T>> {

		public Builder() {
			super(new CollectionDataTypeAdapter<C, T>());
		}

		public Builder<C, T> setAdapter(DataTypeAdapter<T> adapter) {
			building.adapter = adapter;
			return this;
		}

		public Builder<C, T> setParamaterType(Class<T> type) {
			building.paramaterType = type;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct CollectionDataTypeAdapter */
	private CollectionDataTypeAdapter() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.field.type.DataTypeAdapter#getFieldDataType()
	 */
	@Override
	public FieldDataType getFieldDataType() {
		return adapter.getFieldDataType();
	}

}
