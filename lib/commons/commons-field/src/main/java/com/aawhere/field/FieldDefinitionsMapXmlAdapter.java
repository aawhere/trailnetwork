/**
 *
 */
package com.aawhere.field;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * @author aroller
 * 
 */
public class FieldDefinitionsMapXmlAdapter
		extends XmlAdapter<FieldDefinitions, BiMap<FieldKey, FieldDefinition<?>>> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public FieldDefinitions marshal(BiMap<FieldKey, FieldDefinition<?>> map) throws Exception {
		if (map != null) {
			return new FieldDefinitions.Builder().addAll(map.values()).build();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public HashBiMap<FieldKey, FieldDefinition<?>> unmarshal(FieldDefinitions fields) throws Exception {
		if (fields != null) {
			HashBiMap<FieldKey, FieldDefinition<?>> map = HashBiMap.create();
			for (FieldDefinition<?> field : fields.getAll()) {
				map.put(field.getKey(), field);
			}
			return map;
		}
		return null;
	}

}
