/**
 *
 */
package com.aawhere.field.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.aawhere.field.Report;

/**
 * Annotation used to indicate that this Member is a {@link Dictionary} which should be considered
 * embedded in the enclosing class's dictionary.
 * 
 * Usefull for producing {@link Report}s that are "flattened".
 * 
 * @author Brian Chapman
 * 
 */
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface EmbeddedDictionary {

	/**
	 * If true the embedded dictionary will ignore it's field name and use the parent's.
	 * 
	 * For example Forest.Tree.Leaf would have forest-tree-leaf if false and forest-leaf if true.
	 * 
	 * Useful when there is a single field of an embedded dictionary.
	 * 
	 * @return
	 */
	boolean flatten() default false;
}
