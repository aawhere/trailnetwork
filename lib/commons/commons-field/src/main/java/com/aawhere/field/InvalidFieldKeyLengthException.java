/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 1, 2010
commons-lang : com.aawhere.field.InvalidFieldKeyLengthException.java
 */
package com.aawhere.field;

import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates the field key length is too long.
 * 
 * @author roller
 * 
 */
public class InvalidFieldKeyLengthException
		extends InvalidFieldKeyFormatException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2190421353484644766L;

	/**
	 * 
	 */
	public InvalidFieldKeyLengthException(String keyValue, Integer max) {
		super(new CompleteMessage.Builder().setMessage(FieldMessage.INVALID_KEY_LENGTH)
				.addParameter(FieldMessage.Param.FIELD_KEY, keyValue).addParameter(FieldMessage.Param.MAX_LENGTH, max)
				.build());
	}

}
