/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 4, 2010
commons-lang : com.aawhere.field.RecordSchema.java
 */
package com.aawhere.field;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.util.ListUtilsExtended;

/**
 * A set of {@link FieldDefinition}s that define what {@link Field}s are possible for a record.
 * 
 * Order matters and duplicates are not allowed (based on the key).
 * 
 * @author roller
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
public class RecordSchema
		implements Iterable<FieldDefinition<?>>, Serializable {

	private static final long serialVersionUID = -8859338203301622263L;

	/**
	 * Used to construct all instances of RecordSchema.
	 */
	public static class Builder
			extends ObjectBuilder<RecordSchema> {

		public Builder() {
			super(new RecordSchema());
		}

		public Builder addDefinition(FieldDefinition<?> definition) {
			Assertion.assertNotNull("definition", definition);
			building.definitions.add(definition);
			return this;
		}

		public Builder addDefinitions(FieldDefinitions definitions) {
			building.definitions.addAll(definitions.getAll());
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public RecordSchema build() {
			building.definitions = Collections.unmodifiableList(building.definitions);
			return super.build();
		}
	}// end Builder

	@XmlElement
	private List<FieldDefinition<?>> definitions = new LinkedList<FieldDefinition<?>>();

	/**
	 * @return the definitions
	 */
	public List<FieldDefinition<?>> getDefinitions() {
		return definitions;
	}

	/** Use {@link Builder} to construct RecordSchema */
	private RecordSchema() {
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<FieldDefinition<?>> iterator() {

		return this.definitions.iterator();
	}

	@SuppressWarnings("unchecked")
	public <ValueT> FieldDefinition<ValueT> getFieldDefinition(FieldKey key) {

		return (FieldDefinition<ValueT>) ListUtilsExtended.selectFirstOrNull(definitions, new FieldKeyPredicate(key));
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return this.definitions.toString();
	}

	/**
	 * @return
	 */
	public Integer getCount() {

		return this.definitions.size();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.definitions == null) ? 0 : this.definitions.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RecordSchema other = (RecordSchema) obj;
		if (this.definitions == null) {
			if (other.definitions != null) {
				return false;
			}
		} else if (!this.definitions.equals(other.definitions)) {
			return false;
		}
		return true;
	}

}
