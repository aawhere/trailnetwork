/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 16, 2010
commons-lang : com.aawhere.field.FieldKeyPredicate.java
 */
package com.aawhere.field;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

/**
 * Given a {@link FieldKey} this can be used by {@link CollectionUtils} to work with collections of
 * {@link FieldDefinition}.
 * 
 * @author roller
 * 
 */
public class FieldKeyPredicate
		implements Predicate {

	private FieldKey key;

	/**
	 * @param key
	 */
	public FieldKeyPredicate(FieldKey key) {
		super();
		this.key = key;
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {

		FieldDefinition<?> definition = (FieldDefinition<?>) object;

		return definition.getKey().equals(key);
	}

}
