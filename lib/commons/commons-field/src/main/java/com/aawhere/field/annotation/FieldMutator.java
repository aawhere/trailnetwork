/**
 * 
 */
package com.aawhere.field.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.aawhere.field.FieldKey;

/**
 * Indicates a Method will mutate the corresponding {@link Field}.
 * 
 * @author Aaron Roller
 * 
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldMutator {

	/**
	 * The {@link FieldKey} value which will fuse this mutator with the corresponding accessor.
	 * 
	 * @see com.aawhere.field.Defin
	 * @return
	 */
	String key();
}
