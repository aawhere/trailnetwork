/**
 *
 */
package com.aawhere.field;

import java.lang.reflect.Member;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.aawhere.field.annotation.AnnotatedDictionary;
import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.EmbeddedDictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;

import com.google.inject.Inject;

/**
 * Given an object annotated with {@link Dictionary}, produce a record containing FieldKeys from all
 * {@link Member}s annotated with {@link Field}.
 * 
 * @author Brian Chapman
 * 
 */
public class AnnotatedRecordBuilder
		extends Record.Builder {

	private Object annotated;
	private FieldAnnotationDictionaryFactory fieldDefinitionFactory;
	private Set<AnnotatedRecordBuilderFlags> flags = new HashSet<AnnotatedRecordBuilderFlags>();

	@Inject
	public AnnotatedRecordBuilder(FieldAnnotationDictionaryFactory fieldFactory) {
		this.fieldDefinitionFactory = fieldFactory;
	}

	public AnnotatedRecordBuilder setAnnotatedObject(Object object) {
		this.annotated = object;
		return this;
	}

	public AnnotatedRecordBuilder setFlag(AnnotatedRecordBuilderFlags flag) {
		flags.add(flag);
		return this;
	}

	/**
	 * @param flags
	 * @return
	 */
	public AnnotatedRecordBuilder addFlags(Collection<AnnotatedRecordBuilderFlags> flags) {
		this.flags.addAll(flags);
		return this;
	}

	@Override
	public void validate() {
		super.validate();
		if (!AnnotatedDictionaryUtils.isAnnotatedDictionary(annotated.getClass())) {
			throw new NotADictionaryException(this.annotated.getClass());
		}
	}

	@Override
	public Record build() {
		validate();
		addFields(annotated);
		return super.build();
	}

	/**
	 * Adds all the {@link Field}s from the supplied {@link AnnotatedDictionary} and any embedded
	 * dictionaries marked with {@link EmbeddedDictionary} to the record. Also builds the schema
	 * along side adding the Fields.
	 * 
	 * * TODO: for consideration, some of these methods could be retrofited to require the
	 * {@link AnnotatedDictionary} interface to be present for better compile time checking. That
	 * interface was removed since it wasn't widely in use at the time this class was created.
	 * 
	 * TODO: this method is doing 2 things, which isn't best practice, but for 1 line of code, we
	 * save having to loop over all the Members again and creating some complex delegate to allow
	 * for code reuse. Plus it is private.
	 * 
	 * @param annotated
	 * @param schemaBuilder
	 */
	@SuppressWarnings("unchecked")
	private void addFields(Object annotated) {

		FieldDictionary dictionary = fieldDefinitionFactory.getDictionary(annotated.getClass());

		for (FieldDefinition<?> field : dictionary) {
			if (!flags.contains(AnnotatedRecordBuilderFlags.SEARCHABLE)
					|| (flags.contains(AnnotatedRecordBuilderFlags.SEARCHABLE) && field.getSearchable() == true)) {

				Object value = AnnotatedDictionaryUtils.getValue(field.getKey(), annotated);
				addField((FieldDefinition<Object>) field, value);

			}
		}

		if (flags.contains(AnnotatedRecordBuilderFlags.FLATTEN)) {
			Map<Member, EmbeddedDictionary> embeddedDictionaries = AnnotatedDictionaryUtils.getMembers(annotated
					.getClass(), EmbeddedDictionary.class);
			for (Map.Entry<Member, EmbeddedDictionary> memberEntry : embeddedDictionaries.entrySet()) {
				Object embeddedDictionary = AnnotatedDictionaryUtils.getValue(memberEntry.getKey(), annotated);
				addFields(embeddedDictionary);
			}
		}
	}

	public static enum AnnotatedRecordBuilderFlags {
		FLATTEN,
		/**
		 * Only include searchable fields.
		 */
		SEARCHABLE;
	}

}
