/**
 *
 */
package com.aawhere.field;

import com.google.common.base.Function;

/**
 * Simple {@link Function} to translate from {@link FieldDefinition} to {@link FieldKey}
 * 
 * @author Brian Chapman
 * 
 */
public class FieldKeyFromFieldDefinitionFunction
		implements Function<FieldDefinition<?>, FieldKey> {

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public FieldKey apply(FieldDefinition<?> input) {
		return input.getKey();
	}

}
