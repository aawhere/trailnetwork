/**
 *
 */
package com.aawhere.field;

import com.aawhere.lang.ObjectBuilder;

/**
 * A recommended naming standard for these values is to user all lower case with dashes separating
 * each word or acronym (like-this-key). Only letters, numbers, and dash characters may be used.
 * Anything else is considered invalid.
 * 
 * 
 * @author roller
 * 
 */
public class FieldKeyValidator {

	private static FieldKeyValidator instance;

	/** Use Builder */
	@Deprecated
	public static FieldKeyValidator getInstance() {
		if (null == instance) {
			instance = new FieldKeyValidator();
		}
		return instance;
	}

	/**
	 * Used to construct all instances of FieldKeyValidator.
	 */
	public static class Builder
			extends ObjectBuilder<FieldKeyValidator> {

		public Builder() {
			super(new FieldKeyValidator());
		}

		public Builder setFieldDictionary(FieldDictionary dictionary) {
			building.dictionary = dictionary;
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private FieldDictionary dictionary;

	/** Use {@link Builder} to construct FieldKeyValidator */
	private FieldKeyValidator() {
	}

}
