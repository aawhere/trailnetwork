/**
 * 
 */
package com.aawhere.field.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation indicating the the class which can be mutated that corresponds to the
 * {@link Dictionary} annotation.
 * 
 * This annotation should be found on an {@link MutableAnnotatedDictionary} implementation.
 * 
 * @author Aaron Roller
 * 
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DictionaryMutator {
	/**
	 * The domain is the "subject" that this dictionary represents. Default is to use the class
	 * name.
	 */
	String domain();
}
