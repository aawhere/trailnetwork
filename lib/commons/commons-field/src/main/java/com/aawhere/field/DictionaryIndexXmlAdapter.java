/**
 * 
 */
package com.aawhere.field;

import java.util.HashSet;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.google.common.collect.Sets;

/**
 * @see DictionaryIndex
 * @author aroller
 * 
 */
public class DictionaryIndexXmlAdapter
		extends XmlAdapter<HashSet<FieldKey>, DictionaryIndex> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public DictionaryIndex unmarshal(final HashSet<FieldKey> v) throws Exception {
		return new DictionaryIndex(v) {
		};
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public HashSet<FieldKey> marshal(DictionaryIndex v) throws Exception {
		if (v != null) {

			return Sets.newHashSet(v.keys());
		} else {
			return null;
		}
	}

}
