/**
 *
 */
package com.aawhere.field.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.core.annotation.AnnotationUtils;

import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldNotIndexedException;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.annotation.AnnotationUtilsExtended;
import com.aawhere.lang.string.StringUtilsExtended;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

/**
 * Utilities for referencing {@link AnnotatedDictionary}.
 * 
 * TODO: for consideration, some of these methods could be retrofitted to require the
 * {@link AnnotatedDictionary} interface to be present for better compile time checking. That
 * interface was removed since it wasn't in use at the time this class was created.
 * 
 * @author Brian Chapman
 * 
 */
public class AnnotatedDictionaryUtils {

	/**
	 * Get all members of annotatedType that are annotated with annotationType. Also checks that
	 * annotatedType is a {@link Dictionary}.
	 * 
	 * 
	 * @param annotatedType
	 * @param annotationType
	 * @return
	 */
	public static <A extends Annotation> Map<Member, A> getMembers(Class<?> annotatedType, Class<A> annotationType) {
		Assertion.exceptions().assertTrue(isAnnotatedDictionary(annotatedType));
		Map<Member, A> members = AnnotationUtilsExtended.getInstance().findMembers(annotationType, annotatedType);
		return members;
	}

	/**
	 * Determines if the class is a {@link Dictionary} or not.
	 * 
	 * @param annotatedType
	 * @return
	 */
	public static Boolean isAnnotatedDictionary(Class<?> annotatedType) {
		if (annotatedType == null) {
			return false;
		}
		return AnnotationUtilsExtended.getInstance().hasAnnoation(annotatedType, Dictionary.class);
	}

	/**
	 * Provides the dictionary annotation from a class that declares it.
	 * 
	 * @see AnnotationUtil#classAnnotationBottumUp(Class, Class)
	 * 
	 * @param annotatedClass
	 * @return
	 */
	public static @Nullable
	Dictionary dictionaryFrom(@Nonnull Class<?> annotatedClass) {
		Dictionary dictionaryAnnotation = AnnotationUtils.findAnnotation(annotatedClass, Dictionary.class);
		return dictionaryAnnotation;
	}

	public static String domainFrom(@Nonnull Class<?> annotatedClass) {
		Dictionary dictionaryAnnotation = dictionaryFrom(annotatedClass);
		return domainFrom(dictionaryAnnotation);
	}

	/**
	 * @param annotatedClass
	 * @param dictionaryAnnotation
	 * @return
	 */
	static String domainFrom(Dictionary dictionaryAnnotation) {
		String domain = dictionaryAnnotation.domain();
		return domain;
	}

	/**
	 * @param member
	 * @param fieldAnnotation
	 * @return
	 */
	public static String keyFrom(Member member, Field fieldAnnotation) {
		String key = fieldAnnotation.key();
		// use hyphenated field name if not provided
		if (key.equals(Field.UNDEFINED)) {
			String fieldName = member.getName();
			key = formatFieldKeyValue(fieldName);
		}
		return key;
	}

	/**
	 * Only supports accessor style names.
	 * 
	 * @param fieldName
	 * @return
	 */
	private static String formatFieldKeyValue(String fieldName) {
		String key = StringUtilsExtended.accessorToProperty(fieldName);
		return key;
	}

	public static FieldKey fieldKeyFrom(Member member, Field fieldAnnotation) {
		Dictionary dictionary = dictionaryFrom(member.getDeclaringClass());
		return new FieldKey(dictionary.domain(), keyFrom(member, fieldAnnotation));
	}

	@SuppressWarnings("unchecked")
	public static <T> T getValue(FieldKey key, Object annotatedDictionary) {
		Member member = member(key, annotatedDictionary);
		if (member != null) {
			return (T) getValue(member, annotatedDictionary);
		} else {
			return null;
		}
	}

	/**
	 * Searches the given annotatedDictionary for members that have a Field with the given key.
	 * 
	 * @param key
	 * @param annotatedDictionary
	 * @return the member matching the given field key or null if none are found.
	 * @throws FieldNotRegisteredException
	 */
	public static Member member(FieldKey key, Object annotatedDictionary) {
		return member(key, annotatedDictionary, domainFrom(annotatedDictionary.getClass()));
	}

	/**
	 * Searches the given annotatedDictionary for members that have a Field with the given key.
	 * 
	 * @param key
	 * @param annotatedDictionary
	 * @param domainFrom
	 * @return the member matching the given field key or null if none are found.
	 * @throws FieldNotRegisteredException
	 */
	public static Member member(FieldKey key, Object annotatedDictionary, String domainFrom) {
		Map<Member, Field> members = getMembers(annotatedDictionary.getClass(), Field.class);
		Assertion.exceptions().eq(key.getDomainKey(), domainFrom);
		String searchingForKeyValue = key.getRelativeKey();
		for (Entry<Member, Field> element : members.entrySet()) {
			String memberKey = FieldAnnotationDictionaryFactory.keyFrom(element.getKey(), element.getValue());
			if (memberKey.equals(searchingForKeyValue)) {
				return element.getKey();
			}
		}
		// FIXME:Throw the correct exception
		throw new FieldNotIndexedException(key);
	}

	/**
	 * Attempts to get the return value if the member is a method or the assigned object if it is a
	 * field.
	 * 
	 * TODO: this might qualify for integration into {@link AnnotationUtilsExtended}.
	 * 
	 * @param member
	 * @param annotatedDictionary
	 *            the instance of the class that the member belongs to
	 * @return
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public static Object getValue(Member member, Object annotatedDictionary) {
		Object value = null;
		try {
			if (member instanceof Method) {
				((Method) member).setAccessible(true);
				value = ((Method) member).invoke(annotatedDictionary);
			} else if (member instanceof java.lang.reflect.Field) {
				((java.lang.reflect.Field) member).setAccessible(true);
				value = ((java.lang.reflect.Field) member).get(annotatedDictionary);
			}
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		return value;

	}

	/**
	 * specific method to extract the value from the given object for the field matching the given
	 * definition.
	 * 
	 * @param definition
	 * @param key
	 * @return
	 */
	public static <E, V extends Comparable<V>> Function<E, V> fieldComparableValueFunction(final FieldKey key) {
		return fieldValueFunction(key);
	}

	public static <F, T> Function<F, T> fieldValueFunction(final FieldKey key) {
		return new Function<F, T>() {

			@Override
			public T apply(F input) {
				if (input == null) {
					return null;
				}
				final T value = AnnotatedDictionaryUtils.getValue(key, input);
				return value;
			}
		};
	}

	/**
	 * Provides the values from the provided dictionaries that match the given {@link FieldKey}.
	 * Nulls are filtered out.
	 * 
	 * @see #fieldValueFunction(FieldKey)
	 * 
	 * @param key
	 * @param t
	 * @return
	 */
	public static <E, V> Iterable<V> getValues(FieldKey key, Iterable<E> dictionaries) {
		final Function<E, V> fieldValueFunction = fieldValueFunction(key);
		return Iterables.filter(Iterables.transform(dictionaries, fieldValueFunction), Predicates.notNull());

	}
}
