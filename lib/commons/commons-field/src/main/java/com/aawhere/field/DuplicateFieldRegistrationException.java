/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 1, 2010
commons-lang : com.aawhere.field.DuplicateFieldRegistrationException.java
 */
package com.aawhere.field;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates an attempt to register a {@link FieldDefinition} previously registered.
 * 
 * @author roller
 * 
 */
public class DuplicateFieldRegistrationException
		extends BaseRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8649977409078859180L;

	/**
	 * 
	 */
	public DuplicateFieldRegistrationException(FieldKey key) {
		super(new CompleteMessage.Builder().setMessage(FieldMessage.DUPLICATE_REGISTRATION)
				.addParameter(FieldMessage.Param.FIELD_KEY, key).build());
	}
}
