/**
 * 
 */
package com.aawhere.field.annotation;

/**
 * A marker indicating this class is the mutator for an {@link AnnotatedDictionary} class.
 * 
 * @author Aaron Roller
 * 
 */
public interface MutableAnnotatedDictionary {

}
