/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 1, 2010
commons-lang : com.aawhere.field.DuplicateFieldRegistrationException.java
 */
package com.aawhere.field;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * The class you thought was a dictionary was not! Ha!
 * 
 * @author Brian Chapman
 * 
 */
public class NotADictionaryException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 1103695665576560369L;

	/**
	 *
	 */
	public NotADictionaryException(Class<?> thoughtThisWasADictionary) {
		super(new CompleteMessage.Builder().setMessage(FieldMessage.NOT_A_DICTIONARY)
				.addParameter(FieldMessage.Param.OBJECT, thoughtThisWasADictionary.getName()).build());
	}
}
