/**
 *
 */
package com.aawhere.field.message;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * @author brian
 * 
 */
public class MessageDataTypeTextAdapterUnitTest {

	/**
	 * Test method for
	 * {@link com.aawhere.field.message.MessageDataTypeTextAdapter#marshal(java.lang.Object)}.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testMarshal() throws BaseException {
		Bike road = Bike.ROAD;
		String expected = Bike.ROAD.getValue();
		MessageDataTypeTextAdapter adapter = new MessageDataTypeTextAdapter();
		FieldData marshalled = adapter.marshal(road);
		assertEquals(expected, marshalled.getValue());
		assertEquals(FieldDataType.TEXT, marshalled.getType());

		Message unmarshalled = adapter.unmarshal(marshalled, Bike.class);
		assertEquals(road.getValue(), unmarshalled.getValue());
	}

	public enum Bike implements Message {
		ROAD("Road"), MOUNTAIN("Mountain");

		private String value;

		private Bike(String value) {
			this.value = value;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.util.rb.Message#getValue()
		 */
		@Override
		public String getValue() {
			return value;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.util.rb.Message#getParameterKeys()
		 */
		@Override
		public ParamKey[] getParameterKeys() {
			// TODO Auto-generated method stub
			return null;
		}
	}

}
