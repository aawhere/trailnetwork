/**
 *
 */
package com.aawhere.field;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.field.AnnotatedRecordBuilder.AnnotatedRecordBuilderFlags;
import com.aawhere.field.annotation.AnnotatedDictionary;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.EmbeddedDictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;

/**
 * @author Brian Chapman
 * 
 */
public class AnnotatedRecordBuilderUnitTest {

	@Test
	public void testBuildRecord() {

		Parent annotatedInstance = new Parent();
		FieldDictionaryFactory fieldDictionaryFactory = new FieldDictionaryFactory();
		FieldAnnotationDictionaryFactory fieldAnnotationDictionaryFactory = new FieldAnnotationDictionaryFactory(
				fieldDictionaryFactory);
		// register the fields by getting the dictionary.
		fieldAnnotationDictionaryFactory.getDictionary(Parent.class);
		fieldAnnotationDictionaryFactory.getDictionary(Child.class);

		Record record = new AnnotatedRecordBuilder(fieldAnnotationDictionaryFactory)
				.setAnnotatedObject(annotatedInstance).setFlag(AnnotatedRecordBuilderFlags.FLATTEN).build();
		assertNotNull(record);
		assertEquals(Integer.valueOf(2), record.getFieldCount());

	}

	@Test(expected = NotADictionaryException.class)
	public void testObjectWithNoFields() {
		Empty instance = new Empty();
		FieldDictionaryFactory fieldDictionaryFactory = new FieldDictionaryFactory();
		FieldAnnotationDictionaryFactory fieldAnnotationDictionaryFactory = new FieldAnnotationDictionaryFactory(
				fieldDictionaryFactory);

		new AnnotatedRecordBuilder(fieldAnnotationDictionaryFactory).setAnnotatedObject(instance)
				.setFlag(AnnotatedRecordBuilderFlags.FLATTEN).build();
	}

	@Dictionary(domain = FIELD.PARENT_DOMAIN)
	public static class Parent
			implements AnnotatedDictionary {
		public static final String FIELD_VALUE = "foo";
		@Field
		public String parentField = FIELD_VALUE;

		@EmbeddedDictionary
		Child child = new Child();
	}

	@Dictionary(domain = FIELD.CHILD_DOMAIN)
	public static class Child
			implements AnnotatedDictionary {
		public static final String FIELD_VALUE = "bar";
		@Field
		String childField = FIELD_VALUE;
	}

	public static class Empty {

	}

	public static final class FIELD {
		public static final String PARENT_DOMAIN = "parent";
		public static final String CHILD_DOMAIN = "child";
		public static final String CHILD_FIELD = "childField";
		public static final String PARENT_FIELD = "parentField";

		public static final class KEY {
			public static final FieldKey PARENT_FIELD = new FieldKey(FIELD.PARENT_DOMAIN, FIELD.PARENT_FIELD);
			public static final FieldKey CHILD_FIELD = new FieldKey(FIELD.CHILD_DOMAIN, FIELD.CHILD_FIELD);
		}
	}
}
