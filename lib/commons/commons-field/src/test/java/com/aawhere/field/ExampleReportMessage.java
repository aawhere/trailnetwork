/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 4, 2010
commons-lang : com.aawhere.field.ExampleRecordSchemaMessage.java
 */
package com.aawhere.field;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Demonstrates the proper use of I18N for {@link Report}s and {@link Field}s.
 * 
 * @author roller
 * 
 */
public enum ExampleReportMessage implements Message {

	/** Example of the title of the domain */
	ANIMAL_NAME("Animal"),
	/** Describes the domain to users can understand. */
	ANIMAL_DESCRIPTION(
			"A living organism that feeds on organic matter, typically having specialized sense organs and nervous system and able to respond rapidly"),
	/** Report Name */
	ANIMAL_KINGDOM_NAME("Animal Kingdom"),
	/** Schema */
	ANIMAL_KINGDOM_DESCRIPTION("Describes all the animals in the world."),
	ANIMAL_TYPE_NAME("Species"),
	ANIMAL_TYPE_DESCRIPTION("The type of animal."),
	ANIMAL_ABOUT_NAME("Description"),
	ANIMAL_ABOUT_DESCRIPTION("More information about the animal."),
	NUMBER_OF_LEGS_NAME("Legs"),
	NUMBER_OF_LEGS_DESCRIPTION("The number of legs used to walk."),
	/** Records */
	DOG_NAME("Dog"),
	DOG_DESCRIPTION("Domestic canine."),
	CAT_NAME("Cat"),
	CAT_DESCRIPTION("Domestic feline"),
	CHIMP_NAME("Chimp"),
	CHIMP_DESCRIPTION("Like a monkey"),
	/** demonstrates how to name fields. */
	SPECIES_NAME("Species"),
	SPECIES_DESCRIPTION(
			"A group of living organisms consisting of similar individuals capable of exchanging genes or interbreeding. "),
	HAIR_COLOR_NAME("Hair Color"),
	HAIR_COLOR_DESCRIPTION(
			"The pigmentation of hair follicles due to two types of melanin, eumelanin and pheomelanin. "),

	ORDER_NAME("Order"),
	ORDER_DESCRIPTION("A taxonomic category of organisms ranking above a family and below a class.");
	private ParamKey[] parameterKeys;
	private String message;

	private ExampleReportMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** EXPLAIN_PARAM_HERE */
		FIRST_PARAM;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
