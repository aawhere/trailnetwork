/**
 *
 */
package com.aawhere.field.annotation;

import static org.junit.Assert.*;

import java.lang.reflect.Member;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import com.aawhere.field.AnnotatedRecordBuilderUnitTest;
import com.aawhere.field.AnnotatedRecordBuilderUnitTest.Parent;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldNotIndexedException;

/**
 * @author brian
 * 
 */
public class AnnotatedDictionaryUtilsUnitTest {

	/**
	 * Test method for
	 * {@link com.aawhere.field.annotation.AnnotatedDictionaryUtils#getMembers(java.lang.Class, java.lang.Class)}
	 * .
	 */
	@Test
	public void testGetMembers() {
		Map<Member, Field> members = AnnotatedDictionaryUtils
				.getMembers(AnnotatedRecordBuilderUnitTest.Parent.class, Field.class);
		assertEquals(1, members.size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetMembersWithNonDictionary() {
		AnnotatedDictionaryUtils.getMembers(AnnotatedRecordBuilderUnitTest.Empty.class, Field.class);
	}

	@Test
	public void testGetValueByKey() {

		final AnnotatedRecordBuilderUnitTest.Child child = new AnnotatedRecordBuilderUnitTest.Child();
		String value = AnnotatedDictionaryUtils.getValue(AnnotatedRecordBuilderUnitTest.FIELD.KEY.CHILD_FIELD, child);
		assertEquals("incorrect value returned", AnnotatedRecordBuilderUnitTest.Child.FIELD_VALUE, value);
	}

	@Test
	public void testGetMember() {
		Member member = AnnotatedDictionaryUtils.member(AnnotatedRecordBuilderUnitTest.FIELD.KEY.CHILD_FIELD,
														new AnnotatedRecordBuilderUnitTest.Child());
		assertNotNull(member);
	}

	/** When passing a child when you are looking for the parent. */
	@Test(expected = IllegalArgumentException.class)
	public void tesGetMembertWrongDomain() {
		AnnotatedDictionaryUtils.member(AnnotatedRecordBuilderUnitTest.FIELD.KEY.PARENT_FIELD,
										new AnnotatedRecordBuilderUnitTest.Child());
	}

	/**
	 * Provides an invalid key to ensure proper communication FIXME: expect the proper exception
	 */
	@Test(expected = FieldNotIndexedException.class)
	public void testGetMemberKeyNotFound() {
		AnnotatedDictionaryUtils.member(new FieldKey(AnnotatedRecordBuilderUnitTest.FIELD.CHILD_DOMAIN, "junk"),
										new AnnotatedRecordBuilderUnitTest.Child());
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.annotation.AnnotatedDictionaryUtils#isAnnotatedDictionary(java.lang.Class)}
	 * .
	 */
	@Test
	public void testIsAnnotatedDictionary() {
		Boolean resultTrue = AnnotatedDictionaryUtils
				.isAnnotatedDictionary(AnnotatedRecordBuilderUnitTest.Parent.class);
		Boolean resultFalse = AnnotatedDictionaryUtils
				.isAnnotatedDictionary(AnnotatedRecordBuilderUnitTest.Empty.class);
		assertTrue(resultTrue);
		assertTrue(!resultFalse);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.annotation.AnnotatedDictionaryUtils#dictionaryFrom(java.lang.Class)}
	 * .
	 */
	@Test
	public void testDictionaryFrom() {
		Dictionary dictionary = AnnotatedDictionaryUtils.dictionaryFrom(AnnotatedRecordBuilderUnitTest.Parent.class);
		assertNotNull(dictionary);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.annotation.AnnotatedDictionaryUtils#keyFrom(java.lang.reflect.Member, com.aawhere.field.annotation.Field)}
	 * .
	 */
	@Test
	public void testKeyFrom() {
		Map<Member, Field> members = AnnotatedDictionaryUtils
				.getMembers(AnnotatedRecordBuilderUnitTest.Parent.class, Field.class);
		for (Entry<Member, Field> entry : members.entrySet()) {
			String key = AnnotatedDictionaryUtils.keyFrom(entry.getKey(), entry.getValue());
			assertEquals("parentField", key);
		}
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.annotation.AnnotatedDictionaryUtils#fieldKeyFrom(java.lang.reflect.Member, com.aawhere.field.annotation.Field)}
	 * .
	 */
	@Test
	public void testFieldKeyFrom() {
		Map<Member, Field> members = AnnotatedDictionaryUtils
				.getMembers(AnnotatedRecordBuilderUnitTest.Parent.class, Field.class);
		for (Entry<Member, Field> entry : members.entrySet()) {
			FieldKey key = AnnotatedDictionaryUtils.fieldKeyFrom(entry.getKey(), entry.getValue());
			assertEquals("parent", key.getDomainKey());
			assertEquals("parentField", key.getRelativeKey());
		}
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.annotation.AnnotatedDictionaryUtils#getValue(java.lang.reflect.Member, java.lang.Object)}
	 * .
	 */
	@Test
	public void testGetValue() {
		Parent parent = new AnnotatedRecordBuilderUnitTest.Parent();
		Map<Member, Field> members = AnnotatedDictionaryUtils
				.getMembers(AnnotatedRecordBuilderUnitTest.Parent.class, Field.class);
		for (Entry<Member, Field> entry : members.entrySet()) {
			Object value = AnnotatedDictionaryUtils.getValue(entry.getKey(), parent);
			assertEquals(parent.parentField, value);
		}

	}

}
