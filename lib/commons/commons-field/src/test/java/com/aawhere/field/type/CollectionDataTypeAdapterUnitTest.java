/**
 *
 */
package com.aawhere.field.type;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;

import com.aawhere.lang.exception.BaseException;

/**
 * @author brian
 * 
 */
public class CollectionDataTypeAdapterUnitTest {

	/**
	 * Test method for
	 * {@link com.aawhere.field.type.CollectionDataTypeAdapter#marshal(java.lang.Object)}.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testMarshal() throws BaseException {
		CharSeqenceDataTypeTextAdapter stringAdapter = new CharSeqenceDataTypeTextAdapter();
		CollectionDataTypeAdapter<Collection<CharSequence>, CharSequence> collectionAdapter = new CollectionDataTypeAdapter.Builder<Collection<CharSequence>, CharSequence>()
				.setAdapter(stringAdapter).setParamaterType(CharSequence.class).build();
		Collection<String> strings = Arrays.asList("a", "b", "c");
		FieldData marshalled = collectionAdapter.marshal(strings);
		@SuppressWarnings("unchecked")
		Collection<String> unmarshalled = collectionAdapter.unmarshal(marshalled, Collection.class);
		assertEquals(strings, unmarshalled);
	}

}
