/**
 *
 */
package com.aawhere.field.message;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.field.message.MessageDataTypeTextAdapterUnitTest.Bike;
import com.aawhere.field.type.FieldData;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.exception.BaseException;

/**
 * @author brian
 * 
 */
public class EnumDataTypeAtomAdapterUnitTest {

	/**
	 * Test method for
	 * {@link com.aawhere.field.message.MessageDataTypeTextAdapter#marshal(java.lang.Object)}.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testMarshal() throws BaseException {
		Bike road = Bike.ROAD;
		String expected = Bike.ROAD.toString();
		EnumDataTypeAtomAdapter adapter = new EnumDataTypeAtomAdapter();
		FieldData marshalled = adapter.marshal(road);
		assertEquals(expected, marshalled.getValue());
		assertEquals(FieldDataType.ATOM, marshalled.getType());

		Bike unmarshalled = adapter.unmarshal(marshalled, Bike.class);
		assertEquals(road, unmarshalled);
	}

}
