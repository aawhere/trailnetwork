/**
 * 
 */
package com.aawhere.field.annotation;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.Set;

import org.reflections.ReflectionUtils;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.FieldTestUtil;

/**
 * @author aroller
 * 
 */
public class FieldAnnotationTestUtil
		extends FieldTestUtil {

	/**
	 * 
	 */
	protected FieldAnnotationTestUtil() {

	}

	public static FieldAnnotationDictionaryFactory getAnnoationFactory() {
		return new FieldAnnotationDictionaryFactory(getDictionaryFactory());
	}

	/**
	 * Encouraging a standard naming this will inspect the given dictionary class for static inner
	 * class FIELD.KEY and inspect that all keys declared are found when inspecting the dictionary.
	 * 
	 * <pre>
	 * FIELD declares the strings referenced in the annotations 
	 * KEY declares the actual Keys that reference the strings in FIELD
	 * </pre>
	 * 
	 * @param annotatedDictionaryClass
	 */
	public static void assertFieldsAvailable(Class<?> annotatedDictionaryClass) {
		String standardClassName = "$FIELD$KEY";
		String className = annotatedDictionaryClass.getName() + standardClassName;
		try {
			Class<?> classWithDeclarations = Class.forName(className);
			assertFieldsAvailable(annotatedDictionaryClass, classWithDeclarations);
		} catch (ClassNotFoundException e) {
			fail(annotatedDictionaryClass + " doesn't have the standard " + standardClassName);
		}
	}

	/**
	 * using it's own annoation factory it will recursively inspect the given dicationary class for
	 * fields and verify that declared fields given on the classWithDeclaredFields all are found.
	 * 
	 * @param annoationFactory
	 * @param annotatedDictionaryClass
	 * @throws IllegalAccessException
	 */
	public static void assertFieldsAvailable(Class<?> annotatedDictionaryClass, Class<?> classWithDeclaredKeys) {
		FieldAnnotationDictionaryFactory annoationFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		annoationFactory.getDictionary(annotatedDictionaryClass);
		FieldDictionaryFactory factory = annoationFactory.getFactory();
		@SuppressWarnings("unchecked")
		Set<Field> allFields = ReflectionUtils.getAllFields(classWithDeclaredKeys,
															ReflectionUtils.withType(FieldKey.class));
		if (allFields.isEmpty()) {
			fail("no field keys found on " + classWithDeclaredKeys.getSimpleName() + " for "
					+ annotatedDictionaryClass.getSimpleName());
		}
		for (Field field : allFields) {
			FieldKey fieldKey;
			try {
				fieldKey = (FieldKey) field.get(null);
			} catch (IllegalArgumentException e1) {
				throw new RuntimeException(e1);
			} catch (IllegalAccessException e1) {
				throw new RuntimeException(e1);
			}
			try {
				factory.findDefinition(fieldKey);
			} catch (FieldNotRegisteredException e) {
				fail(fieldKey.toString() + " wasn't registered in " + factory.getDefinitions());
			}
		}
	}

	/**
	 * @param fieldDictionaryFactory
	 * @return
	 */
	public static FieldAnnotationDictionaryFactory getAnnoationFactory(FieldDictionaryFactory fieldDictionaryFactory) {
		return new FieldAnnotationDictionaryFactory(fieldDictionaryFactory);
	}
}
