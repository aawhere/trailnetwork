/**
 *
 */
package com.aawhere.field;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author brian
 * 
 */
public class RecordUnitTest {

	private Record sample;
	private FieldKey key;
	Field<String> sampleField;
	FieldDefinition<String> sampleFieldDefinition;
	RecordSchema schema;

	@Before
	public void setUp() {
		key = Examples.MY_KEY;
		sampleFieldDefinition = Examples.MY_DEFINITION;
		sampleField = Examples.MY_FIELD;
		schema = Examples.MY_SCHEMA;
		sample = Examples.MY_RECORD;
	}

	/**
	 * Test method for {@link com.aawhere.field.Record#getField(com.aawhere.field.FieldKey)}.
	 */
	@Test
	public void testGetFieldFieldKey() {
		assertEquals(sampleField, sample.getField(key));
	}

	/**
	 * Test method for {@link com.aawhere.field.Record#getField(com.aawhere.field.FieldDefinition)}.
	 */
	@Test
	public void testGetFieldFieldDefinitionOfValueT() {
		assertEquals(sampleField, sample.getField(sampleFieldDefinition));
	}

	/**
	 * Test method for {@link com.aawhere.field.Record#hasField(com.aawhere.field.FieldKey)}.
	 */
	@Test
	public void testHasFieldFieldKey() {
		assertTrue(sample.hasField(key));
	}

	/**
	 * Test method for {@link com.aawhere.field.Record#hasField(com.aawhere.field.FieldDefinition)}.
	 */
	@Test
	public void testHasFieldFieldDefinitionOfQ() {
		assertTrue(sample.hasField(sampleFieldDefinition));
	}

	/**
	 * Test method for {@link com.aawhere.field.Record#getFieldCount()}.
	 */
	@Test
	public void testGetFieldCount() {
		assertEquals(Integer.valueOf(1), sample.getFieldCount());
	}

}
