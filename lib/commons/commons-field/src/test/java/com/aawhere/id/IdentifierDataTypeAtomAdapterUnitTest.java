/**
 *
 */
package com.aawhere.id;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.field.type.FieldData;
import com.aawhere.lang.exception.BaseException;

/**
 * @author Brian Chapman
 * 
 */
public class IdentifierDataTypeAtomAdapterUnitTest {

	@Test
	public void testMarshall() throws BaseException {
		String idString = "testId";
		IdentifierDataTypeAtomAdapter adapter = new IdentifierDataTypeAtomAdapter();
		MockIdentifier id = new MockIdentifier(idString);
		FieldData marshaled = adapter.marshal(id);
		assertEquals(idString, marshaled.getValue());

		MockIdentifier unmarshalled = adapter.unmarshal(marshaled, MockIdentifier.class);
		assertEquals(id, unmarshalled);
	}

	public static class MockIdentifier
			extends StringIdentifier<MockIdentifier> {

		private static final long serialVersionUID = 6068747483105777544L;

		public MockIdentifier(String id) {
			super(id, MockIdentifier.class);
		}
	}

}
