/**
 *
 */
package com.aawhere.field;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author Brian Chapman
 * 
 */
public class ReportUnitTest {

	private Report sampleReport;
	private Class<?>[] classesToBeBound = { Field.class, Report.class, Record.class };

	@Before
	public void setUp() {
		sampleReport = Examples.MY_REPORT;
	}

	@Test
	public void testJaxBRoundTrip() {
		JAXBTestUtil<Report> util = new JAXBTestUtil.Builder<Report>().element(sampleReport)
				.classesToBeBound(classesToBeBound).build();
		Report actual = util.getActual();
		assertNotNull(actual);
		assertEquals(sampleReport.getNumberOfRecords(), actual.getNumberOfRecords());
		List<Record> sampleRecords = sampleReport.getRecords();
		List<Record> actualRecords = actual.getRecords();
		for (int i = 0; i < sampleRecords.size(); i++) {
			// They should be in the same order.
			assertEquals(sampleRecords.get(i), actualRecords.get(i));
		}
		assertEquals(sampleReport, actual);
	}

}
