/**
 *
 */
package com.aawhere.field.annotation;

import static com.aawhere.field.annotation.FieldAnnotationExample.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.field.ExampleReportMessage;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.annotation.FieldAnnotationExample.Animal;
import com.aawhere.field.annotation.FieldAnnotationExample.Branch;
import com.aawhere.field.annotation.FieldAnnotationExample.FirstSecond;
import com.aawhere.field.annotation.FieldAnnotationExample.LoneTree;
import com.aawhere.field.annotation.FieldAnnotationExample.Species;
import com.aawhere.field.annotation.FieldAnnotationExample.Tree;
import com.aawhere.field.type.FieldDataType;

/**
 * Tests {@link Dictionary} and {@link com.aawhere.field.Field} construction using
 * {@link Dictionary} and {@link Field} annotations.
 * 
 * @see FieldAnnotationExample
 * 
 * @author Aaron Roller
 * 
 */
public class FieldAnnotationDictionaryUnitTest {

	static final String DOMAIN = "animal";

	private FieldAnnotationDictionaryFactory factory;

	@Before
	public void setUp() {
		factory = FieldAnnotationTestUtil.getAnnoationFactory();

	}

	@Test
	public void testRegistrationOfDictionary() throws FieldNotRegisteredException {

		FieldDictionary dictionary = getDictionary();

		// test provided values
		FieldDefinition<String> typeDef = dictionary.findDefinition(FieldAnnotationExample.TYPE_KEY);
		assertNotNull(typeDef);

		// tests automatic conversion since default
		FieldDefinition<Integer> numberOfLegsDef = dictionary.findDefinition(FieldAnnotationExample.LEGS_KEY);
		assertNotNull(numberOfLegsDef);

		FieldDefinition<Species> speciesDef = dictionary.findDefinition(FieldAnnotationExample.SPECIES_DOMAIN_KEY);
		assertNotNull(speciesDef);
	}

	@Test
	public void testGetFromRegularFactory() {
		// this registers it...or it is supposed to.
		FieldDictionary expectedDictionary = getDictionary();
		String domainKey = expectedDictionary.getDomainKey();
		FieldDictionary actualDictionary = factory.getFactory().getDictionary(domainKey);
		assertEquals(domainKey, actualDictionary.getDomainKey());
	}

	@Test
	public void testFlattenNestedDictionary() throws FieldNotRegisteredException {
		FieldDictionary dictionary = factory.getDictionary(LoneTree.class);
		FieldDefinition<Object> definition = dictionary.findDefinition(LoneTree.Key.FLATTENED_BRANCH);
		assertEquals(LoneTree.Key.FLATTENED_BRANCH, definition.getKey());

	}

	@Test(expected = UnsupportedOperationException.class)
	public void testMissingDictionaryAnnotation() {
		factory.getDictionary(FieldAnnotationExample.DictionaryNoAnnotation.class);
		fail("a class with no Dictionary annotation should be denied.");
	}

	/**
	 * ensures the name and description are extracted from the Message enumerations.
	 * 
	 * @throws FieldNotRegisteredException
	 * 
	 */
	@Test
	public void testMessageExtraction() throws FieldNotRegisteredException {
		FieldDictionary dictionary = getDictionary();
		assertEquals(DOMAIN, dictionary.getDomainKey());
		assertEquals(ExampleReportMessage.ANIMAL_NAME, dictionary.getDomain());
		assertEquals(ExampleReportMessage.ANIMAL_DESCRIPTION, dictionary.getDescription());
		FieldDefinition<Object> definition = dictionary.findDefinition(TYPE_KEY);
		assertNotNull(definition);
		assertEquals(ExampleReportMessage.ORDER_NAME, definition.getName());
		assertEquals(ExampleReportMessage.ORDER_DESCRIPTION, definition.getDescription());
		assertEquals(FieldDataType.NONE, definition.getDataType());
		assertEquals(	"documentation should come from swagger annotations when not provided in enums",
						Animal.COLUMN_NAME_PROPERTY_DESCRIPTON,
						dictionary.findDefinition(COLUMN_NAME_KEY).getDescription().toString());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testRegistrationNoDomain() {
		factory.getDictionary(getClass());
		fail("the factory should complain if no domain is registered");
	}

	@Test(expected = IllegalStateException.class)
	public void testCollectionWithNoParameterType() {
		factory.getDictionary(new TestDomain() {
			@Field
			private List<String> list;
		}.getClass());
	}

	@Test
	public void testParameterType() {
		final String key = "key";
		Class<String> expected = String.class;
		FieldDictionary dictionary = factory.getDictionary(new TestDomain() {
			@Field(key = key, parameterType = String.class)
			private List<String> list;
		}.getClass());
		FieldDefinition<Object> definition = dictionary.getDefinition(key);
		assertEquals(expected, definition.getParameterType());
	}

	/**
	 * @return
	 */
	private FieldDictionary getDictionary() {
		return factory.getDictionary(Animal.class);
	}

	@Test
	public void testSplitWords() {
		FieldDictionary dictionary = factory.getDictionary(FirstSecond.class);
		final String expected = FirstSecond.FIRST_SECOND;
		assertEquals(expected, dictionary.getDomainKey());
		// ensures the field with the same name is registered.
		assertNotNull(dictionary.getDefinition(expected));
	}

	/**
	 * When a DIctionary references a Field that its type resolves to a Dictionary then that field
	 * is nested dictionary. This tests that the nested dictionary is respected.
	 * 
	 * @throws FieldNotRegisteredException
	 */
	@Test
	public void testNestedDictionary() throws FieldNotRegisteredException {
		FieldDictionary dictionary = factory.getDictionary(Tree.class);
		FieldDefinition<Branch> lowestBranchDefinition = dictionary.findDefinition(Tree.Key.LOWEST_BRANCH);
		assertNotNull(lowestBranchDefinition);
		FieldDictionary lowestBranchDictionary = lowestBranchDefinition.getNested();
		assertNotNull(lowestBranchDictionary);
		final String lowestBranchDomainKey = lowestBranchDefinition.getKey().getAbsoluteKey();
		assertEquals(lowestBranchDomainKey, lowestBranchDictionary.getDomainKey());
		FieldDefinition<Integer> lowestBranchNumberOfLeavesDefinition = lowestBranchDictionary
				.getDefinition(Branch.NUMBER_OF_LEAVES);
		assertNotNull(lowestBranchNumberOfLeavesDefinition);
		assertEquals(Branch.NUMBER_OF_LEAVES, lowestBranchNumberOfLeavesDefinition.getKey().getRelativeKey());
		assertEquals(	"dictioanry domain + relative key",
						new FieldKey(lowestBranchDomainKey, Branch.NUMBER_OF_LEAVES),
						lowestBranchNumberOfLeavesDefinition.getKey());

		assertEquals(lowestBranchDomainKey, lowestBranchNumberOfLeavesDefinition.getKey().getDomainKey());

		// nested branch definition was successful. let's get the other one and make sure they are
		// the same
		FieldDefinition<Object> highestBranchDefinition = dictionary.findDefinition(Tree.Key.HIGHEST_BRANCH);
		assertNotNull(highestBranchDefinition);
		assertNotSame(	"these are two separate fields of the same type, but not the same",
						lowestBranchDefinition,
						highestBranchDefinition);
		assertEquals("highest branch should have it's own custom dictionary", highestBranchDefinition.getKey()
				.getAbsoluteKey(), highestBranchDefinition.getNested().getDomainKey());
		assertNotSame(	"Dictionaries are singletons, but nested dictionaries get their own singleton",
						highestBranchDefinition.getNested(),
						lowestBranchDictionary);
	}

	@Test
	public void testColumnNameProvided() {
		FieldDictionary dictionary = factory.getDictionary(Animal.class);
		FieldDefinition<Object> definition = dictionary.getDefinition(COLUMN_NAME_KEY);
		assertEquals("Column name is not being defined", COLUMN_NAME_VALUE, definition.getColumnName());
		assertEquals(	"Column name not provided should return key by default",
						HAIR_COLOR_KEY,
						dictionary.getDefinition(HAIR_COLOR_KEY).getColumnName());
	}

	/**
	 * Given a dictionary with a nested dictionary this will find the definition of that nested
	 * dictionary using standard notation.
	 * 
	 * @throws FieldNotRegisteredException
	 * 
	 */
	@Test
	public void testGetNestedProperty() throws FieldNotRegisteredException {
		factory.getDictionary(Tree.class);
		FieldDictionary branchDictionary = factory.getFactory().getDictionary(Tree.Key.LOWEST_BRANCH.getAbsoluteKey());
		assertNotNull("getting a tree should produce the nested Branch", branchDictionary);
		// factory.getDictionary(Branch.class);
		FieldKey expectedKey = Tree.Key.LOWEST_BRANCH_NUMBER_OF_LEAVES;
		FieldDefinition<Integer> lowerTreeNumberOfLeavesDef = factory.getFactory().findDefinition(expectedKey);
		assertNotNull(lowerTreeNumberOfLeavesDef);
		assertEquals(expectedKey, lowerTreeNumberOfLeavesDef.getKey());
		FieldDefinition<Integer> globalDefinition = factory.getFactory().findDefinition(lowerTreeNumberOfLeavesDef
				.getKey().getAbsoluteKey());
		assertEquals("local definition does not match the global", lowerTreeNumberOfLeavesDef, globalDefinition);

	}

	/**
	 * Used for testing by providing a domain for annonymous classes.
	 * 
	 * @author aroller
	 * 
	 */
	@Dictionary(domain = "test")
	public interface TestDomain {

	}
}
