/**
 *
 */
package com.aawhere.field;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Brian Chapman
 * 
 */
public class RecordXmlAdapterUnitTest {

	private FieldKey key;
	private Record sampleRecord;

	@Before
	public void setUp() {
		this.key = Examples.MY_KEY;
		this.sampleRecord = Examples.MY_RECORD;
	}

	@Test
	public void testAdatper() throws Exception {
		RecordXmlAdapter adapter = new RecordXmlAdapter();
		XmlRecord xmlRecord = adapter.marshal(sampleRecord);
		assertNotNull(xmlRecord);
		assertTrue(xmlRecord.fields.contains(sampleRecord.getField(key)));

		// unmarshall
		Record unmarshalled = adapter.unmarshal(xmlRecord);
		assertEquals(sampleRecord, unmarshalled);
	}
}
