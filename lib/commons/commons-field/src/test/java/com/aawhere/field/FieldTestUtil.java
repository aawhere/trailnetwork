/**
 *
 */
package com.aawhere.field;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.MessageTestUtil;

/**
 * Helps produce useful test objects for {@link Field} and it's friends.
 * 
 * @author aroller
 * 
 */
public class FieldTestUtil {

	/** Static...please use */
	protected FieldTestUtil() {
	}

	public static List<FieldDefinition<?>> generateRandomFieldDefinitions(String domainKey) {
		ArrayList<FieldDefinition<?>> results = new ArrayList<FieldDefinition<?>>();
		int size = TestUtil.generateRandomInt(1, 10);
		for (int i = 0; i < size; i++) {
			results.add(generateRandomFieldDefinition(domainKey));
		}
		return results;

	}

	public static FieldDefinition<String> generateRandomFieldDefinition() {
		return generateRandomFieldDefinition(generateRandomKeyValue());
	}

	/**
	 *
	 */
	public static FieldDefinition<String> generateRandomFieldDefinition(String domainKey) {
		// TODO:mix it up with different types
		return new FieldDefinition.Builder<String>().setKey(generateRandomFieldKey(domainKey))
				.setName(MessageTestUtil.generateRandomMessage()).setType(String.class).build();
	}

	public static FieldKey generateRandomFieldKey(String domainKey) {
		return new FieldKey(domainKey, generateRandomKeyValue());
	}

	public static FieldKey generateRandomFieldKey() {
		return new FieldKey(generateRandomKeyValue(), generateRandomKeyValue());
	}

	/**
	 * @return
	 */
	public static String generateRandomKeyValue() {
		return RandomStringUtils.randomAlphanumeric(FieldUtils.MAX_LENGTH / 3).toLowerCase();
	}

	public static FieldDictionary generateDictionary(String domainKey) {
		return new FieldDictionary.Builder().setDomainKey(domainKey)
				.addFields(generateRandomFieldDefinitions(domainKey)).build();
	}

	/**
	 * @return
	 */
	public static FieldDictionary generateUniqueDictionary() {
		return generateDictionary(generateRandomKeyValue());
	}

	/** Provides an empty factory for testing purposes. */
	public static FieldDictionaryFactory getDictionaryFactory() {
		return new FieldDictionaryFactory();
	}

	/** provides a factory with one or more registered dictionaries */
	public static FieldDictionaryFactory generateRegisteredDictionary() {
		return getDictionaryFactory().register(generateUniqueDictionary());
	}

	/**
	 * @return
	 */
	public static FieldDictionary generateDictionary() {

		return generateDictionary(generateRandomKeyValue());
	}

	public static FieldAnnotationDictionaryFactory getFieldAnnotationDictionaryFactory() {
		return new FieldAnnotationDictionaryFactory(getDictionaryFactory());
	}

	static public void assertDictionary(FieldDictionary expected, FieldDictionary actual) {
		assertEquals(expected.getDomainKey(), actual.getDomainKey());
		assertEquals(expected.getDomain(), actual.getDomain());
		assertEquals(expected.getDescription(), actual.getDescription());
		assertNotNull(actual.getFields());
		TestUtil.assertCollectionEquals(expected.getFields(), actual.getFields());
	}

}