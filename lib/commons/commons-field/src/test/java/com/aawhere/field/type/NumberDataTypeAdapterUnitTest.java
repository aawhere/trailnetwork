/**
 * 
 */
package com.aawhere.field.type;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.lang.exception.BaseException;

/**
 * Tests for {@link NumberDataTypeAdapter}
 * 
 * @author Brian Chapman
 * 
 */
public class NumberDataTypeAdapterUnitTest {

	@Test
	public void testMarshallUnmarshall() throws BaseException {
		Integer expected = 111;
		NumberDataTypeAdapter adapter = new NumberDataTypeAdapter();
		FieldData marshalled = adapter.marshal(expected);
		assertNotNull(marshalled);
		assertEquals(expected, marshalled.getValue());
		Integer unmarshalled = adapter.unmarshal(marshalled, Integer.class);
		assertNotNull(unmarshalled);
		assertEquals(expected, unmarshalled);
	}

	/**
	 * The datastore stores all numbers as doubles. The adapter must handle converting doubles into
	 * integers.
	 * 
	 * @throws BaseException
	 * 
	 */
	@Test
	public void testDoubleAsInteger() throws BaseException {
		NumberDataTypeAdapter adapter = new NumberDataTypeAdapter();
		Double doubleValue = 5.0;
		Integer integerValue = doubleValue.intValue();
		FieldData marshal = adapter.marshal(doubleValue);
		Integer actual = adapter.unmarshal(marshal, Integer.class);
		assertEquals(integerValue, actual);
	}
}
