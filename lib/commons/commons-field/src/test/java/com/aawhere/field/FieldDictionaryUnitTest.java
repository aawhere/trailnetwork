/**
 * 
 */
package com.aawhere.field;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.xml.bind.JAXBTestUtil;

import com.google.common.collect.Lists;

/**
 * Tests the production and portability of {@link FieldDictionary} without concern for global
 * registration from {@link FieldDictionaryFactory}.
 * 
 * @see FieldDictionaryFactoryUnitTest T
 * @author aroller
 * 
 */
public class FieldDictionaryUnitTest {

	private FieldDictionary dictionary;
	private String key;
	private Message domain;
	private Message description;
	private Collection<FieldDefinition<?>> fields;
	private DictionaryIndex index;

	@Before
	public void setUp() {
		this.key = FieldTestUtil.generateRandomKeyValue();
		this.domain = new StringMessage(TestUtil.generateRandomString());
		this.description = new StringMessage(TestUtil.generateRandomString());
		this.fields = FieldTestUtil.generateRandomFieldDefinitions(key);
		this.index = new TestIndex();
		this.dictionary = new FieldDictionary.Builder().setDomainKey(key).setDomain(this.domain)
				.setDescription(description).addFields(fields).setIndexes(Lists.newArrayList(index)).build();
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testMutation() {
		this.dictionary.getFields().add(FieldTestUtil.generateRandomFieldDefinition(key));
		fail("ouch...the field list is mutable");
	}

	@Test
	public void testBasics() {
		assertDictionary(this.dictionary);
	}

	/**
	 * `
	 * 
	 */
	private void assertDictionary(FieldDictionary dictionary) {
		assertEquals(this.key, dictionary.getDomainKey());
		assertEquals(this.domain, dictionary.getDomain());
		assertEquals(this.description, dictionary.getDescription());
		assertNotNull(dictionary.getFields());
		assertNotNull(dictionary.getIndexes());
		TestUtil.assertCollectionEquals(this.fields, dictionary.getFields());
	}

	@Test
	@Ignore("get DictionaryIndex to be jaxb compatible with Jersey.  It works here, but not there?")
	public void testJaxb() {
		JAXBTestUtil<FieldDictionary> util = JAXBTestUtil.create(this.dictionary).build();
		assertDictionary(util.getActual());
	}

	@Test(expected = RuntimeException.class)
	public void testDictionaryWithNoFields() {
		new FieldDictionary.Builder().build();
	}

	public class TestIndex
			extends DictionaryIndex {

		public TestIndex() {
			super(FieldDictionaryUnitTest.this.fields.iterator().next().getKey());
		}
	}
}
