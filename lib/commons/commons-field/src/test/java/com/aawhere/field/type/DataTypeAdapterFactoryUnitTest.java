/**
 *
 */
package com.aawhere.field.type;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.adapter.Adapter;
import com.aawhere.field.message.MessageDataTypeTextAdapterUnitTest.Bike;
import com.aawhere.field.message.EnumDataTypeAtomAdapter;
import com.aawhere.field.message.MessageDataTypeTextAdapter;

/**
 * @author Brian Chapman
 * 
 */
public class DataTypeAdapterFactoryUnitTest {

	private DataTypeAdapterFactory factory;
	private final Bike ROAD = Bike.ROAD;
	private final Bike MOUNTAIN = Bike.MOUNTAIN;

	@Before
	public void setUp() {
		factory = new DataTypeAdapterFactory();
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.type.DataTypeAdapterFactory#handles(java.lang.reflect.Type, com.aawhere.field.type.FieldDataType)}
	 * .
	 */
	@Test
	public void testHandlesTypeFieldDataType() {
		assertTrue(factory.handles(ROAD.getClass(), FieldDataType.ATOM));
		assertTrue(factory.handles(ROAD.getClass(), FieldDataType.TEXT));
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.type.DataTypeAdapterFactory#handles(java.lang.reflect.Type, com.aawhere.field.type.FieldDataType, java.lang.Class)}
	 * .
	 */
	@Ignore
	@Test
	public void testHandlesTypeFieldDataTypeClassOfQ() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.type.DataTypeAdapterFactory#getAdapterOrNull(java.lang.reflect.Type, com.aawhere.field.type.FieldDataType, java.lang.Class)}
	 * .
	 */
	@Ignore
	@Test
	public void testGetAdapterOrNullTypeFieldDataTypeClassOfP() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.type.DataTypeAdapterFactory#getAdapterOrNull(java.lang.reflect.Type, com.aawhere.field.type.FieldDataType)}
	 * .
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testGetAdapterOrNullTypeFieldDataType() {
		Adapter enumAdapter = factory.getAdapterOrNull(ROAD.getClass(), FieldDataType.ATOM);
		Adapter textAdapter = factory.getAdapterOrNull(ROAD.getClass(), FieldDataType.TEXT);
		assertMessageAdapter(enumAdapter);
		assertTextAdapter(textAdapter);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.type.DataTypeAdapterFactory#getAdapter(java.lang.reflect.Type, com.aawhere.field.type.FieldDataType)}
	 * .
	 */
	@SuppressWarnings("rawtypes")
	@Test
	public void testGetAdapterTypeFieldDataType() {
		Adapter messageAdapter = factory.getAdapter(ROAD.getClass(), FieldDataType.ATOM);
		Adapter textAdapter = factory.getAdapter(ROAD.getClass(), FieldDataType.TEXT);
		assertMessageAdapter(messageAdapter);
		assertTextAdapter(textAdapter);
	}

	/**
	 * Test method for
	 * {@link com.aawhere.field.type.DataTypeAdapterFactory#getAdapter(java.lang.reflect.Type, com.aawhere.field.type.FieldDataType, java.lang.Class)}
	 * .
	 */
	@Ignore
	@Test
	public void testGetAdapterTypeFieldDataTypeClassOfQ() {
		fail("Not yet implemented");
	}

	private void assertMessageAdapter(@SuppressWarnings("rawtypes") Adapter adapter) {
		assertNotNull(adapter);
		assertTrue("Expected instance of " + EnumDataTypeAtomAdapter.class.getName() + " received "
				+ adapter.getClass().getName() + " instead.", adapter instanceof EnumDataTypeAtomAdapter);
	}

	private void assertTextAdapter(@SuppressWarnings("rawtypes") Adapter adapter) {
		assertNotNull(adapter);
		assertTrue(adapter instanceof MessageDataTypeTextAdapter);
	}

}
