/**
 * 
 */
package com.aawhere.field.message;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.Test;

import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldTestUtil;

/**
 * Tests formats used with Fields.
 * 
 * @author Aaron Roller
 * 
 * @see FieldDefinitionCustomFormat
 */
public class FieldCustomFormatUnitTest {

	@Test
	public void testFieldDefinition() {

		FieldDefinition<String> def = FieldTestUtil.generateRandomFieldDefinition();
		String expected = def.getName().toString();
		String actual = FieldDefinitionCustomFormat.getInstance().format(def, Locale.getDefault());
		assertEquals(expected, actual);

	}
}
