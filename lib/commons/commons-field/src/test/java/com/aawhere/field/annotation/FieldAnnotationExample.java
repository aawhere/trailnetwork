/**
 *
 */
package com.aawhere.field.annotation;

import java.util.Set;

import com.aawhere.field.ExampleReportMessage;
import com.aawhere.field.FieldKey;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.util.rb.Message;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Used to demonstrate how to use the {@link Field}.
 * 
 * @author Aaron Roller
 * 
 */

public class FieldAnnotationExample {
	public static final String TYPE_KEY = "order";
	public static final FieldKey TYPE_FIELD_KEY = new FieldKey(FieldAnnotationDictionaryUnitTest.DOMAIN, TYPE_KEY);
	public static final String HAIR_COLOR_KEY = "hairColor";
	public static final String HAIR_COLORS_KEY = "hairColors";
	public static final String COLUMN_NAME_KEY = "different";
	public static final String COLUMN_NAME_VALUE = "columnNameProvided";
	public static final FieldKey HAIR_COLOR_FIELD_KEY = new FieldKey(FieldAnnotationDictionaryUnitTest.DOMAIN,
			HAIR_COLOR_KEY);
	public static final FieldKey HAIR_COLORS_FIELD_KEY = new FieldKey(FieldAnnotationDictionaryUnitTest.DOMAIN,
			HAIR_COLORS_KEY);
	public static final String LEGS_KEY = "numberOfLegs";
	public static final String SPECIES_DOMAIN_KEY = "species";

	public static enum Species {
		FELIS_CATUS, CANIS_LUPUS, HOMO_SAPIEN
	};

	public static final String MESSAGE_KEY = "message";
	public static final FieldKey MESSAGE_FIELD_KEY = new FieldKey(FieldAnnotationDictionaryUnitTest.DOMAIN, MESSAGE_KEY);
	public static final String CAT_TYPE = "cat";
	public static final String DOG_TYPE = "dog";
	public static final String HUMAN_TYPE = "human";

	public static final Animal CAT = new Animal(CAT_TYPE, 4, FieldAnnotationExample.Species.FELIS_CATUS);
	public static final Animal DOG = new Animal(DOG_TYPE, 4, FieldAnnotationExample.Species.CANIS_LUPUS);
	public static final Animal HUMAN = new Animal(HUMAN_TYPE, 2, FieldAnnotationExample.Species.HOMO_SAPIEN);
	public static final Animal LEG_COUNT_NULL = new Animal(CAT_TYPE, null, FieldAnnotationExample.Species.FELIS_CATUS);
	public static final Animal TYPE_NOT_NULL = new Animal(DOG_TYPE, null, null);

	/**
	 * Super interface used to declare fields and domains and test heirachical annotations
	 * processing.
	 * 
	 * @author Aaron Roller
	 * 
	 */
	@Dictionary(domain = FieldAnnotationDictionaryUnitTest.DOMAIN, messages = ExampleReportMessage.class)
	public interface LivingThing
			extends AnnotatedDictionary {
		@Field
		Species getSpecies();
	}

	/**
	 * A concrete implementation inheriting from {@link LivingThing} and declaring it's own fields
	 * too.
	 * 
	 * @author Aaron Roller
	 * 
	 */
	public static class Animal
			implements LivingThing {

		public static final String COLUMN_NAME_PROPERTY_DESCRIPTON = "Alternative way to document.";
		private String type;

		private Integer numberOfLegs;
		private Species species;

		@Field(key = MESSAGE_KEY)
		private Message message;
		@Field(key = HAIR_COLOR_KEY, dataType = FieldDataType.ATOM)
		private HairColor hairColor;
		@Field(key = HAIR_COLORS_KEY, parameterType = HairColor.class, dataType = FieldDataType.ATOM)
		private Set<HairColor> hairColors;
		/**
		 * Demonstrates the ability to provide a column name allowing a different key than the field
		 */
		@Field(key = COLUMN_NAME_KEY)
		@ApiModelProperty(value = COLUMN_NAME_PROPERTY_DESCRIPTON)
		private String columnNameProvided;

		public static enum HairColor {
			BROWN, BLACK, WHITE
		}

		/**
		 * @param type
		 * @param numberOfLegs
		 */
		public Animal(String type, Integer numberOfLegs, Species species) {
			super();
			this.type = type;
			this.numberOfLegs = numberOfLegs;
			this.species = species;
			Class<? extends Enum<? extends Message>> c = ExampleReportMessage.class;
		}

		/**
		 * @return the numberOfLegs
		 */
		@Field(dataType = FieldDataType.NUMBER)
		public Integer getNumberOfLegs() {
			return this.numberOfLegs;
		}

		/**
		 * @return the type
		 */
		@Field(key = TYPE_KEY)
		public String getType() {
			return this.type;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.field.annotation.AnimalAnnotated.LivingThing#getSpecies()
		 */
		@Override
		public Species getSpecies() {
			return this.species;
		}
	}

	@Dictionary(domain = Plant.PLANT_DOMAIN)
	public interface Plant
			extends AnnotatedDictionary {

		/**
		 *
		 */
		public static final String PLANT_DOMAIN = "plant";
		public static final String LEAF_TYPE_KEY = "leafType";

		// FIXME: We shold be able to declare fields on methods of interfaces
		// @Field(key = LEAF_TYPE_KEY)
		public String getLeafType();
	}

	@Dictionary(domain = Tree.DOMAIN)
	public static class Tree
			implements Plant {

		public static final String LOWEST_BRANCH = "lowestBranch";
		private static final String HIGHEST_BRANCH = "highestBranch";
		public static final String DOMAIN = "tree";
		private static final String FLATTENED_BRANCH = "flattended";

		public static class Key {
			public static final FieldKey LOWEST_BRANCH = new FieldKey(DOMAIN, Tree.LOWEST_BRANCH);
			public static final FieldKey FLATTENED_BRANCH = new FieldKey(DOMAIN, Tree.FLATTENED_BRANCH);
			public static final FieldKey HIGHEST_BRANCH = new FieldKey(DOMAIN, Tree.HIGHEST_BRANCH);
			public static final FieldKey LOWEST_BRANCH_NUMBER_OF_LEAVES = new FieldKey(DOMAIN, Tree.LOWEST_BRANCH,
					Branch.Key.NUMBER_OF_LEAVES);
		}

		/**
		 * Used to construct all instances of FieldAnnotationExample.Tree.
		 */
		@DictionaryMutator(domain = PLANT_DOMAIN)
		public static class Builder
				extends ObjectBuilder<Tree>
				implements MutableAnnotatedDictionary {

			public Builder() {
				super(new Tree());
			}

			public Builder(Tree original) {
				super(original);
			}

			@FieldMutator(key = LEAF_TYPE_KEY)
			public Builder setLeafType(String type) {
				building.leafType = type;
				return this;
			}

		}// end Builder

		@Field(key = LEAF_TYPE_KEY)
		private String leafType;

		@Field(key = LOWEST_BRANCH)
		private Branch lowestBranch;

		@Field(key = HIGHEST_BRANCH)
		private Branch highestBranch;

		/** Use {@link Builder} to construct FieldAnnotationExample.Tree */
		private Tree() {

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.field.annotation.FieldAnnotationExample.Plant#getLeafType()
		 */
		@Override
		public String getLeafType() {
			return this.leafType;
		}
	}

	@Dictionary(domain = LoneTree.DOMAIN)
	public static class LoneTree {
		/**
		 * Demonstrates the ability to flatten the keys for a nested dictionary.
		 * loneTree-numberOfLeaves instead of loneTree-flattenedBranch-numberOfLeaves
		 */
		@Field(key = FLATTENED_BRANCH)
		@EmbeddedDictionary(flatten = true)
		private Branch flattenedBranched;

		public static final String DOMAIN = "loneTree";
		private static final String FLATTENED_BRANCH = "flattended";

		public static class Key {
			public static final FieldKey FLATTENED_BRANCH = new FieldKey(DOMAIN, Tree.FLATTENED_BRANCH);
			// notice this is a normal key, not an embedded key
			public static final FieldKey FLATTENED_NUMBER_OF_LEAVES = new FieldKey(DOMAIN, Branch.NUMBER_OF_LEAVES);
		}
	}

	/**
	 * Demonstrates the ability to embed an object into another multiple times which results in the
	 * keys from the embedded class being available when combined with the key on the field that is
	 * embedding it.
	 * 
	 * Consider if this branch is embedded in the tree with a field key of "lowestBranch" then the
	 * key for the numberOfLeaves would be:
	 * 
	 * tree-lowestBranch-numberOfLeaves
	 * 
	 * @author aroller
	 * 
	 */
	@Dictionary(domain = Branch.DOMAIN)
	public static class Branch {
		public static final String DOMAIN = "branch";
		public static final String NUMBER_OF_LEAVES = "numberOfLeaves";

		public static class Key {
			public static final FieldKey NUMBER_OF_LEAVES = new FieldKey(DOMAIN, Branch.NUMBER_OF_LEAVES);
		}

		@Field(key = NUMBER_OF_LEAVES)
		private Integer numberOfLeaves;
	}

	public static class DictionaryNoAnnotation {

	}

	@Dictionary(domain = "noFields")
	public class DictionaryNoFields
			implements AnnotatedDictionary {

	}

	@Dictionary(domain = FirstSecond.FIRST_SECOND)
	public interface FirstSecond
			extends AnnotatedDictionary {
		/**
		 *
		 */
		public static final String FIRST_SECOND = "firstSecond";

		@Field
		public String getFirstSecond();
	}
}
