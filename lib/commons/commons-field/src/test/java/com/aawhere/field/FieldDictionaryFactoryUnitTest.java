/**
 * 
 */
package com.aawhere.field;

import static com.aawhere.field.FieldTestUtil.generateDictionary;
import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * Tests {@link Field}, {@link FieldDefinition}, {@link FieldKey} and FieldFactory.
 * 
 * @author roller
 * 
 */
public class FieldDictionaryFactoryUnitTest {

	private FieldDictionaryFactory factory;

	@Before
	public void setUp() {
		factory = FieldTestUtil.getDictionaryFactory();
	}

	/**
	 * Tests successful registration in addition to duplicate registrations of the exact same which
	 * is allowed, but fails when a dictionary of the same key, but different fields is registered
	 */
	@Test
	public void testDuplicateFieldRegistration() throws DuplicateFieldRegistrationException,
			InvalidFieldKeyLengthException, InvalidFieldKeyFormatException {
		FieldDictionary dictionary = FieldTestUtil.generateUniqueDictionary();
		FieldDictionary duplicate = FieldTestUtil.generateDictionary(dictionary.getDomainKey());
		factory.register(dictionary);
		assertEquals("first one should be easy", 1, factory.getAllDictionaries().getAll().size());
		factory.register(dictionary);
		assertEquals("the same factory was already registered so there should be only 1", 1, factory
				.getAllDictionaries().getAll().size());
		try {
			factory.register(duplicate);
			fail("the factory shouldn't allow duplicate registration with different fields");
		} catch (Exception e) {
			// this is good.
		}
	}

	@Test(expected = DictionaryNotRegisteredException.class)
	public void testFactoryNotCreated() {
		String keyValue = FieldTestUtil.generateRandomKeyValue();
		assertFalse(factory.instanceExists(keyValue));
		factory.getDictionary(keyValue);
	}

	@Test
	public void testAll() {
		factory.register(generateDictionary());
		Set<FieldDictionary> allDictionaries = factory.getAllDictionaries().getAll();
		TestUtil.assertGreaterThan(0, allDictionaries.size());
	}

}
