/**
 *
 */
package com.aawhere.field;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationExample;
import com.aawhere.field.annotation.FieldAnnotationExample.Tree;

/**
 * @author Brian Chapman
 * 
 */
public class AnnotatedReportBuilderUnitTest {

	public final String LEAF_TYPE_SIMPLE = "simple";
	public final String LEAF_TYPE_COMPOUND = "compound";
	FieldAnnotationDictionaryFactory fieldFactory;

	@Before
	public void setUp() {
		fieldFactory = FieldTestUtil.getFieldAnnotationDictionaryFactory();
	}

	@Test
	public void testFieldReportBuilder() {
		Tree tree = new FieldAnnotationExample.Tree.Builder().setLeafType(LEAF_TYPE_SIMPLE).build();
		Report report = new AnnotatedReportBuilder(fieldFactory).addObject(tree).build();
		assertNotNull(report);
		assertTrue(!report.getRecords().isEmpty());
	}

	@Test
	public void testFieldReportBuilderMultipleEntities() {
		Tree tree1 = new FieldAnnotationExample.Tree.Builder().setLeafType(LEAF_TYPE_SIMPLE).build();
		Tree tree2 = new FieldAnnotationExample.Tree.Builder().setLeafType(LEAF_TYPE_COMPOUND).build();
		Report report = new AnnotatedReportBuilder(fieldFactory).addObject(tree1).addObject(tree2).build();
		assertNotNull(report);
		assertTrue(!report.getRecords().isEmpty());
		assertEquals(2, report.getRecords().size());
	}
}
