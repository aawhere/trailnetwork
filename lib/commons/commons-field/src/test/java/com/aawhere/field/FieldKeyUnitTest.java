/**
 * 
 */
package com.aawhere.field;

import static com.aawhere.field.FieldTestUtil.*;
import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class FieldKeyUnitTest {

	String domainKey = "domain";
	/** */
	public static final String VALID_NUMERIC = "112233";
	public static final String VALID_ALPHA = "aaBb";
	public static final String VALID_ALPHANUMERIC = "aaBb11";
	public static final String VALID_FULL = VALID_ALPHA + FieldUtils.KEY_DILIM + VALID_NUMERIC;
	public static final String INVALID_ALPHANUMERIC_CAPS = "Aa-Bb-11";
	public static final String INVALID_CHARACTERS = "$a%b?c";
	public static final String INVALID_WITH_SPACE = "aa bb";

	@Test
	public void testConstructionAndValue() {
		String keyValue = VALID_ALPHANUMERIC;
		FieldKey key = new FieldKey(domainKey, keyValue);
		assertEquals(domainKey, key.getDomainKey());
		assertEquals(keyValue, key.getRelativeKey());
		TestUtil.assertContains(key.getAbsoluteKey(), keyValue);
		TestUtil.assertContains(key.getAbsoluteKey(), domainKey);
		TestUtil.assertContains(key.getAbsoluteKey(), FieldUtils.KEY_DILIM.toString());
		FieldKey fromAbsolute = new FieldKey(key.getAbsoluteKey());
		assertEquals(domainKey, fromAbsolute.getDomainKey());
		assertEquals(keyValue, fromAbsolute.getRelativeKey());

	}

	/**
	 * Allows for multiple keys to be provided to reference fields on embedded objects. This
	 * demonstrates how keys are chained to represent nested objects.
	 */
	@Test
	public void testNestedKey() {
		FieldKey bottomKey = new FieldKey(domainKey, "bottom");
		FieldKey middleKey = new FieldKey(domainKey, "middle", bottomKey);
		FieldKey topKey = new FieldKey(domainKey, "top", middleKey);
		TestUtil.assertContains(topKey.getAbsoluteKey(), middleKey.getRelativeKey());
		TestUtil.assertContains(topKey.getAbsoluteKey(), bottomKey.getRelativeKey());
		FieldKey middleKeyAnotherWay = new FieldKey(middleKey, bottomKey);
		assertEquals(middleKey, middleKeyAnotherWay);
		assertEquals(middleKey.getNested(), middleKeyAnotherWay.getNested());

	}

	@Test
	public void testFieldKeyValidation() throws InvalidFieldKeyFormatException {
		// good keys
		assertFieldKeyValidation(VALID_NUMERIC, 0);
		assertFieldKeyValidation(VALID_ALPHA, 0);
		assertFieldKeyValidation(VALID_ALPHANUMERIC, 0);
		assertNotNull(new FieldKey(VALID_FULL));

	}

	@Test(expected = InvalidFieldKeyFormatException.class)
	public void testKeyMissingDomain() {
		new FieldKey("bogus");
	}

	@Test(expected = InvalidFieldKeyFormatException.class)
	public void testFieldKeyInvalidCharacters() {
		assertFieldKeyValidation(INVALID_CHARACTERS, 3);
	}

	@Test(expected = InvalidFieldKeyFormatException.class)
	public void testFieldKeyUpperCase() {
		assertFieldKeyValidation(INVALID_ALPHANUMERIC_CAPS, 2);

	}

	@Test(expected = InvalidFieldKeyFormatException.class)
	public void testFieldKeyWithSpace() {
		assertFieldKeyValidation(INVALID_WITH_SPACE, 1);
	}

	@Test(expected = InvalidFieldKeyFormatException.class)
	@Ignore("Ignoring because nested dictionary domains require the hyphen")
	public void testFactoryInvalidKey() throws InvalidFieldKeyFormatException, InvalidFieldKeyLengthException {

		generateDictionary(INVALID_WITH_SPACE);
	}

	/**
	 * @param key
	 * @param expectedCount
	 * @throws InvalidFieldKeyFormatException
	 */
	private void assertFieldKeyValidation(String key, int expectedCount) throws InvalidFieldKeyFormatException {
		FieldUtils.validate(key);
	}
}
