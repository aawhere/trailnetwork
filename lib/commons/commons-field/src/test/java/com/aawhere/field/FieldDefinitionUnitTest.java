/**
 *
 */
package com.aawhere.field;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.field.type.FieldDataType;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.MessageTestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author aroller
 * 
 */
public class FieldDefinitionUnitTest {

	private FieldDefinition<String> def;
	private Message description;
	private Message name;
	private FieldKey fieldKey;
	private Class<String> type;
	private FieldDataType dataType;
	private FieldDictionary nested;
	private Boolean searchable = true;

	@Before
	public void setUp() {
		fieldKey = FieldTestUtil.generateRandomFieldKey();
		description = MessageTestUtil.generateRandomMessage();
		name = MessageTestUtil.generateRandomMessage();
		type = String.class;
		dataType = FieldDataType.ATOM;
		nested = FieldTestUtil.generateDictionary();
		def = new FieldDefinition.Builder<String>().setKey(fieldKey).setDescription(description).setName(name)
				.setType(type).setDataType(dataType).setSearchable(searchable).setNestedDictionary(nested).build();
	}

	@Test
	public void testBasics() {
		assertDefinition(def);
	}

	public void assertDefinition(FieldDefinition<String> def) {
		assertEquals(this.fieldKey, def.getKey());
		assertEquals(this.name, def.getName());
		assertEquals(this.description, def.getDescription());
		assertEquals(this.type, def.getType());
		assertEquals(this.searchable, def.getSearchable());
		FieldTestUtil.assertDictionary(nested, def.getNested());
		// FIXME:for some reason JAXB isn't unmarshalling this enum
		if (def.getDataType() != null) {
			assertEquals(this.dataType, def.getDataType());
		}
	}

	@Test
	public void testJaxb() {
		JAXBTestUtil<FieldDefinition<String>> util = JAXBTestUtil.create(def).build();
		assertDefinition(util.getActual());
	}
}
