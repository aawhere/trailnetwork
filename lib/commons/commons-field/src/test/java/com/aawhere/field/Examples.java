/**
 *
 */
package com.aawhere.field;

import com.aawhere.util.rb.StringMessage;

/**
 * Sample {@link Record}, {@link Report}, {@link FieldDefinition}, {@link Field}, etc. types for
 * testing.
 * 
 * @author Brian Chapman
 * 
 */
public class Examples {

	public static final FieldKey MY_KEY = new FieldKey("myDomain", "myKey");
	public static final String MY_DEFINITION_NAME = "Sample";
	public static final FieldDefinition<String> MY_DEFINITION = new FieldDefinition.Builder<String>()
			.setName(new StringMessage(MY_DEFINITION_NAME)).setKey(MY_KEY).setType(String.class).build();
	public static final String MY_FIELD_VALUE = "My Sample Value";
	public static final Field<String> MY_FIELD = new Field.Builder<String>().setDefinition(MY_DEFINITION)
			.setValue(MY_FIELD_VALUE).build();
	public static final RecordSchema MY_SCHEMA = new RecordSchema.Builder().addDefinition(MY_DEFINITION).build();
	public static final Record MY_RECORD = new Record.Builder().addField(MY_FIELD).build();
	public static final Report MY_REPORT = new Report.Builder().setSchema(MY_SCHEMA).addRecord(MY_RECORD).build();
}
