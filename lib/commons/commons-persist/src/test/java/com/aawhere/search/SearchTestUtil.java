/**
 *
 */
package com.aawhere.search;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import com.aawhere.field.FieldKey;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifier;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.LongMockEntity;
import com.aawhere.persist.LongMockEntityId;
import com.aawhere.persist.LongMockEntityWithParent;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Lists;

/**
 * Just some quick shortcuts for testing with Mocked entities and search results.
 * 
 * @author Brian Chapman
 * 
 */
public class SearchTestUtil {

	public static final String MOCK_SEARCH_DOC_DOMAIN = LongMockEntity.FIELD.DOMAIN_KEY;
	/** Demonstrates handling of Domains that do not declare searchable. */
	public static final String MOCK_SEARCH_DOC_DOMAIN_WITHOUT_FIELD_DECLARATOIN = LongMockEntityWithParent.DOMAIN_KEY;
	public static final String MOCK_SEARCH_ID_KEY = "id";
	public static final FieldKey MOCK_SEARCH_FIELD_NAME1 = new FieldKey(MOCK_SEARCH_DOC_DOMAIN,
			LongMockEntity.FIELD.NAME_KEY);
	public static final FieldKey MOCK_SEARCH_FIELD_NAME2 = new FieldKey(MOCK_SEARCH_DOC_DOMAIN,
			LongMockEntity.FIELD.COUNT);
	public static final SearchDocument.Field MOCK_SEARCH_FIELD1 = new SearchDocument.Field.Builder()
			.key(MOCK_SEARCH_FIELD_NAME1.getAbsoluteKey()).dataType(FieldDataType.TEXT)
			.value(MOCK_SEARCH_FIELD_NAME1.getAbsoluteKey()).build();
	public static final SearchDocument.Field MOCK_SEARCH_FIELD2 = new SearchDocument.Field.Builder()
			.key(MOCK_SEARCH_FIELD_NAME2.getAbsoluteKey()).dataType(FieldDataType.ATOM)
			.value(TestUtil.generateRandomAlphaNumeric()).build();
	public static final SearchDocument.Field MOCK_SEARCH_FIELD3 = new SearchDocument.Field.Builder()
			.key(MOCK_SEARCH_FIELD_NAME2.getAbsoluteKey()).dataType(FieldDataType.ATOM)
			.value(TestUtil.generateRandomAlphaNumeric()).build();

	public static final String NAME_VALUE = "myName";
	public static final String SEARCH_DOCUMENT_TYPE1 = "searchDocumentType1";
	public static final FieldKey NAME_KEY = new FieldKey(MOCK_SEARCH_DOC_DOMAIN, LongMockEntity.FIELD.NAME_KEY);

	public static SearchDocument getSampleSearchDocument(SearchDocument.Field field, String domain) {
		return getSampleSearchDocument(	Lists.newArrayList(field),
										LongMockEntity.FIELD.DOMAIN_KEY,
										new LongMockEntityId(TestUtil.generateRandomId()));
	}

	public static SearchDocument getSampleSearchDocument(List<SearchDocument.Field> fields, String domain) {
		return getSampleSearchDocument(fields, domain, new LongMockEntityId(TestUtil.generateRandomId()));
	}

	/**
	 * Provides the basics with no fields assigned, but all can be overwritten.
	 * 
	 * @return
	 */
	public static SearchDocument.Builder searchDocumentBuilder() {
		return SearchDocument.create().index(MOCK_SEARCH_DOC_DOMAIN).rank(TestUtil.generateRandomInt())
				.id(TestUtil.generateRandomAlphaNumeric());
	}

	public static SearchDocument getSampleSearchDocument(List<SearchDocument.Field> fields, String domain,
			Identifier<?, ?> id) {
		LongMockEntity.MockEmbeded embedded = new LongMockEntity.MockEmbeded();
		embedded.property = TestUtil.generateRandomInt();
		SearchDocument document = new SearchDocument.Builder()//
				.domain(domain)//
				.id(id)//
				.addFields(fields)//
				.rank(TestUtil.generateRandomInt()).build();
		return document;
	}

	public static SearchDocument.Field generateRandomField() {
		return new SearchDocument.Field.Builder().key(TestUtil.generateRandomAlphaNumeric())
				.value(TestUtil.generateRandomDouble()).build();
	}

	public static Filter createFilter(FilterCondition<?>... conditions) {
		Integer limit = 10;
		Filter.Builder filterBuilder = new Filter.Builder().setLimit(limit);
		for (FilterCondition<?> condition : conditions) {
			filterBuilder.addCondition(condition);
		}
		return filterBuilder.build();
	}

	public static FilterCondition<LongMockEntityId> createSearchDocumentIdFilterCondition(Long value) {
		FieldKey fieldKey = new FieldKey(MOCK_SEARCH_DOC_DOMAIN, BaseEntity.BASE_FIELD.ID);
		return new FilterCondition.Builder<LongMockEntityId>().field(fieldKey).operator(FilterOperator.EQUALS)
				.value(new LongMockEntityId(value)).build();
	}

	public static void assertContains(Set<SearchDocument.Field> fields, Object value) {
		Boolean match = false;
		for (SearchDocument.Field field : fields) {
			if (field.value().equals(value)) {
				match = true;
			}
		}
		assertTrue(match);
	}

}
