/**
 *
 */
package com.aawhere.persist;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.ReflectionUtils;

import com.aawhere.collections.Index;
import com.aawhere.id.Identifier;
import com.aawhere.id.KeyPredicate;
import com.aawhere.id.KeyedEntity;
import com.aawhere.lang.reflect.GenericUtils;

import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * An in-memory implementation of a Repository for unit testing purposes.
 * 
 * @author Aaron Roller
 * 
 */
public class MockRepository<EntityT extends BaseEntity<EntityT, IdentifierT>, IdentifierT extends Identifier<?, EntityT>>
		extends BaseRepository<EntityT, IdentifierT>
		implements MutableRepository<EntityT, IdentifierT> {

	private HashMap<IdentifierT, EntityT> entities = new HashMap<IdentifierT, EntityT>();
	private Class<EntityT> classRepresented;
	private Class<IdentifierT> identifierType;

	/**
	 *
	 */
	@SuppressWarnings("unchecked")
	public MockRepository() {
		List<Class<?>> genericClasses = GenericUtils.getTypeArguments(MockRepository.class, getClass());
		classRepresented = (Class<EntityT>) genericClasses.get(Index.FIRST.index);
		identifierType = (Class<IdentifierT>) genericClasses.get(Index.SECOND.index);
	}

	@SuppressWarnings("unchecked")
	public MockRepository(Class<EntityT> classRepresented) {
		this.classRepresented = classRepresented;
		List<Class<?>> genericClasses = GenericUtils.getTypeArguments(MockRepository.class, getClass());
		identifierType = (Class<IdentifierT>) genericClasses.get(Index.SECOND.index);
	}

	public MockRepository(Class<EntityT> classRepresented, Class<IdentifierT> identifierType) {
		this.classRepresented = classRepresented;
		this.identifierType = identifierType;
	}

	/**
	 * @return the entities
	 */
	protected HashMap<IdentifierT, EntityT> getEntities() {
		return this.entities;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#store(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public EntityT store(EntityT entity) {
		IdentifierT id = entity.getId();
		boolean selfIdentifying = entity instanceof SelfIdentifyingEntity;
		if (id != null && !selfIdentifying) {
			throw new IllegalArgumentException(
					id
							+ "Cannot store entities that already have an ID. Use update methods instead or implement SelfIdentifyingEntity.");
		} else {
			try {
				Field field = ReflectionUtils.findField(entity.getClass(), "id");
				if (!selfIdentifying) {
					id = generateRandomId(entity);
				}
				field.setAccessible(true);
				field.set(entity, id.getValue());
				entities.put(id, entity);
				entity.prePersist();
				return entity;
			} catch (SecurityException e) {
				throw new RuntimeException(e);
			} catch (IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Override if you need to do something different. For example, your class supplies its own id.
	 * 
	 * @return
	 */
	protected IdentifierT generateRandomId(EntityT entity) {
		return EntityTestUtil.generateRandomId(identifierType);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#load(com.aawhere.id.l.Id)
	 */
	@Override
	public EntityT load(IdentifierT id) throws EntityNotFoundException {
		final EntityT e = this.entities.get(id);
		if (e == null) {
			throw new EntityNotFoundException(id);
		}
		e.postLoad();
		return e;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#loadAll(java.util.Collection)
	 */
	@Override
	public Map<IdentifierT, EntityT> loadAll(Iterable<IdentifierT> ids) {
		return Maps.filterKeys(this.entities, Predicates.in(Sets.newHashSet(ids)));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.ApplicationRepository#findByKey(com.aawhere.app.ApplicationKey)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <KE extends KeyedEntity<K>, K extends Identifier<?, KE>> KE findEntityByKey(K key)
			throws EntityNotFoundException {

		final Collection entities = this.entities.values();
		final Collection<EntityT> filtered = Collections2.filter(entities, new KeyPredicate(key));
		if (filtered.isEmpty()) {
			throw new EntityNotFoundException(key);
		}
		if (filtered.size() > 1) {
			throw new IllegalStateException("multiple found for the same key " + key + " " + entities);
		}
		return (KE) filtered.iterator().next();
	}

	protected HashMap<IdentifierT, EntityT> getAllEntities() {
		return entities;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#delete(com.aawhere.id.l.Id)
	 */
	@Override
	public void delete(IdentifierT entityId) {
		this.entities.remove(entityId);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#getEntityClassRepresented()
	 */
	@Override
	public Class<EntityT> getEntityClassRepresented() {
		return this.classRepresented;
	}

	/**
	 * Update is provided only to specific implementations that should have used the load, then
	 * mutate the Entity using the mutating builder, then called this update method.
	 * 
	 * @param entity
	 * @return
	 * @throws EntityNotFoundException
	 *             if the entity has been deleted. Client may recover by calling create again.
	 */
	// the ObjectifyRepository doesn't throw an EntityNotFoundException. These should be
	// consistent.
	@Override
	public EntityT update(EntityT entity) {
		final IdentifierT id = entity.getId();
		entity.prePersist();
		this.entities.put(id, entity);
		return entity;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#storeAll(java.lang.Iterable)
	 */
	@Override
	public Map<IdentifierT, EntityT> storeAll(Iterable<EntityT> entities) {
		Map<IdentifierT, EntityT> map = new HashMap<IdentifierT, EntityT>();
		for (EntityT entity : entities) {
			store(entity);
			map.put(entity.getId(), entity);
		}
		return map;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#exists(com.aawhere.id.Identifier)
	 */
	@Override
	public Boolean exists(IdentifierT id) {
		try {
			return load(id) != null;
		} catch (EntityNotFoundException e) {
			return false;
		}
	}

	/*
	 * @see com.aawhere.persist.Repository#persist(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void persist(EntityT entity) {
		store(entity);
	}

}
