/**
 * 
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringAdapterFactory;
import com.aawhere.lang.string.StringAdapterUnitTest;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.test.TestUtil;

/**
 * test for {@link FilterSort}.
 * 
 * @author aroller
 * 
 */
public class FilterSortUnitTest {
	private String field;
	private Direction direction;
	private FilterSort sort;

	@Before
	public void setUp() {
		this.field = TestUtil.generateRandomAlphaNumeric();
		this.direction = Direction.DESCENDING;
		this.sort = new FilterSort.Builder().setField(this.field).setDirection(this.direction).build();
	}

	@Test
	public void testFieldAndDirection() {

		FilterSort sort = this.sort;
		assertFilterSort(sort);
	}

	/**
	 * @param sort
	 */
	private void assertFilterSort(FilterSort sort) {
		assertEquals(this.direction, sort.getDirection());
		assertEquals(this.field, sort.getField());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullField() {
		FilterSort.create().setField(null).build();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullDirection() {
		FilterSort.create().setField(this.field).setDirection(null).build();
	}

	@Test
	public void testDefaultDirection() {
		FilterSort filterSort = FilterSort.create().setField(this.field).build();
		assertEquals(FilterSort.DEFAULT_DIRECTION, filterSort.getDirection());
	}

	/** Does a round trip producing and consuming strings. */
	@Test
	public void testFromStringFieldAndDirection() {
		String string = sort.toString();
		assertNotNull(string);
		FilterSort fromString = FilterSort.valueOf(string);
		assertFilterSort(fromString);
		assertEquals(string, fromString.toString());
	}

	@Test
	public void testFromStringField() {
		FilterSort filterSort = FilterSort.valueOf(field);
		assertEquals(this.field, filterSort.getField());
		assertEquals(FilterSort.DEFAULT_DIRECTION, filterSort.getDirection());
	}

	@Test
	public void testStringAdapter() throws BaseException {
		StringAdapterFactory factory = new StringAdapterFactory();
		factory.registerStandardAdapters();
		StringAdapter<Object> adapter = factory.getAdapter(FilterSort.class);
		assertNotNull(adapter);
		StringAdapterUnitTest.assertAdapter(factory, this.sort);
	}

	@Test
	public void testDescendingPrefix() {
		Direction direction = Direction.ASCENDING;
		assertPrefix(direction);
	}

	@Test
	public void testAscendingPrefix() {
		assertPrefix(Direction.ASCENDING);
	}

	@Test
	public void testFromStringNone() {
		FilterSort filterSort = FilterSort.valueOf(FilterSort.NONE_FIELD);
		assertSame(FilterSort.NONE, filterSort);
	}

	/**
	 * @param direction
	 */
	public void assertPrefix(Direction direction) {
		String field = TestUtil.generateRandomAlphaNumeric();
		String query = FilterSort.prefixString(field, direction);
		FilterSort sort = FilterSort.valueOf(query);
		assertEquals(field, sort.getField());
		assertEquals(direction, sort.getDirection());
	}
}
