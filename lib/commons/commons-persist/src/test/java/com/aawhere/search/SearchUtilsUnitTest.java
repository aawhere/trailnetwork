package com.aawhere.search;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Test;

import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.lang.enums.EnumUtilUnitTest;
import com.aawhere.measure.geocell.GeoCellTestUtil;
import com.aawhere.persist.Filter;
import com.aawhere.search.SearchDocument.Field;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.ExampleMessage;
import com.aawhere.util.rb.Message;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

public class SearchUtilsUnitTest {

	@Test
	public void testDataTypeEnum() {
		assertEquals(FieldDataType.ATOM, SearchUtils.dataTypeFor(EnumUtilUnitTest.Junk.A.getClass()));
		assertEquals(FieldDataType.ATOM, SearchUtils.dataTypeFor(EnumUtilUnitTest.Junk.class));
		assertEquals(	"message enumerations are treated as atom rather than text by default",
						FieldDataType.ATOM,
						SearchUtils.dataTypeFor(ExampleMessage.MESSAGE_IN_FILE.getClass()));
	}

	@Test
	public void testDataTypeNumber() {
		assertEquals(FieldDataType.NUMBER, SearchUtils.dataTypeFor(Integer.class));
	}

	@Test
	public void testIdentifier() {
		assertEquals(FieldDataType.ATOM, SearchUtils.dataTypeFor(ChildId.class));
	}

	@Test
	public void testDataTypeText() {
		assertEquals(FieldDataType.TEXT, SearchUtils.dataTypeFor(String.class));
		assertEquals(FieldDataType.TEXT, SearchUtils.dataTypeFor(Message.class));
	}

	@Test
	public void testFieldValue() {
		Field enumField = SearchDocument.Field.create().key(TestUtil.generateRandomAlphaNumeric())
				.value(EnumUtilUnitTest.Junk.A).build();
		Field doubleField = SearchDocument.Field.create().key(TestUtil.generateRandomAlphaNumeric()).value(5.0).build();
		Field integerField = SearchDocument.Field.create().key(TestUtil.generateRandomAlphaNumeric())
				.value(((Double) doubleField.value()).intValue()).build();
		HashSet<Field> fields = Sets
				.newHashSet(SearchTestUtil.MOCK_SEARCH_FIELD1, enumField, integerField, doubleField);
		Multimap<String, Field> fieldMap = SearchUtils.fieldMap(fields);
		Object fieldValue = SearchUtils.fieldValue(	SearchTestUtil.MOCK_SEARCH_FIELD1.key(),
													SearchTestUtil.MOCK_SEARCH_FIELD1.value().getClass(),
													fieldMap);
		assertEquals("field 1 value incorrect", SearchTestUtil.MOCK_SEARCH_FIELD1.value(), fieldValue);

		assertNull(	"key shouldn't be found " + fieldMap,
					SearchUtils.fieldValue(TestUtil.generateRandomAlphaNumeric(), String.class, fieldMap));

		assertFieldValue(enumField, fieldMap);
		assertFieldValue(integerField, fieldMap);
		assertFieldValue(doubleField, fieldMap);

	}

	private void assertFieldValue(Field field, Multimap<String, Field> fieldMap) {
		assertEquals(	"field expected " + field,
						field.value(),
						SearchUtils.fieldValue(field.key(), field.value().getClass(), fieldMap));

	}

	@Test
	public void testQueryFilterIsDatastore() {
		Filter filter = Filter.create().setQuery(TestUtil.generateRandomAlphaNumeric()).build();
		boolean expectedDatastore = false;

		assertDatastoreFilter(filter, expectedDatastore);
	}

	/**
	 * The default, when nothing else qualifies, is to use the datastore since it's likely an
	 * internal call looking for all entities.
	 * 
	 */
	@Test
	public void testNothingIsDatastore() {
		assertDatastoreFilter(Filter.create().build(), true);
	}

	@Test
	public void testBoundsFilterIsDatastore() {
		assertDatastoreFilter(Filter.create().setBounds(GeoCellTestUtil.MARIN_VIEW_BOUNDS).build(), false);
	}

	@Test
	public void testDatastoreOption() {
		assertDatastoreFilter(SearchUtils.isDatastoreFilter(Filter.create()).build(), true);
	}

	private void assertDatastoreFilter(Filter filter, boolean expectedDatastore) {
		assertEquals(filter.toString(), expectedDatastore, SearchUtils.isDatastoreFilter(filter));
	}
}
