/**
 * 
 */
package com.aawhere.persist;

import com.googlecode.objectify.annotation.Entity;

/**
 * @author aroller
 * 
 */
@Entity
public class StringMockEntityWithParent
		extends BaseEntity<StringMockEntityWithParent, StringMockEntityWithParentId> {

	/**
	 * 
	 */
	public StringMockEntityWithParent() {
	}

	/*
	 * @see com.aawhere.persist.BaseEntity#idValue()
	 */
	@Override
	protected Object idValue() {
		return null;
	}

}
