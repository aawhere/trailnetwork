/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.lang.reflect.GenericUtils;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Base Unit test to be extends by any Entities that extends {@link BaseEntities}. This will provide
 * basic tests common for all BaseEntities.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class BaseEntitiesBaseUnitTest<E extends BaseEntity<E, ?>, EntitiesT extends BaseEntities<E>, B extends BaseEntities.Builder<B, E, EntitiesT>> {

	EntitiesT entities;
	private Class<B> builderClass;

	@Before
	final public void baseSetUp() {
		builderClass = GenericUtils.getTypeArgument(getClass(), Index.THIRD);
		this.entities = getEntitiesSample();
	}

	/**
	 * Provide the builder for the entities allowing the base class to assign common values.
	 */
	final public B getEntitiesBuilder() {
		try {
			return builderClass.newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Pouplates and tests a unique Entities and it's fields.
	 * 
	 * @return
	 */
	public EntitiesT getEntitiesSample() {
		B builder = getEntitiesBuilder();
		createEntities(builder);
		EntitiesT built = builder.build();
		assertCreation(built);
		return built;
	}

	/**
	 * Create entities
	 */
	public B createEntities(B builder) {
		return builder.add(createEntitySampleWithId()).add(createEntitySampleWithId());
	}

	/**
	 * Creates a new sample Entity with the id set. This method should return a new and unique
	 * entity each time it is called.
	 * 
	 * @return
	 */
	public abstract E createEntitySampleWithId();

	public void assertCreation(EntitiesT sample) {
		assertNotNull(sample);
		assertFalse("come on, give me at least one!", sample.isEmpty());
	}

	@Test
	final public void testCreation() {
		EntitiesT sample = getEntitiesSample();
		assertCreation(sample);
	}

	/**
	 * These methods should not be implemented. Mutation should occur in the builder.
	 */

	/**
	 * Runs the {@link #getEntitySample()} through a JAXB Marshalling/Unmarshalling cycle and
	 * verifies properties are being handled appropriately.
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testJaxbRoundTrip() throws JAXBException {
		EntitiesT initial = getEntitiesSample();
		assertFalse(initial.isEmpty());
		JAXBTestUtil<EntitiesT> util = JAXBTestUtil.create(initial).adapter(getAdapters())
				.classesToBeBound(getClassesToBeBound()).build();
		EntitiesT recreated = util.getActual();
		String xml = util.getXml();
		assertFalse("did you remember to add @XmlElement to your overriding getCollections method? " + xml,
					recreated.isEmpty());
		assertEquals(xml, initial, recreated);
	}

	/**
	 * Override if you need to add classes to the JaxB context
	 * 
	 * @return
	 */
	protected Class<?>[] getClassesToBeBound() {
		return new Class[0];
	}

	abstract public Set<XmlAdapter<?, ?>> getAdapters();

}
