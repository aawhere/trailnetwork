/**
 *
 */
package com.aawhere.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.persist.Filter;
import com.aawhere.persist.LongMockEntityId;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.google.common.collect.Lists;

/**
 * @author brian
 * 
 */
public class SearchDocumentUnitTest {

	SearchDocument doc1;
	SearchDocument doc2;
	SearchDocument doc3;

	@Before
	public void setup() {
		doc1 = SearchTestUtil.getSampleSearchDocument(	Lists.newArrayList(SearchTestUtil.generateRandomField()),
														"doc1",
														new LongMockEntityId(1l));
		doc2 = SearchTestUtil.getSampleSearchDocument(	Lists.newArrayList(SearchTestUtil.generateRandomField()),
														"doc2",
														new LongMockEntityId(1l));
		doc3 = SearchTestUtil.getSampleSearchDocument(	Lists.newArrayList(SearchTestUtil.generateRandomField()),
														"doc2",
														new LongMockEntityId(2l));
	}

	@Test
	public void testEquals() {
		assertEquals(doc1, doc2);
		assertFalse(doc1.equals(doc3));

	}

	@Test
	public void testJaxbRoundTrip() {
		Class<?>[] classes = { SearchDocument.class, SearchDocument.Field.class };
		SearchDocument doc = SearchTestUtil.getSampleSearchDocument(Lists.newArrayList(SearchTestUtil
				.generateRandomField()), "doc1", new LongMockEntityId(1l));
		JAXBTestUtil<SearchDocument> util = JAXBTestUtil.create(doc).classesToBeBound(classes).build();
		assertNotNull(util.getXml());
		assertNotNull(util.getActual());
		// System.out.println(util.getXml());
	}

	@Test
	public void testSearchDocumentsJaxbRoundTrip() {
		Class<?>[] classes = { SearchDocument.class, SearchDocument.Field.class, SearchDocuments.class };
		ArrayList<SearchDocument> docs = Lists.newArrayList(doc1, doc2);
		SearchDocuments searchDocuments = SearchDocuments.create().searchDocuments(docs)
				.filter(Filter.create().build()).build();
		JAXBTestUtil<SearchDocuments> util = JAXBTestUtil.create(searchDocuments).classesToBeBound(classes).writeOnly()
				.build();
		assertNotNull(util.getXml());
		util.assertContains("filter");
	}

}
