/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author roller
 * @see RepositoryUtil
 */
public class RepositoryUtilUnitTest {

	final LongMockEntityId id = new LongMockEntityId(3L);

	@Test(expected = EntityNotFoundException.class)
	public void testNotFound() throws Exception {
		LongMockEntity result = null;
		try {
			RepositoryUtil.finderResult(id, result);
		} catch (EntityNotFoundException e) {
			TestUtil.assertContains(e.getMessage(), id.getValue().toString());
			throw e;
		}
	}

	@Test
	public void testFound() throws EntityNotFoundException {
		LongMockEntity entity = (new LongMockEntity.Builder()).setId(id).build();
		assertEquals(entity, RepositoryUtil.finderResult(id, entity));
	}
}
