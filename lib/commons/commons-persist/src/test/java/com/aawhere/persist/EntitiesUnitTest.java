/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Test;

import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.id.IdentifierTestUtil.Child;
import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

import com.google.common.base.Function;
import com.google.common.collect.Ordering;

/**
 * @author brian
 * 
 */
public class EntitiesUnitTest
		extends BaseEntitiesBaseUnitTest<LongMockEntity, LongMockEntities, LongMockEntities.Builder> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#createEntitySampleWithId()
	 */
	@Override
	public LongMockEntity createEntitySampleWithId() {
		return LongMockEntityUnitTest.getInstance().getEntitySampleWithId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return new HashSet<XmlAdapter<?, ?>>();
	}

	@Override
	public Class<?>[] getClassesToBeBound() {
		Class<?>[] classes = { LongMockEntityId.class, LongMockEntity.class, LongMockEntities.class };
		return classes;
	}

	@Test
	public void testSupportingEntities() {
		// simple test by adding supporting entities and ensuring they are available.
		LongMockEntities support = getEntitiesSample();
		LongMockEntityId individualSupport = new LongMockEntityId(9l);
		LongMockEntities supportedEntities = new LongMockEntities.Builder().addSupport(support)
				.addSupport(individualSupport).build();
		LongMockEntities support2 = supportedEntities.support(support.getClass());
		assertEquals(support, support2);
		JAXBTestUtil<LongMockEntities> util = JAXBTestUtil.create(supportedEntities)
				.classesToBeBound(getClassesToBeBound()).build();
		util.assertContains(support.getCollection().get(0).id().toString());
		util.assertContains(individualSupport.toString());
	}

	@Test
	public void testSupportingIds() {
		// simple test by adding supporting entities and ensuring they are available.
		List<ChildId> ids = IdentifierTestUtil.generateRandomChilIds();
		Filter childIdFilter = Filter.create().setPage(2349872).filter();
		FilteredResponse<ChildId> childIdsResponse = FilteredResponse.create(ids).setFilter(childIdFilter).build();
		FilteredResponse<Child> childrenResponse = FilteredResponse.create(IdentifierTestUtil.generateRandomChildren())
				.build();
		LongMockEntities entities = new LongMockEntities.Builder().addSupport(childIdsResponse)
				.addSupport(childrenResponse).build();
		Collection<?> support = entities.support();
		TestUtil.assertContains(childIdsResponse, support);
		TestUtil.assertContains(childrenResponse, support);
		JAXBTestUtil<LongMockEntities> util = JAXBTestUtil.create(entities).classesToBeBound(getClassesToBeBound())
				.classesToBeBound(FilteredResponse.class, ChildId.class, Child.class).build();
		util.assertContains(ids.get(0).toString());
		util.assertContains(childIdsResponse.getFilter().getPage().toString());
	}

	@Test
	public void testTransformer() {
		final String name = TestUtil.generateRandomAlphaNumeric();
		LongMockEntities original = getEntitiesSample();
		Function<LongMockEntity, LongMockEntity> transformer = new Function<LongMockEntity, LongMockEntity>() {

			@Override
			public LongMockEntity apply(LongMockEntity input) {
				return new LongMockEntity.Builder(input).setName(name).build();
			}
		};

		LongMockEntities transformed = new LongMockEntities.Builder().transformer(transformer).addAll(original).build();
		for (LongMockEntity longMockEntity : transformed) {
			assertEquals(name, longMockEntity.getName());
		}
	}

	@Test
	public void testOrdering() {
		LongMockEntities entitiesSample = getEntitiesSample();
		Ordering<LongMockEntity> ordering = Ordering.natural().onResultOf(EntityTestUtil.countFunction());
		LongMockEntities ordered = new LongMockEntities.Builder().ordering(ordering).addAll(entitiesSample).build();
		TestUtil.assertIterablesEquals("ordering made unequal", entitiesSample, ordered);
		int previousCount = Integer.MIN_VALUE;
		for (LongMockEntity longMockEntity : ordered) {
			Integer count = longMockEntity.count;
			TestUtil.assertGreaterThan(longMockEntity + " is out of order " + ordered, previousCount, count);
			previousCount = count;
		}
	}
}
