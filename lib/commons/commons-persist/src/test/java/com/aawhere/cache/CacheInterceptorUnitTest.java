/**
 *
 */
package com.aawhere.cache;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInvocation;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.lang.NotImplementedException;

/**
 * @author Brian Chapman
 * 
 */
public class CacheInterceptorUnitTest {

	CacheInterceptor interceptor;
	String valueToCache = "MyObject";
	String methodName = "toBeCached";
	String methodNameWithKey = "toBeCachedWithKey";

	@Before
	public void setUp() {
		CacheTestUtil.setUpCachManager();
		interceptor = new CacheInterceptor();
		interceptor.setCacheService(new CacheService());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvokeWithNoParameters() throws Throwable {
		Method method = MockTestClass.class.getMethod(methodName);
		Object[] parameters = new Object[0];
		assertInvocation(parameters, method);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvokeWithNoKeyDefined() throws Throwable {
		Method method = MockTestClass.class.getMethod(methodName, String.class, String.class);
		Object[] parameters = new Object[2];
		parameters[0] = "one";
		parameters[1] = "two";
		assertInvocation(parameters, method);
	}

	@Test
	public void testInvokeWithSingleArgument() throws Throwable {
		Method method = MockTestClass.class.getMethod(methodName, String.class);
		Object[] parameters = new Object[1];
		parameters[0] = "one";
		assertInvocation(parameters, method);
	}

	@Test
	public void testInvokeWithKeyDefined() throws Throwable {
		Method method = MockTestClass.class.getMethod(methodNameWithKey, String.class, String.class);
		Object[] parameters = new Object[2];
		parameters[0] = "one";
		parameters[1] = "two";
		assertInvocation(parameters, method);
	}

	private void assertInvocation(final Object[] paramaters, final Method method) throws Throwable {
		MethodInvocation methodInvocation = createMethodInvocation(paramaters, method);
		interceptor.invoke(methodInvocation);
	}

	private MethodInvocation createMethodInvocation(final Object[] paramaters, final Method method) {
		return new MethodInvocation() {

			@Override
			public Object proceed() throws Throwable {
				return valueToCache;
			}

			@Override
			public Object getThis() {
				throw new NotImplementedException();
			}

			@Override
			public AccessibleObject getStaticPart() {
				throw new NotImplementedException();
			}

			@Override
			public Object[] getArguments() {
				return paramaters;
			}

			@Override
			public Method getMethod() {
				return method;
			}
		};
	}

	private class MockTestClass {

		public String expected = "Expected Result";

		@Cache
		public String toBeCached() {
			return expected;
		}

		@Cache
		public String toBeCached(String argOne) {
			return expected;
		}

		@Cache
		public String toBeCached(String argOne, String argTwo) {
			return expected;
		}

		@Cache
		public String toBeCachedWithKey(@Cache.Key String argOne, String argTwo) {
			return expected;
		}

	}
}
