/**
 *
 */
package com.aawhere.persist;

/**
 * @author Aaron Roller
 * 
 */
public class LongMockEntityMockRepository
		extends MockFilterRepository<LongMockFilterEntities, LongMockEntity, LongMockEntityId>
		implements LongMockEntityRepository {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.MockEntityRepository#updateName(java.lang.String)
	 */
	@Override
	public LongMockEntity updateName(LongMockEntityId id, String name) throws EntityNotFoundException {
		LongMockEntity e = load(id);
		return update(new LongMockEntity.Builder(e).setName(name).build());
	}

}
