/**
 * 
 */
package com.aawhere.measure.geocell;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.field.FieldKey;
import com.aawhere.measure.geocell.GeoCellFilterBuilder.Builder;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class GeoCellFilterBuilderUnitTest {

	private static final FieldKey areaKey = new FieldKey("test", "geo");
	private static final FieldKey cellsKey = new FieldKey(areaKey, GeoCells.FIELD.CELLS);
	/** Mimics the external client providing filters via parameters and operations. */
	private Filter.Builder clientFilterBuilder;
	private GeoCellFilterBuilder geoFilterBuilder;
	private Filter mutatedFilter;
	private boolean tested = false;
	private String expectedCellInQuery;
	private Builder geoFilterBuilderBuilder;

	@Before
	public void setUp() {
		this.clientFilterBuilder = Filter.create();
	}

	@After
	public void test() {
		if (!this.tested) {
			this.tested = true;
			mutatedFilter = geoFilterBuilder().build();

			if (this.expectedCellInQuery != null) {
				Collection<FilterCondition> conditions = this.mutatedFilter.conditions(cellsKey);
				TestUtil.assertSize(1, conditions);
				FilterCondition condition = conditions.iterator().next();
				TestUtil.assertContains(cellsKey.getAbsoluteKey(), condition.getField());
				TestUtil.assertContains(condition.getValue().toString(), this.expectedCellInQuery);
			} else {
				TestUtil.assertEmpty(mutatedFilter.conditions(cellsKey));
			}
		}
	}

	/**
	 * @return
	 */
	private GeoCellFilterBuilder geoFilterBuilder() {
		if (this.geoFilterBuilder == null) {
			this.geoFilterBuilder = geoFilterBuilderBuilder().build();
		}
		return this.geoFilterBuilder;
	}

	/**
	 * @return
	 */
	private Builder geoFilterBuilderBuilder() {
		if (this.geoFilterBuilderBuilder == null) {
			this.geoFilterBuilderBuilder = GeoCellFilterBuilder.mutate(this.clientFilterBuilder.build()).key(areaKey);
		}
		return this.geoFilterBuilderBuilder;
	}

	@Test
	public void testNothing() {

	}

	@Test
	public void testBounds() {
		final String cell = GeoCellTestUtil.A.getCellAsString();
		clientFilterBuilder.setBounds(cell);
		this.expectedCellInQuery = cell;

	}

	@Test
	public void testBoundsIgnoredSinceCellsManuallySet() {
		// set the late cell first to the filter....
		final String lateCell = GeoCellTestUtil.A.getCellAsString();
		clientFilterBuilder.setBounds(lateCell);
		GeoCell earlyCell = GeoCellTestUtil.B;
		// but manual calls to the builder win and ignore the given bounds filter.
		this.geoFilterBuilder().intersection(GeoCells.create().add(earlyCell).build());
		this.expectedCellInQuery = earlyCell.getCellAsString();
	}

	@Test
	public void testProximityBounds() {
		final String cell = GeoCellTestUtil.A.getCellAsString();
		clientFilterBuilder.setBounds(cell);
		clientFilterBuilder.setProximity(true);
		this.expectedCellInQuery = cell;
	}

	@Test
	public void testProximityBoundsReducedToTarget() {
		GeoCells expectedCells = GeoCellTestUtil.MARIN_VIEW_CELLS;
		clientFilterBuilder.setBounds(GeoCellTestUtil.MARIN_VIEW_BOUNDS);
		clientFilterBuilder.setProximity(true);
		// restrict to the best number of cells.
		geoFilterBuilderBuilder().maxNumberOfCells(expectedCells.size());
		// use the Set.toString to match the general collections output
		this.expectedCellInQuery = expectedCells.getAll().toString();
	}

	@Test
	public void testProximityBoundsReducedToTargetWithoutProximity() {
		GeoCells expectedCells = GeoCellTestUtil.MARIN_VIEW_CELLS;
		clientFilterBuilder.setBounds(GeoCellTestUtil.MARIN_VIEW_BOUNDS);
		// restrict to the best number of cells.
		geoFilterBuilderBuilder().maxNumberOfCells(expectedCells.size());
		// just try and get one right if proximity isn't used.
		this.expectedCellInQuery = GeoCellTestUtil.MARIN_VIEW_CENTER_CELL.getCellAsString();
	}

}
