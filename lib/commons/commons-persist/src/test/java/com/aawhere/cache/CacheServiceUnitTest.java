/**
 *
 */
package com.aawhere.cache;

import static org.junit.Assert.*;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.test.TestUtil;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicates;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * @author Brian Chapman
 * 
 */
public class CacheServiceUnitTest {

	// Copied from CacheManager since it is private there.

	private final String key = "myKey";
	private final Long value = 12345L;
	private final String key2 = "myKey2";
	private final String value2 = "value2";
	private final NotSerializable valueNotSeralizable = new NotSerializable();
	CacheService cacheService;

	private HashMap<String, Serializable> map;
	Set<String> bothKeys;
	Collection<Serializable> bothValues;

	@Before
	public void setUp() {
		CacheTestUtil.setUpCachManager();
		cacheService = new CacheService();
		this.map = Maps.newHashMap();
		this.map.put(key, value);
		this.map.put(key2, value2);
		this.bothKeys = this.map.keySet();
		this.bothValues = this.map.values();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPutNotSeralizable() {
		CacheService cacheService = new CacheService();
		cacheService.put(key, valueNotSeralizable);
	}

	@Test
	public void testGet() {
		cacheService.put(key, value);
		Long cached = (Long) cacheService.get(key);
		assertEquals(value, cached);
	}

	@Test
	public void testFailedPut() {
		cacheService.put(new FailureCause(), value);
		// just making sure an exception is not thrown.

	}

	@Test
	public void testGetNotFound() {
		assertNull(cacheService.get(key));
	}

	@Test
	public void testGetAll() {
		// NONE IN CACHE

		TestUtil.assertEmpty(cacheService.getAll(bothKeys).entrySet());

		// ONE IN CACHE
		cacheService.put(key, value);

		final HashSet<String> firstKeys = Sets.newHashSet(key);
		Map<String, Serializable> firstResult = cacheService.getAll(firstKeys);
		assertNotNull(firstResult);
		// ignores one that doesn't exist
		TestUtil.assertContainsOnly(value, firstResult.values());
		TestUtil.assertSize("only the one known about should be returned", 1, cacheService.getAll(bothKeys).values());

		// TWO IN CACHE
		cacheService.put(key2, value2);
		TestUtil.assertContainsOnly(value, cacheService.getAll(firstKeys).values());

		final Map<String, Serializable> bothResults = cacheService.getAll(bothKeys);
		assertEquals(bothKeys, bothResults.keySet());

		TestUtil.assertContainsAll(bothValues, bothResults.values());
	}

	@Test
	public void testGetOrProduce() {
		assertNull("nothing in cache, no ability to provide", cacheService.get(key, keyFunction(), nothingProvider()));
		assertEquals("value should have been provided", value, cacheService.get(key, keyFunction(), valueProvider()));
		assertEquals("should have been produced and cached", value, cacheService.get(key, keyFunction()));
		assertEquals(	"value 2 should have been provided",
						value2,
						cacheService.get(key2, keyFunction(), valueProvider()));
	}

	@Test
	public void testGetAllOrProduce() {
		final Map<String, Serializable> emptyCacheNoProvider = cacheService.getAll(	bothKeys,
																					keyFunction(),
																					nothingsProvider());
		TestUtil.assertEmpty(emptyCacheNoProvider.entrySet());
		final Map<String, Serializable> all = cacheService.getAll(bothKeys, keyFunction(), valuesProvider());
		TestUtil.assertSize(2, all.entrySet());
		assertEquals("cache is returning different than expected", map, all);

	}

	@Test
	public void testGetAllOrProduceOneAtATime() {

		final Map<String, Serializable> oneInCache = cacheService.getAll(	Sets.newHashSet(key),
																			keyFunction(),
																			valuesProvider());
		TestUtil.assertContainsOnly(value, oneInCache.values());
		final Map<String, Serializable> twoInCache = cacheService.getAll(bothKeys, keyFunction(), valuesProvider());
		TestUtil.assertSize(2, twoInCache.entrySet());
	}

	/**
	 * @return
	 */
	private Function<String, Serializable> nothingProvider() {
		final Map<String, Serializable> emptyMap = Collections.emptyMap();
		return Functions.forMap(emptyMap, null);
	}

	private Function<Iterable<String>, Map<String, Serializable>> nothingsProvider() {
		HashMap<Iterable<String>, Map<String, Serializable>> emptyMap = new HashMap<>();
		final Map<String, Serializable> defaultValue = Collections.emptyMap();
		return Functions.forMap(emptyMap, defaultValue);
	}

	private Function<String, Serializable> valueProvider() {
		return Functions.forMap(this.map, null);
	}

	private Function<Iterable<String>, Map<String, Serializable>> valuesProvider() {
		return new Function<Iterable<String>, Map<String, Serializable>>() {

			@Override
			public Map<String, Serializable> apply(Iterable<String> input) {
				HashSet<String> keys = Sets.newHashSet(input);
				return Maps.filterKeys(map, Predicates.in(keys));
			}
		};
	}

	public static class NotSerializable {

	}

	public static class FailureCause {
		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			throw new UnsupportedOperationException("this is supposed to happen since it is testing a failed put");
		}
	}

	public Function<String, String> keyFunction() {
		// transforms the key
		return new Function<String, String>() {

			@Override
			public String apply(String input) {
				return (input == null) ? null : input + "-test";
			}
		};
	}

}
