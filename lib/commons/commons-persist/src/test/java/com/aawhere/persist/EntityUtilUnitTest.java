/**
 * 
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Test;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.id.StringIdentifier;

/**
 * @author aroller
 * 
 */
public class EntityUtilUnitTest {

	private StringMockEntity entity = StringMockEntityUnitTest.getInstance().getEntitySampleWithId();

	@Test
	public void testIsEntity() {
		assertTrue(EntityUtil.isEntity(entity.getClass()));
		assertTrue(EntityUtil.isEntity(entity));
		assertFalse(EntityUtil.isEntity(String.class));
		assertFalse(EntityUtil.isEntity("junk"));
	}

	@Test
	public void testIdentifiesEntity() {
		assertTrue("string mock is an entity", EntityUtil.identifiesEntity(StringMockEntityId.class));
		StringIdentifier<EntityUtilUnitTest> id = new StringIdentifier<EntityUtilUnitTest>("junk",
				EntityUtilUnitTest.class) {

			private static final long serialVersionUID = 1L;
		};
		assertFalse(EntityUtil.identifiesEntity((Class<? extends Identifier<?, ?>>) id.getClass()));
		assertFalse("child is not an enitity in the persist sense.", EntityUtil.identifiesEntity(ChildId.class));
	}

	@Test
	public void testModifiedSince() {
		entity = EntityTestUtil.generateRandomStringEntityStored();
		DateTime updated = entity.getDateUpdated();
		DateTime before = updated.minus(2374987);
		DateTime after = updated.plus(1203498);

		assertFalse("same should be false", EntityUtil.modifiedSince(entity, updated));
		assertTrue("before  is  modified since", EntityUtil.modifiedSince(entity, before));
		assertFalse("after isn't modified since given", EntityUtil.modifiedSince(entity, after));
	}

	@Test
	public void testPersisted() {
		LongMockEntity createdNotStored = LongMockEntity.create().build();
		assertFalse(createdNotStored.getEntityVersion() + " isn't persisted", EntityUtil.isPersisted(createdNotStored));

		final StringMockEntity stored = EntityTestUtil.generateRandomStringEntityStored();
		assertTrue("is persisted " + stored.getEntityVersion(), EntityUtil.isPersisted(stored));
	}

	@Test
	public void testContainsEntity() {
		Class<?> entityTypeContained = EntityUtil.entityTypeContained(LongMockEntities.class);
		assertEquals("Wrong type returned for entities", LongMockEntity.class, entityTypeContained);
	}

	@Test
	public void testContainsEntityNotOfEntitiesTYpe() {
		assertNull(EntityUtil.entityTypeContained(getClass()));
	}
}
