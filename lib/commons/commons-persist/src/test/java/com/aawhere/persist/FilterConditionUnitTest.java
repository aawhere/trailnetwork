/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationExample;
import com.aawhere.field.annotation.FieldAnnotationExample.Animal;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.string.StringAdapterFactory;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.aawhere.xml.bind.JAXBTestUtil.Builder;

import com.google.common.collect.Sets;

/**
 * Tests the basics of {@link FilterCondition}.
 * 
 * @author aroller
 * 
 */
public class FilterConditionUnitTest {

	String expectedField = "junk";
	FilterOperator expectedOperator = FilterOperator.EQUALS;
	String expectedValue = "trash with a space";
	FilterCondition<String> condition;

	@Before
	public void setUp() {
		condition = new FilterCondition.Builder<String>().field(expectedField).operator(expectedOperator)
				.value(expectedValue).build();
	}

	public static FilterConditionUnitTest create() {
		final FilterConditionUnitTest filterConditionUnitTest = new FilterConditionUnitTest();
		filterConditionUnitTest.setUp();
		return filterConditionUnitTest;
	}

	/**
	 * Simple unit test to ensure condition is working as suggested.
	 * 
	 */
	@Test
	public void testCondition() {
		assertCondition(this.condition);
		TestUtil.assertContains(condition.toString(), expectedField);
	}

	/**
	 *
	 */
	private void assertCondition(FilterCondition condition) {
		assertEquals(expectedField, condition.getField());
		assertEquals(expectedOperator, condition.getOperator());

		assertEquals(expectedValue, condition.getValue());
	}

	public static <V> void assertCondition(FilterCondition expected, FilterCondition actual) {
		assertEquals(expected.getField(), actual.getField());
		assertEquals(expected.getOperator(), actual.getOperator());
		assertEquals(expected.getValue(), actual.getValue());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testConditionsOnFilter() {
		Filter filter = new Filter.Builder().addCondition(condition).build();
		List<FilterCondition> conditions = filter.getConditions();
		assertEquals(1, conditions.size());
		FilterCondition<?> firstCondition = conditions.get(Index.FIRST.index);
		assertCondition(firstCondition);
		conditions.add(firstCondition);
		fail("that list shouldn't be mutable");
	}

	@Test
	public void testConditionString() {
		String expected = condition.toString();
		assertNotNull(expected);
		TestUtil.assertContains(expectedField, expectedField);
		TestUtil.assertContains(expected, expectedOperator.operator);
		TestUtil.assertContains(expected, expectedValue.toString());
	}

	@Test
	public void testFilterOperatorValueOf() {
		FilterOperator actual = FilterOperator.fromString(expectedOperator.operator);
		assertEquals(expectedOperator, actual);
	}

	@Test
	public void testFilterOperatorValueOfName() {
		final String name = expectedOperator.name();
		FilterOperator actual = FilterOperator.fromString(name);
		assertEquals(expectedOperator, actual);
	}

	@Test
	public void testFilterOperatorBadValue() {
		String badValue = expectedField;
		try {
			FilterOperator actual = FilterOperator.fromString(badValue);
			fail("this should communicate with an appropriate exception, but gave: " + actual);
		} catch (InvalidChoiceException e) {
			String message = e.getMessage();
			TestUtil.assertContains(message, badValue);
			// supposed to provide all choices.
			TestUtil.assertContains(message, expectedOperator.operator);
		}
	}

	@Test
	public void testJaxbStringValue() {

		Object expectedValue = this.expectedValue;
		FieldKey fieldKey = FieldAnnotationExample.TYPE_FIELD_KEY;
		assertJaxb(fieldKey, expectedValue);
	}

	/**
	 * Given a key referencing a field in {@link FieldAnnotationExample.Animal} and it's
	 * corresponding value this will make sure the xml transports successfully.
	 * 
	 * @param expectedValue
	 * @param fieldKey
	 */
	private void assertJaxb(FieldKey fieldKey, Object expectedValue) {
		Class<Animal> annotatedDictionaryClass = Animal.class;

		// TODO: move this to a util if anyone else needs something similar.
		FilterConditions conditions = new FilterConditions();
		FilterCondition<Object> fieldCondition = new FilterCondition.Builder<Object>().field(fieldKey)
				.operator(FilterOperator.EQUALS).value(expectedValue).build();
		conditions.conditions.add(fieldCondition);

		Builder<FilterConditions> jaxbUtilBuilder = JAXBTestUtil.create(conditions);

		FieldAnnotationDictionaryFactory annoationFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		annoationFactory.getDictionary(annotatedDictionaryClass);
		StringAdapterFactory stringAdapterFactory = new StringAdapterFactory();
		FilterConditionStringAdapter stringAdapter = (FilterConditionStringAdapter) new FilterConditionStringAdapter.Builder()
				.setAdapterFactory(stringAdapterFactory).setDictionaryFactory(annoationFactory.getFactory())
				.setRegistry(new FilterConditionSupportedTypeRegistry.Builder().build()).build();
		FilterConditionXmlAdapter xmlAdapter = new FilterConditionXmlAdapter.Builder().setStringAdapter(stringAdapter)
				.build();
		jaxbUtilBuilder.adapter(xmlAdapter);
		JAXBTestUtil<FilterConditions> util = jaxbUtilBuilder.build();
		assertCondition(fieldCondition, util.getActual().conditions.get(0));
	}

	@Test
	@Ignore("The only way you get an acutal enumerated message back is by declaring that enumerated message")
	public void testJaxbMessageValue() {
		assertJaxb(FieldAnnotationExample.MESSAGE_FIELD_KEY, PersistMessage.ENTITY_NOT_FOUND);
	}

	@Test
	public void testJaxbEnumValue() {
		assertJaxb(FieldAnnotationExample.HAIR_COLOR_FIELD_KEY, FieldAnnotationExample.Animal.HairColor.BROWN);
	}

	@Test
	public void testJaxbHashSet() {
		assertJaxb(	FieldAnnotationExample.HAIR_COLORS_FIELD_KEY,
					Sets.newHashSet(Animal.HairColor.BLACK, Animal.HairColor.WHITE));

	}

}
