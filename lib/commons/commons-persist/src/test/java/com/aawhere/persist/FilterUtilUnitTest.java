/**
 * 
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.persist.FilterCondition.Builder;
import com.aawhere.test.TestUtil;

import com.google.common.base.Predicate;
import com.google.common.collect.LinkedListMultimap;

/**
 * @author aroller
 * 
 */
public class FilterUtilUnitTest {

	@Test
	public void testSplitByDomain() {
		FilterCondition<Object> stringCondition = FilterCondition.create().field(StringMockEntity.FIELD.KEY.NAME)
				.operator(FilterOperator.EQUALS).value("junk").build();
		FilterCondition<Object> longCondition = FilterCondition.create().field(LongMockEntity.FIELD.KEY.NAME)
				.operator(FilterOperator.EQUALS).value("junk").build();
		FilterCondition<Object> longNestedCondition = FilterCondition.create()
				.field(LongMockEntity.FIELD.KEY.EMBEDDED_PROPERTY_IN_EMBED_1).operator(FilterOperator.EQUALS)
				.value("junk").build();
		FilterSort longOrder = FilterSort.create().setFieldKey(LongMockEntity.FIELD.KEY.COUNT).build();
		FilterSort stringOrder = FilterSort.create().setFieldKey(StringMockEntity.FIELD.KEY.NAME).build();

		final Integer limit = Filter.DEFAULT_LIMIT + 1;
		Filter filter = Filter.create().setLimit(limit).addCondition(stringCondition).addCondition(longCondition)
				.addCondition(longNestedCondition).orderBy(longOrder).orderBy(stringOrder).build();

		Map<String, Filter> filtersByDomain = FilterUtil.filtersByDomain(filter);
		TestUtil.assertSize(2, filtersByDomain.keySet());
		Filter longFilter = filtersByDomain.get(LongMockEntity.FIELD.DOMAIN_KEY);
		TestUtil.assertContains(longCondition, longFilter.getConditions());
		TestUtil.assertContains(longNestedCondition, longFilter.getConditions());
		TestUtil.assertSize(2, longFilter.getConditions());
		TestUtil.assertContains(longOrder, longFilter.getOrderBy());
		TestUtil.assertSize(1, longFilter.getOrderBy());

		Filter stringFilter = filtersByDomain.get(StringMockEntity.FIELD.DOMAIN);
		TestUtil.assertContains(stringCondition, stringFilter.getConditions());
		TestUtil.assertSize(1, stringFilter.getConditions());
		TestUtil.assertContains(stringOrder, stringFilter.getOrderBy());
		TestUtil.assertSize(1, stringFilter.getOrderBy());

		assertEquals(limit, stringFilter.getLimit());
		assertEquals(limit, longFilter.getLimit());

	}

	@Test
	public void testFilterConditionPredicate() throws FieldNotRegisteredException {
		assertCountFilterCondition(5, 5, FilterOperator.EQUALS, true);
		assertCountFilterCondition(5, 6, FilterOperator.EQUALS, false);
		assertCountFilterCondition(5, 5, FilterOperator.NOT_EQUALS, false);
		assertCountFilterCondition(5, 6, FilterOperator.NOT_EQUALS, true);
		assertCountFilterCondition(5, 6, FilterOperator.GREATER_THAN, true);
		assertCountFilterCondition(6, 5, FilterOperator.LESS_THAN, true);
		assertCountFilterCondition(5, 6, FilterOperator.LESS_THAN, false);
		assertCountFilterCondition(5, 6, FilterOperator.LESS_THAN_OR_EQUALS, false);
		assertCountFilterCondition(5, 5, FilterOperator.LESS_THAN_OR_EQUALS, true);
		assertCountFilterCondition(5, 5, FilterOperator.GREATER_THAN_OR_EQUALS, true);
		assertCountFilterCondition(5, 6, FilterOperator.GREATER_THAN_OR_EQUALS, true);
		assertCountFilterCondition(6, 5, FilterOperator.GREATER_THAN_OR_EQUALS, false);
		// nulls don't participate
		assertCountFilterCondition(6, null, FilterOperator.EQUALS, false);
		assertCountFilterCondition(6, null, FilterOperator.GREATER_THAN, false);
		// when searching for a null then nulls participate
		assertCountFilterCondition(null, null, FilterOperator.EQUALS, true);
		assertCountFilterCondition(null, 6, FilterOperator.NOT_EQUALS, true);
		assertCountFilterCondition(null, null, FilterOperator.NOT_EQUALS, false);
		assertCountFilterCondition(5, 5, FilterOperator.IN, true);

		// collections
		assertCountFilterCondition(5, FilterOperator.IN, true, 5, 6);
		assertCountFilterCondition(4, FilterOperator.IN, false, 5, 6);
		assertCountFilterCondition(5, FilterOperator.NOT_IN, false, 5, 6);
		assertCountFilterCondition(4, FilterOperator.NOT_IN, true, 5, 6);
	}

	@Test(expected = UnsupportedFilterOperatorException.class)
	public void testFilterConditionPredicateGreaterThanOperatorNotSupportedWithNulls()
			throws FieldNotRegisteredException {
		assertCountFilterCondition(null, 6, FilterOperator.GREATER_THAN, false);
	}

	private void assertCountFilterCondition(Integer conditionValue, Integer entityValue, final FilterOperator operator,
			Boolean expectedTrue) throws FieldNotRegisteredException {
		assertCountFilterCondition(entityValue, operator, expectedTrue, conditionValue);
	}

	private void assertCountFilterCondition(Integer entityValue, final FilterOperator operator, Boolean expectedTrue,
			Integer... conditionValues) throws FieldNotRegisteredException {
		// setup the entity to have the value and the condition to have the comparison value
		final Builder<Object> conditionBuilder = FilterCondition.create();
		// exercise the different methods since they are handled differently
		if (conditionValues.length == 1) {
			conditionBuilder.value(conditionValues[0]);
		} else {
			conditionBuilder.value(conditionValues);
		}
		FilterCondition<Object> countCondition = conditionBuilder.field(LongMockEntity.FIELD.KEY.COUNT)
				.operator(operator).build();
		LongMockEntity entity = LongMockEntity.create().setCount(entityValue).build();
		final FieldDictionaryFactory dictionaryFactory = FieldAnnotationTestUtil.getDictionaryFactory();
		final FieldAnnotationDictionaryFactory annotationDictionary = new FieldAnnotationDictionaryFactory(
				dictionaryFactory);
		annotationDictionary.getDictionary(LongMockEntity.class);

		// now test the predicate
		Predicate<LongMockEntity> predicate = FilterUtil.filterConditionPredicate(	countCondition,
																					dictionaryFactory,
																					LongMockEntity.class);
		assertEquals(entityValue + " " + countCondition.toString(), expectedTrue, predicate.apply(entity));
	}

	@Test
	@Ignore("it's difficult to setup the support classes for this method. ")
	public void testConditionsFromQueryParams() throws BaseException {
		LinkedListMultimap<String, String> queryParameters = LinkedListMultimap.create();
		queryParameters.asMap().entrySet();
		FilterUtil.conditions((Set<Entry<String, List<String>>>) null, null);
	}
}
