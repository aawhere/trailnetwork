/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Brian Chapman
 * 
 */
public abstract class TestBaseFilterEntitiesUnitTest<EntitiesT extends BaseFilterEntities<? extends BaseEntity<?, ?>>, BuilderT extends BaseFilterEntities.Builder<BuilderT, ?, EntitiesT>> {

	@Test
	public void testFilters() {
		Filter filter = createTestFilter();
		EntitiesT entities = getEntitiesBuilder().setFilter(filter).build();
		assertNotNull(entities);
		assertEquals(filter, entities.getCollection());
	}

	/**
	 * @return
	 */
	protected abstract Filter createTestFilter();

	protected abstract BuilderT getEntitiesBuilder();
}
