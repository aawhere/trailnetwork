/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Brian Chapman
 * 
 */
public class FilterConditionSupportedTypeRegistryUnitTest {

	private FilterConditionSupportedTypeRegistry registry;
	private final Class<?> fieldType = String.class;
	private final FilterOperator equalsOperator = FilterOperator.EQUALS;
	private final FilterOperator gtOperator = FilterOperator.GREATER_THAN;
	private final FilterOperator unsupportedOperator = FilterOperator.GREATER_THAN;
	private final Class<?> valueType = Number.class;
	private final Class<?> unsupportedType = Void.class;

	@Before
	public void setUp() {
		registry = FilterConditionSupportedTypeRegistry.create().build();
	}

	@Test
	public void testRegistry() {
		registry.register(fieldType, equalsOperator, valueType);

		assertTrue(registry.supported(fieldType, equalsOperator, valueType));
		Set<Class<?>> supportedTypes = registry.supportedTypes(fieldType, equalsOperator);
		assertEquals(1, supportedTypes.size());
		assertEquals(valueType, supportedTypes.iterator().next());

		assertTrue(!registry.supported(fieldType, equalsOperator, unsupportedType));
		assertTrue(!registry.supported(fieldType, unsupportedOperator, valueType));
		assertTrue(!registry.supported(unsupportedType, unsupportedOperator, valueType));

		registry.register(fieldType, gtOperator, valueType);
		assertTrue(registry.supported(fieldType, equalsOperator, valueType));
		assertTrue(registry.supported(fieldType, gtOperator, valueType));
	}

	@Test
	public void testDoubleRegister() {
		registry.register(fieldType, equalsOperator, valueType);
		assertEquals(1, registry.supportedTypes(fieldType, equalsOperator).size());
		registry.register(fieldType, equalsOperator, valueType);
		assertEquals(1, registry.supportedTypes(fieldType, equalsOperator).size());
	}
}
