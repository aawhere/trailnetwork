/**
 *
 */
package com.aawhere.persist;

/**
 * @author Aaron Roller
 * 
 */
public class StringMockEntityMockRepository
		extends MockRepository<StringMockEntity, StringMockEntityId>
		implements StringMockEntityRepository {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.MockEntityRepository#updateName(java.lang.String)
	 */
	@Override
	public StringMockEntity updateName(StringMockEntityId id, String name) throws EntityNotFoundException {
		StringMockEntity e = load(id);
		return update(new StringMockEntity.Builder(e).setName(name).build());
	}
}
