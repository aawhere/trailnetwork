/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.persist.LongMockEntity.Builder;
import com.aawhere.persist.LongMockEntity.MockEmbeded;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Example UnitTest to use the {@link BaseEntityBaseUnitTest} for testing MockEntities.
 * 
 * @author Aaron Roller
 * 
 */
public class LongMockEntityUnitTest
		extends
		BaseEntityBaseUnitTest<LongMockEntity, LongMockEntity.Builder, LongMockEntityId, LongMockEntityRepository> {

	private String originalName;
	private String mutatedName;
	private Integer count;
	private StringMockEntityId relatedId;
	private MockEmbeded embed1;

	/**
	 * 
	 */
	public LongMockEntityUnitTest() {
		super(MUTABLE);
	}

	@Override
	@Before
	public void setUp() {
		originalName = TestUtil.generateRandomString();
		mutatedName = TestUtil.generateRandomString();
		count = TestUtil.generateRandomInt();
		relatedId = new StringMockEntityId(TestUtil.generateRandomAlphaNumeric());
		embed1 = new MockEmbeded();
		embed1.property = TestUtil.generateRandomInt();
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected LongMockEntityRepository createRepository() {
		return new LongMockEntityMockRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#populateBeforeCreation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.setName(originalName);
		builder.setCount(count);
		builder.setRelated(relatedId);
		builder.setEmbed1(embed1);
	}

	@Override
	public void assertCreation(LongMockEntity sample) {
		super.assertCreation(sample);
		assertEquals(this.originalName, sample.getName());
		assertEquals(this.relatedId, sample.getRelated());
		assertEquals(this.count, sample.getCount());
		assertEquals(this.embed1.property, sample.getEmbed1().property);
	};

	@Test
	public void testSameChildDifferentParents() {
		Long parent1Value = TestUtil.generateRandomLong();
		Long parent2Value = TestUtil.generateRandomLong();
		Long childValue = TestUtil.generateRandomLong();
		LongMockEntityWithParentId id1 = new LongMockEntityWithParentId(parent1Value, childValue);
		LongMockEntityWithParentId id2 = new LongMockEntityWithParentId(parent2Value, childValue);

		TestUtil.assertNotEquals(id1, id2);
		TestUtil.assertNotEquals(id1.hashCode(), id2.hashCode());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(LongMockEntity mutated) {
		super.assertMutation(mutated);
		assertEquals(mutatedName, mutated.getName());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.l.Id)
	 */
	@Override
	protected LongMockEntity mutate(LongMockEntityId id) throws EntityNotFoundException {
		return getRepository().updateName(id, mutatedName);
	}

	/**
	 * Runs the {@link #getEntitySample()} through a JAXB Marshalling/Unmarshalling cycle and
	 * verifies properties are being handled appropriately.
	 * 
	 * TODO: promote this to BaseEntityBaseUnitTest
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testJaxbRoundTrip() throws JAXBException {
		LongMockEntity sample = getEntitySample();
		JAXBTestUtil<LongMockEntity> util = JAXBTestUtil.create(sample).build();
		LongMockEntity actual = util.getActual();

		// just re-use the existing method to ensure the sample was "re-created" appropriately
		assertCreation(actual);
		assertEquals(sample.getName(), actual.getName());
	}

	protected static LongMockEntityUnitTest getInstance() {
		LongMockEntityUnitTest instance = new LongMockEntityUnitTest();
		instance.baseSetUp();
		instance.setUp();
		return instance;
	}

	@Test
	public void testMockDictionary() {
		super.testDictionary();
	}

}
