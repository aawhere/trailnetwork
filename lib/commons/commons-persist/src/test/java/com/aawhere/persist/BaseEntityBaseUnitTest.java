/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.collections.Index;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.id.HasParent;
import com.aawhere.id.Identifier;
import com.aawhere.id.KeyedEntity;
import com.aawhere.lang.annotation.AnnotationUtilsExtended;
import com.aawhere.lang.reflect.GenericUtils;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Lists;
import com.googlecode.objectify.annotation.Parent;

/**
 * Base Unit test to be extends by any Entity that extends {@link BaseEntity}. This will provide
 * basic tests common for all BaseEntities and its mingling with it's corresponding
 * {@link Repository}.
 * 
 * Intended to be tested against a {@link MockRepository} for general, in-memory baseline testing.
 * 
 * Actual datastore implementations should extend this Test and provide their repository.
 * 
 * @author Aaron Roller
 * 
 */
abstract public class BaseEntityBaseUnitTest<E extends BaseEntity<E, IdentifierT>, B extends BaseEntity.Builder<E, ?, IdentifierT, B>, IdentifierT extends Identifier<?, E>, R extends Repository<E, IdentifierT>> {

	/**
	 * 
	 */
	private static final int VERSION_AFTER_CREATION = BaseEntity.DEFAULT_VERSION + 1;
	protected static final Boolean NON_MUTABLE = false;
	protected static final Boolean MUTABLE = true;

	private Class<E> entityClass;
	private Class<B> builderClass;
	private Class<IdentifierT> identifierClass;
	private Boolean mutableEntity = MUTABLE;
	private BaseEntityReflection<E, B> reflector;
	private R repository;

	/**
	 *
	 */
	public BaseEntityBaseUnitTest() {
	}

	/**
	 * Reserved method for children to setup their tests.
	 * 
	 * Be sure to add a {@link Before} annotation.
	 * 
	 */
	abstract public void setUp();

	@SuppressWarnings("unchecked")
	@Before
	final public void baseSetUp() {
		List<Class<?>> genericClasses = GenericUtils.getTypeArguments(BaseEntityBaseUnitTest.class, getClass());
		entityClass = (Class<E>) genericClasses.get(Index.FIRST.index);
		builderClass = (Class<B>) genericClasses.get(Index.SECOND.index);
		identifierClass = (Class<IdentifierT>) genericClasses.get(Index.THIRD.index);
		reflector = new BaseEntityReflection<E, B>();
		repository = createRepository();
	}

	/**
	 * Used to indicated the implementing test represents a class that is not mutable in the system.
	 * 
	 * @param mutableEntity
	 */
	protected BaseEntityBaseUnitTest(Boolean mutableEntity) {
		this.mutableEntity = mutableEntity;
	}

	/**
	 * Pouplates and tests a unique Entity and it's fields.
	 * 
	 * @return
	 */
	public E getEntitySample() {
		B builder = getEntityBuilder();
		creationPopulation(builder);
		E built = builder.build();
		assertCreation(built);
		return built;
	}

	protected E getEntitySamplePersisted() {
		return repository.store(getEntitySample());
	}

	/**
	 * Pouplates and tests a unique Entity and it's fields with the Id set.
	 * 
	 * @return
	 */
	public E getEntitySampleWithId() {
		B builder = getEntityBuilder();
		creationPopulationWithId(builder);
		E built = builder.build();
		assertNotNull("id", built.id());
		return built;
	}

	/**
	 * Ensures the entity has a dictionary setup. This is currently not automatic, but should be
	 * pending some busy work to set up all of the entities. Override and add a test annotation to
	 * implementing entity tests.
	 * 
	 * @return
	 */
	@Test
	public void testDictionary() {
		FieldDictionary dictionary = getFieldDictionary();

		assertNotNull(dictionary);
		assertNotNull(dictionary.getDomainKey());
		// the following provides warnings built in to the code. be more forgiving here.
		// assertNotNull("no domain name", dictionary.getDomain());
		// assertNotNull(dictionary.getDescription());
		String idFieldValue = BaseEntity.BASE_FIELD.ID;
		FieldDefinition<?> idDefinition = dictionary.getDefinition(idFieldValue);
		assertNotNull("you didn't define your id", idDefinition);
		assertTrue("your field " + idDefinition.getType() + " doesn't match the expected identifier type "
				+ identifierClass, idDefinition.getType().isAssignableFrom(identifierClass));
		assertEquals(idFieldValue, idDefinition.getKey().getRelativeKey());
	}

	/**
	 * @param builder
	 */
	protected void creationPopulation(B builder) {

	}

	/**
	 * @param builder
	 */
	protected void creationPopulationWithId(B builder) {
		creationPopulation(builder);
		if (!selfIdentifying()) {
			builder.setId(getId());
		}
	}

	/**
	 * @return
	 */
	public boolean selfIdentifying() {
		return SelfIdentifyingEntity.class.isAssignableFrom(entityClass);
	}

	/**
	 * Children with special ids must override this (like ids with parents).
	 * 
	 * @return
	 */
	protected IdentifierT getId() {
		final IdentifierT id;
		if (selfIdentifying()) {
			id = getEntitySample().getId();
		} else {
			id = EntityTestUtil.generateRandomId(identifierClass);
		}
		return id;
	}

	@Test
	final public void testCreation() {
		E sample = getEntitySample();
		assertCreation(sample);
	}

	@Test
	final public void testStore() throws EntityNotFoundException {
		E sample = getEntitySample();
		assertEquals("Versions do not match", BaseEntity.DEFAULT_VERSION, sample.getEntityVersion());
		sample = repository.store(sample);
		assertEquals("Versions do not match", VERSION_AFTER_CREATION, (int) sample.getEntityVersion());
		assertStored(sample);

		E found = repository.load(sample.getId());
		assertLoadAfterStore(found);
	}

	/**
	 * Asserts everything is good after the entity is persisted.
	 * 
	 * @param sample
	 */
	public void assertStored(E sample) {
		assertNotNull(sample.getId());
		assertEquals("Versions do not match", VERSION_AFTER_CREATION, (int) sample.getEntityVersion());
		assertNotNull("creation must be set upon original persit", sample.getDateCreated());
		if (sample instanceof DateUpdatedFilterable) {
			assertNotSame(	"filtering on date updated requires it's own storage",
							sample.getDateCreated(),
							sample.getDateUpdated());
		} else {
			// they aren't the same since new DateTime(Date) creates a separate instance for each.
			assertEquals(	"updated isn't stored unless it's a mutation or indexed. created is returend",
							sample.getDateCreated(),
							sample.getDateUpdated());
		}
		assertSample(sample);
		assertTrue("exists can't find " + sample.id(), repository.exists(sample.id()));

		if (sample.getId() instanceof HasParent) {
			Map<Field, Parent> fieldsWithParent = AnnotationUtilsExtended.getInstance().findFields(	Parent.class,
																									sample.getClass());
			TestUtil.assertSize("composite children must declare the @Parent once and only once",
								1,
								fieldsWithParent.entrySet());
		}
	}

	/**
	 * A common way for each of the assertion methods to call a place that asserts properties that
	 * don't change across state. So if something is different after mutation leave it out of this
	 * one, but otherwise if it stays the same then this will help ensure that it really does stay
	 * the same after storing, loading, etc.
	 * 
	 * @param sample
	 */
	protected void assertSample(E sample) {

	}

	@Test
	public void testMutationBuilderAccess() {

		if (this.mutableEntity) {
			Constructor<B> mutationConstructor;
			try {
				mutationConstructor = builderClass.getDeclaredConstructor(entityClass);
				mutationConstructor.setAccessible(true);
				int modifiers = mutationConstructor.getModifiers();
				assertEquals("way too loose of access for mutation", false, Modifier.isPublic(modifiers));
				assertEquals(	"probably too loose of access.  let's talk about this",
								false,
								Modifier.isProtected(modifiers));
				assertEquals(	"too strict. mutation needs to be done by the Repository.  you should have package access",
								false,
								Modifier.isPrivate(modifiers));
				// now, try to construct mutator
				E sample = getEntitySample();
				try {
					mutationConstructor.newInstance(sample);
					// now, make sure access is denied until built
					try {
						sample.getEntityVersion();
						fail("should have thrown illegal state");
					} catch (IllegalStateException e) {
						// sweet.
					}

				} catch (IllegalArgumentException e) {
					throw new RuntimeException(e);
				} catch (InstantiationException e) {
					throw new RuntimeException(e);
				} catch (IllegalAccessException e) {
					throw new RuntimeException(e);
				} catch (InvocationTargetException e) {
					throw new RuntimeException(e);
				}

			} catch (NoSuchMethodException e) {
				fail("no mutator method provided on builder. Pass #NON_MUTABLE in for the test constructor for non-mutable entities.");
			}
		}
	}

	/**
	 * Tests any entity that implements {@link KeyedEntity} for it's finder.
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testFindByKey() throws EntityNotFoundException {
		E sample = getEntitySample();
		// check for either to make sure the pair implement the right methods
		if (isKeyedEntity(sample, repository)) {
			// if ClassCastException, you forgot to extend KeyedEntity
			KeyedEntity<?> keyedEntity = (KeyedEntity<?>) sample;

			repository.store(sample);
			if (repository instanceof KeyedEntityFinder) {
				KeyedEntityFinder finder = (KeyedEntityFinder) repository;

				Identifier key = keyedEntity.getKey();
				assertNotNull(key);
				KeyedEntity found = finder.findByKey(key);
				assertEquals(sample, found);
				// great, now let's make sure not finding the key communicates
				// properly
				finder.findByKey(key);
				try {
					Identifier badKey = key.getClass().getConstructor(String.class)
							.newInstance("junk" + key.getValue());
					try {
						finder.findByKey(badKey);
						fail("should have thrown EntityNotFoundException");
					} catch (EntityNotFoundException notFound) {
						assertEquals(badKey, notFound.getId());
						// success...bad key should have caused this.
					}
				} catch (Exception e) {
					throw new RuntimeException(e);
				}

			} else {
				fail("Your entity implements KeyedEntity, but your repository doensn't implement the KeyedEntityFinder.");
			}

		}
	}

	/**
	 * a simple test that creates a few and loads them all back
	 * 
	 */
	@Test
	public void testLoadAll() {
		E first = getEntitySamplePersisted();
		E second = getEntitySamplePersisted();
		@SuppressWarnings("unchecked")
		Map<IdentifierT, E> all = repository.loadAll(Lists.newArrayList(first.id(), second.id()));
		TestUtil.assertContains(first, all.values());
		TestUtil.assertContains(second, all.values());

		// some tests create a duplicate id so dont' test the size, just that each entity is in the
		// list.
	}

	/**
	 * @param sample
	 * @param repository
	 * @return
	 */
	private boolean isKeyedEntity(E sample, final Repository<E, IdentifierT> repository) {
		return sample instanceof KeyedEntity<?> || repository instanceof KeyedEntityFinder;
	}

	@Test
	public void testDefaultBuilder() throws InstantiationException {

		B builder = reflector.constructDefaultBuilder(builderClass);
		assertNotNull(builder);
	}

	/**
	 * Provides assertions to make sure building was successful. Storage has not yet happened.
	 * 
	 * @param sample
	 */
	public void assertCreation(E sample) {
		if (sample instanceof SelfIdentifyingEntity) {
			assertNotNull("self-identifiers must provide thier id", sample.getId());
		} else {
			assertNull("if you wish to provide an id implement SelfIdentifyingEntity", sample.getId());

		}
		assertNull(sample.getDateCreated());
		assertNull(sample.getDateUpdated());
		if (sample instanceof KeyedEntity<?>) {
			assertNotNull(((KeyedEntity<?>) sample).getKey());
		}

		assertSample(sample);
	}

	/**
	 * 
	 * Assert that the two Entities are the same. This provides an opportunity to test in greater
	 * detail then relying on the {@link #equals(Object)} method.
	 * 
	 * @param expected
	 * @param actual
	 */
	public void assertEntityEquals(E expected, E actual) {
		assertEquals("The identifiers don't match", expected.getId(), actual.getId());
	}

	@Test
	final public void testMutation() throws EntityNotFoundException {
		if (this.mutableEntity) {
			E original = getEntitySample();

			repository.store(original);
			assertStored(original);

			Integer storedVersion = original.getEntityVersion();
			E mutated = mutate(original.getId());
			// make sure whatever was mutated is persisted.
			mutated = repository.load(original.getId());
			assertMutation(mutated);
			int minIncrementedVersion = storedVersion + 1;
			assertVersion(mutated, minIncrementedVersion);
			DateTime dateCreated = original.getDateCreated();
			assertEquals(dateCreated, mutated.getDateCreated());
			final DateTime mutationDateUpdated = mutated.getDateUpdated();
			assertTrue(dateCreated.isBefore(mutationDateUpdated) || dateCreated.isEqual(mutationDateUpdated));
		}
	}

	/**
	 * Special test for version since some mutations may update more than once. It simply does a
	 * greater than check instead of equality.
	 * 
	 * @param entity
	 * @param minIncrementedVersion
	 */
	private void assertVersion(E entity, int minIncrementedVersion) {
		assertTrue(	"updating should be increment version to at least " + minIncrementedVersion + ", but was "
							+ entity.getEntityVersion() + ".  you probably didn't change anything",
					minIncrementedVersion <= entity.getEntityVersion());
	}

	@Test(expected = EntityNotFoundException.class)
	final public void testMutationNotFound() throws EntityNotFoundException {
		mutate(getId());
	}

	@Test
	public void testCrud() throws EntityNotFoundException, InterruptedException {
		E sample = getEntitySample();
		Integer version = BaseEntity.DEFAULT_VERSION;
		// C is for create
		repository.store(sample);
		assertEquals(++version, sample.getEntityVersion());
		assertStored(sample);

		// R is for Read
		final IdentifierT id = sample.getId();
		E found = repository.load(id);
		assertEquals(version, found.getEntityVersion());
		assertLoadAfterStore(found);
		if (this.mutableEntity) {
			// U is for Update, but different for every Entity
			E mutated = mutate(id);
			// retrieve the mutated from the repository to ensure repository mutation
			mutated = repository.load(id);
			assertNotNull(mutated);
			assertEquals(found, mutated);
			assertVersion(mutated, ++version);
		}

		E foundAfterUpdating = repository.load(id);
		assertNotNull(foundAfterUpdating);
		assertVersion(foundAfterUpdating, version);
		// D is for Delete
		repository.delete(id);
		assertDeleted(foundAfterUpdating);
	}

	protected void assertDeleted(E recentlyFoundBeforeDeleting) throws EntityNotFoundException {
		assertDeleted(recentlyFoundBeforeDeleting.id());
	}

	protected void assertDeleted(IdentifierT id) throws EntityNotFoundException {
		try {
			repository.load(id);
			fail(id.toString() + " didn't delete");
		} catch (EntityNotFoundException e) {
			// cool, as expected
		}
	}

	@Test
	public void testAnnotation() {
		assertNotNull(	"you need to have the com.googlecode.objectify.annotation.Entity annotation",
						this.entityClass.getAnnotation(com.googlecode.objectify.annotation.Entity.class));
	}

	/**
	 * {@link #assertStored(BaseEntity)} is after storing, but not after finding again. The sample
	 * here is actually one found after loading a stored sample.
	 * 
	 * @param found
	 */
	protected void assertLoadAfterStore(E found) {
		// should be the same as after storing, unless postLoad does something
		assertStored(found);
	}

	/**
	 * The BaseEntity doesn't know how to mutate BaseEntity extensions so it is left up to the
	 * extended Test.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 *             when the given id isn't found in the repository.
	 */
	protected E mutate(IdentifierT id) throws EntityNotFoundException {
		return repository.load(id);
	}

	/** Should be overridden by implementation to provide a specific repository of choice. */
	abstract protected R createRepository();

	/**
	 * Provides the appropriate repository to be used for testing the {@link BaseEntity}.
	 * 
	 * Override this if you don't want the default Mock (which should be all Objectify tests.
	 * 
	 * @return
	 */

	final protected R getRepository() {
		return this.repository;
	}

	/**
	 * Optional method for implementing class to override and test the built class after mutation.
	 * 
	 * @param mutated
	 */
	protected void assertMutation(E mutated) {
		assertSample(mutated);

	}

	/**
	 * Provide the builder for the entity allowing the base class to assign common values.
	 */
	final public B getEntityBuilder() {
		try {
			return reflector.constructDefaultBuilder(builderClass);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Provide a Builder that will allow mutations. The implementing class should pass the original
	 * into it's builder for mutation.
	 * 
	 * If the implementing class wishes to test mutation this should be done to the builder before
	 * returning.
	 * 
	 * @param original
	 * @return
	 */
	final public B getMutationBuilder(E original) {
		try {
			return reflector.constructMutationBuilder(builderClass, original);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns the dictionary associated with {@link #entityClass}.
	 * 
	 * @return
	 */
	public FieldDictionary getFieldDictionary() {
		return getFieldDictionaryFactory().getDictionary(entityClass);
	}

	/**
	 * PRovides a {@link FieldDictionaryFactory} registered with the {@link #entityClass} which must
	 * be a {@link Dictionary}.
	 * 
	 * @return
	 */
	public FieldAnnotationDictionaryFactory getFieldDictionaryFactory() {
		FieldAnnotationDictionaryFactory annotationFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		annotationFactory.getDictionary(entityClass);
		return annotationFactory;
	}

	/**
	 * Tests that each entity declares FieldKeys for use in queries using the standard
	 * Entity.FIELD.KEY static inner classes.
	 * 
	 * Each class is tested in isolation because it's annotations provide the path for the factory
	 * to recursively discover all dependents.
	 * 
	 * Should you not need to declare fields or have a reason not to follow the standard override
	 * this method and do nothing and explain why such an override was made.
	 * 
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@Test
	@Ignore("Generating field keys now so these are being removed")
	public void testFieldKeys() throws IllegalArgumentException, IllegalAccessException {

		FieldAnnotationTestUtil.assertFieldsAvailable(this.entityClass);

	}

	@Test
	public void testSerializable() {
		byte[] serialize = SerializationUtils.serialize(getEntitySample());
		E actual = (E) SerializationUtils.deserialize(serialize);
		assertCreation(actual);
	}

}
