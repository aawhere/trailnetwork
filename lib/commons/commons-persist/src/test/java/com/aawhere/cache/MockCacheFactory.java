/**
 *
 */
package com.aawhere.cache;

import java.util.Map;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;

public class MockCacheFactory
		implements CacheFactory {

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.CacheFactory#createCache(java.util.Map)
	 */
	@Override
	public Cache createCache(Map env) throws CacheException {
		return new MockCache();
	}

}