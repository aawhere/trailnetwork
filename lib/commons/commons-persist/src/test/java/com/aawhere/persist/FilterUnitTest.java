/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.aawhere.field.FieldKey;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.aawhere.xml.bind.JAXBTestUtil.Builder;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;

/**
 * 
 * Tests {@link Filter} and it's simple pojo functions.
 * 
 * @author Brian Chapman
 * 
 */
public class FilterUnitTest {

	@Test
	public void testLimit() {
		Integer expected = 3;
		Filter filter = getBuilder().setLimit(expected).build();
		assertEquals(expected, filter.getLimit());
	}

	@Test
	public void testMinLimit() {
		Integer expected = getMinLimit();
		Integer tooSmall = 0;
		Filter filter = getBuilder().setLimit(tooSmall).build();
		assertEquals(expected, filter.getLimit());
		assertFalse("final page", filter.isFinalPage());
	}

	@Test
	public void testOptionsSplitting() {
		String option1 = TestUtil.generateRandomAlphaNumeric();
		String option2 = TestUtil.generateRandomAlphaNumeric();
		String soloOption = TestUtil.generateRandomAlphaNumeric();
		String joined = StringUtils.join(Lists.newArrayList(option1, option2), FilterUtil.LIST_SEPARATOR);
		Filter filter = Filter.create().addOption(joined).addOption(soloOption).build();
		TestUtil.assertContains(option1, filter.getOptions());
		TestUtil.assertContains(option2, filter.getOptions());
		TestUtil.assertContains(soloOption, filter.getOptions());
		TestUtil.assertSize(3, filter.getOptions());
	}

	@Test
	public void testMaxLimit() {
		Integer expected = getMaxLimit();
		Integer tooBig = expected + 1;
		Filter filter = getBuilder().setLimit(tooBig).build();
		assertEquals(expected, filter.getLimit());
	}

	@Test
	public void testParent() {
		final StringMockEntityId id = EntityTestUtil.generateRandomMockStringEntityId();

		final ArrayList<String> values = new ArrayList<String>();

		Filter.Builder builder = parentFilterBuilder(values);

		builder.setParent(id);
		Filter filter = builder.build();
		// since we are assigning the parent id directly no need for conversion
		TestUtil.assertEmpty(values);
		assertTrue("this confirms parent value is not null", filter.hasParent());

	}

	@Test
	public void testParentValue() {
		final StringMockEntityId id = EntityTestUtil.generateRandomMockStringEntityId();
		final ArrayList<String> values = new ArrayList<String>();
		Filter.Builder builder = parentFilterBuilder(values);

		builder.setParentValue(id.getValue());
		Filter filter = builder.build();
		assertTrue("the string value was assigned by us", filter.hasParent());
		// since we are assigning the parent id directly no need for conversion
		TestUtil.assertContains(id.getValue(), values);
		assertEquals(id, filter.getParent());

	}

	/**
	 * Used for testing this will assign any values to the mutable list given.
	 * 
	 * @param values
	 * @return
	 */
	private Filter.Builder parentFilterBuilder(final ArrayList<String> values) {
		Filter.Builder builder = new Filter.Builder() {
			@Override
			protected StringMockEntityId convertParent(String parentIdValue) {
				values.add(parentIdValue);
				return new StringMockEntityId(parentIdValue);
			}
		};
		return builder;
	}

	/** Ensures that ordering by none will clear order filters . */
	@Test
	public void testOrderByNone() {
		Filter filter = getBuilder().setOrderByNone().orderBy(LongMockEntity.FIELD.KEY.COUNT).build();
		assertFalse(filter.hasSortOrder());
	}

	/**
	 * Tests when the client puts a string of none into the field that the build detects and handles
	 * the none call.
	 * 
	 */
	@Test
	public void testOrderByNoneString() {
		Filter filter = getBuilder().orderBy(Filter.ORDER_BY_NONE).orderBy(LongMockEntity.FIELD.KEY.COUNT).build();
		TestUtil.assertEmpty(filter.getOrderBy());
		assertFalse(filter.hasSortOrder());
	}

	@Test
	public void testOrderBy() {
		final FieldKey key = LongMockEntity.FIELD.KEY.COUNT;
		Filter filter = getBuilder().orderBy(key).build();
		assertTrue(filter.hasSortOrder());
		TestUtil.assertContains(filter.toString(), key.getAbsoluteKey());
	}

	@Test
	public void testContainsOption() {
		String option = TestUtil.generateRandomAlphaNumeric();
		Filter filter = getBuilder().addOption(option).build();
		final String message = String.valueOf(filter.getOptions());
		assertTrue(message, filter.containsOption(option));
		assertFalse(message, filter.containsOption(TestUtil.generateRandomAlphaNumeric()));
	}

	@Test
	public void testDefaultLimit() {
		assertNotNull(getBuilder().build().getLimit());
	}

	@Test
	public void testSetIfDefaultLimit() {
		Integer limit = 88;
		assertEquals(limit, getBuilder().setLimitIfDefault(limit).build().getLimit());
	}

	@Test
	public void testSetIfDefaultLimitAlreadySet() {
		Integer limit = 88;
		Integer secondRequest = 77;
		com.aawhere.persist.Filter.Builder builder = getBuilder().setLimitIfDefault(limit);
		builder.setLimitIfDefault(secondRequest);
		assertEquals(limit, builder.build().getLimit());
	}

	@Test
	public void testNoLimit() {
		Filter filter = getBuilder().setNoLimit().build();
		assertNull(filter.getLimit());
		assertFalse(filter.hasLimit());

	}

	/** TN-558 */
	@Test
	public void testNoLimitAfterLimitIsSet() {
		Integer limit = 30;
		Filter filter = getBuilder().setLimit(limit).build();
		assertEquals(limit, filter.getLimit());
		assertTrue(filter.hasLimit());
		Filter mutated = Filter.mutate(filter).setNoLimit().build();
		assertNull(mutated.getLimit());
		assertFalse(filter.hasLimit());
	}

	@Test
	public void testChunkSize() {
		Integer expected = 560;
		Filter filter = getBuilder().setChunkSize(expected).build();
		assertEquals(expected, filter.getChunkSize());
	}

	@Test
	public void testDefaultPage() {
		Integer limit = 10;
		Filter filter = getBuilder().setLimit(limit).build();
		assertEquals(Integer.valueOf(1), filter.getPage());
		assertEquals(1, filter.getStart().intValue());
		assertEquals(limit, filter.getEnd());
		assertNull(filter.getPageTotal());
		assertFalse("final page", filter.isFinalPage());

	}

	@Test(expected = InvalidArgumentException.class)
	public void testMaxConditionBreech() {
		FilterCondition<?> condition = FilterConditionUnitTest.create().condition;
		Filter.Builder builder = Filter.create();
		for (int i = 0; i <= Filter.BaseBuilder.MAX_CONDITIONS; i++) {
			builder.addCondition(condition);
		}
		fail("limit should have been enforced");
	}

	@Test
	public void testContainsCondition() {
		String key = "my key";
		Filter filter = getBuilder().build();
		assertFalse("not yet added", filter.containsCondition(key));
		FilterCondition<Object> condition = FilterCondition.create().field(key).operator(FilterOperator.EQUALS)
				.value("junk").build();
		filter = getBuilder().addCondition(condition).build();
		assertTrue("already added", filter.containsCondition(key));
	}

	@Test
	public void testPage2() {
		Integer expectedPage = 2;
		Integer limit = 10;
		Integer expectedStart = 11;
		Integer expectedEnd = 20;
		Filter filter = getBuilder().setLimit(limit).setPage(expectedPage).build();
		assertEquals(expectedPage, filter.getPage());
		assertEquals(expectedStart, filter.getStart());
		assertEquals(expectedEnd, filter.getEnd());
		assertFalse("final page", filter.isFinalPage());
	}

	/**
	 * Default construction is used by frameworks like JAX-RS and persistence. Test the availability
	 * and health
	 * 
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 */
	@Test
	public void testDefaultConstructor() throws InstantiationException, IllegalAccessException, SecurityException,
			NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
		Filter Filter = getBuilder().build();
		Constructor<? extends Filter> constructor = Filter.getClass().getDeclaredConstructor();
		constructor.setAccessible(true);
		Filter filter = constructor.newInstance();
		assertNotNull(filter.getConditions());
		assertNotNull(filter.getPage());
		assertNotNull(filter.getStart());
		assertNotNull(filter.getEnd());
		assertFalse("final page", filter.isFinalPage());
	}

	@Test
	public void testFinalPageWithPredicates() {
		assertTrue(	"zero results means final",
					getBuilder().setPageTotal(0).filteredByPredicates(0).addPredicate(Predicates.alwaysTrue()).build()
							.isFinalPage());
		assertTrue(	"filtered by predicates is zero so final page, YUP!",
					getBuilder().setPageTotal(1).addPredicate(Predicates.alwaysTrue()).filteredByPredicates(0).build()
							.isFinalPage());
		assertFalse("total of all is the limit so we can't know if this is the final", getBuilder().setLimit(2)
				.setPageTotal(1).addPredicate(Predicates.alwaysTrue()).filteredByPredicates(1).build().isFinalPage());
	}

	@Test
	public void testPageEndForPredicates() {
		assertEquals("end page should be zero since nothing found", 0, getBuilder().setPageTotal(0)
				.filteredByPredicates(0).addPredicate(Predicates.alwaysTrue()).build().getEnd().intValue());
		assertEquals("filtered by predicates is zero so final page, YUP!", 1, getBuilder().setPageTotal(1)
				.addPredicate(Predicates.alwaysTrue()).filteredByPredicates(0).build().getEnd().intValue());
		assertEquals("should be include the predicate filtered", 2, getBuilder().setLimit(2).setPageTotal(1)
				.addPredicate(Predicates.alwaysTrue()).filteredByPredicates(1).build().getEnd().intValue());
	}

	@Test
	public void testPageTotal() {
		Integer expected = 7;
		Filter filter = getBuilder().setPageTotal(expected).build();
		assertEquals(expected, filter.getPageTotal());
		assertEquals(expected, filter.getEnd());
		assertTrue("final page", filter.isFinalPage());
	}

	/**
	 * When the pageTotal is less than the end should match the total found, not the potential limit
	 * that wasn't returned.
	 * 
	 */
	@Test
	public void testEndWhenPageTotalLessThanLimit() {
		Integer limit = 5;
		Integer pageTotal = 3;
		Integer expectedStart = 1;
		Integer expectedEnd = 3;
		Filter filter = getBuilder().setLimit(limit).setPageTotal(pageTotal).build();
		assertEquals("pageTotal", pageTotal, filter.getPageTotal());
		assertEquals("limit", limit, filter.getLimit());
		assertEquals("start", expectedStart, filter.getStart());
		assertEquals("end", expectedEnd, filter.getEnd());
		assertTrue("final page", filter.isFinalPage());
	}

	@Test
	public void testMutation() {
		Integer limit = 11;
		Filter filter = getBuilder().setLimit(limit).build();
		assertEquals(limit, filter.getLimit());
		Filter mutatedFilter = getMutatedBuilder(filter).build();
		assertEquals(limit, mutatedFilter.getLimit());
		assertSame(filter, mutatedFilter);
		// now, make sure the max will hold
		mutatedFilter = getMutatedBuilder(filter).setLimitToMax().build();
		Integer maxLimit = Filter.getMaxLimit();
		assertEquals(maxLimit, mutatedFilter.getLimit());

		// test the edge case for setting page total
		mutatedFilter = getMutatedBuilder(mutatedFilter).setPageTotal(maxLimit).build();
		assertSame(filter, mutatedFilter);
		assertEquals("setting page total caused limit to reset", maxLimit, mutatedFilter.getLimit());
		// TODO:extend this

	}

	@Test
	public void testNotEqualToId() {

		// notice, there is no restriction as to the type of id here. Generics
		// needed
		StringMockEntityId expected = EntityTestUtil.generateRandomMockStringEntityId();
		Filter filter = getBuilder().idNotEqualTo(expected).build();
		List<FilterCondition> conditions = filter.getConditions();
		assertEquals(conditions.toString(), 1, conditions.size());
		FilterCondition<?> condition = conditions.get(0);
		assertEquals(expected, condition.getValue());
	}

	@Test(expected = RuntimeException.class)
	public void testPageTotalExceedsLimit() {
		Integer expected = getMaxLimit() + 1;
		getBuilder().setPageTotal(expected).build();
		fail("should report a failure since a programmer is likely not managing the limit and page total correctly");
	}

	@SuppressWarnings("unchecked")
	public Set<XmlAdapter<?, ?>> getXmlAdapters() {
		return Collections.EMPTY_SET;
	}

	@Test
	public void testJaxBRoundTrip() {
		Filter filter = getSampleFilter();
		Builder<Filter> jaxbUtilBuilder = new JAXBTestUtil.Builder<Filter>();
		jaxbUtilBuilder.adapter(getXmlAdapters());
		JAXBTestUtil<Filter> jaxBTestUtil = jaxbUtilBuilder.element(filter).build();
		Filter actual = jaxBTestUtil.getActual();
		assertNotNull("actual", actual);
		// equality disabled while value is not being transported for FilterCondition
		// assertEquals(filter, actual);

	}

	@Test
	public void testCityCoordinatesAutoLocateNoBoundsProvided() {
		GeoCoordinate coordinates = MeasureTestUtil.createRandomGeoCoordinate();
		Filter.Builder autoLocateFilter = autoLocateFilter(coordinates);
		Filter filter = autoLocateFilter.build();
		assertTrue("bounds should be auto-populated", filter.hasBounds());
		assertTrue("auto locate is on ", filter.isAutoLocate());
		assertEquals("city coordinate is different", coordinates, filter.getCityCoordinate());
	}

	private com.aawhere.persist.Filter.Builder autoLocateFilter(GeoCoordinate coordinates) {
		Filter.Builder autoLocateFilter = Filter.create().setCityCoordinates(coordinates.toString())
				.setAutoLocate(true);
		return autoLocateFilter;
	}

	@Test
	public void testCityCoordinatesAutoLocateBoundsProvided() {
		BoundingBox expectedBox = BoundingBoxTestUtils.createRandomBoundingBox();
		Filter filter = autoLocateFilter().setBounds(expectedBox.getBounds()).build();
		assertEquals(expectedBox, BoundingBox.valueOf(filter.getBounds()));
	}

	private com.aawhere.persist.Filter.Builder autoLocateFilter() {
		return autoLocateFilter(MeasureTestUtil.createRandomGeoCoordinate());
	}

	/**
	 * In this case the city coordinates are provided (which is likely to happen with every
	 * request). The end result is no bounds is available.
	 * 
	 */
	@Test
	public void testCityCoordinatesNoAutoLocate() {
		Filter filter = autoLocateFilter().setAutoLocate(false).build();
		assertFalse(filter.isAutoLocate());
		assertFalse(filter.hasBounds());
	}

	public Filter getSampleFilter() {
		return getBuilder().build();
	}

	public Filter.Builder getBuilder() {
		return Filter.create();
	}

	public Filter.Builder getMutatedBuilder(Filter original) {
		return Filter.mutate(original);
	}

	/**
	 * return the result of BaseFilter#getMinLimit, a static method.
	 */
	public Integer getMinLimit() {
		return Filter.getMinLimit();
	}

	/**
	 * return the result of BaseFilter#getMaxLimit, a static method
	 */
	public Integer getMaxLimit() {
		return Filter.getMaxLimit();
	}

	@Test
	public void testClone() {

		Integer original = 882;
		Filter filter = getBuilder().setLimit(original).build();
		assertEquals("proves limit is working", original, filter.getLimit());
		Integer clonedLimit = 292;
		Filter cloned = Filter.clone(filter).setLimit(clonedLimit).build();
		assertEquals("clone mutation should have held", clonedLimit, cloned.getLimit());
		assertEquals("original limit should have remained unchanged", original, filter.getLimit());
	}

}
