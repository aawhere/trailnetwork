/**
 *
 */
package com.aawhere.persist;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.xml.XmlNamespace;

/**
 * Mock implementation of {@link BaseEntities}
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class LongMockFilterEntities
		extends BaseFilterEntities<LongMockEntity> {

	public static class Builder
			extends BaseBuilder<LongMockFilterEntities, Builder> {

		public Builder() {
			super(new LongMockFilterEntities());
		}
	}

	public static class BaseBuilder<FilterEntitiesT extends LongMockFilterEntities, BuilderT extends BaseBuilder<FilterEntitiesT, BuilderT>>
			extends BaseFilterEntities.Builder<BuilderT, LongMockEntity, FilterEntitiesT> {

		/**
		 * @param entities
		 * @param collection
		 */
		public BaseBuilder(FilterEntitiesT building) {
			super(building);
		}

	}

	@XmlElement(name = "mock")
	@Override
	public List<LongMockEntity> getCollection() {
		return super.getCollection();
	}

	public static Builder create() {
		return new Builder();
	}

}
