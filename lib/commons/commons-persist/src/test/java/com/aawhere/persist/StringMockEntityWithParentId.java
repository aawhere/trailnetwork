/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.IdentifierWithParent;
import com.aawhere.id.StringIdentifierWithParent;

/**
 * @author aroller
 * 
 */
public class StringMockEntityWithParentId
		extends StringIdentifierWithParent<StringMockEntityWithParent, StringMockEntityWithParentId> {

	private static final long serialVersionUID = 7302609950027837012L;

	public StringMockEntityWithParentId() {
		super(StringMockEntityWithParent.class);
	}

	public StringMockEntityWithParentId(String value) {
		this(delegateBuilder().compositeValue(value).build());
	}

	private StringMockEntityWithParentId(IdentifierWithParent<String, StringMockEntityWithParentId> delegate) {
		super(delegate);
	}

	@Deprecated
	public StringMockEntityWithParentId(StringMockEntityWithParentId parent, String value) {
		this(delegateBuilder().value(value).parent(parent).build());
	}

	public StringMockEntityWithParentId(String value, StringMockEntityWithParentId parent) {
		this(delegateBuilder().value(value).parent(parent).build());
	}

	@Deprecated
	public StringMockEntityWithParentId(String parentValue, String value) {
		this(new StringMockEntityWithParentId(parentValue), value);
	}

	private static IdentifierWithParent.Builder<String, StringMockEntityWithParentId> delegateBuilder() {
		return delegateBuilder(StringMockEntityWithParentId.class).kind(StringMockEntityWithParent.class)
				.parentNotRequired();
	}
}
