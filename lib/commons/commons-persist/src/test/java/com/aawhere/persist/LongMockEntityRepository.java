/**
 *
 */
package com.aawhere.persist;

/**
 * Simple test repository for the Test entity {@link LongMockEntity}.
 * 
 * @author Aaron Roller
 * 
 */
public interface LongMockEntityRepository
		extends Repository<LongMockEntity, LongMockEntityId>,
		FilterRepository<LongMockFilterEntities, LongMockEntity, LongMockEntityId> {

	/**
	 * Updates the {@link LongMockEntity#getName()} in the repository.
	 * 
	 * @param name
	 * @return
	 * @throws EntityNotFoundException
	 */
	public LongMockEntity updateName(LongMockEntityId id, String name) throws EntityNotFoundException;
}
