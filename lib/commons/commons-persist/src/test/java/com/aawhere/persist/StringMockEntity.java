package com.aawhere.persist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.AnnotatedDictionary;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.DictionaryMutator;
import com.aawhere.field.annotation.FieldMutator;
import com.aawhere.field.annotation.MutableAnnotatedDictionary;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.search.Searchable;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Searchable(fieldKeys = { StringMockEntity.FIELD.NAME })
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = StringMockEntity.DOMAIN_KEY)
public class StringMockEntity
		extends StringBaseEntity<StringMockEntity, StringMockEntityId>
		implements AnnotatedDictionary, DateUpdatedFilterable {

	private static final long serialVersionUID = 7922332880013120189L;
	public static final String NAME_KEY = FIELD.NAME;
	public static final String DOMAIN_KEY = FIELD.DOMAIN;

	/**
	 * Used to construct all instances of MockEntity.
	 */
	@DictionaryMutator(domain = DOMAIN_KEY)
	public static class Builder
			extends StringBaseEntity.Builder<StringMockEntity, Builder, StringMockEntityId>
			implements MutableAnnotatedDictionary {

		public Builder() {
			this(new StringMockEntity());
		}

		Builder(StringMockEntity original) {
			super(original);
		}

		@FieldMutator(key = NAME_KEY)
		public Builder setName(String name) {
			building.name = name;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct MockEntity */
	private StringMockEntity() {
	}

	@Index
	@XmlElement
	private String name;

	@com.aawhere.field.annotation.Field(key = NAME_KEY, indexedFor = "ObjectifyFilterQueryWriterUnitTest",
			xmlAdapter = ToStringXmlAdapter.class)
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@Override
	@com.aawhere.field.annotation.Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM,
			indexedFor = { "Repository.load()" })
	public StringMockEntityId getId() {
		return super.getId();
	}

	public static class FIELD {
		public static final String DOMAIN = "stringMock";
		public static final String NAME = "name";

		public static class KEY {
			public static final FieldKey NAME = new FieldKey(FIELD.DOMAIN, FIELD.NAME);
		}
	}

}
