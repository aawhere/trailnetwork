/**
 *
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;

/**
 * @author Brian Chapman
 * 
 */
public interface HasFilterTestHelpers<EntityT extends BaseEntity<EntityT, IdentifierT>, EntitiesT extends BaseFilterEntities<EntityT>, BuilderT extends Filter.BaseBuilder<Filter, BuilderT>, IdentifierT extends Identifier<?, EntityT>> {

	public BuilderT getFilterBuilder();

	public BuilderT getFilterBuilder(Filter filter);

	public EntityT getEntitySample();

	public FilterRepository<EntitiesT, EntityT, IdentifierT> getFilterRepository();

	public Repository<EntityT, IdentifierT> getRepository();

	public void assertFilter(Filter filter, EntitiesT entities);

	public EntityT getUniqueEntitySample();

}
