/**
 *
 */
package com.aawhere.persist;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.id.StringIdentifier;
import com.aawhere.persist.EntityTestUtil.StringMockEntityService;
import com.aawhere.xml.XmlNamespace;

/**
 * @author brian
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@ServicedBy(StringMockEntityService.class)
public class StringMockEntityId
		extends StringIdentifier<StringMockEntity> {

	private static final long serialVersionUID = -5034835785846154685L;

	/**
	 * @param kind
	 */
	StringMockEntityId() {
		super(StringMockEntity.class);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public StringMockEntityId(String value) {
		super(value, StringMockEntity.class);
	}

}