/**
 * 
 */
package com.aawhere.persist.objectify;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class IfTestingUnitTest {

	@Test
	public void testTrue() {
		IfTesting ifTesting = new IfTesting();
		assertTrue("we are always testing", ifTesting.matchesValue("junk"));
	}
}
