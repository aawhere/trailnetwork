package com.aawhere.persist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.aawhere.field.annotation.AnnotatedDictionary;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.DictionaryMutator;
import com.aawhere.field.annotation.FieldMutator;
import com.aawhere.field.annotation.MutableAnnotatedDictionary;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = LongMockEntityWithParent.DOMAIN_KEY)
public class LongMockEntityWithParent
		extends LongBaseEntity<LongMockEntityWithParent, LongMockEntityWithParentId>
		implements AnnotatedDictionary {

	private static final long serialVersionUID = -520650829690116133L;
	public static final String NAME_KEY = "name";
	public static final String DOMAIN_KEY = "longMockEntityWithParent";

	@Index
	@XmlElement
	private String name;

	public String getName() {
		return name;
	}

	/**
	 * Used to construct all instances of MockEntity.
	 */
	@DictionaryMutator(domain = DOMAIN_KEY)
	public static class Builder
			extends LongBaseEntity.Builder<LongMockEntityWithParent, Builder, LongMockEntityWithParentId>
			implements MutableAnnotatedDictionary {

		public Builder() {
			this(new LongMockEntityWithParent());
		}

		Builder(LongMockEntityWithParent original) {
			super(original, LongMockEntityWithParentId.class);
		}

		@FieldMutator(key = NAME_KEY)
		public Builder setName(String name) {
			building.name = name;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct MockEntity */
	private LongMockEntityWithParent() {
	}

	public static Builder create() {
		return new Builder();
	}

}
