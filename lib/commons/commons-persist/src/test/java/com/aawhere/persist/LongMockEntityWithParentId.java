/**
 *
 */
package com.aawhere.persist;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.id.LongIdentifierWithParent;
import com.aawhere.xml.XmlNamespace;

/**
 * @author brian
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class LongMockEntityWithParentId
		extends LongIdentifierWithParent<LongMockEntityWithParent, LongMockEntityId> {

	/**
	 * 
	 */
	private static final Class<LongMockEntityWithParent> KIND = LongMockEntityWithParent.class;
	private static final long serialVersionUID = -856938949937907377L;

	/**
	 * @param kind
	 */
	LongMockEntityWithParentId() {
		super(KIND);
	}

	/**
	 * @deprecated encourage {@link #LongMockEntityWithParentId(Long, LongMockEntityId)} for
	 *             consistency with parents and the trend of the value is always needed with
	 *             sometimes extra stuff
	 * @param value
	 * @param kind
	 */
	@Deprecated
	public LongMockEntityWithParentId(LongMockEntityId parent, Long value) {
		super(value, parent, KIND);
	}

	public LongMockEntityWithParentId(Long value, LongMockEntityId parent) {
		super(value, parent, KIND);
	}

	/**
	 * @deprecated too ambiguous. Build the parent id yourself.
	 * @param parent
	 * @param value
	 */
	@Deprecated
	public LongMockEntityWithParentId(Long parent, Long value) {
		super(value, new LongMockEntityId(parent), KIND);
	}

	public LongMockEntityWithParentId(LongMockEntityId parent, LongMockEntityId id) {
		super(id.getValue(), parent, KIND);
	}

	/**
	 * Provided to any client that may have a Long formatted as a String.
	 * 
	 * @param value
	 * @param kind
	 */
	public LongMockEntityWithParentId(String value) {
		super(value, KIND, LongMockEntityId.class);
	}

}