/**
 *
 */
package com.aawhere.cache;

/**
 * @author brian
 * 
 */
public class CacheTestUtil {

	// Copied from CacheManager since it is private there.
	public static final String FACTORY_PROPERTY_NAME = "net.sf.jsr107cache.CacheFactory";

	public static void setUpCachManager() {
		System.setProperty(FACTORY_PROPERTY_NAME, MockCacheFactory.class.getCanonicalName());
	}

	public static CacheService cacheService() {
		setUpCachManager();
		return new CacheService();
	}

}
