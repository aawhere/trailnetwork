/**
 * 
 */
package com.aawhere.persist;

import java.util.Map;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * @author aroller
 * 
 */
abstract public class ServiceTestUtilBase<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends ServiceStandardBase<S, E, I>
		implements ServiceStandard<S, E, I> {

	/**
	 * @param idFunction
	 */
	public ServiceTestUtilBase() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceStandard#all(java.lang.Iterable)
	 */
	@Override
	public Map<I, E> all(Iterable<I> ids) {
		return IdentifierUtils.map(Iterables.transform(ids, idFunction()));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceStandard#idFunction()
	 */
	public static <S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			Function<I, E> function(final ServiceTestUtilBase<S, E, I> base) {
		return new Function<I, E>() {

			@Override
			public E apply(I input) {
				return (input != null) ? base.entity(input) : null;
			}
		};
	}

	public abstract E entity(I id);
}
