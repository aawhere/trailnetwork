/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.commons.lang3.reflect.FieldUtils;

import com.aawhere.collections.Index;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.reflect.GenericUtils;
import com.aawhere.persist.LongMockEntities.Builder;
import com.aawhere.test.TestUtil;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;

/**
 * @author Aaron Roller
 * 
 */
public class EntityTestUtil {

	public static <KindT, IdentifierT extends Identifier<?, KindT>> IdentifierT generateRandomId(
			Class<IdentifierT> identifierType) {

		List<Class<?>> genericClasses = GenericUtils.getTypeArguments(Identifier.class, identifierType);
		Class<?> valueType = genericClasses.get(Index.FIRST.index);

		if (valueType.equals(Long.class)) {
			return IdentifierUtils.idFrom(TestUtil.generateRandomId(), identifierType);
		} else if (valueType.equals(String.class)) {
			return IdentifierUtils.idFrom(TestUtil.generateRandomString(), identifierType);
		} else {
			throw new IllegalArgumentException("Can only generate ids for Identifiers of type Long or String");
		}
	}

	public static <E extends BaseEntity<E, ?>> void assertIdNull(String message, E entity) {
		assertNull(entity.getId());

	}

	public static <E extends BaseEntity<E, ?>> void assertIdAvailable(E entity) {
		assertIdAvailable(entity.getClass().getSimpleName() + " doesn't have id", entity);
	}

	public static <E extends BaseEntity<E, ?>> void assertIdAvailable(String message, E entity) {
		assertNotNull(message, entity.getId());
	}

	public static <E extends LongBaseEntity<?, I>, I extends Identifier<Long, ?>> void setLongId(E entity, I id) {
		try {
			Field field = LongBaseEntity.class.getDeclaredField(BaseEntity.BASE_FIELD.ID);
			field.setAccessible(true);
			FieldUtils.writeField(field, entity, id.getValue());
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		}
	}

	public static <E extends StringBaseEntity<?, I>, I extends Identifier<String, ?>> void setStringId(E entity, I id) {
		try {
			FieldUtils.writeDeclaredField(entity, BaseEntity.BASE_FIELD.ID, id);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public static StringMockEntityId generateRandomMockStringEntityId() {
		return generateRandomId(StringMockEntityId.class);
	}

	/**
	 * Not really persisted, but store has been colled on the repository so it will give those items
	 * modfiied for every store (entity version, date created, date updated, etc).
	 * 
	 * @see #generateRandomStringEntity()
	 * @return
	 */
	public static StringMockEntity generateRandomStringEntityStored() {
		StringMockEntityUnitTest instance = StringMockEntityUnitTest.getInstance();
		return instance.getRepository().store(instance.getEntitySample());
	}

	/**
	 *
	 */
	public static StringMockEntity generateRandomStringEntity() {
		StringMockEntityId id = generateRandomMockStringEntityId();
		return generateRandomStringEntity(id);
	}

	private static StringMockEntity generateRandomStringEntity(StringMockEntityId id) {
		return new StringMockEntity.Builder().setId(id).setName(TestUtil.generateRandomString()).build();
	}

	public static LongMockEntities entities() {
		int size = TestUtil.generateRandomInt(0, 10);
		Builder builder = new LongMockEntities.Builder();
		for (int i = 0; i < size; i++) {
			builder.add(generateRandomLongEntity());

		}
		return builder.build();
	}

	/**
	 *
	 */
	public static LongMockEntity generateRandomLongEntity() {
		final LongMockEntityId id = generateRandomMockLongEntityId();
		return generateLongEntity(id);
	}

	/**
	 * @param id
	 * @return
	 */
	private static LongMockEntity generateLongEntity(final LongMockEntityId id) {
		return new LongMockEntity.Builder().setId(id).setName(TestUtil.generateRandomString()).build();
	}

	public static LongMockEntity generateRandomLongEntityWithId() {
		LongMockEntity entity = generateRandomLongEntity();
		setLongId(entity, generateRandomMockLongEntityId());
		return entity;
	}

	public static LongMockEntityWithParentId generateRandomEntityWithParentId() {
		return new LongMockEntityWithParentId(generateRandomMockLongEntityId(), TestUtil.generateRandomId());
	}

	public static Function<LongMockEntity, Integer> countFunction() {
		return new Function<LongMockEntity, Integer>() {

			@Override
			public Integer apply(LongMockEntity input) {
				return (input != null) ? input.count : null;
			}
		};
	}

	public static Function<LongMockEntity, StringMockEntityId> relatedFunction() {
		return new Function<LongMockEntity, StringMockEntityId>() {

			@Override
			public StringMockEntityId apply(LongMockEntity input) {
				return (input != null) ? input.related : null;
			}
		};
	}

	public static Function<LongMockEntityId, LongMockEntity> longMockEntityFunction() {
		return new Function<LongMockEntityId, LongMockEntity>() {

			@Override
			public LongMockEntity apply(LongMockEntityId input) {
				return (input != null) ? generateLongEntity(input) : null;

			}
		};
	}

	public static Function<StringMockEntityId, StringMockEntity> stringMockEntityFunction() {
		return new Function<StringMockEntityId, StringMockEntity>() {

			@Override
			public StringMockEntity apply(StringMockEntityId input) {
				return (input != null) ? generateRandomStringEntity(input) : null;

			}
		};
	}

	public static ServiceStandard<LongMockFilterEntities, LongMockEntity, LongMockEntityId> service() {
		return new LongMockEntityService();
	}

	/**
	 * @return
	 */
	public static LongMockEntityId generateRandomMockLongEntityId() {

		return generateRandomId(LongMockEntityId.class);
	}

	/** Simple checks if anyone is interested in ensuring an entity as been persisted. */
	public static <E extends BaseEntity<E, ?>> void assertStored(E entity) {
		TestUtil.assertRange(Range.atLeast(1), entity.getEntityVersion());
	}

	public static class LongMockEntityService
			extends ServiceStandardBase<LongMockFilterEntities, LongMockEntity, LongMockEntityId> {
		public LongMockEntityService() {
			super(longMockEntityFunction());
		}
	}

	public static class StringMockEntityService
			extends ServiceStandardBase<StringMockFilterEntities, StringMockEntity, StringMockEntityId> {
		public StringMockEntityService() {
			super(stringMockEntityFunction());
		}
	}

	/**
	 * Iterates each filter and executes {@link FilterRepository#filter(Filter)} confirming the
	 * expected size for each.
	 * 
	 * @param expectedSize
	 * @param filters
	 * @param repository
	 */
	public static
			<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			void
			assertSizeForEach(Integer expectedSize, ImmutableList<Filter> filters, FilterRepository<S, E, I> repository) {
		for (Filter filter : filters) {
			S routes = repository.filter(filter);
			TestUtil.assertSize(filter.toString(), expectedSize, routes);
		}

	}
}
