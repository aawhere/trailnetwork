/**
 *
 */
package com.aawhere.persist;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.xml.XmlNamespace;

/**
 * Mock implementation of {@link BaseEntities}
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class LongMockEntities
		extends BaseEntities<LongMockEntity> {

	public static class Builder
			extends BaseEntities.Builder<Builder, LongMockEntity, LongMockEntities> {

		/**
		 * @param entities
		 * @param collection
		 */
		public Builder() {
			super(new LongMockEntities(), new ArrayList<LongMockEntity>());
		}
	}

	@XmlElement(name = "mock")
	@Override
	public List<LongMockEntity> getCollection() {
		return super.getCollection();
	}

}
