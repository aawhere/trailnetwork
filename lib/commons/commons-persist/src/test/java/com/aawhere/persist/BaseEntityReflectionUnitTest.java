/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.persist.LongMockEntity.Builder;

/**
 * @author Aaron Roller
 * 
 */
public class BaseEntityReflectionUnitTest {

	private BaseEntityReflection<LongMockEntity, LongMockEntity.Builder> reflector = new BaseEntityReflection<LongMockEntity, Builder>();
	private LongMockEntity entity = new LongMockEntity.Builder().build();

	@Test
	public void testBuilderClass() throws ClassNotFoundException {

		Class<LongMockEntity.Builder> builderClass = reflector.builderClassFrom(entity);
		assertEquals(LongMockEntity.Builder.class, builderClass);
	}

	@Test
	public void testMutatingBuilderConstruction() throws NoSuchMethodException, ClassNotFoundException {

		LongMockEntity.Builder builder = reflector.constructMutationBuilder(entity);
		assertNotNull(builder);
		assertSame(entity, builder.build());
	}

	@Test
	public void testDefaultBuilderConstruction() throws InstantiationException {
		LongMockEntity.Builder builder = reflector.constructDefaultBuilder(LongMockEntity.Builder.class);
		assertNotNull(builder);
	}

	@Test
	public void testDefaultBUilderConstruction() throws ClassNotFoundException, InstantiationException {
		LongMockEntity.Builder builder = reflector.constructDefaultBuilder(LongMockEntity.Builder.class);
		assertNotNull(builder);

	}

	@Test
	public void testIdentifierType() {
		StringMockEntity entity = EntityTestUtil.generateRandomStringEntity();
		Class<StringMockEntityId> actual = EntityUtil.getIdentifierType(entity);
		assertEquals(StringMockEntityId.class, actual);
	}
}
