/**
 *
 */
package com.aawhere.search;

import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseBuilder;

/**
 * The base unit test for testing implementations of {@link SearchService}. All Implementations
 * should extend this class.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class SearchServiceTest<FitlerBuilderT extends BaseBuilder<Filter, FitlerBuilderT>> {

}
