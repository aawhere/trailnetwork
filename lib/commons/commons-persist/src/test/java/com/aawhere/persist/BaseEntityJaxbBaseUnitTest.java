/**
 *
 */
package com.aawhere.persist;

import java.util.Set;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Test;

import com.aawhere.id.Identifier;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Extends {@link BaseEntityBaseUnitTest} to add JaxB roundtrip tests.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class BaseEntityJaxbBaseUnitTest<EntityT extends BaseEntity<EntityT, IdentifierT>, B extends BaseEntity.Builder<EntityT, ?, IdentifierT, B>, IdentifierT extends Identifier<?, EntityT>, R extends Repository<EntityT, IdentifierT>>
		extends BaseEntityBaseUnitTest<EntityT, B, IdentifierT, R> {

	/**
	 *
	 */
	public BaseEntityJaxbBaseUnitTest() {
	}

	/**
	 * Used to indicated the implementing test represents a class that is not mutable in the system.
	 * 
	 * @param mutableEntity
	 */
	protected BaseEntityJaxbBaseUnitTest(Boolean mutableEntity) {
		super(mutableEntity);
	}

	/**
	 * Runs the {@link #getEntitySample()} through a JAXB Marshalling/Unmarshalling cycle and
	 * verifies properties are being handled appropriately.
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testJaxbRoundTrip() throws JAXBException {
		EntityT sample = getEntitySample();
		JAXBTestUtil<EntityT> util = JAXBTestUtil.create(sample).adapter(getAdapters())
				.classesToBeBound(getClassesToBeBound()).build();

		assertJaxb(util);
	}

	/**
	 * Perform tests against the transformed entity.
	 * 
	 * @param util
	 */
	protected void assertJaxb(JAXBTestUtil<EntityT> util) {
		EntityT actual = util.getActual();

		// just re-use the existing method to ensure the sample was "re-created" appropriately
		assertCreation(actual);
		// assert that the two are indeed equal.
		assertEntityEquals(util.getExpected(), actual);
	}

	abstract public Set<XmlAdapter<?, ?>> getAdapters();

	/**
	 * Override if you need to add classes to the JaxB context
	 * 
	 * @return
	 */
	protected Class<?>[] getClassesToBeBound() {
		return new Class[0];
	}
}
