/**
 *
 */
package com.aawhere.persist;

/**
 * Simple test repository for the Test entity {@link LongMockEntity}.
 * 
 * @author Aaron Roller
 * 
 */
public interface StringMockEntityRepository
		extends Repository<StringMockEntity, StringMockEntityId> {

	/**
	 * Updates the {@link LongMockEntity#getName()} in the repository.
	 * 
	 * @param name
	 * @return
	 * @throws EntityNotFoundException
	 */
	public StringMockEntity updateName(StringMockEntityId id, String name) throws EntityNotFoundException;
}
