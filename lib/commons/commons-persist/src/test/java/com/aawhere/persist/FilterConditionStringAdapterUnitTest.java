/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldKeyUnitTest;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationExample;
import com.aawhere.field.annotation.FieldAnnotationExample.Animal;
import com.aawhere.field.annotation.FieldAnnotationExample.Animal.HairColor;
import com.aawhere.field.annotation.FieldAnnotationExample.Tree;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.string.StringAdapterFactory;
import com.aawhere.lang.string.StringFormatException;
import com.aawhere.test.TestUtil;

/**
 * Tests {@link FilterConditionStringAdapter}.
 * 
 * @author aroller
 * 
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class FilterConditionStringAdapterUnitTest {

	FieldAnnotationDictionaryFactory annoationFactory;
	FieldDictionary dictionary;
	FieldKey key;
	FilterCondition<Integer> absoluteCondition;
	FilterCondition<Integer> relativeCondition;
	FilterCondition<HairColor> hairColorEnumCondition;
	FilterCondition<String> hairColorStringCondition;

	@Before
	public void setUp() {
		annoationFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		dictionary = annoationFactory.getDictionary(Animal.class);
		key = dictionary.getDefinition(FieldAnnotationExample.LEGS_KEY).getKey();
		absoluteCondition = new FilterCondition.Builder<Integer>().field(key.toString())
				.operator(FilterOperator.EQUALS).value(323).build();
		relativeCondition = new FilterCondition.Builder<Integer>().field(key.getRelativeKey())
				.operator(FilterOperator.EQUALS).value(5).build();
		FieldKey hairColorKey = dictionary.getDefinition(FieldAnnotationExample.HAIR_COLOR_KEY).getKey();
		hairColorEnumCondition = new FilterCondition.Builder<FieldAnnotationExample.Animal.HairColor>()
				.field(hairColorKey).operator(FilterOperator.EQUALS).value(HairColor.BLACK).build();
		hairColorStringCondition = new FilterCondition.Builder<String>().field(hairColorKey)
				.operator(FilterOperator.IN).value("BLACK").build();
	}

	@Test
	public void testInteger() throws BaseException {

		FilterConditionStringAdapter<Integer> adapter = getFieldConditionStringAdapter();

		String expression = adapter.marshal(relativeCondition);
		FilterCondition<Integer> actual = adapter.unmarshal(expression,
															(Class<FilterCondition<Integer>>) relativeCondition
																	.getClass());
		assertEquals(relativeCondition.getValue(), actual.getValue());
	}

	public StringAdapterFactory getFactory() {
		StringAdapterFactory factory = new StringAdapterFactory();
		factory.registerStandardAdapters();
		return factory;
	}

	@Test
	public void testNullStringUnmarshal() throws BaseException {
		FilterConditionStringAdapter<Integer> adapter = getFieldConditionStringAdapter();
		FilterCondition<Object> condition = FilterCondition.create().field(FieldAnnotationExample.LEGS_KEY)
				.operator(FilterOperator.EQUALS).value(FilterConditionStringAdapter.NULL_STRING).build();
		String conditionString = adapter.marshal(condition);
		TestUtil.assertContains(conditionString, FilterConditionStringAdapter.NULL_STRING);
		FilterCondition<Integer> filterConditionUnmarshaled = adapter
				.unmarshal(conditionString, (Class<FilterCondition<Integer>>) condition.getClass());
		assertNull(filterConditionUnmarshaled.getValue());
	}

	@Test
	public void testNoRelativeDictionary() throws BaseException {
		FilterConditionStringAdapter<Integer> adapter = getFieldConditionStringAdapter();
		assertNotNull(adapter.marshal(absoluteCondition));
	}

	@Test
	public void testAbsoluteKey() throws BaseException {

		FilterConditionStringAdapter<Integer> adapter = getFieldConditionStringAdapter();
		FilterCondition<Integer> actual = adapter.unmarshal(adapter.marshal(absoluteCondition),
															(Class<FilterCondition<Integer>>) absoluteCondition
																	.getClass());
		assertEquals(absoluteCondition.getValue(), actual.getValue());
	}

	@Test
	public void testKeyNotFound() throws BaseException {
		FilterCondition<Integer> condition = new FilterCondition.Builder<Integer>().field(FieldKeyUnitTest.VALID_FULL)
				.operator(FilterOperator.EQUALS).value(44).build();
		try {
			getFieldConditionStringAdapter().marshal(condition);
			fail("expecting field not found exception");
		} catch (ToRuntimeException e) {
			TestUtil.assertInstanceOf(FieldNotRegisteredException.class, e.getCause());
		}
	}

	@Test
	public void testConditionNoValue() throws BaseException {
		String conditionString = "junk != ";
		FilterConditionStringAdapter adapter = getFieldConditionStringAdapter();
		try {

			FilterCondition<Integer> actual = adapter.unmarshal(conditionString, FilterCondition.class);
			fail("no value given, but provided: [" + actual.getValue() + "]");
		} catch (StringFormatException e) {
			assertEquals(PersistMessage.FILTER_CONDITION_MISSING_VALUE.name(), e.getErrorCode());
		}
	}

	/** Demonstrates the ability to handle collections of items that have adapters. */

	@Test
	public void testSet() throws BaseException {
		Set<HairColor> expected = new HashSet<FieldAnnotationExample.Animal.HairColor>();
		assertCollection(expected);

	}

	@Test
	public void testList() throws BaseException {
		List<HairColor> expected = new ArrayList<FieldAnnotationExample.Animal.HairColor>();
		assertCollection(expected);

	}

	@Test
	public void testQueue() throws BaseException {
		Queue<HairColor> expected = new ConcurrentLinkedQueue<FieldAnnotationExample.Animal.HairColor>();
		assertCollection(expected);

	}

	/**
	 * @param expected
	 * @throws BaseException
	 */
	private void assertCollection(Collection<HairColor> expected) throws BaseException {
		expected.add(HairColor.BROWN);
		expected.add(HairColor.BLACK);
		FieldKey key = FieldAnnotationExample.HAIR_COLORS_FIELD_KEY;
		FilterCondition<Collection<HairColor>> condition = new FilterCondition.Builder<Collection<HairColor>>()
				.field(key).operator(FilterOperator.EQUALS).value(expected).build();

		FilterConditionStringAdapter adapter = getFieldConditionStringAdapter();
		String string = adapter.marshal(condition);
		FilterCondition<Collection<HairColor>> actual = adapter.unmarshal(string, expected.getClass());
		TestUtil.assertCollectionEquals(expected, actual.getValue());
	}

	/**
	 * Demonstrates how a collection of objects may be encoded into a string for conditons such as
	 * {@link FilterOperator#IN} conditions.
	 */
	@Test
	public void testCollectionForIntegerField() {
		FilterConditionStringAdapter<Integer> adapter = getFieldConditionStringAdapter();
		Set<Integer> set = new HashSet<Integer>();
		FilterCondition condition = new FilterCondition.Builder<Set<Integer>>()
				.field(FieldAnnotationExample.HAIR_COLOR_FIELD_KEY).operator(FilterOperator.EQUALS).value(set).build();
		assertTrue(adapter.handles(condition));

	}

	@Test(expected = UnsupportedFilterOperatorException.class)
	@Ignore("WW-237 - making dataType optional, moving validation to Search system. Can delete after WW-237 is closed")
	public void testUnsupportedOperation() throws BaseException {
		FilterConditionStringAdapter<String> adapter = getFieldConditionStringAdapter();
		adapter.unmarshal(FieldAnnotationExample.HAIR_COLOR_KEY + " > red", FilterCondition.class);
	}

	@Test(expected = InvalidChoiceException.class)
	public void testBadOperator() throws BaseException {
		FilterConditionStringAdapter<String> adapter = getFieldConditionStringAdapter();
		adapter.unmarshal(FieldAnnotationExample.HAIR_COLOR_KEY + " =! red", FilterCondition.class);
	}

	/**
	 * Instead of using the standard syntax of the operator (=,>,<, etc), you may use the name of
	 * the enum which is helpful for bypassing escaping.
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testNamedOperator() throws BaseException {
		FilterConditionStringAdapter<String> adapter = getFieldConditionStringAdapter();
		FilterOperator expectedOperator = FilterOperator.EQUALS;
		HairColor expectedColor = HairColor.BLACK;
		final String expression = FieldAnnotationExample.HAIR_COLOR_KEY + " " + expectedOperator.name() + " "
				+ expectedColor;
		FilterCondition filterCondition = adapter.unmarshal(expression, FilterCondition.class);
		assertEquals("wrong operator found", expectedOperator, filterCondition.getOperator());
	}

	@Test
	public void testSupportedValueType() throws BaseException {
		// Singleton
		FilterConditionSupportedTypeRegistry registry = FilterConditionSupportedTypeRegistry.create().build();
		registry.register(HairColor.class, FilterOperator.IN, String.class);

		FilterConditionStringAdapter<?> adapter = getFieldConditionStringAdapter(registry);
		FilterCondition<HairColor> actualHairColor = adapter.unmarshal(	adapter.marshal(hairColorEnumCondition),
																		hairColorEnumCondition.getClass());
		FilterCondition<String> actualString = adapter.unmarshal(	adapter.marshal(hairColorStringCondition),
																	hairColorStringCondition.getClass());

		assertEquals(actualHairColor.getValue().getClass(), HairColor.class);
		assertEquals(actualString.getValue().getClass(), String.class);
	}

	/**
	 * In operators often provide a list of values to query against a value that may not be a list.
	 * This verifies the field definition will be used to try to parse string in a list format if IN
	 * is provided.
	 * 
	 * I'm not sure if the intention to register was to handle this, but it shouldn't be necessary
	 * to register for such a common thing as providing a list of FromString capable tokens. AR
	 * 2014.04.30
	 * 
	 * @throws BaseException
	 */
	@Test
	public void testEnumListForInOperator() throws BaseException {
		// Singleton
		final String separator = " ";
		assertFromStringList(",", HairColor.values());
		assertFromStringList(separator, HairColor.values());

	}

	@Test(expected = InvalidChoiceException.class)
	public void testEnumListForInOperatorBadString() throws BaseException {
		assertFromStringList("x", HairColor.values());
	}

	@Test(expected = StringFormatException.class)
	public void testEnumListForInOperatorNoValues() throws BaseException {
		assertFromStringList(" ", new HairColor[] {});
	}

	/**
	 * Specializing in making a token list from the given values and tests that they can be
	 * converted to a list for IN operator queries which requires a collection.
	 * 
	 * https://developers.google.com/appengine/docs/java/datastore/queries
	 * 
	 * 
	 * @param separator
	 * @throws BaseException
	 */
	private void assertFromStringList(final String separator, HairColor[] values) throws BaseException {
		FilterConditionSupportedTypeRegistry registry = FilterConditionSupportedTypeRegistry.create().build();
		FilterConditionStringAdapter<?> adapter = getFieldConditionStringAdapter(registry);

		FilterCondition condtion = new FilterCondition.Builder<>().field(FieldAnnotationExample.HAIR_COLOR_FIELD_KEY)
				.operator(FilterOperator.IN).value(StringUtils.join(values, separator)).build();

		FilterCondition<List<HairColor>> actualHairColor = adapter.unmarshal(	adapter.marshal(condtion),
																				hairColorEnumCondition.getClass());
		List<HairColor> colors = Arrays.asList(values);
		assertEquals(colors, actualHairColor.getValue());
	}

	/**
	 * Tests adaptation of an embedded property.
	 * 
	 */
	@Test
	public void testNestedProperty() {
		Integer expectedValue = 3;
		FilterCondition<Integer> nestedPropertyConditions = new FilterCondition.Builder<Integer>()
				.field(Tree.Key.LOWEST_BRANCH_NUMBER_OF_LEAVES).operator(FilterOperator.EQUALS).value(expectedValue)
				.build();
		FilterConditionStringAdapter fieldConditionStringAdapter = getFieldConditionStringAdapter();
		assertFalse("shouldn't be able to handle before registration",
					fieldConditionStringAdapter.handles(nestedPropertyConditions));
		assertNotNull(	"must first register the Tree and nested properties",
						this.annoationFactory.getDictionary(Tree.class));
		assertTrue("can't handle my field?", fieldConditionStringAdapter.handles(nestedPropertyConditions));

	}

	/**
	 * @param registry
	 * @return
	 */
	private FilterConditionStringAdapter<?>
			getFieldConditionStringAdapter(FilterConditionSupportedTypeRegistry registry) {
		return (FilterConditionStringAdapter) new FilterConditionStringAdapter.Builder()
				.setAdapterFactory(getFactory()).setDictionary(dictionary)
				.setDictionaryFactory(annoationFactory.getFactory()).setRegistry(registry).build();
	}

	/**
	 * @return
	 */
	private FilterConditionStringAdapter getFieldConditionStringAdapter() {
		FilterConditionSupportedTypeRegistry registry = FilterConditionSupportedTypeRegistry.create().build();
		return getFieldConditionStringAdapter(registry);
	}

	@Test
	public void testCollectionOfFilterConditions() {

		ArrayList<FilterCondition<?>> conditions = new ArrayList<FilterCondition<?>>();
		StringAdapterFactory factory = getFactory();
		Class<? extends ArrayList> class1 = conditions.getClass();
		Class<FilterCondition> parameterType = FilterCondition.class;
		// Boolean handles = factory.handles(class1, parameterType);
		// handles currently throws a runtime exception...fix that and uncomment this
		// assertFalse(handles);
		factory.register(getFieldConditionStringAdapter());
		Boolean handles = factory.handles(class1, parameterType);
		assertTrue(handles);
	}
}
