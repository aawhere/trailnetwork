/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.id.Identifier;
import com.aawhere.lang.NotImplementedException;

/**
 * @author aroller
 * 
 */
public class MockFilterRepository<EntitiesT extends BaseFilterEntities<EntityT>, EntityT extends BaseEntity<EntityT, IdentifierT>, IdentifierT extends Identifier<?, EntityT>>
		extends MockRepository<EntityT, IdentifierT>
		implements FilterRepository<EntitiesT, EntityT, IdentifierT> {

	private FieldAnnotationDictionaryFactory annoationFactory = FieldAnnotationTestUtil.getAnnoationFactory();

	/**
	 * 
	 */
	public MockFilterRepository() {
		super();
	}

	/**
	 * @param classRepresented
	 * @param identifierType
	 */
	public MockFilterRepository(Class<EntityT> classRepresented, Class<IdentifierT> identifierType) {
		super(classRepresented, identifierType);
	}

	/**
	 * @param classRepresented
	 */
	public MockFilterRepository(Class<EntityT> classRepresented) {
		super(classRepresented);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#filter(com.aawhere.persist.Filter)
	 */
	@Override
	public EntitiesT filter(Filter filter) {

		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#filterAsIterable(com.aawhere.persist.Filter)
	 */
	@Override
	public Iterable<EntityT> filterAsIterable(Filter filter) {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#paginateIds(com.aawhere.persist.Filter)
	 */
	@Override
	public Iterable<IdentifierT> idPaginator(Filter filter) {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#ids(com.aawhere.persist.Filter)
	 */
	@Override
	public Iterable<IdentifierT> ids(Filter filter) {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#paginateEntities(com.aawhere.persist.Filter)
	 */
	@Override
	public Iterable<EntityT> entityPaginator(Filter filter) {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#entities(java.lang.Iterable)
	 */
	@Override
	public EntitiesT entities(Iterable<IdentifierT> ids) {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#dictionary()
	 */
	@Override
	public FieldDictionary dictionary() {

		return annoationFactory.getDictionary(getEntityClassRepresented());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#count(com.aawhere.persist.Filter)
	 */
	@Override
	public Integer count(Filter filter) {
		throw new NotImplementedException();
	}

	/*
	 * @see com.aawhere.persist.FilterRepository#dictionaryFactory()
	 */
	@Override
	public FieldDictionaryFactory dictionaryFactory() {
		return annoationFactory.getFactory();
	}
}
