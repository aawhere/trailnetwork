/**
 *
 */
package com.aawhere.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.lang.Assertion;
import com.aawhere.search.TextSubstringGenerator.Builder;

/**
 * @author Brian Chapman
 * 
 */
public class TextSubstringGeneratorUnitTest {

	private String input;

	@Before
	public void setup() {
		input = "Hello World";
	}

	@Test
	public void testTextSubstringGeneratorWithMinLength2() {
		TextSubstringGenerator generator = builder().build();
		assertGenerator(generator, "lo orl Wor Wo ll Hel ello rl or ell Worl Hell Hello rld el ld World orld He llo");
	}

	private Builder builder() {
		return TextSubstringGenerator.create().input(input).minLength(2);
	}

	@Test
	public void testWithPunctuation() {
		String input = "Hel$l%$o :; () [] World!";
		TextSubstringGenerator generator = TextSubstringGenerator.create().input(input).minLength(2).build();
		assertGenerator(generator, "lo orl Wor Wo ll Hel ello rl or ell Worl Hell Hello rld el ld World orld He llo");
	}

	@Test
	public void testTextSubstringGeneratorLength() {
		String input = "Aaron Roller (aroller/arollertest/trailblazer)";
		TextSubstringGenerator generator = TextSubstringGenerator.create().input(input).minLength(2).build();
		Assertion.assertNotEmpty(generator.substrings());
	}

	/**
	 * TN-848
	 */
	@Test
	public void testStartWordsOnly() {
		TextSubstringGenerator generator = builder().wordStartsOnly().build();
		assertGenerator(generator, "Wor Wo Hel Worl Hell He");
	}

	private void assertGenerator(TextSubstringGenerator generator, String expected) {
		assertNotNull(generator.substrings());
		assertTrue(!generator.substrings().isEmpty());
		assertNotNull(generator.asString());
		assertEquals(expected, generator.asString());
	}

}
