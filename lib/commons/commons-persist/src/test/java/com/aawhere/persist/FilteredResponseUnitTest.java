/**
 * 
 */
package com.aawhere.persist;

import java.util.ArrayList;

import org.junit.Test;

import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author aroller
 * 
 */
public class FilteredResponseUnitTest {

	@Test
	public void testJaxb() {
		ArrayList<ChildId> ids = new ArrayList<ChildId>();
		String value = "test";
		ids.add(new ChildId(value));
		FilteredResponse<ChildId> response = FilteredResponse.create(ids).build();
		JAXBTestUtil<FilteredResponse<ChildId>> util = JAXBTestUtil.create(response)
				.classesToBeBound(new Class<?>[] { ChildId.class, FilteredResponse.class }).build();
		util.assertContains(value);
		util.assertContains(ChildId.DOMAIN);
	}
}
