/**
 * 
 */
package com.aawhere.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheListener;
import net.sf.jsr107cache.CacheStatistics;

import com.aawhere.lang.NotImplementedException;

import com.google.common.base.Predicates;
import com.google.common.collect.Maps;

public class MockCache
		implements Cache {

	private Map<Object, Object> cache = new HashMap<Object, Object>();

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#containsKey(java.lang.Object)
	 */
	@Override
	public boolean containsKey(Object key) {
		return cache.containsKey(key);
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#containsValue(java.lang.Object)
	 */
	@Override
	public boolean containsValue(Object value) {
		// GAE doesn't implement this;
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#entrySet()
	 */
	@Override
	public Set entrySet() {
		return this.cache.entrySet();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return cache.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#keySet()
	 */
	@Override
	public Set keySet() {
		return this.keySet();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#putAll(java.util.Map)
	 */
	@Override
	public void putAll(Map t) {
		this.cache.putAll(t);

	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#size()
	 */
	@Override
	public int size() {
		return cache.size();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#values()
	 */
	@Override
	public Collection values() {
		return this.cache.values();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#get(java.lang.Object)
	 */
	@Override
	public Object get(Object key) {
		return cache.get(key);
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#getAll(java.util.Collection)
	 */
	@Override
	public Map getAll(Collection keys) throws CacheException {
		return Maps.filterKeys(this.cache, Predicates.in(keys));
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#load(java.lang.Object)
	 */
	@Override
	public void load(Object key) throws CacheException {
		throw new UnsupportedOperationException();

	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#loadAll(java.util.Collection)
	 */
	@Override
	public void loadAll(Collection keys) throws CacheException {
		throw new UnsupportedOperationException();

	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#peek(java.lang.Object)
	 */
	@Override
	public Object peek(Object key) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public Object put(Object key, Object value) {
		return cache.put(key, value);
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#getCacheEntry(java.lang.Object)
	 */
	@Override
	public CacheEntry getCacheEntry(Object key) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#getCacheStatistics()
	 */
	@Override
	public CacheStatistics getCacheStatistics() {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#remove(java.lang.Object)
	 */
	@Override
	public Object remove(Object key) {
		return this.cache.remove(key);
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#clear()
	 */
	@Override
	public void clear() {
		this.cache.clear();

	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#evict()
	 */
	@Override
	public void evict() {
		throw new UnsupportedOperationException();

	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#addListener(net.sf.jsr107cache.CacheListener)
	 */
	@Override
	public void addListener(CacheListener listener) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.jsr107cache.Cache#removeListener(net.sf.jsr107cache.CacheListener)
	 */
	@Override
	public void removeListener(CacheListener listener) {
		throw new UnsupportedOperationException();

	}

}