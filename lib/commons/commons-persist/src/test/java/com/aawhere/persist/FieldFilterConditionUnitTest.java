/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.field.Examples;

/**
 * @author brian
 * 
 */
public class FieldFilterConditionUnitTest {

	public static final Class<LongMockEntity> ENTITY_CLASS = LongMockEntity.class;
	FilterCondition<String> condition;

	@Before
	public void setUp() {
		condition = new FilterCondition.Builder<String>().field(Examples.MY_KEY).value("A Value")
				.operator(FilterOperator.EQUALS).build();
	}

	/**
	 * Test method for {@link com.aawhere.persist.FieldFilterCondition#getFieldKey()}.
	 */
	@Test
	public void testGetFieldKey() {
		assertEquals(Examples.MY_KEY.getAbsoluteKey(), condition.getField());
	}

}
