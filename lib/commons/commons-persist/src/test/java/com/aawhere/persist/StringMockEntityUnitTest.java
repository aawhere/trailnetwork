/**
 *
 */
package com.aawhere.persist;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.persist.StringMockEntity.Builder;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Example UnitTest to use the {@link BaseEntityBaseUnitTest} for testing MockEntities.
 * 
 * @author Aaron Roller
 * 
 */
public class StringMockEntityUnitTest
		extends
		BaseEntityBaseUnitTest<StringMockEntity, StringMockEntity.Builder, StringMockEntityId, StringMockEntityRepository> {

	private String originalName;
	private String mutatedName;

	@Override
	@Before
	public void setUp() {
		originalName = TestUtil.generateRandomString();
		mutatedName = TestUtil.generateRandomString();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#populateBeforeCreation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.setName(originalName);
	}

	@Override
	public void assertCreation(StringMockEntity sample) {
		super.assertCreation(sample);
		assertEquals(this.originalName, sample.getName());
	};

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(StringMockEntity mutated) {
		super.assertMutation(mutated);
		assertEquals(mutatedName, mutated.getName());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#testFieldKeys()
	 */
	@Override
	@Test
	public void testFieldKeys() throws IllegalArgumentException, IllegalAccessException {
		// not worth implementing for this test-only class.
		// see LongMockEntity since it is implemented there.
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected StringMockEntityRepository createRepository() {
		return new StringMockEntityMockRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.l.Id)
	 */
	@Override
	protected StringMockEntity mutate(StringMockEntityId id) throws EntityNotFoundException {
		return getRepository().updateName(id, mutatedName);
	}

	/**
	 * Runs the {@link #getEntitySample()} through a JAXB Marshalling/Unmarshalling cycle and
	 * verifies properties are being handled appropriately.
	 * 
	 * TODO: promote this to BaseEntityBaseUnitTest
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testJaxbRoundTrip() throws JAXBException {
		StringMockEntity sample = getEntitySample();
		JAXBTestUtil<StringMockEntity> util = JAXBTestUtil.create(sample).build();
		StringMockEntity actual = util.getActual();

		// just re-use the existing method to ensure the sample was "re-created" appropriately
		assertCreation(actual);
		assertEquals(sample.getName(), actual.getName());
	}

	protected static StringMockEntityUnitTest getInstance() {
		StringMockEntityUnitTest instance = new StringMockEntityUnitTest();
		instance.baseSetUp();
		instance.setUp();
		return instance;
	}
}
