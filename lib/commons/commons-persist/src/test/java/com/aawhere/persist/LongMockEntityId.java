/**
 *
 */
package com.aawhere.persist;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.adapter.AdapterFactoryUnitTest.MockEnumAdapter;
import com.aawhere.id.LongIdentifier;
import com.aawhere.persist.EntityTestUtil.LongMockEntityService;
import com.aawhere.xml.XmlNamespace;

/**
 * @author brian
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@ServicedBy(LongMockEntityService.class)
public class LongMockEntityId
		extends LongIdentifier<LongMockEntity> {

	private static final long serialVersionUID = -5034835785846154685L;

	/**
	 * @param kind
	 */
	LongMockEntityId() {
		super(LongMockEntity.class);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public LongMockEntityId(Long value) {
		super(value, LongMockEntity.class);
	}

	/**
	 * Provided to any client that may have a Long formatted as a String.
	 * 
	 * @param value
	 * @param kind
	 */
	public LongMockEntityId(String value) {
		super(value, LongMockEntity.class);
	}

}