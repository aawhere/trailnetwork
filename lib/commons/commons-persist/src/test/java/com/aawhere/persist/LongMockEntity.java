package com.aawhere.persist;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.field.DictionaryIndex;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.AnnotatedDictionary;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.DictionaryMutator;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.annotation.FieldMutator;
import com.aawhere.field.annotation.MutableAnnotatedDictionary;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.IdentifierToIdentifiableXmlAdapter;
import com.aawhere.search.Searchable;
import com.aawhere.xml.bind.OptionalXmlAdapter;
import com.google.common.base.Function;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@XmlRootElement
@Searchable(fieldKeys = { LongMockEntity.FIELD.NAME_KEY, LongMockEntity.FIELD.COUNT })
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = LongMockEntity.FIELD.DOMAIN_KEY, indexes = { LongMockEntity.DateCreatedIndex.class,
		LongMockEntity.NameCountIndex.class })
public class LongMockEntity
		extends LongBaseEntity<LongMockEntity, LongMockEntityId>
		implements AnnotatedDictionary, DateCreatedFilterable {

	private static final long serialVersionUID = -2081041814069496796L;

	@XmlTransient
	public static final class FIELD {

		public static final String NAME_KEY = "name";
		public static final String COUNT = "count";
		public static final String RELATED = "related";
		public static final String DOMAIN_KEY = "longMock";
		public static final String MOCK_EMBEDDED_DOMAIN = "mockEmbed";
		public static final String DIFFERENT = "different";
		public static final String THAN_THIS = "thanThis";
		public static final String EMBEDED_PROPERTY = "property";
		public static final String EMBED1 = "embed1";
		public static final String EMBED2 = "embed2";

		public static final String EMBEDDED_IN_EMBEDDED = "embeddedInEmbedded";
		public static final String COLLECTION = "collection";
		public static final String EMBED3 = "embed3";
		public static final String DIFFERENT_EMBED = "mapped";
		public static final String COMPARABLE = "comparable";

		/** provides constant access to FieldKeys for reference from the dictionary */
		public static class KEY {

			public static FieldKey RELATED = new FieldKey(DOMAIN_KEY, FIELD.RELATED);
			public static FieldKey COUNT = new FieldKey(DOMAIN_KEY, FIELD.COUNT);
			public static FieldKey COMPARABLE = new FieldKey(DOMAIN_KEY, FIELD.COMPARABLE);
			public static FieldKey ID = new FieldKey(DOMAIN_KEY, BaseEntity.BASE_FIELD.ID);
			public static FieldKey DIFFERENT = new FieldKey(DOMAIN_KEY, FIELD.DIFFERENT);
			public static FieldKey DATE_CREATED = new FieldKey(DOMAIN_KEY, BaseEntity.BASE_FIELD.DATE_CREATED);
			public static FieldKey DATE_UPDATED = new FieldKey(DOMAIN_KEY, BaseEntity.BASE_FIELD.DATE_UPDATED);
			public static FieldKey NAME = new FieldKey(DOMAIN_KEY, NAME_KEY);
			public static FieldKey EMBED_1 = new FieldKey(DOMAIN_KEY, EMBED1);
			public static FieldKey EMBED_2 = new FieldKey(DOMAIN_KEY, EMBED2);
			public static FieldKey DIFFERENT_EMBED = new FieldKey(DOMAIN_KEY, FIELD.DIFFERENT_EMBED);
			public static FieldKey EMBEDDED_IN_DIFFERENT_EMBED = new FieldKey(FIELD.KEY.DIFFERENT_EMBED,
					FIELD.EMBEDED_PROPERTY);

			// here embed_1 essentially becomes the domain
			public static FieldKey EMBEDDED_PROPERTY_IN_EMBED_1 = new FieldKey(FIELD.KEY.EMBED_1,
					FIELD.EMBEDED_PROPERTY);
			public static FieldKey EMBEDDED_ENTITY_IN_EMBED_1 = new FieldKey(EMBED_1, FIELD.EMBEDED_PROPERTY);
			public static FieldKey COLLECTION = new FieldKey(DOMAIN_KEY, FIELD.COLLECTION);

		}
	}

	/**
	 * Used to construct all instances of MockEntity.
	 */
	@DictionaryMutator(domain = FIELD.DOMAIN_KEY)
	public static class Builder
			extends LongBaseEntity.Builder<LongMockEntity, Builder, LongMockEntityId>
			implements MutableAnnotatedDictionary {

		public Builder() {
			this(new LongMockEntity());
		}

		Builder(LongMockEntity original) {
			super(original, LongMockEntityId.class);
		}

		@FieldMutator(key = FIELD.NAME_KEY)
		public Builder setName(String name) {
			building.name = name;
			return this;
		}

		public Builder setEmbed1(MockEmbeded embed1) {
			building.embed1 = embed1;
			return this;
		}

		public Builder setCount(Integer count) {
			building.count = count;
			return this;
		}

		public Builder setStrings(List<String> strings) {
			building.strings = strings;
			return this;
		}

		public Builder setThanThis(String thanThis) {
			building.thanThis = thanThis;
			return this;
		}

		public Builder setRelated(StringMockEntityId related) {
			building.related = related;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct MockEntity */
	protected LongMockEntity() {
	}

	public static Builder create() {
		return new Builder();
	}

	@Index
	@XmlElement
	private String name;

	@XmlElement
	@Index
	@Field(key = FIELD.RELATED, xmlAdapter = StringMockEntityXmlAdapter.class)
	@XmlJavaTypeAdapter(StringMockEntityXmlAdapter.class)
	StringMockEntityId related = EntityTestUtil.generateRandomMockStringEntityId();

	@Field(key = FIELD.DIFFERENT, indexedFor = "testDifferent")
	@Index
	private String thanThis;

	@XmlElement
	@Index
	@Field(key = FIELD.COLLECTION, parameterType = String.class, indexedFor = { "testEmptyCollection" })
	private List<String> strings;

	@XmlElement
	@Index
	@Field(key = FIELD.COUNT, indexedFor = "testDoubleQuery", xmlAdapter = CountXmlAdapter.class)
	@XmlJavaTypeAdapter(CountXmlAdapter.class)
	Integer count;

	@XmlElement
	@Field(key = FIELD.COMPARABLE)
	Integer comparable;

	/** Shows how to filter a common object embedded twice. */
	@XmlElement
	@Field(key = FIELD.EMBED1)
	private MockEmbeded embed1;
	@XmlElement
	@com.aawhere.field.annotation.Field(key = FIELD.EMBED2)
	private MockEmbeded embed2;

	@XmlElement
	@Field(key = FIELD.DIFFERENT_EMBED)
	private MockEmbeded embed3;

	@Field(key = FIELD.NAME_KEY, indexedFor = { "testNameQuery", "testOrderByAscending" }, searchable = true)
	public String getName() {
		return name;
	}

	/**
	 * @return the count
	 */
	public Integer getCount() {
		return this.count;
	}

	/**
	 * @return the embed1
	 */
	public MockEmbeded getEmbed1() {
		return this.embed1;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@Override
	@com.aawhere.field.annotation.Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM,
			indexedFor = { "Repository.load()" }, searchable = true)
	public LongMockEntityId getId() {
		return super.getId();
	}

	/**
	 * @return the embed2
	 */
	public MockEmbeded getEmbed2() {
		return this.embed2;
	}

	/**
	 * @return the strings
	 */
	public List<String> getStrings() {
		return this.strings;
	}

	/**
	 * @return the related
	 */
	public StringMockEntityId getRelated() {
		return this.related;
	}

	public LongMockEntityId getLongMockEntityId() {
		return getId();
	}

	@XmlRootElement
	@XmlType
	@XmlAccessorType(XmlAccessType.FIELD)
	@Dictionary(domain = FIELD.MOCK_EMBEDDED_DOMAIN)
	public static class MockEmbeded
			implements Serializable {
		private static final long serialVersionUID = -2567880745666577700L;
		@com.aawhere.field.annotation.Field(key = FIELD.EMBEDED_PROPERTY,
				indexedFor = { "testOrderByEmbeddedAscending" }, searchable = true)
		@Index
		public Integer property;

	}

	public static class DateCreatedIndex
			extends DictionaryIndex {
		public DateCreatedIndex() {
			super(FIELD.KEY.DATE_CREATED);
		}
	}

	public static class NameCountIndex
			extends DictionaryIndex {
		public NameCountIndex() {
			super(FIELD.KEY.NAME, FIELD.KEY.COUNT);
		}
	}

	public static class StringMockEntityXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<StringMockEntity, StringMockEntityId> {

		public StringMockEntityXmlAdapter() {
			super(EntityTestUtil.stringMockEntityFunction());
		}

		public StringMockEntityXmlAdapter(Function<StringMockEntityId, StringMockEntity> function) {
			super(function);
		}
	}

	/**
	 * Tests targeted inclusing using fields.
	 * 
	 * @author aroller
	 * 
	 */
	public static class CountXmlAdapter
			extends OptionalXmlAdapter<String, Integer> {
		public CountXmlAdapter() {
			super(Visibility.SHOW);
		}

		@Override
		protected String showMarshal(Integer v) throws Exception {
			return String.valueOf(v);
		}

		@Override
		protected Integer showUnmarshal(String v) throws Exception {
			return Integer.parseInt(v);
		}

	}
}
