/**
 * 
 */
package com.aawhere.persist;

import java.util.Set;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

import com.aawhere.id.Identifier;
import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class EntityServiceUtil {

	public static <S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			Function<Set<I>, Iterable<I>> nonExistingIdsFunction(final EntityService<S, E, I> entityService) {
		return new Function<Set<I>, Iterable<I>>() {

			@Override
			public Iterable<I> apply(Set<I> input) {
				return (input != null) ? entityService.nonExisting(input) : null;
			}
		};
	}

	/**
	 * The passkey required to delete entities. Useful to protect external requests from accidental
	 * deletion.
	 * 
	 * @return
	 */
	public static String deletionConfirmationPasskey() {
		LocalDate localDate = LocalDate.now(DateTimeZone.UTC);
		return localDate.toString();
	}

	/**
	 * Adds the necessary passkey to delete entities.
	 * 
	 * @param filterBuilder
	 * @return
	 */
	public static <B extends Filter.BaseBuilder<?, ?>> B deletionConfirmed(B filterBuilder) {
		filterBuilder.addOption(deletionConfirmationPasskey());
		return filterBuilder;
	}
}
