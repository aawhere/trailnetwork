/**
 *
 */
package com.aawhere.persist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.list.SetUniqueList;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.reflections.util.FilterBuilder;

import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.id.Identifier;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.NotImplementedException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.persist.FilterSort.Direction;
import com.aawhere.search.SearchService;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.inject.Inject;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * The all encompassing filter that is used to query {@link Repository} implementations.
 * 
 * {@link FieldKey}s are used to identify the fields of interest for {@link #conditions} and
 * {@link #orderBy}. This has pagination requests built in related to {@link #limit}, {@link #page},
 * {@link #pageTotal}, {@link #getStart()} and {@link #getEnd()}. Defaults will be applied if not
 * provided by the client.
 * 
 * This class caters to the nuances of web resource injection since that is it's primary
 * purpose...to translate a web request to a data request.
 * 
 * The {@link QueryParam} annotation identifies those fields that are to be injected by a GET
 * request. It is apparent, through empirical observation that all fields with QueryParam will be
 * injected with a null if they are not provided in the request. The solution would be to provide a
 * {@link DefaultValue} annotation, but that only accepts a String which is odd, but more
 * importantly the defaults would be different if it were being constructed using JAX-RS vs. the
 * Builder.
 * 
 * It is recommended to insert default values in the getter methods to retrieve defaults if nothing
 * is provided. Then all internal methods should call those getters, rather than the variables
 * directly. This of course should be applied to implementing children of this filter.
 * 
 * Suppressing raw types to support {@link #conditions} limitations.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
@SuppressWarnings("rawtypes")
@ApiModel(value = "The standard filter used to query endpoints return multiple entities.")
public final class Filter
		implements Cloneable, Serializable {

	private static final long serialVersionUID = 2811750219082382858L;
	private static final Length CITY_BOUNDS_RADIUS_DEFAULT = MeasurementUtil.createLengthInMeters(5000);
	static final Integer MIN_LIMIT = 1;
	static final Integer MAX_LIMIT = 1000;
	public static final Integer DEFAULT_LIMIT = 20;
	public static final Integer FIRST_PAGE = 1;

	static final FilterSort ORDER_BY_NONE = FilterSort.NONE;

	/**
	 * Allows a parent id value to be assigned from the web. This must be converted by a
	 * {@link FilterBuilder} assigning to {@link #parentId} in it's class form.
	 * 
	 */
	@XmlElement
	@QueryParam("parent")
	private String parent;

	@XmlTransient
	private Identifier<?, ? extends BaseEntity> parentId;

	/**
	 * Describes the number of results for the current page. Typically this will equal the limit,
	 * but in cases where it is less than the limit then there likely aren't more results.
	 * 
	 */
	@XmlAttribute
	@ApiModelProperty(value = "The number of results available in the response", required = false, position = 3)
	private Integer pageTotal;

	@XmlAttribute(name = FIELD.LIMIT)
	@QueryParam(FIELD.LIMIT)
	@ApiModelProperty(value = PersistMessage.Doc.FILTER_LIMIT_DESCRIPTION, required = false, position = 1)
	private Integer limit;

	/**
	 * has limit is a very specific setting saying do not use the default limit. This is purposely
	 * available to code, not the web.
	 */
	private boolean hasLimit = true;

	// Specifically this is not intended to be modified by web requests
	@XmlAttribute
	// display it if it exists for performance investigations
	private Integer chunkSize;

	/**
	 * Each set of results returned is one page. By incrementing the page, you can scan through
	 * results in chunks of {@link #limit}. Starts at 1;
	 */
	@QueryParam(FIELD.PAGE)
	@ApiModelProperty(value = PersistMessage.Doc.FILTER_PAGE_DESCRIPTION, required = false, position = 2)
	@XmlAttribute(name = FIELD.PAGE)
	private Integer page;

	@XmlAttribute
	private Integer filteredByPredicates;

	/**
	 * Automatic location detection as a gift from App engine. TN-56 This will populate bounds
	 * automatically if it is called and bounds was not already provided.
	 * 
	 * @see #getBounds()
	 * @see #cityCoordinateRadiusInMeters
	 */
	@HeaderParam("X-AppEngine-CityLatLong")
	@XmlAttribute
	private String cityCoordinate;

	@QueryParam("autoLocate")
	@XmlAttribute
	private Boolean autoLocate;

	/**
	 * The diagonal distance for making the bounds for {@link #cityCoordinate}
	 * 
	 */
	@QueryParam("cityCoordinatesRadiusInMeters")
	@XmlElement
	private String cityCoordinateRadiusInMeters;
	/**
	 * Provides the general {@link FilterCondition} query statements that limit the results based on
	 * the {@link Field} of interest.
	 * 
	 * NOTE: The query param requires special translation using {@link FilterConditionStringAdapter}
	 * hooked into the request to populate properly.
	 * 
	 * FIXME: Although FilterCondition should be paramaterized, doing so causes issues with web
	 * injection
	 * 
	 * FIXME: simplify this by getting rid of inject and just use the {@link #conditionsInjected}?
	 * 
	 * WARNING: call {@link #getConditions()} instead of calling this directly. query param
	 * injection wreaks havoc by over-writing conditions that may be in there already so
	 * {@link #conditionsInjected} is separate and combined in {@link #getConditions()}.
	 */
	@ApiModelProperty(value = PersistMessage.Doc.FILTER_CONDITIONS_DESCRIPTION)
	@XmlElement(name = FIELD.CONDITIONS)
	private List<FilterCondition> conditions;

	/**
	 * Anything with conditions=my-field=something will go here.
	 * 
	 * Support for scraping the query parameters that don't match a named property injection like
	 * conditions. so instead of conditions = my-field = something, you just use my-field=something.
	 * 
	 * 
	 * TN-784
	 * 
	 * This must be combined with {@link #conditions} in {@link #getConditions()} since
	 * {@link QueryParam} with jerseys' injection nukes the conditions assigned to
	 * {@link #conditions}.
	 * 
	 * @see #Filter(UriInfo, FilterConditionStringAdapter)
	 * 
	 */
	@QueryParam(FIELD.CONDITIONS)
	private List<FilterCondition> conditionsInjected;

	/**
	 * Similar syntax to {@link #conditions}, but intended for the application to take custom action
	 * rather than a query against the {@link Repository}.
	 * 
	 * The typical use would be passing the string field that would be declared with other fields in
	 * the domain object. The values are flexible, but it is encouraged to use enumerations declared
	 * in the domain if appropriate or in the object that can handle such requests.
	 */
	@XmlElement(name = FIELD.OPTIONS)
	@QueryParam(FIELD.OPTIONS)
	@ApiModelProperty(value = PersistMessage.Doc.FILTER_OPTIONS_DESCRIPTION)
	private List<String> options;

	@XmlElement(name = FIELD.EXPAND)
	@QueryParam(FIELD.EXPAND)
	@ApiModelProperty(value = PersistMessage.Doc.FILTER_EXPAND_DESCRIPTION)
	private List<String> expand;

	@XmlList
	@XmlElement(name = FIELD.SEARCH_INDEXES)
	@QueryParam(FIELD.SEARCH_INDEXES)
	@ApiModelProperty(value = "The indexes used to satisfy the search filters",
			notes = "Typically provided by the system, these provide the location of the search documents to be found.")
	private List<String> searchIndexes;

	@XmlList
	@XmlElement(name = FIELD.FIELDS)
	@QueryParam(FIELD.FIELDS)
	@ApiModelProperty(
			value = "Field Keys to be included in the response (if supported)",
			notes = "Different than expand in that this indicates if field is to be included at all.  Expand will make an existing field verbose...expand an identifier into it's model.")
	private List<String> fields;

	/** Sorts the results using the datastore. */
	@XmlElement(name = FIELD.ORDER_BY)
	@QueryParam(FIELD.ORDER_BY)
	private List<FilterSort> orderBy;

	/** used to sort the results returned from the datastore. */
	@XmlElement
	@QueryParam("orderByInMemory")
	private List<FilterSort> orderByInMemory;

	@XmlElement
	@QueryParam("orderByNone")
	private Boolean orderByNone;

	/**
	 * Full text query.
	 * 
	 * @See {@link SearchService}
	 */
	@XmlElement
	@QueryParam(FIELD.QUERY)
	private String query;

	@XmlAttribute(name = FIELD.BOUNDS)
	@QueryParam(FIELD.BOUNDS)
	private String bounds;

	@XmlAttribute(name = FIELD.NEAR)
	@ApiModelProperty(
			value = "Finds nearby features (coordinates lat,lon or bounds)",
			notes = "If coordinates are provided then the distance to search is provided by the handler. If Bounds is provided then distance to search will be limited to the bounds provided, but the centerpoint will be the focus of the search.")
	private String near;
	/**
	 * Indicates if bounds is proximity search focused on the center of the Bounds rather than
	 * encomapssing every corner.
	 */
	@XmlAttribute(name = FIELD.PROXIMITY)
	@QueryParam(FIELD.PROXIMITY)
	private Boolean proximity;

	/**
	 * Represents a cursor for a particular page. Useful for quickly scanning to a particular page.
	 * This is an internal variable and should not be set by a client (i.e. a JAXB client, where we
	 * cannot hide the internals).
	 * 
	 * This field should not be included as part of the Equals method.
	 * 
	 * This GAE specific field is here due to TN-226 and does not require any GAE library code.
	 * 
	 * <a href= "https://developers.google.com/appengine/docs/java/datastore/queries#Query_Cursors"
	 * >App Engine</a>
	 * 
	 */
	@XmlAttribute
	@QueryParam("pageCursor")
	private String pageCursor;

	/**
	 * Directly converted to predicates when providing conditions against comparable fields.
	 * 
	 */
	@XmlElement(name = FIELD.PREDICATE_CONDITIONS)
	@QueryParam(FIELD.PREDICATE_CONDITIONS)
	private List<FilterCondition> predicateConditions;

	/**
	 * Programatic filter limits the results of the datastore during the building of an Entities
	 * object.
	 * 
	 */
	private List<Predicate<?>> predicates;

	/** Memory sort of the entities after datastore retrieval. */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Ordering<?> ordering;

	@XmlAttribute
	@QueryParam("autoPredicates")
	private Boolean autoPredicates = false;

	/**
	 * Only to be used by Jersey. Jersey (under java 1.6 and probably java 7) was happy with this
	 * being protected, but appengine under Java 7 does not allow reflective calls to non public
	 * members. All other uses should use {@link Builder} to construct {@link Filter}.
	 */
	public Filter() {
		this.conditions = new ArrayList<FilterCondition>();
		this.options = new ArrayList<>();
		this.orderBy = new ArrayList<FilterSort>();
		this.predicates = new ArrayList<Predicate<?>>();
		this.fields = new ArrayList<>();
		this.searchIndexes = new ArrayList<>();
		this.expand = new ArrayList<>();
	}

	/**
	 * Web specific injected constructor providing "simple" conditions TN-784.
	 * 
	 * @param uriInfo
	 * @param conditionAdapter
	 */
	@Inject
	public Filter(@Context UriInfo uriInfo, FilterConditionStringAdapter<?> conditionAdapter) {
		this();
		MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();
		try {
			this.conditions.addAll(FilterUtil.conditions(queryParameters.entrySet(), conditionAdapter));
		} catch (BaseException e) {
			throw ToRuntimeException.wrapAndThrow(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Filter#getLimit()
	 */
	public Integer getLimit() {
		// this must be done here since web api setting nulls it out
		if (this.limit == null && this.hasLimit) {
			this.limit = DEFAULT_LIMIT;
		}
		return this.limit;
	}

	/**
	 * @return the parent
	 */
	public Identifier<?, ? extends BaseEntity> getParent() {
		return this.parentId;
	}

	public Boolean hasParent() {
		return this.parent != null;
	}

	/**
	 * @return the bounds
	 */
	public String getBounds() {

		return this.bounds;
	}

	public Boolean getAutoLocate() {
		return autoLocate;
	}

	public Boolean hasBounds() {
		return StringUtils.isNotEmpty(bounds);
	}

	/**
	 * Returns true if and only if {@link #autoLocate} is true. False otherwise, never null.
	 * 
	 * @return
	 */
	@Nonnull
	public Boolean isAutoLocate() {
		return BooleanUtils.isTrue(autoLocate);
	}

	public Boolean hasCityCoordinates() {
		return StringUtils.isNotEmpty(cityCoordinate);
	}

	/**
	 * Indicates if something besides the default was provided. {@link #getCityCoordinatesRadius()}
	 * always returns the {@link #CITY_BOUNDS_RADIUS_DEFAULT} if not provided.
	 * 
	 * @return
	 */
	public Boolean hasCityCoordinatesRadius() {
		return StringUtils.isNotEmpty(cityCoordinateRadiusInMeters);
	}

	public GeoCoordinate getCityCoordinate() {
		return GeoCoordinate.valueOf(cityCoordinate);
	}

	/**
	 * Returns the provided city bounds of the {@link #CITY_BOUNDS_RADIUS_DEFAULT} if none is
	 * provided.
	 * 
	 * @return
	 */
	@Nonnull
	public Length getCityCoordinatesRadius() {
		Length radius = CITY_BOUNDS_RADIUS_DEFAULT;
		if (StringUtils.isNotEmpty(cityCoordinateRadiusInMeters)) {
			radius = MeasurementUtil.createLengthInMeters(Integer.parseInt(cityCoordinateRadiusInMeters));
		}
		return radius;
	}

	public Boolean hasFields() {
		return CollectionUtils.isNotEmpty(fields);
	}

	public Boolean hasSearchIndexes() {
		return CollectionUtils.isNotEmpty(searchIndexes);
	}

	public Boolean hasNear() {
		return StringUtils.isNotEmpty(near);
	}

	/**
	 * Limit is optional.
	 * 
	 * @return
	 */
	public Boolean hasLimit() {
		return getLimit() != null;
	}

	/**
	 * Provides true if the query has something. Empty string or null indicates false.
	 * 
	 * @return
	 */
	public Boolean hasQuery() {
		return !StringUtils.isEmpty(query);
	}

	public Boolean hasPredicates() {
		return CollectionUtils.isNotEmpty(predicates);
	}

	/**
	 * @return the autoPredicates
	 */
	public Boolean isAutoPredicates() {
		return BooleanUtils.isTrue(this.autoPredicates);
	}

	/**
	 * Chunk size is used for special queries that iterate through the results.
	 * 
	 * @return
	 */
	public Integer getChunkSize() {
		return this.chunkSize;
	}

	public static Integer getMaxLimit() {
		return MAX_LIMIT;
	}

	public static Integer getMinLimit() {
		return MIN_LIMIT;
	}

	// xml here since page default is returned here, not in the field.
	public Integer getPage() {
		return If.nil(this.page).use(FIRST_PAGE);
	}

	public Boolean isFirstPage() {
		return FIRST_PAGE.equals(getPage());
	}

	/**
	 * @return the predicates
	 */
	public List<Predicate<?>> getPredicates() {
		return this.predicates;
	}

	/**
	 * @return the predicateConditions
	 */
	public List<FilterCondition> getPredicateConditions() {
		return this.predicateConditions;
	}

	/**
	 * @return the filteredByPredicates
	 */
	public Integer getFilteredByPredicates() {
		return this.filteredByPredicates;
	}

	@XmlAttribute
	@ApiModelProperty(value = "The result where this page begins.", required = false, position = 4)
	public Integer getStart() {
		return ((getPage() - 1) * getLimit()) + 1;
	}

	/**
	 * The count referencing the last item provided in the query. If page total is less than the
	 * limit this is considered the last page.
	 * 
	 * @return
	 */
	@XmlAttribute
	@ApiModelProperty(value = "The result where this page ends.", required = false, position = 5)
	public Integer getEnd() {
		final Integer total = pageTotal();
		// if total is given then use it since the last page may be less than the limit
		if (total != null) {
			// one-based reporting so adding total reports one high
			return getStart() + total - 1;
		} else {
			return getPage() * getLimit();
		}
	}

	@XmlAttribute
	@Nonnull
	@ApiModelProperty(value = "Indicates false if it's known that no more results exist beyond this page.",
			required = false, position = 5)
	public Boolean isFinalPage() {
		Boolean finalPage = false;
		if (getPageTotal() != null) {
			finalPage = pageTotal() < getLimit();
		}
		return finalPage;
	}

	/**
	 * @return
	 */
	@XmlAttribute
	@Nullable
	public Integer getPageTotalBeforePredicateFiltering() {
		if (hasPredicates()) {
			return pageTotal();
		} else {
			return null;
		}
	}

	/** Internal method used to provide the best page total possible. */
	private Integer pageTotal() {
		if (this.filteredByPredicates == null) {
			return getPageTotal();
		} else {
			return getPageTotal() + this.filteredByPredicates;
		}
	}

	public Integer getPageTotal() {
		return this.pageTotal;
	}

	public Boolean hasCursor() {
		return pageCursor != null;
	}

	public String getCursor() {
		return this.pageCursor;
	}

	/**
	 * Retrieves all the conditions for this filter.
	 * 
	 * 
	 * @return the conditions in an unmodifiable list
	 */
	public List<FilterCondition> getConditions() {
		if (CollectionUtils.isEmpty(this.conditions)) {
			// FIXME:this initialization is in too many places
			this.conditions = new ArrayList<FilterCondition>();
		}
		// add all the lazy conditions into conditions for consolidation
		if (CollectionUtils.isNotEmpty(conditionsInjected)) {
			this.conditions.addAll(conditionsInjected);
			this.conditionsInjected = null;
		}
		return ImmutableList.copyOf(this.conditions);
	}

	/**
	 * @see #options
	 * @return the options
	 */
	public List<String> getOptions() {
		if (this.options == null) {
			this.options = Collections.emptyList();
		}
		return this.options;
	}

	public List<String> getFields() {
		if (this.fields == null) {
			this.fields = Collections.emptyList();
		}
		return fields;
	}

	public List<String> getSearchIndexes() {
		if (this.searchIndexes == null) {
			this.searchIndexes = Collections.emptyList();
		}
		return searchIndexes;
	}

	/**
	 * 
	 * @return {@link #getFields()} transformed into thier corresponding {@link FieldKey}.
	 */
	public List<FieldKey> getFieldKeys() {
		return ImmutableList.<FieldKey> builder().addAll(FieldUtils.keysFromAbsolute(fields)).build();
	}

	/**
	 * @see #containsCondition(String)
	 * @param key
	 * @return
	 */
	public Boolean containsCondition(FieldKey key) {
		return containsCondition(key.getValue());
	}

	public Boolean hasConditions() {
		return !CollectionUtils.isEmpty(this.getConditions());
	}

	public Boolean hasOptions() {
		return CollectionUtils.isNotEmpty(options);
	}

	public Boolean hasOrderBy() {
		return CollectionUtils.isNotEmpty(orderBy);
	}

	/**
	 * 
	 * @param key
	 * @return true if one or more conditions exists with the given key.
	 */
	public Boolean containsCondition(String key) {
		List<FilterCondition> conditions = getConditions();
		if (conditions != null) {
			// exits in the middle of processing if key is found.
			for (FilterCondition<?> condition : conditions) {
				if (condition.getField().equalsIgnoreCase(key)) {
					return true;
				}
			}
		}
		return false;
	}

	public Collection<FilterCondition> conditions(final FieldKey field) {
		return FilterUtil.conditions(this, field);
	}

	public String getQuery() {
		return query;
	}

	/**
	 * @return the orderBy
	 */
	public List<FilterSort> getOrderBy() {
		return Collections.unmodifiableList(this.orderBy);
	}

	public List<FilterSort> getOrderByInMemory() {
		return this.orderByInMemory;
	}

	/**
	 * Provides true if {@link #orderByNone} is not null and is true. Useful when there is a default
	 * order assigned a client should not automatically add an order if this is true.
	 */
	public Boolean isOrderByNone() {
		return BooleanUtils.isTrue(orderByNone);
	}

	/**
	 * Indicates if a sort order has already been provided. Useful when adding a default if no order
	 * has been provided.
	 * 
	 * @return
	 */
	public Boolean hasSortOrder() {
		return !this.orderBy.isEmpty() && !isOrderByNone();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (query != null) {
			builder.append("query=");
			builder.append(query);
		}
		if (parent != null) {
			builder.append("parent=");
			builder.append(getParent());
		}
		List<FilterCondition> conditions = getConditions();
		if (!CollectionUtils.isEmpty(conditions)) {
			builder.append(" conditions=");
			builder.append(conditions);
		}
		if (hasSearchIndexes()) {
			builder.append(" searchIndexes=");
			builder.append(searchIndexes);
		}
		if (BooleanUtils.isTrue(proximity)) {
			builder.append(" proximity=");
			builder.append(this.proximity);
		}
		if (this.limit != null) {
			builder.append(" limit=");
			builder.append(this.limit);
		}
		if (this.page != null) {
			builder.append(" page=");
			builder.append(this.page);
		}
		if (this.orderBy != null) {
			builder.append(" orderBy=");
			builder.append(this.orderBy);
		}
		if (!CollectionUtils.isEmpty(predicates)) {
			builder.append(" predicates=");
			builder.append(this.predicates);
		}

		if (hasOptions()) {
			builder.append(" with options=");
			builder.append(options);
		}

		return builder.toString();
	}

	/**
	 * Used to construct all instances of Filter.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterBuilder<Builder> {

		public Builder() {
			super();
		}

		public Builder(Filter mutant) {
			super(mutant);

		}

	}// end Builder

	/**
	 * Shorthand for new Builder.
	 * 
	 * @return
	 */
	public static Builder create() {
		return new Builder();
	}

	/**
	 * Allows mutation of the original. Consider using {@link #clone()} to avoid mutation of the
	 * original when not necessary.
	 * 
	 * @deprecated use {@link #clone(Filter)} instead since mutation leads to confusion.
	 * 
	 * @param mutant
	 * @return
	 */
	@Deprecated
	public static Builder mutate(Filter mutant) {
		return new Builder(mutant);
	}

	/**
	 * Avoids mutating an existing filter by creating a {@link #clone()} and providing a Builder
	 * allowing mutations without affecting the original.
	 * 
	 * @see #mutate(Filter)
	 * 
	 * @param copy
	 * @return
	 */
	public static Builder clone(Filter copy) {
		return new Builder(cloneForBuilder(copy));
	}

	/** Intended to be used by other domain specfic builders. */
	public static Filter cloneForBuilder(Filter original) {
		try {
			return (Filter) original.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Convenience classes can extend to provide high level build methods for {@link Filter}.
	 */
	public static abstract class BaseFilterBuilder<BuilderT extends BaseFilterBuilder<BuilderT>>
			extends BaseBuilder<Filter, BuilderT> {

		protected BaseFilterBuilder() {
			super(new Filter());

		}

		/**
		 * When a filter goes across domains, this will strip the fields keeping only the domain
		 * issued.
		 * 
		 * @param mutant
		 * @param domain
		 */
		protected BaseFilterBuilder(Filter mutant, String domain) {
			this(FilterUtil.filterForDomain(mutant, domain));
		}

		protected BaseFilterBuilder(Filter mutant) {
			super(mutant);
			// some frameworks bypass setters and assign null instead of empty lists.
			// undo the unmodifiable list restriction.
			building.conditions = (building.conditions == null) ? new ArrayList<FilterCondition>() : Lists
					.newArrayList(building.conditions);

		}

	}// end Builder

	/**
	 * Used to construct all instances of Filter.
	 */
	@XmlTransient
	public static abstract class BaseBuilder<FilterT extends Filter, BuilderT extends BaseBuilder<FilterT, BuilderT>>
			extends AbstractObjectBuilder<FilterT, BuilderT> {

		/**
		 * This is an unpublished limit by GAE. Although it is GAE imposed, we enforce it here since
		 * an enforcement is value although the value may change based on the datastore.
		 * 
		 */
		public static final Integer MAX_CONDITIONS = 100;
		public static final Integer MAX_VALUES_FOR_IN_OPERATOR = 30;
		private FieldKey idFieldKey;

		public BaseBuilder(FilterT filter) {
			super(filter);
		}

		/**
		 * Some filters commonly cross domains. This will ensure the filter given relates only to
		 * the desired domain.
		 * 
		 * @param domain
		 * @param mutant
		 */
		protected BaseBuilder(FilterT mutant, String domain) {
			this((FilterT) FilterUtil.filterForDomain(mutant, domain));
		}

		public BuilderT setParent(Identifier<?, ? extends BaseEntity> parent) {
			building().parentId = parent;
			if (parent != null) {
				building().parent = parent.toString();
			}
			return dis;
		}

		/**
		 * When only the string value is known, which shouldn't happen outide of the web services
		 * that assigns. This is available to test.
		 * 
		 * @param parentValue
		 * @return
		 */
		BuilderT setParentValue(String parentValue) {
			building().parent = parentValue;
			return dis;
		}

		/**
		 * Provides the opportunity for children to implement the conversion of the parent id value
		 * into the parent id since this does not happen automatically. This is called during
		 * {@link #build()}.
		 * 
		 * @param parent
		 * @return
		 */
		protected Identifier<?, ? extends BaseEntity> convertParent(String parentIdValue) {
			if (building().parentId == null) {
				LoggerFactory.getLogger(getClass()).warning(parentIdValue
						+ " is provided, but parentId is being assigned as null. Override #convertParent?");
			}
			return null;
		}

		public BuilderT setLimit(Integer limit) {
			Assertion.assertNotNull(limit);
			building().limit = Math.max(Math.min(limit, MAX_LIMIT), MIN_LIMIT);
			return dis;
		}

		public BuilderT setBounds(String bounds) {
			building().bounds = bounds;
			return dis;
		}

		/**
		 * intended for trusted use only, {@link Filter#cityCoordinate} is intended to be injected
		 * automatically.
		 * 
		 * @param coordinates
		 * @return
		 */
		BuilderT setCityCoordinates(String coordinates) {
			building().cityCoordinate = coordinates;
			return dis;
		}

		/**
		 * @param b
		 * @return
		 */
		public BuilderT setProximity(Boolean proximity) {
			building().proximity = proximity;
			return dis;
		}

		public BuilderT addPredicate(Predicate<?> predicate) {
			building().predicates.add(predicate);
			return dis;
		}

		public BuilderT ordering(Ordering<?> ordering) {
			building().ordering = ordering;
			return dis;
		}

		/**
		 * @param b
		 */
		public BuilderT setOrderByNone() {
			building().orderByNone = true;
			return dis;
		}

		/**
		 * @param b
		 * @return
		 */
		public BuilderT setAutoPredicates(Boolean autoPredicates) {
			building().autoPredicates = true;
			return dis;
		}

		public BuilderT setAutoLocate(boolean autoLocate) {
			building().autoLocate = autoLocate;
			return dis;
		}

		/**
		 * @param filterCondition
		 * @param filterConditionPredicate
		 */
		public BuilderT replaceCondition(FilterCondition filterCondition, Predicate filterConditionPredicate) {
			boolean remove = building().conditions.remove(filterCondition);
			Assertion.exceptions().assertTrue("condition not found", remove);
			addPredicate(filterConditionPredicate);
			return dis;
		}

		/**
		 * removes the given filter condition replacing with the given replacement. The given target
		 * must have come from {@link Filter#conditions(FieldKey)}.
		 * 
		 * @param filterCondition
		 * @param replacement
		 * @return
		 */
		public BuilderT replaceCondition(FilterCondition filterCondition, FilterCondition replacement) {
			boolean remove = building().conditions.remove(filterCondition);
			Assertion.exceptions().assertTrue("condition not found", remove);
			building().conditions.add(replacement);
			return dis;
		}

		/**
		 * attempts to remove the condition that came from the
		 * 
		 * @param filterCondition
		 * @return
		 */
		public Collection<FilterCondition> removed(FieldKey fieldKey) {
			Collection<FilterCondition> found = building.conditions(fieldKey);
			building().conditions.removeAll(found);
			return found;
		}

		/**
		 * Useful after {@link #clone()}, this will clear out all conditions assuming more are
		 * coming to be added.
		 * 
		 * @return
		 * 
		 */
		public BuilderT resetConditions() {
			building().conditions = new ArrayList<>();
			return dis;
		}

		public BuilderT resetOrderBy() {
			building().orderBy = new ArrayList<>();
			return dis;
		}

		/**
		 * Page cursor can be problematic when applied to the wrong domain. Use this to clear it out
		 * if appropriate.
		 * 
		 * @return
		 */
		public BuilderT resetPageCursor() {
			return this.setPageCursor(null);
		}

		/**
		 * To be used carefully in special situations.
		 * 
		 * TN-501
		 * 
		 * @return
		 */
		protected BuilderT setNoLimit() {
			building().hasLimit = false;
			building().limit = null;
			return dis;
		}

		/**
		 * Provides batch size choices for big query situations. TN-501
		 * 
		 * @param chunkSize
		 * @return
		 */
		protected BuilderT setChunkSize(Integer chunkSize) {
			building().chunkSize = chunkSize;
			return dis;
		}

		/**
		 * Set's the limit is one hasn't already been given.
		 * 
		 * @param limit
		 * @return
		 */
		public BuilderT setLimitIfDefault(Integer limit) {

			Integer presetLimit = building().limit;
			if (presetLimit == null || presetLimit.equals(DEFAULT_LIMIT)) {
				setLimit(limit);
			}
			return dis;
		}

		public BuilderT setLimitToMax() {
			setLimit(MAX_LIMIT);
			return dis;
		}

		public BuilderT setLimitToNone() {
			setNoLimit();
			return dis;
		}

		public BuilderT setPage(Integer page) {
			building().page = page;
			return dis;
		}

		public BuilderT incrementPage() {
			building().page = building.getPage() + 1;
			return dis;
		}

		public BuilderT setPageTotal(Integer total) {
			building().pageTotal = total;
			return dis;
		}

		public BuilderT filteredByPredicates(Integer filteredByPredicates) {
			building().filteredByPredicates = filteredByPredicates;
			return dis;
		}

		public BuilderT setPageCursor(String cursor) {
			building().pageCursor = cursor;
			return dis;
		}

		/**
		 * For a field of type string or collection of string this will set a filter that will look
		 * for a string that starts with the search string given.
		 * 
		 * @param fieldKey
		 * @param searchString
		 * @return
		 */
		public BuilderT startsWith(final FieldKey fieldKey, String searchString) {
			final String HIGH_VALUE_ASCII_CHARACTER = "~";
			return addCondition(FilterCondition.create().field(fieldKey)
					.operator(FilterOperator.GREATER_THAN_OR_EQUALS).value(searchString).build())
					.addCondition(FilterCondition.create().field(fieldKey).operator(FilterOperator.LESS_THAN)
							.value(searchString + HIGH_VALUE_ASCII_CHARACTER).build())

			;
		}

		/**
		 * @return
		 */
		protected Integer numberOfConditionsAllowed() {
			// added one extra for cushion
			return MAX_CONDITIONS - numberOfConditions() - 1;
		}

		/**
		 * Adds a general {@link FilterCondition} that will be used to limit the results.
		 */
		public final BuilderT addCondition(FilterCondition<?> condition) {
			final List<FilterCondition> all = conditions();
			if (numberOfConditions() >= MAX_CONDITIONS) {
				throw InvalidArgumentException.limitExceeded(MAX_CONDITIONS);
			}
			all.add(condition);
			return dis;
		}

		public final BuilderT addPredicateCondition(FilterCondition<?> condtion) {
			if (building().predicateConditions == null) {
				building().predicateConditions = Lists.newArrayList();
			}
			building().predicateConditions.add(condtion);
			return dis;
		}

		public final BuilderT addOption(String option) {
			if (building().options == null) {
				building().options = new ArrayList<>();
			}
			building().options.add(option);
			return dis;
		}

		public final BuilderT addExpand(FieldKey fieldKey) {
			if (building().expand == null) {
				building().expand = new ArrayList<>();
			}
			building().expand.add(fieldKey.getAbsoluteKey());
			return dis;
		}

		public BuilderT addField(String fieldKey) {
			if (building().fields == null) {
				building().fields = new ArrayList<>();
			}
			building().fields.add(fieldKey);
			return dis;

		}

		public BuilderT addField(FieldKey fieldKey) {
			return addField(fieldKey.getAbsoluteKey());

		}

		/**
		 * Wipes out the current list immediately before returning so a new one can be added if
		 * desired.
		 * 
		 * @return
		 */
		public BuilderT clearSearchIndexes() {
			building().searchIndexes = Lists.newArrayList();
			return dis;
		}

		/**
		 * @see Filter#searchIndexes
		 * @param indexes
		 * @return
		 */
		public final BuilderT addSearchIndex(String... indexes) {
			if (building().searchIndexes == null) {
				building().searchIndexes = new ArrayList<>();
			}
			building().searchIndexes.addAll(ImmutableList.<String> builder().add(indexes).build());
			return dis;
		}

		public final BuilderT addOptions(Collection<String> options) {
			if (building().options == null) {
				building().options = new ArrayList<>();
			}
			building().options.addAll(options);
			return dis;
		}

		/**
		 * @return
		 */
		protected List<FilterCondition> conditions() {
			if (building().conditions == null) {
				building().conditions = new ArrayList<>();
			}
			return building().conditions;
		}

		/**
		 * @return
		 */
		private Filter building() {
			return building;
		}

		/**
		 * Simply provides the number of conditions for Builders to work within the limit of
		 * {@link #MAX_CONDITIONS}
		 * 
		 * @return
		 */
		protected final Integer numberOfConditions() {
			final List<FilterCondition> conditions = conditions();
			if (conditions == null) {
				return 0;
			}
			return conditions.size();
		}

		/** Order by the standard create date with the most recent first. */
		public final BuilderT orderByMostRecentlyCreated() {
			return orderBy(dateCreatedFieldKey(), Direction.DESCENDING);
		}

		/**
		 * Removes any order by regardless who set it and when.
		 * 
		 * @see #setOrderByNone()
		 * @see Filter#orderByNone
		 * */
		public final BuilderT orderByNone() {
			setOrderByNone();
			return dis;
		}

		public final BuilderT orderByMostRecentlyUpdated() {
			return orderBy(dateUpdatedFieldKey(), Direction.DESCENDING);
		}

		public BuilderT orderBy(FilterSort orderBy) {
			building().orderBy.add(orderBy);
			return dis;
		}

		/**
		 * @param filterSort
		 * @return
		 */
		public BuilderT orderByInMemory(FilterSort filterSort) {
			if (building().orderByInMemory == null) {
				building().orderByInMemory = Lists.newArrayList();
			}
			building().orderByInMemory.add(filterSort);
			return dis;
		}

		/** Orders by the given key in default ascending order. */
		public BuilderT orderBy(FieldKey key) {
			this.orderBy(FilterSort.create().setFieldKey(key).build());
			return dis;
		}

		/** Orders by the given key in the given direction */
		public BuilderT orderBy(FieldKey key, Direction direction) {
			this.orderBy(FilterSort.create().setFieldKey(key).setDirection(direction).build());
			return dis;
		}

		public BuilderT setQuery(String query) {
			building().query = query;
			return dis;
		}

		/**
		 * Adds a {@link FilterCondition} ensuring the entity identified by the given
		 * {@link Identifier} will not be part of the results.
		 * 
		 * @param id
		 * @return
		 */
		public BuilderT idNotEqualTo(Identifier<?, ?> id) {
			FieldKey key = idFieldKey(id);
			return addCondition(new FilterCondition.Builder<Identifier<?, ?>>().field(key)
					.operator(FilterOperator.NOT_EQUALS).value(id).build());
		}

		/**
		 * Adds a {@link FilterCondition} ensuring the entity identified by the given
		 * {@link Identifier} will not be part of the results.
		 * 
		 * @param id
		 * @return
		 */
		public BuilderT idEqualTo(Identifier<?, ?> id) {
			FieldKey key = idFieldKey(id);
			return addCondition(new FilterCondition.Builder<Identifier<?, ?>>().field(key)
					.operator(FilterOperator.EQUALS).value(id).build());
		}

		/**
		 * Given a collection of ids this will filter for any entities that match an id given.
		 * 
		 * @param ids
		 * @return
		 */
		public BuilderT idsIn(Iterable<? extends Identifier<?, ?>> ids) {

			if (!Iterables.isEmpty(ids)) {
				FieldKey key = idFieldKey(ids.iterator().next());
				addCondition(FilterCondition.create().field(key).operator(FilterOperator.IN).value(ids).build());
			}
			return dis;
		}

		/** Creates the field key for create date for the implementing domain. */
		public FieldKey dateCreatedFieldKey() {
			return new FieldKey(getDomain(), BaseEntity.BASE_FIELD.DATE_CREATED);
		}

		/** Created the standard field key for updated date for the implemented domain. */
		public FieldKey dateUpdatedFieldKey() {
			return new FieldKey(getDomain(), BaseEntity.BASE_FIELD.DATE_UPDATED);
		}

		/**
		 * Adds a condition filtering on update date for anything updated since the time given.
		 * 
		 * @param since
		 *            is the earliest time an update could have happened to be considered in this
		 *            query
		 */
		public BuilderT updatedSince(DateTime since) {
			return addCondition(FilterCondition.create().field(dateUpdatedFieldKey())
					.operator(FilterOperator.GREATER_THAN).value(since).build());
		}

		/**
		 * Provides the Domain represented by the Builder allowing for general keys to be created
		 * for conditions and orders.
		 * 
		 * @return
		 */
		public String getDomain() {
			throw new NotImplementedException(
					"your builder must override and provide the domain.  Make this abstract and apply everywhere");
		}

		/**
		 * Given an id, this will return the appropriate {@link FieldKey} for that id. It's better
		 * to override this for efficiency sake, otherwise, this must look it up in the dictionary.
		 * 
		 * @param id
		 * @return
		 */
		public FieldKey idFieldKey(Identifier<?, ?> id) {
			if (this.idFieldKey == null) {
				this.idFieldKey = FieldAnnotationDictionaryFactory.keyFor(id.getKind(), BaseEntity.BASE_FIELD.ID);
			}
			return idFieldKey;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Integer total = building.getPageTotal();

			if (building.hasPredicates() && building().filteredByPredicates == null && building().pageTotal != null) {
				Assertion.exceptions().notNull("filteredByPredicates", building().filteredByPredicates);

			}
			if (total != null && building.hasLimit()) {

				if (total > building.getLimit()) {
					throw new IllegalStateException(total + " pageTotal can't be more than the limit :"
							+ building.getLimit() + ".  You likely aren't managing limit properly");
				}
			}
			// Page cannot be 0 or less else all the math goes bonkers.
			Assertion.exceptions().assertTrue(building.getPage() > 0);
			super.validate();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public FilterT build() {

			building().options = FilterUtil.listParsed(building().options);
			building().fields = FilterUtil.listParsed(building().fields);
			building().searchIndexes = FilterUtil.listParsed(building().searchIndexes);
			building().expand = FilterUtil.listParsed(building().expand);
			if (building().isOrderByNone()
					|| (CollectionUtils.isNotEmpty(building().orderBy) && building().orderBy.contains(ORDER_BY_NONE))) {
				this.resetOrderBy();
			}
			if (building().hasParent() && building().parentId == null) {
				setParent(this.convertParent(building().parent));
			}

			if (building().hasOrderBy()) {
				building().orderBy = SetUniqueList.setUniqueList(building().orderBy);
			}

			// this is here to avoid building it every time...since city coordinates will be
			// available
			// with every call, but many won't need it
			if (!building().hasBounds() && building().hasCityCoordinates() && building().isAutoLocate()) {
				building().bounds = BoundingBoxUtil.boundedBy(	building().getCityCoordinate(),
																building().getCityCoordinatesRadius()).getBounds();
			}
			final FilterT built = super.build();
			return built;

		}

		/**
		 * The resulting noun synonym for the verb {@link #build()}.
		 * 
		 * @return
		 */
		public Filter filter() {
			return build();
		}

	}// end Builder

	/**
	 * Simple builder to assist with creating {@link FilterCondition}s only if the value is not
	 * null.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlTransient
	static public class Conditions {
		private ArrayList<FilterCondition<?>> list = new ArrayList<FilterCondition<?>>();

		private Conditions() {
		}

		/**
		 * synonymous with {@link #add(String, FilterOperator, Object)}, but kicks off the building.
		 * 
		 * @param field
		 * @param operator
		 * @param value
		 * @return
		 */
		public static Conditions create(String field, FilterOperator operator, Object value) {
			Conditions conditions = new Conditions();
			conditions.add(field, operator, value);
			return conditions;
		}

		public Conditions add(String field, FilterOperator operator, Object value) {
			if (value != null) {
				list.add(new FilterCondition.Builder<Object>().field(field).operator(operator).value(value).build());
			}
			return this;
		}

		public List<FilterCondition<?>> build() {
			return Collections.unmodifiableList(list);
		}

	}

	/**
	 * @return
	 */
	public Boolean hasChunkSize() {
		return this.chunkSize != null;
	}

	@XmlTransient
	public static final class FIELD {
		public static final String SEARCH_INDEXES = "searchIndexes";
		public static final String FIELDS = "fields";
		public static final String PROXIMITY = "proximity";
		public static final String PREDICATE_CONDITIONS = "predicateConditions";
		// also defined in BoundingBox and GeoWebApiUri, but no access
		public static final String BOUNDS = "bounds";
		public static final String NEAR = "near";
		public static final String OPTIONS = "options";
		/** Indicates an identifier field is to be expanded into it's corresponding entity */
		public static final String EXPAND = "expand";
		public static final String ORDER_BY = "orderBy";
		public static final String CONDITIONS = "conditions";
		public static final String PAGE = "page";
		public static final String LIMIT = "limit";
		public static final String QUERY = "query";
	}

	/**
	 * @param string
	 * @return
	 */
	public Boolean containsOption(String option) {
		return this.options != null && this.options.contains(option);
	}

	/**
	 * indicates if the expand field key has been provided
	 * 
	 * @param fieldKey
	 * @return
	 */
	public Boolean containsExpand(String fieldKey) {
		return this.expand != null && this.expand.contains(fieldKey);
	}

	/**
	 * @see #containsExpand(String)
	 * 
	 * @param fieldKey
	 * @return
	 */
	public Boolean containsExpand(FieldKey fieldKey) {
		return containsExpand(fieldKey.getAbsoluteKey());
	}

	/**
	 * @return
	 */
	public Ordering<?> getOrdering() {
		return this.ordering;
	}

	/**
	 * Indicates if proximity has been assigned and is true.
	 * 
	 * @see #proximity
	 * @return
	 */
	public Boolean isProximity() {
		return BooleanUtils.isTrue(proximity);
	}

	/**
	 * Provides the condition identified by the fieldKey. It assumes there is one and only one so
	 * any other situation will cause an error. Use {@link #conditions(FieldKey)} if you want that
	 * flexibility.
	 * 
	 * @param fieldKey
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> FilterCondition<T> condition(FieldKey fieldKey) {
		return Iterables.getOnlyElement(conditions(fieldKey));
	}

}
