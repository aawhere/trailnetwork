/**
 * 
 */
package com.aawhere.persist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.FilterConditionXmlAdapter.FilterConditionXml;
import com.aawhere.xml.XmlNamespace;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Transforms a {@link FilterCondition} into {@link FilterConditionXml} which contains transport
 * friendly strings.
 * 
 * @author aroller
 * 
 */
@Singleton
public class FilterConditionXmlAdapter
		extends XmlAdapter<FilterConditionXml, FilterCondition> {

	/**
	 * 
	 */
	private static final String STRING_ADAPTER_FIELD = "stringAdapter";
	private FilterConditionStringAdapter stringAdapter;

	/**
	 * Used to construct all instances of FilterConditionXmlAdapter.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<FilterConditionXmlAdapter> {

		public Builder() {
			super(new FilterConditionXmlAdapter());
		}

		public Builder setStringAdapter(FilterConditionStringAdapter adapter) {
			building.stringAdapter = adapter;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull(STRING_ADAPTER_FIELD, building.stringAdapter);
			super.validate();
		}
	}// end Builder

	private FilterConditionXmlAdapter() {
		// LoggerFactory.getLogger(getClass()).error("someone is calling the default constructor");
	}

	@Inject
	/** Use {@link Builder} to construct FilterConditionXmlAdapter */
	public FilterConditionXmlAdapter(FilterConditionStringAdapter adapter) {
		this.stringAdapter = adapter;
		Assertion.assertNotNull(STRING_ADAPTER_FIELD, adapter);
	}

	@XmlType(name = "FilterCondition", namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class FilterConditionXml {
		String field;
		FilterOperator operator;
		String value;
		String expression;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public FilterConditionXml marshal(FilterCondition condition) throws Exception {
		FilterConditionXml xml;
		if (condition != null) {
			xml = new FilterConditionXml();
			xml.field = condition.getField();
			xml.operator = condition.getOperator();
			if (stringAdapter.handles(condition)) {
				xml.value = stringAdapter.marshalValue(condition);
				xml.expression = stringAdapter.marshal(condition);
			} else {
				// must we do a hard stop here? toString may handle those unhandled situations
				// throw new IllegalStateException(condition + " does not have an adapter");
				xml.value = String.valueOf(condition.getValue());
				xml.expression = String.valueOf(condition);
			}
		} else {
			xml = null;
		}
		return xml;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public FilterCondition unmarshal(FilterConditionXml xml) throws Exception {
		return stringAdapter.unmarshal(xml.expression, null);
	}
}
