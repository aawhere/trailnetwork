/**
 * 
 */
package com.aawhere.persist;

/**
 * Common interface for those classes that have a {@link Filter}.
 * 
 * @author aroller
 * 
 */
public interface FilterProvider {

	Filter getFilter();
}
