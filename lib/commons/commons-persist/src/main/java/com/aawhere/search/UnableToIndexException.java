/**
 *
 */
package com.aawhere.search;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

/**
 * Not able to index the supplied document.
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(value = HttpStatusCode.INTERNAL_ERROR)
public class UnableToIndexException
		extends BaseRuntimeException {

	/**
	 * @param like
	 */
	public UnableToIndexException(Class<?> type) {
		super(type.getName() + " must have @Searchable defined on the class.");
	}

	private static final long serialVersionUID = -4148531382516517749L;

}
