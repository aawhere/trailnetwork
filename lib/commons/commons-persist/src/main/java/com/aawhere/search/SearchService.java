/**
 *
 */
package com.aawhere.search;

import java.util.List;

import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Repository;

/**
 * ServiceStandard for interacting with Search system. Primary methods to use:
 * 
 * <ul>
 * <li>searchDocuments(filter) - the standard search providing search documents matching the filter</li>
 * <li>searchDocumentIds(filter) - ids only response for fast and efficient calls</li>
 * <li>index(searchDocuments) - indexing of one or more documents to be found in searchDocuments</li>
 * <li>delete(filter) - removes those documents matching the filter
 * </ul>
 * 
 * All requests are provided in the {@link Filter} with the following specialties and limitations
 * compared to a {@link Repository}.
 * 
 * <ul>
 * <li>Search Index targeted -An index is like a database table where like entities (documents) are
 * persisted and available for query. Provided by {@link Filter#getSearchIndexes()}</li>
 * <li>Sort options are ignored</li>
 * <li>Location - {@link Filter#getBounds()} and other location query use a geo point for more
 * accurate geo searching</li>
 * <li>Text - {@link Filter#getQuery()} will do full text search on qualifying fields</li>
 * <li>Fields - {@link Filter#getFields()} returns only specific fields identified in the query.
 * Fewer fields is faster!!!</li>
 * <li> {@link Filter#get}</li>
 * 
 * @author Brian Chapman
 * 
 */
public interface SearchService {

	/**
	 * Provides {@link SearchDocuments} matching the filter using the
	 * {@link Filter#getSearchIndexes()} provided. Support of all filter operations is limited as
	 * explained on this class. The resulting SearchDocuments is a "connected" instance that uses
	 * lazy loading so the search can continue in parallel until the client makes a request on any
	 * of the blocking methods:
	 * 
	 * <ul>
	 * <li> {@link SearchDocuments#getSearchDocuments()}</li>
	 * <li> {@link SearchDocuments#iterator().next()}</li>
	 * <li></li>
	 * <li> {@link SearchDocuments#size()}</li>
	 * <li> {@link SearchDocuments#getFilter()}</li>
	 * </ul>
	 * 
	 * Since {@link SearchDocuments} implements {@link Iterable} and is non-blocking there is no
	 * need to return an {@link Iterable} directly.
	 * 
	 * @param filter
	 * @return
	 */
	public SearchDocuments searchDocuments(Filter filter);

	/**
	 * @deprecated use {@link #searchDocumentIds(Filter)} setting the
	 *             {@link Filter#getSearchIndexes()}
	 * */
	@Deprecated
	public Iterable<String> searchDocumentIds(Filter filter, String indexName) throws UnknownIndexException;

	/**
	 * Given the filter that may apply to one or more index this will return only the ids matching
	 * the filter. This is an efficient way to retrieve the id costing less money and processing.
	 * The resulting iterable may be connected to take advantage of asynchronous calls, but the
	 * iterable will behave normally abstracting these details from the client. *
	 * 
	 * @param filter
	 * @return
	 * @throws UnknownIndexException
	 */
	public Iterable<String> searchDocumentIds(Filter filter) throws UnknownIndexException;

	/**
	 * Find all {@link Field}s in the instance (including all Fields contained in the types
	 * represented by {@link java.lang.reflect.Field}s contained in the instance, recursively) and
	 * add them to the index identified by the {@link Field}'s domain.
	 * 
	 * @param indexable
	 *            any object annotated with {@link Dictionary} and {@link Field} annotations
	 * @throws MaxIndexAttemptsExceeded
	 */
	public void index(SearchDocument... searchDocuments) throws UnableToIndexException, MaxIndexAttemptsExceeded;

	/**
	 * @See {@link #index(Object)}
	 * 
	 * @param indexables
	 * @throws UnableToIndexException
	 * @throws MaxIndexAttemptsExceeded
	 */
	public void index(Iterable<? extends SearchDocument> indexables) throws UnableToIndexException,
			MaxIndexAttemptsExceeded;

	/**
	 * Given a stream of ids and an index name this will remove those items identified (without
	 * confirmation of success).
	 * 
	 * @param ids
	 * @param indexName
	 */
	void delete(Iterable<String> ids, String indexName);

	/**
	 * Required that the {@link Filter#hasSearchIndexes()} this will delete and results from each
	 * index that matches the results of the filter using {@link #searchDocumentIds(Filter, String)}
	 * .
	 * 
	 * @param filter
	 * @throws FieldNotSearchableException
	 * @throws UnknownIndexException
	 */
	void delete(Filter filter) throws FieldNotSearchableException, UnknownIndexException;

	/**
	 * Query the index using a filter.
	 * 
	 * @deprecated use {@link #searchDocuments(Filter)}
	 * @param filter
	 * @param index
	 * @return
	 * @throws FieldNotRegisteredException
	 * @throws FieldNotSearchableException
	 * @throws UnknownIndexException
	 */
	@Deprecated
	public SearchDocuments query(Filter filter, String index) throws FieldNotSearchableException, UnknownIndexException;

	/**
	 * @deprecated use {@link #searchDocuments(Filter)}
	 * @param filter
	 * @param indexes
	 * @return
	 * @throws FieldNotSearchableException
	 * @throws UnknownIndexException
	 */
	@Deprecated
	public SearchDocuments query(Filter filter, List<String> indexes) throws FieldNotSearchableException,
			UnknownIndexException;

	/**
	 * Delete a document from the search index.
	 * 
	 * @param identifier
	 *            The document identifier
	 * @param index
	 *            the index to delete from
	 */
	public void delete(String documentId, String index);

	/**
	 * Delete a document from the search index.
	 * 
	 * @see SearchDocument#index()
	 * @see SearchDocument#id()
	 * @see #delete(String, String)
	 * 
	 * @param document
	 */
	public void delete(SearchDocument document);

	/**
	 * Deletes all the documents given.
	 * 
	 * @param documents
	 */
	public void delete(Iterable<SearchDocument> documents);

}
