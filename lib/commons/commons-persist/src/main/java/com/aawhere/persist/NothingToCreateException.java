/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Thrown when a create method is called, but there is nothing to create because the system is up to
 * date.
 * 
 * This should not be thrown when an attempt to create results in no products, but rather when a
 * pre-check results in an answer that a request to create might be a repeat of a previous request.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.NO_CONTENT)
public class NothingToCreateException
		extends BaseException {

	private static final long serialVersionUID = 6937027348674242623L;

	/**
	 * 
	 */
	public NothingToCreateException() {
		super(new CompleteMessage.Builder(PersistMessage.NOTHING_TO_CREATE).build());
	}
}
