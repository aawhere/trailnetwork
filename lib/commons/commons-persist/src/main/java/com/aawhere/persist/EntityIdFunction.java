/**
 * 
 */
package com.aawhere.persist;

import javax.annotation.Nullable;

import com.aawhere.id.Identifier;

import com.google.common.base.Function;

/**
 * Used to transform a {@link BaseEntity} into it's {@link Identifier}.
 * 
 * @author aroller
 * 
 */
public class EntityIdFunction<K extends BaseEntity<K, I>, I extends Identifier<?, K>>
		implements Function<K, I> {

	public static <K extends BaseEntity<K, I>, I extends Identifier<?, K>> EntityIdFunction<K, I> build() {
		return new EntityIdFunction<>();
	}

	/** Use {@link Builder} to construct EntityIdFunction */
	public EntityIdFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public I apply(@Nullable K input) {
		return input.getId();
	}

}
