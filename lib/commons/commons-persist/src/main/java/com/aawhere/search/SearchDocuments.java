/**
 *
 */
package com.aawhere.search;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.Filter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * A collection of {@link SearchDocument}
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
@XmlType(namespace = XmlNamespace.API_VALUE)
public class SearchDocuments
		implements Iterable<SearchDocument>, Serializable {

	private static final long serialVersionUID = -6637904947061931901L;

	/**
	 * Used to construct all instances of SearchDocuments.
	 */
	public static class Builder
			extends ObjectBuilder<SearchDocuments> {

		public Builder() {
			super(new SearchDocuments());
		}

		/**
		 * List is required to properly transform in JAXB (rather than Iterable). It also makes for
		 * a predictable results. Use {@link Lists#transform(List, com.google.common.base.Function)}
		 * to create this list allowing this to be transported while the search continues.
		 * 
		 * @param documents
		 * @return
		 */
		public Builder searchDocuments(Iterable<SearchDocument> documents) {
			Assertion.exceptions().notNull("documents", documents);
			Assertion.exceptions().assertNull("documents already exist", building.documents);

			building.documents = documents;
			return this;

		}

		public Builder add(SearchDocument searchDocument) {
			return addAll(ImmutableList.of(searchDocument));
		}

		public Builder addAll(Iterable<SearchDocument> searchDocuments) {
			if (building.documents == null) {
				building.documents = Lists.newArrayList();
			}
			if (building.documents instanceof List) {
				Iterables.addAll((List<SearchDocument>) building.documents, searchDocuments);
			} else {
				throw new IllegalStateException("an iterable was already assigned " + building.documents);
			}
			return this;
		}

		/**
		 * When you have a concrete filter to share that does not need lazy calls to a service (i.e.
		 * in testing).
		 * 
		 * @see #filter(Supplier)
		 * @param filter
		 * @return
		 */
		public Builder filter(Filter filter) {
			return filter(Suppliers.ofInstance(filter));
		}

		public Builder filter(Supplier<Filter> filter) {
			building.filter = filter;
			return this;
		}

		@Override
		public SearchDocuments build() {

			SearchDocuments built = super.build();
			if (built.documents == null) {
				// fine...you didn't find anything so theere is nothing wrong with an empty list.
				built.documents = ImmutableList.of();
			}

			return built;
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct SearchDocuments */
	private SearchDocuments() {
	}

	private Iterable<SearchDocument> documents;

	/**
	 * This allows asynchronous processing, but will block when someone calls for a filter or
	 * anything dependent upon filter like {@link #size()}.
	 * 
	 */
	private Supplier<Filter> filter;

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<SearchDocument> iterator() {
		return documents.iterator();
	}

	public Integer size() {
		return getSearchDocuments().size();
	}

	/**
	 * Provides an xml transformable list of the {@link #documents}. This forces the search results
	 * to finish if they are still processing. The list is created lazy upon calling this method and
	 * is thread safe.
	 * 
	 * @return
	 */
	@XmlElement
	public List<SearchDocument> getSearchDocuments() {

		if (documents != null) {
			synchronized (documents) {
				if (!(this.documents instanceof List)) {
					this.documents = ImmutableList.<SearchDocument> builder().addAll(documents).build();
				}
			}
		}
		return (List<SearchDocument>) this.documents;
	}

	/**
	 * Provides access to the filter without blocking until {@link Supplier#get()}is called.
	 * 
	 * @return
	 */
	public Supplier<Filter> filterSupplier() {
		return this.filter;
	}

	/**
	 * @return the filter
	 */
	@XmlElement
	public Filter getFilter() {
		return filter.get();
	}

	/**
	 * Notice..this blocks
	 * 
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(getClass().getSimpleName());
		builder.append(" with ");
		builder.append(size());
		builder.append(" document");
		if (filter != null) {
			builder.append(" for ");
			builder.append(getFilter());
		}
		return builder.toString();
	}

}
