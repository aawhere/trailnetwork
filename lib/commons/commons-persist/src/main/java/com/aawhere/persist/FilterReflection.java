/**
 *
 */
package com.aawhere.persist;

import com.aawhere.lang.reflect.BuilderReflection;

/**
 * Useful helpers for constructing {@link BaseFilter} using their {@link BaseFilter.Builder}.
 * 
 * @author Brian Chapman
 * 
 */
public class FilterReflection<FilterT extends Filter, BuilderT extends Filter.BaseBuilder<FilterT, BuilderT>>
		extends BuilderReflection<FilterT, BuilderT> {

	public static <FilterT extends Filter, BuilderT extends Filter.BaseBuilder<FilterT, BuilderT>>
			FilterReflection<FilterT, BuilderT> newInstance() {
		return new FilterReflection<FilterT, BuilderT>();
	}
}
