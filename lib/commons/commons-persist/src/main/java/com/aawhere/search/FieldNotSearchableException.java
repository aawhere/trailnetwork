/**
 *
 */
package com.aawhere.search;

import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldMessage;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Identifies that the given field has not been registered. This may be useful for developers during
 * system coding, but also useful for web services responding to requests by keys. Internal
 * developers need not catch this because they should be using keys that are known to work and would
 * unlikely handle a recovery so it's a runtime.
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class FieldNotSearchableException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 6539078530579582848L;

	/**
	 * @param key
	 * @param keySet
	 */
	public FieldNotSearchableException(FieldKey key, Set<FieldKey> keySet) {
		super(CompleteMessage.create(FieldMessage.FIELD_NOT_REGISTERED)
				.addParameter(FieldMessage.Param.FIELD_KEY, key)
				// TODO: add possible keys to result
				.addParameter(	FieldMessage.Param.FIELD_KEYS,
								StringUtils.join(new TreeSet(keySet), System.lineSeparator())).build());
	}

}
