/**
 *
 */
package com.aawhere.persist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class FilterConditions
		implements Iterable<FilterCondition<?>> {

	@XmlElement(name = "condition")
	public List<FilterCondition<?>> conditions = new ArrayList<FilterCondition<?>>();

	public void addCondition(FilterCondition<?> condition) {
		conditions.add(condition);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<FilterCondition<?>> iterator() {
		return conditions.iterator();
	}
}
