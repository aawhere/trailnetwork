/**
 * 
 */
package com.aawhere.persist;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nullable;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.id.Identifier;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.NotImplementedException;
import com.aawhere.lang.ObjectBuilderMapEntryTransformer;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.FromString;
import com.aawhere.persist.Filter.Builder;
import com.aawhere.persist.FilterSort.Direction;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class FilterUtil {

	/** Used in string-based lists to separate items. */
	static final char LIST_SEPARATOR = '|';
	static final char LIST_SEPARATOR_STANDARD = ',';

	/**
	 * Providing the field for the Filter this will return all conditions matching that key.
	 * 
	 * @param filter
	 * @param field
	 * @return
	 */
	public static Collection<FilterCondition> conditions(Filter filter, final FieldKey field) {
		List<FilterCondition> conditions = filter.getConditions();
		return conditions(field, conditions);
	}

	/**
	 * Provides any condition from the given list that has a condition with the given field key.
	 * 
	 * @param field
	 * @param conditions
	 * @return
	 */
	public static Collection<FilterCondition> conditions(final FieldKey field, Collection<FilterCondition> conditions) {
		return Collections2.filter(conditions, FilterConditionFieldPredicate.create().field(field).build());
	}

	/**
	 * Given a single filter with keys from multiple domains this will return a list of filters
	 * representing each domain.
	 * 
	 * @param filter
	 * @return
	 * @throws FieldNotRegisteredException
	 */
	public static Map<String, Filter> filtersByDomain(final Filter filter) {
		final HashMap<String, Filter.Builder> filterBuilders = new HashMap<>();

		/** Constructs an appropriate builder given the domain . */
		Function<String, Filter.Builder> builderFunction = new Function<String, Filter.Builder>() {
			@Override
			public Builder apply(String field) {

				String domainKey = FieldUtils.key(field).getDomainKey();

				Builder builder = filterBuilders.get(domainKey);
				if (builder == null) {
					builder = Filter.clone(filter);
					filterBuilders.put(domainKey, builder);
					builder.resetConditions();
					builder.resetOrderBy();
					builder.resetPageCursor();
				}
				return builder;
			}
		};
		List<FilterCondition> conditions = filter.getConditions();
		for (FilterCondition condition : conditions) {
			Builder builder = builderFunction.apply(condition.getField());
			builder.addCondition(condition);
		}

		// copy avoids concurrent modification after cloning for some reason
		List<FilterSort> orders = ImmutableList.copyOf(filter.getOrderBy());
		for (FilterSort filterSort : orders) {
			Builder builder = builderFunction.apply(filterSort.getField());
			builder.orderBy(filterSort);
		}

		return Maps.transformEntries(	filterBuilders,
										new ObjectBuilderMapEntryTransformer<String, Filter.Builder, Filter>());

	}

	/**
	 * internal method used by paginator methods to retrieve activities efficiently.
	 * 
	 * @param filter
	 * @return
	 */
	public static Filter unrestricted(@Nullable Filter filter) {
		int numberOfRecordsPerPage = 1000;

		Builder filterBuilder;
		if (filter == null) {
			filterBuilder = Filter.create();
		} else {
			filterBuilder = Filter.clone(filter);
		}
		Filter unrestrictedFilter = filterBuilder.setNoLimit().setChunkSize(numberOfRecordsPerPage).build();
		return unrestrictedFilter;
	}

	/** Helper method to allow general values to find their way to the proper predicate method. */
	public static <C extends Comparable<C>> Predicate<C> predicate(FilterOperator operator, Object value) {
		if (value instanceof Comparable || value == null) {
			return predicate(operator, (C) value);
		} else if (value instanceof Iterable) {
			return predicate(operator, (Iterable<C>) value);
		} else if (value instanceof Object[]) {
			return predicate(operator, (C[]) value);
		} else {
			throw new InvalidChoiceException(value.getClass(), new Class[] { Comparable.class, Iterable.class,
					Array.class });
		}
	}

	/**
	 * Given the operator and the value that operator is to act upon this will return the
	 * appropriate predicate that can be used with {@link Iterables#filter(Iterable, Predicate)}.
	 * 
	 * @param operator
	 * @param value
	 * @return
	 * @throws UnsupportedFilterOperatorException
	 *             when an operator is given that is not supported for the value
	 */
	public static <C extends Comparable<C>> Predicate<C> predicate(FilterOperator operator, C value)
			throws UnsupportedFilterOperatorException {

		final Predicate<C> operatorPredicate;
		switch (operator) {
			case IN:
			case EQUALS:
				operatorPredicate = Predicates.equalTo(value);
				break;
			case GREATER_THAN:
				UnsupportedFilterOperatorException.nullNotSupported(operator, value);
				operatorPredicate = Range.greaterThan(value);
				break;
			case GREATER_THAN_OR_EQUALS:
				UnsupportedFilterOperatorException.nullNotSupported(operator, value);
				operatorPredicate = Range.atLeast(value);
				break;

			case LESS_THAN:
				UnsupportedFilterOperatorException.nullNotSupported(operator, value);
				operatorPredicate = Range.lessThan(value);
				break;
			case LESS_THAN_OR_EQUALS:
				UnsupportedFilterOperatorException.nullNotSupported(operator, value);
				operatorPredicate = Range.atMost(value);
				break;
			case NOT_EQUALS:
				operatorPredicate = Predicates.not(Predicates.equalTo(value));
				break;
			default:
				throw new InvalidChoiceException(FilterOperator.values());
		}

		Predicate<C> result;
		// most predicates don't like nulls so filter first, unless the value is null then go direct
		if (value == null) {
			result = operatorPredicate;
		} else {
			result = Predicates.and(Predicates.notNull(), operatorPredicate);
		}
		return result;
	}

	@SafeVarargs
	public static <C extends Comparable<C>> Predicate<C> predicate(final FilterOperator operator, C... comparables) {
		return predicate(operator, Lists.newArrayList(comparables));
	}

	/**
	 * When given a collection this returns a predicate composition of all values given in the
	 * collection against the operator given.
	 * 
	 * IN/Equals are the same and support null, others use
	 * {@link #predicate(FilterOperator, Comparable)} and do not support null.
	 * 
	 * @param operator
	 * @param comparables
	 * @return
	 */
	public static <C extends Comparable<C>> Predicate<C> predicate(final FilterOperator operator,
			Iterable<C> comparables) {
		switch (operator) {
			case IN:
			case NOT_IN:
				Predicate<C> predicate = Predicates.in(CollectionUtilsExtended.toCollection(comparables));
				if (operator == FilterOperator.NOT_IN) {
					predicate = Predicates.not(predicate);
				}
				return predicate;
			case EQUALS:
			case NOT_EQUALS:
				throw new NotImplementedException(
						"equals and not equals could ensure the value of a collection has equality in accordance with CollectionUtils.isEquals");
			default:
				throw new InvalidChoiceException(operator, FilterOperator.IN, FilterOperator.NOT_IN);
				// the code below is if we wanted to go non-standard and apply other operators to
				// the elements of a collection
				// equals and != would be used for a collection equality
				// Function<C, Predicate<C>> function = new Function<C, Predicate<C>>() {
				//
				// @Override
				// public Predicate<C> apply(C input) {
				// return (input != null) ? predicate(operator, input) : null;
				// }
				// };
				// Iterable<Predicate<C>> predicates = Iterables.transform(comparables, function);
				// predicates = Iterables.filter(predicates, Predicates.notNull());
				// return Predicates.or(predicates);
		}
	}

	/**
	 * Programatically handles filter conditions if the field value extends {@link Comparable}.
	 * 
	 * WARNING: This type of filtering is unlikely to be a complete view of the results nor is it
	 * very efficient.
	 * 
	 * FIXME:Throw an appplication exception when the value is not comparable.
	 * 
	 * @param filterCondition
	 * @param dictionary
	 * @return
	 * @throws FieldNotRegisteredException
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>, C extends Comparable<C>> Predicate<E>
			filterConditionPredicate(final FilterCondition<?> filterCondition, FieldDictionaryFactory dictionary,
					Class<E> entityType) throws FieldNotRegisteredException {

		final FieldDefinition<C> definition = dictionary.findDefinition(filterCondition.getField());
		if (!FieldUtils.definesComparableType(definition)) {
			throw new UnsupportedOperationException(definition + " is not for a comparable");
		}
		final Object right = filterCondition.getValue();

		final Predicate<C> predicate = predicate(filterCondition.getOperator(), right);
		return new Predicate<E>() {

			@Override
			public boolean apply(E input) {
				C left = AnnotatedDictionaryUtils.getValue(definition.getKey(), input);
				return predicate.apply(left);
			}

			/*
			 * (non-Javadoc)
			 * @see java.lang.Object#toString()
			 */
			@Override
			public String toString() {
				return filterCondition.toString();
			}
		};
	}

	/**
	 * Provides a filtered version of the filter containing information relating only to the domain.
	 * If no domain specific information matches the desired domain the original filter is returned
	 * (common for new filters) .
	 * 
	 * @param filter
	 * @param domain
	 * @return
	 */
	public static Filter filterForDomain(Filter filter, String domain) {
		Map<String, Filter> filtersByDomain = filtersByDomain(filter);
		if (filtersByDomain.containsKey(domain)) {
			return filtersByDomain.get(domain);
		} else {
			return filter;
		}
	}

	/**
	 * Always returns a predicate of all predicates assigned to the filter or a predicate that will
	 * accept everything.
	 * 
	 * call {@link Filter#hasPredicates()} if you wish to avoid this.
	 * 
	 * @param filter
	 * @return
	 */
	public static <T> Predicate<T> predicates(Filter filter) {

		if (filter.hasPredicates()) {
			List<Predicate<?>> genericPredicates = filter.getPredicates();
			Function<Object, Predicate<T>> castFunction = CollectionUtilsExtended.castFunction();
			Iterable<Predicate<T>> predicates = Iterables.transform(genericPredicates, castFunction);
			return Predicates.and(predicates);
		} else {
			return Predicates.alwaysTrue();
		}
	}

	/**
	 * Using the conditions this will extract ids from conditions matching the given key. The
	 * function receives the string and returns the set of ids.
	 * 
	 * @param filter
	 * @param fieldKey
	 * @param idFunction
	 * @return
	 */
	public static <I extends Identifier<?, ?>> Set<I> idsFromFilterConditions(Filter filter, final FieldKey fieldKey,
			Class<I> type) {
		return idsFromFilterConditions(filter, fieldKey, FromString.function(type));
	}

	@SuppressWarnings("rawtypes")
	public static <I extends Identifier<?, ?>> Set<I> idsFromFilterConditions(Filter filter, final FieldKey fieldKey,
			Function<String, I> idFunction) {
		Collection<FilterCondition> conditions = filter.conditions(fieldKey);
		HashSet<I> ids = Sets.newHashSet();
		for (FilterCondition filterCondition : conditions) {
			ids.add(idFunction.apply(String.valueOf(filterCondition.getValue())));
		}
		return ids;
	}

	/**
	 * Will split any item in the given list into more items if they are separated by a
	 * {@link #LIST_SEPARATOR} or {@link #LIST_SEPARATOR_STANDARD}, but not combined.
	 * 
	 * If the list is empty then null is returned to avoid displaying unwanted garbage injected by
	 * Jersey.
	 * 
	 * @param items
	 * @return the list of string
	 */
	@Nullable
	public static List<String> listParsed(final List<String> items) {
		if (CollectionUtils.isNotEmpty(items)) {
			List<String> parsedOptions = Lists.newArrayList();
			for (String option : items) {
				String[] split = StringUtils.split(option, LIST_SEPARATOR_STANDARD);
				if (split.length <= 1) {
					// maybe split with alternates.
					split = StringUtils.split(option, LIST_SEPARATOR);
				}
				// inspect every item to ensure they too are not empty (null of empty string
				for (String string : split) {
					if (StringUtils.isNotEmpty(string)) {
						parsedOptions.add(string);
					}
				}
			}

			return (parsedOptions.isEmpty()) ? null : parsedOptions;
		} else {
			return null;
		}

	}

	/**
	 * Given the query parameters, presumably from {@link UriInfo}, this will inspect those query
	 * parameters to see if they qualify to be field conditions.
	 * 
	 * <ul>
	 * <li>the key contains a {@link FieldUtils#KEY_DILIM}</li>
	 * <li>the operator is assumed to be an equals (repalces conditions=my-field=something with
	 * my-field=something)</li>
	 * </ul>
	 * 
	 * TODO: handle more than equals
	 * 
	 * @param queryParameters
	 *            entries from a {@link MultivaluedMap} or a {@link ListMultimap}
	 * @return
	 * @throws BaseException
	 */
	public static ImmutableList<FilterCondition<?>> conditions(Set<Entry<String, List<String>>> queryParameters,
			FilterConditionStringAdapter<?> filterConditionStringAdapter) throws BaseException {
		ImmutableList.Builder<FilterCondition<?>> conditions = ImmutableList.builder();
		for (Entry<String, List<String>> entry : queryParameters) {
			String key = entry.getKey();
			// since bean fields can't have a dilimeter and our fields must be qualified by domain
			// the a key can be detected
			if (FieldUtils.matchesKeyFormat(key)) {
				List<String> values = entry.getValue();
				for (String value : values) {
					String conditionString = FilterConditionStringAdapter.format(key, value);
					@SuppressWarnings({ "unchecked", "rawtypes" })
					FilterCondition filterCondition = filterConditionStringAdapter.unmarshal(	conditionString,
																								FilterCondition.class);
					conditions.add(filterCondition);
				}
			}
		}
		return conditions.build();
	}

	/**
	 * Given any iterable of keys this will return a list of FilterSorts first
	 * {@link Direction#ASCENDING} then {@link Direction#DESCENDING}.
	 * 
	 * @param keys
	 * @return
	 */
	public static List<FilterSort> bothDirections(Iterable<FieldKey> keys) {
		ImmutableList.Builder<FilterSort> builder = ImmutableList.builder();
		for (FieldKey fieldKey : keys) {
			builder.add(FilterSort.create().setFieldKey(fieldKey).setDirection(Direction.ASCENDING).build());
			builder.add(FilterSort.create().setFieldKey(fieldKey).setDirection(Direction.DESCENDING).build());
		}
		return builder.build();
	}

}
