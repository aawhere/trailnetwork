/**
 * 
 */
package com.aawhere.persist;

import java.util.Map;

import com.aawhere.id.Identifier;

import com.google.common.base.Function;

/**
 * A general service that takes identifiers and provides entities, but may cross domains. Use
 * {@link ServiceStandard} for the standard situation of type safe entities.
 * 
 * @author aroller
 * 
 */
public interface ServiceGeneral<S extends BaseFilterEntities<E>, E extends BaseEntity<E, ?>, I extends Identifier<?, ?>> {
	/**
	 * Provides a map of all ids known to the system. Those that aren't known are not included in
	 * the response.
	 * 
	 * @param ids
	 * @return
	 */
	Map<I, E> all(Iterable<I> ids);

	Function<I, E> idFunction();

	/** Indicates the type of Entity this ServiceStandard provides. */
	Class<E> provides();

	/**
	 * Provides the entity when given the id. The service may prepare the entity as appropriate.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 *             when no entity matches the given id.
	 */
	E entity(I id) throws EntityNotFoundException;
}
