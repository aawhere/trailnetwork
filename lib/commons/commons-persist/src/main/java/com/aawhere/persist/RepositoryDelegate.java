/**
 * 
 */
package com.aawhere.persist;

import java.util.Map;

import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * @author aroller
 * @param <R>
 *            the repository this works for
 * @param <S>
 *            the multiple container of E
 * @param <E>
 *            the entity being managed
 * @param <I>
 *            the id that identifies E
 */
abstract public class RepositoryDelegate<R extends CompleteRepository<S, E, I>, S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends BaseRepository<E, I>
		implements Repository<E, I>, FilterRepository<S, E, I> {

	protected final R repository;

	/**
	 * 
	 */
	protected RepositoryDelegate(R repository) {
		this.repository = repository;
		Assertion.exceptions().notNull("repository", this.repository);

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#store(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public E store(E entity) {
		return this.repository.store(entity);
	}

	/**
	 * Convenience method to store the entity after building it. the stored entity will be returned.
	 * 
	 * @param builder
	 * @return
	 */
	protected E update(ObjectBuilder<E> builder) {

		final E entity = builder.build();
		return update(entity);
	}

	/*
	 * @see com.aawhere.persist.Repository#persist(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void persist(E entity) {
		store(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#storeAll(java.lang.Iterable)
	 */
	@Override
	public Map<I, E> storeAll(Iterable<E> entities) {
		return this.repository.storeAll(entities);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#load(com.aawhere.id.Identifier)
	 */
	@Override
	public E load(I id) throws EntityNotFoundException {
		return this.repository.load(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#loadAll(java.util.Collection)
	 */
	@Override
	public Map<I, E> loadAll(Iterable<I> ids) {
		return this.repository.loadAll(ids);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#delete(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void delete(E entity) {
		this.repository.delete(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#delete(com.aawhere.id.Identifier)
	 */
	@Override
	public void delete(I entityId) {
		this.repository.delete(entityId);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#deleteAll(java.lang.Iterable)
	 */
	@Override
	public void deleteAll(Iterable<E> entities) {
		this.repository.deleteAll(entities);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#deleteAllFromIds(java.lang.Iterable)
	 */
	@Override
	public void deleteAllFromIds(Iterable<I> entityIds) {
		this.repository.deleteAllFromIds(entityIds);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#getEntityClassRepresented()
	 */
	@Override
	public Class<E> getEntityClassRepresented() {
		return this.getEntityClassRepresented();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#filter(com.aawhere.persist.Filter)
	 */
	@Override
	public S filter(Filter filter) {
		return repository.filter(filter);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#entities(java.lang.Iterable)
	 */
	@Override
	public S entities(Iterable<I> ids) {
		return repository.entities(ids);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#count(com.aawhere.persist.Filter)
	 */
	@Override
	public Integer count(Filter filter) {
		return repository.count(filter);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#filterAsIterable(com.aawhere.persist.Filter)
	 */
	@Override
	public Iterable<E> filterAsIterable(Filter filter) {
		return repository.filterAsIterable(filter);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#idPaginator(com.aawhere.persist.Filter)
	 */
	@Override
	public Iterable<I> idPaginator(Filter filter) {
		return repository.idPaginator(filter);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#ids(com.aawhere.persist.Filter)
	 */
	@Override
	public Iterable<I> ids(Filter filter) {
		return repository.ids(filter);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#entityPaginator(com.aawhere.persist.Filter)
	 */
	@Override
	public Iterable<E> entityPaginator(Filter filter) {
		return repository.entityPaginator(filter);
	}

	/**
	 * Provides a common way for impelentations to update an entity assuming the repository
	 * impelments {@link MutableRepository}.
	 * 
	 * @see #getMutableRepository()
	 * 
	 * @param entity
	 * @return
	 */
	public E update(E entity) {
		return getMutableRepository().update(entity);
	}

	@SuppressWarnings("unchecked")
	protected MutableRepository<E, I> getMutableRepository() {
		return (MutableRepository<E, I>) this.repository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepository#dictionary()
	 */
	@Override
	public FieldDictionary dictionary() {
		return repository.dictionary();
	}

	/*
	 * @see com.aawhere.persist.FilterRepository#dictionaryFactory()
	 */
	@Override
	public FieldDictionaryFactory dictionaryFactory() {
		return this.repository.dictionaryFactory();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#exists(com.aawhere.id.Identifier)
	 */
	@Override
	public Boolean exists(I id) {
		return repository.exists(id);
	}
}
