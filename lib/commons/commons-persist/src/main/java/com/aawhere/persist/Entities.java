/**
 *
 */
package com.aawhere.persist;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.xml.XmlNamespace;

/**
 * Generic implementation of {@link BaseEntities}
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class Entities<KindT extends BaseEntity<KindT, ?>>
		extends BaseEntities<KindT> {

	/**
	 * Used to construct all instances of Activities.
	 */
	@XmlTransient
	public static class Builder<KindT extends BaseEntity<KindT, ?>>
			extends BaseEntities.Builder<Builder<KindT>, KindT, Entities<KindT>> {
		public Builder(List<KindT> collection) {
			super(new Entities<KindT>(), collection);
		}
	}

	/** Use {@link Builder} to construct Activities */
	private Entities() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntities#getCollection()
	 */
	@XmlElement(name = "collection")
	@Override
	public List<KindT> getCollection() {
		return super.getCollection();
	}

}