/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;

import com.google.common.base.Function;

/**
 * Simply persists the given entity into a repository.
 * 
 * @author aroller
 * 
 */
public class RepositoryStoreFunction<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		implements Function<E, E> {

	private Repository<E, I> repository;

	/**
	 * 
	 */
	public RepositoryStoreFunction(Repository<E, I> repository) {
		this.repository = repository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public E apply(E input) {
		return repository.store(input);
	}

}
