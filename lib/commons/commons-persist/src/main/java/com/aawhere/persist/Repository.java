package com.aawhere.persist;

import java.util.Map;
import java.util.Set;

import com.aawhere.id.Identifier;

/**
 * The Base for all interaction with the database. Each {@link Repository} shall represent a single
 * {@link BaseEntity} type and provide the CRUD for it.
 * 
 * 
 * @author Brian Chapman
 * 
 * @param <E>
 */
public interface Repository<E extends BaseEntity<E, I>, I extends Identifier<?, E>> {

	/**
	 * Persists an unpersisted entity in the database.
	 * 
	 * What about unique constraints? Will we support them?
	 * 
	 * @see http://aawhere.jira.com/wiki/display/AAWHERE/App+Engine#AppEngine-Constraints
	 * 
	 * @param entity
	 * @throws IllegalArgumentException
	 *             when the entity already has an ID and it isn't {@link SelfIdentifyingEntity}.
	 */
	E store(E entity);

	/**
	 * Creates or updates the entity. The persistence will happen asynchronously if allowed by the
	 * implementation instance so there is no return value.
	 * 
	 * @see #store(BaseEntity)
	 * @param entity
	 */
	void persist(E entity);

	/**
	 * @param entities
	 * @return
	 */
	Map<I, E> storeAll(Iterable<E> entities);

	/**
	 * Given the ID, this finds the Entity represented by the ID in the database and returns it
	 * fully loaded.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	E load(I id) throws EntityNotFoundException;

	/**
	 * Given any Collection of ids, this will return a map of ids to their Entities.
	 * 
	 * If the specific Entity is not found then it will be ignored and not returned in the results.
	 * 
	 * @param ids
	 * @return
	 */
	Map<I, E> loadAll(Iterable<I> ids);

	/**
	 * Provides only the identifiers that exist in the datastore from the given set of ids. All
	 * non-existing ids are not returned.
	 * 
	 * @param ids
	 * @return
	 */
	public Set<I> idsExisting(Iterable<I> ids);

	/**
	 * Removes the entity provided from the repository.
	 * 
	 * @param entity
	 */
	void delete(E entity);

	/**
	 * Deletes the entity represented by the Id provided.
	 * 
	 * @param entityId
	 */
	void delete(I entityId);

	void deleteAll(Iterable<E> entities);

	void deleteAllFromIds(Iterable<I> entityIds);

	/**
	 * Each repository declares to represent a single {@link BaseEntity}. This returns the class it
	 * is representing.
	 * 
	 * @return the class of the implementation of {@link BaseEntity}
	 */
	Class<E> getEntityClassRepresented();

	/**
	 * Indicates if {@link #load(Identifier)} will return something.
	 * 
	 * @param id
	 * @return
	 */
	Boolean exists(I id);

}