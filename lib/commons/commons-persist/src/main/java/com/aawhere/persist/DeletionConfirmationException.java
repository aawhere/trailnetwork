/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.persist.DeletionConfirmationExceptionMessage.Param;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Used to confirm a deletion of one or more entities. The count is given to inform the user of how
 * many will be deleted.
 * 
 * @author aroller
 * 
 */
@StatusCode(value = HttpStatusCode.BAD_REQUEST,
		message = "You must provide a value for {PASSKEY_KEY} to confirm the deletion of {COUNT} {TYPE}.")
public class DeletionConfirmationException
		extends BaseException {

	private static final long serialVersionUID = -5441427045083357518L;

	/**
	 * 
	 * @param passkeyKey
	 *            an extra confirmation that must be given to confirm the deletion. This is the key,
	 *            not the value of the passkey. That should be kept secret.
	 * @param countToBeDeleted
	 *            the number to be deleted if confirmation is given
	 * @param type
	 */
	public DeletionConfirmationException(Integer countToBeDeleted, Class<?> type) {
		super(CompleteMessage.create(DeletionConfirmationExceptionMessage.DELETION_CONFIRMATION)
				.param(Param.PASSKEY_KEY, PASSKEY_KEY).param(Param.COUNT, countToBeDeleted).param(Param.TYPE, type)
				.build());
	}

	public static final String PASSKEY_KEY = Filter.FIELD.OPTIONS;
}
