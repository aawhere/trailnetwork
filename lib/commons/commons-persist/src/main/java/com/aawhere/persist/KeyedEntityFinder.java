/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;
import com.aawhere.id.KeyedEntity;

/**
 * Provides ability to generally find {@link BaseEntity}s that are {@link KeyedEntity}s using an
 * implementing {@link Repository}.
 * 
 * @author roller
 * 
 */
public interface KeyedEntityFinder<E extends KeyedEntity<K>, K extends Identifier<?, E>> {

	/**
	 * Finds the implementing {@link BaseEntity} given the key that is the business identifier for
	 * the Entity.
	 * 
	 * 
	 * 
	 * @param key
	 *            the business identifier used to Identify the entity in a more readable manner than
	 *            the datastore identifier.
	 * @return the Entity if only one is found.
	 * @throws EntityNotFoundException
	 *             if none are found matching the key
	 * @throws IllegalStateException
	 *             if multiple are found matching the key
	 */
	E findByKey(K key) throws EntityNotFoundException;

}
