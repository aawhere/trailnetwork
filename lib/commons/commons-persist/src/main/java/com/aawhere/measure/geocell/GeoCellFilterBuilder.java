/**
 * 
 */
package com.aawhere.measure.geocell;

import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.measure.quantity.Length;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.field.FieldKey;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxUtil;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseBuilder;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;

/**
 * A common {@link Filter} mutator used to query against {@link GeoCells}. This assumes the cells
 * being queried is stored in a consistent manner:
 * 
 * <ul>
 * <li>All cells being represented are stored at the max resolution provided</li>
 * <li> {@link GeoCells#getAll()} and {@link GeoCells#getMaxResolution()} are indexed</li>
 * <li>All ancestors of the representative cells are stored with the represented cells.</li>
 * <li>Minimization has not taken place.</li>
 * </ul>
 * 
 * @author aroller
 * 
 */
public class GeoCellFilterBuilder
		extends BaseBuilder<Filter, GeoCellFilterBuilder> {

	/** Searches include the max resolution part of the geocells. */
	private Boolean atResolution = false;
	private FieldKey key;
	private FieldKey cellsKey;
	private FieldKey maxResolutionKey;
	/** Optionally used to restrict the reducer to only a certain resolution or lower. */
	private Resolution maxResolution;
	private Integer maxNumberOfCells = MAX_VALUES_FOR_IN_OPERATOR;

	/**
	 * Provides the bounds if none is given.
	 * 
	 * @see #filter(Filter)
	 */
	@Nullable
	private String defaultBounds;

	/**
	 * Used to construct all instances of GeoCellFilterBuilder.
	 */
	public static class Builder
			extends ObjectBuilder<GeoCellFilterBuilder> {

		public Builder(Filter filter) {
			super(new GeoCellFilterBuilder(filter));
		}

		public Builder key(FieldKey key) {
			building.key = key;
			building.cellsKey = new FieldKey(building.key, GeoCells.FIELD.CELLS);
			return this;
		}

		public Builder maxResolution(Resolution maxResolution) {
			building.maxResolution = maxResolution;
			return this;
		}

		public Builder maxNumberOfCells(Integer maxNumberOfCells) {
			building.maxNumberOfCells = maxNumberOfCells;
			return this;
		}

		public Builder atResolution(Boolean atResolution) {
			building.atResolution = atResolution;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("key", building.key);
			Assertion.exceptions().notNull("cellsKey", building.cellsKey);

		}

		@Override
		public GeoCellFilterBuilder build() {
			GeoCellFilterBuilder built = super.build();
			built.maxResolutionKey = new FieldKey(built.key, GeoCells.FIELD.MAX_RESOLUTION);
			return built;
		}

		/**
		 * If nothing else is provided this will be the default
		 * 
		 * @param world
		 * @return
		 */
		public Builder defaultCells(GeoCells world) {
			return defaultBounds(world.toString());
		}

		/**
		 * @param string
		 * @return
		 */
		private Builder defaultBounds(String bounds) {
			building.defaultBounds = bounds;
			return this;
		}

	}// end Builder

	private GeoCellFilterBuilder(Filter filter) {
		super(filter);
	}

	public static Builder mutate(Filter filter) {
		return new Builder(filter);
	}

	/**
	 * @deprecated use {@link Builder}
	 * @param geoCellsKey
	 * @param filter
	 * @return
	 */
	@Deprecated
	public static GeoCellFilterBuilder mutate(FieldKey geoCellsKey, Filter filter) {
		return mutate(filter).key(geoCellsKey).build();
	}

	/**
	 * Use {@link #intersection(GeoCoordinate, Length)} instead (contains for the use case this was
	 * being used for was really an intersection. Entity.intersects(area)).
	 * 
	 * @param coordinate
	 * @param resolution
	 * @return
	 */
	@Deprecated
	public GeoCellFilterBuilder contains(GeoCoordinate coordinate, Resolution resolution) {
		GeoCells geoCells = GeoCellUtil.geoCells(coordinate, resolution);
		return contains(geoCells);
	}

	/**
	 * Contains at any resolution.
	 * 
	 * @see #contains(GeoCells, Boolean)
	 * 
	 * @param cells
	 * @return
	 */
	public GeoCellFilterBuilder contains(GeoCells cells) {
		return contains(cells, false);
	}

	public GeoCellFilterBuilder contains(GeoCell cell) {
		return contains(GeoCells.create().add(cell).build());
	}

	/**
	 * enforces containment at the resolution of the geocells
	 * 
	 * @see #contains(GeoCells, Boolean)
	 * 
	 * @param cells
	 * @return
	 */
	public GeoCellFilterBuilder containsAtResolution(GeoCells cells) {
		return contains(cells, true);
	}

	/**
	 * Finds entities with cells that have at least the cells given. This provides an equals
	 * condition for as many cells as can be used to fit within the number of allowable conditions.
	 * If there are too many geocells this reduces the resolution of geocells until it fits within
	 * the {@link #numberOfConditionsAllowed()} thereby producing results that remain correct,
	 * however, possibly more results since a lower resolution is used.
	 * 
	 * 
	 * 
	 * 
	 * TN-301
	 * 
	 * <pre>
	 * There is a max count limit of conditions for a filter.  Options:
	 * 
	 * 1. Build a complex iterator that splits the geocells into allowable query sizes that
	 *    and make subsequent calls for all geocells provided.
	 * 2. Reduce the {@link Resolution} until the number of geocells is allowed
	 * 3. Truncate the geocells looking for only the first matches up to the limit.
	 * </pre>
	 * 
	 * Proposal 1 is the most accurate, but will be slow and complicated to write(in the service).
	 * Proposal 2 is less accurate and will provide more results than reality, but still represents
	 * the target well. Proposal 3 is lazy and will produce many that really aren't related enough
	 * at all.
	 * 
	 * Proposal 2 is currently implemented, but if others follow then provide the option based on
	 * the use case desired. This will assume the {@link TrackSummary#getSpatialBuffer()} provides a
	 * consistent style of geocells (essentially that a homogeneous complete resolution is provided
	 * and all of it's parents).
	 * 
	 * 
	 * Contains can't use minimizer because all parents are stored and there may be a false
	 * positive.
	 * 
	 * <pre>
	 * X has 1,12,123
	 * Y has 1,12,120-12e
	 * 
	 * X is stored and Y is the candidate so we are hoping X.contains(Y)
	 * X does not contain Y because 120 is not part of X
	 * 
	 * Y reduced => 1,12, therefore a query with reduced Y results:
	 * X.contains(Y) = true since 1, 12 are both in X so minimization caused a false positive.
	 * 
	 * Y does not need parents however since there is no reduction we must always match the max resolution.
	 * 
	 * </pre>
	 * 
	 * @param cells
	 * @param enforceResolution
	 *            applies the resolution equality filter if provided
	 * @return
	 */
	private GeoCellFilterBuilder contains(GeoCells cells, Boolean enforceResolution) {

		if (enforceResolution) {
			equalsResolution(cells.getMaxResolution());
		}

		// Provides some cushion
		// remove parents doesn't change the query since parents are equal if higher resolutions
		// Removing parents does reduce the number allowing for higher resolution queries
		Set<GeoCell> reduced = GeoCellsReducer.create().setMaxSize(numberOfConditionsAllowed()).removeParents()
				.setGeoCells(cells).build().getGeoCells();

		for (GeoCell geoCell : reduced) {
			FilterCondition<GeoCell> geoCellCondition = new FilterCondition.Builder<GeoCell>().field(this.cellsKey)
					.operator(FilterOperator.EQUALS).value(geoCell).build();
			addCondition(geoCellCondition);
		}
		return this;
	}

	/**
	 * Enforces the filter to equal the given resolution.
	 * 
	 * @param resolution
	 * @return
	 */
	public GeoCellFilterBuilder equalsResolution(Resolution resolution) {
		return addCondition(FilterCondition.create().field(this.maxResolutionKey).operator(FilterOperator.EQUALS)
				.value(resolution.resolution).build());
	}

	/**
	 * Receiving GeoCells that has parent cells included, which is standard for storage techniques,
	 * this will look for the best resolution possible given the limits of the IN operator queries.
	 * 
	 * <pre>
	 * X = 1,12,123
	 * Y = 1,12,120
	 * 
	 * X.intersects(Y) = false since 120 and 123 are not the same.
	 * 
	 * X is stored and Y is the query.
	 * 
	 * If we keep the parents of Y in the query we get a false postive:
	 * X.intersects(Y) = true since 1 or 12 are matched
	 * 
	 * Removing the parents of Y yields:
	 * Y=120
	 * 
	 * A query now provides the correct negative.
	 * X.intersects(Y-without parents)
	 * 
	 * Since Y does not have parents we can benefit from minimization.  Let's say Y has all 3rd resolution:
	 * Y = 1,12,120-12e
	 * X.intersects(Y) = true
	 * 
	 * Y without parents = 120-12e
	 * Y without parents minimized = 12
	 * X.intersects(Y without parents minimized) because both have 12
	 * 
	 * </pre>
	 * 
	 * @param other
	 * @return
	 */
	public GeoCellFilterBuilder intersection(GeoCells geoCells) {
		return intersection(geoCells, this.atResolution);
	}

	/**
	 * @see #intersection(GeoCells)
	 * @param geoCells
	 * @return
	 */
	public GeoCellFilterBuilder intersectionAtResolution(GeoCells geoCells) {
		return intersection(geoCells, true);
	}

	private GeoCellFilterBuilder intersection(GeoCells geoCells, Boolean atResolution) {
		return intersection(geoCells, atResolution, null);
	}

	/**
	 * Accepts the geocells to be queried against. If atResolution is provided it will be added to
	 * the query to isolate the targeted cells of a varying resolution.
	 * 
	 * @see #intersection(GeoCells)
	 * @param geoCells
	 * @param atResolution
	 * @param proximityBoundary
	 *            focuses the intersection query on the center of the bounds when reducing to
	 *            support the {@link #maxNumberOfCells}
	 * @return
	 */
	private GeoCellFilterBuilder intersection(GeoCells geoCells, Boolean atResolution, BoundingBox proximityBoundary) {
		// protected against multiple IN queries being applied...it won't do any good anyway.
		if (!building.containsCondition(cellsKey)) {
			if (BooleanUtils.isTrue(atResolution)) {
				equalsResolution(geoCells.getMaxResolution());
			}

			Set<GeoCell> reduced = GeoCellsReducer.create().setMaxSize(this.maxNumberOfCells).minimize()
					.proximityBoundary(proximityBoundary).setMaxResolution(this.maxResolution).removeParents()
					.setGeoCells(geoCells).build().getGeoCells();
			FilterCondition<Set<GeoCell>> geoCellCondition = new FilterCondition.Builder<Set<GeoCell>>()
					.field(cellsKey).operator(FilterOperator.IN).value(reduced).build();
			addCondition(geoCellCondition);
		} else {
			LoggerFactory.getLogger(getClass()).warning(cellsKey
					+ " is attempting an intersection query when a condition already exists " + building);
		}
		return dis;

	}

	/**
	 * Returns any entity that has at least one match with a cell for the given bounding box. If the
	 * resolution of the bounding box is beyond that which is stored then no results will be found.
	 * This provides full coverage of the bounds given and may grow larger than the given
	 * bounds...especially if {@link #maxNumberOfCells} is small. The results in such a case may be
	 * outside the desired area. Use {@link #intersectionProximity(BoundingBox)} for a targeted
	 * search in such a case.
	 * 
	 * @param bounds
	 * @return
	 */
	public GeoCellFilterBuilder intersection(BoundingBox bounds) {
		GeoCellBoundingBox boundingBox = GeoCellUtil.boundingBox(bounds);
		return intersection(boundingBox.getGeoCells());
	}

	/**
	 * Finds entities within the bounds given. If {@link #maxNumberOfCells} is greater than the
	 * cells needed to represent the bounds the reducer will focus on the center of the bounds and
	 * keep those cells closest to the center....imitating a proximity search that resembles a "+"
	 * rather than the typical circle.
	 * 
	 * @see GeoCellsReducer.Builder#proximityBoundary(BoundingBox)
	 * 
	 * @param bounds
	 * @return
	 */
	public GeoCellFilterBuilder intersectionProximity(BoundingBox bounds) {
		GeoCellBoundingBox boundingBox = GeoCellUtil.boundingBox(bounds);
		return intersection(boundingBox.getGeoCells(), null, boundingBox);
	}

	/**
	 * Returns any entity that has at least one match with a cell for the bounding box that fits
	 * into the box created by the center point and halfDiagonalWidth provided.
	 * 
	 * @param center
	 * @param radius
	 *            the distance around the center.
	 * @return
	 */
	public GeoCellFilterBuilder intersection(GeoCoordinate center, Length radius) {
		return intersection(BoundingBoxUtil.boundedBy(center, radius));
	}

	/**
	 * Handles standard filter operations conditionally if attributes are available and a spatial
	 * query has not yet been added to the filter.
	 * 
	 * converts the bounds into a bounding box and runs and {@link #intersection(BoundingBox)} query
	 * unless {@link Filter#isProximity()} is true then {@link #intersectionProximity(BoundingBox)}
	 * is called.
	 * 
	 * This is intended to be called by the build. There is no harm in calling this more than once
	 * since it will only add a condition if none as been added.
	 * 
	 * @param filter
	 * @return
	 */
	private Filter filter(Filter filter) {
		if (!filter.hasBounds() && this.defaultBounds != null) {
			setBounds(this.defaultBounds);
		}
		if (filter.hasBounds()) {
			if (!filter.containsCondition(cellsKey)) {
				BoundingBox boundingBox = GeoCellUtil.boundingBox(filter.getBounds());
				if (filter.isProximity()) {
					intersectionProximity(boundingBox);
				} else {
					intersection(boundingBox);
				}
			}
		}
		return filter;
	}

	/**
	 * calls {@link #filter(Filter)} to conditionally add filtering if needed.
	 */
	@Override
	public Filter build() {
		filter(building);
		return super.build();
	}

	/**
	 * Conditionally creates the {@link Builder} if the filter has properties this can handle. This
	 * is ideal to use in other {@link Filter.Builder} constructors and assign it to the filter
	 * builder variable.
	 * 
	 * @param filter
	 * @return
	 */
	@Nullable
	public static GeoCellFilterBuilder createIfNeeded(@Nonnull Filter filter) {
		if (filter.hasBounds()) {
			return new GeoCellFilterBuilder(filter);
		} else {
			return null;
		}
	}
}
