/**
 *
 */
package com.aawhere.persist.objectify;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.lang.NotImplementedException;

import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.LongIdentifier;
import com.aawhere.id.LongIdentifierWithParent;
import com.aawhere.id.StringIdentifier;
import com.aawhere.id.StringIdentifierWithParent;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.RegisteredEntities;
import com.aawhere.persist.BaseFilterEntities.Builder;
import com.aawhere.util.rb.MessageStatus;
import com.aawhere.util.rb.StatusMessages;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;

/**
 * Single place to convert {@link Id}s to {@link Key}s and back.
 * 
 * This must live in commons-persist, rather than gae-persist to support the requirement of Key when
 * identifying a Parent.
 * 
 * @author Aaron Roller
 * 
 */
public class ObjectifyKeyUtils {

	private ObjectifyKeyUtils() {
	}

	/**
	 * A recursive function that will convert the key and every ancestor into their correponding
	 * identifiers.
	 * 
	 * @param key
	 * @param registeredEntities
	 * @return
	 */
	public static Identifier<?, ?> id(Key<?> key, RegisteredEntities registeredEntities) {

		final Key<?> parent = key.getParent();
		Class<Identifier<?, ?>> idType = registeredEntities.idType(key.getKind());
		Identifier<?, ?> id;
		if (parent != null) {
			Identifier<?, ?> parentId = id(key.getParent(), registeredEntities);
			id = id(key, idType, parentId);
		} else {
			id = idWithParents(key, idType);
		}
		return id;
	}

	/**
	 * @param key
	 * @param idType
	 * @param parent
	 * @return
	 */
	public static Identifier<?, ?> id(Key<?> key, Class<? extends Identifier<?, ?>> idType, Identifier<?, ?> parent) {
		return IdentifierUtils.idWithParent(value(key), idType, parent);
	}

	/**
	 * Converts the key to an instance of the idtype with the Key's value and it's parents. This
	 * assumes that if there is a parent key then it is of the same type as the idType itself. Use
	 * {@link #idWithParents(Key, Class, Class)} if otherwise.
	 * 
	 * @param key
	 * @param idType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> I id(Key<E> key, Class<I> idType) {
		return (I) idWithParents(key, idType, null);
	}

	public static Identifier<?, ?> idRelaxed(Key<?> key, Class<? extends Identifier<?, ?>> idType) {
		return idWithParents(key, idType);
	}

	/**
	 * Purposely less generic to allow recursive calls with parents of different types.
	 * 
	 * @see #id(Key, Class)
	 * @param key
	 * @param idType
	 * @return
	 */
	public static Identifier<?, ?> idWithParents(Key<?> key, Class<? extends Identifier<?, ?>> idType) {
		return idWithParents(key, idType, null);
	}

	/**
	 * Recursively calls this method (indirectly through {@link #idWithParents(Key, Class)}) looking
	 * for ancestors of the parent id. The type of the parent is reflected for the idType initially
	 * given so the ancestry may change types.
	 * 
	 * @param key
	 * @param idType
	 * @param parentIdType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Identifier<?, ?> idWithParents(Key<?> key, Class<? extends Identifier<?, ?>> idType,
			Class<? extends Identifier<?, ?>> parentIdType) {
		Assertion.exceptions().eq(	getKindFromId((Class<? extends Identifier<?, ? extends BaseEntity<?, ?>>>) idType),
									key.getKind());
		// FIXME:Registered Entities method and this are similar and could be serviced using a
		// Function<String(Kind),Class<P>> this would be a singleton function.
		Identifier<?, ?> id;
		if (hasParent(key)) {
			if (parentIdType == null) {
				parentIdType = IdentifierUtils.parentType(idType);
			}
			final Key<?> parentKey = key.getParent();
			// RECURSIVE CALL
			Identifier<?, ?> parentId = idWithParents(parentKey, parentIdType);
			// this is an intermediate stop of recursion
			id = id(key, idType, parentId);
		} else {
			// this stops recursion
			id = IdentifierUtils.idFrom(value(key), idType);

		}
		return id;
	}

	/**
	 * Retrieves the id or name from the given {@link Key} by inspecting.
	 * 
	 * @param key
	 */
	public static Object value(Key<?> key) {
		String name = key.getName();
		if (name == null) {
			return key.getId();
		} else {
			return name;
		}
	}

	/**
	 * provides the key from the id with all the ancestors included.
	 * 
	 * @param id
	 * @return
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Key<E> key(I id) {
		return (Key<E>) keyWithParents(id);
	}

	/**
	 * Recursively calls looking for parents from the id and adding the to the keys.
	 * 
	 * Purposefully generic since this calls itself for parents which may not be of the same type.
	 * 
	 * @param id
	 * @return
	 */
	public static Key<?> keyWithParents(Identifier<?, ?> id) {
		Key<?> parentKey = null;
		if (IdentifierUtils.hasParent(id)) {
			Identifier<?, ?> parent = IdentifierUtils.parent(id);
			// RECURSIVE call
			parentKey = keyWithParents(parent);
		}
		return createKey(id.getKind(), id.getValue(), parentKey);
	}

	/**
	 * Transforms the given ids into a {@link BiMap} allowing bidirectional reference.
	 * 
	 * @param ids
	 * @return
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> BiMap<I, Key<E>> idKeyBiMap(Iterable<I> ids) {
		final Function<I, Key<E>> keyFunction = keyFunction();
		ImmutableMap<I, Key<E>> map = Maps.toMap(ids, keyFunction);
		return HashBiMap.create(map);
	}

	/**
	 * Provides a lazy map that will provide the values associated with the ID when given an
	 * objectify map with a key. By providing the ids this avoids calling the map so asynchronous
	 * processing can happen until the client invokes a method on the map.
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Map<I, E> idEntityMap(Iterable<I> ids,
			Map<Key<E>, E> objectifyMap, Class<E> entityClass) {
		BiMap<I, Key<E>> idKeyBiMap = idKeyBiMap(ids);
		return CollectionUtilsExtended.transformKeys(objectifyMap, idKeyBiMap.inverse());
	}

	/**
	 * Used to filter keys of only a certain kind. Ancestor queries return heterogenous results
	 * which includes the parent key.
	 */
	public static Function<Key<?>, String> keyKindFunction() {
		return new Function<Key<?>, String>() {

			@Override
			public String apply(Key<?> input) {
				return (input == null) ? null : input.getKind();
			}
		};
	}

	public static <E extends BaseEntity<E, ?>> Predicate<Key<?>> keyKindPredicate(Class<E> kind) {
		return Predicates.compose(Predicates.equalTo(getKind(kind)), keyKindFunction());
	}

	/**
	 * Get the value of the id from the key. Could be a Long or a String.
	 * 
	 * @param id
	 * @return the id as an Object
	 */
	public static Object getIdValue(Key<?> id) {
		Object idValue;
		if ((Long) id.getId() == null || id.getId() == 0) {
			idValue = id.getName();
		} else {
			idValue = id.getId();
		}
		return idValue;
	}

	private static Key<?> createKey(Class<?> kind, Object id, Key<?> parent) {
		Key<?> key;
		if (id instanceof Long) {
			key = Key.create(parent, kind, (Long) id);
		} else if (id instanceof String) {
			key = Key.create(parent, kind, (String) id);
		} else {
			throw new IllegalArgumentException("id must be of type Long or String");
		}

		return key;
	}

	/**
	 * An efficient way to retrieve the kind from the id type. It makes serious assumptions about
	 * the parameters and extension of the id...basically that the id extends directly from
	 * {@link LongIdentifier}, {@link StringIdentifier}, {@link LongIdentifierWithParent} or
	 * {@link StringIdentifierWithParent} and that each of those classes follow a consistent pattern
	 * of parameter declaration.
	 * 
	 * @param entityIdType
	 * @return
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> String getKindFromId(
			Class<? extends Identifier<?, ? extends BaseEntity<?, ?>>> entityIdType) {
		ParameterizedType type = (ParameterizedType) entityIdType.getGenericSuperclass();
		final Type[] actualTypeArguments = type.getActualTypeArguments();
		if (actualTypeArguments.length < 1) {
			throw new IllegalArgumentException(entityIdType
					+ " is expected to declare the kind in it's first parameter, but no parameters were found");
		}
		@SuppressWarnings("unchecked")
		Class<E> entityClass = (Class<E>) actualTypeArguments[0];
		return getKind(entityClass);
	}

	public static String getKind(Class<?> entityClass) {

		return Key.getKind(entityClass);
	}

	/**
	 * Used when you have a type Iterable<Key<EntityT>> and need native {@link Identifier} types as
	 * an iterable.
	 * 
	 * @author brian
	 * 
	 * @param <E>
	 * @param <I>
	 */
	public static class KeyToIdIterator<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			implements Iterable<I> {

		private Iterable<Key<E>> iterable;
		private Class<E> entityType;
		private Class<I> identifierType;

		/*
		 * (non-Javadoc)
		 * @see java.lang.Iterable#iterator()
		 */
		@Override
		public Iterator<I> iterator() {
			return new Iterator<I>() {

				Iterator<Key<E>> it = iterable.iterator();

				@Override
				public boolean hasNext() {
					return it.hasNext();
				}

				@Override
				public I next() {
					return ObjectifyKeyUtils.id(it.next(), identifierType);
				}

				@Override
				public void remove() {
					throw new NotImplementedException();
				}
			};
		}

		/**
		 * Used to construct all instances of KeyToIdIterator.
		 */
		public static class Builder<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
				extends ObjectBuilder<KeyToIdIterator<E, I>> {

			public Builder() {
				super(new KeyToIdIterator<E, I>());
			}

			public Builder<E, I> setIterable(Iterable<Key<E>> iterable) {
				building.iterable = iterable;
				return this;
			}

			public Builder<E, I> setEntityT(Class<E> type) {
				building.entityType = type;
				return this;
			}

			public Builder<E, I> setIdentifierType(Class<I> type) {
				building.identifierType = type;
				return this;
			}

			/*
			 * (non-Javadoc)
			 * @see com.aawhere.lang.ObjectBuilder#validate()
			 */
			@Override
			protected void validate() {
				Assertion.assertNotNull(building.iterable);
				Assertion.assertNotNull(building.entityType);
				Assertion.assertNotNull(building.identifierType);
				super.validate();
			}

		}// end Builder

		/**
		 * Use {@link Builder} to construct KeyToIdIterator
		 */
		private KeyToIdIterator() {
		}

	}

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Function<I, Key<E>> keyFunction() {
		return new Function<I, Key<E>>() {

			@Override
			public Key<E> apply(I input) {
				if (input == null) {
					return null;
				}
				return ObjectifyKeyUtils.key(input);
			}
		};
	}

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Function<Key<E>, I> idFunction(
			final Class<I> idType, final Class<E> entityType) {
		return new Function<Key<E>, I>() {

			@Override
			public I apply(Key<E> input) {
				return (I) ObjectifyKeyUtils.idWithParents(input, idType, null);
			}

		};
	}

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<I> ids(Iterable<Key<E>> keys,
			Class<I> idType, Class<E> entityTtype) {
		final Function<Key<E>, I> idFunction = idFunction(idType, entityTtype);
		return Iterables.transform(keys, idFunction);
	}

	/**
	 * Provides an Iterable wrapping the given ids that will transform the id to a key using
	 * {@link #keyFunction()}.
	 * 
	 * @param ids
	 * @return iterable of keys with nulls filtered out or null if a null was passed
	 */
	@Nullable
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<Key<E>> keys(
			@Nullable Iterable<I> ids) {
		if (ids == null) {
			return null;
		}
		final Function<I, Key<E>> keyFunction = keyFunction();
		return Iterables.filter(Iterables.transform(ids, keyFunction), Predicates.notNull());
	}

	/**
	 * A helper function for {@link Objectify#load()} functions.
	 * 
	 * Transforms the given objectify map into the entities object resulting from the given Entities
	 * builder.
	 * 
	 * The map contains null references to those items not found so this will log an error message
	 * indicating not all the items were found and identifying such entities. NOTE: I can't find the
	 * reference of where this was documented so it may not be true. AR 2014.05.05
	 * 
	 * A quick check of {@link BaseFilterEntities#getStatusMessages()} and
	 * {@link StatusMessages#hasError()} will indicate a problem.
	 * 
	 * All other values are added to builder in the order presented by the keys.
	 * 
	 * @param results
	 * @param entitiesBuilder
	 * @param idType
	 * @return
	 */
	public static
			<S extends BaseFilterEntities<E>, B extends BaseFilterEntities.Builder<B, E, S>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			S entities(Map<Key<E>, E> results, Iterable<I> requested, B entitiesBuilder, Class<I> idType) {
		Set<Entry<Key<E>, E>> entrySet = results.entrySet();
		StatusMessages.Builder messagesBuilder = StatusMessages.create();
		for (Entry<Key<E>, E> entry : entrySet) {
			final E entity = entry.getValue();
			// if a key references a null value, log it as an error
			// this may never happen in objectify since nulls are in the map,
			// but apparently not
			// accessible through standard methods like entrySet()
			if (entity == null || entity.id() == null) {
				I id = ObjectifyKeyUtils.id(entry.getKey(), idType);
				messagesBuilder.addMessage(MessageStatus.ERROR, EntityNotFoundException.message(id));
			} else {
				entitiesBuilder.add(entity);
			}
		}
		HashSet<Key<E>> requestedKeys = Sets.newHashSet(ObjectifyKeyUtils.keys(requested));
		Set<Key<E>> keysNotFound = Sets.difference(requestedKeys, results.keySet());
		if (!keysNotFound.isEmpty()) {
			for (Key<E> keyNotFound : keysNotFound) {
				messagesBuilder.addMessage(	MessageStatus.ERROR,
											EntityNotFoundException.message(ObjectifyKeyUtils.id(keyNotFound, idType)));
			}
		}
		return entitiesBuilder.setStatusMessages(messagesBuilder.build()).build();
	}

	public static Boolean hasParent(Key<?> key) {
		return key.getParent() != null;
	}
}
