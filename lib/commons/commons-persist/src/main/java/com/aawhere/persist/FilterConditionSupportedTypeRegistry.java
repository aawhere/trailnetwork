/**
 *
 */
package com.aawhere.persist;

import java.util.HashSet;
import java.util.Set;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * A registry containing supported value types for {@link FieldFilterCondition}s. Value types are
 * supported based on the field type and {@link FilterOperator} used. For example
 * 
 * {@literal
 * fieldType        FilterOperator  Value
 * GeoCoordinate    in              BoundingBox
 * GeoCoordinate    in              Proximity (hypothetical)
 * AType            *               AType
 * }
 * 
 * The last row illustrates the default case where the fieldType type equals the Value type for any
 * {@link FilterOperator}.
 * 
 * Attempts to add duplicate entries are ignored silently.
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class FilterConditionSupportedTypeRegistry {

	private Table<Class<?>, FilterOperator, Set<Class<?>>> table = HashBasedTable.create();

	@Inject
	private FilterConditionSupportedTypeRegistry() {
	}

	public Set<Class<?>> supportedTypes(Class<?> fieldType, FilterOperator operator) {
		Set<Class<?>> supportedTypes = table.get(fieldType, operator);
		if (supportedTypes == null) {
			supportedTypes = new HashSet<Class<?>>();
		}
		return supportedTypes;
	}

	public Boolean supported(Class<?> fieldType, FilterOperator operator, Class<?> valueType) {
		Set<Class<?>> supportedTypes = supportedTypes(fieldType, operator);
		return supportedTypes.contains(valueType);
	}

	public void register(Class<?> fieldType, FilterOperator operator, Class<?> valueType) {
		if (!supported(fieldType, operator, valueType)) {
			Set<Class<?>> supportedTypes = supportedTypes(fieldType, operator);
			supportedTypes.add(valueType);
			table.put(fieldType, operator, supportedTypes);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Table.Cell<Class<?>, FilterOperator, Set<Class<?>>> cellSet : table.cellSet()) {
			sb.append(cellSet.getRowKey() + ", " + cellSet.getColumnKey() + ", " + cellSet.getValue().toString());
		}
		return sb.toString();
	}

	/**
	 * Used to construct all instances of FilterConditionSupportedTypeRegistry.
	 */
	public static class Builder
			extends ObjectBuilder<FilterConditionSupportedTypeRegistry> {

		public Builder() {
			super(new FilterConditionSupportedTypeRegistry());
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

}
