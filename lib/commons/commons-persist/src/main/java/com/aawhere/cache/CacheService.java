/**
 *
 */
package com.aawhere.cache;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheLoader;
import net.sf.jsr107cache.CacheManager;
import net.sf.jsr107cache.CacheStatistics;

import org.apache.commons.lang3.SerializationUtils;

import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.id.Identifier;
import com.aawhere.log.LoggerFactory;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.inject.Singleton;

/**
 * Utility methods for working with JCache
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class CacheService {

	private static final Level SIZE_LOGGER_LEVEL = Level.FINER;
	private Cache cache;
	private static final Logger logger = LoggerFactory.getLogger(CacheService.class);

	/**
	 * Get an instance of {@link CacheException}
	 * 
	 * @return
	 * @throws CacheException
	 */
	public CacheService() {
		CacheFactory cacheFactory;
		try {
			cacheFactory = CacheManager.getInstance().getCacheFactory();
			cache = cacheFactory.createCache(Collections.emptyMap());
		} catch (CacheException e) {
			throw new RuntimeException(e);
		}
	}

	public CacheStatistics fetchCacheStatistics() {
		return cache.getCacheStatistics();
	}

	public void put(Object key, Object value) {
		if (value instanceof Serializable) {
			try {
				if (logger.isLoggable(SIZE_LOGGER_LEVEL)) {
					logValueSize(key, (Serializable) value);
				}
				cache.put(key, value);
			} catch (Exception e) {
				/*
				 * Something happened with the memcache put. The service could be not available, the
				 * object too big, etc. Google App engine throws a MemcacheServiceException, which
				 * we could catch, but that would violate the contract for commons-persist
				 * (non-persistance-implementation-specific code). It has been documented
				 * (https://groups.google.com/forum/#!msg/google-appengine/j8RS1ObnBG0/ERmyugkUE1kJ)
				 * that google app engine will fail without explanation from time to time. Ignore
				 * errors and continue as a cache miss.
				 */
				logger.warning(e.getMessage());
				logger.throwing(CacheService.class.getSimpleName(), "put", e);
			}
		} else {
			throw new IllegalArgumentException("Cached objects must implement Serializable");
		}

	}

	private void logValueSize(Object key, Serializable value) {
		byte[] bytes = SerializationUtils.serialize(value);
		int length = bytes.length;
		if (length > 1000000) {
			logger.warning("attempting to cache " + key + " which is greater than 1mb (" + length + ")");
		} else {
			logger.log(SIZE_LOGGER_LEVEL, "putting " + key + " of length " + length + " into cache.");
		}
	}

	public Object get(Object key) {
		return cache.get(key);
	}

	/**
	 * Same as {@link #get(Object)}, but will load the desired object using the given
	 * {@link Function} that will convert the {@link Identifier} into the desired object. If the
	 * object is loaded then it will be cached.
	 * 
	 * @param id
	 * @param keyFunction
	 * @param provider
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends Serializable, I, K extends Function<I, String>, P extends Function<I, T>> T get(I id,
			K keyFunction, P provider) {
		String key = keyFunction.apply(id);
		T result = (T) cache.get(key);
		if (result == null) {
			result = provider.apply(id);
			cache.put(key, result);
		}
		return result;
	}

	/**
	 * gets from the cache using the id, but looks for the transformed key in the cache provided by
	 * the keyFunction. This is the same as {@link #get(Object, Function, Function)}, but with no
	 * provider so if it's not in the cache anull is returned without production attempt.
	 * 
	 * @param id
	 * @param keyFunction
	 * @return
	 */
	<T extends Serializable, I, K extends Function<I, String>, P extends Function<I, T>> T get(I id, K keyFunction) {
		return get(id, keyFunction, new Function<I, T>() {
			@Override
			public T apply(I input) {
				return null;
			}

		});
	}

	/**
	 * Given the keys this will return the map associated to the keys. the Map will follow the
	 * behavior of {@link Cache#getAll(Collection)} by only returning those keys that are found.
	 * 
	 * @param keys
	 *            the keys of the items being searched. an empty map is return if null is provided
	 * @return a map with any of the key/value entries found. Empty if none. never null.
	 */
	@SuppressWarnings("unchecked")
	@Nonnull
	public <T extends Serializable, K extends Serializable> Map<K, T> getAll(@Nullable Collection<K> keys) {
		if (keys != null) {
			try {
				Map<K, T> result = cache.getAll(keys);
				if (result != null) {
					return result;
				}// else just do the default
			} catch (CacheException e) {
				// it's better just to deal with this than pass it on since it is undefined as to
				// why
				// this is thrown

			}
		}// else just do the default
			// this is the default
		return Collections.emptyMap();
	}

	/**
	 * Specializing in loading items from cache efficiently using batches, but also combining those
	 * results with efficiently loaded items from the originator (persistence, etc). Any item not in
	 * the cache will be passed to the given function in bulk allowing bulk loading or creation.
	 * 
	 * The result being in most cases a collection with all desired objects that either were in the
	 * cache on the way in or will be now that the loading happened.
	 * 
	 * This approach is provided in place of {@link CacheLoader} which doesn't seem to be part of
	 * the {@link CacheManager} interface.
	 * 
	 * The collection is not guaranteed to have every item requested. If the provider is not able to
	 * provide then it isn't part of the result map. Since this is likely not to happen in most
	 * cases and in even more there isn't much anyone can do about it we simply ignore that they are
	 * missing and return what can be found or provided. If you must know if something can't be
	 * provided then plug into the provider to find out.
	 * 
	 * KeyFunction is necessary because often times an identifier for an entity may be used for the
	 * Entity, but you also use that identifier to produce some supporting object or external
	 * represenation.
	 * 
	 * 
	 * @param ids
	 * @param keyFunction
	 * @param provider
	 * @return
	 */
	public <T extends Serializable, I, K extends Function<I, String>, P extends Function<Iterable<I>, Map<I, T>>>
			Map<I, T> getAll(Collection<I> ids, K keyFunction, P provider) {
		ImmutableMap<I, String> idMap = Maps.toMap(ids, keyFunction);
		Map<String, T> foundInCache = getAll(idMap.values());

		// add those found into the results
		Map<I, T> results = Maps.newHashMap(CollectionUtilsExtended.swapKeys(idMap, foundInCache));

		// handle if not all were found
		if (foundInCache.size() != ids.size()) {
			HashBiMap<I, String> idBiMap = HashBiMap.create(idMap);
			Set<String> cacheKeyRequested = idBiMap.values();
			Set<String> cacheKeysInCache = foundInCache.keySet();
			SetView<String> cacheKeysNotInCache = Sets.difference(cacheKeyRequested, cacheKeysInCache);
			Predicate<String> cacheKeysNotInCachePredicate = Predicates.in(cacheKeysNotInCache);
			BiMap<I, String> idsNotInCache = Maps.filterValues(idBiMap, cacheKeysNotInCachePredicate);
			Map<I, T> objectsNotInCache = provider.apply(idsNotInCache.keySet());
			// add the newly created to the results
			results.putAll(objectsNotInCache);
			// put the newly created into the cache
			for (Entry<I, T> entryCreated : objectsNotInCache.entrySet()) {
				final T value = entryCreated.getValue();
				cache.put(idBiMap.get(entryCreated.getKey()), value);
			}
		}
		return results;
	}
}
