/**
 *
 */
package com.aawhere.search;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.field.FieldKey;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Document containing search results or data for indexing. Each document must contain a domain by
 * which to identify the document's repository.
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class SearchDocument
		implements Serializable {

	private static final long serialVersionUID = 6172583718443315053L;

	/**
	 * Used to construct all instances of SearchDocument.
	 */
	public static class Builder
			extends ObjectBuilder<SearchDocument> {

		public Builder() {
			super(new SearchDocument());
		}

		public Builder id(String id) {
			building.id = id;
			return this;
		}

		public Builder id(Identifier<?, ?> id) {
			building.id = id.toString();
			return this;
		}

		public Builder domain(String type) {
			return index(type);
		}

		public Builder index(String indexName) {
			building.index = indexName;
			building.domain = SearchUtils.domainFromIndex(indexName);

			return this;
		}

		/** @see #rank */
		public Builder rank(Integer rank) {
			building.rank = rank;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("index", building.index);
			Assertion.exceptions().notNull("id", building.id);
			Assertion.exceptions().notNull("fields", building.fields);
			Assertion.exceptions().notNull("rank", building.rank);
			Assertion.exceptions().notNull("domain", building.domain);

		}

		public Builder fields(Iterable<com.aawhere.search.SearchDocument.Field> fields) {
			building.fields = fields;
			return this;
		}

		public Builder addField(SearchDocument.Field field) {
			return addFields(Lists.newArrayList(field));
		}

		public Builder addFields(Iterable<com.aawhere.search.SearchDocument.Field> fields) {
			if (building.fields == null) {
				building.fields = Lists.newArrayList();
			}
			if (building.fields instanceof List) {
				Iterables.addAll((List<SearchDocument.Field>) building.fields, fields);
			} else {
				throw new IllegalStateException("fields is already assigned an iterable " + building.fields);
			}
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct SearchDocument */
	protected SearchDocument() {
	}

	@XmlAttribute
	private String id;

	/**
	 * The domain this search document represents. May contain fields from other domains. This may
	 * be the same as {@link #index}, however, the index may be domain with extra suffixes
	 * indicating a subset of the domain.
	 * 
	 */
	@XmlAttribute
	private String domain;

	@XmlAttribute
	private String index;

	/**
	 * Optional value that when provided it will override the default rank (last updated). The
	 * larger the rank, the more important this document is.
	 * 
	 * https://cloud.google.com/appengine/docs/java/search/best_practices
	 * 
	 */
	@Nullable
	@XmlAttribute
	private Integer rank;

	/**
	 * The result of the document fields. An iterable to allow lazy calling since this is a result
	 * from a search service. {@link #fields()}
	 */
	private Iterable<Field> fields;

	/** @see #rank */
	public Integer rank() {
		return this.rank;
	}

	public Boolean hasRank() {
		return this.rank != null;
	}

	public String id() {
		return this.id;
	}

	public String getDomain() {
		return domain;
	}

	public String domain() {
		return domain;
	}

	public String index() {
		return index;
	}

	/**
	 * Provides xml support for {@link #fields()} by returning a concrete list. Calling this method
	 * will block until the search is complete if not already. The list is lazy created from the
	 * iterables once and this is thread safe.
	 * 
	 * @seee {@link #fields()}
	 * 
	 * @return
	 */
	@XmlElement
	public List<Field> getFields() {
		if (this.fields != null) {
			synchronized (fields) {
				if (!(this.fields instanceof List)) {
					this.fields = ImmutableList.<SearchDocument.Field> builder().addAll(fields).build();
				}
			}
		}
		return (List<Field>) this.fields;
	}

	/**
	 * Provides the iterable as it was provided to this. If it is lazy then it will remain lazy
	 * until forced to produce when calling {@link Iterator#next()} for example.
	 * {@link #getFields()} returns a concrete list.
	 * 
	 * @see #getFields()
	 * 
	 * @return
	 */
	public Iterable<Field> fields() {
		return fields;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SearchDocument other = (SearchDocument) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@XmlRootElement
	public static class Field
			implements Serializable {

		private static final long serialVersionUID = -4301908794188111832L;

		/**
		 * Used to construct all instances of Field.
		 */
		public static class Builder
				extends ObjectBuilder<Field> {

			public Builder() {
				super(new Field());
			}

			public Builder key(String key) {
				building.key = key;
				return this;
			}

			public Builder key(FieldKey key) {
				building.key = SearchUtils.key(key);
				return this;
			}

			public Builder value(Object value) {
				building.value = value;
				return this;
			}

			public Builder dataType(FieldDataType dataType) {
				building.dataType = dataType;
				return this;
			}

			@Override
			protected void validate() {
				Assertion.exceptions().notNull("key", building.key);
				Assertion.exceptions().notNull("value for " + building.key, building.value);

				super.validate();
			}

			@Override
			public Field build() {

				Field built = super.build();
				if (built.dataType == null) {
					built.dataType = SearchUtils.dataTypeFor(built.value.getClass());
				}
				Assertion.exceptions().notNull("dataType", built.dataType);

				return built;
			}
		}

		public static Builder create() {
			return new Builder();
		}

		/**
		 * Use {@link Builder} to create.
		 */
		private Field() {

		}

		@XmlElement
		private String key;

		private Object value;

		private FieldDataType dataType;

		public String key() {
			return key;
		}

		public Object value() {
			return value;
		}

		@XmlElement
		private String getValue() {
			return value.toString();
		}

		public FieldDataType dataType() {
			return dataType;
		}

		@Override
		public String toString() {
			return key + ": " + value + " (" + dataType + ")";
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.dataType == null) ? 0 : this.dataType.hashCode());
			result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
			result = prime * result + ((this.value == null) ? 0 : this.value.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Field other = (Field) obj;
			if (this.dataType != other.dataType)
				return false;
			if (this.key == null) {
				if (other.key != null)
					return false;
			} else if (!this.key.equals(other.key))
				return false;
			if (this.value == null) {
				if (other.value != null)
					return false;
			} else if (!this.value.equals(other.value))
				return false;
			return true;
		}

	}

	@Override
	public String toString() {
		// avoiding forcing anything from field
		return new StringBuilder(id).append(" from ").append(index).toString();
	}

}
