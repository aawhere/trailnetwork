/**
 *
 */
package com.aawhere.persist;

import com.aawhere.field.FieldMessage;
import com.aawhere.measure.MeasureMessage;
import com.aawhere.swagger.DocSupport;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Reporting for the persistence mechanisms.
 * 
 * @author Aaron Roller
 * 
 */
public enum PersistMessage implements Message {

	/** Explain_Entry_Here */
	ENTITY_NOT_FOUND(Doc.ENTITY_NOT_FOUND, Param.OBJECT_NAME, Param.OBJECT_ID),

	/* */
	NOTHING_TO_CREATE("The item you requested to create is already current."),
	/** @see FilterCondition */
	FILTER_CONDITION_MISSING_VALUE("You must provide a value after the operator:" + PersistMessage.Doc.CONDITION_FORMAT
			+ ". Notice spaces are required."),
	SORT_FIELD_NOT_COMPARABLE(
			"{FIELD_KEY} is requested to be sorted, but is not comparable since it is of type {FIELD_DATA_TYPE}.",
			FieldMessage.Param.FIELD_KEY,
			Param.FIELD_DATA_TYPE),
	/* */
	UNSUPPORTED_FILTER_CONDITION(
			"The operator '{FILTER_OPERATOR}' is not supported for data type '{FIELD_DATA_TYPE}'",
			Param.FILTER_OPERATOR,
			Param.FIELD_DATA_TYPE),
	UNSUPPORTED_FILTER_NULL_CONDITION(
			"The operator '{FILTER_OPERATOR}' is not supported for null values.'",
			Param.FILTER_OPERATOR),
	/** When trying to update anobject not yet stored. */
	CANNOT_UPDATE(
			"An atempt to update {OBJECT_ID} could not be completed since it hasn't yet been stored.",
			Param.OBJECT_ID),
	/** When trying to store an object already created. */
	CANNOT_STORE(
			"An attempt to store {OBJECT_ID}, but it has already been stored so it should be updated instead.",
			Param.OBJECT_ID),
	/**
	 * Empty collections are a valid and common request, but the cause of the empty results should
	 * be reported for clarity.
	 */
	EMPTY_COLLECTION_QUERY("An empty collection against {FIELD_KEY} produces no results.", FieldMessage.Param.FIELD_KEY),
	/** A way to report the complete filter operation. */
	OK_QUERY(
			"Filtering {FIELD_KEY} {FILTER_OPERATOR} {VALUE}.",
			FieldMessage.Param.FIELD_KEY,
			Param.FILTER_OPERATOR,
			Param.VALUE),
	/** Identifies that domains don't mix well. */
	DOMAIN_DIFFERENT("Ignoring {FIELD_KEY} because it's not from {DOMAIN}.", FieldMessage.Param.FIELD_KEY, Param.DOMAIN), ;

	private ParamKey[] parameterKeys;
	private String message;

	private PersistMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** The entity */
		OBJECT_NAME,
		/** The entity's ID */
		OBJECT_ID,
		/** General use term */
		VALUE,
		/** the Domain key representing an Entity */
		DOMAIN,
		/** The operator applied to the filter */
		FILTER_OPERATOR,
		/** */
		FIELD_DATA_TYPE;

		private CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static final class Doc {
		public static final String CONDITION_FORMAT = "[field] [operator] [value]";
		public static final String ENTITY_NOT_FOUND = "The {OBJECT_NAME} identified by {OBJECT_ID} was not found.";
		/** Append this to a field key to indicate the sort choice. */
		public static final String DESCENDING = " descending";
		public static final String SORT_NONE_CONDITION = Filter.FIELD.ORDER_BY + FilterOperator.EQUALS_VALUE
				+ FilterSort.NONE_FIELD;
		public static final String EXPAND_DESCRIPTION = "Show more information for a Field.";
		public static final String FILTER_PAGE_DESCRIPTION = "Separates results into sections sized by limt.";
		public static final String FILTER_LIMIT_DESCRIPTION = "The number of results per page requested.";
		public static final String FILTER_LIMIT_DEFAULT = String.valueOf(Filter.DEFAULT_LIMIT);
		public static final String FILTER_PAGE_DEFAULT = String.valueOf(Filter.FIRST_PAGE);
		public static final String FILTER_CONDITIONS_DESCRIPTION = "Limits results: " + CONDITION_FORMAT;
		public static final String FILTER_OPTIONS_DESCRIPTION = "A trigger that changes the behavior of the request";
		public static final String FILTER_FIELDS_DESCRIPTION = "Fields to be included in the response";
		public static final String FILTER_QUERY_DESCRIPTION = "Keyword text search of supported fields";
		public static final String FILTER_OPTIONS_DATA_TYPE = DocSupport.DataType.STRING;
		public static final String FILTER_EXPAND_DATA_TYPE = FILTER_OPTIONS_DATA_TYPE;
		public static final String FILTER_ORDER_BY_DESCRIPTION = "Sorts the results ascending(+) or descending(-). None removes a default sort (if any).";
		public static final String FILTER_BOUNDS_DESCRIPTION = MeasureMessage.Doc.BOUNDING_BOX_FORMAT_SNIPPET;
		public static final String FILTER_EXPAND_DESCRIPTION = "Shows related information that is hidden by default";

	}
}
