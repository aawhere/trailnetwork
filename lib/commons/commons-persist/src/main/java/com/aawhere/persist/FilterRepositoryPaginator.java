/**
 * 
 */
package com.aawhere.persist;

import java.io.Serializable;
import java.util.Iterator;

import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * Using the standard {@link Filter} this will iterate through all pages until no more results
 * remain. This iterator is specifically not {@link Serializable} because it will only work in the
 * container that provides the {@link FilterRepository}. This iterator is likely to be passed to a
 * ServiceStandard for entire processing, however, it may end up in the client code which it should
 * still perform properly although the {@link FilterRepository} is hidden from the client. TN-116
 * 
 * ObjectifyRepository will use QueryResultIterable, but this can be used to provide Repository
 * independent iteration such as crawling external services.
 * 
 * TODO: allow a max page limit
 * 
 * @author aroller
 * 
 * @param <O>
 *            the result of iteration. provided by extension.
 * @param <E>
 *            the entity being represented
 * @param <I>
 *            the identifier of the entity
 * @param <S>
 *            the collection of <E>
 */
abstract public class FilterRepositoryPaginator<O, S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		implements Iterable<O> {

	/**
	 * Used to construct all instances of FilterRepositoryIdIterator.
	 */
	public static class Builder<O, S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			extends ObjectBuilder<FilterRepositoryPaginator<O, S, E, I>> {

		public Builder(FilterRepositoryPaginator<O, S, E, I> paginator) {
			super(paginator);
		}

		public Builder<O, S, E, I> setRepository(FilterRepository<S, E, I> repository) {
			building.repository = repository;
			return this;
		}

		public Builder<O, S, E, I> setFilter(Filter filter) {
			building.filter = filter;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("repository", building.repository);
			Assertion.exceptions().notNull("filter", building.filter);
		}

	}// end Builder

	/** not dangerous to share a stateless object with children. */
	protected FilterRepository<S, E, I> repository;
	private Filter filter;

	/** Use {@link Builder} to construct FilterRepositoryIdIterator */
	protected FilterRepositoryPaginator() {
	}

	/**
	 * Delegate method to implementor to provide the results to iterate.
	 * 
	 * @return
	 */
	abstract protected Iterable<O> filterResults(Filter pagingFilter);

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<O> iterator() {
		return new Iterator<O>() {
			// paging filter increments with each call starting with the given filter from the outer
			// class
			private Filter pagingFilter = filter;
			// automatically sets up for the first page.
			private Iterator<O> iterator = filter();

			/** Runs the filter against the repository for the next page */
			private Iterator<O> filterNextPage() {
				this.pagingFilter = Filter.clone(this.pagingFilter).incrementPage().build();
				return filter();
			}

			/**
			 * simply runs the filter against the repository
			 * 
			 * @return
			 */
			private Iterator<O> filter() {
				Iterable<O> response = filterResults(pagingFilter);
				// This gets the filter with the updated
				this.pagingFilter = ((FilterProvider) response).getFilter();
				return response.iterator();
			}

			@Override
			public boolean hasNext() {
				return this.iterator.hasNext();
			}

			@Override
			public O next() {
				O result = this.iterator.next();
				// TODO:make this asynchronous where it retrieves next before it runs out (Future?)
				// if the iterator is at the end look for the next page
				if (!this.iterator.hasNext()) {
					this.iterator = filterNextPage();
				}
				return result;

			}

			@Override
			public void remove() {
				this.iterator.remove();
			}
		};

	}

}
