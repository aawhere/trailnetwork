/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;

import com.google.common.base.Function;
import com.google.common.base.Functions;

/**
 * Default implementation for all methods that can be generalized when the implementor provides
 * {@link #idFunction()}.
 * 
 * @author aroller
 * 
 */
public class ServiceStandardBase<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends ServiceGeneralBase<S, E, I>
		implements ServiceStandard<S, E, I> {

	/**
	 * 
	 */
	public ServiceStandardBase(Function<I, E> idFunction) {
		super(idFunction);
	}

	/** Requires you to override the {@link #idFunction()}. */
	public ServiceStandardBase() {
		super();
	}

	/** Pre-loads this service with entities provided via the {@link #idFunction()}. */
	public ServiceStandardBase(Iterable<E> entities) {
		this(Functions.forMap(IdentifierUtils.map(entities)));
	}

}
