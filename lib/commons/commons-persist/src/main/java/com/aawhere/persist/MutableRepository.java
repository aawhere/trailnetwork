/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;

/**
 * @author aroller
 * 
 */
public interface MutableRepository<E extends BaseEntity<E, I>, I extends Identifier<?, E>> {

	/** For those that must update an entity this allows common access. */
	E update(E entity);
}
