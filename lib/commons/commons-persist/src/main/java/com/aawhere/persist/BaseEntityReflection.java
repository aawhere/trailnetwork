/**
 *
 */
package com.aawhere.persist;

import com.aawhere.lang.reflect.BuilderReflection;

/**
 * Useful helpers for constructing {@link BaseEntity} using their {@link BaseEntity.Builder}.
 * 
 * @author Aaron Roller
 * 
 */
public class BaseEntityReflection<E extends BaseEntity<E, ?>, B extends BaseEntity.Builder<E, ?, ?, ?>>
		extends BuilderReflection<E, B> {

	public static <E extends BaseEntity<E, ?>, B extends BaseEntity.Builder<E, ?, ?, ?>> BaseEntityReflection<E, B>
			newInstance() {
		return new BaseEntityReflection<E, B>();
	}
}
