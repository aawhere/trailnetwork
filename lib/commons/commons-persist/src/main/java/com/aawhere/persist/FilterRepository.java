/**
 *
 */
package com.aawhere.persist;

import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.id.Identifier;
import com.aawhere.util.rb.StatusMessages;

/**
 * Indicates that this repository supports filtering.
 * 
 * @author Brian Chapman
 * 
 */
public interface FilterRepository<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>> {

	/**
	 * Provides access to {@link E} limited to the criteria set in the filter parameter.
	 * 
	 * @param filter
	 *            the criteria for finding Activities
	 * @return the subset of {@link S} limited by the criteria provided in the filter parameter.
	 *         Activities will also contain the updated filter identifying what the Activities
	 *         represents as well as some potentials if they are able to be provided (i.e. total
	 *         count, etc).
	 */
	public S filter(Filter filter);

	public Iterable<E> filterAsIterable(Filter filter);

	/**
	 * Provides all the entities matching the given ids. Any ids not matched will be reported in the
	 * {@link BaseFilterEntities#getStatusMessages()} and will initially indicate such by
	 * {@link StatusMessages#hasError()}.
	 * 
	 * @param ids
	 * @return
	 */
	public S entities(Iterable<I> ids);

	/**
	 * Counts all results in the filter. It will ignore the limit and return all possible results in
	 * the similar manner as {@link #idPaginator(Filter)} and {@link #entityPaginator(Filter)}.
	 * 
	 * @param filter
	 * @return
	 */
	public Integer count(Filter filter);

/**
	 * Returns a connected Iterator that will make repeated calls applying the filter, but
	 * incrementing the page until no more results are found.
	 * 
	 * The page size is determined by the {@link Filter#getLimit()}.
	 * 
	 * This differs from {@link #ids(Filter) since that function only iterates through the
	 * results of a single page.
	 * 
	 * Predicates do not apply to id filters since the predicates must inspect the entity, not just the id.
	 * 
	 * 
	 * @param filter
	 * @return
	 */
	public Iterable<I> idPaginator(Filter filter);

	/**
	 * Provides the ids for a single page of the filter provided.
	 * 
	 * * Predicates do not apply to id filters since the predicates must inspect the entity, not
	 * just the id.
	 * 
	 * @see #idPaginator(Filter)
	 * @param filter
	 * @return
	 */
	public Iterable<I> ids(Filter filter);

	/**
	 * @see #idPaginator(Filter)
	 * @see FilterRepositoryEntityPaginator
	 * @see FilterRepositoryPaginator
	 * @param filter
	 * @return
	 */
	public Iterable<E> entityPaginator(Filter filter);

	/** Provides the FieldDictionary for the EntityClass represented by this repository */
	public FieldDictionary dictionary();

	public FieldDictionaryFactory dictionaryFactory();

}
