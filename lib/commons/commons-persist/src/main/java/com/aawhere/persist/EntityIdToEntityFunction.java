/**
 * 
 */
package com.aawhere.persist;

import java.util.LinkedList;

import javax.annotation.Nullable;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierToIdentifiableFunction;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;

/**
 * A {@link Function} that when given an ID it will return the corresponding {@link BaseEntity} if
 * found. If it's not found null is returned and the ids are available at the end of transformation
 * via {@link #getNotFound()}.
 * 
 * @author aroller
 * 
 */
public class EntityIdToEntityFunction<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		implements IdentifierToIdentifiableFunction<I, E> {

	private Repository<E, I> repository;
	private LinkedList<I> notFound = new LinkedList<>();

	/**
	 * Used to construct all instances of EntityIdToEntityFunction.
	 */
	public static class Builder<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			extends ObjectBuilder<EntityIdToEntityFunction<E, I>> {

		public Builder() {
			super(new EntityIdToEntityFunction<E, I>());
		}

		public Builder<E, I> repository(Repository<E, I> repository) {
			building.repository = repository;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("repository", building.repository);

		}
	}// end Builder

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Builder<E, I> create() {
		return new Builder<E, I>();
	}

	/** Use {@link Builder} to construct EntityIdToEntityFunction */
	private EntityIdToEntityFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public E apply(@Nullable I input) {
		if (input == null) {
			return null;
		}
		try {
			return repository.load(input);
		} catch (EntityNotFoundException e) {
			notFound.add(input);
			return null;
		}
	}

	/**
	 * @return the notFound
	 */
	public LinkedList<I> getNotFound() {
		return this.notFound;
	}

}
