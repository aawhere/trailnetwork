/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;

/**
 * A single place that is aware of all entities in the system and allows for reference in a variety
 * of fashions like looking up a service by it's Identifier, looking up a id class when given the
 * Objectify Key "Kind", etc.
 * 
 * @author aroller
 * 
 */
public interface RegisteredEntities {

	/**
	 * When the kind is known, but nothing else this will produce the Identifier class that is known
	 * to identify that kind.
	 * 
	 * @param kind
	 * @return the corresponding Id
	 * @throws IllegalArgumentException
	 *             when the kind is not known..use {@link #isKindKnown(String)} to avoid
	 */
	public <I extends Identifier<?, ?>> Class<I> idType(String kind);

	/**
	 * Indicates if the kind is registered used to avoid returning nulls and catching exceptions.
	 * 
	 * @param kind
	 * @return
	 */
	public Boolean isKindKnown(String kind);

	public Boolean isIdKnown(Class<? extends Identifier<?, ?>> idType);

}
