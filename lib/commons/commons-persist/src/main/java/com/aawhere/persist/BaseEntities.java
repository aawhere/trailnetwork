/**
 *
 */
package com.aawhere.persist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.aawhere.collections.ImmutableCollection;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;

/**
 * Base class representing a collection of entities.
 * 
 * Implementing classes expeting to use JaxB should override {@link #getCollection()} and
 * {@link #setCollection(List)} and provide an @XmlElement annotation with a name representing the
 * items in the collection since placing a name here makes for unreadable xml.
 * 
 * Example:
 * 
 * @XmlElement(name="item",namespace = XmlNamespace.API_VALUE)
 * 
 *                                   on ActivityImports yields
 * 
 *                                   <pre>
 * <activityImports xmlns:ns2="http://api.trailnetwork.com">
 *  <ns2:item xsi:type="ns2:activityImport"
 * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ....
 * 
 * instead of
 * 
 * <activityImports xmlns:ns2="http://api.trailnetwork.com"><activityImport>...
 * 
 * <pre>
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class BaseEntities<KindT extends BaseEntity<KindT, ?>>
		extends ImmutableCollection<KindT>
		implements Iterable<KindT>, Serializable {

	private static final long serialVersionUID = -334049966241962213L;

	private List<KindT> collection;

	/**
	 * Allowing for a mixed collection of support items, this will provide other entities and other
	 * xml root objects to be included for complete understanding of the results.
	 */
	@XmlAnyElement(lax = true)
	private Collection<Object> support;

	@XmlElement
	private Ordering<KindT> ordering;

	/** Use {@link Builder} to construct BaseEntities */
	protected BaseEntities() {
		this.collection = new ArrayList<KindT>();
		this.support = new ArrayList<>();
	}

	// @XmlAnyElement(lax = true) - this displays the collection, but not locally TN-559
	public List<KindT> getCollection() {
		return collection;
	}

	/**
	 * @return the ordering
	 */
	public Ordering<KindT> ordering() {
		return this.ordering;
	}

	@SuppressWarnings("unchecked")
	public <E extends BaseEntities<?>> E support(Class<E> type) {
		Predicate<Object> instanceOf = Predicates.instanceOf(type);
		final Iterable<? extends Object> filtered = Iterables.filter(this.support, instanceOf);
		return (E) Iterables.getFirst(filtered, null);
	}

	/**
	 * @return the support
	 */
	public Collection<?> support() {
		return this.support;
	}

	/**
	 * Gradual replacement of the {@link #getCollection()} inappropriately exposing a {@link List}.
	 * 
	 * @return
	 */
	public Collection<KindT> all() {
		return this.collection;
	}

	/*
	 * Methods below are the Collection Implementation.
	 */

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#size()
	 */
	@Override
	public int size() {
		return getCollection().size();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return getCollection().isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o) {
		return getCollection().contains(o);
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#iterator()
	 */
	@Override
	public Iterator<KindT> iterator() {
		return getCollection().iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#toArray()
	 */
	@Override
	public Object[] toArray() {
		return getCollection().toArray();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#toArray(T[])
	 */
	@Override
	public <T> T[] toArray(T[] a) {
		return getCollection().toArray(a);
	}

	@Override
	public final boolean equals(Object obj) {
		if (!(obj instanceof BaseEntities<?>)) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		BaseEntities<?> rhs = (BaseEntities<?>) obj;
		if (this.collection.equals(rhs.collection)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getCollection().toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		return new HashCodeBuilder(17, 37).append(collection).toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Collection#containsAll(java.util.Collection)
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		return getCollection().containsAll(c);
	}

	/**
	 * Used to construct all instances of BaseEntities.
	 */
	@XmlTransient
	public static abstract class Builder<BuilderT extends Builder<BuilderT, KindT, KindsT>, KindT extends BaseEntity<KindT, ?>, KindsT extends BaseEntities<KindT>>
			extends AbstractObjectBuilder<KindsT, BuilderT> {

		private Function<KindT, KindT> transformer;

		public Builder(KindsT entities, List<KindT> collection) {
			super(entities);
			building().collection = collection;
		}

		/** Uses the default provided collection */
		public Builder(KindsT entities) {
			super(entities);
		}

		public BuilderT transformer(Function<KindT, KindT> transformer) {
			this.transformer = transformer;
			return dis;
		}

		public BuilderT add(KindsT items) {
			addAll(((BaseEntities<KindT>) items).collection);
			return dis;
		}

		/** The most efficient way to add a single entity */
		public BuilderT add(KindT item) {
			collection().add(item);
			return dis;
		}

		public BuilderT add(KindT... items) {
			addAll(Arrays.asList(items));
			return dis;
		}

		/** Adds every entity from all other Entities. */
		public BuilderT addOthers(Iterable<KindsT> others) {
			for (KindsT other : others) {
				addAll(other);
			}
			return dis;
		}

		public BuilderT addAll(Iterator<KindT> iterable) {
			Iterators.addAll(collection(), iterable);
			return dis;
		}

		public BuilderT addAll(Iterable<KindT> iterable) {
			Iterables.addAll(collection(), iterable);
			return dis;
		}

		public BuilderT addAll(Collection<KindT> collection) {
			collection().addAll(collection);
			return dis;
		}

		public BuilderT addSupport(Object... support) {
			if (ArrayUtils.isNotEmpty(support)) {
				for (Object item : support) {
					building().support.add(item);
				}
			}
			return dis;
		}

		public BuilderT ordering(Ordering<KindT> ordering) {
			building().ordering = ordering;
			return dis;
		}

		/**
		 * @return
		 */
		private List<KindT> collection() {
			return building().collection;
		}

		/**
		 * @return
		 */
		private BaseEntities<KindT> building() {
			return building;
		}

		@Override
		protected void validate() {
			Assertion.assertNotNull("Must set the collection first!", collection());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public KindsT build() {
			BaseEntities<KindT> built = super.build();
			if (this.transformer != null) {
				built.collection = Lists.transform(built.collection, this.transformer);
			}
			// memory sort if presented with an ordering to indicate such.
			if (built.ordering != null) {
				built.collection = built.ordering.sortedCopy(built.collection);
			}
			return (KindsT) built;

		}
	}// end Builder

}
