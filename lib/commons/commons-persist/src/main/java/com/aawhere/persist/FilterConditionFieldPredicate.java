/**
 * 
 */
package com.aawhere.persist;

import java.util.Collections;

import javax.annotation.Nullable;

import com.aawhere.field.FieldKey;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Predicate;

/**
 * A {@link Predicate} used to filter {@link FilterCondition}s from {@link Collections} based on the
 * provided {@link #field}.
 * 
 * 
 * @author aroller
 * 
 */
public class FilterConditionFieldPredicate
		implements Predicate<FilterCondition> {

	/**
	 * Used to construct all instances of FilterConditionFieldPredicate.
	 */
	public static class Builder
			extends ObjectBuilder<FilterConditionFieldPredicate> {

		public Builder() {
			super(new FilterConditionFieldPredicate());
		}

		public Builder field(String field) {
			building.field = field;
			return this;
		}

		public Builder field(FieldKey field) {
			return field(field.getAbsoluteKey());
		}

		public FilterConditionFieldPredicate build() {
			FilterConditionFieldPredicate built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private String field;

	/** Use {@link Builder} to construct FilterConditionFieldPredicate */
	private FilterConditionFieldPredicate() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(@Nullable FilterCondition input) {
		return field.equals(input.getField());
	}

}
