/**
 *
 */
package com.aawhere.search;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Localizable Messages for the Search Package
 * 
 * @author Brian Chapman
 * 
 */
public enum SearchMessage implements Message {

	/** @see Max Retry Exceeded */
	MAX_RETRY_EXCEEDED("Failed {RETRY_COUNT} times out of {RETRY_COUNT} attempts.", Param.RETRY_COUNT),
	//
	INDEX_NOT_FOUND("Search index \"{INDEX}\" was not found.", Param.INDEX);

	private ParamKey[] parameterKeys;
	private String message;

	private SearchMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** @see Retry count */
		RETRY_COUNT, INDEX;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}
}
