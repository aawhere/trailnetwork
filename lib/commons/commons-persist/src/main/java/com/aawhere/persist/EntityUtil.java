/**
 *
 */
package com.aawhere.persist;

import java.lang.reflect.Type;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.joda.time.DateTime;

import com.aawhere.collections.Index;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.reflect.GenericUtils;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;

/**
 * @author Brian Chapman
 * 
 */
public class EntityUtil {

	/**
	 * Utility classes cannot be instantiated.
	 */
	private EntityUtil() {
	}

	public static <E extends BaseEntity<?, I>, I extends Identifier<?, ?>> Class<I> getIdentifierType(E entity) {
		final Class<E> entityType = (Class<E>) entity.getClass();
		return getIdentifierType(entityType);
	}

	/**
	 * Provides the identifier type for the BaseEntity given.
	 * 
	 * @param entityType
	 * @return
	 */
	public static <E extends BaseEntity<?, I>, I extends Identifier<?, ?>> Class<I> getIdentifierType(
			final Class<E> entityType) {
		return GenericUtils.getTypeArgument(entityType, Index.SECOND);
	}

	/**
	 * @see #isEntity(Object)
	 * 
	 * @param anythingClass
	 * @return
	 */
	public static Boolean isEntity(Class<?> anythingClass) {
		return BaseEntity.class.isAssignableFrom(anythingClass);
	}

	/**
	 * A single place for us to manage what our system thinks is an Entity. Currently, inherits from
	 * BaseEntity, however, that could be expanded to inspect annotations for good reason.
	 * 
	 * @param anything
	 * @return
	 */
	public static Boolean isEntity(Object anything) {
		return anything instanceof BaseEntity;
	}

	/**
	 * When really in generic world types aren't classes, but ParameterizedTypes (like Identifier).
	 * 
	 * @param type
	 * @return
	 */
	public static Boolean identifiesEntity(Type type) {
		if (IdentifierUtils.isIdentifierType(type)) {
			Class<? extends Identifier<?, ?>> clazz = IdentifierUtils.typeToClass(type);
			return identifiesEntity(clazz);
		}
		return false;
	}

	/**
	 * Inspects the given {@link Identifier} to see if it's kind is an Entity as defined by
	 * {@link #isEntity(Class)}.
	 * 
	 * @param idClass
	 */
	public static Boolean identifiesEntity(Class<? extends Identifier<?, ?>> idClass) {

		try {
			Class<?> kind = IdentifierUtils.kindFromUnknown(idClass);
			return isEntity(kind);
		} catch (Exception e) {
			// FIXME:this is a lame way to find out so pre-inspect
			return false;
		}

	}

	/**
	 * Given an entity this will check the {@link BaseEntity#getDateUpdated()} value and return true
	 * if it is after the given timestamp.
	 * 
	 * @param entity
	 * @param timestamp
	 * @return
	 */
	public static Boolean modifiedSince(BaseEntity<?, ?> entity, DateTime timestamp) {
		return entity.getDateUpdated().isAfter(timestamp);
	}

	public static Boolean modifiedBefore(BaseEntity<?, ?> entity, DateTime timestamp) {
		return entity.getDateUpdated().isBefore(timestamp);
	}

	/**
	 * if {@link BaseEntity#dateUpdated()} is before the time given or null the entity will be
	 * selected for the filter.
	 * 
	 * @param time
	 * @return
	 */
	public static <KindT extends BaseEntity<KindT, ?>> Predicate<KindT> modifiedBeforePredicate(DateTime time) {
		final Predicate<KindT> isNullPredicate = Predicates.isNull();
		final Predicate<KindT> modifiedPredicate = modifiedPredicate(Range.atMost(time));
		return Predicates.or(isNullPredicate, modifiedPredicate);
	}

	/**
	 * Only if the {@link BaseEntity#dateUpdated()} is after the time given will the entity be
	 * selected.
	 * 
	 * @param time
	 * @return
	 */
	public static <KindT extends BaseEntity<KindT, ?>> Predicate<KindT> modifiedAfterPredicate(DateTime time) {
		return modifiedPredicate(Range.atLeast(time));
	}

	/**
	 * @param range
	 * @return
	 */
	private static <KindT extends BaseEntity<KindT, ?>> Predicate<KindT> modifiedPredicate(Range<DateTime> range) {
		final Function<KindT, DateTime> dateUpdatedFunction = dateUpdatedFunction();
		return Predicates.compose(range, dateUpdatedFunction);
	}

	public static <KindT extends BaseEntity<KindT, ?>> Function<KindT, DateTime> dateUpdatedFunction() {
		return new Function<KindT, DateTime>() {

			@Override
			public DateTime apply(KindT input) {
				return (input == null) ? null : input.dateUpdated();
			}
		};
	}

	public static <KindT extends BaseEntity<KindT, ?>> Function<KindT, DateTime> dateCreatedFunction() {
		return new Function<KindT, DateTime>() {

			@Override
			public DateTime apply(KindT input) {
				return (input == null) ? null : input.dateCreated();
			}
		};
	}

	/**
	 * Provides the ability to find if any entity has been persisted into the datastore by
	 * inspecting the {@link BaseEntity#getEntityVersion()} to see if it's past the default.
	 * 
	 * @param entity
	 * @return true if the entity version has incremented beyond default.
	 */
	@Nonnull
	public static Boolean isPersisted(@Nullable BaseEntity<?, ?> entity) {
		return entity != null && !BaseEntity.DEFAULT_VERSION.equals(entity.getEntityVersion());
	}

	/**
	 * 
	 * @param entities
	 * @return the first entity that answers true for {@link #isPersisted(BaseEntity)} or null in
	 *         none do
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> E persisted(Iterable<E> entities) {
		for (E e : entities) {
			if (isPersisted(e)) {
				return e;
			}
		}
		return null;
	}

	/**
	 * Returns true if any of the candidates is younger than the target entity, otherwise false. the
	 * target does not necessarily need to be the same type of entity as the candidates, although it
	 * could be.
	 */
	@Nonnull
	public static
			<E extends BaseEntity<E, I>, I extends Identifier<?, E>, E2 extends BaseEntity<E2, I2>, I2 extends Identifier<?, E2>>
			Boolean isStale(@Nullable E target, @Nullable Iterable<E2> candidates) {
		if (target == null || candidates == null) {
			return false;
		}
		Iterable<E2> dateCreatedYoungestFirst = EntityUtil.dateUpdatedOldestFirst(candidates);
		E2 first = Iterables.getFirst(dateCreatedYoungestFirst, null);
		return first != null && first.dateUpdated().isAfter(target.dateUpdated());
	}

	/**
	 * @param mergeCandidates
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<E> dateCreatedOldestFirst(
			Iterable<E> entities) {
		final Function<E, DateTime> function = dateCreatedFunction();
		return dateOldestFirst(entities, function);
	}

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<E> dateUpdatedOldestFirst(
			Iterable<E> entities) {

		final Function<E, DateTime> function = dateUpdatedFunction();
		// FIXME: generic hell kept me from chaining properly AR 2014-07
		return dateOldestFirst(entities, function);
	}

	public static <E extends BaseEntity<E, ?>> Ordering<E> dateCreatedOldestFirst() {
		Function<E, DateTime> function = dateCreatedFunction();
		return Ordering.natural().onResultOf(function);
	}

	public static <E extends BaseEntity<E, ?>> Ordering<E> dateCreatedYoungestFirst() {
		Function<E, DateTime> function = dateCreatedFunction();
		return Ordering.natural().reverse().onResultOf(function);
	}

	public static <E extends BaseEntity<E, ?>> Ordering<E> dateUpdatedOldestFirst() {
		Function<E, DateTime> function = dateUpdatedFunction();
		return Ordering.natural().onResultOf(function);
	}

	public static <E extends BaseEntity<E, ?>> Ordering<E> dateUpdatedYoungestFirst() {
		Function<E, DateTime> function = dateUpdatedFunction();
		return Ordering.natural().reverse().onResultOf(function);
	}

	/**
	 * Provides an easy way to access the {@link ServicedBy}annotation, if available.
	 * 
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Nullable
	public static <S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			Class<ServiceGeneral<?, ?, ?>> servicedBy(@Nonnull Class<?> type) {
		ServicedBy annotation = type.getAnnotation(ServicedBy.class);
		return (Class<ServiceGeneral<?, ?, ?>>) ((annotation != null) ? annotation.value() : null);
	}

	/**
	 * @param entities
	 * @param function
	 * @return
	 */
	private static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<E> dateOldestFirst(
			Iterable<E> entities, final Function<E, DateTime> function) {
		final Ordering<E> ordering = Ordering.natural().onResultOf(function);
		return ordering.sortedCopy(entities);
	}

	/**
	 * Orders by descending create date providing the most recent created first.
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<E> dateCreatedYoungestFirst(
			Iterable<E> entities) {
		final Function<E, DateTime> function = dateCreatedFunction();
		return dateYoungestFirst(entities, function);
	}

	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<E> dateUpdatedYoungestFirst(
			Iterable<E> entities) {
		final Function<E, DateTime> function = dateUpdatedFunction();
		return dateYoungestFirst(entities, function);
	}

	/**
	 * @param entities
	 * @param function
	 * @return
	 */
	private static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<E> dateYoungestFirst(
			Iterable<E> entities, final Function<E, DateTime> function) {
		final Ordering<E> ordering = Ordering.natural().reverse().onResultOf(function);
		return ordering.sortedCopy(entities);
	}

	/**
	 * When given a class that may of type {@link BaseEntities}, this will return the
	 * {@link BaseEntity} type that is contained in the collection, otherwise a null.
	 * 
	 * @param possibleEntitiesType
	 * @return the type contained by the container or null if this is not an entities type
	 */
	@SuppressWarnings("unchecked")
	@Nullable
	public static <S extends BaseEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>> Class<E>
			entityTypeContained(@Nonnull Class<?> possibleEntitiesType) {

		Class<E> entityType = null;
		if (BaseEntities.class.isAssignableFrom(possibleEntitiesType)) {
			Class<S> entitiesType = (Class<S>) possibleEntitiesType;
			List<Class<?>> typeArguments = GenericUtils.getTypeArguments(BaseEntities.class, entitiesType);
			entityType = (Class<E>) typeArguments.get(0);
		}
		return entityType;
	}
}
