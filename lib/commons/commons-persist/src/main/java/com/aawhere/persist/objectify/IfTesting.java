/**
 * 
 */
package com.aawhere.persist.objectify;

import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.ValueIf;

/**
 * Simple conditional to be used with {@link Index} FIXME: This is not ideal because we have test
 * code in the system. A better solution is welcome.
 * 
 * TN-357
 * 
 * @author aroller
 * 
 */
public class IfTesting
		extends ValueIf<Object> {

	private static boolean testing;
	static {
		try {
			// simple test to see if a required test class is available
			Class.forName("com.aawhere.test.category.UnitTest");
			testing = true;
		} catch (ClassNotFoundException e) {
			testing = false;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.googlecode.objectify.condition.If#matchesValue(java.lang.Object)
	 */
	@Override
	public boolean matchesValue(Object arg0) {
		return testing;
	}

}
