/**
 * 
 */
package com.aawhere.persist;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.aawhere.id.Identifier;

/**
 * Typically used by an {@link Identifier} this annotation will expose the {@link ServiceStandard}
 * that will produce the corresponding {@link BaseEntity} for the identifier.
 * 
 * @author aroller
 * 
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ServicedBy {

	/**
	 * The specific implementation of ServiceStandard that will provide the entity when given the
	 * identifier that holds this annotation.
	 */
	Class<? extends ServiceGeneral<?, ?, ?>> value();
}
