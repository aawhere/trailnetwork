/**
 * 
 */
package com.aawhere.persist;

import java.util.Collection;
import java.util.Set;

import com.aawhere.id.Identifier;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Sets;

/**
 * Given a collection of identifiers this will execute the standard In query against the repository
 * provided and return the resulting {@link Iterable} of the ids that not in. The given collection
 * must be limited in size restricted by {@link Filter.BaseBuilder#MAX_VALUES_FOR_IN_OPERATOR}.
 * 
 * @see RepositoryUtil#nonExistingEntityIds(Iterable, FilterRepository)
 * 
 * @author aroller
 * 
 */
public class NonExistingEntitiesFunction<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		implements Function<Collection<I>, Set<I>> {

	/** USeful for the producer of the input to limit the size as required. */
	public static Integer INPUT_SIZE_LIMIT = Filter.BaseBuilder.MAX_VALUES_FOR_IN_OPERATOR;

	private FilterRepository<S, E, I> repository;

	public static <S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			NonExistingEntitiesFunction<S, E, I> build(FilterRepository<S, E, I> repository) {
		NonExistingEntitiesFunction<S, E, I> function = new NonExistingEntitiesFunction<>();
		function.repository = repository;
		return function;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Set<I> apply(Collection<I> input) {
		Set<I> ids = Sets.newHashSet(input);
		ids = Sets.filter(ids, Predicates.notNull());
		Filter filter = Filter.create().idsIn(ids).build();
		Iterable<I> found = repository.idPaginator(filter);
		return Sets.difference(ids, Sets.newHashSet(found));
	}

}
