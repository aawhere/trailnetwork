/**
 *
 */
package com.aawhere.persist;

import com.aawhere.lang.reflect.BuilderReflection;

/**
 * Useful helpers for constructing {@link BaseEntity} using their {@link BaseEntity.Builder}.
 * 
 * @author Aaron Roller
 * 
 */
public class BaseEntitiesReflection<E extends BaseEntities<?>, B extends BaseEntities.Builder<B, ?, E>>
		extends BuilderReflection<E, B> {

	public static <E extends BaseEntities<?>, B extends BaseEntities.Builder<B, ?, E>> BaseEntitiesReflection<E, B>
			newInstance() {
		return new BaseEntitiesReflection<E, B>();
	}
}
