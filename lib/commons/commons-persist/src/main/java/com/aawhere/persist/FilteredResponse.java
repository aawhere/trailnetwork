/**
 * 
 */
package com.aawhere.persist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.util.rb.StatusMessages;
import com.aawhere.xml.XmlNamespace;

/**
 * Similar to {@link BaseFilterEntities}, this is a filter response for any object that may be the
 * result of a filter applied to the datastore against an entity, but the object <T> may not be the
 * entity itself. A common example may be the identifiers of the Entity for minimal response sizes.
 * 
 * @author aroller
 * 
 * @param <T>
 *            Any object that may be the result of an applied {@link Filter}.
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class FilteredResponse<T>
		implements Serializable, Iterable<T>, FilterProvider {
	private static final long serialVersionUID = -869546893828900887L;
	@XmlElement
	private Filter filter;

	@XmlElement
	private StatusMessages statusMessages;

	/**
	 * The collection of T that represents the results of the applied filter.
	 * 
	 */
	@XmlAnyElement(lax = true)
	private Collection<T> items;

	/**
	 * Used to construct all instances of FilteredResponse.
	 */
	@XmlTransient
	public static class Builder<T>
			extends ObjectBuilder<FilteredResponse<T>> {

		/**
		 * @see FilteredResponse#create()
		 * @see FilteredResponse#create(Collection)
		 */
		private Builder() {
			super(new FilteredResponse<T>());
		}

		/** Use {@link #create(Collection)} */
		private <C extends Collection<T>> Builder<T> setAll(C all) {
			building.items = all;
			return this;
		}

		public Builder<T> add(T item) {
			building.items.add(item);
			return this;
		}

		public Builder<T> setStatusMessages(StatusMessages statusMessages) {
			building.statusMessages = statusMessages;
			return this;
		}

		public Builder<T> setFilter(Filter filter) {
			building.filter = filter;
			return this;
		}

	}// end Builder

	/** Creates with a default collection of List. */
	public static <T> Builder<T> create() {
		return new Builder<T>().setAll(new ArrayList<T>());
	}

	public static <T> Builder<T> create(Collection<T> all) {
		return new Builder<T>().setAll(all);
	}

	/** Use {@link Builder} to construct FilteredResponse */
	private FilteredResponse() {
	}

	/**
	 * @return the all
	 */
	public Collection<T> getItems() {
		return this.items;
	}

	/**
	 * @return the statusMessages
	 */
	public StatusMessages getStatusMessages() {
		return this.statusMessages;
	}

	/**
	 * @return the filter
	 */
	public Filter getFilter() {
		return this.filter;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<T> iterator() {
		return items.iterator();
	}
}
