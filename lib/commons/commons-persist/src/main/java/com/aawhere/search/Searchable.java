/**
 *
 */
package com.aawhere.search;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that this class should be indexed in the full text search service.
 * 
 * @author Brian Chapman
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Searchable {

	String[] fieldKeys();
}
