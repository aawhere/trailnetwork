package com.aawhere.persist;

import java.net.HttpURLConnection;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.id.Identifier;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.StatusMessage;
import com.aawhere.util.rb.StatusMessages;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * Exception to report when an entity is not found in the repository.
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(value = HttpStatusCode.NOT_FOUND, message = EntityNotFoundException.Doc)
public class EntityNotFoundException
		extends BaseException {

	/**
	 * 
	 */
	private static final PersistMessage MESSAGE_KEY = PersistMessage.ENTITY_NOT_FOUND;
	public static final String Doc = PersistMessage.Doc.ENTITY_NOT_FOUND;
	private static final long serialVersionUID = 3708095962316518804L;

	public static final int CODE = HttpURLConnection.HTTP_NOT_FOUND;
	private Set<? extends Identifier<?, ?>> ids;

	public EntityNotFoundException(Iterable<? extends Identifier<?, ?>> ids, Exception cause) {
		super(message(ids), cause);
		ids(ids);
	}

	/**
	 * @param ids
	 */
	private void ids(Iterable<? extends Identifier<?, ?>> ids) {
		this.ids = SetUtilsExtended.asSet(ids);
	}

	public EntityNotFoundException(Exception cause, Identifier<?, ?>... id) {
		this(Sets.newHashSet(id), cause);
	}

	public EntityNotFoundException(Identifier<?, ?> id, Exception cause) {
		this(cause, id);
	}

	public EntityNotFoundException(Identifier<?, ?>... id) {
		this((Exception) null, id);
	}

	protected EntityNotFoundException(CompleteMessage message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param routeId
	 * @param build
	 */
	public EntityNotFoundException(CompleteMessage message, Identifier<?, ?>... ids) {
		super(message);
		ids(Sets.newHashSet(ids));
	}

	public static CompleteMessage message(Identifier<?, ?>... id) {
		return message(Sets.newHashSet(id));
	}

	/**
	 * @param id
	 * @return
	 */
	public static CompleteMessage message(Iterable<? extends Identifier<?, ?>> id) {
		String idValues = StringUtils.join(id, ",");

		return new CompleteMessage.Builder().setMessage(MESSAGE_KEY)
				.addParameter(PersistMessage.Param.OBJECT_ID, idValues)
				.addParameter(PersistMessage.Param.OBJECT_NAME, Iterables.getLast(id).getKind()).build();
	}

	public static EntityNotFoundException notFound(Iterable<? extends Identifier<?, ?>> ids) {
		return new EntityNotFoundException(ids, null);
	}

	/**
	 * Conditionally throws EntityNotFoundException if the given ids is not empty.
	 * 
	 * @param ids
	 * @throws EntityNotFoundException
	 */
	public static void throwIfNotFound(Iterable<? extends Identifier<?, ?>> ids) throws EntityNotFoundException {
		if (!Iterables.isEmpty(ids)) {
			throw notFound(ids);
		}
	}

	public Identifier<?, ?> getId() {
		return Iterables.getLast(this.ids);
	}

	/**
	 * @return the id
	 */
	public Set<? extends Identifier<?, ?>> getIds() {
		return this.ids;
	}

	/**
	 * For special cases where the entity is not found. This is reported in the same manner, but the
	 * status code is {@link HttpStatusCode#ACCEPTED}.
	 * 
	 * @param routeId
	 */
	public static EntityNotFoundException forQueue(Identifier<?, ?> id) {
		EntityNotFoundException entityNotFoundException = new EntityNotFoundException(id);
		entityNotFoundException.setStatusCode(HttpStatusCode.ACCEPTED);
		return entityNotFoundException;
	}

	/**
	 * @param statusMessages
	 * @throws EntityNotFoundException
	 */
	public static void throwIf(StatusMessages statusMessages) throws EntityNotFoundException {
		StatusMessage statusMessage;
		if ((statusMessage = statusMessages.statusMessage(MESSAGE_KEY)) != null) {
			throw new EntityNotFoundException(statusMessage.message);
		}
	}

	/**
	 * Modifies the given exception to indicate the request has been accepted.
	 * 
	 * @param e
	 */
	public static EntityNotFoundException forQueue(EntityNotFoundException e) {
		e.setStatusCode(HttpStatusCode.ACCEPTED);
		return e;
	}

	/**
	 * Modifies the current exception to indicate that the entity not found is not the fault of the
	 * client and the system is undergoing repairs.
	 * 
	 * @return
	 */
	public ToRuntimeException comeBackLater() {
		return ToRuntimeException.systemBeingRepaired(this);
	}

	public static EntityNotFoundException notFound(CompleteMessage message) {
		return new EntityNotFoundException(message, (Throwable) null);
	}
}
