/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;
import com.aawhere.lang.ObjectBuilder;

/**
 * Exposes the standard builder update method to be used for updating Entities using the newly
 * introduced Builder parameter.
 * 
 * This is an interim interface to demonstrate widespread value and appropriateness.
 * 
 * @author aroller
 * 
 * @param <E>
 *            entity
 * @param <I>
 *            identifier for entity
 * @param <IV>
 *            the value for the identifier (string or long)
 * @param <EB>
 *            the builder for the entity
 */
public interface RepositoryUpdateable<E extends BaseEntity<E, I>, I extends Identifier<IV, E>, IV extends Comparable<IV>, EB extends BaseEntity.Builder<E, IV, I, EB>>
		extends Repository<E, I> {

	/**
	 * Provides a datastore connected {@link ObjectBuilder} that updates the datastore with the
	 * results of the mutated entity.
	 * 
	 * @param id
	 *            identifies the entity to mutate
	 * @return the builder that will finalize the mutation and persist the updated information into
	 *         the datastore
	 * @throws EntityNotFoundException
	 */
	EB update(I id) throws EntityNotFoundException;
}
