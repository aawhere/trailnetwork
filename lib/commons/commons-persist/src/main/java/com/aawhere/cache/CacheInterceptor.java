/**
 *
 */
package com.aawhere.cache;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import com.aawhere.collections.Index;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * A {@link MethodInterceptor} that caches the return value of a method annotated with {@link Cache}
 * .
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class CacheInterceptor
		implements MethodInterceptor {

	CacheService cache;

	@Inject
	public CacheInterceptor() {
	}

	@Inject
	protected void setCacheService(CacheService cacheService) {
		this.cache = cacheService;
	}

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Object cacheKey = getCacheKey(invocation.getArguments(), invocation.getMethod());
		Object returnValue = cache.get(cacheKey);
		if (returnValue == null) {
			// nothing in cache, return control to method.
			returnValue = invocation.proceed();
			cache.put(cacheKey, returnValue);
		}
		return returnValue;
	}

	/**
	 * @param arguments
	 * @return
	 */
	private Object getCacheKey(Object[] arguments, Method method) {
		Class<?>[] parameters = method.getParameterTypes();
		Annotation[][] paramaterAnnotations = method.getParameterAnnotations();

		if (parameters.length == 0) {
			throw new IllegalArgumentException("Cannot cache method that does not have any paramaters.");
		}

		for (int i = 0; i < parameters.length; i++) {
			if (hasKeyAnnotation(paramaterAnnotations[i])) {
				return arguments[i];
			}
		}
		if (parameters.length == 1) {
			return arguments[Index.FIRST.index];
		} else {
			throw new IllegalArgumentException(
					"Cannot cache method that doesn't define a Key. See docs for Cache annotation.");
		}
	}

	private boolean hasKeyAnnotation(Annotation[] annotations) {
		for (Annotation anno : annotations) {
			if (anno instanceof Cache.Key) {
				return true;
			}
		}
		return false;
	}
}
