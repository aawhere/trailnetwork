package com.aawhere.persist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.xml.XmlNamespace;

import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Unindex;

@Unindex
@XmlType(namespace = XmlNamespace.API_VALUE, name = "long-id-entity")
@XmlAccessorType(XmlAccessType.NONE)
public abstract class LongBaseEntity<KindT extends LongBaseEntity<KindT, IdentifierT>, IdentifierT extends Identifier<Long, KindT>>
		extends BaseEntity<KindT, IdentifierT> {

	private static final long serialVersionUID = 6108646129729189330L;
	@Id
	@XmlAttribute
	private Long id;

	@Override
	protected Long idValue() {
		return id;
	}

	/**
	 * Used to construct all instances of LongBaseEntity.
	 * 
	 * @param <T>
	 */
	@XmlTransient
	@XmlAccessorType(XmlAccessType.NONE)
	public abstract static class Builder<KindT extends LongBaseEntity<KindT, IdentifierT>, BuilderT extends BaseEntity.Builder<KindT, Long, IdentifierT, BuilderT>, IdentifierT extends Identifier<Long, KindT>>
			extends BaseEntity.Builder<KindT, Long, IdentifierT, BuilderT> {

		/**
		 * @deprecated use other constructor
		 * 
		 * @param entity
		 * @param identifierType
		 */
		@Deprecated
		public Builder(KindT entity, Class<IdentifierT> identifierType) {
			this(entity);
		}

		public Builder(KindT entity) {
			super(entity);
		}

		@Override
		protected BuilderT setId(IdentifierT id) {
			Long existingValue = ((LongBaseEntity<KindT, IdentifierT>) building).id;
			if (existingValue != null) {
				Assertion.exceptions().eq(existingValue, id.getValue());
			}
			((LongBaseEntity<KindT, IdentifierT>) building).id = id.getValue();
			return dis;
		}

	}// end Builder

	/** Use {@link Builder} to construct LongBaseEntity */
	protected LongBaseEntity() {
	}
}
