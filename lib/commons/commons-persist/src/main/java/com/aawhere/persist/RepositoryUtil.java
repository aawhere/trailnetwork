/**
 *
 */
package com.aawhere.persist;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;

import com.aawhere.id.Identifier;
import com.aawhere.log.LoggerFactory;
import com.google.common.collect.Iterables;

/**
 * Common tasks that your {@link Repository} needs to do.
 * 
 * @author roller
 * 
 */
public class RepositoryUtil {

	/**
	 * Utility classes cannot be instantiated.
	 */
	private RepositoryUtil() {
	}

	/**
	 * Very common for finders to throw {@link EntityNotFoundException} instead of returning null.
	 * 
	 * @param identifier
	 *            the id of the item being searched for.
	 * @param result
	 *            the item searched for identified by identifier
	 * @return the given result if its not null
	 * @throws EntityNotFoundException
	 *             when the given result is null
	 */
	@Nonnull
	public static <T extends BaseEntity<T, ?>, IdT extends Identifier<?, ?>> T finderResult(@Nonnull IdT identifier,
			@Nullable T result) throws EntityNotFoundException {
		if (result == null) {
			throw new EntityNotFoundException(identifier);
		} else {
			return result;
		}
	}

	/**
	 * Grabs the first result of the given collection or throws an {@link EntityNotFoundException}
	 * if none found or logs a warning if multiple is found.
	 * 
	 * @param id
	 * @param entities
	 * @return
	 * @throws EntityNotFoundException
	 */

	public static <T extends BaseEntity<T, ?>, IdT extends Identifier<?, ?>, E extends BaseEntities<T>> T finderResult(
			IdT id, E entities) throws EntityNotFoundException {
		return finderResult(id, (Collection<T>) entities);
	}

	/**
	 * Grabs the first result of the given collection or throws an {@link EntityNotFoundException}
	 * if none found or logs a warning if multiple is found.
	 * 
	 * @param identifier
	 * @param result
	 * @return
	 * @throws EntityNotFoundException
	 */
	public static <T extends BaseEntity<T, ?>, IdT extends Identifier<?, ?>, C extends Collection<T>> T finderResult(
			@Nonnull IdT identifier, @Nullable C result) throws EntityNotFoundException {
		if (CollectionUtils.isEmpty(result)) {
			throw new EntityNotFoundException(identifier);
		} else if (result.size() > 1) {
			LoggerFactory.getLogger(RepositoryUtil.class).warning("finder for " + identifier.getQualifiedString()
					+ " found multiple results: " + result);
		}

		return result.iterator().next();

	}

	/**
	 * Helper to throw the standard {@link EntityNotFoundException} when
	 * {@link Repository#exists(Identifier)} indicates false.
	 * 
	 * @param repository
	 * @param id
	 * @return true if true, but false throws an exception. Use
	 *         {@link Repository#exists(Identifier)} directly if you want a false
	 * @throws EntityNotFoundException
	 *             when it doesn't exist
	 */
	public static <E extends BaseEntity<E, I>, I extends Identifier<?, E>> Boolean exists(Repository<E, I> repository,
			I id) throws EntityNotFoundException {
		Boolean exists = repository.exists(id);
		if (!exists) {
			throw new EntityNotFoundException(id);
		}
		return exists;
	}

	/**
	 * Given an iterable that has ids in search of this will return only those ids that don't yet
	 * exist in the given repository. The iterable is a connected iterable that will not drain
	 * resources, but call the repository as it iterates through the given iterable. In other words,
	 * it won't drain one into a big heap and then create another big heap, but work in pieces (or
	 * at least that is the goal).
	 * 
	 * @param ids
	 * @param repository
	 * @return
	 */
	public static <S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>> Iterable<I>
			nonExistingEntityIds(Iterable<I> ids, FilterRepository<S, E, I> repository) {

		// breaks up the ids into reasonable chunks for IN queries
		Iterable<List<I>> partition = Iterables.partition(ids, NonExistingEntitiesFunction.INPUT_SIZE_LIMIT);
		// changes the input into only those that don't exist
		Iterable<Set<I>> existingIdPartitions = Iterables.transform(partition,
																	NonExistingEntitiesFunction.build(repository));
		return Iterables.concat(existingIdPartitions);
	}

}
