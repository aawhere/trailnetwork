/**
 *
 */
package com.aawhere.persist;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Provides personalized messages for {@link BaseEntity}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlTransient
public enum EntityMessage implements Message {

	ID_NAME("Identifier"),
	/* */
	ID_DESCRIPTION("An identifier that is unique within the system."),
	DATE_CREATED_NAME("Date created"),
	DATE_CREATED_DESCRIPTION("Date entity was created."),
	DATE_UPDATED_NAME("Date updated"),
	DATE_UPDATED_DESCRIPTION("Date entity was last updated.");

	private ParamKey[] parameterKeys;
	private String message;

	private EntityMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

}
