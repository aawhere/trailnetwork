/**
 *
 */
package com.aawhere.search;

import java.util.HashSet;
import java.util.Set;

import com.aawhere.field.Report;
import com.aawhere.id.Identifier;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.Filter;

/**
 * Holds the results from a search.
 * 
 * @author Brian Chapman
 * 
 */
public class SearchResult {

	private Set<? extends Identifier<?, ?>> ids = new HashSet<Identifier<?, ?>>();
	private Filter filter;
	private Report results;

	/**
	 * @return the filter
	 */
	public Filter getFilter() {
		return this.filter;
	}

	/**
	 * @return the results
	 */
	public Report getResults() {
		return this.results;
	}

	/**
	 * @return the ids
	 */
	public Set<? extends Identifier<?, ?>> getIds() {
		return this.ids;
	}

	/**
	 * Used to construct all instances of Result.
	 */
	public static class Builder
			extends ObjectBuilder<SearchResult> {

		public Builder() {
			super(new SearchResult());
		}

		public Builder(SearchResult mutaee) {
			super(mutaee);
		}

		public Builder setFilter(Filter filter) {
			building.filter = filter;
			return this;
		}

		public Builder setResult(Report result) {
			building.results = result;
			return this;
		}

		public Builder setIds(Set<? extends Identifier<?, ?>> ids) {
			building.ids = ids;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct Result */
	private SearchResult() {
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(SearchResult mutaee) {
		return new Builder(mutaee);
	}

}
