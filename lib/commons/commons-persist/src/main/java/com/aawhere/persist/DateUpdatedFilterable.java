/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.persist.objectify.IfDateUpdatedFilterable;

/**
 * A flag indicating the implementing entity requires searching by update date. This allows multiple
 * entities to have the create date stored without indexing.
 * 
 * @see IfDateUpdatedFilterable
 * 
 * @author aroller
 * 
 */
public interface DateUpdatedFilterable {

}
