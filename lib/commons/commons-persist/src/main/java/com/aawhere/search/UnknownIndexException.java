/**
 * 
 */
package com.aawhere.search;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates that the search index was not found in our system.
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(value = HttpStatusCode.NOT_FOUND)
public class UnknownIndexException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 6037396130643716198L;

	public UnknownIndexException(String index) {
		super(new CompleteMessage.Builder().addParameter(SearchMessage.Param.INDEX, index)
				.setMessage(SearchMessage.INDEX_NOT_FOUND).build());
	}
}
