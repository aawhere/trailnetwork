/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;

/**
 * Combines the Repository and {@link FilterRepository} into a single interface for this common
 * situation.
 * 
 * @author aroller
 * 
 */
public interface CompleteRepository<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends FilterRepository<S, E, I>, Repository<E, I> {

}
