/**
 *
 */
package com.aawhere.search;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.google.common.base.Joiner;
import com.google.common.collect.Sets;

/**
 * Generates a set of substrings from a provided string. For example, "Hello World" becomes
 * "he hel hell hello el ell ello ll llo wo wor worl world or orl orld rl rld ld"
 * 
 * The minimum substring length can be provided, which can be used to limit the size of the
 * substring set. With a minimum length of 3, "Hello World" becomes
 * "hel hell hello ell ello llo wor worl world orl orld rld"
 * 
 * Used to provide auto-suggest and auto-complete functionality in Google Search API.
 * 
 * @See http://stackoverflow.com/questions/10960384/google-app-engine-python-search
 *      -api-string-search /10962212#10962212
 * 
 *      TN-848 added support for begging of words instead of embedded words
 * 
 * @author Brian Chapman
 * 
 */
public class TextSubstringGenerator {

	private static final String JOIN_DELIMINATOR = " ";
	private static final String WORD_SPLIT_DELIM = " ";
	private static final String WORD_SPLIT_DELIM2 = "/";
	private static final String REMOVE_PUNCUATION_REGEX = "[\\.,\\~\\`\\+\\[\\]\\-\\#\\!\\$%\\^&\\@\\<\\>\\\\\\'\\\"\\?\\*;:{}=\\|\\-_`~()]";
	private static final Integer DEFAULT_MIN_LENGTH = 1;

	/**
	 * Used to construct all instances of TextSubstringGenerator.
	 */
	public static class Builder
			extends ObjectBuilder<TextSubstringGenerator> {

		/**
		 * Indicates if only the beginning of the word should be provided or if subwords within the
		 * word should be included.
		 * 
		 */
		private boolean wordStartsOnly = false;

		public Builder() {
			super(new TextSubstringGenerator());
		}

		public Builder input(String input) {
			building.input = input;
			return this;
		}

		public Builder minLength(Integer length) {
			building.minLength = length;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.input);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public TextSubstringGenerator build() {
			building.substrings = buildSubStrings();
			TextSubstringGenerator built = super.build();
			return built;
		}

		private Set<String> buildSubStrings() {
			Set<String> substrings = Sets.newHashSet();

			Pattern p = Pattern.compile(REMOVE_PUNCUATION_REGEX);
			Matcher m = p.matcher(building.input);
			String withoutPunct = m.replaceAll("");
			String[] words = withoutPunct.split(WORD_SPLIT_DELIM);
			for (String word : words) {
				String[] words2 = word.split(WORD_SPLIT_DELIM2);
				for (String word2 : words2) {
					for (int i = 0; i < word2.length(); i++) {
						String substring = "";
						if (this.wordStartsOnly) {
							addConditionally(substrings, word2.substring(0, i));
						} else {
							for (int j = 0; j < (word2.length() - i); j++) {
								substring += word2.substring(i + j, i + j + 1);
								addConditionally(substrings, substring);
							}
						}
					}
				}

				// String prefix = "";
				// for (char letter : word.toCharArray()) {
				// prefix += letter;
				// if (prefix.length() >= building.minLength) {
				// substrings.add(prefix);
				// }
				// }
			}

			return substrings;
		}

		private void addConditionally(Set<String> substrings, String substring) {
			if (substring.length() >= building.minLength) {
				substrings.add(substring);
			}
		}

		/**
		 * Only provides substrings from the beginning of a word, not tokens within the word.
		 * 
		 * @return
		 */
		public Builder wordStartsOnly() {
			this.wordStartsOnly(true);
			return this;
		}

		public Builder wordStartsOnly(Boolean given) {
			this.wordStartsOnly = BooleanUtils.isTrue(given);
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct TextSubstringGenerator */
	private TextSubstringGenerator() {
	}

	public static Builder create() {
		return new Builder();
	}

	private String input;
	private Set<String> substrings = new HashSet<>();
	private Integer minLength = DEFAULT_MIN_LENGTH;

	/**
	 * @return the input
	 */
	public String input() {
		return this.input;
	}

	/**
	 * @return the substrings
	 */
	public Set<String> substrings() {
		return this.substrings;
	}

	public String asString() {
		return Joiner.on(JOIN_DELIMINATOR).skipNulls().join(substrings);
	}

}
