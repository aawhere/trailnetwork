/**
 * 
 */
package com.aawhere.persist;

/**
 * A class flag indicating the implementing entity builds its own key. This allows Repositories to
 * avoid creating an identifier for it and bypass integrity checks.
 * 
 * @author aroller
 * 
 */
public interface SelfIdentifyingEntity {

}
