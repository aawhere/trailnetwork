package com.aawhere.search;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.google.common.collect.Iterables;

@StatusCode(
		value = HttpStatusCode.BAD_REQUEST,
		message = "{INDEXES} requested to search, but we currently support searching a single index.  Let's talk about your needs.")
public class MultipleIndexesNotSupportedException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 3567265853329271199L;

	public MultipleIndexesNotSupportedException(Iterable<?> indexes) {
		super(MultipleIndexesNotSupportedExceptionMessage.message(indexes));
	}
}
