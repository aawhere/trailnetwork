/**
 *
 */
package com.aawhere.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Flags a method whose return value should be cached.
 * 
 * WARNING: Your methods that use this annotation must NOT BE PRIVATE.
 * 
 * https://code.google.com/p/google-guice/wiki/AOP
 * 
 * 
 * 
 * @See optional {@link Cache.Key}
 * 
 * @author Brian Chapman
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Cache {

	/**
	 * Optional annotation that flags a parameter to be used as the key for the cache. If not
	 * present and the method accepts one parameter, that parameter is used as the key.
	 * 
	 * @author Brian Chapman
	 * 
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.PARAMETER)
	public static @interface Key {

	}
}
