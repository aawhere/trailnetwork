/**
 *
 */
package com.aawhere.search;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.map.ListOrderedMap;
import org.apache.commons.lang3.StringUtils;

import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifier;
import com.aawhere.measure.GeoCoordinate;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseFilterBuilder;
import com.aawhere.search.SearchDocument.Field;
import com.aawhere.util.rb.Message;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

/**
 * Helper methods for Search.
 * 
 * @author Brian Chapman
 * 
 */
public class SearchUtils {

	/**
	 * designated option indicating if this is to be a datastore query.
	 * 
	 */
	public static final String DATASTORE = "datastore";

	private final static String INDEX_NAME_SEPARATOR = FieldUtils.KEY_DILIM;

	private static ListOrderedMap<Class<?>, FieldDataType> dataTypeMapping = new ListOrderedMap<>();
	static {
		// order matters.
		dataTypeMapping.put(String.class, FieldDataType.TEXT);
		dataTypeMapping.put(Number.class, FieldDataType.NUMBER);
		dataTypeMapping.put(Identifier.class, FieldDataType.ATOM);
		dataTypeMapping.put(GeoCoordinate.class, FieldDataType.GEO);

		// Messages that are constants are treated as ATOM by default.
		// Override by direct declaration if you wish for other behavior
		dataTypeMapping.put(Enum.class, FieldDataType.ATOM);
		dataTypeMapping.put(Message.class, FieldDataType.TEXT);

	}

	private SearchUtils() {

	}

	public static Set<FieldKey> searchableFieldKeys(FieldDictionary dictionary) {
		Set<FieldKey> keys = new HashSet<>();
		for (FieldDefinition<?> def : dictionary) {
			if (def.getSearchable() == true) {
				keys.add(def.getKey());
			}
		}
		return keys;
	}

	public static FieldDataType dataTypeFor(Class<?> type) {
		FieldDataType dataType = dataTypeMapping.get(type);
		if (dataType == null) {
			// check super types..break if found
			for (Class<?> superType : dataTypeMapping.keySet()) {
				if (superType.isAssignableFrom(type)) {
					dataType = dataTypeMapping.get(superType);
					break;
				}
			}
		}
		return dataType;
	}

	public static FieldDataType dataTypeFor(FieldDefinition<?> definition) {
		FieldDataType dataType = definition.getDataType();
		if (FieldDataType.isNone(dataType)) {
			Class<?> type = definition.getType();
			if (definition.hasParameterType()) {
				dataType = dataTypeFor(definition.getParameterType());
			} else {
				dataType = dataTypeFor(type);
			}
		}

		return dataType;
	}

	/**
	 * This finds the FieldDataType for registered types.
	 * 
	 * @param definition
	 * @param dictionaryFactory
	 * @return
	 * @throws FieldNotSearchableException
	 */
	public static FieldDataType mappedFieldDataType(FieldDefinition<?> definition,
			FieldDictionaryFactory dictionaryFactory) throws FieldNotSearchableException {

		FieldDataType mappedDataType = definition.getDataType();
		if (definition.getDataType().equals(FieldDataType.NONE)) {
			// Then we need to map it if possible.

			Class<?> type;
			if (definition.hasParameterType()) {
				type = definition.getParameterType();
			} else {
				type = definition.getType();
			}

			mappedDataType = SearchUtils.dataTypeFor(type);
			if (mappedDataType == null) {
				throw new IllegalStateException("Could not map FieldDataType " + definition.getDataType() + " on "
						+ definition.getKey() + " of type " + definition.getType()
						+ " to a Type. Add a mapping or declare the data type on the @Field annotation");
			}
		}
		return mappedDataType;
	}

	public static void throwNotSearchable(FieldKey fieldKey, FieldDictionaryFactory dictionaryFactory)
			throws FieldNotSearchableException {
		Set<FieldKey> fieldKeys = null;

		try {
			fieldKeys = searchableFieldKeys(dictionaryFactory.getDictionary(fieldKey.getDomainKey()));
		} catch (UnsupportedOperationException e) {
			fieldKeys = new HashSet<>();
		}
		throw new FieldNotSearchableException(fieldKey, fieldKeys);
	}

	public static Boolean isSearchable(Object object) {
		return org.springframework.core.annotation.AnnotationUtils.isAnnotationDeclaredLocally(	Searchable.class,
																								object.getClass());
	}

	public static Identifier<?, ?> getIdentifier(FieldDictionary dictionary, Object annotated) {
		FieldDefinition<?> definition;
		try {
			definition = dictionary.findDefinition(BaseEntity.BASE_FIELD.ID);
		} catch (FieldNotRegisteredException e) {
			throw new RuntimeException(e);
		}
		if (definition == null) {
			throw new IllegalStateException("Identifier expected to exist.");
		}
		Object value = AnnotatedDictionaryUtils.getValue(definition.getKey(), annotated);
		if (value instanceof Identifier<?, ?>) {
			return (Identifier<?, ?>) value;
		} else {
			throw new IllegalStateException("Expected the identifier field " + definition.getKey().toString()
					+ " to be and instance of Identifier.");
		}
	}

	/**
	 * Returns the first field in the {@link SearchDocument} to match the key or null if not found
	 * 
	 * @param document
	 * @param key
	 * @return
	 */
	@Nullable
	public static SearchDocument.Field findField(@Nonnull SearchDocument document, @Nonnull String key) {
		return Iterables.getFirst(fieldMap(document.fields()).get(key), null);
	}

	@Nullable
	public static Field findField(SearchDocument document, FieldKey fieldKey) {
		return findField(document, key(fieldKey));
	}

	public static Set<Field> findFields(SearchDocument document, FieldKey fieldKey) {
		return findFields(document, key(fieldKey));
	}

	/**
	 * Returns all fields in the SearchDocument matching the key.
	 * 
	 * @param document
	 * @param key
	 * @return
	 */
	public static Set<SearchDocument.Field> findFields(SearchDocument document, String key) {
		Builder<SearchDocument.Field> builder = ImmutableSet.builder();
		Collection<com.aawhere.search.SearchDocument.Field> collection = fieldMap(document.fields()).get(key);
		if (collection != null) {
			builder.addAll(collection);
		}
		return builder.build();
	}

	public static String key(FieldKey fieldKey) {
		return fieldKey.getAbsoluteKey();
	}

	public static Iterable<String> keys(Iterable<com.aawhere.search.SearchDocument.Field> fields) {
		return Iterables.transform(fields, keyFunction());
	}

	public static Function<com.aawhere.search.SearchDocument.Field, String> keyFunction() {
		return new Function<SearchDocument.Field, String>() {

			@Override
			public String apply(com.aawhere.search.SearchDocument.Field input) {
				return (input != null) ? input.key() : null;
			}
		};
	}

	public static Iterable<String> ids(Iterable<SearchDocument> searchDocuments) {
		return Iterables.transform(searchDocuments, idFunction());
	}

	public static Function<SearchDocument, String> idFunction() {
		return new Function<SearchDocument, String>() {

			@Override
			public String apply(SearchDocument input) {
				return (input != null) ? input.id() : null;
			}
		};
	}

	/**
	 * Allows for a domain to have sub indexes by splitting the suffix from the domain.
	 * 
	 * @see #indexName(String, String...)
	 * @param indexName
	 * @return
	 */
	public static String domainFromIndex(@Nonnull String indexName) {
		return StringUtils.split(indexName, INDEX_NAME_SEPARATOR)[0];
	}

	/**
	 * Allows for a domain to have sub indexes
	 * 
	 * @see #domainFromIndex(String)
	 * @param domain
	 * @param strings
	 * @return
	 */
	public static String indexName(@Nonnull String domain, String... strings) {
		ImmutableList<String> all = ImmutableList.<String> builder().add(domain).add(strings).build();
		return StringUtils.join(all, INDEX_NAME_SEPARATOR);
	}

	public static Function<SearchDocument, String> indexNameFunction() {
		return new Function<SearchDocument, String>() {

			@Override
			public String apply(SearchDocument input) {
				return (input != null) ? input.index() : null;
			}
		};
	}

	/**
	 * Provides a map with the {@link SearchDocument.Field#key()} as the {@link Map.Entry#getKey()}
	 * and the {@link SearchDocument.Field} as the {@link Map.Entry#getValue()}.
	 * 
	 * @param fields
	 * @return
	 */
	public static Multimap<String, SearchDocument.Field> fieldMap(Iterable<SearchDocument.Field> fields) {
		return Multimaps.index(fields, keyFunction());
	}

	/**
	 * Helper method to encourage the use of {@link FieldDefinition} to construct the value into the
	 * proper type.
	 * 
	 * @param definition
	 * @param fieldMap
	 * @return
	 */
	public static <T> T fieldValue(FieldDefinition<T> definition, Multimap<String, SearchDocument.Field> fieldMap) {
		return fieldValue(definition.getKey().getAbsoluteKey(), definition.getType(), fieldMap);
	}

	/**
	 * A little helper to retrieve the fields from a map efficiently and cast to the expected value.
	 * It returns the first value if multiple exist.
	 * 
	 * @param fieldKey
	 * @param valueType
	 * @param fieldMap
	 * @return the value or null if the value is not found.
	 */
	public static <T> T
			fieldValue(String fieldKey, Class<T> valueType, Multimap<String, SearchDocument.Field> fieldMap) {
		T result;
		Collection<SearchDocument.Field> fields = fieldMap.get(fieldKey);

		if (CollectionUtils.isNotEmpty(fields)) {
			SearchDocument.Field field = fields.iterator().next();
			Object value = field.value();
			result = (T) value;

		} else {
			result = null;
		}
		return result;
	}

	/**
	 * Indicates if this particular search intends to use the datastore specifically. If not then
	 * the service may choose to use another repository such as the search service.
	 * 
	 * @see #isDatastoreFilter(com.aawhere.persist.Filter.BaseBuilder)
	 * @param filter
	 * @return
	 */
	public static boolean isDatastoreFilter(Filter filter) {
		// near and bounds are provided externally and are best serviced by the search service
		// query can only be handled by the search service
		return filter.containsOption(DATASTORE)
		// if no conditions or other common query info then use the datastore
				|| !(filter.hasConditions() || filter.hasSearchIndexes() || filter.hasQuery() || filter.hasNear() || filter
						.hasBounds());
	}

	/**
	 * Makes the filter a datastore filter which will bypass using the search service no matter what
	 * is in the filter.
	 * 
	 * @see #isDatastoreFilter(Filter)
	 * @param filterBuilder
	 * @return
	 */
	public static BaseFilterBuilder<?> isDatastoreFilter(BaseFilterBuilder<?> filterBuilder) {
		return filterBuilder.addOption(DATASTORE);
	}
}
