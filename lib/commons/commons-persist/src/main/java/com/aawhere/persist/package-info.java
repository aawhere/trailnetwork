@XmlJavaTypeAdapters({

@XmlJavaTypeAdapter(value = com.aawhere.collections.SetXmlAdapter.class, type = java.util.Set.class) })
@XmlSchema(xmlns = @XmlNs(prefix = com.aawhere.xml.XmlNamespace.API_PREFIX,
		namespaceURI = com.aawhere.xml.XmlNamespace.API_VALUE))
package com.aawhere.persist;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

