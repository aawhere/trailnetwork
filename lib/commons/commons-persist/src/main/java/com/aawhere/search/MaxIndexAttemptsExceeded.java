/**
 *
 */
package com.aawhere.search;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * @author brian
 * 
 */
@StatusCode(value = HttpStatusCode.UNAVAILABLE)
public class MaxIndexAttemptsExceeded
		extends BaseException {

	private static final long serialVersionUID = -1947133967914967671L;

	public MaxIndexAttemptsExceeded(Integer retryCount) {
		super(new CompleteMessage.Builder().addParameter(SearchMessage.Param.RETRY_COUNT, retryCount)
				.setMessage(SearchMessage.MAX_RETRY_EXCEEDED).build());
	}
}
