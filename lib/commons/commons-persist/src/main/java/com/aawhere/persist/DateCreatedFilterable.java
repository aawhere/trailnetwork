/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.persist.objectify.IfDateCreatedFilterable;

/**
 * A flag indicating the implementing entity requires searching by create date. This allows multiple
 * entities to have the create date stored without indexing.
 * 
 * @see IfDateCreatedFilterable
 * 
 * @author aroller
 * 
 */
public interface DateCreatedFilterable {

}
