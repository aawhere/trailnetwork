/**
 * 
 */
package com.aawhere.persist;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.field.FieldKey;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.FromStringAdapter;
import com.aawhere.xml.XmlNamespace;

/**
 * Provides the ability to sort {@link Filter} results given a field string that must resolve to a
 * {@link FieldKey} and a {@link Direction} that indicates the order or sorting.
 * 
 * Adaptation to a String conforms to the {@link FromStringAdapter} rules.
 * 
 * @author aroller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class FilterSort
		implements Serializable {

	private static final long serialVersionUID = 3642874110444635923L;
	/** An indicator that if passed in will remove a default sort. */
	public static final String NONE_FIELD = "none";
	static final Direction DEFAULT_DIRECTION = Direction.ASCENDING;
	private static final int FILTER_INDEX = 0;
	private static final int DIRECTION_INDEX = 1;
	public static final FilterSort NONE = FilterSort.create().setField(FilterSort.NONE_FIELD).build();

	@XmlEnum
	@XmlType(namespace = XmlNamespace.API_VALUE)
	public static enum Direction {
		ASCENDING('+'), DESCENDING('-');
		/** Shorthand to describe direction. prepended before a field key. */
		public final Character prefix;

		private Direction(char alternatePrefix) {
			this.prefix = alternatePrefix;
		}

		/**
		 * Given the lower or upper case string matching the direction.name() this will return the
		 * value or throw an exception explaning the problem.
		 * 
		 * @param string
		 * @return
		 * @throws InvalidChoiceException
		 */
		public static Direction fromString(String string) throws InvalidChoiceException {
			Direction value = valueOf(string.toUpperCase());
			if (value == null) {
				throw new InvalidChoiceException(string, (Object[]) values());
			}
			return value;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return super.toString().toLowerCase();

		}
	}

	/**
	 * Used to construct all instances of FilterSort.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<FilterSort> {

		public Builder() {
			super(new FilterSort());
		}

		public Builder setDirection(Direction direction) {
			building.direction = direction;
			return this;
		}

		public Builder setField(String field) {
			building.field = field;
			return this;
		}

		public Builder setFieldKey(FieldKey fieldKey) {
			building.field = fieldKey.getAbsoluteKey();
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("field", building.field);
			Assertion.exceptions().notNull("direction", building.direction);

		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** helper to add the prefix of the direction to the field. */
	@Nonnull
	public static String prefixString(@Nonnull String field, @Nullable Direction direction) {
		if (direction == null) {
			direction = DEFAULT_DIRECTION;
		}
		return direction.prefix + field;
	}

	/**
	 * Given a one or two part string separated by whitespace this will create the sort by assigning
	 * the first part to the {@link #getField()} and the second optional part to the
	 * {@link #getDirection()}. If the direction is not given then the {@link #DEFAULT_DIRECTION} is
	 * chosen.
	 * 
	 * This also supports + or - prefix to indicate direction.
	 * 
	 * @param syntax
	 * @return
	 */
	public static FilterSort valueOf(String syntax) {
		String field;
		Direction direction;
		Pair<Direction, String> parsePrefix = parsePrefix(syntax);
		if (parsePrefix != null) {
			field = parsePrefix.getRight();
			direction = parsePrefix.getKey();
		} else {
			String[] parts = StringUtils.split(syntax);
			field = parts[FILTER_INDEX];
			if (parts.length == 2) {
				direction = Direction.fromString(parts[DIRECTION_INDEX]);
			} else {
				direction = DEFAULT_DIRECTION;
			}
		}
		if (field.equalsIgnoreCase(NONE_FIELD)) {
			return NONE;
		} else {
			return FilterSort.create().setField(field).setDirection(direction).build();
		}
	}

	/**
	 * @param syntax
	 * @return
	 */
	private static Pair<Direction, String> parsePrefix(String syntax) {
		Pair<Direction, String> desc = parsePrefix(syntax, Direction.DESCENDING);
		return (desc == null) ? parsePrefix(syntax, Direction.ASCENDING) : desc;
	}

	/**
	 * @param descending
	 * @return
	 */
	private static Pair<Direction, String> parsePrefix(String syntax, Direction possibleDirection) {
		Pair<Direction, String> result = null;
		if (StringUtils.startsWith(syntax, possibleDirection.prefix.toString())) {
			String field = StringUtils.stripStart(syntax, possibleDirection.prefix.toString());
			result = Pair.of(possibleDirection, field);
		}
		return result;
	}

	/**
	 * Provides a description of the Filter Sort.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return prefixString(field, this.direction);
	}

	@XmlElement
	private Direction direction = DEFAULT_DIRECTION;
	@XmlElement
	private String field;

	/** Use {@link Builder} to construct FilterSort */
	private FilterSort() {
	}

	/**
	 * @return the direction
	 */
	public Direction getDirection() {
		return this.direction;
	}

	/**
	 * @return the field
	 */
	public String getField() {
		return this.field;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.direction == null) ? 0 : this.direction.hashCode());
		result = prime * result + ((this.field == null) ? 0 : this.field.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilterSort other = (FilterSort) obj;
		if (this.direction != other.direction)
			return false;
		if (this.field == null) {
			if (other.field != null)
				return false;
		} else if (!this.field.equals(other.field))
			return false;
		return true;
	}
}
