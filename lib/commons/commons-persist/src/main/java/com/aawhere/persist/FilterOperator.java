/**
 *
 */
package com.aawhere.persist;

import static com.aawhere.field.type.FieldDataType.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * For use in constructing {@link FilterCondition}s. The {@link #supportedDataTypes} lists the
 * {@link FieldDataType}s that are supported by the operation.
 * 
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
public enum FilterOperator {
	/* */
	EQUALS(FilterOperator.EQUALS_VALUE, TEXT, NUMBER, DATE, ATOM),
	/* */
	NOT_EQUALS("!=", TEXT, NUMBER, DATE, ATOM),
	/* */
	GREATER_THAN(">", DATE, NUMBER, TEXT),
	/* */
	GREATER_THAN_OR_EQUALS(">=", DATE, NUMBER, TEXT),
	/* */
	LESS_THAN("<", DATE, NUMBER, TEXT),
	/* */
	LESS_THAN_OR_EQUALS("<=", DATE, NUMBER, TEXT),
	/* */
	IN("in", GEO, TEXT, ATOM, NUMBER, DATE),
	/** Specifically for predicates as of now, this returns the opposite of in. */
	NOT_IN("!in");

	public static final String EQUALS_VALUE = "=";

	public String operator;
	public Collection<FieldDataType> supportedDataTypes;

	FilterOperator(String operator, FieldDataType... supportedDataTypes) {
		this.operator = operator;
		this.supportedDataTypes = Lists.newArrayList(supportedDataTypes);
	}

	public static FilterOperator fromString(String string) {
		List<FilterOperator> all = Arrays.asList(values());
		Collection<FilterOperator> matches = Collections2.filter(all, new EqualToPredicate(string));
		FilterOperator result;
		if (matches.isEmpty()) {
			// fine, maybe they provided name
			try {
				result = valueOf(string);
			} catch (Exception e) {
				throw new InvalidChoiceException(string, (Object[]) values());
			}
		} else {
			result = Iterables.getFirst(matches, null);
		}
		// not handling duplicates...that's the constructors job
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {

		return operator;
	}

	public Boolean supportsFieldDataType(FieldDataType dataType) {
		return supportedDataTypes.contains(dataType);
	}

	@XmlTransient
	public static class EqualToPredicate
			implements Predicate<FilterOperator> {
		private String targetOperator;

		public EqualToPredicate(String targetOperator) {
			this.targetOperator = targetOperator;
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.common.base.Predicate#apply(java.lang.Object)
		 */
		@Override
		public boolean apply(@Nullable FilterOperator input) {

			return input != null && this.targetOperator.equals(input.operator);
		}

	}

	/**
	 * Given the field name this will create the filter string syntax used by GAE Python and
	 * Objectify when filtering directly outside of our filter framework.
	 * 
	 * Something like "myField >" or "myField in"
	 * 
	 * 
	 * @param applicationDataId
	 * @return
	 */
	public String filterString(String fieldName) {
		return new StringBuilder(fieldName).append(" ").append(this.operator).toString();
	}
}
