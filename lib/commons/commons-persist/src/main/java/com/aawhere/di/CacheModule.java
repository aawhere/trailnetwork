/**
 *
 */
package com.aawhere.di;

import com.aawhere.cache.Cache;
import com.aawhere.cache.CacheInterceptor;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

/**
 * Guice Module for com.aawhere.cache
 * 
 * @author Brian Chapman
 * 
 */
public class CacheModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		CacheInterceptor cacheInterceptor = new CacheInterceptor();
		requestInjection(cacheInterceptor);
		bindInterceptor(Matchers.any(), Matchers.annotatedWith(Cache.class), cacheInterceptor);
	}

}
