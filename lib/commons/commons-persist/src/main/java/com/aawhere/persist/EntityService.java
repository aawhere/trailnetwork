/**
 * 
 */
package com.aawhere.persist;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.log.LoggerFactory;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * Base class for stateless service classes that interact with {@link Repository} and provide
 * {@link BaseEntity} and {@link BaseEntities}.
 * 
 * TODO: Use {@link CompleteRepository}.
 * 
 * @author aroller
 * 
 */
abstract public class EntityService<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends ServiceStandardBase<S, E, I> {

	private CompleteRepository<S, E, I> repository;

	public EntityService(CompleteRepository<S, E, I> repository) {
		super(new EntityIdToEntityFunction.Builder<E, I>().repository(repository).build());
		this.repository = repository;
	}

	/**
	 * @return the repository
	 */
	protected CompleteRepository<S, E, I> getRepository() {
		return this.repository;
	}

	/**
	 * Transforms the given ids into a map of the ids referencing their entities.
	 * 
	 * @see Repository#loadAll(Iterable)
	 * @return
	 */
	public Function<Iterable<I>, Map<I, E>> idsFunction() {
		return new Function<Iterable<I>, Map<I, E>>() {
			@Override
			public Map<I, E> apply(Iterable<I> input) {
				return all(input);
			}
		};
	}

	/**
	 * Provides a connected iterable of ids matching the given filter.
	 * 
	 * @param filter
	 * @return
	 */
	public Iterable<I> ids(Filter filter) {
		return this.repository.idPaginator(filter);
	}

	/**
	 * Provides a Map that will contain all the Entities related to the given ids.
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Map<I, E> all(Iterable<I> id) {
		// delays any calls until #iterable invokes. It will call the repository map for us, but
		// also check for missing values
		return Maps.uniqueIndex(iterable(id), IdentifierUtils.<E, I> idFunction());
	}

	/**
	 * This is an efficient call to the datastore and memcached using batch get. If something is not
	 * returned then a subsequent individual call is made to retrieve the missing entity (assuming
	 * it is an alias that is handled when calling the id directly). If the result is still missing
	 * then the requested id is ignored. It is up to the client to diff the ids to see if anything
	 * is missing since it will not be reported (outside of the log warning). Consider using
	 * {@link #all(Iterable)} for map capabilities.
	 * 
	 * @param ids
	 * @return
	 */
	public Iterable<E> iterable(final Iterable<I> ids) {
		return new Iterable<E>() {
			// this ensures uniqueness and also prepares for missing
			HashSet<I> requested = Sets.newHashSet(ids);
			// asynchronously loads upon calling, but does not block until iteration begins
			private Map<I, E> entityMap = repository.loadAll(requested);

			@Override
			public Iterator<E> iterator() {
				return new Iterator<E>() {

					// this starts the block
					// all found are exhausted first
					Iterator<E> found = entityMap.values().iterator();

					Set<I> missingIds = Sets.difference(requested, entityMap.keySet());
					// pre-fetch asynchronously each individual which will call for an alias
					Iterator<E> missing = Iterators.filter(	Iterators.transform(missingIds.iterator(), idFunction()),
															Predicates.<E> notNull());

					@Override
					public void remove() {
						throw new UnsupportedOperationException();
					}

					@Override
					public E next() {
						if (found.hasNext()) {
							return found.next();
						} else {
							E next = missing.next();
							LoggerFactory.getLogger(getClass()).warning(next
									+ " was missing in bulk load...it must be one alias of " + missingIds);
							// assuming cleanup is being handled by idFunction
							return next;
						}
					}

					@Override
					public boolean hasNext() {
						if (found.hasNext()) {
							return this.found.hasNext();
						} else {
							return this.missing.hasNext();
						}
					}
				};
			}
		};
	}

	/**
	 * @see Repository#exists(Identifier)
	 * @param id
	 * @return
	 */
	public Boolean exists(I id) {
		return repository.exists(id);
	}

	/**
	 * Like {@link #exists(Identifier)}, but throws an exception.
	 * 
	 * @param id
	 * @throws EntityNotFoundException
	 */
	public void existsEnforced(I id) throws EntityNotFoundException {
		RepositoryUtil.exists(repository, id);
	}

	/**
	 * Given ids this ensures all given exist or throws EntityNotFoundException.
	 * 
	 * @param ids
	 *            those that must be found
	 * @return the ids reduced to a set...necessary to ensure unqiueness so provided back for your
	 *         potential benefit
	 * @throws EntityNotFoundException
	 *             when one or more ids does not exist. All are identified in the message.
	 */
	public Set<I> existsEnforced(Iterable<I> ids) throws EntityNotFoundException {
		Set<I> uniquePersonIds = SetUtilsExtended.asSet(ids);
		Iterable<I> nonExisting = nonExisting(uniquePersonIds);
		EntityNotFoundException.throwIfNotFound(nonExisting);
		return uniquePersonIds;
	}

	/**
	 * Provides only those that do not exist.
	 * 
	 * 
	 * @see #exists(I)
	 * @see RepositoryUtil#nonExistingEntityIds(Iterable, com.aawhere.persist.FilterRepository)
	 * @param ids
	 * @return
	 */
	public Iterable<I> nonExisting(Set<I> ids) {
		return RepositoryUtil.nonExistingEntityIds(ids, getRepository());
	}

	/**
	 * Deletes all entities that match the given filter. To support the removal of associated
	 * entities this iterates each match and calls {@link #deleted(Identifier)} individually. A
	 * Filter option must be provided matching the current GTC date. This is a secondary check to
	 * avoid accidental deletion rather than real security. The passkey is the current
	 * {@link LocalDate#now(DateTimeZone)} in {@link DateTimeZone#UTC} in
	 * {@link LocalDate#toString()} format (YYYY-MM-dd). You must look here to find the passkey
	 * since it is purposely not published in the exception thrown.
	 * 
	 * @param filter
	 * @return
	 */
	protected Integer deleted(Filter filter) throws DeletionConfirmationException {

		if (filter.containsOption(EntityServiceUtil.deletionConfirmationPasskey())) {
			Iterable<I> ids = ids(filter);
			int count = 0;
			for (I id : ids) {
				deleted(id);
				count++;
			}
			return count;
		} else {
			int numberToDelete = Iterables.size(ids(filter));
			throw new DeletionConfirmationException(numberToDelete, getRepository().getEntityClassRepresented());
		}
	}

	/**
	 * Provides the ability to delete an entity. This should be used with caution. and often times
	 * over-ridden to support deletion of related entities
	 */
	protected void deleted(I id) {
		repository.delete(id);
	}
}
