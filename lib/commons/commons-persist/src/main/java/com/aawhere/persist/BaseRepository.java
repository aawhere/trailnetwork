/**
 *
 */
package com.aawhere.persist;

import java.util.Set;

import com.aawhere.id.Identifier;

/**
 * Common implementations for specific repositories to take advantage of.
 * 
 * TODO: Make ObjectifyRepository inherit from this.
 * 
 * @author Aaron Roller
 * 
 */
public abstract class BaseRepository<EntityT extends BaseEntity<EntityT, IdentifierT>, IdentifierT extends Identifier<?, EntityT>>
		implements Repository<EntityT, IdentifierT> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#delete(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void delete(EntityT entity) {
		delete(entity.getId());

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#deleteAll(java.lang.Iterable)
	 */
	@Override
	public void deleteAll(Iterable<EntityT> entities) {
		for (EntityT e : entities) {
			delete(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#deleteAllFromIds(java.lang.Iterable)
	 */
	@Override
	public void deleteAllFromIds(Iterable<IdentifierT> entityIds) {
		for (IdentifierT id : entityIds) {
			delete(id);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Repository#idsExisting(java.lang.Iterable)
	 */
	@Override
	public Set<IdentifierT> idsExisting(Iterable<IdentifierT> ids) {
		return loadAll(ids).keySet();
	}

}
