/**
 *
 */
package com.aawhere.persist;

import java.util.Map;

import com.aawhere.collections.Index;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.reflect.GenericUtils;

import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * Default implementation for all methods that can be generalized when the implementor provides
 * {@link #idFunction()}.
 * 
 * @author aroller
 * 
 */
public class ServiceGeneralBase<S extends BaseFilterEntities<E>, E extends BaseEntity<E, ?>, I extends Identifier<?, ?>>
		implements ServiceGeneral<S, E, I> {

	final private Function<I, E> idFunction;

	/**
	 *
	 */
	public ServiceGeneralBase(Function<I, E> idFunction) {
		this.idFunction = idFunction;
		Assertion.exceptions().notNull("idFunction", this.idFunction);
	}

	/** Requires you to override the {@link #idFunction()}. */
	public ServiceGeneralBase() {
		this.idFunction = null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Service#all(java.lang.Iterable)
	 */
	@Override
	public Map<I, E> all(Iterable<I> ids) {
		return Maps.asMap(Sets.newHashSet(ids), idFunction());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Service#idFunction()
	 */
	@Override
	public Function<I, E> idFunction() {
		return this.idFunction;
	}

	@Override
	public E entity(I id) throws EntityNotFoundException {
		return RepositoryUtil.finderResult(id, idFunction().apply(id));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Service#provides()
	 */
	@Override
	public Class<E> provides() {
		return GenericUtils.getTypeArgument(this, Index.SECOND);
	}
}
