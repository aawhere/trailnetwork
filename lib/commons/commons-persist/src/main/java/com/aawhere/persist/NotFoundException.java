package com.aawhere.persist;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

/**
 * Like {@link EntityNotFoundException}, but used when you don't know the id. For example a query.
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(HttpStatusCode.NOT_FOUND)
public class NotFoundException
		extends BaseException {

	private static final long serialVersionUID = 3708095962316518804L;

	public NotFoundException() {
		super();
	}

}
