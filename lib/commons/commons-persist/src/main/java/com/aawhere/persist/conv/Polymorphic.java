/**
 * 
 */
package com.aawhere.persist.conv;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Placing this annotation on a field will tell objectify to use the PolymorphicTranslator.
 * 
 * @author brian
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.TYPE })
public @interface Polymorphic {

}
