/**
 *
 */
package com.aawhere.persist;

import java.util.ArrayList;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.collections.Index;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.lang.NotImplementedException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringAdapterFactory;
import com.aawhere.lang.string.StringFormatException;
import com.aawhere.util.rb.CompleteMessage;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Transforms a formatted conditional String into a {@link FilterCondition} with the proper type
 * resolved by the type indicated by the {@link FilterCondition#getField()}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class FilterConditionStringAdapter<T>
		implements StringAdapter<FilterCondition<T>> {

	/**
	 * Used to indicate a null value is desired, regardless of the type.
	 */
	static final String NULL_STRING = "null";

	private static final String DILIMETER = " ";

	/** Provides adaptation for the operand value. */
	private StringAdapterFactory adapterFactory;
	/** provides relative access to releative field keys */
	private FieldDictionary dictionary;
	/** provides access to field definitions using absolute keys. */
	private FieldDictionaryFactory dictionaryFactory;

	private FilterConditionSupportedTypeRegistry supportedTypeRegistry;

	/** Use {@link Builder} to construct FilterConditionStringAdapter */
	@Inject
	public FilterConditionStringAdapter(StringAdapterFactory adapterFactory, FieldDictionaryFactory dictionaryFactory,
			FilterConditionSupportedTypeRegistry registry) {
		this.dictionaryFactory = dictionaryFactory;
		this.adapterFactory = adapterFactory;
		this.supportedTypeRegistry = registry;
		this.validate();
	}

	private void validate() {
		Assertion.assertNotNull("dictionaryFactory", this.dictionaryFactory);
		Assertion.assertNotNull("adapterFactory", this.adapterFactory);
		Assertion.assertNotNull("registry", this.supportedTypeRegistry);
	}

	private FilterConditionStringAdapter() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Object filterCondition) {
		if (!(filterCondition instanceof FilterCondition)) {
			throw new IllegalArgumentException(filterCondition + " must be a " + FilterCondition.class);
		}

		@SuppressWarnings("unchecked")
		FilterCondition<T> condition = (FilterCondition<T>) filterCondition;
		final String field = condition.getField();
		final String operator = condition.getOperator().operator;
		String valueString;
		try {
			valueString = marshalValue(condition);
		} catch (FieldNotRegisteredException e) {
			throw new ToRuntimeException(e);
		}
		return format(field, operator, valueString);
	}

	/**
	 * Adds the {@link #DILIMETER} in the right place to provide a condition in the syntax ready to
	 * be returned.
	 * 
	 * @param field
	 * @param operator
	 * @param valueString
	 * @return
	 */
	public static String format(final String field, final String operator, String valueString) {
		StringBuilder builder = new StringBuilder();
		builder.append(field);
		builder.append(DILIMETER);
		builder.append(operator);
		builder.append(DILIMETER);
		builder.append(If.nil(valueString).use(NULL_STRING));
		return builder.toString();
	}

	/**
	 * @param condition
	 * @return
	 * @throws FieldNotRegisteredException
	 * @throws BaseException
	 */
	public String marshalValue(FilterCondition<T> condition) throws FieldNotRegisteredException {
		StringAdapter<T> adapter = adapterFromField(condition.getField());
		String valueString = adapter.marshal(condition.getValue());
		return valueString;
	}

	/**
	 * Assistant to standard map entries used during queries this will return the standard string
	 * condition ready to be used in {@link #unmarshal(String, Class)}.
	 * 
	 * TODO: Support more than just equals
	 * 
	 * FIXME: unmarshal should be better abstracted so we don't have to create the string, but could
	 * pass an entry directly to the unmarshaler.
	 * 
	 * @param entry
	 * @return
	 */
	public static String format(String key, String value) {
		return format(key, FilterOperator.EQUALS_VALUE, value);
	}

	/**
	 * When provided a string with a standard syntax, this will parse the string and create the
	 * condition.
	 * 
	 * This method throws well handled runtime exceptions intended to be displayed to:
	 * 
	 * You, the developer looking at this code.
	 * 
	 * The API user sending formatted strings that aren't valid.
	 * 
	 * In either case it is typically a coding error so we don't throw checked exceptions here.
	 * 
	 * 
	 * @param fromString
	 * @param type
	 *            not used. Required by parent signature, but the type of FilterCondition
	 * @return
	 * @throws BaseException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <F extends FilterCondition<T>> F unmarshal(@Nonnull String fromString, @Nullable Class<F> type)
			throws BaseException {
		String[] parts = partsFrom(fromString);

		// Field
		final String fieldString = parts[Index.FIRST.index];
		FieldDefinition<?> fieldDefinition = retrieveAndValidateField(fieldString);

		// Operator
		final String operatorValue = parts[Index.SECOND.index];
		FilterOperator operator = retrieveAndValidateFilterOperator(operatorValue, fieldDefinition);

		// Value
		T value;
		try {
			value = retrieveAndValidateValue(	StringUtils.substringAfter(fromString, operator.operator),
												operator,
												fieldDefinition);
		} catch (StringFormatException badFormat) {
			// maybe the operator name was used.
			value = retrieveAndValidateValue(	StringUtils.substringAfter(fromString, operator.name()),
												operator,
												fieldDefinition);
		}

		// CreateCondition
		return (F) new FilterCondition.Builder<T>().field(fieldDefinition.getKey()).operator(operator).value(value)
				.build();

	}

	private String[] partsFrom(String fromString) throws StringFormatException {
		String[] parts = StringUtils.split(fromString, DILIMETER);
		if (parts.length < 3) {
			throw new StringFormatException(new CompleteMessage.Builder()
					.setMessage(PersistMessage.FILTER_CONDITION_MISSING_VALUE).build());
		}
		return parts;
	}

	private FieldDefinition<?> retrieveAndValidateField(String fieldString) throws FieldNotRegisteredException {
		FieldDefinition<?> fieldDefinition = field(fieldString);
		return fieldDefinition;
	}

	private FilterOperator retrieveAndValidateFilterOperator(String operatorValue, FieldDefinition<?> fieldDefinition)
			throws UnsupportedFilterOperatorException {
		FilterOperator operator = FilterOperator.fromString(operatorValue);
		// Removed as part of WW-237. DataTypes should be optional. their default values should be
		// infered by the field type
		// and optionally overridden if a special need arises.
		// TODO: move check to search subsystem.
		// if (!operator.supportsFieldDataType(fieldDefinition.getDataType())) {
		// throw new UnsupportedFilterOperatorException(operator, fieldDefinition.getDataType());
		// }
		return operator;
	}

	@SuppressWarnings("unchecked")
	private T retrieveAndValidateValue(String valueString, FilterOperator operator, FieldDefinition<?> fieldDefinition)
			throws BaseException {
		valueString = StringUtils.trim(valueString);
		if (StringUtils.isEmpty(valueString)) {
			throw new StringFormatException(new CompleteMessage.Builder()
					.setMessage(PersistMessage.FILTER_CONDITION_MISSING_VALUE).build());
		}

		T value = null;
		// a string value of "null" should provide a value of null
		if (!StringUtils.equalsIgnoreCase(NULL_STRING, valueString)) {
			Set<Class<?>> supportedTypes = supportedTypeRegistry.supportedTypes(fieldDefinition.getType(), operator);
			if (supportedTypes.isEmpty()) {
				// IN operator requires a collection. If parameter type isn't provided try to use a
				// generic
				// list
				if (operator.equals(FilterOperator.IN) && !fieldDefinition.hasParameterType()) {
					value = valueForType((Class<T>) ArrayList.class, fieldDefinition.getType(), valueString);
				} else {
					value = valueFromField((FieldDefinition<T>) fieldDefinition, valueString);
				}
			} else {
				if (supportedTypes.size() == 1) {
					// Get value for type.
					Class<T> valueType = (Class<T>) supportedTypes.iterator().next();
					value = valueForType(valueType, null, valueString);
				} else {
					throw new NotImplementedException(
							"Handling of multiple value types is not supported at this time. If you want it suppoted, implement it!");
				}
			}
		}
		return value;
	}

	/**
	 * Given the field, this will find the definition, get the type from it, find a suitable adapter
	 * and unmarshal the value string into the typed object.
	 * 
	 * @param fieldString
	 * @param valueString
	 * @return
	 * @throws FieldNotRegisteredException
	 * @throws StringFormatException
	 * @throws BaseException
	 */
	private T valueFromField(FieldDefinition<T> field, String valueString) throws BaseException {
		return valueForType(field.getType(), field.getParameterType(), valueString);
	}

	private T valueForType(Class<T> type, Class<?> paramaterType, String valueString) throws BaseException {
		StringAdapter<T> adapter = adapterFactory.getAdapter(type, paramaterType);
		return adapter.unmarshal(valueString, type);
	}

	/**
	 * provides the adapter when given the field string
	 * 
	 * @param fieldString
	 * @return
	 * @throws FieldNotRegisteredException
	 */
	@SuppressWarnings("unchecked")
	private StringAdapter<T> adapterFromField(String fieldString) throws FieldNotRegisteredException {
		FieldDefinition<T> fieldDef = field(fieldString);
		return (StringAdapter<T>) adapterFactory.getAdapter(fieldDef.getType(), fieldDef.getParameterType());
	}

	/**
	 * Retrieves the field type when given only the field string that must resolve to a relative or
	 * absolute field key.
	 * 
	 * @param fieldString
	 * @return
	 * @throws FieldNotRegisteredException
	 */
	private FieldDefinition<T> field(String fieldString) throws FieldNotRegisteredException {
		FieldDefinition<T> fieldDefinition;
		// first look for a relative key
		if (dictionary != null && dictionary.containsKey(fieldString)) {
			fieldDefinition = dictionary.findDefinition(fieldString);

		} else {
			// look for an absolute key
			fieldDefinition = dictionaryFactory.findDefinition(fieldString);
		}
		return fieldDefinition;

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.string.StringAdapter#getType()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Class<FilterCondition<T>> getType() {
		return (Class<FilterCondition<T>>) (Class<?>) FilterCondition.class;
	}

	/**
	 * Provides access to the adapter factory that this adapter is using to transform the value into
	 * Strings.
	 * 
	 * @return the adapterFactory
	 */
	public StringAdapterFactory getAdapterFactory() {
		return this.adapterFactory;
	}

	/**
	 * @param condition
	 * @return
	 */
	public boolean handles(FilterCondition<?> condition) {
		Boolean handles;
		try {
			FieldDefinition<T> field = field(condition.getField());
			handles = this.adapterFactory.handles(field.getType(), field.getParameterType());
		} catch (FieldNotRegisteredException e) {
			handles = false;
		}
		return handles;
	}

	/**
	 * Used to construct all instances of FilterConditionStringAdapter.
	 */
	public static class Builder<T>
			extends ObjectBuilder<FilterConditionStringAdapter<T>> {

		public Builder() {
			super(new FilterConditionStringAdapter<T>());
		}

		public Builder<T> setAdapterFactory(StringAdapterFactory adapterFactory) {
			building.adapterFactory = adapterFactory;
			return this;
		}

		public Builder<T> setDictionary(FieldDictionary dictionary) {
			building.dictionary = dictionary;
			return this;
		}

		public Builder<T> setDictionaryFactory(FieldDictionaryFactory dictionaryFactory) {
			building.dictionaryFactory = dictionaryFactory;
			return this;
		}

		/**
		 * @param registry
		 * @return
		 */
		public Builder<T> setRegistry(FilterConditionSupportedTypeRegistry registry) {
			building.supportedTypeRegistry = registry;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			building.validate();
			super.validate();
		}

	}// end Builder

}
