/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;

/**
 * Provides common interface standard for all services.
 * 
 * @author aroller
 * 
 */
public interface ServiceStandard<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends ServiceGeneral<S, E, I> {

}
