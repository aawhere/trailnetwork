/**
 *
 */
package com.aawhere.persist;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.util.rb.StatusMessages;
import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.Ordering;

/**
 * Extention of {@link BaseEntities} to provide a {@link Filter} as part of the collection.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class BaseFilterEntities<EntityT extends BaseEntity<EntityT, ?>>
		extends BaseEntities<EntityT>
		implements FilterProvider {

	@XmlElement
	private Filter filter;

	@XmlElement
	private StatusMessages statusMessages;

	/** Use {@link Builder} to construct BaseFilterEntities */
	protected BaseFilterEntities() {
	}

	/**
	 * This limiting criteria that these activities were found within this system.
	 * 
	 * @return the filter
	 */
	public Filter getFilter() {
		return this.filter;
	}

	/**
	 * Provides information about the query being performed including errors, warnings and
	 * informational messages.
	 * 
	 * @return the messages
	 */
	public StatusMessages getStatusMessages() {
		return this.statusMessages;
	}

	/**
	 * Used to construct all instances of BaseFilterEntities.
	 */
	@XmlTransient
	public static class Builder<BuilderT extends BaseFilterEntities.Builder<BuilderT, EntityT, EntitiesT>, EntityT extends BaseEntity<EntityT, ?>, EntitiesT extends BaseFilterEntities<EntityT>>
			extends BaseEntities.Builder<BuilderT, EntityT, EntitiesT> {

		public Builder(EntitiesT entities) {
			super(entities, new ArrayList<EntityT>());
		}

		public Builder(EntitiesT entities, List<EntityT> collection) {
			super(entities, collection);
		}

		@SuppressWarnings("unchecked")
		public BuilderT setFilter(Filter filter) {
			((BaseFilterEntities<EntityT>) building).filter = filter;
			this.ordering((Ordering<EntityT>) filter.getOrdering());
			return dis;
		}

		public BuilderT setStatusMessages(StatusMessages statusMessages) {
			((BaseFilterEntities<EntityT>) building).statusMessages = statusMessages;
			return dis;
		}

		public Integer size() {
			return ((BaseFilterEntities<EntityT>) building).size();
		}

	}

}
