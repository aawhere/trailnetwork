/**
 * 
 */
package com.aawhere.persist.objectify;

import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.DateCreatedFilterable;

import com.googlecode.objectify.condition.PojoIf;

/**
 * Allows specific implementors of {@link BaseEntity} to index the
 * {@link BaseEntity#getDateCreated()} field.
 * 
 * @author aroller
 * 
 */
public class IfDateCreatedFilterable
		extends PojoIf<BaseEntity<?, ?>> {

	/**
	 * Returns true if and only if the given entity implements {@link DateCreatedFilterable} and the
	 * dateCreated is not null.
	 */
	@Override
	public boolean matchesPojo(BaseEntity<?, ?> entity) {
		return entity instanceof DateCreatedFilterable && entity.getDateCreated() != null;
	}

}
