package com.aawhere.persist;

import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.id.HasParent;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.xml.XmlNamespace;
import com.googlecode.objectify.annotation.Id;

@XmlType(namespace = XmlNamespace.API_VALUE, name = "string-id-entity")
@XmlAccessorType(XmlAccessType.NONE)
public abstract class StringBaseEntity<KindT extends StringBaseEntity<KindT, IdentifierT>, IdentifierT extends Identifier<String, KindT>>
		extends BaseEntity<KindT, IdentifierT> {

	private static final long serialVersionUID = -2954458165183919811L;

	/** Use {@link Builder} to construct BaseEntity */
	protected StringBaseEntity() {
	}

	@Id
	@XmlAttribute
	private String id;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#idValue()
	 */
	@Override
	protected Object idValue() {
		return id;
	}

	/**
	 * Used to construct all instances of StringBaseEntity.
	 * 
	 * @param <T>
	 */
	@XmlTransient
	@XmlAccessorType(XmlAccessType.NONE)
	public abstract static class Builder<KindT extends StringBaseEntity<KindT, IdentifierT>, BuilderT extends BaseEntity.Builder<KindT, String, IdentifierT, BuilderT>, IdentifierT extends Identifier<String, KindT>>
			extends BaseEntity.Builder<KindT, String, IdentifierT, BuilderT> {

		/**
		 * @deprecated no need to pass identifier Type.
		 * 
		 * @param entity
		 * @param identifierType
		 */
		@Deprecated
		public Builder(KindT entity, Class<IdentifierT> identifierType) {
			super(entity);
		}

		/**
		 * 
		 */
		public Builder(KindT entity) {
			super(entity);
		}

		@Override
		protected BuilderT setId(IdentifierT id) {

			String existingId = ((StringBaseEntity<KindT, IdentifierT>) building).id;
			if (existingId != null) {
				Assertion.exceptions().eq(existingId, id.getValue());
			}
			if (id instanceof HasParent<?>) {
				parentId(((HasParent<?>) id).getParent());
			}
			((StringBaseEntity<KindT, IdentifierT>) building).id = id.getValue();
			return dis;
		}

		/**
		 * simply assigns null to the id when a clone wishes to take over the properties, but assign
		 * a new id.
		 * 
		 * @return
		 */
		@Override
		protected BuilderT removeId() {
			super.removeId();
			((StringBaseEntity<KindT, IdentifierT>) building).id = null;
			return dis;
		}
	}// end Builder
}
