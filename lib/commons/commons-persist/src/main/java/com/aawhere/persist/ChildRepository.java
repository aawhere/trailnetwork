/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;

/**
 * A special Repository for those Entities which HasParent. FIXME: Identify the parent through
 * generics.
 * 
 * @author aroller
 * 
 */
public interface ChildRepository<E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends Repository<E, I> {

	public Iterable<E> children(Identifier<?, ? extends BaseEntity> parentId);

	/** Provides a stream of children ids when given the parent. */
	public Iterable<I> childrenIds(Identifier<?, ? extends BaseEntity> parentId);
}
