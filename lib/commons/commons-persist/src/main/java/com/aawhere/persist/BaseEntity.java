package com.aawhere.persist;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;

import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.HasParent;
import com.aawhere.id.Identifiable;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.StringIdentifierWithParent;
import com.aawhere.joda.time.xml.SimpleDateTimeXmlAdapter;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.NonMutable;
import com.aawhere.persist.objectify.IfDateCreatedFilterable;
import com.aawhere.persist.objectify.IfDateUpdatedFilterable;
import com.aawhere.persist.objectify.ObjectifyKeyUtils;
import com.aawhere.xml.XmlNamespace;
import com.google.appengine.api.utils.SystemProperty;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Parent;

@XmlType(namespace = XmlNamespace.API_VALUE, name = "entity")
@XmlAccessorType(XmlAccessType.NONE)
public abstract class BaseEntity<KindT extends BaseEntity<KindT, IdentifierT>, IdentifierT extends Identifier<?, KindT>>
		extends NonMutable
		implements Identifiable<IdentifierT>, Serializable {

	private static final long serialVersionUID = 6386994666322466600L;
	/**
	 *
	 */
	static final Integer DEFAULT_VERSION = 0;

	/** Use {@link Builder} to construct BaseEntity */
	protected BaseEntity() {
	}

	@XmlAttribute
	private Integer version = DEFAULT_VERSION;
	@Index(IfDateUpdatedFilterable.class)
	private Date dateUpdated;
	@Index(IfDateCreatedFilterable.class)
	private Date dateCreated;

	/** Used for efficient caching of the id */
	@Ignore
	private IdentifierT idObject;

	/** Optional field which comes from {@link #parentKey}. */
	@XmlElement
	@Ignore
	private Identifier<?, ?> parentId;

	/**
	 * An objectify necessity to store as the Key instead of an Identifier. It is for this reason
	 * the objectify dependency was brought into commons-persist.
	 */
	@Parent
	private Key<? extends BaseEntity<?, ?>> parentKey;

	/**
	 * Auto-increment version # whenever persisted
	 */
	@OnSave
	protected void prePersist() {
		validateNotMutable();
		this.version++;
		// it can only be created once. Everything else is an update
		if (this.dateCreated == null) {
			this.dateCreated = new Date();
			// if date updated is indexed then it's worth setting immediately
			if (this instanceof DateUpdatedFilterable) {
				this.dateUpdated = new Date();
			}
		} else {
			// no reason to store update until it is actually updated (i.e. version 2)
			this.dateUpdated = new Date();
		}

	}

	protected Identifiable<?> parentStub(Identifier<?, ?> parentId) {
		return null;
	}

	@OnLoad
	protected void postLoad() {
		validateNotMutable();
		if (this.parentId == null) {
			Class<? extends Identifier<?, ?>> parentIdClass = parentIdType();
			if (parentIdClass != null) {
				InvalidArgumentException.required(parentKey, "parentKey");
				this.parentId = ObjectifyKeyUtils.idRelaxed(parentKey, parentIdClass);
			}
		}

	}

	/**
	 * Implemented by children that emit a parent.
	 * 
	 * @return
	 */
	protected Class<? extends Identifier<?, ?>> parentIdType() {
		return null;
	}

	public final Integer getEntityVersion() {
		validateNotMutable();
		return version;
	}

	abstract protected Object idValue();

	public IdentifierT id() {
		return getId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.id.Identifiable#getId()
	 */
	@Override
	public IdentifierT getId() {
		if (this.idObject == null) {
			final Object idValue = idValue();
			if (idValue != null) {
				Class<IdentifierT> identifierType = EntityUtil.getIdentifierType(this);
				if (HasParent.class.isAssignableFrom(identifierType)) {
					Identifier<?, ?> parentId = getParentId();
					Assertion.assertNotNull("parentId", parentId);
					this.idObject = (IdentifierT) IdentifierUtils.idWithParent(idValue, identifierType, parentId);
				} else {
					this.idObject = IdentifierUtils.idFrom(idValue, identifierType);
				}
			}
		}
		return this.idObject;
	}

	/**
	 * Provides the composition of parent-child ids allowing aboslute reference. This can be used in
	 * Keeping protected to avoid confusion for those that don't have a composite id. Children may
	 * expose more publicly.
	 * 
	 * @see StringIdentifierWithParent#getCompositeValue()
	 * @return
	 */
	@XmlAttribute
	protected String getCompositeId() {
		return (getId() instanceof HasParent) ? String.valueOf(getId()) : null;
	}

	/**
	 * For those implementations that have a parent (i.e. composite relationship) they should
	 * override and provide the parent using the {@link Parent} annotation on the field.
	 * 
	 * @return
	 */
	public Identifier<?, ?> getParentId() {
		return this.parentId;
	}

	public DateTime dateCreated() {
		return getDateCreated();
	}

	public DateTime dateUpdated() {
		return getDateUpdated();
	}

	/**
	 * @return the dateCreated
	 */
	@XmlElement(name = BASE_FIELD.DATE_CREATED)
	@XmlJavaTypeAdapter(type = DateTime.class, value = SimpleDateTimeXmlAdapter.class)
	@Field(key = BASE_FIELD.DATE_CREATED, dataType = FieldDataType.DATE)
	public final DateTime getDateCreated() {
		validateNotMutable();
		DateTime result;
		if (this.dateCreated == null) {
			result = null;
		} else {
			result = new DateTime(this.dateCreated);
		}
		return result;
	}

	/**
	 * Returns the last time this entity was modified. This time would correspond with the last
	 * increment of {@link #getEntityVersion()}. If no update date is stored this will simply return
	 * the create date as an indicator of the last write.
	 * 
	 * @return
	 */
	@XmlElement(name = BASE_FIELD.DATE_UPDATED)
	@XmlJavaTypeAdapter(type = DateTime.class, value = SimpleDateTimeXmlAdapter.class)
	@Field(key = BASE_FIELD.DATE_UPDATED, dataType = FieldDataType.DATE)
	public final DateTime getDateUpdated() {
		validateNotMutable();
		DateTime result;
		if (this.dateUpdated == null) {
			// updated isn't stored during creation anymore to save space
			result = this.getDateCreated();
		} else {
			result = new DateTime(this.dateUpdated);
		}
		return result;
	}

	@Override
	public final boolean equals(Object obj) {
		validateNotMutable();
		// assume false until proven otherwise
		boolean result = false;
		if (obj instanceof BaseEntity<?, ?>) {
			BaseEntity<?, ?> that = (BaseEntity<?, ?>) obj;
			final Object id = this.getId();
			final Object thatId = that.getId();
			if (id != null && thatId != null && thatId.equals(id)) {
				result = true;
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		validateNotMutable();
		int hash = 1;
		if (this.getId() == null) {
			hash = hash + super.hashCode();
		} else {
			int prime = 31;
			hash = hash * prime + this.getId().hashCode();
		}
		return hash;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		final IdentifierT id = getId();
		if (id != null) {
			if (parentId != null) {
				return IdentifierUtils.createCompositeIdValue(parentId, id);
			} else {
				return id.getQualifiedString();
			}
		} else {
			return super.toString();
		}
	}

	/**
	 * Used to construct all instances of BaseEntity.
	 * 
	 * @param <T>
	 */
	@XmlTransient
	@XmlAccessorType(XmlAccessType.NONE)
	public abstract static class Builder<KindT extends BaseEntity<KindT, IdentifierT>, ValueT extends Comparable<ValueT>, IdentifierT extends Identifier<?, KindT>, BuilderT extends Builder<KindT, ValueT, IdentifierT, BuilderT>>
			extends AbstractObjectBuilder<KindT, BuilderT> {

		public Builder(KindT entity) {
			super(entity);
		}

		/** Called before building this will indicate if an id has already been set. */
		protected Boolean hasId() {
			return building.idValue() != null;
		}

		protected IdentifierT id() {
			return building().id();
		}

		@SuppressWarnings("unchecked")
		protected BuilderT parentId(Identifier<?, ?> parentId) {
			Assertion.assertNotNull("you must provide parentIdType to use the parent id", building().parentIdType());
			building().parentId = parentId;
			try {
				// parent key can not be set in pre-persist
				// https://groups.google.com/forum/?fromgroups#!searchin/objectify-appengine/parent$20prepersist/objectify-appengine/FSKjxfX7M7I/fGRoQw1vWBAJ
				building().parentKey = (Key<? extends BaseEntity<?, ?>>) ObjectifyKeyUtils
						.keyWithParents(building().parentId);
			} catch (Exception e) {
				// not available in test environments, but fail if this happens in the dev server or
				// production
				if (SystemProperty.Environment.Value.Production.equals(SystemProperty.environment.value())
						|| SystemProperty.Environment.Value.Production.equals(SystemProperty.environment.value())) {
					throw new RuntimeException("unable to create parent key " + building().parentId + " for "
							+ building().idObject + e.getMessage());
				}
			}
			return dis;
		}

		protected Boolean hasParentId() {
			return building().parentId != null;
		}

		/* package */abstract BuilderT setId(IdentifierT id);

		protected BuilderT removeId() {
			building().idObject = null;
			return dis;
		}

		private BaseEntity<KindT, IdentifierT> building() {
			return (building);
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			if (this instanceof SelfIdentifyingEntity) {
				Assertion.exceptions().assertTrue("self identifying must have id.", hasId());
			}
		}

	}// end Builder

	@XmlType(namespace = XmlNamespace.API_VALUE, name = "baseEntityFields")
	public static abstract class BASE_FIELD {
		protected static final String FIELDS = "Fields";
		@XmlAttribute
		public static final String DATE_UPDATED = "dateUpdated";
		@XmlAttribute
		public static final String ID = "id";
		@XmlAttribute
		public static final String DATE_CREATED = "dateCreated";
		public static final String DESCRIPTION = "description";
	}

}
