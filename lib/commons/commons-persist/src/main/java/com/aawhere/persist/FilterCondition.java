/**
 *
 */
package com.aawhere.persist;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.field.FieldKey;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;

/**
 * A simple transport for a field operand, a {@link FilterOperator} and a value operand.
 * 
 * 
 * http://en.wikipedia.org/wiki/Relational_operator
 * 
 * 
 * @author aroller
 * 
 */
@XmlJavaTypeAdapter(FilterConditionXmlAdapter.class)
public class FilterCondition<V>
		implements Serializable {

	private static final long serialVersionUID = -9088889260851153108L;

	/**
	 * Used to construct all instances of FilterCondition.
	 */
	@XmlTransient
	public static class Builder<V>
			extends BaseBuilder<FilterCondition<V>, V, Builder<V>> {
		public Builder() {
			super(new FilterCondition<V>());
		}

	}

	/**
	 * Supports building extensions of {@link FilterCondition}.
	 * 
	 * @author aroller
	 * 
	 * @param <FC>
	 * @param <B>
	 */
	abstract public static class BaseBuilder<FC extends FilterCondition<V>, V, B extends BaseBuilder<FC, V, B>>
			extends AbstractObjectBuilder<FC, B> {

		protected BaseBuilder(FC filterCondition) {
			super(filterCondition);
		}

		public B field(String field) {
			((FilterCondition<V>) building).field = field;
			return dis;
		}

		public B field(FieldKey key) {
			((FilterCondition<V>) building).field = key.getAbsoluteKey();
			return dis;
		}

		public B operator(FilterOperator operator) {
			((FilterCondition<V>) building).operator = operator;
			return dis;
		}

		public B value(V value) {
			((FilterCondition<V>) building).value = value;
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("field", ((FilterCondition<V>) building).field);
			Assertion.assertNotNull("operator", ((FilterCondition<V>) building).operator);
		}

	}// end Builder

	/** Use {@link Builder} to construct FilterCondition */
	protected FilterCondition() {
	}

	public static <V> Builder<V> create() {
		return new Builder<>();
	}

	/**
	 * The field name that is to be queried against.
	 * 
	 * TODO: formalize this using an object, but for now just provide the exact database field match
	 */
	@XmlAttribute
	private String field;
	/**
	 * A relational operator that will be applied between the {@link #field} and the {@link #value}.
	 * 
	 */
	@XmlAttribute
	private FilterOperator operator = FilterOperator.EQUALS;
	/**
	 * Any object, in its orginal form, that is to be applied against the field using the operator.
	 * These objects are translated using appropriate translators that may be datastore dependent.
	 * 
	 * 
	 * TODO: the field would indicate the data type supported for this value.
	 * 
	 * TODO:Enable to transport via XML, however, this is a challenge with anything beyond a simple
	 * type. See unit tests.
	 * 
	 */
	@XmlElement
	private V value;

	/**
	 * @return the field
	 */
	public String getField() {
		return this.field;
	}

	/**
	 * @return the operator
	 */
	public FilterOperator getOperator() {
		return this.operator;
	}

	/**
	 * @return the value
	 */
	public V getValue() {
		return this.value;
	}

	/**
	 * Provides a standard syntax of a condition using a field operator value combination separated
	 * by spaces.
	 * 
	 * {field} {operator} {value can have spaces}
	 */
	@Override
	public String toString() {
		String separator = " ";
		return new StringBuilder().append(field).append(separator).append(operator.operator).append(separator)
				.append(value).toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.field == null) ? 0 : this.field.hashCode());
		result = prime * result + ((this.operator == null) ? 0 : this.operator.hashCode());
		result = prime * result + ((this.value == null) ? 0 : this.value.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		@SuppressWarnings("rawtypes")
		FilterCondition other = (FilterCondition) obj;
		if (this.field == null) {
			if (other.field != null) {
				return false;
			}
		} else if (!this.field.equals(other.field)) {
			return false;
		}
		if (this.operator != other.operator) {
			return false;
		}
		if (this.value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!this.value.equals(other.value)) {
			return false;
		}
		return true;
	}

}
