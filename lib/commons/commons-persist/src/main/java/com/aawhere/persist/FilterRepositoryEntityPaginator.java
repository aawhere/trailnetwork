/**
 * 
 */
package com.aawhere.persist;

import com.aawhere.id.Identifier;

/**
 * @author aroller
 * 
 */
public class FilterRepositoryEntityPaginator<S extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
		extends FilterRepositoryPaginator<E, S, E, I> {

	public static <ES extends BaseFilterEntities<E>, E extends BaseEntity<E, I>, I extends Identifier<?, E>>
			FilterRepositoryPaginator.Builder<E, ES, E, I> create() {
		return new FilterRepositoryPaginator.Builder<E, ES, E, I>(new FilterRepositoryEntityPaginator<ES, E, I>());
	}

	/** Use {@link Builder} to construct FilterRepositoryIdIterator */
	private FilterRepositoryEntityPaginator() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.FilterRepositoryPaginator#filterResults(com.aawhere.persist.Filter)
	 */
	@Override
	protected Iterable<E> filterResults(Filter pagingFilter) {
		return repository.filter(pagingFilter);
	}

}
