/**
 *
 */
package com.aawhere.persist;

import com.aawhere.field.type.FieldDataType;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Thrown when attempting to use a {@link FilterOperator} that doesn't support the provided
 * {@link FieldDataType}
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(HttpStatusCode.BAD_REQUEST)
public class UnsupportedFilterOperatorException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 7159209084472173330L;
	final private FilterOperator operator;
	final private FieldDataType dataType;

	/**
		 *
		 */
	public UnsupportedFilterOperatorException(FilterOperator operator, FieldDataType dataType) {
		super(new CompleteMessage.Builder().setMessage(PersistMessage.UNSUPPORTED_FILTER_CONDITION)
				.addParameter(PersistMessage.Param.FILTER_OPERATOR, operator.operator)
				.addParameter(PersistMessage.Param.FIELD_DATA_TYPE, dataType.getValue()).build());
		this.operator = operator;
		this.dataType = dataType;
	}

	public UnsupportedFilterOperatorException(FilterOperator operator, CompleteMessage message) {
		super(message);
		this.operator = operator;
		this.dataType = null;
	}

	/**
	 * Some operators just don't make sense when the value is null. This will throw if the value is
	 * null.
	 */
	public static void nullNotSupported(FilterOperator operator, Object value)
			throws UnsupportedFilterOperatorException {
		if (value == null) {
			throw new UnsupportedFilterOperatorException(operator, CompleteMessage
					.create(PersistMessage.UNSUPPORTED_FILTER_CONDITION)
					.param(PersistMessage.Param.FILTER_OPERATOR, operator)
					.param(PersistMessage.Param.FIELD_DATA_TYPE, Iterable.class.getSimpleName()).build());
		}
	}

	/**
	 * @return the dataType
	 */
	public FieldDataType getDataType() {
		return this.dataType;
	}

	/**
	 * @return the operator
	 */
	public FilterOperator getOperator() {
		return this.operator;
	}

}
