/**
 *
 */
package com.aawhere.queue;

import org.joda.time.DateTime;

/**
 * An abstraction for a messaging queue that receives tasks and executes them at some rate until
 * empty.
 * 
 * @author Brian Chapman
 * 
 */
public interface Queue {

	static final Integer MAX_BATCH_SIZE = 100;
	public static Boolean ASYNCH = Boolean.TRUE;
	public static Boolean SYNCH = Boolean.FALSE;

	/**
	 * Provides the ability for a user of the queue to know if a task was successfully added.
	 * 
	 * @author aroller
	 * 
	 */
	public static enum Result {
		/** Successfully added to the queue. */
		ADDED,
		/** Task not added because it is a duplicate of another. */
		DUPLICATE,
		/** Unable to confirm if task successfully was added since the request was asynchronous. */
		ASYNCH
	}

	/**
	 * A simple way to add a task when you want a guarantee it will be added.
	 * 
	 * @param task
	 */
	public Result add(Task task);

	/**
	 * Useful for more performance-oriented tasks this will add the {@link Task}s provided to the
	 * queue in a bulk fashion reducing communication times when adding each task separately
	 * {@link #add(Task)}. The batch is limited to {@link #MAX_BATCH_SIZE}
	 * 
	 * If asynch is true then this will improve the performance that much greater, but it would be
	 * less secure in knowing your task was added.
	 * 
	 * @see #ASYNCH
	 * @see #SYNCH
	 * 
	 * @param task
	 * @param asynch
	 * @return ADDED if successfully added, DUPLICATE if not added because it is a duplicate, ASYNCH
	 *         if the value is asynchronous and success is not able to be confirmed.
	 */
	public Result add(Iterable<Task> task, Boolean asynch);

	/**
	 * The number of tasks remaining in the queue.
	 * */
	public Integer size();

	/**
	 * Provides an estimated time when the last task in the queue will be processed.
	 * 
	 * @return
	 */
	public DateTime lastTaskEta();

}
