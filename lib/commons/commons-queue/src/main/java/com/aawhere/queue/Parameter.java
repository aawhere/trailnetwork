/**
 *
 */
package com.aawhere.queue;

/**
 * A parameter to pass to a {@link Queue}
 * 
 * @author Brian Chapman
 * 
 */
public abstract class Parameter<ValueT> {

	private String name;
	private ValueT value;

	public Parameter(String name, ValueT value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public ValueT getValue() {
		return value;
	}

	public abstract byte[] getValueAsByte();
}
