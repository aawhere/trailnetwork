/**
 * 
 */
package com.aawhere.queue;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.number.NumberUtilsExtended;

import com.google.common.base.Function;
import com.google.common.collect.Maps;

/**
 * A manager of multiple batch queues that may be accessed during the same iteration. The task is
 * dispatched based off the key given.
 * 
 * @author aroller
 * @param <K>
 *            a key used to determine where to dispatch the task. It must follow object equality
 *            requirements to work properly.
 */
public class BatchQueues<K> {

	/**
	 * Used to construct all instances of BatchQueues.
	 */
	public static class Builder<K>
			extends ObjectBuilder<BatchQueues<K>> {

		public Builder() {
			super(new BatchQueues<K>());
		}

		public Builder<K> queueProvider(Function<K, Queue> queueProvider) {
			building.queueProvider = queueProvider;
			return this;
		}

		/*
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("queueProvider", building.queueProvider);
		}

		@Override
		public BatchQueues<K> build() {
			BatchQueues<K> built = super.build();
			built.queues = Maps.newHashMap();
			return built;
		}

	}// end Builder

	public static <K> Builder<K> create() {
		return new Builder<K>();
	}

	/** Use {@link Builder} to construct BatchQueues */
	private BatchQueues() {
	}

	private Map<K, BatchQueue> queues;
	private Function<K, Queue> queueProvider;

	public void add(Task task, K key) {
		BatchQueue batchQueue = queues.get(key);
		if (batchQueue == null) {
			batchQueue = BatchQueue.create().setQueue(this.queueProvider.apply(key)).build();
			queues.put(key, batchQueue);
		}
		batchQueue.add(task);
	}

	/**
	 * Calls finish on all queues being managed and returns the total for each key.
	 * 
	 * @return
	 */
	public Map<K, Integer> finishEach() {

		HashMap<K, Integer> counts = Maps.newHashMap();
		Set<Entry<K, BatchQueue>> entrySet = this.queues.entrySet();
		for (Entry<K, BatchQueue> entry : entrySet) {
			counts.put(entry.getKey(), entry.getValue().finish());
		}
		return counts;
	}

	/**
	 * Calls {@link #finishEach()} and cumulates the total for this common scenario.
	 * 
	 * @return
	 */
	public Integer finishAll() {
		return NumberUtilsExtended.sum(finishEach().values());
	}
}
