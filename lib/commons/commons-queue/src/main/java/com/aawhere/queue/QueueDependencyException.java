/**
 * 
 */
package com.aawhere.queue;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates that the request depends upon an asynchronous {@link Queue} to complete processing and
 * any resulting data is not currently available.
 * 
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.ACCEPTED)
public class QueueDependencyException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 6256251284093949847L;

	/**
	 * 
	 */
	public QueueDependencyException() {
		super(new CompleteMessage.Builder(QueueMessage.ACCEPTED_BUT_PROCESSING).build());
	}
}
