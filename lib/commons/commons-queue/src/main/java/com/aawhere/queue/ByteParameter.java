/**
 *
 */
package com.aawhere.queue;

/**
 * @author brian
 * 
 */
public class ByteParameter
		extends Parameter<byte[]> {

	/**
	 * @param name
	 * @param value
	 */
	public ByteParameter(String name, byte[] value) {
		super(name, value);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.queue.Parameter#getValueAsByte()
	 */
	@Override
	public byte[] getValueAsByte() {
		return getValue();
	}

}
