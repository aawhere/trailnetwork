/**
 * 
 */
package com.aawhere.queue;

import java.util.Set;
import java.util.concurrent.TimeoutException;

/**
 * Useful methods to assist with managing {@link Queue}s and {@link Task}s.
 * 
 * @author aroller
 * 
 */
public class QueueUtil {

	public static Boolean REPEAT_TASKS_ALLOWED = true;
	public static Boolean REPEAT_TASKS_SUPPRESSED = false;

	/**
	 * 
	 */
	private QueueUtil() {
	}

	/**
	 * Allows the queue to process and returns when all queues are empty.
	 * 
	 * @param timeoutMillis
	 * @return the total time in millis it took to process. If the timeoutMillis is greater than
	 *         this value...then it was a timeout.
	 * @throws TimeoutException
	 * @throws QueueTimeoutException
	 */
	public static Integer waitForEmptyQueues(com.aawhere.queue.QueueFactory queueFactory, Integer timeoutMillis)
			throws QueueTimeoutException {
		long start = System.currentTimeMillis();
		Integer startCount = null;
		int count = 0;
		Integer duration;
		// we can make this a param
		int sleepMillis = 100;
		do {
			try {
				Thread.sleep(sleepMillis);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
			Set<String> knownQueueNames = queueFactory.getKnownQueueNames();
			count = 0;
			for (String queueName : knownQueueNames) {
				Queue queue = queueFactory.getQueue(queueName);
				count += queue.size();
			}
			if (startCount == null) {
				startCount = count;
			}
			duration = (int) (System.currentTimeMillis() - start);
		} while (count > 0 && duration <= timeoutMillis);
		if (duration > timeoutMillis) {
			throw new QueueTimeoutException(startCount, count, timeoutMillis);
		}
		return duration;

	}

}
