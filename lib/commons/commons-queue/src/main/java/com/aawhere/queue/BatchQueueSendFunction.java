/**
 * 
 */
package com.aawhere.queue;

import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.lang.Assertion;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Given a task this will add the task to the batch queue given during build. So the task returned
 * is actually the same as given, however, it has been added to the batch queue so it's "state" has
 * changed.
 * 
 * <pre>
 * Usage:
 * taskFunction would be some function that converts your id into a task
 * 
 * Iterables.transform(ids,taskFunction);
 * Iterables.transform(tasks,batchQueueFunction);
 * </pre>
 * 
 * @author aroller
 * 
 */
public class BatchQueueSendFunction
		implements Function<Task, Task> {

	public static BatchQueueSendFunction build(BatchQueue queue) {
		BatchQueueSendFunction batchQueueSendFunction = new BatchQueueSendFunction();
		batchQueueSendFunction.queue = queue;
		Assertion.assertNotNull(queue);
		return batchQueueSendFunction;
	}

	private BatchQueue queue;

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Task apply(Task input) {
		if (input != null) {
			queue.add(input);
		}
		return input;
	}

	/**
	 * Sends all those tasks in the given iterable to the given queue, calling finish and returning
	 * the size sent.
	 * 
	 * @param tasks
	 * @param queue
	 * @return
	 */
	public static Integer send(Iterable<Task> tasks, BatchQueue queue) {
		BatchQueueSendFunction function = BatchQueueSendFunction.build(queue);
		Iterable<Task> sent = Iterables.transform(tasks, function);
		// forces iteration.
		CollectionUtilsExtended.flush(sent);
		return queue.finish();
	}
}
