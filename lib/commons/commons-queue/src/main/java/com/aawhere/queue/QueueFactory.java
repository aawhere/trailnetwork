/**
 * 
 */
package com.aawhere.queue;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Used by all in the system to abstract queue implementation from the code.
 * 
 * Implementations should Inherit {@link Producer}.
 * 
 * Systems using specific implementations should setup Injection so this Factory is a singleton.
 * 
 * @author aroller
 * 
 */
@Singleton
public class QueueFactory
		implements Serializable {

	private static final long serialVersionUID = -2506789200904509675L;
	private Producer producer;
	private HashSet<String> knownQueueNames = new HashSet<String>();

	/**
	 * 
	 */
	@Inject
	public QueueFactory(Producer producer) {
		this.producer = producer;
	}

	public Queue getQueue(String queueName) {
		this.knownQueueNames.add(queueName);
		return producer.getQueue(queueName);
	}

	/**
	 * The names of any queues that has been served up by this queue factory. This is not
	 * necessarily all inclusive since some code may call this after, but it will provide any queue
	 * that has already been interacted which is typically the only ones that matter.
	 * 
	 * @return
	 */
	public Set<String> getKnownQueueNames() {
		return this.knownQueueNames;
	}

	public static interface Producer
			extends Serializable {

		Queue getQueue(String queueName);
	}
}
