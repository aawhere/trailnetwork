/**
 *
 */
package com.aawhere.queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;

/**
 * An task that is insertable into a Queue.
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class Task
		implements Cloneable {

	/**
	 * Indicates how often this task may repeat. With an allowed error of 1. these are calendar
	 * objects so depending upon where it lands in the calendar a repeat could happen soon after,
	 * but not again. Request monthly at 23:59 on January 31st you can post another on February 1st
	 * at 00:00 only 1 minute later, but not again until March.
	 * */
	public static enum RepeatFrequency {
		HOURLY("yyyy-MM-dd-HH"), TWICE_DAILY("yyyy-MM-dd-a"), DAILY("yyyy-MM-dd"), WEEKLY("yyyy'w'ww"), MONTHLY(
				"yyyy-MM");
		/**
		 * DateTime format that will produce a date string matching the frequency indicated
		 * 
		 * @see DateTimeFormat
		 */
		public final String pattern;

		private RepeatFrequency(String pattern) {
			this.pattern = pattern;
		}
	}

	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private List<Parameter<?>> params = new ArrayList<Parameter<?>>();
	@XmlElement
	private EndPoint endPoint;
	@XmlElement
	private String payload;

	private byte[] payloadByte;
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private MediaType payloadContentType;
	@XmlAttribute
	private Method method = Method.POST;
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private Map<String, String> headers = new HashMap<String, String>();
	/** @see #isRepeatsAllowed() */
	@XmlAttribute
	private Boolean allowRepeats = true;
	/**
	 * Indicates how often the task may be repeated. Repeats should be ignored if within the period.
	 */
	@XmlAttribute
	private RepeatFrequency repeatFrequencyAllowed;

	/** The number of milliseconds the task will be delayed in the queue, if supported. */
	@XmlAttribute
	private Long countdownMillis;

	/**
	 * Indicates that this task should target the version of the system which this code is running.
	 * False indicates the default system version is preferred. The default is to target the
	 * version.
	 */
	@XmlAttribute
	private Boolean versionTargeted = true;

	public enum Method {
		GET, POST, PUT
	};

	private void addParam(Parameter<?> param) {
		params.add(param);
	}

	public Parameter<?>[] getParams() {
		return params.toArray(new Parameter<?>[params.size()]);
	}

	/**
	 * @return the headers
	 */
	public Map<String, String> getHeaders() {
		return this.headers;
	}

	public EndPoint getEndPoint() {
		return endPoint;
	}

	public String getStringPayload() {
		return payload;
	}

	/**
	 * @return the versionTargeted
	 */
	public Boolean isVersionTargeted() {
		return this.versionTargeted;
	}

	public byte[] getBinaryPayload() {
		return payloadByte;
	}

	/**
	 * @return the countdownMillis
	 */
	public Long getCountdownMillis() {
		return this.countdownMillis;
	}

	public Boolean hasCountdown() {
		return this.countdownMillis != null;
	}

	/**
	 * Can the task be queued multiple times in the same queue.
	 * 
	 * @return
	 */
	public Boolean isRepeatsAllowed() {
		return this.allowRepeats;
	}

	/**
	 * @return the repeatFrequencyAllowed
	 */
	public RepeatFrequency getRepeatFrequencyAllowed() {
		return this.repeatFrequencyAllowed;
	}

	public Boolean hasRepeatFrequencyAllowed() {
		return this.repeatFrequencyAllowed != null;
	}

	public boolean hasPayload() {
		if (payload == null && payloadByte == null) {
			return false;
		}
		return true;
	}

	public boolean hasBinaryPayload() {
		if (payloadByte == null) {
			return false;
		}
		return true;
	}

	public boolean hasStringPayload() {
		if (payload == null) {
			return false;
		}
		return true;
	}

	/**
	 * @return the payloadContentType
	 */
	public MediaType getPayloadContentType() {
		return this.payloadContentType;
	}

	/**
	 * @return the method
	 */
	public Method getMethod() {
		return this.method;
	}

	/**
	 * Used to construct all instances of Task.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<Task> {

		public Builder() {
			super(new Task());
		}

		public Builder(Task task) {
			super(task);
		}

		public Builder setEndPoint(EndPoint endPoint) {
			building.endPoint = endPoint;
			return this;
		}

		public Builder addParam(Parameter<?> param) {
			building.addParam(param);
			return this;
		}

		/**
		 * Assigning this also indicates repeats are not allowed.
		 * 
		 * @see #setAllowRepeats(Boolean)
		 * @see Task#repeatFrequencyAllowed
		 * @param repeatFrequencyAllowed
		 * @return
		 */
		public Builder setRepeatFrequencyAllowed(RepeatFrequency repeatFrequencyAllowed) {
			building.repeatFrequencyAllowed = repeatFrequencyAllowed;
			setAllowRepeats(false);
			return this;
		}

		public Builder setAllowRepeats(Boolean allowRepeats) {
			building.allowRepeats = allowRepeats;
			return this;
		}

		public Builder setMethod(Method method) {
			building.method = method;
			return this;
		}

		/** Calling this will make this task target the default version, not the current. */
		public Builder setVersionExcluded() {
			building.versionTargeted = false;
			return this;
		}

		/**
		 * 
		 * @param headerKey
		 *            available from {@link HttpHeaders}.
		 * @param headerValue
		 *            value appropriate for corresponding headerKey
		 * @return
		 */
		public Builder addHeader(String headerKey, String headerValue) {
			building.headers.put(headerKey, headerValue);
			return this;
		}

		/**
		 * Set the payload as a string.
		 * 
		 * @param string
		 * @return
		 */
		public Builder setPayload(String string, MediaType contentType) {
			building.payload = string;
			building.payloadContentType = contentType;
			return this;
		}

		public Builder setPayload(byte[] payload, MediaType payloadContentType) {
			building.payloadByte = payload;
			building.payloadContentType = payloadContentType;
			return this;
		}

		/**
		 * Combination task that sets the allowed frequency to the duration requested and also
		 * delays the task for that same frequency allowed.
		 * 
		 * @see #setRepeatFrequencyAllowed(RepeatFrequency)
		 * @see #setCountdown(DateTime)
		 * 
		 * @param repeatFrequency
		 *            the chosen frequency. null does nothing
		 * @return
		 */
		public Builder setCountdown(@Nullable RepeatFrequency repeatFrequency) {
			if (repeatFrequency != null) {
				DateTime nextTaskDate = DateTime.now();

				// determine the task execution and how often another can be posted
				// this must ensure that when the task is executed again another task of the
				// same
				// frequency could be posted again.
				// so the delay must be long enough to get into the next calendar period
				switch (repeatFrequency) {
					case HOURLY:
						nextTaskDate = nextTaskDate.plusHours(1);
						break;
					case TWICE_DAILY:
						nextTaskDate = nextTaskDate.plusHours(12);
						break;
					case DAILY:
						nextTaskDate = nextTaskDate.plusDays(1);
						break;
					case WEEKLY:
						nextTaskDate = nextTaskDate.plusWeeks(1);
						break;
					case MONTHLY:
						nextTaskDate = nextTaskDate.plusMonths(1);
						break;
					default:
						throw new InvalidChoiceException(repeatFrequency);
				}
				setCountdown(nextTaskDate);
				setRepeatFrequencyAllowed(repeatFrequency);
			}
			return this;

		}

		public Builder setCountdownMillis(Long millis) {
			building.countdownMillis = millis;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().assertTrue(	"Cannot set both string and byte payloads",
												!(building.payload != null && building.payloadByte != null));
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public Task build() {
			Task built = super.build();
			built.headers = Collections.unmodifiableMap(built.headers);
			built.params = Collections.unmodifiableList(built.params);
			return built;
		}

		/**
		 * Calculates the millis so you don't have to. If the given date is in the past the argument
		 * is just ignored.
		 * 
		 * @param dateTimeAtStartOfDay
		 * @return
		 */
		public Builder setCountdown(DateTime estimatedTimeOfExecution) {
			final DateTime now = DateTime.now();
			if (estimatedTimeOfExecution.isAfter(now)) {
				setCountdownMillis(estimatedTimeOfExecution.getMillis() - now.getMillis());
			}
			return this;
		}
	}// end Builder

	/** Use {@link Builder} to construct Task */
	private Task() {
	}

	/**
	 * Convenience method to create builder.
	 * 
	 * @return
	 */
	public static Builder create() {
		return new Builder();
	}

	public static Builder clone(Task original) {
		try {
			return new Builder((Task) original.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return method + ":" + this.endPoint;
	}
}
