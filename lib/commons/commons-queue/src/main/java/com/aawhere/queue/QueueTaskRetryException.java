/**
 * 
 */
package com.aawhere.queue;

import static com.aawhere.queue.QueueMessage.*;
import static com.aawhere.queue.QueueMessage.Param.*;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.queue.QueueMessage.Param;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates that the request could not be processed and should be scheduled for retry.
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(HttpStatusCode.TASK_RETRY)
public class QueueTaskRetryException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 4106718321122050897L;

	/**
	 * 
	 */
	public QueueTaskRetryException(Task task) {
		super(new CompleteMessage.Builder(QueueMessage.RETRY_TASK).addParameter(Param.TASK_END_POINT,
																				task.getEndPoint().toString()).build());
	}

	QueueTaskRetryException(CompleteMessage message) {
		super(message);
	}

	public static QueueTaskRetryException sizeExceeded(String exceededingQueue, int size, int maxSize) {
		return new QueueTaskRetryException(CompleteMessage.create(MAX_SIZE_EXCEEDED)
				.addParameter(QUEUE_NAME, exceededingQueue).addParameter(SIZE, size).addParameter(MAX_SIZE, maxSize)
				.build());
	}

	/** If we can't check the stats let's play on the safe size and reject. */
	public static QueueTaskRetryException statsUnavailable(String queueName) {
		return new QueueTaskRetryException(CompleteMessage.create(STATS_UNAVAILBLE).param(QUEUE_NAME, queueName)
				.build());
	}
}
