/**
 *
 */
package com.aawhere.queue;

import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.xml.XmlNamespace;

/**
 * Represents an endpoint (url) for a {@link Task}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class EndPoint {

	private URI endPoint;

	public URI getUri() {
		return endPoint;
	}

	/** Synonym for xml requirements. */
	@XmlValue
	public String getAsString() {
		return toString();
	}

	@Override
	public String toString() {
		return endPoint.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.endPoint == null) ? 0 : this.endPoint.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EndPoint)) {
			return false;
		}
		EndPoint other = (EndPoint) obj;
		if (this.endPoint == null) {
			if (other.endPoint != null) {
				return false;
			}
		} else if (!this.endPoint.equals(other.endPoint)) {
			return false;
		}
		return true;
	}

	public static class Builder
			extends AbstractBuilder<EndPoint, Builder> {

		/**
		 * @param building
		 */
		protected Builder() {
			super(new EndPoint());
		}
	}

	public abstract static class AbstractBuilder<ResultT extends EndPoint, BuilderT extends AbstractBuilder<ResultT, BuilderT>>
			extends AbstractObjectBuilder<ResultT, BuilderT> {

		/**
		 * @param building
		 */
		protected AbstractBuilder(ResultT building) {
			super(building);
		}

		public BuilderT setEndPoint(String uri) throws URISyntaxException {
			setEndPoint(new URI(uri));
			return dis;
		}

		public BuilderT setEndPoint(URI endPoint) {
			((EndPoint) building).endPoint = endPoint;
			return dis;
		}
	}

	public static Builder create() {
		return new Builder();
	}

	/* Use Builder to construct */
	protected EndPoint() {
	}
}
