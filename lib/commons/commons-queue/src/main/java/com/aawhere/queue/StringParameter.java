/**
 *
 */
package com.aawhere.queue;

/**
 * @author brian
 * 
 */
public class StringParameter
		extends Parameter<String> {

	/**
	 * @param name
	 * @param value
	 */
	public StringParameter(String name, String value) {
		super(name, value);
	}

	/**
	 * Accepts any object and displays it's toString value. If toString isn't appropriate, then you
	 * convert it and pass it to {@link #StringParameter(String, String)}.
	 * 
	 * @param name
	 * @param value
	 */
	public StringParameter(String name, Object value) {
		super(name, String.valueOf(value));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.queue.Parameter#getValueAsByte()
	 */
	@Override
	public byte[] getValueAsByte() {
		return getValue().getBytes();
	}

}
