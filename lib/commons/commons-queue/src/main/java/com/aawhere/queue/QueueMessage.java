/**
 * 
 */
package com.aawhere.queue;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Messages useful for {@link Queue} exceptions and other reporting.
 * 
 * @author aroller
 * 
 */
public enum QueueMessage implements Message {

	/** @see Queue */
	ACCEPTED_BUT_PROCESSING("The information you requested is currently being processed.  Please try again later."),
	TIMEOUT(
			"Maximum {MAXIMUM_MILLIS} millis exceeded while the queue processed from {START_COUNT} to {FINISH_COUNT}.",
			Param.MAXIMUM_MILLIS,
			Param.START_COUNT,
			Param.FINISH_COUNT),
	/** @see QueueTaskRetryException */
	RETRY_TASK("The task {TASK_END_POINT} could not be processed and should be rescheduled", Param.TASK_END_POINT),
	/** @see QueueTaskRetryException#statsUnavailable(String) */
	STATS_UNAVAILBLE("The stats are currently unavailable for {QUEUE_NAME}.", Param.QUEUE_NAME),
	/** When the queue size has exceeded allowable limits. */
	MAX_SIZE_EXCEEDED(
			"The '{QUEUE_NAME}' queue has {SIZE} tasks, but is limited to {MAX_SIZE}.",
			Param.QUEUE_NAME,
			Param.SIZE,
			Param.MAX_SIZE);

	private ParamKey[] parameterKeys;
	private String message;

	private QueueMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** EXPLAIN_PARAM_HERE */
		FIRST_PARAM,
		/** total count of milliseconds willing to wait */
		MAXIMUM_MILLIS,
		/** The number in the queue when waiting began */
		START_COUNT,
		/** The number in the queue when we gave up waiting */
		FINISH_COUNT,
		/** The task id */
		TASK_END_POINT, QUEUE_NAME, SIZE, MAX_SIZE;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
