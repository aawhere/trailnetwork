/**
 * 
 */
package com.aawhere.queue;

import java.util.ArrayList;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.log.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Range;

/**
 * Simplifies the posting of tasks using batch, asynchroous processing.
 * 
 * Builder, add iteratively, finish when complete.
 * 
 * @author aroller
 * 
 */
public class BatchQueue {

	/**
	 * Used to construct all instances of BatchQueue.
	 */
	public static class Builder
			extends ObjectBuilder<BatchQueue> {

		public Builder() {
			super(new BatchQueue());
		}

		public BatchQueue build() {
			BatchQueue built = super.build();
			built.send();
			return built;
		}

		public Builder setQueue(Queue queue) {
			building.queue = queue;
			return this;
		}

		public Builder setBatchSize(Integer batchSize) {
			building.batchSize = batchSize;
			return this;
		}

		public Builder setAsynch(Boolean asynch) {
			building.asynch = asynch;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("queue", building.queue);
			Assertion.exceptions().assertRange("batchSize", building.batchSize, Range.atMost(Queue.MAX_BATCH_SIZE));

		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private int batchSize = Queue.MAX_BATCH_SIZE;
	private Queue queue;
	private ArrayList<Task> tasks;
	private int count = 0;
	private int numberOfBatches = 0;
	private Boolean asynch = Boolean.TRUE;

	/** Use {@link Builder} to construct BatchQueue */
	private BatchQueue() {
	}

	/** Adds the task to the batch. */
	public void add(Task task) {
		tasks.add(task);
		if (tasks.size() == batchSize) {
			send();
		}
	}

	/**
	 * Actually adds the tasks to the queue.
	 * 
	 */
	private void send() {
		if (this.tasks != null) {
			queue.add(tasks, this.asynch);
			this.count += tasks.size();
			this.numberOfBatches++;
		}
		this.tasks = Lists.newArrayListWithCapacity(batchSize);
	}

	/**
	 * Required call to complete the batch processing. It can only be called once.
	 * 
	 * @return total count of tasks added
	 */
	public Integer finish() {
		send();
		this.tasks = null;
		return count;
	}

	/**
	 * @return the numberOfBatches
	 */
	public Integer getNumberOfBatches() {
		return this.numberOfBatches;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		// not ideal, but will ensure tasks are sent if caller did not call finish
		// if you see this being called there is a batch queue being used, but not correctly caling
		// finish()
		if (this.tasks != null) {
			LoggerFactory.getLogger(getClass()).warning("finish() was never called so some tasks never sent "
					+ tasks.size());
			send();
		}
		super.finalize();
	}
}
