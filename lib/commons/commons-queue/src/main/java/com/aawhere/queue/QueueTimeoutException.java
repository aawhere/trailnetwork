/**
 * 
 */
package com.aawhere.queue;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Provides information about the queue while giving up waiting for it to empty.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.CLIENT_TIMEOUT)
public class QueueTimeoutException
		extends BaseException {

	private static final long serialVersionUID = -3172617700380103464L;

	/**
	 * 
	 */
	public QueueTimeoutException(Integer queueStartCount, Integer queueFinishCount, Integer timeoutInMillis) {
		super(new CompleteMessage.Builder(QueueMessage.TIMEOUT)
				.addParameter(QueueMessage.Param.START_COUNT, queueStartCount)
				.addParameter(QueueMessage.Param.FINISH_COUNT, queueFinishCount)
				.addParameter(QueueMessage.Param.MAXIMUM_MILLIS, timeoutInMillis).build());
	}

}
