/**
 *
 */
package com.aawhere.queue;

import static org.junit.Assert.*;

import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;

import org.joda.time.DateTime;

import com.aawhere.io.Marshal;
import com.aawhere.log.LoggerFactory;
import com.aawhere.test.TestUtil;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;

/**
 * @author Brian Chapman
 * 
 */
public class QueueTestUtil {
	public static final ArrayList<String> DEFAULT_PAYLOAD = Lists.newArrayList("Test1", "Test2");
	public static final String DEFAULT_STRING_PAYLOAD = "Test";
	public static final String TEST_ENDPOINT_URI = "/test";
	public static final String TEST_PARAM_VALUE1 = "testId";
	public static final String TEST_PARAM_NAME1 = "1";
	public static final String TEST_PARAM_VALUE2 = "testName";
	public static final String TEST_PARAM_NAME2 = "Testing";
	public static final String TEST_HEADER_KEY1 = HttpHeaders.ACCEPT;
	public static final String TEST_HEADER_VALUE1 = MediaType.JSON_UTF_8.toString();
	public static final String TEST_HEADER_KEY2 = HttpHeaders.ACCEPT_LANGUAGE;
	public static final String TEST_HEADER_VALUE2 = Locale.US.toString();

	public static Task createTaskWithBinaryPayload() {
		return createTaskWithBinaryPayload(DEFAULT_PAYLOAD, MediaType.ANY_APPLICATION_TYPE);
	}

	public static EndPoint getEndPoint() {
		return getEndPoint("");
	}

	public static EndPoint getEndPoint2() {
		return getEndPoint("/2");
	}

	public static EndPoint getEndPoint(String additionalPath) {
		try {
			return new EndPoint.Builder().setEndPoint(TEST_ENDPOINT_URI + additionalPath).build();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	public static EndPoint getRandomEndPoint() {
		return getEndPoint(TestUtil.generateRandomAlphaNumeric());
	}

	public static <ObjectT extends Serializable> Task
			createTaskWithBinaryPayload(ObjectT payload, MediaType contentType) {
		byte[] serialized = Marshal.marshal(payload);
		Task task = Task.create().setEndPoint(getEndPoint()).setPayload(serialized, contentType).build();
		return task;
	}

	public static <ObjectT extends Serializable> Task
			createTaskWithStringPayload(String payload, MediaType contentType) {
		Task task = Task.create().setEndPoint(getEndPoint()).setPayload(payload, contentType).build();
		return task;
	}

	public static Task createTaskWithParameters() {
		return createTaskWithParameters(getEndPoint());
	}

	public static Task createTaskWithCountdown(Long millis) {
		return Task.create().setEndPoint(getEndPoint()).setCountdownMillis(millis).build();
	}

	public static Task createTaskWithParameters(EndPoint endPoint) {
		Task task = Task.create().setEndPoint(endPoint)
				.addParam(new StringParameter(TEST_PARAM_NAME1, TEST_PARAM_VALUE1))
				.addParam(new StringParameter(TEST_PARAM_NAME2, TEST_PARAM_VALUE2)).build();
		return task;
	}

	public static Task createTaskWithHeaders() {
		Task task = Task.create().setEndPoint(getEndPoint()).addHeader(TEST_HEADER_KEY1, TEST_HEADER_VALUE1)
				.addHeader(TEST_HEADER_KEY2, TEST_HEADER_VALUE2).build();
		return task;
	}

	/**
	 * Asserts that any given expected string exists in the url posted in the queue.
	 * 
	 * 
	 * 
	 * @param queue
	 * @param expectedSize
	 *            ensures the queue has the right number. Pass null if you don't know.
	 * @param expectedInEndpoint
	 *            one or more strings expected to be in the queue.
	 */
	public static void assertQueueContains(Queue queue, Integer expectedSize, String... expectedInEndpoint) {
		TestQueue testQueue = (TestQueue) queue;
		java.util.Queue<Task> taskQueue = testQueue.getQueue();
		if (expectedSize != null) {
			assertEquals("wrong queue size " + taskQueue.toString(), expectedSize.intValue(), queue.size().intValue());
		}
		// for each expected make sure one of the tasks includes it. This avoids caring about order.
		for (int whichExpectedEndpoint = 0; whichExpectedEndpoint < expectedInEndpoint.length; whichExpectedEndpoint++) {
			String expected = expectedInEndpoint[whichExpectedEndpoint];
			boolean found = false;
			for (Task task : taskQueue) {
				String endpoint = task.getEndPoint().toString();
				if (endpoint.contains(expected)) {
					found = true;
				}
			}
			// if found once that's good enough
			assertTrue(expected + ", item #" + whichExpectedEndpoint + ", not found in any task " + taskQueue, found);

		}
	}

	/**
	 * Provides a simple queue implementation for unit testing only. No system Implementation (like
	 * GAE tasks) will be available in the scope of UnitTests.
	 * 
	 * @return
	 */
	public static QueueFactory getQueueFactory() {

		return new QueueFactory(new QueueFactoryUnitTestProducer());
	}

	public static class QueueFactoryUnitTestProducer
			implements QueueFactory.Producer {

		private static final long serialVersionUID = -8016912473888815560L;
		private HashMap<String, Queue> queues = new HashMap<String, Queue>();

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.queue.QueueFactory.Producer#getQueue(java.lang.String)
		 */
		@Override
		public Queue getQueue(String queueName) {
			Queue queue = queues.get(queueName);
			if (queue == null) {
				queue = new TestQueue(queueName);
				queues.put(queueName, queue);
			}
			return queue;
		}

	}

	public static class TestQueue
			implements Queue {

		private LinkedList<Task> queue = new LinkedList<Task>();
		private String queueName;
		private Result result = Result.ADDED;

		/**
		 * @param queueName2
		 */
		public TestQueue(String queueName) {
			this.queueName = queueName;
		}

		public TestQueue(String queueName, Queue.Result result) {
			this.queueName = queueName;
			this.result = result;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.queue.Queue#add(com.aawhere.queue.Task)
		 */
		@Override
		public Result add(Task task) {
			queue.add(task);
			return this.result;
		}

		/**
		 * Useful for tests who want access.
		 * 
		 * @return the queue
		 */
		public LinkedList<Task> getQueue() {
			return this.queue;
		}

		public Object pop() {
			return queue.poll();
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return queueName + queue.toString();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.queue.Queue#count()
		 */
		@Override
		public Integer size() {
			return queue.size();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.queue.Queue#add(java.lang.Iterable, java.lang.Boolean)
		 */
		@Override
		public Result add(Iterable<Task> task, Boolean asynch) {
			Result result;
			if (asynch) {
				LoggerFactory.getLogger(getClass())
						.warning("asynch requested, but we haven't implemented that in the test queue");
				result = Result.ASYNCH;
			} else {
				result = Result.ADDED;
			}
			Iterables.addAll(queue, task);
			return Result.ADDED;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.queue.Queue#lastTaskEta()
		 */
		@Override
		public DateTime lastTaskEta() {
			// TODO: if logic to estimated task eta is required, implement here.
			return DateTime.now();
		}

	}

	/**
	 * @param timingCourseAttemptQueue
	 * @return
	 */
	public static Task getLast(TestQueue queue) {
		return queue.getQueue().getLast();
	}

	/**
	 * Populates the given queue with random tasks equal to the size given.
	 * 
	 * @param queue
	 * @param size
	 *            the number of tasks to create.
	 */
	public static void populate(Queue queue, int size) {
		for (int i = 0; i < size; i++) {
			queue.add(QueueTestUtil.createTaskWithParameters());
		}
	}

}
