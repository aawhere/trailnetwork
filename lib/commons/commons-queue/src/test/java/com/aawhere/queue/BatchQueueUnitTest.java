/**
 * 
 */
package com.aawhere.queue;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class BatchQueueUnitTest {

	private Integer batchSize = 3;
	private BatchQueue batchQueue;
	private Queue queue;

	@Before
	public void setUp() throws Exception {
		setUp(QueueTestUtil.getQueueFactory().getQueue("junk"));
	}

	public void setUp(Queue queue) {
		this.queue = queue;
		this.batchQueue = new BatchQueue.Builder().setQueue(queue).setBatchSize(batchSize).setAsynch(false).build();
	}

	@Test
	public void testLessThanSingleBatch() {
		assertFirstCouple();
		assertFinish(2);
		assertEquals(1, this.batchQueue.getNumberOfBatches().intValue());

	}

	/**
	 * 
	 */
	private void assertFinish(Integer expectedSize) {
		Integer total = this.batchQueue.finish();
		assertEquals("total sent from batch did not match", expectedSize, total);
		assertEquals("queue received wrong amount", expectedSize, queueSize(queue));
	}

	/**
	 * Provided for other testers to use in case size for testing is not correct.
	 * 
	 * @param queue
	 * @return
	 */
	protected Integer queueSize(Queue queue) {
		return queue.size();
	}

	@Test(expected = NullPointerException.class)
	public void testAddAfterFinish() {
		assertFinish(0);
		addTask();
	}

	@Test
	public void testLargeBatches() {
		this.batchQueue = BatchQueue.create().setBatchSize(100).setAsynch(false).setQueue(queue).build();
		int count = 327;
		for (int i = 0; i < count; i++) {
			addTask();
		}
		assertFinish(count);
	}

	@Test
	public void testMaxSizeExceeded() {
		BatchQueue.create().setBatchSize(Queue.MAX_BATCH_SIZE + 1);
	}

	/**
	 * 
	 */
	private void assertFirstCouple() {
		assertEquals("batches should start at zero", 0, this.batchQueue.getNumberOfBatches().intValue());
		addTask();
		addTask();

		assertEquals("should be no batches sent", 0, this.batchQueue.getNumberOfBatches().intValue());
	}

	/**
	 * 
	 */
	private void addTask() {
		this.batchQueue.add(QueueTestUtil.createTaskWithParameters(QueueTestUtil.getRandomEndPoint()));
	}

	@Test
	public void testSingleBatch() {
		assertFirstCouple();
		addTask();
		assertEquals("should be the first batch sent", 1, this.batchQueue.getNumberOfBatches().intValue());

		assertFinish(3);
		assertEquals("finish sent the second batch", 2, this.batchQueue.getNumberOfBatches().intValue());
	}
}
