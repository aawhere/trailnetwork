/**
 * 
 */
package com.aawhere.queue;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class BatchQueuesUnitTest {

	static enum Key {
		A, B, C
	};

	@Test
	public void testAddingMultipleTasksToMultipleQueues() {
		BatchQueues<Key> batchQueues = new BatchQueues.Builder<Key>().queueProvider(queueProvider()).build();

		batchQueues.add(QueueTestUtil.createTaskWithHeaders(), Key.A);
		batchQueues.add(QueueTestUtil.createTaskWithHeaders(), Key.A);
		batchQueues.add(QueueTestUtil.createTaskWithHeaders(), Key.A);
		batchQueues.add(QueueTestUtil.createTaskWithHeaders(), Key.B);
		batchQueues.add(QueueTestUtil.createTaskWithHeaders(), Key.B);
		batchQueues.add(QueueTestUtil.createTaskWithHeaders(), Key.C);
		Map<Key, Integer> finish = batchQueues.finishEach();
		assertEquals(Key.A + " is not correct", 3, finish.get(Key.A).intValue());
		assertEquals(Key.B + " is not correct", 2, finish.get(Key.B).intValue());
		assertEquals(Key.C + " is not correct", 1, finish.get(Key.C).intValue());
		assertEquals("total is not correct", 6, batchQueues.finishAll().intValue());
	}

	public Function<Key, Queue> queueProvider() {
		return new Function<BatchQueuesUnitTest.Key, Queue>() {

			@Override
			public Queue apply(Key input) {
				return new QueueTestUtil.TestQueue(input.name());
			}
		};
	}
}
