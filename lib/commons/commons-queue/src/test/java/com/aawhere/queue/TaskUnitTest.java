/**
 *
 */
package com.aawhere.queue;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.io.IoException;
import com.aawhere.io.Marshal;
import com.aawhere.queue.Task.RepeatFrequency;
import com.aawhere.test.TestUtil;
import com.aawhere.test.category.UnitTest;

import com.google.common.net.MediaType;

/**
 * @author Brian Chapman
 * 
 */
@Category(UnitTest.class)
public class TaskUnitTest {
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testTaskBinaryPayload() throws NumberFormatException, IoException {
		Task task = QueueTestUtil.createTaskWithBinaryPayload();

		byte[] marshaled = task.getBinaryPayload();
		List<String> payloadResult = Marshal.unMarshal(new ByteArrayInputStream(marshaled));
		assertEquals(QueueTestUtil.DEFAULT_PAYLOAD, payloadResult);
	}

	@Test
	public void testTaskStringPayload() throws NumberFormatException, IoException {
		Task task = QueueTestUtil.createTaskWithStringPayload(	QueueTestUtil.DEFAULT_STRING_PAYLOAD,
																MediaType.PLAIN_TEXT_UTF_8);
		assertTrue(task.hasStringPayload());
		assertTrue(task.hasPayload());
		assertTrue(!task.hasBinaryPayload());
		assertEquals(QueueTestUtil.DEFAULT_STRING_PAYLOAD, task.getStringPayload());
	}

	@Test
	public void testRepeatFrequency() {
		Task taskWithoutRepeats = QueueTestUtil.createTaskWithParameters();
		assertFalse(taskWithoutRepeats.hasRepeatFrequencyAllowed());
		assertNull(taskWithoutRepeats.getRepeatFrequencyAllowed());
		assertTrue(taskWithoutRepeats.isRepeatsAllowed());
		RepeatFrequency repeatFrequency = RepeatFrequency.WEEKLY;
		Task taskWithRepeats = Task.clone(taskWithoutRepeats).setRepeatFrequencyAllowed(repeatFrequency).build();
		assertEquals(repeatFrequency, taskWithRepeats.getRepeatFrequencyAllowed());
		assertTrue(taskWithRepeats.hasRepeatFrequencyAllowed());
		assertFalse(taskWithRepeats.isRepeatsAllowed());
	}

	@Test
	public void testCountDownRepeatFrequency() {
		final RepeatFrequency expectedFrequency = RepeatFrequency.DAILY;
		Task task = Task.clone(QueueTestUtil.createTaskWithParameters()).setCountdown(expectedFrequency).build();
		assertEquals(expectedFrequency, task.getRepeatFrequencyAllowed());
		TestUtil.assertLessThan(DateTime.now().minusHours(11).getMillis(), task.getCountdownMillis());
	}

	@Test
	public void testTaskParameters() throws NumberFormatException, IoException {
		Task task = QueueTestUtil.createTaskWithParameters();

		assertEquals(QueueTestUtil.getEndPoint(), task.getEndPoint());
		assertEquals(2, task.getParams().length);
	}

	@Test
	public void testRepeats() {
		assertFalse(Task.create().setAllowRepeats(false).build().isRepeatsAllowed());
		assertTrue(Task.create().setAllowRepeats(true).build().isRepeatsAllowed());
	}

	@Test
	public void testQueueFactoryNames() {
		QueueFactory queueFactory = QueueTestUtil.getQueueFactory();
		String queueName = "junk";
		queueFactory.getQueue(queueName);
		Set<String> knownQueueNames = queueFactory.getKnownQueueNames();
		TestUtil.assertContains(queueName, knownQueueNames);
		TestUtil.assertSize(1, knownQueueNames);
		queueFactory.getQueue(queueName);
		// same name, shouldn't duplicate
		TestUtil.assertSize(1, knownQueueNames);
		queueFactory.getQueue("another");
		TestUtil.assertSize(2, knownQueueNames);
	}

	@Test
	public void testQueueSize() {
		QueueFactory queueFactory = QueueTestUtil.getQueueFactory();
		Queue queue = queueFactory.getQueue(TestUtil.generateRandomString());
		assertEquals(0, queue.size().intValue());
		queue.add(QueueTestUtil.createTaskWithParameters());
		assertEquals(1, queue.size().intValue());
		queue.add(QueueTestUtil.createTaskWithParameters());
		assertEquals(2, queue.size().intValue());

	}

	@Test(expected = QueueTimeoutException.class)
	public void testWaitForQueueTimeout() throws QueueTimeoutException {
		QueueFactory queueFactory = QueueTestUtil.getQueueFactory();
		Queue queue = queueFactory.getQueue("test");
		queue.add(QueueTestUtil.createTaskWithParameters());
		QueueUtil.waitForEmptyQueues(queueFactory, 200);

	}

	/**
	 * Poor man's concurrency test...just don't add anything and expect it to return before the
	 * timeout.
	 * 
	 * @throws QueueTimeoutException
	 */
	@Test
	public void testWaitForQueue() throws QueueTimeoutException {
		QueueFactory queueFactory = QueueTestUtil.getQueueFactory();
		queueFactory.getQueue("blah");
		// don't add anything
		// generous timeout for those slow test servers
		int timeoutMillis = 500;
		long duration = QueueUtil.waitForEmptyQueues(queueFactory, timeoutMillis);
		TestUtil.assertLessThan(timeoutMillis, duration);
	}

}
