/**
 * 
 */
package com.aawhere.retrieve;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.app.ApplicationDataIdImpl;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.RemoteDocumentNotFoundException;
import com.aawhere.doc.RemoteDocumentNotModifiedException;
import com.aawhere.doc.text.TextDocumentProducer;
import com.aawhere.io.ConnectionTimeoutException;
import com.aawhere.io.IoException;
import com.aawhere.io.UnauthorizedException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.net.HttpStatusCodeTestEndpoints;
import com.aawhere.test.category.WebClientTest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

/**
 * Basic testing to ensure The retriever is able to download web resources.
 * 
 * @author aroller
 * 
 */
@Category(WebClientTest.class)
public class WebDocumentRetrieverWebClientTest {

	private ApplicationDataIdImpl id;
	private DocumentFactory factory;

	@Before
	public void setUp() {
		id = new ApplicationDataIdImpl("junk");
		factory = new DocumentFactory.Builder().register(new TextDocumentProducer.Builder().build()).build();

	}

	@Test(expected = RemoteDocumentNotFoundException.class)
	public void testNotFound() throws MalformedURLException, BaseException {
		assertRetrieval(HttpStatusCode.NOT_FOUND);

	}

	@Test
	public void testIfModified() throws MalformedURLException, IOException, BaseException {
		String urlValue = "http://www.w3.org/Protocols/rfc2616/rfc2616.html";
		final WebResource resource = Client.create().resource(urlValue);
		final URL url = new URL(urlValue);
		URLConnection connection = url.openConnection();

		DateTime lastModified = new DateTime(connection.getLastModified());

		WebDocumentRetriever retriever = getRetriever(url);
		final int enoughMillisToMakeADifference = 100000;
		// retriever.retrieve(DocumentFormatKey.UNKNOWN,
		// lastModified.minus(enoughMillisToMakeADifference));
		// assertNotNull("document retrieved, but no document", retriever.getDocument());
		try {
			retriever.retrieve(DocumentFormatKey.TEXT, lastModified.plus(enoughMillisToMakeADifference));
			fail("shouldn't have succeded since document is out-dated");
		} catch (RemoteDocumentNotModifiedException e) {
			// this is expected
		}

	}

	@Test(expected = RemoteDocumentNotModifiedException.class)
	public void testNotUpdated() throws BaseException {
		assertRetrieval(HttpStatusCode.NOT_MODIFIED);
	}

	@Test(expected = IoException.class)
	public void testTimeout() throws BaseException, MalformedURLException {
		assertRetrieval(HttpStatusCode.GATEWAY_TIMEOUT);
	}

	@Test(expected = ConnectionTimeoutException.class)
	public void testConnectionTimeout() throws BaseException, MalformedURLException {
		URL url = new URL("http://www.google.com:81");

		WebDocumentRetriever retriever = new WebDocumentRetriever.Builder().setApplicationDataId(id).setUrl(url)
				.connectionTimeoutMillis(10).setDocumentFactory(factory).build();
		retriever.retrieve(DocumentFormatKey.TEXT);
	}

	@Test(expected = UnauthorizedException.class)
	public void testUnauthorized() throws BaseException, MalformedURLException {
		assertRetrieval(HttpStatusCode.UNAUTHORIZED);
	}

	/**
	 * @param url
	 * @throws BaseException
	 */
	private void assertRetrieval(HttpStatusCode status) throws BaseException {
		URL url = HttpStatusCodeTestEndpoints.testUrl(status);
		WebDocumentRetriever retriever = getRetriever(url);
		retriever.retrieve(DocumentFormatKey.TEXT);
	}

	/**
	 * @param url
	 * @return
	 */
	private WebDocumentRetriever getRetriever(URL url) {
		WebDocumentRetriever retriever = new WebDocumentRetriever.Builder().setApplicationDataId(id).setUrl(url)
				.setDocumentFactory(factory).build();
		return retriever;
	}

	@Test
	public void testOk() throws MalformedURLException, BaseException {
		assertRetrieval(HttpStatusCode.OK);
	}
}
