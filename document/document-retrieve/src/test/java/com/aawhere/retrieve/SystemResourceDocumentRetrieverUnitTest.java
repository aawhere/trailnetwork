/**
 *
 */
package com.aawhere.retrieve;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.doc.text.TextDocumentProducer;
import com.aawhere.io.FileNotFoundException;

/**
 * @author brian
 * 
 */
public class SystemResourceDocumentRetrieverUnitTest {

	private Resource resource1;
	private SystemResourceDocumentRetriever retriever1;
	private SystemResourceDocumentRetriever retriever2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		resource1 = RetrieveTestUtil.RESOURCE_1_GPX;
		retriever1 = RetrieveTestUtil.RETRIEVER_1_TXT;
		retriever2 = RetrieveTestUtil.RETRIEVER_1_TXT;
	}

	@Test
	public void testEquals() {
		assertEquals(retriever1.getProviderDocumentIdentifier(), retriever2.getProviderDocumentIdentifier());
		assertTrue(retriever1.equals(retriever2));
	}

	@Test
	public void testNotEquals() {
		Resource resource3 = new ClassPathResource("NotEqualsPath");
		SystemResourceDocumentRetriever retriever3 = new SystemResourceDocumentRetriever(resource3);
		assert (!resource1.equals(retriever3));
	}

	@Test
	public void testRetrieveTxt() throws FileNotFoundException, DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		DocumentFactory.getInstance().register(new TextDocumentProducer.Builder().build());
		retriever1.retrieve(DocumentFormatKey.TEXT);
		Document doc = retriever1.getDocument();
		assertNotNull(doc);
		assertEquals(RetrieveTestUtil.TEST_RESOURCE_1_TXT_CONTENTS, doc.toString());
	}

	@Test
	public void testRetrieveTxtFromTxt() throws FileNotFoundException, DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		DocumentFactory.getInstance().register(new TextDocumentProducer.Builder().build());
		Resource resource3 = new ClassPathResource(RetrieveTestUtil.TEST_RESOURCE_1_TXT_PATH);
		SystemResourceDocumentRetriever retriever3 = new SystemResourceDocumentRetriever(resource3);
		retriever3.retrieve(DocumentFormatKey.TEXT);
		Document doc = retriever3.getDocument();
		assertNotNull(doc);
		assertEquals(RetrieveTestUtil.TEST_RESOURCE_1_TXT_CONTENTS, doc.toString());
	}
}
