/**
 *
 */
package com.aawhere.retrieve;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.retrieve.crawler.SystemResourceDocumentCrawler;
import com.aawhere.retrieve.crawler.TestDocumentCrawlerListener;

/**
 * @author brian
 * 
 */
public class RetrieveTestUtil {

	public static final String TEST_RESOURCE_1_GPX_PATH = "1.gpx";
	public static final String TEST_RESOURCE_1_GPX_CONTENTS = "gpx";
	public static final Resource RESOURCE_1_GPX = new ClassPathResource(RetrieveTestUtil.TEST_RESOURCE_1_GPX_PATH);
	public static final SystemResourceDocumentRetriever RETRIEVER_1_GPX = new SystemResourceDocumentRetriever(
			RESOURCE_1_GPX);

	public static final String TEST_RESOURCE_1_TXT_PATH = "1.txt";
	public static final String TEST_RESOURCE_1_TXT_CONTENTS = "txt";
	public static final Resource RESOURCE_1_TXT = new ClassPathResource(RetrieveTestUtil.TEST_RESOURCE_1_TXT_PATH);
	public static final SystemResourceDocumentRetriever RETRIEVER_1_TXT = new SystemResourceDocumentRetriever(
			RESOURCE_1_TXT);

	public static final String TEST_RESOURCE_2_CONTENTS = "Long live the TrailNetwork!";
	public static final String TEST_RESOURCE_2_PATH = "test-document.txt";
	public static final Resource RESOURCE_TEST_DOC = new ClassPathResource(RetrieveTestUtil.TEST_RESOURCE_2_PATH);
	public static final SystemResourceDocumentRetriever RETRIEVER_TEST_DOC = new SystemResourceDocumentRetriever(
			RESOURCE_TEST_DOC);

	public static DocumentRetriever loadFile(String filename, DocumentFormatKey formatKey) {
		TestDocumentCrawlerListener<SystemResourceDocumentRetriever> listener = new TestDocumentCrawlerListener<SystemResourceDocumentRetriever>();
		new SystemResourceDocumentCrawler.Builder().addResourcePath(filename).setExpectedFormat(formatKey)
				.registerListener(listener).build();

		return listener.successes.get(0);

	}
}
