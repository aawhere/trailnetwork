/**
 *
 */
package com.aawhere.retrieve.crawler;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.aawhere.doc.Document;
import com.aawhere.doc.text.TextDocument;
import com.aawhere.io.Directory;
import com.aawhere.io.DirectoryNotFoundException;
import com.aawhere.io.DirectoryUnitTest;
import com.aawhere.io.NotADirectoryException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.FileDocumentRetriever;
import com.aawhere.test.TestUtil;

/**
 * Unit tests for {@link FileDocumentCrawler}.
 * 
 * @author roller
 * 
 */
public class FileDocumentCrawlerUnitTest {

	/**
	 * Test method for {@link com.aawhere.doc.retriever.crawler.FileDocumentCrawler#iterator()}.
	 * 
	 * @throws DirectoryNotFoundException
	 * @throws NotADirectoryException
	 * @throws IOException
	 */
	@Test
	public void testIterator() throws NotADirectoryException, DirectoryNotFoundException, IOException {
		int numberOfFiles = TestUtil.generateRandomInt(1, 20);
		Directory dir = DirectoryUnitTest.createTempDir(numberOfFiles);
		final Listener listener = new Listener();
		FileDocumentCrawler crawler = new FileDocumentCrawler.Builder().setDirectory(dir).registerListener(listener)
				.build();

		assertEquals(numberOfFiles, listener.count);
		assertEquals(crawler.getFailureCount() + crawler.getSuccessCount(), listener.count);

	}

	@Test
	public void testGetDocumentRetriever() throws NotADirectoryException, DirectoryNotFoundException {
		assertNotNull(getFileDocumentRetriever());
	}

	@Test
	public void testGetFileDocumentCrawler() throws NotADirectoryException, DirectoryNotFoundException {
		assertNotNull(getFileDocumentCrawler());
	}

	public static FileDocumentRetriever getFileDocumentRetriever() throws NotADirectoryException,
			DirectoryNotFoundException {
		FileDocumentCrawler crawler = getFileDocumentCrawler();

		return crawler.iterator().next();
	}

	public static FileDocumentCrawler getFileDocumentCrawler() throws NotADirectoryException,
			DirectoryNotFoundException {
		Directory directory = DirectoryUnitTest.creatTempDir();
		FileDocumentCrawler crawler = new FileDocumentCrawler.Builder().setDirectory(directory).build();
		return crawler;
	}

	public static class Listener
			implements DocumentCrawlerListener<FileDocumentRetriever> {

		int count = 0;

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.doc.retriever.crawler.DocumentCrawlerListener#documentRetrieved(com.aawhere
		 * .doc.retriever.DocumentRetriever, com.aawhere.doc.retriever.crawler.DocumentCrawler)
		 */
		@Override
		public void documentRetrieved(FileDocumentRetriever retriever, DocumentCrawler<FileDocumentRetriever> crawler) {
			Document document = retriever.getDocument();
			assertNotNull(document);
			assertTrue(document instanceof TextDocument);
			TextDocument textDocument = (TextDocument) document;
			TestUtil.assertContains(textDocument.getContents(), retriever.getFile().getName());
			count++;
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.doc.retriever.crawler.DocumentCrawlerListener#documentRetrievalFailed(com
		 * .aawhere.doc.retriever.DocumentRetriever, com.aawhere.lang.exception.BaseException,
		 * com.aawhere.doc.retriever.crawler.DocumentCrawler)
		 */
		@Override
		public void documentRetrievalFailed(FileDocumentRetriever retriever, BaseException reason,
				DocumentCrawler<FileDocumentRetriever> crawler) {
			count++;
		}

	}
}
