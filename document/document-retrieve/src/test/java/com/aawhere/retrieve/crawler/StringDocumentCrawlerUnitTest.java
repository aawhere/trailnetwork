/**
 *
 */
package com.aawhere.retrieve.crawler;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.text.TextDocument;
import com.aawhere.doc.text.TextDocumentProducer;
import com.aawhere.retrieve.StringDocumentRetriever;
import com.aawhere.test.TestUtil;

/**
 * @see StringDocumentCrawler
 * @see StringDocumentRetriever
 * 
 * @author Aaron Roller
 * 
 */
public class StringDocumentCrawlerUnitTest {

	@Test
	public void testStringBasics() {
		DocumentFactory.getInstance().register(new TextDocumentProducer.Builder().build());
		String expected1 = TestUtil.generateRandomString();
		String expected2 = TestUtil.generateRandomString();
		TestDocumentCrawlerListener<StringDocumentRetriever> listener = new TestDocumentCrawlerListener<StringDocumentRetriever>();
		StringDocumentCrawler crawler = new StringDocumentCrawler.Builder().addString(expected1, expected2)
				.registerListener(listener).build();
		assertEquals(0, (int) crawler.getFailureCount());
		assertEquals(2, (int) crawler.getSuccessCount());
		assertEquals(expected1, ((TextDocument) listener.successes.get(0).getDocument()).getContents());
		assertEquals(expected2, ((TextDocument) listener.successes.get(1).getDocument()).getContents());

	}
}
