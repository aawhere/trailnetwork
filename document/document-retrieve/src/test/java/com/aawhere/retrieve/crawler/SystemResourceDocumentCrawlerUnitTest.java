/**
 *
 */
package com.aawhere.retrieve.crawler;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.text.TextDocument;
import com.aawhere.doc.text.TextDocumentProducer;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.retrieve.RetrieveTestUtil;
import com.aawhere.retrieve.SystemResourceDocumentRetriever;
import com.aawhere.test.TestUtil;

/**
 * Tests {@link SystemResourceDocumentCrawler} and {@link SystemResourceDocumentRetriever}.
 * 
 * @author Aaron Roller
 * 
 */
public class SystemResourceDocumentCrawlerUnitTest {

	@Before
	public void setUp() {
		DocumentFactory.getInstance().register(new TextDocumentProducer.Builder().build());
	}

	@Test
	public void testResource() {
		TestDocumentCrawlerListener<SystemResourceDocumentRetriever> listener = new TestDocumentCrawlerListener<SystemResourceDocumentRetriever>();
		SystemResourceDocumentCrawler crawler = SystemResourceDocumentCrawler.create()

		.addResourcePath(RetrieveTestUtil.TEST_RESOURCE_2_PATH).registerListener(listener).build();
		final Integer successes = 1;
		final Integer failures = 0;
		assertEquals(listener.failures.toString(), failures, crawler.getFailureCount());
		assertEquals(listener.successes.toString(), successes, crawler.getSuccessCount());
		DocumentRetriever retriever = listener.successes.get(0);
		TextDocument doc = (TextDocument) retriever.getDocument();
		TestUtil.assertContains(doc.getContents(), RetrieveTestUtil.TEST_RESOURCE_2_CONTENTS);
	}
}
