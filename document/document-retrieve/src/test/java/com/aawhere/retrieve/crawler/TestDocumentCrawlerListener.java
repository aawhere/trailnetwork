/**
 *
 */
package com.aawhere.retrieve.crawler;

import static org.junit.Assert.*;

import java.util.ArrayList;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.DocumentRetriever;

/**
 * @author Aaron Roller
 * 
 */
public class TestDocumentCrawlerListener<R extends DocumentRetriever>
		implements DocumentCrawlerListener<R> {

	public ArrayList<R> successes = new ArrayList<R>();
	public ArrayList<R> failures = new ArrayList<R>();

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.doc.retriever.crawler.DocumentCrawlerListener#documentRetrieved(com.aawhere.doc
	 * .retriever.DocumentRetriever, com.aawhere.doc.retriever.crawler.DocumentCrawler)
	 */
	@Override
	public void documentRetrieved(R retriever, DocumentCrawler<R> crawler) {
		assertNotNull(retriever);
		assertNotNull(crawler);
		assertNotNull(retriever.getDocument());
		assertNotNull(retriever.getProviderDocumentIdentifier());
		successes.add(retriever);
		assertCount(crawler);
	}

	/**
	 * @param crawler
	 */
	private void assertCount(DocumentCrawler<R> crawler) {
		assertEquals(successes.size(), (int) crawler.getSuccessCount());
		assertEquals(failures.size(), (int) crawler.getFailureCount());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.doc.retriever.crawler.DocumentCrawlerListener#documentRetrievalFailed(com.aawhere
	 * .doc.retriever.DocumentRetriever, com.aawhere.lang.exception.BaseException,
	 * com.aawhere.doc.retriever.crawler.DocumentCrawler)
	 */
	@Override
	public void documentRetrievalFailed(R retriever, BaseException reason, DocumentCrawler<R> crawler) {
		failures.add(retriever);
		assertNotNull(retriever);
		assertNotNull(reason);
		assertNotNull(crawler);
		assertCount(crawler);
	}

}
