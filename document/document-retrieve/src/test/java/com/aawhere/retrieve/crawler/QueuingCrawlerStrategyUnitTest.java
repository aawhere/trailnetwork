/**
 *
 */
package com.aawhere.retrieve.crawler;

import static org.junit.Assert.assertEquals;

import java.net.URISyntaxException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.aawhere.queue.EndPoint;
import com.aawhere.queue.Queue;
import com.aawhere.queue.Task;
import com.aawhere.retrieve.StringDocumentRetriever;

/**
 * @author brian
 * 
 */
public class QueuingCrawlerStrategyUnitTest {

	public static final String TEST_STRING = "AString";
	public static final String[] TEST_CRAWL_STRINGS = { "AString1", "AString2", "AString3" };

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = NullPointerException.class)
	public void testBuildDoesntValidate() {
		QueuingCrawlerStrategy.create().build();
	}

	@Test
	public void testBuild() throws URISyntaxException {
		QueuingCrawlerStrategy.create().setEndPoint(EndPoint.create().setEndPoint(TEST_STRING).build())
				.setQueue(Mockito.mock(Queue.class)).build();
	}

	@Test
	public void testCrawl() throws URISyntaxException {
		Queue queue = Mockito.mock(Queue.class);
		QueuingCrawlerStrategy strategy = QueuingCrawlerStrategy.create()
				.setEndPoint(EndPoint.create().setEndPoint(TEST_STRING).build()).setQueue(queue).build();
		CollectingDocumentCrawlerListener<StringDocumentRetriever> listener = new CollectingDocumentCrawlerListener<StringDocumentRetriever>();
		new StringDocumentCrawler.Builder().setCrawlerStrategy(strategy).addString(TEST_CRAWL_STRINGS)
				.registerListener(listener).build();
		Mockito.verify(queue, Mockito.times(3)).add(Mockito.any(Task.class));
		assertEquals(3, listener.getSuccesses().size());
	}
}
