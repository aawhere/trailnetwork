/**
 *
 */
package com.aawhere.retrieve.crawler;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.text.TextDocumentProducer;
import com.aawhere.retrieve.StringDocumentRetriever;

/**
 * @author brian
 * 
 */
public class RetrievingCrawlerStrategyUnitTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCrawl() {
		DocumentFactory.getInstance().register(new TextDocumentProducer.Builder().build());
		RetrievingCrawlerStrategy strategy = new RetrievingCrawlerStrategy.Builder().build();
		TestDocumentCrawlerListener<StringDocumentRetriever> listener = new TestDocumentCrawlerListener<StringDocumentRetriever>();
		new StringDocumentCrawler.Builder().setCrawlerStrategy(strategy)
				.addString(QueuingCrawlerStrategyUnitTest.TEST_CRAWL_STRINGS).setExpectedFormat(DocumentFormatKey.TEXT)
				.registerListener(listener).build();
		assertEquals(3, listener.successes.size());
	}
}
