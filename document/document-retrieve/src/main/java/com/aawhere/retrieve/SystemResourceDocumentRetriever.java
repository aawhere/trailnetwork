/**
 *
 */
package com.aawhere.retrieve;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.io.FileNotFoundException;

/**
 * Retrieves documents packaged with the deployment. Assumes that files with basenames of similar
 * extensions exist. For example given a resource with path "a/b/c.gpx", then
 * retrieve(DocumentFormatKey.axm_1_0) would retrieve the file "a/b/c.axm".
 * 
 * @author Aaron Roller
 * 
 */
public class SystemResourceDocumentRetriever
		implements DocumentRetriever {

	private static final long serialVersionUID = 2002395049868065260L;
	private Document document;
	private transient Resource resource;
	private InputStream inputStream;;

	/**
	 * @param inputStream
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws FileNotFoundException
	 */
	public SystemResourceDocumentRetriever(Resource resource) {
		this.resource = resource;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.DocumentRetriever#getDocument()
	 */
	@Override
	public Document getDocument() {
		return this.document;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.DocumentRetriever#getProviderDocumentIdentifier ()
	 */
	@Override
	public String getProviderDocumentIdentifier() {
		String filename;
		filename = resource.getFilename();
		return FilenameUtils.getFullPath(filename) + FilenameUtils.getBaseName(filename);
	}

	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		out.writeObject(resource.getURI().toString());
		out.close();
	}

	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		String path = "";
		try {
			path = (String) in.readObject();
		} catch (EOFException e) {
			e.printStackTrace();
		}
		in.close();
		resource = new UrlResource(path);
	}

	protected Resource getResource() {
		return this.resource;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.InputStreamDocumentRetriever#retrieve()
	 */
	@Override
	public void retrieve(DocumentFormatKey expectedFormat) throws FileNotFoundException, DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		try {
			inputStream = resource.getInputStream();

			this.document = DocumentFactory.getInstance().getDocument(expectedFormat, inputStream);
		} catch (IOException e) {
			throw new FileNotFoundException(resource.getFilename());
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SystemResourceDocumentRetriever == false) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		SystemResourceDocumentRetriever rhs = (SystemResourceDocumentRetriever) obj;
		try {
			return new EqualsBuilder().append(this.resource.getURI(), rhs.resource.getURI()).isEquals();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getProviderDocumentIdentifier()).hashCode();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.retrieve.DocumentRetriever#getOrigionalData()
	 */
	@Override
	public byte[] getOrigionalData() {
		try {
			return IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
