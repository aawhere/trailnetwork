/**
 *
 */
package com.aawhere.retrieve.crawler;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.collections.set.ListOrderedSet;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.aawhere.retrieve.SystemResourceDocumentRetriever;

/**
 * Crawls files that are distributed as part of the resources in the deployed jar. This almost seems
 * like a test class, but young applications may choose to test documents distributed with the
 * deployment.
 * 
 * @author Aaron Roller
 * 
 */
public class SystemResourceDocumentCrawler
		extends DocumentCrawler<SystemResourceDocumentRetriever> {

	/** Use {@link Builder} to construct SystemResourceDocumentCrawler */
	private SystemResourceDocumentCrawler() {
	}

	private ListOrderedSet resources = new ListOrderedSet();

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Iterator<SystemResourceDocumentRetriever> iterator() {
		final Iterator<ClassPathResource> pathsIterator = this.resources.iterator();

		return new Iterator<SystemResourceDocumentRetriever>() {

			@Override
			public void remove() {
				pathsIterator.remove();
			}

			@Override
			public SystemResourceDocumentRetriever next() {
				return new SystemResourceDocumentRetriever(pathsIterator.next());
			}

			@Override
			public boolean hasNext() {
				return pathsIterator.hasNext();
			}
		};
	}

	/**
	 * Used to construct all instances of SystemResourceDocumentCrawler.
	 */
	public static class Builder
			extends DocumentCrawler.Builder<SystemResourceDocumentCrawler, SystemResourceDocumentRetriever, Builder> {

		public Builder() {
			super(new SystemResourceDocumentCrawler());
		}

		public Builder addResourcePath(String... paths) {
			PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
			for (String path : paths) {
				Resource resource = resolver.getResource(path);
				building.resources.add(resource);
			}
			return this;
		}

		public Builder addResources(Collection<Resource> resources) {
			building.resources.addAll(resources);
			return this;
		}

		public Builder addResource(Resource resource) {
			building.resources.add(resource);
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

}
