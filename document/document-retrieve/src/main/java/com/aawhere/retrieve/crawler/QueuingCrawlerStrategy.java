/**
 *
 */
package com.aawhere.retrieve.crawler;

import com.aawhere.io.Marshal;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.queue.EndPoint;
import com.aawhere.queue.Queue;
import com.aawhere.queue.Task;
import com.aawhere.retrieve.DocumentRetriever;

import com.google.common.net.MediaType;

/**
 * Uses an Aynchronous Queue to notify ??? to receive the {@link DocumentRetriever}.
 * 
 * @author Brian Chapman
 * 
 */
public class QueuingCrawlerStrategy
		implements CrawlerStrategy {

	protected EndPoint endPoint;
	protected Queue queue;

	@Override
	public <R extends DocumentRetriever> void crawl(DocumentCrawler<R> documentCrawler) {
		for (R retriever : documentCrawler) {
			Task task;
			byte[] bytes = Marshal.marshal(retriever);
			task = Task.create().setEndPoint(endPoint).setPayload(bytes, MediaType.ANY_APPLICATION_TYPE).build();
			queue.add(task);
			documentCrawler.notifyListenersOfSuccess(retriever);
		}
	}

	public static class Builder
			extends BaseBuilder<QueuingCrawlerStrategy, Builder> {

		/**
		 * @param building
		 */
		protected Builder() {
			super(new QueuingCrawlerStrategy());
		}

	}

	public static class BaseBuilder<StrategyT extends QueuingCrawlerStrategy, BuilderT extends BaseBuilder<StrategyT, BuilderT>>
			extends AbstractObjectBuilder<StrategyT, BuilderT> {

		/**
		 * @param building
		 */
		protected BaseBuilder(StrategyT strategy) {
			super(strategy);
		}

		public BuilderT setEndPoint(EndPoint endPoint) {
			building.endPoint = endPoint;
			return dis;
		}

		public BuilderT setQueue(Queue queue) {
			building.queue = queue;
			return dis;
		}

		@Override
		protected void validate() {
			Assertion.assertNotNull(building.queue);
			Assertion.assertNotNull(building.endPoint);
		}
	}

	public static Builder create() {
		return new Builder();
	}
}
