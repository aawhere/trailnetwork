/**
 * 
 */
package com.aawhere.retrieve;

import org.joda.time.DateTime;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.exception.BaseException;

/**
 * A {@link DocumentRetriever} that supports retrieving modified documents from the remote location.
 * 
 * Modified documents are those that can be updated for whatever reason and the remote server has
 * the capability of reporting if it is updated.
 * 
 * @author aroller
 * 
 */
public interface ModifiableDocumentRetriever
		extends DocumentRetriever {

	/**
	 * Special method required by {@link ModifiableDocumentRetriever}s to allow retrieval if
	 * modified.
	 * 
	 * @param format
	 * @param lastKnownUpdate
	 */
	void retrieve(DocumentFormatKey format, DateTime lastKnownUpdate) throws BaseException;
}
