/**
 *
 */
package com.aawhere.retrieve.crawler;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.NotFileFilter;

import com.aawhere.io.Directory;
import com.aawhere.lang.Assertion;
import com.aawhere.retrieve.FileDocumentRetriever;

/**
 * Looks through directories and provides {@link FileDocumentRetriever}s for the files matching the
 * criteria given.
 * 
 * @author roller
 * 
 */
public class FileDocumentCrawler
		extends DocumentCrawler<FileDocumentRetriever> {

	public static class Builder
			extends DocumentCrawler.Builder<FileDocumentCrawler, FileDocumentRetriever, Builder> {

		public Builder() {
			super(new FileDocumentCrawler());
		}

		public Builder setDirectory(Directory directory) {
			building.directory = directory;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.directory);
		}

	}

	private Directory directory;

	protected FileDocumentCrawler() {
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<FileDocumentRetriever> iterator() {

		FileFilter filter = new NotFileFilter(DirectoryFileFilter.DIRECTORY);
		File[] files = directory.listFiles(filter);
		List<File> fileList = Arrays.asList(files);
		final Iterator<File> filesIterator = fileList.iterator();

		return new Iterator<FileDocumentRetriever>() {

			@Override
			public boolean hasNext() {

				return filesIterator.hasNext();
			}

			@Override
			public FileDocumentRetriever next() {

				return new FileDocumentRetriever(filesIterator.next());
			}

			@Override
			public void remove() {
				filesIterator.remove();

			}
		};

	}
}
