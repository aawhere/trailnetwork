/**
 * 
 */
package com.aawhere.retrieve;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map.Entry;

import javax.annotation.Nonnull;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentUtil;
import com.aawhere.doc.RemoteDocumentNotFoundException;
import com.aawhere.doc.RemoteDocumentNotModifiableException;
import com.aawhere.doc.RemoteDocumentNotModifiedException;
import com.aawhere.io.ConnectionTimeoutException;
import com.aawhere.io.zip.GzipCompression;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.net.NetUtils;

import com.google.api.client.http.HttpHeaders;

/**
 * A streamlined downloader that specifically ignores the format and puts it right into the database
 * in it's original form.
 * 
 * This is intended to speed up downloads and avoid duplicate processing of large files in the
 * download process.
 * 
 * @author aroller
 * 
 */
public class WebDocumentRetriever
		implements ModifiableDocumentRetriever {

	/**
	 * 
	 */
	public static final int DEFAULT_CONNECTION_TIMEOUT = 15000;
	private static final long serialVersionUID = -4491311812054195528L;

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Used to construct all instances of WebDocumentRetriever.
	 */
	public static class Builder
			extends ObjectBuilder<WebDocumentRetriever> {

		public Builder() {
			super(new WebDocumentRetriever());
		}

		public Builder setApplicationDataId(ApplicationDataId id) {
			building.applicationDataId = id;
			return this;
		}

		public Builder setUrl(URL url) {
			building.url = url;
			return this;
		}

		public Builder connectionTimeoutMillis(Integer millis) {
			building.connectionTimeoutMillis = millis;
			return this;
		}

		/**
		 * @param documentFactory
		 * @return
		 */
		public Builder setDocumentFactory(DocumentFactory documentFactory) {
			building.documentFactory = documentFactory;
			return this;
		}

		public Builder setHeaders(HttpHeaders headers) {
			building.headers = headers;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull("documentFactory", building.documentFactory);
			Assertion.assertNotNull("url", building.url);
			Assertion.assertNotNull("applicationDataId", building.applicationDataId);
			super.validate();
		}

	}// end Builder

	/** Use {@link Builder} to construct WebDocumentRetriever */
	private WebDocumentRetriever() {
	}

	private URL url;
	private ApplicationDataId applicationDataId;
	private Document document;
	private DocumentFactory documentFactory;
	private int connectionTimeoutMillis = DEFAULT_CONNECTION_TIMEOUT;
	private byte[] bytes;
	private HttpHeaders headers;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.retrieve.DocumentRetriever#retrieve(com.aawhere.doc.DocumentFormatKey)
	 */
	@Override
	public void retrieve(DocumentFormatKey expectedFormat) throws BaseException {
		retrieve(expectedFormat, null);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.retrieve.ModifiableDocumentRetriever#retreive(com.aawhere.doc.
	 * DocumentFormatKey, org.joda.time.DateTime)
	 */
	@Override
	public void retrieve(DocumentFormatKey expectedFormat, DateTime lastModified) throws BaseException {
		try {
			long start = System.currentTimeMillis();

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(connectionTimeoutMillis);
			if (this.headers != null) {
				for (Entry<String, Object> header : this.headers.entrySet()) {

					final Object value = header.getValue();
					if (value instanceof Iterable) {
						Iterable<Object> values = (Iterable<Object>) value;
						for (Object repeatValue : values) {
							connection.setRequestProperty(header.getKey(), String.valueOf(repeatValue));
						}
					} else {
						connection.setRequestProperty(header.getKey(), String.valueOf(value));
					}
				}
			}

			// TODO: set an upper limit connection.setFixedLengthStreamingMode();
			if (lastModified != null) {
				connection.setIfModifiedSince(lastModified.getMillis());
			}
			connection.connect();
			int responseCode = connection.getResponseCode();

			if (HttpStatusCode.isOk(responseCode)) {

				// this is a double-check to make sure the server has implemented
				// Last-Modified/If-Modified-Since
				// correctly. It must be done after the connection was made.
				if (lastModified != null) {

					if (!NetUtils.lastModifiedAvailable(connection)) {
						throw new RemoteDocumentNotModifiableException(applicationDataId);
					} else {
						DateTime lastModifiedResponse = NetUtils.lastModified(connection);
						if (lastModifiedResponse.isBefore(lastModified)) {
							throw new RemoteDocumentNotModifiedException(applicationDataId.toString());
						}
					}
				}

				InputStream is = null;
				try {
					is = connection.getInputStream();
					// this may not be the most efficient way. Consider passing the stream to the
					// document
					// producer
					bytes = IOUtils.toByteArray(is);
					// although we could trust the response header, we already have bytes so let's
					// just inspect ourselves
					if (GzipCompression.isCompressed(bytes)) {
						bytes = new GzipCompression.Builder().build().uncompress(bytes);
					}
					this.document = documentFactory.getDocument(expectedFormat, bytes);
				} finally {
					IOUtils.closeQuietly(is);
					connection.disconnect();
				}
				long finish = System.currentTimeMillis() - start;
				if (finish > 2000) {
					LoggerFactory.getLogger(getClass()).warning(finish + " ms to retrieve " + url);
				}
			} else {
				throw DocumentUtil.bestException(HttpStatusCode.valueOf(responseCode), url.toString());
			}
		} catch (java.net.SocketTimeoutException timeoutException) {
			throw new ConnectionTimeoutException(new Duration(connectionTimeoutMillis), url, timeoutException);
		} catch (IOException e) {
			if (e instanceof java.io.FileNotFoundException) {
				throw new RemoteDocumentNotFoundException(this.url);
			} else {
				// TODO:handle the errors better
				throw new com.aawhere.io.IoException(e);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.retrieve.DocumentRetriever#getProviderDocumentIdentifier()
	 */
	@Override
	@Nonnull
	public String getProviderDocumentIdentifier() {
		return this.applicationDataId.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.retrieve.DocumentRetriever#getDocument()
	 */
	@Override
	@Nonnull
	public Document getDocument() {

		return this.document;
	}

	@Override
	public byte[] getOrigionalData() {
		return this.bytes;
	}

}
