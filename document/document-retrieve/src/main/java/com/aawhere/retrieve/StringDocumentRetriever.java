/**
 *
 */
package com.aawhere.retrieve;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.crawler.StringDocumentCrawler;

/**
 * Pretty much used for testing, this is a retriever that just gets a string and passes it on. Works
 * with the {@link StringDocumentCrawler}.
 * 
 * This can support fast Unit Tests or provide a user interface that accepts Text and processes
 * directly as a string (no fancy resources,files or streams).
 * 
 * @author Aaron Roller
 * 
 */
public class StringDocumentRetriever
		implements DocumentRetriever {

	private static final long serialVersionUID = -8212786090784333851L;
	private String string;
	private Document document;
	private BaseException failureCause;

	/**
	 *
	 */
	public StringDocumentRetriever(String string) {
		this.string = string;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.DocumentRetriever#retrieve()
	 */
	@Override
	public void retrieve(DocumentFormatKey expectedFormat) throws BaseException {
		try {
			this.document = DocumentFactory.getInstance().getDocument(expectedFormat, string);
		} catch (BaseException e) {
			this.failureCause = e;
			throw (e);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.DocumentRetriever#getProviderDocumentIdentifier()
	 */
	@Override
	public String getProviderDocumentIdentifier() {
		return string;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.DocumentRetriever#getDocument()
	 */
	@Override
	public Document getDocument() {
		return this.document;
	}

	/**
	 * @return the failureCause
	 */
	public BaseException getFailureCause() {
		return this.failureCause;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result;
		if (this.failureCause != null) {
			result = this.failureCause.toString();
		} else {
			result = super.toString();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.retrieve.DocumentRetriever#getOrigionalData()
	 */
	@Override
	public Object getOrigionalData() {
		return getDocument().toByteArray();
	}
}
