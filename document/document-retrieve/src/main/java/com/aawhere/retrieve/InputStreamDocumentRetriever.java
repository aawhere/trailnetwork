/**
 *
 */
package com.aawhere.retrieve;

import java.io.InputStream;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.exception.BaseException;

/**
 * The common implmentation for {@link DocumentRetriever}.
 * 
 * Provides access to the {@link DocumentFactory} by using common resources like {@link InputStream}
 * .
 * 
 * @author roller
 * 
 */
abstract public class InputStreamDocumentRetriever
		implements DocumentRetriever {

	private static final long serialVersionUID = -2716197792307456190L;
	private Document document;
	private InputStream inputStream;

	public InputStreamDocumentRetriever(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.DocumentRetriever#retrieve()
	 */
	@Override
	public void retrieve(DocumentFormatKey expectedFormat) throws BaseException {
		this.document = DocumentFactory.getInstance().getDocument(expectedFormat, this.inputStream);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.DocumentRetriever#getDocument()
	 */
	@Override
	public Document getDocument() {
		return this.document;
	}

}
