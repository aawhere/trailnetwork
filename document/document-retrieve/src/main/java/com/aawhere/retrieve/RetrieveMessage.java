/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Oct 30, 2010
commons-lang : com.aawhere.io.IoMessages.java
 */
package com.aawhere.retrieve;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;

/**
 * Messages for the entire com.aawhere.retrieve.
 * 
 * @author Brian Chapman
 * 
 */
public enum RetrieveMessage implements Message {

	/** @see FormatNotRegisteredException */
	NOT_REGISTERED("The DocumentFormatKey {KEY} is not registered.", Param.KEY), CRAWLED(
			"Sucessfully crawled {CRAWL_SUCCESS_COUNT} documents. Failed to crawl {CRAWL_FAILURE_COUNT} documents.",
			Param.CRAWL_SUCCESS_COUNT,
			Param.CRAWL_FAILURE_COUNT);

	private com.aawhere.util.rb.ParamKey[] parameterKeys;
	private String message;

	private RetrieveMessage(String message, com.aawhere.util.rb.ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public com.aawhere.util.rb.ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements com.aawhere.util.rb.ParamKey {
		/** the {@link DocumentFormatKey}. */
		KEY, CRAWL_SUCCESS_COUNT, CRAWL_FAILURE_COUNT;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
