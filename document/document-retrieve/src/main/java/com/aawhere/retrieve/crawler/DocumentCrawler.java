/**
 *
 */
package com.aawhere.retrieve.crawler;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.retrieve.log.RetrieverLogger;

/**
 * Common implementations for all those {@link DocumentCrawler}s that are crawling their specific
 * protocol or media.
 * 
 * @see CrawlerStrategy
 * 
 * @author roller
 * 
 */
abstract public class DocumentCrawler<R extends DocumentRetriever>
		implements Iterable<R> {

	/** Use {@link Builder} to construct DocumentCrawler */
	protected DocumentCrawler() {
	}

	private Integer successCount = 0;
	private Integer failureCount = 0;
	private List<DocumentCrawlerListener<R>> listeners = new ArrayList<DocumentCrawlerListener<R>>();
	private DocumentFormatKey expectedFormat = DocumentFormatKey.UNKNOWN;
	private CrawlerStrategy crawlerStrategy = new RetrievingCrawlerStrategy.Builder().build();
	private RetrieverLogger<R> logger;

	/**
	 * Called during building. Use {@link CrawlerStrategy} if you want to change the behavior of
	 * crawling.
	 * 
	 */
	private void crawl() {
		crawlerStrategy.crawl(this);
	}

	/**
	 * @param retriever
	 */
	protected void notifyListenersOfFailure(@Nonnull R retriever, @Nonnull BaseException reason) {
		failureCount++;
		for (DocumentCrawlerListener<R> listener : this.listeners) {
			listener.documentRetrievalFailed(retriever, reason, this);
		}

	}

	protected void notifyListenersOfSuccess(@Nonnull R retriever) {
		successCount++;
		for (DocumentCrawlerListener<R> listener : this.listeners) {
			listener.documentRetrieved(retriever, this);
		}
	}

	/**
	 * Used to report details about the successes and failures. You must register the logger during
	 * building for participation.
	 * 
	 * @see Builder#registerLogger()
	 * 
	 * @return the logger
	 */
	public RetrieverLogger<R> getLogger() {
		return this.logger;
	}

	/**
	 * @return the failureCount
	 */
	@Nonnull
	public Integer getFailureCount() {
		return this.failureCount;
	}

	/**
	 * @return the successCount
	 */
	@Nonnull
	public Integer getSuccessCount() {
		return this.successCount;
	}

	protected DocumentFormatKey getExpectedFormat() {
		return this.expectedFormat;
	}

	/**
	 * Used to construct all instances of DocumentCrawler.
	 */
	public static class Builder<C extends DocumentCrawler<R>, R extends DocumentRetriever, B extends Builder<C, R, B>>
			extends AbstractObjectBuilder<C, B> {

		public Builder(@Nonnull C crawler) {
			super(crawler);
		}

		public B registerListener(DocumentCrawlerListener<R> listener) {
			((DocumentCrawler<R>) building).listeners.add(listener);
			return dis;
		}

		public B registerLogger() {
			((DocumentCrawler<R>) building).logger = new RetrieverLogger.Builder<R>().build();
			registerListener(((DocumentCrawler<R>) building).logger);
			return dis;
		}

		public B setExpectedFormat(DocumentFormatKey format) {
			((DocumentCrawler<R>) building).expectedFormat = format;
			return dis;
		}

		public B setCrawlerStrategy(CrawlerStrategy strategy) {
			((DocumentCrawler<R>) building).crawlerStrategy = strategy;
			return dis;
		}

		@Override
		protected void validate() {
			Assertion.assertNotNull("crawlerStrategy", ((DocumentCrawler<R>) building).crawlerStrategy);
		}

		/**
		 * Called during {@link #build()} before crawling begins for implementations to prepare for
		 * building since {@link #build()} is final.
		 */
		protected void prepareBuild() {

		}

		/**
		 * Final because this kicks off the crawling. Any overriding would not be called until after
		 * crawling. Override {@link #prepareBuild()} for any building activities before crawling.
		 * 
		 */
		@Override
		@Nonnull
		final public C build() {
			prepareBuild();
			C built = super.build();

			((DocumentCrawler<R>) built).crawl();
			return built;
		}

	}// end Builder
}
