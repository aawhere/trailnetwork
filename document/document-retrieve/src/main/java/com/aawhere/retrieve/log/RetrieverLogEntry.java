/**
 *
 */
package com.aawhere.retrieve.log;

import com.aawhere.doc.Document;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.DocumentRetriever;

/**
 * One to one with an attempt to retrieve a {@link Document} from a {@link DocumentRetriever}.
 * 
 * @author roller
 * 
 */
public class RetrieverLogEntry {

	private String providerDocumentIdentifier;

	private BaseException cause;

	/**
	 * @return the cause
	 */
	public BaseException getCause() {
		return cause;
	}

	/**
	 * Use {@link Builder}.
	 */
	private RetrieverLogEntry() {
	}

	/**
	 * @return the providerDocumentIdentifier
	 */
	public String getProviderDocumentIdentifier() {
		return providerDocumentIdentifier;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder result = new StringBuilder().append(providerDocumentIdentifier).append(" : ");
		if (this.cause != null) {
			result.append(cause.getMessage());
		}
		return result.toString();
	}

	public static class Builder
			extends ObjectBuilder<RetrieverLogEntry> {

		/**
		 * @param building
		 */
		protected Builder() {
			super(new RetrieverLogEntry());
		}

		/**
		 * @param providerDocumentIdentifier
		 *            the providerDocumentIdentifier to set
		 */
		public Builder setProviderDocumentIdentifier(String providerDocumentIdentifier) {
			building.providerDocumentIdentifier = providerDocumentIdentifier;
			return this;
		}

		/**
		 * @param cause
		 * @return
		 */
		public Builder setCause(BaseException cause) {

			building.cause = cause;
			return this;
		}
	}
}
