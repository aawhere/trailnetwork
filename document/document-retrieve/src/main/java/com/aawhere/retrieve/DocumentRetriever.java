/**
 *
 */
package com.aawhere.retrieve;

import java.io.Serializable;

import javax.annotation.Nonnull;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.exception.BaseException;

/**
 * A tool that communicates with a system to provide a document.
 * 
 * @author roller
 * 
 */
public interface DocumentRetriever
		extends Serializable {

	/**
	 * Does the actual work to retrieve the document wherever it exists. Any failure must be
	 * reported from an extension of {@link BaseException} so errors may be formally reported.
	 * 
	 * @return
	 * @throws BaseException
	 */
	void retrieve(DocumentFormatKey expectedFormat) throws BaseException;

	@Nonnull
	String getProviderDocumentIdentifier();

	/**
	 * Provides the document.
	 * 
	 * @return
	 * @throws RuntimeException
	 *             if a document is requested when {@link #retrieve()} hasn't been called or has
	 *             failed.
	 */
	@Nonnull
	Document getDocument();

	/**
	 * Provides the original data retrieved.
	 * 
	 * @return
	 */
	Object getOrigionalData();

}
