/**
 *
 */
package com.aawhere.retrieve;

import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * @author brian
 * 
 */
public class FormatNotRegisteredException
		extends BaseException {

	/**
	 *
	 */
	private static final long serialVersionUID = 729409540545006767L;

	public FormatNotRegisteredException(DocumentFormatKey key) {
		this(key, null);
	}

	/**
	 *
	 */
	public FormatNotRegisteredException(DocumentFormatKey key, java.io.FileNotFoundException e) {
		super(new CompleteMessage.Builder(RetrieveMessage.NOT_REGISTERED).addParameter(	RetrieveMessage.Param.KEY,
																						key.getMediaType()).build(), e);
	}

}
