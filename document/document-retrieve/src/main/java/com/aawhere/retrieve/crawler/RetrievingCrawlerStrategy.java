/**
 *
 */
package com.aawhere.retrieve.crawler;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.DocumentRetriever;

/**
 * @author Brian Chapman
 * 
 */
public class RetrievingCrawlerStrategy
		implements CrawlerStrategy {

	public <R extends DocumentRetriever> void crawl(DocumentCrawler<R> documentCrawler) {
		for (R retriever : documentCrawler) {
			try {
				retriever.retrieve(documentCrawler.getExpectedFormat());
				documentCrawler.notifyListenersOfSuccess(retriever);
			} catch (BaseException reason) {
				documentCrawler.notifyListenersOfFailure(retriever, reason);
			}
		}
	}

	/**
	 * Used to construct all instances of RetrievingCrawlerStrategy.
	 */
	public static class Builder
			extends ObjectBuilder<RetrievingCrawlerStrategy> {

		public Builder() {
			super(new RetrievingCrawlerStrategy());
		}

	}// end Builder

	/** Use {@link Builder} to construct RetrievingCrawlerStrategy */
	private RetrievingCrawlerStrategy() {
	}

}
