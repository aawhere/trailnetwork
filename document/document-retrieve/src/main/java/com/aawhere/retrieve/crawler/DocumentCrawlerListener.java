/**
 *
 */
package com.aawhere.retrieve.crawler;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.DocumentRetriever;

/**
 * Any object interested in being notified of the success and/or failure of a
 * {@link DocumentCrawler} use of a {@link DocumentRetriever}.
 * 
 * @author Aaron Roller
 * 
 */
public interface DocumentCrawlerListener<R extends DocumentRetriever> {

	/**
	 * Notifies the implementing listener that a document was successfully retrieved and is
	 * available in the retriever.
	 * 
	 * @param retriever
	 * @param crawler
	 */
	void documentRetrieved(R retriever, DocumentCrawler<R> crawler);

	/**
	 * Notifies the implementing listener that an attempt to retrieve a document was not successful.
	 * The reason is provided.
	 * 
	 * @param retriever
	 * @param reason
	 * @param crawler
	 */
	void documentRetrievalFailed(R retriever, BaseException reason, DocumentCrawler<R> crawler);

}
