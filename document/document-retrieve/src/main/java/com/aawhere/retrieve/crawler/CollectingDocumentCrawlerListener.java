/**
 *
 */
package com.aawhere.retrieve.crawler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.DocumentRetriever;

/**
 * Does the bare minimum listening to a crawler by storing the Retrievers in the order they were
 * received separated by the successes and failures.
 * 
 * This should be used with a limited number of retrievals since the collection will grow
 * indefinitely.
 * 
 * @author Aaron Roller
 * @param <R>
 * 
 */
public class CollectingDocumentCrawlerListener<R extends DocumentRetriever>
		implements DocumentCrawlerListener<R> {
	private ArrayList<DocumentRetriever> successes = new ArrayList<DocumentRetriever>();
	private HashMap<DocumentRetriever, BaseException> failures = new HashMap<DocumentRetriever, BaseException>();

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.doc.retriever.crawler.DocumentCrawlerListener#documentRetrieved(com.aawhere.doc
	 * .retriever.DocumentRetriever, com.aawhere.doc.retriever.crawler.DocumentCrawler)
	 */
	@Override
	public void documentRetrieved(R retriever, DocumentCrawler<R> crawler) {
		this.successes.add(retriever);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.doc.retriever.crawler.DocumentCrawlerListener#documentRetrievalFailed(com.aawhere
	 * .doc.retriever.DocumentRetriever, com.aawhere.lang.exception.BaseException,
	 * com.aawhere.doc.retriever.crawler.DocumentCrawler)
	 */
	@Override
	public void documentRetrievalFailed(R retriever, BaseException reason, DocumentCrawler<R> crawler) {
		this.failures.put(retriever, reason);

	}

	public List<DocumentRetriever> getSuccesses() {
		return Collections.unmodifiableList(this.successes);
	}

	public Map<DocumentRetriever, BaseException> getFailures() {
		return Collections.unmodifiableMap(this.failures);
	}
}
