/**
 *
 */
package com.aawhere.retrieve.log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.retrieve.DocumentRetriever;
import com.aawhere.retrieve.crawler.DocumentCrawler;
import com.aawhere.retrieve.crawler.DocumentCrawlerListener;

/**
 * A structured way to log retrieval of documents.
 * 
 * @author roller
 * 
 */
public class RetrieverLogger<RetrieverT extends DocumentRetriever>
		implements DocumentCrawlerListener<RetrieverT> {

	private RetrieverLogger() {
	}

	private ArrayList<RetrieverLogEntry> successes = new ArrayList<RetrieverLogEntry>();
	private ArrayList<RetrieverLogEntry> failures = new ArrayList<RetrieverLogEntry>();

	/**
	 * Used to construct all instances of RetrieverLogger.
	 */
	public static class Builder<RetrieverT extends DocumentRetriever>
			extends ObjectBuilder<RetrieverLogger<RetrieverT>> {

		public Builder() {
			super(new RetrieverLogger<RetrieverT>());
		}

	}// end Builder

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.retrieve.crawler.DocumentCrawlerListener#documentRetrieved(com.aawhere.retrieve
	 * .DocumentRetriever, com.aawhere.retrieve.crawler.DocumentCrawler)
	 */
	@Override
	public void documentRetrieved(RetrieverT retriever, DocumentCrawler<RetrieverT> crawler) {
		RetrieverLogEntry entry = new RetrieverLogEntry.Builder()
				.setProviderDocumentIdentifier(retriever.getProviderDocumentIdentifier()).build();
		successes.add(entry);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.retrieve.crawler.DocumentCrawlerListener#documentRetrievalFailed(com.aawhere.
	 * retrieve.DocumentRetriever, com.aawhere.lang.exception.BaseException,
	 * com.aawhere.retrieve.crawler.DocumentCrawler)
	 */
	@Override
	public void
			documentRetrievalFailed(RetrieverT retriever, BaseException reason, DocumentCrawler<RetrieverT> crawler) {
		RetrieverLogEntry entry = new RetrieverLogEntry.Builder()
				.setProviderDocumentIdentifier(retriever.getProviderDocumentIdentifier()).setCause(reason).build();
		failures.add(entry);

	}

	/**
	 * @return the failures
	 */
	public List<RetrieverLogEntry> getFailures() {
		return Collections.unmodifiableList(this.failures);
	}

	/**
	 * @return the successes
	 */
	public List<RetrieverLogEntry> getSuccesses() {
		return Collections.unmodifiableList(this.successes);
	}
}
