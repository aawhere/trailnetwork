/**
 *
 */
package com.aawhere.retrieve.crawler;

import com.aawhere.retrieve.DocumentRetriever;

/**
 * Delegates the actual retrieval for a {@link DocumentCrawler} to an implementation that provides
 * the method of retrieval desired.
 * 
 * @see RetrievingCrawlerStrategy for the basic implementation which is also the default for
 *      {@link DocumentCrawler}
 * @see QueuingCrawlerStrategy for the asynchronous implementation
 * 
 * @author Brian Chapman
 * 
 */
public interface CrawlerStrategy {

	public <R extends DocumentRetriever> void crawl(DocumentCrawler<R> documentCrawler);
}
