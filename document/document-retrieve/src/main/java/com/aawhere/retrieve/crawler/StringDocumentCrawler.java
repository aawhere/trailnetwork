/**
 *
 */
package com.aawhere.retrieve.crawler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.aawhere.retrieve.StringDocumentRetriever;

/**
 * Given a bunch of strings this will produced {@link StringDocumentRetriever}s to produce
 * documents.
 * 
 * The documents could be of any format since the intention is to use this to crawl file contents
 * provided in Unit Tests or from a web user interface.
 * 
 * @author Aaron Roller
 * 
 */
public class StringDocumentCrawler
		extends DocumentCrawler<StringDocumentRetriever> {

	/**
	 * Used to construct all instances of StringDocumentCrawler.
	 */
	public static class Builder
			extends DocumentCrawler.Builder<StringDocumentCrawler, StringDocumentRetriever, Builder> {

		public Builder() {
			super(new StringDocumentCrawler());
		}

		public Builder addString(String... strings) {
			building.strings.addAll(Arrays.asList(strings));
			return this;
		}

	}// end Builder

	private List<String> strings = new ArrayList<String>();

	/** Use {@link Builder} to construct StringDocumentCrawler */
	private StringDocumentCrawler() {
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<StringDocumentRetriever> iterator() {

		return new Iterator<StringDocumentRetriever>() {
			final Iterator<String> iterator = strings.iterator();

			@Override
			public void remove() {
				iterator.remove();
			}

			@Override
			public StringDocumentRetriever next() {
				return new StringDocumentRetriever(iterator.next());
			}

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}
		};
	}

}
