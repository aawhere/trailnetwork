/**
 *
 */
package com.aawhere.retrieve;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.lang.Assertion;

/**
 * Specializes in producing documents given a {@link File}.
 * 
 * @author roller
 * 
 */
public class FileDocumentRetriever
		implements DocumentRetriever {

	private static final long serialVersionUID = 7618294354076181946L;

	private Document document;

	private File file;

	private FileInputStream inputStream;

	public FileDocumentRetriever(File file) {
		this.file = file;

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.DocumentRetriever#getDocument()
	 */
	@Override
	public Document getDocument() {
		return Assertion.assertNotNull(this.document);
	}

	@Override
	public void retrieve(DocumentFormatKey expectedFormat) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, com.aawhere.io.FileNotFoundException,
			DocumentFormatNotRegisteredException {
		try {
			inputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {

			throw new com.aawhere.io.FileNotFoundException(file, e);
		}
		this.document = DocumentFactory.getInstance().getDocument(expectedFormat, inputStream);

	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.retriever.DocumentRetriever#getProviderDocumentIdentifier()
	 */
	@Override
	public String getProviderDocumentIdentifier() {

		return this.file.getAbsolutePath();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.retrieve.DocumentRetriever#getOrigionalData()
	 */
	@Override
	public byte[] getOrigionalData() {
		try {
			return IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
