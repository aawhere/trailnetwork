/**
 * 
 */
package com.aawhere.doc.kml;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractFeatureType;

import org.apache.commons.io.FileUtils;

import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBStringWriter;

/**
 * Helpful for testing kml documents like converting into strings and files..
 * 
 * @author aroller
 * 
 */
public class KmlTestUtil {

	/**
	 * 
	 * @param main
	 * @return
	 */
	public static String writeToXml(JAXBElement<? extends AbstractFeatureType> main) {
		KmlDocument doc = KmlDocument.create().setMain(main).build();
		return writeToXml(doc);
	}

	/**
	 * @param doc
	 * @return
	 */
	public static String writeToXml(KmlDocument doc) {
		JAXBStringWriter util = JAXBStringWriter.create(doc.getElement()).build();
		String xml = util.getXml();
		return xml;
	}

	/**
	 * Writes the given main element to a kml document in the target directory named as the given.
	 * 
	 * @param main
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public static String writeToFile(KmlDocument doc, String filename) throws IOException {
		String xml = writeToXml(doc);
		FileUtils.writeStringToFile(new File(TestUtil.TEMP_DIR, KmlUtil.addFilenameSuffix(filename)), xml);
		return xml;

	}

}
