/**
 * 
 */
package com.aawhere.doc.kml;

import java.io.IOException;

import org.junit.Test;

import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class KmlDocumentUnitTest {

	@Test
	public void testEmptyDoc() throws IOException {
		KmlDocument document = KmlDocument.create()
				.setMain(KmlUtil.FACTORY.createFolder(KmlUtil.FACTORY.createFolderType())).build();
		String xml = KmlTestUtil.writeToXml(document);
		TestUtil.assertContains(xml, "kml");
	}
}
