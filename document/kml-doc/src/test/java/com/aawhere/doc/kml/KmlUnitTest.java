/**
 * 
 */
package com.aawhere.doc.kml;

import java.io.IOException;

import net.opengis.kml.v_2_2_0.AbstractFeatureType;

import org.junit.Rule;
import org.junit.rules.TestName;

/**
 * Parent class for testing {@link Kml} implementations.
 * 
 * @author aroller
 * 
 */
public abstract class KmlUnitTest<K extends Kml<F>, F extends AbstractFeatureType> {
	@Rule
	public TestName name = new TestName();

	protected abstract K main();

	@org.junit.After
	public void finish() throws IOException {
		KmlDocument document = KmlDocument.create().setMain(main().get()).build();
		final String filename = getClass().getSimpleName() + "_" + name.getMethodName();
		KmlTestUtil.writeToFile(document, filename);
	}
}
