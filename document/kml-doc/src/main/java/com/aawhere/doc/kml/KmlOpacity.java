/**
 * 
 */
package com.aawhere.doc.kml;

import java.awt.Color;

import com.aawhere.style.AlphaChannel;

/**
 * Constants for setting visibility of geometries in kml. The alpha value is the first byte of the
 * color element with 0x00 being transparent and 0xff being opaque.
 * 
 * @author aroller
 * @deprecated use {@link AlphaChannel} with {@link Color}
 */
public enum KmlOpacity {

	TRANSPARENT(0x00), TRANSLUCENT(0xaa), OPAQUE(0xff);

	public final byte alpha;

	/**
	 * 
	 */
	private KmlOpacity(int alpha) {
		this.alpha = (byte) alpha;
	}
}
