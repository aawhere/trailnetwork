/**
 * 
 */
package com.aawhere.doc.kml;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.FolderType;

/**
 * Super simple transformer of {@link FolderType} to it's corresponding {@link JAXBElement} for
 * container assignment.
 * 
 * @author aroller
 * 
 */
public class KmlFolderTypeToElementFunction
		extends KmlFunctionBase<FolderType, JAXBElement<FolderType>> {

	@SuppressWarnings("unchecked")
	public static KmlFolderTypeToElementFunction build() {
		return (KmlFolderTypeToElementFunction) new Builder(new KmlFolderTypeToElementFunction()).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public JAXBElement<FolderType> apply(@Nullable FolderType input) {
		return factory.createFolder(input);
	}
}
