/**
 * 
 */
package com.aawhere.doc.kml;

import net.opengis.kml.v_2_2_0.ObjectFactory;

import com.aawhere.lang.AbstractObjectBuilder;

import com.google.common.base.Function;

/**
 * Base class providing common necessities to implementors of {@link Function}.
 * 
 * @author aroller
 * 
 */
abstract public class KmlFunctionBase<F, T>
		implements Function<F, T> {

	/**
	 * Used to construct all instances of KmlFunctionBase.
	 */
	public static class Builder<F, T, K extends KmlFunctionBase<F, T>, B extends Builder<F, T, K, B>>
			extends AbstractObjectBuilder<K, B> {

		public Builder(K building) {
			super(building);
		}

		public B setFactory(ObjectFactory factory) {
			building.factory = factory;
			return dis;
		}

		@Override
		public K build() {
			K built = super.build();
			if (built.factory == null) {
				built.factory = KmlUtil.FACTORY;
			}
			return built;
		}

	}// end Builder

	/** provided for all to use as needed. */
	protected ObjectFactory factory;

	/** Use {@link Builder} to construct KmlFunctionBase */
	protected KmlFunctionBase() {
	}

}
