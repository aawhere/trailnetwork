/**
 * 
 */
package com.aawhere.doc.kml;

import static com.aawhere.doc.kml.KmlUtil.*;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractViewType;

import org.joda.time.Duration;

import com.aawhere.joda.time.DurationUtil;
import com.aawhere.lang.ObjectBuilder;

import com.google.kml.ext._2.FlyToModeEnumType;
import com.google.kml.ext._2.FlyToType;
import com.google.kml.ext._2.PlaylistType;

/**
 * @author aroller
 * 
 */
public class FlyToKml
		implements Kml<FlyToType> {

	private static final long serialVersionUID = 684738278695348588L;
	private FlyToType flyTo;

	/**
	 * Used to construct all instances of FlyToKml.
	 */
	public static class Builder
			extends ObjectBuilder<FlyToKml> {

		private ViewKml<? extends AbstractViewType> view;
		private FlyToModeEnumType mode = FlyToModeEnumType.BOUNCE;
		private Duration duration = new Duration(5000);

		public Builder() {
			super(new FlyToKml());
		}

		public Builder mode(FlyToModeEnumType mode) {
			this.mode = mode;
			return this;
		}

		public Builder view(ViewKml<? extends AbstractViewType> view) {
			this.view = view;
			return this;
		}

		public Builder duration(Duration duration) {
			this.duration = duration;
			return this;
		}

		public Builder playlist(PlaylistType playlist) {
			this.playlist = playlist;
			return this;
		}

		private PlaylistType playlist;

		@Override
		public FlyToKml build() {
			FlyToKml built = super.build();
			FlyToType flyTo = GX_FACTORY.createFlyToType();
			flyTo.setFlyToMode(this.mode);
			flyTo.setDuration(DurationUtil.secondsFromDuration(duration));
			flyTo.setAbstractViewGroup(view.get());
			built.flyTo = flyTo;
			if (playlist != null) {
				playlist.getAbstractTourPrimitiveGroup().add(built.get());
			}
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct FlyToKml */
	private FlyToKml() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public JAXBElement<FlyToType> get() {
		return KmlUtil.GX_FACTORY.createFlyTo(flyTo);
	}

}
