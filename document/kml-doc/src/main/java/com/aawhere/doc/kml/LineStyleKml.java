/**
 * 
 */
package com.aawhere.doc.kml;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.LineStyleType;
import net.opengis.kml.v_2_2_0.StyleType;

/**
 * @author aroller
 * 
 */
public class LineStyleKml
		extends AbstractColorStyleKml<LineStyleType> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6168828033262654353L;

	/**
	 * Used to construct all instances of LineStyleKml.
	 */
	public static class Builder
			extends AbstractColorStyleKml.Builder<LineStyleKml, LineStyleType, Builder> {

		public Builder(StyleType composition) {
			super(composition, new LineStyleKml(), new LineStyleType());
		}

		@Override
		public LineStyleKml build() {
			LineStyleKml built = super.build();
			return built;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.kml.AbstractColorStyleKml.Builder#element(net.opengis.kml.v_2_2_0.
		 * AbstractColorStyleType)
		 */
		@Override
		protected JAXBElement<LineStyleType> element(LineStyleType kml) {
			return KmlUtil.FACTORY.createLineStyle(kml);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.kml.AbstractColorStyleKml.Builder#attach(net.opengis.kml.v_2_2_0.
		 * AbstractColorStyleType, net.opengis.kml.v_2_2_0.StyleType)
		 */
		@Override
		protected void attach(LineStyleType kml, StyleType composition) {
			composition.setLineStyle(kml);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.kml.AbstractColorStyleKml.Builder#key()
		 */
		@Override
		protected String key() {
			return "line";
		}

		/**
		 * @see KmlUtil#WIDE_LINE_WIDTH
		 * 
		 * @param lineWidth
		 * @return
		 */
		public Builder width(Double lineWidth) {
			kml().setWidth(lineWidth);
			return this;
		}

	}// end Builder

	public static Builder create(StyleType composition) {
		return new Builder(composition);
	}

	/** Use {@link Builder} to construct LineStyleKml */
	private LineStyleKml() {
		super();
	}

}
