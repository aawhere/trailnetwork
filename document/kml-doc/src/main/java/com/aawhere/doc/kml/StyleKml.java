/**
 * 
 */
package com.aawhere.doc.kml;

import java.util.ArrayList;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.ChangeType;
import net.opengis.kml.v_2_2_0.StyleType;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.ObjectBuilder;

/**
 * @author aroller
 * 
 */
public class StyleKml
		implements Kml<StyleType> {

	private static final long serialVersionUID = 2440280587094852400L;
	private StyleType style;

	/**
	 * Used to construct all instances of StyleKml.
	 */
	public static class Builder
			extends ObjectBuilder<StyleKml> {

		private IconStyleKml.Builder icon;
		private String targetId;
		private LabelStyleKml.Builder label;
		private LineStyleKml.Builder line;
		private ArrayList<AbstractColorStyleKml.Builder<?, ?, ?>> children = new ArrayList<>();

		public Builder() {
			super(new StyleKml());
			building.style = KmlUtil.FACTORY.createStyleType();
		}

		public LineStyleKml.Builder line() {
			this.line = LineStyleKml.create(building.style);
			children.add(this.line);
			return this.line;
		}

		public IconStyleKml.Builder icon() {
			if (this.icon == null) {
				if (this.targetId == null) {
					this.icon = IconStyleKml.create();
				} else {
					this.icon = IconStyleKml.change(targetId);
				}
			}
			return this.icon;
		}

		/**
		 * Provides build time access to the id for reference in the common use of
		 * {@link KmlUtil#styleUrl(String)}
		 */
		public String id() {
			return building.style.getId();
		}

		public LabelStyleKml.Builder label() {
			this.label = LabelStyleKml.create(building.style);
			return label;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();

		}

		@Override
		@SuppressWarnings({ "rawtypes", "unchecked" })
		public StyleKml build() {
			StyleKml built = super.build();

			// build all the children that are registered. transform isn't working?
			// final ObjectBuilderFunction buildFunction = ObjectBuilderFunction.build();
			// Lists.transform(this.children, buildFunction);

			for (com.aawhere.doc.kml.AbstractColorStyleKml.Builder<?, ?, ?> childBuilder : this.children) {
				childBuilder.build();
			}
			if (this.icon != null) {
				if (built.style.isSetId()) {
					icon.id(built.style.getId());
				}

				built.style.setIconStyle(icon.build().iconStyle());
			}
			if (this.label != null) {
				this.label.build();
			}
			return built;
		}

		/**
		 * @param idSuffix
		 *            identifies your object and will be the suffix to style-...
		 * @return
		 */
		public Builder id(String idSuffix) {
			building.style.setId(IdentifierUtils.createCompositeIdValue("style", idSuffix));
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	public static class Changer {
		StyleType changing;

		private Changer(StyleType changing) {
			this.changing = changing;
		}

		public LabelStyleKml.Builder label() {
			return new LabelStyleKml.Builder(changing.getLabelStyle());
		}

		/**
		 * @param style2
		 * @return
		 */
		public static Changer create(StyleKml changing) {
			return new Changer(changing.style);
		}
	}

	/**
	 * Creates a new instance which is used to for {@link ChangeType}.
	 * 
	 * @param targetId
	 * @return
	 */
	public static Builder change(String targetId) {
		final Builder builder = create();
		builder.targetId = targetId;
		return builder;
	}

	/** Use {@link Builder} to construct StyleKml */
	private StyleKml() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public JAXBElement<StyleType> get() {
		return KmlUtil.FACTORY.createStyle(style);
	}

	/**
	 * @return
	 */
	public com.aawhere.doc.kml.StyleKml.Builder change() {
		final Builder builder = StyleKml.create();
		builder.targetId = style.getId();
		return builder;
	}

}
