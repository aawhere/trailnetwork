/**
 * 
 */
package com.aawhere.doc.kml;

import java.util.Collection;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.BasicLinkType;
import net.opengis.kml.v_2_2_0.DocumentType;
import net.opengis.kml.v_2_2_0.FolderType;
import net.opengis.kml.v_2_2_0.LineStyleType;
import net.opengis.kml.v_2_2_0.ObjectFactory;

import com.aawhere.style.AlphaChannel;
import com.aawhere.style.Color;

import com.google.common.collect.Collections2;

/**
 * @author aroller
 * 
 */
public class KmlUtil {

	/** Here's an old school singleton for y'all */
	public static final ObjectFactory FACTORY = new ObjectFactory();
	public static final com.google.kml.ext._2.ObjectFactory GX_FACTORY = new com.google.kml.ext._2.ObjectFactory();
	public static final Double THIN_LINE_WIDTH = 1.0;
	public static final Double MEDIUM_LINE_WIDTH = 3.0;
	public static final Double WIDE_LINE_WIDTH = 6.0;
	public static final Double REALLY_WIDE_LINE_WIDTH = 10.0;

	/**
	 * Simply a common place to append the .kml to a desired filename.
	 * 
	 * @param filenameBase
	 * @return
	 */
	public static String addFilenameSuffix(String filenameBase) {
		return new StringBuilder().append(filenameBase).append(".kml").toString();
	}

	/**
	 * Given a {@link Color} this will return the byte[] ready for
	 * {@link LineStyleType#setColor(byte[])} and others.
	 * 
	 * @see AlphaChannel for opacity assignment to Color.
	 * 
	 * @param color
	 * @return
	 */
	public static byte[] color(Color color) {
		return new byte[] { AlphaChannel.create(color).build().byteValue(), (byte) color.getBlue(),
				(byte) color.getGreen(), (byte) color.getRed() };
	}

	/**
	 * @param deviationStyleId
	 * @return
	 */
	public static String styleUrl(String styleId) {
		final String hash = "#";
		if (!styleId.startsWith(hash)) {
			styleId = new StringBuilder().append(hash).append(styleId).toString();
		}
		return styleId;
	}

	/**
	 * Creates the link given the href.
	 * 
	 * @param string
	 * @return
	 */
	public static BasicLinkType link(String href) {
		BasicLinkType link = FACTORY.createBasicLinkType();
		link.setHref(href);
		return link;
	}

	public static DocumentType document(Collection<FolderType> folders) {
		Collection<JAXBElement<FolderType>> elements = Collections2.transform(	folders,
																				KmlFolderTypeToElementFunction.build());
		DocumentType document = FACTORY.createDocumentType();
		document.getAbstractFeatureGroup().addAll(elements);
		return document;
	}

}
