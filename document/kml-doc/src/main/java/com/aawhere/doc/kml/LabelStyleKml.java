/**
 * 
 */
package com.aawhere.doc.kml;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.LabelStyleType;
import net.opengis.kml.v_2_2_0.StyleType;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.style.Color;

/**
 * @author aroller
 * 
 */
public class LabelStyleKml
		implements Kml<LabelStyleType> {

	public LabelStyleType label;

	/**
	 * Used to construct all instances of LabelStyleKml.
	 */
	public static class Builder
			extends ObjectBuilder<LabelStyleKml> {

		public Builder(StyleType composition) {
			super(new LabelStyleKml());
			building.label = new LabelStyleType();
			final String styleId = composition.getId();
			if (styleId != null) {
				building.label.setId(IdentifierUtils.createCompositeIdValue("label", styleId));
			}
			composition.setLabelStyle(building.label);
		}

		public Builder(LabelStyleType changing) {
			super(new LabelStyleKml());
			building.label = new LabelStyleType();
			building.label.setTargetId(changing.getId());
		}

		public Builder color(Color color) {
			building.label.setColor(KmlUtil.color(color));
			return this;
		}

		@Override
		public LabelStyleKml build() {
			LabelStyleKml built = super.build();

			return built;
		}

	}// end Builder

	public static Builder create(StyleType composition) {
		return new Builder(composition);
	}

	/** Use {@link Builder} to construct LabelStyleKml */
	private LabelStyleKml() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public JAXBElement<LabelStyleType> get() {
		return KmlUtil.FACTORY.createLabelStyle(this.label);
	}

}
