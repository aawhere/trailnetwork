/**
 * 
 */
package com.aawhere.doc.kml;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractColorStyleType;
import net.opengis.kml.v_2_2_0.StyleType;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.style.Color;

/**
 * @author aroller
 * 
 */
abstract public class AbstractColorStyleKml<T extends AbstractColorStyleType>
		implements Kml<T> {

	private static final long serialVersionUID = 3050445501428304241L;

	private JAXBElement<T> element;

	/**
	 * Used to construct all instances of AbstractColorStyleKml.
	 */
	abstract public static class Builder<K extends AbstractColorStyleKml<T>, T extends AbstractColorStyleType, B extends Builder<K, T, B>>
			extends AbstractObjectBuilder<K, B> {

		private StyleType composition;
		private T kml;

		public Builder(StyleType composition, K building, T kml) {
			super(building);
			this.composition = composition;
			this.kml = kml;
		}

		/** The implementor must use the appropriate factory method. */
		abstract protected JAXBElement<T> element(T kml);

		/** Each child must set to the style to be included in teh composition. */
		abstract protected void attach(T kml, StyleType composition);

		/** A unique key that differentiates itself from other implements ("line, label, poly, etc). */
		abstract protected String key();

		protected T kml() {
			return this.kml;
		}

		public B color(Color color) {
			kml().setColor(KmlUtil.color(color));
			return dis;
		}

		public K build() {
			K built = super.build();
			attach(kml(), composition);
			IdentifierUtils.createCompositeIdValue(key(), composition.getId());
			attach(kml, composition);
			((AbstractColorStyleKml<T>) built).element = element(this.kml);
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct AbstractColorStyleKml */
	protected AbstractColorStyleKml() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Supplier#get()
	 */
	@Override
	public JAXBElement<T> get() {
		return this.element;

	}

}
