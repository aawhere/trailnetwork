/**
 * 
 */
package com.aawhere.doc.kml;

import static com.aawhere.doc.kml.KmlUtil.*;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.BasicLinkType;
import net.opengis.kml.v_2_2_0.ChangeType;
import net.opengis.kml.v_2_2_0.IconStyleType;
import net.opengis.kml.v_2_2_0.Scaleliteral;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.style.Color;

/**
 * A simple builder for Icons that allows style setting. This intends to discourage mutation of the
 * results.
 * 
 * FIXME: This class is confusing because it is composite in nature of Style. It could be improved
 * to avoid confusion.
 * 
 * @author aroller
 * 
 */
public class IconStyleKml
		implements Kml<IconStyleType> {

	private static final long serialVersionUID = 7003688948538114188L;
	private IconStyleType iconStyle;

	/**
	 * Used to construct all instances of KmlIcon.
	 */
	public static class Builder
			extends ObjectBuilder<IconStyleKml> {
		private IconStyleType iconStyle = FACTORY.createIconStyleType();

		public Builder() {
			super(new IconStyleKml());
		}

		public Builder icon(KmlStandardIcon icon) {
			final BasicLinkType link = link(icon.url);
			iconStyle.setIcon(link);
			return this;
		}

		public Builder color(Color color) {
			iconStyle.setColor(KmlUtil.color(color));
			return this;
		}

		public Builder scale(Double scale) {
			iconStyle.setScale(new Scaleliteral(scale));
			return this;
		}

		/**
		 * Adds the provided suffix to the standard prefix allowing your provided id to remain
		 * unique.
		 * 
		 * @param idSuffix
		 * @return
		 */
		public Builder id(String idSuffix) {
			iconStyle.setId(makeId(idSuffix));
			return this;
		}

		/**
		 * @param idSuffix
		 * @return
		 */
		private String makeId(String idSuffix) {
			return IdentifierUtils.createCompositeIdValue("iconStyle", idSuffix);
		}

		@Override
		public IconStyleKml build() {
			IconStyleKml built = super.build();

			built.iconStyle = this.iconStyle;
			// clear out to avoid future mutation
			this.iconStyle = null;
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/**
	 * 
	 * @param styleTargetId
	 *            the id of th containing style so this can add the iconstyle prefix.
	 * @return
	 */
	public static Builder change(String styleTargetId) {
		Builder builder = create();
		builder.iconStyle.setTargetId(builder.makeId(styleTargetId));
		return builder;
	}

	/** Use {@link Builder} to construct KmlIcon */
	private IconStyleKml() {
	}

	@Override
	public JAXBElement<IconStyleType> get() {
		return FACTORY.createIconStyle(iconStyle);
	}

	/**
	 * When you need to make a {@link ChangeType} to this then use the resulting builder.
	 * 
	 * @return
	 */
	public IconStyleKml.Builder change() {
		final Builder change = IconStyleKml.create();
		change.iconStyle.setTargetId(this.iconStyle.getId());
		return change;

	}

	/**
	 * @return the iconStyle
	 */
	IconStyleType iconStyle() {
		return this.iconStyle;
	}

}
