/**
 * 
 */
package com.aawhere.doc.kml;

import net.opengis.kml.v_2_2_0.AbstractViewType;

/**
 * @author aroller
 * 
 */
public interface ViewKml<T extends AbstractViewType>
		extends Kml<T> {

}
