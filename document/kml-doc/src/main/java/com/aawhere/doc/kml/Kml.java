/**
 * 
 */
package com.aawhere.doc.kml;

import java.io.Serializable;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractObjectType;

import com.google.common.base.Supplier;

/**
 * @author aroller
 * 
 */
public interface Kml<T extends AbstractObjectType>
		extends Serializable, Supplier<JAXBElement<T>> {

}
