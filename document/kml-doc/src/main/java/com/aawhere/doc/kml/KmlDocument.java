/**
 * 
 */
package com.aawhere.doc.kml;

import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.AbstractFeatureType;
import net.opengis.kml.v_2_2_0.AbstractGeometryType;
import net.opengis.kml.v_2_2_0.DocumentType;
import net.opengis.kml.v_2_2_0.KmlType;
import net.opengis.kml.v_2_2_0.ObjectFactory;
import net.opengis.kml.v_2_2_0.PlacemarkType;

import com.aawhere.lang.ObjectBuilder;

/**
 * A single {@link KmlType} that contains components to visualize system components.
 * 
 * 
 * @author aroller
 * 
 */
public class KmlDocument {

	/**
	 * Used to construct all instances of KmlDocument.
	 */
	public static class Builder
			extends ObjectBuilder<KmlDocument> {

		private ObjectFactory factory;

		public Builder() {
			super(new KmlDocument());
			this.factory = new ObjectFactory();
			building.kml = factory.createKmlType();
		}

		public Builder setMain(JAXBElement<? extends AbstractFeatureType> container) {
			building.kml.setAbstractFeatureGroup(container);
			return this;
		}

		/**
		 * Useful when you only have a geometry that you'd like to be the main. This will simply
		 * create a placemark with no other properties.
		 * 
		 * @param geometry
		 * @return
		 */
		public Builder setMainGeometry(JAXBElement<? extends AbstractGeometryType> geometry) {
			PlacemarkType placemark = factory.createPlacemarkType();
			placemark.setAbstractGeometryGroup(geometry);
			return setMain(factory.createPlacemark(placemark));
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public KmlDocument build() {
			KmlDocument build = super.build();
			build.element = factory.createKml(build.kml);
			return build;
		}

		/**
		 * @param document
		 * @return
		 */
		public Builder setMain(DocumentType document) {
			return setMain(KmlUtil.FACTORY.createDocument(document));

		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	private KmlType kml;
	private JAXBElement<KmlType> element;

	/** Use {@link Builder} to construct KmlDocument */
	private KmlDocument() {
	}

	/**
	 * @return the kml
	 */
	public KmlType getKml() {
		return this.kml;
	}

	public KmlType kml() {
		return kml;
	}

	public JAXBElement<KmlType> getElement() {
		return this.element;
	}

	public JAXBElement<KmlType> element() {
		return this.element;
	}

}
