/**
 * 
 */
package com.aawhere.doc.kml;

import net.opengis.kml.v_2_2_0.AbstractObjectType;

/**
 * @author aroller
 * 
 */
public abstract class KmlBase<T extends AbstractObjectType>
		implements Kml<T> {

}
