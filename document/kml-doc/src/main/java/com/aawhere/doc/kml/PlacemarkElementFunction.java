/**
 * 
 */
package com.aawhere.doc.kml;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBElement;

import net.opengis.kml.v_2_2_0.PlacemarkType;

import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public class PlacemarkElementFunction
		implements Function<PlacemarkType, JAXBElement<PlacemarkType>> {

	/**
	 * Used to construct all instances of PlacemarkElementFunction.
	 */
	public static class Builder
			extends ObjectBuilder<PlacemarkElementFunction> {

		public Builder() {
			super(new PlacemarkElementFunction());
		}

		@Override
		public PlacemarkElementFunction build() {
			PlacemarkElementFunction built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct PlacemarkElementFunction */
	private PlacemarkElementFunction() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public JAXBElement<PlacemarkType> apply(@Nullable PlacemarkType input) {
		return KmlUtil.FACTORY.createPlacemark(input);
	}

}
