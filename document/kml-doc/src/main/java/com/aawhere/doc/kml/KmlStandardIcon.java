/**
 * 
 */
package com.aawhere.doc.kml;

/**
 * An enumeration of hrefs pointing to standard icons found with Google Earth.
 * 
 * @author aroller
 * 
 */
public enum KmlStandardIcon {

	NONE(null),

	DOT("http://maps.google.com/mapfiles/kml/shapes/shaded_dot.png"), // sphere
	FLAG("http://maps.google.com/mapfiles/kml/shapes/flag.png"), // start/finish
	SQUARE("http://maps.google.com/mapfiles/kml/shapes/placemark_square.png"), // small
	CIRCLE("http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png"), // bullseye
	;
	public final String url;

	/**
	 * 
	 */
	private KmlStandardIcon(String url) {
		this.url = url;
	}

}
