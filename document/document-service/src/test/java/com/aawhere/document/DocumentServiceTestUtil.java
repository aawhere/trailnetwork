/**
 *
 */
package com.aawhere.document;

import static org.junit.Assert.*;

import java.util.List;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentObjectifyRepository;
import com.aawhere.doc.archive.ArchivedDocumentRepository;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.doc.text.TextDocument;
import com.aawhere.doc.text.TextDocumentProducer;
import com.aawhere.gpx._1._1.GpxUnitTest;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;
import com.aawhere.person.PersonServiceTestUtil;

/**
 * Test Utility for creating singleton test instances for Document Services.
 * 
 * @author Brian Chapman
 * 
 */
public class DocumentServiceTestUtil {

	private DocumentService documentService;
	private ArchivedDocumentObjectifyRepository documentRepository;
	private DocumentFactory documentFactory;
	private PersonServiceTestUtil personServiceTestUtil;

	/**
	 * @return the documentService
	 */
	public DocumentService getDocumentService() {
		return this.documentService;
	}

	/**
	 * @return the documentRepository
	 */
	public ArchivedDocumentRepository getDocumentRepository() {
		return this.documentRepository;
	}

	/**
	 * @return the documentFactory
	 */
	public DocumentFactory getDocumentFactory() {
		return this.documentFactory;
	}

	/**
	 * @return the personServiceTestUtil
	 */
	public PersonServiceTestUtil getPersonServiceTestUtil() {
		return this.personServiceTestUtil;
	}

	public List<ArchivedDocument> findAll() {
		// how many are you really gonna create in test?
		return this.documentRepository.findAll(1000);
	}

	public ArchivedDocument createPersistedArchivedDocument() {
		final DocumentFormatKey format = DocumentFormatKey.GPX_1_1;
		final String contents = GpxUnitTest.GARMIN_CONNECT_COMPLETE;
		return createPersistedArchivedDocument(contents, format);
	}

	/**
	 * @param format
	 * @return
	 */
	public ArchivedDocument createPersistedArchivedDocument(String contents, final DocumentFormatKey format) {
		ArchivedDocument doc;
		try {
			doc = documentService.createDocument(contents, format);
			assertNotNull(doc.getId());
			return doc;
		} catch (DocumentDecodingException e) {
			throw new RuntimeException(e);
		} catch (DocumentFormatNotRegisteredException e) {
			throw new RuntimeException(e);
		}
	}

	/** Given most everything and we'll give you the persisted version back. */
	public ArchivedDocument createPersistedArchivedTextDocument(String contents, DocumentFormatKey format,
			ApplicationDataId dataId) {
		return this.documentService.createDocument(new TextDocument(contents, format), dataId);
	}

	/**
	 * Used to construct all instances of DocumentServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<DocumentServiceTestUtil> {

		private DocumentFactory.Builder documentFactoryBuilder;
		private PersonServiceTestUtil.Builder personServiceTestUtilBuilder;

		public Builder() {
			super(new DocumentServiceTestUtil());
			documentFactoryBuilder = new DocumentFactory.Builder();
			documentFactoryBuilder.register(new TextDocumentProducer.Builder().build());
			personServiceTestUtilBuilder = PersonServiceTestUtil.create();
		}

		/**
		 * Use this to configure the document factory.
		 * 
		 * Text document is automatically registered.
		 * 
		 * @return the documentFactoryBuilder
		 */
		public DocumentFactory.Builder getDocumentFactoryBuilder() {
			return this.documentFactoryBuilder;
		}

		/**
		 * @return the personServiceTestUtilBuilder
		 */
		public PersonServiceTestUtil.Builder getPersonServiceTestUtilBuilder() {
			return this.personServiceTestUtilBuilder;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public DocumentServiceTestUtil build() {
			this.personServiceTestUtilBuilder.getAnnotationDictionaryFactory().getDictionary(ArchivedDocument.class);
			building.personServiceTestUtil = this.personServiceTestUtilBuilder.build();
			building.documentFactory = this.documentFactoryBuilder.build();
			building.documentRepository = new ArchivedDocumentObjectifyRepository(Synchronization.SYNCHRONOUS);
			building.documentService = new DocumentService(building.documentRepository, building.documentFactory);
			return super.build();
		}

	}// end Builder

	/** Use {@link Builder} to construct DocumentServiceTestUtil */
	private DocumentServiceTestUtil() {
	}

	public static Builder create() {
		return new Builder();
	}

}
