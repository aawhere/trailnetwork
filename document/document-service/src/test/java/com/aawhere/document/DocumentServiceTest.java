/**
 *
 */
package com.aawhere.document;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.ApplicationDataIdImpl;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.doc.DocumentTestUtil;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentId;
import com.aawhere.doc.archive.DocumentService;
import com.aawhere.doc.text.TextDocument;
import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * Tests the coarse grain features of {@link DocumentService} and it's duty to provide
 * {@link ArchivedDocument}s and {@link Document}s.
 * 
 * @author Aaron Roller
 * 
 */
public class DocumentServiceTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private DocumentService docService;
	TextDocument expected;
	ApplicationDataIdImpl applicationDataId;

	@Before
	public void setUpServices() {
		helper.setUp();

		docService = new DocumentServiceTestUtil.Builder().build().getDocumentService();
		expected = DocumentTestUtil.createRandomDocument();
		applicationDataId = new ApplicationDataIdImpl(TestUtil.generateRandomString());
	}

	@After
	public void tearDownServices() {
		helper.tearDown();
	}

	@Test(expected = EntityNotFoundException.class)
	public void testGetDocumentNotFound() throws EntityNotFoundException {
		docService.getArchivedDocument(IdentifierTestUtil.generateRandomLongId(	ArchivedDocument.class,
																				ArchivedDocumentId.class));
	}

	@Test
	public void testCreateTextDocument() throws DocumentContentsNotRecognizedException, DocumentDecodingException,
			DocumentFormatNotRegisteredException {
		String contents = "test";
		ArchivedDocument archivedDocument = docService.createDocument(contents, DocumentFormatKey.TEXT);
		assertNotNull(archivedDocument);

		TextDocument doc = (TextDocument) archivedDocument.getDocument();
		assertEquals(contents, doc.getContents());

	}

	@Test
	public void testCreateDocument() throws EntityNotFoundException {
		ArchivedDocument created = docService.createDocument(expected, applicationDataId);
		Document actual = created.getDocument();
		assertTextDocument(expected, actual);
		ArchivedDocument recreated = docService.createDocument(expected, applicationDataId);
		assertEquals("oops, the service is supposed to just send the original created.", created, recreated);
		ArchivedDocument found = docService.getArchivedDocument(applicationDataId, expected.getDocumentFormat());
		assertEquals("finder isn't finding the right one.", created, found);
		assertTextDocument(expected, found.getDocument());
		// document isn't complete until service puts it back together
		final Document documentFromArchive = found.getDocument();
		assertEquals(expected.getDocumentFormat(), documentFromArchive.getDocumentFormat());

		assertEquals(expected, documentFromArchive);

	}

	@Test(expected = EntityNotFoundException.class)
	public void testNotFoundByApplicationDataId() throws EntityNotFoundException {
		docService.getArchivedDocument(applicationDataId, expected.getDocumentFormat());
	}

	@Test
	public void testCreateDocumentFromString() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {

		TextDocument expected = DocumentTestUtil.createRandomDocument();
		ArchivedDocument created = docService.createDocument(expected.getContents(), DocumentFormatKey.TEXT);
		assertTextDocument(expected, created.getDocument());

	}

	@Test
	public void testCreateDocumentsBatched() throws EntityNotFoundException {
		TextDocument doc1 = DocumentTestUtil.createRandomDocument();
		ApplicationDataId dataId1 = DocumentTestUtil.applicationDataId();
		TextDocument doc2 = DocumentTestUtil.createRandomDocument();
		ApplicationDataId dataId2 = DocumentTestUtil.applicationDataId();
		TextDocument doc3 = DocumentTestUtil.createRandomDocument();
		ApplicationDataId dataId3 = DocumentTestUtil.applicationDataId();
		ImmutableMap<ApplicationDataId, Document> map = ImmutableMap.<ApplicationDataId, Document> builder()
				.put(dataId1, doc1).put(dataId2, doc2).put(dataId3, doc3).build();
		docService.createDocuments(map);
		assertEquals(dataId1.toString(), docService.getArchivedDocument(dataId1, doc1.getDocumentFormat())
				.getApplicationDataId());
		assertEquals(dataId2.toString(), docService.getArchivedDocument(dataId2, doc2.getDocumentFormat())
				.getApplicationDataId());
		assertEquals(dataId3.toString(), docService.getArchivedDocument(dataId3, doc3.getDocumentFormat())
				.getApplicationDataId());
	}

	/**
	 * @param expected
	 * @param actual
	 */
	private void assertTextDocument(TextDocument expected, Document actual) {
		assertEquals(expected.getClass(), actual.getClass());
		assertEquals(expected.getDocumentFormat(), actual.getDocumentFormat());
		assertEquals(expected.getContents(), ((TextDocument) actual).getContents());
	}
}
