/**
 *
 */
package com.aawhere.document;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.ApplicationDataIdImpl;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.archive.ArchivedDocumentRepository;
import com.aawhere.doc.archive.ArchivedDocumentUnitTest;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Sets;

/**
 * @author Aaron Roller
 * 
 */
public class ArchivedDocumentObjectifyRepositoryTest
		extends ArchivedDocumentUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private DocumentServiceTestUtil testUtil;

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();

	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.doc.archive.ArchivedDocumentUnitTest#createRepository()
	 */
	@Override
	protected ArchivedDocumentRepository createRepository() {
		// this is called before by the parent before this setup.
		this.testUtil = DocumentServiceTestUtil.create().build();
		return testUtil.getDocumentRepository();
	}

	@Test
	public void testFindByApplicationDataId() {
		ArchivedDocument sample = getEntitySample();
		ArchivedDocument stored = getRepository().store(sample);
		String applicationDataId = sample.getApplicationDataId();
		List<ArchivedDocument> found = getRepository().findByApplicationDataId(applicationDataId, sample.getFormat());
		assertEquals(	"we didn't find the " + sample.getApplicationDataId() + " using " + applicationDataId,
						1,
						found.size());
		assertEquals(stored.getId(), found.get(0).getId());
		ArchivedDocument second = getRepository().store(getEntitySample());
		TestUtil.assertNotEquals(sample.getId(), second.getId());
		List<ArchivedDocument> foundAgain = getRepository().findByApplicationDataId(applicationDataId,
																					sample.getFormat());
		assertEquals(2, foundAgain.size());

	}

	/**
	 * TN-856 .. previously an exception was being thrown
	 * 
	 */
	@Test
	public void testFindByApplicationIdsEmpty() {
		TestUtil.assertEmpty(getRepository().findByApplicationDataIds(	Collections.<String> emptyList(),
																		DocumentFormatKey.GPX));
	}

	@Test
	public void testFindByApplicationIds() {
		// doc1 is alone by it's data id, but shares format with 2
		// doc2 shares dataid with 3, but each have their own format
		String contents1 = TestUtil.generateRandomAlphaNumeric();
		String contents2 = TestUtil.generateRandomAlphaNumeric();
		String contents3 = TestUtil.generateRandomAlphaNumeric();

		ApplicationDataIdImpl dataId1 = new ApplicationDataIdImpl(TestUtil.generateRandomAlphaNumeric());
		ApplicationDataIdImpl dataId2And3 = new ApplicationDataIdImpl(TestUtil.generateRandomAlphaNumeric());

		DocumentFormatKey format1And2 = DocumentFormatKey.TEXT;
		DocumentFormatKey format3 = DocumentFormatKey.UNKNOWN;
		ArchivedDocument doc1 = this.testUtil.createPersistedArchivedTextDocument(contents1, format1And2, dataId1);
		ArchivedDocument doc2 = this.testUtil.createPersistedArchivedTextDocument(contents2, format1And2, dataId2And3);
		ArchivedDocument doc3 = this.testUtil.createPersistedArchivedTextDocument(contents3, format3, dataId2And3);

		// all ids, but limited by format
		TestUtil.assertContainsAll(	Sets.newHashSet(doc1, doc2),
									getRepository()
											.findByApplicationDataIds(	Sets.newHashSet(dataId1.toString(),
																						dataId2And3.toString()),
																		format1And2));
		TestUtil.assertContainsAll(	Sets.newHashSet(doc3),
									getRepository()
											.findByApplicationDataIds(Sets.newHashSet(dataId2And3.toString()), format3));
		TestUtil.assertContainsAll(	Sets.newHashSet(doc1),
									getRepository().findByApplicationDataIds(	Sets.newHashSet(dataId1.toString()),
																				format1And2));
		TestUtil.assertContainsAll(	Sets.newHashSet(doc2),
									getRepository()
											.findByApplicationDataIds(	Sets.newHashSet(dataId2And3.toString()),
																		format1And2));
		TestUtil.assertEmpty(getRepository().findByApplicationDataIds(	Sets.newHashSet(TestUtil
																				.generateRandomAlphaNumeric()),
																		format3));
	}
}
