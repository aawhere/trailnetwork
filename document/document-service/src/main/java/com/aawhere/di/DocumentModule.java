/**
 *
 */
package com.aawhere.di;

import com.aawhere.doc.archive.ArchivedDocumentObjectifyRepository;
import com.aawhere.doc.archive.ArchivedDocumentRepository;

import com.google.inject.AbstractModule;

/**
 * @author brian
 * 
 */
public class DocumentModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(ArchivedDocumentRepository.class).to(ArchivedDocumentObjectifyRepository.class);
	}

}
