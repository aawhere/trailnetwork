/**
 *
 */
package com.aawhere.doc.archive;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nonnull;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.ApplicationDataIdToStringFunction;
import com.aawhere.app.ApplicationUtil;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.id.Identifier;
import com.aawhere.id.LongIdentifier;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.RepositoryUtil;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Provides the interaction between systems to process {@link Document}s and
 * {@link ArchivedDocument}s using {@link DocumentRetriever}s and {@link DocumentCrawler}s.
 * 
 * TODO: Delegate most of this work to a User Stories class. We shouldn't be instantiating crawlers.
 * 
 * @author Aaron Roller
 * 
 * @see http ://aawhere.jira.com/wiki/display/AAWHERE/Documents%2C+Retrievers+and+ Crawlers
 * 
 */
@Singleton
public class DocumentService {

	private ArchivedDocumentRepository archivedDocumentRepository;
	private DocumentFactory documentFactory;

	@Inject
	public DocumentService(ArchivedDocumentRepository repository, DocumentFactory documentFactory) {
		this.archivedDocumentRepository = repository;
		this.documentFactory = documentFactory;
	}

	/**
	 * Retrieves an {@link ArchivedDocument} given it's {@link LongIdentifier}.
	 * 
	 * @param documentId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Nonnull
	public ArchivedDocument getArchivedDocument(@Nonnull ArchivedDocumentId documentId) throws EntityNotFoundException {

		return ArchivedDocumentUtil.complete(archivedDocumentRepository.load(documentId), this.documentFactory);
	}

	public ArchivedDocument getArchivedDocument(@Nonnull ApplicationDataId id, DocumentFormatKey format)
			throws EntityNotFoundException {
		List<ArchivedDocument> results = archivedDocumentRepository
				.findByApplicationDataId(new ApplicationDataIdToStringFunction().apply(id), format);
		ApplicationDataId dataId = id;
		ArchivedDocument finderResult = RepositoryUtil.finderResult((Identifier<?, ?>) dataId, results);
		return ArchivedDocumentUtil.complete(finderResult, this.documentFactory);
	}

	/**
	 * Given the set of ids and the format desired for those ids this will return the matching
	 * documents efficiently ready for use.
	 * 
	 * @param ids
	 * @param format
	 * @return
	 */
	public Iterable<ArchivedDocument> getArchivedDocuments(@Nonnull Iterable<? extends ApplicationDataId> ids,
			DocumentFormatKey format) {
		Set<String> idValues = Sets.newHashSet(ApplicationUtil.applicationDataIdValues(ids));
		Iterable<ArchivedDocument> incompleteDocs = archivedDocumentRepository.findByApplicationDataIds(idValues,
																										format);
		return Iterables.transform(incompleteDocs, ArchivedDocumentUtil.completeFunction(documentFactory));
	}

	/**
	 * Provides access to the encoded data as it is stored in the system.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	public byte[] getEncodedData(@Nonnull ArchivedDocumentId id) throws EntityNotFoundException {
		return getArchivedDocument(id).getEncodedData();
	}

	/**
	 * Given the content as a String this will create a document and return the corresponding Id.
	 * 
	 * @see TN-20
	 * 
	 * @param supportedContent
	 * @return
	 * @throws DocumentFormatNotRegisteredException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 */
	@Nonnull
	public ArchivedDocument createDocument(@Nonnull String supportedContent, DocumentFormatKey expectedFormat)
			throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		Document doc = documentFactory.getDocument(expectedFormat, supportedContent);
		return archivedDocumentRepository.store(new ArchivedDocument.Builder().setDocument(doc).build());
	}

	/**
	 * Creates the document only if it doesn't already exist in the datastore. It will return the
	 * existing document if it is found.
	 * 
	 * @param document
	 * @param applicationDataId
	 * @return
	 */
	@Nonnull
	public ArchivedDocument createDocument(@Nonnull Document document, @Nonnull ApplicationDataId applicationDataId) {
		return createDocument(document, applicationDataId, document.toByteArray());
	}

	/**
	 * Creates the document using the document and origional data to avoid costly marshalling.
	 * 
	 * @see #createDocument(Document, ApplicationDataId)
	 * 
	 * @param document
	 * @param applicationDataId
	 * @return
	 */
	@Nonnull
	public ArchivedDocument createDocument(@Nonnull Document document, @Nonnull ApplicationDataId applicationDataId,
			Object data) {
		ArchivedDocument archivedDocument;
		try {
			archivedDocument = getArchivedDocument(applicationDataId, document.getDocumentFormat());
		} catch (EntityNotFoundException e) {
			archivedDocument = new ArchivedDocument.Builder().setUncompressedData(data).setDocument(document)
					.setApplicationDataId(applicationDataId).build();
			archivedDocument = archivedDocumentRepository.store(archivedDocument);
		}

		return archivedDocument;
	}

	/**
	 * Modifes the currently stored {@link ArchivedDocument} with the updated document contents.
	 * 
	 * @param id
	 * @param document
	 * @param object
	 * @return
	 * @throws EntityNotFoundException
	 */
	public ArchivedDocument update(ArchivedDocumentId id, Document document, Object object)
			throws EntityNotFoundException {
		return this.archivedDocumentRepository.update(id, document);

	}

	/**
	 * Batch persist of the documents provided with no verification if the documents already exist.
	 * This is asynchronous since the resulting map is not returned and never called. There is no
	 * feedback if this succeeded in this thread, but will show up in the logs?
	 * 
	 * @param documents
	 */
	public void createDocuments(Map<? extends ApplicationDataId, ? extends Document> documents) {
		Builder<ArchivedDocument> archivedDocumentsBuilder = ImmutableList.builder();
		for (Entry<? extends ApplicationDataId, ? extends Document> entry : documents.entrySet()) {
			Document document = entry.getValue();
			ArchivedDocument archivedDocument = ArchivedDocument.create().setUncompressedData(document.toByteArray())
					.setDocument(document).setApplicationDataId(entry.getKey()).build();
			archivedDocumentsBuilder.add(archivedDocument);
		}
		this.archivedDocumentRepository.storeAll(archivedDocumentsBuilder.build());
	}

	/**
	 * Bulk loads all those ids provided during application.
	 * 
	 * @return
	 */
	public Function<Iterable<ArchivedDocumentId>, Map<ArchivedDocumentId, ArchivedDocument>> idsFunction() {
		return new Function<Iterable<ArchivedDocumentId>, Map<ArchivedDocumentId, ArchivedDocument>>() {

			@Override
			public Map<ArchivedDocumentId, ArchivedDocument> apply(Iterable<ArchivedDocumentId> input) {
				return ArchivedDocumentUtil.complete(archivedDocumentRepository.loadAll(input), documentFactory);
			}
		};
	}

}
