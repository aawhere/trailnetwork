/**
 *
 */
package com.aawhere.doc.archive;

import java.util.Collections;
import java.util.List;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

/**
 * Implements the {@link ArchivedDocumentRepository} for {@link ObjectifyRepository} to manage
 * {@link ArchivedDocument}s.
 * 
 * @author Aaron Roller
 * 
 */
@Singleton
public class ArchivedDocumentObjectifyRepository
		extends ObjectifyRepository<ArchivedDocument, ArchivedDocumentId>
		implements ArchivedDocumentRepository {

	static {
		ObjectifyService.register(ArchivedDocument.class);
	}

	/**
	 * 
	 */
	public ArchivedDocumentObjectifyRepository() {
		super();
	}

	public ArchivedDocumentObjectifyRepository(Synchronization synchronization) {
		super(synchronization);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.doc.archive.ArchivedDocumentRepository#findByApplicationDataId(com.aawhere.app
	 * .ApplicationDataId, com.aawhere.doc.DocumentFormatKey)
	 */
	@Override
	public List<ArchivedDocument> findByApplicationDataId(String applicationDataId, DocumentFormatKey format) {
		// IN queries are actually just many equal queries so the transformation to a list is
		// minimal?
		return Lists.newArrayList(findByApplicationDataIds(Lists.newArrayList(applicationDataId), format));
	}

	/**
	 * Provides any document found with the given applicationId and format. This guarantees
	 * uniqueness, but order is not
	 * 
	 * @param applicationDataIds
	 * @param format
	 * @return
	 */
	@Override
	public Iterable<ArchivedDocument> findByApplicationDataIds(Iterable<String> applicationDataIds,
			DocumentFormatKey format) {
		// only one happy situation..the rest show empty
		Iterable<ArchivedDocument> result = Collections.emptyList();
		if (applicationDataIds != null) {
			ImmutableSet<String> uniqueIds = ImmutableSet.<String> builder().addAll(applicationDataIds).build();
			if (!uniqueIds.isEmpty()) {
				// This reduces the need for a composite index for since most ids would have very
				// few formats
				String filterString = FilterOperator.IN.filterString(ArchivedDocument.FIELD.APPLICATION_DATA_ID);
				Query<ArchivedDocument> queryResult = ofy().load().type(ArchivedDocument.class)
						.filter(filterString, applicationDataIds);
				result = ArchivedDocumentUtil.filterFormat(queryResult, format);
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.archive.ArchivedDocumentRepository#update(com.aawhere.doc.archive.
	 * ArchivedDocumentId, com.aawhere.doc.Document)
	 */
	@Override
	public ArchivedDocument update(ArchivedDocumentId id, Document document) throws EntityNotFoundException {
		return update(ArchivedDocumentRepositoryUtil.mutate(id, document, this));
	}

}
