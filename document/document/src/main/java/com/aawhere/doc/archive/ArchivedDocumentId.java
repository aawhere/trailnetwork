/**
 *
 */
package com.aawhere.doc.archive;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.id.Identifier;
import com.aawhere.id.LongIdentifier;
import com.aawhere.xml.XmlNamespace;

/**
 * The {@link Identifier} for {@link ArchivedDocument}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class ArchivedDocumentId
		extends LongIdentifier<ArchivedDocument> {

	private static final long serialVersionUID = 1574212897049656756L;

	/**
	 * @param kind
	 */
	ArchivedDocumentId() {
		super(ArchivedDocument.class);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public ArchivedDocumentId(Long value) {
		super(value, ArchivedDocument.class);
	}

	/**
	 * Provided to any client that may have a Long formatted as a String.
	 * 
	 * @param value
	 * @param kind
	 */
	public ArchivedDocumentId(String value) {
		super(value, ArchivedDocument.class);
	}

}
