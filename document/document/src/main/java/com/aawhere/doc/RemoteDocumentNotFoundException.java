/**
 * 
 */
package com.aawhere.doc;

import java.net.URL;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates the document retrieved is not found.
 * 
 * @author Aaron Roller
 * 
 */
@StatusCode(HttpStatusCode.NOT_FOUND)
public class RemoteDocumentNotFoundException
		extends BaseException {

	private static final long serialVersionUID = -4557167143837389975L;

	public RemoteDocumentNotFoundException(URL url) {
		this(url.toExternalForm());
	}

	/**
	 * @param path
	 *            the URL attempted to retrieve the document
	 */
	public RemoteDocumentNotFoundException(String path) {
		super(new CompleteMessage.Builder(DocumentMessage.REMOTE_DOC_NOT_FOUND)
				.addParameter(DocumentMessage.Param.PATH, path).build());
	}

}
