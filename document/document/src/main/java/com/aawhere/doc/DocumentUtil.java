/**
 * 
 */
package com.aawhere.doc;

import com.aawhere.doc.text.TextDocument;
import com.aawhere.io.IoException;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.net.HttpStatusCode;

import com.google.common.base.Function;

/**
 * Handy to work with {@link Document}.
 * 
 * @author aroller
 * 
 */
public class DocumentUtil {

	/**
	 * Given the {@link HttpStatusCode} this will return the best exception for Document retrieval
	 * known to this scope.
	 * 
	 * @see IoException#best(HttpStatusCode, String)
	 * 
	 * @param code
	 * @param path
	 * @return
	 */
	public static BaseException bestException(HttpStatusCode code, String path) {
		BaseException exception;
		switch (code) {
			case FORBIDDEN:
				exception = new RemoteDocumentNotPublicException(path);
				break;
			case NOT_FOUND:
				exception = new RemoteDocumentNotFoundException(path);
				break;
			case NOT_MODIFIED:
				exception = new RemoteDocumentNotModifiedException(path);
				break;
			default:
				exception = IoException.best(code, path);
		}

		return exception;
	}

	public static Function<Document, String> textDocumentFunction() {
		return new Function<Document, String>() {

			@Override
			public String apply(Document input) {
				return (input == null) ? null : ((TextDocument) input).getContents();
			}
		};
	}

}
