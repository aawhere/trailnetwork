/**
 *
 */
package com.aawhere.doc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.Nonnull;

import org.slf4j.LoggerFactory;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

import com.google.inject.Singleton;

/**
 * Inspects the contents of a stream and produces the appropriate document if available.
 * 
 * @author roller
 * 
 */
@Singleton
public class DocumentFactory {

	private static DocumentFactory instance;

	private List<DocumentProducer> producers;
	private HashMap<DocumentFormatKey, DocumentProducer> producersByFormat = new HashMap<DocumentFormatKey, DocumentProducer>();

	static {

		instance = new DocumentFactory();
	}

	/**
	 * Used to construct all instances of DocumentFactory.
	 */
	public static class Builder
			extends ObjectBuilder<DocumentFactory> {

		public Builder() {
			super(new DocumentFactory());
		}

		public Builder register(DocumentProducer producer) {
			building.register(producer);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public DocumentFactory build() {

			return super.build();

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotEmpty(building.producers);
			super.validate();
		}
	}// end Builder

	/**
	 * @deprecated this is going private
	 */
	@Deprecated
	public DocumentFactory() {
		this.producers = new ArrayList<>();
	}

	/**
	 * @deprecated converted to Guice Singleton.
	 * @return
	 */
	@Nonnull
	@Deprecated
	public static DocumentFactory getInstance() {
		return instance;
	}

	/**
	 * Provides the ability for the Factory to make attempts for the producer to produce a
	 * {@link Document}.
	 * 
	 * 
	 * @deprecated this will be private soon.
	 * 
	 * @param textDocumentProducer
	 */
	@Deprecated
	public DocumentFactory register(@Nonnull DocumentProducer documentProducer) {

		DocumentFormatKey[] formats = documentProducer.getDocumentFormatsSupported();
		for (int whichFormat = 0; whichFormat < formats.length; whichFormat++) {
			DocumentFormatKey documentFormatKey = formats[whichFormat];
			// FIXME: Two producers that support the same format get overwritten. Use MultiMap?
			this.producersByFormat.put(documentFormatKey, documentProducer);

		}
		// although not very efficient, it doesn't happen often and will be thorough
		this.producers = new ArrayList<>(this.producersByFormat.values());
		Collections.sort(this.producers, new DocumentProducer.DocumentProducerComparator());
		return this;
	}

	/**
	 * 
	 * @param inputStream
	 * @return The document recognized. It will not return null, but rather throw
	 *         {@link DocumentContentsNotRecognizedException} to communicate no document was able to
	 *         be created.
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 * @throws DocumentFormatNotRegisteredException
	 */
	@Nonnull
	public Document getDocument(@Nonnull Object uncompressedData) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {

		return getDocument(DocumentFormatKey.UNKNOWN, uncompressedData);
	}

	/**
	 * Produces a document when the format is known. If the format is
	 * {@link DocumentFormatKey#UNKNOWN} this will iterate through all producers searching for a
	 * match.
	 * 
	 * This is a much more efficient and reliable way to get a document if you know the format. It
	 * will remove confusion as to which {@link DocumentProducer} will be used and is much more
	 * efficient.
	 * 
	 * @param format
	 *            the expected format
	 * @param uncompressedData
	 * @return
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentDecodingException
	 * @throws DocumentFormatNotRegisteredException
	 * @throws NullPointerException
	 *             when a format is not registered for the key given
	 */
	@SuppressWarnings("unchecked")
	@Nonnull
	public <T extends Document> T getDocument(@Nonnull DocumentFormatKey format, @Nonnull Object uncompressedData)
			throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		Iterator<DocumentProducer> producerIterator = this.producers.iterator();
		T document = null;

		if (uncompressedData instanceof String) {
			uncompressedData = ((String) uncompressedData).getBytes();
		}

		if (format.equals(DocumentFormatKey.UNKNOWN)) {
			while (producerIterator.hasNext() && document == null) {
				DocumentProducer producer = producerIterator.next();
				try {
					document = (T) producer.produce(uncompressedData);
				} catch (DocumentContentsNotRecognizedException e) {
					// fine, we'll try the next one
				}
			}

			if (document == null) {
				throw new DocumentContentsNotRecognizedException();
			}
		} else { // format is known
			final DocumentProducer documentProducer = this.producersByFormat.get(format);
			if (documentProducer == null) {
				throw new DocumentFormatNotRegisteredException(format);
			}
			document = (T) documentProducer.produce(uncompressedData);
		}
		return document;
	}
}
