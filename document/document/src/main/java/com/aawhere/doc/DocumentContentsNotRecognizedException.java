/**
 * 
 */
package com.aawhere.doc;

import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

/**
 * Indicates that the content provided is not parseable by a certain document.
 * 
 * @author roller
 * 
 */
@StatusCode(HttpStatusCode.UNSUPPORTED_TYPE)
public class DocumentContentsNotRecognizedException
		extends DocumentDecodingException {

	private static final long serialVersionUID = 1391443295401463999L;

	/**
	 * 
	 */
	public DocumentContentsNotRecognizedException() {
	}

	public DocumentContentsNotRecognizedException(Exception cause) {
		super(cause);
	}

}
