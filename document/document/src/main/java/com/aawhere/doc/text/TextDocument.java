/**
 *
 */
package com.aawhere.doc.text;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFormatKey;

/**
 * A really basic document that just transports the contents of a simple text document.
 * 
 * @author roller
 * 
 */
public class TextDocument
		implements Document {

	private static final long serialVersionUID = -8528787646328332930L;
	private String contents;
	private DocumentFormatKey documentFormatKey = DocumentFormatKey.TEXT;

	public TextDocument(String contents) {
		this.contents = contents;
	}

	public TextDocument(String contents, DocumentFormatKey documentFormatKey) {
		this.contents = contents;
		this.documentFormatKey = documentFormatKey;
	}

	@Override
	public String toString() {
		return this.contents;
	}

	public String getContents() {

		return this.contents;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		return getContents().getBytes();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return this.documentFormatKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.contents == null) ? 0 : this.contents.hashCode());
		return result;
	}

	/**
	 * Compares the contents for equality.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextDocument other = (TextDocument) obj;
		if (this.contents == null) {
			if (other.contents != null)
				return false;
		} else if (!this.contents.equals(other.contents))
			return false;
		return true;
	}

}
