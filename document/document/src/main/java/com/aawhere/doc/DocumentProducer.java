/**
 * 
 */
package com.aawhere.doc;

import java.io.ByteArrayInputStream;
import java.util.Comparator;

import javax.annotation.Nonnull;

import com.aawhere.lang.ComparatorResult;

/**
 * Independently inspects a stream, string or other data input and determines if it can understand
 * the contents provided. If it does understand it will create a document given the data input it
 * claims to recognize.
 * 
 * @author roller
 * 
 */
public interface DocumentProducer {

	/**
	 * Used by producers to indicate their order in discovery. This will help prioritize popular
	 * formats in addition to making sure general formats that accept practically anything are last
	 * in the list.
	 * 
	 * Order of the declarations matters since they will be sorted by this order.
	 * 
	 * @see Enum#compareTo(Enum)
	 * @see Enum#ordinal()
	 * 
	 * @author Aaron Roller
	 * 
	 */
	static enum DiscoveryOrder {

		FIRST, HIGH, MEDIUM, LOW, LAST;

	}

	/**
	 * Given the data Input this will create a document containing the contents provided in the
	 * dataInput. The data Input can be different for every type of producer, however, a
	 * {@link ByteArrayInputStream} is the ideal data type for efficient use from database storage.
	 * 
	 * TODO: Determine if throwing exceptions will slow down processing when many formats are
	 * available.
	 * 
	 * @param dataInput
	 * @return
	 * @throws DocumentDecodingException
	 *             when the dataINput is recognized, but some other problem was identified during
	 *             the decoding of the Input. It is o.k. to throw a more specific exception to
	 *             explain more details about the decoding problems.
	 * @throws DocumentContentsNotRecognizedException
	 *             when this producer does not understand the dataInput.
	 */
	@Nonnull
	public Document produce(@Nonnull Object dataInput) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException;

	/**
	 * Simply claims to support a specific format for quick production when the format is known.
	 * 
	 * @return all the formats supported
	 */
	@Nonnull
	public DocumentFormatKey[] getDocumentFormatsSupported();

	/** Declaration for each format to determine their priority in the discovery process. */
	public DiscoveryOrder getDiscoveryOrder();

	public static class DocumentProducerComparator
			implements Comparator<DocumentProducer> {

		/*
		 * (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(DocumentProducer left, DocumentProducer right) {

			int result = left.getDiscoveryOrder().compareTo(right.getDiscoveryOrder());

			return result;
		}

	}

}
