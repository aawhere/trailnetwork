/**
 *
 */
package com.aawhere.doc;

import static com.aawhere.activity.ActivityWebApiUri.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.web.api.geo.GeoWebApiUri;
import com.aawhere.ws.rs.MediaTypes;
import com.aawhere.xml.XmlNamespace;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.net.MediaType;

/**
 * A global declaration of supported types by the system. Each implementing {@link DocumentProducer}
 * should provide one or more of these to indicate the formats they support and can produce.
 * 
 * @author Aaron Roller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public enum DocumentFormatKey {

	/** http://www.topografix.com/gpx */
	GPX_1_1(APPLICATION_GPX_XML, GPX_EXTENSION), GPX_1_0(APPLICATION_GPX_XML, GPX_EXTENSION),
	/** a simple format mostly used for demonstration and testing */
	TEXT("text/plain", "txt"),
	/** Garmin specific Activity XML */
	AXM_1_0("application/axm+xml", "axm"),
	/**
	 * General Comma Separated Values file. http://www.rfc-editor.org/rfc/rfc4180.txt
	 */
	ACTIVITY_CSV("text/csv", "csv"),
	/** The contents aren't recognized or not yet known */
	UNKNOWN("application/octet-stream", "bin"),
	/** Every trail activity document */
	ETX("application/vnd.every-trail.trip+xml", "xml"),
	/** every trail search document */
	ETSX("application/vnd.every-trail.trip-search+xml", "xml"),
	/** Every trail user document */
	ETUX("application/vnd.every-trail.user+xml", "xml"),
	/**
	 * When other parsers fail to identify exactly what type of GPX, this may be found by a relaxed
	 * parser.
	 */
	GPX(APPLICATION_GPX_XML, GPX_EXTENSION),
	/** Map my fitness workout JSON v7 */
	MMFW7,
	/** Map my fitness user JSON v7 */
	MMFU7,
	/** Map My Fitness User Workouts JSON v7 */
	MMFUW7,
	/** Map My Fitness Routes JSON v7 */
	MMFR7,
	/** GPolylines are a google map encoded version of a track document. */
	GPOLYLINE(GeoWebApiUri.POLYLINE_ENCODED.toString(), GeoWebApiUri.GPOLYLINE),
	/** JSON provided by Garmin Connect explore/search endpoints. */
	GC_SEARCH_JSON,
	/** a Standard map thumbnail for some entity. Trailhead, Route, Activity, Person, etc. */
	MAP_THUMBNAIL_PNG(MediaType.PNG.toString(), MediaTypes.PNG),
	/** Ride with GPS User Activities. */
	RW_USER_ACTIVITIES_JSON,
	/** Ride with GPS Produces a single Activity. */
	RW_TRIP_JSON,
	/** Ride with GPS Account info */
	RW_USER_JSON;

	private final int TYPES_SIZE = 2;
	private final int MEDIA_IDX = 0;
	private final int EXT_IDX = 1;

	private final String[] types = new String[TYPES_SIZE];

	private DocumentFormatKey() {
	}

	private DocumentFormatKey(String mediaType, String extension) {
		this.types[MEDIA_IDX] = mediaType;
		this.types[EXT_IDX] = extension;
	}

	public String getMediaType() {
		return types[MEDIA_IDX];
	}

	public String getExtension() {
		return types[EXT_IDX];
	}

	public static List<DocumentFormatKey> valueList() {
		return Arrays.asList(values());
	}

	public static Collection<String> extensions() {
		return Lists.transform(valueList(), new DocumentFormatKeyExtensionFunction());
	}

	public static DocumentFormatKey byExtension(String extension) {
		return CollectionUtilsExtended.firstOrNull(

		Collections2.filter(valueList(), new DocumentFormatKeyExtensionPredicate(extension)));

	}
}
