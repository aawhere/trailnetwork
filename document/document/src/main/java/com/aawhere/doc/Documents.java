/**
 * 
 */
package com.aawhere.doc;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ClassUtils;

import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.lang.ObjectBuilder;

/**
 * Represents multiple {@link Document}s. Documents are indexed by their available interfaces,
 * class, and superclasses (which must extend {@link Document}) and are retrievable using
 * {@link #getDocument(Class)}. If one document implements multiple {@link Document} interfaces, it
 * may be retrieved multiple times. Each interface may be mapped to only one {@link Document}.
 * 
 * @author Brian Chapman
 * 
 */
public class Documents {

	/**
	 * Used to construct all instances of ArchiveDocuments.
	 */
	public static class Builder
			extends ObjectBuilder<Documents> {

		public Builder() {
			super(new Documents());
		}

		public Builder addDocuments(ArchivedDocument[] docs) {
			return addDocuments(Arrays.asList(docs));
		}

		public Builder addDocuments(Collection<ArchivedDocument> docs) {
			for (ArchivedDocument document : docs) {
				addDocument(document);
			}
			return this;
		}

		/**
		 * Adds the document to the list of available documents.
		 * 
		 * @param document
		 * @return
		 */
		@SuppressWarnings("unchecked")
		public Builder addDocument(ArchivedDocument archivedDocument) {
			Document document = archivedDocument.getDocument();
			List<Class<?>> types = (List<Class<?>>) ClassUtils.getAllInterfaces(document.getClass());
			types.add(document.getClass());
			types.addAll(ClassUtils.getAllSuperclasses(document.getClass()));
			for (Class<?> type : types) {
				if (type != Document.class) {
					if (Document.class.isAssignableFrom(type)) {
						ArchivedDocument previous = building.documents.put(	(Class<? extends Document>) type,
																			archivedDocument);
						if (previous != null) {
							throw new IllegalArgumentException(
									"Each interface may only be mapped to one Document. Found " + previous
											+ " with more than one reference for " + type.getName() + " of "
											+ document.getClass());
						}
					}
				}
			}
			return this;
		}
	}// end Builder

	private Map<Class<? extends Document>, ArchivedDocument> documents = new HashMap<Class<? extends Document>, ArchivedDocument>();

	/** Use {@link Builder} to construct ArchiveDocuments */
	private Documents() {
	}

	/**
	 * Retrieve
	 * 
	 * @param type
	 * @return
	 * @throws DocumentNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public <DocT extends Document> DocT getDocument(Class<DocT> type) throws DocumentNotFoundException {
		return (DocT) getArchivedDocument(type).getDocument();
	}

	public ArchivedDocument getArchivedDocument(Class<? extends Document> type) throws DocumentNotFoundException {
		ArchivedDocument doc = documents.get(type);
		if (doc == null) {
			throw new DocumentNotFoundException(type);
		} else {
			return doc;
		}
	}

}
