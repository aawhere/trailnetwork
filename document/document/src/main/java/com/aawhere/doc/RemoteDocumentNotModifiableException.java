/**
 * 
 */
package com.aawhere.doc;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates that does not offer the ability to retrieve updates of a document. This may not mean
 * that the document is not different, but rather that the system may not follow a protocol that
 * allows for our system to know if the document is updated (without inspecting the entire
 * document).
 * 
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.BAD_METHOD)
public class RemoteDocumentNotModifiableException
		extends BaseException {
	private static final long serialVersionUID = 3031319732708005212L;

	/**
	 * 
	 */
	public RemoteDocumentNotModifiableException(ApplicationDataId id) {
		this(id.getValue());
	}

	public RemoteDocumentNotModifiableException(String path) {
		super(CompleteMessage.create(DocumentMessage.REMOTE_DOC_NOT_MODIFIABLE)
				.addParameter(DocumentMessage.Param.PATH, path).build());
	}
}
