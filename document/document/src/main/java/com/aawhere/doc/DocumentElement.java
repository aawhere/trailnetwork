/**
 * 
 */
package com.aawhere.doc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Most of the documents are XML documents that are generated using JAXB and XJC. This interface
 * indicates a class is a part of a Document.
 * 
 * 
 * @author Aaron Roller
 * 
 */
public interface DocumentElement<DocumentRootT, DocumentElementT> {

	/**
	 * This is the actual Document Element annotated with {@link XmlType}.
	 * 
	 * @return
	 */
	DocumentElementT getDocumentElement();

	/**
	 * The top level element annotated with {@link XmlRootElement} that will contain the
	 * {@link #getDocumentElement()}.
	 * 
	 * @return
	 */
	JAXBElement<DocumentRootT> toDocumentRoot();
}
