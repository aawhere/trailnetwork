/**
 *
 */
package com.aawhere.doc.archive;

import java.util.List;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Repository;

/**
 * Provides a {@link Repository} for {@link ArchivedDocument}s.
 * 
 * @author Aaron Roller
 * 
 */
public interface ArchivedDocumentRepository
		extends Repository<ArchivedDocument, ArchivedDocumentId> {

	List<ArchivedDocument> findByApplicationDataId(String applicationDataId, DocumentFormatKey format);

	Iterable<ArchivedDocument> findByApplicationDataIds(Iterable<String> applicationDataId, DocumentFormatKey format);

	ArchivedDocument update(ArchivedDocumentId id, Document document) throws EntityNotFoundException;
}
