@XmlSchema(xmlns = { @XmlNs(prefix = com.aawhere.xml.XmlNamespace.API_PREFIX,
		namespaceURI = com.aawhere.xml.XmlNamespace.API_VALUE) })
@XmlJavaTypeAdapters({
		@XmlJavaTypeAdapter(value = com.aawhere.util.rb.MessagePersonalizedXmlAdapter.class,
				type = com.aawhere.util.rb.Message.class),
		@XmlJavaTypeAdapter(value = com.aawhere.joda.time.xml.DateTimeXmlAdapter.class,
				type = org.joda.time.DateTime.class) })
package com.aawhere.doc.archive;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

