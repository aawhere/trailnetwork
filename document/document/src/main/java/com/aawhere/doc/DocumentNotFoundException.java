/**
 * 
 */
package com.aawhere.doc;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates that the document you were looking for could not be found.
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(HttpStatusCode.NOT_FOUND)
public class DocumentNotFoundException
		extends BaseException {

	private static final long serialVersionUID = 8255491279200620581L;

	/**
	 * @param type
	 * 
	 */
	public DocumentNotFoundException(Class<? extends Document> type) {
		super(CompleteMessage.create(DocumentMessage.MISSING_TYPE).addParameter(DocumentMessage.Param.TYPE, type)
				.build());
	}

}
