/**
 *
 */
package com.aawhere.doc.archive;

import java.io.Serializable;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.ApplicationDataIdToStringFunction;
import com.aawhere.app.ApplicationKey;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.io.zip.GzipCompression;
import com.aawhere.lang.Assertion;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.LongBaseEntity;
import com.aawhere.xml.XmlNamespace;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;

/**
 * Prepares any document for storage. This may be built by the builder providing the document or it
 * may be populated from a persistence framework. There is purposely no way to build this from
 * encoded data without injecting it into the field.
 * 
 * TODO:Support Uncompressed Data since overhead of Gzipping small files is bad.
 * http://code.google.com/speed/page-speed/docs/payload.html
 * 
 * Note that gzipping is only beneficial for larger resources. Due to the overhead and latency of
 * compression and decompression, you should only gzip files above a certain size threshold; we
 * recommend a minimum range between 150 and 1000 bytes. Gzipping files below 150 bytes can actually
 * make them larger.
 * 
 * 
 * @see GZIPOutputStream
 * @see GZIPInputStream
 * 
 * @author Aaron Roller
 * 
 */
@Unindex
@Entity
@XmlType(namespace = XmlNamespace.API_VALUE, name = "document")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Dictionary(domain = ArchivedDocument.FIELD.DOMAIN)
@Cache
public class ArchivedDocument
		extends LongBaseEntity<ArchivedDocument, ArchivedDocumentId> {

	private static final long serialVersionUID = -6436941193737751882L;

	/**
	 * Used to construct all instances of DocumentCompressor.
	 */
	@XmlTransient
	public static class Builder
			extends LongBaseEntity.Builder<ArchivedDocument, Builder, ArchivedDocumentId> {

		public DocumentFactory documentFactory;
		private Object uncompressedData;

		public Builder() {
			this(new ArchivedDocument());

		}

		/** service mutator */
		Builder(ArchivedDocument archivedDocument) {
			super(archivedDocument);

		}

		public Builder setDocument(Document document) {
			building.document = document;
			return this;
		}

		public Builder setUncompressedData(Object uncompressedData) {
			this.uncompressedData = uncompressedData;
			// encoded date is created by the builder from uncompressed data
			// this is needed for mutation
			building.encodedData = null;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public ArchivedDocument build() {

			if (building.encodedData == null) {
				if (uncompressedData == null) {
					uncompressedData = building.document.toByteArray();
				} else if (!(uncompressedData instanceof byte[])) {
					throw new IllegalArgumentException(
							"This method currently does not support non-byte array data. this may change in the "
									+ "future if we need to handle binary formats.");
				}
				long start = System.currentTimeMillis();
				building.encodedData = new GzipCompression.Builder().build().compress((byte[]) uncompressedData);
				building.format = building.document.getDocumentFormat();
				building.numberofBytes = ((byte[]) uncompressedData).length;
				building.numberOfBytesEncoded = building.encodedData.length;
				long duration = System.currentTimeMillis() - start;
				if (duration > 500) {
					LoggerFactory.getLogger(getClass()).warning(duration + "ms to compress data "
							+ building.applicationDataId);
				}
			}
			return super.build();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("format", building.format);
		}

		/**
		 * @param applicationDataId
		 * @return
		 */
		public Builder setApplicationDataId(ApplicationDataId applicationDataId) {
			building.applicationDataId = new ApplicationDataIdToStringFunction().apply(applicationDataId);
			return this;
		}

		public Builder setApplicationDataId(String applicationDataId) {
			building.applicationDataId = applicationDataId;
			return this;
		}

	}// end Builder

	static public Builder create() {
		return new Builder();
	}

	static Builder mutate(ArchivedDocument old) {
		return new Builder(old);
	}

	/**
	 * Used to declare incompatible changes that will cause the Uncompressor to handle it
	 * differently. See header
	 */
	@XmlType(namespace = XmlNamespace.API_VALUE, name = "documentEncoding")
	@XmlEnum
	static enum Encoding {
		GZIP
	}

	private byte[] encodedData;

	/**
	 * The built document representation. There is no guarantee that the document will be
	 * {@link Serializable} so we mark it transient here. This document must be prepared by an
	 * external service at this point.
	 * 
	 * @see ArchivedDocumentUtil#complete(ArchivedDocument, DocumentFactory)
	 */
	@Ignore
	private transient Document document;

	/**
	 * The compressed private byte[] bytes;
	 * 
	 * /** Indicates the version of compression used in case it changes. Hard coded unless the
	 * compressor allows enough configurability to provide different Formats.
	 */
	private Encoding encoding = Encoding.GZIP;

	@com.aawhere.field.annotation.Field(key = FIELD.DOCUMENT_FORMAT_KEY)
	private DocumentFormatKey format;

	private Integer numberofBytes;

	private Integer numberOfBytesEncoded;

	/**
	 * The {@link ApplicationKey} and remote id of the data item that uiniquely identifies it in the
	 * origin {@link Application} combined as a string making it unique within the system (when
	 * combined with the {@link #format}.
	 * 
	 */
	// See TN-367 for why this index exists.
	@Index
	@com.aawhere.field.annotation.Field(key = FIELD.APPLICATION_DATA_ID, indexedFor = "TN-367")
	private String applicationDataId;

	/** Use {@link Builder} to construct DocumentCompressor */
	private ArchivedDocument() {
	};

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@com.aawhere.field.annotation.Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM)
	@Override
	public ArchivedDocumentId getId() {
		return super.getId();
	}

	public Document getDocument() {
		// the document is transient so it must be built

		return this.document;
	}

	byte[] getEncodedData() {
		return this.encodedData;
	}

	public Encoding getEncoding() {
		return this.encoding;
	}

	public DocumentFormatKey getFormat() {
		return this.format;
	}

	/**
	 * @return the applicationDataId
	 */
	public String getApplicationDataId() {
		return this.applicationDataId;
	}

	public Integer getNumberofBytes() {
		return this.numberofBytes;
	}

	public Integer getNumberOfBytesEncoded() {
		return this.numberOfBytesEncoded;
	}

	@XmlTransient
	public static final class FIELD {
		public static final String DOMAIN = "doc";
		public static final String APPLICATION_DATA_ID = "applicationDataId";
		public static final String DOCUMENT_FORMAT_KEY = "format";

		public static final class KEY {
			public static final FieldKey APPLICATION_DATA_ID = new FieldKey(DOMAIN, FIELD.APPLICATION_DATA_ID);
			public static final FieldKey ID = new FieldKey(DOMAIN, BaseEntity.BASE_FIELD.ID);
			public static final FieldKey DOCUMENT_FORMAT_KEY = new FieldKey(DOMAIN, FIELD.DOCUMENT_FORMAT_KEY);
		}
	}

}
