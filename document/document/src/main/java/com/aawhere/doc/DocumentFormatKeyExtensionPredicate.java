/**
 * 
 */
package com.aawhere.doc;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

/**
 * A predicate used to look for the {@link DocumentFormatKey#getExtension()} using
 * {@link Collections2} and such.
 * 
 * @author aroller
 * 
 */
public class DocumentFormatKeyExtensionPredicate
		implements Predicate<DocumentFormatKey> {

	private String targetExtension;

	/**
	 * @param targetExtension
	 *            the extension that you are looking for.
	 */
	public DocumentFormatKeyExtensionPredicate(String targetExtension) {
		super();
		this.targetExtension = targetExtension;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(@Nullable DocumentFormatKey input) {
		return input != null && targetExtension.equalsIgnoreCase(input.getExtension());
	}

}
