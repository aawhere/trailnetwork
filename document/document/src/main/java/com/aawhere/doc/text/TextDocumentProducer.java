/**
 *
 */
package com.aawhere.doc.text;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentProducer;
import com.aawhere.io.InputStreamReader;
import com.aawhere.lang.ObjectBuilder;

/**
 * The simplest of producers this will convert the results into a string.
 * 
 * @author roller
 * 
 */
public class TextDocumentProducer
		implements DocumentProducer {

	/**
	 * Used to construct all instances of TextDocumentProducer.
	 */
	public static class Builder
			extends ObjectBuilder<TextDocumentProducer> {

		public Builder() {
			super(new TextDocumentProducer());
		}

		public Builder setDiscoveryOrder(DiscoveryOrder order) {
			building.order = order;
			return this;
		}

		public Builder setFormats(DocumentFormatKey... formats) {
			building.formats = formats;
			return this;
		}

	}// end Builder

	/**
	 * @return
	 */
	public static DocumentProducer create() {
		return new Builder().build();
	}

	/** Registers with the {@link DocumentFactory}. */
	public static void register() {
		DocumentFactory.getInstance().register(create());
	}

	private DiscoveryOrder order = DiscoveryOrder.LAST;
	private DocumentFormatKey[] formats = new DocumentFormatKey[] { DocumentFormatKey.TEXT };

	/** Use {@link Builder} to construct TextDocumentProducer */
	private TextDocumentProducer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {
		return order;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	public DocumentFormatKey[] getDocumentFormatsSupported() {

		return formats;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	public Document produce(Object dataInput) throws DocumentDecodingException, DocumentContentsNotRecognizedException {
		TextDocument document;
		if (dataInput instanceof String) {
			document = new TextDocument((String) dataInput);
		} else if (dataInput instanceof InputStream) {
			InputStream inputStream = (InputStream) dataInput;
			try {
				InputStreamReader reader = new InputStreamReader(inputStream);
				String contents = reader.getString();
				document = createDocument(contents);
			} catch (IOException e) {
				throw new DocumentDecodingException();
			}
		} else if (dataInput instanceof byte[]) {
			String contents = new String((byte[]) dataInput);
			if (StringUtils.isAsciiPrintable(contents)) {
				document = createDocument(contents);
			} else {
				throw new DocumentContentsNotRecognizedException();
			}
		} else {
			throw new DocumentContentsNotRecognizedException();
		}

		return document;

	}

	/**
	 * Given the contents
	 * 
	 * @param contents
	 * @return
	 */
	protected TextDocument createDocument(String contents) {
		return new TextDocument(contents);
	}
}
