/**
 *
 */
package com.aawhere.doc;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;

import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;

/**
 * Base class providing common features for any document that uses {@link JAXB} generated classes.
 * 
 * @author Aaron Roller
 * 
 */
public abstract class JaxbDocument<RootT>
		implements Document {

	private static final long serialVersionUID = -7012465126990384248L;

	/**
	 * Used to construct all instances of JaxbDocument.
	 */
	public static class Builder<D extends JaxbDocument<RootT>, RootT, B extends Builder<D, RootT, B>>
			extends AbstractObjectBuilder<D, B> {

		public Builder(D document) {
			super(document);
		}

		public B setRoot(RootT root) {
			((JaxbDocument<RootT>) building).root = root;
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull(((JaxbDocument<RootT>) building).root);
			super.validate();
		}
	}// end Builder

	private RootT root;

	/** Use {@link Builder} to construct JaxbDocument */
	protected JaxbDocument() {
	}

	/**
	 * @return the root
	 */
	final public RootT getRoot() {
		return this.root;
	}

	/**
	 * Sometimes XJC generates root objects without the {@link XmlRootElement} annotion.
	 * 
	 * This will return the object that ensures the XmlRootElement is available. Children should
	 * override and use their factory method to provide the {@link JAXBElement} wrapping the root.
	 * 
	 * @return either the root itself or a {@link JAXBElement} wrapping the root.
	 */
	public Object getRootElement() {
		return root;
	}

	/**
	 * Marshalls the GPX type back into a String so it can produce the required bytes and be useful
	 * for debugging.
	 * 
	 * NOTE: String may be heavier than just going directly to bytes and cause performance problems
	 * as the system goes live.
	 * 
	 * @see http 
	 *      ://stackoverflow.com/questions/1023701/any-suggestion-on-how-to-improve-the-performance
	 *      -of-a-java-string-to-byte-conv
	 * 
	 * @param gpxType
	 * @return
	 * @throws DocumentDecodingException
	 */
	byte[] marshallToBytes() {
		try {

			JAXBContext jc = JAXBContext.newInstance(getObjectFactoryClass());
			// Create marshaller
			Marshaller m = jc.createMarshaller();
			// Marshal object into file.
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			m.marshal(getRootElement(), byteStream);
			return byteStream.toByteArray();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Provides the class of the ObjectFactory generated by XJC.
	 * 
	 * @return
	 */
	protected abstract Class<?> getObjectFactoryClass();

	@Override
	public byte[] toByteArray() {
		return marshallToBytes();
	}

}
