/**
 * 
 */
package com.aawhere.doc;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;

/**
 * Indicates the inability to properly decode a document.
 * 
 * The document was likely recognized by the
 * {@link DocumentProducer#understands(java.io.InputStream)} but it was unable to decode during the
 * construction of the document.
 * 
 * @author roller
 * 
 */
@StatusCode(HttpStatusCode.UNPROCESSABLE_ENTITY)
public class DocumentDecodingException
		extends BaseException {

	private static final long serialVersionUID = 3304722883813781853L;

	/**
	 * 
	 */
	public DocumentDecodingException() {
		this(DocumentMessage.GENERAL_MISUNDERSTANDING);
	}

	public DocumentDecodingException(BaseException e) {
		super(e);
	}

	public DocumentDecodingException(Exception e) {
		super(DocumentMessage.GENERAL_MISUNDERSTANDING, e);
	}

	public DocumentDecodingException(CompleteMessage message) {
		super(message);
	}

	protected DocumentDecodingException(Message message) {
		super(message);
	}

	/**
	 * @param version
	 * @param version2
	 * @return
	 */
	public static DocumentDecodingException unexpectedValue(String expected, String actual) {
		return new DocumentDecodingException(new CompleteMessage.Builder(DocumentMessage.UNEXPECTED)
				.addParameter(DocumentMessage.Param.EXPECTED, expected)
				.addParameter(DocumentMessage.Param.UNEXPECTED, actual).build());
	}
}
