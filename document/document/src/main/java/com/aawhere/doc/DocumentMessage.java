/**
 *
 */
package com.aawhere.doc;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * I18N messages for document parsing, etc.
 * 
 * @author Aaron Roller
 * 
 */
public enum DocumentMessage implements Message {

	/** When a document format isn't found in the system. */
	FORMAT_NOT_REGISTERED("The expected format {FORMAT_KEY} is not found.", Param.FORMAT_KEY),
	/**
	 * Used to indicate the external identifier at the remote web app hosting the desired document
	 * represented by the given path
	 */
	REMOTE_DOC_NOT_FOUND("The document requested was not found: {PATH}", Param.PATH),
	/** @see DocumentNotFoundException */
	MISSING_TYPE("Unable to find a document of type {TYPE}.", Param.TYPE),
	/**
	 * Used to indicate the external identifier at the remote web app hosting the desired document
	 * represented by the given path is not available (private or not public).
	 */
	REMOTE_DOC_NOT_PUBLIC("The document requested was not available: {PATH}", Param.PATH), MALFORMED(
			"The document is a {FORMAT_KEY} document, however, our parsers failed to understand the details.",
			Param.FORMAT_KEY),
	/** */
	GENERAL_MISUNDERSTANDING("The document was provided in a format not understood by our parsers"),

	/** General message to indicate inequality occurred. */
	UNEXPECTED("The document expected {EXPECTED}, but was {UNEXPECTED}", Param.EXPECTED, Param.UNEXPECTED),
	/** @see RemoteDocumentNotUpdatedException */
	REMOTE_DOC_NOT_MODIFIED("The document requested is already up-to-date: {PATH}", Param.PATH),
	/**
	 * Indicates that the document requested does not report if it's modified.
	 * 
	 * @see RemoteDocumentNotModifiableException
	 * 
	 */
	REMOTE_DOC_NOT_MODIFIABLE("The document requested is not modifiable: {PATH}", Param.PATH);

	private ParamKey[] parameterKeys;
	private String message;

	private DocumentMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** the actual {@link DocumentFormatKey} */
		FORMAT_KEY, PATH,
		/** explains what actual was provided */
		UNEXPECTED,
		/** explains what was wanted. */
		EXPECTED, TYPE;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
