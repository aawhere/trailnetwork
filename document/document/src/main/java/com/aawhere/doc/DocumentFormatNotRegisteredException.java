/**
 * 
 */
package com.aawhere.doc;

import com.aawhere.lang.exception.BaseException;

/**
 * Used to indicate somemone is looking for a format that is not yet registered in the system.
 * 
 * @see DocumentFactory#register(DocumentProducer)
 * 
 * @author Aaron Roller
 * 
 */
public class DocumentFormatNotRegisteredException
		extends BaseException {

	private static final long serialVersionUID = 6136966210828602462L;

	/**
	 * 
	 */
	public DocumentFormatNotRegisteredException(DocumentFormatKey formatKey) {
		super(DocumentMessage.FORMAT_NOT_REGISTERED, DocumentMessage.Param.FORMAT_KEY, formatKey);
	}
}
