/**
 * 
 */
package com.aawhere.doc;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates that a remote document was requested, but it is not out-dated.
 * 
 * @author aroller
 * 
 */
@StatusCode(HttpStatusCode.NOT_MODIFIED)
public class RemoteDocumentNotModifiedException
		extends BaseException {

	private static final long serialVersionUID = 1510488985970544295L;

	public RemoteDocumentNotModifiedException(String path) {
		super(new CompleteMessage.Builder(DocumentMessage.REMOTE_DOC_NOT_MODIFIED)
				.addParameter(DocumentMessage.Param.PATH, path).build());
	}
}
