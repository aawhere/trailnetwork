/**
 * 
 */
package com.aawhere.doc;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * A document that does the bare minimum dealing with only bytes.
 * 
 * @author aroller
 * 
 */
public class SimpleDocument
		implements Document {

	private static final long serialVersionUID = 4957005964170109618L;

	private byte[] bytes;
	private DocumentFormatKey format;

	/**
	 * Used to construct all instances of SimpleDocument.
	 */
	public static class Builder
			extends ObjectBuilder<SimpleDocument> {

		public Builder() {
			super(new SimpleDocument());
		}

		public Builder setBytes(byte[] bytes) {
			building.bytes = bytes;
			return this;
		}

		public Builder setDocumentFormat(DocumentFormatKey format) {
			building.format = format;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("format", building.format);
			Assertion.exceptions().notNull("bytes", building.bytes);

		}
	}// end Builder

	/** Use {@link Builder} to construct SimpleDocument */
	private SimpleDocument() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#toByteArray()
	 */
	@Override
	public byte[] toByteArray() {
		return bytes;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.Document#getDocumentFormat()
	 */
	@Override
	public DocumentFormatKey getDocumentFormat() {
		return format;
	}

}
