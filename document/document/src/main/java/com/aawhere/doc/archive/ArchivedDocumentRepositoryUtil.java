/**
 * 
 */
package com.aawhere.doc.archive;

import com.aawhere.doc.Document;
import com.aawhere.persist.EntityNotFoundException;

/**
 * Does the work for the repository so the implementors don't have to.
 * 
 * @author aroller
 * 
 */
class ArchivedDocumentRepositoryUtil {

	public static ArchivedDocument mutate(ArchivedDocumentId id, Document mutatedDocument,
			ArchivedDocumentRepository repository) throws EntityNotFoundException {
		return ArchivedDocument.mutate(repository.load(id)).setDocument(mutatedDocument).setUncompressedData(null)
				.build();
	}
}
