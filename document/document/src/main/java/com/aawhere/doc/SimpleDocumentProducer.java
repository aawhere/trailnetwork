/**
 * 
 */
package com.aawhere.doc;

import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Nonnull;

import org.apache.commons.io.IOUtils;

/**
 * @author aroller
 * 
 */
public class SimpleDocumentProducer
		implements DocumentProducer {

	private DocumentFormatKey[] formatKeys = {};

	/**
	 * 
	 */
	public SimpleDocumentProducer() {

	}

	public SimpleDocumentProducer(DocumentFormatKey... formatKeys) {
		this.formatKeys = formatKeys;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
	 */
	@Override
	@Nonnull
	public Document produce(@Nonnull Object dataInput) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException {
		byte[] bytes;
		if (dataInput instanceof byte[]) {
			bytes = (byte[]) dataInput;
		} else if (dataInput instanceof InputStream) {
			try {
				bytes = IOUtils.toByteArray((InputStream) dataInput);
			} catch (IOException e) {
				throw new DocumentDecodingException(e);
			}
		} else {
			throw new DocumentContentsNotRecognizedException();
		}
		final DocumentFormatKey format = (this.formatKeys.length > 0) ? this.formatKeys[0] : DocumentFormatKey.UNKNOWN;
		return new SimpleDocument.Builder().setBytes(bytes).setDocumentFormat(format).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
	 */
	@Override
	@Nonnull
	public DocumentFormatKey[] getDocumentFormatsSupported() {

		return this.formatKeys;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
	 */
	@Override
	public DiscoveryOrder getDiscoveryOrder() {

		return DiscoveryOrder.LAST;
	}

}
