/**
 * 
 */
package com.aawhere.doc.archive;

import java.util.Map;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.doc.DocumentUtil;
import com.aawhere.io.zip.CorruptGzipFormatException;
import com.aawhere.io.zip.GzipCompression;
import com.aawhere.lang.Assertion;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

/**
 * @author aroller
 * 
 */
public class ArchivedDocumentUtil {

	/**
	 * Only the bytes are loaded. We need to create the document and assign it.
	 * 
	 * @param archivedDocument
	 * @return
	 */
	static ArchivedDocument complete(ArchivedDocument archivedDocument, DocumentFactory documentFactory) {

		Assertion.assertNotNull("archivedDocument", archivedDocument);
		try {
			byte[] encodedData = archivedDocument.getEncodedData();
			Assertion.assertNotNull("encodedData", encodedData);

			// first decompress
			byte[] decompressed = new GzipCompression.Builder().build().uncompress(encodedData);
			Document document = documentFactory.getDocument(archivedDocument.getFormat(), decompressed);
			return new ArchivedDocument.Builder(archivedDocument).setDocument(document).build();

			// this is an archive class, errors here are system errors
		} catch (DocumentDecodingException e) {
			throw new RuntimeException(e);
		} catch (CorruptGzipFormatException e) {
			throw new RuntimeException(e);
		} catch (DocumentFormatNotRegisteredException e) {
			throw new RuntimeException(e);
		}
	}

	public static Function<ArchivedDocument, ArchivedDocument> completeFunction(final DocumentFactory documentFactory) {
		return new Function<ArchivedDocument, ArchivedDocument>() {

			@Override
			public ArchivedDocument apply(ArchivedDocument input) {
				return (input == null) ? null : complete(input, documentFactory);
			}
		};
	}

	public static Function<ArchivedDocument, Document> documentFunction() {
		return new Function<ArchivedDocument, Document>() {

			@Override
			public Document apply(ArchivedDocument input) {
				if (input == null) {
					return null;
				}
				return input.getDocument();
			}
		};
	}

	public static Function<ArchivedDocument, String> textArchivedDocumentFunction() {
		return Functions.compose(DocumentUtil.textDocumentFunction(), documentFunction());
	}

	public static Function<ArchivedDocument, DocumentFormatKey> formatFunction() {
		return new Function<ArchivedDocument, DocumentFormatKey>() {

			@Override
			public DocumentFormatKey apply(ArchivedDocument input) {
				return (input == null) ? null : input.getFormat();
			}
		};
	}

	/**
	 * Given an iterable of documents this will filter those documents to include only that of the
	 * given format.
	 */
	public static Iterable<ArchivedDocument>
			filterFormat(Iterable<ArchivedDocument> documents, DocumentFormatKey format) {
		return Iterables.filter(documents, Predicates.compose(Predicates.equalTo(format), formatFunction()));
	}

	/** Completes the map without pushing it allowing asynchronicity to continue. */
	public static Map<ArchivedDocumentId, ArchivedDocument> complete(Map<ArchivedDocumentId, ArchivedDocument> all,
			DocumentFactory documentFactory) {
		return Maps.transformValues(all, completeFunction(documentFactory));
	}

	public static Function<ArchivedDocument, String> applicationDataIdFunction() {
		return new Function<ArchivedDocument, String>() {

			@Override
			public String apply(ArchivedDocument input) {
				return (input != null) ? input.getApplicationDataId() : null;
			}
		};
	}
}
