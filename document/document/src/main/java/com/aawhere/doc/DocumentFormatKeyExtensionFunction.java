/**
 * 
 */
package com.aawhere.doc;

import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;

/**
 * Simply calls the {@link DocumentFormatKey#getExtension()} to be used with {@link Collections2},
 * etc.
 * 
 * @author aroller
 * 
 */
public class DocumentFormatKeyExtensionFunction
		implements Function<DocumentFormatKey, String> {

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public String apply(@Nullable DocumentFormatKey input) {
		return input.getExtension();
	}

}
