/**
 *
 */
package com.aawhere.doc;

/**
 * Any document used to transport data.
 * 
 * @author roller
 * 
 */
public interface Document {

	/**
	 * Necessary for all documents to provide their byte version of themselves, uncompressed. This
	 * function is delegated to each document because text documents require different conversion to
	 * byte[] than a binary document.
	 * 
	 * @return the bytes, uncompressed and unencoded ready to be used to rebuild this document.
	 */
	byte[] toByteArray();

	/**
	 * Indicates the format of the document for easier production from {@link DocumentProducer}s.
	 * 
	 * @return
	 */
	DocumentFormatKey getDocumentFormat();
}
