/**
 *
 */
package com.aawhere.doc;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates the document retrieved is not publicly available (private).
 * 
 * @author Brian Chapman
 * 
 */
@StatusCode(HttpStatusCode.FORBIDDEN)
public class RemoteDocumentNotPublicException
		extends BaseException {

	private static final long serialVersionUID = 5171684670053316087L;

	/**
	 * @param path
	 *            the URL attempted to retrieve the document
	 */
	public RemoteDocumentNotPublicException(String path) {
		super(new CompleteMessage.Builder(DocumentMessage.REMOTE_DOC_NOT_PUBLIC)
				.addParameter(DocumentMessage.Param.PATH, path).build());
	}

}
