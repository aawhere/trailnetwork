/**
 * 
 */
package com.aawhere.doc.text;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.test.TestUtil;

/**
 * @author Aaron Roller
 * 
 */
public class TextDocumentTestUtil {

	/**
	 * Used to construct all instances of TextDocumentTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<TextDocumentTestUtil> {

		public Builder() {
			super(new TextDocumentTestUtil());
		}

	}// end Builder

	/** Use {@link Builder} to construct TextDocumentTestUtil */
	private TextDocumentTestUtil() {
	}

	public TextDocument getSampleDocument() {
		return new TextDocument(TestUtil.generateRandomString(false));
	}
}
