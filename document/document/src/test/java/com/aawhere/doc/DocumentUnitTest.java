/**
 * 
 */
package com.aawhere.doc;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author aroller
 * 
 */
public class DocumentUnitTest {

	@Test
	public void testFormatByExtension() {
		DocumentFormatKey expected = DocumentFormatKey.AXM_1_0;
		DocumentFormatKey actual = DocumentFormatKey.byExtension(expected.getExtension());
		assertEquals(expected, actual);
	}

	@Test
	public void testFormatByExtensionNotFound() {
		assertNull(DocumentFormatKey.byExtension("1@#%@#%@#"));
	}
}
