package com.aawhere.doc.text;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFactory;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentFormatNotRegisteredException;
import com.aawhere.test.TestUtil;

/**
 * Unit test for {@link TextDocument} and {@link DocumentFactory}'s use of text document.
 * 
 * @author roller
 * 
 */
public class TextDocumentUnitTest {

	private String contents;

	@Before
	public void setUp() {
		contents = TestUtil.generateRandomString();
		DocumentFactory.getInstance().register(new TextDocumentProducer.Builder().build());
	}

	@Test
	public void testDocument() {

		TextDocument document = new TextDocument(contents);
		assertEquals(contents, document.getContents());
	}

	@Test
	public void testStringFactory() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		testFactory(contents, contents);
	}

	@Test
	public void testFromBytes() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		byte[] bytes = contents.getBytes();
		testFactory(contents, bytes);
	}

	/**
	 * Common method used to verify the factory will produce the desired result.
	 * 
	 * @param expected
	 * @param inputData
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentFormatNotRegisteredException
	 */
	private void testFactory(String expected, Object inputData) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		DocumentFactory factory = DocumentFactory.getInstance();
		Document document = factory.getDocument(inputData);
		assertNotNull(document);
		assertTrue(document instanceof TextDocument);
		TextDocument textDocument = (TextDocument) document;
		assertEquals(expected, textDocument.getContents());
	}

	@Test
	public void testFactoryWithKnownFormat() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		TextDocument doc = DocumentFactory.getInstance().getDocument(DocumentFormatKey.TEXT, contents);
		assertEquals(contents, doc.getContents());
	}

	@Test
	public void testInputStreamFactory() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		String contents = TestUtil.generateRandomString();

		java.io.ByteArrayInputStream inputStream = new ByteArrayInputStream(contents.getBytes());
		testFactory(contents, inputStream);
	}

	@Test(expected = DocumentContentsNotRecognizedException.class)
	public void testNotRecognized() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		// pass it something that couldn't possibly be recognized
		testFactory("", this);
	}

	/**
	 * This is a runtime exception for now. Should there be a need for a web service to send in a
	 * format type this should be formalized to DocumentFormatNotRegisteredException
	 * 
	 * @throws DocumentDecodingException
	 * @throws DocumentContentsNotRecognizedException
	 * @throws DocumentFormatNotRegisteredException
	 */
	@Test(expected = DocumentFormatNotRegisteredException.class)
	public void testFormatNotRegistered() throws DocumentDecodingException, DocumentContentsNotRecognizedException,
			DocumentFormatNotRegisteredException {
		try {
			DocumentFactory.getInstance().getDocument(DocumentFormatKey.GPX_1_1, contents);
		} catch (DocumentFormatNotRegisteredException e) {
			TestUtil.assertContains(e.getMessage(), DocumentFormatKey.GPX_1_1.name());
			throw e;

		}
	}

	@Test
	public void testEquals() {
		TextDocumentTestUtil util = new TextDocumentTestUtil.Builder().build();
		TextDocument doc1 = util.getSampleDocument();
		TextDocument sameAsDoc1 = new TextDocument(doc1.getContents());
		assertEquals(doc1, sameAsDoc1);
		assertNotSame(doc1, sameAsDoc1);

		TextDocument doc2 = util.getSampleDocument();
		TestUtil.assertNotEquals(doc1, doc2);

	}
}
