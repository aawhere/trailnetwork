/**
 * 
 */
package com.aawhere.doc;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.aawhere.doc.archive.ArchivedDocument;
import com.aawhere.doc.text.TextDocument;
import com.aawhere.test.TestUtil;

/**
 * @author Brian Chapman
 * 
 */
public class DocumentsUnitTest {

	@Test
	public void testBuild() throws DocumentNotFoundException {
		Document doc = DocumentTestUtil.createRandomDocument();
		ArchivedDocument archivedDocument = new ArchivedDocument.Builder().setDocument(doc).build();

		Documents documents = createDocuments(archivedDocument);
		assertDocument(archivedDocument, documents, TextDocument.class);
	}

	@Test
	public void testMultipleDocuments() throws DocumentNotFoundException {
		Document docText = DocumentTestUtil.createRandomDocument();
		Document docSimple = DocumentTestUtil.createRandomSimpleDocument();
		ArchivedDocument archivedDocumentText = new ArchivedDocument.Builder().setDocument(docText).build();
		ArchivedDocument archivedDocumentSimple = new ArchivedDocument.Builder().setDocument(docSimple).build();
		Documents documents = createDocuments(archivedDocumentText, archivedDocumentSimple);
		assertDocument(archivedDocumentText, documents, TextDocument.class);
		assertDocument(archivedDocumentSimple, documents, SimpleDocument.class);
	}

	@Test
	public void testOneDocumentMultipleInterfaces() throws DocumentNotFoundException {
		Document multiDoc = new MultipleDocument();
		ArchivedDocument multiArchivedDocument = new ArchivedDocument.Builder().setDocument(multiDoc).build();
		Documents documents = createDocuments(multiArchivedDocument);
		assertDocument(multiArchivedDocument, documents, ADocument.class);
		assertDocument(multiArchivedDocument, documents, BDocument.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCantAddMultipleDocumentsToSameInterface() {
		Document docText1 = DocumentTestUtil.createRandomDocument();
		Document docText2 = DocumentTestUtil.createRandomDocument();
		ArchivedDocument archivedDocument1 = new ArchivedDocument.Builder().setDocument(docText1).build();
		ArchivedDocument archivedDocument2 = new ArchivedDocument.Builder().setDocument(docText2).build();
		createDocuments(archivedDocument1, archivedDocument2);
	}

	private Documents createDocuments(ArchivedDocument... docs) {
		Documents documents = new Documents.Builder().addDocuments(docs).build();
		// TODO Put assertions here.
		return documents;
	}

	private void assertDocument(ArchivedDocument archivedDocument, Documents documents, Class<? extends Document> type)
			throws DocumentNotFoundException {
		assertEquals(archivedDocument.getDocument(), documents.getDocument(type));
		assertEquals(archivedDocument.getDocument(), documents.getArchivedDocument(type).getDocument());
	}

	public static class MultipleDocument
			implements ADocument, BDocument {

		private static final long serialVersionUID = -1549657523984528787L;

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.Document#toByteArray()
		 */
		@Override
		public byte[] toByteArray() {
			return TestUtil.generateRandomBytes();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.Document#getDocumentFormat()
		 */
		@Override
		public DocumentFormatKey getDocumentFormat() {
			return DocumentFormatKey.UNKNOWN;
		}
	}

	public static interface ADocument
			extends Document {

	}

	public static interface BDocument
			extends Document {

	}
}
