/**
 *
 */
package com.aawhere.doc;

import static org.junit.Assert.*;

import org.apache.commons.lang.StringUtils;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.ApplicationDataIdImpl;
import com.aawhere.doc.text.TextDocument;
import com.aawhere.doc.text.TextDocumentProducer;
import com.aawhere.test.TestUtil;

/**
 * @author Aaron Roller
 * 
 */
public class DocumentTestUtil {

	/**
	 * Given an xml string and a format, this will test the byte array of the document to make sure
	 * what goes in...comes out!
	 * 
	 * @param format
	 * @param xml
	 */
	public static void assertByteArray(final DocumentFormatKey format, final String xml) {
		Document doc;
		try {
			doc = DocumentFactory.getInstance().getDocument(format, xml);
		} catch (DocumentDecodingException e) {
			throw new RuntimeException(e);
		} catch (DocumentFormatNotRegisteredException e) {
			throw new RuntimeException(e);
		}
		byte[] bytes = doc.toByteArray();
		String string = new String(bytes);
		// could unmarshall again and test the tracks, but probably overkill...let's trust JAXB a
		// bit.
		TestUtil.assertContains(string, "xml");

		assertTrue("there is some junk in there", StringUtils.isAsciiPrintable(string));
	}

	public static TextDocument createRandomDocument() {
		return new TextDocument(TestUtil.generateRandomString());
	}

	public static SimpleDocument createRandomSimpleDocument() {
		return new SimpleDocument.Builder().setBytes(TestUtil.generateRandomBytes())
				.setDocumentFormat(DocumentFormatKey.UNKNOWN).build();
	}

	/**
	 * @return
	 */
	public static DocumentFactory createTestDocumentFactory() {

		return new DocumentFactory.Builder().register(new TextDocumentProducer.Builder().build()).build();
	}

	public static ApplicationDataId applicationDataId() {
		return new ApplicationDataIdImpl(TestUtil.generateRandomString());
	}
}
