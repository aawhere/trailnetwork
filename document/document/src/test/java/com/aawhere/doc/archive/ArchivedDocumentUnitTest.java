/**
 *
 */
package com.aawhere.doc.archive;

import static org.junit.Assert.*;

import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.ApplicationDataIdImpl;
import com.aawhere.doc.Document;
import com.aawhere.doc.DocumentContentsNotRecognizedException;
import com.aawhere.doc.DocumentDecodingException;
import com.aawhere.doc.DocumentFormatKey;
import com.aawhere.doc.DocumentTestUtil;
import com.aawhere.doc.archive.ArchivedDocument.Builder;
import com.aawhere.doc.text.TextDocument;
import com.aawhere.doc.text.TextDocumentTestUtil;
import com.aawhere.io.zip.CorruptGzipFormatException;
import com.aawhere.io.zip.GzipCompression;
import com.aawhere.persist.BaseEntityBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.MockRepository;
import com.aawhere.personalize.PersonalizedEntityJaxbTestUtil;
import com.aawhere.test.TestUtil;

/**
 * @author Aaron Roller
 * 
 */
public class ArchivedDocumentUnitTest
		extends
		BaseEntityBaseUnitTest<ArchivedDocument, ArchivedDocument.Builder, ArchivedDocumentId, ArchivedDocumentRepository> {
	private static final DocumentFormatKey FORMAT = DocumentFormatKey.TEXT;
	private TextDocument originalDocument;
	private TextDocument mutatedDocument;
	private ApplicationDataId applicationDataId;

	/**
 *
 */
	public ArchivedDocumentUnitTest() {
		super(MUTABLE);
	}

	@Override
	@Before
	public void setUp() {
		this.originalDocument = new TextDocumentTestUtil.Builder().build().getSampleDocument();
		this.mutatedDocument = new TextDocumentTestUtil.Builder().build().getSampleDocument();
		applicationDataId = DocumentTestUtil.applicationDataId();
	}

	public static ArchivedDocumentUnitTest newInstance() {
		final ArchivedDocumentUnitTest unitTest = new ArchivedDocumentUnitTest();
		unitTest.setUp();
		return unitTest;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#populateBeforeCreation(com
	 * .aawhere.persist.BaseEntity .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.setDocument(originalDocument);
		builder.setApplicationDataId(applicationDataId);
	}

	public void assertDocument(ArchivedDocument archivedDoc, TextDocument document) throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, CorruptGzipFormatException {

		String contents = document.getContents();

		byte[] encodedData = archivedDoc.getEncodedData();
		assertNotNull(encodedData);

		assertTrue(GzipCompression.isCompressed(encodedData));
		String uncompressContents = new String(new GzipCompression.Builder().build().uncompress(encodedData));
		assertEquals(contents, uncompressContents);
		assertEquals(document.getDocumentFormat(), archivedDoc.getFormat());
		assertEquals(contents.getBytes().length, (int) archivedDoc.getNumberofBytes());
		assertEquals(encodedData.length, (int) archivedDoc.getNumberOfBytesEncoded());
		// http://code.google.com/speed/page-speed/docs/payload.html
		// compressed seems to be larger than original for small strings.
		// TestUtil.assertGreaterThan("compressed should be less than original",
		// archivedDoc.getNumberofBytes(),
		// archivedDoc.getNumberOfBytesEncoded());
		assertEquals(ArchivedDocument.Encoding.GZIP, archivedDoc.getEncoding());
		assertEquals(applicationDataId.getValue(), archivedDoc.getApplicationDataId());

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	public void assertCreation(ArchivedDocument sample) {
		super.assertCreation(sample);
		try {
			assertDocument(sample, this.originalDocument);

		} catch (DocumentDecodingException e) {
			throw new RuntimeException(e);
		} catch (CorruptGzipFormatException e) {
			throw new RuntimeException(e);
		}
	}

	@Test(expected = RuntimeException.class)
	public void testDocumentRequired() {
		new ArchivedDocument.Builder().build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertStored(com.aawhere.persist .BaseEntity)
	 */
	@Override
	public void assertStored(ArchivedDocument sample) {
		super.assertStored(sample);
		try {
			assertDocument(sample, originalDocument);
		} catch (DocumentDecodingException e) {
			throw new RuntimeException(e);
		} catch (CorruptGzipFormatException e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	@Ignore("Archived Document doesn't support round trip with document factory building dependency")
	public void testJaxb() throws JAXBException {

		PersonalizedEntityJaxbTestUtil.testJaxbRoundTrip(this);

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.Identifier)
	 */
	@Override
	protected ArchivedDocument mutate(ArchivedDocumentId id) throws EntityNotFoundException {
		return getRepository().update(id, mutatedDocument);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(ArchivedDocument mutated) {
		super.assertMutation(mutated);
		ArchivedDocumentUtil.complete(mutated, DocumentTestUtil.createTestDocumentFactory());
		TextDocument document = (TextDocument) mutated.getDocument();
		try {
			assertDocument(mutated, document);
		} catch (DocumentDecodingException | CorruptGzipFormatException e) {
			throw new RuntimeException(e);
		}
	}

	class MockArchivedDocumentRepository
			extends MockRepository<ArchivedDocument, ArchivedDocumentId>
			implements ArchivedDocumentRepository {

		/**
		 * 
		 */
		public MockArchivedDocumentRepository() {
			super(ArchivedDocument.class, ArchivedDocumentId.class);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.doc.archive.ArchivedDocumentRepository#findByApplicationDataId(java.lang.
		 * String, com.aawhere.doc.DocumentFormatKey)
		 */
		@Override
		public List<ArchivedDocument> findByApplicationDataId(String applicationDataId, DocumentFormatKey format) {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.archive.ArchivedDocumentRepository#update(com.aawhere.doc.archive.
		 * ArchivedDocumentId, com.aawhere.doc.Document)
		 */
		@Override
		public ArchivedDocument update(ArchivedDocumentId id, Document document) throws EntityNotFoundException {
			return update(ArchivedDocumentRepositoryUtil.mutate(id, document, this));
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.doc.archive.ArchivedDocumentRepository#findByApplicationDataIds(java.lang
		 * .Iterable, com.aawhere.doc.DocumentFormatKey)
		 */
		@Override
		public Iterable<ArchivedDocument> findByApplicationDataIds(Iterable<String> applicationDataId,
				DocumentFormatKey format) {
			throw new RuntimeException("TODO:Implement this");
		}

	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected ArchivedDocumentRepository createRepository() {
		return new MockArchivedDocumentRepository();
	}
}
