/**
 * 
 */
package com.aawhere.doc;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @see DocumentFactory
 * 
 * @author Aaron Roller
 * 
 */
public class DocumentFactoryUnitTest {

	@Test(expected = IllegalArgumentException.class)
	public void testRegistrationOrderHighOnly() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		// package access allows not using instance
		DocumentFactory factory = new DocumentFactory();
		factory.register(new High());
		factory.getDocument("junk");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testRegistrationOrderHighFirst() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		// package access allows not using instance
		DocumentFactory factory = new DocumentFactory();
		factory.register(new High());
		factory.register(new Low());
		factory.getDocument("junk");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testRegistrationOrderLowFirst() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		// package access allows not using instance
		DocumentFactory factory = new DocumentFactory();
		factory.register(new Low());
		factory.register(new High());
		factory.getDocument("junk");

	}

	@Test(expected = NullPointerException.class)
	public void testRegistrationOrderLowOnly() throws DocumentDecodingException,
			DocumentContentsNotRecognizedException, DocumentFormatNotRegisteredException {
		// package access allows not using instance
		DocumentFactory factory = new DocumentFactory();
		factory.register(new Low());
		factory.getDocument("junk");

	}

	@Test
	@Ignore("the order swaps randomly so no good way to test that both are being registered.")
	public void testRegistrationDuplicateOrder() throws DocumentContentsNotRecognizedException,
			DocumentDecodingException, DocumentFormatNotRegisteredException {
		DocumentFactory factory = new DocumentFactory();
		factory.register(new Low());
		factory.register(new SecondLow());

	}

	/**
	 * Used to test order. Mockito or something esle might have worked better, but just throw
	 * different Exceptions based on the order.
	 * 
	 * @see High
	 * 
	 * @author Aaron Roller
	 * 
	 */
	public static class Low
			implements DocumentProducer {

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
		 */
		@Override
		public Document produce(Object dataInput) throws DocumentDecodingException,
				DocumentContentsNotRecognizedException {
			throw new NullPointerException("Low");
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
		 */
		@Override
		public DocumentFormatKey[] getDocumentFormatsSupported() {

			return ArrayUtils.toArray(DocumentFormatKey.TEXT);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
		 */
		@Override
		public DiscoveryOrder getDiscoveryOrder() {

			return DiscoveryOrder.LOW;
		}

	}

	public static class SecondLow
			extends Low {
		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentFactoryUnitTest.Low#getDocumentFormatsSupported()
		 */
		@Override
		public DocumentFormatKey[] getDocumentFormatsSupported() {
			return ArrayUtils.toArray(DocumentFormatKey.ETSX);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentFactoryUnitTest.Low#produce(java.lang.Object)
		 */
		@Override
		public Document produce(Object dataInput) throws DocumentDecodingException,
				DocumentContentsNotRecognizedException {
			throw new UnsupportedOperationException();
		}
	}

	public static class High
			implements DocumentProducer {

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentProducer#produce(java.lang.Object)
		 */
		@Override
		public Document produce(Object dataInput) throws DocumentDecodingException,
				DocumentContentsNotRecognizedException {
			throw new IllegalArgumentException();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentProducer#getDocumentFormatsSupported()
		 */
		@Override
		public DocumentFormatKey[] getDocumentFormatsSupported() {
			return ArrayUtils.toArray(DocumentFormatKey.GPX_1_1);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.doc.DocumentProducer#getDiscoveryOrder()
		 */
		@Override
		public DiscoveryOrder getDiscoveryOrder() {
			return DiscoveryOrder.HIGH;
		}
	}
}
