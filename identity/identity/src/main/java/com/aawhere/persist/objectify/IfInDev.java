/**
 * 
 */
package com.aawhere.persist.objectify;

import com.aawhere.ws.rs.SystemWebApiUri;

import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.ValueIf;

/**
 * Used when an Index is desired for development and testing environments, but not desired on the
 * Live or Demo system where increased costs could be incurred.
 * 
 * 
 * @see Index
 * @see ValueIf
 * 
 * @author aroller
 * 
 */
public class IfInDev
		extends ValueIf<Object> {

	private static boolean inDev;
	static {
		inDev = SystemWebApiUri.isInDevelopment();
	}

	/*
	 * (non-Javadoc)
	 * @see com.googlecode.objectify.condition.If#matchesValue(java.lang.Object)
	 */
	@Override
	public boolean matchesValue(Object arg0) {
		return inDev;

	}

}
