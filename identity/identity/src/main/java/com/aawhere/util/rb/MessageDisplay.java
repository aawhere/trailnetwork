/**
 *
 */
package com.aawhere.util.rb;

import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.personalize.PersonalizedBase;
import com.aawhere.personalize.xml.LocaleXmlAdapter;
import com.aawhere.personalize.xml.XmlLang;
import com.aawhere.xml.XmlNamespace;

/**
 * A {@link Message} translated into a {@link PersonalizedBase} object for display purposes.
 * 
 * @see MessagePersonalizedXmlAdapter
 * 
 * @author roller
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "message", namespace = XmlNamespace.API_VALUE)
public class MessageDisplay
		extends PersonalizedBase {

	/** A convenience method for constructing a builder */
	public static Builder create() {
		return new Builder();
	}

	@XmlAttribute(name = XmlLang.ATTRIBUTE_NAME, required = false)
	@XmlJavaTypeAdapter(LocaleXmlAdapter.class)
	Locale locale;

	/**
	 * If the message is an enumeration, this is the type of key referencing the enumeration that
	 * created the value.
	 */
	@XmlAttribute(required = false)
	String type;

	/**
	 * If the message is an enumeration, this is the key referencing the enumeration that created
	 * the value.
	 */
	@XmlAttribute(required = false)
	String key;

	/**
	 * The actual message to be displayed in the language designated by {@link #lang}.
	 * 
	 */
	@XmlValue
	String value;

	public String value() {
		return value;
	}

	/** Use {@link Builder} to construct MessageDisplay */
	private MessageDisplay() {
	}

	/**
	 * Used to construct all instances of MessageDisplay.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<MessageDisplay> {

		public Builder() {
			super(new MessageDisplay());
		}

		public Builder value(String value) {
			building.value = value;
			return this;
		}

		public Builder key(String key) {
			building.key = key;
			return this;
		}

		public Builder type(String type) {
			building.type = type;
			return this;
		}

		public Builder locale(Locale locale) {
			building.locale = locale;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull(building.value);
			super.validate();
		}
	}// end Builder

}
