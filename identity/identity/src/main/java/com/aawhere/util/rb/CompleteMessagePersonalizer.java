package com.aawhere.util.rb;

import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.identity.IdentityManager;
import com.aawhere.personalize.BasePersonalizer;
import com.aawhere.personalize.PersonalizeUtil;
import com.aawhere.personalize.Personalized;
import com.aawhere.xml.XmlNamespace;

import com.google.inject.Inject;
import com.wordnik.swagger.annotations.ApiModel;

public class CompleteMessagePersonalizer
		extends BasePersonalizer<CompleteMessagePersonalizer.CompleteMessagePersonalized> {

	/**
	 * Used to construct all instances of CompleteMessagePersonalizer.
	 */
	public static class Builder
			extends BasePersonalizer.Builder<CompleteMessagePersonalizer, Builder> {

		public Builder() {
			super(new CompleteMessagePersonalizer());
		}

		@Override
		public CompleteMessagePersonalizer build() {
			CompleteMessagePersonalizer built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct CompleteMessagePersonalizer */
	private CompleteMessagePersonalizer() {
	}

	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.NONE)
	@ApiModel(description = "Details of a message including parameters and personlized message")
	public static class CompleteMessagePersonalized
			extends CompleteMessageXmlAdapter.CompleteMessageXml
			implements Personalized {

		private static final long serialVersionUID = 6975165971538361764L;
		@XmlElement
		public final String personalized;

		public CompleteMessagePersonalized() {
			this(null, null);
		}

		public CompleteMessagePersonalized(CompleteMessage message, String personalizedMessage) {
			super(message);
			this.personalized = personalizedMessage;
		}

		@Override
		public Personalized getPersonalized() {
			return this;
		}

	}

	@Override
	public void personalize(Object object) {
		if (object != null) {
			CompleteMessage message = (CompleteMessage) object;
			final Locale locale = getLocale();
			final String personalizedMessage = MessageFormatter.create().locale(locale).message(message).build()
					.getFormattedMessage();
			result = new CompleteMessagePersonalized(message, personalizedMessage);
		}
	}

	@Override
	public Class<?> handlesType() {
		return CompleteMessage.class;
	}

	public static class CompleteMessagePersonalizedXmlAdapter
			extends CompleteMessageXmlAdapter {
		private CompleteMessagePersonalizer personalizer;

		public CompleteMessagePersonalizedXmlAdapter() {
			this(PersonalizeUtil.personalizerDefault(new CompleteMessagePersonalizer.Builder()));
		}

		public CompleteMessagePersonalizedXmlAdapter(CompleteMessagePersonalizer plizer) {
			this.personalizer = plizer;
		}

		@Inject
		public CompleteMessagePersonalizedXmlAdapter(IdentityManager identityManager) {
			this(new CompleteMessagePersonalizer.Builder().setPreferences(identityManager.preferences()).build());
		}

		@Override
		public CompleteMessageXml marshal(CompleteMessage message) throws Exception {
			personalizer.personalize(message);
			return personalizer.getResult();
		}

	}
}
