/**
 *
 */
package com.aawhere.util.rb;

import com.aawhere.identity.IdentityManager;
import com.aawhere.personalize.xml.PersonalizedXmlAdapter;

import com.google.inject.Inject;

/**
 * Converts a {@link Message} to/from XML.
 * 
 * @author Aaron Roller
 * 
 */
public class MessagePersonalizedXmlAdapter
		extends PersonalizedXmlAdapter<MessageDisplay, Message> {

	public MessagePersonalizedXmlAdapter() {
		super(new MessagePersonalizer.Builder());
	}

	public MessagePersonalizedXmlAdapter(MessagePersonalizer plizer) {
		super(plizer);
	}

	@Inject
	public MessagePersonalizedXmlAdapter(IdentityManager identityManager) {
		super(new MessagePersonalizer.Builder().setPreferences(identityManager.preferences()).build());
	}

	protected MessagePersonalizedXmlAdapter(MessagePersonalizer.Builder builder) {
		super(builder);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Message showUnmarshal(MessageDisplay xml) throws Exception {
		String key = ((MessagePersonalizer) getPersonalizer()).getMessageKeyFromPersonalized(xml.type, xml.key);
		return MessageFactory.getInstance().bestMessage(key, xml.value);
	}

}
