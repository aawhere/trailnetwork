/**
 *
 */
package com.aawhere.util.rb;

import java.util.Locale;

import com.aawhere.collections.Index;
import com.aawhere.personalize.BasePersonalizer;
import com.aawhere.personalize.Personalizer;

/**
 * A {@link Personalizer} for {@link Message}s to be transformed into a {@link MessageDisplay} by
 * {@link MessagePersonalizedXmlAdapter}.
 * 
 * @author roller
 * 
 */
public class MessagePersonalizer
		extends BasePersonalizer<MessageDisplay> {

	public final String KEY_DELIMINATOR = ".";
	public final String KEY_DELIMINATOR_REGEX = "\\" + KEY_DELIMINATOR;

	/** A convience method for constructing a builder */
	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct MessagePersonalizer */
	private MessagePersonalizer() {
	}

	/**
	 * Used to construct all instances of MessagePersonalizer.
	 */
	public static class Builder
			extends BasePersonalizer.Builder<MessagePersonalizer, Builder> {

		public Builder() {
			super(new MessagePersonalizer());
		}

	}// end Builder

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#personalize(java.lang.Object)
	 */
	@Override
	public void personalize(Object object) {
		Message message = (Message) object;
		String key = getPersonalizedKey(message);
		String keyType = getPersonalizedKeyType(message);
		final Locale locale = getLocale();
		final String value = MessageFormatter.create().locale(locale).message(message).build().getFormattedMessage();
		result = MessageDisplay.create().value(value).type(keyType).key(key).locale(locale).build();
	}

	protected String getPersonalizedKey(Message message) {
		return getKeyPart(message, Index.SECOND);
	}

	protected String getPersonalizedKeyType(Message message) {
		return getKeyPart(message, Index.FIRST);
	}

	private String getKeyPart(Message message, Index index) {
		if (message instanceof Enum) {
			String[] parts = MessageFactory.getInstance().keyForObject(message).split(KEY_DELIMINATOR_REGEX);
			return parts[index.ordinal()];
		} else {
			return null;
		}
	}

	protected String getMessageKeyFromPersonalized(String type, String key) {
		if (key == null || type == null) {
			return null;
		} else {
			return type + KEY_DELIMINATOR + key;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#handlesType()
	 */
	@Override
	public Class<?> handlesType() {

		return Message.class;
	}

}
