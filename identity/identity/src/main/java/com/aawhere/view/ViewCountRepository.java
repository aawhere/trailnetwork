/**
 * 
 */
package com.aawhere.view;

import com.aawhere.persist.Repository;

/**
 * @author aroller
 * 
 */
public interface ViewCountRepository
		extends Repository<ViewCount, ViewCountId> {

	/**
	 * Increments the count of an existing entity or creates a new one if necessary. Returns the
	 * latest count.
	 * 
	 */
	public Integer increment(ViewCountId id);
}
