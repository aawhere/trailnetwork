/**
 * 
 */
package com.aawhere.view;

import com.aawhere.persist.EntityNotFoundException;

/**
 * @author aroller
 * 
 */
public class ViewCountRepositoryDelegate {

	private ViewCountRepository repository;

	/**
	 * @param viewCountMockRepository
	 */
	public ViewCountRepositoryDelegate(ViewCountRepository repository) {
		this.repository = repository;
	}

	/**
	 * @see ViewCountRepository#increment(ViewCountId)
	 * @param viewCountId
	 * @return
	 */
	Integer increment(ViewCountId viewCountId) {
		ViewCount.Builder mutator;
		try {
			mutator = ViewCount.mutate(repository.load(viewCountId));
		} catch (EntityNotFoundException e) {
			mutator = ViewCount.create().id(viewCountId);
		}
		return repository.store(mutator.increment().build()).count();
	}

}
