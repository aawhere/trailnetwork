/**
 * 
 */
package com.aawhere.view;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.text.format.custom.ClassFormat;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

public enum ViewCountMessage implements Message {

	/** Comments go here. */
	MESSAGE_1("something {FIRST} for {WITH_FORMAT}.", Param.FIRST, Param.WITH_FORMAT);

	private ParamKey[] parameterKeys;
	private String message;

	private ViewCountMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		FIRST, WITH_FORMAT(new ClassFormat());

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
