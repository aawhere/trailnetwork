/**
 * 
 */
package com.aawhere.view;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.id.Identifiable;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.xml.XmlNamespace;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Unindex;

/**
 * Indicates how many times an {@link Identifiable} has been viewed.
 * 
 * @author aroller
 * 
 */
@Cache
@Unindex
@Entity
@Dictionary(domain = ViewCount.FIELD.DOMAIN, messages = ViewCountMessage.class)
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class ViewCount
		extends StringBaseEntity<ViewCount, ViewCountId>
		implements SelfIdentifyingEntity {

	@XmlElement
	private Integer count = 0;

	/**
	 * Used to construct all instances of ViewCount.
	 */
	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<ViewCount, Builder, ViewCountId> {
		private Identifier<?, ?> counting;

		public Builder() {
			super(new ViewCount());
		}

		Builder(ViewCount mutant) {
			super(mutant);
		}

		public Builder increment() {
			building.count++;
			return this;
		}

		public Builder counting(Identifier<?, ?> counting) {
			this.counting = counting;
			return this;
		}

		Builder id(ViewCountId id) {
			setId(id);
			return this;
		}

		@Override
		public ViewCount build() {
			if (!hasId()) {
				Assertion.exceptions().notNull("counting", this.counting);
				setId(new ViewCountId(counting));
			}
			ViewCount built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct ViewCount */
	private ViewCount() {
	}

	public static Builder create() {
		return new Builder();
	}

	static Builder mutate(ViewCount mutant) {
		return new Builder(mutant);
	}

	public static class FIELD {
		public static final String DOMAIN = "viewCount";
		public static final String ID = BASE_FIELD.ID;

		public static final class KEY {
			public static final FieldKey ID = new FieldKey(FIELD.DOMAIN, FIELD.ID);
		}

	}

	@Override
	@Field(key = FIELD.ID, indexedFor = "Repository.load()")
	public ViewCountId getId() {
		return super.getId();
	}

	/**
	 * @return the count
	 */
	public Integer count() {
		return this.count;
	}

	private static final long serialVersionUID = 3398872044034278083L;
}
