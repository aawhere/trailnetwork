/**
 * 
 */
package com.aawhere.view;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.StringIdentifier;

/**
 * A composite key uniquely identifying an entity in the system. Combining the
 * {@link Identifier#getKind()} and the {@link Identifier#getValue()} using
 * {@link IdentifierUtils#join(String, Identifier)}.
 * 
 * @see ViewCount
 * @author aroller
 * 
 */
public class ViewCountId
		extends StringIdentifier<ViewCount> {

	/**
	 * 
	 */
	ViewCountId() {
		super(ViewCount.class);
	}

	public ViewCountId(String value) {
		super(value, ViewCount.class);
	}

	public ViewCountId(Identifier<?, ?> countingId) {
		this(IdentifierUtils.join(IdentifierUtils.domain(countingId), countingId));
	}

	public String countingIdValue() {
		return IdentifierUtils.splitCompositeId(this).getRight();
	}

	private static final long serialVersionUID = -4640069503955367739L;
}
