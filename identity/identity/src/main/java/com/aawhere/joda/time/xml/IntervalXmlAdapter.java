/**
 * 
 */
package com.aawhere.joda.time.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;
import org.joda.time.Interval;

/**
 * Splits an interval into the start and end so it may be recognized by the existing adapters.
 * 
 * @author aroller
 * 
 */
public class IntervalXmlAdapter
		extends XmlAdapter<IntervalXmlAdapter.Interval, Interval> {

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Interval {
		@XmlElement
		@XmlJavaTypeAdapter(DateTimeXmlAdapter.class)
		private DateTime start;
		@XmlElement
		@XmlJavaTypeAdapter(DateTimeXmlAdapter.class)
		private DateTime end;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Interval marshal(org.joda.time.Interval interval) throws Exception {
		if (interval == null) {
			return null;
		}
		Interval xml = new Interval();
		xml.start = interval.getStart();
		xml.end = interval.getEnd();
		return xml;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public org.joda.time.Interval unmarshal(Interval xml) throws Exception {
		return new org.joda.time.Interval(xml.start, xml.end);
	}
}
