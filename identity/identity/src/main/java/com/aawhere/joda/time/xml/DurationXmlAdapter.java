/**
 *
 */
package com.aawhere.joda.time.xml;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.Duration;

import com.aawhere.identity.IdentityManager;
import com.aawhere.personalize.BaseDurationPersonalizer;
import com.aawhere.personalize.DurationPersonalized;
import com.aawhere.personalize.DurationPersonalizedPersonalizer;
import com.aawhere.personalize.DurationPersonalizer;
import com.aawhere.personalize.xml.PersonalizedXmlAdapter;

import com.google.inject.Inject;

/**
 * {@link XmlAdapter} for {@link Duration}. This adapter is capable of including localized
 * durations.
 * 
 * @author Aaron Roller
 * 
 */
public class DurationXmlAdapter
		extends PersonalizedXmlAdapter<DurationPersonalized, Duration> {

	public DurationXmlAdapter() {
		super(new DurationPersonalizedPersonalizer.Builder());
	}

	public DurationXmlAdapter(BaseDurationPersonalizer<?> plizer) {
		super(plizer);
	}

	@Inject
	public DurationXmlAdapter(IdentityManager identityManager) {
		super(new DurationPersonalizer.Builder().setPreferences(identityManager.preferences()).build());
	}

	@Override
	public Duration showUnmarshal(DurationPersonalized context) throws Exception {
		return new Duration(new Long(context.getDuration()));
	}
}