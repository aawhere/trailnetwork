/**
 *
 */
package com.aawhere.joda.time.xml;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;

import com.aawhere.identity.IdentityManager;
import com.aawhere.personalize.BaseDateTimePersonalizer;
import com.aawhere.personalize.DateTimePersonalized;
import com.aawhere.personalize.DateTimePersonalizedPersonalizer;
import com.aawhere.personalize.DateTimePersonalizer;
import com.aawhere.personalize.xml.PersonalizedXmlAdapter;

import com.google.inject.Inject;

/**
 * {@link XmlAdapter} for {@link DateTime}. This adapter is capable of including localized dates and
 * times.
 * 
 * @author Aaron Roller
 * 
 */
public class DateTimeXmlAdapter
		extends PersonalizedXmlAdapter<DateTimePersonalized, DateTime> {

	public DateTimeXmlAdapter() {
		super(new DateTimePersonalizedPersonalizer.Builder());
	}

	public DateTimeXmlAdapter(BaseDateTimePersonalizer<?> plizer) {
		super(plizer);
	}

	@Inject
	public DateTimeXmlAdapter(IdentityManager identityManager) {
		super(new DateTimePersonalizer.Builder().setPreferences(identityManager.preferences()).build());
	}

	@Override
	public DateTime showUnmarshal(DateTimePersonalized context) throws Exception {
		return new DateTime(context.getDate());
	}
}
