@XmlJavaTypeAdapters({ @XmlJavaTypeAdapter(value = com.aawhere.util.rb.MessagePersonalizedXmlAdapter.class,
		type = com.aawhere.util.rb.Message.class) })
@XmlSchema(xmlns = @XmlNs(prefix = com.aawhere.xml.XmlNamespace.API_PREFIX,
		namespaceURI = com.aawhere.xml.XmlNamespace.API_VALUE))
package com.aawhere.field;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

