/**
 *
 */
package com.aawhere.field.xml;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.collections.CollectionUtils;

import com.aawhere.collections.FunctionsExtended;
import com.aawhere.field.FieldDefinition;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldExpansionNotPossibleException;
import com.aawhere.field.FieldNotRegisteredException;
import com.aawhere.field.FieldsRequestedOnInvalidDictionary;
import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.identity.IdentityManager;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.lang.reflect.GenericUtils;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.BaseEntities;
import com.aawhere.persist.EntityUtil;
import com.aawhere.persist.FilterUtil;
import com.aawhere.persist.ServiceGeneral;
import com.aawhere.ws.ErrorResponse;
import com.aawhere.xml.bind.OptionalXmlAdapter;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.inject.Injector;

/**
 * Configures XmlAdapters to be used with marshallers when given fields. An advanced procedure
 * initially accepting an entity type and optionally a collection to be prepared to for
 * transformation. The fields are provided requesting the specific fields desired to be included in
 * the final transformation of xml. If the entity being transformed is a collection of entities then
 * the properties matching the fields of each entity in the collection is retrieved in a batch.
 * 
 * Nested transformation is supported by providing the field targeting the result of the nested
 * object.
 * 
 * So routeCompletion-personId will provide a person result so person-accountId can be successfully
 * found. Any request made that can't be fulfilled results in an exception explaining how to fix it.
 * 
 * 
 * @author aroller
 * 
 */
public class FieldXmlAdapterService {

	/**
	 * Used to construct all instances of FieldXmlAdapterService.
	 */
	public static class Builder
			extends ObjectBuilder<FieldXmlAdapterService> {

		private MarshallerProxy marshaller;
		/**
		 *
		 */
		private static final Logger LOG = LoggerFactory.getLogger(FieldXmlAdapterService.class);
		/** The keys for the fields requested. */
		private List<String> fieldsRequested;
		private FieldDictionaryFactory dictionaryFactory;
		/** The fields requested converted into their definitions grouped by domain */
		private Multimap<String, FieldDefinition<?>> definitionsForDomain;

		/**
		 * The target type being marshalled which should be a {@link Dictionary}. This provides the
		 * dictionary annotation for us to limit configurations only for the target. If the type is
		 * a collection then this represents the type inside the collection which is presumably a
		 * {@link Dictionary}.
		 */
		private HashMap<Class<?>, Object> typesBeingMarshalled = Maps.newHashMap();
		/**
		 * Acts as the "factory" providing dynamic access to XmlAdapters provided from the
		 * {@link Field#xmlAdapter()} so unable to use annotations to inject.
		 */
		private InjectorProxy injector;

		public Builder fieldsRequested(List<String> fieldsRequested) {
			this.fieldsRequested = FilterUtil.listParsed(fieldsRequested);
			return this;
		}

		public Builder entities(Collection<?> entities) {
			if (CollectionUtils.isNotEmpty(entities)) {
				Object item = entities.iterator().next();
				typesBeingMarshalled.put(item.getClass(), entities);
			}
			return this;
		}

		public Builder typeBeingMarshalled(Class<?> typeBeingMarshalled) {
			this.typesBeingMarshalled.put(typeBeingMarshalled, null);
			return this;
		}

		public Builder marshaller(MarshallerProxy marshallerProxy) {
			this.marshaller = marshallerProxy;
			return this;
		}

		public Builder marshaller(Marshaller marshaller) {
			this.marshaller = new MarshallerProxyPassThrough(marshaller);
			return this;
		}

		public Builder dictionaryFactory(FieldDictionaryFactory dictionaryFactory) {
			this.dictionaryFactory = dictionaryFactory;
			return this;
		}

		/**
		 * Testable replacement to {@link #injector(Injector)}
		 * 
		 * @param injector
		 * @return
		 */
		public Builder injector(InjectorProxy injector) {
			this.injector = injector;
			return this;
		}

		public Builder injector(final Injector injector) {
			this.injector = new InjectorPassThrough(injector);
			return this;
		}

		public Builder() {
			super(new FieldXmlAdapterService());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("marshaller", this.marshaller);
			Assertion.exceptions().notNull("adapterSingletonFunction", this.injector);
			Assertion.exceptions().notNull("dictionaryFactory", this.dictionaryFactory);

		}

		public FieldXmlAdapterService buildWithExceptions() throws FieldNotRegisteredException,
				FieldExpansionNotPossibleException {
			FieldXmlAdapterService built = super.build();
			load();
			return built;
		}

		@Override
		public FieldXmlAdapterService build() {
			try {
				return buildWithExceptions();
			} catch (FieldNotRegisteredException e) {
				throw ToRuntimeException.wrapAndThrow(e);
			}
		}

		/**
		 * @throws FieldNotRegisteredException
		 */
		private void load() throws FieldNotRegisteredException {

			if (CollectionUtils.isNotEmpty(fieldsRequested)) {

				// only configure those adapters for the dictionary of the type being marshalled
				// an empty collection given wouldn't know what type it is marshalling
				if (!this.typesBeingMarshalled.isEmpty()) {

					Class<?> typeBeingMarshalled = Iterables.getOnlyElement(this.typesBeingMarshalled.keySet());
					if (AnnotatedDictionaryUtils.dictionaryFrom(typeBeingMarshalled) != null) {

						populateDefinitions();

						load(typeBeingMarshalled);
						if (!this.definitionsForDomain.isEmpty()) {
							throw new FieldExpansionNotPossibleException(this.definitionsForDomain.values(),
									typeBeingMarshalled);
						}
					} else {
						// Error response finds it way in here from the ExceptionMapper. Ignore it
						// so the error can be reported.
						if (!ErrorResponse.class.isAssignableFrom(typeBeingMarshalled)) {
							// base entities has a special handler that can make expand requests
							if (!BaseEntities.class.isAssignableFrom(typeBeingMarshalled)) {
								// if the client requests a field against an entity not a dictionary
								// then
								// feedback is good
								throw new FieldsRequestedOnInvalidDictionary(fieldsRequested, typeBeingMarshalled);
							}
						}
					}
				}
			}

		}

		/**
		 * iterates all the {@link #fieldsRequested} and loads the definitions.
		 * 
		 * @throws FieldNotRegisteredException
		 */
		private void populateDefinitions() throws FieldNotRegisteredException {
			this.definitionsForDomain = HashMultimap.create();
			// first, find the definitions for each organized by domain
			for (String option : fieldsRequested) {
				FieldDefinition<Object> definition = this.dictionaryFactory.findDefinition(option);
				this.definitionsForDomain.put(definition.getKey().getDomainKey(), definition);
			}
		}

		/**
		 * A delegate used from both single and collection loading.
		 * 
		 * @param typeBeingMarshalled
		 */
		private void load(Class<?> typeBeingMarshalled) {
			Dictionary dictionary = AnnotatedDictionaryUtils.dictionaryFrom(typeBeingMarshalled);
			if (dictionary != null) {
				final String domain = dictionary.domain();
				Collection<FieldDefinition<?>> definitions = this.definitionsForDomain.get(domain);
				for (FieldDefinition<?> fieldDefinition : definitions) {
					loadDefinition(fieldDefinition, typeBeingMarshalled);
				}
				// remove each from the list to ensure we handle all requests.
				this.definitionsForDomain.removeAll(domain);
			}

		}

		/**
		 * @param definition
		 * @throws Throwable
		 */
		private void loadDefinition(FieldDefinition<?> definition, Class<?> typeBeingMarshalled) {
			Class<? extends XmlAdapter<?, ?>> adapterType = definition.getXmlAdapterType();
			// adapter types are optionally provided...if they aren't then this field
			// does not support
			if (adapterType != null) {
				XmlAdapter<?, ?> registeredAdapter = marshaller.getAdapter(adapterType);
				// only configure once
				if (registeredAdapter == null) {
					Object entityBeingMarshalled = this.typesBeingMarshalled.get(typeBeingMarshalled);
					// if entities is provided we must try to batch load if possible
					Class<?> typeOfResults;
					// optional xml adapters just need simple replacement
					// collections should be handled by batch processing
					if (!OptionalXmlAdapter.class.isAssignableFrom(adapterType)
							&& (entityBeingMarshalled instanceof Collection || entityBeingMarshalled instanceof Map)) {
						Collection<?> collectionOfDictionaries;
						if (entityBeingMarshalled instanceof Map) {
							// delaying until now allows async processing...this forces load
							collectionOfDictionaries = ((Map) entityBeingMarshalled).values();
						} else {
							collectionOfDictionaries = ((Collection<?>) entityBeingMarshalled);
						}
						typeOfResults = loadCollection(definition, adapterType, collectionOfDictionaries);

					} else {
						// optional adapters get the singleton which represents the preference to
						// show or not.
						XmlAdapter<?, ?> singletonXmlAdapter = injector.getInstance(adapterType);
						// just use the singleton as a replacement
						marshaller.setAdapter(singletonXmlAdapter);
						typeOfResults = GenericUtils.getTypeArguments(XmlAdapter.class, singletonXmlAdapter.getClass())
								.iterator().next();

					}
					// RECURSION CALL HERE
					// load calls load definition so this is recusive
					// the resulting type might be a dictionary satisfy a request...
					load(typeOfResults);
				} else {
					throw new IllegalStateException(registeredAdapter + " already configured for " + definition);
				}

			} else {
				throw new FieldNotExpandableException(definition.getKey());
			}
		}

		/**
		 * This is called when the entity being marshalled is a collection, not the field being
		 * adapted. The collection is used for batching.
		 * 
		 * @param definition
		 * @param adapterType
		 * @return
		 */
		@SuppressWarnings({ "rawtypes", "unchecked" })
		private Class<?> loadCollection(FieldDefinition<?> definition, Class<? extends XmlAdapter<?, ?>> adapterType,
				final Collection<?> collection) {
			final Class<?> idType;
			// the id type we seek may be a collection
			if (definition.hasParameterType()) {
				idType = definition.getParameterType();
			} else {
				idType = definition.getType();
			}
			Class<ServiceGeneral<?, ?, ?>> servicedBy = EntityUtil.servicedBy(adapterType);
			if (servicedBy == null) {
				servicedBy = EntityUtil.servicedBy(idType);
			}
			// we have a way to retrieve the item
			if (servicedBy != null) {
				ServiceGeneral<?, ?, ?> service = injector.getInstance(servicedBy);

				// ids in a collection must be concatenated
				Iterable<Object> fieldValues = AnnotatedDictionaryUtils.getValues(definition.getKey(), collection);
				if (definition.hasParameterType()) {
					final Function<Object, Iterable<Object>> castFunction = FunctionsExtended.castFunction();
					fieldValues = Iterables.concat(Iterables.transform(fieldValues, castFunction));
				}
				Iterable ids = Sets.newHashSet(fieldValues);
				Map map = service.all(ids);

				if (LOG.isLoggable(Level.FINE)) {
					LOG.fine("replacing " + adapterType + " for " + idType + " batching ids " + ids);
				}
				replaceAdapter(adapterType, map);

				// now use the results to fulfill a request against the domain if the type matches
				final Class<?> memberEntityType = service.provides();
				this.typesBeingMarshalled.put(memberEntityType, map);
				return memberEntityType;
			} else {
				// this is a coding error since the field request is valid, but the developer didn't
				// finish the job
				throw new IllegalArgumentException(" provide a service to read using ServicedBy annotation " + idType);
			}
		}

		/**
		 * Out with the default and in with the requested replacement.
		 * 
		 * @param adapterType
		 * @param map
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private void replaceAdapter(Class<? extends XmlAdapter<?, ?>> adapterType, Map map) {
			// provides the value by id or null if it's not found
			// exploding here is ugly
			Function<?, ?> batchProvider = Functions.forMap(map, null);
			try {
				XmlAdapter<?, ?> xmlAdapterPreloaded;
				try {
					// FIXME: we should enforce that AccessControlled classes only use this
					// constructor
					xmlAdapterPreloaded = adapterType.getConstructor(Function.class, IdentityManager.class)
							.newInstance(batchProvider, identityManager());
				} catch (NoSuchMethodException e) {
					xmlAdapterPreloaded = adapterType.getConstructor(Function.class).newInstance(batchProvider);
				}

				// FIXME:verify loading has not already begun for this
				// adapter
				marshaller.setAdapter(xmlAdapterPreloaded);
			} catch (NoSuchMethodException e) {
				throw new UnsupportedOperationException(
						"Your XmlAdapter must receive a function to adapt id to entity. If it is AccessControlled then it should access IdentityManager as the second argument");
			} catch (InstantiationException | IllegalAccessException | InvocationTargetException | SecurityException e) {
				throw new RuntimeException(e);
			}
		}

		private IdentityManager identityManager() {
			return this.injector.getInstance(IdentityManager.class);
		}
	}// end Builder

	/** Use {@link Builder} to construct FieldXmlAdapterService */
	private FieldXmlAdapterService() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * Provides only those methods {@link FieldXmlAdapterService} needs from marshaller to fulfill
	 * it's duties. This is provided mainly for testibility.
	 * 
	 * @see MarshallerProxyPassThrough
	 * @author aroller
	 * 
	 */
	public static interface MarshallerProxy {

		public void setAdapter(XmlAdapter adapter);

		public <A extends XmlAdapter> A getAdapter(Class<A> type);
	}

	public static class MarshallerProxyPassThrough
			implements MarshallerProxy {

		private Marshaller marshaller;

		public MarshallerProxyPassThrough(Marshaller marshaller) {
			this.marshaller = marshaller;
		}

		@Override
		public void setAdapter(XmlAdapter adapter) {
			marshaller.setAdapter(adapter);

		}

		@Override
		public <A extends XmlAdapter> A getAdapter(Class<A> type) {
			return marshaller.getAdapter(type);
		}

	}

	public static interface InjectorProxy {
		public <T> T getInstance(Class<T> type);
	}

	public static class InjectorPassThrough
			implements InjectorProxy {
		final private Injector injector;

		public InjectorPassThrough(Injector injector) {
			this.injector = injector;
		}

		@Override
		public <T> T getInstance(Class<T> type) {
			return injector.getInstance(type);
		}
	}
}
