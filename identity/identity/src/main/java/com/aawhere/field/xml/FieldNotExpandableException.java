package com.aawhere.field.xml;

import com.aawhere.field.FieldKey;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

@StatusCode(value = HttpStatusCode.BAD_REQUEST, message = "{FIELD_KEY} requested, but no adapter is provided.")
public class FieldNotExpandableException
		extends BaseRuntimeException {

	private static final long serialVersionUID = -5045612793173375302L;

	public FieldNotExpandableException(FieldKey fieldKey) {
		super(FieldNotExpandableExceptionMessage.message(fieldKey));
	}

}
