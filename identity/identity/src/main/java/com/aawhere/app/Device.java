/**
 * 
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.xml.XmlNamespace;

/**
 * Represents a physical device on the planet earth. Although there requires physical hardward, the
 * device instance represents the {@link Application} represented by a device too...meaning the
 * firware or software running on the device is also represented together as a package. The
 * {@link Version} is reprorted separately which can also affect a device.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
@Dictionary(domain = Device.FIELD.DOMAIN)
public class Device
		extends Instance {

	private static final long serialVersionUID = 7890789719215968622L;

	/**
	 * Used to construct all instances of Device.
	 */
	@XmlTransient
	public static class Builder
			extends Instance.BaseBuilder<Builder, Device> {

		public Builder() {
			super(new Device());
		}

	}// end Builder

	/** Use {@link Builder} to construct Device */
	private Device() {
	}

	/** A convince method for constructing a builder */
	public static Builder createDevice() {
		return new Builder();
	}

	public static class FIELD {
		public static final String DOMAIN = "device";
	}
}
