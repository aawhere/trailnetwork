/**
 * 
 */
package com.aawhere.app;

import javax.annotation.Nullable;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Predicate;

/**
 * @author aroller
 * 
 */
public class InstallationApplicationPredicate
		implements Predicate<Installation> {

	public ApplicationKey applicationKey;

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(@Nullable Installation input) {
		return input != null && this.applicationKey.equals(input.getApplication().getKey());
	}

	/**
	 * Used to construct all instances of InstallationApplicationPredicate.
	 */
	public static class Builder
			extends ObjectBuilder<InstallationApplicationPredicate> {

		public Builder() {
			super(new InstallationApplicationPredicate());
		}

		public Builder setApplicationKey(ApplicationKey key) {
			building.applicationKey = key;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			Assertion.assertNotNull(building.applicationKey);
			super.validate();
		}
	}// end Builder

	/** Use {@link Builder} to construct InstallationApplicationPredicate */
	private InstallationApplicationPredicate() {
	}

}
