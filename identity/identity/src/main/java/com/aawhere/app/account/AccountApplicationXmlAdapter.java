/**
 * 
 */
package com.aawhere.app.account;

import com.aawhere.app.ApplicationXmlAdapter;

import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class AccountApplicationXmlAdapter
		extends ApplicationXmlAdapter {

	/**
	 * 
	 */
	public AccountApplicationXmlAdapter() {
	}

	/**
	 * @param show
	 */
	public AccountApplicationXmlAdapter(Boolean show) {
		super(show);
	}

}
