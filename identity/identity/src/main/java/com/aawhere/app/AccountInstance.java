/**
 *
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.xml.XmlNamespace;

/**
 * An {@link Instance} that represents a particular Person at a particular {@link Application}.
 * 
 * @author Brian Chapman
 * 
 * @deprecated use {@link com.aawhere.app.account.Account}
 */
@Deprecated
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class AccountInstance
		extends Instance {

	private static final long serialVersionUID = -1381947039021818586L;

	/**
	 * Used to construct all instances of User.
	 */
	@XmlTransient
	public static class Builder
			extends Instance.BaseBuilder<Builder, AccountInstance> {

		public Builder() {
			super(new AccountInstance());
		}

	}// end Builder

	/** Use {@link Builder} to construct User */
	private AccountInstance() {
	}

	/** A convince method for constructing a builder */
	public static Builder createUser() {
		return new Builder();
	}

}
