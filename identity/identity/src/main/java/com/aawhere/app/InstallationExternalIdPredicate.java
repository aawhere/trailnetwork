/**
 * 
 */
package com.aawhere.app;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

import com.google.common.base.Predicate;

/**
 * Searches History for an Installation that matches the {@link Installation#getExternalId()} for an
 * {@link Installation#getApplication()}.
 * 
 * @author Aaron Roller
 * 
 */
public class InstallationExternalIdPredicate
		implements Predicate<Installation> {

	private String externalId;
	private ApplicationKey applicationKey;

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Predicate#apply(java.lang.Object)
	 */
	@Override
	public boolean apply(Installation input) {
		return this.externalId.equals(input.getExternalId())
				&& this.applicationKey.equals(input.getApplication().getKey());
	}

	/**
	 * Used to construct all instances of InstallationExternalIdPredicate.
	 */
	public static class Builder
			extends ObjectBuilder<InstallationExternalIdPredicate> {

		public Builder() {
			super(new InstallationExternalIdPredicate());
		}

		public Builder setExternalId(String externalId) {
			building.externalId = externalId;
			return this;
		}

		public Builder setApplicationKey(ApplicationKey applicationKey) {
			building.applicationKey = applicationKey;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.externalId);
			Assertion.assertNotNull(building.applicationKey);
		}

	}// end Builder

	/** Use {@link Builder} to construct InstallationExternalIdPredicate */
	private InstallationExternalIdPredicate() {
	}

}
