/**
 * 
 */
package com.aawhere.app;

import java.util.Locale;

import com.aawhere.text.format.custom.CustomFormat;

/**
 * @author aroller
 * 
 */
public class ApplicationCustomFormat
		implements CustomFormat<Application> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#format(java.lang.Object, java.util.Locale)
	 */
	@Override
	public String format(Application application, Locale locale) {
		// TODO:localize the name here since it is
		return application.getName().getValue();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#handlesType()
	 */
	@Override
	public Class<? super Application> handlesType() {

		return Application.class;
	}

}
