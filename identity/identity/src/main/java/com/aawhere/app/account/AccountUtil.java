/**
 * 
 */
package com.aawhere.app.account;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.NotNullPredicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationKeyProvider;
import com.aawhere.app.ApplicationUtil;
import com.aawhere.app.account.Account.Builder;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldValueProvider;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.identity.IdentityPrivacyUtil;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.predicate.StringNotEmptyPredicate;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.Person;
import com.aawhere.person.PersonAccountProvider;
import com.aawhere.queue.BatchQueues;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.util.rb.MessageUtil;
import com.aawhere.util.rb.StringMessage;
import com.aawhere.xml.StringXml;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class AccountUtil {

	/**
	 * Provides the ordering of the best names first to last
	 * 
	 */
	private static final Ordering<Account> BETTER_NAME_ORDERING = managedAccountPreferred()
			.compound(betterNameOrdering());
	/**
	 * Preferring non-null emails this selects the
	 * {@link ApplicationUtil#managingApplicationOrdering()} to provide the emails in order of
	 * preference.
	 * 
	 */
	private static final Ordering<Account> BETTER_EMAIL_ORDER = managedAccountPreferred();

	/**
	 * Preferring non-null photoUrl this selects the
	 * {@link ApplicationUtil#managingApplicationOrdering()} to provide the photoUrls in order of
	 * preference.
	 * 
	 */
	private static final Ordering<Account> BETTER_PHOTOURL_ORDER = managedAccountPreferred();

	private static String EMAIL_PATTERN = "^([_A-Za-z0-9-\\+]+[\\._A-Za-z0-9-]*)@([A-Za-z0-9-]+[\\.A-Za-z0-9]*\\.[A-Za-z]{2,})$";
	/**
	 * 
	 */
	private static final Pattern EMAIL_PATTERN_COMPILED = Pattern.compile(EMAIL_PATTERN);
	private static Function<String, String> ALIAS_CLEANER_FUNCTION = aliasCleanerFunction();

	/**
	 * Given the account ID this will split it into it's parts identifying the application and
	 * account id at the application.
	 * 
	 * @param accountId
	 * @return
	 */
	public static final Pair<ApplicationKey, String> split(AccountId accountId) {
		Pair<String, String> split = IdentifierUtils.splitCompositeId(accountId);
		return Pair.of(new ApplicationKey(split.getLeft()), split.getRight());
	}

	/**
	 * Strips the email addresses from a username leaving only the username of the email addres.
	 * Lower cases all aliases.
	 * 
	 * @return
	 */
	public static Function<String, String> aliasCleanerFunction() {
		Function<String, String> userNameFunction = new Function<String, String>() {

			@Override
			public String apply(String input) {
				if (input == null) {
					return null;
				}
				input = input.toLowerCase();
				String[] split = StringUtils.split(input, '@');
				if (split.length > 1) {
					input = split[0];
				}
				return input;
			}
		};
		return userNameFunction;
	}

	/** uses {@link #ALIAS_CLEANER_FUNCTION} to standardize the alias. */
	public static String cleanupAlias(String alias) {
		return ALIAS_CLEANER_FUNCTION.apply(alias);
	}

	/** Provides the aliases cleaned up removing unwanted items like email addresses, etc. */
	public static Set<String> cleanupAliases(Set<String> aliases) {

		Iterable<String> filtered = Iterables.transform(aliases, ALIAS_CLEANER_FUNCTION);
		HashSet<String> cleanedupAliases = Sets.newHashSet(filtered);
		// TN-553 remove null values
		CollectionUtils.filter(cleanedupAliases, NotNullPredicate.getInstance());
		CollectionUtils.filter(cleanedupAliases, StringNotEmptyPredicate.getInstance());
		aliases = Sets.newHashSet(cleanedupAliases);

		return aliases;
	}

	/**
	 * Accepts the values from the updated into the original if they are provided. ignores them
	 * otherwise.
	 * 
	 * @param original
	 * @param possiblyUpdated
	 * @return the merged Account paired with the differences found (FieldKey=value) or null if
	 *         there is nothing to merge.
	 */
	public static Pair<Account, ImmutableMap<FieldKey, Object>> merged(Account original, Account possiblyUpdated) {
		ImmutableMap.Builder<FieldKey, Object> differencesBuilder = ImmutableMap.builder();
		Builder merged = Account.clone(original);
		if (original.id().equals(possiblyUpdated.id())) {
			if (possiblyUpdated.hasName()) {
				String name = possiblyUpdated.name();
				if (!name.equals(original.name())) {
					merged.setName(name);
					differencesBuilder.put(AccountField.Key.NAME, name);
				}
			}
			if (possiblyUpdated.hasPhotoUrl()) {
				URL photoUrl = possiblyUpdated.photoUrl();
				if (!photoUrl.equals(original.photoUrl())) {
					merged.photoUrl(photoUrl);
					differencesBuilder.put(AccountField.Key.PHOTO_URL, photoUrl);
				}
			}
			if (possiblyUpdated.hasAlias()) {
				Set<String> aliases = possiblyUpdated.aliases();
				Set<String> existing = original.aliases();
				if (!aliases.equals(existing)) {
					merged.addAliases(aliases);
					differencesBuilder.put(AccountField.Key.ALIASES, Sets.difference(aliases, existing));
				}
			}
			if (possiblyUpdated.hasEmail()) {
				EmailAddress email = possiblyUpdated.email();
				if (!email.equals(original.email())) {
					merged.email(email);
					differencesBuilder.put(AccountField.Key.EMAIL, email);
				}
			}
			ImmutableMap<FieldKey, Object> differences = differencesBuilder.build();

			if (differences.isEmpty()) {
				return null;
			} else {
				return Pair.of(merged.build(), differences);
			}
		} else {
			throw new IllegalArgumentException(original.id() + " cannot be updated with " + possiblyUpdated.id());
		}
	}

	/**
	 * @return
	 */
	private static Pattern emailPattern() {
		return EMAIL_PATTERN_COMPILED;
	}

	/**
	 * @param aliases
	 * @return
	 */
	public static Boolean containsEmail(Set<String> aliases) {
		if (CollectionUtils.isEmpty(aliases)) {
			return false;
		}
		// TN-711 do a null check to ensure aliases aren't coming in null.
		Collection<String> nonNullAliases = Collections2.filter(aliases, Predicates.notNull());
		return !Collections2.filter(nonNullAliases, Predicates.contains(emailPattern())).isEmpty();
	}

	/**
	 * Provides the application key from the {@link Account} using the standard
	 * {@link ApplicationKeyProvider}.
	 * 
	 * @see ApplicationUtil#applicationKeyFunction()
	 * 
	 * @return
	 */
	public static Function<Account, ApplicationKey> applicationKeyFunction() {
		return ApplicationUtil.applicationKeyFunction();
	}

	/** Filters accounts based on the given applicationKey. */
	public static Predicate<Account> applicationKeyPredicate(ApplicationKey applicationKey) {
		return Predicates.compose(ApplicationUtil.applicationKeyPredicate(applicationKey), applicationKeyFunction());
	}

	/**
	 * Given a string as a csv this will find the remote id in the first column of each line...the
	 * only item on that line. It will return {@link AccountId}s with the {@link ApplicationKey}
	 * given.
	 * 
	 * @see #remoteIdAccountIdFunction(ApplicationKey)
	 * 
	 * 
	 * @param csv
	 * @return
	 */
	public static Iterable<AccountId> accountIdsFromCsv(ApplicationKey applicationKey, String csv) {
		String[] lines = csv.split(System.lineSeparator());
		ArrayList<String> remoteIds = Lists.newArrayList(lines);
		return Iterables.transform(remoteIds, remoteIdAccountIdFunction(applicationKey));
	}

	/**
	 * Provides the aliases from the given account.
	 * 
	 * @see #aliases(Iterable)
	 * @return
	 */
	public static Function<Account, Set<String>> aliasesFunction() {
		return new Function<Account, Set<String>>() {
			@Override
			public Set<String> apply(Account input) {
				return (input == null) ? null : input.aliases();
			}
		};
	}

	public static Function<Account, AccountAliasesXml> aliasesXmlFunction() {
		return new Function<Account, AccountUtil.AccountAliasesXml>() {

			@Override
			public AccountAliasesXml apply(Account input) {
				// being an @XmlValue or @XmlList return null causes nullpointer exception...so
				// return an empty list
				return (input != null) ? new AccountAliasesXml(input.aliases()) : new AccountAliasesXml();
			}
		};
	}

	private enum EmailFunction implements Function<Account, EmailAddress> {
		INSTANCE;
		@Override
		@Nullable
		public EmailAddress apply(@Nullable Account account) {
			return (account != null) ? account.email() : null;
		}

		@Override
		public String toString() {
			return "EmailFunction";
		}
	}

	/**
	 * A function that provides the email address from the account or null if the email or account
	 * is null.
	 * 
	 * @return
	 */
	public static Function<Account, EmailAddress> emailFunction() {
		return EmailFunction.INSTANCE;
	}

	public static List<Account> accountsWithBetterEmailFirst(Iterable<Account> accounts) {
		accounts = accountsWithEmail(accounts);
		return BETTER_EMAIL_ORDER.sortedCopy(accounts);
	}

	private enum PhotoUrlFunction implements Function<Account, URL> {
		INSTANCE;
		@Override
		@Nullable
		public URL apply(@Nullable Account account) {
			return (account != null) ? account.photoUrl() : null;
		}

		@Override
		public String toString() {
			return "PhotoUrlFunction";
		}
	}

	/**
	 * A function that provides the photo url from the account or null if the photo url or account
	 * is null.
	 * 
	 * @return
	 */
	public static Function<Account, URL> photoUrlFunction() {
		return PhotoUrlFunction.INSTANCE;
	}

	public static List<Account> accountsWithBetterPhotoUrlFirst(Iterable<Account> accounts) {
		accounts = accountsWithPhotoUrl(accounts);
		return BETTER_PHOTOURL_ORDER.sortedCopy(accounts);
	}

	/**
	 * A filter retaining only those accounts that have emails.
	 * 
	 * @param accounts
	 * @return
	 */
	public static Iterable<Account> accountsWithEmail(Iterable<Account> accounts) {
		return Iterables.filter(accounts, Predicates.compose(Predicates.notNull(), emailFunction()));
	}

	/**
	 * A filter retaining only those accounts that have photoUrls.
	 * 
	 * @param accounts
	 * @return
	 */
	public static Iterable<Account> accountsWithPhotoUrl(Iterable<Account> accounts) {
		return Iterables.filter(accounts, Predicates.compose(Predicates.notNull(), photoUrlFunction()));
	}

	/** provides unique aliases removing any knowledge of the application from which the alias came. */
	@Nullable
	public static Set<String> aliases(@Nullable Iterable<Account> accounts) {
		if (accounts == null) {
			return null;
		}
		return Sets.newHashSet(Iterables.concat(Iterables.transform(accounts, aliasesFunction())));
	}

	/**
	 * Converts any remote id given into the {@link AccountId} for the {@link ApplicationKey} given.
	 * 
	 * @param applicationKey
	 * @return
	 */
	public static Function<String, AccountId> remoteIdAccountIdFunction(final ApplicationKey applicationKey) {
		return new Function<String, AccountId>() {

			@Override
			public AccountId apply(String remoteId) {
				return new AccountId(applicationKey, remoteId);
			}
		};
	}

	public static Function<Account, String> nameFunction() {
		return new Function<Account, String>() {

			@Override
			public String apply(Account input) {
				return (input == null) ? null : input.name();
			}
		};
	}

	/**
	 * Optionally provides identifying information only if the {@link Person#identityShared()} is
	 * true.
	 * 
	 * @param provider
	 *            provides the person given the account information
	 * @param identityFunction
	 *            extracts identifying information from the account (allowing multiple uses of this)
	 * @see #nameFunction()
	 * @see #aliasesFunction()
	 * @return
	 */
	public static <T> Function<Account, T> identityPrivacyFunction(final PersonAccountProvider provider,
			final Function<Account, T> identityFunction) {
		return new Function<Account, T>() {

			@Override
			public T apply(Account input) {
				if (input == null || provider == null) {
					return null;
				}
				try {
					Person person = provider.person(input.id());
					return IdentityPrivacyUtil.filterIdentity(person.identityShared(), identityFunction.apply(input));
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/**
	 * Provides any names from the given accounts if they exist. Nulls are filtered out so empty
	 * Iterable is possible.
	 */
	public static Iterable<String> names(Iterable<Account> accounts) {
		return Iterables.filter(Iterables.transform(accounts, nameFunction()), Predicates.notNull());
	}

	public static AccountProvider provider(Function<AccountId, Account> idFunction) {
		return new AccountProvider.Default(idFunction);
	}

	/**
	 * Useful when used with {@link BatchQueues}.
	 * 
	 * @param queueFactory
	 * @return
	 */
	public static Function<ApplicationKey, Queue> queueProvider(final QueueFactory queueFactory) {
		return new Function<ApplicationKey, Queue>() {

			@Override
			public Queue apply(ApplicationKey input) {
				return AccountWebApiUri.getCrawlQueue(queueFactory, input);
			}
		};
	}

	/**
	 * @see AccountField#ACTIVITY_COUNT
	 * @return
	 */
	public static Function<AccountId, Integer> activityCountProviderFunction(FieldValueProvider fieldValueProvider) {
		return FieldValueProvider.Util.function(AccountField.Key.ACTIVITY_COUNT, fieldValueProvider);
	}

	/**
	 * Provides a {@link StringMessage} wrapper around {@link Account#name()}.
	 * 
	 * @return
	 */
	public static Function<Account, StringMessage> nameMessageFunction() {
		return Functions.compose(MessageUtil.stringToMessageFunction(), AccountUtil.nameFunction());
	}

	/**
	 * @return
	 */
	public static Function<Account, StringMessage> aliasMessageFunction() {
		return Functions.compose(MessageUtil.stringToMessageFunction(), aliasFunction());
	}

	public static Function<Account, StringXml> aliasXmlFunction() {
		return Functions.compose(StringXml.function(), aliasFunction());
	}

	/**
	 * Provides the best alias for the account since most have only one and most would only be
	 * interested in one.
	 * 
	 * @return
	 */
	public static Function<Account, String> aliasFunction() {
		return new Function<Account, String>() {

			@Override
			public String apply(Account input) {
				return (input != null) ? input.alias() : null;
			}
		};
	}

	/**
	 * Using {@link XmlValue} and {@link XmlList} this returns a single element with space-dilimeted
	 * aliases. It must not return null so if it isn't found then return the default constructor
	 * {@link AccountAliasesXml#AccountAliasesXml()}.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlRootElement
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.NONE)
	public static class AccountAliasesXml {

		/**
		 * {@link XmlList} as the only value of this adapter.
		 */
		@XmlValue
		@Nullable
		private final Set<String> all;

		/** return this instead of null */
		public AccountAliasesXml() {
			this.all = ImmutableSet.of();
		}

		AccountAliasesXml(Set<String> aliases) {
			this.all = aliases;
		}
	}

	/**
	 * provides the accounts that are by applications that are managed.
	 * 
	 * @see ApplicationUtil#managingApplicationOrdering()
	 * 
	 * @return
	 */
	public static Ordering<Account> managedAccountPreferred() {
		return ApplicationUtil.managingApplicationOrdering().nullsLast().onResultOf(applicationKeyFunction());
	}

	public static Ordering<AccountId> managedAccountIdPreferred() {
		return ApplicationUtil.managingApplicationOrdering().nullsLast().onResultOf(applicationKeyFromIdFunction());
	}

	/**
	 * indicates true if any of the ids provided is from a managing Application
	 * 
	 * @see ApplicationUtil#managingApplication(ApplicationKey)
	 * @param accountIds
	 * @return
	 */
	public static Boolean managingAccountIdContained(Iterable<AccountId> accountIds) {
		return !managingAccountIds(accountIds).isEmpty();
	}

	public static Set<AccountId> managingAccountIds(Iterable<AccountId> accountIds) {
		Function<AccountId, Boolean> managingAccountFunction = managingAccountIdFunction();
		Predicate<AccountId> managingAccountPredicate = managingAccountIdPredicate(managingAccountFunction);
		Iterable<AccountId> filtered = Iterables.filter(accountIds, managingAccountPredicate);
		return ImmutableSet.<AccountId> builder().addAll(filtered).build();
	}

	public static Function<AccountId, Boolean> managingAccountIdFunction() {
		return Functions.compose(ApplicationUtil.managingApplicationFunction(), applicationKeyFromIdFunction());
	}

	public static Predicate<AccountId> managingAccountIdPredicate(Function<AccountId, Boolean> managingAccountFunction) {
		return Predicates.compose(Predicates.equalTo(Boolean.TRUE), managingAccountFunction);
	}

	private enum ApplicationKeyFromIdFunction implements Function<AccountId, ApplicationKey> {
		INSTANCE;
		@Override
		@Nullable
		public ApplicationKey apply(@Nullable AccountId accountId) {
			return (accountId != null) ? accountId.getApplicationKey() : null;
		}

		@Override
		public String toString() {
			return "ApplicationKeyFromIdFunction";
		}
	}

	public static Function<AccountId, ApplicationKey> applicationKeyFromIdFunction() {
		return ApplicationKeyFromIdFunction.INSTANCE;
	}

	/**
	 * @param accounts
	 * @return
	 */
	public static List<Account> accountsWithBetterNameFirst(Iterable<Account> accounts) {
		return BETTER_NAME_ORDERING.sortedCopy(accounts);
	}

	/** Provides the ordering which will produce the accounts with the prefered names first. */
	private static Ordering<Account> betterNameOrdering() {
		// notice the reverse at the end makes this descending
		return new Ordering<Account>() {

			@Override
			public int compare(Account left, Account right) {
				// possible endhancements:
				// 1. Application preference...maybe one source is more reliable
				// 2. Use multisset to find any repeats and weight those higher since it is found at
				// multiple applications
				// for the moment managing accounts trump any other account WW-296
				return compareName(left.name(), right.name());
			}
		}.reverse();
	}

	/**
	 * Provides the {@link Comparator} resutl comparing two names.
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static int compareName(String left, String right) {
		int leftScore = scoreName(left);
		int rightScore = scoreName(right);
		return leftScore - rightScore;
	}

	/** Inspecting the names this returns a score for each to help with picking the best. */
	public static int scoreName(String name) {
		int score = 0;
		if (name != null) {
			// this leaves room for improvement...so improve it.
			score += (name.contains(" ")) ? 5 : 0;
			score += (StringUtils.isAllLowerCase(name)) ? 0 : 2;
			score += (StringUtils.isAllUpperCase(name)) ? 0 : 1;
		}
		return score;
	}

	/**
	 * Provides the profile page reference from a provider (ApplicationActivityImportInstructions).
	 * 
	 * @param provider
	 * @return the URL or null if not found
	 */
	public static Function<Account, URL> accountProfilePageUrlFunction(final AccountProvider.ProfilePage provider) {
		return new Function<Account, URL>() {

			@Override
			public URL apply(Account input) {
				try {
					return (provider != null) ? new URL(provider.profilePageUrl(input)) : null;
				} catch (MalformedURLException malformed) {
					LoggerFactory.getLogger(getClass()).warning(input + " failed to find valid url from " + provider
							+ " because " + malformed.getMessage());
					return null;
				} catch (BaseException e) {
					// this is how an application communicates it is not possible so no need to log
					return null;
				}
			}
		};
	}

}
