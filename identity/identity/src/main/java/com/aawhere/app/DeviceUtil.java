/**
 * 
 */
package com.aawhere.app;

import static com.aawhere.lang.string.StringUtilsExtended.*;
import static org.apache.commons.lang3.StringUtils.*;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.app.DeviceCapabilities.SignalQuality;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

/**
 * @author aroller
 * 
 */
public class DeviceUtil {

	private static Function<DeviceCapabilities, SignalQuality> GPS_SIGNAL_QUALITY_FUNCTION = DeviceCapabilities
			.gpsSignalQualityFunction();
	private static Function<DeviceCapabilities, SignalQuality> ELEVATION_SIGNAL_QUALITY_FUNCTION = DeviceCapabilities
			.elevationSignalQualityFunction();
	public static final Ordering<DeviceCapabilities> DEVICE_SIGNAL_QUALITY_ORDERING = deviceSignalQualityBetterFirst();
	public static final Ordering<DeviceCapabilities> GPS_SIGNAL_QUALITY_ORDERING = gpsSignalQualityBetterFirst();
	public static final Ordering<DeviceCapabilities> ELEVATION_SIGNAL_QUALITY_ORDERING = elevationSignalQualityBetterFirst();

	public static Ordering<DeviceCapabilities> gpsSignalQualityBetterFirst() {
		return Ordering.natural().nullsFirst().reverse().onResultOf(GPS_SIGNAL_QUALITY_FUNCTION);
	}

	public static Ordering<DeviceCapabilities> elevationSignalQualityBetterFirst() {
		return Ordering.natural().nullsFirst().reverse().onResultOf(ELEVATION_SIGNAL_QUALITY_FUNCTION);
	}

	/**
	 * Provides a compound comparator resulting in providing the devices with the better signal
	 * first. This is the natural ordering for {@link DeviceCapabilities}. See
	 * {@link DeviceCapabilities#compareTo(DeviceCapabilities)}.
	 * 
	 * @see #DEVICE_SIGNAL_QUALITY_ORDERING
	 */
	static Ordering<DeviceCapabilities> deviceSignalQualityBetterFirst() {
		return gpsSignalQualityBetterFirst().compound(elevationSignalQualityBetterFirst());
	}

	/** Helper to sort removing nulls. */
	public static List<DeviceCapabilities> signalQualityBetterFirst(Iterable<DeviceCapabilities> capabilities) {
		final Iterable<DeviceCapabilities> filtered = Iterables.filter(capabilities, Predicates.notNull());
		final List<DeviceCapabilities> sortedCopy = deviceSignalQualityBetterFirst().sortedCopy(filtered);
		return sortedCopy;
	}

	public static Function<Device, DeviceCapabilities> capabilitiesFunction(DeviceCapabilitiesRepository repository) {
		return Functions.compose(repository.capabilitiesFunction(), keyFunction());
	}

	/** @see #keyCleansed(ApplicationKey) */
	public static Function<ApplicationKey, ApplicationKey> keyCleansingFunction() {
		return new Function<ApplicationKey, ApplicationKey>() {

			@Override
			public ApplicationKey apply(ApplicationKey input) {
				return (input == null) ? null : keyCleansed(input);
			}
		};
	}

	/**
	 * Provides the key from the Device. Also calls {@link #keyCleansed(ApplicationKey)}.
	 * 
	 * @return
	 */
	public static Function<Device, ApplicationKey> keyFunction() {
		return new Function<Device, ApplicationKey>() {

			@Override
			public ApplicationKey apply(Device input) {
				return keyCleansed(key(input));
			}
		};
	}

	/**
	 * @param device
	 * @return
	 */
	public static ApplicationKey key(Device input) {
		return (input == null) ? null : input.getApplication().getKey();
	}

	/**
	 * @param applicationKey
	 * @return
	 */
	public static ApplicationKey keyCleansed(ApplicationKey applicationKey) {
		if (applicationKey != null) {
			if (StringUtils.contains(applicationKey.getValue(), SPACE)) {
				String cleansed = StringUtils.replaceChars(applicationKey.getValue(), SPACE, EMPTY);
				applicationKey = new ApplicationKey(cleansed);
			}
		}
		return applicationKey;
	}

}
