/**
 * 
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Supplier;
import com.googlecode.objectify.annotation.Index;

/**
 * Useful for producing summary statistics of items that reference an {@link Application}.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = ApplicationStats.FIELD.DOMAIN)
public class ApplicationStats
		extends CategoricalStatistic<Application> {

	/**
	 * Used to construct all instances of ApplicationStats.
	 */
	@XmlTransient
	public static class Builder
			extends CategoricalStatistic.Builder<ApplicationStats, Application, Builder> {

		public Builder() {
			super(new ApplicationStats());
		}

		/**
		 * @param mutant
		 */
		public Builder(ApplicationStats mutant) {
			super(mutant);
		}

		public Builder application(Application application) {
			building.application = application;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.calc.CategoricalStatistic.Builder#category(java.lang.Object)
		 */
		@Override
		public Builder category(Application category) {
			return application(category);
		}

	}// end Builder

	/** Use {@link Builder} to construct ApplicationStats */
	private ApplicationStats() {
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(ApplicationStats mutant) {
		return new Builder(mutant);
	}

	/**
	 * This is not an embedded dictionary because {@link Application} is translated to it's key at
	 * the datastore.
	 */
	@XmlElement(name = FIELD.APPLICATION)
	@Field(key = FIELD.APPLICATION, indexedFor = "TN-585")
	@Index
	private Application application;

	/**
	 * @return the application
	 */
	public Application application() {
		return this.application;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.calc.CategoricalStatistic#category()
	 */
	@Override
	public Application category() {
		return application();
	}

	public static Supplier<Builder> builderSupplier() {
		return new Supplier<ApplicationStats.Builder>() {

			@Override
			public Builder get() {
				return create();
			}
		};
	}

	/**
	 * Builds and counts the given applications.
	 * 
	 * @param applications
	 */
	public static CategoricalStatsCounter<ApplicationStats, Application> counter(Iterable<Application> applications) {
		final CategoricalStatsCounter.Builder<ApplicationStats, Application, Builder> builder = CategoricalStatsCounter
				.create();
		return builder.categories(applications).statsBuilder(builderSupplier()).build();
	}

	public static class FIELD {
		public static final String DOMAIN = "applicationStats";
		public static final String APPLICATION = "application";

		public static class KEY {
			public static final FieldKey APPLICATION = new FieldKey(DOMAIN, FIELD.APPLICATION);
			public static final FieldKey APPLICATION_KEY = new FieldKey(FIELD.APPLICATION,
					Application.FIELD.APPLICATION_KEY);
			public static final FieldKey PERCENT = new FieldKey(FIELD.DOMAIN, CategoricalStatistic.FIELD.PERCENT);
		}
	}

	private static final long serialVersionUID = 9134111326049962213L;
}
