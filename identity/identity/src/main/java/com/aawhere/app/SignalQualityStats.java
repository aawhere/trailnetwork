/**
 * 
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Supplier;

/**
 * Useful for producing summary statistics of items that reference an {@link SignalQuality}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = SignalQualityStats.FIELD.DOMAIN)
public class SignalQualityStats
		extends CategoricalStatistic<SignalQuality> {

	/**
	 * Used to construct all instances of SignalQualityStats.
	 */
	@XmlTransient
	public static class Builder
			extends CategoricalStatistic.Builder<SignalQualityStats, SignalQuality, Builder> {

		public Builder() {
			super(new SignalQualityStats());
		}

		/**
		 * @param mutant
		 */
		public Builder(SignalQualityStats mutant) {
			super(mutant);
		}

		public Builder signalQuality(SignalQuality signal) {
			building.signalQuality = signal;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.calc.CategoricalStatistic.Builder#category(java.lang.Object)
		 */
		@Override
		public Builder category(SignalQuality category) {
			return signalQuality(category);
		}

	}// end Builder

	private SignalQualityStats() {
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(SignalQualityStats mutant) {
		return new Builder(mutant);
	}

	@XmlElement(name = FIELD.SIGNAL_QUALITY)
	@Field(key = FIELD.SIGNAL_QUALITY)
	private SignalQuality signalQuality;

	/**
	 * @return the SignalQuality
	 */
	public SignalQuality signalQuality() {
		return this.signalQuality;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.calc.CategoricalStatistic#category()
	 */
	@Override
	public SignalQuality category() {
		return signalQuality();
	}

	public static Supplier<Builder> builderSupplier() {
		return new Supplier<SignalQualityStats.Builder>() {

			@Override
			public Builder get() {
				return create();
			}
		};
	}

	/**
	 * Builds and counts the given signals.
	 * 
	 * @param signals
	 */
	public static CategoricalStatsCounter<SignalQualityStats, SignalQuality> counter(Iterable<SignalQuality> signals) {
		final CategoricalStatsCounter.Builder<SignalQualityStats, SignalQuality, Builder> builder = CategoricalStatsCounter
				.create();
		return builder.categories(signals).statsBuilder(builderSupplier()).build();
	}

	public static class FIELD {
		public static final String DOMAIN = "signalQualityStats";
		public static final String SIGNAL_QUALITY = "signalQuality";

	}

	private static final long serialVersionUID = 9134111326049962213L;
}
