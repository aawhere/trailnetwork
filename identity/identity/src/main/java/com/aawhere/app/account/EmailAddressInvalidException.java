package com.aawhere.app.account;

import org.apache.commons.validator.routines.EmailValidator;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

@StatusCode(value = HttpStatusCode.BAD_REQUEST, message = "{INVALID_EMAIL_ADDRESS} is not a valid email address.")
public class EmailAddressInvalidException
		extends BaseException {

	public EmailAddressInvalidException(String invalidEmailAddress) {
		super(EmailAddressInvalidExceptionMessage.message(invalidEmailAddress));
	}

	private static final long serialVersionUID = -5206293390892011281L;

	public static String validate(String emailAddress) throws EmailAddressInvalidException {
		if (!EmailValidator.getInstance().isValid(emailAddress)) {
			throw new EmailAddressInvalidException(emailAddress);
		}
		return emailAddress;
	}
}
