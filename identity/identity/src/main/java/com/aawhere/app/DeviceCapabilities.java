/**
 * 
 */
package com.aawhere.app;

import java.io.Serializable;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.enums.EnumUtil;

import com.google.common.base.Function;

/**
 * Provides properties describing a device's capabilities such as sensors and sensor quality and/or
 * an application's ability to relay that information. This is useful in picking better activities.
 * 
 * @author aroller
 * 
 */
public class DeviceCapabilities
		implements Serializable {

	public static enum SignalQuality {
		NONE, UNKNOWN, POOR, FAIR, GOOD, GREAT
	};

	static SignalQuality MAX_ELE_FOR_GPS_ONLY = SignalQuality.FAIR;
	private SignalQuality barometricAltimeterSignalQuality = SignalQuality.UNKNOWN;
	private SignalQuality gpsSignalQuality = SignalQuality.UNKNOWN;
	private SignalQuality elevationSignalQuality = SignalQuality.UNKNOWN;
	private ApplicationKey deviceKey;

	/**
	 * Used to construct all instances of DeviceCapabilities.
	 */
	public static class Builder
			extends ObjectBuilder<DeviceCapabilities> {

		public Builder() {
			super(new DeviceCapabilities());
		}

		public Builder baro(SignalQuality barometricAltimeter) {
			building.barometricAltimeterSignalQuality = barometricAltimeter;
			return this;
		}

		public Builder gps(SignalQuality gpsSignalQuality) {
			building.gpsSignalQuality = gpsSignalQuality;
			return this;
		}

		public Builder ele(SignalQuality elevationSignalQuality) {
			building.elevationSignalQuality = elevationSignalQuality;
			return this;
		}

		public Builder key(ApplicationKey deviceKey) {
			building.deviceKey = deviceKey;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("deviceKey", building.deviceKey);

		}

		@Override
		public DeviceCapabilities build() {
			DeviceCapabilities built = super.build();
			if (built.elevationSignalQuality == SignalQuality.UNKNOWN) {
				built.elevationSignalQuality = EnumUtil.min(built.barometricAltimeterSignalQuality,
															built.gpsSignalQuality);
			}
			// enforces elevation Signal quality for gps only is no better than fair
			if (built.barometricAltimeterSignalQuality == SignalQuality.UNKNOWN
					|| built.barometricAltimeterSignalQuality == SignalQuality.NONE) {
				built.elevationSignalQuality = EnumUtil.min(MAX_ELE_FOR_GPS_ONLY, built.elevationSignalQuality);
			}
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct DeviceCapabilities */
	private DeviceCapabilities() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return the barometricAltimeterSignalQuality
	 */
	public SignalQuality barometricAltimeterSignalQuality() {
		return this.barometricAltimeterSignalQuality;
	}

	/**
	 * @return the gpsSignalQuality
	 */
	public SignalQuality gpsSignalQuality() {
		return this.gpsSignalQuality;
	}

	/**
	 * @return the deviceKey
	 */
	public ApplicationKey deviceKey() {
		return this.deviceKey;
	}

	/**
	 * @return
	 */
	public SignalQuality elevationSignalQuality() {
		return this.elevationSignalQuality;
	}

	private static final long serialVersionUID = 6933752445049755242L;

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.deviceKey == null) ? 0 : this.deviceKey.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeviceCapabilities other = (DeviceCapabilities) obj;
		if (this.deviceKey == null) {
			if (other.deviceKey != null)
				return false;
		} else if (!this.deviceKey.equals(other.deviceKey))
			return false;
		return true;
	}

	public static Function<DeviceCapabilities, SignalQuality> gpsSignalQualityFunction() {
		return new Function<DeviceCapabilities, DeviceCapabilities.SignalQuality>() {

			@Override
			public SignalQuality apply(DeviceCapabilities input) {
				return (input == null) ? null : input.gpsSignalQuality;
			}
		};
	}

	public static Function<DeviceCapabilities, SignalQuality> elevationSignalQualityFunction() {
		return new Function<DeviceCapabilities, DeviceCapabilities.SignalQuality>() {

			@Override
			public SignalQuality apply(DeviceCapabilities input) {
				return (input == null) ? null : input.elevationSignalQuality;
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.deviceKey.toString();
	}
}
