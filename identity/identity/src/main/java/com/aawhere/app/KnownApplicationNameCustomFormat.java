/**
 * 
 */
package com.aawhere.app;

import java.util.Locale;

import com.aawhere.app.KnownApplication.Name;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Localizes the names of a Known Application which may commonly be used in {@link CompleteMessage}
 * s.
 * 
 * @author aroller
 * 
 */
public class KnownApplicationNameCustomFormat
		implements CustomFormat<Name> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#format(java.lang.Object, java.util.Locale)
	 */
	@Override
	public String format(Name name, Locale locale) {
		// TODO:localize this
		return name.getValue();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#handlesType()
	 */
	@Override
	public Class<? super Name> handlesType() {
		return Name.class;
	}

}
