/**
 *
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.id.StringIdentifier;
import com.aawhere.xml.XmlNamespace;

/**
 * The unique identifier for an installation of an {@link Application}. This alone can not identify
 * a device, but when combined with the {@link Application} it gains and can globally identify a
 * device in this world.
 * 
 * Examples:
 * 
 * Garmin's UnitId for devices, username, phone id, etc.
 * 
 * @author Aaron Roller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
public class InstanceKey
		extends StringIdentifier<Application> {

	private static final long serialVersionUID = 2228750746522526941L;

	/**
	 * Default constructor available to persistence, web and xml libraries.
	 * 
	 * @param kind
	 */
	protected InstanceKey() {
		super(Application.class);
	}

	/**
	 * The primary constructor used to provide the key.
	 * 
	 * @param value
	 * @param kind
	 */
	public InstanceKey(String value) {
		super(value, Application.class);
	}

}
