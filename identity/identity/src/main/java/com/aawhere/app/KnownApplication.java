/**
 *
 */
package com.aawhere.app;

import static com.aawhere.swagger.DocSupport.Allowable.*;

import java.util.HashMap;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.enums.EnumWithDefault;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * For those Applications that are referenced in the application, this lists them with their
 * elements. These relate to the system by the {@link #key} and do not have the
 * {@link Application#getId()} since that is managed by persistence. Use this enumeration with the
 * ServiceStandard to retrieve the fully loaded version of the Application.
 * 
 * Other Applications can be created dynamically or by user input.
 * 
 * Per the requirements of {@link ApplicationKey} all key values must be lowercase.
 * 
 * @author roller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public enum KnownApplication implements EnumWithDefault<KnownApplication> {

	/**
	 * http://connect.garmin.com
	 */
	GARMIN_CONNECT(Name.GARMIN_CONNECT, KnownApplication.GARMIN_CONNECT_KEY, false),
	/** Http://2peak.com The activities posted by the customers. */
	TWO_PEAK_ACTIVITIES(
			Name.TWO_PEAK_ACTIVITIES,
			KnownApplication.TWO_PEAK_ACTIVITY_KEY,
			false,
			KnownApplication.TWO_PEAK_ALIAS_KEY),
	/**
	 * Two peak has another data structure called sections so it is considered an entirely different
	 * application. Sections are portions of activities.
	 */
	TWO_PEAK_SECTIONS(Name.TWO_PEAK_SECTIONS, "2ps", true),
	/**
	 * TrailNetwork and it's API are currently the same system.
	 */
	TRAIL_NETWORK(Name.TRAIL_NETWORK, "tn", false),
	/** www.everytrail.com" */
	EVERY_TRAIL(Name.EVERY_TRAIL, KnownApplication.EVERY_TRAIL_KEY, false),
	/** Map My Fitness. */
	MMF(Name.MMF, KnownApplication.MMF_KEY, false),
	/**
	 * When we don't know the application using this to indicate.
	 */
	UNKNOWN(Name.UNKNOWN, "unknown", false),
	/** Ride with GPS http://ridewithgps.com/ */
	RWGPS(Name.RWGPS, "rw", false),
	/** Web Application Accounts https://developers.google.com/identity-toolkit/ **/
	GOOGLE_IDENTITY_TOOLKIT(Name.GOOGLE_IDENTITY_TOOLKIT, "gi", false);

	/**
	 * This is available for constant reference in annotations. Typical use would reference
	 * {@link #asApplication()}.
	 * 
	 */
	public static final String GARMIN_CONNECT_KEY = "gc";
	public static final String MMF_KEY = "mm";
	public static final String EVERY_TRAIL_KEY = "et";
	/**
	 *
	 */
	public static final String TWO_PEAK_ACTIVITY_KEY = "2p";
	public static final String TWO_PEAK_ALIAS_KEY = "2peak";
	private static BiMap<ApplicationKey, Application> appsByKey;
	private static HashMap<ApplicationKey, Application> aliasesByKey;

	static {
		aliasesByKey = new HashMap<>();
		KnownApplication[] knownApplications = getValidChoices();
		appsByKey = HashBiMap.create(knownApplications.length);
		for (KnownApplication knownApplication : knownApplications) {
			Application app = knownApplication.asApplication();
			appsByKey.put(app.getKey(), app);
			for (String alias : knownApplication.aliases) {
				aliasesByKey.put(new ApplicationKey(alias), app);
			}
		}

	}

	private String[] aliases;
	private Application application;
	public final ApplicationKey key;
	public final Name name;

	private KnownApplication(Name name, String key, Boolean derived, String... aliases) {
		this.key = new ApplicationKey(key);
		this.name = name;
		this.application = new Application.Builder().setKey(this.key).setName(name).setDerived(derived).build();
		this.aliases = aliases;

	}

	public Application asApplication() {
		return this.application;
	}

	/**
	 * @return the aliases
	 */
	public String[] getAliases() {
		return this.aliases;
	}

	/**
	 * Indicates the good choices to be offered.
	 * 
	 * @return
	 */
	public static KnownApplication[] getValidChoices() {
		return values();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {

		return this.application.getKey().getValue();
	}

	/**
	 * Provides the Application given the key. This will report an
	 * {@link UnknownApplicationException} if the app is not known useful when users are providing
	 * keys.
	 * 
	 * use {@link #byKnownKey(ApplicationKey)} if such reporting is not desired.
	 * 
	 * @see #byKnownKey(ApplicationKey)
	 * 
	 * @param key
	 * @return
	 * @throws UnknownApplicationException
	 *             a runtime exception indicating a bad key was called. It isn't intended to be
	 *             caught since you, the internal developer, should be using
	 *             {@link KnownApplication}. Use {@link #exists(ApplicationKey)} before if needed.
	 */
	public static Application byKey(ApplicationKey key) throws UnknownApplicationException {
		return byKey(key, aliasesByKey.get(key));
	}

	public static Application byKeyWithDefault(ApplicationKey key) {
		return byKey(key, UNKNOWN.getDefault().application);
	}

	/** Returns the default if provided or throws an exception if not. */
	public static Application byKey(ApplicationKey key, Application defaultApplication)
			throws UnknownApplicationException {
		Application app = appsByKey.get(key);
		if (app == null) {
			if (defaultApplication != null) {
				// handling future additions. TN-245
				app = defaultApplication;
			} else {
				throw new UnknownApplicationException(key);
			}
		}
		return app;
	}

	/**
	 * indicates if the {@link Application} identified by {@link #key} will be provided when calling
	 * {@link #byKey(ApplicationKey)}.
	 * 
	 * @param key
	 * @return
	 */
	public static boolean exists(ApplicationKey key) {
		return appsByKey.containsKey(key);
	}

	/** Provides a unique view into all known applications. */
	public static Set<Application> all() {
		return appsByKey.inverse().keySet();
	}

	public static enum Name implements Message {

		/** Explain_Entry_Here */
		GARMIN_CONNECT("Garmin Connect"),
		TWO_PEAK_ACTIVITIES("2PEAK"),
		TWO_PEAK_SECTIONS("2PEAK"),
		UNKNOWN("Unknown"),
		TRAIL_NETWORK("TrailNetwork"),
		EVERY_TRAIL("EveryTrail"),
		MMF("Map My Fitness"),
		MMFR("Map My Fitness Routes"),
		RWGPS("RideWithGPS"),
		GOOGLE_IDENTITY_TOOLKIT("Google Identity Toolkit");
		private ParamKey[] parameterKeys;
		private String message;

		private Name(String message, ParamKey... parameters) {
			this.message = message;
			this.parameterKeys = parameters;
		}

		@Override
		public String getValue() {
			return this.message;
		}

		@Override
		public ParamKey[] getParameterKeys() {
			return this.parameterKeys;
		}

		public enum Param implements ParamKey {
			/** EXPLAIN_PARAM_HERE */
			FIRST_PARAM;

			CustomFormat<?> format;

			private Param() {
			}

			private Param(CustomFormat<?> format) {
				this.format = format;
			}

			@Override
			public CustomFormat<?> getFormat() {
				return this.format;
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.lang.enums.EnumWithDefault#getDefault()
	 */
	@Override
	public KnownApplication getDefault() {
		return UNKNOWN;
	}

	public static final class Doc {
		public static final String ALLOWABLE_KEYS = OPEN + GARMIN_CONNECT_KEY + NEXT + MMF_KEY + NEXT + EVERY_TRAIL_KEY
				+ CLOSE;
	}
}
