/**
 * 
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.NotImplementedException;

import com.aawhere.id.StringIdentifier;
import com.aawhere.xml.XmlNamespace;

/**
 * Temporary implementation of the general {@link ApplicationDataId} until it becomes the parent.
 * 
 * @author aroller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE, name = "applicationDataId")
public class ApplicationDataIdImpl
		extends StringIdentifier<ApplicationDataId.Data>
		implements ApplicationDataId {

	private static final long serialVersionUID = -1080388728114663403L;

	/**
	 * 
	 */
	ApplicationDataIdImpl() {
		this(null);
	}

	/**
	 * @param remoteId
	 */
	public ApplicationDataIdImpl(String id) {
		super(id, Data.class);
	}

	@Override
	public String getRemoteId() {

		throw new NotImplementedException();
	}

	@Override
	public ApplicationKey getApplicationKey() {

		throw new NotImplementedException();
	}

}
