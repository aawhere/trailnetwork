/**
 * 
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Supplier;

/**
 * Useful for producing summary statistics of items that reference an {@link ApplicationKeyKey}.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = ApplicationKeyStats.FIELD.DOMAIN)
public class ApplicationKeyStats
		extends CategoricalStatistic<ApplicationKey> {

	/**
	 * Used to construct all instances of ApplicationKeyStats.
	 */
	@XmlTransient
	public static class Builder
			extends CategoricalStatistic.Builder<ApplicationKeyStats, ApplicationKey, Builder> {

		public Builder() {
			super(new ApplicationKeyStats());
		}

		/**
		 * @param mutant
		 */
		public Builder(ApplicationKeyStats mutant) {
			super(mutant);
		}

		public Builder application(ApplicationKey application) {
			building.application = application;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.calc.CategoricalStatistic.Builder#category(java.lang.Object)
		 */
		@Override
		public Builder category(ApplicationKey category) {
			return application(category);
		}

	}// end Builder

	private ApplicationKeyStats() {
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(ApplicationKeyStats mutant) {
		return new Builder(mutant);
	}

	/**
	 * This is not an embedded dictionary because {@link ApplicationKey} is translated to it's key
	 * at the datastore.
	 */
	@XmlElement(name = FIELD.APPLICATION_KEY)
	@Field(key = FIELD.APPLICATION_KEY)
	private ApplicationKey application;

	/**
	 * @return the application
	 */
	public ApplicationKey application() {
		return this.application;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.calc.CategoricalStatistic#category()
	 */
	@Override
	public ApplicationKey category() {
		return application();
	}

	public static Supplier<Builder> builderSupplier() {
		return new Supplier<ApplicationKeyStats.Builder>() {

			@Override
			public Builder get() {
				return create();
			}
		};
	}

	/**
	 * Builds and counts the given applications.
	 * 
	 * @param applications
	 */
	public static CategoricalStatsCounter<ApplicationKeyStats, ApplicationKey> counter(
			Iterable<ApplicationKey> applications) {
		final CategoricalStatsCounter.Builder<ApplicationKeyStats, ApplicationKey, Builder> builder = CategoricalStatsCounter
				.create();
		return builder.categories(applications).statsBuilder(builderSupplier()).build();
	}

	public static class FIELD {
		public static final String DOMAIN = "applicationStats";
		public static final String APPLICATION_KEY = Application.FIELD.APPLICATION_KEY;

		public static class KEY {
			public static final FieldKey APPLICATION_KEY = new FieldKey(DOMAIN, FIELD.APPLICATION_KEY);
			public static final FieldKey PERCENT = new FieldKey(FIELD.DOMAIN, CategoricalStatistic.FIELD.PERCENT);
		}
	}

	private static final long serialVersionUID = 9134111326049962213L;
}
