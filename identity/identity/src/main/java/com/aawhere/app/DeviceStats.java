/**
 * 
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Supplier;

/**
 * Useful for producing summary statistics of items that reference an {@link Device}. Notice that a
 * device is a combination of both the {@link Application} and the {@link InstanceKey} which in the
 * case of the device is a unique identifier for the individual hardware or software installation on
 * hardware. So this may be too detailed of information for most situations, but it would tell you
 * about any commononality used with a specific device. It was written first before realizing this
 * so ApplicationKeyStats is used to produce the stats for the types of devices.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = DeviceStats.FIELD.DOMAIN)
public class DeviceStats
		extends CategoricalStatistic<Device> {

	/**
	 * Used to construct all instances of ApplicationStats.
	 */
	@XmlTransient
	public static class Builder
			extends CategoricalStatistic.Builder<DeviceStats, Device, Builder> {

		public Builder() {
			super(new DeviceStats());
		}

		/**
		 * @param mutant
		 */
		public Builder(DeviceStats mutant) {
			super(mutant);
		}

		public Builder device(Device device) {
			building.device = device;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.measure.calc.CategoricalStatistic.Builder#category(java.lang.Object)
		 */
		@Override
		public Builder category(Device category) {
			return device(category);
		}

	}// end Builder

	/** Use {@link Builder} to construct ApplicationStats */
	private DeviceStats() {
	}

	public static Builder create() {
		return new Builder();
	}

	public static Builder mutate(DeviceStats mutant) {
		return new Builder(mutant);
	}

	/**
	 * This is not an embedded dictionary because {@link Application} is translated to it's key at
	 * the datastore.
	 */
	@XmlElement(name = FIELD.DEVICE)
	@Field(key = FIELD.DEVICE)
	private Device device;

	/**
	 * @return the device
	 */
	public Device device() {
		return this.device;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.measure.calc.CategoricalStatistic#category()
	 */
	@Override
	public Device category() {
		return device();
	}

	public static Supplier<Builder> builderSupplier() {
		return new Supplier<DeviceStats.Builder>() {

			@Override
			public Builder get() {
				return create();
			}
		};
	}

	/**
	 * Builds and counts the given devices.
	 * 
	 * @param devices
	 */
	public static CategoricalStatsCounter<DeviceStats, Device> counter(Iterable<Device> devices) {
		final CategoricalStatsCounter.Builder<DeviceStats, Device, Builder> builder = CategoricalStatsCounter.create();
		return builder.categories(devices).statsBuilder(builderSupplier()).build();
	}

	public static class FIELD {
		public static final String DOMAIN = "deviceStats";
		public static final String DEVICE = Device.FIELD.DOMAIN;

		public static class KEY {
			public static final FieldKey DEVICE = new FieldKey(DOMAIN, FIELD.DEVICE);
			public static final FieldKey DEVICE_KEY = new FieldKey(FIELD.DEVICE, Device.FIELD.DOMAIN);
			public static final FieldKey PERCENT = new FieldKey(FIELD.DOMAIN, CategoricalStatistic.FIELD.PERCENT);
		}
	}

	private static final long serialVersionUID = 9134111326049962213L;
}
