package com.aawhere.app.account;

import com.aawhere.identity.AccessScope;
import com.aawhere.identity.AccessScopedXmlAdapter;
import com.aawhere.identity.Identity;
import com.aawhere.identity.IdentityManager;
import com.aawhere.person.AccountIdentity;
import com.aawhere.person.PersonAccountProvider;
import com.google.common.base.Function;

/**
 * Provides a common way to provide protection against exposing private information in an account.
 * 
 * @author aroller
 * 
 * @param <X>
 */
abstract public class AccountAccessScopedXmlAdapter<X>
		extends AccessScopedXmlAdapter<X, Account> {
	private final PersonAccountProvider provider;

	AccountAccessScopedXmlAdapter() {
		this(null, null, null, null);
	}

	AccountAccessScopedXmlAdapter(IdentityManager identityManager, PersonAccountProvider provider, AccessScope scope,
			Function<Account, X> xmlFunction) {
		super(identityManager, scope, xmlFunction);
		this.provider = provider;
	}

	@Override
	protected Identity owner(Account from) {
		return AccountIdentity.create().accountId(from.id()).provider(this.provider).build();
	}
}
