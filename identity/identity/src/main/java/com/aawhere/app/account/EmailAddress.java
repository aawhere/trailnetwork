package com.aawhere.app.account;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.FromString;
import com.aawhere.xml.XmlNamespace;

/**
 * Simple class to indicate a string is an email address. Useful for providing privacy (TN-884) and
 * also allows storage translation for Google Datastore
 * (https://cloud.google.com/appengine/docs/java/javadoc/com/google/appengine/api/datastore/Email).
 * 
 * 
 * 
 * @author aroller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class EmailAddress
		implements Serializable, Comparable<EmailAddress> {

	// unable to use @XmlValue since JAXB throws nullpointers when a null XmlValue is returned from
	// an Adapter
	@XmlAttribute
	@Nonnull
	private String address;

	/**
	 * Used to construct all instances of Email.
	 */
	public static class Builder
			extends ObjectBuilder<EmailAddress> {

		public Builder() {
			super(new EmailAddress());
		}

		public Builder address(String value) throws EmailAddressInvalidException {
			building.address = EmailAddressInvalidException.validate(value);
			return this;
		}

		@Override
		protected void validate() {
			Assertion.exceptions().notNull("value", building.address);
		}

		@Override
		public EmailAddress build() {
			EmailAddress built = super.build();
			return built;
		}

	}// end Builder

	/**
	 * same as {@link #value()}. Conforms to {@link FromString}.
	 */
	@Override
	public String toString() {
		return address;
	}

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct Email */
	private EmailAddress() {
	}

	/**
	 * Constructor conforming to {@link FromString} allowing web insertion.
	 * 
	 * @param string
	 * @return
	 * @throws EmailAddressInvalidException
	 */
	public static EmailAddress valueOf(String string) throws EmailAddressInvalidException {
		return create().address(string).build();
	}

	private static final long serialVersionUID = 3555231983211765702L;

	/**
	 * same as {@link #toString()}
	 * 
	 * @return
	 */
	public String address() {
		return address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailAddress other = (EmailAddress) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		return true;
	}

	@Override
	public int compareTo(EmailAddress o) {
		return this.address.compareTo(o.address);
	}

}
