/**
 * 
 */
package com.aawhere.app;

import org.joda.time.DateTime;

import com.aawhere.id.Identifier;
import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * Used to indicate known systems and it's version.
 * 
 * @author roller
 * 
 */
public class KnownInstallation {

	private static final Instance instance;
	private static final String version;

	static {
		version = SystemWebApiUri.APP_VERSION;
		instance = Instance.createInstance().setApplication(KnownApplication.TRAIL_NETWORK.asApplication())
				.setKey(new InstanceKey(SystemWebApiUri.APP_NAME)).build();
	}

	/**
	 * used by all code to mark the system that is relevant to the data they are trying to track the
	 * origin.
	 */
	public static Installation thisSystem() {
		return new Installation.Builder().setInstance(instance).setVersion(version).setTimestamp(DateTime.now())
				.build();
	}

	/**
	 * Provides an installation for this system with an {@link Installation#getExternalId()}
	 * assigned with the given id parameter.
	 * 
	 * @param id
	 */
	public static Installation thisSystem(Identifier<?, ?> id) {
		return new Installation.Builder().setInstance(instance).setVersion(version)
				.setExternalId(id.getValue().toString()).build();

	}

}
