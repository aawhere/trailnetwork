/**
 * 
 */
package com.aawhere.app;

import javax.annotation.Nullable;

import com.google.common.base.Function;

/**
 * Provides the official conversion of an {@link ApplicationDataId} to it's String representation.
 * 
 * @see ApplicationUtil#applicationDataIdValueFunction()
 * @author aroller
 * 
 */
public class ApplicationDataIdToStringFunction
		implements Function<ApplicationDataId, String> {

	/*
	 * (non-Javadoc)
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	@Nullable
	public String apply(@Nullable ApplicationDataId input) {
		return (input == null) ? null : input.getValue();
	}

}
