package com.aawhere.app;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.BooleanUtils;

import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldValueProvider;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.id.KeyedEntity;
import com.aawhere.identity.IdentityManager;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.xml.CountFromIntegerXmlAdapter;
import com.aawhere.personalize.QuantityPersonalized;
import com.aawhere.util.rb.Message;
import com.aawhere.xml.XmlNamespace;
import com.aawhere.xml.bind.OptionalFunctionXmlAdapter;

import com.google.common.base.Functions;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

/**
 * Any system that participates by providing data. Applications are often Web Applications and also
 * devices like mobile phones or portable GPS units. Devices often times are identified individually
 * by providing an {@link Instance} which would articulate exactly which device the data originated.
 * Web Applications are usually a single instance and wouldn't have any {@link Instance} associated.
 * 
 * @see Installation
 * 
 * @see ApplicationTranslatorFactory
 * 
 * @author roller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = Application.FIELD.DOMAIN, messages = ApplicationMessage.class)
public final class Application
		implements KeyedEntity<ApplicationKey> {

	private static final long serialVersionUID = -1953899044024921450L;

	@XmlTransient
	public static class Builder
			extends ObjectBuilder<Application> {

		public Builder() {
			super(new Application());

		}

		public Builder setName(Message name) {
			building.name = name;
			return this;
		}

		public Builder setDerived(Boolean derived) {
			building.derived = derived;
			return this;
		}

		public Builder setKey(ApplicationKey key) {
			building.key = key;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("key", building.key);

			Assertion.assertNotNull(building.name);
		}

	}// end Builder

	// Use builder to create object.
	private Application() {
	}

	/**
	 * the display name of the Application which is presentable to users.
	 * 
	 */
	@XmlElement(name = FIELD.NAME)
	@Ignore
	private Message name;
	/**
	 * a unique simple alphanumeric with dashes identifier that is used to identifier this
	 * application
	 * 
	 */
	@XmlElement(name = FIELD.APPLICATION_KEY)
	@Field(key = FIELD.APPLICATION_KEY, indexedFor = "TN-584")
	@Index
	private ApplicationKey key;

	/**
	 * Derived applications indicate the data may have originated as a GPS track, but has since been
	 * modified and possibly duplicate other activity tracks. Examples are 2peak sections and garmin
	 * connect "courses", etc.
	 */
	@XmlAttribute
	@Ignore
	private Boolean derived;

	public Message getName() {
		return name;
	}

	/** The number of accounts for this application. */
	@Field(xmlAdapter = Application.AccountCountXmlAdapter.class)
	private Integer accountCount() {
		throw new UnsupportedOperationException("used for field definition only");
	}

	/** The number of activities imported for this application */
	@Field
	private Integer activityCount() {
		throw new UnsupportedOperationException("used for field definition only");
	}

	@XmlElement(name = ApplicationField.ACCOUNT_COUNT)
	@XmlJavaTypeAdapter(AccountCountXmlAdapter.class)
	private ApplicationKey getAccountCountXml() {
		return this.key;
	}

	/**
	 * @return the key
	 */
	@Override
	public ApplicationKey getKey() {
		return this.key;
	}

	/**
	 * @return the derived
	 */
	public Boolean isDerived() {
		return BooleanUtils.isTrue(this.derived);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name.toString() + " (" + key + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Application)) {
			return false;
		}
		Application other = (Application) obj;
		if (this.key == null) {
			if (other.key != null) {
				return false;
			}
		} else if (!this.key.equals(other.key)) {
			return false;
		}
		return true;
	}

	@XmlTransient
	public static final class FIELD {
		public static final String DOMAIN = ApplicationWebApiUri.APPLICATION;
		public static final String APPLICATION_KEY = ApplicationWebApiUri.APPLICATION_KEY;
		public static final String NAME = "name";

		public static final class KEY {
			public static final FieldKey APPLICATION_KEY = new FieldKey(DOMAIN, FIELD.APPLICATION_KEY);
		}
	}

	/**
	 * Provides the application
	 * 
	 * @param applicationKey
	 * @return
	 */
	public static Application valueOf(String applicationKey) {
		return KnownApplication.byKey(new ApplicationKey(applicationKey));
	}

	/**
	 * @return
	 */
	public static Builder create() {
		return new Builder();
	}

	@Singleton
	public static class AccountCountXmlAdapter
			extends OptionalFunctionXmlAdapter<QuantityPersonalized, ApplicationKey> {

		public AccountCountXmlAdapter() {
		}

		@Inject
		public AccountCountXmlAdapter(FieldValueProvider fieldValueProvider, IdentityManager identityManager) {
			// Declaring Integer is a non-starter because null from Integer causes Nullpointer in
			// Sun code
			// Declaring Count says it isn't handled...bummer
			super(Functions.compose(new CountFromIntegerXmlAdapter(identityManager).function(),
									ApplicationUtil.accountCountProviderFunction(fieldValueProvider)), Visibility.SHOW);
		}

	}

}
