/**
 * 
 */
package com.aawhere.app;

import java.util.Locale;

import com.aawhere.text.format.custom.CustomFormat;

/**
 * Provides proper language describing the {@link Application} when only the {@link ApplicationKey}
 * is known.
 * 
 * This is only really useful when the key matches a {@link KnownApplication} which is most of the
 * time.
 * 
 * @author aroller
 * 
 */
public class ApplicationKeyCustomFormat
		implements CustomFormat<ApplicationKey> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#format(java.lang.Object, java.util.Locale)
	 */
	@Override
	public String format(ApplicationKey key, Locale locale) {
		// TODO:Query KnownApplication for the proper name.
		return key.getValue();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.text.format.custom.CustomFormat#handlesType()
	 */
	@Override
	public Class<? super ApplicationKey> handlesType() {
		return ApplicationKey.class;
	}

}
