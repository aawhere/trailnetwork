/**
 * 
 */
package com.aawhere.app;

import com.aawhere.id.IdentifierCustomFormat;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates the {@link Application} being searched, typically identified by an
 * {@link ApplicationKey} is not known to this system.
 * 
 * This is currently a runtime because all applications within the system should be using
 * {@link KnownApplication} to request activities.
 * 
 * A runtime will bypass everyone and go right to the web service to explain the problem to an
 * external developer.
 * 
 * @author Aaron Roller
 * 
 */
@StatusCode(HttpStatusCode.NOT_FOUND)
public class UnknownApplicationException
		extends BaseRuntimeException {

	static {
		IdentifierCustomFormat.register();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 4526519563458986129L;

	/**
	 * @param message
	 */
	public UnknownApplicationException(ApplicationKey key) {
		super(new CompleteMessage.Builder(ApplicationMessage.UNKNOWN_APPLICATION)
				.addParameter(ApplicationMessage.Param.APPLICATION_KEY, key)
				.addParameter(ApplicationMessage.Param.VALID_APPLICATIONS, KnownApplication.getValidChoices()).build());
	}

}
