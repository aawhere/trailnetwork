/**
 *
 */
package com.aawhere.app;

import java.io.Serializable;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;

import com.aawhere.joda.time.xml.DateTimeXmlAdapter;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;
import com.googlecode.objectify.annotation.Unindex;

/**
 * An {@link Application} with an optional {@link Version} representing the iteration of the
 * Application and optional {@link Instance} representing a unique device serving up the
 * application.
 * 
 * The application is available only through it's instance. Although many Applications only have a
 * single Instance since they are web applications, that single instance can be provided.
 * 
 * @author roller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Unindex
public class Installation
		implements Serializable {

	private static final long serialVersionUID = 2902250857720969640L;

	/** A convenience method for constructing a builder */
	public static Builder create() {
		return new Builder();
	}

	@XmlElement
	@Nullable
	// See http://code.google.com/p/objectify-appengine/issues/detail?id=127
	// Also see IdentityHistoryUnitTest when removing the @Ignore (search for the link above).
	@Unindex
	private Instance instance;

	@Nullable
	@XmlAttribute
	private String version;

	/**
	 * Used to represent some data item for the {@link #application}. This is often null.
	 * 
	 * For example, this would be the Garmin Connect Activity ID if the application was
	 * {@link KnownApplication#GARMIN_CONNECT}.
	 * 
	 */
	@Nullable
	@XmlAttribute
	private String externalId;

	@Nullable
	@XmlJavaTypeAdapter(DateTimeXmlAdapter.class)
	private DateTime timestamp;

	/** Use {@link Builder} to construct Installation */
	private Installation() {
	}

	/**
	 * @return the application
	 */
	public Application getApplication() {
		return this.instance.getApplication();
	}

	/**
	 * @return the externalId
	 */
	public String getExternalId() {
		return this.externalId;
	}

	/**
	 * @return the instance
	 */
	public Instance getInstance() {
		return this.instance;
	}

	/**
	 * @return the version
	 */
	@Nullable
	public String getVersion() {
		return this.version;
	}

	/**
	 * @return the timestamp
	 */
	public DateTime getTimestamp() {
		return this.timestamp;
	}

	@Override
	public String toString() {
		return "Application: " + String.valueOf(this.instance.getApplication()) + "\n Instance: "
				+ String.valueOf(this.instance.getInstanceKey()) + "\n Version: " + version + "\n" + "ID: "
				+ externalId;
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.externalId == null) ? 0 : this.externalId.hashCode());
		result = prime * result + ((this.instance == null) ? 0 : this.instance.hashCode());
		result = prime * result + ((this.version == null) ? 0 : this.version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Installation)) {
			return false;
		}
		Installation other = (Installation) obj;
		if (this.externalId == null) {
			if (other.externalId != null) {
				return false;
			}
		} else if (!this.externalId.equals(other.externalId)) {
			return false;
		}
		if (this.instance == null) {
			if (other.instance != null) {
				return false;
			}
		} else if (!this.instance.equals(other.instance)) {
			return false;
		}
		if (this.version == null) {
			if (other.version != null) {
				return false;
			}
		} else if (!this.version.equals(other.version)) {
			return false;
		}
		return true;
	}

	/**
	 * Used to construct all instances of Installation.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<Installation> {

		public Builder() {
			super(new Installation());
		}

		Builder(Installation mutatee) {
			super(mutatee);
		}

		public Builder setInstance(Instance instance) {
			building.instance = instance;
			return this;
		}

		public Builder setVersion(String version) {
			building.version = version;
			return this;
		}

		public Builder setExternalId(String externalId) {
			building.externalId = externalId;
			return this;
		}

		public Builder setTimestamp(DateTime timestamp) {
			building.timestamp = timestamp;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			// It seems many installations do not have an instance.
			// Assertion.assertNotNull(building.instance);
			super.validate();
		}
	}// end Builder

}
