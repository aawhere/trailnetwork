/**
 * 
 */
package com.aawhere.app;

import static com.aawhere.app.DeviceCapabilities.*;
import static com.aawhere.app.DeviceCapabilities.SignalQuality.*;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.aawhere.app.DeviceCapabilities.Builder;
import com.aawhere.lang.Assertion;
import com.aawhere.log.LoggerFactory;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Singleton;

/**
 * Where you retrieve {@link DeviceCapabilities} using an {@link InstanceKey} or {@link Device}.
 * 
 * Currently a disconnected, in-memory map.
 * 
 * In General, quality is favored towards newer devices with barometric altimeters. Phones are
 * discouraged.Bike devices over wrist-worn mostly due to location on the body assumed to be on the
 * wrist and swinging.
 * 
 * @author aroller
 * 
 */
@Singleton
public class DeviceCapabilitiesRepository {

	private static final Logger LOG = LoggerFactory.getLogger(DeviceCapabilitiesRepository.class);
	private Map<ApplicationKey, DeviceCapabilities> capabilities;
	final private Function<ApplicationKey, DeviceCapabilities> function;
	public static final DeviceCapabilities UNKNOWN = DeviceCapabilities.create().key(KnownApplication.UNKNOWN.key)
			.build();

	/**
			 * 
			 */
	public DeviceCapabilitiesRepository() {
		this.capabilities = new HashMap<>();
		// Garmin Edge
		put("edge1000", create().baro(GREAT).gps(GREAT));
		put(FIELD.EDGE_810, create().baro(GREAT).gps(GREAT));
		put(FIELD.EDGE_800, create().baro(GOOD).gps(GREAT));
		put(FIELD.EDGE_705, create().baro(GOOD).gps(GOOD));
		put(FIELD.EDGE_605, create().baro(NONE).gps(GOOD).ele(FAIR));
		put(FIELD.EDGE_510, create().baro(GREAT).gps(GREAT));
		put(FIELD.EDGE_500, create().baro(GOOD).gps(GOOD));
		put(FIELD.EDGE_305, create().baro(GOOD).gps(FAIR));
		put(FIELD.EDGE_205, create().baro(NONE).gps(FAIR).ele(FAIR));
		put(FIELD.EDGE_200, create().baro(NONE).gps(GOOD).ele(FAIR));
		// touring key?
		// https://buy.garmin.com/en-US/US/into-sports/cycling/edge-touring-plus/prod143677.html

		// GARMIN Forerunner
		// arm-based has challenges compared to the more stable handlebar

		// The 920 has a barometer, but according to DC Rainmaker, it isn't
		// implemented well, resulting in inaccurate elevation gains for the first
		// 30 minutes or so of an activity.
		// http://www.dcrainmaker.com/2014/11/garmin-forerunner-920xt-depth-review.html
		put(FIELD.FORERUNNER_920, create().baro(FAIR).gps(GOOD).ele(GOOD));
		put(FIELD.FORERUNNER_910, create().baro(GOOD).gps(GOOD).ele(GOOD));
		put(FIELD.FORERUNNER_620, create().baro(NONE).gps(GOOD).ele(GOOD));
		put(FIELD.FORERUNNER_610, create().baro(NONE).gps(GOOD).ele(GOOD));
		put(FIELD.FORERUNNER_410, create().baro(NONE).gps(GOOD).ele(FAIR));
		put(FIELD.FORERUNNER_405cx, create().baro(NONE).gps(FAIR).ele(POOR));
		put(FIELD.FORERUNNER_405, create().baro(NONE).gps(FAIR).ele(POOR));
		put(FIELD.FORERUNNER_405beta, create().baro(NONE).gps(POOR).ele(POOR));
		put(FIELD.FORERUNNER_310, create().baro(NONE).gps(GOOD).ele(FAIR));
		put(FIELD.FORERUNNER_310xt, create().baro(NONE).gps(GOOD).ele(FAIR));
		put(FIELD.FORERUNNER_305, create().baro(NONE).gps(FAIR).ele(FAIR));
		// space observed in testing
		alias(FIELD.FORERUNNER_305, "forerunner 305");
		put(FIELD.FORERUNNER_301, create().baro(NONE).gps(POOR).ele(POOR));
		// space observed in testing
		alias(FIELD.FORERUNNER_301, "forerunner 301");
		put(FIELD.FORERUNNER_220, create().baro(NONE).gps(FAIR).ele(FAIR));
		put(FIELD.FORERUNNER_210, create().baro(NONE).gps(FAIR).ele(FAIR));
		put(FIELD.FORERUNNER_205, create().baro(NONE).gps(FAIR).ele(FAIR));
		put(FIELD.FORERUNNER_201, create().baro(NONE).gps(POOR).ele(POOR));
		put(FIELD.FORERUNNER_112, create().baro(NONE).gps(FAIR).ele(FAIR));
		put(FIELD.FORERUNNER_110, create().baro(NONE).gps(FAIR).ele(FAIR));
		put(FIELD.FORERUNNER_101, create().baro(NONE).gps(POOR).ele(POOR));
		put("forerunner15", create().baro(NONE).gps(FAIR).ele(POOR));
		put("forerunner10", create().baro(NONE).gps(FAIR).ele(POOR));
		put("garminfitnessdevice", create().baro(NONE).gps(POOR).ele(POOR));
		put("unknown", create().baro(NONE).gps(POOR).ele(POOR));
		put("user", create().baro(NONE).gps(POOR).ele(POOR));

		// GARMIN Outdoor
		put(FIELD.FENIX_2, create().baro(GOOD).gps(GOOD).ele(GOOD));
		put("etrexvistacx", create().baro(GOOD).gps(GOOD).ele(GOOD));
		put("fenix", create().baro(GOOD).gps(GOOD).ele(GOOD));
		put("foretrex", create().baro(NONE).gps(POOR).ele(POOR));

		// Garmin Phones
		put("garminFitIphone", create().baro(NONE).gps(POOR).ele(POOR));
		put("garminFitAndroid", create().baro(NONE).gps(POOR).ele(POOR));

		// TN-923 MMF keys for Garmin Devices
		alias(FIELD.FENIX_2, FIELD.FENIX_2_ALIAS);
		alias(FIELD.FORERUNNER_112, "Forerunner 112");
		alias(FIELD.FORERUNNER_110, "Forerunner 110");
		alias(FIELD.FORERUNNER_201, "Forerunner 201");
		alias(FIELD.FORERUNNER_301, "Forerunner 301");
		alias(FIELD.FORERUNNER_305, "Forerunner 305");
		alias(FIELD.FORERUNNER_310xt, "Forerunner 310XT");
		alias(FIELD.FORERUNNER_405, "Forerunner 405");
		alias(FIELD.FORERUNNER_405cx, "Forerunner 405CX");
		alias(FIELD.FORERUNNER_610, "Forerunner 610");
		// TN-923, MMF mapps the 205 to a 305
		// alias(FIELD.FORERUNNER_205, "Forerunner 305");
		alias(FIELD.FORERUNNER_910, "Forerunner 910XT");
		alias(FIELD.FORERUNNER_920, "Forerunner 920XT");
		alias(FIELD.EDGE_205, "EDGE_205");
		alias(FIELD.EDGE_305, "Edge 305");
		alias(FIELD.EDGE_500, "Edge 500");
		alias(FIELD.EDGE_800, "Edge 800");
		alias(FIELD.EDGE_705, "Edge 705");
		alias(FIELD.EDGE_605, "Edge 605");
		alias(FIELD.EDGE_200, "Edge 200");

		// TN-923 MMF keys for MMF Phones
		// you could argue that all the mapmy* apps are the same and should be aliases instead of
		// separate
		// devices for each app type.
		put("mapmywalkiphone", create().baro(NONE).gps(POOR).ele(POOR));
		put("android_walk", create().baro(NONE).gps(POOR).ele(POOR));

		put(FIELD.MMF_RUN_ANDROID, create().baro(NONE).gps(POOR).ele(POOR));
		put(FIELD.MMF_RUN_IPHONE, create().baro(NONE).gps(POOR).ele(POOR));
		alias(FIELD.MMF_RUN_IPHONE, "mapmyrun+iphone");
		alias(FIELD.MMF_RUN_ANDROID, "mapmyrunandroid3.x");

		put("mapmyfitnessiphone", create().baro(NONE).gps(POOR).ele(POOR));
		// Not sure what this is. It could be the record app or an alias for mmf android?
		put("android_fitness", create().baro(NONE).gps(POOR).ele(POOR));

		put(FIELD.MMF_RIDE_IPHONE, create().baro(NONE).gps(POOR).ele(POOR));
		alias(FIELD.MMF_RIDE_IPHONE, "mapmyride+iphone");
		put(FIELD.MMF_RIDE_ANDROID, create().baro(NONE).gps(POOR).ele(POOR));
		alias(FIELD.MMF_RIDE_ANDROID, "mapmyride+android");

		put(FIELD.MMF_HIKE_IPHONE, create().baro(NONE).gps(POOR).ele(POOR));

		this.capabilities = ImmutableMap.copyOf(this.capabilities);
		// the function accesses the capabilities providing a default value with the key if it
		// doesn't exist.
		// Functions#forMap accepts a default, but not with the key that's given.
		this.function = new Function<ApplicationKey, DeviceCapabilities>() {

			@Override
			public DeviceCapabilities apply(ApplicationKey input) {
				return capabilities(input);
			}
		};

	}

	/**
	 * @param key
	 * @param builder
	 */
	private void put(String key, Builder builder) {
		final ApplicationKey deviceKey = new ApplicationKey(key);
		this.capabilities.put(deviceKey, builder.key(deviceKey).build());
	}

	/**
	 * Maps another key to an existing capabilities.
	 * 
	 * @param key
	 * @param aliasKey
	 */
	private void alias(String key, String aliasKey) {
		DeviceCapabilities capabilities = this.capabilities(key);
		Assertion.exceptions().notNull("alias capabilities", capabilities);
		// lookups use cleansed key so the alias should define similarly
		ApplicationKey aliasApplicationKey = DeviceUtil.keyCleansed(new ApplicationKey(aliasKey));
		this.capabilities.put(aliasApplicationKey, capabilities);
	}

	public DeviceCapabilities capabilities(String key) {
		return capabilities(new ApplicationKey(key));
	}

	/**
	 * Use with descretion since this may have to go away if we have too many.
	 * 
	 * @return the capapbilities
	 */
	Map<ApplicationKey, DeviceCapabilities> capabilities() {
		return this.capabilities;
	}

	/**
	 * @param applicationKey
	 * @return the capabilities matching the key or null if not known.
	 */
	public DeviceCapabilities capabilities(ApplicationKey applicationKey) {
		if (applicationKey == null) {
			return null;
		}
		applicationKey = DeviceUtil.keyCleansed(applicationKey);
		DeviceCapabilities result = capabilities.get(applicationKey);
		if (result == null) {
			if (LOG.isLoggable(Level.INFO)) {
				LOG.info(applicationKey + " device not found in capabilities");
			}
			result = DeviceCapabilities.create().key(applicationKey).build();
		}
		return result;
	}

	public Function<ApplicationKey, DeviceCapabilities> capabilitiesFunction() {
		return function;
	}

	public static class FIELD {
		public static final String FORERUNNER_920 = "forerunner920";
		public static final String FORERUNNER_910 = "forerunner910";
		public static final String FORERUNNER_620 = "forerunner620";
		public static final String FORERUNNER_610 = "forerunner610";
		public static final String FORERUNNER_410 = "forerunner410";
		public static final String FORERUNNER_405cx = "forerunner405cx";
		public static final String FORERUNNER_405 = "forerunner405";
		public static final String FORERUNNER_405beta = "forerunner405beta";
		public static final String FORERUNNER_310 = "forerunner310";
		public static final String FORERUNNER_310xt = "forerunner310xt";
		public static final String FORERUNNER_305 = "forerunner305";
		public static final String FORERUNNER_301 = "forerunner301";
		public static final String FORERUNNER_220 = "forerunner220";
		public static final String FORERUNNER_210 = "forerunner210";
		public static final String FORERUNNER_205 = "forerunner205";
		public static final String FORERUNNER_201 = "forerunner201";
		public static final String FORERUNNER_112 = "forerunner112";
		public static final String FORERUNNER_110 = "forerunner110";
		public static final String FORERUNNER_101 = "forerunner101";
		public static final String EDGE_200 = "edge200";
		public static final String EDGE_205 = "edge205";
		public static final String EDGE_305 = "edge305";
		public static final String EDGE_510 = "edge510";
		public static final String EDGE_605 = "edge605";
		public static final String EDGE_705 = "edge705";
		public static final String EDGE_810 = "edge810";
		public static final String EDGE_800 = "edge800";
		public static final String EDGE_500 = "edge500";
		public static final String FENIX_2 = "garminfenix2";
		// defined here to allow for testing aliases
		public static final String FENIX_2_ALIAS = "Fenix 2";
		public static final String MMF_RIDE_IPHONE = "mapmyrideiphone";
		public static final String MMF_RIDE_ANDROID = "mapmyrideandroidapp";
		public static final String MMF_RUN_IPHONE = "mapmyruniphone";
		public static final String MMF_RUN_ANDROID = "mapmyrunandroidapp";
		public static final String MMF_HIKE_IPHONE = "mapmyhikeiphone";
	}
}
