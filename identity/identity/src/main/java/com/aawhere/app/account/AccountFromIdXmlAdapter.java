/**
 * 
 */
package com.aawhere.app.account;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Transforms an id into the Account the id represents. This must do so with the bound
 * {@link AccountProvider} which is typically a service.
 * 
 * @author aroller
 * 
 */
@Singleton
public class AccountFromIdXmlAdapter
		extends XmlAdapter<Account, AccountId> {

	private AccountProvider provider;

	/**
	 * @param activityTestUtil
	 */
	@Inject
	public AccountFromIdXmlAdapter(AccountProvider provider) {
		this.provider = provider;
	}

	/**
	 * No injection used when nothing is to be provided.
	 */
	public AccountFromIdXmlAdapter() {
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public AccountId unmarshal(Account account) throws Exception {
		if (account == null || provider == null) {
			return null;
		}
		return account.id();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Account marshal(AccountId id) throws Exception {
		if (id == null || provider == null) {
			return null;
		}
		return provider.account(id);
	}

}
