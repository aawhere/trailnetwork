package com.aawhere.app;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlValue;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;

/**
 * A version of an application. Version may be a software version, web-application version, or any
 * other indicator of changes the application ( fixes, minor model updates, etc).
 * 
 * @author Brian Chapman
 * 
 */
public final class Version
		implements Serializable {

	private static final long serialVersionUID = -7419735863387862394L;

	public static class Builder
			extends ObjectBuilder<Version> {

		public Builder() {
			super(new Version());

		}

		public Builder setVersion(String version) {
			building.value = version;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.value);
		}

	}// end Builder

	// Use builder to create object.
	private Version() {
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return value;
	}

	@XmlValue
	private String value;

	public String getVersion() {
		return value;
	}

}
