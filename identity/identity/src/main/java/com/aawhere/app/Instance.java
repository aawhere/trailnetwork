/**
 *
 */
package com.aawhere.app;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.persist.conv.Polymorphic;
import com.aawhere.xml.XmlNamespace;

/**
 * A representation of an application. It identifies an {@link Application} by an
 * {@link InstanceKey}. May represent an account, device, app, etc. For example I have an Edge 800
 * device (Application) with a device Id xyz (InstanceKey). This example provides all the
 * information required to globally identify any account or device in the world.
 * 
 * @author Brian Chapman
 * 
 */
// This class is indexed by default. To override, add @Unindex to the property specifying this. See
// Activity and Person for examples.
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Polymorphic
@XmlSeeAlso({ AccountInstance.class, Device.class })
@Dictionary(domain = Instance.FIELD.DOMAIN)
public class Instance
		implements Serializable {

	private static final long serialVersionUID = 3693916702791753186L;

	@XmlElement
	@Field(key = FIELD.APPLICATION)
	private Application application;

	@XmlElement
	@Field(key = FIELD.INSTANCE_KEY)
	private InstanceKey instanceKey;

	public Application getApplication() {
		return application;
	}

	public InstanceKey getInstanceKey() {
		return this.instanceKey;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.application == null) ? 0 : this.application.hashCode());
		result = prime * result + ((this.instanceKey == null) ? 0 : this.instanceKey.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Instance)) {
			return false;
		}
		Instance other = (Instance) obj;
		if (this.application == null) {
			if (other.application != null) {
				return false;
			}
		} else if (!this.application.equals(other.application)) {
			return false;
		}
		if (this.instanceKey == null) {
			if (other.instanceKey != null) {
				return false;
			}
		} else if (!this.instanceKey.equals(other.instanceKey)) {
			return false;
		}
		return true;
	}

	/**
	 * Used to construct all instances of User.
	 */
	@XmlTransient
	public static class Builder
			extends BaseBuilder<Builder, Instance> {

		public Builder() {
			super(new Instance());
		}

	}// end Builder

	/**
	 * Used to construct all instances of User.
	 */
	public static class BaseBuilder<BuilderT extends BaseBuilder<BuilderT, InstanceT>, InstanceT extends Instance>
			extends AbstractObjectBuilder<InstanceT, BuilderT> {

		public BaseBuilder(InstanceT instance) {
			super(instance);
		}

		public BuilderT setApplication(Application application) {
			((Instance) building).application = application;
			return dis;
		}

		public BuilderT setKey(InstanceKey instance) {
			((Instance) building).instanceKey = instance;
			return dis;
		}

	}// end Builder

	/** Use {@link Builder} to construct User */
	protected Instance() {
	}

	/** A convince method for constructing a builder */
	public static Builder createInstance() {
		return new Builder();
	}

	@XmlTransient
	public static final class FIELD {
		public static final String INSTANCE_KEY = "instanceKey";
		public static final String APPLICATION = "application";
		public static final String DOMAIN = "instance";

		public static final class KEY {
			public static final FieldKey APPLICATION = new FieldKey(DOMAIN, FIELD.APPLICATION);
			public static final FieldKey APPLICATION_KEY = new FieldKey(APPLICATION,
					Application.FIELD.KEY.APPLICATION_KEY);
			public static final FieldKey INSTANCE_KEY = new FieldKey(DOMAIN, FIELD.INSTANCE_KEY);
		}
	}
}
