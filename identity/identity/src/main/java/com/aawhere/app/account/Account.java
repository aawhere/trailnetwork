/**
 *
 */
package com.aawhere.app.account;

import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.app.Application;
import com.aawhere.app.ApplicationKey;
import com.aawhere.app.ApplicationKeyProvider;
import com.aawhere.app.ApplicationWebApiUri;
import com.aawhere.app.account.AccountProvider.ProfilePage;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldValueProvider;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.identity.AccessControlled;
import com.aawhere.identity.AccessScope;
import com.aawhere.identity.IdentityManager;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.xml.CountFromIntegerXmlAdapter;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.persist.StringBaseEntity;
import com.aawhere.person.Person;
import com.aawhere.person.PersonAccountProvider;
import com.aawhere.personalize.QuantityPersonalized;
import com.aawhere.xml.UrlDisplay;
import com.aawhere.xml.XmlNamespace;
import com.aawhere.xml.bind.OptionalFunctionXmlAdapter;
import com.google.common.base.Functions;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfNull;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * A {@link Person} registered at an {@link Application}.
 * 
 * @author aroller
 * 
 */
@Cache
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE, name = AccountField.DOMAIN)
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Dictionary(domain = Account.FIELD.DOMAIN, messages = AccountMessage.class)
@ApiModel(description = "A Person's User Account at an Application")
@AccessControlled(scope = AccessScope.UNRESTRICTED)
public class Account
		extends StringBaseEntity<Account, AccountId>
		implements SelfIdentifyingEntity, Cloneable, ApplicationKeyProvider {

	private static final long serialVersionUID = 6069106437313304441L;

	@Field(parameterType = String.class, indexedFor = "TN-545")
	@Index
	@XmlElement(name = AccountField.ALIASES)
	@AccessControlled(scope = AccessScope.PUBLIC_READ)
	@QueryParam(FIELD.ALIASES)
	@ApiModelProperty(
			value = "The username at the Application",
			required = false,
			notes = "Allows multiple because a specific account may have changed usernames. This keeps a history since there is no requirement of uniqueness.")
	private Set<String> aliases = new HashSet<String>();

	/** The name of the person as provided by the remote {@link Application}. */
	@IgnoreSave(IfNull.class)
	@QueryParam(AccountField.NAME)
	@Field
	@ApiModelProperty(value = "The name of the person as provided by the Application.", required = false)
	@XmlElement
	@AccessControlled(scope = AccessScope.PUBLIC_READ)
	private String name;

	@IgnoreSave(IfNull.class)
	@QueryParam(AccountField.PHOTO_URL)
	@XmlElement
	@Field
	@ApiModelProperty(value = "A URL providing a photo of the person associated with this account")
	@AccessControlled(scope = AccessScope.PUBLIC_READ)
	private URL photoUrl;

	@ApiModelProperty(value = "The email address associated with the account (shown only to owner).")
	@QueryParam(AccountField.EMAIL)
	@XmlElement(name = AccountField.EMAIL)
	@Field
	@AccessControlled(scope = AccessScope.OWNER_READ)
	private EmailAddress email;

	/**
	 * Used to construct all instances of Account.
	 */
	@XmlTransient
	public static class Builder
			extends StringBaseEntity.Builder<Account, Builder, AccountId> {
		private String remoteId;
		private ApplicationKey application;

		public Builder() {
			super(new Account());
		}

		Builder(Account mutant) {
			super(mutant);
			// make mutable
			mutant.aliases = new HashSet<String>(mutant.getAliases());
		}

		public Builder setName(String name) {
			building.name = name;
			return this;
		}

		public Builder setRemoteId(String remoteId) {
			this.remoteId = remoteId;
			return this;
		}

		public Builder setApplicationKey(ApplicationKey applicationKey) {
			this.application = applicationKey;
			return this;
		}

		public Builder addAlias(String alias) {
			building.aliases.add(alias);
			return this;
		}

		/** @see #email */
		public Builder email(EmailAddress email) {
			building.email = email;
			return this;
		}

		/**
		 * Unlike {@link #addAliases(Collection)}, this assigns the given and will replace any
		 * aliases already in the system.
		 * 
		 * @param aliases
		 * @return
		 */
		Builder setAliases(Set<String> aliases) {
			building.aliases = aliases;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			if (!hasId()) {
				Assertion.exceptions().notNull("remoteId", this.remoteId);
				Assertion.exceptions().notNull("applicationKey", this.application);
			} else if (this.application != null || this.remoteId != null) {
				Assertion.exceptions().eq(id().getApplicationKey(), this.application);
				Assertion.exceptions().eq(id().getRemoteId(), this.remoteId);

			}
		}

		@Override
		public Account build() {
			if (!hasId()) {
				setId(new AccountId(this.application, this.remoteId));
			}
			Account built = super.build();
			built.aliases = AccountUtil.cleanupAliases(built.aliases);
			return built;
		}

		/**
		 * @param union
		 */
		public Builder addAliases(Collection<String> aliases) {
			building.aliases.addAll(aliases);
			return this;
		}

		/**
		 * @param photoUrl
		 * @return
		 */
		public Builder photoUrl(URL photoUrl) {
			building.photoUrl = photoUrl;
			return this;
		}

		public Builder id(AccountId id) {
			setApplicationKey(id.getApplicationKey());
			setRemoteId(id.getRemoteId());
			return this;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	static Builder mutate(Account account) {
		return new Builder(account);
	}

	public static Builder clone(Account account) {
		try {
			return new Builder((Account) account.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	/** Use {@link Builder} to construct Account */
	private Account() {
	}

	/**
	 * @return the remoteId
	 */
	@XmlElement
	public String getRemoteId() {
		return getId().getRemoteId();
	}

	public String name() {
		return this.name;
	}

	/** @see #email */
	public EmailAddress email() {
		return this.email;
	}

	public URL photoUrl() {
		return photoUrl;
	}

	@ApiModelProperty(value = "Identifier representing this Account at the Application", required = true)
	public String remoteId() {
		return getRemoteId();
	}

	/**
	 * @return the aliases
	 */
	public Set<String> getAliases() {
		return this.aliases;
	}

	/**
	 * Provides the main alias,if any. If none are found then this will provide the
	 * {@link #remoteId()}. Use {@link #hasAlias()} if interested in knowing if aliases are
	 * available.
	 * 
	 * TODO: alias should map to aliases column name allowing queries against aliases.
	 * 
	 * @return
	 */
	@Nonnull
	@Field
	public String alias() {
		return Iterables.getFirst(this.aliases, getRemoteId());
	}

	public Boolean hasAlias() {
		return this.aliases != null && !this.aliases.isEmpty();
	}

	@XmlJavaTypeAdapter(ApplicationReferenceXmlAdapter.class)
	@XmlElement(name = FIELD.APPLICATION_REFERENCE)
	@Field(key = FIELD.APPLICATION_REFERENCE, xmlAdapter = ApplicationReferenceXmlAdapter.class)
	private Account getApplicationReference() {
		return this;
	}

	public Set<String> aliases() {
		return this.aliases;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@Override
	@Field(key = FIELD.ID, indexedFor = "Repository.load()")
	@ApiModelProperty(value = "The globally unique identifier combining the {Application}-{RemoteId}.", required = true)
	public
			AccountId getId() {
		return super.getId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.ApplicationKeyProvider#applicationKey()
	 */
	@Override
	@Field(key = FIELD.APPLICATION_KEY)
	public ApplicationKey applicationKey() {
		return getId().getApplicationKey();
	}

	@XmlJavaTypeAdapter(AccountApplicationXmlAdapter.class)
	@XmlElement(name = Application.FIELD.DOMAIN)
	public ApplicationKey getApplicationKey() {
		return applicationKey();
	}

	@Field(xmlAdapter = Account.ActivityCountXmlAdapter.class)
	@ApiModelProperty("The number of Activiites imported for this account.")
	private Integer activityCount() {
		throw new UnsupportedOperationException("just for field def.");
	}

	@XmlElement(name = AccountField.ACTIVITY_COUNT)
	@XmlJavaTypeAdapter(Account.ActivityCountXmlAdapter.class)
	private AccountId getActivityCountXml() {
		return this.id();
	}

	@XmlRootElement(name = FIELD.FIELDS_NAME)
	@XmlType(namespace = XmlNamespace.API_VALUE, name = FIELD.FIELDS_NAME)
	@XmlTransient
	public static final class FIELD
			extends BaseEntity.BASE_FIELD {
		static final String FIELDS_NAME = "accountFields";
		@XmlAttribute
		public final static String DOMAIN = "account";
		@XmlAttribute
		public final static String REMOTE_ID = "remoteId";
		@XmlAttribute
		public final static String APPLICATION_KEY = Application.FIELD.DOMAIN;
		@XmlAttribute
		public final static String ALIASES = "aliases";
		public static final String ACCOUNTS = DOMAIN + "s";
		public static final String ACCOUNT_ID = AccountWebApiUri.ACCOUNT_ID;
		public static final String ACCOUNT_IDS = ACCOUNT_ID + "s";
		public static final String APPLICATION_REFERENCE = ApplicationWebApiUri.APPLICATION_REFERENCE;
		public static final String NAME = "name";

		public static final class KEY {
			public final static FieldKey ACCOUNT_ID = new FieldKey(DOMAIN, FIELD.ID);
			public final static FieldKey APPLICATION_KEY = new FieldKey(DOMAIN, FIELD.APPLICATION_KEY);
			public final static FieldKey ALIASES = new FieldKey(DOMAIN, FIELD.ALIASES);
		}

		public static final class OPTIONS {

			public static final String APPLICATION = new FieldKey(FIELD.DOMAIN, Application.FIELD.DOMAIN)
					.getAbsoluteKey();

		}
	}

	/**
	 * @return
	 */
	public Boolean hasName() {
		return this.name != null;
	}

	public Boolean hasPhotoUrl() {
		return this.photoUrl != null;
	}

	public Boolean hasEmail() {
		return this.email != null;
	}

	@Singleton
	public static class ActivityCountXmlAdapter
			extends OptionalFunctionXmlAdapter<QuantityPersonalized, AccountId> {
		/** The default constructor when not in a container */
		public ActivityCountXmlAdapter() {
		}

		@Inject
		public ActivityCountXmlAdapter(FieldValueProvider fieldValueProvider, IdentityManager identityManager) {
			// see Application.AccountCountXmlAdapter
			super(Functions.compose(new CountFromIntegerXmlAdapter(identityManager).function(),
									AccountUtil.activityCountProviderFunction(fieldValueProvider)), Visibility.SHOW);
		}
	}

	/**
	 * Provides the url reference to the account in the remote application.
	 * 
	 * @author aroller
	 * 
	 */
	static class ApplicationReferenceXmlAdapter
			extends AccountAccessScopedXmlAdapter<UrlDisplay> {

		public ApplicationReferenceXmlAdapter() {
			super();
		}

		@Inject
		public ApplicationReferenceXmlAdapter(IdentityManager identityManager, PersonAccountProvider provider,
				ProfilePage profilePageProvider) {
			super(identityManager, provider, AccessScope.PUBLIC_READ, Functions
					.compose(UrlDisplay.urlFunction(), AccountUtil.accountProfilePageUrlFunction(profilePageProvider)));

		}

	}

}
