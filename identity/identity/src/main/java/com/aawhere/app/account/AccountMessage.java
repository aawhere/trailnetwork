/**
 * 
 */
package com.aawhere.app.account;

import com.aawhere.app.ApplicationMessage;
import com.aawhere.persist.EntityMessage;
import com.aawhere.swagger.DocSupport;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Message for {@link Account} and friends.
 * 
 * @author aroller
 * 
 */
public enum AccountMessage implements Message {

	/** Explain_Entry_Here */
	USER_ACCOUNT_NAME("Account name"),
	USER_ACCOUNT_DESCRIPTION("The name of the account."),
	REMOTE_ID_NAME("Remote Identifier"),
	REMOTE_ID_DESCRIPTION("Account identifier in remote application"),
	ALIASES_NAME("Aliases"),
	ALIASES_DESCRIPTION("Other names for this account."),
	ID_NAME(EntityMessage.ID_NAME.getValue()),
	ID_DESCRIPTION(EntityMessage.ID_DESCRIPTION.getValue()),
	DATE_CREATED_NAME(EntityMessage.DATE_CREATED_NAME.getValue()),
	DATE_CREATED_DESCRIPTION(EntityMessage.DATE_CREATED_DESCRIPTION.getValue()),
	DATE_UPDATED_NAME(EntityMessage.DATE_UPDATED_NAME.getValue()),
	DATE_UPDATED_DESCRIPTION(EntityMessage.DATE_UPDATED_DESCRIPTION.getValue()),
	APPLICATION_NAME(ApplicationMessage.APPLICATION_NAME.getValue()),
	APPLICATION_DESCRIPTION(ApplicationMessage.APPLICATION_DESCRIPTION.getValue());

	private ParamKey[] parameterKeys;
	private String message;

	private AccountMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** EXPLAIN_PARAM_HERE */
		FIRST_PARAM;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static final class Doc {
		/**
		 * Unfortunately needed by data type since it only accepts a string.
		 * 
		 * @see ApiModelProperty#dataType()
		 */
		public static final String DATA_TYPE = "Account";
		public static final String EXPAND_ALLOWABLE = AccountField.Absolute.ACTIVITY_COUNT + DocSupport.Allowable.NEXT
				+ AccountField.Absolute.ALIASES;
	}

}
