/**
 * 
 */
package com.aawhere.app;

import com.aawhere.swagger.DocSupport;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Localizable messages for the app package.
 * 
 * @author Aaron Roller
 * 
 */
public enum ApplicationMessage implements Message {

	/** @see UnknownApplicationException */
	UNKNOWN_APPLICATION(
			"The application with a key of \"{APPLICATION_KEY}\" is unknown.  Choose from: {VALID_APPLICATIONS}.",
			Param.APPLICATION_KEY,
			Param.VALID_APPLICATIONS),
	/* */
	APPLICATION_NAME("Application name"),
	/* */
	APPLICATION_DESCRIPTION(
			"A unique business key that identifies an application with a readable, dash-separated-naming-standard."), /* */
	KEY_NAME("Key"), /* */
	KEY_DESCRIPTION("A unique identifier that is used to identifier this application");

	private ParamKey[] parameterKeys;
	private String message;

	private ApplicationMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** @see ApplicationKey */
		APPLICATION_KEY, /**
		 * An array of enumerations that represent the valid choices.
		 */
		VALID_APPLICATIONS;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static final class Doc {
		public static final String ALLOWABLE = ApplicationField.Absolute.ACCOUNT_COUNT + DocSupport.Allowable.NEXT
				+ ApplicationField.Absolute.ACTIVITY_COUNT;
	}

}
