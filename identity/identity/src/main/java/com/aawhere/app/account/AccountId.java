/**
 * 
 */
package com.aawhere.app.account;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.app.ApplicationDataId;
import com.aawhere.app.ApplicationKey;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.ServicedBy;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
@ServicedBy(AccountProvider.class)
public class AccountId
		extends com.aawhere.id.StringIdentifier<Account>
		implements ApplicationDataId {

	/**
	 * @param applicationKey
	 * @param remoteId
	 */
	public AccountId(ApplicationKey applicationKey, String remoteId) {
		this(IdentifierUtils.createCompositeIdValue(applicationKey, remoteId));
	}

	/**
	 * 
	 */
	AccountId() {
		super(Account.class);
	}

	public AccountId(String value) {
		super(value, Account.class);
	}

	private static final long serialVersionUID = 1L;

	@XmlAttribute(name = "app")
	public String getApp() {
		return getApplicationKey().getValue();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.ApplicationDataId#getApplicationKey()
	 */
	@Override
	public ApplicationKey getApplicationKey() {
		return new ApplicationKey(IdentifierUtils.splitCompositeId(this).getLeft());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.ApplicationDataId#getRemoteId()
	 */
	@Override
	@XmlAttribute(name = "appId")
	public String getRemoteId() {
		return IdentifierUtils.splitCompositeId(this).getRight();
	}

}
