/**
 * 
 */
package com.aawhere.app.account;

import java.util.Set;

import com.aawhere.app.account.Account.Builder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.RepositoryDelegate;

/**
 * @author aroller
 * 
 */
class AccountRepositoryDelegate
		extends RepositoryDelegate<AccountRepository, Accounts, Account, AccountId>
		implements AccountRepository {

	/**
	 * @param repository
	 */
	protected AccountRepositoryDelegate(AccountRepository repository) {
		super(repository);
	}

	Builder mutate(AccountId accountId) throws EntityNotFoundException {
		return Account.mutate(load(accountId));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.account.AccountRepository#setAliases(com.aawhere.app.account.AccountId,
	 * java.util.Set)
	 */
	@Override
	public Account aliasesAdded(AccountId id, Set<String> aliases) throws EntityNotFoundException {
		return update(mutate(id).addAliases(AccountUtil.cleanupAliases(aliases)));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.account.AccountRepository#setName(com.aawhere.app.account.AccountId,
	 * java.lang.String)
	 */
	@Override
	public Account setName(AccountId id, String name) throws EntityNotFoundException {
		return update(mutate(id).setName(name));
	}

}
