/**
 * 
 */
package com.aawhere.app.account;

import java.util.Set;

import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Repository;

/**
 * {@link Repository} for {@link Account}.
 * 
 * @author aroller
 * 
 */
public interface AccountRepository
		extends CompleteRepository<Accounts, Account, AccountId> {

	/**
	 * replaces the aliases with the given.
	 * 
	 * @throws EntityNotFoundException
	 */
	public Account aliasesAdded(AccountId id, Set<String> aliases) throws EntityNotFoundException;

	public Account setName(AccountId id, String name) throws EntityNotFoundException;
}
