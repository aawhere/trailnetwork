/**
 *
 */
package com.aawhere.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.util.CollectionUtilsExtended;
import com.aawhere.xml.XmlNamespace;
import com.google.common.collect.Collections2;
import com.googlecode.objectify.annotation.Unindex;

/**
 * Provides creation and modification details about a data item's past interaction with
 * {@link Application}s.
 * 
 * @author roller
 * 
 */
@Unindex
@XmlRootElement
@XmlType(name = "history", namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@Dictionary(domain = IdentityHistory.FIELD.DOMAIN)
public class IdentityHistory
		implements Iterable<Installation>, Serializable {

	private static final long serialVersionUID = 7206062523675555450L;
	/**
	 * The visits ordered by their {@link ApplicationVisit#order}
	 * 
	 */
	@Unindex
	@XmlElement(name = FIELD.INSTALLATIONS)
	@Field(key = FIELD.INSTALLATIONS, parameterType = Installation.class)
	private List<Installation> installations = new ArrayList<Installation>();

	/** A convenience method for constructing a builder */
	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct DocumentHistory */
	private IdentityHistory() {
	}

	/**
	 * @return the installations
	 */
	public List<Installation> getInstallations() {
		return this.installations;
	}

	/**
	 * Each activity has a single device. Use this to search the installations for the device if it
	 * exists.
	 * 
	 * @return
	 */
	public Device getDevice() {
		Device result = null;
		for (Installation installation : this.installations) {
			Instance instance = installation.getInstance();
			if (instance instanceof Device) {
				result = (Device) instance;
			}
		}
		return result;
	}

	/**
	 * Searches the {@link #installations} for an {@link Installation} matching the externalId and
	 * applicationKey given.
	 * 
	 * @param applicationKey
	 * @param externalId
	 * @return the installation matching the given parameters or null if not found.
	 */
	@Nullable
	public Installation getByExternalId(ApplicationKey applicationKey, String externalId) {
		InstallationExternalIdPredicate predicate = new InstallationExternalIdPredicate.Builder()
				.setApplicationKey(applicationKey).setExternalId(externalId).build();
		return CollectionUtilsExtended.firstOrNull(Collections2.filter(this.installations, predicate));
	}

	/**
	 * Searches the {@link #installations} for an {@link Installation} matching the
	 * {@link ApplicationKey} given.
	 * 
	 * @param applicationKey
	 * @return
	 */
	@Nullable
	public Collection<Installation> getByApplication(ApplicationKey applicationKey) {
		InstallationApplicationPredicate predicate = new InstallationApplicationPredicate.Builder()
				.setApplicationKey(applicationKey).build();
		return Collections2.filter(this.installations, predicate);
	}

	/**
	 * Used to construct all instances of DocumentHistory.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<IdentityHistory> {

		public Builder() {
			super(new IdentityHistory());
		}

		/**
		 * @param entity
		 */
		public Builder(IdentityHistory mutatee) {
			super(mutatee);
			mutatee.installations = new ArrayList<Installation>(mutatee.installations);
		}

		public Builder add(Installation installation) {
			building.installations.add(installation);
			return this;
		}

		/**
		 * @param visits
		 * @return
		 */
		public Builder addAll(Collection<Installation> installations) {
			building.installations.addAll(installations);
			return this;

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public IdentityHistory build() {
			building.installations = Collections.unmodifiableList(building.installations);
			return super.build();
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotEmpty(building.installations);
		}

	}// end Builder

	/*
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Installation> iterator() {

		return installations.iterator();
	}

	@XmlTransient
	public static final class FIELD {
		public static final String INSTALLATIONS = "installations";
		public static final String DOMAIN = "identityHistory";
	}
}
