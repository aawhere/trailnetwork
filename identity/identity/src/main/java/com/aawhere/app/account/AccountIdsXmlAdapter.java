/**
 * 
 */
package com.aawhere.app.account;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.identity.IdentityManager;
import com.google.common.base.Function;
import com.google.inject.Inject;

/**
 * Adapts AccountsIds to their full Account for inline display.
 * 
 * @author aroller
 * 
 */
// request scoped
public class AccountIdsXmlAdapter
		extends XmlAdapter<Accounts, Iterable<AccountId>> {

	final private AccountProvider provider;
	private final IdentityManager identityManager;

	/**
	 * 
	 */
	public AccountIdsXmlAdapter() {
		this.provider = null;
		this.identityManager = null;
	}

	public AccountIdsXmlAdapter(final Function<AccountId, Account> provider, IdentityManager identityManager) {
		this.provider = AccountUtil.provider(provider);
		this.identityManager = identityManager;
	}

	@Inject
	AccountIdsXmlAdapter(AccountProvider provider, IdentityManager identityManager) {
		this.provider = provider;
		this.identityManager = identityManager;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Iterable<AccountId> unmarshal(Accounts accounts) throws Exception {
		return IdentifierUtils.ids(accounts);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Accounts marshal(Iterable<AccountId> ids) throws Exception {
		if (provider == null || ids == null) {
			return null;
		}
		Accounts accounts = this.provider.accounts(ids);
		// mutation occurs directly with the entity..no need for the resulting iterator
		identityManager.enforceAccessNow(accounts);
		return accounts;
	}
}
