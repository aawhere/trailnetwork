/**
 *
 */
package com.aawhere.app;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.field.FieldValueProvider;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.ComparatorResult;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

/**
 * @author aroller
 * 
 */
public class ApplicationUtil {

	/**
	 * Simple transformer of any iterable containing applications to it's keys.
	 * 
	 * @see IdentifierUtils#keyFunction()
	 * @param applications
	 * @return
	 */
	public static Iterable<ApplicationKey> keys(Iterable<Application> applications) {
		return IdentifierUtils.keys(applications);
	}

	public static Function<ApplicationDataId, String> applicationDataIdValueFunction() {
		return new ApplicationDataIdToStringFunction();
	}

	public static Iterable<String> applicationDataIdValues(Iterable<? extends ApplicationDataId> ids) {
		return Iterables.transform(ids, applicationDataIdValueFunction());
	}

	public static Supplier<ApplicationKey> applicationKeySupplier(final ApplicationKeyProvider provider) {
		return new Supplier<ApplicationKey>() {

			@Override
			public ApplicationKey get() {
				return provider.applicationKey();
			}
		};
	}

	public static Predicate<ApplicationKey> applicationKeyPredicate(ApplicationKey applicationKey) {
		return Predicates.equalTo(applicationKey);
	}

	/**
	 * provides a function that will provide the application key for any Class that implements
	 * {@link ApplicationKeyProvider}.
	 * 
	 * @return
	 */
	public static <P extends ApplicationKeyProvider> Function<P, ApplicationKey> applicationKeyFunction() {
		return new Function<P, ApplicationKey>() {

			@Override
			public ApplicationKey apply(P input) {
				return input.applicationKey();
			}
		};
	}

	/**
	 * Provides the value associated with {@link ApplicationField#ACCOUNT_COUNT}.
	 * 
	 * @return
	 */
	public static Function<ApplicationKey, Integer> accountCountProviderFunction(FieldValueProvider fieldValueProvider) {
		return FieldValueProvider.Util.function(ApplicationField.Key.ACCOUNT_COUNT, fieldValueProvider);
	}

	/**
	 * Indicates if the given Application is known to manage our accounts.
	 * 
	 * @see KnownApplication#GOOGLE_IDENTITY_TOOLKIT
	 * 
	 * @param applicationKey
	 * @return
	 */
	@Nonnull
	public static Boolean managingApplication(@Nullable ApplicationKey applicationKey) {
		return applicationKey != null && applicationKey.equals(KnownApplication.GOOGLE_IDENTITY_TOOLKIT.key);
	}

	private static class ManagingApplicationPreferredOrdering
			extends Ordering<ApplicationKey>
			implements Serializable {

		private static final long serialVersionUID = 9065993486871380957L;
		private static final ManagingApplicationPreferredOrdering INSTANCE = new ManagingApplicationPreferredOrdering();

		@Override
		public String toString() {
			return "ManagingAccountPreferredOrdering";
		}

		@Override
		public int compare(ApplicationKey left, ApplicationKey right) {
			Boolean leftManages = ApplicationUtil.managingApplication(left);
			Boolean rightManages = ApplicationUtil.managingApplication(right);
			return ComparatorResult.binary(leftManages, rightManages);

		}
	}

	/**
	 * Ordering by preferring managing accounts over those that aren't.
	 * 
	 * @return
	 */
	public static Ordering<ApplicationKey> managingApplicationOrdering() {
		return ManagingApplicationPreferredOrdering.INSTANCE;
	}

	private enum ManagingApplicationFunction implements Function<ApplicationKey, Boolean> {
		INSTANCE;
		@Override
		@Nullable
		public Boolean apply(@Nullable ApplicationKey application) {
			return (application != null) ? managingApplication(application) : null;
		}

		@Override
		public String toString() {
			return "ManagingApplicationFunction";
		}
	}

	/**
	 * Indicates if the application is managing.
	 * 
	 * @return
	 */
	public static Function<ApplicationKey, Boolean> managingApplicationFunction() {
		return ManagingApplicationFunction.INSTANCE;
	}

}
