/**
 * 
 */
package com.aawhere.app;

/**
 * Simple interface for those that can provide an {@link ApplicationKey} to be used in general
 * filtering,etc.
 * 
 * @author aroller
 * 
 */
public interface ApplicationKeyProvider {

	/** provides the application key. */
	ApplicationKey applicationKey();
}
