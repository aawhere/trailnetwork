/**
 * 
 */
package com.aawhere.app;

import com.aawhere.id.Identifier;
import com.aawhere.id.LongIdentifier;

/**
 * @see Application#getApplicationId()
 * 
 * @author roller
 * 
 */
public class ApplicationId
		extends LongIdentifier<Application> {

	private static final long serialVersionUID = -4425374240818281917L;
	private static final Class<Application> KIND = Application.class;

	/**
	 * 
	 */
	ApplicationId() {
		super(KIND);
	}

	/**
	 * @param other
	 */
	public ApplicationId(Identifier<Long, Application> other) {
		super(other);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public ApplicationId(Long value) {
		super(value, KIND);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public ApplicationId(String value) {
		super(value, KIND);
	}

}
