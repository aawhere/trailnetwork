/**
 * 
 */
package com.aawhere.app.account;

import com.aawhere.app.ApplicationKey;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;

/**
 * @author aroller
 * 
 */
public class AccountFilterBuilder
		extends Filter.BaseFilterBuilder<AccountFilterBuilder> {

	public static AccountFilterBuilder create() {
		return new AccountFilterBuilder();
	}

	/**
	 * Searches for an account that matches the given alias.
	 * 
	 * @param alias
	 * @return
	 */
	public AccountFilterBuilder alias(String alias) {
		return addCondition(FilterCondition.create().field(Account.FIELD.KEY.ALIASES).operator(FilterOperator.EQUALS)
				.value(alias).build());
	}

	/** Doesn't look for an exact match so this is useful when user is searching for like answers. */
	public AccountFilterBuilder aliasStartsWith(String alias) {
		return startsWith(Account.FIELD.KEY.ALIASES, alias);
	}

	/**
	 * Adds a predicate since the application key is embedded into the {@link AccountId}. This also
	 * avoids the need for an index for items that may not have many results, such as aliases.
	 * 
	 * @param applicationKey
	 * @return
	 */
	public AccountFilterBuilder application(ApplicationKey applicationKey) {
		return addPredicate(AccountUtil.applicationKeyPredicate(applicationKey));
	}

}
