/**
 * 
 */
package com.aawhere.app.account;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE, name = Account.FIELD.ACCOUNTS)
public class Accounts
		extends BaseFilterEntities<Account> {

	/**
	 * Used to construct all instances of Accounts.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, Account, Accounts> {

		public Builder() {
			super(new Accounts());
		}

		@Override
		public Accounts build() {
			Accounts built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntities#getCollection()
	 */
	@Override
	@XmlElement(name = Account.FIELD.DOMAIN)
	public List<Account> getCollection() {
		return super.getCollection();
	}

	/** Use {@link Builder} to construct Accounts */
	private Accounts() {
	}

}
