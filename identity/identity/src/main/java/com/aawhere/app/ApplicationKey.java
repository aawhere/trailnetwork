/**
 *
 */
package com.aawhere.app;

import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.id.StringIdentifier;
import com.aawhere.xml.XmlNamespace;

import com.wordnik.swagger.annotations.ApiModel;

/**
 * A unique business key that identifies an application with a readable,
 * dash-separated-naming-standard.
 * 
 * All key values are automatically made to be lower case and trimmed.
 * 
 * @author Aaron Roller
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@ApiModel(description = "An abbreviation (2-3 lower case letters or numbers) uniquely identifing an Application.")
public class ApplicationKey
		extends StringIdentifier<Application> {

	private static final long serialVersionUID = 234321970714048470L;

	protected ApplicationKey() {
		super(Application.class);
	}

	/**
	 * Constructor that accepts the key value.
	 * 
	 * @param value
	 * @param kind
	 */
	public ApplicationKey(String value) {
		super(value.toLowerCase().trim(), Application.class);
	}
}
