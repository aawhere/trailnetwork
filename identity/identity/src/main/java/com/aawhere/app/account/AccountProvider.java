/**
 * 
 */
package com.aawhere.app.account;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.RepositoryUtil;
import com.aawhere.persist.ServiceStandard;
import com.aawhere.persist.ServiceStandardBase;
import com.google.common.base.Function;

/**
 * Simple interface for getting an account by it's id. The {@link AccountId#getRemoteId()} may
 * return an alias that can be used to find the account also. This is not guaranteed to be unique
 * and if a system allows an alias to be the same as a remote id this will return the remote id
 * first.
 * 
 * @author aroller
 * 
 */
public interface AccountProvider
		extends ServiceStandard<Accounts, Account, AccountId> {

	/**
	 * Provides the account known to the system that matches the account id or the account id that
	 * contains the alias as the remote id. If multiple are found when using an alias then only one
	 * is returned...which one is undefined and may change.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 *             when no match can be found
	 */
	Account account(AccountId id) throws EntityNotFoundException;

	/**
	 * Provides the fully loaded Accounts when provided the corresponding ids. Ids not found aren't
	 * included in the response with an explanation in {@link Accounts#getStatusMessages()} If a
	 * null is provided then a null is returned.
	 * 
	 * @param ids
	 * @return
	 */
	@Nullable
	public Accounts accounts(@Nullable Iterable<AccountId> ids);

	public interface ProfilePage {

		/**
		 * Provides the destination on the web where the profile can be viewed with a browser.
		 * 
		 * @param account
		 * @return
		 * @throws BaseException
		 *             when it's not possible to provide. The exact reason is up to the implementor.
		 */
		@Nonnull
		String profilePageUrl(Account account) throws BaseException;

	}

	public static class Default
			extends ServiceStandardBase<Accounts, Account, AccountId>
			implements AccountProvider {

		/**
		 * 
		 */
		public Default(Function<AccountId, Account> idFunction) {
			super(idFunction);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.app.account.AccountProvider#account(com.aawhere.app.account.AccountId)
		 */
		@Override
		public Account account(AccountId id) throws EntityNotFoundException {
			return RepositoryUtil.finderResult(id, idFunction().apply(id));
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.app.account.AccountProvider#accounts(java.lang.Iterable)
		 */
		@Override
		public Accounts accounts(Iterable<AccountId> ids) {
			return Accounts.create().addAll(all(ids).values()).build();
		}

	}
}
