/**
 * 
 */
package com.aawhere.app;

/**
 * A general indicator that this identifier is used to identify external data at an originating
 * {@link Application}.
 * 
 * @author aroller
 * 
 */
public interface ApplicationDataId {

	public static interface Data {
	}

	/**
	 * @return the applicationKey
	 */
	public ApplicationKey getApplicationKey();

	/**
	 * The ID that the application, identifieid by {@link #applicationKey}, uses to reference the
	 * activity.
	 * 
	 * @return
	 */
	public String getRemoteId();

	/**
	 * Returns the entire combination of the {@link #getApplicationKey()} and {@link #getRemoteId()}
	 * .
	 * 
	 * @return
	 */
	public String getValue();
}
