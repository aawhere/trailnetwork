/**
 * 
 */
package com.aawhere.app;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.BooleanUtils;

import com.google.inject.Singleton;

/**
 * Provides {@link Application} when given the {@link ApplicationKey}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class ApplicationXmlAdapter
		extends XmlAdapter<Application, ApplicationKey> {

	private Boolean show;

	/**
	 * 
	 */
	public ApplicationXmlAdapter() {
	}

	public ApplicationXmlAdapter(Boolean show) {
		this.show = show;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public ApplicationKey unmarshal(Application application) throws Exception {
		return (application != null && BooleanUtils.isTrue(show)) ? application.getKey() : null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Application marshal(ApplicationKey applicationKey) throws Exception {
		return (applicationKey != null && BooleanUtils.isTrue(show)) ? KnownApplication.byKey(applicationKey) : null;
	}

}
