/**
 * 
 */
package com.aawhere.app;

import com.aawhere.id.Identifiable;
import com.aawhere.id.Identifier;
import com.aawhere.id.StringIdentifier;

/**
 * When an {@link Identifier} needs to be an {@link ApplicationDataId}.
 * 
 * @author aroller
 * 
 */
public class IdentifierApplicationDataId<I extends Identifier<?, ?>, E extends Identifiable<I>>
		extends StringIdentifier<E>
		implements ApplicationDataId {

	private Object remoteId;

	/**
	 * 
	 */
	public IdentifierApplicationDataId() {

	}

	/**
	 * @param value
	 * @param kind
	 */
	public IdentifierApplicationDataId(I id, Class<E> kind) {
		super(id.getQualifiedString(), kind);
		this.remoteId = id.getValue();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.ApplicationDataId#getApplicationKey()
	 */
	@Override
	public ApplicationKey getApplicationKey() {
		return KnownApplication.TRAIL_NETWORK.key;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.ApplicationDataId#getRemoteId()
	 */
	@Override
	public String getRemoteId() {
		return String.valueOf(this.remoteId);
	}

	private static final long serialVersionUID = 7765022417042592747L;

}
