/**
 * 
 */
package com.aawhere.app;

import com.aawhere.ws.rs.SystemWebApiUri;

/**
 * {@link Instance}s that are known and must be referenced within the code. These may be used to
 * define and create database entries and/or used without a database (using only their keys for
 * identification and not their ids).
 * 
 * This is intended to be used with {@link SystemWebApiUri#APP_NAME}
 * 
 * FIXME:This should be moved to a more application specific area to avoid improper coupling with TN
 * behavior
 * 
 * @author roller
 * 
 */
public enum KnownInstance {

	/** The main instance for Garmin Connect to reference. */
	GARMIN_CONNECT(KnownApplication.GARMIN_CONNECT, "prod"),
	/** The main instance of TrailNetwork to reference. */
	TRAIL_NETWORK(KnownApplication.TRAIL_NETWORK, SystemWebApiUri.APP_NAME_LIVE),
	/**
	 * Staging server preparing to go live. This should behave almost the same, without as much
	 * data.
	 */
	TRAIL_NETWORK_DEMO(KnownApplication.TRAIL_NETWORK, SystemWebApiUri.APP_NAME_DEMO),
	/** The dev server where devleopment and testing is done before stable. */
	TRAIL_NETWORK_DEV(KnownApplication.TRAIL_NETWORK, SystemWebApiUri.APP_NAME_DEV),
	/** local development servers */
	TRAIL_NETWORK_DEVSERVER(KnownApplication.TRAIL_NETWORK, SystemWebApiUri.APP_NAME_LOCAL);

	final public KnownApplication application;
	final public InstanceKey key;

	/**
	 * @param application
	 * @param key
	 */
	private KnownInstance(KnownApplication application, String key) {
		this.application = application;
		this.key = new InstanceKey(key);
	}

	public Instance asInstance() {
		return new Instance.Builder().setKey(key).setApplication(application.asApplication()).build();
	}

	/**
	 * Indicates true if the {@link KnownInstance#key} equals {@link SystemWebApiUri#APP_NAME}.
	 * 
	 * This should be carefully used to programmatically define custom behavior based on the
	 * deployment of the code.
	 * 
	 * @return
	 */
	public boolean isThisSystem() {
		return this.key.getValue().equals(SystemWebApiUri.APP_NAME);
	}
}
