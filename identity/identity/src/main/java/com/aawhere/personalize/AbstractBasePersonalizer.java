/**
 *
 */
package com.aawhere.personalize;

import com.aawhere.lang.AbstractObjectBuilder;

/**
 * All Personalizers must extend AbstractBasePersonalizer.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class AbstractBasePersonalizer<DisplayT extends Personalized>
		implements Personalizer {

	/**
	 * Used to construct all instances of AbstractBasePersonalizer.
	 */
	/* @formatter:off */
	public abstract static class Builder
			<PlizerT extends AbstractBasePersonalizer< ?>,
			 BuilderT extends Builder<PlizerT,BuilderT>>
		extends AbstractObjectBuilder<PlizerT,BuilderT> {
		/* @formatter:on */

		public Builder(PlizerT plizer) {
			super(plizer);
		}

	}// end Builder

	/** Use {@link Builder} to construct AbstractBasePersonalizer */
	protected AbstractBasePersonalizer() {
	}

	protected DisplayT result;

	@Override
	public DisplayT getResult() {
		if (result == null) {
			throw new RuntimeException("You must call personalize first");
		}
		return result;
	}

}
