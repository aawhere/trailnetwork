/**
 *
 */
package com.aawhere.personalize;

import org.joda.time.DateTime;

/**
 * Convenience class that Produces a {@link DateTimePersonalized} with the personalized field
 * populated with {@link DateTimeDisplay}
 * 
 * @author Brian Chapman
 * 
 */
public class DateTimePersonalizer
		extends BaseDateTimePersonalizer<DateTimePersonalized> {

	CompositePersonalizer<DateTime> compPlizer;

	/**
	 * Used to construct all instances of DateTimePersonalizer.
	 */
	public static class Builder
			extends BasePersonalizer.Builder<DateTimePersonalizer, Builder> {
		public Builder() {
			super(new DateTimePersonalizer());
		}

		public DateTimePersonalizer build() {
			DateTimePersonalizedPersonalizer personalizedPlizer = new DateTimePersonalizedPersonalizer.Builder()
					.setPreferences(building.prefs).build();

			DateTimeDisplayPersonalizer displayPlizer = new DateTimeDisplayPersonalizer.Builder()
					.setPreferences(building.prefs).build();
			building.compPlizer = new CompositePersonalizer.Builder<DateTime>().add(personalizedPlizer)
					.add(displayPlizer).handlesType(DateTime.class).build();
			return super.build();
		}
	}

	/** Use {@link Builder} to construct DateTimePersonalizer */
	private DateTimePersonalizer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#personalize()
	 */
	@Override
	public void personalize(Object context) {
		if (context instanceof DateTime) {
			personalizeDateTime((DateTime) context);
		} else {
			throw new NotHandledException(this.handlesType(), context);
		}
	}

	public void personalizeDateTime(DateTime context) {
		compPlizer.personalize(context);
		result = (DateTimePersonalized) compPlizer.getResult();
	}
}
