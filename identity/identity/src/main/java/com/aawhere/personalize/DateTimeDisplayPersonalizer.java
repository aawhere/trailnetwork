/**
 *
 */
package com.aawhere.personalize;

import org.joda.time.DateTime;

import com.aawhere.joda.time.format.JodaDateTimeFormat;

/**
 * Takes a {@link DateTime} and produces a {@link DateTimeDisplay} with localized fields. Fields are
 * localized according to the supplied {@link Configuration} (aka Preferences}.
 * 
 * @see Personalizer, BasePersonalizer, Preferences, DateTime
 * 
 * @author Brian Chapman
 * 
 */
public class DateTimeDisplayPersonalizer
		extends BaseDateTimePersonalizer<DateTimeDisplay> {

	private JodaDateTimeFormat fmt = new JodaDateTimeFormat();

	/**
	 * Used to construct all instances of DateTimeDisplayPersonalizer.
	 */
	public static class Builder
			extends BaseDateTimePersonalizer.Builder<Builder, DateTimeDisplayPersonalizer> {

		public Builder() {
			super(new DateTimeDisplayPersonalizer());
			building.result = new DateTimeDisplay();
		}
	}// end Builder

	/** Use {@link Builder} to construct DateTimeDisplayPersonalizer */
	private DateTimeDisplayPersonalizer() {
	}

	@Override
	protected void personalizeDateTime(DateTime context) {
		result.lang = getLocale();
		result.type = personalizeType(context);
		result.fullTime = personalizeDate(JodaDateTimeFormat.Format.FULL_TIME, context);
		result.mediumDate = personalizeDate(JodaDateTimeFormat.Format.MEDIUM_DATE, context);
		result.mediumDateTime = personalizeDate(JodaDateTimeFormat.Format.MEDIUM_DATETIME, context);
		result.mediumTime = personalizeDate(JodaDateTimeFormat.Format.MEDIUM_TIME, context);
		result.shortDate = personalizeDate(JodaDateTimeFormat.Format.SHORT_DATE, context);
		result.shortDateTime = personalizeDate(JodaDateTimeFormat.Format.SHORT_DATETIME, context);
		result.sortableDateTime = personalizeDate(JodaDateTimeFormat.Format.SORTABLE_DATETIME, context);
	}

	private String personalizeDate(JodaDateTimeFormat.Format format, DateTime context) {
		return fmt.format(context, getLocale(), format);
	}
}
