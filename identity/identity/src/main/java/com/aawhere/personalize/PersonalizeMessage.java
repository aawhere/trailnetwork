/**
 *
 */
package com.aawhere.personalize;

import com.aawhere.swagger.DocSupport;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.Messages;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.util.rb.ParamWrappingCompleteMessage;

/**
 * {@link Messages}s for the Personalize package.
 * 
 * @author Brian Chapman
 * 
 */
public enum PersonalizeMessage implements Message {

	/** @see NotHandledException */
	NOT_HANDLED("Could not handle type {OBJECT}. Supported type: {SUPPORTED_TYPE}", Param.OBJECT, Param.SUPPORTED_TYPE),
	/** Indicates that localization is required when requesting a localization only expansion */
	LOCALIZATION_REQUIRED(
			"{HEADER_KEY} must be in the header since a request was made to expand a field that requires localization.",
			Param.HEADER_KEY),
	/**
	 * do not localize. Using Message for param substitution.
	 * 
	 * @deprecated use {@link CompleteMessage.Builder#paramWrapped(ParamKey, Object)}
	 */
	WRAPPER_OPEN_SPAN_TAG("<span class=\"{WRAPPER_KEY}\">", Param.WRAPPER_KEY),
	/**
	 * do not localize. Using Message for param substitution.
	 * 
	 * @deprecated use {@link CompleteMessage.Builder#paramWrapped(ParamKey, Object)}
	 */
	WRAPPER_CLOSE_SPAN_TAG("</span>");

	private ParamKey[] parameterKeys;
	private String message;

	private PersonalizeMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** @see Personalizer#personalize(Object) */
		OBJECT,
		/** When a header is required, this is the key to use. */
		HEADER_KEY,
		/** @see Personalizer#personalize(Object) */
		SUPPORTED_TYPE,
		/**
		 * @deprecated use {@link CompleteMessage.Builder#paramWrapped(ParamKey, Object)} opening
		 *             wrapper. Typically used for formatting descriptions to allow wrapping values
		 *             in span tags for client side href substitution. Used across many bundles.
		 */
		WO,
		/**
		 * @deprecated use {@link ParamWrappingCompleteMessage#paramWrapped(ParamKey, Object)}
		 *             closing wrapper
		 */
		WC,
		/**
		 * @deprecated use {@link ParamWrappingCompleteMessage#paramWrapped(ParamKey, Object)} key
		 *             to identify a wrapper
		 */
		WRAPPER_KEY;

		private CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static class Doc {
		public static final String ACCEPT_I18N_DATA_TYPE = DocSupport.DataType.BOOLEAN;
		public static final String ACCEPT_I18N_PARAM_TYPE = DocSupport.ParamType.HEADER;
		public static final String ACCEPT_I18_NAME = PersonalizeUtil.ACCEPT_I18N;
		public static final boolean ACCEPT_I18_REQUIRED = false;
	}

}
