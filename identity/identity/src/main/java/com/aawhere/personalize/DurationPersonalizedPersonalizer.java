/**
 *
 */
package com.aawhere.personalize;

import org.joda.time.Duration;

/**
 * Takes a {@link Duration} and produces a {@link DurationPersonalized} with localized fields.
 * Fields are localized according to the supplied {@link Configuration} (aka Preferences}.
 * 
 * @see Personalizer, BasePersonalizer, Preferences, Duration
 * 
 * @author Brian Chapman
 * 
 */
public class DurationPersonalizedPersonalizer
		extends BaseDurationPersonalizer<DurationPersonalized> {

	/**
	 * Used to construct all instances of DurationPersonalizedPersonalizer.
	 */
	public static class Builder
			extends BaseDurationPersonalizer.Builder<Builder, DurationPersonalizedPersonalizer> {

		public Builder() {
			super(new DurationPersonalizedPersonalizer());
			building.result = new DurationPersonalized();
		}
	}

	/** Use {@link Builder} to construct DurationPersonalizedPersonalizer */
	private DurationPersonalizedPersonalizer() {
	}

	protected void personalizeDuration(Duration context) {
		result.duration = context.getMillis();
	}
}
