/**
 *
 */
package com.aawhere.personalize;

import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.joda.time.format.DurationFormat;
import com.aawhere.xml.XmlNamespace;

/**
 * Localized data for {@link Duration}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
public class DurationDisplay
		extends PersonalizedBase {

	/**
	 * Local used to personalize Duration.
	 */
	public Locale getLang() {
		return lang;
	}

	@XmlAttribute
	protected Locale lang;

	/**
	 * Duration type e.g. elapsed-time
	 */
	public String getType() {
		return type;
	}

	@XmlAttribute
	protected String type;

	/**
	 * Duration compact format
	 * 
	 * @see DurationFormat
	 */
	public String getCompact() {
		return compact;
	}

	@XmlElement
	protected String compact;

	/**
	 * Duration Iso format
	 * 
	 * @see DurationFormat
	 */
	public String getIso() {
		return iso;
	}

	@XmlElement
	protected String iso;

	/**
	 * Duration written out format
	 * 
	 * @see DurationFormat
	 */
	public String getWrittenOut() {
		return shorthand;
	}

	@XmlElement
	protected String shorthand;
}
