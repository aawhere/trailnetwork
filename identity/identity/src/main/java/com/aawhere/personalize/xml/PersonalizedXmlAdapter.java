/**
 *
 */
package com.aawhere.personalize.xml;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.lang.NotImplementedException;
import com.aawhere.personalize.BasePersonalizer;
import com.aawhere.personalize.PersonalizeUtil;
import com.aawhere.personalize.Personalized;
import com.aawhere.personalize.PersonalizedBase;
import com.aawhere.personalize.Personalizer;
import com.aawhere.xml.bind.OptionalXmlAdapter;

/**
 * The parent of all {@link XmlAdapter}s that manage objects that are to be {@link PersonalizedBase}
 * .
 * 
 * @author Aaron Roller
 * @author Brian Chapman
 * 
 */

public abstract class PersonalizedXmlAdapter<DisplayT extends Personalized, BoundingT>
		extends OptionalXmlAdapter<DisplayT, BoundingT> {

	private static final Visibility DEFAULT_VISIBILITY = Visibility.SHOW;

	/**
	 * Default constructors should use this to create a {@link Personalizer} with System default
	 * Locale and Preferences.
	 */
	protected PersonalizedXmlAdapter(BasePersonalizer.Builder<?, ?> builder, Visibility visibility) {
		super(visibility);
		plizer = PersonalizeUtil.personalizerDefault(builder);
	}

	protected PersonalizedXmlAdapter(BasePersonalizer.Builder<?, ?> builder) {
		this(builder, DEFAULT_VISIBILITY);
	}

	public PersonalizedXmlAdapter(Personalizer plizer) {
		this(plizer, DEFAULT_VISIBILITY);
	}

	public PersonalizedXmlAdapter(Personalizer plizer, Visibility visibility) {
		super(visibility);
		this.plizer = plizer;
	}

	public PersonalizedXmlAdapter(Personalizer plizer, Show show) {
		super(show);
		this.plizer = plizer;
	}

	protected Personalizer plizer;

	protected Personalizer getPersonalizer() {
		return plizer;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public DisplayT showMarshal(BoundingT b) throws Exception {
		if (b == null) {
			return null;
		}
		getPersonalizer().personalize(b);
		return (DisplayT) getPersonalizer().getResult();
	}

	@Override
	protected BoundingT showUnmarshal(DisplayT v) throws Exception {
		throw new NotImplementedException("Cannot unmarshal a " + v.getClass().getSimpleName());
	}
}