/**
 *
 */
package com.aawhere.personalize;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.format.UnitFormatFactory;
import com.aawhere.measure.xml.MeasurementXmlUtil;
import com.aawhere.person.pref.PrefMeasureUtil;
import com.aawhere.text.format.custom.DoubleFormat;

import com.google.common.collect.Maps;

/**
 * Takes a {@link Quantity} and produces a {@link PersonalizedBase} with localized fields. Fields
 * are localized according to the preferences setup in the {@link Builder}.
 * 
 * @see Builder
 * 
 * @author Brian Chapman
 * 
 */
public abstract class BaseQuantityPersonalizer<DisplayT extends PersonalizedBase>
		extends BasePersonalizer<DisplayT> {

	private static final String DEFAULT_UNIT_DELIMINATOR = " ";
	private static final Map<Class<? extends Quantity<?>>, String> deliminatorMap = Maps.newHashMap();

	static {
		deliminatorMap.put(Ratio.class, "");
	}

	private Map<Class<? extends Quantity<?>>, Unit<?>> unitMap = new HashMap<Class<? extends Quantity<?>>, Unit<?>>();
	private Map<Class<? extends Quantity<?>>, Integer> maxDigitsMap = new HashMap<Class<? extends Quantity<?>>, Integer>();

	/** Use {@link Builder} to construct BaseQuantityPersonalizer */
	protected BaseQuantityPersonalizer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#getResult()
	 */
	@Override
	public DisplayT getResult() {
		return super.getResult();
	}

	/**
	 * return the {@link Quantity's type}
	 * 
	 * @return
	 */
	protected <Q extends Quantity<?>> String personalizeType(Q context) {
		return MeasurementXmlUtil.typeFor(context);
	}

	protected <Q extends Quantity<Q>> Double personalizeValue(Q context) {
		com.aawhere.number.format.custom.DoubleFormat format = new com.aawhere.number.format.custom.DoubleFormat(5);
		return format.format(context.doubleValue(getUnit(context)));
	}

	/**
	 * Format the value and unit with custom deliminator such as a space or dash "-".
	 * 
	 * @param context
	 * @param deliminator
	 * @return
	 */
	protected <Q extends Quantity<Q>> String personalizeFormatted(Q context, String deliminator) {
		String formattedValue = personalizeFormattedValue(context);
		String unit = personalizeUnit(context);
		return formattedValue + deliminator + unit; // TODO: identify if some quantities and/or
		// locals
		// require a different order here. see personalizeUnit.
	}

	protected <Q extends Quantity<Q>> String personalizeFormatted(Q context) {
		String deliminator = DEFAULT_UNIT_DELIMINATOR;
		Class<?>[] interfaces = context.getClass().getInterfaces();
		for (Class<?> quantityType : interfaces) {
			if (deliminatorMap.containsKey(quantityType)) {
				deliminator = deliminatorMap.get(quantityType);
			}
		}
		return personalizeFormatted(context, deliminator);
	}

	protected <Q extends Quantity<Q>> String personalizeFormattedValue(Q context) {
		Class<Q> contextClass = MeasurementUtil.classFor(context);
		int maximumFractionDigits = 0;
		int minimumFractionDigits = 0;

		// Cache the results of the Configuration (prefs) lookup by class.
		if (maxDigitsMap.containsKey(contextClass)) {
			maximumFractionDigits = maxDigitsMap.get(contextClass);
			minimumFractionDigits = maximumFractionDigits; // for now.
		} else {
			maximumFractionDigits = PrefMeasureUtil.getMaxDigitsFor(contextClass, prefs);
			minimumFractionDigits = maximumFractionDigits;
			maxDigitsMap.put(contextClass, maximumFractionDigits);
		}
		return personalizeFormattedValue(maximumFractionDigits, minimumFractionDigits, context);
	}

	protected <Q extends Quantity<Q>> String personalizeFormattedValue(int maxDigits, int minDigits, Q context) {
		boolean isGroupingEnabled = true;
		DoubleFormat formatter = new DoubleFormat(maxDigits, minDigits, isGroupingEnabled);
		return formatter.format(personalizeValue(context), getLocale());
	}

	/**
	 * return a localized string representing the unit. The unit is obtained from the preferences.
	 * 
	 * @return
	 */
	protected <Q extends Quantity<Q>> String personalizeUnit(Q context) {
		StringBuffer buf = new StringBuffer();
		try {
			// You can also pass in a java.text.FieldPosition here, which may be useful in the
			// personalizeFormatted method.
			UnitFormatFactory.getInstance().createUnitFormat(getLocale()).format(getUnit(context), buf);
		} catch (IOException e) {
			// Don't known why we'd end up here, but it must be bad!
			throw new RuntimeException(e);
		}
		return buf.toString();
	}

	@SuppressWarnings("unchecked")
	protected <Q extends Quantity<Q>> Unit<Q> getUnit(Q context) {
		Class<Q> contextClass = MeasurementUtil.classFor(context);
		if (unitMap.containsKey(contextClass)) {
			return (Unit<Q>) unitMap.get(contextClass);
		} else {
			Unit<Q> unit = PrefMeasureUtil.getUnitFor(MeasurementUtil.classFor(context), prefs);
			unitMap.put(contextClass, unit);
			return unit;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#handlesType()
	 */
	@Override
	public Class<?> handlesType() {
		return Quantity.class;
	}

	/**
	 * Used to construct all instances of BaseQuantityPersonalizer.
	 */
	/* @formatter:off */
	public static class Builder<BuilderT extends Builder<BuilderT, PlizerT>,
	PlizerT extends BaseQuantityPersonalizer<?>>
	extends BasePersonalizer.Builder<PlizerT, BuilderT> {
		/* @formatter:on */

		public Builder(PlizerT plizer) {
			super(plizer);
		}

	}// end Builder
}
