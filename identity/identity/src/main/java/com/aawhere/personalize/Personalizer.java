/**
 *
 */
package com.aawhere.personalize;

import com.aawhere.personalize.xml.PersonalizedXmlAdapter;

/**
 * Personalizers take in objects and produce {@link PersonalizedBase} objects. Personalizers may be
 * used for any purpose that requires translating from an input object to a {@link PersonalizedBase}
 * object. However, they are typically used to localize data or in {@link PersonalizedXmlAdapter}
 * for JaxB translations.
 * 
 * 
 * @see Preferences, Personalized
 * 
 * @author Brian Chapman
 * 
 */
public interface Personalizer {

	/**
	 * Personalize an object. This method must be called before {@link #getResult()}. If the
	 * personalizer cannot handle the object, it should throw a cannot handle exception.
	 * 
	 * @param object
	 * 
	 * @throws NotHandledException
	 *             the Personalizer could not handle the supplied object.
	 */
	void personalize(Object object);

	/**
	 * The result of the {@link Personalizer#personalize(Object)} method. You must call the
	 * {@link #personalize(Object)} method first before retrieving the result.
	 * 
	 * @return
	 */
	Personalized getResult();

	/**
	 * The type this Personalizer handles.
	 * 
	 * @return
	 */
	Class<?> handlesType();

}