/**
 *
 */
package com.aawhere.personalize;

import org.joda.time.Duration;

/**
 * Convenience class that Produces a {@link DurationPersonalized} with the personalized field
 * populated with {@link DurationDisplay}
 * 
 * @author Brian Chapman
 * 
 */
public class DurationPersonalizer
		extends BaseDurationPersonalizer<DurationPersonalized> {

	CompositePersonalizer<Duration> compPlizer;

	/**
	 * Used to construct all instances of DurationPersonalizer.
	 */
	public static class Builder
			extends BasePersonalizer.Builder<DurationPersonalizer, Builder> {
		public Builder() {
			super(new DurationPersonalizer());
		}

		public DurationPersonalizer build() {
			DurationPersonalizedPersonalizer personalizedPlizer = new DurationPersonalizedPersonalizer.Builder()
					.setPreferences(building.prefs).build();

			DurationDisplayPersonalizer displayPlizer = new DurationDisplayPersonalizer.Builder()
					.setPreferences(building.prefs).build();

			building.compPlizer = new CompositePersonalizer.Builder<Duration>().add(personalizedPlizer)
					.add(displayPlizer).handlesType(Duration.class).build();
			return super.build();
		}
	}

	/** Use {@link Builder} to construct DurationPersonalizer */
	private DurationPersonalizer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#personalize()
	 */
	@Override
	public void personalize(Object context) {
		if (context instanceof Duration) {
			personalizeDuration((Duration) context);
		} else {
			throw new NotHandledException(this.handlesType(), context);
		}
	}

	public void personalizeDuration(Duration context) {
		compPlizer.personalize(context);
		result = (DurationPersonalized) compPlizer.getResult();
	}
}
