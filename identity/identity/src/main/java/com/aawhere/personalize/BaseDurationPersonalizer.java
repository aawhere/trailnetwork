/**
 *
 */
package com.aawhere.personalize;

import org.joda.time.Duration;

/**
 * Abstract base {@link Personalizer} for personalizing {@link Duration}.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class BaseDurationPersonalizer<PersonalizedT extends PersonalizedBase>
		extends BasePersonalizer<PersonalizedT> {

	/**
	 * Used to construct all instances of BaseDurationPersonalizer.
	 */
	/* @formatter:off */
	public static class Builder<BuilderT extends Builder<BuilderT, PlizerT>,
			PlizerT extends BaseDurationPersonalizer<?>>
			extends BasePersonalizer.Builder<PlizerT,
					BuilderT> {
	/* @formatter:on */
		public Builder(PlizerT plizer) {
			super(plizer);
		}
	}

	/** Use {@link Builder} to construct BaseDurationPersonalizer */
	protected BaseDurationPersonalizer() {
		super();
	}

	protected PersonalizedT result;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#getResult()
	 */
	@Override
	public PersonalizedT getResult() {
		return result;
	}

	@Override
	public void personalize(Object context) {
		if (context instanceof Duration) {
			personalizeDuration((Duration) context);
		} else {
			throw new NotHandledException(handlesType(), context);
		}
	}

	protected abstract void personalizeDuration(Duration context);

	public Class<Duration> handlesType() {
		return Duration.class;
	}

}