package com.aawhere.personalize;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Marker class for Personalized objects. Personalized objects are produced by {@link Personalizer}
 * s. Personalized objects may be nested within each other to create a hierarchy of Personalized
 * objects.
 * 
 * Note: this would normally be an interface, however due to the way JaxB handles things it is a
 * class.
 * 
 * @author Brian Chapman
 * 
 */
@XmlTransient
@XmlAccessorType(XmlAccessType.NONE)
public abstract class PersonalizedBase
		implements Personalized {

	/**
	 * This is not annotated with JaxB annotations to prevent duplicate elements generated.
	 * 
	 * See {@link #getPersonalized()}
	 */
	@XmlTransient
	protected Personalized personalized;

	/**
	 * You probably need to override this for JaxB to work. This is not annotated to prevent
	 * duplicate elements generated.
	 * 
	 * @return the personalized object.
	 */
	@Override
	@XmlTransient
	public Personalized getPersonalized() {
		return personalized;
	}

}
