/**
 *
 */
package com.aawhere.personalize;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.joda.time.DateTime;

import com.aawhere.joda.time.xml.DateTimeXmlAdapter;
import com.aawhere.xml.XmlNamespace;

/**
 * The XML Representation of {@link DateTime} for {@link DateTimeXmlAdapter} to marshal and
 * unmarshal.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(name = DateTimePersonalized.API_NAME, namespace = XmlNamespace.API_VALUE)
public class DateTimePersonalized
		extends PersonalizedBase {

	public static final String API_NAME = "DateTime";

	/**
	 * The duration in XML format
	 */
	public String getDate() {
		return date;
	}

	@XmlAttribute
	protected String date;

	// Required for JaxB, not required for java.
	@XmlElement(name = "display")
	public DateTimeDisplay getPersonalized() {
		return (DateTimeDisplay) personalized;
	}
}
