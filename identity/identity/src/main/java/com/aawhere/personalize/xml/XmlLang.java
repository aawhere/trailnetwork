/**
 * 
 */
package com.aawhere.personalize.xml;

import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

import org.apache.commons.lang3.StringUtils;

import com.aawhere.collections.Index;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.xml.XmlNamespace;

/**
 * A utility to translate the Java {@link Locale} into the xml:lang attribute standard.
 * 
 * You should include this class in your class as follows:
 * 
 * <pre>
 * <code>
 * 		@XmlAttribute(namespace = XmlLang.NAMESPACE, name = XmlLang.ATTRIBUTE_NAME)
 * 	XmlLang lang;
 * </code>
 * </pre>
 * 
 * @see http://www.w3.org/TR/REC-xml/#sec-lang-tag
 * 
 * @author roller
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = XmlLang.ATTRIBUTE_NAME, namespace = XmlLang.NAMESPACE)
public class XmlLang {

	private static final char XML_SEPARATOR = '-';
	private static final char LOCALE_SEPARATOR = '_';
	/**
	 * 
	 */
	public static final String NAMESPACE = XmlNamespace.W3C_XML_1998_VALUE;
	public static final String ATTRIBUTE_NAME = "lang";
	@XmlValue
	String value;

	/** A convience method for constructing a builder */
	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct XmlLang */
	private XmlLang() {
	}

	/**
	 * Used to construct all instances of XmlLang.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<XmlLang> {

		public Builder() {
			super(new XmlLang());
		}

		public Builder locale(Locale locale) {
			building.value = locale.toString().replace(LOCALE_SEPARATOR, XML_SEPARATOR);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull(building.value);
		}

	}// end Builder

	/**
	 * @return
	 */
	public Locale toLocale() {
		String[] parts = StringUtils.split(value, XML_SEPARATOR);
		Locale locale;

		switch (parts.length) {
			case 1:
				locale = new Locale(parts[Index.FIRST.index]);
				break;
			case 2:
				locale = new Locale(parts[Index.FIRST.index], parts[Index.SECOND.index]);
				break;
			default:
				throw new InvalidChoiceException(parts.length, value);

		}
		return locale;
	}
}
