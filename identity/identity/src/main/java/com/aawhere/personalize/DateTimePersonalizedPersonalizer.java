/**
 *
 */
package com.aawhere.personalize;

import org.joda.time.DateTime;

/**
 * Takes a {@link DateTime} and produces a {@link DateTimePersonalized} with localized fields.
 * Fields are localized according to the supplied {@link Configuration} (aka Preferences}.
 * 
 * @see Personalizer, BasePersonalizer, Preferences, DateTime
 * 
 * @author Brian Chapman
 * 
 */
public class DateTimePersonalizedPersonalizer
		extends BaseDateTimePersonalizer<DateTimePersonalized> {

	/**
	 * Used to construct all instances of DateTimePersonalizedPersonalizer.
	 */
	public static class Builder
			extends BaseDateTimePersonalizer.Builder<Builder, DateTimePersonalizedPersonalizer> {

		public Builder() {
			super(new DateTimePersonalizedPersonalizer());
			building.result = new DateTimePersonalized();
		}
	}

	/** Use {@link Builder} to construct DateTimePersonalizedPersonalizer */
	private DateTimePersonalizedPersonalizer() {
	}

	protected void personalizeDateTime(DateTime context) {
		result.date = context.toString();
	}
}
