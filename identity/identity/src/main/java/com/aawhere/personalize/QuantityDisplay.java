/**
 *
 */
package com.aawhere.personalize;

import java.util.Locale;

import javax.measure.quantity.Quantity;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.xml.XmlNamespace;

/**
 * Localized data for {@link Quantity}.
 * 
 * @author Brian Chapman
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
public class QuantityDisplay
		extends PersonalizedBase {

	/**
	 * Used to construct all instances of QuantityDisplay.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<QuantityDisplay> {

		public Builder() {
			super(new QuantityDisplay());
		}

		public Builder setAbbreviatedUnit(String unit) {
			building.abbreviatedUnit = unit;
			return this;
		}

		public Builder setFormatted(String formatted) {
			building.formatted = formatted;
			return this;
		}

		public Builder setFormattedValue(String formatted) {
			building.formattedValue = formatted;
			return this;
		}

		public Builder setValue(Double value) {
			building.value = value;
			return this;
		}

		public Builder setLang(Locale lang) {
			building.lang = lang;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct QuantityDisplay */
	private QuantityDisplay() {
	}

	@XmlElement
	private String abbreviatedUnit;
	@XmlElement
	private String formatted;
	@XmlElement
	private String formattedValue;
	@XmlElement
	private Double value;
	@XmlAttribute
	private Locale lang;

	/**
	 * Abbreviated Unit e.g. m for meters.
	 * 
	 * @return the abbreviatedUnit
	 */
	public String getAbbreviatedUnit() {
		return this.abbreviatedUnit;
	}

	/**
	 * Full unit localized unit e.g. meters or mètres (fr)
	 */
	// Currently limited by javax.measure.
	// public string getFullUnit() {
	//
	// }

	/**
	 * Value formatted with unit. e.g. 1,234.56 km
	 * 
	 * @return the full
	 */
	public String getFormatted() {
		return this.formatted;
	}

	/**
	 * Value formatted without unit. e.g. 1,234.56
	 * 
	 * @return
	 */
	public String getFormattedValue() {
		return this.formattedValue;
	}

	/**
	 * Raw value
	 * 
	 * @return the value
	 */
	public Double getValue() {
		return this.value;
	}

	/**
	 * The language used to localize the quantity.
	 * 
	 * @return the lang
	 */
	public Locale getLang() {
		return this.lang;
	}
}