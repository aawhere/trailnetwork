/**
 *
 */
package com.aawhere.personalize;

import javax.measure.quantity.Quantity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.measure.xml.QuantityXmlAdapter;
import com.aawhere.xml.XmlNamespace;

/**
 * The XML Representation of {@link Quantity} for {@link QuantityXmlAdapter} to marshal and
 * unmarshal.
 * 
 * 
 * @author Aaron Roller
 * 
 */
@XmlRootElement
@XmlType(name = "quantity", namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class QuantityPersonalized
		extends PersonalizedBase {

	public static class Builder
			extends ObjectBuilder<QuantityPersonalized> {
		/**
		 * @param personalized
		 */
		public Builder() {
			super(new QuantityPersonalized());
		}

		public Builder setType(String type) {
			building.type = type;
			return this;
		}

		public Builder setValue(Double value) {
			building.value = value;
			return this;
		}

		public Builder setUnit(String unit) {
			building.unit = unit;
			return this;
		}
	}

	@XmlAttribute(required = true)
	private String type;
	@XmlAttribute(required = true)
	private String unit;
	@XmlAttribute(required = true)
	private Double value;

	public String getType() {
		return type;
	}

	public String getUnit() {
		return unit;
	}

	public Double getValue() {
		return value;
	}

	/**
	 * Requried for JAXB.
	 * 
	 */
	public QuantityPersonalized() {
	}

	// Required for JaxB, not required for java.
	@XmlElement(name = "display")
	@Override
	public QuantityDisplay getPersonalized() {
		return (QuantityDisplay) personalized;
	}
}
