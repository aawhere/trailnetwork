/**
 *
 */
package com.aawhere.personalize;

import java.util.ArrayList;
import java.util.List;

import com.aawhere.lang.Assertion;

/**
 * Takes several {@link Personalizer}s and creates a composite personalizer. The
 * CompositePersonalizer will personalize each composed personalizer in the order they were added.
 * The resulting {@link PersonalizedBase} objects will be chained together as such:
 * 
 * {@literal
 * 		PersonalizedFromFirstPersonalizer
 * 			PersonalizedFromSecondPersonalizer
 * 				PersonalizedFromNthPersonalizer
 *} @see Personalized
 * 
 * @author Brian Chapman
 * 
 */
public class CompositePersonalizer<BoundingT>
		extends AbstractBasePersonalizer<PersonalizedBase> {
	/**
	 * Used to construct all instances of CompositePersonalizer.
	 */
	public static class Builder<BoundingT>
			extends AbstractBasePersonalizer.Builder<CompositePersonalizer<BoundingT>, Builder<BoundingT>> {

		public Builder() {
			super(new CompositePersonalizer<BoundingT>());
		}

		public Builder(CompositePersonalizer<BoundingT> plizer) {
			super(plizer);
		}

		public Builder<BoundingT> add(Personalizer plizer) {
			building.plizers.add(plizer);
			return this;
		}

		public Builder<BoundingT> handlesType(Class<BoundingT> type) {
			building.handlesType = type;
			return this;
		}

		@Override
		protected void validate() {
			Assertion.assertNotNull(building.handlesType);
		}

	}// end Builder

	/** Use {@link Builder} to construct CompositePersonalizer */
	protected CompositePersonalizer() {
	}

	private List<Personalizer> plizers = new ArrayList<Personalizer>();

	private Class<BoundingT> handlesType;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#personalize()
	 */
	@Override
	public void personalize(Object object) {
		if (handlesType.isAssignableFrom(object.getClass())) {
			for (Personalizer plizer : plizers) {
				plizer.personalize(object);
				PersonalizedBase plizerResult = (PersonalizedBase) plizer.getResult();
				if (result == null) {
					result = plizerResult;
				} else {
					result.personalized = plizerResult;
				}
			}
		} else {
			throw new NotHandledException(handlesType(), object);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#handlesType()
	 */
	@Override
	public Class<?> handlesType() {
		return handlesType;
	}
}
