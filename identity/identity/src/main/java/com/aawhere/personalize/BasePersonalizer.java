/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 23, 2010
person : com.aawhere.person.alize.Personalizer.java
 */
package com.aawhere.personalize;

import java.util.Locale;

import org.apache.commons.configuration.Configuration;

import com.aawhere.identity.IdentityManager;
import com.aawhere.lang.Assertion;
import com.aawhere.person.pref.PrefUtil;

/**
 * Base class for {@link Personalizer}s that use preferences to format {@link PersonalizedBase}
 * displays.
 * 
 * Building takes a {@link Configuration} (referred to as Preferences). These preferences are used
 * to personalize an object (see {@link Personalizer#personalize(Object object)}.
 * 
 * The supplied {@link Configuration} should be set prior to adding to the {@link Builder}. See
 * {@link PrefUtil} for helper methods.
 * 
 * @author Brian Chapman
 * 
 */
public abstract class BasePersonalizer<DisplayT extends Personalized>
		extends AbstractBasePersonalizer<DisplayT> {

	/** Use {@link Builder} to construct Personalizer */
	protected BasePersonalizer() {
	}

	protected Configuration prefs;
	protected IdentityManager identityManager;
	private Locale locale;

	protected <BoundingT extends Object> String personalizeType(BoundingT context) {
		return context.getClass().getSimpleName().toLowerCase();
	}

	protected Locale getLocale() {
		return locale;
	}

	/**
	 * Used to construct all instances of Personalizer.
	 */
	/* @formatter:off */
	public abstract static class Builder
			<PlizerT extends BasePersonalizer<?>,
			 BuilderT extends Builder<PlizerT, BuilderT>>
		 extends AbstractBasePersonalizer.Builder< PlizerT,BuilderT> {
		/* @formatter:on */

		public Builder(PlizerT plizer) {
			super(plizer);
		}

		public BuilderT setPreferences(Configuration prefs) {
			building.prefs = prefs;
			return dis;
		}

		public BuilderT setIdentityManager(IdentityManager identityManager) {
			building.identityManager = identityManager;
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("DefaultPreferences", building.prefs);
		}

		@Override
		public PlizerT build() {
			((BasePersonalizer<?>) building).locale = PrefUtil.getLocale(building.prefs);
			return super.build();
		}
	}// end Builder
}
