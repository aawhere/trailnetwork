/**
 *
 */
package com.aawhere.personalize;

import org.joda.time.Duration;

import com.aawhere.joda.time.format.DurationFormat;

/**
 * Takes a {@link Duration} and produces a {@link DurationDisplay} with localized fields. Fields are
 * localized according to the supplied {@link Configuration} (aka Preferences}.
 * 
 * @see Personalizer, BasePersonalizer, Preferences, Duration
 * 
 * @author Brian Chapman
 * 
 */
public class DurationDisplayPersonalizer
		extends BaseDurationPersonalizer<DurationDisplay> {

	/**
	 * Used to construct all instances of DurationDisplayPersonalizer.
	 */
	public static class Builder
			extends BaseDurationPersonalizer.Builder<Builder, DurationDisplayPersonalizer> {

		public Builder() {
			super(new DurationDisplayPersonalizer());
			building.result = new DurationDisplay();
		}
	}// end Builder

	/** Use {@link Builder} to construct DurationDisplayPersonalizer */
	private DurationDisplayPersonalizer() {
	}

	@Override
	protected void personalizeDuration(Duration context) {
		result.lang = getLocale();
		result.type = personalizeType(context);
		result.compact = personalizeDuration(DurationFormat.Format.COMPACT, context);
		result.iso = personalizeDuration(DurationFormat.Format.ISO, context);
		result.shorthand = personalizeDuration(DurationFormat.Format.SHORTHAND, context);
	}

	private String personalizeDuration(DurationFormat.Format format, Duration context) {
		DurationFormat fmt = new DurationFormat();
		return fmt.format(context, getLocale(), format);
	}
}
