/**
 *
 */
package com.aawhere.personalize;

import javax.measure.quantity.Quantity;

/**
 * Convenience class that Produces a {@link QuantityPersonalized} with the personalized field
 * populated with {@link QuantityDisplay}
 * 
 * @author Brian Chapman
 * 
 */
public class QuantityPersonalizer
		extends BaseQuantityPersonalizer<QuantityPersonalized> {

	QuantityPersonalizedPersonalizer personalizedPlizer;
	QuantityDisplayPersonalizer displayPlizer;

	/**
	 * Used to construct all instances of QuantityPersonalizer.
	 */
	public static class Builder
			extends BasePersonalizer.Builder<QuantityPersonalizer, Builder> {
		public Builder() {
			super(new QuantityPersonalizer());
		}

		@Override
		public QuantityPersonalizer build() {
			building.personalizedPlizer = new QuantityPersonalizedPersonalizer.Builder().setPreferences(building.prefs)
					.build();
			building.displayPlizer = new QuantityDisplayPersonalizer.Builder().setPreferences(building.prefs).build();
			return super.build();
		}
	}

	/** Use {@link Builder} to construct QuantityPersonalizer */
	private QuantityPersonalizer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#personalize()
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void personalize(Object context) {
		if (context instanceof Quantity) {
			personalizeQuantity((Quantity) context);
		} else {
			throw new NotHandledException(this.handlesType(), context);
		}
	}

	public <Q extends Quantity<Q>> void personalizeQuantity(Q context) {

		@SuppressWarnings("unchecked")
		CompositePersonalizer<Q> compPlizer = new CompositePersonalizer.Builder<Q>().add(personalizedPlizer)
				.add(displayPlizer).handlesType((Class<Q>) context.getClass()).build();

		compPlizer.personalize(context);
		result = (QuantityPersonalized) compPlizer.getResult();
	}
}
