@XmlJavaTypeAdapters({ @XmlJavaTypeAdapter(value = LocaleXmlAdapter.class, type = Locale.class) })
@XmlSchema(xmlns = { @XmlNs(prefix = com.aawhere.xml.XmlNamespace.API_PREFIX,
		namespaceURI = com.aawhere.xml.XmlNamespace.API_VALUE) })
package com.aawhere.personalize;

import java.util.Locale;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import com.aawhere.personalize.xml.LocaleXmlAdapter;

