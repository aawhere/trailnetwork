/**
 *
 */
package com.aawhere.personalize;

import org.joda.time.DateTime;

/**
 * Abstract base {@link Personalizer} for personalizing {@link DateTime}.
 * 
 * @author brian
 * 
 */
public abstract class BaseDateTimePersonalizer<PersonalizedT extends PersonalizedBase>
		extends BasePersonalizer<PersonalizedT> {

	/**
	 * Used to construct all instances of BaseDateTimePersonalizer.
	 */
	/* @formatter:off */
	public static class Builder<BuilderT extends Builder<BuilderT, PlizerT>,
			PlizerT extends BaseDateTimePersonalizer<?>>
			extends BasePersonalizer.Builder<PlizerT,
					BuilderT> {
	/* @formatter:on */
		public Builder(PlizerT plizer) {
			super(plizer);
		}
	}

	/** Use {@link Builder} to construct BaseDateTimePersonalizer */
	protected BaseDateTimePersonalizer() {
		super();
	}

	protected PersonalizedT result;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#getResult()
	 */
	@Override
	public PersonalizedT getResult() {
		return result;
	}

	@Override
	public void personalize(Object context) {
		if (context instanceof DateTime) {
			personalizeDateTime((DateTime) context);
		} else {
			throw new NotHandledException(handlesType(), context);
		}
	}

	protected abstract void personalizeDateTime(DateTime context);

	public Class<DateTime> handlesType() {
		return DateTime.class;
	}

}