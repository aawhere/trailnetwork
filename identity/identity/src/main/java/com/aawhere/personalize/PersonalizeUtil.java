/**
 *
 */
package com.aawhere.personalize;

import java.util.Locale;

import javax.annotation.Nullable;
import javax.ws.rs.core.HttpHeaders;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.Configuration;

import com.aawhere.i18n.LocaleUtil;
import com.aawhere.lang.InvalidArgumentException;
import com.aawhere.lang.string.StringUtilsExtended;
import com.aawhere.person.pref.PrefUtil;
import com.aawhere.personalize.BasePersonalizer.Builder;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.MessageFormatter;
import com.aawhere.util.rb.ParamKey;

/**
 * Utilities for Personalization.
 * 
 * @author Brian Chapman
 * 
 */
public class PersonalizeUtil {

	public static final Locale DEFAULT = Locale.US;
	public static final String ACCEPT_I18N = "Accept-Localization";

	private PersonalizeUtil() {
		// Static fields only.
	}

	/**
	 * @deprecated use {@link CompleteMessage.Builder#paramWrapped(ParamKey, Object)}
	 * @param key
	 * @return
	 */
	@Deprecated
	public static String formatWrapperOpenTag(ParamKey key) {
		String keyString = StringUtilsExtended.constantCaseToHyphenated(key.toString());
		CompleteMessage wrapper = CompleteMessage.create(PersonalizeMessage.WRAPPER_OPEN_SPAN_TAG)
				.addParameter(PersonalizeMessage.Param.WRAPPER_KEY, keyString).build();
		return MessageFormatter.create().message(wrapper).locale(Locale.getDefault()).build().getFormattedMessage();
	}

	/**
	 * @deprecated use {@link CompleteMessage.Builder#paramWrapped(ParamKey, Object)}
	 * @return
	 */
	@Deprecated
	public static String formatWrapperCloseTag() {
		return MessageFormatter.create().message(PersonalizeMessage.WRAPPER_CLOSE_SPAN_TAG).locale(Locale.getDefault())
				.build().getFormattedMessage();
	}

	/**
	 * Provides the locale based on the HTTP header values for Accept-Language and
	 * Accept-Localization (which can have any value...if present localization happens).
	 * 
	 * 
	 * 
	 * @param requestHeaders
	 * @return the locale chosen or null indicating no locale.
	 */
	@Nullable
	public static Locale locale(HttpHeaders requestHeaders) {

		Boolean acceptLocalization = CollectionUtils.isNotEmpty(requestHeaders.getRequestHeader(ACCEPT_I18N));

		if (acceptLocalization) {
			Locale response = DEFAULT;
			if (requestHeaders != null && !requestHeaders.getAcceptableLanguages().isEmpty()) {
				response = requestHeaders.getAcceptableLanguages().get(0);
				try {
					// Will throw runtime exception if not valid
					LocaleUtil.validateLocale(response);
				} catch (Exception e) {
					response = DEFAULT;
				}
			}
			return LocaleUtil.convertFromAny(response, DEFAULT);
		} else {
			return null;
		}
	}

	/**
	 * A common place to indicate that localization is required to fullfill the request.
	 * 
	 */
	public static void throwLocalizationRequired() throws InvalidArgumentException {
		throw new InvalidArgumentException(CompleteMessage.create(PersonalizeMessage.LOCALIZATION_REQUIRED)
				.param(PersonalizeMessage.Param.HEADER_KEY, PersonalizeUtil.ACCEPT_I18N).build());
	}

	public static <P extends BasePersonalizer<?>> P personalizerDefault(Builder<P, ?> builder) {
		Configuration prefs = PrefUtil.getPreferences(Locale.getDefault(), PrefUtil.getDefaultSiPreferences());
		builder.setPreferences(prefs);
		P plizer = builder.build();
		return plizer;
	}
}
