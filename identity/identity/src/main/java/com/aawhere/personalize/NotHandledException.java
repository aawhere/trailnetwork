/**
 *
 */
package com.aawhere.personalize;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Indicates that the supplied argument could not be handled by the {@link Personalizer}.
 * 
 * @author Brian Chapman
 * 
 */
public class NotHandledException
		extends BaseRuntimeException {

	private static final long serialVersionUID = 4668121884762556649L;

	/* @formatter:off */
	public NotHandledException(Class<?> supportedType, Object object) {
		super(new CompleteMessage.Builder()
			.setMessage(PersonalizeMessage.NOT_HANDLED)
			.addParameter(PersonalizeMessage.Param.OBJECT, object.getClass().toString())
			.addParameter(PersonalizeMessage.Param.SUPPORTED_TYPE, supportedType.toString())
			.build());
	}
	/* @formatter:on */
}
