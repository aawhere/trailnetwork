/**
 *
 */
package com.aawhere.personalize;

import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import org.joda.time.DateTime;

import com.aawhere.xml.XmlNamespace;

/**
 * Localized data for {@link DateTime}.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.FIELD)
public class DateTimeDisplay
		extends PersonalizedBase {

	/**
	 * Local used to personalize DateTime.
	 */
	public Locale getLang() {
		return lang;
	}

	@XmlAttribute
	protected Locale lang;

	/**
	 * DateTime type e.g. datetime, instance, duration
	 */
	public String getType() {
		return type;
	}

	@XmlAttribute
	protected String type;

	/**
	 * DateTime formatted short. e.g.
	 * 
	 * {@literal
	 * shortDate(en_US):	1/20/12
	 * shortDate(fr_FR):	20/01/12
	 * shortDate(de_DE):	20.01.12
	 * shortDate(el_GR):	20/1/2012
	 * }
	 */
	public String getShortDate() {
		return shortDate;
	}

	@XmlElement
	protected String shortDate;

	/**
	 * DateTime formatted short. e.g.
	 * 
	 * {@literal
	 * shortDateTime(en_US):	1/20/12 3:13 PM
	 * shortDateTime(fr_FR):	20/01/12 15:13
	 * shortDateTime(de_DE):	20.01.12 15:13
	 * shortDateTime(el_GR):	20/1/2012 3:13 μμ
	 * }
	 */
	public String getShortDateTime() {
		return shortDateTime;
	}

	@XmlElement
	protected String shortDateTime;

	/**
	 * DateTime formatted long. e.g.
	 * 
	 * {@literal
	 * mediumDate(en_US):	Jan 20, 2012
	 * mediumDate(fr_FR):	20 janv. 2012
	 * mediumDate(de_DE):	20.01.2012
	 * mediumDate(el_GR):	20 Ιαν 2012
	 * }
	 */
	public String getMediumDate() {
		return mediumDate;
	}

	@XmlElement
	protected String mediumDate;

	/**
	 * DateTime formatted long. e.g.
	 * 
	 * {@literal
	 * mediumDateTime(en_US):	Jan 20, 2012 3:13:24 PM
	 * mediumDateTime(fr_FR):	20 janv. 2012 15:13:24
	 * mediumDateTime(de_DE):	20.01.2012 15:13:24
	 * mediumDateTime(el_GR):	20 Ιαν 2012 3:13:24 μμ
	 * }
	 */
	public String getMediumDateTime() {
		return mediumDateTime;
	}

	@XmlElement
	protected String mediumDateTime;

	/**
	 * DateTime formatted long. e.g.
	 * 
	 * {@literal
	 * fullTime(en_US):	3:13:24 PM PST
	 * fullTime(fr_FR):	15 h 13 PST
	 * fullTime(de_DE):	15:13 Uhr PST
	 * fullTime(el_GR):	3:13:24 μμ PST
	 * }
	 */
	public String getFullTime() {
		return fullTime;
	}

	@XmlElement
	protected String fullTime;

	/**
	 * DateTime formatted long. e.g.
	 * 
	 * {@literal
	 * mediumTime(en_US):	3:13:24 PM
	 * mediumTime(fr_FR):	15:13:24
	 * mediumTime(de_DE):	15:13:24
	 * mediumTime(el_GR):	3:13:24 μμ
	 * }
	 */
	public String getMediumTime() {
		return mediumTime;
	}

	@XmlElement
	protected String mediumTime;

	/**
	 * DateTime formatted in a sortable way. e.g.
	 * 
	 * {@literal
	 * 2012-01-20T14:11:04.670
	 * }
	 */
	public String getSortableDateTime() {
		return sortableDateTime;
	}

	@XmlElement
	protected String sortableDateTime;
}
