/**
 *
 */
package com.aawhere.personalize;

/**
 * Returns null, useful when you don't want the default XmlAdapter to return anything unless
 * configured with Preferences and Locale.
 * 
 * @author Brian Chapman
 * 
 */
public class DisabledPersonalizer
		extends BasePersonalizer {

	public static class Builder
			extends BasePersonalizer.Builder {

		public Builder() {
			super(new DisabledPersonalizer());
		}
	}// end Builder

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#personalize(java.lang.Object)
	 */
	@Override
	public void personalize(Object object) {
		this.result = null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#handlesType()
	 */
	@Override
	public Class<?> handlesType() {
		return Object.class;
	}

	@Override
	public Personalized getResult() {
		return result;
	}

}
