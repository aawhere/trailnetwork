/**
 * 
 */
package com.aawhere.personalize.xml;

import java.util.Locale;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Translates {@link Locale} to {@link XmlLang} for better representation in XML.
 * 
 * @author roller
 * 
 */
public class LocaleXmlAdapter
		extends XmlAdapter<XmlLang, Locale> {

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public XmlLang marshal(Locale locale) throws Exception {
		if (locale == null) {
			return null;
		}
		return XmlLang.create().locale(locale).build();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Locale unmarshal(XmlLang lang) throws Exception {
		if (lang == null) {
			return null;
		}
		return lang.toLocale();
	}

}
