/**
 *
 */
package com.aawhere.personalize;

import javax.measure.quantity.Quantity;

/**
 * Takes a {@link Quantity} and produces a {@link PersonalizedBase} with localized fields. Fields
 * are localized according to the {@link Preferences} setup in the {@link Builder}.
 * 
 * @author Brian Chapman
 * 
 */
public class QuantityDisplayPersonalizer
		extends BaseQuantityPersonalizer<QuantityDisplay> {

	/** Use {@link Builder} to construct QuantityDisplayPersonalizer */
	protected QuantityDisplayPersonalizer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#getResult()
	 */
	@Override
	public QuantityDisplay getResult() {
		return super.getResult();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.BasePersonalizer#personalize(java.lang.Object)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void personalize(Object context) {
		if (context instanceof Quantity) {
			personalizeQuantity((Quantity) context);
		} else {
			throw new NotHandledException(handlesType(), context);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.BasePersonalizer#personalize(java.lang.Object)
	 */
	private <Q extends Quantity<Q>> void personalizeQuantity(Q context) {
		QuantityDisplay.Builder builder = new QuantityDisplay.Builder();
		builder.setLang(getLocale());
		builder.setAbbreviatedUnit(personalizeUnit(context));
		builder.setFormatted(personalizeFormatted(context));
		builder.setFormattedValue(personalizeFormattedValue(context));
		builder.setValue(personalizeValue(context));
		result = builder.build();
	}

	/**
	 * Used to construct all instances of QuantityDisplayPersonalizer.
	 */
	/* @formatter:off */
	public static class Builder extends
	BaseQuantityPersonalizer.Builder<Builder,
	QuantityDisplayPersonalizer> {
		/* @formatter:on */
		public Builder() {
			super(new QuantityDisplayPersonalizer());
		}

	}// end Builder

}
