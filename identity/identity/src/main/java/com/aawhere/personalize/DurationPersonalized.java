/**
 *
 */
package com.aawhere.personalize;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.joda.time.xml.DurationXmlAdapter;
import com.aawhere.xml.XmlNamespace;

/**
 * The XML Representation of {@link Duration} for {@link DurationXmlAdapter} to marshal and
 * unmarshal.
 * 
 * @author Brian Chapman
 * 
 */
@XmlType(name = DurationPersonalized.API_NAME, namespace = XmlNamespace.API_VALUE)
public class DurationPersonalized
		extends PersonalizedBase {

	public static final String API_NAME = "Duration";

	/**
	 * The duration in XML format
	 */
	public Long getDuration() {
		return duration;
	}

	@XmlAttribute
	protected Long duration;

	// Required for JaxB, not required for java.
	@XmlElement(name = "display")
	public DurationDisplay getPersonalized() {
		return (DurationDisplay) personalized;
	}
}
