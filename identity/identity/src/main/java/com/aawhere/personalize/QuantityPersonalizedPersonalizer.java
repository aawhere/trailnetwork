/**
 *
 */
package com.aawhere.personalize;

import java.util.HashMap;
import java.util.Map;

import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.number.format.custom.DoubleFormat;
import com.aawhere.person.pref.PrefMeasureUtil;

/**
 * Takes a {@link Quantity} object and produces a {@link QuantityPersonalized} object with localized
 * fields. Fields are localized according to the preferences setup in the {@link Builder}.
 * 
 * @author Brian Chapman
 * 
 */
public class QuantityPersonalizedPersonalizer
		extends BaseQuantityPersonalizer<QuantityPersonalized> {

	private Map<Class<? extends Quantity<?>>, Unit<?>> unitMap = new HashMap<Class<? extends Quantity<?>>, Unit<?>>();

	/** Use {@link Builder} to construct QuantityPersonalizedPersonalizer */
	protected QuantityPersonalizedPersonalizer() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#getResult()
	 */
	@Override
	public QuantityPersonalized getResult() {
		return (QuantityPersonalized) super.getResult();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.BasePersonalizer#personalize(java.lang.Object)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void personalize(Object context) {
		if (context instanceof Quantity) {
			personalizeQuantity((Quantity) context);
		} else {
			throw new NotHandledException(handlesType(), context);
		}
	}

	public <Q extends Quantity<Q>> void personalizeQuantity(Q context) {
		QuantityPersonalized.Builder builder = new QuantityPersonalized.Builder();
		builder.setType(personalizeType(context));
		builder.setUnit(personalizeUnit(context));
		builder.setValue(personalizeValue(context));
		result = builder.build();
	}

	@Override
	protected <Q extends Quantity<Q>> Double personalizeValue(Q context) {
		// TODO: Consider creating a preference for this?
		// PrefMeasureUtil.getMaxDigitsFor(MeasurementUtil.classFor(context), prefs);
		DoubleFormat format = new DoubleFormat(5);
		return format.format(context.doubleValue(getUnit(context)));
	}

	@SuppressWarnings("unchecked")
	@Override
	protected <Q extends Quantity<Q>> Unit<Q> getUnit(Q context) {
		Class<Q> contextClass = MeasurementUtil.classFor(context);
		if (unitMap.containsKey(contextClass)) {
			return (Unit<Q>) unitMap.get(contextClass);
		} else {
			Unit<Q> unit = PrefMeasureUtil.getBaseUnitFor(MeasurementUtil.classFor(context), prefs);
			unitMap.put(contextClass, unit);
			return unit;
		}
	}

	/**
	 * Used to construct all instances of QuantityPersonalizedPersonalizer.
	 */
	/* @formatter:off */
	public static class Builder extends BaseQuantityPersonalizer.Builder<
				Builder,
				QuantityPersonalizedPersonalizer> {
		/* @formatter:on */
		public Builder() {
			super(new QuantityPersonalizedPersonalizer());
		}

	}// end Builder
}
