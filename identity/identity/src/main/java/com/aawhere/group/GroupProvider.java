/**
 * 
 */
package com.aawhere.group;

import com.aawhere.id.Identifier;
import com.aawhere.person.group.SocialGroup;

/**
 * @author aroller
 * 
 */
public interface GroupProvider {

	/**
	 * returns the group represented by the given groupid or if other is provided it is assumed to
	 * be the context.
	 * 
	 * @param contextOrGroupId
	 * @return
	 */
	SocialGroup group(Identifier<?, ?> contextOrGroupId);
}
