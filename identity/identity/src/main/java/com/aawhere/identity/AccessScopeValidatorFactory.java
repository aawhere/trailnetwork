/**
 * 
 */
package com.aawhere.identity;

import static com.aawhere.identity.AccessScope.*;

import java.util.Map;
import java.util.Set;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.Singleton;

/**
 * Provides {@link AccessScopeValidator} for a given {@link AccessScope}
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class AccessScopeValidatorFactory {

	private static Map<AccessScope, Set<IdentityRole>> scopeRoleMap = Maps.newHashMap();

	static {
		scopeRoleMap.put(UNRESTRICTED, Sets.newHashSet(IdentityRole.values()));
		scopeRoleMap.put(PUBLIC_READ, Sets.newHashSet(IdentityRole.developer, IdentityRole.admin));
		scopeRoleMap.put(OWNER_READ, Sets.newHashSet(IdentityRole.admin));
		scopeRoleMap.put(OWNER_WRITE, Sets.newHashSet(IdentityRole.admin));
	}

	private Map<AccessScope, AccessScopeValidator> validatorMap = Maps.newHashMap();

	public AccessScopeValidatorFactory() {
		// fill up validatorMap.
		validatorMap.put(UNRESTRICTED, new UnrestrictedAccessScopeValidator(UNRESTRICTED));
		validatorMap.put(	PUBLIC_READ,
							CompositeSharedAccessScopeValidator.create()
									.addValidator(new IdentitySharedAccessScopeValidator(PUBLIC_READ))
									.addValidator(new IsRolePermittedAccessScopeValidator(PUBLIC_READ))
									.addValidator(new IsOwnerAccessScopeValidator(PUBLIC_READ)).build());
		validatorMap.put(	OWNER_READ,
							CompositeSharedAccessScopeValidator.create()
									.addValidator(new IsRolePermittedAccessScopeValidator(OWNER_READ))
									.addValidator(new IsOwnerAccessScopeValidator(OWNER_READ)).build());
		validatorMap.put(	OWNER_WRITE,
							CompositeSharedAccessScopeValidator.create()
									.addValidator(new IsRolePermittedAccessScopeValidator(OWNER_WRITE))
									.addValidator(new IsOwnerAccessScopeValidator(OWNER_WRITE)).build());
	}

	public AccessScopeValidator validator(AccessScope scope) {
		if (validatorMap.containsKey(scope)) {
			return validatorMap.get(scope);
		} else {
			throw new IllegalArgumentException("Access scope not found.");
		}
	}

	private static abstract class BaseAccessScopeValidator
			implements AccessScopeValidator {

		@Override
		public Boolean handles(AccessScope scope) {
			return handles().equals(scope);
		}
	}

	private static class UnrestrictedAccessScopeValidator
			extends BaseAccessScopeValidator {

		private AccessScope scope;

		UnrestrictedAccessScopeValidator(AccessScope scope) {
			this.scope = scope;
		}

		@Override
		public Boolean isPermitted(IdentityManager identityManager, Identity owner) {
			return true;
		}

		@Override
		public AccessScope handles() {
			return scope;
		}

	}

	private static class CompositeSharedAccessScopeValidator
			implements AccessScopeValidator {

		/**
		 * Used to construct all instances of
		 * AccessScopeValidatorFactory.CompositeSharedAccessScopeValidator.
		 */
		static class Builder
				extends ObjectBuilder<CompositeSharedAccessScopeValidator> {

			public Builder() {
				super(new CompositeSharedAccessScopeValidator());
			}

			public Builder addValidator(AccessScopeValidator validator) {
				building.validators.add(validator);
				return this;
			}

			/*
			 * (non-Javadoc)
			 * @see com.aawhere.lang.ObjectBuilder#validate()
			 */
			@Override
			protected void validate() {
				Assertion.assertNotEmpty(building.validators);
			}

			public CompositeSharedAccessScopeValidator build() {
				CompositeSharedAccessScopeValidator built = super.build();
				Set<AccessScope> scopes = Sets.newHashSet();
				for (AccessScopeValidator v : built.validators) {
					scopes.add(v.handles());
				}
				// Only one access scope is allowed.
				Assertion.e().assertTrue(scopes.size() == 1);
				built.scope = scopes.iterator().next();

				return built;
			}

		}// end Builder

		public static Builder create() {
			return new Builder();
		}

		// LinkedHashSet preserves iteration order, which is the same as the insertion order.
		private Set<AccessScopeValidator> validators = Sets.newLinkedHashSet();
		private AccessScope scope;

		/**
		 * Use {@link Builder} to construct
		 * AccessScopeValidatorFactory.CompositeSharedAccessScopeValidator
		 */
		private CompositeSharedAccessScopeValidator() {
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * com.aawhere.identity.AccessScopeValidator#isPermitted(com.aawhere.identity.IdentityManager
		 * , com.aawhere.identity.Identity)
		 */
		@Override
		public Boolean isPermitted(IdentityManager identityManager, Identity owner) {
			Boolean permitted = false;
			for (AccessScopeValidator v : validators) {
				if (v.isPermitted(identityManager, owner)) {
					permitted = true;
					break;
				}
			}
			return permitted;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.identity.AccessScopeValidator#handles(com.aawhere.identity.AccessScope)
		 */
		@Override
		public Boolean handles(AccessScope scope) {
			return scope.equals(scope);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.identity.AccessScopeValidator#handles()
		 */
		@Override
		public AccessScope handles() {
			return scope;
		}

	}

	private static class IdentitySharedAccessScopeValidator
			extends BaseAccessScopeValidator {

		private AccessScope scope;

		IdentitySharedAccessScopeValidator(AccessScope scope) {
			this.scope = scope;
		}

		@Override
		public Boolean isPermitted(IdentityManager identityManager, Identity owner) {
			return owner.identityShared();
		}

		@Override
		public AccessScope handles() {
			return scope;
		}
	}

	private static class IsRolePermittedAccessScopeValidator
			extends BaseAccessScopeValidator {

		private AccessScope scope;

		IsRolePermittedAccessScopeValidator(AccessScope scope) {
			this.scope = scope;
		}

		@Override
		public Boolean isPermitted(IdentityManager identityManager, Identity subject) {
			Boolean permitted = false;
			for (IdentityRole role : scopeRoleMap.get(handles())) {
				if (identityManager.hasRole(role)) {
					permitted = true;
				}
			}
			return permitted;
		}

		@Override
		public AccessScope handles() {
			return scope;
		}
	}

	private static class IsOwnerAccessScopeValidator
			extends BaseAccessScopeValidator {

		private AccessScope scope;

		IsOwnerAccessScopeValidator(AccessScope scope) {
			this.scope = scope;
		}

		@Override
		public Boolean isPermitted(IdentityManager identityManager, Identity subject) {
			return identityManager.hasPerson() && subject.isOwner(identityManager.person());
		}

		@Override
		public AccessScope handles() {
			return scope;
		}
	}

}
