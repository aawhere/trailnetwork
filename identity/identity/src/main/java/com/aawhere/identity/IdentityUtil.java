package com.aawhere.identity;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicates;

public class IdentityUtil {

	public static Function<IdentityRole, Boolean> identityRoleFunction(IdentityRole role) {
		return Functions.forPredicate(Predicates.equalTo(role));
	}

	public static IdentityRoleValidator adminIdentityRoleValidator() {
		return new IdentityRoleFunctionValidator(identityRoleFunction(IdentityRole.admin));
	}

	public static IdentityRoleValidator identityRoleValidator(IdentityRole role) {
		return new IdentityRoleFunctionValidator(identityRoleFunction(role));
	}

	/**
	 * Function allowing bulk transformation of an entity
	 * 
	 * @param identityManager
	 * @return
	 */
	public static <T> Function<T, T> enforceAccessFunction(final IdentityManager identityManager) {
		return new Function<T, T>() {

			@Override
			public T apply(T input) {
				return (input == null) ? identityManager.enforceAccess(input) : null;
			}
		};
	}
}
