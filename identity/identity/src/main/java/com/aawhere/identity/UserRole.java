package com.aawhere.identity;

public enum UserRole {

	/**
	 * A super user allowed access to see and do anything possible.
	 * 
	 */
	ADMIN;
}
