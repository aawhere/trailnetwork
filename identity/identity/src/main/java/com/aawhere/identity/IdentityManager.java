/**
 *
 */
package com.aawhere.identity;

import java.lang.reflect.AnnotatedElement;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.configuration.Configuration;
import org.reflections.ReflectionUtils;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.Person;
import com.aawhere.person.pref.PrefUtil;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.inject.servlet.RequestScoped;

/**
 * Represents the identity of the current request.
 * 
 * accountId: The account of the client making the request personId: the person the client is making
 * the request on behalf of
 * 
 * Request scoped
 * 
 * @author Brian Chapman
 * 
 */
@RequestScoped
public class IdentityManager {

	/**
	 * Used to construct all instances of IdentityManager.
	 */
	public static class Builder
			extends ObjectBuilder<IdentityManager> {

		public Builder() {
			super(new IdentityManager());
		}

		public Builder person(Person person) {
			building.person = person;
			return this;
		}

		public Builder locale(Locale locale) {
			building.locale = locale;
			return this;
		}

		/** @see #identityRoleValidator */
		public Builder identityRoleValidator(IdentityRoleValidator identityRoleValidator) {
			building.identityRoleValidator = identityRoleValidator;
			return this;
		}

		public Builder accessScopeValidatorFactory(AccessScopeValidatorFactory factory) {
			building.accessScopeValidatorFactory = factory;
			return this;
		}

		public Builder preferences(Configuration preferences) {
			building.preferences = preferences;
			return this;
		}

		/** @see #identityFactory */
		public Builder identityFactory(IdentityFactory identityFactory) {
			building.identityFactory = identityFactory;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("locale", building.locale);
			Assertion.assertNotNull("AcccessScopeValidatorFactory", building.accessScopeValidatorFactory);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public IdentityManager build() {
			if (building.preferences == null) {
				building.preferences = PrefUtil.createPrefBuilder().locale(building.locale).build();
			}
			// the default is to not allow specific access to any role unless something can provide
			// proof otherwise.
			if (building.identityRoleValidator == null) {
				building.identityRoleValidator = new IdentityRoleFunctionValidator(Functions.constant(Boolean.FALSE));
			}
			return super.build();
		}

	}// end Builder

	/** Use {@link Builder} to construct IdentityManager */
	private IdentityManager() {
	}

	public static Builder create() {
		return new Builder();
	}

	/** the person who is logged into the system. */
	private Person person;
	private Configuration preferences;
	private Locale locale;
	private IdentityRoleValidator identityRoleValidator;
	private IdentityFactory identityFactory;
	private AccessScopeValidatorFactory accessScopeValidatorFactory;

	/**
	 * @return the person
	 * @throws EntityNotFoundException
	 */
	public Person person() {
		return person;
	}

	/**
	 * @return
	 */
	public boolean hasPerson() {
		return person != null;
	}

	public Locale locale() {
		return locale;
	}

	/**
	 * The preferences provided when building or the default preferences based on the locale if not
	 * provided.
	 * 
	 * @return
	 */
	public Configuration preferences() {
		return preferences;
	}

	public Boolean hasRole(IdentityRole role) {
		return identityRoleValidator.isUserInRole(role);
	}

	/**
	 * When interested if some information is accessible by the {@link #person()} this accepts the
	 * owner of the information and the required scope.
	 * 
	 * @param owner
	 *            the owner of the data being viewed. This allows null as a courtesy to admins, but
	 *            if that's a problem then remove because the owner is important to answer this
	 *            question
	 * @param scope
	 *            the scope of the data being accessed
	 * @return
	 */
	public Boolean hasAccess(@Nullable Identity owner, @Nonnull AccessScope scope) {
		Boolean permitted = accessScopeValidatorFactory.validator(scope).isPermitted(this, owner);
		return permitted;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Identity(Person: " + person() + ")";
	}

	/**
	 * Given any entity this will mutate the entity for those fields declaring
	 * {@link AccessControlled} by providing null if {@link #hasAccess(Identity, AccessScope)} does
	 * not return true for the {@link AccessControlled#scope()} given.
	 * 
	 * The entity must be able to provide it's {@link Identity}.
	 * 
	 * @param entity
	 * @param identityFactory
	 * @return
	 */
	public <T> T enforceAccess(T entity) {
		// TODO: handle class level scope
		if (isAccessControlled(entity)) {
			Identity identity = identityFactory.identity(entity);

			Predicate<AnnotatedElement> withAnnotation = ReflectionUtils.withAnnotation(AccessControlled.class);
			@SuppressWarnings("unchecked")
			Set<java.lang.reflect.Field> accessControlledFields = ReflectionUtils.getAllFields(	entity.getClass(),
																								withAnnotation);
			for (java.lang.reflect.Field field : accessControlledFields) {
				AccessControlled accessControlled = field.getAnnotation(AccessControlled.class);
				AccessScope scope = accessControlled.scope();
				if (!hasAccess(identity, scope)) {
					field.setAccessible(true);
					try {
						field.set(entity, null);
					} catch (IllegalArgumentException | IllegalAccessException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		return entity;

	}

	/**
	 * Encorces Access to each entityin the iterable using
	 * {@link IdentityUtil#enforceAccessFunction(IdentityManager)} so the client must iterate the
	 * result or mutation will not happen.
	 * 
	 * @see #enforceAccessNow(Iterable)
	 * @param entities
	 * @return
	 */
	public <T> Iterable<T> enforceAccess(Iterable<T> entities) {
		return Iterables.transform(entities, IdentityUtil.<T> enforceAccessFunction(this));
	}

	/**
	 * Calls {@link #enforceAccess(Iterable)}, but applies iteration now instead of using it's lazy
	 * iteration
	 * 
	 * @param entities
	 */
	public <T> void enforceAccessNow(Iterable<T> entities) {
		for (T t : entities) {
			enforceAccess(t);
		}

	}

	public static Boolean isAccessControlled(Object object) {
		return object != null && isAccessControlled(object.getClass());
	}

	public static Boolean isAccessControlled(Class<?> type) {
		return type.getAnnotation(AccessControlled.class) != null;
	}

}
