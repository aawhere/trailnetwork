/**
 * 
 */
package com.aawhere.identity;

import javax.annotation.Nullable;

import org.apache.commons.lang3.BooleanUtils;

/**
 * @author aroller
 * 
 */
public class IdentityPrivacyUtil {

	/**
	 * Provides the standard response when identifying information is to be withheld.
	 * 
	 * @param identityShared
	 * @param personalInformation
	 * @return the personalInformation given only if identityShared is true, otherwise null.
	 */
	@Nullable
	public static <T> T filterIdentity(Boolean identityShared, T personalInformation) {
		return filterIdentity(identityShared, personalInformation, null);
	}

	public static <T> T filterIdentity(@Nullable Boolean identityShared, @Nullable T personalInformation,
			@Nullable T defaultValue) {
		return (BooleanUtils.isTrue(identityShared)) ? personalInformation : defaultValue;
	}

}
