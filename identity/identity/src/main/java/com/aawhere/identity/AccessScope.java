package com.aawhere.identity;

import com.aawhere.person.Person;

/**
 * Written in the spirit of OAuth Scopes, this is an enumeration that all classes should use to
 * generalize privacy permissions of a specific data item into a broad category of access.
 * 
 * These scopes should work in terms of {@link Identity} and will be used by the
 * {@link IdentityManager} to indicate if access is allowed.
 * 
 * Terms that apply:
 * <ul>
 * <li>Read - Viewable</li>
 * <li>Write - Modifiable (view,update, create, delete); Write automatically implies read.</li>
 * <li>Public - Shared with all viewers of the site, authenticated or not</li>
 * <li>Possession - The {@link Identity} is the</li>
 * </ul>
 * 
 * Access identifies two roles. The owner of the information (which applies to all types of
 * information) and the {@link Person} requesting access to that information.
 * 
 * @author aroller
 * 
 */
public enum AccessScope {

	/** Indicates there are no restrictions placed on this information */
	UNRESTRICTED,

	/**
	 * Information that may be shared with the general public. Name and photo is a good example.
	 * 
	 * This corresponds with {@link Identity#identityShared()} where the {@link Identity} is of the
	 * person who's information is being protected.
	 */
	PUBLIC_READ,

	/**
	 * Private information is for the {@link Identity} owners as well as those keeping this system
	 * running. Passwords, email are examples.
	 */
	OWNER_READ,

	/**
	 * Creation of modification of information that belongs to an {@link Identity}. This may be
	 * private or public information. Write access automatically implies the ability to view it.
	 * 
	 */
	OWNER_WRITE,

}
