/**
 *
 */
package com.aawhere.identity;

import com.aawhere.person.Person;

/**
 * A specific type of entity that is used to identify information about a person, place, group.
 * 
 * @author aroller
 * 
 */
public interface Identity {

	/**
	 * Indicates the information protected by {@link AccessScope#PUBLIC_READ} is available or not.
	 * 
	 * @return
	 */
	public Boolean identityShared();

	/**
	 * delegates responsibility of the {@link Identity} to verify if the given id is the owner of
	 * the identified information allowing for multiple owners as defined by the {@link Identity}
	 * implementation.
	 * 
	 * @param requestingAccess
	 * @return
	 */
	public Boolean isOwner(Person requestingAccess);
}
