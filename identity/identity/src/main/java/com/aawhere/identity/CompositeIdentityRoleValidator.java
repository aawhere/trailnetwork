/**
 * 
 */
package com.aawhere.identity;

import java.util.Set;

import com.aawhere.lang.ObjectBuilder;
import com.google.common.collect.Sets;

/**
 * Allows composing {@link IdentityRoleValidator} into one IdentityRoleValidator. A User is
 * considered in a role if any of the validators returns true. Otherwise the user is not considered
 * in a role.
 * 
 * @author Brian Chapman
 * 
 */
public class CompositeIdentityRoleValidator
		implements IdentityRoleValidator {

	/**
	 * Used to construct all instances of CompositeIdentityRoleValidator.
	 */
	public static class Builder
			extends ObjectBuilder<CompositeIdentityRoleValidator> {

		public Builder() {
			super(new CompositeIdentityRoleValidator());
		}

		public Builder addValidator(IdentityRoleValidator validator) {
			this.building.validators.add(validator);
			return this;
		}

		public CompositeIdentityRoleValidator build() {
			CompositeIdentityRoleValidator built = super.build();
			return built;
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct CompositeIdentityRoleValidator */
	private CompositeIdentityRoleValidator() {
	}

	private Set<IdentityRoleValidator> validators = Sets.newHashSet();

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.identity.IdentityRoleValidator#isUserInRole(com.aawhere.identity.IdentityRole)
	 */
	@Override
	public Boolean isUserInRole(IdentityRole role) {
		Boolean inRole = false;
		for (IdentityRoleValidator validator : validators) {
			if (validator.isUserInRole(role)) {
				inRole = true;
				break;
			}
		}
		return inRole;
	}

}
