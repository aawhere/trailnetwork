/**
 * 
 */
package com.aawhere.identity;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.person.Person;

/**
 * Validator that checks if the person has the role.
 * 
 * @author Brian Chapman
 * 
 */
public class PersonIdentityRoleValidator
		implements IdentityRoleValidator {

	/**
	 * Used to construct all instances of PersonIdentityRoleValidator.
	 */
	public static class Builder
			extends ObjectBuilder<PersonIdentityRoleValidator> {

		public Builder() {
			super(new PersonIdentityRoleValidator());
		}

		public Builder person(Person person) {
			this.building.person = person;
			return this;
		}

		public PersonIdentityRoleValidator build() {
			PersonIdentityRoleValidator built = super.build();
			return built;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("person", building.person);

		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct PersonIdentityRoleValidator */
	private PersonIdentityRoleValidator() {
	}

	private Person person;

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.identity.IdentityRoleValidator#isUserInRole(com.aawhere.identity.IdentityRole)
	 */
	@Override
	public Boolean isUserInRole(IdentityRole role) {
		return person.identityRoles().contains(role);
	}

}
