/**
 * 
 */
package com.aawhere.identity;

/**
 * Validates that an {@link Identity} has access to an {@link AccessScope}
 * 
 * @author Brian Chapman
 * 
 */
public interface AccessScopeValidator {

	/**
	 * Indicates true if the identity identified in the IdentityManager is permitted to access the
	 * AccessScope identified in {@link #handles(AccessScope)}.
	 * 
	 * Since people may choose to have their identity shared publically or not, the owner of the
	 * identifiable information is provided along with the current IdentityManager as some
	 * {@link IdentityRole}s don't permit viewing non-publically visible information.
	 * 
	 * @param role
	 * @return true only if the authenticated user is in the role provided.
	 */
	public Boolean isPermitted(IdentityManager identityManager, Identity owner);

	public Boolean handles(AccessScope scope);

	public AccessScope handles();
}
