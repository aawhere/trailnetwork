package com.aawhere.identity;

import com.aawhere.app.account.Account;
import com.aawhere.person.AccountIdentity;
import com.aawhere.person.PersonAccountProvider;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Given any entity this will give {@link Identity} related to the entity...if possible.
 * 
 * FIXME: this hides the details from the users that need identity, but the implementation will
 * quickly get coupled if identity spreads.
 * 
 * @author aroller
 * 
 */
@Singleton
public class IdentityFactory {

	private PersonAccountProvider personAccountProvider;

	@Inject
	public IdentityFactory(PersonAccountProvider personAccountProvider) {
		this.personAccountProvider = personAccountProvider;
	}

	public Identity identity(Object entity) {
		if (entity instanceof Identity) {
			return (Identity) entity;
		} else if (entity instanceof Account) {
			return AccountIdentity.create().accountId(((Account) entity).id()).provider(personAccountProvider).build();
		} else {
			// TODO:scan the entity for fields that are person id or account id
			// TODO:make another interface Owned that indicates the owner of the entity which would
			// be person or account?
			throw new IllegalArgumentException(entity + " provided, but we don't know how to provide identity for "
					+ entity.getClass());
		}

	}
}
