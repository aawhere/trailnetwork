package com.aawhere.identity;

import com.google.common.base.Function;
import com.google.common.base.Functions;

/**
 * Super simple validator that delegates the answer to a function allowing for re-use of functions
 * if available and also useful for testing or providing constants
 * {@link Functions#constant(Object)}
 * 
 * @author aroller
 * 
 */
public class IdentityRoleFunctionValidator
		implements IdentityRoleValidator {

	private Function<? super IdentityRole, Boolean> function;

	public IdentityRoleFunctionValidator(Function<? super IdentityRole, Boolean> function) {
		this.function = function;
	}

	@Override
	public Boolean isUserInRole(IdentityRole role) {
		return function.apply(role);
	}

}
