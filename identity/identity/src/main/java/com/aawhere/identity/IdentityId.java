/**
 *
 */
package com.aawhere.identity;

import com.aawhere.id.Identifier;

/**
 * The {@link Identifier} for an {@link Identity}.
 * 
 * @author aroller
 * 
 */
public interface IdentityId {

}
