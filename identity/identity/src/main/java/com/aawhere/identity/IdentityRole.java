package com.aawhere.identity;

import com.aawhere.app.Application;
import com.aawhere.person.Person;

/**
 * Various roles used by {@link Person} and {@link Application} clients. This provides access
 * control in conjunction with {@link IdentityRoleValidator} and {@link IdentityManager}. This
 * breaks naming convention for convenience to match those values declared in {@link Name} to take
 * advantage of {@link #valueOf(String)}, {@link #toString()}, etc.
 * 
 * @author aroller
 * 
 */
public enum IdentityRole {
	/** A generic customer with no particular priveleges, but indicates authentication has happened */
	user,
	/** A developer can see people's identity, but not modify them */
	developer,
	/** The super user allowed to do anything. */
	admin,
	/**
	 * A client that has special privileges to perform actions on the system (like create accounts,
	 * manage passwords, etc).
	 * 
	 */
	trustedApp;

	/**
	 * Static string values available for use in annotations.
	 * 
	 * @author aroller
	 * 
	 */
	public static final class Name {

		public static final String USER = "user";
		public static final String DEVELOPER = "developer";
		/**
		 * Appengine automatically assigns this role to logged in admins of appspot.com. On the dev
		 * server you can use localhost:8080/_ah/login to choose between admin and standard user.
		 */
		public static final String ADMIN = "admin";
		/**
		 * 
		 */
		public static final String TRUSTED_APP = "trustedApp";
	}

}
