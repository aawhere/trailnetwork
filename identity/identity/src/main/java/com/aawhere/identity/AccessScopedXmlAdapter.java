package com.aawhere.identity;

import javax.annotation.Nullable;
import javax.xml.bind.JAXB;

import com.aawhere.xml.bind.OptionalXmlAdapter;
import com.google.common.base.Function;

/**
 * 
 * @author aroller
 * 
 * @param <X>
 *            the {@link JAXB} ready object
 * @param <F>
 *            the object that may provide access to the X AND provide the {@link Identity} that will
 *            indicate ownership.
 */
public abstract class AccessScopedXmlAdapter<X, F>
		extends OptionalXmlAdapter<X, F> {

	final private IdentityManager authenticatedUser;
	final private AccessScope scope;
	final private Function<F, X> xmlFunction;

	public AccessScopedXmlAdapter() {
		this(null, null, null, null);

	}

	/**
	 * Shows the xml using the identity manager if appropriate for the scope.
	 * 
	 * @param identityManager
	 * @param scope
	 */
	public AccessScopedXmlAdapter(IdentityManager identityManager, AccessScope scope) {
		this(Visibility.SHOW, identityManager, scope, null);
	}

	/**
	 * Shows the xml from the function using the identity manager if appropriate for the scope
	 * 
	 * @param identityManager
	 * @param scope
	 * @param xmlFunction
	 */
	public AccessScopedXmlAdapter(IdentityManager identityManager, AccessScope scope, Function<F, X> xmlFunction) {
		this(Visibility.SHOW, identityManager, scope, xmlFunction);
	}

	public AccessScopedXmlAdapter(Visibility visibility, IdentityManager identityManager, AccessScope scope,
			Function<F, X> xmlFunction) {
		super(visibility);
		this.authenticatedUser = identityManager;
		this.scope = scope;
		this.xmlFunction = xmlFunction;
	}

	@Override
	final protected X showMarshal(F from) throws Exception {
		X result = null;
		if (this.authenticatedUser != null) {
			@Nullable
			Identity owner = owner(from);
			if (this.authenticatedUser.hasAccess(owner, scope)) {
				result = xml(from);
			} else {
				result = accessDenied(from);
			}
		}
		return result;
	}

	/**
	 * The typical role of {@link #marshal(Object)}, but conditionally called only if access is
	 * allowed.
	 * 
	 * @param from
	 * @return
	 */
	protected X xml(F from) {
		if (this.xmlFunction == null) {
			throw new UnsupportedOperationException("override this method or provide an xmlFunction");
		}
		return this.xmlFunction.apply(from);
	}

	/**
	 * This provides the owner of the data that will be exposed in {@link #xml(Object)} if the
	 * {@link IdentityManager#hasAccess(Identity, AccessScope)}.
	 * 
	 * @param from
	 * @return the owner of the data being adapted or null if the owner can not be provided
	 */
	protected Identity owner(F from) {
		// if you are here because of a ClassCastException...you must override and provide the
		// identity because the argument given cannot
		return (Identity) from;
	}

	/**
	 * indicates that access is denied by returning a subsitute for X. the default is null, override
	 * if something else is desired.
	 * 
	 * @param from
	 * @return null or any other item that properly represents the information is not able to be
	 *         exposed.
	 */
	@Nullable
	protected X accessDenied(F from) {
		return null;
	}

}
