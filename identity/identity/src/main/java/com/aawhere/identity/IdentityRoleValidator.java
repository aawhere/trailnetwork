package com.aawhere.identity;

public interface IdentityRoleValidator {

	/**
	 * Indicates true if the user is in a particular role. This corresponds with
	 * HttpServletRequest#isUserInRole and OauthService.isAdmin().
	 * 
	 * Since users may have multiple roles assigned to them, there is no logic applied to user roles
	 * in this validator. For example, an {@link IdentityRole#admin} is also a
	 * {@link IdentityRole#user}, but the client need only provide the minimum role requred for the
	 * access since the authenticated user that is an admin is also a user so this must return true
	 * if either role is provided.
	 * 
	 * @param role
	 * @return true only if the authenticated user is in the role provided.
	 */
	public Boolean isUserInRole(IdentityRole role);
}
