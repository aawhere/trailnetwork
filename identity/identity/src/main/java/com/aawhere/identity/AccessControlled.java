package com.aawhere.identity;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Indicates the scope that data should be restricted to be shown to users. If the annotation is at
 * the class level then it is the default for all fields in the class. If the annotation is at the
 * field level it overrides the default for that field (not yet implment 2015-02-26.
 * 
 * TODO: Implement class level access control
 * 
 * TODO:support methods?
 * 
 * @author aroller
 * 
 */
@Retention(RUNTIME)
@Target({ TYPE, FIELD })
public @interface AccessControlled {

	/**
	 * Required to identity which {@link AccessScope} should be allowed access when
	 * {@link IdentityManager#hasAccess(Identity, AccessScope)} returns true.
	 * 
	 * @return the scope allowed for access.
	 */
	AccessScope scope();
}
