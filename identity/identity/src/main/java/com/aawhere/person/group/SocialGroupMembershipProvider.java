/**
 * 
 */
package com.aawhere.person.group;

import com.aawhere.app.account.AccountId;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.PersonId;

/**
 * @author aroller
 * 
 */
public interface SocialGroupMembershipProvider {

	SocialGroupMembership membership(SocialGroupId groupId, PersonId personId) throws EntityNotFoundException;

	SocialGroupMembership membership(SocialGroupId groupId, AccountId accountId) throws EntityNotFoundException;

}
