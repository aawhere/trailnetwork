/**
 * 
 */
package com.aawhere.person.group;

import com.aawhere.persist.ChildRepository;
import com.aawhere.persist.CompleteRepository;

/**
 * @author aroller
 * 
 */
public interface SocialGroupMembershipRepository
		extends CompleteRepository<SocialGroupMemberships, SocialGroupMembership, SocialGroupMembershipId>,
		ChildRepository<SocialGroupMembership, SocialGroupMembershipId> {

}
