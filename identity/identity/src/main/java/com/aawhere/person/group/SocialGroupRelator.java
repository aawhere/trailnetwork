/**
 * 
 */
package com.aawhere.person.group;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.If;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.log.LoggerFactory;
import com.aawhere.person.PersonId;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultiset;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.collect.Sets;

/**
 * Relates people finding common the same people in multiple groups. The result being sorted by the
 * "best" groups which is a combination of score, the number of people that have a group in common,
 * the number of people that
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class SocialGroupRelator {

	/**
	 * The social group that is of interest. This is used to remove results that are obvious such as
	 * relating itself or others of the same context.
	 */
	@XmlElement
	private SocialGroup socialGroupTargeted;

	/**
	 * Groups each social group by it's category since they all may serve a very different purpose,
	 * but still be valuable. The groups are in order of recommendation. If a group has the same
	 * context it will be found in {@link #groupsWithSameContext} instead.
	 */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private ListMultimap<String, SocialGroup> groupsByCategory;
	/**
	 * separates all groups sharing the same context as the target into a separate category since it
	 * is likely an abvious relation that the relation exists, however, seeing the separate
	 * categories may be helpful. These are in order of best match.
	 */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private List<SocialGroup> groupsWithSameContext;

	/**
	 * for the group id this provides the number of persons in common and the score used to
	 * determine the order in {@link #groups}.
	 */
	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private LinkedHashMap<SocialGroupId, Pair<Integer, Integer>> counts;

	/**
	 * Affects the choices made during discovery and processing that will affect efficiency,
	 * accuracy resulting in different results.
	 * 
	 */
	@XmlElement
	private Options options;

	/**
	 * Used to construct all instances of SocialGroupRelator.
	 */
	@XmlTransient
	public static class Builder
			extends ObjectBuilder<SocialGroupRelator> {

		private Set<PersonId> personIds;
		/** given the ids this returns the corresponding group. */
		private Function<Iterable<SocialGroupId>, Map<SocialGroupId, SocialGroup>> groupProvider;
		/** given social group ids this should return those group ids the person is a member. */
		private Function<PersonId, ? extends Iterable<SocialGroupId>> groupIdsForPersonProvider;

		public Builder() {
			super(new SocialGroupRelator());
		}

		public Builder personIds(Iterable<PersonId> personIds) {
			this.personIds = SetUtilsExtended.asSet(personIds);
			return this;
		}

		public Builder groupProvider(Function<Iterable<SocialGroupId>, Map<SocialGroupId, SocialGroup>> function) {
			this.groupProvider = function;
			return this;
		}

		public Builder groupIdsForPersonProvider(
				Function<PersonId, ? extends Iterable<SocialGroupId>> groupIdsForPersonProvider) {
			this.groupIdsForPersonProvider = groupIdsForPersonProvider;
			return this;
		}

		public Builder options(@Nullable Options options) {
			if (options != null) {
				building.options = options.init();
			}
			return this;
		}

		public Builder socialGroupTargeted(SocialGroup socialGroupTargeted) {
			building.socialGroupTargeted = socialGroupTargeted;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("groupProvider", this.groupProvider);
			Assertion.exceptions().notNull("groupIdsForPersonProvider", this.groupIdsForPersonProvider);
			Assertion.exceptions().notNull("personIds", this.personIds);
			Assertion.exceptions().notNull("socialGroupTargeted", building.socialGroupTargeted);

		}

		@Override
		public SocialGroupRelator build() {
			SocialGroupRelator built = super.build();
			// FIXME:break this big method up into smaller chunks for better readibility
			built.groupsByCategory = LinkedListMultimap.create();
			built.groupsWithSameContext = Lists.newArrayList();
			built.counts = new LinkedHashMap<>();
			if (built.options == null) {
				built.options = Options.create();
			}
			Iterable<SocialGroupId> socialGroupIdsForAllPersons = Collections.emptyList();
			for (PersonId personId : personIds) {
				// this concatenation allows asynchronous work to be done
				socialGroupIdsForAllPersons = Iterables.concat(	socialGroupIdsForAllPersons,
																groupIdsForPersonProvider.apply(personId));
			}

			// don't include the targeted group since that is obviously related
			socialGroupIdsForAllPersons = Iterables.filter(socialGroupIdsForAllPersons, Predicates.not(Predicates
					.equalTo(built.socialGroupTargeted.id())));
			// every person that visits a group will provide that group id
			// this counts every occurrence of each group,
			// then provides the highest occurrence first
			ImmutableMultiset<SocialGroupId> inCommon = Multisets.copyHighestCountFirst(HashMultiset
					.create(socialGroupIdsForAllPersons));
			if (!inCommon.isEmpty()) {
				Iterable<SocialGroup> socialGroups = this.groupProvider.apply(inCommon.elementSet()).values();

				// require score and count
				socialGroups = SocialGroupUtil.scoreRequired(socialGroups);
				socialGroups = SocialGroupUtil.countRequired(socialGroups);

				List<SocialGroup> socialGroupsHighestScoreFirst = SocialGroupUtil.SCORE_DESCENDING
						.sortedCopy(socialGroups);

				// we've identified the most popular in visits, but what about score?
				// we must consult the group.
				// the most in common may be members of a low scoring group.
				// we must normalize the count and score adding them to make a combined weighted
				// score
				//

				Map<String, Integer> inCommonCountHighs = inCommonCountHighs(socialGroups, inCommon);
				Map<String, Integer> groupScoreHighs = Maps.newHashMap();

				// this ensures the integer math is high enough to result in integers
				int weightedScoreFactor = 100;
				LinkedHashMultiset<SocialGroup> groupsWeightedScore = LinkedHashMultiset.create();

				// calculate the weighted score for each
				for (SocialGroup socialGroup : socialGroupsHighestScoreFirst) {
					int countInCommon = inCommon.count(socialGroup.id());

					if (countInCommon >= built.options.inCommonMinimum) {
						// high scores are relative to similar categories
						// the first score for each category is the high since this is inside a
						// sorted iteration
						final String category = socialGroup.category();
						Integer groupScoreHigh = groupScoreHighs.get(category);
						if (groupScoreHigh == null) {
							groupScoreHigh = socialGroup.score();
							groupScoreHighs.put(category, groupScoreHigh);
						}

						Integer inCommonCountHigh = inCommonCountHighs.get(category);

						// normalized score is a ratio of the highest corresponding value times
						// the weight added together we can compare the results

						int normalizedGroupScore = 0;
						if (groupScoreHigh > 0) {
							// notice the integer math must multiply first and divide last.
							normalizedGroupScore = ((weightedScoreFactor * socialGroup.score()) * built.options.groupScoreWeight)
									/ (weightedScoreFactor * groupScoreHigh);
						}

						int normalizedCount = 0;
						if (inCommonCountHigh > 0) {
							normalizedCount = ((weightedScoreFactor * countInCommon) * built.options.inCommonCountWeight)
									/ (weightedScoreFactor * inCommonCountHigh);
						}
						int weightedScore = normalizedGroupScore + normalizedCount;
						if (weightedScore > 0) {
							groupsWeightedScore.add(socialGroup, weightedScore);
						} else {
							LoggerFactory.getLogger(getClass()).warning(socialGroup
									+ " produces an invalid weighted score of " + weightedScore);
						}
					}
				}
				// order by the weighted score
				ImmutableMultiset<SocialGroup> byWeightedScore = Multisets.copyHighestCountFirst(groupsWeightedScore);

				Identifier<?, ?> contextTargeted = built.socialGroupTargeted.context();
				for (SocialGroup socialGroup : byWeightedScore.elementSet()) {
					if (contextTargeted.equals(socialGroup.context())) {
						built.groupsWithSameContext.add(socialGroup);
					} else {
						built.groupsByCategory.put(socialGroup.category(), socialGroup);
						built.counts.put(	socialGroup.id(),
											Pair.of(inCommon.count(socialGroup.id()),
													byWeightedScore.count(socialGroup)));
					}
				}
			}
			return built;
		}

		/**
		 * Provides a map containing the incommon high count for every category that is found in the
		 * social groups.
		 * 
		 * @param inCommon
		 * @return
		 */
		private Map<String, Integer> inCommonCountHighs(Iterable<SocialGroup> socialGroups,
				ImmutableMultiset<SocialGroupId> inCommon) {
			ImmutableListMultimap<String, SocialGroup> categories = Multimaps.index(socialGroups,
																					SocialGroupUtil.categoryFunction());
			HashMap<String, Integer> categoryHighs = Maps.newHashMap();
			for (String category : categories.keySet()) {
				// puts the new high if not already found
				if (!categoryHighs.containsKey(category)) {
					ImmutableList<SocialGroup> socialGroupsForCategory = categories.get(category);
					Multiset<SocialGroupId> countsForCategory = Multisets.filter(inCommon, Predicates.in(Sets
							.newHashSet(IdentifierUtils.ids(socialGroupsForCategory))));
					// these should already be in the same order, but just in case...
					ImmutableMultiset<SocialGroupId> countsForCategoryHighestFirst = Multisets
							.copyHighestCountFirst(countsForCategory);
					categoryHighs.put(category, countsForCategoryHighestFirst.count(countsForCategoryHighestFirst
							.iterator().next()));
				}
			}
			return categoryHighs;
		}
	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct SocialGroupRelator */
	private SocialGroupRelator() {
	}

	public List<SocialGroup> groupsMatchingTargetCategory() {
		return groupsMatchingCategory(this.socialGroupTargeted.category());

	}

	/**
	 * The groups with the category of that given. Groups are returned in order of importance.
	 * 
	 * @param category
	 * @return
	 */
	public List<SocialGroup> groupsMatchingCategory(String category) {
		List<SocialGroup> list = this.groupsByCategory.get(category);
		if (list == null) {
			list = Collections.emptyList();
		}
		return list;
	}

	/**
	 * @return the groupsWithSameContext
	 */
	public List<SocialGroup> groupsWithSameContext() {
		return this.groupsWithSameContext;
	}

	/**
	 * @return the socialGroupTargeted
	 */
	public SocialGroup socialGroupTargeted() {
		return this.socialGroupTargeted;
	}

	/**
	 * For a specific group this will provide the corresponding number of persons that have
	 * memberships.
	 * 
	 * @param group
	 * @return
	 */
	public Integer numberOfPeopleInCommon(SocialGroupId groupId) {
		return peopleCount(this.counts.get(groupId));
	}

	private Integer peopleCount(Pair<Integer, Integer> pair) {
		return pair.getKey();
	}

	private Integer score(Pair<Integer, Integer> pair) {
		return pair.getValue();
	}

	/**
	 * TO avoid null pointers calling other methods this can be used to query if you aren't sure
	 * that the group made the cut.
	 * 
	 * @param groupId
	 * @return
	 */
	public Boolean contains(SocialGroupId groupId) {
		return this.counts.containsKey(groupId);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.counts.toString();
	}

	/**
	 * Affects the results of the {@link SocialGroupRelator}.
	 * 
	 * @author aroller
	 * 
	 */
	@XmlType(namespace = XmlNamespace.API_VALUE)
	@XmlAccessorType(XmlAccessType.NONE)
	@Dictionary(domain = "socialGroupRelatorOptions")
	public static class Options
			implements Cloneable {
		private Options() {
		}

		public static final int WEIGHT_DEFAULT = 100;
		@XmlAttribute
		@QueryParam(OptionsField.IN_COMMON_MINIMUM)
		@Field
		private Integer inCommonMinimum;
		/**
		 * A choice of how much the group score should affect the final choice. This is relative to
		 * {@link #inCommonCountWeight} so if you set one you should set the other.
		 */
		@XmlAttribute
		@QueryParam(OptionsField.GROUP_SCORE_WEIGHT)
		@Field
		private Integer groupScoreWeight;
		/**
		 * A factor of how much influence the number of persons in common with a group should
		 * influence the chosen groups. This is relative to {@link #groupScoreWeight} so both should
		 * be set.
		 * 
		 */
		@XmlAttribute
		@QueryParam(OptionsField.IN_COMMON_COUNT_WEIGHT)
		@Field
		private Integer inCommonCountWeight;

		/**
		 * this is necessary since Query param wipes out the defaults. thanks for nothing!
		 * 
		 * @return
		 */
		private Options init() {
			inCommonCountWeight = If.nil(inCommonCountWeight).use(WEIGHT_DEFAULT);
			groupScoreWeight = If.nil(groupScoreWeight).use(WEIGHT_DEFAULT);
			inCommonMinimum = If.nil(inCommonMinimum).use(3);
			return this;

		}

		public static Options create() {
			return new Options().init();
		}

		public Options inCommonMinimum(Integer inCommonMinimum) {
			this.inCommonMinimum = inCommonMinimum;
			return this;
		}

		public Options groupScoreWeight(Integer groupScoreWeight) {
			this.groupScoreWeight = groupScoreWeight;
			return this;
		}

		public Options inCommonCountWeight(Integer inCommonCountWeight) {
			this.inCommonCountWeight = inCommonCountWeight;
			return this;
		}
	}

}
