package com.aawhere.person;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.CompleteMessage;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;
import com.aawhere.util.rb.StringMessage;

public enum PersonDescriptionMessage implements Message {

	/**
	 * Substitute for {@link Param#PERSON} when the identity is not shared.
	 * 
	 * FIXME:move this to PersonDescription so it may be shared.
	 */
	PERSON_IDENTITY_UNKNOWN_SENTENCE_START("{PERSON_COUNT_VALUE, plural, " //
			+ "=0{Noone}"//
			+ "one{One person}"//
			+ "=2{Two people}"//
			+ "other{# people}"//
			+ "}", Param.PERSON_COUNT),

	/** Used to describe multiple people together...Aaron, Brian and 3 others... */
	PERSONS("{PERSON_COUNT_VALUE, plural, " //
			+ "=0{Noone}"//
			+ "=1{{PERSON}}"//
			+ "=2{{PERSON} and {PERSON2}}"//
			+ "=3{{PERSON},{PERSON2} and {PERSON3}}"//
			// FIXME: the number needs to subtract 2. I think this is ICU offset
			+ "other{{PERSON}, {PERSON2} and # others}"//
			+ "}", Param.PERSON_COUNT), ;

	private ParamKey[] parameterKeys;
	private String message;

	private PersonDescriptionMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		/** The number of persons */
		PERSON_COUNT,
		/** The representation of the person (name, username, etc) */
		PERSON,
		/** the second person in a list. */
		PERSON2,
		/** the third person in a list */
		PERSON3, ;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	/**
	 * Provides the name of a person or the phrase for an unknown or anonymous person appropriate
	 * for starting a sentence.
	 * 
	 * Aaron Roller or One person, Two people, etc.
	 * 
	 * @param persons
	 * @return
	 */
	public static CompleteMessage personStartingASentence(@Nullable Person person) {

		if (person == null || person.identityShared()) {
			return CompleteMessage.create(new StringMessage(person.name())).build();
		} else {
			Message message = PersonDescriptionMessage.PERSON_IDENTITY_UNKNOWN_SENTENCE_START;
			return CompleteMessage.create(message).addParameter(PersonDescriptionMessage.Param.PERSON_COUNT, 1).build();
		}
	}
}
