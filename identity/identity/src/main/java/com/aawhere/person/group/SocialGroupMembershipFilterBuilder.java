/**
 * 
 */
package com.aawhere.person.group;

import static com.aawhere.person.group.SocialGroupMembership.FIELD.KEY.*;

import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseFilterBuilder;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.person.PersonId;

/**
 * @author aroller
 * 
 */
public class SocialGroupMembershipFilterBuilder
		extends BaseFilterBuilder<SocialGroupMembershipFilterBuilder> {

	/**
	 * 
	 */
	public SocialGroupMembershipFilterBuilder() {
	}

	public SocialGroupMembershipFilterBuilder(Filter filter) {
		super(filter);
	}

	public static SocialGroupMembershipFilterBuilder create() {
		return new SocialGroupMembershipFilterBuilder();
	}

	public static SocialGroupMembershipFilterBuilder clone(Filter filter) {
		return new SocialGroupMembershipFilterBuilder(Filter.cloneForBuilder(filter));
	}

	/**
	 * The PersonId is the child part of the SocialGroupMembershipId so this is a convenience call
	 * to {@link #idEqualTo(com.aawhere.id.Identifier)}.
	 * 
	 * @param personId
	 * @return
	 */
	public SocialGroupMembershipFilterBuilder personId(PersonId personId) {
		return addCondition(FilterCondition.create().field(PERSON_ID).operator(FilterOperator.EQUALS).value(personId)
				.build());
	}

	/**
	 * The SocialGroupId is the parent so this is a convenience call to
	 * {@link #setParent(com.aawhere.id.Identifier)}.
	 * 
	 * @param groupId
	 * @return
	 */
	public SocialGroupMembershipFilterBuilder groupId(SocialGroupId groupId) {
		return addCondition(FilterCondition.create().field(GROUP_ID).operator(FilterOperator.EQUALS).value(groupId)
				.build());
	}

	/**
	 * @param personIds
	 * @return
	 */
	public SocialGroupMembershipFilterBuilder personIds(Iterable<PersonId> personIds) {
		return addCondition(FilterCondition.create().field(PERSON_ID).operator(FilterOperator.IN).value(personIds)
				.build());
	}

}
