/**
 * 
 */
package com.aawhere.person;

import com.aawhere.app.account.AccountId;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceGeneral;
import com.aawhere.persist.ServiceGeneralBase;

import com.google.common.base.Function;

/**
 * @author aroller
 * 
 */
public interface PersonAccountProvider
		extends ServiceGeneral<Persons, Person, AccountId> {

	/**
	 * provides a person when given the accountId. This is a guaranteed producer that must be able
	 * to create a person if it doesn't already exist.
	 * 
	 * @param account
	 * @return
	 * @throws EntityNotFoundException
	 */
	Person person(AccountId accountId) throws EntityNotFoundException;

	public static class Default
			extends ServiceGeneralBase<Persons, Person, AccountId>
			implements PersonAccountProvider {

		public Default() {
			super();
		}

		public Default(Function<AccountId, Person> idFunction) {
			super(idFunction);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.person.PersonAccountProvider#person(com.aawhere.app.account.AccountId)
		 */
		@Override
		public Person person(AccountId accountId) throws EntityNotFoundException {
			return entity(accountId);
		}

	}
}
