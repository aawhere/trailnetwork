/**
 *
 */
package com.aawhere.person;

import java.net.URL;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountUtil;
import com.aawhere.app.account.EmailAddress;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityUtil;
import com.aawhere.persist.Filter;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;

/**
 * Helps out {@link Person} and friends.
 * 
 * @author aroller
 * 
 */
public class PersonUtil {

	/**
	 * Provides a suggested name for all accounts considered. Just combines the aliases into a space
	 * separated token string if the account does not have a name. Gives the selected name, and the
	 * name with aliases for a more descriptive
	 * 
	 * @param accounts
	 * @return the selected name in the left, the name with aliases as the right
	 */
	@Nonnull
	public static Pair<String, String> personName(Iterable<Account> accounts) {

		List<Account> accountsWithBetterNameFirst = AccountUtil.accountsWithBetterNameFirst(accounts);
		Account first = Iterables.getFirst(accountsWithBetterNameFirst, null);
		if (first == null) {
			// TODO:sort the accounts by most used account first
			first = Iterables.getLast(accounts);
		}

		String selectedName;
		if (first.hasName()) {
			selectedName = first.name();
		} else {
			selectedName = first.alias();
		}

		final Set<String> aliases = Sets.newHashSet(AccountUtil.aliases(accounts));
		aliases.remove(selectedName);
		String nameWithAliases;
		if (aliases.isEmpty()) {
			nameWithAliases = selectedName;
		} else {
			String joinedAliases = StringUtils.join(aliases, "/");
			nameWithAliases = new StringBuilder(selectedName).append(" (").append(joinedAliases).append(")").toString();
		}

		return Pair.of(selectedName, nameWithAliases);
	}

	/**
	 * @see #personName(Iterable)
	 * @param accounts
	 * @return
	 */
	@Nonnull
	public static Pair<String, String> personName(Account... accounts) {
		return personName(Lists.newArrayList(accounts));
	}

	/**
	 * Provides a common place to update the person given necessary information. This should be
	 * modified to accommodate what is pertinent to an update.
	 * 
	 * @param winner
	 * @param accounts
	 * @return
	 */
	public static Person updated(Person winner, Set<Account> accounts) {
		final Pair<String, String> names = personName(accounts);
		return Person.mutate(winner).name(names.getLeft()).nameWithAliases(names.getRight()).build();
	}

	public static Person update(Person person, Configuration preferences) {
		return Person.mutate(person).preferences(preferences).build();
	}

	/**
	 * Provides a stream of all {@link Person#aliases()} from each {@link Person} given.
	 * 
	 * @param persons
	 * @return
	 */
	public static Iterable<PersonId> aliases(Iterable<Person> persons) {
		return Iterables.concat(Iterables.transform(persons, aliasFunction()));
	}

	public static Function<Person, Set<PersonId>> aliasFunction() {
		return new Function<Person, Set<PersonId>>() {

			@Override
			public Set<PersonId> apply(Person input) {
				return (input == null) ? null : input.aliases();
			}
		};
	}

	/**
	 * Allows muatation of persons to provide an alternate filter which led to the results of the
	 * persons. This useful when retrieving by ids provided from a different entity.
	 * 
	 * @param persons
	 * @param filter
	 * @return
	 */
	public static Persons filterReplaced(Persons persons, Filter filter) {
		return Persons.mutate(persons).setFilter(filter).build();
	}

	/**
	 * @param provider
	 * @return
	 */
	public static Function<PersonId, Person> personFunction(final PersonProvider provider) {
		return new Function<PersonId, Person>() {
			@Override
			public Person apply(PersonId input) {
				try {
					return (provider != null && input != null) ? provider.person(input) : null;
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	public static Function<AccountId, Person> personFunction(final PersonAccountProvider provider) {
		return new Function<AccountId, Person>() {
			@Override
			public Person apply(AccountId input) {
				try {
					return (provider != null && input != null) ? provider.person(input) : null;
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/**
	 * When given the person and the account aliases this will determine if the person's name is
	 * worth sharing since the person's name is often the same as the alias.
	 * 
	 * @param person
	 * @param accountAliases
	 * @return
	 */
	public static boolean nameIsUseful(Person person, Iterable<String> accountAliases) {
		boolean useful = false;
		if (person != null) {
			String name = person.name();
			if (name != null) {
				if (accountAliases != null) {
					Set<String> aliases = SetUtilsExtended.asSet(accountAliases);
					useful = !aliases.contains(name);
				} else {
					// person's name with no aliases is useful
					useful = true;
				}
			}
		}
		return useful;
	}

	private enum AccountIdsFunction implements Function<Person, Set<AccountId>> {
		INSTANCE;
		@Override
		@Nullable
		public Set<AccountId> apply(@Nullable Person person) {
			return (person != null) ? person.accountIds() : null;
		}

		@Override
		public String toString() {
			return "AccountIdsFunction";
		}
	}

	/**
	 * provides the account ids from the person.
	 * 
	 * @return
	 */
	public static Function<Person, Set<AccountId>> accountIdsFunction() {
		return AccountIdsFunction.INSTANCE;
	}

	public static Set<AccountId> accountIdsFromPersons(Iterable<Person> persons) {
		return ImmutableSet.<AccountId> builder()
				.addAll(Iterables.concat(Iterables.transform(persons, accountIdsFunction()))).build();
	}

	/**
	 * Provides the best email from the given accounts.
	 * 
	 * @param combined
	 * @return
	 */
	@Nullable
	public static EmailAddress email(@Nonnull Iterable<Account> accounts) {
		Account account = Iterables.getFirst(AccountUtil.accountsWithBetterEmailFirst(accounts), null);
		return (account != null) ? account.email() : null;
	}

	/**
	 * Provides the best photoUrl from the given accounts.
	 * 
	 * @param combined
	 * @return
	 */
	@Nullable
	public static URL photoUrl(@Nonnull Iterable<Account> accounts) {
		Account account = Iterables.getFirst(AccountUtil.accountsWithBetterPhotoUrlFirst(accounts), null);
		return (account != null) ? account.photoUrl() : null;
	}

	/**
	 * Provides those with managed accounts first. If there are ties then the oldest wins.
	 * 
	 * @param persons
	 * @return
	 */
	public static List<Person> managedAccountPersonsFirst(Iterable<Person> persons) {
		return Ordering.natural().onResultOf(managedAccountPersonFunction())
				.compound(EntityUtil.<Person> dateCreatedOldestFirst()).sortedCopy(persons);
	}

	private enum ManagedAccountPersonFunction implements Function<Person, Boolean> {
		INSTANCE;
		@Override
		@Nullable
		public Boolean apply(@Nullable Person person) {
			return (person != null) ? AccountUtil.managingAccountIdContained(person.accountIds()) : null;
		}

		@Override
		public String toString() {
			return "ManagedAccountPersonFunction";
		}
	}

	public static Function<Person, Boolean> managedAccountPersonFunction() {
		return ManagedAccountPersonFunction.INSTANCE;
	}

	private enum EmailFunction implements Function<Person, EmailAddress> {
		INSTANCE;
		@Override
		@Nullable
		public EmailAddress apply(@Nullable Person input) {
			return (input != null) ? input.email() : null;
		}

		@Override
		public String toString() {
			return "EmailFunction";
		}
	}

	public static Function<Person, EmailAddress> emailFunction() {
		return EmailFunction.INSTANCE;
	}

	private enum NameFunction implements Function<Person, String> {
		INSTANCE;
		@Override
		@Nullable
		public String apply(@Nullable Person person) {
			return (person != null) ? person.name() : null;
		}

		@Override
		public String toString() {
			return "PersonNameFunction";
		}
	}

	public static Function<Person, String> nameFunction() {
		return NameFunction.INSTANCE;
	}

	private enum NameWithAliasesFunction implements Function<Person, String> {
		INSTANCE;
		@Override
		@Nullable
		public String apply(@Nullable Person person) {
			return (person != null) ? person.nameWithAliases() : null;
		}

		@Override
		public String toString() {
			return "NameWithAliasesFunction";
		}
	}

	public static Function<Person, String> nameWithAliasesFunction() {
		return NameWithAliasesFunction.INSTANCE;
	}
}
