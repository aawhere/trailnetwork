/**
 * 
 */
package com.aawhere.person;

import com.aawhere.id.Identifiers;

/**
 * Identifiers for PersonId.
 * 
 * @author aroller
 * 
 */
public class PersonIds
		extends Identifiers<PersonId, Person> {

	/**
	 * @param identifierString
	 */
	public PersonIds(String identifierString) {
		super(identifierString);
	}

	PersonIds() {
		super();
	}

	/**
	 * @param ids
	 */
	public PersonIds(Iterable<PersonId> ids) {
		super(ids);
	}

	private static final long serialVersionUID = 7866708802555366309L;

}
