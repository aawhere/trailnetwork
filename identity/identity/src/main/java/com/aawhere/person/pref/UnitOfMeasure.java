/**
 *
 */
package com.aawhere.person.pref;

/**
 * Preferences for Units of Measure (i.e. metric vs. US customary units
 * 
 * @author brian
 * 
 */
public enum UnitOfMeasure {
	METRIC, US_CUSTOMARY
}
