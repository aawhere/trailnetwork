/**
 * 
 */
package com.aawhere.person.group;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.id.LongIdentifierWithParent;
import com.aawhere.person.PersonId;

/**
 * A composition of the SocialGroup and the Person resulting in a unique identifier ensuring each
 * person may only have a single membership in any group.
 * 
 * @author aroller
 * 
 */
public class SocialGroupMembershipId
		extends LongIdentifierWithParent<SocialGroupMembership, SocialGroupId> {

	/**
	 * @param kind
	 */
	protected SocialGroupMembershipId() {
		super(SocialGroupMembership.class);
	}

	public SocialGroupMembershipId(SocialGroupId groupId, PersonId personId) {
		super(value(personId), groupId, SocialGroupMembership.class);
	}

	public SocialGroupMembershipId(PersonId personId, SocialGroupId groupId) {
		super(value(personId), groupId, SocialGroupMembership.class);
	}

	@Deprecated
	SocialGroupMembershipId(Long parentIdValue, Long childIdValue) {
		super(childIdValue, new SocialGroupId(parentIdValue), SocialGroupMembership.class);
	}

	@Deprecated
	public SocialGroupMembershipId(SocialGroupId groupId, Long value) {
		super(value, groupId, SocialGroupMembership.class);
	}

	public SocialGroupMembershipId(Long value, SocialGroupId groupId) {
		super(value, groupId, SocialGroupMembership.class);
	}

	private static Long value(PersonId personId) {
		return IdentifierUtils.valueOf(personId);
	}

	/**
	 * @param compositeValue
	 * @param kind
	 */
	public SocialGroupMembershipId(String compositeValue) {
		super(compositeValue, SocialGroupMembership.class, SocialGroupId.class);
	}

	private static final long serialVersionUID = -2848400388036192954L;

	public PersonId getPersonId() {
		return new PersonId(getValue());
	}

	public SocialGroupId getSocialGroupId() {
		return getParent();
	}
}
