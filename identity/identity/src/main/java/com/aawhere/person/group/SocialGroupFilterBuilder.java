/**
 * 
 */
package com.aawhere.person.group;

import javax.annotation.Nullable;

import com.aawhere.id.Identifier;
import com.aawhere.persist.Filter;
import com.aawhere.persist.Filter.BaseFilterBuilder;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.FilterSort;
import com.aawhere.persist.FilterSort.Direction;

/**
 * @author aroller
 * 
 */
public class SocialGroupFilterBuilder
		extends BaseFilterBuilder<SocialGroupFilterBuilder> {

	public static SocialGroupFilterBuilder create() {
		return new SocialGroupFilterBuilder();
	}

	/**
	 * use {@link #create()}
	 */
	private SocialGroupFilterBuilder() {
		// TODO Auto-generated constructor stub
	}

	/** use {@link #clone(Filter)} */
	private SocialGroupFilterBuilder(Filter filter) {
		super(filter);
	}

	public SocialGroupFilterBuilder context(@Nullable Identifier<?, ?> context) {
		if (context != null) {
			addCondition(FilterCondition.create().field(SocialGroupField.Key.CONTEXT).operator(FilterOperator.EQUALS)
					.value(context).build());
		}
		return this;
	}

	/**
	 * When filtering alone use this. When combining with context use
	 * {@link #categoryPredicate(String)}.
	 * 
	 * @param category
	 * @return
	 */
	public SocialGroupFilterBuilder category(String category) {
		if (category != null) {
			final FilterCondition<Object> categoryPredicate = categoryCondition(category);
			addCondition(categoryPredicate);
		}
		return this;
	}

	/**
	 * When combined with {@link #context(Identifier)} this will provide sufficient filtering of
	 * categories.
	 * 
	 * When used across all context, use {@link #category(String)}.
	 * 
	 * @param category
	 * @return
	 */
	public SocialGroupFilterBuilder categoryPredicate(@Nullable String category) {
		if (category != null) {
			addPredicateCondition(categoryCondition(category));
		}
		return this;
	}

	/**
	 * @param category
	 * @return
	 */
	private FilterCondition<Object> categoryCondition(String category) {
		final FilterCondition<Object> categoryPredicate = FilterCondition.create().field(SocialGroupField.Key.CATEGORY)
				.operator(FilterOperator.EQUALS).value(category).build();
		return categoryPredicate;
	}

	/**
	 * use this to order by score within a context. There won't be many groups within a single
	 * context.
	 * 
	 * @return
	 */
	public SocialGroupFilterBuilder orderByScoreInMemory() {
		return orderByInMemory(FilterSort.create().setFieldKey(SocialGroupField.Key.SCORE)
				.setDirection(Direction.DESCENDING).build());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.Filter.BaseBuilder#build()
	 */
	@Override
	public Filter build() {
		return super.build();
	}

	/**
	 * @param filter
	 * @return
	 */
	public static SocialGroupFilterBuilder clone(Filter filter) {
		return new SocialGroupFilterBuilder(Filter.cloneForBuilder(filter));
	}

	/**
	 * @param category
	 * @param context
	 * @return
	 */
	public SocialGroupFilterBuilder categoryContext(String category, Identifier<?, ?> context) {
		return context(context).categoryPredicate(category);
	}

}
