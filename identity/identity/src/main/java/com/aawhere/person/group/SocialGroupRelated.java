/**
 * 
 */
package com.aawhere.person.group;

/**
 * Generalizes the many entities that are related to a {@link SocialGroup}.
 * 
 * @author aroller
 * 
 */
public interface SocialGroupRelated {

	/**
	 * Identifies the SocialGroup related to the entity or null if none.
	 * 
	 * @return the identifier of the SocialGroup related to the containing entity or null if no
	 *         relation is made.
	 */
	public SocialGroupId socialGroupId();
}
