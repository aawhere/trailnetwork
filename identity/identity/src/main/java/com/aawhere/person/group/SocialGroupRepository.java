/**
 * 
 */
package com.aawhere.person.group;

import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;

/**
 * Access to persisted {@link SocialGroup} and {@link SocialGroups}.
 * 
 * @author aroller
 * 
 */
public interface SocialGroupRepository
		extends CompleteRepository<SocialGroups, SocialGroup, SocialGroupId> {

	/**
	 * Provides a builder that upon building will persist all the fields modified.
	 * 
	 * @param id
	 * @return
	 */
	SocialGroup.Builder update(SocialGroupId id) throws EntityNotFoundException;
}
