/**
 *
 */
package com.aawhere.person;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.aawhere.id.Identifier;
import com.aawhere.id.LongIdentifier;
import com.aawhere.identity.IdentityId;
import com.aawhere.persist.ServicedBy;
import com.aawhere.xml.XmlNamespace;

/**
 * @author brian
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@ServicedBy(PersonProvider.class)
public class PersonId
		extends LongIdentifier<Person>
		implements IdentityId {

	private static final long serialVersionUID = 4978044001290408204L;
	private static final Class<Person> KIND = Person.class;

	/**
	 *
	 */
	PersonId() {
		super(KIND);
	}

	/**
	 * @param other
	 */
	public PersonId(Identifier<Long, Person> other) {
		super(other);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public PersonId(Long value) {
		super(value, KIND);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public PersonId(String value) {
		super(value, KIND);
	}

}
