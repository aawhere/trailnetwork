/**
 * 
 */
package com.aawhere.person.group;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

public enum SocialGroupMessage implements Message {

	/** Comments go here. */
	CATEGORY_CONTEXT_NOT_FOUND("Social Group {CATEGORY} for {CONTEXT} not found.", Param.CATEGORY, Param.CONTEXT);

	private ParamKey[] parameterKeys;
	private String message;

	private SocialGroupMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		/**
		 * @see SocialGroupField#CATEGORY
		 */
		CATEGORY,
		/**
		 * @see SocialGroupField#CONTEXT
		 */
		CONTEXT;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static class Doc {

	}
}
