/**
 *
 */
package com.aawhere.person;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.app.account.AccountId;
import com.aawhere.identity.IdentityManager;
import com.aawhere.persist.ServicedBy;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;

/**
 * @author aroller
 * 
 */
@ServicedBy(PersonAccountProvider.class)
// request scoped for Identity manager
public class PersonFromAccountIdXmlAdapter
		extends XmlAdapter<Person, AccountId> {

	private Function<AccountId, Person> provider;
	private IdentityManager identityManager;

	public PersonFromAccountIdXmlAdapter() {
		this.provider = null;
	}

	@Inject
	public PersonFromAccountIdXmlAdapter(PersonAccountProvider personAccountProvider, IdentityManager identityManager) {
		this.provider = PersonUtil.personFunction(personAccountProvider);
		this.identityManager = identityManager;
	}

	public PersonFromAccountIdXmlAdapter(Function<AccountId, Person> provider, IdentityManager identityManager) {
		this.provider = provider;
		this.identityManager = identityManager;
	}

	public PersonFromAccountIdXmlAdapter(Function<AccountId, Person> provider) {
		this.provider = provider;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public AccountId unmarshal(Person person) throws Exception {
		return (person != null) ? Iterables.getFirst(person.accountIds(), null) : null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public Person marshal(AccountId accountId) throws Exception {
		return (accountId != null && this.provider != null && this.identityManager != null) ? this.identityManager
				.enforceAccess(provider.apply(accountId)) : null;
	}

}
