/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 23, 2010
person : com.aawhere.person.pref.Preferences.java
 */
package com.aawhere.person.pref;

import com.aawhere.person.Person;

/**
 * Preferences for a {@link Person}. Preferences are implemented using Apache Configuration (see
 * {@link Configuration}). This enumeration documents the names of the keys for the configuration.
 * 
 * The keys should never be hard coded (aka. "magic numbers"). This enumeration should be used
 * instead.
 * 
 * @author Brian Chapman
 * 
 */
public enum Preferences {

	/* @formatter:off */
	DISTANCE_UNIT("quantity-length-unit"),
	DISTANCE_DIGIT("quantity-length-digit"),
	ELEVATION_UNIT("quantity-elevation-unit"),
	ELEVATION_DIGIT("quantity-elevation-digit"),
	ELEVATION_CHANGE_UNIT("quantity-elevationChange-unit"),
	ELEVATION_CHANGE_DIGIT("quantity-elevationChange-digit"),
	RATIO_DIGIT("quantity-ratio-digit"),
	RATIO_UNIT("quantity-ratio-unit"),
	LOCALE("locale");
	/* @formatter:on */

	private final String key;

	Preferences(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

}
