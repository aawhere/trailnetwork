package com.aawhere.person;

import com.aawhere.persist.CompleteRepository;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.Repository;

/**
 * Provides a {@link Repository} for {@link Person}s.
 * 
 * @author Brian Chapman
 * 
 */
public interface PersonRepository
		extends CompleteRepository<Persons, Person, PersonId> {

	public Person name(PersonId personId, String name, String nameWithAliases) throws EntityNotFoundException;

	/** Adds that which is given to the merged ids that may already exist. */
	public Person aliasesAdded(PersonId personId, Iterable<PersonId> personIds) throws EntityNotFoundException;

	/**
	 * Provides a connected Builder that will persist the entity upon building.
	 * 
	 * @param personId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Person.Builder update(PersonId personId) throws EntityNotFoundException;
}