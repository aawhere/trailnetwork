/**
 * 
 */
package com.aawhere.person.group;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.person.PersonId;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

/**
 * @author aroller
 * 
 */
public class SocialGroupUtil {

	/** Provides ordering of the social groups in order of highest score first...null scores last. */
	public static final Ordering<SocialGroup> SCORE_DESCENDING = Ordering.natural().reverse().nullsLast()
			.onResultOf(scoreFunction());

	public static Iterable<SocialGroupId> groupIdsFromMemberships(Iterable<SocialGroupMembership> memberships) {
		return groupIds(IdentifierUtils.ids(memberships));
	}

	public static Iterable<SocialGroupId> groupIds(Iterable<SocialGroupMembershipId> ids) {
		return Iterables.transform(ids, groupIdFunction());
	}

	public static Set<SocialGroupId> groupIdSet(Iterable<SocialGroupMembership> memberships) {
		HashSet<SocialGroupId> set = new HashSet<>();
		Iterables.addAll(set, groupIdsFromMemberships(memberships));
		return set;
	}

	/**
	 * @param memberships
	 * @return
	 */
	public static Iterable<PersonId> personIdsFromMemberships(Iterable<SocialGroupMembership> memberships) {
		return personIds(IdentifierUtils.ids(memberships));
	}

	public static Iterable<PersonId> personIds(Iterable<SocialGroupMembershipId> ids) {
		return Iterables.transform(ids, personIdFunction());
	}

	public static Function<SocialGroupMembershipId, PersonId> personIdFunction() {
		return new Function<SocialGroupMembershipId, PersonId>() {

			@Override
			public PersonId apply(SocialGroupMembershipId input) {
				return (input != null) ? input.getPersonId() : null;
			}
		};
	}

	public static Function<SocialGroupMembershipId, SocialGroupId> groupIdFunction() {
		return new Function<SocialGroupMembershipId, SocialGroupId>() {

			@Override
			public SocialGroupId apply(SocialGroupMembershipId input) {
				return (input != null) ? input.getSocialGroupId() : null;
			}
		};
	}

	public static Iterable<SocialGroupMembership> memberships(SocialGroupId groupId,
			Iterable<Entry<PersonId, Integer>> personIds) {

		return Iterables.transform(personIds, membershipWithCountFunction(groupId));
	}

	public static Function<Map.Entry<PersonId, Integer>, SocialGroupMembership> membershipWithCountFunction(
			final SocialGroupId groupId) {
		return new Function<Map.Entry<PersonId, Integer>, SocialGroupMembership>() {

			@Override
			public SocialGroupMembership apply(Entry<PersonId, Integer> input) {
				return (input != null) ? SocialGroupMembership.create().group(groupId).member(input.getKey())
						.contributions(input.getValue()).build() : null;
			}
		};

	}

	public static Function<PersonId, SocialGroupMembership> membershipFunction(final SocialGroupId groupId) {
		return new Function<PersonId, SocialGroupMembership>() {

			@Override
			public SocialGroupMembership apply(PersonId input) {
				return (input != null) ? SocialGroupMembership.create().group(groupId).member(input).build() : null;
			}
		};
	}

	public static Set<PersonId> personIdSet(Iterable<SocialGroupMembership> memberships) {
		HashSet<PersonId> set = new HashSet<>();
		Iterables.addAll(set, personIdsFromMemberships(memberships));
		return set;
	}

	public static Function<SocialGroup, Integer> scoreFunction() {
		return new Function<SocialGroup, Integer>() {

			@Override
			public Integer apply(SocialGroup input) {
				return (input != null) ? input.score() : null;
			}
		};
	}

	public static Function<SocialGroup, Integer> membershipCountFunction() {
		return new Function<SocialGroup, Integer>() {

			@Override
			public Integer apply(SocialGroup input) {
				return (input != null) ? input.membershipCount() : null;
			}
		};
	}

	/**
	 * @param loadAll
	 * @return
	 */
	public static Iterable<? extends Identifier<?, ?>> contextIds(Iterable<SocialGroup> groups) {
		return Iterables.transform(groups, contextFunction());
	}

	/**
	 * Provides the contextIds for the type given. This does not filter and will cause a class cast
	 * exception if the context is anything but that which matches the idType given.
	 * 
	 * @param groups
	 * @param idType
	 * @return
	 */
	public static <I extends Identifier<?, ?>> Iterable<I> contextIds(Iterable<SocialGroup> groups, Class<I> idType) {
		Function<SocialGroup, I> contextFunction = contextFunction();
		return Iterables.filter(Iterables.transform(groups, contextFunction), Predicates.instanceOf(idType));
	}

	public static <I extends Identifier<?, ?>> Function<SocialGroup, I> contextFunction() {
		return new Function<SocialGroup, I>() {
			@SuppressWarnings("unchecked")
			@Override
			public I apply(SocialGroup input) {
				return (I) ((input != null) ? input.context() : null);
			}
		};

	}

	public static Function<SocialGroup, String> categoryFunction() {
		return new Function<SocialGroup, String>() {

			@Override
			public String apply(SocialGroup input) {
				return (input != null) ? input.category() : null;
			}
		};
	}

	public static Iterable<SocialGroupMembershipId> membershipIds(Iterable<SocialGroupMembership> memberships) {
		return IdentifierUtils.ids(memberships);
	}

	/**
	 * @param socialGroups
	 * @return
	 */
	public static Iterable<SocialGroup> scoreRequired(Iterable<SocialGroup> socialGroups) {
		return Iterables.filter(socialGroups, Predicates.compose(Predicates.notNull(), scoreFunction()));
	}

	public static Iterable<SocialGroup> countRequired(Iterable<SocialGroup> socialGroups) {
		return Iterables.filter(socialGroups, Predicates.compose(Predicates.notNull(), membershipCountFunction()));
	}

	/**
	 * Returns the ids from those entities that are related to a single social group. Nulls are
	 * filtered out since it is common to have a related entity return null.
	 */
	public static Iterable<SocialGroupId> socialGroupIdsRelated(Iterable<? extends SocialGroupRelated> relatedEntities) {
		return Iterables.filter(Iterables.transform(relatedEntities, socialGroupIdRelatedFunction()),
								Predicates.notNull());
	}

	public static Function<SocialGroupRelated, SocialGroupId> socialGroupIdRelatedFunction() {
		return new Function<SocialGroupRelated, SocialGroupId>() {

			@Override
			public SocialGroupId apply(SocialGroupRelated input) {
				return (input != null) ? input.socialGroupId() : null;
			}
		};
	}
}
