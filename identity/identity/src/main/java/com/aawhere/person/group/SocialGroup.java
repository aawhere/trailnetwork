/** Copyright(c) 2011 AAWhere LLC.  All rights reserved.
added by roller on Nov 23, 2010
person : com.aawhere.person.group.SocialGroup.java
 */
package com.aawhere.person.group;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifier;
import com.aawhere.lang.Assertion;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.LongBaseEntity;
import com.aawhere.xml.XmlNamespace;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

/**
 * A general purpose item that organizes data like Activities or Routes into collections related to
 * Persons, Applications, etc.
 * 
 * FIXME: move out of person into group package.
 * 
 * @author roller
 * 
 */
@Cache
@Entity
@Dictionary(domain = SocialGroup.FIELD.DOMAIN)
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class SocialGroup
		extends LongBaseEntity<SocialGroup, SocialGroupId>
		implements Cloneable {

	/**
	 * Used to construct all instances of SocialGroup.
	 */
	@XmlTransient
	public static class Builder
			extends LongBaseEntity.Builder<SocialGroup, Builder, SocialGroupId> {
		private boolean isParent = false;

		public Builder() {
			super(new SocialGroup());
		}

		Builder(SocialGroup group) {
			super(group);
		}

		private Builder(SocialGroupId id) {
			this();
			setId(id);
			this.isParent = true;
		}

		public Builder category(String category) {
			building.category = category;
			return this;
		}

		public Builder score(Integer score) {
			building.score = score;
			return this;
		}

		public Builder context(Identifier<?, ?> context) {
			building.context = context;
			return this;
		}

		Builder membershipCount(Integer membershipCount) {
			building.membershipCount = membershipCount;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			// as a parent we may create an empty shell so don't validate
			if (!isParent) {
				super.validate();
				// membership count may be null since it is updated by the service
				Assertion.exceptions().notNull("context", building.context);
				Assertion.exceptions().notNull("category", building.category);
				Assertion.exceptions().notNull("score", building.score);
			}
		}

		@Override
		public SocialGroup build() {
			SocialGroup built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct SocialGroup */
	private SocialGroup() {
	}

	public static Builder create() {
		return new Builder();
	}

	static Builder mutate(SocialGroup group) {
		return new Builder(group);
	}

	/**
	 * A number used to compare this group against others in the same category.
	 * 
	 */
	@XmlElement(name = FIELD.SCORE)
	@Field(key = "score", indexedFor = "TN-743")
	@Index
	@Nonnull
	private Integer score;

	/**
	 * A relation to the item in the system that created this group. Storage and retrieval for this
	 * takes special handling. See RegisteredEntities and associated translatomvn rs.
	 */
	@XmlAnyElement(lax = true)
	@Field(key = "context", indexedFor = "TN-743")
	@Index
	@Nonnull
	private Identifier<?, ?> context;

	/**
	 * The "subject" which this group represents which is a broad generalization of the context. If
	 * the context is the Sausalito bridge club, then this category may be "CardGames" or more
	 * specifically "BridgeCardGames"...whatever makes sense to the Domain related to the
	 * {@link #context()}.
	 * 
	 */
	@XmlElement(name = SocialGroupField.CATEGORY)
	@Field(key = "category", indexedFor = "TN-743")
	@Index
	@Nonnull
	private String category;

	@XmlElement(name = SocialGroupField.MEMBERSHIP_COUNT)
	@Field(key = "membershipCount")
	private Integer membershipCount;

	@Override
	@Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM, indexedFor = "Repository.load()")
	public SocialGroupId getId() {
		return super.getId();
	}

	/**
	 * @return the criteria
	 */
	public String category() {
		return this.category;
	}

	/**
	 * @return the score
	 */
	public Integer score() {
		return this.score;
	}

	/**
	 * @return the context
	 */
	public Identifier<?, ?> context() {
		return this.context;
	}

	public Integer membershipCount() {
		return this.membershipCount;
	}

	@XmlTransient
	public static final class FIELD
			extends BaseEntity.BASE_FIELD {
		public static final String DOMAIN = "socialGroup";
		public static final String SOCIAL_GROUPS = DOMAIN + "s";
		public static final String CATEGORY = "category";
		public static final String SCORE = "score";
		public static final String MEMBERSHIP_COUNT = "membershipCount";

	}

	private static final long serialVersionUID = -770291439658827851L;

	/**
	 * Friendly method allowing children to create this as an empty shell for use with Parent.
	 * 
	 * @param groupId
	 * @return
	 */
	static SocialGroup parent(SocialGroupId groupId) {
		return new Builder(groupId).build();
	}

	/**
	 * @param socialGroup
	 * @return
	 */
	public static Builder clone(SocialGroup socialGroup) {
		try {
			return new Builder((SocialGroup) socialGroup.clone());
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

}
