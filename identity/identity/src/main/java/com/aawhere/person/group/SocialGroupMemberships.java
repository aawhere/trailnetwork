/**
 * 
 */
package com.aawhere.person.group;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class SocialGroupMemberships
		extends BaseFilterEntities<SocialGroupMembership> {

	/**
	 * Used to construct all instances of SocialGroupMemberships.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, SocialGroupMembership, SocialGroupMemberships> {

		public Builder() {
			super(new SocialGroupMemberships());
		}

		@Override
		public SocialGroupMemberships build() {
			SocialGroupMemberships built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct SocialGroupMemberships */
	private SocialGroupMemberships() {
	}

	public static Builder create() {
		return new Builder();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntities#getCollection()
	 */
	@Override
	@XmlElement(name = SocialGroupMembership.FIELD.DOMAIN)
	public List<SocialGroupMembership> getCollection() {
		return super.getCollection();
	}
}
