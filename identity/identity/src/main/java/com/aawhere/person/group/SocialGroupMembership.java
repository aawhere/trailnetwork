/**
 * 
 */
package com.aawhere.person.group;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierToIdentifiableXmlAdapter;
import com.aawhere.identity.IdentityManager;
import com.aawhere.lang.Assertion;
import com.aawhere.measure.xml.CountFromIntegerXmlAdapter;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.LongBaseEntity;
import com.aawhere.persist.SelfIdentifyingEntity;
import com.aawhere.person.Person;
import com.aawhere.person.PersonField;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonIdXmlAdapter;
import com.aawhere.person.PersonProvider;
import com.aawhere.person.PersonWebApiUri;
import com.aawhere.xml.XmlNamespace;
import com.google.common.base.Function;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Associates {@link Person} to a {@link SocialGroup} with other membership related information.
 * 
 * This membership allows queries of all groups for a person or all persons in a group with
 * pagination, etc.
 * 
 * @author aroller
 * 
 */
@Cache
@Entity
@Dictionary(domain = SocialGroupMembership.FIELD.DOMAIN)
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class SocialGroupMembership
		extends LongBaseEntity<SocialGroupMembership, SocialGroupMembershipId>
		implements SelfIdentifyingEntity {

	/**
	 * Used to construct all instances of SocialGroupMembership.
	 */
	@XmlTransient
	public static class Builder
			extends LongBaseEntity.Builder<SocialGroupMembership, Builder, SocialGroupMembershipId> {

		public Builder() {
			super(new SocialGroupMembership());
		}

		public Builder group(SocialGroupId group) {
			parentId(group);
			return this;
		}

		public Builder member(PersonId person) {
			building.personId = person;
			return this;
		}

		public Builder contributions(Integer count) {
			building.contributionCount = count;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("group", this.hasParentId());
			Assertion.exceptions().notNull("person", building.personId);

		}

		@Override
		public SocialGroupMembership build() {
			if (!hasId()) {
				setId(new SocialGroupMembershipId(building.socialGroupId(), building.personId));
			}
			SocialGroupMembership built = super.build();
			return built;
		}

	}// end Builder

	@XmlElement(name = FIELD.PERSON_ID)
	@Field(key = FIELD.PERSON_ID, indexedFor = "TN-743", xmlAdapter = SocialGroupMembership.PersonXmlAdapter.class)
	@Index
	@ApiModelProperty(value = "The member of the SocialGroup", required = true)
	private PersonId personId;

	@Field
	@ApiModelProperty(value = "The investement the Person has made to the SocialGroup.",
			notes = "Relative to other groups in the same category", required = false)
	@XmlElement(name = SocialGroupMembershipField.CONTRIBUTION_COUNT)
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	private Integer contributionCount;

	/** Use {@link Builder} to construct SocialGroupMembership */
	private SocialGroupMembership() {
	}

	public static Builder create() {
		return new Builder();
	}

	@Override
	@Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM, indexedFor = "Repository.load()")
	public SocialGroupMembershipId getId() {
		return super.getId();
	}

	private static final long serialVersionUID = -2927507353742319549L;

	/**
	 * @return the personId
	 */
	public PersonId personId() {
		return this.personId;
	}

	@XmlElement(name = PersonField.DOMAIN)
	@XmlJavaTypeAdapter(SocialGroupMembership.PersonXmlAdapter.class)
	public PersonId getPersonIdXml() {
		return this.personId;
	}

	/**
	 * @return the groupId
	 */
	@Field(indexedFor = "TN-743", xmlAdapter = SocialGroupMembership.SocialGroupXmlAdapter.class)
	@ApiModelProperty(value = "The SocialGroup for which the Person is a member.", required = true)
	public SocialGroupId socialGroupId() {
		return (SocialGroupId) getParentId();
	}

	@XmlElement(name = FIELD.GROUP_ID)
	public SocialGroupId getSocialGroupId() {
		return socialGroupId();
	}

	@Override
	protected Class<? extends Identifier<?, ?>> parentIdType() {
		return SocialGroupId.class;
	}

	@XmlElement(name = SocialGroupField.DOMAIN)
	@XmlJavaTypeAdapter(SocialGroupMembership.SocialGroupXmlAdapter.class)
	public SocialGroupId getSocialGroupXml() {
		return socialGroupId();
	}

	/**
	 * @see #contributionCount
	 * @return the contributions
	 */
	public Integer contributionCount() {
		return this.contributionCount;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#prePersist()
	 */
	@Override
	protected void prePersist() {
		super.prePersist();

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#postLoad()
	 */
	@Override
	protected void postLoad() {
		super.postLoad();
		this.personId = id().getPersonId();
	}

	public static class FIELD {
		public static final String PERSON_ID = PersonWebApiUri.PERSON_ID;
		public static final String GROUP_ID = SocialGroupWebApiUri.SOCIAL_GROUP_ID;
		public static final String DOMAIN = "membership";

		public static class KEY {
			public static final FieldKey ID = new FieldKey(DOMAIN, BaseEntity.BASE_FIELD.ID);
			public static final FieldKey PERSON_ID = new FieldKey(DOMAIN, FIELD.PERSON_ID);
			public static final FieldKey GROUP_ID = new FieldKey(DOMAIN, FIELD.GROUP_ID);
		}
	}

	@Singleton
	public static class SocialGroupXmlAdapter
			extends IdentifierToIdentifiableXmlAdapter<SocialGroup, SocialGroupId> {
		public SocialGroupXmlAdapter() {
		}

		public SocialGroupXmlAdapter(Function<SocialGroupId, SocialGroup> provider) {
			super(provider);
		}

		@Inject
		public SocialGroupXmlAdapter(SocialGroupProvider socialGroupProvider) {
			super(socialGroupProvider.idFunction());
		}
	}

	// request scoped
	public static class PersonXmlAdapter
			extends PersonIdXmlAdapter {
		public PersonXmlAdapter() {
		}

		public PersonXmlAdapter(Function<PersonId, Person> provider, IdentityManager identityManager) {
			super(provider, identityManager);
		}

		@Inject
		public PersonXmlAdapter(PersonProvider provider, IdentityManager identityManager) {
			super(provider, identityManager);
		}
	}

}
