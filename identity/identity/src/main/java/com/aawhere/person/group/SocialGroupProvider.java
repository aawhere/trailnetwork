/**
 * 
 */
package com.aawhere.person.group;

import com.aawhere.persist.ServiceStandard;

/**
 * Interface to provide an Identified {@link SocialGroup}.
 * 
 * @author aroller
 * 
 */
public interface SocialGroupProvider
		extends ServiceStandard<SocialGroups, SocialGroup, SocialGroupId> {

}
