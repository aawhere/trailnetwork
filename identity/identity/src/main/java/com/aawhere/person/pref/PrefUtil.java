/**
 *
 */
package com.aawhere.person.pref;

import java.net.URL;
import java.util.Locale;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.lang3.LocaleUtils;

import com.aawhere.i18n.LocaleUtil;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.ResourceUtils;
import com.aawhere.person.Person;

/**
 * Utility methods for Preferences. Preferences are implemented using Apache Configuration (
 * {@link Configuration}).
 * 
 * @author Brian Chapman
 * 
 */
public class PrefUtil {

	private PrefUtil() {
	}

	private static final String DEFAULT_SI_PREFS = "default-si-preferences.xml";
	private static final String DEFAULT_US_CUSTOMARY_PREFS = "default-us-customary-preferences.xml";

	public static Locale getLocale(Configuration prefs) {
		String prefsLocale = prefs.getString(Preferences.LOCALE.getKey());
		if (prefsLocale == null) {
			throw new RuntimeException("Locale must be set in the preferences");
		}
		return LocaleUtils.toLocale(prefsLocale);
	}

	/**
	 * Set the locale for the given {@link Configuration} (aka Preferences).
	 * 
	 * NOTE: This is not threadsafe. {@link XMLConfugration}.
	 * 
	 * NOTE2: Typically the default preferences is shared between threads, so best practices are
	 * that a seperate Configuration holds the Locale and a {@link CompositeConfiguration} combines
	 * the two.
	 * 
	 * @param locale
	 * @param prefs
	 */
	public static void setLocale(Locale locale, Configuration prefs) {
		String localeString = locale.toString();
		prefs.addProperty(Preferences.LOCALE.getKey(), localeString);
	}

	public static Configuration getDefaultPreferences(Locale locale) {
		if (locale.equals(Locale.US)) {
			return PrefUtil.getDefaultUsCustomaryPreferences();
		} else {
			return PrefUtil.getDefaultSiPreferences();
		}
	}

	public static Configuration getDefaultSiPreferences() {
		URL resourceUrl = getResource(DEFAULT_SI_PREFS);
		return getPreferencesFromResource(resourceUrl);
	}

	public static Configuration getDefaultUsCustomaryPreferences() {
		URL resourceUrl = getResource(DEFAULT_US_CUSTOMARY_PREFS);
		return getPreferencesFromResource(resourceUrl);
	}

	public static Configuration getEmptyPreferences() {
		return new XMLConfiguration();
	}

	public static Configuration createPreferencesWith(Locale locale) {
		Configuration prefs = getEmptyPreferences();
		setLocale(locale, prefs);
		return prefs;
	}

	private static Configuration getPreferencesFromResource(URL url) {
		XMLConfiguration prefs;
		try {
			prefs = new XMLConfiguration(url);
		} catch (ConfigurationException e) {
			// You should never be here.
			throw new RuntimeException(e);
		}
		return prefs;
	}

	private static URL getResource(String resourceName) {
		return ResourceUtils.resourceFromPackage(PrefUtil.class, resourceName);
	}

	/**
	 * Get composite preferences for locale, default and {@link Person} preferences.
	 * 
	 * @param locale
	 * @param defaultPrefs
	 * @param personPrefs
	 * @return
	 */
	public static Configuration getPreferences(Locale locale, Configuration defaultPrefs, Configuration personPrefs) {
		LocaleUtil.validateLocale(locale);
		Assertion.assertNotNull(defaultPrefs);
		Assertion.assertNotNull(personPrefs);
		CompositeConfiguration prefs = new CompositeConfiguration();
		prefs.addConfiguration(personPrefs);
		prefs.addConfiguration(createPreferencesWith(locale));
		prefs.addConfiguration(defaultPrefs);
		return prefs;
	}

	/**
	 * Get composite preferences for locale and default preferences.
	 * 
	 * @param locale
	 * @param defaultPrefs
	 * @param personPrefs
	 * @return
	 */
	public static Configuration getPreferences(Locale locale, Configuration defaultPrefs) {
		LocaleUtil.validateLocale(locale);
		Assertion.assertNotNull(defaultPrefs);
		CompositeConfiguration prefs = new CompositeConfiguration();
		prefs.addConfiguration(createPreferencesWith(locale));
		prefs.addConfiguration(defaultPrefs);
		return prefs;
	}

	/**
	 * Get composite preferences for locale. Looks up the defaultPrefs for you based on the provided
	 * Locale.
	 * 
	 * @param locale
	 * @return
	 */
	public static Configuration getPreferences(Locale locale) {
		LocaleUtil.validateLocale(locale);
		return getPreferences(locale, getDefaultPreferences(locale));
	}

	private static Configuration getPreferences(UnitOfMeasure uom) {
		if (uom.equals(UnitOfMeasure.METRIC)) {
			return getDefaultSiPreferences();
		} else {
			return getDefaultUsCustomaryPreferences();
		}
	}

	/**
	 * Does the @{link Preferences} have a {@link Preferences#LOCALE} set?
	 * 
	 * @param prefs
	 * @return
	 */
	public static boolean hasLocale(Configuration prefs) {
		return prefs.containsKey(Preferences.LOCALE.getKey());
	}

	/**
	 * Builder for creating a preference based on a variety of preference sources. Preferences will
	 * be provided in this order, person's preferences, unit of measure preferences, locale based
	 * preferences (SI or US Customary)
	 * 
	 * Essentially the same as Composite locale->UOM->Person where person overrides UOM and Locale.
	 * 
	 * Locale is required on all builds, the other parameters will be ignored if empty.
	 * 
	 * @return
	 */
	public static class PrefBuilder
			extends ObjectBuilder<Configuration> {

		private Locale locale;
		private UnitOfMeasure unitOfMeasure;
		private Person person;

		public PrefBuilder() {
			super(new CompositeConfiguration());
		}

		public PrefBuilder locale(Locale locale) {
			this.locale = locale;
			return this;
		}

		public PrefBuilder unitOfMeasure(UnitOfMeasure uom) {
			unitOfMeasure = uom;
			return this;
		}

		public PrefBuilder person(Person person) {
			this.person = person;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotNull("locale", this.locale);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public Configuration build() {
			CompositeConfiguration prefs = (CompositeConfiguration) super.build();
			if (person != null && person.hasPreferences()) {
				prefs.addConfiguration(person.preferences());
			}
			if (unitOfMeasure != null) {
				prefs.addConfiguration(getPreferences(unitOfMeasure));
			}
			prefs.addConfiguration(getPreferences(locale));

			return prefs;
		}
	}// end Builder

	public static PrefBuilder createPrefBuilder() {
		return new PrefBuilder();
	}

}
