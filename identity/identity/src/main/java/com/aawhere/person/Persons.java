/**
 *
 */
package com.aawhere.person;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

/**
 * Collection of {@link Person}s.
 * 
 * @author Brian Chapman
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
public class Persons
		extends BaseFilterEntities<Person> {

	/**
	 * Use Builder to construct Activities
	 */
	private Persons() {
		super();
	}

	@XmlElement(name = Person.FIELD.DOMAIN)
	@Override
	public List<Person> getCollection() {
		return super.getCollection();
	}

	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, Person, Persons> {

		public Builder() {
			super(new Persons());
		}

		Builder(Persons mutant) {
			super(mutant, mutant.getCollection());
		}

	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @param persons
	 * @return
	 */
	static Builder mutate(Persons persons) {
		return new Builder(persons);
	}
}
