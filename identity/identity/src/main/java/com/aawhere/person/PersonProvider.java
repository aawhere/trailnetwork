/**
 * 
 */
package com.aawhere.person;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceStandard;

/**
 * General interface for any service that can produce a {@link Person} given it's {@link PersonId}.
 * 
 * @author aroller
 * 
 */
public interface PersonProvider
		extends ServiceStandard<Persons, Person, PersonId> {
	Person person(PersonId id) throws EntityNotFoundException;
}
