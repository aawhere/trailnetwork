/**
 * 
 */
package com.aawhere.person;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.UnknownApplicationException;
import com.aawhere.persist.EntityMessage;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * Localizable messages for the person package.
 * 
 * @author Brian Chapman
 * 
 */
public enum PersonMessage implements Message {

	/** @see UnknownApplicationException */
	UNKNOWN_APPLICATION(
			"The application with a key of \"{APPLICATION_KEY}\" is unknown.  Choose from: {VALID_APPLICATIONS}.",
			Param.APPLICATION_KEY,
			Param.VALID_APPLICATIONS),
	ID_NAME(EntityMessage.ID_NAME.getValue()),
	ID_DESCRIPTION(EntityMessage.ID_DESCRIPTION.getValue()),
	/* */
	PERSON_NAME("Person"),
	/* */
	PERSON_DESCRIPTION("A representation of a human on earth that has multiple Accounts at various Applications."),
	ACCOUNTS_NAME("Accounts"),
	ACCOUNTS_DESCRIPTION("Registrations made at an Application."),
	NAME_NAME("Name"),
	NAME_DESCRIPTION("The person's name."),
	DATE_CREATED_NAME(EntityMessage.DATE_CREATED_NAME.getValue()), /* */
	DATE_CREATED_DESCRIPTION(EntityMessage.DATE_CREATED_DESCRIPTION.getValue()), /* */
	DATE_UPDATED_NAME(EntityMessage.DATE_UPDATED_NAME.getValue()), /* */
	DATE_UPDATED_DESCRIPTION(EntityMessage.DATE_UPDATED_DESCRIPTION.getValue());

	private ParamKey[] parameterKeys;
	private String message;

	private PersonMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** @see ApplicationKey */
		APPLICATION_KEY, /**
		 * An array of enumerations that represent the valid choices.
		 */
		VALID_APPLICATIONS;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static final class Doc {
		public static final String DATA_TYPE = "Person";
		public static final String EXPAND_ALLOWABLE_VALUES = PersonField.Absolute.IDENTITY_SHARED;
	}
}
