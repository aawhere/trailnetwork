/**
 * 
 */
package com.aawhere.person.group;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.id.LongIdentifier;
import com.aawhere.persist.ServicedBy;
import com.aawhere.xml.XmlNamespace;

/**
 * å
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@ServicedBy(SocialGroupProvider.class)
public class SocialGroupId
		extends LongIdentifier<SocialGroup> {

	private static final long serialVersionUID = 8476949962857374734L;

	/**
	 * 
	 */
	SocialGroupId() {
		super(SocialGroup.class);
	}

	/**
	 * @param value
	 * @param kind
	 */
	public SocialGroupId(String value) {
		super(value, SocialGroup.class);
	}

	public SocialGroupId(Long value) {
		super(value, SocialGroup.class);
	}

}
