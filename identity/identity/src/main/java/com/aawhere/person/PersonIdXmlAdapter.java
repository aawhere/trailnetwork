/**
 * 
 */
package com.aawhere.person;

import java.util.Map;

import com.aawhere.id.IdentifierToIdentifiableXmlAdapter;
import com.aawhere.id.IdentifiersToIdentifiablesXmlAdapterProvider;
import com.aawhere.identity.IdentityManager;
import com.aawhere.identity.IdentityUtil;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.google.inject.Inject;

/**
 * All Persons being expanded must extend this so it can provide proper access control.
 * 
 * @author aroller
 * 
 */
// request scoped
public class PersonIdXmlAdapter
		extends IdentifierToIdentifiableXmlAdapter<Person, PersonId>
		implements IdentifiersToIdentifiablesXmlAdapterProvider<PersonId, Person> {

	private final PersonProvider provider;
	private final IdentityManager identityManager;

	/**
	 * 
	 */
	public PersonIdXmlAdapter() {
		this.provider = null;
		this.identityManager = null;
	}

	/**
	 * A function that can provide the persons to be requested.
	 * 
	 * @param provider
	 */
	public PersonIdXmlAdapter(Function<PersonId, Person> provider, IdentityManager identityManager) {
		super(provider);
		this.identityManager = identityManager;
		// xmlAdapterPreLoaded won't work with this method
		this.provider = null;
	}

	@Inject
	public PersonIdXmlAdapter(PersonProvider provider, IdentityManager identityManager) {
		super(PersonUtil.personFunction(provider));
		this.provider = provider;
		this.identityManager = identityManager;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.id.IdentifiersToIdentifiablesXmlAdapterProvider#xmlAdapterPreloaded(java.lang
	 * .Iterable)
	 */
	@Override
	public Map<PersonId, Person> xmlAdapterPreloaded(Iterable<PersonId> ids) {
		if (provider == null) {
			throw new IllegalStateException(
					"provider is null so the incorrect constructor was called to use this method");
		}
		// must enforce access, but do so without blocking now
		return Maps.transformValues(provider.all(ids), IdentityUtil.<Person> enforceAccessFunction(identityManager));
	}

	@Override
	public Person marshal(PersonId id) throws Exception {
		if (this.identityManager != null) {
			return this.identityManager.enforceAccess(super.marshal(id));
		} else {
			return super.marshal(id);
		}
	}

}
