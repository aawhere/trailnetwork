package com.aawhere.person;

import com.aawhere.app.account.AccountId;
import com.aawhere.identity.Identity;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.persist.EntityNotFoundException;

/**
 * Provides identity capabilities related to an account if the account itself can answer the
 * questions, otherwise it uses the {@link PersonAccountProvider} to get the person and answer the
 * questsions using it's building in {@link Identity}.
 * 
 * @author aroller
 * 
 */
public class AccountIdentity
		implements Identity {

	private AccountId accountId;
	private PersonAccountProvider provider;
	private Person person;

	/**
	 * Used to construct all instances of AccountIdentity.
	 */
	public static class Builder
			extends ObjectBuilder<AccountIdentity> {

		public Builder() {
			super(new AccountIdentity());
		}

		public Builder accountId(AccountId accountId) {
			building.accountId = accountId;
			return this;
		}

		public Builder provider(PersonAccountProvider provider) {
			building.provider = provider;
			return this;
		}

		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("provider", building.provider);
			Assertion.exceptions().notNull("accountId", building.accountId);
		}

	}// end Builder

	public static Builder create() {
		return new Builder();
	}

	/** Use {@link Builder} to construct AccountIdentity */
	private AccountIdentity() {
	}

	@Override
	public Boolean identityShared() {
		if (this.person == null) {
			try {
				this.person = provider.person(accountId);
			} catch (EntityNotFoundException e) {
				throw new ToRuntimeException(e);
			}
		}
		return this.person.identityShared();
	}

	@Override
	public Boolean isOwner(Person requestingAccess) {
		if (this.person != null) {
			return this.person.isOwner(requestingAccess);
		}
		// saves time requesting since the person provides all account ids
		return requestingAccess.accountIds().contains(accountId);
	}

}
