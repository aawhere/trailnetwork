package com.aawhere.person.pref;

/**
 *
 */

import java.util.NoSuchElementException;

import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang3.StringUtils;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.xml.MeasurementXmlUtil;

/**
 * Utility methods for working with {@link Quantity} and {@link Measurement} in {@link Preferences}
 * (implemented using Apache {@link Configuration}).
 * 
 * @author Brian Chapman
 * 
 */
public class PrefMeasureUtil {

	/**
	 *
	 */
	private static final String DIGIT_SUFFIX = "-digit";
	/**
	 *
	 */
	private static final String UNIT_SUFFIX = "-unit";
	/**
	 *
	 */
	private static final String QUANTITY_PREFIX = "quantity-";

	@SuppressWarnings("unchecked")
	public static <Q extends Quantity<Q>> Unit<Q> getUnitFor(Class<Q> quantityType, Configuration prefs) {
		String type = MeasurementXmlUtil.typeFor(quantityType);
		String preferenceKey = buildUnitKey(quantityType);
		return (Unit<Q>) getUnitForKey(type, preferenceKey, prefs);
	}

	/**
	 * @param type
	 * @return
	 */
	static <Q extends Quantity<Q>> String buildUnitKey(Class<Q> type) {
		return QUANTITY_PREFIX + MeasurementXmlUtil.typeFor(type) + UNIT_SUFFIX;
	}

	static <Q extends Quantity<Q>> String buildDigitKey(Class<Q> type) {
		return QUANTITY_PREFIX + MeasurementXmlUtil.typeFor(type) + DIGIT_SUFFIX;
	}

	@SuppressWarnings("unchecked")
	public static <Q extends Quantity<Q>> Unit<Q> getBaseUnitFor(Class<Q> quantityType, Configuration prefs) {
		Unit<? extends Quantity<?>> unit = getUnitFor(quantityType, prefs);
		return (Unit<Q>) getBaseUnit(unit);
	}

	public static <Q extends Quantity<Q>> int getMaxDigitsFor(Class<Q> quantityType, Configuration prefs) {
		String type = MeasurementXmlUtil.typeFor(quantityType);
		String key = QUANTITY_PREFIX + type + DIGIT_SUFFIX;
		return prefs.getInt(key);
	}

	private static Unit<? extends Quantity<?>> getUnitForKey(String type, String preferenceKey, Configuration prefs) {
		try {
			String preferenceValue = prefs.getString(preferenceKey);
			if (preferenceValue == null) {
				throw new NullPointerException("Could not get unit from preferences (type: " + type
						+ ", preferenceKey: " + preferenceKey + ")");
			}
			Unit<? extends Quantity<?>> unit = MeasurementUtil.getUnitFromSymbol(preferenceValue);
			return unit;
		} catch (NoSuchElementException e) {
			throw new RuntimeException("Could not find a preference for quantity type (" + preferenceKey
					+ ") Make sure your default preferences contains a key for all "
					+ "expected quantity types. List of available Preferences: "
					+ StringUtils.join(prefs.getKeys(), "\n"), e);
		}
	}

	/*
	 * newer versions of javax.measure have this method built in to Unit.
	 */
	private static <Q extends Quantity<Q>> Unit<Q> getBaseUnit(Unit<Q> unit) {
		return unit.toMetric();
	}

}
