/**
 * 
 */
package com.aawhere.person.group;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.persist.BaseFilterEntities;
import com.aawhere.xml.XmlNamespace;

/**
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE, name = SocialGroupWebApiUri.SOCIAL_GROUPS)
@XmlAccessorType(XmlAccessType.NONE)
public class SocialGroups
		extends BaseFilterEntities<SocialGroup> {

	/**
	 * Used to construct all instances of SocialGroups.
	 */
	@XmlTransient
	public static class Builder
			extends BaseFilterEntities.Builder<Builder, SocialGroup, SocialGroups> {

		public Builder() {
			super(new SocialGroups());
		}

		@Override
		public SocialGroups build() {
			SocialGroups built = super.build();
			return built;
		}

	}// end Builder

	/** Use {@link Builder} to construct SocialGroups */
	private SocialGroups() {
	}

	public static Builder create() {
		return new Builder();
	}

	@XmlElement(name = SocialGroup.FIELD.DOMAIN)
	public List<SocialGroup> getCollection() {
		return super.getCollection();
	}
}
