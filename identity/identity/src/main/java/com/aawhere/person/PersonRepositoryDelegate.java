/**
 * 
 */
package com.aawhere.person;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.RepositoryDelegate;
import com.aawhere.person.Person.Builder;

/**
 * @author aroller
 * 
 */
public class PersonRepositoryDelegate
		extends RepositoryDelegate<PersonRepository, Persons, Person, PersonId>
		implements PersonRepository {

	/**
	 * @param repository
	 */
	public PersonRepositoryDelegate(PersonRepository repository) {
		super(repository);
	}

	Person.Builder mutate(PersonId personId) throws EntityNotFoundException {
		return Person.mutate(load(personId));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.person.PersonRepository#setName(com.aawhere.person.PersonId,
	 * java.lang.String)
	 */
	@Override
	public Person name(PersonId personId, String name, String nameWithAliases) throws EntityNotFoundException {
		return update(mutate(personId).name(name).nameWithAliases(nameWithAliases));
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.person.PersonRepository#addMergedIds(java.lang.Iterable)
	 */
	@Override
	public Person aliasesAdded(PersonId personId, Iterable<PersonId> personIds) throws EntityNotFoundException {
		return update(mutate(personId).aliasesAdded(personIds));
	}

	@Override
	public Builder update(PersonId personId) throws EntityNotFoundException {
		return new Builder(load(personId)) {
			@Override
			public Person build() {
				return update(super.build());
			}
		};
	}
}
