package com.aawhere.person;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;

import com.aawhere.app.Application;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountIdsXmlAdapter;
import com.aawhere.app.account.EmailAddress;
import com.aawhere.collections.CollectionUtilsExtended;
import com.aawhere.collections.FunctionsExtended;
import com.aawhere.field.FieldKey;
import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.field.type.FieldDataType;
import com.aawhere.identity.AccessControlled;
import com.aawhere.identity.AccessScope;
import com.aawhere.identity.AccessScopedXmlAdapter;
import com.aawhere.identity.Identity;
import com.aawhere.identity.IdentityManager;
import com.aawhere.identity.IdentityRole;
import com.aawhere.lang.Assertion;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.LongBaseEntity;
import com.aawhere.persist.ServicedBy;
import com.aawhere.search.Searchable;
import com.aawhere.xml.XmlNamespace;
import com.aawhere.xml.bind.OptionalPassThroughXmlAdapter;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;
import com.googlecode.objectify.condition.IfDefault;
import com.googlecode.objectify.condition.IfNotNull;
import com.googlecode.objectify.condition.IfNull;
import com.googlecode.objectify.condition.IfTrue;
import com.googlecode.objectify.condition.IfFalse;
import com.googlecode.objectify.condition.PojoIf;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * A representation of a human on earth. Unlike a {@link Account} which represents a registration at
 * a {@link Application} it is intended that a Person object is 1:1 with the actual person that is
 * participating and 1:Many with the Accounts they own. Some accounts may be at the same
 * application.
 * 
 * @author roller
 * 
 */
@Cache
@Unindex
@Entity
@Searchable(fieldKeys = { Person.FIELD.NAME_WITH_ALIASES, PersonField.NAME })
@Dictionary(domain = Person.FIELD.DOMAIN, messages = PersonMessage.class)
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
@XmlAccessorType(XmlAccessType.NONE)
@ServicedBy(PersonProvider.class)
@AccessControlled(scope = AccessScope.UNRESTRICTED)
public final class Person
		extends LongBaseEntity<Person, PersonId>
		implements Identity {

	private static final long serialVersionUID = 5288582245642865242L;
	@Field(key = FIELD.NAME, indexedFor = "TN-682", xmlAdapter = NameXmlAdapter.class)
	@IgnoreSave(IfNull.class)
	@Nullable
	@Index(IfNotNull.class)
	@ApiModelProperty(value = "Full name of Person.")
	@XmlElement(name = PersonField.NAME)
	@AccessControlled(scope = AccessScope.PUBLIC_READ)
	private String name;

	@IgnoreSave(IfNameWithAliasesWorthy.class)
	@Field(key = FIELD.NAME_WITH_ALIASES, searchable = true, xmlAdapter = NameWithAliasesXmlAdapter.class)
	@AccessControlled(scope = AccessScope.PUBLIC_READ)
	@XmlElement
	private String nameWithAliases;

	@Index
	@Field(key = FIELD.ACCOUNT_ID, parameterType = AccountId.class, indexedFor = "TN-676",
			xmlAdapter = AccountIdsXmlAdapter.class)
	@XmlElement(name = FIELD.ACCOUNT_ID)
	private Set<AccountId> accounts;

	@Index
	@Field(key = FIELD.ALIASES, parameterType = PersonId.class, indexedFor = "TN-678")
	@XmlElement(name = FIELD.ALIASES)
	private Set<PersonId> aliases;

	/**
	 * True means the name, username and other personal information is o.k. to share.
	 */
	@Field(xmlAdapter = IdentitySharedXmlAdapter.class, indexedFor = "WW-250")
	@XmlAttribute
	@XmlJavaTypeAdapter(IdentitySharedXmlAdapter.class)
	@IgnoreSave(IfDefault.class)
	// WW-317 switched this to opt-out for demonstration purposes
	@Index(IfFalse.class)
	@ApiModelProperty(value = "Indicates this user has authorized to expose the name and other private information.")
	private Boolean identityShared = true;

	@Field
	@ApiModelProperty(value = "Email address of the Person.")
	@Nullable
	@XmlElement(name = PersonField.EMAIL)
	@AccessControlled(scope = AccessScope.OWNER_READ)
	private EmailAddress email;

	@Field
	@ApiModelProperty(value = "Photo URL of the Person")
	@Nullable
	@XmlElement(name = PersonField.PHOTO_URL)
	@AccessControlled(scope = AccessScope.OWNER_READ)
	private URL photoUrl;

	@Field(parameterType = IdentityRole.class)
	@XmlElement
	@ApiModelProperty(value = "Set of IdentityRoles the person belongs to.")
	@AccessControlled(scope = AccessScope.OWNER_READ)
	private Set<IdentityRole> identityRoles = Sets.newHashSet();

	@XmlTransient
	// TN-885 addresses the persistence of these preferences.
	@Ignore
	private Configuration preferences;

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#getId()
	 */
	@Override
	@com.aawhere.field.annotation.Field(key = BaseEntity.BASE_FIELD.ID, dataType = FieldDataType.ATOM,
			indexedFor = "Repository.load()", searchable = true)
	public PersonId getId() {
		return super.getId();
	}

	public String getName() {
		return name;
	}

	/**
	 * @return the {@link #identityShared}
	 */
	@Override
	public Boolean identityShared() {
		return this.identityShared;
	}

	@Override
	public Boolean isOwner(Person person) {
		return this.equals(person) || (this.hasAliases() && aliases.contains(person.id()));
	}

	/**
	 * @return the email
	 */
	public EmailAddress email() {
		return this.email;
	}

	public URL photoUrl() {
		return this.photoUrl;
	}

	public String name() {
		return this.name;
	}

	/** Provides the names with account aliases. */
	public String nameWithAliases() {
		return nameWithAliases;
	}

	/**
	 * @return the accounts
	 */
	public Set<AccountId> accountIds() {
		return this.accounts;
	}

	@XmlElement(name = Account.FIELD.ACCOUNTS)
	@XmlJavaTypeAdapter(AccountIdsXmlAdapter.class)
	@Field(key = Account.FIELD.DOMAIN, parameterType = Account.class)
	public Set<AccountId> getAccountIds() {
		return accountIds();
	}

	/**
	 * @return the identityRoles
	 */
	public Set<IdentityRole> identityRoles() {
		return this.identityRoles;
	}

	/**
	 * Although {@link #aliases} can be null, this will always return a set including at least
	 * {@link #id()} for convenience. use {@link #hasAliases()} if you wish to know if any aliases
	 * besides me has been assigned.
	 * 
	 * @return
	 */
	@Nonnull
	public Set<PersonId> aliases() {
		if (this.aliases == null) {
			return ImmutableSet.of(id());
		} else {
			return ImmutableSet.copyOf(this.aliases);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntity#postLoad()
	 */
	@Override
	protected void postLoad() {
		super.postLoad();
		if (this.nameWithAliases == null) {
			this.nameWithAliases = this.name;
		}
	}

	/**
	 * indicates if {@link #aliases} is null.
	 * 
	 * @see #aliases()
	 * @return
	 */
	public Boolean hasAliases() {
		return CollectionUtils.isNotEmpty(aliases);
	}

	public Configuration preferences() {
		return preferences;
	}

	public Boolean hasPreferences() {
		return preferences != null;
	}

	/**
	 * Used to construct all instances of Person.
	 */
	@XmlTransient
	public static class Builder
			extends LongBaseEntity.Builder<Person, Builder, PersonId> {

		public Builder() {
			this(new Person());
		}

		Builder(Person person) {
			super(person);
		}

		@Field(key = PersonField.NAME)
		public Builder name(String name) {
			building.name = name;
			return this;
		}

		/** adds the given account to the set enforcing no duplicates. */
		public Builder account(Account account) {
			if (building.accounts == null) {
				building.accounts = Sets.newHashSet();
			}
			building.accounts.add(account.id());
			return this;
		}

		public Builder preferences(Configuration preferences) {
			building.preferences = preferences;
			return this;
		}

		@Field(key = PersonField.IDENTITY_ROLES)
		public Builder identityRoles(Set<IdentityRole> roles) {
			building.identityRoles = roles;
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.assertNotEmpty(FIELD.ACCOUNT_ID, building.accounts);

		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public Person build() {

			final Person built = super.build();
			if (built.nameWithAliases == null) {
				built.nameWithAliases = built.name;
			}
			return built;
		}

		/**
		 * @param accountIds
		 */
		public Builder accountIdsAdded(Set<AccountId> accountIds) {
			building.accounts.addAll(accountIds);
			return this;
		}

		/**
		 * Absorbs the properties worth keeping from the given person.
		 * 
		 * TN-6778
		 * 
		 * @param person
		 */
		public Builder merge(Person person) {
			accountIdsAdded(person.accountIds());
			// make the person an alias
			aliasesAdded(Sets.newHashSet(person.id()));
			// inherit the other persons aliases too
			aliasesAdded(person.aliases());
			// cannot determine which name is better here so leaving that up to a client
			if (building.name == null) {
				// unless we don't have one
				building.name = person.name;
			}
			return this;
		}

		/**
		 * @param personIds
		 * @return
		 */
		Builder aliasesAdded(Iterable<PersonId> personIds) {
			if (building.aliases == null) {
				building.aliases = new HashSet<PersonId>();
				// identifying thyself in the merged makes easy queries
				building.aliases.add(id());
			}
			CollectionUtilsExtended.addAll(building.aliases, personIds);
			return this;
		}

		/**
		 * @param right
		 * @return
		 */
		public Builder nameWithAliases(String nameWithAliases) {
			building.nameWithAliases = nameWithAliases;
			return this;
		}

		/**
		 * @param value
		 * @return
		 */
		@Field(key = PersonField.IDENTITY_SHARED)
		public Builder identityShared(Boolean shared) {
			building.identityShared = shared;
			return this;
		}

		/**
		 * 
		 * @param email
		 * @return
		 */
		@Field(key = PersonField.EMAIL)
		public Builder email(EmailAddress email) {
			building.email = email;
			return this;
		}

		@Field(key = PersonField.PHOTO_URL)
		public Builder photoUrl(URL photoUrl) {
			building.photoUrl = photoUrl;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct Person */
	private Person() {
	}

	public static Builder create() {
		return new Builder();
	}

	/* friends */static Builder mutate(Person person) {
		return new Builder(person);
	}

	public static class FIELD {

		public static final String EMAIL = "email";
		public static final String ALIASES = "aliases";
		public static final String NAME = "name";
		public static final String DOMAIN = "person";
		public static final String ACCOUNT_ID = Account.FIELD.ACCOUNT_ID;
		public static final String PERSON_ID = PersonWebApiUri.PERSON_ID;
		public static final String PERSONS = DOMAIN + "s";
		public static final String NAME_WITH_ALIASES = "nameWithAliases";

		public static final class KEY {
			public static final FieldKey NAME = new FieldKey(FIELD.DOMAIN, FIELD.NAME);
			public static final FieldKey ACCOUNT_IDS = new FieldKey(FIELD.DOMAIN, FIELD.ACCOUNT_ID);
			public static final FieldKey MERGED_IDS = new FieldKey(FIELD.DOMAIN, FIELD.ALIASES);
		}

		public static final class OPTION {
			public static final String ACCOUNT = new FieldKey(FIELD.DOMAIN, Account.FIELD.DOMAIN).getAbsoluteKey();
		}
	}

	/**
	 * true if the {@link Person#name()} is the same as {@link Person#nameWithAliases()}. or if
	 * aliases is null.
	 * 
	 * @author aroller
	 * 
	 */
	public static class IfNameWithAliasesWorthy
			extends PojoIf<Person> {

		/*
		 * (non-Javadoc)
		 * @see com.googlecode.objectify.condition.If#matchesPojo(java.lang.Object)
		 */
		@Override
		public boolean matchesPojo(Person person) {
			return person.nameWithAliases == null || person.nameWithAliases.equals(person.name);

		}

	}

	@Singleton
	private static class IdentitySharedXmlAdapter
			extends OptionalPassThroughXmlAdapter<Boolean> {
		private IdentitySharedXmlAdapter() {
			super(Visibility.HIDE);
		}

		@Inject
		private IdentitySharedXmlAdapter(Show show) {
			super(show);
		}
	}

	// request scoped
	static class NameXmlAdapter
			extends AccessScopedXmlAdapter<String, Person> {
		public NameXmlAdapter() {
		}

		@Inject
		public NameXmlAdapter(IdentityManager identityManager) {
			super(identityManager, AccessScope.PUBLIC_READ, FunctionsExtended.stringEmptyFunction(PersonUtil
					.nameFunction()));
		}

		@Override
		protected String accessDenied(Person from) {
			return StringUtils.EMPTY;
		}
	}

	static class NameWithAliasesXmlAdapter
			extends AccessScopedXmlAdapter<String, Person> {
		public NameWithAliasesXmlAdapter() {
		}

		@Inject
		public NameWithAliasesXmlAdapter(IdentityManager identityManager) {
			super(identityManager, AccessScope.PUBLIC_READ, FunctionsExtended.stringEmptyFunction(PersonUtil
					.nameWithAliasesFunction()));
		}

		@Override
		protected String accessDenied(Person from) {
			return StringUtils.EMPTY;
		}
	}

}
