/**
 * 
 */
package com.aawhere.person.group;

import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.swagger.DocSupport;
import com.aawhere.text.format.custom.ClassFormat;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

public enum SocialGroupMembershipMessage implements Message {

	/** Comments go here. */
	MESSAGE_1("something {FIRST} for {WITH_FORMAT}.", Param.FIRST, Param.WITH_FORMAT);

	private ParamKey[] parameterKeys;
	private String message;

	private SocialGroupMembershipMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	@XmlTransient
	public enum Param implements ParamKey {
		FIRST, WITH_FORMAT(new ClassFormat());

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

	public static final class Doc {
		public static final String EXAPAND_ALLOWABLE = SocialGroupMembershipField.Absolute.PERSON_ID
				+ DocSupport.Allowable.NEXT + SocialGroupMembershipField.Absolute.SOCIAL_GROUP_ID;
	}
}
