/**
 *
 */
package com.aawhere.measure.xml;

import javax.measure.quantity.Quantity;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * @author Aaron Roller
 * 
 */
public class QuantityTypeKey {

	private BiMap<String, Class<? extends Quantity<?>>> keyTypeMap = HashBiMap.create();

	private static QuantityTypeKey instance = new QuantityTypeKey();;

	/**
	 * Used to retrieve the only instance of QuantityTypeKey.
	 */
	public static QuantityTypeKey getInstance() {
		return instance;

	}// end Builder

	/** Use {@link #getInstance()} to get QuantityTypeKey */
	QuantityTypeKey() {
	}

	/**
	 * Given the type of quantity this will return a key
	 * 
	 * @param type
	 * @return
	 */
	public String keyFor(Class<? extends Quantity<?>> type) {
		String result = keyTypeMap.inverse().get(type);
		if (result == null) {
			result = StringUtils.uncapitalize(type.getSimpleName());
			keyTypeMap.put(result, type);
		}
		return result;
	}

	/**
	 * Given a key, this will return the associated {@link Class} for {@link Quantity} that the key
	 * represents.
	 * 
	 * @param key
	 * @return
	 */
	public Class<? extends Quantity<?>> typeFor(String key) {
		Class<? extends Quantity<?>> result = this.keyTypeMap.get(key);
		if (result == null) {

			try {
				result = instantiate(key, "javax.measure.quantity.");
			} catch (ClassNotFoundException e) {
				try {
					result = instantiate(key, "com.aawhere.measure.");
				} catch (ClassNotFoundException e2) {
					throw new RuntimeException(e2);
				}
			}
			this.keyTypeMap.put(key, result);
		}
		return result;

	}

	/**
	 * @param key
	 * @param packageName
	 * @return
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private Class<Quantity<?>> instantiate(String key, final String packageName) throws ClassNotFoundException {
		return (Class<Quantity<?>>) Class.forName(packageName + StringUtils.capitalize(key));
	}
}
