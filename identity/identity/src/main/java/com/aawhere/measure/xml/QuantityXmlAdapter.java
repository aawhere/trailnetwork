/**
 *
 */
package com.aawhere.measure.xml;

import java.text.ParsePosition;

import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;

import com.aawhere.identity.IdentityManager;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.unit.format.UnitFormatFactory;
import com.aawhere.personalize.BaseQuantityPersonalizer;
import com.aawhere.personalize.QuantityPersonalized;
import com.aawhere.personalize.QuantityPersonalizedPersonalizer;
import com.aawhere.personalize.QuantityPersonalizer;
import com.aawhere.personalize.xml.PersonalizedXmlAdapter;

import com.google.inject.Inject;

/**
 * @author Aaron Roller
 * 
 */
@SuppressWarnings("rawtypes")
public class QuantityXmlAdapter
		extends PersonalizedXmlAdapter<QuantityPersonalized, Quantity> {

	public QuantityXmlAdapter() {
		super(new QuantityPersonalizedPersonalizer.Builder());
	}

	public QuantityXmlAdapter(BaseQuantityPersonalizer<?> plizer) {
		super(plizer);
	}

	@Inject
	public QuantityXmlAdapter(IdentityManager identityManager) {
		super(new QuantityPersonalizer.Builder().setPreferences(identityManager.preferences()).build());
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Quantity showUnmarshal(QuantityPersonalized xml) throws Exception {
		final Unit unit = UnitFormatFactory.getInstance().createDefaultUnitFormat()
				.parse(xml.getUnit(), new ParsePosition(0));
		final Class<? extends Quantity> type = QuantityTypeKey.getInstance().typeFor(xml.getType());
		final Double value = xml.getValue();
		return MeasurementUtil.quantityOrNull(value, unit, type);
	}

}
