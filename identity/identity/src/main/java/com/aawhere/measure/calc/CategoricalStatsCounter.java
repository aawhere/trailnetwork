/**
 * 
 */
package com.aawhere.measure.calc;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.lang.Assertion;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.number.NumberUtilsExtended;
import com.aawhere.lang.string.ToStringXmlAdapter;
import com.aawhere.xml.XmlNamespace;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;

/**
 * Builds the results for {@link ActivityTypeCount} and {@link ActivityTypePopularity} when given
 * {@link Activities}.
 * 
 * An optional predicate can be provided that will filter the results to include only those matching
 * the predicate. This is done after counting so an incomplete result set is provided since the
 * total will remain the same as will the counts and percentages so the final result will not equal
 * 100% if any item is removed by the filter.
 * 
 * @author aroller
 * 
 */
@XmlRootElement
@XmlType(namespace = XmlNamespace.API_VALUE)
public class CategoricalStatsCounter<S extends CategoricalStatistic<C>, C> {

	/**
	 * Used to construct all instances of CategoricalStatsCounter.
	 */
	@XmlTransient
	public static class Builder<S extends CategoricalStatistic<C>, C, B extends CategoricalStatistic.Builder<S, C, B>>
			extends ObjectBuilder<CategoricalStatsCounter<S, C>> {
		private Iterable<C> categories;
		private Supplier<B> statsBuilderSupplier;
		/**
		 * A {@link Function} or chain of Functions that will be called once the results are
		 * calculated, but before predicates used to filter the results.
		 * 
		 */
		private Function<SortedSet<S>, SortedSet<S>> preFilterPostProcessor;
		/**
		 * Optional predicate that will filter after counting.
		 * 
		 */
		@Nullable
		private Predicate<S> predicate;

		public Builder() {
			super(new CategoricalStatsCounter<S, C>());
		}

		public Builder<S, C, B> categories(Iterable<C> categories) {
			this.categories = categories;
			return this;
		}

		public Builder<S, C, B> statsBuilder(Supplier<B> statsBuilderSupplier) {
			this.statsBuilderSupplier = statsBuilderSupplier;
			return this;
		}

		public Builder<S, C, B> predicate(Predicate<S> predicate) {
			if (this.predicate == null) {
				this.predicate = predicate;
			} else {
				this.predicate = Predicates.and(this.predicate, predicate);
			}
			return this;
		}

		public Builder<S, C, B> preFilterPostProcessor(Function<SortedSet<S>, SortedSet<S>> postProcessor) {
			if (this.preFilterPostProcessor == null) {
				this.preFilterPostProcessor = postProcessor;
			} else {
				Functions.compose(this.preFilterPostProcessor, postProcessor);
			}
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("categories", this.categories);
			Assertion.exceptions().notNull("statsBuilder", this.statsBuilderSupplier);
		}

		@Override
		public CategoricalStatsCounter<S, C> build() {
			CategoricalStatsCounter<S, C> built = super.build();
			Multiset<C> countingSet = HashMultiset.create(this.categories);
			Integer total = countingSet.size();
			SortedSet<S> counts = new TreeSet<>();
			for (C activityType : countingSet) {
				// TN-728 avoid nulls
				if (activityType != null) {
					int count = countingSet.count(activityType);
					B statsBuilder = statsBuilderSupplier.get();
					S stats = statsBuilder.count(count).total(total).category(activityType).build();
					counts.add(stats);
				}
			}

			if (this.preFilterPostProcessor != null) {
				counts = this.preFilterPostProcessor.apply(counts);
			}
			// filter the results if the optional predicate is provided.
			// notice the total is not affected.
			if (this.predicate != null) {
				Iterable<S> filtered = Iterables.filter(counts, this.predicate);
				counts = Sets.newTreeSet(filtered);
			}

			built.stats = Collections.unmodifiableSortedSet(counts);
			built.total = countingSet.size();
			return built;
		}

	}// end Builder

	@XmlJavaTypeAdapter(ToStringXmlAdapter.class)
	private SortedSet<S> stats;

	/**
	 * The total number of ActivityTypes counted. This value equals all values in {@link #stats()}
	 * put together.
	 * 
	 */
	@XmlAttribute
	private Integer total;

	/** Use {@link Builder} to construct CategoricalStatsCounter */
	protected CategoricalStatsCounter() {
	}

	/** Generics get mean using this so consider doing a new on the Builder() */
	public static <T extends CategoricalStatistic<C>, C, B extends CategoricalStatistic.Builder<T, C, B>>
			Builder<T, C, B> create() {
		return new Builder<T, C, B>();
	}

	/**
	 * @return the stats
	 */
	public SortedSet<S> stats() {
		return this.stats;
	}

	/**
	 * @return the total
	 */
	public Integer total() {
		return this.total;
	}

	/** provide the activity type this will return the count or null if not present. */
	public S stats(C type) {
		final Predicate<C> equalTo = Predicates.equalTo(type);
		final Function<S, C> categoryFunction = CategoricalStatistic.categoryFunction();
		final Predicate<S> equalToType = Predicates.compose(equalTo, categoryFunction);
		return Iterables.getFirst(Iterables.filter(stats, equalToType), null);
	}

	/**
	 * Adds all the stats together into a single sum.
	 * 
	 * @param stats
	 * @return
	 */
	public static <T extends CategoricalStatistic<C>, C> Integer sum(final Iterable<T> counts) {
		return NumberUtilsExtended.sum(Iterables.transform(counts, CategoricalStatistic.countFunction()));
	}

	/**
	 * Given some iterable of homogenous stats that may or may not contain the same categories this
	 * will cumulate the counts for each.
	 * 
	 * @param stats
	 * @param builder
	 * @return
	 */
	public static <T extends CategoricalStatistic<C>, C, B extends CategoricalStatistic.Builder<T, C, B>> SortedSet<T>
			cumulation(Iterable<T> stats, Supplier<B> statsBuilderSupplier) {
		// null stats must be ignored
		stats = Iterables.filter(stats, Predicates.notNull());

		HashMap<C, Integer> counts = new HashMap<>();
		int total = 0;
		for (T t : stats) {
			Integer count = counts.get(t.category());
			if (count == null) {
				count = 0;
			}
			count += t.count();
			total += count;
			counts.put(t.category(), count);
		}

		Set<Entry<C, Integer>> countEntries = counts.entrySet();
		TreeSet<T> results = new TreeSet<>();
		for (Entry<C, Integer> entry : countEntries) {
			B builder = statsBuilderSupplier.get();
			builder.category(entry.getKey());
			builder.count(entry.getValue());
			builder.total(total);
			results.add(builder.build());
		}

		return results;
	}
}
