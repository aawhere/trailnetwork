/**
 * 
 */
package com.aawhere.measure.xml;

import java.text.NumberFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.lang.ObjectBuilder;

/**
 * Used by JAXB Marshalling to transform values into personalized values.
 * 
 * 
 * 
 * @author Aaron Roller
 * 
 */
public class NumberPersonalizedXmlAdapter
		extends XmlAdapter<String, Number> {

	/**
	 * Used to construct all instances of NumberPersonalizedXmlAdapter.
	 */
	public static class Builder
			extends ObjectBuilder<NumberPersonalizedXmlAdapter> {

		public Builder() {
			super(new NumberPersonalizedXmlAdapter());
		}

		public Builder formatter(NumberFormat formatter) {
			building.formatter = formatter;
			return this;
		}

	}// end Builder

	/** Use {@link Builder} to construct NumberPersonalizedXmlAdapter */
	private NumberPersonalizedXmlAdapter() {
	}

	/**
	 * Just an example...actual may use CustomFormat, etc
	 * 
	 */
	private NumberFormat formatter = NumberFormat.getNumberInstance();

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Number unmarshal(String formatted) throws Exception {
		return formatter.parse(formatted);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Number value) throws Exception {

		return formatter.format(value);
	}

}
