@XmlJavaTypeAdapters({ @XmlJavaTypeAdapter(value = QuantityXmlAdapter.class, type = Quantity.class) })
@XmlSchema(xmlns = { @XmlNs(prefix = com.aawhere.xml.XmlNamespace.API_PREFIX,
		namespaceURI = com.aawhere.xml.XmlNamespace.API_VALUE) })
package com.aawhere.measure;

import javax.measure.quantity.Quantity;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import com.aawhere.measure.xml.QuantityXmlAdapter;

