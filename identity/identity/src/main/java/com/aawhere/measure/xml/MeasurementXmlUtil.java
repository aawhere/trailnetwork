/**
 *
 */
package com.aawhere.measure.xml;

import javax.measure.quantity.Quantity;

import com.aawhere.measure.MeasurementUtil;

/**
 * @author roller
 * 
 */
public class MeasurementXmlUtil {

	/**
	 * return a formatted string representing the type of quantity. Also see
	 * {@link MeasurementUtil#classFor(Quantity)}
	 */
	public static <Q extends Quantity<?>> String typeFor(Q quantity) {
		return typeFor(MeasurementUtil.classFor(quantity));
	}

	/**
	 * return a formatted string representing the type of quantity. Also see
	 * {@link MeasurementUtil#classFor(Quantity)}
	 * 
	 * @see QuantityTypeKey
	 */
	public static <Q extends Quantity<?>> String typeFor(Class<Q> quantityType) {
		return QuantityTypeKey.getInstance().keyFor(quantityType);
	}
}
