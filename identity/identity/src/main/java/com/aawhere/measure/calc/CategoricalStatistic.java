/**
 * 
 */
package com.aawhere.measure.calc;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aawhere.field.annotation.Dictionary;
import com.aawhere.field.annotation.Field;
import com.aawhere.lang.AbstractObjectBuilder;
import com.aawhere.lang.Assertion;
import com.aawhere.log.LoggerFactory;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.xml.CountFromIntegerXmlAdapter;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import com.googlecode.objectify.annotation.Ignore;

/**
 * A simple base class used to report the {@link #count} of the occurrences of {@link #category()}
 * out of a {@link #total} providing the {@link #percent()}.
 * 
 * The {@link #category()} must adhere to the rules {@link #equals(Object)} and {@link #hashCode()}.
 * 
 * Since total is common across all stats in a Set this does not persist total, but relies on the
 * container of this to store it and provide it before use #postLod
 * 
 * @author aroller
 * 
 * @param <C>
 *            the category this calculates statistics for.
 */
@Dictionary(domain = "categoricalStatistic")
abstract public class CategoricalStatistic<C>
		implements Serializable, Comparable<CategoricalStatistic<C>> {

	private static final long serialVersionUID = -6459797948817927780L;

	/**
	 * Used to construct all instances of ActivityTypeCount.
	 */
	protected abstract static class Builder<T extends CategoricalStatistic<C>, C, B extends Builder<T, C, B>>
			extends AbstractObjectBuilder<T, B> {

		protected Builder(T stats) {
			super(stats);

		}

		public B count(Integer count) {
			building().count = count;
			return dis;
		}

		abstract public B category(C category);

		/**
		 * @return
		 */
		private CategoricalStatistic<C> building() {
			return building;
		}

		public B total(Integer total) {
			building().total = total;
			return dis;
		}

		/**
		 * Extracts standard information from the {@link CategoricalStatistic} that is common to
		 * all. A specific implementation that provides more should override this and add extras.
		 * 
		 * @see #copy(CategoricalStatistic)
		 * @param differentCategoryStat
		 * @return
		 */
		public B copyDifferent(CategoricalStatistic<?> differentCategoryStat) {
			total(differentCategoryStat.total);
			count(differentCategoryStat.count);

			return dis;
		}

		/**
		 * Extracts information from another without using {@link #clone()}.
		 * 
		 * @see #copyDifferent(CategoricalStatistic)
		 * @param other
		 * @return
		 */
		public B copy(CategoricalStatistic<C> other) {
			copyDifferent(other);
			category(other.category());
			return dis;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#validate()
		 */
		@Override
		protected void validate() {
			super.validate();
			Assertion.exceptions().notNull("count", building().count);
			Assertion.exceptions().notNull("total", building().total);
			Assertion.exceptions().notNull("type", building.category());

		}

		@SuppressWarnings("unchecked")
		@Override
		public T build() {
			CategoricalStatistic<C> built = super.build();
			return (T) built;
		}

	}// end Builder

	@XmlElement
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Field
	private Integer count;

	/**
	 * The total count of all activity types across this set of activity type counts.
	 * 
	 * Not persisted because it can be assigned by the container when held in a collection.
	 * 
	 */
	@XmlElement
	@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
	@Ignore
	@Field
	private Integer total;

	abstract public C category();

	/**
	 * @return the count
	 */
	public Integer count() {
		return this.count;
	}

	/**
	 * @return the total
	 */
	public Integer total() {
		return this.total;
	}

	/**
	 * @return the percent
	 */
	@Field
	public Ratio percent() {
		return MeasurementUtil.ratio(count, total);
	}

	/**
	 * Called by children to mutate this by providing a total. To protect from actual mutation this
	 * will only assign a total if none has yet been assigned so it's more like a completing of a
	 * build than a mutation. Use the builder to change total.
	 * 
	 * @param total
	 */
	protected void postLoad(Integer total) {
		if (this.total == null) {
			this.total = total;
		} else {
			LoggerFactory.getLogger(getClass()).warning(total + " is being ignored since total already exists: "
					+ this.total);
		}
	}

	@XmlElement
	public Ratio getPercent() {
		return percent();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return category() + "(" + this.count.toString() + ")";
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.count == null) ? 0 : this.count.hashCode());
		result = prime * result + ((category() == null) ? 0 : category().hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		CategoricalStatistic<C> other = (CategoricalStatistic<C>) obj;
		if (this.count == null) {
			if (other.count() != null)
				return false;
		} else if (!this.count.equals(other.count()))
			return false;
		if (this.category() != other.category())
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CategoricalStatistic<C> that) {
		// equal count must not return or else it will clobber in a hash
		// use something else that is consistent
		if (this.count.equals(that.count())) {
			// well, we don't want equality and comparison must be consistent
			// categories likely hae a decent toString representation of the category
			// secondary sorting will be consisting
			return this.category().toString().compareTo(that.category().toString());
		} else {
			// purposely reversed to go highest first
			return that.count().compareTo(this.count);
		}
	}

	public static <C extends CategoricalStatistic<T>, T> Function<C, T> categoryFunction() {
		return new Function<C, T>() {

			@Override
			public T apply(C input) {
				return (input == null) ? null : input.category();
			}
		};
	}

	public static <C extends CategoricalStatistic<?>> Function<C, Integer> countFunction() {
		return new Function<C, Integer>() {

			@Override
			public Integer apply(C input) {
				return (input == null) ? null : input.count();
			}
		};
	}

	public static <C extends CategoricalStatistic<?>> Function<C, Ratio> percentFunction() {
		return new Function<C, Ratio>() {

			@Override
			public Ratio apply(C input) {
				return (input == null) ? null : input.percent();
			}
		};
	}

	/**
	 * Given the stats and a desired range this will return only those results that satisfy that
	 * range.
	 * 
	 * @param stats
	 * @param range
	 * @return
	 */
	public static <C extends CategoricalStatistic<?>> Iterable<C>
			filterByPercent(Iterable<C> stats, Range<Ratio> range) {
		Predicate<C> predicate = percentPredicate(range);
		return Iterables.filter(stats, predicate);

	}

	/**
	 * Provides the stats that has the given category or null if none found. Returns the first in
	 * the order of iteration if there are multiple, which there typically shouldn't be.
	 * 
	 * @return the stats matching the given category or null if not found
	 * */
	@Nullable
	public static <S extends CategoricalStatistic<C>, C extends Comparable<C>> S statsForCategory(Iterable<S> stats,
			C category) {
		final Function<CategoricalStatistic<C>, C> categoryFunction = categoryFunction();
		Predicate<CategoricalStatistic<C>> predicate = Predicates.compose(	Predicates.equalTo(category),
																			categoryFunction);
		return Iterables.getFirst(Iterables.filter(stats, predicate), null);
	}

	/**
	 * Given the range this will return the predicate that can be used to filter the results to
	 * match only the range.
	 * 
	 * @param range
	 * @return
	 */
	public static <C extends CategoricalStatistic<?>> Predicate<C> percentPredicate(Range<Ratio> range) {
		final Function<C, Ratio> percentFunction = percentFunction();
		Predicate<C> predicate = Predicates.compose(range, percentFunction);
		return predicate;
	}

	public static <C extends CategoricalStatistic<T>, T> Set<T> categories(Iterable<C> stats) {
		final Function<C, T> categoryFunction = categoryFunction();
		return Sets.newHashSet(Iterables.transform(stats, categoryFunction));
	}

	public static class FIELD {
		public static final String PERCENT = "percent";

	}

	/**
	 * Adds the count "from" to "into" keeping the category of "into" while adding the counts and
	 * keeping the total after ensuring each agree on the total.
	 * 
	 * @param from
	 *            the discard
	 * @param into
	 *            the keeper
	 * @param builderSupplier
	 *            provides the builder for the result
	 */
	public static <S extends CategoricalStatistic<C>, C extends Comparable<C>, B extends Builder<S, C, B>> S merged(
			S from, S into, Supplier<B> builderSupplier) {
		// if they don't have the same total then they aren't coming from the same batch and
		// shouldn't be merged.
		Assertion.exceptions().eq(from.total(), into.total());
		return builderSupplier.get().category(into.category()).total(into.total()).count(from.count() + into.count())
				.build();
	}

	/**
	 * Called to accommodate the storage of total in a single place this will assign the total to
	 * every stats in an iterable. Any existing total will remain since this is not meant for
	 * mutation, but completion of an incomplete entity.
	 * 
	 * @param all
	 * @param total
	 */
	public static <S extends CategoricalStatistic<?>> void postLoad(Iterable<S> all, Integer total) {
		if (all != null) {
			for (S s : all) {
				s.postLoad(total);
			}
		}
	}
}
