/**
 *
 */
package com.aawhere.measure.xml;

import com.aawhere.identity.IdentityManager;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.personalize.BaseQuantityPersonalizer;
import com.aawhere.personalize.QuantityPersonalized;
import com.aawhere.personalize.QuantityPersonalizedPersonalizer;
import com.aawhere.personalize.QuantityPersonalizer;
import com.aawhere.personalize.xml.PersonalizedXmlAdapter;

import com.google.common.base.Function;
import com.google.inject.Inject;

/**
 * Allows an Integer to be transformed into a quantity and back again.
 * 
 * @author aroller
 * 
 */
public class CountFromIntegerXmlAdapter
		extends PersonalizedXmlAdapter<QuantityPersonalized, Integer> {

	public CountFromIntegerXmlAdapter() {
		super(new QuantityPersonalizedPersonalizer.Builder());
	}

	public CountFromIntegerXmlAdapter(BaseQuantityPersonalizer<?> plizer) {
		super(plizer);
	}

	@Inject
	public CountFromIntegerXmlAdapter(IdentityManager identityManager) {
		super(new QuantityPersonalizer.Builder().setPreferences(identityManager.preferences()).build());
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Integer showUnmarshal(QuantityPersonalized v) throws Exception {
		return (v != null) ? v.getValue().intValue() : null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public QuantityPersonalized showMarshal(Integer v) throws Exception {
		if (v != null) {
			getPersonalizer().personalize(MeasurementUtil.count(v));
			return (QuantityPersonalized) getPersonalizer().getResult();
		} else {
			return null;
		}
	}

	public Function<Integer, QuantityPersonalized> function() {
		return new Function<Integer, QuantityPersonalized>() {

			@Override
			public QuantityPersonalized apply(Integer input) {
				try {
					return showMarshal(input);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		};
	}
}
