/**
 * 
 */
package com.aawhere.config;

import java.net.URL;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

import com.aawhere.lang.ResourceUtils;

/**
 * Useful methods with {@link Configuration}s for our system.
 * 
 * @author roller
 * 
 */
public class ConfigurationUtil {

	/**
	 * Useful method to get standard XML configurations matching the name of the given class.
	 * 
	 * @param targetClass
	 * @return
	 */
	public static Configuration getConfiguration(Class<?> targetClass) {
		final URL url = ResourceUtils.resourceFromClass(targetClass, "xml");
		try {
			return new XMLConfiguration(url);
		} catch (ConfigurationException e) {
			throw new RuntimeException(url.getPath(), e);
		}
	}
}
