/**
 * 
 */
package com.aawhere.view;

import static com.aawhere.lang.number.NumberUtilsExtended.*;
import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.id.IdentifierTestUtil;
import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.MockRepository;
import com.aawhere.view.ViewCount.Builder;

/**
 * @author aroller
 * 
 */
public class ViewCountUnitTest
		extends BaseEntityJaxbBaseUnitTest<ViewCount, ViewCount.Builder, ViewCountId, ViewCountRepository> {

	private ChildId counting;
	private ViewCountId viewCountId;
	private Integer expectedCount;

	/**
	 * 
	 */
	public ViewCountUnitTest() {
		super(MUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		this.counting = IdentifierTestUtil.generateRandomChildId();
		this.viewCountId = new ViewCountId(counting);
		this.expectedCount = 0;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.counting(counting);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.Identifier)
	 */
	@Override
	protected ViewCount mutate(ViewCountId id) throws EntityNotFoundException {
		final ViewCount mutant = super.mutate(id);
		getRepository().increment(id);
		this.expectedCount++;
		return mutant;

	}

	@Test
	public void testIncrementNew() throws EntityNotFoundException {
		getRepository().increment(viewCountId);
		ViewCount found = getRepository().load(viewCountId);
		assertEquals(this.viewCountId, found.id());
		assertEquals("didn't increment", I(1), found.count());
	}

	@Test
	public void testId() {
		assertEquals("id values aren't being handled properly", counting.getValue(), this.viewCountId.countingIdValue());
		assertEquals(this.viewCountId, new ViewCountId(this.viewCountId.getValue()));
		assertEquals(this.viewCountId, new ViewCountId(this.viewCountId.toString()));
		assertEquals(this.viewCountId.getValue(), new ViewCountId(this.viewCountId.getValue()).getValue());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(ViewCount mutated) {
		super.assertMutation(mutated);
		assertEquals(this.expectedCount, mutated.count());
		assertEquals(this.viewCountId, mutated.id());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertSample(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertSample(ViewCount sample) {
		super.assertSample(sample);
		assertEquals(viewCountId, sample.id());

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return AccountTestUtil.adapters();
	}

	public static class ViewCountMockRepository
			extends MockRepository<ViewCount, ViewCountId>
			implements ViewCountRepository {
		private ViewCountRepositoryDelegate delegate = new ViewCountRepositoryDelegate(this);

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.view.ViewCountRepository#increment(com.aawhere.view.ViewCountId)
		 */
		@Override
		public Integer increment(ViewCountId id) {
			return delegate.increment(id);
		}

	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected ViewCountRepository createRepository() {
		return new ViewCountMockRepository();
	}

}
