package com.aawhere.util.rb;

import static org.junit.Assert.*;

import java.util.Locale;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.person.pref.PrefUtil;
import com.aawhere.personalize.xml.XmlLang;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;
import com.aawhere.xml.bind.JAXBTestUtil.Builder;

/**
 * @author roller
 * 
 */
public class MessagePersonalizerUnitTest {

	/**
	 *
	 */
	private static final Locale DEFAULT_LOCALE = Locale.US;
	MessagePersonalizer personalizer;

	@Before
	public void setUp() {
		Configuration prefs = PrefUtil.getPreferences(DEFAULT_LOCALE, PrefUtil.getDefaultSiPreferences());
		personalizer = new MessagePersonalizer.Builder().setPreferences(prefs).build();
	}

	/**
	 * Runs the personalization without providing a custom adapter.
	 * 
	 */
	@Test
	public void testDefaultJaxbRoundTrip() {
		assertJaxbRoundTrip(null);

	}

	@Test
	public void testJaxbRoundTrip() throws Exception {
		final MessagePersonalizedXmlAdapter adapter = getAdapter();
		assertJaxbRoundTrip(adapter);
	}

	/**
	 * @param adapter
	 */
	private void assertJaxbRoundTrip(final MessagePersonalizedXmlAdapter adapter) {
		Root expected = new Root();
		final ExampleMessage messageEnum = ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE;
		expected.message = messageEnum;
		final Builder<Root> builder = JAXBTestUtil.create(expected);
		if (adapter != null) {
			builder.adapter(adapter);
		}
		JAXBTestUtil<Root> util = builder.build();
		util.assertContains("root");
		util.assertContains(messageEnum.getValue());
		util.assertContains(messageEnum.name());
		util.assertContains(XmlLang.ATTRIBUTE_NAME);
		assertEquals(util.getXml(), expected.message, util.getActual().message);
	}

	@Test
	public void testPersonalizer() {
		ExampleMessage message = ExampleMessage.MESSAGE_IN_FILE;
		final String expectedValue = MessageResourceBundleUnitTest.MESSAGE_IN_FILE_VALUE;
		final Locale locale = DEFAULT_LOCALE;
		final String expectedKey = message.name();

		assertPersonalizer(message, expectedValue, locale, expectedKey);
	}

	/**
	 * @param message
	 * @param expectedValue
	 * @param locale
	 * @param expectedKey
	 */
	private void assertPersonalizer(ExampleMessage message, final String expectedValue, final Locale locale,
			final String expectedKey) {
		// override the default during these tests.
		Configuration prefs = PrefUtil.getPreferences(locale, PrefUtil.getDefaultSiPreferences());
		personalizer = new MessagePersonalizer.Builder().setPreferences(prefs).build();
		personalizer.personalize(message);
		MessageDisplay display = personalizer.getResult();
		assertEquals(expectedValue, display.value);
		assertEquals(locale, display.locale);
		TestUtil.assertContains(display.key, expectedKey);
	}

	@Test
	public void testPersonalizerAlternateLanguage() {
		assertPersonalizer(	ExampleMessage.MESSAGE_IN_FILE,
							MessageResourceBundleUnitTest.MESSAGE_IN_FILE_VALUE_ZU,
							MessageResourceBundleUnitTest.TEST_LOCALE,
							ExampleMessage.MESSAGE_IN_FILE.name());
	}

	@Test
	public void testXmlAdapter() throws Exception {

		MessagePersonalizedXmlAdapter adapter = getAdapter();
		final ExampleMessage expected = ExampleMessage.MESSAGE_IN_FILE;
		MessageDisplay display = adapter.marshal(expected);
		Message actual = adapter.unmarshal(display);
		assertEquals(expected, actual);
	}

	public MessagePersonalizedXmlAdapter getAdapter() {
		return new MessagePersonalizedXmlAdapter(personalizer);
	}

	@XmlRootElement
	public static class Root {
		@XmlElement
		@XmlJavaTypeAdapter(MessagePersonalizedXmlAdapter.class)
		Message message;
	}

	public static MessagePersonalizerUnitTest getInstance() {
		MessagePersonalizerUnitTest instance = new MessagePersonalizerUnitTest();
		instance.setUp();
		return instance;
	}

}
