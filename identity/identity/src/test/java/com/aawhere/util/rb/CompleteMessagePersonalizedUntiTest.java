package com.aawhere.util.rb;

import static org.junit.Assert.*;

import java.util.Locale;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.person.pref.PrefUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.CompleteMessagePersonalizer.CompleteMessagePersonalized;

public class CompleteMessagePersonalizedUntiTest {

	/**
	 *
	 */
	private static final Locale DEFAULT_LOCALE = Locale.US;
	CompleteMessagePersonalizer personalizer;

	@Before
	public void setUp() {
		Configuration prefs = PrefUtil.getPreferences(DEFAULT_LOCALE, PrefUtil.getDefaultSiPreferences());
		personalizer = new CompleteMessagePersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testNull() {
		personalizer.personalize(null);
	}

	@Test
	public void testSimpleMessage() {
		personalizer.personalize(CompleteMessage.create(ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE).build());
		CompleteMessagePersonalized result = personalizer.getResult();
		assertNotNull(result.message);
		assertNull(result.params);
		assertNotNull(result.personalized);
	}

	@Test
	public void testWithParams() {
		String firstName = TestUtil.generateRandomAlphaNumeric();
		personalizer.personalize(CompleteMessage.create(ExampleMessage.MESSAGE_IN_FILE_WITH_PARAMS)
				.param(ExampleMessage.Param.FIRST_NAME, firstName).build());
		TestUtil.assertContains(personalizer.getResult().personalized, firstName);
	}
}
