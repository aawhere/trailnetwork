package com.aawhere.field.xml;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldExpansionNotPossibleException;
import com.aawhere.field.FieldTestUtil;
import com.aawhere.field.FieldsRequestedOnInvalidDictionary;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.xml.FieldXmlAdapterService.Builder;
import com.aawhere.field.xml.FieldXmlAdapterService.InjectorProxy;
import com.aawhere.field.xml.FieldXmlAdapterService.MarshallerProxy;
import com.aawhere.identity.IdentityManager;
import com.aawhere.identity.IdentityTestUtil;
import com.aawhere.lang.If;
import com.aawhere.persist.BaseEntities;
import com.aawhere.persist.Entities;
import com.aawhere.persist.EntityTestUtil;
import com.aawhere.persist.EntityTestUtil.StringMockEntityService;
import com.aawhere.persist.LongMockEntities;
import com.aawhere.persist.LongMockEntity;
import com.aawhere.persist.LongMockEntity.CountXmlAdapter;
import com.aawhere.persist.StringMockEntity;
import com.aawhere.persist.StringMockEntityId;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.inject.ConfigurationException;
import com.google.inject.spi.Message;

/**
 * Tests {@link FieldXmlAdapterService} using only java and no web resources.
 * 
 * 
 * @author aroller
 * 
 */
@SuppressWarnings("rawtypes")
public class FieldXmlAdapterServiceUnitTest {

	private static final String ADAPTABLE_FIELD = LongMockEntity.FIELD.KEY.COUNT.getAbsoluteKey();

	private List<String> fieldsRequested;

	/**
	 * The top level object that is the root element being returned in the web service
	 * (LongMockEntity, StringMockEntity or LongMockEntities)
	 */
	private Object root;
	/**
	 * The object that must correspond to the {@link #fieldsRequested}. This will be passed into the
	 * corresponding marshaller returned.
	 * 
	 */
	private Object fieldValue;
	private MarshallerProxyForTesting marshaller;
	private Class<? extends Exception> expectedFailure;
	/** Keeps track of the adapters for the marshaller..setting and getting. */
	private Map<Class<? extends XmlAdapter>, XmlAdapter> marshallerAdapters;
	private Map<Class<?>, Object> injectorInstances;
	private CountXmlAdapter expectedReplacement;

	@Before
	public void setUp() {
		this.fieldsRequested = Lists.newArrayList();
		this.marshaller = new MarshallerProxyForTesting();
		LongMockEntity entity = EntityTestUtil.generateRandomLongEntity();
		this.root = entity;
		this.fieldValue = entity.getCount();
		this.marshallerAdapters = Maps.newHashMap();
		this.injectorInstances = Maps.newHashMap();

	}

	@After
	public void test() throws Exception {

		FieldAnnotationDictionaryFactory fieldAnnotationDictionaryFactory = FieldTestUtil
				.getFieldAnnotationDictionaryFactory();
		FieldDictionaryFactory fieldDictionaryFactory = fieldAnnotationDictionaryFactory.register(LongMockEntity.class)
				.register(StringMockEntity.class).getFactory();
		Builder builder = FieldXmlAdapterService.create();
		builder.dictionaryFactory(fieldDictionaryFactory).fieldsRequested(fieldsRequested).injector(new Injector())
				.marshaller(this.marshaller);

		// jersey receives either the type that is being marshalled, but if we detect a full
		// entities then we scape the collection and request bulk..one or the other
		if (root instanceof BaseEntities<?>) {
			builder.entities((Collection<?>) this.root);
		} else {
			assertNotNull("root", this.root);
			builder.typeBeingMarshalled(this.root.getClass());
		}
		try {
			builder.buildWithExceptions();
			TestUtil.assertNullExplanation("failure was expected", this.expectedFailure);

			if (!fieldsRequested.isEmpty()) {

				Iterator<XmlAdapter> adaptersIterator = this.marshallerAdapters.values().iterator();
				assertTrue(	fieldsRequested + " were requested, but no adapter provided for marshalling " + root,
							adaptersIterator.hasNext());
				XmlAdapter replacementAdapter = adaptersIterator.next();
				// iterables should do a bulk load where the xml adapter provides a "pre-loaded" map
				// avoiding individual calls to the datastore or cache
				if (root instanceof Iterable<?>) {
					// this only supports retrieval of string mock entity by id
					TestUtil.assertInstanceOf(Iterable.class, fieldValue);
					Iterable<StringMockEntityId> iterable = (Iterable<StringMockEntityId>) this.fieldValue;
					Iterable<StringMockEntity> expectedValues = Iterables.transform(iterable, EntityTestUtil
							.stringMockEntityFunction());
					for (Object valueFromIterable : iterable) {
						Object adaptedResult = replacementAdapter.marshal(valueFromIterable);
						TestUtil.assertContains(adaptedResult, expectedValues);
					}

				} else {
					Object adaptedResult = replacementAdapter.marshal(fieldValue);
					assertEquals("adaptation produced wrong results", String.valueOf(fieldValue), adaptedResult);
				}
			}

		} catch (Exception e) {
			if (!(this.expectedFailure != null && expectedFailure.isAssignableFrom(e.getClass()))) {
				throw (e);
			}
		}
		// providing an instance means you expect adapter replacment of that type
		if (this.expectedReplacement != null) {
			CountXmlAdapter replacement = this.marshaller.getAdapter(this.expectedReplacement.getClass());
			assertEquals("the adapter didn't get replaced properly", expectedReplacement, replacement);
		}

	}

	@Test
	public void testNoFieldsRequested() {
		// nothing to do..this is the default
	}

	@Test
	public void testExpansionOfNonExpandableField() {
		this.fieldsRequested.add(LongMockEntity.FIELD.KEY.COMPARABLE.getAbsoluteKey());
		this.expectedFailure = FieldNotExpandableException.class;
	}

	@Test
	public void testEntities() {
		LongMockEntity item1 = LongMockEntity.create().build();
		LongMockEntity item2 = LongMockEntity.create().build();
		LongMockEntities entities = new LongMockEntities.Builder().add(item1).add(item2).build();

		this.root = entities;
		this.fieldValue = Lists.transform(entities.getCollection(), EntityTestUtil.relatedFunction());
		this.fieldsRequested.add(LongMockEntity.FIELD.KEY.RELATED.getAbsoluteKey());
		this.injectorInstances.put(StringMockEntityService.class, new EntityTestUtil.StringMockEntityService());
		this.injectorInstances.put(IdentityManager.class, IdentityTestUtil.admin());
	}

	@Test
	public void testEntitiesWithoutServicedBy() {
		this.root = new Entities.Builder<StringMockEntity>(Lists.newArrayList(EntityTestUtil
				.generateRandomStringEntity())).build();
		this.fieldsRequested.add(StringMockEntity.FIELD.KEY.NAME.getAbsoluteKey());
		this.expectedFailure = IllegalArgumentException.class;

	}

	@Test
	public void testExpansionOfSingleFieldOnEntity() {
		expecingReplacement();
		requestField();
	}

	private void requestField() {
		this.fieldsRequested.add(ADAPTABLE_FIELD);
	}

	/**
	 * this configures all that is necessary to setup for replacement of the default adapter
	 * provided by jaxb with the adapter provided by the injector.
	 */
	private void expecingReplacement() {
		this.expectedReplacement = new LongMockEntity.CountXmlAdapter();
		Class<? extends CountXmlAdapter> adapterType = expectedReplacement.getClass();
		this.injectorInstances.put(adapterType, expectedReplacement);
	}

	@Test
	public void testNoSingletonRegistered() {
		requestField();
		this.expectedFailure = ConfigurationException.class;
	}

	@Test
	public void testWrongTypeIsTargeted() {
		requestField();
		this.root = EntityTestUtil.generateRandomStringEntity();
		this.expectedFailure = FieldExpansionNotPossibleException.class;
	}

	@Test
	public void testFieldNotADictionary() {
		this.root = "Not a dictionary";
		requestField();
		this.expectedFailure = FieldsRequestedOnInvalidDictionary.class;
	}

	@Test
	@Ignore("TN-884 manually tested using web, but the identity manager should be formalized and tested")
	public void testRestrictedAccess() {
		// TODO:write the test to enforce
	}

	/**
	 * The marshaller shouldn't already have an XmlAdapter of this type. if it does then an
	 * exception must be thrown.
	 * 
	 */
	@Test
	public void testDualAdapterConfiguration() {
		requestField();
		this.marshaller.setAdapter(countAdapter());
		this.expectedFailure = IllegalStateException.class;
	}

	private CountXmlAdapter countAdapter() {
		return new CountXmlAdapter();
	}

	public class MarshallerProxyForTesting
			implements MarshallerProxy {

		@Override
		public void setAdapter(XmlAdapter adapter) {
			marshallerAdapters.put(adapter.getClass(), adapter);

		}

		@SuppressWarnings("unchecked")
		@Override
		public <A extends XmlAdapter> A getAdapter(Class<A> type) {
			return (A) marshallerAdapters.get(type);
		}

	}

	public class Injector
			implements InjectorProxy {

		@SuppressWarnings("unchecked")
		@Override
		public <T> T getInstance(Class<T> type) {
			T result = (T) injectorInstances.get(type);
			return If.nil(result).raise(new ConfigurationException(Lists.newArrayList(new Message(
					"The instance is not found " + type))));
		}

	}

}
