/**
 * 
 */
package com.aawhere.config;

import static org.junit.Assert.assertEquals;

import java.util.MissingResourceException;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;

import com.aawhere.config.ConfigurationUtil;

/**
 * @author roller
 * 
 */
public class ConfigurationUtilUnitTest {

	@Test
	public void testBasics() throws Exception {
		Configuration config = ConfigurationUtil.getConfiguration(getClass());
		String actual = config.getString("basics");
		String expected = "test";
		assertEquals(expected, actual);
	}

	@Test(expected = MissingResourceException.class)
	public void testMissing() {
		ConfigurationUtil.getConfiguration(ConfigurationUtil.class);
	}
}
