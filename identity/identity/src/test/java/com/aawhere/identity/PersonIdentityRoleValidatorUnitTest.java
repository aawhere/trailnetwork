/**
 * 
 */
package com.aawhere.identity;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.person.Person;
import com.aawhere.person.PersonTestUtil;
import com.google.common.collect.Sets;

/**
 * Unit tests for {@link PersonIdentityRoleValidator}
 * 
 * @author Brian Chapman
 * 
 */
public class PersonIdentityRoleValidatorUnitTest {

	@Test
	public void testIsInRole() {
		Person person = PersonTestUtil.builder().identityRoles(Sets.newHashSet(IdentityRole.admin)).build();
		PersonIdentityRoleValidator validator = PersonIdentityRoleValidator.create().person(person).build();
		assertTrue(validator.isUserInRole(IdentityRole.admin));
		assertTrue(!validator.isUserInRole(IdentityRole.user));
	}

	@Test
	public void testIsInUserRoleUserWithoutRoles() {
		Person person = PersonTestUtil.person();
		PersonIdentityRoleValidator validator = PersonIdentityRoleValidator.create().person(person).build();
		assertTrue(!validator.isUserInRole(IdentityRole.admin));
		assertTrue(!validator.isUserInRole(IdentityRole.user));
	}
}
