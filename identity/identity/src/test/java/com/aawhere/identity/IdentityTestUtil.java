/**
 *
 */
package com.aawhere.identity;

import java.util.Locale;

import com.aawhere.app.account.AccountId;
import com.aawhere.collections.FunctionsExtended;
import com.aawhere.person.AccountIdentity;
import com.aawhere.person.Person;
import com.aawhere.person.PersonAccountProvider;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.personalize.PersonalizeUtil;

/**
 * @author brian
 * 
 */
public class IdentityTestUtil {

	private IdentityTestUtil() {
		// static helper methods only
	}

	/**
	 * Identity Manager without a person.
	 * 
	 * @return
	 */
	public static IdentityManager identityManager() {
		return builder().build();
	}

	/**
	 * @param person
	 * @return
	 */
	public static IdentityManager identityManager(Person person) {
		return builder().person(person).build();
	}

	public static IdentityManager admin() {
		return builder().person(PersonTestUtil.personWithId())
				.identityRoleValidator(IdentityUtil.adminIdentityRoleValidator()).identityFactory(identityFactory())
				.build();
	}

	public static IdentityManager withRole(IdentityRole role) {
		return builder().person(PersonTestUtil.personWithId())
				.identityRoleValidator(IdentityUtil.identityRoleValidator(role)).identityFactory(identityFactory())
				.build();
	}

	public static IdentityFactory identityFactory() {
		return new IdentityFactory(PersonTestUtil.personAccountProvider());
	}

	private static IdentityManager.Builder builder() {
		return builder(PersonalizeUtil.DEFAULT).identityFactory(identityFactory());
	}

	private static IdentityManager.Builder builder(Locale locale) {
		return IdentityManager.create().locale(locale).accessScopeValidatorFactory(new AccessScopeValidatorFactory());
	}

	public static Identity identity(final Boolean identityShared, final Boolean isOwner) {
		return new Identity() {

			@Override
			public Boolean isOwner(Person person) {
				return isOwner;
			}

			@Override
			public Boolean identityShared() {
				return identityShared;
			}
		};
	}

	public static IdentityManager identityManagerWithPerson() {
		return identityManager(PersonTestUtil.personWithId());
	}

	public static Identity accountIdentity(Person person) {
		PersonAccountProvider.Default provider = new PersonAccountProvider.Default(
				FunctionsExtended.<AccountId, Person> constant(person));
		return AccountIdentity.create().accountId(person.accountIds().iterator().next()).provider(provider).build();
	}
}
