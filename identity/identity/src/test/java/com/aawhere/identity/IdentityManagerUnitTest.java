package com.aawhere.identity;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.person.PersonTestUtil;

public class IdentityManagerUnitTest {

	@Test
	public void testAdminWriteAccessNotOwner() {
		assertTrue(	"admin should be able to write anything",
					IdentityTestUtil.admin()
							.hasAccess(IdentityTestUtil.identity(false, false), AccessScope.OWNER_WRITE));
	}

	@Test
	public void testAdminReadAccessNotOwner() {
		assertTrue(	"admin should be able to read anything",
					IdentityTestUtil.admin().hasAccess(IdentityTestUtil.identity(false, false), AccessScope.OWNER_READ));
	}

	@Test
	public void testDeveloperReadAccessNotOwner() {
		assertTrue("developer should be able to read PUBLIC_READ", IdentityTestUtil.withRole(IdentityRole.developer)
				.hasAccess(IdentityTestUtil.identity(false, false), AccessScope.PUBLIC_READ));
	}

	@Test
	public void testOwnerReadAccess() {
		IdentityManager identityManagerWithPerson = IdentityTestUtil.identityManagerWithPerson();
		assertTrue(	"owner must have read access",
					identityManagerWithPerson.hasAccess(identityManagerWithPerson.person(), AccessScope.OWNER_READ));
	}

	@Test
	public void testOwnerWriteAccess() {
		IdentityManager identityManagerWithPerson = IdentityTestUtil.identityManagerWithPerson();
		assertTrue(	"owner must have write access",
					identityManagerWithPerson.hasAccess(identityManagerWithPerson.person(), AccessScope.OWNER_WRITE));
	}

	@Test
	public void testPublicAccessUnauthenticated() {
		assertTrue(	"anyone can view public data if identity is shared",
					IdentityTestUtil.identityManager().hasAccess(	IdentityTestUtil.identity(true, false),
																	AccessScope.PUBLIC_READ));
	}

	@Test
	public void testUnauthenticatedOwnerReadDenied() {
		assertFalse("anonymous should not see private owner data",
					IdentityTestUtil.identityManager().hasAccess(	IdentityTestUtil.identity(true, false),
																	AccessScope.OWNER_READ));
	}

	@Test
	public void testNonOwnerReadDenied() {
		IdentityManager identityManagerWithPerson = IdentityTestUtil.identityManagerWithPerson();
		assertFalse("authenticated user shouldn't see other owner's data",
					identityManagerWithPerson.hasAccess(PersonTestUtil.personWithId(), AccessScope.OWNER_READ));
	}

	@Test
	public void testNonOwnerWriteDenied() {
		IdentityManager identityManagerWithPerson = IdentityTestUtil.identityManagerWithPerson();
		assertFalse("authenticated user shouldn't modify other owner's data",
					identityManagerWithPerson.hasAccess(PersonTestUtil.personWithId(), AccessScope.OWNER_WRITE));
	}

	@Test
	public void testAuthenticatedPublic() {
		IdentityManager identityManagerWithPerson = IdentityTestUtil.identityManagerWithPerson();
		assertTrue(	"authenticated user may view other's public data",
					identityManagerWithPerson.hasAccess(IdentityTestUtil.identity(true, false), AccessScope.PUBLIC_READ));

	}

	@Test
	public void testPersonAccessToOwnedAccountIdentity() {
		IdentityManager manager = IdentityTestUtil.identityManagerWithPerson();
		Identity accountIdentity = IdentityTestUtil.accountIdentity(manager.person());
		assertTrue(	"the person should be able to view their own account data",
					manager.hasAccess(accountIdentity, AccessScope.OWNER_WRITE));
	}

	@Test
	public void testPersonAccessDeniedToNonOwnedAccountIdentity() {
		IdentityManager manager = IdentityTestUtil.identityManagerWithPerson();
		Identity accountIdentity = IdentityTestUtil.accountIdentity(PersonTestUtil.personWithId());
		assertFalse("the requestor is not the owner of the account",
					manager.hasAccess(accountIdentity, AccessScope.OWNER_WRITE));
	}
}
