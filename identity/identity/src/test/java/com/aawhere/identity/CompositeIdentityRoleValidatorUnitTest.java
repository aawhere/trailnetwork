/**
 * 
 */
package com.aawhere.identity;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Functions;

/**
 * Unit tests for {@link CompositeIdentityRoleValidator}
 * 
 * @author Brian Chapman
 * 
 */
public class CompositeIdentityRoleValidatorUnitTest {

	private IdentityRoleValidator falseValidator;
	private IdentityRoleValidator trueValidator;

	@Before
	public void setup() {
		falseValidator = new IdentityRoleFunctionValidator(Functions.constant(Boolean.FALSE));
		trueValidator = new IdentityRoleFunctionValidator(Functions.constant(Boolean.TRUE));
	}

	@Test
	public void testCompositeValidatorOneValidatorFalse() {
		IdentityRoleValidator validator = CompositeIdentityRoleValidator.create().addValidator(falseValidator).build();
		assertValidator(validator, IdentityRole.admin, Boolean.FALSE);
	}

	@Test
	public void testCompositeValidatorOneValidatorTrue() {
		IdentityRoleValidator validator = CompositeIdentityRoleValidator.create().addValidator(trueValidator).build();
		assertValidator(validator, IdentityRole.admin, Boolean.TRUE);
	}

	@Test
	public void testCompositeValidatorTwoValidators() {
		IdentityRoleValidator validator = CompositeIdentityRoleValidator.create().addValidator(falseValidator)
				.addValidator(trueValidator).build();
		assertValidator(validator, IdentityRole.admin, Boolean.TRUE);
	}

	private void assertValidator(IdentityRoleValidator validator, IdentityRole role, Boolean expected) {
		assertEquals(expected, validator.isUserInRole(role));
	}
}
