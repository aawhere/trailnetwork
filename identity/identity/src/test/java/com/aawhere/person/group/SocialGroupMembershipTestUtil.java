/**
 * 
 */
package com.aawhere.person.group;

import com.aawhere.person.PersonId;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class SocialGroupMembershipTestUtil {

	public static SocialGroupMembershipId membershipId() {
		return new SocialGroupMembershipId(SocialGroupTestUtil.socialGroupId(), TestUtil.generateRandomId());
	}

	/**
	 * @return
	 */
	public static SocialGroupMembership membership() {
		return membership(null, null);
	}

	public static SocialGroupMembership membership(SocialGroupId groupId) {
		return membership(groupId, null);
	}

	/**
	 * @param groupId
	 * @return
	 */
	public static SocialGroupMembership membership(SocialGroupId groupId, PersonId personId) {
		SocialGroupMembershipUnitTest instance = new SocialGroupMembershipUnitTest();
		instance.groupId = groupId;
		instance.personId = personId;
		instance.baseSetUp();
		instance.setUp();
		return instance.getEntitySample();
	}

	/**
	 * @param personId
	 * @return
	 */
	public static SocialGroupMembership membership(PersonId personId) {
		return membership(null, personId);
	}

}
