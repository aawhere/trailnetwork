/**
 *
 */
package com.aawhere.person;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.configuration.XMLConfiguration;

import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.identity.IdentityManager;
import com.aawhere.identity.IdentityTestUtil;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceTestUtilBase;
import com.aawhere.test.TestUtil;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * @author brian
 * 
 */
public class PersonTestUtil
		extends ServiceTestUtilBase<Persons, Person, PersonId>
		implements PersonProvider {

	public static final String TEST_NAME = TestUtil.generateRandomAlphaNumeric();
	public static final String NAME_BAD = null;
	public static final String NAME_OK = "aroller";
	public static final String NAME_GOOD = "Aaron";
	public static final String NAME_BETTER = "Aaron Roller";

	public static PersonId createPersonId() {
		return new PersonId(TestUtil.generateRandomId());
	}

	public static XMLConfiguration createPreferences() {
		XMLConfiguration prefs = new XMLConfiguration();
		prefs.addProperty("locale", Locale.US.toString());
		// This would probably be a Unit value from javax.measure, but I didn't
		// want to drag that dependency into this project just for a unit test.
		prefs.addProperty("distance-unit", "miles");
		return prefs;
	}

	public static Person person() {
		return PersonUnitTest.instance().getEntitySample();
	}

	public static Person personWithName(String name) {
		return PersonUnitTest.create().name(name).getEntitySample();
	}

	/** Provides a builder with an id populated with {@link PersonUnitTest}. */
	public static Person.Builder builder() {
		return Person.mutate(personWithId());
	}

	/**
	 * @return
	 */
	public static Person personWithId() {
		return PersonUnitTest.instance().getEntitySampleWithId();

	}

	public static Person person(Boolean identityShared) {
		return PersonTestUtil.builder().identityShared(identityShared).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.person.PersonProvider#person(com.aawhere.person.PersonId)
	 */
	@Override
	public Person person(PersonId id) throws EntityNotFoundException {
		PersonUnitTest instance = PersonUnitTest.instance();
		instance.setUp();
		instance.baseSetUp();
		instance.setPersonId(id);
		return instance.getEntitySampleWithId();
	}

	public Function<PersonId, Person> personFunction() {
		return new Function<PersonId, Person>() {

			@Override
			public Person apply(PersonId input) {
				try {
					return (input != null) ? person(input) : null;
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.person.PersonAccountProvider#person(com.aawhere.app.account.AccountId)
	 */
	public static Person person(AccountId accountId) {
		Account account;
		try {
			account = AccountTestUtil.instance().account(accountId);
		} catch (EntityNotFoundException e) {
			throw new RuntimeException(e);
		}
		return PersonUnitTest.instance().account(account).getEntitySampleWithId();
	}

	/**
	 * @return
	 * 
	 */
	public static PersonTestUtil instance() {
		return new PersonTestUtil();
	}

	/**
	 * @return
	 */
	public static Set<XmlAdapter<?, ?>> adapters() {
		Set<XmlAdapter<?, ?>> adapters = AccountTestUtil.adapters();
		adapters.addAll(AccountTestUtil.adapters());
		adapters.add(new PersonIdXmlAdapter(new PersonTestUtil(), IdentityTestUtil.admin()));
		adapters.add(new PersonFromAccountIdXmlAdapter(PersonTestUtil.personAccountProvider(), IdentityTestUtil.admin()));
		IdentityManager identityManager = IdentityTestUtil.identityManager();
		adapters.add(new Person.NameXmlAdapter(identityManager));
		adapters.add(new Person.NameWithAliasesXmlAdapter(identityManager));
		return adapters;
	}

	/**
	 * @return
	 */
	public static PersonAccountProvider personAccountProvider() {
		return new PersonAccountProvider.Default(new Function<AccountId, Person>() {

			@Override
			public Person apply(AccountId input) {
				return (input != null) ? person(input) : null;
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceStandard#all(java.lang.Iterable)
	 */
	@Override
	public Map<PersonId, Person> all(Iterable<PersonId> ids) {
		Iterable<Person> persons = Iterables.transform(ids, personFunction());
		return IdentifierUtils.map(persons);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceTestUtilBase#entity(com.aawhere.id.Identifier)
	 */
	@Override
	public Person entity(PersonId id) {
		try {
			return person(id);
		} catch (EntityNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return
	 */
	public static PersonId personId() {
		return createPersonId();
	}
}
