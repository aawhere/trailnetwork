package com.aawhere.person;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.Application;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.app.account.EmailAddress;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.person.Person.Builder;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * 
 * @author Brian Chapman
 * 
 */
public class PersonUnitTest
		extends BaseEntityJaxbBaseUnitTest<Person, Person.Builder, PersonId, PersonRepository> {

	private PersonId personId;
	protected Account account1;
	protected Account account2;
	private String nameMutated;
	private String name;
	private String nameWithAliasesMutated;
	private String nameWithAliases;
	private Boolean identityShared;
	private EmailAddress email;

	public PersonUnitTest() {
		super(true);
	}

	@Override
	@Before
	public void setUp() {
		personId = PersonTestUtil.createPersonId();
		account1 = AccountTestUtil.account();
		account2 = AccountTestUtil.account();
		nameMutated = TestUtil.generateRandomAlphaNumeric();
		nameWithAliases = TestUtil.generateRandomAlphaNumeric();
		nameWithAliasesMutated = TestUtil.generateRandomAlphaNumeric();
		name = PersonTestUtil.TEST_NAME;
		identityShared = true;
		email = AccountTestUtil.emailAddress();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#getId()
	 */
	@Override
	protected PersonId getId() {
		return this.personId;
	}

	/**
	 * Mutator for {@link PersonTestUtil} to provide the id before popluation, but after
	 * {@link #setUp()}
	 * 
	 * @param personId
	 * @return
	 */
	PersonUnitTest setPersonId(PersonId personId) {
		this.personId = personId;
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#populateBeforeCreation(com
	 * .aawhere.persist.BaseEntity .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.name(this.name);
		builder.account(account1);
		builder.account(account2);
		builder.nameWithAliases(nameWithAliases);
		builder.identityShared(identityShared);
		builder.email(email);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere
	 * .persist.BaseEntity)
	 */
	@Override
	public void assertCreation(Person sample) {
		super.assertCreation(sample);
		assertEntity(sample);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testNameRequired() {
		new Application.Builder().build();
	}

	@Test
	public void testMerge() {
		Person winner = getEntitySampleWithId();
		assertFalse(winner.hasAliases());
		Person loser1 = PersonTestUtil.personWithId();
		Person merged1 = Person.mutate(winner).merge(loser1).build();
		assertEquals(winner, merged1);
		TestUtil.assertContainsAll(loser1.accountIds(), merged1.accountIds());
		TestUtil.assertContains("the winner should also be an alias", winner.id(), merged1.aliases());
		TestUtil.assertContains(loser1.id(), merged1.aliases());
		Person loser2 = PersonTestUtil.personWithId();

		Person merged2 = Person.mutate(winner).merge(loser2).build();
		// ensure loser 1 is still there
		TestUtil.assertContainsAll(loser1.accountIds(), merged2.accountIds());
		TestUtil.assertContains(loser1.id(), merged2.aliases());
		TestUtil.assertContainsAll(loser2.accountIds(), merged2.accountIds());
		TestUtil.assertContains(loser2.id(), merged2.aliases());

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#assertStored(com.aawhere.persist .BaseEntity)
	 */
	@Override
	public void assertStored(Person sample) {
		super.assertStored(sample);
		assertEntity(sample);
		assertName(sample);
		assertEquals(identityShared, sample.identityShared());
	}

	/**
	 * @param sample
	 */
	public void assertName(Person sample) {
		assertEquals("Name is required", this.name, sample.getName());
	}

	private void assertEntity(Person sample) {
		TestUtil.assertContains(this.account1.id(), sample.accountIds());
		TestUtil.assertContains(this.account2.id(), sample.accountIds());

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.Identifier)
	 */
	@Override
	protected Person mutate(PersonId id) throws EntityNotFoundException {
		getRepository().name(id, nameMutated, nameWithAliasesMutated);
		return super.mutate(id);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(Person mutated) {
		super.assertMutation(mutated);
		assertEquals("name didn't get mutated", this.nameMutated, mutated.getName());
	}

	public static PersonUnitTest instance() {
		PersonUnitTest instance = new PersonUnitTest();
		instance.setUp();
		instance.baseSetUp();
		return instance;
	}

	PersonUnitTest account(Account account) {
		this.account1 = account;
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityJaxbBaseUnitTest#assertJaxb(com.aawhere.xml.bind.JAXBTestUtil)
	 */
	@Override
	protected void assertJaxb(JAXBTestUtil<Person> util) {
		super.assertJaxb(util);
		util.assertContains(Account.FIELD.ACCOUNTS);
		// checks only the output and doesn't expect unmarshalling since these optional values use
		// asymetrical adapters
		final String includedInXmlIfIdentityShared = PersonField.NAME_WITH_ALIASES;
		if (util.getExpected().identityShared()) {
			util.assertContains(includedInXmlIfIdentityShared);
			util.assertContains(this.email.address());
		} else {
			TestUtil.assertDoesntContain(util.getXml(), includedInXmlIfIdentityShared);
			TestUtil.assertDoesntContain(util.getXml(), this.email.address());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return PersonTestUtil.adapters();
	}

	public static class PersonMockRepository
			extends MockFilterRepository<Persons, Person, PersonId>
			implements PersonRepository {

		private PersonRepositoryDelegate delegate = new PersonRepositoryDelegate(this);

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.person.PersonRepository#setName(com.aawhere.person.PersonId,
		 * java.lang.String)
		 */
		@Override
		public Person name(PersonId personId, String name, String nameWithAliases) throws EntityNotFoundException {
			return delegate.name(personId, name, nameWithAliases);
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.person.PersonRepository#aliasesAdded(com.aawhere.person.PersonId,
		 * java.lang.Iterable)
		 */
		@Override
		public Person aliasesAdded(PersonId personId, Iterable<PersonId> personIds) throws EntityNotFoundException {
			return delegate.aliasesAdded(personId, personIds);
		}

		/*
		 * @see com.aawhere.person.PersonRepository#update(com.aawhere.person.PersonId)
		 */
		@Override
		public Builder update(PersonId personId) throws EntityNotFoundException {
			return delegate.update(personId);
		}

	}

	static PersonUnitTest create() {
		return instance();
	}

	PersonUnitTest name(String name) {
		this.name = name;
		return this;
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected PersonRepository createRepository() {
		return new PersonMockRepository();
	}

}
