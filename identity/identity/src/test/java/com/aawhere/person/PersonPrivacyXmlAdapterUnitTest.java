/**
 *
 */
package com.aawhere.person;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.identity.IdentityManager;
import com.aawhere.identity.IdentityTestUtil;

/**
 * @author brian
 * 
 */
public class PersonPrivacyXmlAdapterUnitTest {

	@Test
	public void testMarshalSameIdIdentityNotShared() throws Exception {
		Person person = PersonTestUtil.person(Boolean.FALSE);
		IdentityManager identityManager = IdentityTestUtil.identityManager(person);
		assertVisible(identityManager, person);
	}

	@Test
	public void testMarshalIdentitySharedTrue() throws Exception {
		Person person = PersonTestUtil.person(Boolean.TRUE);
		IdentityManager identityManager = IdentityTestUtil.identityManager(person);
		assertVisible(identityManager, person);
	}

	@Test
	public void testMarshalDifferentId() throws Exception {
		Person person = PersonTestUtil.person(Boolean.FALSE);
		Person differentPerson = PersonTestUtil.personWithId();
		IdentityManager identityManager = IdentityTestUtil.identityManager(differentPerson);
		assertHidden(identityManager, person);
	}

	@Test
	public void testMarshalWithEmptyIdentityManager() throws Exception {
		Person person = PersonTestUtil.person(Boolean.FALSE);
		IdentityManager identityManager = IdentityTestUtil.identityManager();
		assertHidden(identityManager, person);
	}

	@Test
	public void testMarshalWithEmptyIdentityManagerWithIdentityShared() throws Exception {
		Person person = PersonTestUtil.person(Boolean.TRUE);
		IdentityManager identityManager = IdentityTestUtil.identityManager();

		assertVisible(identityManager, person);
	}

	@Test
	public void testEmailHiddenFromUnauthenticated() {
		IdentityTestUtil.identityManager();
	}

	private void assertHidden(IdentityManager identityManager, Person person) throws Exception {
		identityManager.enforceAccess(person);
		assertEquals(null, person.name());
	}

	private void assertVisible(IdentityManager identityManager, Person person) throws Exception {

		String expectedName = person.getName();
		identityManager.enforceAccess(person);
		assertEquals(expectedName, person.getName());
	}
}
