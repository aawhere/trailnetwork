/**
 *
 */
package com.aawhere.person.pref;

import static org.junit.Assert.*;

import java.util.Locale;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Test;

import com.aawhere.person.Person;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.person.PersonUtil;

/**
 * @author brian
 * 
 */
public class PrefUtilUnitTest {

	@Test
	public void testGetLocale() {
		String localString = "en_US";
		String localeLang = "en";
		String localeCountry = "US";
		Configuration prefs = new PropertiesConfiguration();
		prefs.addProperty("locale", localString);
		Locale result = PrefUtil.getLocale(prefs);
		Locale expected = new Locale(localeLang, localeCountry);

		assertEquals(expected, result);
	}

	@Test
	public void testgetDefaultSiPreferences() {
		Configuration prefs = PrefUtil.getDefaultSiPreferences();
		assertNotNull(prefs);
		assertNotNull(prefs.getString(Preferences.DISTANCE_UNIT.getKey()));
	}

	@Test
	public void testgetDefaultUsCustomaryPreferences() {
		Configuration prefs = PrefUtil.getDefaultUsCustomaryPreferences();
		assertNotNull(prefs);
		assertNotNull(prefs.getString(Preferences.DISTANCE_UNIT.getKey()));
	}

	@Test
	public void testGetPreferencesLocaleDefaultPerson() {
		Locale locale = Locale.FRANCE;
		Locale localeOverride = Locale.GERMANY;
		Configuration personPrefs = PrefTestUtil.getPreferences(localeOverride);
		Configuration defaultPrefs = PrefTestUtil.createSiPreferences(locale);
		Configuration prefs = PrefUtil.getPreferences(locale, defaultPrefs, personPrefs);
		assertNotNull(prefs);
		assertEquals(localeOverride, PrefUtil.getLocale(prefs));
	}

	@Test
	public void testGetPreferencesLocaleDefault() {
		Locale locale = Locale.FRANCE;
		Configuration defaultPrefs = PrefTestUtil.createSiPreferences(locale);
		Configuration prefs = PrefUtil.getPreferences(locale, defaultPrefs);
		assertNotNull(prefs);
		assertEquals(locale, PrefUtil.getLocale(prefs));
	}

	@Test
	public void testBuilderAllParams() {
		Person person = PersonTestUtil.personWithId();
		Configuration personPrefs = PrefUtil.getEmptyPreferences();
		String elevationUnit = "steps";
		personPrefs.addProperty(Preferences.ELEVATION_UNIT.getKey(), elevationUnit);
		person = PersonUtil.update(person, personPrefs);
		UnitOfMeasure metric = UnitOfMeasure.METRIC;
		Locale locale = Locale.US;
		Configuration prefs = PrefUtil.createPrefBuilder().person(person).unitOfMeasure(metric).locale(locale).build();
		assertNotNull(prefs);
		assertEquals(prefs.getString(Preferences.DISTANCE_UNIT.getKey()), "km");
		assertEquals(prefs.getString(Preferences.ELEVATION_UNIT.getKey()), elevationUnit);
	}

}
