/**
 *
 */
package com.aawhere.person.pref;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.USCustomarySystem;
import javax.measure.unit.Unit;
import javax.measure.unit.UnitFormat;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Test;

import com.aawhere.measure.Elevation;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.unit.format.UnitFormatFactory;
import com.aawhere.measure.xml.MeasurementXmlUtil;
import com.aawhere.test.TestUtil;

/**
 * @author brian
 * 
 */
public class PrefMeasureUtilUnitTest {

	@Test
	public void testBuildKey() {
		final Class<Length> quantityType = Length.class;
		String key = PrefMeasureUtil.buildUnitKey(quantityType);
		TestUtil.assertContains(key, MeasurementXmlUtil.typeFor(quantityType));
	}

	@Test
	public void testGetUnitFor() throws IOException {
		UnitFormat uf = UnitFormatFactory.getInstance().createDefaultUnitFormat();
		assertUnitFor(uf.format(MetricSystem.KILO(MetricSystem.METRE)), Length.class);
		assertUnitFor(uf.format(MetricSystem.KILO(MetricSystem.METRE)), Elevation.class);
		assertUnitFor(uf.format(USCustomarySystem.DEGREE_ANGLE), Latitude.class);
		assertUnitFor(uf.format(USCustomarySystem.DEGREE_ANGLE), Longitude.class);
	}

	/**
	 * @param unitSymbol
	 * @param quantityTypeKey
	 * @param quantityType
	 */
	private <Q extends Quantity<Q>> void assertUnitFor(String unitSymbol, final Class<Q> quantityType) {
		Configuration prefs = new PropertiesConfiguration();
		prefs.addProperty(PrefMeasureUtil.buildUnitKey(quantityType), unitSymbol);
		Unit<?> unit = PrefMeasureUtil.getUnitFor(quantityType, prefs);
		assertNotNull(unit);
		assertEquals(unitSymbol, MeasurementUtil.getUnitSymbol(unit));
	}

	@Test
	public void testGetBaseUnitFor() throws IOException {
		String unitSymbol = UnitFormatFactory.getInstance().createDefaultUnitFormat()
				.format(MetricSystem.KILO(MetricSystem.METRE), new StringBuffer()).toString();

		Configuration prefs = new PropertiesConfiguration();
		prefs.addProperty(PrefMeasureUtil.buildUnitKey(Length.class), unitSymbol);

		Unit<?> unit = PrefMeasureUtil.getBaseUnitFor(Length.class, prefs);
		assertNotNull(unit);
		assertEquals(MetricSystem.METRE.getSymbol(), unit.getSymbol());
	}

	@Test
	public void testGetMaxDigitsFor() {
		final Class<Length> quantityType = Length.class;
		String key = PrefMeasureUtil.buildDigitKey(quantityType);

		Configuration prefs = PrefTestUtil.createSiPreferences();

		int digits = PrefMeasureUtil.getMaxDigitsFor(quantityType, prefs);
		assertEquals(prefs.getInt(key), digits);
	}

}
