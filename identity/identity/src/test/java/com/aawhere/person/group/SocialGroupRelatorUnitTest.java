/**
 * 
 */
package com.aawhere.person.group;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.person.group.SocialGroupRelator.Options;
import com.aawhere.test.TestUtil;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

/**
 * @see SocialGroupRelator
 * @author aroller
 * 
 */
public class SocialGroupRelatorUnitTest {
	/** The groups put into the relator. */
	private Multimap<SocialGroup, PersonId> groupsIn;
	/**
	 * The expected groups to be suggested by the relator. Only one social group is expected to be
	 * registered so the results is a LinkedSet. Order of entry matters since the order you enter
	 * here must be the expected order of output based on the scores, etc.
	 */
	private LinkedHashMultimap<String, SocialGroup> expectedGroups;
	/**
	 * Social gropus of the same context should be separated out since the relationship is already
	 * obvious.
	 */
	private List<SocialGroup> expectedGroupsSameContext;
	private Integer highScore;
	private Integer mediumScore;
	private Integer lowScore;
	private Options options;
	private SocialGroup socialGroupTargeted;
	private String category;

	@Before
	public void setUp() {
		this.category = TestUtil.generateRandomAlphaNumeric();
		socialGroupTargeted = SocialGroupTestUtil.withId().category(category).build();
		groupsIn = HashMultimap.create();
		expectedGroups = LinkedHashMultimap.create();
		expectedGroupsSameContext = Lists.newArrayList();
		highScore = 10;
		mediumScore = highScore / 2;
		lowScore = mediumScore / 2;
		options = options();

	}

	/**
	 * 
	 */
	private Options options() {
		final Integer commonWeight = 100;
		return Options.create().inCommonMinimum(1).groupScoreWeight(commonWeight).inCommonCountWeight(commonWeight);
	}

	@After
	public void test() {
		// setup functions to provide values
		HashMultimap<PersonId, SocialGroupId> groupIdsForPersonId = HashMultimap.create();

		for (Entry<SocialGroup, PersonId> socialGroup : groupsIn.entries()) {
			groupIdsForPersonId.put(socialGroup.getValue(), socialGroup.getKey().id());
		}

		Map<PersonId, ? extends Iterable<SocialGroupId>> asMap = groupIdsForPersonId.asMap();
		Function<PersonId, Iterable<SocialGroupId>> groupIdsForPersonIdsFunction = (Function<PersonId, Iterable<SocialGroupId>>) Functions
				.forMap(asMap);

		// build the relator

		SocialGroupRelator relator = SocialGroupRelator.create().socialGroupTargeted(socialGroupTargeted)
				.groupIdsForPersonProvider(groupIdsForPersonIdsFunction)
				.groupProvider(IdentifierUtils.mapFunction(this.groupsIn.keySet())).personIds(groupsIn.values())
				.options(options).build();
		Set<String> expectedCategories = this.expectedGroups.keySet();
		for (String expectedCategory : expectedCategories) {

			final Set<SocialGroup> expectedGroupsForCategory = expectedGroups.get(expectedCategory);
			final List<SocialGroup> actualGroupForCategory = relator.groupsMatchingCategory(expectedCategory);
			TestUtil.assertIterablesEquals(	"groups for category " + expectedCategory,
											expectedGroupsForCategory,
											actualGroupForCategory);
		}

		assertEquals("groups with the same context", this.expectedGroupsSameContext, relator.groupsWithSameContext());

	}

	/**
	 * The bare minimum test expecting no results because nothing was requested.
	 * 
	 */
	@Test
	public void testEmptyPersonIds() {
		// see after...empty results is the baseline
	}

	/**
	 * Proves that the group being targetd will not be part of the results.
	 * 
	 */
	@Test
	public void testTargetThyself() {
		this.groupsIn.put(socialGroupTargeted, PersonTestUtil.createPersonId());
		// just go with the default to expect nothing
	}

	@Test
	public void testOneGroupOnePerson() {
		expected(highScore);
	}

	/**
	 * A Group is provided from a different category than the targeted so it should be avilable only
	 * in it's category results.
	 * 
	 */
	@Test
	public void testOnGroupOnePersonDifferentCategory() {
		SocialGroup groupWithAlternateCategory = SocialGroupTestUtil.withId()
				.category(TestUtil.generateRandomAlphaNumeric()).build();
		expected(groupWithAlternateCategory);
	}

	/**
	 * A different group with the same category, but has the same context as the target.
	 * 
	 */
	@Test
	public void testOneGroupOnePersonSameContextSameCategory() {
		SocialGroup socialGroup = SocialGroupTestUtil.withId().category(category)
				.context(socialGroupTargeted.context()).build();
		expected(socialGroup);
	}

	@Test
	public void testOneGroupOnePersonSameContextDifferentCategory() {
		SocialGroup socialGroup = SocialGroupTestUtil.withId().context(socialGroupTargeted.context()).build();
		expected(socialGroup);
	}

	/** When all else is equal the high score should be first. */
	@Test
	public void testHigherScoreFirst() {
		Pair<PersonId, SocialGroup> expected = expected(highScore);
		// same person in two groups
		expected(lowScore, expected.getKey());

	}

	/** When all else is equal the more people in a group should come first */
	@Test
	public void testHigherScoreLosesMorePeopleWins() {
		Pair<PersonId, SocialGroup> expected = expected(highScore);
		// same person in two groups
		expected(expected.getRight());

		// another group with one cross member, but higher score
		expected(highScore, expected.getLeft());
	}

	/** More people in a low scoring group loses since score weight is big */
	@Test
	public void testGroupScoreFactorWins() {
		// first group iwth high score and 1 member
		expected(highScore);
		// second group with low score and two members
		Pair<PersonId, SocialGroup> losing = expected(lowScore);
		// same person in two groups
		expected(losing.getRight());
		// heavily weigh the score
		this.options = this.options().inCommonCountWeight(1).groupScoreWeight(10);

	}

	/**
	 * Low scoring group with two members wins over high score because of weight towards member
	 * count
	 * 
	 */
	@Test
	public void testInCommonWeightWins() {
		// two people have the first group in common
		Pair<PersonId, SocialGroup> expected = expected(lowScore);
		expected(expected.getRight());
		// high score, but only 1 member
		expected(highScore);
		this.options = this.options().inCommonCountWeight(10).groupScoreWeight(1);

	}

	/**
	 * Just a brut force test to ensure if five groups are found to match then all five will be
	 * provided back in order...mostly just to test support for many
	 * 
	 */
	@Test
	public void testFiveGroupsAllReciprocalDescendingScores() {
		// descending score for each so we may test order
		Integer score = socialGroupTargeted.score();
		List<Pair<PersonId, SocialGroup>> seeds = Arrays.asList(expected(score -= 1),
																expected(score -= 1),
																expected(score -= 1),
																expected(score -= 1),
																expected(score -= 1));
		// add all to each others groups
		for (Pair<PersonId, SocialGroup> outer : seeds) {
			for (Pair<PersonId, SocialGroup> inner : seeds) {
				if (!outer.getKey().equals(inner.getKey())) {
					expected(outer.getKey(), inner.getRight());
				}
			}
		}

	}

	@Test
	public void testFiveGroupSomeDescendingInCommonSameScores() {
		// descending score for each so we may test order
		Integer score = socialGroupTargeted.score();
		List<Pair<PersonId, SocialGroup>> seeds = Arrays.asList(expected(score),
																expected(score),
																expected(score),
																expected(score),
																expected(score));
		// the first group guides the most and each one guides less
		guide(seeds.get(0), seeds.get(1));
		guide(seeds.get(0), seeds.get(2));
		guide(seeds.get(0), seeds.get(3));
		guide(seeds.get(0), seeds.get(4));

		guide(seeds.get(1), seeds.get(0));
		guide(seeds.get(1), seeds.get(2));
		guide(seeds.get(1), seeds.get(3));

		guide(seeds.get(2), seeds.get(0));
		guide(seeds.get(2), seeds.get(1));

		guide(seeds.get(3), seeds.get(0));

	}

	/**
	 * The first pair given will provide the group which the follower will be added.
	 * 
	 * @param pair
	 * @param pair2
	 */
	private void guide(Pair<PersonId, SocialGroup> guide, Pair<PersonId, SocialGroup> follower) {
		expected(follower.getLeft(), guide.getRight());
	}

	/**
	 * Adds a person and a group of which the group id is expected to be in the results. The order
	 * this is called is the expected order of the results.
	 * 
	 * @return
	 * 
	 */
	private Pair<PersonId, SocialGroup> expected(Integer groupScore) {
		return expected(groupScore, null, null);
	}

	private Pair<PersonId, SocialGroup> expected(SocialGroup group) {
		return expected(null, null, group);
	}

	private Pair<PersonId, SocialGroup> expected(PersonId personId, SocialGroup group) {
		return expected(null, personId, group);
	}

	private Pair<PersonId, SocialGroup> expected(Integer groupScore, PersonId personId) {
		return expected(groupScore, personId, null);
	}

	/**
	 * creates a new group for the given person id is a member
	 * 
	 * @param groupScore
	 * @param personId
	 * @return
	 */
	private Pair<PersonId, SocialGroup> expected(@Nullable Integer groupScore, @Nullable PersonId personId,
			@Nullable SocialGroup socialGroup) {
		if (socialGroup == null) {
			socialGroup = SocialGroupTestUtil.withId().score(groupScore).category(category).build();
		}
		if (personId == null) {
			personId = PersonTestUtil.createPersonId();
		}
		this.groupsIn.put(socialGroup, personId);

		if (socialGroup.context().equals(socialGroupTargeted.context())) {
			this.expectedGroupsSameContext.add(socialGroup);
		} else {
			this.expectedGroups.put(socialGroup.category(), socialGroup);
		}
		return Pair.of(personId, socialGroup);
	}
}
