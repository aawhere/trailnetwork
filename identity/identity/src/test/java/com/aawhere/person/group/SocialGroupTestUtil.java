/**
 * 
 */
package com.aawhere.person.group;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.persist.EntityUtil;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.test.TestUtil;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * @author aroller
 * 
 */
public class SocialGroupTestUtil {

	public static final Class<? extends Identifier<?, ?>> CONTEXT_ID_TYPE = ChildId.class;

	public static SocialGroup group() {
		return SocialGroupUnitTest.instance().getEntitySample();
	}

	public static SocialGroup socialGroupWithId() {
		return SocialGroupUnitTest.instance().getEntitySampleWithId();
	}

	public static SocialGroup.Builder withId() {
		return withId(null);
	}

	public static SocialGroup.Builder withId(SocialGroupId id) {
		SocialGroupUnitTest instance = SocialGroupUnitTest.instance();
		instance.id = id;
		return SocialGroup.mutate(instance.getEntitySampleWithId());
	}

	public static SocialGroupId socialGroupId() {
		return new SocialGroupId(TestUtil.generateRandomId());
	}

	public static Set<XmlAdapter<?, ?>> adapters() {
		Set<XmlAdapter<?, ?>> adapters = PersonTestUtil.adapters();

		return adapters;
	}

	/**
	 * A unit level simulator of the connected function provided by the service to be used with unit
	 * testing. Social groups given come back persisted and updated matching the count of the
	 * memberships given.
	 * */
	public static Function<Entry<SocialGroup, ? extends Iterable<Entry<PersonId, Integer>>>, SocialGroup>
			membershipUpdateFunction() {
		return new Function<Map.Entry<SocialGroup, ? extends Iterable<Entry<PersonId, Integer>>>, SocialGroup>() {

			@Override
			public SocialGroup apply(Entry<SocialGroup, ? extends Iterable<Entry<PersonId, Integer>>> input) {
				if (input == null) {
					return null;
				}
				int size = Iterables.size(input.getValue());
				SocialGroup.Builder socialGroupBuilder;
				SocialGroup socialGroupGiven = input.getKey();
				if (EntityUtil.isPersisted(socialGroupGiven)) {
					socialGroupBuilder = SocialGroup.clone(socialGroupGiven);
				} else {
					socialGroupBuilder = SocialGroupTestUtil.withId();
					socialGroupBuilder.category(socialGroupGiven.category());
					socialGroupBuilder.context(socialGroupGiven.context());
				}
				socialGroupBuilder.membershipCount(size);
				return socialGroupBuilder.build();
			}
		};
	}
}
