/**
 *
 */
package com.aawhere.person.pref;

import java.util.Locale;

import org.apache.commons.configuration.Configuration;

/**
 * @author brian
 * 
 */
public class PrefTestUtil {

	public static Configuration createSiPreferences() {
		return createSiPreferences(Locale.US);
	}

	public static Configuration createSiPreferences(Locale locale) {
		Configuration prefs = PrefUtil.getDefaultSiPreferences();
		PrefUtil.setLocale(locale, prefs);
		return prefs;
	}

	public static Configuration createUsCustomaryPreferences() {
		return createUsCustomaryPreferences(Locale.US);
	}

	public static Configuration createUsCustomaryPreferences(Locale locale) {
		Configuration prefs = PrefUtil.getDefaultUsCustomaryPreferences();
		return prefs;
	}

	/**
	 * Use {@link PrefUtil#getEmptyPreferences()} instead.
	 * 
	 * @return
	 */
	@Deprecated()
	public static Configuration getEmptyPreferences() {
		return PrefUtil.getEmptyPreferences();
	}

	public static Configuration getPreferences(Locale locale) {
		Configuration prefs = PrefUtil.getEmptyPreferences();
		PrefUtil.setLocale(locale, prefs);
		return prefs;
	}

}
