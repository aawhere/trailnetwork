/**
 * 
 */
package com.aawhere.person.group;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Before;

import com.aawhere.id.Identifier;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityTestUtil;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.person.group.SocialGroup.Builder;
import com.aawhere.test.TestUtil;

/**
 * For {@link SocialGroup}.
 * 
 * @author aroller
 * 
 */
public class SocialGroupUnitTest
		extends BaseEntityJaxbBaseUnitTest<SocialGroup, Builder, SocialGroupId, SocialGroupRepository> {

	protected static Class<? extends Identifier<?, ?>> contextIdType = SocialGroupTestUtil.CONTEXT_ID_TYPE;
	private Identifier<?, ?> context;
	private String category;
	private Integer score;
	private Integer membershipCount;
	private Integer membershipCountMutated;
	// optionally allows overriding of ids.
	SocialGroupId id;

	/**
	 * 
	 */
	public SocialGroupUnitTest() {
		super(MUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		this.context = EntityTestUtil.generateRandomMockLongEntityId();
		this.category = TestUtil.generateRandomAlphaNumeric();
		this.score = TestUtil.generateRandomInt(0, 10000);
		this.membershipCount = TestUtil.generateRandomInt();
		this.membershipCountMutated = TestUtil.generateRandomInt();
	}

	public static SocialGroupUnitTest instance() {
		SocialGroupUnitTest test = new SocialGroupUnitTest();
		test.setUp();
		test.baseSetUp();
		return test;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertEntityEquals(com.aawhere.persist.BaseEntity,
	 * com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertEntityEquals(SocialGroup expected, SocialGroup actual) {
		super.assertEntityEquals(expected, actual);
		assertEquals(expected.context(), actual.context());
		assertEquals(expected.category(), actual.category());
		assertEquals(expected.membershipCount(), actual.membershipCount());

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#getId()
	 */
	@Override
	protected SocialGroupId getId() {
		if (this.id != null) {
			return id;
		} else {
			return super.getId();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.context(context);
		builder.category(category);
		builder.membershipCount(membershipCount);
		builder.score(score);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertCreation(SocialGroup sample) {
		super.assertCreation(sample);
		assertNonMutableProperties(sample);
		assertEquals(this.membershipCount, sample.membershipCount());

	}

	/**
	 * @param sample
	 */
	private void assertNonMutableProperties(SocialGroup sample) {
		assertEquals(this.context, sample.context());
		assertEquals(this.category, sample.category());
		assertEquals(this.score, sample.score());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#mutate(com.aawhere.id.Identifier)
	 */
	@Override
	protected SocialGroup mutate(SocialGroupId id) throws EntityNotFoundException {
		SocialGroup group = getRepository().update(id).membershipCount(this.membershipCountMutated).build();
		return group;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertMutation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	protected void assertMutation(SocialGroup mutated) {
		super.assertMutation(mutated);
		assertNonMutableProperties(mutated);
		assertEquals(this.membershipCountMutated, mutated.membershipCount());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return SocialGroupTestUtil.adapters();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getClassesToBeBound()
	 */
	@Override
	protected Class<?>[] getClassesToBeBound() {
		Class<?>[] classesToBeBound = super.getClassesToBeBound();
		classesToBeBound = (Class<?>[]) ArrayUtils.add(classesToBeBound, this.context.getClass());
		return classesToBeBound;
	}

	public static class GroupMockRepository
			extends MockFilterRepository<SocialGroups, SocialGroup, SocialGroupId>
			implements SocialGroupRepository {

		/*
		 * @see
		 * com.aawhere.person.group.SocialGroupRepository#update(com.aawhere.person.group.SocialGroupId
		 * )
		 */
		@Override
		public Builder update(SocialGroupId id) throws EntityNotFoundException {
			return new Builder(load(id)) {
				/*
				 * @see com.aawhere.person.group.SocialGroup.Builder#build()
				 */
				@Override
				public SocialGroup build() {
					return update(super.build());
				}
			};
		}

	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected SocialGroupRepository createRepository() {
		return new GroupMockRepository();
	}
}
