/**
 * 
 */
package com.aawhere.person.group;

import static org.junit.Assert.*;

import org.junit.Before;

import com.aawhere.id.Identifier;
import com.aawhere.lang.If;
import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.BaseEntityBaseUnitTest;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.person.group.SocialGroupMembership.Builder;

/**
 * @author aroller
 * 
 */
public class SocialGroupMembershipUnitTest
		extends
		BaseEntityBaseUnitTest<SocialGroupMembership, SocialGroupMembership.Builder, SocialGroupMembershipId, SocialGroupMembershipRepository> {

	/* package for util */PersonId personId;
	/* package for util */SocialGroupId groupId;

	/**
	 * 
	 */
	public SocialGroupMembershipUnitTest() {
		super(NON_MUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		this.personId = If.nil(this.personId).use(PersonTestUtil.createPersonId());
		this.groupId = If.nil(this.groupId).use(SocialGroupTestUtil.socialGroupWithId().getId());

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.group(groupId);
		builder.member(personId);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertCreation(SocialGroupMembership sample) {
		super.assertCreation(sample);
		assertEquals(personId, sample.personId());
		assertEquals(groupId, sample.socialGroupId());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#getId()
	 */
	@Override
	protected SocialGroupMembershipId getId() {
		return SocialGroupMembershipTestUtil.membershipId();
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected SocialGroupMembershipRepository createRepository() {
		return new SocialGroupMembershipMockRepository();

	}

	public static class SocialGroupMembershipMockRepository
			extends MockFilterRepository<SocialGroupMemberships, SocialGroupMembership, SocialGroupMembershipId>
			implements SocialGroupMembershipRepository {

		/*
		 * @see com.aawhere.persist.ChildRepository#children(com.aawhere.id.Identifier)
		 */
		@Override
		public Iterable<SocialGroupMembership> children(Identifier<?, ? extends BaseEntity> parentId) {
			return null;
		}

		/*
		 * @see com.aawhere.persist.ChildRepository#childrenIds(com.aawhere.id.Identifier)
		 */
		@Override
		public Iterable<SocialGroupMembershipId> childrenIds(Identifier<?, ? extends BaseEntity> parentId) {
			return null;
		}

	}

}
