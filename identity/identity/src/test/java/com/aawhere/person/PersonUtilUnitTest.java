/**
 * 
 */
package com.aawhere.person;

import static com.aawhere.person.PersonTestUtil.*;
import static org.junit.Assert.*;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class PersonUtilUnitTest {

	@Test
	public void testSuggestNameSingleAccountNullName() {
		Account account = AccountTestUtil.accountWithName(null);
		final Pair<String, String> names = PersonUtil.personName(Sets.newHashSet(account));
		String name = names.getLeft();
		assertEquals("the alias is the name", account.alias(), name);
		assertEquals("one alias so the 'long' name is just the alias", account.alias(), names.getRight());
	}

	@Test
	public void testSuggestNameTwoAccountsWithoutNames() {
		Account account = AccountTestUtil.accountWithName(null);
		Account account2 = AccountTestUtil.accountWithName(null);
		final Pair<String, String> names = PersonUtil.personName(Sets.newHashSet(account, account2));
		String name = names.getLeft();
		String nameWithAliases = names.getRight();
		TestUtil.assertContains("name should be one of the aliases",
								name,
								Iterables.concat(account.aliases(), account2.aliases()));
		TestUtil.assertContains("both aliases shoudl be in name with aliases", nameWithAliases, account.alias());
		TestUtil.assertContains("both aliases shoudl be in name with aliases", nameWithAliases, account2.alias());
	}

	@Test
	public void testSuggestNameTwoAccountsWithNames() {
		Account accountWithBetterName = AccountTestUtil.accountWithName(PersonTestUtil.NAME_GOOD);
		Account accountWithWorseName = AccountTestUtil.accountWithName(PersonTestUtil.NAME_OK);
		final Pair<String, String> names = PersonUtil.personName(Sets.newHashSet(	accountWithBetterName,
																					accountWithWorseName));
		String name = names.getLeft();
		String nameWithAliases = names.getRight();
		assertEquals(accountWithBetterName.name(), name);
		TestUtil.assertContains("better name should be in there", nameWithAliases, accountWithBetterName.name());
		TestUtil.assertContains("first alias should be there", nameWithAliases, accountWithBetterName.alias());
		TestUtil.assertContains("second alias should be there", nameWithAliases, accountWithWorseName.alias());

	}

	@Test
	public void testPersonNameFromManagingAccountSelected() {
		Account mangingAccount = Account.clone(AccountTestUtil.account(KnownApplication.GOOGLE_IDENTITY_TOOLKIT.key))
				.setName(NAME_OK).build();
		Account betterNameNotManaged = AccountTestUtil.accountWithName(NAME_BETTER);
		Pair<String, String> personName = PersonUtil.personName(betterNameNotManaged, mangingAccount);
		assertEquals(	"managed account names are preferred over all others, even if it is not as good.  This preference is subject to discussion",
						mangingAccount.name(),
						personName.getLeft());
	}

	@Test
	public void testSuggestNameForAccountWithName() {
		Account account = AccountTestUtil.account();
		final Pair<String, String> names = PersonUtil.personName(account);
		String personName = names.getLeft();
		String nameWithAliases = names.getRight();
		assertEquals(account.name(), personName);
		TestUtil.assertContains("name with aliases should contain name", nameWithAliases, account.name());
		TestUtil.assertContains("alias should also be in the long name", nameWithAliases, account.alias());
	}

	@Test
	public void testHasName() {
		assertTrue(	"different name was provided",
					PersonUtil.nameIsUseful(PersonTestUtil.personWithName(NAME_OK),
											Sets.newHashSet(NAME_GOOD, NAME_BETTER)));
		assertFalse("same name as alias",
					PersonUtil.nameIsUseful(PersonTestUtil.personWithName(NAME_GOOD),
											Sets.newHashSet(NAME_GOOD, NAME_BETTER)));
		assertTrue("no aliases", PersonUtil.nameIsUseful(PersonTestUtil.person(), null));
		assertFalse("bad name", PersonUtil.nameIsUseful(PersonTestUtil.personWithName(NAME_BAD), null));
		assertFalse("no person", PersonUtil.nameIsUseful(null, null));
	}
}
