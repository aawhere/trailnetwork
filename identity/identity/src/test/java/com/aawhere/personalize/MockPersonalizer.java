/**
 *
 */
package com.aawhere.personalize;

import org.apache.commons.configuration.Configuration;

/**
 * Fake personalizer used for testing {@link BasePersonalizer}.
 * 
 * @author brian
 * @see BasePersonalizerUnitTest
 */
public class MockPersonalizer
		extends BasePersonalizer<MockPersonalizer.MockDisplay> {

	public static class Builder
			extends BasePersonalizer.Builder<MockPersonalizer, Builder> {

		public Builder() {
			super(new MockPersonalizer());
		}
	}

	public static final String NAME = "Name";

	MockDisplay result;

	@Override
	public void personalize(Object context) {
		result = new MockDisplay(NAME);
	}

	@Override
	public MockDisplay getResult() {
		return result;
	}

	public Configuration getPrefs() {
		return prefs;
	}

	public static class MockDisplay
			extends PersonalizedBase {
		public MockDisplay(String name) {
			this.name = name;
		}

		public String name = MockPersonalizer.NAME;

		public MockDisplay getPersonalized() {
			return (MockDisplay) personalized;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.personalize.Personalizer#handlesType()
	 */
	@Override
	public Class<String> handlesType() {
		return String.class;
	}
}
