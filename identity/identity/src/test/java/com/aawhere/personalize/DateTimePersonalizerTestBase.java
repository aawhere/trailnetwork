/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import java.util.Locale;

import org.apache.commons.configuration.Configuration;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.person.pref.PrefTestUtil;
import com.aawhere.person.pref.PrefUtil;

/**
 * 
 * Base test class for all Personalizers extending {@link BaseDateTimePersonalizer}.
 * 
 * @author Brian Chapman
 * 
 */
@SuppressWarnings("rawtypes")
public abstract class DateTimePersonalizerTestBase<PlizerT extends BasePersonalizer> {

	DateTime date;
	PlizerT plizer;
	Configuration prefs;

	@Before
	public void setUp() {
		prefs = PrefUtil.getPreferences(Locale.US, PrefTestUtil.createSiPreferences(), PrefUtil.getEmptyPreferences());
		date = new DateTime();
		plizer = createPersonalizer();
	}

	protected abstract PlizerT createPersonalizer();

	@Test
	public void testBuilder() {
		assertNotNull(plizer);
	}

	/**
	 * Test method for {@link com.aawhere.personalize.DateTimeDisplayPersonalizer#handlesType()}.
	 */
	@Test
	public void testHandlesType() {
		assertEquals(DateTime.class, plizer.handlesType());
	}

}
