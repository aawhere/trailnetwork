/**
 *
 */
package com.aawhere.personalize;

import java.util.Locale;

import com.aawhere.joda.time.xml.DateTimeXmlAdapter;
import com.aawhere.measure.xml.QuantityXmlAdapter;
import com.aawhere.person.pref.PrefUtil;

/**
 * @author Brian Chapman
 * 
 */
public class MeasurePersonalizeTestUtil {

	public static QuantityPersonalizer createQuantityPersonalizer() {
		return new QuantityPersonalizer.Builder().setPreferences(PrefUtil.getPreferences(Locale.US, PrefUtil
				.getDefaultUsCustomaryPreferences())).build();
	}

	public static DateTimePersonalizer createDateTimePersonalizer() {
		return new DateTimePersonalizer.Builder().setPreferences(PrefUtil.getPreferences(Locale.US, PrefUtil
				.getDefaultUsCustomaryPreferences())).build();
	}

	public static QuantityXmlAdapter createQuantityXmlAdapter() {
		return new QuantityXmlAdapter(createQuantityPersonalizer());
	}

	public static DateTimeXmlAdapter createDateTimeXmlAdapter() {
		return new DateTimeXmlAdapter(createDateTimePersonalizer());
	}

}
