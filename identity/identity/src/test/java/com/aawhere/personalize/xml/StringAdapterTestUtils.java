/**
 * 
 */
package com.aawhere.personalize.xml;

import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringAdapterFactory;
import com.aawhere.measure.QuantityStringAdapter;

/**
 * Provides {@link StringAdapter} related items setup for testing with personalization capabilities.
 * 
 * Other uses of StringAdapter that don't want to be setup for all should just construct their own
 * {@link StringAdapterFactory}.
 * 
 * @author aroller
 * 
 */
public class StringAdapterTestUtils {

	public static StringAdapterFactory getFactory() {
		StringAdapterFactory factory = new StringAdapterFactory();
		factory.register((StringAdapter) new QuantityStringAdapter.Builder().build());
		return factory;
	}
}
