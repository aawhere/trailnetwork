/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

/**
 * @author Brian Chapman
 * 
 */
public class DateTimeDisplayPersonalizerUnitTest
		extends DateTimePersonalizerTestBase<DateTimeDisplayPersonalizer> {

	public DateTimeDisplayPersonalizer createPersonalizer() {
		return new DateTimeDisplayPersonalizer.Builder().setPreferences(prefs).setPreferences(prefs).build();
	}

	@Test
	public void testGetResult() {
		plizer.personalize(date);
		DateTimeDisplay result = plizer.getResult();

		assertNotNull(result);
		assertEquals(DateTimeFormat.mediumDateTime().print(date), result.mediumDateTime);
	}
}
