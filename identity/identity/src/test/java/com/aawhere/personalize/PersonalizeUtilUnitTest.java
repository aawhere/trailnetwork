/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.junit.Test;

import com.aawhere.i18n.LocaleUtil;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

/**
 * @author brian
 * 
 */
public class PersonalizeUtilUnitTest {

	@Test
	public void testWrapperKey() {
		String formattedWrapper = PersonalizeUtil.formatWrapperOpenTag(PersonalizeMessage.Param.WRAPPER_KEY);
		assertNotNull(formattedWrapper);
		assertEquals("<span class=\"wrapper-key\">", formattedWrapper);
	}

	@Test
	public void testAny() {
		Locale input = LocaleUtil.any();
		Locale expected = PersonalizeUtil.DEFAULT;
		assertLocale(input, expected);
	}

	/**
	 * @param input
	 * @param expected
	 */
	private void assertLocale(Locale input, Locale expected) {
		TestHttpHeaders headers = new TestHttpHeaders();
		if (input != null) {
			headers.acceptableLanguages.add(input);
		}
		headers.requestHeaders.put(PersonalizeUtil.ACCEPT_I18N, Boolean.TRUE.toString());
		Locale locale = PersonalizeUtil.locale(headers);
		assertEquals(expected, locale);
	}

	@Test
	public void testInvalid() {
		assertLocale(new Locale("bogus"), PersonalizeUtil.DEFAULT);
	}

	@Test
	public void testNoLanguageRequested() {
		assertLocale(null, PersonalizeUtil.DEFAULT);
	}

	@Test
	public void testFrenchRequested() {
		Locale france = Locale.FRANCE;
		assertLocale(france, france);
	}

	@Test
	public void testAcceptNoLocalization() {
		Locale requested = Locale.FRANCE;
		TestHttpHeaders headers = new TestHttpHeaders();
		headers.acceptableLanguages.add(requested);
		// no accept localization
		Locale locale = PersonalizeUtil.locale(headers);
		assertNull(locale);
	}

	public static class TestHttpHeaders
			implements HttpHeaders {
		Multimap<String, String> requestHeaders = HashMultimap.create();
		List<Locale> acceptableLanguages = Lists.newArrayList();

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.HttpHeaders#getRequestHeader(java.lang.String)
		 */
		@Override
		public List<String> getRequestHeader(String name) {
			return Lists.newArrayList(requestHeaders.get(name));
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.HttpHeaders#getRequestHeaders()
		 */
		@Override
		public MultivaluedMap<String, String> getRequestHeaders() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.HttpHeaders#getAcceptableMediaTypes()
		 */
		@Override
		public List<MediaType> getAcceptableMediaTypes() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.HttpHeaders#getAcceptableLanguages()
		 */
		@Override
		public List<Locale> getAcceptableLanguages() {
			return acceptableLanguages;
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.HttpHeaders#getMediaType()
		 */
		@Override
		public MediaType getMediaType() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.HttpHeaders#getLanguage()
		 */
		@Override
		public Locale getLanguage() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.HttpHeaders#getCookies()
		 */
		@Override
		public Map<String, Cookie> getCookies() {
			throw new RuntimeException("TODO:Implement this");
		}

	}
}
