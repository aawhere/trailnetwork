/**
 *
 */
package com.aawhere.personalize.xml;

import static org.junit.Assert.*;

import java.util.Locale;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.junit.Test;

import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Tests {@link XmlLang}.
 * 
 * @author roller
 * 
 */
public class XmlLangUnitTest {

	/**
	 *
	 */
	private static final String XML_LANG_EN_US = "xml:lang=\"en-US\"";

	@Test
	public void testLanguageOnly() throws Exception {
		String expected = "en";
		Locale locale = Locale.ENGLISH;
		assertLang(expected, locale);
	}

	@Test
	public void testLanguageCountry() {
		assertLang("en-US", Locale.US);
	}

	/**
	 * @param expected
	 * @param locale
	 */
	private void assertLang(String expected, Locale locale) {
		XmlLang lang = XmlLang.create().locale(locale).build();
		assertEquals(expected, lang.value);
	}

	@Test
	public void testJaxb() {
		LangExample expected = new LangExample();
		String expectedContains = XML_LANG_EN_US;
		Locale locale = Locale.US;
		expected.lang = new XmlLang.Builder().locale(locale).build();
		assertJaxbRoundTrip(expected, expectedContains);
	}

	/**
	 * @param expected
	 * @param expectedContains
	 * @return
	 */
	private JAXBTestUtil<LangExample> assertJaxbRoundTrip(LangExample expected, String expectedContains) {
		JAXBTestUtil<LangExample> util = JAXBTestUtil.create(expected).build();
		util.assertContains(expectedContains);
		return util;
	}

	@Test
	public void testJaxbLangIsNull() {
		assertJaxbRoundTrip(new LangExample(), LangExample.ROOT_NAME);
	}

	@Test
	public void testJaxbLocale() {
		LangExample expected = new LangExample();
		expected.locale = Locale.US;
		JAXBTestUtil<LangExample> util = assertJaxbRoundTrip(expected, "locale=\"en-US\"");
		assertEquals(expected.locale, util.getActual().locale);
	}

	@Test
	public void testToLocaleLanguageCountry() {
		Locale expected = Locale.CANADA_FRENCH;
		assertToLocale(expected);
	}

	@Test
	public void testToLocaleLanguage() {
		assertToLocale(Locale.FRENCH);
	}

	@Test(expected = InvalidChoiceException.class)
	public void testToLocaleVariant() {
		assertToLocale(new Locale("en", "US", "bogus"));
	}

	@Test
	public void testLocaleAdapter() throws Exception {
		LocaleXmlAdapter adapter = new LocaleXmlAdapter();
		Locale expected = Locale.US;
		XmlLang lang = adapter.marshal(expected);
		Locale actual = adapter.unmarshal(lang);
		assertEquals(expected, actual);
	}

	/**
	 * @param expected
	 */
	private void assertToLocale(Locale expected) {
		Locale actual = XmlLang.create().locale(expected).build().toLocale();
		assertEquals(expected, actual);
	}

	@XmlRootElement(name = LangExample.ROOT_NAME)
	public static class LangExample {

		/**
		 *
		 */
		static final String ROOT_NAME = "root";
		@XmlAttribute(namespace = XmlLang.NAMESPACE, name = XmlLang.ATTRIBUTE_NAME)
		XmlLang lang;

		// This could be named to match xml:lang, but duplicate names cause conflict
		@XmlAttribute
		@XmlJavaTypeAdapter(LocaleXmlAdapter.class)
		Locale locale;

	}

}
