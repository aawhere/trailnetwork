/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Brian Chapman
 * 
 */
public class DateTimePersonalizedPersonalizerUnitTest
		extends DateTimePersonalizerTestBase<DateTimePersonalizedPersonalizer> {

	public DateTimePersonalizedPersonalizer createPersonalizer() {
		return new DateTimePersonalizedPersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testGetResult() {
		plizer.personalize(date);
		DateTimePersonalized result = plizer.getResult();

		assertNotNull(result);
		assertEquals(date.toString(), result.getDate());

	}
}
