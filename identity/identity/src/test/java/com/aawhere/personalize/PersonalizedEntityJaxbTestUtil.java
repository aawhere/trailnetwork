/**
 *
 */
package com.aawhere.personalize;

import javax.xml.bind.JAXBException;

import com.aawhere.persist.BaseEntity;
import com.aawhere.persist.BaseEntityBaseUnitTest;
import com.aawhere.util.rb.MessageDisplay;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Works with {@link BaseEntityBaseUnitTest} to make sure an implementation of {@link BaseEntity} is
 * able to be marshalled and unmarshalled using the {@link JAXBTestUtil}.
 * 
 * @deprecated Duplicates BaseEntityJaxbBaseUnitTest
 * @author roller
 * 
 */
@Deprecated
public class PersonalizedEntityJaxbTestUtil {

	/**
	 * Runs the {@link #getEntitySample()} through a JAXB Marshalling/Unmarshalling cycle and
	 * verifies properties are being handled appropriately.
	 * 
	 * TODO: promote this to BaseEntityBaseUnitTest
	 * 
	 * @return
	 * 
	 * @throws JAXBException
	 */
	public static <E extends BaseEntity<E, ?>> JAXBTestUtil<E> testJaxbRoundTrip(
			BaseEntityBaseUnitTest<E, ?, ?, ?> unitTest) throws JAXBException {
		E sample = unitTest.getEntitySample();

		// just use default personalizers for adaptation
		JAXBTestUtil<E> util = JAXBTestUtil.create(sample).classesToBeBound(MessageDisplay.class).build();
		E actual = util.getActual();

		// Ensure the sample was "re-created" appropriately
		unitTest.assertCreation(actual);
		unitTest.assertEntityEquals(sample, actual);
		return util;
	}

}
