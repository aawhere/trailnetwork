package com.aawhere.personalize;

import static org.junit.Assert.*;

import java.io.StringWriter;

import javax.measure.quantity.Quantity;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.Unit;
import javax.xml.bind.JAXBException;

import org.junit.Test;

import com.aawhere.measure.ElevationChange;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.person.pref.Preferences;
import com.aawhere.test.TestUtil;

import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;

/**
 * Verifies that Quantities are Personalized correctly.
 * 
 * TODO: refactor to create a base unit test that can be used by different quantity types.
 * 
 * @author Brian Chapman
 * 
 */
public class QuantityPersonalizerUnitTest
		extends QuantityPersonalizerTestBase<QuantityPersonalizer> {

	@Override
	protected QuantityPersonalizer createPersonalizer() {
		return new QuantityPersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testGetDistanceResult() {
		assertEquals(	MeasurementUtil.getUnitSymbol(MetricSystem.KILO(MetricSystem.METRE)),
						prefs.getString(Preferences.DISTANCE_UNIT.getKey()));
		plizer.personalize(quantity);
		QuantityPersonalized result = plizer.getResult();

		assertNotNull(result);
		assertEquals("length", result.getType());
		assertEquals(quantity.doubleValue(MetricSystem.METRE), result.getValue(), 0.001);
		assertEquals("m", result.getUnit());

		assertNotNull(result.getPersonalized());
		QuantityDisplay quantityDisplay = result.getPersonalized();
		assertEquals(QuantityPersonalizerTestBase.FORMATED_QUANTITY, quantityDisplay.getFormattedValue());
	}

	/**
	 * @param expected
	 * @param expectedUnitSymbol
	 * @param expectedDisplayValue
	 * @param expectedUnit
	 * @return
	 */
	private <Q extends Quantity<Q>> QuantityPersonalized assertUnit(Q expected, String expectedUnitSymbol,
			String expectedDisplayValue, Unit<Q> expectedUnit) {
		String unitSymbol = MeasurementUtil.getUnitSymbol(expectedUnit);
		assertEquals(expectedUnitSymbol, unitSymbol);
		Personalizer personalizer = createPersonalizer();
		personalizer.personalize(expected);
		QuantityPersonalized result = (QuantityPersonalized) personalizer.getResult();
		QuantityDisplay personalized = result.getPersonalized();
		assertEquals(expectedDisplayValue, personalized.getFormattedValue());
		assertEquals(unitSymbol, personalized.getAbbreviatedUnit());
		TestUtil.assertContains(personalized.getFormatted(), unitSymbol);
		return result;
	}

	@Test
	// @Ignore("https://aawhere.jira.com/browse/TN-127")
			public
			void testPercent() {
		Double expectedRatio = 0.123456;
		QuantityPersonalized personalizer = assertUnit(	MeasurementUtil.createRatio(expectedRatio),
														ExtraUnits.PERCENT_SYMBOL,
														"12",
														ExtraUnits.PERCENT);

		assertEquals("12%", personalizer.getPersonalized().getFormatted());

	}

	@Test
	public void testLatitude() {
		Double expectedValue = 12.34567890;
		Latitude expected = MeasurementUtil.createLatitude(expectedValue);
		String unitSymbol = MeasurementUtil.getUnitSymbol(ExtraUnits.DECIMAL_DEGREES_LATITUDE);
		assertNotNull(unitSymbol);
		Personalizer personalizer = createPersonalizer();
		personalizer.personalize(expected);
		QuantityPersonalized result = (QuantityPersonalized) personalizer.getResult();
		assertEquals(0.00001, expectedValue.doubleValue(), result.getValue().doubleValue());
		assertEquals("12.345680 °", result.getPersonalized().getFormatted());
		assertEquals(ExtraUnits.DEGREES_SYMBOL, result.getUnit());
	}

	/**
	 * Lets make sure the json format is correct.
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void jsonResponse() throws JAXBException {
		plizer.personalize(quantity);
		QuantityPersonalized result = plizer.getResult();

		JSONJAXBContext jc = new JSONJAXBContext(QuantityDisplay.class, PersonalizedBase.class,
				QuantityPersonalized.class);

		JSONMarshaller jsonMarshaller = jc.createJSONMarshaller();
		StringWriter writer = new StringWriter();
		jsonMarshaller.marshallToJSON(result, System.out);
		jsonMarshaller.marshallToJSON(result, writer);
		String actual = writer.toString();
		TestUtil.assertContains(actual, "\"display\":");
	}

	@Test
	public void testGetElevationResult() {
		assertEquals(	MeasurementUtil.getUnitSymbol(MetricSystem.METRE),
						prefs.getString(Preferences.ELEVATION_UNIT.getKey()));
		plizer.personalize(quantity);
		QuantityPersonalized result = plizer.getResult();

		assertNotNull(result);
		assertEquals("length", result.getType());
		assertEquals(quantity.doubleValue(MetricSystem.METRE), result.getValue(), 0.001);
		assertEquals("m", result.getUnit());

		assertNotNull(result.getPersonalized());
		QuantityDisplay quantityDisplay = result.getPersonalized();
		assertEquals(QuantityPersonalizerTestBase.FORMATED_QUANTITY, quantityDisplay.getFormattedValue());
	}

	@Test
	public void testElevationChange() {
		Double value = 12.34567890;
		Integer expectedValue = 12;
		Unit<ElevationChange> unit = ExtraUnits.METER_CHANGE;
		String unitSymbol = MeasurementUtil.getUnitSymbol(unit);
		assertNotNull(unitSymbol);
		ElevationChange expected = MeasurementUtil.createElevationChange(value, unit);
		Personalizer personalizer = createPersonalizer();
		personalizer.personalize(expected);
		QuantityPersonalized result = (QuantityPersonalized) personalizer.getResult();
		assertEquals(0.00001, expectedValue, result.getValue().doubleValue());
		assertEquals("" + expectedValue + " " + unitSymbol, result.getPersonalized().getFormatted());
		assertEquals(unitSymbol, result.getUnit());
	}

	@Test
	public void testGetResultClassType() {
		plizer.personalize(quantity);
		assertEquals(QuantityPersonalized.class, plizer.getResult().getClass());
		// QuantityPersonalized result = plizer.getResult();
	}
}
