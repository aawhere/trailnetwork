/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import java.util.Locale;

import org.apache.commons.configuration.Configuration;
import org.joda.time.Duration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.person.pref.PrefTestUtil;
import com.aawhere.person.pref.PrefUtil;

/**
 * 
 * Base test class for all Personalizers extending {@link BaseDurationPersonalizer}.
 * 
 * @author Brian Chapman
 * 
 */
@SuppressWarnings("rawtypes")
public abstract class DurationPersonalizerTestBase<PlizerT extends BasePersonalizer> {

	private final Long durationMillis = 10000000L;
	final String expectedCompact = "2:46:40";
	Duration duration;
	PlizerT plizer;
	Configuration prefs;

	@Before
	public void setUp() {
		prefs = PrefUtil.getPreferences(Locale.US, PrefTestUtil.createSiPreferences(), PrefUtil.getEmptyPreferences());
		duration = new Duration(durationMillis);
		plizer = createPersonalizer();
	}

	protected abstract PlizerT createPersonalizer();

	@Test
	public void testBuilder() {
		assertNotNull(plizer);
	}

	/**
	 * Test method for {@link com.aawhere.personalize.DurationDisplayPersonalizer#handlesType()}.
	 */
	@Test
	public void testHandlesType() {
		assertEquals(Duration.class, plizer.handlesType());
	}

}
