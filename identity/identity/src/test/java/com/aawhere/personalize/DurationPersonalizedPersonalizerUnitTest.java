/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Brian Chapman
 * 
 */
public class DurationPersonalizedPersonalizerUnitTest
		extends DurationPersonalizerTestBase<DurationPersonalizedPersonalizer> {

	public DurationPersonalizedPersonalizer createPersonalizer() {
		return new DurationPersonalizedPersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testGetResult() {
		plizer.personalize(duration);
		DurationPersonalized result = plizer.getResult();

		assertNotNull(result);
		assertEquals(new Long(duration.getMillis()), result.getDuration());

	}
}
