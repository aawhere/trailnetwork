/**
 * 
 */
package com.aawhere.personalize.xml;

import static org.junit.Assert.assertEquals;

import javax.measure.quantity.Length;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Works with all personalization of values that may be used as {@link FilterCondition}
 * {@link FilterCondition#getValue()}.
 * 
 * @author aroller
 * 
 */
public class PersonalizedFilterConditionUnitTest {

	@Test
	@Ignore("this is disabled while the value isn't being transported waiting for filter condition handling.")
	public void testLengthValue() {
		Length length = MeasurementUtil.createLengthInMeters(32.5);
		FilterCondition<Length> condition = new FilterCondition.Builder<Length>().field("fieldName")
				.operator(FilterOperator.GREATER_THAN).value(length).build();
		JAXBTestUtil<FilterCondition<Length>> util = JAXBTestUtil.create(condition).build();
		FilterCondition<Length> actual = util.getActual();
		assertEquals(length, actual.getValue());
	}
}
