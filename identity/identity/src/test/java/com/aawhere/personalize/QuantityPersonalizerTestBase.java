/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import java.util.Locale;

import javax.measure.quantity.Length;
import javax.measure.unit.MetricSystem;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.measure.MeasureTestUtil;
import com.aawhere.person.pref.PrefTestUtil;
import com.aawhere.person.pref.PrefUtil;

/**
 * 
 * Base test class for all Personalizers extending {@link BaseQuantityPersonalizer}.
 * 
 * @author Brian Chapman
 * 
 */
@SuppressWarnings("rawtypes")
public abstract class QuantityPersonalizerTestBase<PlizerT extends BaseQuantityPersonalizer> {

	final static String FORMATED_QUANTITY = "1,234.6";
	final static Double DOUBLE_QUANTITY = 1234567.89d;

	Length quantity;
	PlizerT plizer;
	Configuration prefs;

	@Before
	public void setUp() {
		prefs = PrefUtil.getPreferences(Locale.US, PrefTestUtil.createSiPreferences(), PrefUtil.getEmptyPreferences());
		quantity = MeasureTestUtil.createLength(DOUBLE_QUANTITY, MetricSystem.METRE);
		plizer = createPersonalizer();
	}

	protected abstract PlizerT createPersonalizer();

	@Test
	public void testBuilder() {
		assertNotNull(plizer);
	}

}
