package com.aawhere.personalize;

import static org.junit.Assert.*;

import javax.measure.unit.MetricSystem;

import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.person.pref.Preferences;

/**
 * @author Brian Chapman
 * 
 */
public class QuantityPersonalizedPersonalizerUnitTest
		extends QuantityPersonalizerTestBase<QuantityPersonalizedPersonalizer> {

	@Override
	protected QuantityPersonalizedPersonalizer createPersonalizer() {
		return new QuantityPersonalizedPersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testGetResult() {
		assertEquals(	MeasurementUtil.getUnitSymbol(MetricSystem.KILO(MetricSystem.METRE)),
						prefs.getString(Preferences.DISTANCE_UNIT.getKey()));
		plizer.personalize(quantity);
		QuantityPersonalized result = (QuantityPersonalized) plizer.getResult();

		assertNotNull(result);
		assertEquals("length", result.getType());
		assertEquals(quantity.doubleValue(MetricSystem.METRE), result.getValue(), 0.001);
		assertEquals("m", result.getUnit());
		assertNull(result.getPersonalized());
	}
}
