/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

/**
 * @author Brian Chapman
 * 
 */
public class DateTimePersonalizerUnitTest
		extends DateTimePersonalizerTestBase<DateTimePersonalizer> {

	public DateTimePersonalizer createPersonalizer() {
		return new DateTimePersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testGetResult() {
		plizer.personalize(date);
		DateTimePersonalized result = plizer.getResult();

		assertNotNull(result);
		assertEquals(date.toString(), result.getDate());

		DateTimeDisplay display = (DateTimeDisplay) result.getPersonalized();
		assertEquals(DateTimeFormat.mediumDateTime().print(date), display.mediumDateTime);
	}
}
