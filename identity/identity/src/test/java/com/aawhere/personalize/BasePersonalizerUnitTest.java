/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import java.util.Locale;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.person.pref.PrefTestUtil;
import com.aawhere.person.pref.PrefUtil;

/**
 * Tests {@link BasePersonalizer} using a {@link MockPersonalizer}.
 * 
 * @author brian
 * 
 */
public class BasePersonalizerUnitTest {

	MockPersonalizer.Builder pBuilder;
	String context;
	Configuration prefs;

	@Before
	public void setUp() {
		prefs = PrefUtil.getPreferences(Locale.US, PrefTestUtil.createSiPreferences(), PrefUtil.getEmptyPreferences());
		context = "Context";
		pBuilder = new MockPersonalizer.Builder().setPreferences(prefs);
	}

	@Test
	public void testCreatePersonalizer() {
		MockPersonalizer plizer = pBuilder.build();
		assertNotNull(plizer);
	}

	@Test(expected = NullPointerException.class)
	public void testCreatePersonalizerWithoutDefaultPrefs() {
		pBuilder.setPreferences(null).build();
	}

	@Test
	public void testPrefs() {
		MockPersonalizer plizer = pBuilder.build();
		Configuration prefs = plizer.getPrefs();
		assertNotNull(prefs);
		assertEquals("en_US", prefs.getString("locale"));
	}

	@Test
	public void testPersonalizeType() {
		MockPersonalizer plizer = pBuilder.build();
		String result = plizer.personalizeType(context);
		assertEquals("string", result);
	}

	@Test
	public void testgetLocale() {
		MockPersonalizer plizer = pBuilder.build();
		Locale result = plizer.getLocale();
		assertEquals(Locale.US, result);
	}

	@Test
	public void testPersonalizedLocale() {
		MockPersonalizer plizer = pBuilder.build();
		Locale result = plizer.getLocale();
		assertEquals(Locale.US, result);
	}
}
