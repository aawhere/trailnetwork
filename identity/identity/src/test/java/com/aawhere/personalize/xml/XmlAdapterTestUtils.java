/**
 * 
 */
package com.aawhere.personalize.xml;

import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.measure.xml.NumberPersonalizedXmlAdapter;
import com.aawhere.persist.FilterConditionStringAdapter;
import com.aawhere.persist.FilterConditionSupportedTypeRegistry;
import com.aawhere.persist.FilterConditionXmlAdapter;
import com.aawhere.personalize.MeasurePersonalizeTestUtil;
import com.aawhere.util.rb.MessagePersonalizerUnitTest;

/**
 * Utilities for accessing adaptors
 * 
 * @author Brian Chapman
 * 
 */
public class XmlAdapterTestUtils {

	public static Set<XmlAdapter<?, ?>> getAdapters() {
		Set<XmlAdapter<?, ?>> adapters = new HashSet<XmlAdapter<?, ?>>();
		Locale locale = Locale.US;

		final NumberFormat formatter = NumberFormat.getNumberInstance(locale);
		adapters.add(new NumberPersonalizedXmlAdapter.Builder().formatter(formatter).build());
		adapters.add(MeasurePersonalizeTestUtil.createQuantityXmlAdapter());
		adapters.add(MeasurePersonalizeTestUtil.createDateTimeXmlAdapter());
		adapters.add(MessagePersonalizerUnitTest.getInstance().getAdapter());

		return adapters;
	}

	/**
	 * Provides all adapters from {@link #getAdapters()} in addition to those that require a
	 * {@link FieldDictionaryFactory} for field dependencies.
	 * 
	 * @param dictionaryFactory
	 * @return
	 */
	public static Set<XmlAdapter<?, ?>> getAdapters(FieldDictionaryFactory dictionaryFactory) {

		Set<XmlAdapter<?, ?>> adapters = getAdapters();
		adapters.add(getFilterConditionXmlAdapter(dictionaryFactory));
		return adapters;
	}

	public static FilterConditionXmlAdapter getFilterConditionXmlAdapter(FieldDictionaryFactory dictionaryFactory) {
		FilterConditionStringAdapter filterConditionStringAdapter = (FilterConditionStringAdapter) new FilterConditionStringAdapter.Builder()
				.setAdapterFactory(StringAdapterTestUtils.getFactory()).setDictionaryFactory(dictionaryFactory)
				.setRegistry(new FilterConditionSupportedTypeRegistry.Builder().build()).build();
		FilterConditionXmlAdapter filterConditionXmlAdapter = new FilterConditionXmlAdapter.Builder()
				.setStringAdapter(filterConditionStringAdapter).build();
		return filterConditionXmlAdapter;
	}
}
