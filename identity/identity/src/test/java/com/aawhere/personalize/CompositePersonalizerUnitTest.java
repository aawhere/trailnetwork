/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import java.util.Locale;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.person.pref.PrefUtil;
import com.aawhere.personalize.MockPersonalizer.MockDisplay;

/**
 * @author brian
 * 
 */
public class CompositePersonalizerUnitTest {

	MockPersonalizer plizer1;
	MockPersonalizer plizer2;
	String context;

	@Before
	public void setUp() {
		Configuration prefs = PrefUtil.getPreferences(	Locale.US,
														PrefUtil.getDefaultSiPreferences(),
														PrefUtil.getEmptyPreferences());
		context = "context";
		plizer1 = new MockPersonalizer.Builder().setPreferences(prefs).build();
		plizer2 = new MockPersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testCreate() {
		Personalizer composite = new CompositePersonalizer.Builder<String>().add(plizer1).handlesType(String.class)
				.add(plizer2).build();
		assertNotNull(composite);
	}

	@Test(expected = NullPointerException.class)
	public void testCreateHandlesTypeCannotBeNull() {
		new CompositePersonalizer.Builder<String>().add(plizer1).add(plizer2).build();
	}

	@Test
	public void testGetResult() {
		Personalizer composite = new CompositePersonalizer.Builder<String>().add(plizer1).handlesType(String.class)
				.add(plizer2).build();
		composite.personalize(context);
		MockDisplay result = (MockDisplay) composite.getResult();

		assertNotNull(result);
		assertNotNull(result.getPersonalized());
		assertEquals(MockPersonalizer.NAME, result.name);
		assertEquals(MockPersonalizer.NAME, result.getPersonalized().name);
	}
}
