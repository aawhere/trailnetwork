/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Brian Chapman
 * 
 */
public class DurationPersonalizerUnitTest
		extends DurationPersonalizerTestBase<DurationPersonalizer> {

	public DurationPersonalizer createPersonalizer() {
		return new DurationPersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testGetResult() {
		plizer.personalize(duration);
		DurationPersonalized result = plizer.getResult();

		assertNotNull(result);
		assertEquals(new Long(duration.getMillis()), result.getDuration());

		DurationDisplay display = (DurationDisplay) result.getPersonalized();
		assertEquals(expectedCompact, display.compact);
	}
}
