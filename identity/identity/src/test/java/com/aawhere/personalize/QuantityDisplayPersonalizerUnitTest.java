package com.aawhere.personalize;

import static org.junit.Assert.*;

import javax.measure.unit.MetricSystem;

import org.junit.Test;

import com.aawhere.measure.MeasurementQuantityFactory;
import com.aawhere.measure.Ratio;
import com.aawhere.measure.unit.ExtraUnits;
import com.aawhere.person.pref.Preferences;

/**
 * @author Brian Chapman
 * 
 */
public class QuantityDisplayPersonalizerUnitTest
		extends QuantityPersonalizerTestBase<QuantityDisplayPersonalizer> {

	@Override
	protected QuantityDisplayPersonalizer createPersonalizer() {
		return new QuantityDisplayPersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testGetResult() {
		plizer.personalize(quantity);
		QuantityDisplay result = plizer.getResult();

		assertNotNull(result);
		assertEquals(prefs.getString(Preferences.LOCALE.getKey()), result.getLang().toString());
		assertEquals(FORMATED_QUANTITY + " km", result.getFormatted());
		assertEquals(FORMATED_QUANTITY, result.getFormattedValue());
		assertEquals(quantity.doubleValue(MetricSystem.KILO(MetricSystem.METRE)), result.getValue(), 0.001);
		assertEquals("km", result.getAbbreviatedUnit());
		assertNull(result.getPersonalized());
	}

	@Test
	public void testGetRatioResult() {
		String FORMATTED_RATIO = "100";
		Ratio quantity = MeasurementQuantityFactory.getInstance(Ratio.class).create(1, ExtraUnits.RATIO);
		plizer.personalize(quantity);
		QuantityDisplay result = plizer.getResult();

		assertNotNull(result);
		assertEquals(prefs.getString(Preferences.LOCALE.getKey()), result.getLang().toString());
		// Note no space.
		assertEquals(FORMATTED_RATIO + "%", result.getFormatted());
		assertEquals(FORMATTED_RATIO, result.getFormattedValue());
		assertEquals(quantity.doubleValue(ExtraUnits.PERCENT), result.getValue(), 0.001);
		assertEquals("%", result.getAbbreviatedUnit());
		assertNull(result.getPersonalized());
	}
}
