/**
 *
 */
package com.aawhere.personalize;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Brian Chapman
 * 
 */
public class DurationDisplayPersonalizerUnitTest
		extends DurationPersonalizerTestBase<DurationDisplayPersonalizer> {

	public DurationDisplayPersonalizer createPersonalizer() {
		return new DurationDisplayPersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testGetResult() {
		plizer.personalize(duration);
		DurationDisplay result = plizer.getResult();

		assertNotNull(result);
		assertEquals(expectedCompact, result.getCompact());
	}
}
