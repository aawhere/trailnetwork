/**
 *
 */
package com.aawhere.joda.time.xml;

import static org.junit.Assert.*;

import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.Duration;
import org.junit.Test;

import com.aawhere.person.pref.PrefUtil;
import com.aawhere.personalize.DurationPersonalized;
import com.aawhere.personalize.DurationPersonalizer;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author Brian Chapman
 * 
 */
public class DurationXmlAdapterUnitTest {

	static Long MILLIS = 10000000L;

	@Test
	public void testDuration() throws Exception {
		Duration source = new Duration(MILLIS);
		DurationPersonalizer plizer = new DurationPersonalizer.Builder()
				.setPreferences(PrefUtil.getPreferences(Locale.US, PrefUtil.getDefaultUsCustomaryPreferences()))
				.build();
		DurationXmlAdapter adapter = new DurationXmlAdapter(plizer);
		DurationPersonalized marshalled = adapter.marshal(source);
		Duration actual = adapter.unmarshal(marshalled);
		assertEquals(source, actual);
	}

	@Test
	public void testJaxb() {
		JAXBTestUtil<TestDuration> util = JAXBTestUtil.create(new TestDuration()).build();
		assertEquals(util.getXml(), util.getExpected().duration, util.getActual().duration);
		assertNull(util.getXml(), util.getActual().nullDuration);
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	static public class TestDuration {
		@XmlJavaTypeAdapter(DurationXmlAdapter.class)
		Duration duration = new Duration(MILLIS);
		@XmlJavaTypeAdapter(DurationXmlAdapter.class)
		Duration nullDuration;
	}
}
