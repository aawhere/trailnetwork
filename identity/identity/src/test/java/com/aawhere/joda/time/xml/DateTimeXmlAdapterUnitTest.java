/**
 *
 */
package com.aawhere.joda.time.xml;

import static org.junit.Assert.*;

import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;

import com.aawhere.joda.time.JodaTestUtil;
import com.aawhere.person.pref.PrefUtil;
import com.aawhere.personalize.DateTimePersonalized;
import com.aawhere.personalize.DateTimePersonalizer;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * Converts {@link DateTime} into XML and back again.
 * 
 * @author Aaron Roller
 * 
 */
public class DateTimeXmlAdapterUnitTest {

	@Test
	public void testZulu() throws Exception {
		DateTime date = new DateTime();
		String expectedString = date.toString();
		DateTimePersonalizer plizer = new DateTimePersonalizer.Builder()
				.setPreferences(PrefUtil.getPreferences(Locale.US, PrefUtil.getDefaultUsCustomaryPreferences()))
				.build();
		DateTimeXmlAdapter adapter = new DateTimeXmlAdapter(plizer);

		DateTimePersonalized actualPersonalized = adapter.marshal(date);
		assertEquals(expectedString, actualPersonalized.getDate());

		DateTime actualDateTime = adapter.unmarshal(actualPersonalized);
		assertNotNull(actualDateTime);
		assertEquals(date.getMillis(), actualDateTime.getMillis());
		assertEquals(date.getChronology(), actualDateTime.getChronology());
		assertEquals(date.getZone(), actualDateTime.getZone());
		assertEquals(date, actualDateTime);
	}

	@Test
	public void testNow() throws Exception {
		DateTime expected = new DateTime();
		DateTimeXmlAdapter adapter = new DateTimeXmlAdapter();
		DateTimePersonalized marshalled = adapter.marshal(expected);
		DateTime actual = adapter.unmarshal(marshalled);
		assertEquals(expected, actual);
	}

	@Test
	public void testJaxb() {
		JAXBTestUtil<TestDate> util = JAXBTestUtil.create(new TestDate()).build();
		assertEquals(util.getXml(), util.getExpected().timestamp, util.getActual().timestamp);
		assertNull(util.getXml(), util.getActual().nullTimestamp);
	}

	@Test
	public void testInterval() {
		TestInterval expected = new TestInterval();
		JAXBTestUtil<TestInterval> util = JAXBTestUtil.create(expected).build();
		assertEquals(util.getXml(), expected.interval, util.getActual().interval);
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	static public class TestDate {
		@XmlJavaTypeAdapter(DateTimeXmlAdapter.class)
		DateTime timestamp = new DateTime();
		DateTime nullTimestamp;
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	static public class TestInterval {
		@XmlJavaTypeAdapter(IntervalXmlAdapter.class)
		Interval interval = JodaTestUtil.interval();

	}
}
