/**
 * 
 */
package com.aawhere.app.account;

import static com.aawhere.app.account.AccountUtil.*;
import static com.aawhere.person.PersonTestUtil.*;
import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.aawhere.app.KnownApplication;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class AccountUtilUnitTest {

	/**
	 * the alias matching {@link #EXPECTED_ALIAS}, but contains email address that should not remain
	 * in our system.
	 */
	private static final String EMAIL_ALIAS = "aroller@gmail.com";
	/**
	 * The properly formatted alias
	 */
	private static final String EXPECTED_ALIAS = "aroller";
	/**
	 * The alias matching {@link #EXPECTED_ALIAS}, but has upper case.
	 */
	private static final String UPPER_CASE_ALIAS = "Aroller";

	@Test
	public void testCleanupEmailAddressAliases() {
		Set<String> aliases = AccountUtil.cleanupAliases(Sets.newHashSet(EMAIL_ALIAS));
		TestUtil.assertContains(EXPECTED_ALIAS, aliases);
	}

	@Test
	public void testCleanupUpperCaseAliases() {
		Set<String> aliases = AccountUtil.cleanupAliases(Sets.newHashSet(UPPER_CASE_ALIAS));
		TestUtil.assertContains(EXPECTED_ALIAS, aliases);
	}

	@Test
	public void testCleanupUpperCaseAlias() {
		assertEquals(EXPECTED_ALIAS, AccountUtil.cleanupAlias(UPPER_CASE_ALIAS));
	}

	@Test
	public void testCleanupEmailAlias() {
		assertEquals(EXPECTED_ALIAS, AccountUtil.cleanupAlias(EMAIL_ALIAS));
	}

	@Test
	public void testEqualsDeeply() throws MalformedURLException {
		String name1 = TestUtil.generateRandomAlphaNumeric();
		String name2 = TestUtil.generateRandomAlphaNumeric();
		String alias1 = TestUtil.generateRandomAlphaNumeric();
		String alias2 = TestUtil.generateRandomAlphaNumeric();
		URL photoUrl1 = new URL("http://example.com/1");
		URL photoUrl2 = new URL("http://example.com/2");
		Account account1 = AccountTestUtil.creator().setName(name1).photoUrl(photoUrl1).addAlias(alias1).build();
		Account account2 = AccountTestUtil.creator().setName(name2).photoUrl(photoUrl2).addAlias(alias2).build();
		try {
			merged(account1, account2);
			fail("merge shouldn't be possible with different ids");
		} catch (Exception e) {
			// expected..not equal ids shouldn't be merged
		}
		assertNull("exactly the same", merged(account1, account1));
		assertNull("exactly the same", merged(account2, account2));
		// name
		{
			Account nameDifferent = Account.clone(account1).setName(name2).build();
			Account nameNull = Account.clone(account1).setName(null).build();
			String context = "name";
			assertEqualsDeeply(context, account1, nameDifferent, nameNull);
		}
		// photoUrl
		{
			Account photUrlDifferent = Account.clone(account1).photoUrl(photoUrl2).build();
			Account photoUrlNull = Account.clone(account1).photoUrl(null).build();
			assertEqualsDeeply("photoUrl", account1, photUrlDifferent, photoUrlNull);
		}
		// alias1
		{
			Account aliasesDifferent = Account.clone(account1).setAliases(account2.aliases()).build();
			Account aliasesEmpty = Account.clone(account1).setAliases(new HashSet<String>()).build();
			assertEqualsDeeply("aliases", account1, aliasesDifferent, aliasesEmpty);

			Account aliasesExtra = Account.clone(account1).addAlias(alias2).build();
			assertNotNull("left aliases extra", merged(aliasesExtra, account1));
			assertNotNull("right aliases extra", merged(account1, aliasesExtra));

		}
	}

	public void assertEqualsDeeply(String property, Account target, Account propertyDifferent, Account propertyNull) {
		assertNotNull(property + " right different", merged(target, propertyDifferent));
		assertNotNull(property + " left different", merged(propertyDifferent, target));
		assertNull(property + "right null means no update", merged(target, propertyNull));
		assertNotNull(property + "left null means update", merged(propertyNull, target));
		assertNull(property + " both null", merged(propertyNull, propertyNull));
	}

	@Test
	public void testBetterNames() {
		String preferred = NAME_BETTER;
		String good = NAME_GOOD;
		String ok = NAME_OK;
		String bad = NAME_BAD;
		TestUtil.assertGreaterThan(scoreName(good), scoreName(preferred));
		TestUtil.assertGreaterThan(scoreName(ok), scoreName(preferred));
		TestUtil.assertGreaterThan(scoreName(bad), scoreName(preferred));
		TestUtil.assertGreaterThan(scoreName(ok), scoreName(good));
		TestUtil.assertGreaterThan(scoreName(bad), scoreName(good));
		TestUtil.assertGreaterThan(scoreName(bad), scoreName(ok));
		assertEquals(scoreName(preferred), scoreName(preferred));
		ArrayList<Account> accountsInOrder = Lists.newArrayList(AccountTestUtil.accountWithName(preferred),
																AccountTestUtil.accountWithName(good),
																AccountTestUtil.accountWithName(ok),
																AccountTestUtil.accountWithName(bad));
		List<Account> accountsShuffled = Lists.newArrayList(accountsInOrder);
		Collections.shuffle(accountsShuffled);
		List<Account> accountsWithBetterNameFirst = accountsWithBetterNameFirst(accountsShuffled);
		assertEquals(	StringUtils.join(AccountUtil.names(accountsWithBetterNameFirst), ","),
						accountsInOrder,
						accountsWithBetterNameFirst);

	}

	@Test
	public void testBetterEmails() {
		Account gi = AccountTestUtil.account(KnownApplication.GOOGLE_IDENTITY_TOOLKIT.key);
		Account gc = AccountTestUtil.account(KnownApplication.GARMIN_CONNECT.key);
		List<Account> accountsWithBetterEmailFirst = AccountUtil.accountsWithBetterEmailFirst(Lists
				.newArrayList(gc, gi));
		Iterator<Account> iterator = accountsWithBetterEmailFirst.iterator();
		String message = "managing accounts should be first gi=" + gi.email() + " gc=" + gc.email();
		assertEquals(message, gi, iterator.next());
		assertEquals(gc, iterator.next());
	}

	/**
	 * provides that some email is better than none.
	 * 
	 */
	@Test
	public void testBetterEmailsManagingIsNull() {
		Account gi = Account.clone(AccountTestUtil.account(KnownApplication.GOOGLE_IDENTITY_TOOLKIT.key)).email(null)
				.build();
		Account gc = AccountTestUtil.account(KnownApplication.GARMIN_CONNECT.key);
		List<Account> accountsWithBetterEmailFirst = AccountUtil.accountsWithBetterEmailFirst(Lists
				.newArrayList(gc, gi));
		TestUtil.assertContainsOnly(gc, accountsWithBetterEmailFirst);

	}
}
