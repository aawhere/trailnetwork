/**
 * 
 */
package com.aawhere.app.account;

import static org.junit.Assert.*;

import java.net.URL;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.app.account.Account.Builder;
import com.aawhere.persist.BaseEntityJaxbBaseUnitTest;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.MockFilterRepository;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author aroller
 * 
 */
public class AccountUnitTest
		extends BaseEntityJaxbBaseUnitTest<Account, Account.Builder, AccountId, AccountRepository> {

	private String remoteId;
	private ApplicationKey applicationKey;
	private String alias;
	private String name;
	private EmailAddress emailAddress;
	private URL photoUrl;

	/**
 * 
 */
	public AccountUnitTest() {
		super(NON_MUTABLE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		this.remoteId = TestUtil.generateRandomAlphaNumeric();
		this.applicationKey = KnownApplication.UNKNOWN.key;
		this.alias = AccountTestUtil.alias();
		this.name = TestUtil.generateRandomAlphaNumeric();
		this.emailAddress = AccountTestUtil.emailAddress();
		this.photoUrl = TestUtil.url();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertCreation(com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertCreation(Account sample) {
		super.assertCreation(sample);
		assertEquals(sample.getRemoteId(), this.remoteId);
		assertEquals(sample.getApplicationKey(), this.applicationKey);
		TestUtil.assertContains(this.alias, sample.getAliases());
		assertEquals(name, sample.name());
		assertEquals(emailAddress, sample.email());
		assertEquals(photoUrl, sample.photoUrl());

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#creationPopulation(com.aawhere.persist.BaseEntity
	 * .Builder)
	 */
	@Override
	protected void creationPopulation(Builder builder) {
		super.creationPopulation(builder);
		builder.setRemoteId(remoteId);
		builder.setApplicationKey(applicationKey);
		builder.addAlias(alias);
		builder.setName(name);
		builder.email(emailAddress);
		builder.photoUrl(photoUrl);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityBaseUnitTest#assertEntityEquals(com.aawhere.persist.BaseEntity,
	 * com.aawhere.persist.BaseEntity)
	 */
	@Override
	public void assertEntityEquals(Account expected, Account actual) {
		super.assertEntityEquals(expected, actual);
		assertEquals(expected.getRemoteId(), actual.getRemoteId());
		assertEquals(expected.getApplicationKey(), actual.getApplicationKey());
		// TODO:compare sets.

	}

	/**
	 * @return
	 */
	public static AccountUnitTest create(AccountId id) {
		AccountUnitTest test = create();
		Pair<ApplicationKey, String> split = AccountUtil.split(id);
		test.applicationKey = split.getKey();
		test.remoteId = split.getRight();
		return test;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.persist.BaseEntityJaxbBaseUnitTest#assertJaxb(com.aawhere.xml.bind.JAXBTestUtil)
	 */
	@Override
	protected void assertJaxb(JAXBTestUtil<Account> util) {
		util.assertContains(Account.FIELD.APPLICATION_REFERENCE);
		util.assertContains(remoteId);
		util.assertContains(this.applicationKey.toString());
	}

	public static AccountUnitTest create() {
		AccountUnitTest test = new AccountUnitTest();
		test.setUp();
		test.baseSetUp();
		return test;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntityJaxbBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return AccountTestUtil.adapters();
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected AccountRepository createRepository() {
		return new AccountMockRepository();
	}

	public static class AccountMockRepository
			extends MockFilterRepository<Accounts, Account, AccountId>
			implements AccountRepository {

		/*
		 * @see
		 * com.aawhere.app.account.AccountRepository#aliasesAdded(com.aawhere.app.account.AccountId,
		 * java.util.Set)
		 */
		@Override
		public Account aliasesAdded(AccountId id, Set<String> aliases) throws EntityNotFoundException {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * @see com.aawhere.app.account.AccountRepository#setName(com.aawhere.app.account.AccountId,
		 * java.lang.String)
		 */
		@Override
		public Account setName(AccountId id, String name) throws EntityNotFoundException {
			throw new RuntimeException("TODO:Implement this");
		}

	}
}
