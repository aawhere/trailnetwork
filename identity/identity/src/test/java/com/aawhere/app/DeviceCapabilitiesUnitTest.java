/**
 * 
 */
package com.aawhere.app;

import static com.aawhere.app.DeviceCapabilities.*;
import static com.aawhere.app.DeviceCapabilities.SignalQuality.*;
import static com.aawhere.app.DeviceCapabilitiesRepository.FIELD.*;
import static com.aawhere.app.DeviceTestUtil.*;
import static com.aawhere.test.TestUtil.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.aawhere.app.DeviceCapabilities.SignalQuality;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Lists;

/**
 * @author aroller
 * 
 */
public class DeviceCapabilitiesUnitTest {

	@Test
	public void testProperties() {
		SignalQuality expectedGps = GOOD;
		SignalQuality expectedEle = GREAT;
		SignalQuality expectedBaro = POOR;
		ApplicationKey key = new ApplicationKey(s());
		DeviceCapabilities capabilties = create().gps(expectedGps).baro(expectedBaro).ele(expectedEle).key(key).build();

		assertEquals(expectedBaro, capabilties.barometricAltimeterSignalQuality());
		assertEquals(expectedGps, capabilties.gpsSignalQuality());
		assertEquals(expectedEle, capabilties.elevationSignalQuality());
		assertEquals(key, capabilties.deviceKey());
	}

	@Test
	public void testRepository() {
		assertEquals(GREAT, REPOSITORY.capabilities(EDGE_810).gpsSignalQuality());
		assertEquals(EDGE_810, REPOSITORY.capabilities(EDGE_810).deviceKey().getValue());
		assertEquals(GOOD, REPOSITORY.capabilities(EDGE_705).gpsSignalQuality());
		assertEquals(NONE, REPOSITORY.capabilities(FORERUNNER_101).barometricAltimeterSignalQuality());
		assertEquals(GOOD, REPOSITORY.capabilities(FORERUNNER_910).barometricAltimeterSignalQuality());
	}

	/**
	 * TN-923 created the need for aliases. This ensures they are defined properly.
	 */
	@Test
	public void testAlias() {
		DeviceCapabilities byKey = capabilities(FENIX_2);
		DeviceCapabilities byAlias = capabilities(FENIX_2_ALIAS);
		assertSame("alias should be the same as original", byKey, byAlias);
	}

	@Test
	public void testOrderingBySignalQuality() {
		final DeviceCapabilities NOT_FOUND = capabilities("junk");
		ArrayList<DeviceCapabilities> expectedOrder = Lists.newArrayList(	capabilities(EDGE_810),
																			capabilities(FORERUNNER_910),
																			capabilities(FORERUNNER_101),
																			NOT_FOUND);
		ArrayList<DeviceCapabilities> shuffled = Lists.newArrayList(expectedOrder);
		Collections.shuffle(shuffled);
		List<DeviceCapabilities> sortedCopy = DeviceUtil.signalQualityBetterFirst(shuffled);
		assertEquals("incorrect order", expectedOrder, sortedCopy);
	}

	@Test
	public void testNullKey() {
		assertNull(REPOSITORY.capabilities((ApplicationKey) null));
	}

	/** Ensures gps-only is considered fair at best for elevation quality */
	@Test
	public void testOverOptimisticGpsOnly() {
		DeviceCapabilities capabilities = DeviceCapabilities.create().key(ApplicationTestUtil.key()).baro(NONE)
				.ele(GOOD).gps(GOOD).build();
		assertEquals(MAX_ELE_FOR_GPS_ONLY, capabilities.elevationSignalQuality());
	}

	/**
	 * opposite of {@link #testOverOptimisticGpsOnly()}
	 */
	@Test
	public void testBaroEleStaysStrongOnly() {
		DeviceCapabilities capabilities = DeviceCapabilities.create().key(ApplicationTestUtil.key()).baro(GOOD)
				.ele(GREAT).gps(GOOD).build();
		assertEquals(GREAT, capabilities.elevationSignalQuality());
	}

	@Test
	public void testKeyCleansing() {
		assertEquals(FORERUNNER_305, DeviceUtil.keyCleansed(FR305_WITH_SPACE).getValue());
	}

	@Test
	public void testKeyWithSpace() {

		DeviceCapabilities capabilities = REPOSITORY.capabilities(DeviceTestUtil.FR305_WITH_SPACE);
		assertEquals(FORERUNNER_305, capabilities.deviceKey().getValue());
		TestUtil.assertNotEquals(FR305_WITH_SPACE, capabilities.deviceKey());
	}

	@Test
	public void testKeyNotFound() {
		final ApplicationKey key = ApplicationTestUtil.key();
		DeviceCapabilities capabilities = REPOSITORY.capabilities(key);
		assertEquals(key, capabilities.deviceKey());
		assertEquals(SignalQuality.UNKNOWN, capabilities.gpsSignalQuality());
		assertEquals(	"function should behave the same as get",
						REPOSITORY.capabilitiesFunction().apply(key),
						capabilities);
	}
}
