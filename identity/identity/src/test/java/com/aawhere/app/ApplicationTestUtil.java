/**
 * 
 */
package com.aawhere.app;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.personalize.xml.XmlAdapterTestUtils;
import com.aawhere.test.TestUtil;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class ApplicationTestUtil {

	public static ApplicationStats applicationStats() {
		Integer count = TestUtil.generateRandomInt(1, 100);
		return ApplicationStats.create().application(application()).count(count)
				.total(TestUtil.generateRandomInt(count, count + 100)).build();
	}

	public static Supplier<Application> applicationSupplier() {

		return new Supplier<Application>() {
			@Override
			public Application get() {
				return application();
			}
		};
	}

	public static Function<Object, Application> applicationFunction() {
		return new Function<Object, Application>() {
			private final KnownApplication[] values = KnownApplication.values();
			private int count = 0;

			@Override
			public Application apply(Object input) {
				// ensures we don't grow beyond the length
				count++;
				int index = count % values.length;
				return values[index].asApplication();
			}
		};
	}

	public static Set<Application> applications() {
		Integer count = randomNumberOfApplications();
		return Sets.newHashSet(applications(count));
	}

	public static Iterable<Application> applications(Integer count) {
		List<String> seed = Collections.nCopies(count, "ignore");
		return Iterables.transform(seed, applicationFunction());
	}

	public static ApplicationStats applicationStats(Application application) {
		return ApplicationStats.mutate(applicationStats()).application(application).build();
	}

	public static SortedSet<ApplicationStats> applicationsStats() {
		return applicationsStats(randomNumberOfApplications());
	}

	/** Gives some number of applciation stats, each with random data. */
	public static SortedSet<ApplicationStats> applicationsStats(Integer size) {
		TreeSet<ApplicationStats> all = new TreeSet<>();
		for (int i = 0; i < size; i++) {
			all.add(applicationStats());
		}
		return all;
	}

	public static Application application() {
		KnownApplication[] values = KnownApplication.values();
		Integer index = TestUtil.generateRandomInt(0, values.length - 1);
		return values[index].asApplication();
	}

	/** 0 to the number of Known Applications */
	public static Integer randomNumberOfApplications() {
		KnownApplication[] values = KnownApplication.values();
		return TestUtil.generateRandomInt(1, values.length);
	}

	/**
	 * @return
	 */
	public static ApplicationKey key() {
		return new ApplicationKey(TestUtil.s());
	}

	public static Set<XmlAdapter<?, ?>> adapters() {
		Set<XmlAdapter<?, ?>> adapters = new HashSet<XmlAdapter<?, ?>>();
		adapters.addAll(XmlAdapterTestUtils.getAdapters());
		return adapters;
	}

}
