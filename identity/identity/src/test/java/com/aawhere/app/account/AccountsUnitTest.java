/**
 * 
 */
package com.aawhere.app.account;

import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.app.account.Accounts.Builder;
import com.aawhere.persist.BaseEntitiesBaseUnitTest;

/**
 * @author aroller
 * 
 */
public class AccountsUnitTest
		extends BaseEntitiesBaseUnitTest<Account, Accounts, Builder> {

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#createEntitySampleWithId()
	 */
	@Override
	public Account createEntitySampleWithId() {
		return AccountUnitTest.create().getEntitySample();

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseEntitiesBaseUnitTest#getAdapters()
	 */
	@Override
	public Set<XmlAdapter<?, ?>> getAdapters() {
		return AccountTestUtil.adapters();

	}

}
