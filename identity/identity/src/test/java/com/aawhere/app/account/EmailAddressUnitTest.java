package com.aawhere.app.account;

import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * 
 * @author aroller
 * 
 */
public class EmailAddressUnitTest {

	private static final String VALID_GMAIL_PLUS = "someone+custom@gmail.com";
	private static final String VALID = "aaron.roller@trailnetwork.com";

	@Test
	public void testBuilder() throws EmailAddressInvalidException {
		String value = VALID;
		assertEmail(value);
	}

	public void assertEmail(String value) throws EmailAddressInvalidException {
		EmailAddress emailAddress = EmailAddress.create().address(value).build();
		assertEquals(value, emailAddress.address());
	}

	@Test(expected = EmailAddressInvalidException.class)
	public void testInvalid() throws EmailAddressInvalidException {
		EmailAddress.create().address("junk").build();
	}

	/**
	 * Google allows adding a plus to provide a unique address commonly used for anti-spam tracking
	 * techniques. Many websites say this email is invalid.
	 * 
	 * @throws EmailAddressInvalidException
	 * 
	 */
	@Test
	public void testGoogleAntiSpamEmail() throws EmailAddressInvalidException {
		String expected = VALID_GMAIL_PLUS;
		assertEmail(expected);
	}

	@Test
	public void testSocialDoamin() throws EmailAddressInvalidException {
		String expected = "example@something.social";
		assertEmail(expected);
	}

	@Test
	public void testBlackFriday() throws EmailAddressInvalidException {
		assertEmail("someone@somewhere.blackfriday");
	}

	@Test(expected = EmailAddressInvalidException.class)
	public void testNotValidTld() throws EmailAddressInvalidException {
		assertEmail("someone@somewhere.notvalid");
	}

	@Test
	public void testEquals() throws EmailAddressInvalidException {
		assertEquals(EmailAddress.valueOf(VALID), EmailAddress.valueOf(VALID));
	}

	@Test
	public void testNotEquals() {
		TestUtil.assertNotEquals(VALID_GMAIL_PLUS, VALID);
	}

	@Test
	public void testJaxb() {
		EmailAddress email = AccountTestUtil.emailAddress();
		JAXBTestUtil<EmailAddress> util = JAXBTestUtil.create(email).build();
		util.assertContains(email.address());
	}
}
