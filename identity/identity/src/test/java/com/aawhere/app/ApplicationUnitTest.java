package com.aawhere.app;

import static org.junit.Assert.*;

import java.util.Set;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import com.aawhere.id.IdentifierCustomFormat;
import com.aawhere.test.TestUtil;
import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.text.format.custom.CustomFormatFactory;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @see Application
 * 
 * @author Brian Chapman
 * 
 */
public class ApplicationUnitTest {

	final private KnownApplication knownApplication = KnownApplication.TWO_PEAK_ACTIVITIES;

	public static ApplicationUnitTest getInstance() {
		ApplicationUnitTest result = new ApplicationUnitTest();
		return result;
	}

	@Test
	public void testBasics() {
		assertApplication(knownApplication.asApplication());
	}

	public Application getApplication() {
		return knownApplication.asApplication();
	}

	/**
	 * 
	 */
	public void assertApplication(Application application) {
		assertEquals(knownApplication.key, application.getKey());
		assertEquals(knownApplication.name, application.getName());
	}

	@Test
	public void testJaxb() throws JAXBException {
		JAXBTestUtil<Application> util = JAXBTestUtil.create(knownApplication.asApplication())
				.adapter(ApplicationTestUtil.adapters()).build();
		assertApplication(util.getActual());
	}

	@Test
	public void testAll() {
		Set<Application> all = KnownApplication.all();
		TestUtil.assertContains(KnownApplication.GARMIN_CONNECT.asApplication(), all);
		TestUtil.assertContains(KnownApplication.EVERY_TRAIL.asApplication(), all);
		// etc, etc, and so on...
	}

	@Test
	public void testUnknownApplicationException() {
		ApplicationKey key = new ApplicationKey("junk");
		UnknownApplicationException exception = new UnknownApplicationException(key);
		final String message = exception.getMessage();
		TestUtil.assertContains(message, key.getValue());
		TestUtil.assertContains(message, KnownApplication.GARMIN_CONNECT.key.getValue());
		TestUtil.assertDoesntContain(message, "com.");
	}

	@Test
	public void testKnownApplicationAlias() throws UnknownApplicationException {
		Application application = KnownApplication.byKey(new ApplicationKey(KnownApplication.TWO_PEAK_ALIAS_KEY));
		assertEquals(KnownApplication.TWO_PEAK_ACTIVITIES.asApplication(), application);
	}

	@Test
	public void testKnownApplicationByKey() throws UnknownApplicationException {
		assertEquals(knownApplication.asApplication(), KnownApplication.byKey(knownApplication.key));
	}

	@Test
	public void testUnKnownApplicationByKeyWithDefault() throws UnknownApplicationException {
		assertEquals(	KnownApplication.UNKNOWN.getDefault().asApplication(),
						KnownApplication.byKeyWithDefault(new ApplicationKey("bogus")));
	}

	@Test(expected = UnknownApplicationException.class)
	public void testUnKnownApplicationByKey() throws UnknownApplicationException {

		KnownApplication.byKey(new ApplicationKey("bogus"));
	}

	@Test
	public void testApplicationKeyCustomFormat() {
		CustomFormat<?> format = CustomFormatFactory.getInstance().getFormat(ApplicationKey.class);
		assertEquals(IdentifierCustomFormat.class, format.getClass());
	}

	/**
	 * Unit tests are only ever run in the development environment so this should always return true
	 * for Development and false for Live.
	 * 
	 */
	@Test
	public void assertDevelopmentSystem() {
		assertFalse("how are we in Live?", KnownInstance.TRAIL_NETWORK.isThisSystem());
	}

}
