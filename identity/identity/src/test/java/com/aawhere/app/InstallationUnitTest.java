/**
 * 
 */
package com.aawhere.app;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBException;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author roller
 * 
 */
public class InstallationUnitTest {

	private Instance instance;
	private InstanceKey instanceKey;
	private String version;
	private ApplicationUnitTest applicationTest;
	private String externalId;
	private DateTime timestamp;

	@Before
	public void setUp() {
		this.instanceKey = new InstanceKey(TestUtil.generateRandomString());
		this.applicationTest = ApplicationUnitTest.getInstance();
		this.version = TestUtil.generateRandomString();
		this.externalId = TestUtil.generateRandomId().toString();
		this.timestamp = DateTime.now();
		this.instance = new Instance.Builder().setApplication(applicationTest.getApplication()).setKey(instanceKey)
				.build();
	}

	public static InstallationUnitTest getInstance() {

		InstallationUnitTest test = new InstallationUnitTest();
		test.setUp();
		return test;
	}

	public static InstallationUnitTest getDeviceInstance() {
		InstallationUnitTest installationUnitTest = getInstance();
		installationUnitTest.instance = new Device.Builder()
				.setApplication(installationUnitTest.applicationTest.getApplication())
				.setKey(installationUnitTest.instanceKey).build();
		return installationUnitTest;
	}

	public Installation getInstallation() {
		return new Installation.Builder().setInstance(instance).setVersion(version).setExternalId(externalId)
				.setTimestamp(timestamp).build();
	}

	@Test
	public void testKnownInstallationThisSystem() {
		Installation thisSystem = KnownInstallation.thisSystem();
		assertNotNull(thisSystem.getInstance());
		assertNotNull(thisSystem.getVersion());
		assertNotNull(thisSystem.getTimestamp());
	}

	/**
	 * @param sample
	 */
	public void assertInstallation(Installation sample) {
		this.applicationTest.assertApplication(sample.getApplication());
		assertEquals(this.instance, sample.getInstance());
		assertEquals(this.externalId, sample.getExternalId());
		assertEquals(this.version, sample.getVersion());
		assertEquals(this.timestamp, sample.getTimestamp());
	}

	@Test
	public void testJaxb() throws JAXBException {
		JAXBTestUtil<Installation> util = JAXBTestUtil.create(getInstallation()).build();
		assertInstallation(util.getActual());
	}

	@Test
	public void testBasics() {
		assertInstallation(getInstallation());
	}

}
