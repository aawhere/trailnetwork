/**
 * 
 */
package com.aawhere.app;

import static com.aawhere.app.DeviceTestUtil.*;
import static com.aawhere.lang.number.NumberUtilsExtended.*;
import static org.junit.Assert.*;

import org.junit.Test;

import com.aawhere.measure.calc.CategoricalStatistic;
import com.aawhere.measure.calc.CategoricalStatsCounter;
import com.aawhere.test.TestUtil;

import com.google.common.collect.ImmutableList;

/**
 * Relies mostly on general testing of {@link CategoricalStatistic}. This does only the basics.
 * 
 * @author aroller
 * 
 */
public class DeviceStatsUnitTest {

	@Test
	public void testBasics() {
		final Device fair = device(FAIR_DEVICE);
		final Device good = device(GOOD_DEVICE);
		final Device great = device(GREAT_DEVICE);
		ImmutableList<Device> devices = ImmutableList.of(fair, fair, fair, good, good, great);
		CategoricalStatsCounter<DeviceStats, Device> counter = DeviceStats.counter(devices);
		Integer typesOfDevicesExpected = 3;
		TestUtil.assertSize(typesOfDevicesExpected, counter.stats());
		assertEquals(I(3), counter.stats(fair).count());
		assertEquals(I(2), counter.stats(good).count());
		assertEquals(I(1), counter.stats(great).count());
	}

	@Test
	public void testKeys() {
		// This was dupped from above and modified slightly. maybe there could be a common method
		final ApplicationKey fair = FAIR_DEVICE;
		final ApplicationKey good = GOOD_DEVICE;
		final ApplicationKey great = GREAT_DEVICE;
		ImmutableList<ApplicationKey> devices = ImmutableList.of(fair, fair, fair, good, good, great);
		CategoricalStatsCounter<ApplicationKeyStats, ApplicationKey> counter = ApplicationKeyStats.counter(devices);
		Integer typesOfDevicesExpected = 3;
		TestUtil.assertSize(typesOfDevicesExpected, counter.stats());
		assertEquals(I(3), counter.stats(fair).count());
		assertEquals(I(2), counter.stats(good).count());
		assertEquals(I(1), counter.stats(great).count());
	}
}
