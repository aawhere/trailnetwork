/**
 * 
 */
package com.aawhere.app.account;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.identity.IdentityManager;
import com.aawhere.identity.IdentityTestUtil;
import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ServiceTestUtilBase;
import com.aawhere.person.PersonAccountProvider;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.personalize.xml.XmlAdapterTestUtils;
import com.aawhere.test.TestUtil;
import com.google.common.collect.Range;

/**
 * @author aroller
 * 
 */
public class AccountTestUtil
		extends ServiceTestUtilBase<Accounts, Account, AccountId>
		implements AccountProvider {

	public static Account account() {
		return AccountUnitTest.create().getEntitySampleWithId();
	}

	public static Account.Builder creator() {
		return Account.mutate(account());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.account.AccountProvider#account(com.aawhere.app.account.AccountId)
	 */
	@Override
	public Account account(AccountId id) throws EntityNotFoundException {
		return accountWithId(id);
	}

	/**
	 * @param id
	 * @return
	 */
	public static Account accountWithId(AccountId id) {
		return AccountUnitTest.create(id).getEntitySampleWithId();
	}

	/**
	 * Provides an account for the given application.
	 * 
	 * @param applicationKey
	 * @return
	 */
	public static Account account(ApplicationKey applicationKey) {
		return accountWithId(new AccountId(applicationKey, TestUtil.generateRandomIdAsString()));
	}

	public static Set<XmlAdapter<?, ?>> adapters() {
		Set<XmlAdapter<?, ?>> adapters = new HashSet<XmlAdapter<?, ?>>();
		final AccountTestUtil provider = new AccountTestUtil();
		adapters.add(new AccountFromIdXmlAdapter(provider));
		adapters.add(new AccountIdsXmlAdapter(provider, IdentityTestUtil.admin()));
		PersonAccountProvider personAccountProvider = PersonTestUtil.personAccountProvider();
		adapters.addAll(XmlAdapterTestUtils.getAdapters());
		IdentityManager admin = IdentityTestUtil.admin();
		adapters.add(new Account.ApplicationReferenceXmlAdapter(admin, personAccountProvider, AccountTestUtil
				.profilePageProvider()));
		return adapters;
	}

	public static AccountTestUtil instance() {
		return new AccountTestUtil();
	}

	/**
	 * Random remote id coupled with the given application.
	 * 
	 * @param key
	 * @return
	 */
	public static AccountId accountId(ApplicationKey key) {
		return new AccountId(key, TestUtil.generateRandomAlphaNumeric());
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.account.AccountProvider#accounts(java.lang.Iterable)
	 */
	@Override
	public Accounts accounts(Iterable<AccountId> ids) {
		Accounts.Builder accounts = Accounts.create();
		for (AccountId accountId : ids) {
			try {
				accounts.add(account(accountId));
			} catch (EntityNotFoundException e) {
				throw ToRuntimeException.wrapAndThrow(e);
			}
		}
		return accounts.build();
	}

	/**
	 * @return
	 */
	public static String alias() {
		return AccountUtil.cleanupAlias(TestUtil.generateRandomAlphaNumeric());
	}

	/**
	 * {@link #account()} with the given name.
	 * 
	 * @param name
	 */
	public static Account accountWithName(String name) {
		return Account.mutate(account()).setName(name).build();
	}

	/**
	 * 
	 */
	public static String accountName() {
		return TestUtil.generateRandomAlphaNumeric();
	}

	public static AccountProvider.ProfilePage profilePageProvider() {
		return new ProfilePage() {

			@Override
			public String profilePageUrl(Account account) throws BaseException {
				return (account != null) ? "http://www.coolsite.com/" + account.id().getRemoteId() : null;
			}
		};
	}

	/**
	 * @return
	 * 
	 */
	public static AccountId accountId() {
		return accountId(KnownApplication.UNKNOWN.key);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.ServiceTestUtilBase#entity(com.aawhere.id.Identifier)
	 */
	@Override
	public Account entity(AccountId id) {
		try {
			return account(id);
		} catch (EntityNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 
	 */
	public static Set<AccountId> accountIds() {
		final Range<Integer> range = Range.closed(10, 20);
		return accountIds(range);
	}

	/**
	 * @param range
	 * @return
	 */
	private static Set<AccountId> accountIds(final Range<Integer> range) {
		HashSet<AccountId> ids = new HashSet<AccountId>();
		Integer i = TestUtil.i(range);
		while (ids.size() < i) {
			ids.add(accountId());
		}
		return ids;
	}

	/**
	 * Provides a unique email address with the same domain
	 * 
	 * @return
	 */
	public static EmailAddress emailAddress() {
		try {
			return EmailAddress.valueOf(TestUtil.generateRandomAlphaNumeric() + "@example.com");
		} catch (EmailAddressInvalidException e) {
			throw new RuntimeException(e);
		}
	}
}
