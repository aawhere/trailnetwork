/**
 *
 */
package com.aawhere.app;

import com.aawhere.util.rb.StringMessage;

/**
 * @author aroller
 * 
 */
public class DeviceTestUtil {

	public static ApplicationKey POOR_DEVICE = new ApplicationKey(DeviceCapabilitiesRepository.FIELD.FORERUNNER_101);
	public static ApplicationKey FAIR_DEVICE = new ApplicationKey(DeviceCapabilitiesRepository.FIELD.FORERUNNER_305);
	public static ApplicationKey GOOD_DEVICE = new ApplicationKey(DeviceCapabilitiesRepository.FIELD.EDGE_705);
	public static ApplicationKey GREAT_DEVICE = new ApplicationKey(DeviceCapabilitiesRepository.FIELD.EDGE_810);
	/** older device keys have a space. not sure why. */
	public static ApplicationKey FR305_WITH_SPACE = new ApplicationKey("forerunner 305");

	public static final DeviceCapabilitiesRepository REPOSITORY = new DeviceCapabilitiesRepository();

	/**
	 * Provides a device with the application key given and everything else is just made up.
	 * 
	 */
	public static Device device(ApplicationKey key) {
		if (key == null) {
			return null;
		}
		return Device.createDevice().setApplication(KnownApplication.UNKNOWN.asApplication())
				.setApplication(Application.create().setKey(key).setName(new StringMessage(key.toString())).build())
				.build();
	}

	public static DeviceCapabilities capabilities(String key) {
		return REPOSITORY.capabilities(key);
	}

}
