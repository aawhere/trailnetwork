/**
 *
 */
package com.aawhere.app;

import static org.junit.Assert.*;

import java.util.Collection;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import com.aawhere.field.FieldDictionary;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.annotation.FieldAnnotationTestUtil;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @see IdentityHistory
 * 
 * @author roller
 * 
 */
public class IdentityHistoryUnitTest {

	private int numberOfVisits = TestUtil.generateRandomInt(2, 10);
	/** used to verify the depth of at least one of these installations. */
	private InstallationUnitTest firstInstallation;

	public IdentityHistory getEntitySample() {
		return getEntitySample(InstallationUnitTest.getDeviceInstance().getInstallation());
	}

	public IdentityHistory getEntitySampleWithDevice(Device device) {
		return getEntitySample(Installation.create().setInstance(device).build());
	}

	private IdentityHistory getEntitySample(Installation deviceInstallation) {
		IdentityHistory.Builder builder = IdentityHistory.create();

		final int roomForDevice = (deviceInstallation == null) ? 0 : 1;
		for (int i = 0; i < numberOfVisits - roomForDevice; i++) {
			final InstallationUnitTest installationFactory = InstallationUnitTest.getInstance();
			final Installation sample = installationFactory.getInstallation();
			if (firstInstallation == null) {
				firstInstallation = installationFactory;
			}
			builder.add(sample);
		}
		if (deviceInstallation != null) {
			builder.add(deviceInstallation);
		}

		return builder.build();
	}

	/**
	 * @return the firstInstallation
	 */
	public InstallationUnitTest getFirstInstallation() {
		return this.firstInstallation;
	}

	/*
	 * /**
	 * @param sample
	 */
	public void assertHistory(IdentityHistory sample) {
		assertEquals(this.numberOfVisits, sample.getInstallations().size());
		// See http://code.google.com/p/objectify-appengine/issues/detail?id=127
		// this.firstInstallation.assertInstallation(sample.getInstallations().get(Index.FIRST.index));

	}

	@Test
	public void testGetDevice() {
		Device device = getEntitySample().getDevice();
		assertNotNull("device not found", device);
	}

	@Test
	public void testJaxb() throws JAXBException {
		JAXBTestUtil<IdentityHistory> util = JAXBTestUtil.create(getEntitySample()).build();
		util.assertContains(IdentityHistory.FIELD.DOMAIN);

	}

	@Test
	public void testGetByExternalId() {
		IdentityHistory history = getEntitySample();
		Installation expected = firstInstallation.getInstallation();
		final ApplicationKey applicationKey = expected.getApplication().getKey();
		Installation actual = history.getByExternalId(applicationKey, expected.getExternalId());
		firstInstallation.assertInstallation(actual);

		String badId = expected.getExternalId() + "-bad";
		assertNull(	"we shouldn't be able to find with an external id not in the collection: " + badId,
					history.getByExternalId(applicationKey, badId));
	}

	/**
	 * Finds all matching by Application. NOTE: Not a very good test since apparently all generated
	 * history has a single Application.
	 */
	@Test
	public void testGetByApplication() {
		IdentityHistory history = getEntitySample();
		Installation expected = firstInstallation.getInstallation();
		final ApplicationKey applicationKey = expected.getApplication().getKey();
		Collection<Installation> actual = history.getByApplication(applicationKey);
		assertEquals(actual.toString(), numberOfVisits, actual.size());
		Installation actualFirstInstallation = actual.iterator().next();
		firstInstallation.assertInstallation(actualFirstInstallation);

		actual = history.getByApplication(new ApplicationKey("junk"));
		assertEquals(actual.toString(), 0, actual.size());
	}

	/**
	 * This doesn't use the standard mutation because history isn't currently being independnently
	 * stored.
	 * 
	 */
	@Test
	public void testAddingToHistory() {
		IdentityHistory sample = getEntitySample();
		int originalSize = sample.getInstallations().size();
		Installation thisSystem = KnownInstallation.thisSystem();
		sample = new IdentityHistory.Builder(sample).add(thisSystem).build();
		assertEquals(originalSize + 1, sample.getInstallations().size());

	}

	/**
	 * @return
	 */
	public static IdentityHistoryUnitTest getInstance() {
		IdentityHistoryUnitTest instance = new IdentityHistoryUnitTest();
		return instance;
	}

	@Test
	public void testDictionary() {
		FieldAnnotationDictionaryFactory annoationFactory = FieldAnnotationTestUtil.getAnnoationFactory();
		FieldDictionary dictionary = annoationFactory.getDictionary(IdentityHistory.class);
		assertNotNull("mostly this is for the dictionary validation", dictionary);

	}

}
