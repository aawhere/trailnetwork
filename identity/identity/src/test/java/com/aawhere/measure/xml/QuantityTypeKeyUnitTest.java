/**
 * 
 */
package com.aawhere.measure.xml;

import static org.junit.Assert.*;

import javax.measure.quantity.ElectricCurrent;
import javax.measure.quantity.Length;
import javax.measure.quantity.Mass;
import javax.measure.quantity.Quantity;
import javax.measure.quantity.RadiationDoseAbsorbed;
import javax.measure.quantity.Temperature;

import org.junit.Test;

import com.aawhere.measure.Elevation;

/**
 * Tests {@link QuantityTypeKey}
 * 
 * @author Aaron Roller
 * 
 */
public class QuantityTypeKeyUnitTest {

	@Test
	public void testKey() {
		assertQuantityTypeKey("length", Length.class);
		assertQuantityTypeKey("mass", Mass.class);
		assertQuantityTypeKey("temperature", Temperature.class);
		assertQuantityTypeKey("electricCurrent", ElectricCurrent.class);
		assertQuantityTypeKey("radiationDoseAbsorbed", RadiationDoseAbsorbed.class);
		assertQuantityTypeKey("elevation", Elevation.class);
	}

	public static void assertQuantityTypeKey(String key, Class<? extends Quantity<?>> type) {
		// we have special construction rights.
		QuantityTypeKey factory = new QuantityTypeKey();
		Class<? extends Quantity<?>> actualType = factory.typeFor(key);
		Class<? extends Quantity<?>> actualTypeAgain = factory.typeFor(key);
		assertEquals(type, actualType);
		assertSame("cache isn't working", actualType, actualTypeAgain);

		// clear it out and try the reverse
		factory = new QuantityTypeKey();
		String actualKey = factory.keyFor(type);
		assertEquals(key, actualKey);
		assertSame("key cache isn't working", actualKey, factory.keyFor(type));

	}
}
