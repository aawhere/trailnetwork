/**
 *
 */
package com.aawhere.measure.xml;

import static org.junit.Assert.*;

import java.util.Locale;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.unit.MetricSystem;
import javax.measure.unit.Unit;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.measure.GeoCoordinate;
import com.aawhere.measure.Latitude;
import com.aawhere.measure.Longitude;
import com.aawhere.measure.MeasurementUtil;
import com.aawhere.person.pref.PrefMeasureUtil;
import com.aawhere.person.pref.PrefTestUtil;
import com.aawhere.personalize.QuantityPersonalized;
import com.aawhere.personalize.QuantityPersonalizedPersonalizer;
import com.aawhere.personalize.QuantityPersonalizer;
import com.aawhere.personalize.xml.LocaleXmlAdapter;
import com.aawhere.test.TestUtil;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * @author Aaron Roller
 * 
 */
public class QuantityXmlAdapterUnitTest {

	private static final String DISPLAY_ELEMENT_NAME = "display";
	private static final String FR_FR_LOCALE = "fr-FR";
	Configuration prefs;
	Locale locale;
	Length q;
	QuantityPersonalizer plizer;

	@Before
	public void setUp() {
		locale = Locale.FRANCE;
		prefs = PrefTestUtil.createSiPreferences(locale);
		q = MeasurementUtil.createLengthInMeters(12345.678);
		plizer = new QuantityPersonalizer.Builder().setPreferences(prefs).build();
	}

	@Test
	public void testDisplayManually() throws Exception {
		QuantityXmlAdapter adapter = new QuantityXmlAdapter(plizer);

		QuantityPersonalized marshalResult = adapter.marshal(q);
		assertNotNull(marshalResult.getPersonalized());

		Unit<Length> unit = PrefMeasureUtil.getUnitFor(Length.class, prefs);
		final String unitSymbol = MeasurementUtil.getUnitSymbol(unit);
		assertEquals(unitSymbol, marshalResult.getPersonalized().getAbbreviatedUnit());

		TestUtil.assertDoubleEquals(q.doubleValue(unit), marshalResult.getPersonalized().getValue());
		assertNotNull(marshalResult.getPersonalized().getFormatted());
		assertEquals(locale, marshalResult.getPersonalized().getLang());

		final String formatExpected = "12,3";
		assertEquals(formatExpected + " " + unitSymbol, marshalResult.getPersonalized().getFormatted());

		Quantity<?> unmarshalResult = adapter.unmarshal(marshalResult);
		assertEquals(q, unmarshalResult);
	}

	// The display object doesn't get marshalled into XML.
	@Test
	public void testJaxbRoundTrip() {
		Junk junk = new Junk();

		junk.theLongestYard = MeasurementUtil.createLengthInMeters(TestUtil.generateRandomDouble(0, 100000));
		QuantityPersonalizedPersonalizer plizer = new QuantityPersonalizedPersonalizer.Builder().setPreferences(prefs)
				.build();
		QuantityXmlAdapter adapter = new QuantityXmlAdapter(plizer);
		JAXBTestUtil<Junk> util = new JAXBTestUtil.Builder<Junk>().element(junk).adapter(adapter).build();
		util.assertContains("junk");
		util.assertContains("theLongestYard");
		assertEquals(	junk.theLongestYard.doubleValue(MetricSystem.METRE),
						util.getActual().theLongestYard.doubleValue(MetricSystem.METRE),
						0.001);
		TestUtil.assertDoesntContain(util.getXml(), DISPLAY_ELEMENT_NAME);
		TestUtil.assertDoubleEquals(junk.theLongestYard.doubleValue(MetricSystem.METRE),
									util.getActual().theLongestYard.doubleValue(MetricSystem.METRE));
	}

	@Test
	@Ignore("radians instead of degress")
	public void testJaxbGeo() {
		Junk junk = new Junk();
		Double xValue = -173.3;
		Double yValue = 24.6542;
		junk.x = MeasurementUtil.createLongitude(xValue);
		JAXBTestUtil<Junk> jaxbUtil = JAXBTestUtil.create(junk).build();
		jaxbUtil.assertContains(xValue.toString());
		jaxbUtil.assertContains(yValue.toString());
	}

	// The display object doesn't get marshalled into XML.
	@Test
	public void testJaxbPersonalizedRoundTrip() {
		Junk junk = new Junk();

		junk.theLongestYard = MeasurementUtil.createLengthInMeters(TestUtil.generateRandomDouble(0, 100000));
		QuantityXmlAdapter adapter = new QuantityXmlAdapter(plizer);
		JAXBTestUtil<Junk> util = new JAXBTestUtil.Builder<Junk>().element(junk)
				.adapter(adapter, new LocaleXmlAdapter()).build();
		assertEquals(	junk.theLongestYard.doubleValue(MetricSystem.METRE),
						util.getActual().theLongestYard.doubleValue(MetricSystem.METRE),
						0.001);
		util.assertContains(DISPLAY_ELEMENT_NAME);
		util.assertContains(FR_FR_LOCALE);
	}

	@Test
	public void testCount() {
		Junk junk = new Junk();
		junk.count = 123456;
		CountFromIntegerXmlAdapter countAdapter = new CountFromIntegerXmlAdapter(plizer);
		JAXBTestUtil<Junk> util = new JAXBTestUtil.Builder<Junk>().element(junk).adapter(countAdapter).build();
		assertEquals(junk.count, util.getActual().count);
		// it's french.
		util.assertContains("123 456");

	}

	@XmlRootElement
	public static class Junk {
		@XmlElement
		@XmlJavaTypeAdapter(QuantityXmlAdapter.class)
		private Length theLongestYard;

		@SuppressWarnings("unused")
		private Latitude y;

		@SuppressWarnings("unused")
		@XmlElement
		private Longitude x;

		@SuppressWarnings("unused")
		private GeoCoordinate xy;

		@XmlJavaTypeAdapter(CountFromIntegerXmlAdapter.class)
		private Integer count;
	}
}
