/**
 *
 */
package com.aawhere.measure.xml;

import static org.junit.Assert.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.measure.BoundingBox;
import com.aawhere.measure.BoundingBoxTestUtils;
import com.aawhere.measure.BoundingBoxXmlAdapter;
import com.aawhere.measure.BoundingBoxXmlAdapter.BoundingBoxXml;
import com.aawhere.measure.geocell.GeoCellBoundingBox;
import com.aawhere.xml.bind.JAXBTestUtil;

/**
 * 
 * @author Brian Chapman
 * 
 */
public class BoundingBoxXmlAdapterUnitTest {

	BoundingBox box;

	@Before
	public void setUp() {
		this.box = new BoundingBox.Builder().setEast(1d).setNorth(2d).setWest(0d).setSouth(-1d).build();
	}

	@Test
	public void testMarshaller() throws Exception {
		BoundingBoxXmlAdapter marshaller = new BoundingBoxXmlAdapter();
		BoundingBoxXml marshalled = marshaller.marshal(box);
		BoundingBox unmarshalled = marshaller.unmarshal(marshalled);
		assertEquals(box, unmarshalled);
	}

	@Test
	public void testJaxb() {
		Container container = new Container(box);
		JAXBTestUtil<Container> jaxBTestUtil = new JAXBTestUtil.Builder<Container>().element(container).build();
		jaxBTestUtil.assertContains(box.getBounds());
		// jaxBTestUtil.assertContains("∘");
		Container actual = jaxBTestUtil.getActual();
		assertNotNull("actual", actual);
		assertEquals(box, actual.box);
	}

	@XmlRootElement
	private static class Container {
		@XmlElement
		BoundingBox box;

		@SuppressWarnings("unused")
		Container() {
		}

		Container(BoundingBox box) {
			this.box = box;
		}
	}

	/**
	 * This demonstrates that JAXB will work if no XML adapter is provided. The value string and
	 * quantities will be transported.
	 * 
	 * 
	 * 
	 */
	@Test
	@Ignore("adapters don't work for root elements. Currently we are Manually marshalling these element")
	public void testJaxbWithoutAdapter() {
		BoundingBox expected = BoundingBoxTestUtils.createRandomBoundingBox();
		JAXBTestUtil<BoundingBox> util = JAXBTestUtil.create(expected).build();
		String value = expected.getBounds();
		util.assertContains(value);
		BoundingBox actual = util.getActual();
		assertNotNull(actual.getMax());
		assertNotNull(actual.getMin());
		// the values are truncated in the xml so equality does not happen
		// assertEquals(expected.getNorthEast(),actual.getNorthEast());
		// assertEquals(value, actual.getAsString());
	}

	@Test
	@Ignore("adapters don't work for root elements. Currently we are Manually marshalling these element")
	public void testGeoCellBoundingBoxJaxb() {
		GeoCellBoundingBox b = new GeoCellBoundingBox.Builder()
				.setOther(BoundingBoxTestUtils.createRandomBoundingBox()).build();
		JAXBTestUtil<GeoCellBoundingBox> util = JAXBTestUtil.create(b).build();
		GeoCellBoundingBox actual = util.getActual();
		util.assertContains(b.getGeoCells().getAll().iterator().next().getCellAsString());
		assertNotNull(actual.getBounds());
		assertEquals(b.getGeoCells(), actual.getGeoCells());

	}
}
