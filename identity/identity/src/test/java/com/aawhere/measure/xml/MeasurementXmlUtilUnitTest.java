/**
 * 
 */
package com.aawhere.measure.xml;

import static org.junit.Assert.assertEquals;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;

import org.junit.Test;

import com.aawhere.measure.MeasurementUtil;
import com.aawhere.test.TestUtil;

/**
 * @author Aaron Roller
 * 
 */
public class MeasurementXmlUtilUnitTest {

	/**
	 * @see MeasurementUtil#typeFor(Quantity)
	 * 
	 */
	@Test
	public void testTypeForQuantity() {
		Length seed = MeasurementUtil.createLengthInMeters(TestUtil.generateRandomDouble());
		String actual = MeasurementXmlUtil.typeFor(seed);
		assertEquals("length", actual);
	}

	/**
	 * @see MeasurementUtil#typeFor(Class)
	 * 
	 */
	@Test
	public void testTypeForClass() {
		String actual = MeasurementXmlUtil.typeFor(Length.class);
		assertEquals("length", actual);
	}

}
