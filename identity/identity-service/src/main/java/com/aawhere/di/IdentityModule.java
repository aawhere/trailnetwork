/**
 *
 */
package com.aawhere.di;

import com.aawhere.app.ApplicationXmlAdapter;
import com.aawhere.app.account.AccountApplicationXmlAdapter;
import com.aawhere.app.account.AccountObjectifyRepository;
import com.aawhere.app.account.AccountProvider;
import com.aawhere.app.account.AccountRepository;
import com.aawhere.app.account.AccountService;
import com.aawhere.person.PersonAccountProvider;
import com.aawhere.person.PersonObjectifyRepository;
import com.aawhere.person.PersonProvider;
import com.aawhere.person.PersonRepository;
import com.aawhere.person.PersonService;
import com.aawhere.person.group.SocialGroupMembershipObjectifyRepository;
import com.aawhere.person.group.SocialGroupMembershipRepository;
import com.aawhere.person.group.SocialGroupObjectifyRepository;
import com.aawhere.person.group.SocialGroupProvider;
import com.aawhere.person.group.SocialGroupRepository;
import com.aawhere.person.group.SocialGroupService;
import com.aawhere.view.ViewCountObjectifyRepository;
import com.aawhere.view.ViewCountRepository;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

/**
 * @author brian
 * 
 */
public class IdentityModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		bind(PersonRepository.class).to(PersonObjectifyRepository.class);
		bind(AccountRepository.class).to(AccountObjectifyRepository.class);
		bind(ViewCountRepository.class).to(ViewCountObjectifyRepository.class);
		bind(AccountProvider.class).to(AccountService.class);
		// account profile page xml provider is in import module
		bind(PersonProvider.class).to(PersonService.class);

		bind(SocialGroupRepository.class).to(SocialGroupObjectifyRepository.class);
		bind(SocialGroupMembershipRepository.class).to(SocialGroupMembershipObjectifyRepository.class);
		bind(SocialGroupProvider.class).to(SocialGroupService.class);
	}

	@Provides
	public ApplicationXmlAdapter applicationXmlAdapter() {
		// anyone requesting this will wish for it to show the details.
		return new ApplicationXmlAdapter(true);
	}

	@Provides
	public AccountApplicationXmlAdapter accountApplicationXmlAdapter() {
		return new AccountApplicationXmlAdapter(true);
	}

	@Provides
	public PersonAccountProvider personAccountProvider(PersonService personService) {
		return personService.personAccountProvider();
	}
}