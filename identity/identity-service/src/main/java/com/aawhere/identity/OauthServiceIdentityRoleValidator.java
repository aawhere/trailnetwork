package com.aawhere.identity;

import com.google.appengine.api.oauth.OAuthRequestException;
import com.google.appengine.api.oauth.OAuthService;

/**
 * An {@link OAuthService} implementation of the {@link IdentityRoleValidator} similar to
 * {@link HttpServletRequestIdentityRoleValidator}. Currently not used, this is provided to
 * demonstrate future use and ensure we are using security that can handle multiple providers.
 * 
 * @author aroller
 * 
 */
public class OauthServiceIdentityRoleValidator
		implements IdentityRoleValidator {

	private OAuthService oauthService;

	public OauthServiceIdentityRoleValidator(OAuthService oauthService) {
		this.oauthService = oauthService;
	}

	@Override
	public Boolean isUserInRole(IdentityRole role) {
		switch (role) {
			case admin:
				try {
					return oauthService.isUserAdmin();
				} catch (OAuthRequestException e) {
					throw new RuntimeException(e);
				}
			default:
				return false;
		}
	}

}
