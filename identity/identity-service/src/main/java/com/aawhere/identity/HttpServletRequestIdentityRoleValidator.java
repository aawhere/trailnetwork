package com.aawhere.identity;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import com.aawhere.lang.Assertion;

public class HttpServletRequestIdentityRoleValidator
		implements IdentityRoleValidator {

	@Nonnull
	private HttpServletRequest request;

	public HttpServletRequestIdentityRoleValidator(@Nonnull HttpServletRequest request) {
		this.request = request;
		Assertion.exceptions().notNull("request", this.request);

	}

	@Override
	public Boolean isUserInRole(IdentityRole role) {
		return request.isUserInRole(role.toString());
	}

}
