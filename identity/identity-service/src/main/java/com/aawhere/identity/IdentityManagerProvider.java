package com.aawhere.identity;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.configuration.Configuration;

import com.aawhere.app.account.AccountId;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.Person;
import com.aawhere.person.PersonService;
import com.aawhere.person.pref.PrefUtil;
import com.aawhere.person.pref.UnitOfMeasure;
import com.aawhere.personalize.PersonalizeUtil;

/**
 * Provides the current persons identity.
 * 
 * @author Brian Chapman
 * 
 */
public class IdentityManagerProvider
		implements com.google.inject.Provider<IdentityManager> {

	public static final String APPID_HEADER_NAME = "X-AppEngine-Inbound-AppId";
	private static final Logger LOGGER = LoggerFactory.getLogger(IdentityManagerProvider.class);
	public final static String ACCOUNT_ID_HEADER_NAME = "tn-accountId";
	public static final String UNIT_OF_MEASURE_HEADER_NAME = "Accept-UnitOfMeasure";

	private HttpServletRequest request;
	private PersonService personService;
	private IdentityFactory identityFactory;
	private AccessScopeValidatorFactory accessScopeValidatorFactory;

	@Inject
	public IdentityManagerProvider(HttpServletRequest request, PersonService personService,
			IdentityFactory identityFactory, AccessScopeValidatorFactory accessScopeValidatorFactory) {
		this.request = request;
		this.personService = personService;
		this.identityFactory = identityFactory;
		this.accessScopeValidatorFactory = accessScopeValidatorFactory;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.Provider#get()
	 */
	@Override
	public IdentityManager get() {

		Locale locale = request.getLocale();
		String accountIdString = (String) request.getAttribute(ACCOUNT_ID_HEADER_NAME);
		if (LOGGER.isLoggable(Level.FINE)) {
			LOGGER.fine(ACCOUNT_ID_HEADER_NAME + ": " + accountIdString);
			LOGGER.fine(APPID_HEADER_NAME + ": " + request.getHeader(APPID_HEADER_NAME));
			LOGGER.fine("personService: " + personService.toString());
		}
		IdentityManager.Builder builder = IdentityManager.create();

		if (locale == null) {
			locale = PersonalizeUtil.DEFAULT;
		}
		builder.locale(locale);

		Person person = null;
		if (accountIdString != null) {
			AccountId accountId = new AccountId(accountIdString);
			try {
				person = personService.person(accountId);
				builder.person(person);
			} catch (EntityNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
		CompositeIdentityRoleValidator.Builder identityRoleValidator = CompositeIdentityRoleValidator.create();
		identityRoleValidator.addValidator(new HttpServletRequestIdentityRoleValidator(request));
		if (person != null) {
			identityRoleValidator.addValidator(PersonIdentityRoleValidator.create().person(person).build());
		}

		builder.identityRoleValidator(identityRoleValidator.build());
		builder.identityFactory(identityFactory);
		builder.accessScopeValidatorFactory(accessScopeValidatorFactory);
		builder.preferences(createPreferences(person, locale));

		return builder.build();
	}

	private Configuration createPreferences(Person person, Locale locale) {
		String uomHeaderValue = request.getHeader(UNIT_OF_MEASURE_HEADER_NAME);
		if (LOGGER.isLoggable(Level.FINE)) {
			LOGGER.fine(UNIT_OF_MEASURE_HEADER_NAME + ": " + uomHeaderValue);
		}
		UnitOfMeasure uom = null;
		if (uomHeaderValue != null) {
			uom = UnitOfMeasure.valueOf(uomHeaderValue);
		}
		return PrefUtil.createPrefBuilder().locale(locale).person(person).unitOfMeasure(uom).build();
	}
}