/**
 * 
 */
package com.aawhere.view;

import com.aawhere.persist.objectify.ObjectifyRepository;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author aroller
 * 
 */
@Singleton
public class ViewCountObjectifyRepository
		extends ObjectifyRepository<ViewCount, ViewCountId>
		implements ViewCountRepository {

	static {
		ObjectifyService.register(ViewCount.class);
	}

	private final ViewCountRepositoryDelegate delegate;

	/**
	 * 
	 */
	public ViewCountObjectifyRepository() {
		this(SYNCHRONIZATION_DEFAULT);
	}

	/**
	 * @param synchronous
	 */
	public ViewCountObjectifyRepository(com.aawhere.persist.objectify.ObjectifyRepository.Synchronization synchronous) {
		super(synchronous);
		this.delegate = new ViewCountRepositoryDelegate(this);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.view.ViewCountRepository#increment(com.aawhere.view.ViewCountId)
	 */
	@Override
	public Integer increment(ViewCountId id) {
		// TODO:investigate if going native would be better for this type of frequent, low impact
		// modification (last updated would not be modified then).
		return delegate.increment(id);
	}

}
