/**
 *
 */
package com.aawhere.app;

import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.StringMessage;

import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

/**
 * Provides the small and simple {@link ApplicationKey} to be stored in place of the
 * {@link Application} which can be provided from {@link KnownApplication}.
 * 
 * @author aroller
 * 
 */
public class ApplicationTranslatorFactory
		extends ValueTranslatorFactory<Application, String> {

	/**
	 *
	 */
	public ApplicationTranslatorFactory() {
		super(Application.class);
	}

	@Override
	protected ValueTranslator<Application, String> createValueTranslator(TypeKey<Application> tk, CreateContext ctx,
			Path path) {
		return new ValueTranslator<Application, String>(String.class) {

			@Override
			protected String saveValue(Application value, boolean index, SaveContext ctx, Path path)
					throws SkipException {
				return value.getKey().getValue();
			}

			@Override
			protected Application loadValue(String value, LoadContext ctx, Path path) throws SkipException {
				ApplicationKey key = new ApplicationKey(value);
				if (KnownApplication.exists(key)) {
					return KnownApplication.byKey(key);
				} else {
					Message name = new StringMessage(key.toString());
					return Application.create().setKey(key).setName(name).build();
				}
			}
		};
	}

}
