/**
 * 
 */
package com.aawhere.app.account;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.NotPredicate;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.account.Account.Builder;
import com.aawhere.field.FieldDictionary;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldKey;
import com.aawhere.lang.string.predicate.StringNotEmptyPredicate;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.persist.RepositoryUtil;
import com.aawhere.queue.BatchQueue;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.Task;
import com.aawhere.ws.rs.SystemWebApiUri;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class AccountService
		extends EntityService<Accounts, Account, AccountId>
		implements AccountProvider {

	private AccountObjectifyRepository repository;
	private FieldDictionaryFactory fieldDictionaryFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	private QueueFactory queueFactory;

	@Inject
	public AccountService(AccountObjectifyRepository repository, QueueFactory queueFactory,
			FieldDictionaryFactory fieldDictionaryFactory) {
		super(repository);
		this.repository = repository;
		this.fieldDictionaryFactory = fieldDictionaryFactory;
		this.queueFactory = queueFactory;

	}

	/**
	 * Creates the account if it doesn't already exists. It will update an existing account if
	 * anything has changed such as an additional alias has been found. Aliases are combined keeping
	 * all previously existing and adding any new that are found. Other incoming properties are
	 * persisted if the {@link Account#hasName()}, etc.
	 * 
	 * @param account
	 * @return
	 */
	public Account accountDetailsUpdated(Account account) {
		Account result;
		try {
			result = this.repository.load(account.getId());
			Pair<Account, ImmutableMap<FieldKey, Object>> merged = AccountUtil.merged(result, account);
			if (merged != null) {
				result = repository.store(merged.getKey());
			}
		} catch (EntityNotFoundException e) {
			result = this.repository.store(account);
		}
		return result;
	}

	/**
	 * Finds all accounts that have the alias for a given application. There is no guarantee of
	 * uniqueness in our system and often not in the remote system.
	 * 
	 * @param applicationKey
	 * @param alias
	 * @return
	 */
	public Accounts accounts(ApplicationKey applicationKey, String alias) {
		return accounts(AccountFilterBuilder.create().application(applicationKey).alias(alias).build());
	}

	@Override
	public Account account(AccountId accountId) throws EntityNotFoundException {

		Account loaded;
		try {
			loaded = this.repository.load(accountId);
			// update once a month
			if (loaded.dateUpdated().plusMonths(1).isBefore(DateTime.now())) {
				// either the account will be updated or it will be touched
				// see #updateNotPossible
				queueForUpdate(accountId);
			}
		} catch (EntityNotFoundException notFoundException) {
			Accounts accounts = accounts(accountId.getApplicationKey(), accountId.getRemoteId());
			loaded = RepositoryUtil.finderResult(accountId, accounts);
		}
		Set<String> aliases = loaded.getAliases();

		// TN-553 add check for null alias to clean up corrupted entities.
		// TODO:apply this upon persisting
		// TN-525 clean up email addresses in aliases that have been stored.
		if (AccountUtil.containsEmail(aliases)
				|| CollectionUtils.countMatches(aliases,
												NotPredicate.getInstance(StringNotEmptyPredicate.getInstance())) > 0) {
			final Set<String> cleanedupAliases = AccountUtil.cleanupAliases(aliases);
			loaded = repository.aliasesAdded(accountId, cleanedupAliases);
			logger.warning("cleaning up aliases with email pattern from " + aliases + " to " + cleanedupAliases);
		}
		return loaded;
	}

	/** queues up the account for update. It protects against repeats. */
	void queueForUpdate(AccountId id) {
		queue(id).add(AccountWebApiUri.accountUpdatedTask(id));
	}

	/**
	 * @return
	 */
	private Queue queue(AccountId accountId) {
		return AccountWebApiUri.getCrawlQueue(queueFactory, accountId.getApplicationKey());
	}

	/**
	 * Given the filter this will return {@link Accounts} matching the results.
	 * 
	 * @param filter
	 * @return
	 */
	public Accounts accounts(Filter filter) {
		return this.repository.filter(filter);
	}

	@Override
	public Accounts accounts(Iterable<AccountId> accounts) {
		return this.repository.entities(accounts);
	}

	/**
	 * Updates the accounts resulting from the filter given.
	 * 
	 * @param filter
	 * @return
	 */
	public Integer accountsUpdated(Filter filter) {

		if (filter.getOptions().contains(SystemWebApiUri.TOUCH)) {
			return this.repository.touched(filter);
		} else {
			Iterable<AccountId> idPaginator = repository.idPaginator(filter);
			// each application gets it's own queue.
			HashMap<ApplicationKey, BatchQueue> queues = Maps.newHashMap();

			for (AccountId accountId : idPaginator) {
				ApplicationKey applicationKey = accountId.getApplicationKey();
				BatchQueue queue = queues.get(applicationKey);
				if (queue == null) {
					queue = BatchQueue.create().setQueue(queue(accountId)).build();
					queues.put(applicationKey, queue);
				}
				Task task = AccountWebApiUri.accountUpdatedTask(accountId);
				queue.add(task);
			}
			int count = 0;
			for (BatchQueue batchQueue : queues.values()) {
				count += batchQueue.finish();
			}
			return count;
		}

	}

	/**
	 * @return
	 * 
	 */
	public FieldDictionary getDictionary() {
		return this.fieldDictionaryFactory.getDictionary(Account.FIELD.DOMAIN);
	}

	/**
	 * Allows a client to update a username, assumingly a service connected to the remote service.
	 * 
	 * @param accountId
	 * @param alias
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Account addAlias(AccountId accountId, String alias) throws EntityNotFoundException {
		// technically a job of the repository so move it there.
		Account load = this.repository.load(accountId);
		HashSet<String> aliases = Sets.newHashSet(load.getAliases());
		aliases.add(alias);
		return this.repository.aliasesAdded(accountId, aliases);
	}

	/**
	 * @param accountId
	 * @param name
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Account accountNameUpdated(AccountId accountId, String name) throws EntityNotFoundException {
		return this.repository.setName(accountId, name);
	}

	/**
	 * Indicates the service that updates the account is not able to do so. In this case we mark it
	 * so it has been updated.
	 * 
	 * @return
	 * @throws EntityNotFoundException
	 * 
	 */
	public Account updateNotPossible(AccountId id) throws EntityNotFoundException {
		return this.repository.touch(id);
	}

	/**
	 * Provides an atomic way of updated the name and alias for a given account. This will always
	 * write even if the values are the same.
	 * 
	 * @param accountId
	 * @param name
	 *            the display name
	 * @param username
	 * @return the updated account
	 */
	public Account accountUpdated(AccountId accountId, String name, String username) {
		Builder builder;
		try {
			builder = Account.mutate(this.repository.load(accountId));
		} catch (EntityNotFoundException e) {
			builder = Account.create().setApplicationKey(accountId.getApplicationKey())
					.setRemoteId(accountId.getRemoteId());
		}
		return this.repository.store(builder.addAlias(username).setName(name).build());
	}

}
