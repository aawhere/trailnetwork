package com.aawhere.app.account;

import com.aawhere.log.LoggerFactory;

import com.google.appengine.api.datastore.Email;
import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.CreateContext;
import com.googlecode.objectify.impl.translate.LoadContext;
import com.googlecode.objectify.impl.translate.SaveContext;
import com.googlecode.objectify.impl.translate.SkipException;
import com.googlecode.objectify.impl.translate.TypeKey;
import com.googlecode.objectify.impl.translate.ValueTranslator;
import com.googlecode.objectify.impl.translate.ValueTranslatorFactory;

public class EmaiAddressTranslatorFactory
		extends ValueTranslatorFactory<EmailAddress, Email> {

	public EmaiAddressTranslatorFactory() {
		super(EmailAddress.class);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.googlecode.objectify.impl.translate.ValueTranslatorFactory#createValueTranslator(com.
	 * googlecode.objectify.impl.translate.TypeKey,
	 * com.googlecode.objectify.impl.translate.CreateContext, com.googlecode.objectify.impl.Path)
	 */
	@Override
	protected ValueTranslator<EmailAddress, Email> createValueTranslator(TypeKey<EmailAddress> arg0,
			CreateContext arg1, Path arg2) {

		return new ValueTranslator<EmailAddress, Email>(Email.class) {

			@Override
			protected Email saveValue(EmailAddress value, boolean index, SaveContext ctx, Path path)
					throws SkipException {
				return new Email(value.address());
			}

			@Override
			protected EmailAddress loadValue(Email value, LoadContext ctx, Path path) throws SkipException {
				try {
					return EmailAddress.valueOf(value.getEmail());
				} catch (EmailAddressInvalidException e) {
					LoggerFactory.getLogger(getClass()).severe(value.getEmail() + " is invalid and already persisted");
					// I think skip does exactly that. logging this should provide enough
					// attention...no need to halt loading
					throw new SkipException();
				}
			}
		};
	}

}
