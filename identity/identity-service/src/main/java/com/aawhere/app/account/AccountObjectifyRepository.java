/**
 *
 */
package com.aawhere.app.account;

import java.util.Set;

import com.aawhere.app.IdentityTranslators;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author aroller
 * 
 */
@Singleton
public class AccountObjectifyRepository
		extends ObjectifyFilterRepository<Account, AccountId, Accounts, Accounts.Builder>
		implements AccountRepository {

	static {
		IdentityTranslators.add(ObjectifyService.factory());
		ObjectifyService.register(Account.class);
	}

	private AccountRepositoryDelegate delegate;

	public AccountObjectifyRepository(FieldDictionaryFactory fieldDictionaryFactory, Synchronization synchronization) {
		super(fieldDictionaryFactory, synchronization);
		this.delegate = new AccountRepositoryDelegate(this);
	}

	/**
	 * @param fieldDictionaryFactory
	 */
	@Inject
	public AccountObjectifyRepository(FieldDictionaryFactory fieldDictionaryFactory) {
		this(fieldDictionaryFactory, SYNCHRONIZATION_DEFAULT);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.app.account.AccountRepository#setAliases(java.util.Set)
	 */
	@Override
	public Account aliasesAdded(AccountId id, Set<String> aliases) throws EntityNotFoundException {
		return this.delegate.aliasesAdded(id, aliases);
	}

	/**
	 * @param accountId
	 * @param name
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Override
	public Account setName(AccountId accountId, String name) throws EntityNotFoundException {
		return this.delegate.setName(accountId, name);
	}
}
