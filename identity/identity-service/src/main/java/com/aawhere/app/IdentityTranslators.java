/**
 *
 */
package com.aawhere.app;

import com.aawhere.app.account.EmaiAddressTranslatorFactory;

import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.impl.translate.Translators;

/**
 * <p>
 * A convenient static method that adds all the track related translators to your factory's
 * conversions.
 * </p>
 * 
 * <p>
 * {@code JodaTimeConverters.add(ObjectifyService.factory());}
 * 
 * @author Brian Chapman
 */
public class IdentityTranslators {
	public static void add(ObjectifyFactory fact) {
		Translators translators = fact.getTranslators();
		translators.add(new ApplicationTranslatorFactory());
		translators.add(new EmaiAddressTranslatorFactory());
	}
}