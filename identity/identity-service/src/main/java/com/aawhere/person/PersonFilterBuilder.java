/**
 * 
 */
package com.aawhere.person;

import java.util.Set;

import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.field.FieldKey;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;

import com.google.common.collect.Sets;

/**
 * Constructs a query you may use with the repository to find {@link Person} entities.
 * 
 * @author aroller
 * 
 */
public class PersonFilterBuilder
		extends Filter.BaseFilterBuilder<PersonFilterBuilder> {

	private PersonFilterBuilder() {

	}

	private PersonFilterBuilder(Filter filter) {
		super(filter);
	}

	public static PersonFilterBuilder create() {
		return new PersonFilterBuilder();
	}

	public static PersonFilterBuilder clone(Filter filter) {
		return new PersonFilterBuilder(Filter.cloneForBuilder(filter));
	}

	public PersonFilterBuilder accounts(Account... accounts) {
		return accounts(Sets.newHashSet(accounts));
	}

	public PersonFilterBuilder accounts(Set<Account> accounts) {
		return accountIds(IdentifierUtils.ids(accounts));
	}

	public PersonFilterBuilder accountIds(AccountId... accounts) {
		return accountIds(Sets.newHashSet(accounts));
	}

	public PersonFilterBuilder mergedIds(PersonId... personIds) {
		return mergedIds(Sets.newHashSet(personIds));
	}

	public PersonFilterBuilder mergedIds(Iterable<PersonId> mergedIds) {
		return addCondition(FilterCondition.create().field(Person.FIELD.KEY.MERGED_IDS).operator(FilterOperator.IN)
				.value(mergedIds).build());
	}

	public PersonFilterBuilder accountIds(Iterable<AccountId> accountIds) {
		return addCondition(FilterCondition.create().field(Person.FIELD.KEY.ACCOUNT_IDS).operator(FilterOperator.IN)
				.value(accountIds).build());
	}

	/**
	 * Searches the {@link Person#name()} for matches.
	 * 
	 * @param searchString
	 */
	public PersonFilterBuilder nameStartsWith(String searchString) {
		final FieldKey fieldKey = Person.FIELD.KEY.NAME;
		return startsWith(fieldKey, searchString);
	}

	/**
	 * Provides an equality condition for {@link PersonField#NAME_SHARED}.
	 * 
	 * @param
	 */
	public PersonFilterBuilder nameShared(Boolean nameShared) {
		return addCondition(FilterCondition.create().field(PersonField.Key.IDENTITY_SHARED)
				.operator(FilterOperator.EQUALS).value(nameShared).build());
	}

}
