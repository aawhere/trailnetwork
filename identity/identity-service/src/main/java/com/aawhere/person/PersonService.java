/**
 *
 */
package com.aawhere.person;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;

import com.aawhere.app.Application;
import com.aawhere.app.Instance;
import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountService;
import com.aawhere.app.account.Accounts;
import com.aawhere.app.account.EmailAddress;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.field.FieldKey;
import com.aawhere.field.FieldUtils;
import com.aawhere.field.annotation.AnnotatedDictionaryUtils;
import com.aawhere.field.annotation.Field;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.lang.Assertion;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.person.Person.Builder;
import com.aawhere.person.pref.Preferences;
import com.aawhere.queue.BatchQueue;
import com.aawhere.queue.Queue;
import com.aawhere.queue.QueueFactory;
import com.aawhere.view.ViewCount;
import com.aawhere.view.ViewCountId;
import com.aawhere.view.ViewCountRepository;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.googlecode.objectify.Work;

/**
 * 
 * Provides services for interactions with {@link Person}, {@link Application}, {@link Instance},
 * and {@link Preferences}
 * 
 * FIXME: {@link #exists(PersonId)} and such doesn't look for aliases. Determine the best way to
 * handle this if it becomes a problem.
 */
public class PersonService
		extends EntityService<Persons, Person, PersonId>
		implements PersonProvider {

	final private PersonObjectifyRepository repository;
	final private AccountService accountService;
	final private Queue queue;
	final private ViewCountRepository viewCountRepository;
	private static final Logger logger = LoggerFactory.getLogger(PersonService.class);

	@Inject
	public PersonService(PersonObjectifyRepository repository, AccountService accountService,
			ViewCountRepository viewCountRepository, QueueFactory queueFactory) {
		super(repository);
		this.repository = repository;
		this.accountService = accountService;
		this.viewCountRepository = viewCountRepository;
		this.queue = queueFactory.getQueue(PersonWebApiUri.PERSON_QUEUE_NAME);
	}

	/**
	 * increments the {@link ViewCount} for the given person id.
	 * 
	 * @param personId
	 * @return
	 */
	public Integer viewed(PersonId personId) {
		return this.viewCountRepository.increment(new ViewCountId(personId));
	}

	/**
	 * Provides the {@link ViewCount} for the personId given. If none is found then a non-persisted
	 * blank will be provided indicated zero.
	 * 
	 * @param personId
	 * @return
	 */
	public ViewCount viewCount(PersonId personId) {
		try {
			return this.viewCountRepository.load(new ViewCountId(personId));
		} catch (EntityNotFoundException e) {
			return ViewCount.create().counting(personId).build();
		}
	}

	/**
	 * Retrieve the person from the database using a {@link PersonId}.
	 * 
	 * @param personId
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Override
	public Person person(PersonId personId) throws EntityNotFoundException {
		Person person;
		try {
			person = repository.load(personId);
		} catch (EntityNotFoundException e) {
			Persons persons = persons(PersonFilterBuilder.create().mergedIds(personId).build());
			if (persons.isEmpty()) {
				throw e;
			}
			if (persons.size() > 1) {
				logger.warning(persons + " both had a mergedId with " + personId + " TN-678 merging");
				person = merged(IdentifierUtils.ids(persons));
			} else {
				person = Iterables.getOnlyElement(persons);
			}
		}
		// TN-677
		if (person.name() == null) {
			final Pair<String, String> names = PersonUtil.personName(accounts(person));
			person = repository.name(personId, names.getLeft(), names.getRight());
		}

		return person;
	}

	/**
	 * Provides a function that calls {@link #person(PersonId)} which will find the person by id or
	 * by alias.
	 * 
	 * @return
	 */
	public Function<PersonId, Person> personFunction() {
		return new Function<PersonId, Person>() {

			@Override
			public Person apply(PersonId input) {
				try {
					return (input == null) ? null : person(input);
				} catch (EntityNotFoundException e) {
					return null;
				}
			}
		};
	}

	void store(Person person) {
		repository.store(person);
	}

	/**
	 * Provides easy access to the accounts when the person is known.
	 * 
	 * @param person
	 * @return
	 */
	public Set<Account> accounts(Person person) {
		return Sets.newHashSet(accountService.accounts(person.accountIds()));
	}

	/**
	 * Given an account, this will return the person associated with that account by finding it
	 * using the {@link Account#id()} or creating a new one if it's not already persisted. So be
	 * sure to send an account with good information, otherwise use {@link #person(AccountId)}
	 * 
	 * It will also merge if multiple persons are found matching the account and return the
	 * consolidated person.
	 * 
	 * This also handles updating a person with the given account details since the details are high
	 * quality and assumed to be most up-to-date.
	 */
	public Pair<Person, Account> person(final Account accountDetails) {
		return this.repository.ofy().transact(new Work<Pair<Person, Account>>() {

			@Override
			public Pair<Person, Account> run() {
				// first thing...make sure the account exists
				Account account = accountService.accountDetailsUpdated(accountDetails);
				Filter filter = PersonFilterBuilder.create().accounts(account).build();
				Persons persons = repository.filter(filter);
				Person person;
				if (persons.isEmpty()) {
					final Pair<String, String> suggestedNames = PersonUtil.personName(account);
					person = repository.store(Person.create().account(account).name(suggestedNames.getLeft())
							.nameWithAliases(suggestedNames.getRight()).email(account.email())
							.photoUrl(account.photoUrl()).build());
				} else {
					person = Iterables.getFirst(persons, null);
					if (persons.size() > 1) {
						logger.warning(persons + " both matched " + account + " TN-678 merging");
						try {
							person = merged(IdentifierUtils.ids(persons));
						} catch (EntityNotFoundException e) {
							// we just tried to merge something we just created in a transaction...
							// I give up!
							throw ToRuntimeException.wrapAndThrow(e);
						}
					} else {
						// choose from Account names. TODO:respect Person name if editable
						// maybe it's an update...check if there is a better name.
						// consider all the accounts
						Set<Account> accounts = accounts(person);
						HashSet<Account> combined = Sets.newHashSet(accounts);
						// don't trust the datastore has the update due to asynch
						combined.add(accountDetails);
						final Pair<String, String> suggestedNames = PersonUtil.personName(combined);
						try {
							// individual calls to repository causes problems.
							Builder updator = repository.update(person.id());
							boolean updated = false;

							EmailAddress email = PersonUtil.email(combined);
							if (email != null && !email.equals(person.email())) {
								updated = true;
								updator.email(email);
							}
							URL photoUrl = PersonUtil.photoUrl(combined);
							if (photoUrl != null && !photoUrl.equals(person.photoUrl())) {
								updated = true;
								updator.photoUrl(photoUrl);
							}
							String personName = suggestedNames.getLeft();
							if (personName != null && !personName.equals(person.name())) {
								updator.name(personName);
								updated = true;
							}
							String nameWithAliases = suggestedNames.getRight();
							if (nameWithAliases != null && !nameWithAliases.equals(person.nameWithAliases())) {
								updator.nameWithAliases(nameWithAliases);
								updated = true;
							}
							if (updated) {
								// this is a connected builder so this action stores.
								person = updator.build();
							}
						} catch (EntityNotFoundException e1) {
							// TODO Auto-generated catch block
							throw new RuntimeException(e1);
						}
					}
				}

				return Pair.of(person, account);
			}
		});

	}

	/**
	 * @return the accountService
	 */
	public AccountService accountService() {
		return this.accountService;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.person.PersonAccountProvider#person(com.aawhere.app.account.AccountId)
	 */
	public Person person(AccountId accountId) throws EntityNotFoundException {
		Filter filter = PersonFilterBuilder.create().accountIds(accountId).filter();
		Persons persons = persons(filter);
		Person result;
		if (persons.size() == 1) {
			// this is the efficient and most common path
			result = Iterables.getOnlyElement(persons);
		} else {
			// many accounts are created without persons, but that shouldn't stop us
			// create and provide the person. It also handles merging so it will return a single for
			// us
			result = person(account(accountId)).getKey();
		}
		return result;
	}

	@Provides
	public PersonAccountProvider personAccountProvider() {
		Function<AccountId, Person> idFunction = personByAccountIdFunction();
		return new PersonAccountProvider.Default(idFunction);
	}

	/**
	 * @return
	 */
	private Function<AccountId, Person> personByAccountIdFunction() {
		return new Function<AccountId, Person>() {

			@Override
			public Person apply(AccountId input) {
				try {
					return (input != null) ? person(input) : null;
				} catch (EntityNotFoundException e) {
					return null;
				}
			}

		};
	}

	public Account account(AccountId accountId) throws EntityNotFoundException {
		return accountService.account(accountId);
	}

	/**
	 * @param filter
	 * @return
	 */
	public Persons persons(Filter filter, Object... support) {
		Persons result;
		result = this.repository.filter(filter);
		if (ArrayUtils.isNotEmpty(support)) {
			result = Persons.mutate(result).addSupport(support).build();
		}
		return result;
	}

	/**
	 * Provides the persons matching the accounts with no guarantee of uniqueness. If no accounts
	 * are provided then a Persons with no results is returned.
	 * 
	 * @param accounts
	 * @return
	 */
	public Persons persons(Accounts accounts) {
		// if no ids provided then all will be returned when you really want none.
		if (Iterables.isEmpty(accounts)) {
			return Persons.create().build();
		}
		Iterable<AccountId> ids = IdentifierUtils.ids(accounts);
		return persons(PersonFilterBuilder.create().accountIds(ids).build());
	}

	/**
	 * Provides the persons matching the given by their {@link Person#id()} or
	 * {@link Person#aliases()}.
	 * 
	 * @param personIds
	 * @return
	 * @throws EntityNotFoundException
	 *             reporting the first, if any, not found by id or alias
	 */
	public Persons persons(Iterable<PersonId> personIds, Object... support) throws EntityNotFoundException {
		Set<PersonId> personsRequested = SetUtilsExtended.asSet(personIds);
		Persons persons = this.repository.entities(personsRequested);
		// if there are some missing...maybe they were aliases...let's check
		if (persons.getStatusMessages().hasError()) {
			Set<PersonId> aliases = Sets.newHashSet(PersonUtil.aliases(persons));
			SetView<PersonId> missing = Sets.difference(personsRequested, aliases);
			HashSet<Person> personsWithAliases = Sets.newHashSet(persons);
			for (PersonId missingPersonId : missing) {
				// throws entity not found exception
				personsWithAliases.add(person(missingPersonId));
			}
			persons = Persons.create().addAll(personsWithAliases).build();
		}

		// FIXME: move this into the Repository
		if (ArrayUtils.isNotEmpty(support)) {
			persons = Persons.mutate(persons).addSupport(support).build();
		}
		return persons;
	}

	/**
	 * Merges all the persons into the oldest one created. The merged winner is returned or null if
	 * no ids are found to match any existing persons. A runtime exception is thrown if no ids are
	 * given. Only those persons that exist in the system will be merged so this may be shared with
	 * a web service. Internal methods wishing to be trusted should use {@link #merged(Collection)}.
	 * 
	 * @param idsFrom
	 * @return
	 * @throws EntityNotFoundException
	 */
	@Nonnull
	public Person merged(@Nonnull Iterable<PersonId> personsIds) throws EntityNotFoundException {

		HashSet<PersonId> uniqueIds = Sets.newHashSet(personsIds);
		// loadAll will ignore missing persons that may have already been merged
		Set<Person> persons = Sets.newHashSet(this.repository.loadAll(uniqueIds).values());
		if (persons.size() == uniqueIds.size()) {
			return merged(persons);
		} else {
			// one of them not found...report it gracefully
			throw EntityNotFoundException.notFound(Sets.difference(uniqueIds, SetUtilsExtended.asSet(IdentifierUtils
					.ids(persons))));
		}
	}

	/**
	 * Using conditions, this will update values in person to the value given. If no conditions are
	 * given then {@link #updated(PersonId)} is called.
	 * 
	 * @throws EntityNotFoundException
	 */
	public Person updated(PersonId personId, Filter propertiesFilter) throws EntityNotFoundException {
		Person result = null;
		if (propertiesFilter == null || !propertiesFilter.hasConditions()) {
			result = updated(personId);
		} else {
			Builder mutator = this.repository.update(personId);
			/*
			 * This essentially looks up the @Field annotation on the Builder. However, unlike most
			 * classes with @Field, the Builder does not register a @Dictionary. We want to allow
			 * keys with the "person" dictionary to be accepted, but set fields on the Builder. If
			 * we add a dictionary to the builder, it needs to have a different name. In addition,
			 * the FieldDictionaryConstantsAnnotationProcessor will not work against the mutator,
			 * which is a proxy due to a AnnotationTypeMismatchException. Thus the reason for the
			 * "manual" lookup of annotated fields.
			 */
			// TODO: This concept of individual mutation of fields should be more centralized.
			// Perhaps with a special flag on an existing
			// annotation or a new annotation.
			Reflections reflections = new Reflections(Person.class.getPackage().getName(),
					new MethodAnnotationsScanner());
			Set<Method> methods = reflections.getMethodsAnnotatedWith(Field.class);
			for (FilterCondition<?> c : propertiesFilter.getConditions()) {
				FieldKey key = FieldUtils.key(c.getField());

				Assertion.exceptions().eq(key.getDomainKey(), AnnotatedDictionaryUtils.domainFrom(Person.class));
				for (Method method : methods) {
					if (method.getAnnotation(Field.class).key().equals(key.getRelativeKey())) {
						try {
							method.invoke(mutator, c.getValue());
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							throw new RuntimeException(e);
						}
					}
				}
			}

			result = mutator.build();
		}
		return result;
	}

	/**
	 * Updates the person represented by the given id (direct match or alias).
	 * 
	 * @param personId
	 * @param force
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Person updated(PersonId personId) throws EntityNotFoundException {
		return updated(person(personId));
	}

	/**
	 * Internal access to update common attributes of the given person. The p better be fresh
	 * because the whole thing is going into the datastore without further retrieval.
	 */
	private Person updated(Person person) {
		Person updated = PersonUtil.updated(person, accounts(person));
		return this.repository.update(updated);
	}

	public void updateQueued(PersonId personId) {
		queue.add(PersonWebApiUri.personUpdatedTask(personId));
	}

	/**
	 * Internal method from other service methods that have received persons from query and they
	 * must be merged. All persons will be treated that they exist regardless if they can be found
	 * or not (missing due to deletions likely happening during a merge).
	 * 
	 * @param personsIds
	 * @param persons
	 * @return
	 * @throws EntityNotFoundException
	 */
	Person merged(Collection<Person> given) throws EntityNotFoundException {
		Person merged;
		Set<Person> persons = SetUtilsExtended.asSet(given);
		// pulling into a set forces iteration now before mutation
		Set<PersonId> personsIds = Sets.newHashSet(IdentifierUtils.ids(persons));
		if (persons.size() > 1) {
			List<Person> oldestFirst = PersonUtil.managedAccountPersonsFirst(persons);
			Person.Builder winner = null;
			for (Person person : oldestFirst) {
				if (winner == null) {
					winner = Person.mutate(person);
					// add the ids given blindly..the merge was requested so respect it even if the
					// person can't be found because it was likely removed and merged with
					// someone...likely this
					winner.aliasesAdded(personsIds);
				} else {
					winner.merge(person);
					repository.delete(person);
				}
			}
			// now get the accounts to update the person name
			merged = updated(winner.build());
		} else {
			// runtime error if no ids were given
			final PersonId solo = Iterables.getLast(personsIds);
			if (personsIds.size() > 1) {
				// finds the only one or throws not found
				// adds the aliases given even though they weren't found
				merged = repository.aliasesAdded(solo, personsIds);
				// no need to update here
			} else {
				// nothing to merge
				merged = Iterables.getLast(persons);
			}

		}
		// tell others that care about the merge
		this.queue.add(PersonWebApiUri.personsMergedNotifiedTask(personsIds));

		return merged;
	}

	/**
	 * Provides all of the aliases for the persons given, but only those that are found. Missing
	 * persons are ignored.
	 * 
	 * @param personIds
	 * @return
	 */
	public Set<PersonId> aliasIdsExisting(Iterable<PersonId> personIds) {
		return Sets.newHashSet(PersonUtil.aliases(this.repository.entities(personIds)));
	}

	public Integer updated(Filter filter) {
		Integer count;
		if (filter.containsOption("TN-682")) {
			count = repository.touched(filter);
		} else {
			Iterable<PersonId> idPaginator = repository.idPaginator(filter);
			BatchQueue queue = BatchQueue.create().setQueue(this.queue).build();
			for (PersonId personId : idPaginator) {
				queue.add(PersonWebApiUri.personUpdatedTask(personId));
			}
			count = queue.finish();
		}
		return count;
	}

}
