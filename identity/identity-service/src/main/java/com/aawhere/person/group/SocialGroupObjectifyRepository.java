/**
 *
 */
package com.aawhere.person.group;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.persist.ObjectifyFilterRepositoryDelegate;
import com.aawhere.person.group.SocialGroup.Builder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * A common delegate allowing single implementation of required methods for various repository
 * implementations.
 * 
 * @author aroller
 * 
 */
@Singleton
public class SocialGroupObjectifyRepository
		extends ObjectifyFilterRepository<SocialGroup, SocialGroupId, SocialGroups, SocialGroups.Builder>
		implements SocialGroupRepository {

	static {
		ObjectifyService.register(SocialGroup.class);
	}

	@Inject
	public SocialGroupObjectifyRepository(FieldDictionaryFactory factory) {
		this(factory, SYNCHRONIZATION_DEFAULT);

	}

	/**
	 * @param synchronous
	 */
	public SocialGroupObjectifyRepository(FieldDictionaryFactory factory, Synchronization synchronous) {
		super(new ObjectifyFilterRepositoryDelegate<SocialGroups, SocialGroups.Builder, SocialGroup, SocialGroupId>(
				factory) {
		}, synchronous);
	}

	/*
	 * @see
	 * com.aawhere.person.group.SocialGroupRepository#update(com.aawhere.person.group.SocialGroupId)
	 */
	@Override
	public Builder update(SocialGroupId id) throws EntityNotFoundException {
		return new Builder(load(id)) {
			@Override
			public SocialGroup build() {
				return update(super.build());

			}
		};
	}

}
