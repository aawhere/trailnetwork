/**
 * 
 */
package com.aawhere.person.group;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.Nullable;

import com.aawhere.app.account.AccountId;
import com.aawhere.id.Identifier;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.person.Person;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonService;
import com.aawhere.person.Persons;
import com.aawhere.person.group.SocialGroupRelator.Options;
import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * The service for managing {@link SocialGroup} and {@link SocialGroupMembership}.
 * 
 * @author aroller
 * 
 */
@Singleton
public class SocialGroupMembershipService
		extends EntityService<SocialGroupMemberships, SocialGroupMembership, SocialGroupMembershipId> {

	private static final Logger LOG = LoggerFactory.getLogger(SocialGroupMembershipService.class);
	final SocialGroupMembershipRepository membershipRepository;
	final SocialGroupService groupService;
	final PersonService personService;

	/**
	 * @param repository
	 */
	@Inject
	public SocialGroupMembershipService(SocialGroupService groupService, PersonService personService,
			SocialGroupMembershipRepository membershipRepository) {
		super(membershipRepository);
		this.membershipRepository = membershipRepository;
		this.groupService = groupService;
		this.personService = personService;
	}

	public Set<SocialGroupId> groupIds(PersonId memberId, Filter filter) {
		return SocialGroupUtil.groupIdSet(memberships(SocialGroupMembershipFilterBuilder.clone(filter)
				.personId(memberId).build()));
	}

	/** provides access to all groups related to the given person. */
	public SocialGroups groups(PersonId personId, Filter filter) {
		return this.groupService.socialGroups(groupIds(personId, filter));
	}

	/**
	 * Provides access to all persons within a group.
	 * 
	 * @throws EntityNotFoundException
	 */
	public Persons members(SocialGroupId groupId, Filter filter) throws EntityNotFoundException {
		SocialGroupMemberships memberships = memberships(SocialGroupMembershipFilterBuilder.clone(filter)
				.groupId(groupId).build());
		Iterable<PersonId> personIds = SocialGroupUtil.personIdSet(memberships);
		return personService.persons(personIds);
	}

	/**
	 * Provides a stream of perosn Ids for the given social group.
	 * 
	 * FIXME: This could use a project query.
	 * 
	 * @param socialGroupId
	 * @return
	 */
	public Iterable<PersonId> memberIds(SocialGroupId socialGroupId) {
		return SocialGroupUtil.personIds(membershipIds(socialGroupId));
	}

	/**
	 * Provides distinct set of Groups that the person without consideration of the context of the
	 * group.
	 * 
	 * FIXME: Projection & Distinct queries would improve costs and performance TN-756
	 * 
	 * @param personId
	 * @return
	 */
	public Set<SocialGroupId> groupIds(PersonId personId) {
		Filter filter = SocialGroupMembershipFilterBuilder.create().personId(personId).build();
		Iterable<SocialGroupMembershipId> idPaginator = this.membershipRepository.idPaginator(filter);
		Iterable<SocialGroupId> groupIds = SocialGroupUtil.groupIds(idPaginator);
		return Sets.newHashSet(groupIds);
	}

	public Function<PersonId, Set<SocialGroupId>> groupsIdsForPersonFunction() {
		return new Function<PersonId, Set<SocialGroupId>>() {

			@Override
			public Set<SocialGroupId> apply(PersonId input) {
				return (input != null) ? groupIds(input) : null;
			}
		};
	}

	/**
	 * Looking at the members of the given group this finds other groups that have common
	 * memberships.
	 * 
	 * 
	 * 
	 * @param groupId
	 * @return the SocialGroupRelator providing the results and information relating to it's
	 *         decisions
	 * @throws EntityNotFoundException
	 */
	public SocialGroupRelator groupsInCommon(SocialGroupId groupId) throws EntityNotFoundException {
		return groupsInCommon(groupId, null);
	}

	/**
	 * @see #groupsInCommon(SocialGroupId)
	 * @param groupId
	 * @param options
	 * @return
	 * @throws EntityNotFoundException
	 */
	public SocialGroupRelator groupsInCommon(SocialGroupId groupId, @Nullable Options options)
			throws EntityNotFoundException {
		Iterable<PersonId> members = memberIds(groupId);
		// TODO:Efficiency and accuracy could be improved by limiting the number of members
		// inspected which could
		// be sorted by membership contribution.
		return SocialGroupRelator.create().personIds(members).groupIdsForPersonProvider(groupsIdsForPersonFunction())
				.groupProvider(groupService.idsFunction()).socialGroupTargeted(groupService.socialGroup(groupId))
				.options(options).build();
	}

	/**
	 * Provided a context this will find other groups in common with the group identified by the
	 * context.
	 * 
	 * @param context
	 * @return
	 * @throws EntityNotFoundException
	 */
	public SocialGroupRelator groupsInCommon(String category, Identifier<?, ?> context) throws EntityNotFoundException {
		SocialGroup group = this.groupService.socialGroup(category, context);
		SocialGroupId groupId = group.id();
		return groupsInCommon(groupId);
	}

	/**
	 * Registers the {@link SocialGroupMemberships} by storing them into the
	 * {@link SocialGroupMembershipRepository}.
	 * 
	 * @param memberships
	 *            to be registered
	 * @return registered memberships including information after storage.
	 */
	public SocialGroupMemberships register(SocialGroupMemberships memberships) {
		membershipRepository.storeAll(memberships);
		return memberships;
	}

	/**
	 * @return the groupService
	 */
	public SocialGroupService groupService() {
		return this.groupService;
	}

	/**
	 * @param filter
	 * @return
	 */
	public SocialGroupMemberships memberships(Filter filter) {
		return this.membershipRepository.filter(filter);
	}

	/**
	 * Determines the persisted membership count and returns the latest while updating the stored
	 * value in the {@link SocialGroup}.
	 * 
	 * @param groupId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public SocialGroup groupUpdated(SocialGroupId groupId) throws EntityNotFoundException {
		// TODO:update other properties of social groups like search for new
		// members, verify existing members, etc.
		// update the latest membership count
		Integer size = Iterables.size(this.membershipRepository.childrenIds(groupId));
		return groupService.repository.update(groupId).membershipCount(size).build();

	}

	/**
	 * When the social group is already known this will register the memberships with the associated
	 * group.
	 * 
	 * @param socialGroupId
	 * @param personIds
	 *            personId mapped to the corresponding {@link SocialGroupMembership#}
	 * @return
	 * @throws EntityNotFoundException
	 */
	public SocialGroup membershipsRegistered(SocialGroupId socialGroupId,
			Iterable<Map.Entry<PersonId, Integer>> personIds) throws EntityNotFoundException {

		// load all the existing children for an updated final count
		final Iterable<SocialGroupMembershipId> existingMembershipIds = this.membershipRepository
				.childrenIds(socialGroupId);
		Set<SocialGroupMembershipId> existingAndNewMembershipIds = Sets.newHashSet(existingMembershipIds);
		int existingMembershipCount = existingAndNewMembershipIds.size();

		Iterable<SocialGroupMembership> memberships = SocialGroupUtil.memberships(socialGroupId, personIds);

		// combine the ids from those given with the existing ids which is the updated count
		// don't mess with the return results so asynch store is possible
		Iterables.addAll(existingAndNewMembershipIds, SocialGroupUtil.membershipIds(memberships));

		try {
			// update the membership count..throws Not Found
			final SocialGroup group = groupService.repository.update(socialGroupId)
					.membershipCount(existingAndNewMembershipIds.size()).build();
			// the update validates the group exists...now we can store asynchronously so
			// memberships
			// match the count
			this.membershipRepository.storeAll(memberships);

			return group;
		} catch (EntityNotFoundException e) {
			// the group is gone, but we found memberships? let's rid them
			if (existingMembershipCount > 0) {
				membershipRepository.deleteAllFromIds(existingMembershipIds);
			}
			throw e;
		}
	}

	/**
	 * Registers each of the persons related to the given accounts as a member of the SocialGroup
	 * identified by the given {@link SocialGroupId}.
	 * 
	 * @param groupId
	 * @param accountIds
	 * @return
	 */
	public SocialGroupMemberships register(SocialGroupId groupId, Iterable<AccountId> accountIds) {
		SocialGroupMemberships.Builder memberships = SocialGroupMemberships.create();
		for (AccountId accountId : accountIds) {
			Person person;
			try {
				person = this.personService.person(accountId);
				memberships.add(SocialGroupMembership.create().group(groupId).member(person.id()).build());
			} catch (EntityNotFoundException e) {
				// this is unexpected, not worth crying over
				LoggerFactory.getLogger(getClass()).warning(accountId + " doesn't match a person: " + e);
			}
		}

		return register(memberships.build());
	}

	/**
	 * @param socialGroupId
	 * @return
	 */
	public SocialGroupMemberships memberships(SocialGroupId socialGroupId, Filter filter) {
		return memberships(SocialGroupMembershipFilterBuilder.clone(filter).setParent(socialGroupId).build());
	}

	public SocialGroupMemberships memberships(SocialGroup socialGroup, Filter filter) {
		return memberships(socialGroup.id(), filter);
	}

	/**
	 * @param socialGroupMembershipId
	 * @return
	 * @throws EntityNotFoundException
	 */
	public SocialGroupMembership membership(SocialGroupMembershipId socialGroupMembershipId)
			throws EntityNotFoundException {
		return super.entity(socialGroupMembershipId);
	}

	/**
	 * Provides the memberships matching the context limited by the filter.
	 * 
	 * @param routeId
	 * @param filter
	 * @return
	 * @throws EntityNotFoundException
	 *             when the context is not found to exist.
	 */
	public SocialGroupMemberships memberships(String category, Identifier<?, ?> context, Filter filter)
			throws EntityNotFoundException {
		SocialGroup group = groupService.socialGroup(category, context);
		return memberships(group, filter);

	}

	/**
	 * Provides a stream of all memberships regardless of the size.
	 * 
	 * @see #memberships(SocialGroupId, Filter)
	 * @param groupId
	 */
	public Iterable<SocialGroupMembership> memberships(SocialGroupId groupId) {
		return this.membershipRepository.children(groupId);
	}

	public Iterable<SocialGroupMembershipId> membershipIds(SocialGroupId groupId) {
		return this.membershipRepository.childrenIds(groupId);
	}

	public Iterable<SocialGroupMembership> memberships(PersonId personId) {
		return this.membershipRepository.entityPaginator(SocialGroupMembershipFilterBuilder.create().personId(personId)
				.build());
	}

	/**
	 * A specialty function allowing organizers to provide a SocialGroup that will be persisted
	 * (whether it already persisted or not)
	 * 
	 * @return
	 */
	public Function<Entry<SocialGroup, ? extends Iterable<Entry<PersonId, Integer>>>, SocialGroup>
			membershipsRegisteredFunction() {
		return new Function<Map.Entry<SocialGroup, ? extends Iterable<Entry<PersonId, Integer>>>, SocialGroup>() {

			@Override
			public SocialGroup apply(Entry<SocialGroup, ? extends Iterable<Entry<PersonId, Integer>>> input) {
				if (input == null) {
					return null;
				}
				// updated or created social group (they may be mixed updates/creates
				SocialGroup socialGroup = groupService.socialGroupPersisted(input.getKey());
				try {
					return membershipsRegistered(socialGroup.id(), input.getValue());
				} catch (EntityNotFoundException e) {
					// this shouldn't happen
					throw new RuntimeException(e);
				}
			}
		};
	}

	/** Removes all the social groups and the associated memberships. */
	public void socialGroupsDeleted(Iterable<SocialGroupId> socialGroupIds) {
		if (socialGroupIds != null) {
			this.groupService.deleteAll(socialGroupIds);
			for (SocialGroupId socialGroupId : Iterables.filter(socialGroupIds, Predicates.notNull())) {
				Iterable<SocialGroupMembershipId> membershipIds = this.membershipIds(socialGroupId);
				this.membershipRepository.deleteAllFromIds(membershipIds);
			}
		}
	}
}
