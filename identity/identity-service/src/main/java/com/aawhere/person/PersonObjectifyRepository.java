/**
 *
 */
package com.aawhere.person;

import com.aawhere.app.IdentityTranslators;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.lang.exception.ToRuntimeException;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.persist.objectify.ObjectifyRepository;
import com.aawhere.person.Person.Builder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Work;

/**
 * Implements the {@link PersonRepository} for {@link ObjectifyRepository} to manage {@link Person}
 * s.
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class PersonObjectifyRepository
		extends ObjectifyFilterRepository<Person, PersonId, Persons, Persons.Builder>
		implements PersonRepository {

	static {
		IdentityTranslators.add(ObjectifyService.factory());
		ObjectifyService.register(Person.class);
	}

	private final PersonRepositoryDelegate delegate;

	@Inject
	public PersonObjectifyRepository(FieldDictionaryFactory fieldDictionaryFactory) {
		this(fieldDictionaryFactory, SYNCHRONIZATION_DEFAULT);
	}

	public PersonObjectifyRepository(FieldDictionaryFactory fieldDictionaryFactory, Synchronization synchronous) {
		super(fieldDictionaryFactory, synchronous);
		this.delegate = new PersonRepositoryDelegate(this);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.person.PersonRepository#setName(com.aawhere.person.PersonId,
	 * java.lang.String)
	 */
	@Override
	public Person name(PersonId personId, String name, String nameWithAliases) throws EntityNotFoundException {
		return this.delegate.name(personId, name, nameWithAliases);
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.person.PersonRepository#aliasesAdded(com.aawhere.person.PersonId,
	 * java.lang.Iterable)
	 */
	@Override
	public Person aliasesAdded(final PersonId personId, final Iterable<PersonId> personIds)
			throws EntityNotFoundException {

		try {
			return ofy().transact(new Work<Person>() {

				@Override
				public Person run() {
					try {
						return delegate.aliasesAdded(personId, personIds);
					} catch (EntityNotFoundException e) {
						throw ToRuntimeException.wrapAndThrow(e);
					}
				}
			});
		} catch (ToRuntimeException e) {
			throw ToRuntimeException.unwrap(e, EntityNotFoundException.class);
		}

	}

	/*
	 * @see com.aawhere.person.PersonRepository#update(com.aawhere.person.PersonId)
	 */
	@Override
	public Builder update(PersonId personId) throws EntityNotFoundException {
		return delegate.update(personId);
	}

}
