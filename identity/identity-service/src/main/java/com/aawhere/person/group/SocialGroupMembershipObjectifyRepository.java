/**
 *
 */
package com.aawhere.person.group;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.persist.ObjectifyFilterRepository;
import com.aawhere.persist.ObjectifyFilterRepositoryDelegate;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author aroller
 * 
 */
@Singleton
public class SocialGroupMembershipObjectifyRepository
		extends
		ObjectifyFilterRepository<SocialGroupMembership, SocialGroupMembershipId, SocialGroupMemberships, SocialGroupMemberships.Builder>
		implements SocialGroupMembershipRepository {

	static {
		ObjectifyService.register(SocialGroupMembership.class);
		ObjectifyService.register(SocialGroup.class);
	}

	/**
	 * @param filterRepository
	 * @param synchronous
	 */
	@Inject
	protected SocialGroupMembershipObjectifyRepository(FieldDictionaryFactory factory) {
		this(factory, SYNCHRONIZATION_DEFAULT);
	}

	public SocialGroupMembershipObjectifyRepository(FieldDictionaryFactory factory, Synchronization synchronization) {
		super(
				new ObjectifyFilterRepositoryDelegate<SocialGroupMemberships, SocialGroupMemberships.Builder, SocialGroupMembership, SocialGroupMembershipId>(
						factory) {
				}, synchronization);
	}
}
