/**
 * 
 */
package com.aawhere.person.group;

import com.aawhere.id.Identifier;
import com.aawhere.log.LoggerFactory;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityService;
import com.aawhere.persist.Filter;
import com.aawhere.util.rb.CompleteMessage;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author aroller
 * 
 */
@Singleton
public class SocialGroupService
		extends EntityService<SocialGroups, SocialGroup, SocialGroupId>
		implements SocialGroupProvider {

	final SocialGroupRepository repository;

	/**
	 * @param repository
	 */
	@Inject
	public SocialGroupService(SocialGroupRepository repository) {
		super(repository);
		this.repository = repository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityService#getRepository()
	 */
	@Override
	protected SocialGroupRepository getRepository() {
		return (SocialGroupRepository) super.getRepository();
	}

	/**
	 * Finds the group related to the given group by inspecting the given criteria. If no group is
	 * found then one is created.
	 * 
	 * @param group
	 * @return the persistent updated group
	 * @throws EntityNotFoundException
	 */
	public SocialGroup socialGroupPersisted(final SocialGroup group) {
		Identifier<?, ?> context = group.context();
		SocialGroup result;
		try {
			result = socialGroup(group.category(), context);
			result = getRepository().update(result.id()).membershipCount(group.membershipCount()).score(group.score())
					.category(group.category()).build();
		} catch (EntityNotFoundException e) {
			result = this.getRepository().store(group);
		}
		return result;
	}

	/**
	 * Provides the group that matches the context identifier given. If the given context is
	 * SocialGroupId then a call to {@link #socialGroup(SocialGroupId)} is made ignore the search
	 * for context.
	 * 
	 * @param context
	 * @return
	 * @throws EntityNotFoundException
	 */
	public SocialGroup socialGroup(String category, Identifier<?, ?> context) throws EntityNotFoundException {

		SocialGroups groups = socialGroups(SocialGroupFilterBuilder.create().categoryContext(category, context).build());
		if (groups.size() > 1) {
			// FIXME:We must merge
			LoggerFactory.getLogger(getClass()).warning(category + context
					+ " found more than one, but we can't merge yet: " + groups);
		}
		if (groups.isEmpty()) {
			throw EntityNotFoundException.notFound(CompleteMessage
					.create(SocialGroupMessage.CATEGORY_CONTEXT_NOT_FOUND)
					.param(SocialGroupMessage.Param.CATEGORY, category)
					.param(SocialGroupMessage.Param.CONTEXT, context).build());
		} else {
			return groups.iterator().next();
		}
	}

	/**
	 * provides the {@link SocialGroups} limited by the given {@link Filter}.
	 * 
	 * @param filter
	 * @return
	 */
	public SocialGroups socialGroups(Filter filter) {
		return getRepository().filter(filter);
	}

	/**
	 * Simple getter by ID.
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	public SocialGroup socialGroup(SocialGroupId id) throws EntityNotFoundException {
		return getRepository().load(id);
	}

	/**
	 * @param groupIds
	 * @return
	 */
	public SocialGroups socialGroups(Iterable<SocialGroupId> groupIds) {
		return getRepository().entities(groupIds);
	}

	/**
	 * Provides a stream of context Identifiers for the given socialgroups.
	 * 
	 * 
	 * FIXME: this could use a projection query
	 * 
	 * @param ids
	 * @return
	 */
	public Iterable<? extends Identifier<?, ?>> contexts(Iterable<SocialGroupId> ids) {
		return SocialGroupUtil.contextIds(this.getRepository().loadAll(ids).values());
	}

	/**
	 * Contexts may be found in multiple {@link SocialGroup#category()}. This returns them all.
	 * 
	 * @param context
	 * @return
	 */
	public SocialGroups socialGroups(Identifier<?, ?> context, Filter filter) {
		return socialGroups(SocialGroupFilterBuilder.clone(filter).context(context).orderByNone().build());
	}

	public SocialGroups socialGroups(String category, Filter filter) {
		return socialGroups(SocialGroupFilterBuilder.clone(filter).category(category).build());
	}

	/**
	 * @param socialGroupIds
	 */
	public void deleteAll(Iterable<SocialGroupId> socialGroupIds) {
		this.repository.deleteAllFromIds(socialGroupIds);
	}

}
