/**
 *
 */
package com.aawhere.app.account;

import org.junit.After;
import org.junit.Before;

import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;
import com.aawhere.person.PersonServiceTestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author aroller
 * 
 */
public class AccountObjectifyRepositoryTest
		extends AccountUnitTest {
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();

	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected AccountRepository createRepository() {
		return new AccountObjectifyRepository(new PersonServiceTestUtil.Builder().getAnnotationDictionaryFactory()
				.getFactory(), Synchronization.SYNCHRONOUS);
	}
}
