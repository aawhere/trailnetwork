/**
 * 
 */
package com.aawhere.app.account;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.ApplicationKey;
import com.aawhere.app.KnownApplication;
import com.aawhere.collections.SetUtilsExtended;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.person.PersonServiceTestUtil;
import com.aawhere.test.TestUtil;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Sets;

/**
 * @author aroller
 * 
 */
public class AccountServiceTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private PersonServiceTestUtil personTestUtil;
	private AccountService accountService;

	@Before
	public void setUpServices() {
		helper.setUp();
		this.personTestUtil = new PersonServiceTestUtil.Builder().build();
		this.accountService = this.personTestUtil.getPersonService().accountService();
	}

	@After
	public void tearDown() {
		helper.tearDown();
	}

	@Test
	public void testMergeAliasesAndPersist() throws EntityNotFoundException {
		Account expected = AccountTestUtil.account();
		try {
			this.accountService.account(expected.getId());
			fail("why did we find " + expected.getId());
		} catch (EntityNotFoundException e) {
			// this is good...let's see if the filter finds anything.
			Accounts filteredNoResults = this.accountService.accounts(AccountFilterBuilder.create().build());
			TestUtil.assertEmpty(filteredNoResults.getCollection());
		}
		Account persisted = this.accountService.accountDetailsUpdated(expected);
		Integer persistedOnceEntityVersion = 1;
		assertEquals(	"ensures the account is now persisted since it already had an id",
						persistedOnceEntityVersion,
						persisted.getEntityVersion());

		Accounts filterAnyOneResult = this.accountService.accounts(AccountFilterBuilder.create().build());
		TestUtil.assertContains("didn't find the one created with nothing filtered",
								persisted,
								filterAnyOneResult.getCollection());
		Accounts filterIdOneResult = this.accountService.accounts(AccountFilterBuilder.create()
				.idEqualTo(persisted.getId()).build());
		TestUtil.assertContains("didn't find by id filter", persisted, filterIdOneResult.getCollection());
		assertNotNull("didn't find by Id", this.accountService.account(persisted.getId()));
		Account persistedAgain = this.accountService.accountDetailsUpdated(expected);
		assertEquals(	"shouldn't have been modified since nothing changed",
						persistedOnceEntityVersion,
						persistedAgain.getEntityVersion());
		String additionalAlias = AccountTestUtil.alias();
		Account withAdditionalAlias = Account.clone(persistedAgain).addAlias(additionalAlias).build();
		Account persistedWithAlias = this.accountService.accountDetailsUpdated(withAdditionalAlias);
		assertEquals("should have changed", persistedOnceEntityVersion + 1, persistedWithAlias.getEntityVersion()
				.intValue());
		TestUtil.assertContains(additionalAlias, persistedWithAlias.getAliases());
		Accounts filteredAfterRepersist = this.accountService.accounts(AccountFilterBuilder.create().build());
		TestUtil.assertContains("should still find only one", persisted, filteredAfterRepersist.getCollection());

	}

	/**
	 * WW-296 confirms that updating only happens if there is a change. It also confirms that the
	 * new account name is used. This assumes similar results for {@link Account#photoUrl()} and
	 * other properties that are tested in {@link AccountUtilUnitTest#testEqualsDeeply()	}.
	 */
	@Test
	public void testNameChange() {
		Account account = this.personTestUtil.account();
		Account persistedAgain = this.accountService.accountDetailsUpdated(account);
		assertEquals("nothing changed", account.getEntityVersion(), persistedAgain.getEntityVersion());
		Account nameChanged = Account.clone(account).setName(TestUtil.generateRandomAlphaNumeric()).build();
		Account persistedWithNewName = this.accountService.accountDetailsUpdated(nameChanged);
		assertEquals(nameChanged.name(), persistedWithNewName.name());
		assertEquals("not Persisted", persistedAgain.getEntityVersion() + 1, persistedWithNewName.getEntityVersion()
				.intValue());
	}

	/**
	 * 
	 */
	@Test
	public void testEmailChange() {
		Account account = this.personTestUtil.account();
		assertNotNull(account.email());
		EmailAddress newEmailAddress = AccountTestUtil.emailAddress();
		Account withNewEmail = this.accountService.accountDetailsUpdated(Account.clone(account).email(newEmailAddress)
				.build());
		TestUtil.assertNotEquals(account.getEntityVersion(), withNewEmail.getEntityVersion());
		assertEquals(newEmailAddress, withNewEmail.email());
	}

	@Test
	public void testAddAlias() throws EntityNotFoundException {

		Account account = this.personTestUtil.account();
		Set<String> aliases = account.getAliases();
		String additional = AccountTestUtil.alias();
		Account updated = this.accountService.addAlias(account.id(), additional);
		HashSet<String> expectedAliases = Sets.newHashSet(aliases);
		expectedAliases.add(additional);
		assertEquals("alias not updated", expectedAliases, updated.getAliases());
		assertEquals("not really persisted", expectedAliases, this.accountService.account(account.id()).getAliases());

		// persist another to ensure test is working.
		this.personTestUtil.account();
		Accounts accounts = accountService.accounts(account.applicationKey(), additional);
		TestUtil.assertContainsOnly(account, accounts);
	}

	@Test
	public void testAliasFromDifferentApplications() throws EntityNotFoundException {
		AccountId gcId = AccountTestUtil.accountId(KnownApplication.GARMIN_CONNECT.key);
		AccountId etId = AccountTestUtil.accountId(KnownApplication.EVERY_TRAIL.key);
		String alias = AccountTestUtil.alias();
		Account gc = this.personTestUtil.createPersistedAccount(gcId);
		Account et = this.personTestUtil.createPersistedAccount(etId);
		assertEquals(gcId, gc.id());
		assertEquals(etId, et.id());
		gc = this.accountService.addAlias(gcId, alias);
		et = this.accountService.addAlias(etId, alias);
		TestUtil.assertContainsOnly(gc, this.accountService.accounts(gcId.getApplicationKey(), alias));
		TestUtil.assertContainsOnly(et, this.accountService.accounts(etId.getApplicationKey(), alias));
	}

	/**
	 * Ensures that when searching by an alias as an account id that doesn't have a remote id then
	 * the account will be returned by the alias.
	 * 
	 * allows for clients to use the commonly used username as the search item and works well for
	 * most situations since aliases are typically unique.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testFindByAliasInAccountId() throws EntityNotFoundException {
		ApplicationKey app = KnownApplication.GARMIN_CONNECT.key;
		AccountId aId = new AccountId(app, "a");
		Account a = this.personTestUtil.createPersistedAccount(aId);
		// b has alias of a
		AccountId bId = new AccountId(app, "b");

		Account b = this.personTestUtil.createPersistedAccount(bId);
		b = this.accountService.addAlias(bId, aId.getRemoteId());
		assertEquals("remote id wins over alias!", a, this.accountService.account(aId));
		String alias = "c";
		b = this.accountService.addAlias(bId, alias);
		AccountId cId = new AccountId(app, alias);
		assertEquals("c is the alias of b and doesn't exist", b, this.accountService.account(cId));
	}

	/**
	 * TN-525 Garmin aliases have been email addresses. This cleans it up.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testCleanUpAliases() throws EntityNotFoundException {
		String username = "me";
		String email = username + "@bogus.net";
		assertCleanUpAliases(username, email, true);
	}

	/**
	 * TN-525 Garmin aliases have been email addresses. This cleans it up.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testCleanUpAliasesNotAnEmailAddress() throws EntityNotFoundException {
		String email = "not@anemailaddress";
		assertCleanUpAliases(null, email, false);
	}

	/**
	 * TN-553 Some aliases are being persisted as null.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testCleanUpAliasesCheckNotNull() throws EntityNotFoundException {
		String email = "@bogus.net";
		assertCleanUpAliases(null, email, false);
	}

	private void assertCleanUpAliases(String expected, String original, Boolean expectedToBeCleanedUp)
			throws EntityNotFoundException {
		Account createPersistedAccount = personTestUtil.account();
		Account withBadEmail = Account.mutate(createPersistedAccount).addAlias(original).build();
		// bypass service who cleans it all up
		Account storedWithEmail = personTestUtil.getAccountRepository().store(withBadEmail);
		Account found = accountService.account(storedWithEmail.getId());
		if (expectedToBeCleanedUp) {

			// we can no longer get bad emails inserted now that repository is doing the cleaup as
			// is account builder.
			// TestUtil.assertNotEquals(storedWithEmail.getAliases(), found.getAliases());
		} else {
			assertEquals(storedWithEmail.getAliases(), found.getAliases());
		}

		Account loadedWithoutFilter = personTestUtil.getAccountRepository().load(found.id());
		assertEquals("the filtered aliases  not persisted.", found.getAliases(), loadedWithoutFilter.getAliases());
		// We always have one when we createPersistedAccount. So if we don't have > 1, we didn't
		// filter the username.
		if (expectedToBeCleanedUp) {
			TestUtil.assertContains(expected, loadedWithoutFilter.getAliases());
		} else {
			assertEquals(found.getEntityVersion(), loadedWithoutFilter.getEntityVersion());
		}
	}

	@Test
	public void testUpdateNotPossible() throws EntityNotFoundException {
		Account account = personTestUtil.account();
		Account updated = accountService.updateNotPossible(account.id());
		TestUtil.assertGreaterThan(account.getEntityVersion(), updated.getEntityVersion());
	}

	@Test
	public void testAccountUpdated() {
		Account account = personTestUtil.account();
		String expectedName = AccountTestUtil.accountName();
		String expectedUsername = AccountTestUtil.alias();
		Account accountUpdated = this.accountService.accountUpdated(account.id(), expectedName, expectedUsername);
		assertEquals(expectedName, accountUpdated.name());
		TestUtil.assertIterablesEquals(	"aliases not updated properly",
										SetUtilsExtended.newHashSet(account.aliases(), expectedUsername),
										accountUpdated.aliases());

	}

	@Test
	public void testAccountUpdatedWhenAccountMissing() {
		AccountId accountId = AccountTestUtil.accountId();
		String expectedName = AccountTestUtil.accountName();
		String expectedUsername = AccountTestUtil.alias();
		Account accountUpdated = this.accountService.accountUpdated(accountId, expectedName, expectedUsername);
		assertEquals(expectedName, accountUpdated.name());
		TestUtil.assertContainsOnly("aliases not updated properly", expectedUsername, accountUpdated.aliases());

	}
}
