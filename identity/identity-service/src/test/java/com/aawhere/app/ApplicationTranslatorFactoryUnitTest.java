/**
 * 
 */
package com.aawhere.app;

import com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest;
import com.googlecode.objectify.annotation.Entity;

/**
 * @author aroller
 * 
 */
public class ApplicationTranslatorFactoryUnitTest
		extends ValueTranslatorFactoryObjectifyTest<ApplicationTranslatorFactoryUnitTest.ApplicationTestEntity> {

	public ApplicationTranslatorFactoryUnitTest() {
		super(new ApplicationTranslatorFactory());
	}

	@Entity
	public static class ApplicationTestEntity
			extends com.aawhere.persist.conv.ValueTranslatorFactoryObjectifyTest.TestEntity {

		public ApplicationTestEntity(Application pojo) {
			this.pojo = pojo;
		}

		public Application pojo;

	}

	@Override
	public void testBasic() {
		test("gc", new ApplicationTestEntity(KnownApplication.GARMIN_CONNECT.asApplication()));

	}

	@Override
	public void testNull() {
		test("null", new ApplicationTestEntity(null));

	}
}
