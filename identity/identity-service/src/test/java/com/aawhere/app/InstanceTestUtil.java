/**
 *
 */
package com.aawhere.app;

import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.StringMessage;

/**
 * @author brian
 * 
 */
public class InstanceTestUtil {

	public static AccountInstance createRandomAccount() {
		return new AccountInstance.Builder().setApplication(KnownApplication.TRAIL_NETWORK.asApplication())
				.setKey(new InstanceKey(TestUtil.generateRandomAlphaNumeric())).build();
	}

	/**
	 * @return
	 */
	public static Device createRandomDevice() {
		return new Device.Builder()
				.setApplication(new Application.Builder()
						.setKey(new ApplicationKey(TestUtil.generateRandomAlphaNumeric()))
						.setName(new StringMessage(TestUtil.generateRandomAlphaNumeric())).build())
				.setKey(new InstanceKey(TestUtil.generateRandomAlphaNumeric())).build();
	}
}
