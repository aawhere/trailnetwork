/**
 * 
 */
package com.aawhere.app.account;

import org.junit.Before;

import com.aawhere.persist.BaseObjectifyFilterRepositoryTest;
import com.aawhere.person.PersonServiceTestUtil;

/**
 * @author aroller
 * 
 */
public class AccountObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<AccountId, Account, AccountObjectifyRepository, AccountObjectifyRepository, Accounts, Accounts.Builder> {

	private AccountObjectifyRepository repository;

	@Before
	public void setUp() {
		this.repository = PersonServiceTestUtil.create().build().getAccountRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected Account createEntity() {
		return AccountUnitTest.create().getEntitySampleWithId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected AccountObjectifyRepository getRepository() {
		return this.repository;
	}

}
