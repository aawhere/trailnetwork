/**
 * 
 */
package com.aawhere.view;

import org.junit.After;
import org.junit.Before;

import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author aroller
 * 
 */
public class ViewCountObjectifyRepositoryTest
		extends ViewCountUnitTest {
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();

	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.view.ViewCountUnitTest#createRepository()
	 */
	@Override
	protected ViewCountRepository createRepository() {
		return new ViewCountObjectifyRepository(Synchronization.SYNCHRONOUS);
	}

}
