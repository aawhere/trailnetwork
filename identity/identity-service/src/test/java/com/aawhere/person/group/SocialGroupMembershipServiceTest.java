/**
 * 
 */
package com.aawhere.person.group;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityServiceBaseTest;
import com.aawhere.person.PersonId;
import com.aawhere.person.PersonServiceTestUtil;
import com.aawhere.person.group.SocialGroupRelator.Options;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class SocialGroupMembershipServiceTest
		extends
		EntityServiceBaseTest<SocialGroupMembershipService, SocialGroupMemberships, SocialGroupMembership, SocialGroupMembershipId> {

	private SocialGroupMembershipService service;
	private PersonServiceTestUtil testUtil;

	@Before
	public void setUp() {
		super.setUpServices();
		this.testUtil = PersonServiceTestUtil.create().build();
		this.service = testUtil.membershipService();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#service()
	 */
	@Override
	public SocialGroupMembershipService service() {
		return service;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#entity()
	 */
	@Override
	public SocialGroupMembership entity() {
		return SocialGroupMembershipTestUtil.membership();
	}

	@Test
	public void testMembershipsForGroup() {
		SocialGroupMembership firstInGroup = testUtil.socialGroupMembership();
		SocialGroupId groupId = firstInGroup.socialGroupId();
		SocialGroupMembership secondInGroup = testUtil.socialGroupMembership(groupId);
		SocialGroupMembership notInGroup = testUtil.socialGroupMembership();
		Iterable<SocialGroupMembership> memberships = service.memberships(groupId);
		TestUtil.assertContains(firstInGroup, memberships);
		TestUtil.assertContains(secondInGroup, memberships);
		TestUtil.assertDoesntContain(notInGroup, memberships);

	}

	@Test
	public void testMembershipsForPerson() {
		SocialGroupMembership firstGroup = testUtil.socialGroupMembership();
		PersonId personId = firstGroup.personId();
		SocialGroupMembership secondGroup = testUtil.socialGroupMembership(personId);
		SocialGroupMembership differentPerson = testUtil.socialGroupMembership();
		Iterable<SocialGroupMembership> memberships = service.memberships(personId);
		TestUtil.assertContains(firstGroup, memberships);
		TestUtil.assertContains(secondGroup, memberships);
		TestUtil.assertDoesntContain(differentPerson, memberships);
	}

	/**
	 * Finds those groups where all the persons given are found in every group. Three memberships
	 * created, only one where both persons are members...the other two have single members...one in
	 * each.
	 * 
	 * @throws EntityNotFoundException
	 * 
	 */
	@Test
	public void testGroupsInCommon() throws EntityNotFoundException {
		// same score for each...leave the score testing up to the unit test
		Integer score = 50;
		// first person sets up the group
		SocialGroupMembership firstPersonSoloMembership = testUtil.socialGroupMembership();
		// the groups must have the same category to be in common
		String commonCategory = this.testUtil.groupService().socialGroup(firstPersonSoloMembership.socialGroupId())
				.category();
		PersonId firstPerson = firstPersonSoloMembership.personId();
		SocialGroupMembership commonMembershipForFirstPerson = testUtil.socialGroupMembership(firstPerson);
		// the targeted common group
		SocialGroupId commonGroupId = commonMembershipForFirstPerson.socialGroupId();
		this.testUtil.groupRepository().update(commonGroupId).score(score).category(commonCategory).build();

		// second person joins the common group
		SocialGroupMembership commonMembershipForSecondPerson = testUtil.socialGroupMembership(commonGroupId);
		PersonId secondPerson = commonMembershipForSecondPerson.personId();
		this.testUtil.groupRepository().update(commonMembershipForSecondPerson.socialGroupId()).score(score)
				.category(commonCategory).build();

		// second person joins another uncommon group
		SocialGroupMembership secondPersonSoloMembership = testUtil.socialGroupMembership(secondPerson);
		this.testUtil.groupRepository().update(secondPersonSoloMembership.socialGroupId()).score(score)
				.category(commonCategory).build();

		// create a group just like the common so we can target it and find the common
		SocialGroup target = testUtil.socialGroup();
		HashMap<PersonId, Integer> membershipCounts = new HashMap<PersonId, Integer>();
		membershipCounts.put(firstPerson, TestUtil.generateRandomInt());
		membershipCounts.put(secondPerson, TestUtil.generateRandomInt());
		target = this.service.membershipsRegistered(target.id(), membershipCounts.entrySet());
		final int expectedGroupSize = 2;
		assertEquals("members didn't get registered for target", expectedGroupSize, target.membershipCount().intValue());

		// setup options to allow the small numbers to match
		Options options = SocialGroupRelator.Options.create().inCommonMinimum(expectedGroupSize);

		// now find the commmon group that is similar to the target
		SocialGroupRelator relator = service.groupsInCommon(target.id(), options);
		TestUtil.assertContains(commonGroupId, IdentifierUtils.ids(relator.groupsMatchingCategory(commonCategory)));
		assertTrue("in common membership not contained", relator.contains(commonGroupId));
		assertEquals(	"Both persons should be part of this group",
						expectedGroupSize,
						relator.numberOfPeopleInCommon(commonGroupId).intValue());
		assertFalse("only a single entry", relator.contains(firstPersonSoloMembership.socialGroupId()));
		assertFalse("only a single entry", relator.contains(secondPersonSoloMembership.socialGroupId()));

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#testUtil.socialGroupMembership()
	 */
	@Override
	public SocialGroupMembership persisted() {
		return this.testUtil.socialGroupMembership();
	}

}
