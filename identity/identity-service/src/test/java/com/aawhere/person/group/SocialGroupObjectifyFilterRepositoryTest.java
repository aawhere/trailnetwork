/**
 * 
 */
package com.aawhere.person.group;

import org.junit.Before;

import com.aawhere.id.IdentifierTestUtil.ChildId;
import com.aawhere.persist.BaseObjectifyFilterRepositoryTest;
import com.aawhere.person.PersonServiceTestUtil;

/**
 * @author aroller
 * 
 */
public class SocialGroupObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<SocialGroupId, SocialGroup, SocialGroupObjectifyRepository, SocialGroupRepository, SocialGroups, SocialGroups.Builder> {

	private PersonServiceTestUtil util;
	private SocialGroupObjectifyRepository repository;

	@Before
	public void setUp() {
		util = PersonServiceTestUtil.create().register(ChildId.class).build();
		repository = (SocialGroupObjectifyRepository) util.groupRepository();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected SocialGroup createEntity() {
		return util.persistedGroup();

	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected SocialGroupRepository getRepository() {
		return repository;
	}

}
