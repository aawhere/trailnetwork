/**
 * 
 */
package com.aawhere.person.group;

import org.junit.After;
import org.junit.Before;

import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.person.PersonServiceTestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author aroller
 * 
 */
public class SocialGroupMembershipObjectifyRepositoryTest
		extends SocialGroupMembershipUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();
	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.persist.BaseEntityBaseUnitTest#createRepository()
	 */
	@Override
	protected SocialGroupMembershipRepository createRepository() {
		return PersonServiceTestUtil.create().build().groupMembershipRepository();
	}

}
