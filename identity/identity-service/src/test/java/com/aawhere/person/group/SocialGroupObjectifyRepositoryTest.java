/**
 * 
 */
package com.aawhere.person.group;

import org.junit.After;
import org.junit.Before;

import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.person.PersonServiceTestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * @author aroller
 * 
 */
public class SocialGroupObjectifyRepositoryTest
		extends SocialGroupUnitTest {
	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();

	}

	/*
	 * @see com.aawhere.person.group.SocialGroupUnitTest#createRepository()
	 */
	@Override
	protected SocialGroupRepository createRepository() {
		return PersonServiceTestUtil.create()
		// must register any type to be used in the generic SocialGroup#context
		// field
				.register(contextIdType).build().groupRepository();
	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

}
