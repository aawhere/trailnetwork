/**
 *
 */
package com.aawhere.person;

import java.util.HashSet;

import org.apache.commons.lang3.tuple.Pair;

import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountId;
import com.aawhere.app.account.AccountObjectifyRepository;
import com.aawhere.app.account.AccountService;
import com.aawhere.app.account.AccountUnitTest;
import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.field.FieldTestUtil;
import com.aawhere.field.annotation.FieldAnnotationDictionaryFactory;
import com.aawhere.field.type.DataTypeAdapterFactory;
import com.aawhere.id.Identifier;
import com.aawhere.id.IdentifierTranslators;
import com.aawhere.lang.ObjectBuilder;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.LongMockEntity;
import com.aawhere.persist.LongMockEntityId;
import com.aawhere.persist.RegisteredEntitiesObjectifyFactory;
import com.aawhere.persist.StringMockEntity;
import com.aawhere.persist.objectify.ObjectifyRepository.Synchronization;
import com.aawhere.person.group.SocialGroup;
import com.aawhere.person.group.SocialGroupId;
import com.aawhere.person.group.SocialGroupMembership;
import com.aawhere.person.group.SocialGroupMembershipObjectifyRepository;
import com.aawhere.person.group.SocialGroupMembershipService;
import com.aawhere.person.group.SocialGroupMembershipTestUtil;
import com.aawhere.person.group.SocialGroupObjectifyRepository;
import com.aawhere.person.group.SocialGroupRepository;
import com.aawhere.person.group.SocialGroupService;
import com.aawhere.person.group.SocialGroupTestUtil;
import com.aawhere.queue.QueueFactory;
import com.aawhere.queue.QueueTestUtil;
import com.aawhere.search.GaeSearchService;
import com.aawhere.search.SearchService;
import com.aawhere.view.ViewCountObjectifyRepository;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author brian
 * 
 */
public class PersonServiceTestUtil {

	private PersonService personService;
	private PersonObjectifyRepository personRepository;
	private SocialGroupRepository groupRepository;
	private SocialGroupMembershipObjectifyRepository membershipRepository;
	private AccountObjectifyRepository accountRepository;
	private FieldDictionaryFactory dictionaryFactory;
	private SocialGroupMembershipService membershipService;
	private SocialGroupService groupService;
	private AccountService accountService;
	private QueueFactory queueFactory;
	private RegisteredEntitiesObjectifyFactory registeredEntities;

	public PersonService getPersonService() {
		return personService;
	}

	public PersonObjectifyRepository getRepository() {
		return personRepository;
	}

	/**
	 * @return the accountService
	 */
	public AccountService getAccountService() {
		return this.accountService;
	}

	/**
	 * @return the groupService
	 */
	public SocialGroupService groupService() {
		return this.groupService;
	}

	/**
	 * @return the dictinoaryFactory
	 */
	public FieldDictionaryFactory getDictionaryFactory() {
		return this.dictionaryFactory;
	}

	/**
	 * @return the personGroupService
	 */
	public SocialGroupMembershipService membershipService() {
		return this.membershipService;
	}

	public PersonObjectifyRepository getFilterRepository() {
		return this.personRepository;
	}

	/**
	 * @return the accountRepository
	 */
	public AccountObjectifyRepository getAccountRepository() {
		return this.accountRepository;
	}

	/**
	 * @return the queueFactory
	 */
	public QueueFactory getQueueFactory() {
		return this.queueFactory;
	}

	public com.aawhere.app.account.Account account() {
		return this.accountRepository.store(AccountUnitTest.create().getEntitySample());
	}

	public com.aawhere.app.account.Account createPersistedAccount(AccountId accountId) {
		return this.accountRepository.store(AccountUnitTest.create(accountId).getEntitySample());
	}

	/**
	 * Used to construct all instances of ActivityServiceTestUtil.
	 */
	public static class Builder
			extends ObjectBuilder<PersonServiceTestUtil> {

		FieldAnnotationDictionaryFactory annotationDictionaryFactory;

		public Builder() {
			super(new PersonServiceTestUtil());
			annotationDictionaryFactory = FieldTestUtil.getFieldAnnotationDictionaryFactory();
			register(LongMockEntityId.class);
			register(SocialGroupTestUtil.CONTEXT_ID_TYPE);
			// social group id is needed to be the parent of social group memebership and it's
			// generic in base entity
			register(SocialGroupId.class);
		}

		/**
		 * Provides a registration for {@link PersonServiceTestUtil#registeredEntities} to be used
		 * in RegisteredEntityIdentifierTranslatorFactory and such.
		 * 
		 * @param id
		 * @return
		 */
		public Builder register(Class<? extends Identifier<?, ?>> id) {

			if (building.registeredEntities == null) {
				building.registeredEntities = new RegisteredEntitiesObjectifyFactory();
			}
			building.registeredEntities.register(id);
			return this;
		}

		/**
		 * Used to register field keys during construction so the factory can be used during
		 * queries.
		 * 
		 * @return the annotationDictionaryFactory
		 */
		public FieldAnnotationDictionaryFactory getAnnotationDictionaryFactory() {
			return this.annotationDictionaryFactory;
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public PersonServiceTestUtil build() {
			// Setup those system resources that may need the registered
			// entities
			// FIXME:This belongs in a better place, but SocialGroupRepostory
			// needed it first and Person is early in the reactor
			if (building.registeredEntities != null) {
				IdentifierTranslators.add(ObjectifyService.factory(), building.registeredEntities);
			}
			annotationDictionaryFactory.getDictionary(Person.class);
			annotationDictionaryFactory.getDictionary(com.aawhere.app.account.Account.class);
			// these are only for testing purposes.
			annotationDictionaryFactory.getDictionary(LongMockEntity.class);
			annotationDictionaryFactory.getDictionary(StringMockEntity.class);
			building.dictionaryFactory = annotationDictionaryFactory.getFactory();
			SearchService searchService = new GaeSearchService(annotationDictionaryFactory,
					new DataTypeAdapterFactory(), Synchronization.SYNCHRONOUS);
			building.personRepository = new PersonObjectifyRepository(building.dictionaryFactory,
					Synchronization.SYNCHRONOUS);
			building.accountRepository = new AccountObjectifyRepository(building.dictionaryFactory,
					Synchronization.SYNCHRONOUS);
			building.groupRepository = new SocialGroupObjectifyRepository(building.dictionaryFactory,
					Synchronization.SYNCHRONOUS);
			building.membershipRepository = new SocialGroupMembershipObjectifyRepository(building.dictionaryFactory,
					Synchronization.SYNCHRONOUS);
			building.queueFactory = QueueTestUtil.getQueueFactory();
			building.accountService = new AccountService(building.accountRepository, building.queueFactory,
					building.dictionaryFactory);
			building.personService = new PersonService(building.personRepository, building.accountService,
					new ViewCountObjectifyRepository(), building.queueFactory);
			building.groupService = new SocialGroupService(building.groupRepository);
			building.membershipService = new SocialGroupMembershipService(building.groupService,
					building.personService, building.membershipRepository);
			return super.build();
		}

	}// end Builder

	/** Use {@link Builder} to construct ActivityServiceTestUtil */
	private PersonServiceTestUtil() {
	}

	public static Builder create() {
		return new Builder();
	}

	/**
	 * @return
	 */
	public Person persistedPerson() {
		Person person = PersonTestUtil.person();
		return persistedPerson(person);
	}

	/**
	 * @param person
	 * @return
	 */
	private Person persistedPerson(Person person) {
		return personRepository.store(person);
	}

	public Person persistedPersonWithName(String name) {
		return personRepository.store(PersonTestUtil.personWithName(name));
	}

	public Pair<Person, Account> persistedPersonWithNameAndAlias(String name, String alias) {
		Person person1 = persistedPersonWithName(name);
		Account account1 = createPersistedAccount(Iterables.getFirst(person1.accountIds(), null));
		try {
			account1 = getAccountRepository().aliasesAdded(account1.getId(), Sets.newHashSet(alias));
		} catch (EntityNotFoundException e) {
			throw new RuntimeException(e);
		}

		person1 = Person.mutate(person1).nameWithAliases(PersonUtil.personName(account1).getRight()).build();
		return Pair.of(person1, account1);
	}

	/**
	 * @return
	 */
	public SocialGroupRepository groupRepository() {
		return groupRepository;
	}

	public SocialGroupMembershipObjectifyRepository groupMembershipRepository() {
		return membershipRepository;
	}

	/**
	 * @return
	 */
	public SocialGroup persistedGroup() {
		return this.groupRepository.store(SocialGroupTestUtil.group());

	}

	/**
	 * @return
	 * 
	 */
	public Pair<Person, Account> persistedPersonWithPersistedAccount() {
		Account persistedAccount = account();
		return this.personService.person(persistedAccount);

	}

	/**
	 * @param accountWithName
	 * @return
	 */
	public Account account(Account accountDetails) {
		return this.accountService.accountDetailsUpdated(accountDetails);
	}

	/**
	 * @param accounts
	 *            one or more accounts that will be created and associated to a single person
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Person persistedPerson(Account... accounts) throws EntityNotFoundException {
		HashSet<Person> persons = Sets.newHashSet();
		for (Account account : accounts) {
			Pair<Person, Account> person = this.personService.person(account);
			persons.add(person.getKey());
		}
		return this.personService.merged(persons);
	}

	/**
	 * @param groupId
	 * @return
	 */
	public SocialGroupMembership socialGroupMembership(SocialGroupId groupId) {
		return socialGroupMembership(SocialGroupMembershipTestUtil.membership(groupId, persistedPerson().id()));
	}

	/**
	 * Creates the social group membership and a persisted person and group to go with it.
	 * 
	 * @return
	 */
	public SocialGroupMembership socialGroupMembership() {
		Person persistedPerson = persistedPerson();
		SocialGroup persistedGroup = persistedGroup();
		return socialGroupMembership(SocialGroupMembershipTestUtil
				.membership(persistedGroup.id(), persistedPerson.id()));
	}

	/** Creates a random persisted socialGroup. */
	public SocialGroup socialGroup() {
		return this.groupService.socialGroupPersisted(SocialGroupTestUtil.group());
	}

	public SocialGroupMembership socialGroupMembership(SocialGroupMembership membership) {
		return this.membershipRepository.store(membership);
	}

	public SocialGroupMembership socialGroupMembership(PersonId personId) {
		return socialGroupMembership(SocialGroupMembershipTestUtil.membership(persistedGroup().id(), personId));
	}
}