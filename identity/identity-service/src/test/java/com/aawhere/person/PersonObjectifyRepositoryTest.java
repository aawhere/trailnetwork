/**
 *
 */
package com.aawhere.person;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.account.AccountId;
import com.aawhere.persist.Filter;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;

import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Iterables;

/**
 * Repository tests for the {@link Person} entity
 * 
 * @author Brian Chapman
 * 
 */
public class PersonObjectifyRepositoryTest
		extends PersonUnitTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private PersonServiceTestUtil util;

	@Before
	public void setUpObjectify() throws Exception {
		helper.setUp();
	}

	@After
	public void tearDownObjectify() throws Exception {
		helper.tearDown();
	}

	/*
	 * @see com.aawhere.person.PersonUnitTest#createRepository()
	 */
	@Override
	protected PersonRepository createRepository() {
		this.util = PersonServiceTestUtil.create().build();
		return util.getRepository();
	}

	@Test
	public void testFindByAccount() {
		Person person = getPersisted();
		AccountId expected = Iterables.getFirst(person.accountIds(), null);
		Filter filter = PersonFilterBuilder.create().accountIds(expected).build();
		Persons persons = getRepository().filter(filter);
		TestUtil.assertSize(1, persons);
		Person foundPerson = Iterables.getFirst(persons, null);
		assertNotNull(foundPerson);
		TestUtil.assertContains(expected, foundPerson.accountIds());
	}

	@Test
	public void testFindByBothAccounts() {
		Person person = getPersisted();
		Set<AccountId> expected = person.accountIds();
		TestUtil.assertSize(2, expected);
		Filter filter = PersonFilterBuilder.create().accountIds(expected).build();
		Persons persons = getRepository().filter(filter);
		TestUtil.assertSize(1, persons);
		Person person1 = persons.getCollection().get(0);
		// make sure both are different
		assertEquals(expected, person1.accountIds());

	}

	@Test
	public void testFindOneAccountAmongstTwoPersons() {
		Person person1 = getPersisted();
		Person person2 = util.persistedPerson();
		Person expected = person2;
		TestUtil.assertNotEquals(person1.accountIds(), expected.accountIds());
		Filter filter = PersonFilterBuilder.create().accountIds(expected.accountIds()).build();
		Persons persons = getRepository().filter(filter);
		TestUtil.assertSize(1, persons);
		TestUtil.assertContains(expected, persons);

	}

	@Test
	public void testFidnByWrongAccount() {

	}

	public Person getPersisted() {
		return getRepository().store(getEntitySample());
	}
}
