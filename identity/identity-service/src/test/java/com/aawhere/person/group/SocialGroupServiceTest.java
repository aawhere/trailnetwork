/**
 * 
 */
package com.aawhere.person.group;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityServiceBaseTest;
import com.aawhere.persist.Filter;
import com.aawhere.person.PersonServiceTestUtil;
import com.aawhere.test.TestUtil;

/**
 * @author aroller
 * 
 */
public class SocialGroupServiceTest
		extends EntityServiceBaseTest<SocialGroupService, SocialGroups, SocialGroup, SocialGroupId> {

	private PersonServiceTestUtil testUtil;
	private SocialGroupService service;

	@Before
	public void setUp() {
		this.testUtil = PersonServiceTestUtil.create()
		// registering LMEI for use with SocialGroup#context
				.build();
		this.service = testUtil.groupService();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#service()
	 */
	@Override
	public SocialGroupService service() {
		return this.service;
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#entity()
	 */
	@Override
	public SocialGroup entity() {
		return SocialGroupTestUtil.group();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.EntityServiceBaseTest#id()
	 */
	@Override
	public SocialGroupId id() {
		return SocialGroupTestUtil.socialGroupId();
	}

	@Test
	public void testFindContextGroups() {
		SocialGroup group = this.testUtil.persistedGroup();
		SocialGroups groups = service.socialGroups(group.context(), Filter.create().build());
		TestUtil.assertContainsOnly(group, groups);
	}

	@Test
	public void testFindCategoryGroups() {
		SocialGroup group = this.testUtil.persistedGroup();
		SocialGroups groups = service.socialGroups(group.category(), Filter.create().build());
		TestUtil.assertContainsOnly(group, groups);
	}

	@Test
	public void testFindByCategoryContext() throws EntityNotFoundException {
		SocialGroup group = this.testUtil.persistedGroup();

		SocialGroup groupCreated = service.socialGroup(group.category(), group.context());
		assertEquals(	"didn't find by category/context" + group.category() + group.context(),
						groupCreated,
						service.socialGroup(groupCreated.category(), groupCreated.context()));
	}

	@Test
	public void testGroupPersisted() throws EntityNotFoundException {
		// identifier must represent an entity for this to work.
		SocialGroup group = SocialGroupTestUtil.group();
		assertNull(group.id());
		SocialGroup groupCreated = service.socialGroupPersisted(group);
		assertNotNull(groupCreated.id());
		SocialGroup found = service.socialGroup(groupCreated.id());

		assertEquals(groupCreated, found);
		Integer newScore = TestUtil.generateRandomInt();
		SocialGroup mutated = SocialGroup.mutate(groupCreated).score(newScore).build();
		SocialGroup groupUpdated = service.socialGroupPersisted(mutated);
		assertEquals("created two instead of finding", groupCreated, groupUpdated);
		assertEquals(newScore, groupUpdated.score());

	}

}
