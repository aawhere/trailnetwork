/**
 *
 */
package com.aawhere.person;

import static com.aawhere.lang.number.NumberUtilsExtended.*;
import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.account.Account;
import com.aawhere.app.account.AccountTestUtil;
import com.aawhere.app.account.EmailAddress;
import com.aawhere.id.IdentifierUtils;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.persist.EntityUtil;
import com.aawhere.persist.Filter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterOperator;
import com.aawhere.persist.GaePersistTestUtils;
import com.aawhere.test.TestUtil;
import com.aawhere.view.ViewCount;
import com.aawhere.view.ViewCountId;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author Brian Chapman
 * 
 */
public class PersonServiceTest {

	private LocalServiceTestHelper helper = GaePersistTestUtils.createRepositoryTestHelper();
	private PersonServiceTestUtil personTestUtil;
	private PersonService service;

	@Before
	public void setUpServices() {
		helper.setUp();
		this.personTestUtil = new PersonServiceTestUtil.Builder().build();
		this.service = personTestUtil.getPersonService();
	}

	@After
	public void tearDown() {
		helper.tearDown();
	}

	public void assertPersisted(Person person) throws EntityNotFoundException {
		Person result = personTestUtil.getPersonService().person(person.getId());
		assertNotNull(result);
		assertEquals(person, result);
	}

	@Test
	public void testForAccount() throws EntityNotFoundException {
		Account account = personTestUtil.account();
		final Pair<Person, Account> personAccount = service.person(account);
		assertEquals("wrong account created", account.id(), personAccount.getRight().id());
		assertNotSame("seriously, you just gave me back my object", account, personAccount.getRight());
		Person created = personAccount.getKey();

		// this confirms the account was actually created.
		Account foundAccount = this.personTestUtil.getAccountService().account(account.id());
		assertEquals(account.id(), foundAccount.id());

		TestUtil.assertContains(account.id(), created.accountIds());
		Person found = personAccount.getKey();
		assertEquals(created, found);
		Person foundById = service.person(created.id());
		assertEquals(created, foundById);

		// now test by id
		Person byAccountId = service.person(account.id());
		assertEquals("not finding by account id", byAccountId, created);
	}

	@Test
	public void testForAccountWithUpdate() throws EntityNotFoundException, MalformedURLException {

		URL photoUrl1 = new URL("http://example.com/1");
		URL photoUrl2 = new URL("http://example.com/2");
		Account account = Account.clone(personTestUtil.account()).photoUrl(photoUrl1).build();
		final Pair<Person, Account> personAccount = service.person(account);

		Account created = personAccount.getValue();
		Person updatedPerson = personAccount.getKey();

		assertAccountAndPersonUpdated(account, created, updatedPerson);

		Account modified = Account.clone(created).photoUrl(photoUrl2).build();

		final Pair<Person, Account> personAccountModified = service.person(modified);

		assertEquals(photoUrl2, personAccountModified.getValue().photoUrl());

		Account retreivedModifiedAccount = service.accountService().account(modified.getId());
		assertEquals(photoUrl2, retreivedModifiedAccount.photoUrl());
		assertAccountAndPersonUpdated(modified, personAccountModified.getValue(), personAccountModified.getKey());
	}

	private void assertAccountAndPersonUpdated(Account origionalAccount, Account createdAccount, Person updatedPerson) {
		assertEquals(origionalAccount.photoUrl(), createdAccount.photoUrl());
		assertEquals(origionalAccount.photoUrl(), updatedPerson.photoUrl());
		assertEquals(origionalAccount.email(), createdAccount.email());
		assertEquals(origionalAccount.email(), updatedPerson.email());
	}

	@Test
	public void testMerged() throws EntityNotFoundException {
		Person person1 = personTestUtil.persistedPersonWithPersistedAccount().getLeft();
		Person person2 = personTestUtil.persistedPersonWithPersistedAccount().getLeft();
		Person merged = this.service.merged(Lists.newArrayList(person1.id(), person2.id()));
		Person loser;
		if (merged.equals(person1)) {
			loser = person2;
		} else {
			loser = person1;
		}
		// refresh...don't trust what's returned
		merged = service.person(merged.id());

		TestUtil.assertContainsAll(loser.accountIds(), merged.accountIds());
		TestUtil.assertContains(loser.id(), merged.aliases());

		Person foundLoser = service.person(loser.id());
		assertEquals("service should find replacmement", merged, foundLoser);
		try {
			personTestUtil.getRepository().load(loser.id());
			fail("loser didn't get removed");
		} catch (EntityNotFoundException e) {
			// good, it should have been removed
		}
	}

	/**
	 * Even if an id is passed without it being foudn it should be added to the alias of the person
	 * being returned.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test
	public void testMergedOnePersonNotFound() throws EntityNotFoundException {
		Person person1 = personTestUtil.persistedPersonWithPersistedAccount().getLeft();
		Person missingPerson = personTestUtil.persistedPersonWithPersistedAccount().getLeft();
		personTestUtil.getRepository().delete(missingPerson);
		Person merged = this.service.merged(Sets.newHashSet(person1, missingPerson));
		TestUtil.assertContains(missingPerson.id(), merged.aliases());
	}

	/**
	 * Same as {@link #testMergedOnePersonNotFound()}, but uses the external method so the id is not
	 * trusted to be worth keeping around.
	 * 
	 * @throws EntityNotFoundException
	 */
	@Test(expected = EntityNotFoundException.class)
	public void testMergedOnePersonNotFoundFromIds() throws EntityNotFoundException {
		Person person1 = personTestUtil.persistedPerson();
		PersonId missingPerson = PersonTestUtil.createPersonId();
		this.service.merged(Sets.newHashSet(person1.id(), missingPerson));
	}

	@Test(expected = EntityNotFoundException.class)
	public void testMergedTwoPersonNotFoundFromIds() throws EntityNotFoundException {
		this.service.merged(Sets.newHashSet(PersonTestUtil.createPersonId(), PersonTestUtil.createPersonId()));
	}

	@Test
	public void testUpdated() throws EntityNotFoundException {
		Account account1 = personTestUtil.account();
		Person person = service.person(account1).getKey();
		assertEquals(account1.name(), person.name());
		String secondName = AccountTestUtil.accountName();
		Account updatedAccount = personTestUtil.getAccountService().accountNameUpdated(account1.id(), secondName);
		assertEquals(secondName, updatedAccount.name());
		Person updated = service.updated(person.id());
		assertEquals(secondName, updated.name());
	}

	@Test
	public void testAccountUpdatedExistingHasBetterName() throws EntityNotFoundException {
		Account accountWithWorseName = this.personTestUtil.account(AccountTestUtil
				.accountWithName(PersonTestUtil.NAME_OK));
		final String expectedName = PersonTestUtil.NAME_BETTER;
		Account accountWithBetterName = this.personTestUtil.account(AccountTestUtil.accountWithName(expectedName));

		Person createdPerson = this.personTestUtil.persistedPerson(accountWithBetterName, accountWithWorseName);
		assertEquals("created person didn't get better name", expectedName, createdPerson.name());
		// name is good, but other's is still better
		Pair<Person, Account> personUpdated = this.service.person(Account.clone(accountWithWorseName)
				.setName(PersonTestUtil.NAME_GOOD).build());
		assertEquals("better name lost during update", expectedName, personUpdated.getKey().name());
	}

	@Test
	public void testUpdatedAll() {
		Person person1 = personTestUtil.persistedPerson();
		Filter filter = PersonFilterBuilder.create().idEqualTo(person1.id()).build();
		Integer updated = this.service.updated(filter);
		assertEquals(I(1), updated);
	}

	@Test
	public void testMergedTwoPersonsSameAliasDuringLoad() throws EntityNotFoundException {
		Person person1 = personTestUtil.persistedPersonWithPersistedAccount().getLeft();
		Person person2 = personTestUtil.persistedPersonWithPersistedAccount().getLeft();
		// assign a common alias to both.
		PersonId alias = PersonTestUtil.createPersonId();
		{
			Set<PersonId> aliases = Sets.newHashSet(alias);
			personTestUtil.getRepository().aliasesAdded(person1.id(), aliases);
			personTestUtil.getRepository().aliasesAdded(person2.id(), aliases);
		}
		// now search for that alias
		Person winner = service.person(alias);
		TestUtil.assertNotEquals(alias, winner.id());
		TestUtil.assertContainsAll(winner.aliases(), Sets.newHashSet(person1.id(), person2.id(), alias));
		Person loser = (winner.equals(person1)) ? person2 : person1;
		assertEquals("searching for loser should give winner", winner, service.person(loser.id()));
		try {
			personTestUtil.getRepository().load(loser.id());
			fail("loser didn't get deleted");
		} catch (EntityNotFoundException e) {
			// expected
		}
	}

	@Test
	public void testMergedTwoPersonsSameAccountsDuringLoad() throws EntityNotFoundException {
		Person person1 = personTestUtil.persistedPerson();
		Person person2 = personTestUtil.persistedPerson();
		Account account = personTestUtil.account();
		{
			person1 = personTestUtil.getRepository().update(Person.mutate(person1).account(account).build());
			person2 = personTestUtil.getRepository().update(Person.mutate(person2).account(account).build());
		}

		Person winner = service.person(account).getKey();
		Person loser = (winner.equals(person1)) ? person2 : person1;
		assertEquals("loser should be an alias of winner", winner, service.person(loser.id()));
		try {
			personTestUtil.getRepository().load(loser.id());
			fail("loser should be gone");
		} catch (EntityNotFoundException e) {
			// good.
		}

		// don't trust what is given, reload before tests
		Person winnerFound = service.person(winner.id());
		assertEquals(winner, winnerFound);
		assertEquals(winner, service.person(account).getKey());
	}

	@Test
	public void testPersonsByIds() throws EntityNotFoundException {
		Person person1 = this.personTestUtil.persistedPerson();
		Person person2 = this.personTestUtil.persistedPerson();
		final Iterable<PersonId> expected = IdentifierUtils.ids(person1, person2);
		Persons persons = this.service.persons(expected);
		TestUtil.assertIterablesEquals("didn't find existing person", expected, IdentifierUtils.ids(persons));
	}

	@Test(expected = EntityNotFoundException.class)
	public void testPersonsByIdsNotFound() throws EntityNotFoundException {
		Person person1 = this.personTestUtil.persistedPerson();
		final Iterable<PersonId> expected = Lists.newArrayList(person1.id(), PersonTestUtil.createPersonId());
		this.service.persons(expected);
	}

	@Test
	public void testPersonNameSharedUpdate() throws EntityNotFoundException {
		Person person = this.personTestUtil.persistedPerson();

		// expect the opposite.
		final boolean expected = !person.identityShared();
		Filter filter = PersonFilterBuilder.create().nameShared(expected).build();
		Person updated = this.service.updated(person.id(), filter);
		assertEquals("response didn't get updated", expected, updated.identityShared());
		assertEquals("name shared didn't persist", expected, service.person(person.id()).identityShared());
	}

	@Test
	public void testPersonNameUpdate() throws EntityNotFoundException {
		Person person = this.personTestUtil.persistedPerson();

		// expect the opposite.
		final String expected = person.getName() + " Peters";
		Filter filter = PersonFilterBuilder
				.create()
				.addCondition(FilterCondition.create().field(PersonField.Key.NAME).operator(FilterOperator.EQUALS)
						.value(expected).build()).build();
		Person updated = this.service.updated(person.id(), filter);
		assertEquals("response didn't get updated", expected, updated.getName());
		assertEquals("name didn't persist", expected, service.person(person.id()).name());
	}

	@Test
	public void testPersonEmailUpdate() throws EntityNotFoundException {
		Person person = this.personTestUtil.persistedPerson();

		// expect the opposite.
		final EmailAddress expected = AccountTestUtil.emailAddress();
		Filter filter = PersonFilterBuilder
				.create()
				.addCondition(FilterCondition.create().field(PersonField.Key.EMAIL).operator(FilterOperator.EQUALS)
						.value(expected).build()).build();
		Person updated = this.service.updated(person.id(), filter);
		assertEquals("response didn't get updated", expected, updated.email());
		assertEquals("name didn't persist", expected, service.person(person.id()).email());
	}

	@Test
	public void testViewed() {
		PersonId personId = PersonTestUtil.createPersonId();
		ViewCount viewCountBeforeView = this.service.viewCount(personId);
		assertFalse("shouldn't be persisted", EntityUtil.isPersisted(viewCountBeforeView));
		assertEquals(I(0), viewCountBeforeView.count());
		this.service.viewed(personId);
		ViewCount viewCountAfterViewing = this.service.viewCount(personId);
		assertEquals(I(1), viewCountAfterViewing.count());
		assertEquals(new ViewCountId(personId), viewCountAfterViewing.id());
	}
}
