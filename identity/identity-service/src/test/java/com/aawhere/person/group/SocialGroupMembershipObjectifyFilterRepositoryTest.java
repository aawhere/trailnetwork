/**
 * 
 */
package com.aawhere.person.group;

import com.aawhere.persist.BaseObjectifyFilterRepositoryTest;
import com.aawhere.person.PersonServiceTestUtil;

/**
 * @author aroller
 * 
 */
public class SocialGroupMembershipObjectifyFilterRepositoryTest
		extends
		BaseObjectifyFilterRepositoryTest<SocialGroupMembershipId, SocialGroupMembership, SocialGroupMembershipObjectifyRepository, SocialGroupMembershipRepository, SocialGroupMemberships, SocialGroupMemberships.Builder> {

	private SocialGroupMembershipObjectifyRepository repository = PersonServiceTestUtil.create().build()
			.groupMembershipRepository();

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#createEntity()
	 */
	@Override
	protected SocialGroupMembership createEntity() {
		return SocialGroupMembershipTestUtil.membership();
	}

	/*
	 * (non-Javadoc)
	 * @see com.aawhere.persist.BaseObjectifyFilterRepositoryTest#getRepository()
	 */
	@Override
	protected SocialGroupMembershipRepository getRepository() {
		return repository;
	}

}
