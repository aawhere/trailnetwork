package com.aawhere.identity;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;

import com.aawhere.app.account.AccountId;
import com.aawhere.persist.EntityNotFoundException;
import com.aawhere.person.Person;
import com.aawhere.person.PersonService;
import com.aawhere.person.PersonTestUtil;
import com.aawhere.person.pref.Preferences;
import com.aawhere.person.pref.UnitOfMeasure;

/**
 * Unit test for {@link IdentityManagerProvider}
 * 
 * @author Brian Chapman
 * 
 */
public class IdentityManagerProviderUnitTest {

	private AccountId accountId;
	private Person person;
	private Locale locale;
	private UnitOfMeasure uom;
	private HttpServletRequest req;
	private PersonService personService;
	private final String EXPECTED_ELEVATION_METRIC_UNIT = "m";
	private final String EXPECTED_ELEVATION_US_UNIT = "ft";

	@Before
	public void setup() {
		accountId = new AccountId("test-1234");
		person = PersonTestUtil.person(accountId);
		locale = Locale.GERMAN;
		uom = UnitOfMeasure.US_CUSTOMARY;
		req = mock(HttpServletRequest.class);
		personService = mock(PersonService.class);
	}

	@Test
	public void testProviderLocaleOnly() throws EntityNotFoundException {
		IdentityManagerProvider provider = createIdentityManagerProvider(null, null, locale);
		assertIdentityManagerProvider(provider, locale, EXPECTED_ELEVATION_METRIC_UNIT);
	}

	@Test
	public void testProviderUomAndLocale() throws EntityNotFoundException {
		IdentityManagerProvider provider = createIdentityManagerProvider(null, uom, locale);
		assertIdentityManagerProvider(provider, locale, EXPECTED_ELEVATION_US_UNIT);
	}

	@Test
	public void testProviderAll() throws EntityNotFoundException {
		IdentityManagerProvider provider = createIdentityManagerProvider(person, uom, locale);
		assertIdentityManagerProvider(provider, locale, EXPECTED_ELEVATION_US_UNIT);
	}

	private IdentityManagerProvider createIdentityManagerProvider(Person person, UnitOfMeasure uom, Locale locale)
			throws EntityNotFoundException {
		if (person != null) {
			when(req.getAttribute(IdentityManagerProvider.ACCOUNT_ID_HEADER_NAME)).thenReturn(accountId.toString());
			when(personService.person(accountId)).thenReturn(person);
		} else {
			when(req.getAttribute(IdentityManagerProvider.ACCOUNT_ID_HEADER_NAME)).thenReturn(null);
			when(personService.person(accountId)).thenReturn(null);
		}

		if (uom != null) {
			when(req.getHeader(IdentityManagerProvider.UNIT_OF_MEASURE_HEADER_NAME)).thenReturn(uom.toString());
		} else {
			when(req.getHeader(IdentityManagerProvider.UNIT_OF_MEASURE_HEADER_NAME)).thenReturn(null);
		}

		if (locale != null) {
			when(req.getLocale()).thenReturn(locale);
		} else {
			throw new IllegalArgumentException("locale cannot be null");
		}

		return new IdentityManagerProvider(req, personService, IdentityTestUtil.identityFactory(),
				new AccessScopeValidatorFactory());
	}

	private void assertIdentityManagerProvider(IdentityManagerProvider provider, Locale expectedLocale,
			String expectedElevationUnit) throws EntityNotFoundException {
		IdentityManager manager = provider.get();

		verify(req).getHeader(IdentityManagerProvider.UNIT_OF_MEASURE_HEADER_NAME);
		if (manager.person() != null) {
			verify(personService).person(accountId);
		}
		assertNotNull(manager);

		Configuration prefs = manager.preferences();
		assertEquals(expectedLocale.toString(), prefs.getProperty(Preferences.LOCALE.getKey()));
		assertEquals(expectedElevationUnit, prefs.getProperty(Preferences.ELEVATION_UNIT.getKey()));
	}
}
