/**
 *
 */
package com.aawhere.web.api;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.apache.commons.collections.CollectionUtils;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.CollectionStringsAdapter;
import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringAdapterFactory;

import com.google.inject.Inject;
import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.core.spi.component.ComponentScope;

/**
 * Using the {@link StringAdapterFactory} to provide {@link StringAdapter}s this will convert a
 * collection of query paramter strings into corresponding objects and add them to a collection for
 * the result.
 * 
 * @author aroller
 * 
 */
@Provider
public class CollectionStringAdapterQueryParamInjector<C extends Collection<T>, T>
		extends ExceptionMappingQueryParamInjector<C> {

	private com.google.inject.Provider<UriInfo> uriInfo;
	private StringAdapterFactory stringAdapterFactory;

	/**
	 *
	 */
	@Inject
	public CollectionStringAdapterQueryParamInjector(com.google.inject.Provider<UriInfo> uriInfo,
			StringAdapterFactory adapterFactory) {
		this.uriInfo = uriInfo;
		// TODO:inject this.
		this.stringAdapterFactory = adapterFactory;

	}

	/*
	 * (non-Javadoc)
	 * @see com.sun.jersey.spi.inject.InjectableProvider#getScope()
	 */
	@Override
	public ComponentScope getScope() {
		return ComponentScope.PerRequest;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.web.api.ExceptionMappingQueryParamInjector#makeInjectable(com.sun.jersey.core
	 * .spi.component.ComponentContext, javax.ws.rs.QueryParam, java.lang.reflect.Type)
	 */
	@Override
	protected C obtainValue(ComponentContext ic, final QueryParam a, Type c) throws BaseException {
		CollectionStringsAdapter<C, T> adapter = getAdaptor(c);

		final List<String> values = uriInfo.get().getQueryParameters().get(a.value());
		if (!CollectionUtils.isEmpty(values)) {
			return adapter.marshal(values);
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.web.api.ExceptionMappingQueryParamInjector#getAdaptor(com.sun.jersey.core.spi
	 * .component.ComponentContext, javax.ws.rs.QueryParam, java.lang.reflect.Type)
	 */
	@Override
	protected boolean handlesType(ComponentContext ic, QueryParam a, Type c) {
		return getAdaptor(c).handles();
	}

	private CollectionStringsAdapter<C, T> getAdaptor(Type c) {
		return new CollectionStringsAdapter.Builder<C, T>().setFactory(stringAdapterFactory).setType(c).build();
	}
}
