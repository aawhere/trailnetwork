/**
 * 
 */
package com.aawhere.web.api.client;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import com.aawhere.lang.ObjectBuilder;
import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.web.api.XmlTypeElementWrapperProvider;
import com.aawhere.web.api.client.WebApiClientHelper.Builder.OutputType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;

/**
 * Useful utilities to assist using generated clients.
 * 
 * @author Aaron Roller
 * 
 */
public class WebApiClientHelper {

	/**
	 * Used to construct all instances of WebApiClientHelper.
	 */
	public static class Builder
			extends ObjectBuilder<WebApiClientHelper> {

		public static enum OutputType {
			/**
			 * Outputs the logging results to be available of client execution via
			 * {@link WebApiClientHelper#getLoggingOutput()}
			 */
			STRING,
			/** Outputs the logging to {@link System#out}. */
			SYSTEM_OUT
		};

		private OutputType logOutput;

		public Builder() {
			super(new WebApiClientHelper());
		}

		/*
		 * (non-Javadoc)
		 * @see com.aawhere.lang.ObjectBuilder#build()
		 */
		@Override
		public WebApiClientHelper build() {
			// this can be made optional if you don't like it always, but make it default?
			ClientConfig config = new DefaultClientConfig(XmlTypeElementWrapperProvider.App.class,
					XmlTypeElementWrapperProvider.Text.class);

			building.client = Client.create(config);
			if (this.logOutput != null) {

				PrintStream out;
				switch (this.logOutput) {
					case STRING:
						building.output = new ByteArrayOutputStream();
						out = new PrintStream(building.output);
						break;
					case SYSTEM_OUT:
						out = System.out;
						break;
					default:
						throw new InvalidChoiceException(this.logOutput);
				}
				building.client.addFilter(new LoggingFilter(out));
			}
			return super.build();
		}

		public Builder setLogOutput(OutputType logOutput) {
			this.logOutput = logOutput;
			return this;
		}

	}// end Builder

	private Client client;

	/**
	 * the recipient of the logging statements when {@link OutputType#STRING} is chosen.
	 * 
	 */
	public ByteArrayOutputStream output;

	/** Use {@link Builder} to construct WebApiClientHelper */
	private WebApiClientHelper() {
	}

	/**
	 * @return the client
	 */
	public Client getClient() {
		return this.client;
	}

	/**
	 * The results of the logging output when {@link OutputType#STRING} is chosen.
	 * 
	 * @return
	 */
	public String getLoggingOutput() {
		String result;
		if (this.output != null) {
			// this could be more efficient if we checked for dirty, but this is most complete
			result = new String(output.toByteArray());
		} else {
			result = "";
		}
		return result;
	}

}
