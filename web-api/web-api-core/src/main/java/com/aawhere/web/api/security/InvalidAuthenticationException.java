package com.aawhere.web.api.security;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.StatusCode;
import com.aawhere.net.HttpStatusCode;

import com.google.appengine.api.oauth.OAuthRequestException;

/**
 * @author brian
 * 
 */
@StatusCode(value = HttpStatusCode.UNAUTHORIZED, message = "Unable to authenticate user: {DETAILS}")
public class InvalidAuthenticationException
		extends BaseRuntimeException {

	// Consider removing this and using the generic response instead for security reasons.
	// Additional details could be logged to
	// the logs for debugging.
	public InvalidAuthenticationException(OAuthRequestException cause) {
		super(InvalidAuthenticationExceptionMessage.message(cause.getMessage()), cause);

	}

	public InvalidAuthenticationException() {
		// Purposely not providing details about the exception for security reasons.
		super(InvalidAuthenticationExceptionMessage.message(""));

	}

	private static final long serialVersionUID = 1L;

}