/**
 * 
 */
package com.aawhere.web.api;

import com.aawhere.text.format.custom.CustomFormat;
import com.aawhere.util.rb.Message;
import com.aawhere.util.rb.ParamKey;

/**
 * @author Aaron Roller
 * 
 */
public enum WebApiMessage implements Message {

	/** Explain_Entry_Here */
	FORMAT_NOT_SUPPORTED(
			"{FORMAT} was requested, but only {SUPPORTED_FORMATS} are supported.",
			Param.FORMAT,
			Param.SUPPORTED_FORMATS);

	private ParamKey[] parameterKeys;
	private String message;

	private WebApiMessage(String message, ParamKey... parameters) {
		this.message = message;
		this.parameterKeys = parameters;
	}

	@Override
	public String getValue() {
		return this.message;
	}

	@Override
	public ParamKey[] getParameterKeys() {
		return this.parameterKeys;
	}

	public enum Param implements ParamKey {
		/** Describes the web api format given like xml or json. */
		FORMAT,
		/** Describes all the formats that are supported by the API. */
		SUPPORTED_FORMATS;

		CustomFormat<?> format;

		private Param() {
		}

		private Param(CustomFormat<?> format) {
			this.format = format;
		}

		@Override
		public CustomFormat<?> getFormat() {
			return this.format;
		}
	}

}
