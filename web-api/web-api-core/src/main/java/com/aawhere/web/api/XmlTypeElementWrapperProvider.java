/**
 * 
 */
package com.aawhere.web.api;

import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Providers;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;

import com.aawhere.xml.bind.JAXBElementFactory;
import com.aawhere.xml.bind.NamespaceResolver;

import com.sun.jersey.core.provider.jaxb.AbstractRootElementProvider;

/**
 * Provides the capbility to write a class that has an {@link XmlType} annotation, but not an
 * {@link XmlRootElement} annotation by wrapping with a {@link JAXBElement} using
 * {@link JAXBElementFactory}.
 * 
 * 
 * @author Aaron Roller
 * 
 */
abstract public class XmlTypeElementWrapperProvider
		extends AbstractRootElementProvider {

	/**
	 * @param ps
	 * @param mt
	 */
	private XmlTypeElementWrapperProvider(Providers ps, MediaType mt) {
		super(ps, mt);
	}

	/**
	 * @param ps
	 */
	private XmlTypeElementWrapperProvider(Providers ps) {
		super(ps);

	}

	@Produces("application/xml")
	@Consumes("application/xml")
	public static final class App
			extends XmlTypeElementWrapperProvider {

		public App(@Context Providers ps) {
			super(ps, MediaType.APPLICATION_XML_TYPE);
		}
	}

	@Produces("text/xml")
	@Consumes("text/xml")
	public static final class Text
			extends XmlTypeElementWrapperProvider {

		public Text(@Context Providers ps) {
			super(ps, MediaType.TEXT_XML_TYPE);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.sun.jersey.core.provider.jaxb.AbstractRootElementProvider#isReadable(java.lang.Class,
	 * java.lang.reflect.Type, java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType)
	 */
	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {

		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.sun.jersey.core.provider.jaxb.AbstractRootElementProvider#isWriteable(java.lang.Class,
	 * java.lang.reflect.Type, java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType)
	 */
	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {

		return JAXBElementFactory.isWrapWorthy(type) && isSupported(mediaType);
	}

	/*
	 * (non-Javadoc)
	 * @see com.sun.jersey.core.provider.jaxb.AbstractRootElementProvider#writeTo(java.lang.Object,
	 * javax.ws.rs.core.MediaType, java.nio.charset.Charset, javax.xml.bind.Marshaller,
	 * java.io.OutputStream)
	 */
	@Override
	protected void writeTo(Object t, MediaType mediaType, Charset c, Marshaller m, OutputStream entityStream)
			throws JAXBException {
		NamespaceResolver resolver = new NamespaceResolver.Builder().setTargetClass(t.getClass()).build();
		if (!resolver.isNamespaceAvailable()) {
			throw new IllegalStateException("unable to find a declared namespace anywhere for " + t.getClass());
		}
		JAXBElementFactory factory = new JAXBElementFactory.Builder().setNamespace(resolver.getNamespace()).build();
		t = factory.createElement(t);
		super.writeTo(t, mediaType, c, m, entityStream);
	}
}
