/**
 * 
 */
package com.aawhere.web.api;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.util.rb.CompleteMessage;

/**
 * Used to indicate there was a request for a media type that is not yet supported.
 * 
 * @author Aaron Roller
 * 
 */
public class UnsupportedApiFormatException
		extends BaseRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5689825511451722090L;

	/**
	 * 
	 */
	public UnsupportedApiFormatException(String format, Object... supportedFormats) {
		super(new CompleteMessage.Builder(WebApiMessage.FORMAT_NOT_SUPPORTED)
				.addParameter(WebApiMessage.Param.FORMAT, format)
				.addParameter(WebApiMessage.Param.SUPPORTED_FORMATS, supportedFormats).build());
	}

	@Provider
	public static class Mapper
			implements ExceptionMapper<UnsupportedApiFormatException> {

		public Mapper() {
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
		 */
		@Override
		public Response toResponse(UnsupportedApiFormatException exception) {
			return Response.status(Status.UNSUPPORTED_MEDIA_TYPE).entity(exception.getMessage())
					.type(MediaType.TEXT_PLAIN).build();
		}
	}
}
