/**
 *
 */
package com.aawhere.web.api;

import java.lang.reflect.Type;
import java.util.List;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.apache.commons.collections.CollectionUtils;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.string.StringAdapter;
import com.aawhere.lang.string.StringAdapterFactory;

import com.google.inject.Inject;
import com.sun.jersey.core.spi.component.ComponentContext;

/**
 * This adapts a string to an object using the {@link StringAdapterFactory} to provide
 * {@link StringAdapter}s that do the conversion.
 * 
 * @see CollectionStringAdapterQueryParamInjector for handling of collections
 * @author aroller
 * 
 */
@Provider
public class StringAdapterQueryParamInjector<T>
		extends ExceptionMappingQueryParamInjector<T> {

	private com.google.inject.Provider<UriInfo> uriInfo;
	private StringAdapterFactory stringAdapterFactory;

	/**
	 *
	 */
	@Inject
	public StringAdapterQueryParamInjector(com.google.inject.Provider<UriInfo> uriInfo,
			StringAdapterFactory adapterFactory) {
		this.uriInfo = uriInfo;
		this.stringAdapterFactory = adapterFactory;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.web.api.ExceptionMappingQueryParamInjector#obtainValue(com.sun.jersey.core.spi
	 * .component.ComponentContext, javax.ws.rs.QueryParam, java.lang.reflect.Type)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T obtainValue(ComponentContext ic, QueryParam a, Type c) throws BaseException {
		StringAdapter<T> adapter = stringAdapterFactory.getAdapter(c);
		final List<String> values = uriInfo.get().getQueryParameters().get(a.value());
		if (!CollectionUtils.isEmpty(values)) {

			if (values.size() > 1) {
				throw new RuntimeException(values.toString()
						+ " providing multiple values doesn't work for this field of " + c);
			}
			return adapter.unmarshal(values.get(0), (Class<T>) c);

		} else {
			// this is required for all...they pass a null we pass it through
			return null;
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.aawhere.web.api.ExceptionMappingQueryParamInjector#handlesType(com.sun.jersey.core.spi
	 * .component.ComponentContext, javax.ws.rs.QueryParam, java.lang.reflect.Type)
	 */
	@Override
	protected boolean handlesType(ComponentContext ic, QueryParam a, Type c) {
		return stringAdapterFactory.handles(c);
	}
}
