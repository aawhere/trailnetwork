/**
 * 
 */
package com.aawhere.web.api;

import javax.annotation.Nonnull;
import javax.ws.rs.core.MediaType;

/**
 * @author Aaron Roller
 * 
 */
public enum WebApiFormat {

	XML(MediaType.APPLICATION_XML_TYPE), JSON(MediaType.APPLICATION_JSON_TYPE);

	@Nonnull
	public static WebApiFormat find(String format) throws UnsupportedApiFormatException {

		try {
			return WebApiFormat.valueOf(format.toUpperCase());
		} catch (IllegalArgumentException e) {
			// I'm not really supposed to catch runtimes, but they leave me no choice?
			throw new UnsupportedApiFormatException(format, (Object[]) WebApiFormat.values());
		}

	}

	private MediaType mediaType;

	/**
	 * 
	 */
	private WebApiFormat(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	/**
	 * @return the mediaType
	 */
	public MediaType getMediaType() {
		return this.mediaType;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return name().toLowerCase();
	}
}
