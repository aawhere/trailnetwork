/**
 *
 */
package com.aawhere.web.api;

import java.lang.reflect.Type;

import javax.ws.rs.QueryParam;

import com.aawhere.lang.exception.BaseException;

import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.core.spi.component.ComponentScope;
import com.sun.jersey.spi.inject.Injectable;
import com.sun.jersey.spi.inject.InjectableProvider;

/**
 * This adapts the Injectable Provider to handle mapping exceptions to a response code. Mapping the
 * BaseException needs to be handled here because the {@link BaseExceptionMapper} doesn't get
 * invoked at this stage in the request. @See
 * http://stackoverflow.com/questions/10682249/what-comes-
 * first-injectableproviders-or-exceptionmappers
 * 
 * @author Brian Chapman
 * 
 */
public abstract class ExceptionMappingQueryParamInjector<T>
		implements InjectableProvider<QueryParam, Type> {

	public ExceptionMappingQueryParamInjector() {
	}

	/*
	 * (non-Javadoc)
	 * @see com.sun.jersey.spi.inject.InjectableProvider#getScope()
	 */
	@Override
	public ComponentScope getScope() {
		return ComponentScope.PerRequest;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.sun.jersey.spi.inject.InjectableProvider#getInjectable(com.sun.jersey.core.spi.component
	 * .ComponentContext, java.lang.annotation.Annotation, java.lang.Object)
	 */
	@Override
	public Injectable<T> getInjectable(final ComponentContext ic, final QueryParam a, final Type c) {
		Injectable<T> result;

		if (handlesType(ic, a, c)) {

			result = new Injectable<T>() {

				@Override
				public T getValue() {
					try {
						return obtainValue(ic, a, c);
					} catch (Exception e) {
						throw new BaseExceptionMapper().asWebApplicationException(e);
					}
				}

			};
		} else {
			result = null;
		}

		return result;

	}

	protected abstract T obtainValue(ComponentContext ic, final QueryParam a, final Type c) throws BaseException;

	protected abstract boolean handlesType(final ComponentContext ic, final QueryParam a, final Type c);

}
