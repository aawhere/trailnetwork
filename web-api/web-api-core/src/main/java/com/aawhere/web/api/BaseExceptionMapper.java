/**
 *
 */
package com.aawhere.web.api;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.aawhere.lang.exception.BaseException;
import com.aawhere.lang.exception.BaseRuntimeException;
import com.aawhere.lang.exception.MessageException;
import com.aawhere.log.LoggerFactory;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.ws.ErrorResponse;

/**
 * A general Mapper for all {@link BaseException}s, {@link BaseRuntimeException}s and
 * {@link Exception}s in general thrown into the web API. It always returns JSON of
 * {@link ErrorResponse}.
 * 
 * @author Aaron Roller
 * 
 */
@Provider
public class BaseExceptionMapper
		implements ExceptionMapper<Exception> {

	private static Logger logger = LoggerFactory.getLogger(BaseExceptionMapper.class);

	static Response makeResponse(Exception exception) {

		Exception wrappedException = exception;
		// during parameter injection the container wraps our exceptions in a web application
		// exception...check to see if the cause is better than the given for any wrapper...no
		// matter how deep
		while (!(wrappedException instanceof MessageException) && wrappedException.getCause() != null) {
			// the root cause is typically better to report than the wrappers.
			wrappedException = (Exception) wrappedException.getCause();
		}
		// replace exception if a better one is found
		if (exception != wrappedException && wrappedException instanceof MessageException) {
			exception = wrappedException;
		}

		ErrorResponse errorResponse = ErrorResponse.create().exception(exception).build();
		if (exception instanceof WebApplicationException) {
			errorResponse = ErrorResponse
					.create()
					.statusCode(HttpStatusCode.valueOf(((WebApplicationException) exception).getResponse().getStatus()))
					.build();
		}

		if (errorResponse.statusCode().isServerError()) {
			logger.log(Level.SEVERE, errorResponse.message(), exception);
		}

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, errorResponse.message(), exception);
		}

		return Response.status(errorResponse.statusCode().value).entity(errorResponse).type(MediaType.APPLICATION_JSON)
				.build();
	}

	static WebApplicationException makeWebApplicationException(Exception exception) {
		return new WebApplicationException(exception, makeResponse(exception));
	}

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
	 */
	@Override
	public Response toResponse(Exception exception) {

		return makeResponse(exception);
	}

	public WebApplicationException asWebApplicationException(Exception exception) {
		return makeWebApplicationException(exception);
	}

}
