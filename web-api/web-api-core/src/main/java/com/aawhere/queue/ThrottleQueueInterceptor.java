/**
 * 
 */
package com.aawhere.queue;

import java.lang.reflect.Method;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormat;

import com.aawhere.log.LoggerFactory;
import com.aawhere.ws.rs.SystemWebApiUri;

import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Method Interceptor for @ThrottleQueue
 * 
 * TODO: move this to the queue Commons now that it is decoupled from our application
 * 
 * @author Brian Chapman
 * 
 */
@Singleton
public class ThrottleQueueInterceptor
		implements MethodInterceptor {

	protected static final Level LOG_LEVEL = Level.FINE;
	protected static final Logger LOG = LoggerFactory.getLogger(ThrottleQueueInterceptor.class);

	private QueueFactory queueFactory;
	// A time in thousands of a second that represents how long a queue can get backed up before we
	// stop crawling.
	// 600000 = 10 min.
	private static final Long QUEUE_ETA_BUFFER = 600000l;
	/**
	 * A number representing the maximum number length a queue can get before we stop crawling. 99
	 * is used because of misreporting of large sized queues as 100.
	 */
	static final int QUEUE_MAX_QUEUE_LENGTH_FOR_CRAWL = 99;
	/** A Constant expression indicating this will consider all queues known to the factory. */
	static final String[] ALL_QUEUES = {};

	public ThrottleQueueInterceptor() {
	}

	@Inject
	public ThrottleQueueInterceptor(QueueFactory queueFactory) {
		this.queueFactory = queueFactory;
	}

	@Inject
	public void setQueueFactory(QueueFactory queueFactory) {
		this.queueFactory = queueFactory;
	}

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		checkQueueStatus(invocation);
		return invocation.proceed();
	}

	/**
	 * Checks the state of various queues and determines if this queue should run.
	 * 
	 */
	private void checkQueueStatus(MethodInvocation invocation) {
		Method method = invocation.getMethod();
		ThrottleQueue annotation = method.getAnnotation(ThrottleQueue.class);
		Set<String> queueNames = Sets.newHashSet(annotation.queues());
		int maxSize = annotation.maxSize();
		String[] ignores = annotation.ignore();
		throttle(queueNames, maxSize, Sets.newHashSet(ignores));
	}

	/**
	 * Does the actual work without the injection requirements allowing for testing.
	 * 
	 * @param queueNames
	 * @param maxSize
	 * @param ignores
	 *            those queues to ignore
	 */
	void throttle(Set<String> queueNames, int maxSize, final Set<String> ignores) {
		// not exactly referencing #ALL_QUEUES, but works better as long as it remains empty set.
		if (queueNames.isEmpty()) {
			queueNames = this.queueFactory.getKnownQueueNames();
		}
		// remove the ignores
		queueNames = Sets.difference(queueNames, ignores);
		for (String queueName : queueNames) {
			Queue queue = queueFactory.getQueue(queueName);
			DateTime eta = queue.lastTaskEta();
			Integer size = queue.size();
			DateTime queueTooLongIfAfter = DateTime.now().plus(QUEUE_ETA_BUFFER);
			if (LOG.isLoggable(LOG_LEVEL)) {
				DateTime now = new DateTime();
				Period period = new Period(now, eta);

				LOG.log(LOG_LEVEL, "Queue " + queueName + " lastEtaTask: " + PeriodFormat.getDefault().print(period)
						+ " queue size: " + size);
			}
			if (queueTooLongIfAfter.isBefore(eta.getMillis()) || queueTooLongIfAfter.equals(eta) || size > maxSize) {
				if (LOG.isLoggable(LOG_LEVEL)) {
					LOG.log(LOG_LEVEL, "Queue " + queueName + "'s backup is too long (" + size + ", " + eta + ")"
							+ " retrying crawl later.");
				}
				// TODO: get the request and pass it along properly. Currently it is not available
				// to this interceptor.
				// TODO:Report a different message when eta is exceeded
				throw QueueTaskRetryException.sizeExceeded(queueName, size, maxSize);
			}
		}
	}
}
