/**
 * 
 */
package com.aawhere.queue;

import com.aawhere.app.KnownInstance;
import com.aawhere.persist.objectify.IfInDev;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

/**
 * Guice Module for com.aawhere.cache
 * 
 * @author Brian Chapman
 * 
 */
public class QueueModule
		extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		// local development servers have a problem with the queue so ignore. Appspot is fine.
		if (!(KnownInstance.TRAIL_NETWORK_DEVSERVER.isThisSystem())) {
			ThrottleQueueInterceptor throttleQueueInterceptor = new ThrottleQueueInterceptor();
			requestInjection(throttleQueueInterceptor);
			bindInterceptor(Matchers.any(), Matchers.annotatedWith(ThrottleQueue.class), throttleQueueInterceptor);
		}
	}
}
