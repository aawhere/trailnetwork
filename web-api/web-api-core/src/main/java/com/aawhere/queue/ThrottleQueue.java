/**
 * 
 */
package com.aawhere.queue;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that the *WebApi method should run if the other queues (as defined by the queue
 * parameter) are not backed up too far. If the queues are backed up, this throws a 503 Service
 * Unavailable ( {@link QueueTaskRetryException}).
 * 
 * @see ThrottleQueueInterceptor
 * @see QueueTaskRetryException
 * 
 * @author Brian Chapman
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ThrottleQueue {

	/**
	 * Include any queue that should be ignored. The queue where the tasks are being queued must be
	 * in this list if it is also in the {@link #queues()} or the default queues to allow
	 * consumption if the number of throttled tasks exceeds the max size.
	 * 
	 * @return
	 */
	public String[] ignore() default {};

	/**
	 * If any queue, besides the {@link #ignore()} given, exceeds the given {@link #maxSize()} the
	 * denial will be provided.
	 * 
	 * @return
	 */
	public int maxSize() default ThrottleQueueInterceptor.QUEUE_MAX_QUEUE_LENGTH_FOR_CRAWL;

	/**
	 * The queues to consider. This is the whitelist. The default is to consider all queues known to
	 * the queue.
	 * 
	 * FIXME:All queues may not work with GAE. It may be better to explicitly describe your target
	 * queues. TN-510
	 * 
	 * @see ThrottleQueueInterceptor#ALL_QUEUES
	 * @return
	 */
	public String[] queues() default {};// unfortunately unable to reference constant
}
