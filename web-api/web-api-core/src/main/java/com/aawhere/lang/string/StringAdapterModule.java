/**
 *
 */
package com.aawhere.lang.string;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

/**
 * Guice Module for configuring string adapters
 * 
 * @author Brian Chapman
 * 
 */
public class StringAdapterModule
		extends AbstractModule {

	@Override
	protected void configure() {
		bind(StringAdapterFactory.class).toProvider(StringAdapterFactoryProvider.class).in(Singleton.class);
	}

}