/**
 *
 */
package com.aawhere.lang.string;

import com.aawhere.field.FieldDictionaryFactory;
import com.aawhere.joda.time.format.DateTimeStringAdapter;
import com.aawhere.measure.QuantityStringAdapter;
import com.aawhere.persist.FilterCondition;
import com.aawhere.persist.FilterConditionStringAdapter;
import com.aawhere.persist.FilterConditionSupportedTypeRegistry;

import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Provides and configures a string adapter when requested.
 * 
 * @author Brian Chapman
 * 
 */
public class StringAdapterFactoryProvider
		implements Provider<StringAdapterFactory> {

	private FieldDictionaryFactory fieldDictionaryFactory;
	private FilterConditionSupportedTypeRegistry filterConditionSupportedTypesRegistry;

	/**
	 * Setup Field Dictionary
	 */
	@Inject
	StringAdapterFactoryProvider(FieldDictionaryFactory fieldDictionaryFactory,
			FilterConditionSupportedTypeRegistry filterConditionSupportedTypesRegistry) {
		this.fieldDictionaryFactory = fieldDictionaryFactory;
		this.filterConditionSupportedTypesRegistry = filterConditionSupportedTypesRegistry;
	}

	@Override
	public StringAdapterFactory get() {
		StringAdapterFactory stringAdapterFactory = new StringAdapterFactory().registerStandardAdapters();
		stringAdapterFactory.register(new DateTimeStringAdapter());
		stringAdapterFactory.register((QuantityStringAdapter<?>) new QuantityStringAdapter.Builder().build());
		stringAdapterFactory.register(new FromStringAdapter.Builder<FilterCondition>().setType(FilterCondition.class)
				.build());

		FilterConditionStringAdapter<Object> filterConditionAdapter = new FilterConditionStringAdapter.Builder<Object>()
				.setAdapterFactory(stringAdapterFactory).setDictionaryFactory(this.fieldDictionaryFactory)
				.setRegistry(filterConditionSupportedTypesRegistry).build();
		stringAdapterFactory.register(filterConditionAdapter);
		return stringAdapterFactory;
	}

}
