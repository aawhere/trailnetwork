/**
 *
 */
package com.aawhere.web.api;

import javax.ws.rs.core.UriBuilder;

import org.junit.Before;

import com.sun.jersey.api.client.Client;

/**
 * Parent class for all WebApiTests.
 * 
 * The default port is 8888 which is the Eclipse port for GAE.
 * 
 * Maven invoked tests will change the host to 8080 to avoid collisions should Eclipse still be
 * running.
 * 
 * You may run these tests against any deployment by setting the system property
 * {@value #WEB_API_TEST_HOST_KEY}.
 * 
 * @author Aaron Roller
 * 
 */
public class BaseWebApiTest {
	/**
	 *
	 */
	private static final String WEB_API_TEST_HOST_KEY = "webApiTestHost";

	public static String GAE_HOST = System.getProperty(WEB_API_TEST_HOST_KEY, "http://localhost:8888");

	protected Client c = Client.create();
	protected UriBuilder uriBuilder;

	@Before
	public void setUp() {
		uriBuilder = UriBuilder.fromUri(GAE_HOST);
	}
}
