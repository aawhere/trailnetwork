/**
 * 
 */
package com.aawhere.web.api;

import static org.junit.Assert.*;

import java.net.URI;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.junit.Ignore;
import org.junit.Test;

import com.aawhere.lang.string.StringAdapterFactory;

import com.google.inject.Provider;

/**
 * @see CollectionStringAdapterQueryParamInjector
 * 
 * @author aroller
 * 
 */

public class CollectionStringAdapterQueryParamInjectorUnitTest {

	@Test
	@Ignore("working through bugs...this should work")
	public void testHandles() {
		StringAdapterFactory factory = new StringAdapterFactory();
		Provider<UriInfo> uriInfo = getProvider();
		CollectionStringAdapterQueryParamInjector injector = new CollectionStringAdapterQueryParamInjector(uriInfo,
				factory);
		Object object = new Integer(7);
		assertTrue(injector.handlesType(null, null, object.getClass()));
		assertFalse(injector.handlesType(null, null, getClass()));
	}

	@Test
	@Ignore("FIXME:This is too difficult to implement at this time and may not be worth it since class impl is simple")
	public void testGet() {

	}

	/**
	 * @return
	 */
	private Provider<UriInfo> getProvider() {
		return new Provider<UriInfo>() {
			public UriInfo get() {

				return new MockUriInfo();
			};
		};
	}

	public static class MockUriInfo
			implements UriInfo {

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getPath()
		 */
		@Override
		public String getPath() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getPath(boolean)
		 */
		@Override
		public String getPath(boolean decode) {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getPathSegments()
		 */
		@Override
		public List<PathSegment> getPathSegments() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getPathSegments(boolean)
		 */
		@Override
		public List<PathSegment> getPathSegments(boolean decode) {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getRequestUri()
		 */
		@Override
		public URI getRequestUri() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getRequestUriBuilder()
		 */
		@Override
		public UriBuilder getRequestUriBuilder() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getAbsolutePath()
		 */
		@Override
		public URI getAbsolutePath() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getAbsolutePathBuilder()
		 */
		@Override
		public UriBuilder getAbsolutePathBuilder() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getBaseUri()
		 */
		@Override
		public URI getBaseUri() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getBaseUriBuilder()
		 */
		@Override
		public UriBuilder getBaseUriBuilder() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getPathParameters()
		 */
		@Override
		public MultivaluedMap<String, String> getPathParameters() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getPathParameters(boolean)
		 */
		@Override
		public MultivaluedMap<String, String> getPathParameters(boolean decode) {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getQueryParameters()
		 */
		@Override
		public MultivaluedMap<String, String> getQueryParameters() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getQueryParameters(boolean)
		 */
		@Override
		public MultivaluedMap<String, String> getQueryParameters(boolean decode) {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getMatchedURIs()
		 */
		@Override
		public List<String> getMatchedURIs() {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getMatchedURIs(boolean)
		 */
		@Override
		public List<String> getMatchedURIs(boolean decode) {
			throw new RuntimeException("TODO:Implement this");
		}

		/*
		 * (non-Javadoc)
		 * @see javax.ws.rs.core.UriInfo#getMatchedResources()
		 */
		@Override
		public List<Object> getMatchedResources() {
			throw new RuntimeException("TODO:Implement this");
		}

	}

}
