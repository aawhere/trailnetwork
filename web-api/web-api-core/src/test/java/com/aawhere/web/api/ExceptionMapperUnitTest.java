/**
 *
 */
package com.aawhere.web.api;

import static org.junit.Assert.*;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.xml.bind.JAXBException;

import org.junit.Test;

import com.aawhere.lang.InvalidChoiceException;
import com.aawhere.net.GeneralHttpException;
import com.aawhere.net.HttpStatusCode;
import com.aawhere.test.TestUtil;
import com.aawhere.util.rb.ExampleMessage;
import com.aawhere.ws.ErrorResponse;
import com.aawhere.xml.bind.JAXBReadException;

/**
 * @author Aaron Roller
 * 
 */
public class ExceptionMapperUnitTest {

	private InvalidChoiceException exception = new InvalidChoiceException("junk");
	private HttpStatusCode exceptionCode = InvalidChoiceException.CODE;

	@Test
	public void testBaseException() {
		JAXBReadException exception = new JAXBReadException(ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE);
		assertMapper(new BaseExceptionMapper(), exception, HttpStatusCode.UNPROCESSABLE_ENTITY);
	}

	@Test
	public void testBaseRuntimeException() {
		InvalidChoiceException exception = new InvalidChoiceException("junk");
		assertMapper(new BaseExceptionMapper(), exception, InvalidChoiceException.CODE);
	}

	@Test
	public void testBaseRuntimeAsWebApplicationException() {

		assertAsWebApplicationException(new BaseExceptionMapper().asWebApplicationException(exception),
										exception,
										InvalidChoiceException.CODE);
	}

	/**
	 * If a JAXBException is thrown then we should unwrap it and report it with our goodness.
	 * 
	 */
	@Test
	public void testJAXBExceptionWrapper() {
		JAXBException jaxbException = new JAXBException(exception);
		assertWrapper(jaxbException);
	}

	@Test
	public void testJAXBExceptionTwiceWrapper() {
		JAXBException jaxbException = new JAXBException(exception);
		assertWrapper(new RuntimeException(jaxbException));
	}

	@Test
	public void testRuntimeExceptionTwiceWrapper() {
		RuntimeException one = new RuntimeException(exception);
		assertWrapper(new RuntimeException(one));
	}

	/**
	 * Jersey will wrap our exceptions in some cases hiding our informaton. this ensures we expose
	 * the correct exception.
	 * 
	 */
	@Test
	public void testWrappedMessageExceptionExposed() {
		WebApplicationException wrapper = new WebApplicationException(exception);
		assertWrapper(wrapper);
	}

	/**
	 * @param wrapper
	 */
	public void assertWrapper(Exception wrapper) {
		assertEquals("wrapped didn't get unwrapped", exceptionCode.value, new BaseExceptionMapper().toResponse(wrapper)
				.getStatus());
	}

	@Test
	public void testBaseAsWebApplicationException() {
		JAXBReadException exception = new JAXBReadException(ExampleMessage.MESSAGE_PROVIDED_NOT_IN_FILE);
		assertAsWebApplicationException(new BaseExceptionMapper().asWebApplicationException(exception),
										exception,
										HttpStatusCode.UNPROCESSABLE_ENTITY);
	}

	@Test
	public void testStatusCodeContainer() {
		final HttpStatusCode expectedCode = HttpStatusCode.CONFLICT;
		GeneralHttpException exception = new GeneralHttpException(expectedCode);
		assertMapper(new BaseExceptionMapper(), exception, expectedCode);
	}

	public static <E extends Exception> void assertMapper(ExceptionMapper<E> mapper, E exception, HttpStatusCode code) {
		Response response = mapper.toResponse(exception);
		assertResponse(response, exception, code);
	}

	/**
	 * @param response
	 * @param exception
	 * @param code
	 */
	public static <E extends Exception> void assertResponse(Response response, E exception, HttpStatusCode code) {
		assertEquals(code.value, response.getStatus());
		final Object entity = response.getEntity();
		TestUtil.assertInstanceOf(ErrorResponse.class, entity);
		final ErrorResponse errorResponse = (ErrorResponse) entity;
		assertEquals(exception.getMessage(), errorResponse.message());
	}

	public static void assertAsWebApplicationException(WebApplicationException exception, Exception cause,
			HttpStatusCode code) {
		assertResponse(exception.getResponse(), cause, code);
	}
}
