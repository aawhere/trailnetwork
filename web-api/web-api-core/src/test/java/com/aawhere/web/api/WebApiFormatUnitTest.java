/**
 * 
 */
package com.aawhere.web.api;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Aaron Roller
 * 
 */
public class WebApiFormatUnitTest {

	@Test
	public void testFormatFound() {
		WebApiFormat expected = WebApiFormat.JSON;
		WebApiFormat actual = WebApiFormat.valueOf(expected.name());
		assertEquals(expected, actual);
		// test lower case
		actual = WebApiFormat.find(expected.name().toLowerCase());
		assertEquals(expected, actual);
	}

	@Test(expected = UnsupportedApiFormatException.class)
	public void testFormatNotFound() {
		WebApiFormat.find("junk@$@");
	}

	@Test
	public void testExceptionMessage() {
		String format = "junk";
		UnsupportedApiFormatException exception = new UnsupportedApiFormatException(format,
				(Object[]) WebApiFormat.values());
		String message = exception.getMessage();
		assertTrue(message, message.contains(format));
		assertTrue(message, message.contains("xml"));
	}
}
