/**
 * 
 */
package com.aawhere.web.api.client;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.aawhere.test.TestUtil;
import com.aawhere.test.category.WebClientTest;
import com.aawhere.web.api.client.WebApiClientHelper.Builder.OutputType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

/**
 * @see WebApiClientHelper
 * 
 * @author Aaron Roller
 * 
 */
// TODO: categories do not appear to work from the method annotation. Ideally this annotation would
// be removed and the one on testStringOutput would remain.
@Category(WebClientTest.class)
public class WebApiClientHelperClientTest {

	/**
	 * @see WebApiClientHelper#getLoggingOutput()
	 * @see WebApiClientHelper.Builder.OutputType#STRING
	 */
	@Test
	@Category(WebClientTest.class)
	public void testStringOutput() {
		final OutputType output = OutputType.STRING;
		WebApiClientHelper helper = new WebApiClientHelper.Builder().setLogOutput(output).build();
		Client client = helper.getClient();
		WebResource resource = client.resource("http://www.google.com");
		String results = resource.get(String.class);
		final String searchItem = "Google";
		TestUtil.assertContains(results, searchItem);
		TestUtil.assertContains(helper.getLoggingOutput(), searchItem);
	}

}
