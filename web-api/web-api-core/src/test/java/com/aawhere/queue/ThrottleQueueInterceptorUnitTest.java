/**
 * 
 */
package com.aawhere.queue;

import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.aawhere.queue.QueueTestUtil.TestQueue;

import com.google.appengine.repackaged.com.google.common.collect.Sets;

/**
 * Tests the functionality of the throttling of the queue without the Interceptor features, only the
 * logic of queue allowance or denial.
 * 
 * @author aroller
 * 
 */
public class ThrottleQueueInterceptorUnitTest {

	private ThrottleQueueInterceptor interceptor;
	private TestQueue targetQueue;
	private TestQueue anotherQueue;
	private int maxSize;
	private Set<String> queueNames;
	private String targetQueueName = "target";
	private Set<String> ignores = Sets.newHashSet(targetQueueName);
	private String anotherQueueName = "another";
	private boolean failureExpected = false;
	private String notWhitelistedQueueName = "notWhitelisted";
	private TestQueue notWhitelistedQueue;

	@Before
	public void before() {
		QueueFactory factory = QueueTestUtil.getQueueFactory();
		// creates the queues
		this.anotherQueue = (TestQueue) factory.getQueue(anotherQueueName);
		this.targetQueue = (TestQueue) factory.getQueue(targetQueueName);
		this.notWhitelistedQueue = (TestQueue) factory.getQueue(this.notWhitelistedQueueName);
		this.interceptor = new ThrottleQueueInterceptor(factory);
		this.queueNames = Collections.emptySet();
		this.maxSize = 3;
	}

	@org.junit.After
	public void after() {
		try {
			interceptor.throttle(queueNames, maxSize, ignores);
			assertFalse("throttle failed to throw exception", this.failureExpected);
		} catch (QueueTaskRetryException e) {
			assertTrue(e.getMessage(), this.failureExpected);
		}
	}

	@Test
	public void withinLimits() {
		QueueTestUtil.populate(this.anotherQueue, this.maxSize);
	}

	@Test
	public void targetQueueExceedLimits() {
		QueueTestUtil.populate(this.targetQueue, this.maxSize + 1);
	}

	@Test
	public void anotherQueueExceedLimits() {
		QueueTestUtil.populate(this.anotherQueue, this.maxSize + 1);
		this.failureExpected = true;
	}

	/**
	 * Tests that when providing custom queue names this will detect size exceeded.
	 * 
	 */
	@Test
	public void testProvideQueueNames() {
		QueueTestUtil.populate(anotherQueue, maxSize + 1);
		this.queueNames = Sets.newHashSet(this.anotherQueueName);
		this.failureExpected = true;
	}

	/**
	 * Tests that when providing custom queue names this will detect size exceeded.
	 * 
	 */
	@Test
	public void testProvideQueueNamesWhileNonWhitelistExceeded() {
		QueueTestUtil.populate(this.notWhitelistedQueue, maxSize + 1);
		this.queueNames = Sets.newHashSet(this.anotherQueueName);
	}
}
